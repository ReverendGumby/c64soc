Ran https://github.com/christopherpow/nes-test-roms/tree/master/ppu_vbl_nmi on mine and (SW emulator) Mesen.

First error in 02-vbl_set_time.s. Fixed by prioritizing VBL_FLAG clear over set.

Second error in 10-even-odd-timing.
Mesen logs explain what's expected:

	set_test 2,"Clock is skipped too soon, relative to enabling BG"

E41A  $8C $01 $20  STY $2001                       A:FF X:00 Y:08 P:84 SP:FB CYC:328 SL:-1  FC:1490 CPU Cycle:44343399  Enable BG @337-339
E41D  $08          PHP                             A:FF X:00 Y:08 P:84 SP:FB CYC:340 SL:-1  FC:1490 CPU Cycle:44343403  NOT skipped
E41E  $48          PHA                             A:FF X:00 Y:08 P:84 SP:FA CYC:8   SL:0   FC:1490 CPU Cycle:44343406
:
E467  $8C $01 $20  STY $2001                       A:00 X:00 Y:08 P:06 SP:FB CYC:328 SL:-1  FC:1493 CPU Cycle:44432741  Enable BG @337-339
E46A  $08          PHP                             A:00 X:00 Y:08 P:06 SP:FB CYC:0   SL:0   FC:1493 CPU Cycle:44432745  skipped

	set_test 3,"Clock is skipped too late, relative to enabling BG"

E41A  $8C $01 $20  STY $2001                       A:FF X:00 Y:08 P:84 SP:FB CYC:329 SL:-1  FC:1523 CPU Cycle:45326161  Enable BG @338-340
E41D  $08          PHP                             A:FF X:00 Y:08 P:84 SP:FB CYC:0   SL:0   FC:1523 CPU Cycle:45326165  NOT skipped
E41E  $48          PHA                             A:FF X:00 Y:08 P:84 SP:FA CYC:9   SL:0   FC:1523 CPU Cycle:45326168
:
E467  $8C $01 $20  STY $2001                       A:00 X:00 Y:08 P:06 SP:FB CYC:329 SL:-1  FC:1526 CPU Cycle:45415503  Enable BG @338-340
E46A  $08          PHP                             A:00 X:00 Y:08 P:06 SP:FB CYC:0   SL:0   FC:1526 CPU Cycle:45415507  NOT skipped

	set_test 4,"Clock is skipped too soon, relative to disabling BG"

E41A  $8C $01 $20  STY $2001                       A:FF X:08 Y:00 P:84 SP:FB CYC:328 SL:-1  FC:1555 CPU Cycle:46279142  Disable BG @337-339
E41D  $08          PHP                             A:FF X:08 Y:00 P:84 SP:FB CYC:340 SL:-1  FC:1555 CPU Cycle:46279146  NOT skipped
E41E  $48          PHA                             A:FF X:08 Y:00 P:84 SP:FA CYC:8   SL:0   FC:1555 CPU Cycle:46279149
:
E467  $8C $01 $20  STY $2001                       A:00 X:08 Y:00 P:06 SP:FB CYC:328 SL:-1  FC:1558 CPU Cycle:46368484  Disable BG @337-339
E46A  $08          PHP                             A:00 X:08 Y:00 P:06 SP:FB CYC:340 SL:-1  FC:1558 CPU Cycle:46368488  NOT skipped
E46B  $48          PHA                             A:00 X:08 Y:00 P:06 SP:FA CYC:8   SL:0   FC:1558 CPU Cycle:46368491

	set_test 5,"Clock is skipped too late, relative to disabling BG"

E41A  $8C $01 $20  STY $2001                       A:FF X:08 Y:00 P:84 SP:FB CYC:329 SL:-1  FC:1589 CPU Cycle:47291685  Disable BG @338-340(0)
E41D  $08          PHP                             A:FF X:08 Y:00 P:84 SP:FB CYC:1   SL:0   FC:1589 CPU Cycle:47291689  skipped
E41E  $48          PHA                             A:FF X:08 Y:00 P:84 SP:FA CYC:10  SL:0   FC:1589 CPU Cycle:47291692
:
E467  $8C $01 $20  STY $2001                       A:00 X:08 Y:00 P:06 SP:FB CYC:330 SL:-1  FC:1592 CPU Cycle:47381027  Disable BG @339-341(0)
E46A  $08          PHP                             A:00 X:08 Y:00 P:06 SP:FB CYC:1   SL:0   FC:1592 CPU Cycle:47381031  NOT skipped

In summary:
1. BKG_EN,SPR_EN registers are updated (observed by the rest of the chip) on nCS deassertion.
2. A write cycle to BKG_EN,SPR_EN ($2001[3,4]) that starts on C337 is observed at end of C339.
3. If rendering is disabled at the end of R261/C339, then C340 is not skipped.

Fixed 1. the register update logic (referred to Visual2C02)

For 2. scoping CPU clocks/data and PPU COL/nCS/RENDER_EN [test10-3_01.PNG] shows that PPU is late by two cycles:
  In test 3 @E41A, CP2- (and thus nCS-) on write cycle of STY $2001 aligns with C338 instead of 340

Yet our monitor says this:
  $ where
  E41A: 8C0120 STY $2001         A=FF X=00 Y=08 PF=czIdBvN  PPU=C329,R261,F01497   CCy=44581644
The PPU state (C329) was latched in ICD on CP2-, which is 2 PPU cycles after CP1+.

Fixed the ICD to report PPU state at CP1+ (+1 CLK, because CC+ and CP1+ coincide).
Now monitor confirms, along with scope [test10-3_02.PNG] that PPU is early by one cycle.
  In test 3 @E41A, CP2- (and thus nCS-) on write cycle of STY $2001 aligns with C339 instead of C340
  $ where
  E41A: 8C0120 STY $2001         A=FF X=00 Y=08 PF=czIdBvN  PPU=C327,R261,F01526   CCy=45445282
  E41D: 08     PHP               A=FF X=00 Y=08 PF=czIdBvN  PPU=C339,R261,F01526   CCy=45445286

Timing is established in sync_vbl_timing. We're two PPU clocks early just before it returns:
  Mesen:
    E232  $68          PLA                             A:00 X:08 Y:00 P:87 SP:F6 CYC:10  SL:241 FC:1548 CPU Cycle:46068298
  Us:
    E232: 68     PLA               A=00 X=00 Y=00 PF=cZIdBvN  PPU=C008,R241,F00216   CCy=6430237

----------------------------------------------------------------------

Explore CPU-PPU clock alignment space
Try different initial values of div4_clk in rp2c02/clkgen.v:

  2'b00     Failed 10-even-odd-timing, #3: "08 07 / Clock is skipped too late, relative to enabling BG"
  2'b01     Failed 10-even-odd-timing, #3: "08 07 / Clock is skipped too late, relative to enabling BG"
  2'b10     Failed 02-vbl_set_time: "998398C3"; T+ 03 and 04 are both -- --
  2'b11     Failed 02-vbl_set_time: "998398C3"; T+ 03 and 04 are both -- --

Run the standalone ROM 10-even_odd_timing.nes:

  2'b0x     (presumed same as above)
  2'b1x     Failed 10-even-odd-timing, #3: "08 07 / Clock is skipped too late, relative to enabling BG"

----------------------------------------------------------------------

Digging down into microcycle timing on Visual2A03, we see that the
PHI2 pad (aka M2) goes high 1.5x CLK before CP2. That pad drives the
address decoder and ultimately PPU nCS. Its duty cycle is 62.5%. An
earlier nCS will advance when the PPU latches VBL_FLAG for readout.

Now we're just one PPU clock early in sync_vbl_timing:

  Mesen:
    E232  $68          PLA                             A:00 X:00 Y:08 P:86 SP:F6 CYC:10  SL:241 FC:13 CPU Cycle:354983
  Us:
    E232: 68     PLA               A=00 X=00 Y=08 PF=cZIdBvN  PPU=C009,R241,F00020   CCy=593228

And we managed to overflow COL on test 3:
  E41D: 08     PHP               A=FF X=00 Y=08 PF=czIdBvN  PPU=C340,R261,F00060   CCy=1786838
  E41E: 48     PHA               A=FF X=00 Y=08 PF=czIdBvN  PPU=C349,R261,F00060   CCy=1786841

Fixed that. Test 10 passed! [test10-3_03.PNG]
But test 2 failed (T+ 03 and 04 are both - -).

----------------------------------------------------------------------

Do the back-to-back read of $2002 where VBL_FLAG normally gets set.

Going back to Visual2C02
Edit reset state to start on hpos=0x150 vpos=0x0f0
file:///Users/dhunter/c64/visual2c02/Visual%202C02%20in%20JavaScript.html?graphics=false&logmore=vbl_flag,set_vbl_flag,io_ce,io_db7&c=001d32000018320000ff
START is the "1d" that controls the CPU-PPU sync and first pixel that sees nCS-

                       $2002R
START       COL+       1 2
-----       ---        ------
.. - 1D     340+5/8    - V
1E - ..     340+6/8    - -
.. - 28     001+0/8    - -
29 - ..     001+1/8    V -

If START = 1E..20, then $2002R will read clear twice for two neighboring COL, and test will fail.
This would occur with CPU-PPU alignments where M2+ is CC+ - (1-0 PCLK), or CP1+ is CC+ - (1.5-0.5 PCLK).

Fixed it. Adjusted initial div4_clk to 2'b10.
02- and 10- pass, 03-vbl_clear_time fails. Fixed that, too.
