References
==========
https://en.wikipedia.org/wiki/Nintendo_Entertainment_System
http://visual6502.org/images/pages/Nintendo_RP2A_die_shots.html
http://www.qmtpro.com/~nes/chipimages/index.php


Core Design
===========

- Ricoh CPU + SPU, PPU
- Dedicated memory buses and on-board RAM for CPU and PPU:
  - "work" RAM: 2K SRAM on address/data bus
  - VRAM, tile RAM: 2K SRAM on "BUFF" address/data bus
  - Both buses route to cartridge
- 2x controller ports
  - use D0-4 and port enables NP0-1


Core Components
===============

Ricoh RP2A03 - CPU + SPU
------------

Features:
- CPU is physical copy of MOS 6502
  - BCD status flag disabled (5x Qs deleted)
- SPU (Sound Processing Unit)
  - Five channels:
    - 2x pulse
    - 1x triangle
    - 1x noise
    - 1x LPCM
- sprite DMA, frame counter

References:
- Visual6502-style simulator: http://www.qmtpro.com/~nes/chipimages/visual2a03/
- http://www.vgmpf.com/Wiki/index.php?title=RP2A03
- https://www.nesdev.com/2A03%20technical%20reference.txt


Ricoh RP2C02 - PPU
------------

Features:
- Interal RAM:
  - 256B OAM (object attribute memory)
  - 28B palette RAM
- External RAM:
  - 2K external VRAM (on MLB)
  - 8K external tile RAM (on cartridge)
- Color:
  - NTSC-based palette: 48 colors + 6 grays
  - 25 dedicated color registers:
    - 1x BG
    - 4x sets of 3x tile colors
    - 4x sets of 3x sprite colors
- Sprites:
  - 64 sprites
- Buses
  - External RAM: 8-bit data, 13-bit address. A/D0-7 multiplexed
  - Registers: 8-bit data, 3-bit address.
  
- Resolution: 256 x 240

References:
- Visual6502-style simulator: http://www.qmtpro.com/~nes/chipimages/visual2c02/
- https://wiki.nesdev.com/w/index.php?title=PPU
- https://www.nesdev.com/2C02%20technical%20reference.TXT
