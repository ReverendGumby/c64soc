`timescale 1us / 1ns

// Studying the CPU schematic clock generator

// Reference: https://github.com/rgalland/SNES_S-CPU_Schematics/

module clk();

initial begin
  $timeformat(-6, 0, " us", 1);

  $dumpfile("clk.vcd");
  $dumpvars();
end

reg         nres, clk;
reg [8:0]   sr;
wire        sri, src;
wire        src_fast, src_norm, src_slow;
reg         slow_cpu, fast_cpu;
reg         refresh_en;
reg         jaff;
wire        aclk;

initial begin
  nres = 0;
  clk = 0;
  slow_cpu = 1;
  fast_cpu = 0;
  refresh_en = 0;
end

initial forever begin :clkgen
  #0.5 clk = ~clk;
end

always @(posedge clk or negedge nres) begin
  if (~nres)
    sr <= 0;
  else
    sr <= {sr[7:0], sri};
end

// refresh_en forces rate to /8 (WRAM rate)
// latched on falling edge of aclk

always @(posedge clk or negedge nres) begin
  if (~nres)
    jaff <= 0;
  else if (sr[1] & ~sr[2])
    jaff <= refresh_en;
end

assign src_fast = fast_cpu & ~jaff;
assign src_norm = ~((slow_cpu | fast_cpu) & ~jaff) & sr[4];
assign src_slow = slow_cpu & sr[4] & sr[6] & sr[8];
assign src = src_fast | src_norm | src_slow;
assign sri = ~(sr[2] & src);

assign aclk = ~sr[2];

initial #0 begin
  repeat (3) @(posedge clk) ;
  nres <= 1;

  #20 refresh_en <= 1'b1;
  #25 refresh_en <= 1'b0;

  #50 $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s clk -o clk.vvp clk.sv && ./clk.vvp"
// End:
