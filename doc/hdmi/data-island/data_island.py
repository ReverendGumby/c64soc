#!/usr/local/bin python
# A study in the HDMI Data Island.

from __future__ import print_function
import collections

class Pixel(list):
    '''A Pixel occupies 3x 4-bit channels.'''
    def __str__(self):
        c0, c1, c2 = (self[0] >> 2) & 1, self[1], self[2]
        return 'x{0:1b}xx {1:04b} {2:04b}'.format(c0, c1, c2)

class Packet:
    num_subpackets = 4

    # Lengths do not include parity.
    header_length = 3
    subpacket_length = 7

    AllBytes = collections.namedtuple('AllBytes', ['hb', 'sb'])

    @staticmethod
    def parity(msg):
        '''Computes 8 parity bits for N message bits, using the
        polynomial x^8 + x^7 + x^6 + 1.

        '''
        pc = 0
        for x in msg:
            for b in range(8):
                di = (x >> b) & 1
                s = (pc & 1) ^ di
                pc = ((pc >> 1) & 0xff) ^ (0x83 * s)
                #print('{0} {1:08b}'.format(di, pc))
        return pc

    def __str__(self):
        ret = ''

        ab = self.get_all_bytes(parity=True)
        ret += 'HB[00:02]: {0[0]:02x} {0[1]:02x} {0[2]:02x} : {0[3]:02x}\n'.format(ab.hb)

        for i in range(len(ab.sb)):
            sb = ab.sb[i]
            bl = i * 7
            bh = bl + 6
            ret += 'PB[{0:02d}:{1:02d}]: {2[0]:02x} {2[1]:02x} {2[2]:02x} {2[3]:02x} {2[4]:02x} {2[5]:02x} {2[6]:02x} : {2[7]:02x}\n'.format(bl, bh, sb)

        return ret

    def get_header_bytes(self):
        '''Return HB0-2.'''
        pass

    def get_subpacket_bytes(self, i):
        '''Return SB0-6 for subpacket i.'''
        pass

    def add_parity(self, ba):
        '''Return a new list with the input list's array appended.'''
        return ba + [ Packet.parity(ba) ]

    def get_all_bytes(self, parity=True):
        '''Return all bytes in the packet.'''        

        hb = self.get_header_bytes()
        if parity:
            hb = self.add_parity(hb)

        nsp = Packet.num_subpackets
        sb = []
        for i in range(nsp):
            sb.append(self.get_subpacket_bytes(i))
            if parity:
                sb[i] = self.add_parity(sb[i])

        return Packet.AllBytes(hb, sb)


class PixelPacketGen:
    @staticmethod
    def encode(pkt):
        '''Convert packet from 31 octets to 32 pixels.'''

        ab = pkt.get_all_bytes()

        px = []
        for i in range(32):
            hbi = i % 8
            hby = i / 8
            sbi = (i * 2) % 8
            sby = (i * 2) / 8
            
            d0a2 = (ab.hb[hby] >> hbi) & 1
            db = [(s[sby] >> (sbi + 0)) & 1 for s in ab.sb]
            dc = [(s[sby] >> (sbi + 1)) & 1 for s in ab.sb]
            
            c0 = d0a2 << 2
            c1, c2 = 0, 0
            for b in range(len(ab.sb)):
                c1 |= db[b] << b
                c2 |= dc[b] << b

            px.append(Pixel([c0, c1, c2]))

        return px


class WidePacketWord(int):
    def __str__(self):
        return '{0:09x}'.format(self)


class WidePacketGen:
    @staticmethod
    def encode(pkt):
        '''Convert packet from 31 octets to 8 x 36 bits.'''

        ab = pkt.get_all_bytes()

        wa = []
        for i in range(8):
            w = 0
            for j in range(4):
                w |= ab.sb[j][i] << (j * 8)
            w |= ((ab.hb[i / 2] >> ((i % 2) * 4)) & 15) << 32

            wa.append(WidePacketWord(w))

        return wa

class NullPacket(Packet):
    def get_header_bytes(self):
        return [0] * Packet.header_length

    def get_subpacket_bytes(self, i):
        return [0] * Packet.subpacket_length


class TestPacket(Packet):
    def get_header_bytes(self):
        return [0x75, 0xf2, 0xcb]

    def get_subpacket_bytes(self, i):
        # Random data produced by rtl/hdmi_tx/tb/data_island_ecc_tb/pkt_source.v
        sb = [
            [0x00, 0xb0, 0xf0, 0x36, 0x33, 0xf9, 0x08],
            [0x66, 0xc4, 0x22, 0x0b, 0x80, 0x66, 0xda],
            [0x69, 0x3b, 0x51, 0x5f, 0xd9, 0xde, 0x27],
            [0x00, 0x58, 0x78, 0x9b, 0x19, 0x7c, 0x04],
        ]
        return sb[i]


def main():
    #pkt = NullPacket()
    pkt = TestPacket()
    print(pkt)

    print('Pixel packet:')
    px = PixelPacketGen.encode(pkt)
    for p in px:
        print(p)
    print()

    print('Wide packet:')
    wp = WidePacketGen.encode(pkt)
    for p in wp:
        print(p)

if __name__ == "__main__":
    main()
