#!/usr/local/bin python
# A user study in the HDMI InfoFrame.

from __future__ import print_function
import collections
import data_island

class InfoFramePacket(data_island.Packet):
    def __init__(self, hb, pb):
        self.hb = hb
        self.pb = pb + [0] * (28 - len(pb))

        s = sum(self.hb) + sum(self.pb[1:])
        self.pb[0] = (0x100 - s) & 0xff

    def get_header_bytes(self):
        return self.hb

    def get_subpacket_bytes(self, i):
        o = i * 7
        return self.pb[o:o+7]


AVIInfoFrame_fields = 'Y A B S C M R SC VIC PR'

AVIInfoFrame = collections.namedtuple('AVIInfoFrame', AVIInfoFrame_fields)

class AVIInfoFramePacket(InfoFramePacket):
    def __init__(self, info):

        f = info
        hb = [0x82, 0x02, 0x0d]
        pb = [0] * 6
        pb[1] = (f.Y << 5) | (f.A << 4) | (f.B << 2) | (f.S << 0)
        pb[2] = (f.C << 6) | (f.M << 4) | (f.R << 0)
        pb[3] = (f.SC << 0)
        pb[4] = (f.VIC << 0)
        pb[5] = (f.PR << 0)
        
        InfoFramePacket.__init__(self, hb, pb)


AudioInfoFrame_fields = 'CC CA LSV'

AudioInfoFrame = collections.namedtuple('AudioInfoFrame', AudioInfoFrame_fields)

class AudioInfoFramePacket(InfoFramePacket):
    def __init__(self, info):

        f = info
        hb = [0x84, 0x01, 0x0a]
        pb = [0] * 6
        pb[1] = (f.CC << 0)
        pb[4] = f.CA
        pb[5] = (f.LSV << 3)

        InfoFramePacket.__init__(self, hb, pb)


def process(title, pkt):
    print(title)
    print('-' * 40)
    print(pkt)
    print()

    print('Pixel packet:')
    px = data_island.PixelPacketGen.encode(pkt)
    for p in px:
        print(p)
    print()

    print('Wide packet:')
    wp = data_island.WidePacketGen.encode(pkt)
    for p in wp:
        print(p)
    print()

def main():
    avi = AVIInfoFramePacket(AVIInfoFrame(
        Y=0,                    # RGB
        A=0,                    # Active Format Information: not present
        B=0,                    # Bar Data not present
        S=2,                    # Scan information: underscanned
        C=0,                    # Colorimetry: No Data
        M=2,                    # 16:9
        R=0,                    # Active Portion AR: unused
        SC=0,                   # Non-Uniform Picture Scaling: none
        VIC=0,                  # Undefined video format
        PR=0,                   # Pixel Repetition Factor: no rep.
    ))
    process('AVIInfoFrame', avi)

    audio = AudioInfoFramePacket(AudioInfoFrame(
        CC=1,                   # Channel Count: 2
        CA=0,                   # Channel Allocation: 1=FL, 2=FR
        LSV=0,                  # Level Shift Value: 0dB
    ))
    process('AudioInfoFrame', audio)


if __name__ == "__main__":
    main()
