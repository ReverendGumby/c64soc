HDMI Transmitter Design
=======================

History
-------

In the start was the humble DVI transmitter. Given a pixel clock of
74.25 MHz, this block could drive out 720p60. All HDMI sinks tested
happily displayed its signal. Best of all, only the 4 LVDS pairs (3
data + clock) were required -- none of that EDID stuff was needed.

And this worked just fine for years... until your author decided, one
day, that what he REALLY wanted was to send the emulator's audio to a
conference room system. Thus the requirement for HDMI audio was born.


Blocks
------
                            -------------    --
                           |  Video data |-->| \
                            -------------    |  |--> TMDS encoder
 ---------------    --      -------------    |  |--> TMDS encoder
| Audio samples |-->| \    | Data island |   |  |--> TMDS encoder
 ---------------    |  |-->|   packet    |-->| /
|   InfoFrames  |-->| /    |    gen.     |   --
 ---------------    --      -------------     ^ encoder MUX
                     ^
                      Data island packet MUX

Timing Control
--------------

The video_timing module sources HSYNC/VSYNC and active video
timings. Down-counters for H/V blanking periods are exported
to the HDMI encoder MUX, so that it can select between the three
encoding periods: data island, video data, and control.

Design choices made to meet HDMI timing requirements:
- InfoFrame packets are transmitted once every frame


Packet format
-------------

INPUTs to data_island_gen and downstream:
- 4 subpackets * 8 bits + 4 bits pkt header = 36 bit wide bus
- 8 clocks total / packet
- Bitrate matches post-encoding transmission rate
- Bus partitioned by BCH block
- Header ordered LSB first: Byte 0 bits 3:0, Byte 0 bits 7:4, ...
- Last clocks contain BCH parity bits (BP)

_DATA[]         35:32       31:24       23:16       15:08       07:00
Subpacket                   3           2           1           0
Header          ^
BCH block       4           3           2           1           0
Total bits      32          64          64          64          64

Clock/_DATA[]   35:32       31:24       23:16       15:08       07:00
0               HB0[3:0]    PB21        PB14        PB7         PB0
1               HB0[7:4]    PB22        PB15        PB8         PB1
2               HB1[3:0]    PB23        PB16        PB9         PB2
3               HB1[7:4]    PB24        PB17        PB10        PB3
4               HB2[3:0]    PB25        PB18        PB11        PB4
5               HB2[7:4]    PB26        PB19        PB12        PB5
6               BP4[3:0]    PB27        PB20        PB13        PB6
7               BP4[7:4]    BP3         BP2         BP1         BP0


Signal nomenclature
-------------------

These control and data signals are used for packet data transfer from
a source (OUTPUT) to a sink (INPUT).

Name     Direction        Meaning
----     ---------        -------
*_VALID  Source -> Sink   Source has data to send.
*_LAST   Source -> Sink   End of packet has been reached.
*_NEXT   Sink -> Source   Sink is receiving data.
*_DATA   Source -> Sink   Carries a word of packet data.

When *_VALID is set, the source is ready to transmit data, and *_DATA
has a valid word of packet data. It is set while the source has any
data to send, and cleared when it does not.

When *_NEXT is set, the sink is receiving a word of packet
data. Unlike the AXI bus protcol, *_NEXT is NOT set when *_VALID is
clear.


References
----------

[1] High-Definition Multimedia Interface, Specification Version 1.1 (May 20, 2004)
