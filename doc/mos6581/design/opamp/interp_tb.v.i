/*
 To compute actual vs. expected interpretation results:
   make @base@
 To list the largest errors:
   make @base@ | sort -g -k 9 | tail
 */

module @base@_tb
  (
   );

localparam CCNT = @CCNT@;
localparam CSELW = @CSELW@;
localparam NUM_IM = @NUM_IM@;
localparam IW = @IW@;
localparam OW = @OW@;

reg [OW-1:0] im [0:CCNT-1][0:NUM_IM-1];

initial $readmemh("@base@_tb_im.hex", im);

reg clk;
reg iclken;
reg [CSELW-1:0] csel;
reg [IW-1:0] in;
wire oclken;
wire [OW-1:0] out;

initial begin
  clk = 1'b0;
  iclken = 1'b0;
end
always #1
  clk = ~clk;

@base@ #(CSELW,IW,OW) @base@
  (
   .CSEL(csel[CSELW-1:0]),
   .I(in),
   .O(out),
   .CLK(clk),
   .ICLKEN(iclken),
   .OCLKEN(oclken)
   );

task go;
input [CSELW-1:0] csel;
input [IW-1:0] inp;
reg [OW-1:0] exp_out;
reg signed [OW:0] err;
  begin
    while (!clk)
      @(posedge clk) ;

    in <= inp;
    iclken <= 1'b1;
    @(posedge clk) ;

    iclken <= 1'b0;
    while (!(clk && oclken))
      @(posedge clk) ;

    exp_out = im[csel][inp];
    err = out - exp_out;
    $display("%2d csel %10d in %10d out %10d want %10d err", 
             csel, in, out, exp_out, err);
  end
endtask

integer i;
initial begin
  for (csel = 0; csel < CCNT; csel = csel + 1) begin
    for (i = 0; i < NUM_IM; i = i + 100) begin
      go(csel, i);
    end
    go(csel, NUM_IM - 1);
  end
  $finish();
end

endmodule
