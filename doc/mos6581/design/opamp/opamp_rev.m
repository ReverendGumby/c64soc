run ../svf/resid/filter_data.m

close all

%% Configuration

base='opamp_rev';
x=1:65536;

% size(m) = <x(end)> <# of curves>
m=double(mf.opamp_rev(x(1):x(end)))';
% Saturate the early values
m(1:1825) = m(1826);

x=x-1;
ix=[0 1824 2400 3330 5e3 8e3 12e3 13.4e3 15e3 16.1e3 17.4e3 18.2e3 ... 
    19e3 21e3 22e3 23e3 24e3 27e3 32e3 37e3 43e3 48e3 52e3 55e3 ...
    56e3 57e3 57.7e3 59e3 63710 x(end)];

who=mfilename('fullpath');
run interp_cmn.m
