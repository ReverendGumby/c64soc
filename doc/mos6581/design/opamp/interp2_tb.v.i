/*
 To compute actual vs. expected interpretation results:
   make @base@
 To list the largest errors:
   make @base@ | sort -g -k 9 | tail
 */

module @base@_tb
  (
   );

localparam NUM_IM = @NUM_IM@;

/* Defines SCNT, IXDIQ, ix_lut, iy_lut, ... */
`include "@base@.vh"

/* Defines the linear approximation interpolation function */
`include "interp2.vh"

reg [IOW-1:0] im [0:CCNT-1][0:NUM_IM-1];

initial $readmemh("@base@_tb_im.hex", im);

task go;
input [CSELW-1:0] csel;
input [IIW-1:0] in;
reg [IOW-1:0] exp_out;
reg [IOW-1:0] out;
reg signed [IOW:0] err;
  begin
    interp2(csel, in, out);
  
    exp_out = im[csel][in];
    if (exp_out != 1'dx) begin
      err = out - exp_out;
      $display("%2d csel %10d in %10d out %10d want %10d err", 
               csel, in, out, exp_out, err);
    end
  end
endtask

integer i, csel;
initial begin
  for (csel = 0; csel < CCNT; csel = csel + 1) begin
    for (i = 0; i < NUM_IM; i = i + 100) begin
      go(csel, i);
    end
    go(csel, NUM_IM - 1);
  end
  $finish();
end

endmodule
