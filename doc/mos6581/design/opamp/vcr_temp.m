%% A hack to tune in the offsets in vcr_n_ids_term

run ../svf/resid/filter_data.m

close all

%% Configuration

base='vcr_n_ids_term';
x=1:65536;

% size(m) = <x(end)> <# of curves>
m=double(f.vcr_n_Ids_term(x(1):x(end)))';

x=x-1;
ixdm = [
    0       0
    8495    0.01
    8826    -0.15
    9002    -0.13
    9212    -0.04
    9388    -0.14
    9543    -0.09
    9735    -0.09
    9918    -0.14
    10106   -0.14
    10.4e3  -0.54
    10.8e3  -0.5
    11.2e3  -0.5
    11.6e3  -0.5
    12e3    -3
    13e3    -3
    14e3    -3
    15e3    -3
    16e3    -3
    17e3    -3
    18e3    -3
    19e3    -3
    20e3    -3
    21e3    -3
    22e3    -3
    23e3    -3
    24e3    -3
    25e3    -3
    26e3    -3
    27e3    -3
    28e3    -3
    29e3    -3
    30e3    -3
    31e3    -3
    32e3    -3
    33e3    -3
    34e3    -3
    35e3    -3
    36e3    -3
    37e3    -3
    38e3    -3
    39e3    -3
    40e3    -3
    41e3    -3
    42e3    -3
    43e3    -3
    44e3    -3
    45e3    -3
    46e3    -3
    47e3    -3
    48e3    -3
    49e3    -3
    50e3    -3
    51e3    -3
    52e3    -3
    53e3    -3
    54e3    -3
    55e3    -9
    57e3    -10
    59e3    -10
    61e3    -10
    63e3    -10
    65e3    -10
    x(end)  0
];
    
ix = ixdm(:,1)';
dm = ixdm(:,2)';
nx = 8400:11000;

a = 2; % ix/dm(a)

figure

while true
    cla
    
    am = m;
    for i = 1:length(dm)
        am(ix(i)+1) = m(ix(i)+1) + dm(i);
    end

    im = zeros(size(m));
    for csel = 1:size(m,2)
        im(:,csel)=floor(interp1(ix,am(ix+1,csel),x) + 0.5);
    end

    e = im - m;
    plot(nx,e(nx),ix,dm,'go',ix(a),dm(a),'ro') %xxx
    xlim([nx(1) nx(end)])
    ylim([-2 2])
    
    waitforbuttonpress
    k = double(get(gcf,'CurrentCharacter'));
    switch k
      case 97 %a
        if a > 2
            a = a - 1;
        end
      case 115 %s
        if a < length(ix)
            a = a + 1;
        end
      case 104 %h
        ix(a) = ix(a) - 1;
      case 106 %j
        ix(a) = ix(a) + 1;
      case 107 %k
        dm(a) = dm(a) - 0.01;
      case 108 %l
        dm(a) = dm(a) + 0.01;
    end
end
