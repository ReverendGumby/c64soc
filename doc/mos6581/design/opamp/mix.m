run ../svf/resid/filter_data.m

close all

%% Configuration

base='mixer';
who=mfilename('fullpath');

ii = 1:7;

pmixer=nan(65536*7,7);   % padded to widest curve
smixer=nan(65536,7);     % scaled to narrowest curve
for i = ii
    j = i;
    s0 = mf.mixer_offsets(i + 1) + 1;
    s1 = s0 + 65536 * j - 1;
    dmixer(1:65536*j) = double(mf.mixer(s0:s1))';
    pmixer(1:s1-s0+1, i) = dmixer;
    smixer(:, i) = dmixer(1:j:65536*j);
    clear dmixer
end

%hold on
%for i = ii
%    subplot(2, 4, i)
%    plot(smixer(:, i))
%end
%hold off

m = pmixer;
ix = [
    0  5e3 10e3 15e3 20e3 25e3  30e3  35e3  40e3  45e3  48e3  51e3  54e3  57e3  60e3 65535*1;
    0  3e3  6e3  8e3 10e3 12e3  18e3  30e3  40e3  50e3  60e3  70e3  80e3  90e3 110e3 65535*2;
    0 13e3 16e3 19e3 23e3 27e3  50e3  70e3  90e3 100e3 106e3 115e3 130e3 145e3 160e3 65535*3;
    0 24e3 28e3 32e3 36e3 42e3  50e3  75e3 105e3 125e3 132e3 140e3 160e3 185e3 220e3 65535*4;
    0 32e3 42e3 48e3 52e3 59e3  75e3 110e3 150e3 160e3 175e3 200e3 225e3 250e3 300e3 65535*5;
    0 44e3 52e3 59e3 66e3 75e3  84e3 120e3 164e3 180e3 192e3 210e3 235e3 280e3 340e3 65535*6;
    0 55e3 70e3 76e3 83e3 95e3 110e3 155e3 190e3 205e3 212e3 225e3 250e3 300e3 350e3 65535*7;
    ];

%% Compute mixer linear approximation curve

nix = length(ix);
im = nan(size(m));
for csel = 1:size(m,2)
    x = 0:find(~isnan(m(:,csel)),1,'last')-1;
    im(x+1,csel)=floor(interp1(ix(csel,:),m(ix(csel,:)+1,csel),x) + 0.5);
end

figure
hold on
plot(m)
plot(im)
hold off

e = im - m;
figure
plot(e)

%% Generate mixer.vh and mixer_tb.v

IW=19;
OW=16;
ix = ix';
gen_interp2(base, who, IW, OW, ix, nix, m, im)
