run ../svf/resid/filter_data.m

close all

%% Configuration

who=mfilename('fullpath');
base='vcr_kvg';
x=1:65529;

% size(m) = <x(end)> <# of curves>
m=double(f.vcr_kVg(x(1):x(end)))';

x=x-1;
ix=[0 100 500 1e3 2e3 3e3 4e3 5e3 6e3 7e3 8e3 9e3 10e3 ...
    11e3 12e3 13e3 14.5e3 16e3 17.5e3 19e3 20.5e3 22e3 ...
    24e3 26e3 28e3 30e3 32e3 34e3 37e3 40e3 45e3 52e3 x(end)];

who=mfilename('fullpath');
run interp_cmn.m
