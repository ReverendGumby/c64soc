#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

//#define FULL 1

#ifdef FULL

#define FILT_POS            0                   // bin[pos] = filt[shift]
#define FILT_SHIFT          0
#define FILT_WIDTH          8
#define MODE_POS            (FILT_POS + FILT_WIDTH)
#define MODE_SHIFT          0
#define MODE_WIDTH          8

#else

#define FILT_POS            0                   // bin[pos] = filt[shift]
#define FILT_SHIFT          0
#define FILT_WIDTH          3
#define MODE_POS            (FILT_POS + FILT_WIDTH)
#define MODE_SHIFT          4
#define MODE_WIDTH          4

#endif

#define BIN_WIDTH           (MODE_WIDTH + FILT_WIDTH)
#define BIN_MAX             ((1 << BIN_WIDTH) - 1)
#define BIN(mode, filt)     (((mode) >> MODE_SHIFT << MODE_POS) | \
                             ((filt) >> FILT_SHIFT << FILT_POS))
#define BIN_EX(bin, pos, width, shift) \
    ((((bin) >> pos) & ((1 << width) - 1)) << shift)
#define BIN_TO_NAME(bin, name)  \
    BIN_EX(bin, name##_POS, name##_WIDTH, name##_SHIFT)
#define BIN_TO_MODE(bin)    BIN_TO_NAME(bin, MODE)
#define BIN_TO_FILT(bin)    BIN_TO_NAME(bin, FILT)

enum {
    BIT_X = 0,
    BIT_0,
    BIT_1,
    BITS
};

typedef uint8_t mix_t;
typedef uint16_t bin_t;

int PAT_NUM;

typedef struct {
    bin_t bin;
    bin_t msk;                                // bits unset => bin don't care
} pat_t;

static void pat_to_str(pat_t pat, char *str, int str_width,
                       int pos, int width, int shift)
{
    uint8_t bin = BIN_EX(pat.bin, pos, width, shift);
    uint8_t msk = BIN_EX(pat.msk, pos, width, shift);
    //printf("  pat={.bin=0x%02x,.msk=0x%02x} pws=%d,%d,%d bin=0x%02x msk=0x%02x\n", (int)pat.bin, (int)pat.msk, pos, width, shift, (int)bin, (int)msk);
    for (int b = str_width - 1; b >= 0; b--) {
        uint8_t m = 1 << b;
        char c = (msk & m) ? ((bin & m) ? '1' : '0') : 'x';
        *str++ = c;
    }
    *str = '\0';
}

static int pat_bin_match(pat_t pat, bin_t bin)
{
    return ((bin ^ pat.bin) & pat.msk) == 0;
}

/* Does 'x' match 'y', and is 'y' more specific? */
static int pat_pat_match(pat_t x, pat_t y)
{
    return (((x.bin ^ y.bin) & x.msk) == 0)
        && ((x.msk & y.msk) == x.msk);
}

static char bin_to_mix(bin_t bin)
{
    uint8_t mode = BIN_TO_MODE(bin);
    uint8_t filt = BIN_TO_FILT(bin);

    int enabled = 1;
    int voice_mask = 0xf0 | 0x07;

    return (enabled ? (mode & 0x70) | ((~(filt | (mode & 0x80) >> 5)) & 0x0f) : 0x0f)
        & voice_mask;
}

static int test_bin_mix(bin_t bin, mix_t wanted_mix)
{
    mix_t got_mix = bin_to_mix(bin);
    //printf("   bin=0x%x mix=%d<>%d\n", bin, (int)wanted_mix, (int)got_mix);
    return got_mix == wanted_mix;
}

static int test_pat(mix_t mix, pat_t pat)
{
    /* Find the x's in 'pat'. */
    char xs[BIN_WIDTH];
    int nxs = 0;
    for (int b = 0; b < BIN_WIDTH; b++) {
        if ((pat.msk & (1 << b)) == 0) {
            //printf("  xs[%d]=%d\n", nxs, b);
            xs[nxs++] = b;
        }
    }

    /* Special case: no x's. */
    if (nxs == 0) {
        return test_bin_mix(pat.bin, mix);
    }

    /* Fill in each x. */
    int num_xbin = (1 << (nxs - 1)) * 2;
    for (int xbin = 0; xbin < num_xbin; xbin++) {
        bin_t bin = pat.bin & pat.msk;
        for (int n = 0; n < nxs; n++)
            bin |= (xbin & (1 << n)) << (xs[n] - n);
        if (!test_bin_mix(bin, mix))
            return 0;
    }
    return 1;
}

static void print_state(int i, mix_t mix, pat_t pat, char *eol)
{
    const char bits = 8;
    char mode[bits + 1];
    char filt[bits + 1];
    pat_to_str(pat, mode, bits, MODE_POS, MODE_WIDTH, MODE_SHIFT);
    pat_to_str(pat, filt, bits, FILT_POS, FILT_WIDTH, FILT_SHIFT);
    printf("mix=8'h%02x: mode=8'b%s filt=8'b%s%s",
           (int)mix, mode, filt, eol);
    if (*eol == '\r')
        fflush(stdout);
}

static int update(int i)
{
    return i % (PAT_NUM / 10) == 0;
}

static void test_mix(mix_t mix)
{
    enum { UNKNOWN, YES, NO };
    typedef struct {
        pat_t pat;
        char result;
    } pats_t;

    pats_t *pats = calloc(sizeof(pats_t), PAT_NUM);
    pats_t *pats_end = pats + PAT_NUM;

    /* Build patterns for all bit combinations of {x,0,1} */
    for (int i = 0; i < PAT_NUM; i++) {
        pats_t *p = &pats[i];
        p->result = UNKNOWN;
        for (int b = 0, mb = i; b < BIN_WIDTH; b++, mb /= BITS) {
            char bit = mb % BITS;
            p->pat.msk |= (bit != BIT_X) << b;
            p->pat.bin |= (bit == BIT_1) << b;
        }
    }
    for (int i = 0; i < PAT_NUM; i++) {
        pats_t *p = &pats[i];
        //print_state(i, mix, p->pat, "\n");
        if (p->result != UNKNOWN)
            continue;
        if (update(i))
            print_state(i, mix, p->pat, "\r");
        if (test_pat(mix, p->pat)) {
            p->result = YES;
            print_state(i, mix, p->pat, "\n");
            /* Cross out all more specific patterns that match this one */
            pats_t *q = p;
            for (int j = i; j < PAT_NUM; j++, q++) {
                if (q->result == UNKNOWN) {
                    if (pat_pat_match(p->pat, q->pat)) {
                        //printf("X "); print_state(j, mix, q->pat, "\n");
                        q->result = NO;
                    }
                }
            }
        }
    }
    printf("%40s\r", "");
    fflush(stdout);

    free(pats);
}

int main()
{
    PAT_NUM = (int)pow(BITS, BIN_WIDTH);
    for (mix_t mix = 0; mix <= 0x7f; mix++) {
        test_mix(mix);
    }
    return 0;
}
