%% Compute linear approximation curves

if ~exist('am', 'var')
    am = m;
end

nix = length(ix);
im = zeros(size(m));
for csel = 1:size(m,2)
    im(:,csel)=floor(interp1(ix,am(ix+1,csel),x) + 0.5);
end

figure
hold on
plot(m)
plot(im)
hold off

e = im - m;
figure
plot(e)

%% Generate <base>.v and <base>_tb.v
IW=16;
OW=16;
gen_interp(base, who, IW, OW, x, ix, nix, am, im)

clear am
