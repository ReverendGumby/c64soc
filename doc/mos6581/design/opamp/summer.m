run ../svf/resid/filter_data.m

close all

%% Configuration

base='summer';
who=mfilename('fullpath');

ii = 1:5;

psummer=nan(65536*6,5);   % padded to widest curve
ssummer=nan(65536,5);     % scaled to narrowest curve
for i=ii
    j = i + 1;
    s0 = mf.summer_offsets(i) + 1;
    s1 = s0 + 65535 * j + i;
    dsummer(1:65536*j) = double(mf.summer(s0:s1))';
    psummer(1:s1-s0+1, i) = dsummer;
    ssummer(:, i) = dsummer(1:j:65536*j);
    clear dsummer
end

%hold on
%for i=ii
%    subplot(2, 3, i)
%    plot(ssummer(:, i))
%end
%hold off

m = psummer;
ix = [
    0  1e3  2e3  4e3  6e3  8e3 10e3  18e3  40e3  60e3  80e3  90e3 100e3 110e3 120e3 65535*2;
    0  5e3 10e3 14e3 16e3 19e3 23e3  50e3  70e3  90e3 100e3 115e3 130e3 145e3 160e3 65535*3;
    0 13e3 21e3 24e3 26e3 28e3 32e3  50e3  90e3 120e3 140e3 160e3 185e3 215e3 240e3 65535*4;
    0 24e3 32e3 40e3 45e3 50e3 60e3 100e3 140e3 155e3 170e3 190e3 205e3 225e3 250e3 65535*5;
    0 32e3 42e3 52e3 59e3 66e3 75e3 110e3 155e3 175e3 190e3 205e3 225e3 250e3 300e3 65535*6;
     ];

%% Compute linear approximation curves

nix = length(ix);
im = nan(size(m));
for csel = 1:size(m,2)
    x = 0:find(~isnan(m(:,csel)),1,'last')-1;
    im(x+1,csel)=floor(interp1(ix(csel,:),m(ix(csel,:)+1,csel),x) + 0.5);
end

figure
hold on
plot(m)
plot(im)
hold off

e = im - m;
figure
plot(e)

%% Generate <base>.vh and <base>_tb.v
IW=19;
OW=16;
ix = ix';
gen_interp2(base, who, IW, OW, ix, nix, m, im)
