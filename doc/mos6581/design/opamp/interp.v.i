module @base@ #(
    parameter CSELW=@CSELW@,
    parameter IW=@IW@,
    parameter OW=@OW@)
(
   input [CSELW-1:0] CSEL,
   input             CLK,
   input             ICLKEN,
   output            OCLKEN,

   input [IW-1:0]    I,
   output [OW-1:0]   O
 );

/* Defines SCNT, IXDIQ, ix_lut, iy_lut, ... */
`include "@base@.vh"

/* Defines the linear approximation interpolation function */
`include "interp.vh"

reg [IW-1:0]       in;
reg [OW-1:0]       out;
reg                oclken;
always @(posedge CLK) begin
  if (ICLKEN)
    in <= I;
  // out will be valid by the oclken=1 latch time.
  oclken <= ICLKEN;
end

always @*
  interp(CSEL, in, out);

assign O = out;
assign OCLKEN = oclken;

endmodule
