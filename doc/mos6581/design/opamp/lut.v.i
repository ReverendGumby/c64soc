module @base@ #(
    parameter IW=@IW@,
    parameter OW=@OW@)
(
   input             CSEL,
   input             CLK,
   input             ICLKEN,
   output            OCLKEN,

   input [IW-1:0]    I,
   output [OW-1:0]   O
 );

/* Defines CNT, the table */
`include "@base@.vh"

reg [IW-1:0]       in;
reg [OW-1:0]       out;
reg                oclken;
always @(posedge CLK) begin
  if (ICLKEN)
    in <= I;
  // out will be valid by the oclken=1 latch time.
  oclken <= ICLKEN;
end

always @in
  out = lut[in];

assign O = out;
assign OCLKEN = oclken;

endmodule
