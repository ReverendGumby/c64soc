run ../svf/resid/filter_data.m

close all

%% Configuration

base='gain';

% size(m) = <x(end) + 1> <# of curves>
m=double(mf.gain)';

%% Compute gain linear approximation curves

x=0:65535;
ix=[ 0    5e2 10e2 22e2  4e3 10e3 15e3 20e3 ...
    26e3 31e3 36e3 41e3 45e3 50e3 57e3 x(end)];

who=mfilename('fullpath');
run interp_cmn.m
