#include <iostream>
#include "filter.h"

double vmin, vmax;

double to_volts(int i)
{
    return vmin + (vmax - vmin) * (double)i / (1 << 16);
}

int main(int argc, char *argv[])
{
    reSID::Filter f;

    auto& mf = f.model_filter[0]; //6581
    vmin = f._vmin;
    std::cout << "vmin = " << vmin << ";\n";
    vmax = f._vmax;
    std::cout << "vmax = " << vmax << ";\n";

    std::cout << "gain = {\n";
    for (int n8 = 0; n8 < 16; n8++) {
        if (n8) {
            std::cout << ", ";
        }
        std::cout << "  { (* res=" << n8 << " *)\n";
        for (int vi = 0; vi < (1 << 16); vi++) {
            if (vi % 64)
                continue;
            auto vif = to_volts(vi);
            auto g = mf.gain[n8][vi];
            auto vof = to_volts(g);
            if (vi) {
                std::cout << ", ";
                if (vi % 8 == 0)
                    std::cout << "\n";
            }
            std::cout << '{' << vif << ',' << vof << '}';
        }
        std::cout << "  }\n";
    }
    std::cout << "};\n";

    return 0;
}

