#include <iostream>
#include <fstream>
#include "wave.h"
#include "envelope.h"
#include "matlab_formatter.h"
#include "verilog_formatter.h"
#include "printer.h"

enum class omode_t {
    unknown,
    matlab,
    verilog,
};
omode_t mode = omode_t::unknown;

enum class smode_t {
    wave,
    envelope,
};
smode_t smode = smode_t::wave;

const char *name = "dac";
reSID::WaveformGenerator wg;
reSID::EnvelopeGenerator eg;

void print_data(formatter &fmt)
{
    printer p{fmt};

    switch (smode) {
    case smode_t::wave:
        {
            auto &dac = wg.model_dac[0];
            p.print(name, dac);
        }
        break;
    case smode_t::envelope:
        {
            auto &dac = eg.model_dac[0];
            p.print(name, dac);
        }
        break;
    }
}

void print_data_matlab()
{
    matlab_formatter fmt{std::cout};
    print_data(fmt);
}

void print_data_verilog()
{
    verilog_formatter fmt{std::cout};
    print_data(fmt);
}

void print_data()
{
    switch (mode) {
    case omode_t::matlab:
        print_data_matlab();
        break;
    case omode_t::verilog:
        print_data_verilog();
        break;
    case omode_t::unknown:
        std::cerr << "Format not set!\n";
        exit(1);
    }
}

int main(int argc, char *argv[])
{
    for (; argc; argc--, argv++) {
        if (argv[0][0] == '-' && argv[0][1] != '\0') {
            char opt = argv[0][1];
            switch (opt) {
            case 'm':
                mode = omode_t::matlab;
                break;
            case 'v':
                mode = omode_t::verilog;
                break;
            case 'n':
                if (!argc) {
                    std::cerr << "-n: missing argument\n";
                    return -1;
                }
                name = argv[1];
                argc--, argv++;
                break;
            case 'd':
                print_data();
                break;
            case 'e':
                smode = smode_t::envelope;
                break;
            default:
                std::cerr << "unknown option " << opt << "\n";
                return -1;
            }
        }
    }

    return 0;
}
