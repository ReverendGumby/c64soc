#include <iostream>
#include <fstream>
#include "filter.h"
#include "matlab_formatter.h"
#include "printer.h"

// enum omode_t {
//      mathematica = 0,
//      matlab = 1,
// };
// omode_t mode;

reSID::Filter f;
auto& mf = f.model_filter[0]; //6581

#define P(n)            p.print(#n, n)
#define PP(n, v)        p.print(n, v)

void print_data(char argc, char *argv[])
{
    //set_mode((omode_t)atoi(argv[0]));

    matlab_formatter fmt_matlab{std::cout};
    formatter* fmt = &fmt_matlab;
    printer p{*fmt};

    int summer_offsets[] = {
        reSID::summer_offset<0>::value,
        reSID::summer_offset<1>::value,
        reSID::summer_offset<2>::value,
        reSID::summer_offset<3>::value,
        reSID::summer_offset<4>::value,
    };
    PP("mf.summer_offsets", summer_offsets);

    int mixer_offsets[] = {
        reSID::mixer_offset<0>::value,
        reSID::mixer_offset<1>::value,
        reSID::mixer_offset<2>::value,
        reSID::mixer_offset<3>::value,
        reSID::mixer_offset<4>::value,
        reSID::mixer_offset<5>::value,
        reSID::mixer_offset<6>::value,
        reSID::mixer_offset<7>::value,
    };
    PP("mf.mixer_offsets", mixer_offsets);

    PP("mf.vmin", mf._vmin);
    PP("mf.vmax", mf._vmax);

    P(mf.kVddt);
    P(mf.n_snake);
    P(mf.voice_scale_s14);
    P(mf.voice_DC);
    P(mf.ak);
    P(mf.bk);

    P(mf.opamp_rev);
    P(mf.summer);
    P(mf.gain);
    P(mf.mixer);
    P(mf.f0_dac);

    P(f.vcr_kVg);
    P(f.vcr_n_Ids_term);
}

void integrate(int argc, char *argv[])
{
    if (!argc) {
        std::cerr << "option requires filename\n";
        return;
    }
    const char *fn = *argv;
    std::fstream fs{fn, std::ios::in};

    int x = 0;
    int vc = 0;
    int vout = 0;
    int in;
    while (fs >> in) {
        int vin = in + vout;
        vin = std::max(std::min(vin, 65535), 0);
        std::cout << "vin = " << vin << "\n";
        vout = f.solve_integrate_6581(1, vin, x, vc, mf);
        std::cout << "vout = " << vout << "\n\n";
    }
}

int main(int argc, char *argv[])
{
    argc--, argv++;
    if (argc && argv[0][0] == '-' && argv[0][1] != '\0') {
        char opt = argv[0][1];
        argc--, argv++;
        switch (opt) {
        case 'd':
            print_data(argc, argv);
            break;
        case 'i':
            integrate(argc, argv);
            break;
        default:
            std::cerr << "unknown option " << opt << "\n";
            return -1;
        }
    }

    return 0;
}
