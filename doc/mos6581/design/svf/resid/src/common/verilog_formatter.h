#pragma once

#include <iostream>
#include <sstream>
#include "formatter.h"

class verilog_formatter : public formatter {
public:
    verilog_formatter(std::ostream& os) : formatter(os) {}

    virtual void name(const char* s);
    virtual void scalar(const std::string &s);

    virtual void assign_open(const std::type_info&);
    virtual void assign_equal();
    virtual void assign_close();

    virtual void array_open(size_t count);
    virtual void array_next();
    virtual void array_close();

protected:

private:
    static const size_t line_max;
    static const size_t indent_width;

    const std::type_info* assign_type;

    bool first_scalar;
    std::string reg_name;
    std::stringstream reg_decl;
    int reg_bits;
    std::string scalar_prefix;
};
