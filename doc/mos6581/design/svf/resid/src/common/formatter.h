#pragma once

#include <iostream>
#include <vector>
#include <typeinfo>

class formatter {
public:
    formatter() = delete;
    formatter(std::ostream& os) : os(os) {}

    template <typename t>
    void scalar(t val) { scalar(std::to_string(val)); }

    virtual void name(const char* s) = 0;
    virtual void scalar(const std::string &s) = 0;

    virtual void assign_open(const std::type_info&) = 0;
    virtual void assign_equal() = 0;
    virtual void assign_close() = 0;

    virtual void array_open(size_t count);
    virtual void array_next();
    virtual void array_close();

protected:
    struct array_state {
        size_t count;
        size_t idx;
    };

    const array_state& get_array_state() const
    {
        return array_states.back();
    }
    size_t get_array_depth() const
    {
        return array_states.size();
    }

    std::ostream& os;
    std::vector<array_state> array_states;
};

