#pragma once

#include <iostream>
#include "formatter.h"

class matlab_formatter : public formatter {
public:
    matlab_formatter(std::ostream& os) : formatter(os) {}

    virtual void name(const char* s) { os << s; }
    virtual void scalar(const std::string &s) { os << s; }

    virtual void assign_open(const std::type_info&);
    virtual void assign_equal();
    virtual void assign_close();

    virtual void array_open(size_t count);
    virtual void array_next();
    virtual void array_close();

protected:
    virtual void indent();

private:
    static const size_t line_max;
    static const size_t indent_width;

    const std::type_info* assign_type;
    size_t array_line_count;

    bool oneline_array;
};
