#pragma once

#include <stdlib.h>
#include <type_traits>
#include "formatter.h"

class printer {
public:
    printer() = delete;
    printer(formatter& f) : f(f) {}

    template <typename T>
    void print(const char* name, const T (&));

private:
    template <typename T>
    void print(const T (&val)) { f.scalar(val); }

    template <typename T, size_t N>
    void print(const T (&)[N]);

    formatter& f;
};

template <typename T>
void printer::print(const char* name, const T (&val))
{
    typedef typename std::remove_all_extents<T>::type base;
 
    f.assign_open(typeid(base));
    f.name(name);
    f.assign_equal();
    print(val);
    f.assign_close();
}

template <typename T, size_t N>
void printer::print(const T (&val)[N])
{
    f.array_open(N);
    for (auto i = 0; i < N; i++) {
        if (i)
            f.array_next();
        print(val[i]);
    }
    f.array_close();
}
