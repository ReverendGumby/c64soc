#include <stdint.h>
#include <iomanip>

#include "verilog_formatter.h"

const size_t verilog_formatter::indent_width = 2;

void verilog_formatter::name(const char* s)
{
    reg_name = s;
    reg_decl << reg_name;
}

void verilog_formatter::scalar(const std::string &s)
{
    // We finally have the full reg declaration ready.
    if (first_scalar) {
        os << reg_decl.str() << ";\n";
        os << "initial begin\n";
        first_scalar = false;
    }

    os << std::setw(indent_width) << ' ';
    os << reg_name << '[';
    unsigned idx = 0;
    for (const auto &s : array_states) {
        if (idx != 0)
            os << ':';
        os << s.idx;
        idx++;
    }
    os << "] = " << scalar_prefix << 'd' << s << ";\n";
}

void verilog_formatter::assign_open(const std::type_info& type)
{
    first_scalar = true;
    reg_decl.clear();
    reg_decl << "reg ";

    assign_type = &type;
    bool is_signed;

    if (*assign_type == typeid(uint32_t)) {
        reg_bits = 32;
        is_signed = false;
    } else if (*assign_type == typeid(int32_t)) {
        reg_bits = 32;
        is_signed = true;
    } else if (*assign_type == typeid(uint16_t)) {
        reg_bits = 16;
        is_signed = false;
    } else if (*assign_type == typeid(int16_t)) {
        reg_bits = 16;
        is_signed = true;
    } else {
        assign_type = nullptr;
        return;
    }

    if (is_signed)
        reg_decl << "signed ";
    reg_decl << "[" << (reg_bits - 1) << ":0] ";

    std::stringstream ss;
    ss << reg_bits << "'";
    if (is_signed)
        ss << 's';
    scalar_prefix = ss.str();
}

void verilog_formatter::assign_equal()
{
}

void verilog_formatter::assign_close()
{
    os << "end\n";
}

void verilog_formatter::array_open(size_t count)
{
    reg_decl << " [0:" << (count - 1) << ']';

    formatter::array_open(count);
}

void verilog_formatter::array_next()
{
    formatter::array_next();
}

void verilog_formatter::array_close()
{
}
