#include <stdint.h>
#include <iomanip>

#include "matlab_formatter.h"

const size_t matlab_formatter::line_max = 16;
const size_t matlab_formatter::indent_width = 4;

void matlab_formatter::indent()
{
    os << "\n" << std::setw(indent_width) << ' ';
    array_line_count = 0;
}

void matlab_formatter::assign_open(const std::type_info& type)
{
    assign_type = &type;
}

void matlab_formatter::assign_equal()
{
    os << " = ";
    if (*assign_type == typeid(uint32_t)) {
        os << "uint32(";
    } else if (*assign_type == typeid(int32_t)) {
        os << "int32(";
    } else if (*assign_type == typeid(uint16_t)) {
        os << "uint16(";
    } else if (*assign_type == typeid(int16_t)) {
        os << "int16(";
    } else {
        assign_type = nullptr;
    }
}

void matlab_formatter::assign_close()
{
    if (assign_type)
        os << ')';
    os << ";\n";
}

void matlab_formatter::array_open(size_t count)
{
    auto depth = get_array_depth();
    bool first_sub = depth && get_array_state().idx == 0;

    array_line_count = 0;
    if (depth == 0) {
        os << "[ ";
        oneline_array = true;
        if (count > line_max) {
            oneline_array = false;
            indent();
        }
    } else {
        if (!first_sub) {
            os << ';';
            indent();
        }
    }

    formatter::array_open(count);
}

void matlab_formatter::array_next()
{
    formatter::array_next();
    array_line_count ++;

    if (array_line_count == line_max) {
        oneline_array = false;
        os << " ...";
        indent();
    } else {
        os << " ";
    }
}

void matlab_formatter::array_close()
{
    formatter::array_close();
    array_line_count = 0;

    auto depth = get_array_depth();
    if (depth == 0) {
        if (!oneline_array)
            os << "\n]";
        else
            os << " ]";
    }
}
