#include "formatter.h"

void formatter::array_open(size_t count)
{
    array_state as;
    as.count = count;
    as.idx = 0;
    array_states.push_back(as);
}

void formatter::array_next()
{
    array_states.back().idx ++;
}

void formatter::array_close()
{
    array_states.pop_back();
}

