`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/29/2014 02:52:34 PM
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top(
    input clk,  // 125 MHz
    
    output [3:0] led
    
    );
    
    wire clk16;
    clkgen_16mhz clkgen_16mhz(
        .CLK_IN1(clk),
        .CLK_OUT1(clk16));
    
    wire cpucp1, cpucp2, memclk;
    clkgen clkgen(
        .clkin(clk16),
        .cpucp1(cpucp1),
        .cpucp2(cpucp2),
        .memclk(memclk)
    );
    
    wire nRES;    
    rstgen rstgen(
        .clk(cpucp1),
        .nrst(nRES)
    );
    
    wire cpu_rw;
    (* s, mark_debug = "true" *) wire cpu_sync;
    wire [15:0] cpu_a;
    wire [7:0] cpu_db;
    
    mos6510 mos6510(
        .nRES(nRES),
        .CP1(cpucp1),
        .CP2(cpucp2),
        .nIRQ(1'b1),
        .nNMI(1'b1),
        .AEC(1'b1),
        .RDY(1'b1),
        .SO(1'b0),
        .RW(cpu_rw),
        .SYNC(cpu_sync),
        .A(cpu_a),
        .DB(cpu_db)
    );
    
    wire [7:0] rom_dout, ram_dout, ram_din;

    addr_dec addr_dec(
        .cpu_rw(cpu_rw),
        .cpu_a(cpu_a),
        .rom_dout(rom_dout),
        .ram_dout(ram_dout),
        .ram_wea(ram_wea),
        .cpu_db(cpu_db),
        .ram_din(ram_din)
    );
    
    ram ram(
        .clka(memclk),
        .addra(cpu_a[11:0]),
        .dina(ram_din),
        .wea(ram_wea),
        .douta(ram_dout)
    );
    
    rom rom(
        .clka(memclk),
        .addra(cpu_a[8:0]),
        .douta(rom_dout)
    );
    
    test_det test_det(
                      .clk(cpucp2),
                      .a(cpu_a),
                      .leds(led)
                      );

endmodule
