`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/29/2014 03:26:57 PM
// Design Name: 
// Module Name: rstgen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rstgen(
              input      clk,
              output reg nrst
              );

reg [3:0]                cnt;

initial begin
  cnt <= 4'd0;
  nrst <= 1'b0;
end

always @(posedge clk) begin
  cnt <= cnt + 1;
end

always @(posedge clk) begin
  if (cnt == 4'd15)
    nrst <= 1'b1;
end

endmodule
