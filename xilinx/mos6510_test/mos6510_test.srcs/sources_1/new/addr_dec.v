`timescale 1ns / 1ps

module addr_dec(
	 input cpu_rw,
    input [15:0] cpu_a,
	 input [7:0] rom_dout,
	 input [7:0] ram_dout,	
	 inout [7:0] cpu_db,
	 output ram_wea,
	 output [7:0] ram_din
    );

wire rom_cs = cpu_rw && cpu_a[15:9] == 7'b1111_111;
wire ram_cs = !rom_cs;

assign ram_wea = !cpu_rw && ram_cs;
assign ram_din = cpu_db;

reg [7:0] dout; 
always @* begin
	if (ram_cs)
		dout <= ram_dout;
	else if (rom_cs)
		dout <= rom_dout;
	else
		dout <= 8'hxx;
end

assign cpu_db = cpu_rw ? dout : 8'hzz;

endmodule
