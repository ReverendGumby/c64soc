`timescale 1ns / 1ps
module mos6510_test_tb(
    );

wire [3:0] led;
reg clk;

top uut(
	.clk(clk),
	.led(led)
);

initial begin
	clk = 1;
	forever
		#4 clk = !clk;
end

endmodule
