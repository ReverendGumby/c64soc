`timescale 1ns / 1ps
module mos6510_test_tb(
    );

wire [3:0] led;
reg clk;
reg btn;

top uut(
	.clk(clk),
	.btn(btn),
	.led(led)
);

initial begin
	clk = 1;
	forever
		#4 clk = !clk;
end

initial begin
	btn = 1;
	#8000 btn = 0;
end

endmodule
