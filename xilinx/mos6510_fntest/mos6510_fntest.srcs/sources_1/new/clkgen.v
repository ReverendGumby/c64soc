`timescale 1ns / 1ps

// Prevent internal clocks from bubbling up into the design.
(* KEEP_HIERARCHY = "TRUE" *)

module clkgen
  (
   input      clkin,
   output reg cpucp1,
   output reg cpucp2,
   output reg memclk
   );

reg           clk_div2, clk_div4, clk_div8, clk_div16;

reg [3:0]     cnt;

initial begin
  cnt <= 0;
end

always @(posedge clkin) begin
  if (cnt == 4'd13)
    cnt <= 0;
  else  
    cnt <= cnt + 1;
end

reg [2:0] clks;
always @* begin
  case (cnt)
    4'd0:  clks = 3'b100;
    4'd1:  clks = 3'b101;
    4'd2:  clks = 3'b101;
    4'd3:  clks = 3'b101;
    4'd4:  clks = 3'b001;
    4'd5:  clks = 3'b000;
    4'd6:  clks = 3'b000;
    4'd7:  clks = 3'b010;
    4'd8:  clks = 3'b011;
    4'd9:  clks = 3'b011;
    4'd10: clks = 3'b011;
    4'd11: clks = 3'b001;
    4'd12: clks = 3'b000;
    4'd13: clks = 3'b000;
endcase
end

always @(posedge clkin) begin
  cpucp1 <= clks[2];
  cpucp2 <= clks[1];
  memclk <= clks[0];
end

endmodule
