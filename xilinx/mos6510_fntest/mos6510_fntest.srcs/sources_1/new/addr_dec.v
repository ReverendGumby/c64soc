`timescale 1ns / 1ps

module addr_dec(
	 input cpu_rw,
    input [15:0] cpu_a,
	 input [7:0] ram_dout,	
	 inout [7:0] cpu_db,
	 output ram_wea,
	 output [7:0] ram_din
    );

wire ram_cs = 1'b1;

assign ram_wea = !cpu_rw && ram_cs;
assign ram_din = cpu_db;

reg [7:0] dout; 
always @* begin
	if (ram_cs)
		dout <= ram_dout;
	else
		dout <= 8'hxx;
end

assign cpu_db = cpu_rw ? dout : 8'hzz;

endmodule
