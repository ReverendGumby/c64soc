module test_det
  (
   input clk,
   input [15:0] a,
   output [3:0] leds
   );

reg             bump;
//reg [3:0]       prescale_cnt;
reg [3:0]       led_cnt;

initial begin
  bump = 0;
//  prescale_cnt = 0;
  led_cnt = 0;
end

always @(posedge clk) begin
  if (a == 16'hfffc) begin
//    prescale_cnt <= prescale_cnt + 1;
    bump <= 1'b1;
  end else begin
    bump <= 1'b0;
  end
end

always @(posedge clk) begin
  if (bump /*&& prescale_cnt == 0*/)
    led_cnt <= led_cnt + 1;
end

assign leds = led_cnt;

endmodule
