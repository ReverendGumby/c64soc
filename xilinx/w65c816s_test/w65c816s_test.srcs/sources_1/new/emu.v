// Emulation system:
// - S-SMP / PS7 bridge

module emu
  (
   output [31:0] DEBUG,
   input         MCLK, // master clock input: 21477272 Hz
   input         CPUMEM_CLK, // 3x master clock input: 64431817 Hz
   input         EXT_RES,

   // AXI Lite slave bus interface
   input         EMUBUS_ARESETn,
   input [31:0]  EMUBUS_AWADDR,
   input         EMUBUS_AWVALID,
   output        EMUBUS_AWREADY,
   input [31:0]  EMUBUS_WDATA,
   output        EMUBUS_WREADY,
   input [3:0]   EMUBUS_WSTRB,
   input         EMUBUS_WVALID,
   output [1:0]  EMUBUS_BRESP,
   output        EMUBUS_BVALID,
   input         EMUBUS_BREADY,
   input [31:0]  EMUBUS_ARADDR,
   input         EMUBUS_ARVALID,
   output        EMUBUS_ARREADY,
   output [31:0] EMUBUS_RDATA,
   output [1:0]  EMUBUS_RRESP,
   output        EMUBUS_RVALID,
   input         EMUBUS_RREADY,

   // AXI_ACP master bus interface for CPU memory
   output [31:0] CPUMEM_ARADDR,
   output [1:0]  CPUMEM_ARBURST,
   output [3:0]  CPUMEM_ARCACHE,
   output [2:0]  CPUMEM_ARID,
   output [3:0]  CPUMEM_ARLEN,
   output [1:0]  CPUMEM_ARLOCK,
   output [2:0]  CPUMEM_ARPROT,
   output [3:0]  CPUMEM_ARQOS,
   input         CPUMEM_ARREADY,
   output [2:0]  CPUMEM_ARSIZE,
   output [4:0]  CPUMEM_ARUSER,
   output        CPUMEM_ARVALID,
   output [31:0] CPUMEM_AWADDR,
   output [1:0]  CPUMEM_AWBURST,
   output [3:0]  CPUMEM_AWCACHE,
   output [2:0]  CPUMEM_AWID,
   output [3:0]  CPUMEM_AWLEN,
   output [1:0]  CPUMEM_AWLOCK,
   output [2:0]  CPUMEM_AWPROT,
   output [3:0]  CPUMEM_AWQOS,
   input         CPUMEM_AWREADY,
   output [2:0]  CPUMEM_AWSIZE,
   output [4:0]  CPUMEM_AWUSER,
   output        CPUMEM_AWVALID,
   input [2:0]   CPUMEM_BID,
   output        CPUMEM_BREADY,
   input [1:0]   CPUMEM_BRESP,
   input         CPUMEM_BVALID,
   input [63:0]  CPUMEM_RDATA,
   input [2:0]   CPUMEM_RID,
   input         CPUMEM_RLAST,
   output        CPUMEM_RREADY,
   input [1:0]   CPUMEM_RRESP,
   input         CPUMEM_RVALID,
   output [63:0] CPUMEM_WDATA,
   output [2:0]  CPUMEM_WID,
   output        CPUMEM_WLAST,
   input         CPUMEM_WREADY,
   output [7:0]  CPUMEM_WSTRB,
   output        CPUMEM_WVALID
   );

wire            cp1_posedge, cp1_negedge, cp2_posedge, cp2_negedge,
                cp2_negedge_pre;

wire [31:0]     cpumem_base_wram, cpumem_base_rom;
wire            cpumem_cpu_ready;
reg [23:0]      cpumem_a;
wire [7:0]      cpumem_d_i, cpumem_d_o;
reg             cpumem_rw, cpumem_vda, cpumem_vpa;
wire [7:0]      cpumem_mon_sel;
wire            cpumem_mon_ws;
wire [31:0]     cpumem_mon_dout, cpumem_mon_din;

wire            cpu_rdy;
wire [23:0]     cpu_a;
reg [7:0]       cpu_d;
wire [7:0]      cpu_d_i, cpu_d_o;
wire            cpu_rwb, cpu_vda, cpu_vpa;
wire [7:0]      cpu_mon_sel;
wire [7:0]      cpu_mon_din, cpu_mon_dout;
wire            cpu_mon_ws;

reg             sys_hold_ack;
wire            sys_hold_req;

wire            icd_sys_enable;
wire            icd_sys_force_halt;
wire            icd_sys_resume;
wire            icd_sys_hold;
wire [7:0]      icd_match_enable;
wire [7:0]      icd_match_trigger;
wire [2:0]      icd_match_sel;
wire [3:0]      icd_match_reg_sel;
wire [3:0]      icd_match_reg_wstrb;
wire [31:0]     icd_match_reg_din;
wire [31:0]     icd_match_reg_dout;


//////////////////////////////////////////////////////////////////////
// CPU memory / PS7 memory (AXI_ACP) bridge

// Temporary local clkgen
localparam cycle_ccnt = 6; // fast-ROM, MMIO, and "internal" cycles
//localparam cycle_ccnt = 20; // slowest DRAM latency + lots of margin
reg [3:0] cp2_clkcnt;
initial
  cp2_clkcnt = 4'd0;
always @(posedge MCLK) begin
  if (cpumem_cpu_ready) begin
    if (cp2_negedge_pre)
      cp2_clkcnt <= 4'd0;
    else
      cp2_clkcnt <= cp2_clkcnt + 1'd1;
  end
end
assign cp2_negedge_pre = (cp2_clkcnt == (cycle_ccnt - 1'd1));
assign cp2_negedge = (cp2_clkcnt == 4'd0);
assign cp1_posedge = (cp2_clkcnt == 4'd1);
assign cp1_negedge = (cp2_clkcnt == 4'd2);
assign cp2_posedge = (cp2_clkcnt == 4'd3);

cpumem cpumem
  (
   .MCLK(MCLK),
   .CP1_POSEDGE(cp1_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .CP2_NEGEDGE_PRE(cp2_negedge_pre),

   // Module status/control
   .BASE_WRAM(cpumem_base_wram),
   .BASE_ROM(cpumem_base_rom),
   .AXI_EN(1'b1),
   .CPU_READY(cpumem_cpu_ready),

   // CPU memory bus interface
   .A(cpumem_a),
   .D_I(cpumem_d_i),
   .D_O(cpumem_d_o),
   .RW(cpumem_rw),
   .VDA(cpumem_vda),
   .VPA(cpumem_vpa),

   // Monitor interface
   .MON_SEL(cpumem_mon_sel),
   .MON_WS(cpumem_mon_ws),
   .MON_DOUT(cpumem_mon_dout),
   .MON_DIN(cpumem_mon_din),

   // AXI bus leader for ACP
   .ACLK(CPUMEM_CLK),
   .ARADDR(CPUMEM_ARADDR),
   .ARBURST(CPUMEM_ARBURST),
   .ARCACHE(CPUMEM_ARCACHE),
   .ARID(CPUMEM_ARID),
   .ARLEN(CPUMEM_ARLEN),
   .ARLOCK(CPUMEM_ARLOCK),
   .ARPROT(CPUMEM_ARPROT),
   .ARQOS(CPUMEM_ARQOS),
   .ARREADY(CPUMEM_ARREADY),
   .ARSIZE(CPUMEM_ARSIZE),
   .ARUSER(CPUMEM_ARUSER),
   .ARVALID(CPUMEM_ARVALID),
   .AWADDR(CPUMEM_AWADDR),
   .AWBURST(CPUMEM_AWBURST),
   .AWCACHE(CPUMEM_AWCACHE),
   .AWID(CPUMEM_AWID),
   .AWLEN(CPUMEM_AWLEN),
   .AWLOCK(CPUMEM_AWLOCK),
   .AWPROT(CPUMEM_AWPROT),
   .AWQOS(CPUMEM_AWQOS),
   .AWREADY(CPUMEM_AWREADY),
   .AWSIZE(CPUMEM_AWSIZE),
   .AWUSER(CPUMEM_AWUSER),
   .AWVALID(CPUMEM_AWVALID),
   .BID(CPUMEM_BID),
   .BREADY(CPUMEM_BREADY),
   .BRESP(CPUMEM_BRESP),
   .BVALID(CPUMEM_BVALID),
   .RDATA(CPUMEM_RDATA),
   .RID(CPUMEM_RID),
   .RLAST(CPUMEM_RLAST),
   .RREADY(CPUMEM_RREADY),
   .RRESP(CPUMEM_RRESP),
   .RVALID(CPUMEM_RVALID),
   .WDATA(CPUMEM_WDATA),
   .WID(CPUMEM_WID),
   .WLAST(CPUMEM_WLAST),
   .WREADY(CPUMEM_WREADY),
   .WSTRB(CPUMEM_WSTRB),
   .WVALID(CPUMEM_WVALID)
   );

always @* begin
  cpumem_a = cpu_a;
  cpumem_rw = cpu_rwb;
  cpumem_vda = cpu_vda;
end

always @* cpumem_vpa = cpu_vpa;
assign cpumem_d_i = cpu_d;


//////////////////////////////////////////////////////////////////////
// CPU (temporary)

wire cpu_cp1_posedge = cp1_posedge & ~sys_hold_ack;
wire cpu_cp1_negedge = cp1_negedge & ~sys_hold_ack;
wire cpu_cp2_posedge = cp2_posedge & ~sys_hold_ack;
wire cpu_cp2_negedge = cp2_negedge & ~sys_hold_ack;

w65c816s cpu
  (
   .RESB(emu_nres),
   .CLK(MCLK),
   .CP1_POSEDGE(cpu_cp1_posedge),
   .CP1_NEGEDGE(cpu_cp1_negedge),
   .CP2_POSEDGE(cpu_cp2_posedge),
   .CP2_NEGEDGE(cpu_cp2_negedge),
   .HOLD(sys_hold_ack),
   .IRQB(1'b1),
   .NMIB(1'b1),
   .ABORTB(1'b1),
   .E(),
   .MLB(),
   .M(),
   .X(),
   .A(cpu_a),
   .D_I(cpu_d_i),
   .D_O(cpu_d_o),
   .RWB(cpu_rwb),
   .RDY_I(cpu_rdy),
   .RDY_O(cpu_rdy),
   .VPB(),
   .VDA(cpu_vda),
   .VPA(cpu_vpa),

   .MON_SEL(cpu_mon_sel),
   .MON_WS(cpu_mon_ws),
   .MON_DOUT(cpu_mon_dout),
   .MON_DIN(cpu_mon_din)
   );

always @* begin
  cpu_d = cpumem_d_o;
  if (~cpu_rwb)
    cpu_d = cpu_d_o;
end

assign cpu_d_i = cpu_d;


//////////////////////////////////////////////////////////////////////
// CPU in-circuit debugger

initial
  sys_hold_ack = 1'b0;

assign sys_hold_req = icd_sys_hold;

// Synchonize system hold timing with the slowest entity: the CPU.
always @(posedge MCLK) if (cp2_negedge)
  sys_hold_ack <= sys_hold_req;

icd #(.NUM_MATCH(2)) icd
  (
   .nRES(emu_nres),
   .CLK(MCLK),
   .CP2_NEGEDGE(cp2_negedge),
   .HOLD(sys_hold_ack),

   .CPU_A(cpu_a),
   .CPU_DB(cpu_d_i),
   .CPU_RW(cpu_rwb),
   .CPU_RDY(1'b1),
   .CPU_SYNC(cpu_vda & cpu_vpa),
   .CPU_CYC(/*cpu_cyc*/32'b0),

   .MATCH_ENABLE(icd_match_enable[1:0]),
   .MATCH_TRIGGER(icd_match_trigger[1:0]),
   .MATCH_SEL(icd_match_sel),
   .MATCH_REG_SEL(icd_match_reg_sel),
   .MATCH_REG_WSTRB(icd_match_reg_wstrb),
   .MATCH_REG_DIN(icd_match_reg_din),
   .MATCH_REG_DOUT(icd_match_reg_dout),

   .SYS_ENABLE(icd_sys_enable),
   .SYS_FORCE_HALT(icd_sys_force_halt),
   .SYS_RESUME(icd_sys_resume),
   .SYS_HOLD(icd_sys_hold)
   );


//////////////////////////////////////////////////////////////////////
// EMU / PS7 bridge

emubus emubus
  (
   .CLK(MCLK),
   .CLKEN(1'b1),
   .APU_MCLK(AUD_MCLK),

   .ARESETn(EMUBUS_ARESETn),
   .AWADDR(EMUBUS_AWADDR),
   .AWVALID(EMUBUS_AWVALID),
   .AWREADY(EMUBUS_AWREADY),
   .WDATA(EMUBUS_WDATA),
   .WREADY(EMUBUS_WREADY),
   .WSTRB(EMUBUS_WSTRB),
   .WVALID(EMUBUS_WVALID),
   .BRESP(EMUBUS_BRESP),
   .BVALID(EMUBUS_BVALID),
   .BREADY(EMUBUS_BREADY),
   .ARADDR(EMUBUS_ARADDR),
   .ARVALID(EMUBUS_ARVALID),
   .ARREADY(EMUBUS_ARREADY),
   .RDATA(EMUBUS_RDATA),
   .RRESP(EMUBUS_RRESP),
   .RVALID(EMUBUS_RVALID),
   .RREADY(EMUBUS_RREADY),

   .EXT_RES(EXT_RES),
   .CLK_RESET(clk_reset),
   .EMU_nRES(emu_nres),
   .RES_COLD(res_cold),

   .CPU_MON_SEL(cpu_mon_sel),
   .CPU_MON_WS(cpu_mon_ws),
   .CPU_MON_DOUT(cpu_mon_dout),
   .CPU_MON_DIN(cpu_mon_din),

   .ICD_SYS_ENABLE(icd_sys_enable),
   .ICD_SYS_FORCE_HALT(icd_sys_force_halt),
   .ICD_SYS_RESUME(icd_sys_resume),
   .ICD_SYS_HOLD(icd_sys_hold),
   .ICD_MATCH_ENABLE(icd_match_enable),
   .ICD_MATCH_TRIGGER(icd_match_trigger),
   .ICD_MATCH_SEL(icd_match_sel),
   .ICD_MATCH_REG_SEL(icd_match_reg_sel),
   .ICD_MATCH_REG_WSTRB(icd_match_reg_wstrb),
   .ICD_MATCH_REG_DIN(icd_match_reg_din),
   .ICD_MATCH_REG_DOUT(icd_match_reg_dout),

   .CPUMEM_BASE_WRAM(cpumem_base_wram),
   .CPUMEM_BASE_ROM(cpumem_base_rom),
   .CPUMEM_MON_SEL(cpumem_mon_sel),
   .CPUMEM_MON_WS(cpumem_mon_ws),
   .CPUMEM_MON_DOUT(cpumem_mon_dout),
   .CPUMEM_MON_DIN(cpumem_mon_din)
   );


//////////////////////////////////////////////////////////////////////
// Debug

assign DEBUG[15:8] = cpu_d;
assign DEBUG[7] = ~cpumem_cpu_ready;
assign DEBUG[6] = cpumem_a[15];
assign DEBUG[5] = cpumem_a[1];
assign DEBUG[4] = cpumem_a[0];
assign DEBUG[3] = cpumem_rw;
assign DEBUG[2] = cpumem_vpa;
assign DEBUG[1] = cpumem_vda;
assign DEBUG[0] = cp2_negedge;

endmodule
