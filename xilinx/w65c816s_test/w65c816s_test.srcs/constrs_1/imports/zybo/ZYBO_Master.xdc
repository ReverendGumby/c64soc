## This file is a general .xdc for the ZYBO Rev B board
## To use it in a project:
## - uncomment the lines corresponding to used pins
## - rename the used signals according to the project

##Clock signal
##IO_L11P_T1_SRCC_35	
#set_property PACKAGE_PIN L16 [get_ports clk]
#set_property IOSTANDARD LVCMOS33 [get_ports clk]
#create_clock -add -name sys_clk_pin -period 8.00 -waveform {0 4} [get_ports clk]

##Switches
##IO_L19N_T3_VREF_35
set_property PACKAGE_PIN G15 [get_ports {SW[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SW[0]}]

##IO_L24P_T3_34
set_property PACKAGE_PIN P15 [get_ports {SW[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SW[1]}]

##IO_L4N_T0_34
set_property PACKAGE_PIN W13 [get_ports {SW[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SW[2]}]

##IO_L9P_T1_DQS_34
set_property PACKAGE_PIN T16 [get_ports {SW[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SW[3]}]

##Buttons
##IO_L20N_T3_34
set_property PACKAGE_PIN R18 [get_ports {BTN[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BTN[0]}]

##IO_L24N_T3_34
set_property PACKAGE_PIN P16 [get_ports {BTN[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BTN[1]}]

##IO_L18P_T2_34
set_property PACKAGE_PIN V16 [get_ports {BTN[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BTN[2]}]

##IO_L7P_T1_34
set_property PACKAGE_PIN Y16 [get_ports {BTN[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BTN[3]}]

##LEDs
##IO_L23P_T3_35
set_property PACKAGE_PIN M14 [get_ports {LED[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[0]}]

##IO_L23N_T3_35
set_property PACKAGE_PIN M15 [get_ports {LED[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[1]}]

##IO_0_35
set_property PACKAGE_PIN G14 [get_ports {LED[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[2]}]

##IO_L3N_T0_DQS_AD1N_35
set_property PACKAGE_PIN D18 [get_ports {LED[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[3]}]

##I2S Audio Codec
##IO_L12N_T1_MRCC_35
#set_property PACKAGE_PIN K18 [get_ports AC_BCLK]
#set_property IOSTANDARD LVCMOS33 [get_ports AC_BCLK]

##IO_25_34
#set_property PACKAGE_PIN T19 [get_ports AC_MCLK]
#set_property IOSTANDARD LVCMOS33 [get_ports AC_MCLK]

##IO_L23N_T3_34
#set_property PACKAGE_PIN P18 [get_ports AC_MUTEN]
#set_property IOSTANDARD LVCMOS33 [get_ports AC_MUTEN]

##IO_L8P_T1_AD10P_35
#set_property PACKAGE_PIN M17 [get_ports AC_PBDAT]
#set_property IOSTANDARD LVCMOS33 [get_ports AC_PBDAT]

##IO_L11N_T1_SRCC_35
#set_property PACKAGE_PIN L17 [get_ports AC_PBLRC]
#set_property IOSTANDARD LVCMOS33 [get_ports AC_PBLRC]

##IO_L12P_T1_MRCC_35
#set_property PACKAGE_PIN K17 [get_ports AC_RECDAT]!
#set_property IOSTANDARD LVCMOS33 [get_ports AC_RECDAT]

##IO_L8N_T1_AD10N_35
#set_property PACKAGE_PIN M18 [get_ports AC_RECLRC]
#set_property IOSTANDARD LVCMOS33 [get_ports AC_RECLRC]

##Audio Codec/external EEPROM IIC bus
##IO_L13P_T2_MRCC_34
#set_property PACKAGE_PIN N18 [get_ports AC_SCL]
#set_property IOSTANDARD LVCMOS33 [get_ports AC_SCL]

##IO_L23P_T3_34
#set_property PACKAGE_PIN N17 [get_ports AC_SDA]
#set_property IOSTANDARD LVCMOS33 [get_ports AC_SDA]

##Additional Ethernet signals
##IO_L6P_T0_35
#set_property PACKAGE_PIN F16 [get_ports ETH_INT_B]
#set_property IOSTANDARD LVCMOS33 [get_ports ETH_INT_B]

##IO_L3P_T0_DQS_AD1P_35
#set_property PACKAGE_PIN E17 [get_ports ETH_RST_B]
#set_property IOSTANDARD LVCMOS33 [get_ports ETH_RST_B]

##HDMI Signals
##IO_L13N_T2_MRCC_35
#set_property PACKAGE_PIN H17 [get_ports HDMI_CLK_N]
#set_property IOSTANDARD TMDS_33 [get_ports HDMI_CLK_N]

##IO_L13P_T2_MRCC_35
#set_property PACKAGE_PIN H16 [get_ports HDMI_CLK_P]
#set_property IOSTANDARD TMDS_33 [get_ports HDMI_CLK_P]

##IO_L4N_T0_35
#set_property PACKAGE_PIN D20 [get_ports {HDMI_DN[0]}]
#set_property IOSTANDARD TMDS_33 [get_ports {HDMI_DN[0]}]

##IO_L4P_T0_35
#set_property PACKAGE_PIN D19 [get_ports {HDMI_DP[0]}]
#set_property IOSTANDARD TMDS_33 [get_ports {HDMI_DP[0]}]

##IO_L1N_T0_AD0N_35
#set_property PACKAGE_PIN B20 [get_ports {HDMI_DN[1]}]
#set_property IOSTANDARD TMDS_33 [get_ports {HDMI_DN[1]}]

##IO_L1P_T0_AD0P_35
#set_property PACKAGE_PIN C20 [get_ports {HDMI_DP[1]}]
#set_property IOSTANDARD TMDS_33 [get_ports {HDMI_DP[1]}]

##IO_L2N_T0_AD8N_35
#set_property PACKAGE_PIN A20 [get_ports {HDMI_DN[2]}]
#set_property IOSTANDARD TMDS_33 [get_ports {HDMI_DN[2]}]

##IO_L2P_T0_AD8P_35
#set_property PACKAGE_PIN B19 [get_ports {HDMI_DP[2]}]
#set_property IOSTANDARD TMDS_33 [get_ports {HDMI_DP[2]}]

##IO_L5N_T0_AD9N_35
#set_property PACKAGE_PIN E19 [get_ports HDMI_CEC]
#set_property IOSTANDARD LVCMOS33 [get_ports HDMI_CEC]

##IO_L5P_T0_AD9P_35
#set_property PACKAGE_PIN E18 [get_ports HDMI_HPD]
#set_property IOSTANDARD LVCMOS33 [get_ports HDMI_HPD]

##IO_L6N_T0_VREF_35
#set_property PACKAGE_PIN F17 [get_ports HDMI_OUT_EN]
#set_property IOSTANDARD LVCMOS33 [get_ports HDMI_OUT_EN]

##IO_L16P_T2_35
#set_property PACKAGE_PIN G17 [get_ports HDMI_SCL]
#set_property IOSTANDARD LVCMOS33 [get_ports HDMI_SCL]

##IO_L16N_T2_35
#set_property PACKAGE_PIN G18 [get_ports HDMI_SDA]
#set_property IOSTANDARD LVCMOS33 [get_ports HDMI_SDA]

##Pmod Header JA (XADC)
##IO_L21N_T3_DQS_AD14N_35
set_property PACKAGE_PIN N16 [get_ports {JA_N[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JA_N[0]}]

##IO_L21P_T3_DQS_AD14P_35
set_property PACKAGE_PIN N15 [get_ports {JA_P[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JA_P[0]}]

##IO_L22N_T3_AD7N_35
set_property PACKAGE_PIN L15 [get_ports {JA_N[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JA_N[1]}]

##IO_L22P_T3_AD7P_35
set_property PACKAGE_PIN L14 [get_ports {JA_P[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JA_P[1]}]

##IO_L24N_T3_AD15N_35
set_property PACKAGE_PIN J16 [get_ports {JA_N[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JA_N[2]}]

##IO_L24P_T3_AD15P_35
set_property PACKAGE_PIN K16 [get_ports {JA_P[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JA_P[2]}]

##IO_L20N_T3_AD6N_35
set_property PACKAGE_PIN J14 [get_ports {JA_N[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JA_N[3]}]

##IO_L20P_T3_AD6P_35
set_property PACKAGE_PIN K14 [get_ports {JA_P[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JA_P[3]}]

##Pmod Header JB
##IO_L15N_T2_DQS_34
set_property PACKAGE_PIN U20 [get_ports {JB_N[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JB_N[0]}]

##IO_L15P_T2_DQS_34
set_property PACKAGE_PIN T20 [get_ports {JB_P[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JB_P[0]}]

##IO_L16N_T2_34
set_property PACKAGE_PIN W20 [get_ports {JB_N[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JB_N[1]}]

##IO_L16P_T2_34
set_property PACKAGE_PIN V20 [get_ports {JB_P[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JB_P[1]}]

##IO_L17N_T2_34
set_property PACKAGE_PIN Y19 [get_ports {JB_N[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JB_N[2]}]

##IO_L17P_T2_34
set_property PACKAGE_PIN Y18 [get_ports {JB_P[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JB_P[2]}]

##IO_L22N_T3_34
set_property PACKAGE_PIN W19 [get_ports {JB_N[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JB_N[3]}]

##IO_L22P_T3_34
set_property PACKAGE_PIN W18 [get_ports {JB_P[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JB_P[3]}]

##Pmod Header JC
##IO_L10N_T1_34
set_property PACKAGE_PIN W15 [get_ports {JC_N[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JC_N[0]}]

##IO_L10P_T1_34
set_property PACKAGE_PIN V15 [get_ports {JC_P[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JC_P[0]}]

##IO_L1N_T0_34
set_property PACKAGE_PIN T10 [get_ports {JC_N[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JC_N[1]}]

##IO_L1P_T0_34
set_property PACKAGE_PIN T11 [get_ports {JC_P[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JC_P[1]}]

##IO_L8N_T1_34
set_property PACKAGE_PIN Y14 [get_ports {JC_N[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JC_N[2]}]

##IO_L8P_T1_34
set_property PACKAGE_PIN W14 [get_ports {JC_P[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JC_P[2]}]

##IO_L2N_T0_34
set_property PACKAGE_PIN U12 [get_ports {JC_N[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JC_N[3]}]

##IO_L2P_T0_34
set_property PACKAGE_PIN T12 [get_ports {JC_P[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JC_P[3]}]

##Pmod Header JD
##IO_L5N_T0_34
set_property PACKAGE_PIN T15 [get_ports {JD_N[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD_N[0]}]

##IO_L5P_T0_34
set_property PACKAGE_PIN T14 [get_ports {JD_P[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD_P[0]}]

##IO_L6N_T0_VREF_34
set_property PACKAGE_PIN R14 [get_ports {JD_N[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD_N[1]}]

##IO_L6P_T0_34
set_property PACKAGE_PIN P14 [get_ports {JD_P[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD_P[1]}]

##IO_L11N_T1_SRCC_34
set_property PACKAGE_PIN U15 [get_ports {JD_N[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD_N[2]}]

##IO_L11P_T1_SRCC_34
set_property PACKAGE_PIN U14 [get_ports {JD_P[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD_P[2]}]

##IO_L21N_T3_DQS_34
set_property PACKAGE_PIN V18 [get_ports {JD_N[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD_N[3]}]

##IO_L21P_T3_DQS_34
set_property PACKAGE_PIN V17 [get_ports {JD_P[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JD_P[3]}]

##Pmod Header JE
##IO_L4P_T0_34
set_property PACKAGE_PIN V12 [get_ports {JE[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JE[0]}]

##IO_L18N_T2_34
set_property PACKAGE_PIN W16 [get_ports {JE[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JE[1]}]

##IO_25_35
set_property PACKAGE_PIN J15 [get_ports {JE[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JE[2]}]

##IO_L19P_T3_35
set_property PACKAGE_PIN H15 [get_ports {JE[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JE[3]}]

##IO_L3N_T0_DQS_34
set_property PACKAGE_PIN V13 [get_ports {JE[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JE[4]}]

##IO_L9N_T1_DQS_34
set_property PACKAGE_PIN U17 [get_ports {JE[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JE[5]}]

##IO_L20P_T3_34
set_property PACKAGE_PIN T17 [get_ports {JE[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JE[6]}]

##IO_L7N_T1_34
set_property PACKAGE_PIN Y17 [get_ports {JE[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {JE[7]}]


##USB-OTG overcurrent detect pin
##IO_L3P_T0_DQS_PUDC_B_34
#set_property PACKAGE_PIN U13 [get_ports OTG_OC]
#set_property IOSTANDARD LVCMOS33 [get_ports OTG_OC]


##VGA Connector
##IO_L7P_T1_AD2P_35
#set_property PACKAGE_PIN M19 [get_ports {VGA_R[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_R[0]}]

##IO_L9N_T1_DQS_AD3N_35
#set_property PACKAGE_PIN L20 [get_ports {VGA_R[1]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_R[1]}]

##IO_L17P_T2_AD5P_35
#set_property PACKAGE_PIN J20 [get_ports {VGA_R[2]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_R[2]}]

##IO_L18N_T2_AD13N_35
#set_property PACKAGE_PIN G20 [get_ports {VGA_R[3]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_R[3]}]

##IO_L15P_T2_DQS_AD12P_35
#set_property PACKAGE_PIN F19 [get_ports {VGA_R[4]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_R[4]}]

##IO_L14N_T2_AD4N_SRCC_35
#set_property PACKAGE_PIN H18 [get_ports {VGA_G[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_G[0]}]

##IO_L14P_T2_SRCC_34
#set_property PACKAGE_PIN N20 [get_ports {VGA_G[1]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_G[1]}]

##IO_L9P_T1_DQS_AD3P_35
#set_property PACKAGE_PIN L19 [get_ports {VGA_G[2]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_G[2]}]

##IO_L10N_T1_AD11N_35
#set_property PACKAGE_PIN J19 [get_ports {VGA_G[3]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_G[3]}]

##IO_L17N_T2_AD5N_35
#set_property PACKAGE_PIN H20 [get_ports {VGA_G[4]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_G[4]}]

##IO_L15N_T2_DQS_AD12N_35
#set_property PACKAGE_PIN F20 [get_ports {VGA_G[5]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_G[5]}]

##IO_L14N_T2_SRCC_34
#set_property PACKAGE_PIN P20 [get_ports {VGA_B[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_B[0]}]

##IO_L7N_T1_AD2N_35
#set_property PACKAGE_PIN M20 [get_ports {VGA_B[1]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_B[1]}]

##IO_L10P_T1_AD11P_35
#set_property PACKAGE_PIN K19 [get_ports {VGA_B[2]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_B[2]}]

##IO_L14P_T2_AD4P_SRCC_35
#set_property PACKAGE_PIN J18 [get_ports {VGA_B[3]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_B[3]}]

##IO_L18P_T2_AD13P_35
#set_property PACKAGE_PIN G19 [get_ports {VGA_B[4]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {VGA_B[4]}]

##IO_L13N_T2_MRCC_34
#set_property PACKAGE_PIN P19 [get_ports VGA_HS]
#set_property IOSTANDARD LVCMOS33 [get_ports VGA_HS]

##IO_0_34
#set_property PACKAGE_PIN R19 [get_ports VGA_VS]
#set_property IOSTANDARD LVCMOS33 [get_ports VGA_VS]

