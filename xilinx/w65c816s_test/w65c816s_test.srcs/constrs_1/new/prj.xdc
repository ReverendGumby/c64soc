create_generated_clock -name cpumem_clk [get_pins design_1_i/design_1_i/clk_wiz_1/inst/mmcm_adv_inst/CLKOUT0]
create_generated_clock -name vid_mclk [get_pins design_1_i/design_1_i/clk_wiz_1/inst/mmcm_adv_inst/CLKOUT1]

set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]

