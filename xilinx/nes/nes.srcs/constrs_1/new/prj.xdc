create_generated_clock -name dvi_pclk [get_pins design_1_i/design_1_i/clk_wiz_0/inst/mmcm_adv_inst/CLKOUT0]
create_generated_clock -name dvi_bclk [get_pins design_1_i/design_1_i/clk_wiz_0/inst/mmcm_adv_inst/CLKOUT1]
create_generated_clock -name sid_mclk [get_pins design_1_i/design_1_i/clk_wiz_1/inst/mmcm_adv_inst/CLKOUT0]
create_generated_clock -name nes_mclk [get_pins design_1_i/design_1_i/clk_wiz_1/inst/mmcm_adv_inst/CLKOUT1]

set_clock_groups -asynchronous -group [get_clocks dvi_pclk] -group [get_clocks nes_mclk] -group [get_clocks sid_mclk]

set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]

