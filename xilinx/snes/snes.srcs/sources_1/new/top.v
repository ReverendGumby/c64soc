`timescale 1ns / 1ps

module top
  (
   inout [53:0] MIO,

   inout        DDR_Clk,
   inout        DDR_Clk_n,
   inout        DDR_CKE,
   inout        DDR_CS_n,
   inout        DDR_RAS_n,
   inout        DDR_CAS_n,
   inout        DDR_WEB,
   inout [2:0]  DDR_BankAddr,
   inout [14:0] DDR_Addr,
   inout        DDR_ODT,
   inout        DDR_DRSTB,
   inout [31:0] DDR_DQ,
   inout [3:0]  DDR_DM,
   inout [3:0]  DDR_DQS,
   inout [3:0]  DDR_DQS_n,
   inout        DDR_VRN,
   inout        DDR_VRP,

   inout        PS_SRSTB,
   inout        PS_PORB,
   inout        PS_CLK,

   output [2:0] HDMI_DP,
   output [2:0] HDMI_DN,
   output       HDMI_CLK_P,
   output       HDMI_CLK_N,
   output       HDMI_OUT_EN,

   output       AC_BCLK,
   output       AC_MCLK,
   output       AC_MUTEN,
   output       AC_PBDAT,
   output       AC_PBLRC,
   input        AC_RECDAT,
   input        AC_RECLRC,

   inout        AC_SCL,
   inout        AC_SDA,

   input [3:0]  SW,
   input [3:0]  BTN,
   output [3:0] LED,

   output [3:0] JA_N, // Pmod Header JA
   output [3:0] JA_P,
   inout [3:0]  JB_N, // Pmod Header JB
   inout [3:0]  JB_P,
   output [3:0] JC_N, // Pmod Header JC
   output [3:0] JC_P,
   output [3:0] JD_N, // Pmod Header JD
   output [3:0] JD_P,
   output [7:0] JE    // Pmod Header JE
   );

localparam FB_BASEADDR = 32'h10000000;

wire            FCLK_CLK0;
wire            dvi_pclk;
wire [7:0]      dvi_r, dvi_g, dvi_b;
wire [11:0]     dvi_hblank_dcnt, dvi_vblank_dcnt;
wire            dvi_hs, dvi_vs, dvi_de;
wire            dvi_bclk;
wire [29:0]     dvi_serout;
wire [3:0]      sw_db;
wire [3:0]      btn_db;

wire            vid_pce, vid_de, vid_hs, vid_vs;
wire [7:0]      vid_r;
wire [7:0]      vid_g;
wire [7:0]      vid_b;

wire            emu_mclk, emu_nres;

wire            aud_mclk, aud_sclk, aud_lrclk, aud_sdout;
wire [15:0]     aud_pdoutl, aud_pdoutr;

wire            fb_ARESETn;
wire [31:0]     fb_AWADDR;
wire [2:0]      fb_AWSIZE;
wire [3:0]      fb_AWLEN;
wire [1:0]      fb_AWBURST;
wire [3:0]      fb_AWCACHE;
wire [2:0]      fb_AWPROT;
wire            fb_AWVALID;
wire            fb_AWREADY;
wire [31:0]     fb_WDATA;
wire [3:0]      fb_WSTRB;
wire            fb_WLAST;
wire            fb_WVALID;
wire            fb_WREADY;
wire [1:0]      fb_BRESP;
wire            fb_BVALID;
wire            fb_BREADY;
wire [31:0]     fb_ARADDR;
wire [3:0]      fb_ARLEN;
wire [2:0]      fb_ARSIZE;
wire [1:0]      fb_ARBURST;
wire [3:0]      fb_ARCACHE;
wire [2:0]      fb_ARPROT;
wire            fb_ARVALID;
wire            fb_ARREADY;
wire [31:0]     fb_RDATA;
wire [1:0]      fb_RRESP;
wire            fb_RVALID;
wire            fb_RREADY;

wire            cpumem_clk;
wire [31:0]     cpumem_ARADDR;
wire [1:0]      cpumem_ARBURST;
wire [3:0]      cpumem_ARCACHE;
wire [2:0]      cpumem_ARID;
wire [3:0]      cpumem_ARLEN;
wire [1:0]      cpumem_ARLOCK;
wire [2:0]      cpumem_ARPROT;
wire [3:0]      cpumem_ARQOS;
wire            cpumem_ARREADY;
wire [2:0]      cpumem_ARSIZE;
wire [4:0]      cpumem_ARUSER;
wire            cpumem_ARVALID;
wire [31:0]     cpumem_AWADDR;
wire [1:0]      cpumem_AWBURST;
wire [3:0]      cpumem_AWCACHE;
wire [2:0]      cpumem_AWID;
wire [3:0]      cpumem_AWLEN;
wire [1:0]      cpumem_AWLOCK;
wire [2:0]      cpumem_AWPROT;
wire [3:0]      cpumem_AWQOS;
wire            cpumem_AWREADY;
wire [2:0]      cpumem_AWSIZE;
wire [4:0]      cpumem_AWUSER;
wire            cpumem_AWVALID;
wire [2:0]      cpumem_BID;
wire            cpumem_BREADY;
wire [1:0]      cpumem_BRESP;
wire            cpumem_BVALID;
wire [63:0]     cpumem_RDATA;
wire [2:0]      cpumem_RID;
wire            cpumem_RLAST;
wire            cpumem_RREADY;
wire [1:0]      cpumem_RRESP;
wire            cpumem_RVALID;
wire [63:0]     cpumem_WDATA;
wire [2:0]      cpumem_WID;
wire            cpumem_WLAST;
wire            cpumem_WREADY;
wire [7:0]      cpumem_WSTRB;
wire            cpumem_WVALID;

wire            emubus_ARESETn;
wire [31:0]     emubus_AWADDR;
wire            emubus_AWVALID;
wire            emubus_AWREADY;
wire [31:0]     emubus_WDATA;
wire            emubus_WREADY;
wire [3:0]      emubus_WSTRB;
wire            emubus_WVALID;
wire [1:0]      emubus_BRESP;
wire            emubus_BVALID;
wire            emubus_BREADY;
wire [31:0]     emubus_ARADDR;
wire            emubus_ARVALID;
wire            emubus_ARREADY;
wire [31:0]     emubus_RDATA;
wire [1:0]      emubus_RRESP;
wire            emubus_RVALID;
wire            emubus_RREADY;

wire            sysctl_ARESETn;
wire [31:0]     sysctl_AWADDR;
wire            sysctl_AWVALID;
wire            sysctl_AWREADY;
wire [31:0]     sysctl_WDATA;
wire            sysctl_WREADY;
wire [3:0]      sysctl_WSTRB;
wire            sysctl_WVALID;
wire [1:0]      sysctl_BRESP;
wire            sysctl_BVALID;
wire            sysctl_BREADY;
wire [31:0]     sysctl_ARADDR;
wire            sysctl_ARVALID;
wire            sysctl_ARREADY;
wire [31:0]     sysctl_RDATA;
wire [1:0]      sysctl_RRESP;
wire            sysctl_RVALID;
wire            sysctl_RREADY;

//////////////////////////////////////////////////////////////////////
// design_1 (PS7 + friends) instance

// Clocks:
//
// CPUMEM_CLK = 3x PPU master clock
// VID_MCLK = PPU master clock = 236250/11 KHz
// AUD_MCLK = APU oscillator = 24576 KHz
// DVI_PCLK: HD video pixel clock: 720p = 74.25 MHz
// DVI_BCLK: HD video bit block = DVI_PCLK * 5

(* BOX_TYPE = "user_black_box" *)
design_1_wrapper design_1_i
  (
   .AC_scl_io ( AC_SCL ),
   .AC_sda_io ( AC_SDA ),
   .AUD_MCLK ( aud_mclk ),
   .CPUMEM_CLK ( cpumem_clk ),
   .CPUMEM_araddr ( cpumem_ARADDR ),
   .CPUMEM_arburst ( cpumem_ARBURST ),
   .CPUMEM_arcache ( cpumem_ARCACHE ),
   .CPUMEM_arid ( cpumem_ARID ),
   .CPUMEM_arlen ( cpumem_ARLEN ),
   .CPUMEM_arlock ( cpumem_ARLOCK ),
   .CPUMEM_arprot ( cpumem_ARPROT ),
   .CPUMEM_arqos ( cpumem_ARQOS ),
   .CPUMEM_arready ( cpumem_ARREADY ),
   .CPUMEM_arsize ( cpumem_ARSIZE ),
   .CPUMEM_aruser ( cpumem_ARUSER ),
   .CPUMEM_arvalid ( cpumem_ARVALID ),
   .CPUMEM_awaddr ( cpumem_AWADDR ),
   .CPUMEM_awburst ( cpumem_AWBURST ),
   .CPUMEM_awcache ( cpumem_AWCACHE ),
   .CPUMEM_awid ( cpumem_AWID ),
   .CPUMEM_awlen ( cpumem_AWLEN ),
   .CPUMEM_awlock ( cpumem_AWLOCK ),
   .CPUMEM_awprot ( cpumem_AWPROT ),
   .CPUMEM_awqos ( cpumem_AWQOS ),
   .CPUMEM_awready ( cpumem_AWREADY ),
   .CPUMEM_awsize ( cpumem_AWSIZE ),
   .CPUMEM_awuser ( cpumem_AWUSER ),
   .CPUMEM_awvalid ( cpumem_AWVALID ),
   .CPUMEM_bid ( cpumem_BID ),
   .CPUMEM_bready ( cpumem_BREADY ),
   .CPUMEM_bresp ( cpumem_BRESP ),
   .CPUMEM_bvalid ( cpumem_BVALID ),
   .CPUMEM_rdata ( cpumem_RDATA ),
   .CPUMEM_rid ( cpumem_RID ),
   .CPUMEM_rlast ( cpumem_RLAST ),
   .CPUMEM_rready ( cpumem_RREADY ),
   .CPUMEM_rresp ( cpumem_RRESP ),
   .CPUMEM_rvalid ( cpumem_RVALID ),
   .CPUMEM_wdata ( cpumem_WDATA ),
   .CPUMEM_wid ( cpumem_WID ),
   .CPUMEM_wlast ( cpumem_WLAST ),
   .CPUMEM_wready ( cpumem_WREADY ),
   .CPUMEM_wstrb ( cpumem_WSTRB ),
   .CPUMEM_wvalid ( cpumem_WVALID ),
   .DDR_addr ( DDR_Addr ),
   .DDR_ba ( DDR_BankAddr ),
   .DDR_cas_n ( DDR_CAS_n ),
   .DDR_ck_n ( DDR_Clk_n ),
   .DDR_ck_p ( DDR_Clk ),
   .DDR_cke ( DDR_CKE ),
   .DDR_cs_n ( DDR_CS_n ),
   .DDR_dm ( DDR_DM ),
   .DDR_dq ( DDR_DQ ),
   .DDR_dqs_n ( DDR_DQS_n ),
   .DDR_dqs_p ( DDR_DQS ),
   .DDR_odt ( DDR_ODT ),
   .DDR_ras_n ( DDR_RAS_n ),
   .DDR_reset_n ( DDR_DRSTB ),
   .DDR_we_n ( DDR_WEB ),
   .DVI_BCLK ( dvi_bclk ),
   .DVI_PCLK ( dvi_pclk ),
   .EMUBUS_ARESETn ( emubus_ARESETn ),
   .EMUBUS_araddr ( emubus_ARADDR ),
   .EMUBUS_arprot (),
   .EMUBUS_arready ( emubus_ARREADY ),
   .EMUBUS_arvalid ( emubus_ARVALID ),
   .EMUBUS_awaddr ( emubus_AWADDR ),
   .EMUBUS_awprot (),
   .EMUBUS_awready ( emubus_AWREADY ),
   .EMUBUS_awvalid ( emubus_AWVALID ),
   .EMUBUS_bready ( emubus_BREADY ),
   .EMUBUS_bresp ( emubus_BRESP ),
   .EMUBUS_bvalid ( emubus_BVALID ),
   .EMUBUS_rdata ( emubus_RDATA ),
   .EMUBUS_rready ( emubus_RREADY ),
   .EMUBUS_rresp ( emubus_RRESP ),
   .EMUBUS_rvalid ( emubus_RVALID ),
   .EMUBUS_wdata ( emubus_WDATA ),
   .EMUBUS_wready ( emubus_WREADY ),
   .EMUBUS_wstrb ( emubus_WSTRB ),
   .EMUBUS_wvalid ( emubus_WVALID ),
   .FB_ARESETn ( fb_ARESETn ),
   .FB_araddr ( fb_ARADDR ),
   .FB_arburst ( fb_ARBURST ),
   .FB_arcache ( fb_ARCACHE ),
   .FB_arid (),
   .FB_arlen ( fb_ARLEN ),
   .FB_arlock (),
   .FB_arprot ( fb_ARPROT ),
   .FB_arqos (),
   .FB_arready ( fb_ARREADY ),
   .FB_arsize ( fb_ARSIZE ),
   .FB_arvalid ( fb_ARVALID ),
   .FB_awaddr ( fb_AWADDR ),
   .FB_awburst ( fb_AWBURST ),
   .FB_awcache ( fb_AWCACHE ),
   .FB_awid (),
   .FB_awlen ( fb_AWLEN ),
   .FB_awlock (),
   .FB_awprot ( fb_AWPROT ),
   .FB_awqos (),
   .FB_awready ( fb_AWREADY ),
   .FB_awsize ( fb_AWSIZE ),
   .FB_awvalid ( fb_AWVALID ),
   .FB_bid (),
   .FB_bready ( fb_BREADY ),
   .FB_bresp ( fb_BRESP ),
   .FB_bvalid ( fb_BVALID ),
   .FB_rdata ( fb_RDATA ),
   .FB_rid (),
   .FB_rlast (),
   .FB_rready ( fb_RREADY ),
   .FB_rresp ( fb_RRESP ),
   .FB_rvalid ( fb_RVALID ),
   .FB_wdata ( fb_WDATA ),
   .FB_wid (),
   .FB_wlast ( fb_WLAST ),
   .FB_wready ( fb_WREADY ),
   .FB_wstrb ( fb_WSTRB ),
   .FB_wvalid ( fb_WVALID ),
   .FIXED_IO_ddr_vrn ( DDR_VRN ),
   .FIXED_IO_ddr_vrp ( DDR_VRP ),
   .FIXED_IO_mio ( MIO ),
   .FIXED_IO_ps_clk ( PS_CLK ),
   .FIXED_IO_ps_porb ( PS_PORB ),
   .FIXED_IO_ps_srstb ( PS_SRSTB ),
   .SYSCTL_ARESETn ( sysctl_ARESETn ),
   .SYSCTL_araddr ( sysctl_ARADDR ),
   .SYSCTL_arprot (),
   .SYSCTL_arready ( sysctl_ARREADY ),
   .SYSCTL_arvalid ( sysctl_ARVALID ),
   .SYSCTL_awaddr ( sysctl_AWADDR ),
   .SYSCTL_awprot (),
   .SYSCTL_awready ( sysctl_AWREADY ),
   .SYSCTL_awvalid ( sysctl_AWVALID ),
   .SYSCTL_bready ( sysctl_BREADY ),
   .SYSCTL_bresp ( sysctl_BRESP ),
   .SYSCTL_bvalid ( sysctl_BVALID ),
   .SYSCTL_rdata ( sysctl_RDATA ),
   .SYSCTL_rready ( sysctl_RREADY ),
   .SYSCTL_rresp ( sysctl_RRESP ),
   .SYSCTL_rvalid ( sysctl_RVALID ),
   .SYSCTL_wdata ( sysctl_WDATA ),
   .SYSCTL_wready ( sysctl_WREADY ),
   .SYSCTL_wstrb ( sysctl_WSTRB ),
   .SYSCTL_wvalid ( sysctl_WVALID ),
   .VID_MCLK ( vid_mclk )
   );

assign emu_mclk = vid_mclk;

//////////////////////////////////////////////////////////////////////
// System-level SW control interface

sysctl sysctl
  (
   .CLK(emu_mclk),

   .ARESETn(sysctl_ARESETn),
   .AWADDR(sysctl_AWADDR),
   .AWVALID(sysctl_AWVALID),
   .AWREADY(sysctl_AWREADY),
   .WDATA(sysctl_WDATA),
   .WREADY(sysctl_WREADY),
   .WSTRB(sysctl_WSTRB),
   .WVALID(sysctl_WVALID),
   .BRESP(sysctl_BRESP),
   .BVALID(sysctl_BVALID),
   .BREADY(sysctl_BREADY),
   .ARADDR(sysctl_ARADDR),
   .ARVALID(sysctl_ARVALID),
   .ARREADY(sysctl_ARREADY),
   .RDATA(sysctl_RDATA),
   .RRESP(sysctl_RRESP),
   .RVALID(sysctl_RVALID),
   .RREADY(sysctl_RREADY),

   .HMI_SW(sw_db),
   .HMI_BTN(btn_db),
   .HMI_LED(LED),

   .VID_RESET(vid_reset),
   .VID_HDMI_MODE(hdmi_mode),

   .AUD_MUTE(aud_mute),
   .AUD_SDOUT_EN(aud_sdout_en)
   );

//////////////////////////////////////////////////////////////////////
// Emulation system

wire [31:0] debug;
wire        btn_rst = btn_db[0];
wire        btn_key = btn_db[3];
wire        sw_src = sw_db[0];

emu emu
  (
   .DEBUG(debug[31:0]),
   .CPUMEM_CLK(cpumem_clk),
   .MCLK(emu_mclk),
   .AUD_MCLK(aud_mclk),
   .EXT_HOLD(1'b0),
   .EXT_RES(btn_rst),
   .EMU_nRES(emu_nres),
   .BTN_KEY(btn_key),
   .SW_SRC(sw_src),

   .VID_PCE(vid_pce),
   .VID_DE(vid_de),
   .VID_HS(vid_hs),
   .VID_VS(vid_vs),
   .VID_R(vid_r),
   .VID_G(vid_g),
   .VID_B(vid_b),

   .AUD_SCLK(aud_sclk),
   .AUD_LRCLK(aud_lrclk),
   .AUD_SDOUT(aud_sdout),
   .AUD_PDOUTL(aud_pdoutl),
   .AUD_PDOUTR(aud_pdoutr),

   .EMUBUS_ARESETn(emubus_ARESETn),
   .EMUBUS_AWADDR(emubus_AWADDR),
   .EMUBUS_AWVALID(emubus_AWVALID),
   .EMUBUS_AWREADY(emubus_AWREADY),
   .EMUBUS_WDATA(emubus_WDATA),
   .EMUBUS_WREADY(emubus_WREADY),
   .EMUBUS_WSTRB(emubus_WSTRB),
   .EMUBUS_WVALID(emubus_WVALID),
   .EMUBUS_BRESP(emubus_BRESP),
   .EMUBUS_BVALID(emubus_BVALID),
   .EMUBUS_BREADY(emubus_BREADY),
   .EMUBUS_ARADDR(emubus_ARADDR),
   .EMUBUS_ARVALID(emubus_ARVALID),
   .EMUBUS_ARREADY(emubus_ARREADY),
   .EMUBUS_RDATA(emubus_RDATA),
   .EMUBUS_RRESP(emubus_RRESP),
   .EMUBUS_RVALID(emubus_RVALID),
   .EMUBUS_RREADY(emubus_RREADY),

   .CPUMEM_ARADDR(cpumem_ARADDR),
   .CPUMEM_ARBURST(cpumem_ARBURST),
   .CPUMEM_ARCACHE(cpumem_ARCACHE),
   .CPUMEM_ARID(cpumem_ARID),
   .CPUMEM_ARLEN(cpumem_ARLEN),
   .CPUMEM_ARLOCK(cpumem_ARLOCK),
   .CPUMEM_ARPROT(cpumem_ARPROT),
   .CPUMEM_ARQOS(cpumem_ARQOS),
   .CPUMEM_ARREADY(cpumem_ARREADY),
   .CPUMEM_ARSIZE(cpumem_ARSIZE),
   .CPUMEM_ARUSER(cpumem_ARUSER),
   .CPUMEM_ARVALID(cpumem_ARVALID),
   .CPUMEM_AWADDR(cpumem_AWADDR),
   .CPUMEM_AWBURST(cpumem_AWBURST),
   .CPUMEM_AWCACHE(cpumem_AWCACHE),
   .CPUMEM_AWID(cpumem_AWID),
   .CPUMEM_AWLEN(cpumem_AWLEN),
   .CPUMEM_AWLOCK(cpumem_AWLOCK),
   .CPUMEM_AWPROT(cpumem_AWPROT),
   .CPUMEM_AWQOS(cpumem_AWQOS),
   .CPUMEM_AWREADY(cpumem_AWREADY),
   .CPUMEM_AWSIZE(cpumem_AWSIZE),
   .CPUMEM_AWUSER(cpumem_AWUSER),
   .CPUMEM_AWVALID(cpumem_AWVALID),
   .CPUMEM_BID(cpumem_BID),
   .CPUMEM_BREADY(cpumem_BREADY),
   .CPUMEM_BRESP(cpumem_BRESP),
   .CPUMEM_BVALID(cpumem_BVALID),
   .CPUMEM_RDATA(cpumem_RDATA),
   .CPUMEM_RID(cpumem_RID),
   .CPUMEM_RLAST(cpumem_RLAST),
   .CPUMEM_RREADY(cpumem_RREADY),
   .CPUMEM_RRESP(cpumem_RRESP),
   .CPUMEM_RVALID(cpumem_RVALID),
   .CPUMEM_WDATA(cpumem_WDATA),
   .CPUMEM_WID(cpumem_WID),
   .CPUMEM_WLAST(cpumem_WLAST),
   .CPUMEM_WREADY(cpumem_WREADY),
   .CPUMEM_WSTRB(cpumem_WSTRB),
   .CPUMEM_WVALID(cpumem_WVALID)
   );


//////////////////////////////////////////////////////////////////////
// Video upscaler: SNES PPU to HD video 720p

localparam [11:0] IN_WIDTH = 12'd272;
localparam [11:0] IN_HEIGHT = 12'd239;
localparam [11:0] OUT_WIDTH = 12'd1280;
localparam [11:0] OUT_HEIGHT = 12'd720;
localparam [11:0] OUT_WINDOW_WIDTH = 12'd926; // (270/280) * 960
localparam [11:0] OUT_WINDOW_HEIGHT = IN_HEIGHT * 3;
localparam [11:0] OUT_LEFT = ((OUT_WIDTH - OUT_WINDOW_WIDTH) / 2);
localparam [11:0] OUT_RIGHT = (OUT_LEFT + OUT_WINDOW_WIDTH - 1);
localparam [11:0] OUT_TOP = ((OUT_HEIGHT - OUT_WINDOW_HEIGHT) / 2);
localparam [11:0] OUT_BOTTOM = (OUT_TOP + OUT_WINDOW_HEIGHT - 1);

video_scaler_axi video_scaler_axi
  (
   .RST(~vid_reset),

   .IN_PCLK(vid_mclk),
   .IN_PCE(vid_pce),
   .IN_HS(vid_hs),
   .IN_VS(vid_vs),
   .IN_DE(vid_de),
   .IN_D({ vid_b, vid_g, vid_r }),

   .IN_WIDTH(IN_WIDTH),
   .IN_HEIGHT(IN_HEIGHT),

   .IN_HS_NEG(1'b1),
   .IN_VS_NEG(1'b1),

   .OUT_PCLK(dvi_pclk),
   .OUT_HBLANK_DCNT(dvi_hblank_dcnt),
   .OUT_VBLANK_DCNT(dvi_vblank_dcnt),
   .OUT_HS(dvi_hs),
   .OUT_VS(dvi_vs),
   .OUT_DE(dvi_de),
   .OUT_D({ dvi_b, dvi_g, dvi_r }),

   .OUT_WIDTH(OUT_WIDTH),
   .OUT_HEIGHT(OUT_HEIGHT),
   .OUT_LEFT(OUT_LEFT),
   .OUT_RIGHT(OUT_RIGHT),
   .OUT_TOP(OUT_TOP),
   .OUT_BOTTOM(OUT_BOTTOM),

   .FB_BASEADDR(FB_BASEADDR),

   .FB_AWADDR(fb_AWADDR),
   .FB_AWSIZE(fb_AWSIZE),
   .FB_AWLEN(fb_AWLEN),
   .FB_AWBURST(fb_AWBURST),
   .FB_AWCACHE(fb_AWCACHE),
   .FB_AWPROT(fb_AWPROT),
   .FB_AWVALID(fb_AWVALID),
   .FB_AWREADY(fb_AWREADY),
   .FB_WDATA(fb_WDATA),
   .FB_WSTRB(fb_WSTRB),
   .FB_WLAST(fb_WLAST),
   .FB_WVALID(fb_WVALID),
   .FB_WREADY(fb_WREADY),
   .FB_BRESP(fb_BRESP),
   .FB_BVALID(fb_BVALID),
   .FB_BREADY(fb_BREADY),
   .FB_ARADDR(fb_ARADDR),
   .FB_ARLEN(fb_ARLEN),
   .FB_ARSIZE(fb_ARSIZE),
   .FB_ARBURST(fb_ARBURST),
   .FB_ARCACHE(fb_ARCACHE),
   .FB_ARPROT(fb_ARPROT),
   .FB_ARVALID(fb_ARVALID),
   .FB_ARREADY(fb_ARREADY),
   .FB_RDATA(fb_RDATA),
   .FB_RRESP(fb_RRESP),
   .FB_RVALID(fb_RVALID),
   .FB_RREADY(fb_RREADY),

   .ERROR()
   );
   
//////////////////////////////////////////////////////////////////////
// HDMI transmitter

hdmi_tx #(.AUDIO_SAMPLE_RATE(32000)) hdmi
  (
   .PCLK(dvi_pclk),
   .RESET(vid_reset),

   .MODE(hdmi_mode),            // 0=DVI, 1=HDMI
   .AUD_EN(1'b1),

   .MCLK_DIV(3'd5),             // 24.576MHz / (128 * 32KHz) -> /6
   .MCLK(aud_mclk),
   .FCLK(aud_lrclk),
   .PDINL(aud_pdoutl),
   .PDINR(aud_pdoutr),

   .R(dvi_r),
   .G(dvi_g),
   .B(dvi_b),
   .HS(dvi_hs),
   .VS(dvi_vs),
   .DE(dvi_de),
   .HBLANK_DCNT(dvi_hblank_dcnt),
   .VBLANK_DCNT(dvi_vblank_dcnt),

   .SEROUT(dvi_serout)
   );

OBUFDS
  #(.IOSTANDARD ("TMDS_33"))
obufds_inst
  (.O          (HDMI_CLK_P),
   .OB         (HDMI_CLK_N),
   .I          (dvi_pclk)
   );

serializer serializer
  (
   .clk_in(dvi_bclk),
   .clk_div_in(dvi_pclk),

   .io_reset(vid_reset),
   .clk_reset(1'b0),

   .data_out_from_device(dvi_serout),
   .data_out_to_pins_p(HDMI_DP),
   .data_out_to_pins_n(HDMI_DN)
   );

assign HDMI_OUT_EN = 1'b1;      // HDMI is a source

//////////////////////////////////////////////////////////////////////
// Board-level HMI

debounce sw0_db(.CLK(emu_mclk), .D(SW[0]), .Q(sw_db[0]));
debounce sw1_db(.CLK(emu_mclk), .D(SW[1]), .Q(sw_db[1]));
debounce sw2_db(.CLK(emu_mclk), .D(SW[2]), .Q(sw_db[2]));
debounce sw3_db(.CLK(emu_mclk), .D(SW[3]), .Q(sw_db[3]));

debounce btn0_db(.CLK(emu_mclk), .D(BTN[0]), .Q(btn_db[0]));
debounce btn1_db(.CLK(emu_mclk), .D(BTN[1]), .Q(btn_db[1]));
debounce btn2_db(.CLK(emu_mclk), .D(BTN[2]), .Q(btn_db[2]));
debounce btn3_db(.CLK(emu_mclk), .D(BTN[3]), .Q(btn_db[3]));

//////////////////////////////////////////////////////////////////////
// Audio CODEC interface

assign AC_BCLK = aud_sclk;
assign AC_MCLK = aud_mclk;
assign AC_MUTEN = ~aud_mute;
assign AC_PBDAT = aud_sdout_en & aud_sdout;
assign AC_PBLRC = aud_lrclk;

//////////////////////////////////////////////////////////////////////
// Pmod Headers

assign JA_N = 4'b0;
assign JA_P = 4'b0;

wire [3:0]  jb_n_i, jb_p_i;
wire [3:0]  jb_n_o, jb_p_o;
wire [3:0]  jb_n_t, jb_p_t;

assign jb_n_o = 4'b0;
assign jb_p_o = 4'b0;

// tristate when t = 1
assign jb_n_t = 4'b1111;
assign jb_p_t = 4'b1111;

IOBUF IOBUF_jb_n_0 (.O(jb_n_i[0]), .I(jb_n_o[0]), .T(jb_n_t[0]), .IO(JB_N[0]));
IOBUF IOBUF_jb_n_1 (.O(jb_n_i[1]), .I(jb_n_o[1]), .T(jb_n_t[1]), .IO(JB_N[1]));
IOBUF IOBUF_jb_n_2 (.O(jb_n_i[2]), .I(jb_n_o[2]), .T(jb_n_t[2]), .IO(JB_N[2]));
IOBUF IOBUF_jb_n_3 (.O(jb_n_i[3]), .I(jb_n_o[3]), .T(jb_n_t[3]), .IO(JB_N[3]));
IOBUF IOBUF_jb_p_0 (.O(jb_p_i[0]), .I(jb_p_o[0]), .T(jb_p_t[0]), .IO(JB_P[0]));
IOBUF IOBUF_jb_p_1 (.O(jb_p_i[1]), .I(jb_p_o[1]), .T(jb_p_t[1]), .IO(JB_P[1]));
IOBUF IOBUF_jb_p_2 (.O(jb_p_i[2]), .I(jb_p_o[2]), .T(jb_p_t[2]), .IO(JB_P[2]));
IOBUF IOBUF_jb_p_3 (.O(jb_p_i[3]), .I(jb_p_o[3]), .T(jb_p_t[3]), .IO(JB_P[3]));

assign JC_N = { debug[23], debug[21], debug[19], debug[17] };
assign JC_P = { debug[22], debug[20], debug[18], debug[16] };
assign JD_N = { debug[15], debug[13], debug[11], debug[9] };
assign JD_P = { debug[14], debug[12], debug[10], debug[8] };
assign JE = debug[7:0];

endmodule
