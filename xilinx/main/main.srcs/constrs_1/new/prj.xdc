set_false_path -from [get_clocks dvi_pclk_design_1_clk_wiz_0_0] -to [get_clocks clk_fpga_2]
set_false_path -from [get_clocks clk_fpga_2] -to [get_clocks dvi_pclk_design_1_clk_wiz_0_0]

set_clock_groups -asynchronous -group [get_clocks sid_mclk_design_1_clk_wiz_1_2_1] -group [get_clocks dvi_pclk_design_1_clk_wiz_0_0]

set_clock_groups -asynchronous -group [get_clocks clk_fpga_2] -group [get_clocks sid_mclk_design_1_clk_wiz_1_2_1]

set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]
