`timescale 1ns / 1ps

module c64bus_tb();

reg pclk;

reg            c64bus_ARESETn;
reg [31:0]     c64bus_AWADDR;
reg            c64bus_AWVALID;
wire           c64bus_AWREADY;
reg [31:0]     c64bus_WDATA;
wire           c64bus_WREADY;
reg [3:0]      c64bus_WSTRB;
reg            c64bus_WVALID;
wire [1:0]     c64bus_BRESP;
wire           c64bus_BVALID;
reg            c64bus_BREADY;
reg [31:0]     c64bus_ARADDR;
reg            c64bus_ARVALID;
wire           c64bus_ARREADY;
wire [31:0]    c64bus_RDATA;
wire [1:0]     c64bus_RRESP;
wire           c64bus_RVALID;
reg            c64bus_RREADY;

c64 c64
  (
   .DOT_CLOCK(pclk),
   .EXT_RST(),
   .VID_RSTn(),
   .VIC_R(),
   .VIC_G(),
   .VIC_B(),
   .VIC_HS(),
   .VIC_VS(),
   .VIC_DE(),
   .ARESETn(c64bus_ARESETn),
   .AWADDR(c64bus_AWADDR),
   .AWVALID(c64bus_AWVALID),
   .AWREADY(c64bus_AWREADY),
   .WDATA(c64bus_WDATA),
   .WREADY(c64bus_WREADY),
   .WSTRB(c64bus_WSTRB),
   .WVALID(c64bus_WVALID),
   .BRESP(c64bus_BRESP),
   .BVALID(c64bus_BVALID),
   .BREADY(c64bus_BREADY),
   .ARADDR(c64bus_ARADDR),
   .ARVALID(c64bus_ARVALID),
   .ARREADY(c64bus_ARREADY),
   .RDATA(c64bus_RDATA),
   .RRESP(c64bus_RRESP),
   .RVALID(c64bus_RVALID),
   .RREADY(c64bus_RREADY)
   );

initial
  pclk = 1'b1;

always #62.5 begin :pclkgen
  pclk = !pclk;
end

task read(input [31:0] addr);
  begin
    @(posedge pclk) ;
    c64bus_ARADDR <= addr;
    c64bus_ARVALID <= 1'b1;
    c64bus_RREADY <= 1'b1;

    while (!c64bus_ARREADY)
      @(posedge pclk) ;

    c64bus_ARVALID <= 1'b0;

    while (!c64bus_RVALID)
      @(posedge pclk) ;

    c64bus_RREADY <= 1'b0;
  end
endtask

task write(input [31:0] addr, input [31:0] data, input [3:0] strb);
  begin
    @(posedge pclk) ;
    c64bus_AWADDR <= addr;
    c64bus_AWVALID <= 1'b1;
    c64bus_WDATA <= data;
    c64bus_WSTRB <= strb;
    c64bus_WVALID <= 1'b1;
    c64bus_BREADY <= 1'b1;

    while (!c64bus_AWREADY && !c64bus_WREADY)
      @(posedge pclk) ;

    c64bus_AWVALID <= 1'b0;
    c64bus_WVALID <= 1'b0;

    while (!c64bus_BVALID)
      @(posedge pclk) ;

    c64bus_BREADY <= 1'b0;
  end
endtask

task write32(input [31:0] addr, input [31:0] data);
  write(addr, data, 4'hf);
endtask

task write8(input [31:0] addr, input [7:0] data);
  reg [31:0] data32;
  begin
    data32 = {32{1'bx}};
    case (addr[1:0])
      2'b11: data32[31:24] = data;
      2'b10: data32[23:16] = data;
      2'b01: data32[15:08] = data;
      2'b00: data32[07:00] = data;
      default: ;
    endcase
    write(addr, data32, 4'b1 << addr[1:0]);
  end
endtask

task test_cart();
begin
  write8(32'h01000, 8'h01);    // ccsg_enable = 1
  write8(32'h01004, 8'h01);    // nEXROM = 0, nGAME = 1

  write32(32'h20000, 32'h92509247); // first 32 bytes of PIPER.CRT
  write32(32'h20004, 32'h38cdc2c3);
  write32(32'h20008, 32'h060e0c30);
  write32(32'h2000c, 32'h7efec606);
  write32(32'h20010, 32'hfe7e0000);
  write32(32'h20014, 32'h0e0606c6);
  write32(32'h20018, 32'h0000000c);
  write32(32'h2001c, 32'h7fffc000);

  read(32'h20000);
  read(32'h2001c);

  read(32'h01000);
  read(32'h01004);
  read(32'h01008);
  read(32'h0100c);
  read(32'h01010);
  read(32'h01014);

  #8000 ;
  write32(32'h00050, 32'h7);    // set c64_usr_nres, VID_nRES, CART_nRES

  write8(32'h11234, 8'haa);
  write8(32'h11235, 8'hbb);
  write8(32'h11236, 8'hcc);
  write8(32'h11237, 8'hdd);

  read(32'h11234);
  read(32'h11235);
  read(32'h11236);
  read(32'h11237);
end
endtask

task test_vic_reg();
begin    
  #20000 ;
  write8(32'h1d020, 8'h01);     // VIC reg
  read(32'h1d020);              // VIC reg

  // Test chip select override
  write32(32'h00058, 32'h0000);
  read(32'h1d011);              // VIC register / CHARGEN

  write32(32'h00058, 32'h8000);
  read(32'h1d011);              // VIC register / CHARGEN

  write32(32'h00058, 32'h8001);
  read(32'h1d011);              // VIC register / CHARGEN

  write32(32'h00058, 32'h0);
end
endtask

task test_vic_hold();
begin
  c64.c64_mb.vic_u19.reg_file.RSEL = 1'b1;
  c64.c64_mb.vic_u19.reg_file.CSEL = 1'b1;
  c64.c64_mb.vic_u19.reg_file.XSCROLL = 3'h0;
  c64.c64_mb.vic_u19.reg_file.VM[13:10] = 4'h1;
  c64.c64_mb.vic_u19.reg_file.CB[13:11] = 4'h2;
  c64.c64_mb.vic_u19.reg_file.EC = 4'he;
  c64.c64_mb.vic_u19.reg_file.B0C = 4'h6;
  c64.c64_mb.vic_u19.reg_file.MCM = 1'b1;

  c64.c64_mb.ram_u21.mem[16'h0400] = 8'h00;
  c64.c64_mb.ram_u21.mem[16'h0401] = 8'h01;
  c64.c64_mb.ram_u21.mem[16'h0402] = 8'h02;
  c64.c64_mb.ram_u21.mem[16'h0403] = 8'h03;
  c64.c64_mb.ram_u21.mem[16'h0404] = 8'h04;
  c64.c64_mb.ram_u21.mem[16'h0405] = 8'h05;
  c64.c64_mb.ram_u21.mem[16'h0406] = 8'h06;
  c64.c64_mb.ram_u21.mem[16'h0407] = 8'h07;
  c64.c64_mb.ram_u21.mem[16'h0408] = 8'h08;
  c64.c64_mb.ram_u21.mem[16'h0409] = 8'h09;
  c64.c64_mb.ram_u21.mem[16'h040a] = 8'h0a;
  c64.c64_mb.ram_u21.mem[16'h040b] = 8'h0b;
  c64.c64_mb.ram_u21.mem[16'h040c] = 8'h0c;
  c64.c64_mb.ram_u21.mem[16'h040d] = 8'h0d;
  c64.c64_mb.ram_u21.mem[16'h040e] = 8'h0e;
  c64.c64_mb.ram_u21.mem[16'h040f] = 8'h0f;
  c64.c64_mb.color_u6.mem[10'h000] = 4'h8;
  c64.c64_mb.color_u6.mem[10'h001] = 4'h0;
  c64.c64_mb.color_u6.mem[10'h002] = 4'h9;
  c64.c64_mb.color_u6.mem[10'h003] = 4'h1;
  c64.c64_mb.color_u6.mem[10'h004] = 4'hc;
  c64.c64_mb.color_u6.mem[10'h005] = 4'hd;
  c64.c64_mb.color_u6.mem[10'h006] = 4'he;
  c64.c64_mb.color_u6.mem[10'h007] = 4'hf;
  c64.c64_mb.color_u6.mem[10'h008] = 4'h8;
  c64.c64_mb.color_u6.mem[10'h009] = 4'h9;
  c64.c64_mb.color_u6.mem[10'h00a] = 4'ha;
  c64.c64_mb.color_u6.mem[10'h00b] = 4'hb;
  c64.c64_mb.color_u6.mem[10'h00c] = 4'hc;
  c64.c64_mb.color_u6.mem[10'h00d] = 4'hd;
  c64.c64_mb.color_u6.mem[10'h00e] = 4'he;
  c64.c64_mb.color_u6.mem[10'h00f] = 4'hf;

  #3313000 ;
  read(32'h10000);
  read(32'h10000);
  read(32'h10000);
  read(32'h10000);
  read(32'h10000);
end
endtask

task test_vic_hold_mob();
begin
  c64.c64_mb.vic_u19.reg_file.RSEL = 1'b1;
  c64.c64_mb.vic_u19.reg_file.CSEL = 1'b1;
  c64.c64_mb.vic_u19.reg_file.XSCROLL = 3'h0;
  c64.c64_mb.vic_u19.reg_file.YSCROLL = 3'h3;
  c64.c64_mb.vic_u19.reg_file.VM[13:10] = 4'h1;
  c64.c64_mb.vic_u19.reg_file.CB[13:11] = 4'h2;
  c64.c64_mb.vic_u19.reg_file.EC = 4'he;
  c64.c64_mb.vic_u19.reg_file.B0C = 4'h6;
  c64.c64_mb.vic_u19.reg_file.MCM = 1'b0;

  c64.c64_mb.ram_u21.mem[16'h0400] = 8'h00;
  c64.c64_mb.ram_u21.mem[16'h0401] = 8'h01;
  c64.c64_mb.ram_u21.mem[16'h0402] = 8'h02;
  c64.c64_mb.ram_u21.mem[16'h0403] = 8'h03;
  c64.c64_mb.ram_u21.mem[16'h0404] = 8'h04;
  c64.c64_mb.ram_u21.mem[16'h0405] = 8'h05;
  c64.c64_mb.ram_u21.mem[16'h0406] = 8'h06;
  c64.c64_mb.ram_u21.mem[16'h0407] = 8'h07;
  c64.c64_mb.ram_u21.mem[16'h0408] = 8'h08;
  c64.c64_mb.ram_u21.mem[16'h0409] = 8'h09;
  c64.c64_mb.ram_u21.mem[16'h040a] = 8'h0a;
  c64.c64_mb.ram_u21.mem[16'h040b] = 8'h0b;
  c64.c64_mb.ram_u21.mem[16'h040c] = 8'h0c;
  c64.c64_mb.ram_u21.mem[16'h040d] = 8'h0d;
  c64.c64_mb.ram_u21.mem[16'h040e] = 8'h0e;
  c64.c64_mb.ram_u21.mem[16'h040f] = 8'h0f;
  c64.c64_mb.color_u6.mem[10'h000] = 4'h9;
  c64.c64_mb.color_u6.mem[10'h001] = 4'ha;
  c64.c64_mb.color_u6.mem[10'h002] = 4'hb;
  c64.c64_mb.color_u6.mem[10'h003] = 4'hc;
  c64.c64_mb.color_u6.mem[10'h004] = 4'hd;
  c64.c64_mb.color_u6.mem[10'h005] = 4'he;
  c64.c64_mb.color_u6.mem[10'h006] = 4'hf;
  c64.c64_mb.color_u6.mem[10'h007] = 4'h1;
  c64.c64_mb.color_u6.mem[10'h008] = 4'h2;
  c64.c64_mb.color_u6.mem[10'h009] = 4'h3;
  c64.c64_mb.color_u6.mem[10'h00a] = 4'h4;
  c64.c64_mb.color_u6.mem[10'h00b] = 4'h5;
  c64.c64_mb.color_u6.mem[10'h00c] = 4'h6;
  c64.c64_mb.color_u6.mem[10'h00d] = 4'h7;
  c64.c64_mb.color_u6.mem[10'h00e] = 4'h9;
  c64.c64_mb.color_u6.mem[10'h00f] = 4'ha;

  c64.c64_mb.ram_u21.mem[16'h07f8] = 8'h20; // MP[0]: 0800

  // MOB 0 data
  c64.c64_mb.ram_u21.mem[16'h0800] = 8'b10101010;
  c64.c64_mb.ram_u21.mem[16'h0801] = 8'b10101010;
  c64.c64_mb.ram_u21.mem[16'h0802] = 8'b10101010;
  c64.c64_mb.ram_u21.mem[16'h0803] = 8'b00000011;
  c64.c64_mb.ram_u21.mem[16'h0804] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0805] = 8'b11000000;
  c64.c64_mb.ram_u21.mem[16'h0806] = 8'b00000111;
  c64.c64_mb.ram_u21.mem[16'h0807] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0808] = 8'b11100000;
  c64.c64_mb.ram_u21.mem[16'h0809] = 8'b00011111;
  c64.c64_mb.ram_u21.mem[16'h080a] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h080b] = 8'b11111000;
  c64.c64_mb.ram_u21.mem[16'h080c] = 8'b00011111;
  c64.c64_mb.ram_u21.mem[16'h080d] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h080e] = 8'b11111000;
  c64.c64_mb.ram_u21.mem[16'h080f] = 8'b00111111;
  c64.c64_mb.ram_u21.mem[16'h0810] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0811] = 8'b11111100;
  c64.c64_mb.ram_u21.mem[16'h0812] = 8'b01111111;
  c64.c64_mb.ram_u21.mem[16'h0813] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0814] = 8'b11111110;
  c64.c64_mb.ram_u21.mem[16'h0815] = 8'b01111111;
  c64.c64_mb.ram_u21.mem[16'h0816] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0817] = 8'b11111110;
  c64.c64_mb.ram_u21.mem[16'h0818] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0819] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h081a] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h081b] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h081c] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h081d] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h081e] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h081f] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0820] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0821] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0822] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0823] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0824] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0825] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0826] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0827] = 8'b01111111;
  c64.c64_mb.ram_u21.mem[16'h0828] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0829] = 8'b11111110;
  c64.c64_mb.ram_u21.mem[16'h082a] = 8'b01111111;
  c64.c64_mb.ram_u21.mem[16'h082b] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h082c] = 8'b11111110;
  c64.c64_mb.ram_u21.mem[16'h082d] = 8'b00111111;
  c64.c64_mb.ram_u21.mem[16'h082e] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h082f] = 8'b11111100;
  c64.c64_mb.ram_u21.mem[16'h0830] = 8'b00011111;
  c64.c64_mb.ram_u21.mem[16'h0831] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0832] = 8'b11111000;
  c64.c64_mb.ram_u21.mem[16'h0833] = 8'b00011111;
  c64.c64_mb.ram_u21.mem[16'h0834] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0835] = 8'b11111000;
  c64.c64_mb.ram_u21.mem[16'h0836] = 8'b00000111;
  c64.c64_mb.ram_u21.mem[16'h0837] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h0838] = 8'b11100000;
  c64.c64_mb.ram_u21.mem[16'h0839] = 8'b00000011;
  c64.c64_mb.ram_u21.mem[16'h083a] = 8'b11111111;
  c64.c64_mb.ram_u21.mem[16'h083b] = 8'b11000000;
  c64.c64_mb.ram_u21.mem[16'h083c] = 8'b00000000;
  c64.c64_mb.ram_u21.mem[16'h083d] = 8'b01111110;
  c64.c64_mb.ram_u21.mem[16'h083e] = 8'b00000000;
  c64.c64_mb.ram_u21.mem[16'h083f] = 8'bxxxxxxxx;

  c64.c64_mb.vic_u19.reg_file.mxx[0] = 9'h022;
  c64.c64_mb.vic_u19.reg_file.mxy[0] = 8'h31;
  c64.c64_mb.vic_u19.reg_file.mxc[0] = 4'h1;
  c64.c64_mb.vic_u19.reg_file.mxxe[0] = 1'b1;
  c64.c64_mb.vic_u19.reg_file.mxye[0] = 1'b1;
  c64.c64_mb.vic_u19.reg_file.mxmc[0] = 1'b0;
  c64.c64_mb.vic_u19.reg_file.mxdp = 8'b00000001;
  c64.c64_mb.vic_u19.reg_file.mxe = 8'b00000001;

  #(3295000) ;                  // MOB data read
  #(21000) ;                    // MOB display
  read(32'h10000);
end
endtask

task test_icd();
begin
  write32(32'h00074, 32'h0200ffff); // MATCH_DATA_EN = SYNC, A[15:0]
  write32(32'h00078, 32'h0200fce2); // MATCH_DATA = SYNC=1, A=reset
  write32(32'h00070, 32'h00010001); // MATCH_ENABLE = 1, CPU_ENABLE = 1

  #8000;
  write32(32'h00050, 32'h7);    // set c64_usr_nres, VID_nRES, CART_nRES

  #16000;                       // CPU is stalled by now
  write32(32'h00070, 32'h00000001); // MATCH_ENABLE = 0
  // enter monitor
  write32(32'h00070, 32'h00000003); // CPU_FORCE_HALT = 1
  write32(32'h00070, 32'h00000001); // CPU_FORCE_HALT = 0
  #8000;                        // monitor does its thing
  // exit monitor
  write32(32'h00070, 32'h00000005); // CPU_RESUME = 1
  read(32'h00070);
  while (c64bus_RDATA[8] == 1'b1) // wait for CLR_RDY=0
    read(32'h00070);
  write32(32'h00070, 32'h00000001); // CPU_RESUME = 0
  #8000;                        // CPU continues
  
end
endtask

task test_irst();
begin
  write8(32'h1d019, 8'h01);     // clear IRST

  c64.c64_mb.vic_u19.reg_file.RSEL = 1'b1;
  c64.c64_mb.vic_u19.reg_file.CSEL = 1'b1;
  c64.c64_mb.vic_u19.reg_file.XSCROLL = 3'h0;
  c64.c64_mb.vic_u19.reg_file.VM[13:10] = 4'h1;
  c64.c64_mb.vic_u19.reg_file.CB[13:11] = 4'h2;
  c64.c64_mb.vic_u19.reg_file.EC = 4'he;
  c64.c64_mb.vic_u19.reg_file.B0C = 4'h6;
  c64.c64_mb.vic_u19.reg_file.MCM = 1'b1;
  c64.c64_mb.vic_u19.reg_file.RASTER_CMP = 9'h002;
  c64.c64_mb.vic_u19.reg_file.ERST = 1'b1;
  c64.c64_mb.vic_u19.reg_file.EMBC = 1'b0;
  c64.c64_mb.vic_u19.reg_file.EMMC = 1'b0;
  c64.c64_mb.vic_u19.reg_file.ELP = 1'b0;

  #65000 ;
  write8(32'h1d019, 8'h01);     // clear IRST

  #65000 ;
  write8(32'h1d019, 8'h01);     // clear IRST

  #65000 ;
  write8(32'h1d019, 8'h01);     // clear IRST

  #65000 ;
  write8(32'h1d019, 8'h01);     // clear IRST
end
endtask

initial begin
  c64bus_ARESETn <= 1'b0;
  c64bus_AWVALID <= 1'b0;
  c64bus_WVALID <= 1'b0;
  c64bus_BREADY <= 1'b0;
  c64bus_ARVALID <= 1'b0;
  c64bus_RREADY <= 1'b0;

  #8000 ;
  c64bus_ARESETn <= 1'b1;

  #8000 ;
  //test_cart();
  //test_vic_reg();
  //test_vic_hold();
  //test_vic_hold_mob();
  //test_icd();
  test_irst();

  #8000 ;
  $stop;
end

endmodule
