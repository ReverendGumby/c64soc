`timescale 1ns / 1ps

module c64_tb
  (
   );

reg pclk, rst;

c64 c64
  (
   .DOT_CLOCK(pclk),
   .EXT_RST(!rst),
   .VID_RSTn(rst),
   .ARESETn(rst)
   );

initial begin
  pclk = 1'b1;
  rst = 1'b0;
  #8000 rst = 1'b1;
  force c64.c64bus.c64_usr_nres = 1'b1;
end

always #62.5 begin :pclkgen
  pclk = !pclk;
end

endmodule
