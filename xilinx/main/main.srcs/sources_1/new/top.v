`timescale 1ns / 1ps

module top
  (
   inout [53:0] MIO,

   inout        DDR_Clk,
   inout        DDR_Clk_n,
   inout        DDR_CKE,
   inout        DDR_CS_n,
   inout        DDR_RAS_n,
   inout        DDR_CAS_n,
   inout        DDR_WEB,
   inout [2:0]  DDR_BankAddr,
   inout [14:0] DDR_Addr,
   inout        DDR_ODT,
   inout        DDR_DRSTB,
   inout [31:0] DDR_DQ,
   inout [3:0]  DDR_DM,
   inout [3:0]  DDR_DQS,
   inout [3:0]  DDR_DQS_n,
   inout        DDR_VRN,
   inout        DDR_VRP,

   inout        PS_SRSTB,
   inout        PS_PORB,
   inout        PS_CLK,
   
   output [2:0] HDMI_DP,
   output [2:0] HDMI_DN,
   output       HDMI_CLK_P,
   output       HDMI_CLK_N,
   output       HDMI_OUT_EN,

   output       AC_BCLK,
   output       AC_MCLK,
   output       AC_MUTEN,
   output       AC_PBDAT,
   output       AC_PBLRC,
   input        AC_RECDAT,
   input        AC_RECLRC,

   inout        AC_SCL,
   inout        AC_SDA,

   input [3:0]  SW,
   input [3:0]  BTN,
   output [3:0] LED,

   output [3:0] JA_N, // Pmod Header JA
   output [3:0] JA_P,
   inout  [3:0] JB_N, // Pmod Header JB
   inout  [3:0] JB_P,
   output [3:0] JC_N, // Pmod Header JC
   output [3:0] JC_P,
   output [3:0] JD_N, // Pmod Header JD
   output [3:0] JD_P,
   output [7:0] JE    // Pmod Header JE
   );

localparam FB_BASEADDR = 32'h10000000;

wire            FCLK_CLK0;
wire            vic_pclk;
wire [7:0]      vic_r, vic_g, vic_b;
wire            vic_hs, vic_vs, vic_de;
wire            dvi_pclk;
wire [7:0]      dvi_r, dvi_g, dvi_b;
wire [11:0]     dvi_hblank_dcnt, dvi_vblank_dcnt;
wire            dvi_hs, dvi_vs, dvi_de;
wire            dvi_bclk;
wire [29:0]     dvi_serout;
wire [3:0]      sw_db;
wire [3:0]      btn_db;

wire            sid_mclk, sid_sclk, sid_lrclk, sid_sdout;
wire [15:0]     sid_pdout;

wire            fb_ARESETn;
wire [31:0]     fb_AWADDR;
wire [2:0]      fb_AWSIZE;
wire [3:0]      fb_AWLEN;
wire [1:0]      fb_AWBURST;
wire [3:0]      fb_AWCACHE;
wire [2:0]      fb_AWPROT;
wire            fb_AWVALID;
wire            fb_AWREADY;
wire [31:0]     fb_WDATA;
wire [3:0]      fb_WSTRB;
wire            fb_WLAST;
wire            fb_WVALID;
wire            fb_WREADY;
wire [1:0]      fb_BRESP;
wire            fb_BVALID;
wire            fb_BREADY;
wire [31:0]     fb_ARADDR;
wire [3:0]      fb_ARLEN;
wire [2:0]      fb_ARSIZE;
wire [1:0]      fb_ARBURST;
wire [3:0]      fb_ARCACHE;
wire [2:0]      fb_ARPROT;
wire            fb_ARVALID;
wire            fb_ARREADY;
wire [31:0]     fb_RDATA;
wire [1:0]      fb_RRESP;
wire            fb_RVALID;
wire            fb_RREADY;

wire            c64bus_ARESETn;
wire [31:0]     c64bus_AWADDR;
wire            c64bus_AWVALID;
wire            c64bus_AWREADY;
wire [31:0]     c64bus_WDATA;
wire            c64bus_WREADY;
wire [3:0]      c64bus_WSTRB;
wire            c64bus_WVALID;
wire [1:0]      c64bus_BRESP;
wire            c64bus_BVALID;
wire            c64bus_BREADY;
wire [31:0]     c64bus_ARADDR;
wire            c64bus_ARVALID;
wire            c64bus_ARREADY;
wire [31:0]     c64bus_RDATA;
wire [1:0]      c64bus_RRESP;
wire            c64bus_RVALID;
wire            c64bus_RREADY;

wire            sysctl_ARESETn;
wire [31:0]     sysctl_AWADDR;
wire            sysctl_AWVALID;
wire            sysctl_AWREADY;
wire [31:0]     sysctl_WDATA;
wire            sysctl_WREADY;
wire [3:0]      sysctl_WSTRB;
wire            sysctl_WVALID;
wire [1:0]      sysctl_BRESP;
wire            sysctl_BVALID;
wire            sysctl_BREADY;
wire [31:0]     sysctl_ARADDR;
wire            sysctl_ARVALID;
wire            sysctl_ARREADY;
wire [31:0]     sysctl_RDATA;
wire [1:0]      sysctl_RRESP;
wire            sysctl_RVALID;
wire            sysctl_RREADY;

//////////////////////////////////////////////////////////////////////
// design_1 (PS7 + friends) instance

// Clocks:
//
// VIC_PCLK: C64 VIC-II pixel clock = 8.192 MHz
// DVI_PCLK: HD video pixel clock: 720p = 74.25 MHz
// DVI_BCLK: HD video bit block = DVI_PCLK * 5

(* BOX_TYPE = "user_black_box" *)
design_1_wrapper design_1_i
  (
   .AC_scl_io ( AC_SCL ),
   .AC_sda_io ( AC_SDA ),
   .C64BUS_ARESETn ( c64bus_ARESETn ),
   .C64BUS_araddr ( c64bus_ARADDR ),
   .C64BUS_arprot (),
   .C64BUS_arready ( c64bus_ARREADY ),
   .C64BUS_arvalid ( c64bus_ARVALID ),
   .C64BUS_awaddr ( c64bus_AWADDR ),
   .C64BUS_awprot (),
   .C64BUS_awready ( c64bus_AWREADY ),
   .C64BUS_awvalid ( c64bus_AWVALID ),
   .C64BUS_bready ( c64bus_BREADY ),
   .C64BUS_bresp ( c64bus_BRESP ),
   .C64BUS_bvalid ( c64bus_BVALID ),
   .C64BUS_rdata ( c64bus_RDATA ),
   .C64BUS_rready ( c64bus_RREADY ),
   .C64BUS_rresp ( c64bus_RRESP ),
   .C64BUS_rvalid ( c64bus_RVALID ),
   .C64BUS_wdata ( c64bus_WDATA ),
   .C64BUS_wready ( c64bus_WREADY ),
   .C64BUS_wstrb ( c64bus_WSTRB ),
   .C64BUS_wvalid ( c64bus_WVALID ),
   .CLKRST (1'b0),
   .DDR_addr ( DDR_Addr ),
   .DDR_ba ( DDR_BankAddr ),
   .DDR_cas_n ( DDR_CAS_n ),
   .DDR_ck_n ( DDR_Clk_n ),
   .DDR_ck_p ( DDR_Clk ),
   .DDR_cke ( DDR_CKE ),
   .DDR_cs_n ( DDR_CS_n ),
   .DDR_dm ( DDR_DM ),
   .DDR_dq ( DDR_DQ ),
   .DDR_dqs_n ( DDR_DQS_n ),
   .DDR_dqs_p ( DDR_DQS ),
   .DDR_odt ( DDR_ODT ),
   .DDR_ras_n ( DDR_RAS_n ),
   .DDR_reset_n ( DDR_DRSTB ),
   .DDR_we_n ( DDR_WEB ),
   .DVI_BCLK ( dvi_bclk ),
   .DVI_PCLK ( dvi_pclk ),
   .FB_ARESETn ( fb_ARESETn ),
   .FB_araddr ( fb_ARADDR ),
   .FB_arburst ( fb_ARBURST ),
   .FB_arcache ( fb_ARCACHE ),
   .FB_arid (),
   .FB_arlen ( fb_ARLEN ),
   .FB_arlock (),
   .FB_arprot ( fb_ARPROT ),
   .FB_arqos (),
   .FB_arready ( fb_ARREADY ),
   .FB_arsize ( fb_ARSIZE ),
   .FB_arvalid ( fb_ARVALID ),
   .FB_awaddr ( fb_AWADDR ),
   .FB_awburst ( fb_AWBURST ),
   .FB_awcache ( fb_AWCACHE ),
   .FB_awid (),
   .FB_awlen ( fb_AWLEN ),
   .FB_awlock (),
   .FB_awprot ( fb_AWPROT ),
   .FB_awqos (),
   .FB_awready ( fb_AWREADY ),
   .FB_awsize ( fb_AWSIZE ),
   .FB_awvalid ( fb_AWVALID ),
   .FB_bid (),
   .FB_bready ( fb_BREADY ),
   .FB_bresp ( fb_BRESP ),
   .FB_bvalid ( fb_BVALID ),
   .FB_rdata ( fb_RDATA ),
   .FB_rid (),
   .FB_rlast (),
   .FB_rready ( fb_RREADY ),
   .FB_rresp ( fb_RRESP ),
   .FB_rvalid ( fb_RVALID ),
   .FB_wdata ( fb_WDATA ),
   .FB_wid (),
   .FB_wlast ( fb_WLAST ),
   .FB_wready ( fb_WREADY ),
   .FB_wstrb ( fb_WSTRB ),
   .FB_wvalid ( fb_WVALID ),
   .FIXED_IO_ddr_vrn ( DDR_VRN ),
   .FIXED_IO_ddr_vrp ( DDR_VRP ),
   .FIXED_IO_mio ( MIO ),
   .FIXED_IO_ps_clk ( PS_CLK ),
   .FIXED_IO_ps_porb ( PS_PORB ),
   .FIXED_IO_ps_srstb ( PS_SRSTB ),
   .SID_MCLK ( sid_mclk ),
   .SYSCTL_ARESETn ( sysctl_ARESETn ),
   .SYSCTL_araddr ( sysctl_ARADDR ),
   .SYSCTL_arprot (),
   .SYSCTL_arready ( sysctl_ARREADY ),
   .SYSCTL_arvalid ( sysctl_ARVALID ),
   .SYSCTL_awaddr ( sysctl_AWADDR ),
   .SYSCTL_awprot (),
   .SYSCTL_awready ( sysctl_AWREADY ),
   .SYSCTL_awvalid ( sysctl_AWVALID ),
   .SYSCTL_bready ( sysctl_BREADY ),
   .SYSCTL_bresp ( sysctl_BRESP ),
   .SYSCTL_bvalid ( sysctl_BVALID ),
   .SYSCTL_rdata ( sysctl_RDATA ),
   .SYSCTL_rready ( sysctl_RREADY ),
   .SYSCTL_rresp ( sysctl_RRESP ),
   .SYSCTL_rvalid ( sysctl_RVALID ),
   .SYSCTL_wdata ( sysctl_WDATA ),
   .SYSCTL_wready ( sysctl_WREADY ),
   .SYSCTL_wstrb ( sysctl_WSTRB ),
   .SYSCTL_wvalid ( sysctl_WVALID ),
   .VIC_PCLK ( vic_pclk )
   );

//////////////////////////////////////////////////////////////////////
// System-level SW control interface

sysctl sysctl
  (
   .CLK(vic_pclk),

   .ARESETn(sysctl_ARESETn),
   .AWADDR(sysctl_AWADDR),
   .AWVALID(sysctl_AWVALID),
   .AWREADY(sysctl_AWREADY),
   .WDATA(sysctl_WDATA),
   .WREADY(sysctl_WREADY),
   .WSTRB(sysctl_WSTRB),
   .WVALID(sysctl_WVALID),
   .BRESP(sysctl_BRESP),
   .BVALID(sysctl_BVALID),
   .BREADY(sysctl_BREADY),
   .ARADDR(sysctl_ARADDR),
   .ARVALID(sysctl_ARVALID),
   .ARREADY(sysctl_ARREADY),
   .RDATA(sysctl_RDATA),
   .RRESP(sysctl_RRESP),
   .RVALID(sysctl_RVALID),
   .RREADY(sysctl_RREADY),

   .HMI_SW(sw_db),
   .HMI_BTN(btn_db),
   .HMI_LED(LED),

   .VID_RESET(vid_reset),
   .VID_HDMI_MODE(hdmi_mode)
   );

//////////////////////////////////////////////////////////////////////
// C64 system

wire [7:0] col;
wire [7:0] row;
wire       nrestore;
wire       sp1 = 1'bz;
wire       sp2 = 1'bz;
wire       pa2 = 1'bz;
wire [7:0] pb = 8'hzz;
wire       cass_sense = 1'bz;
wire        nexrom, ngame;
wire        c64_rw;
wire [31:0] debug;
wire        btn_rst = btn_db[0];

c64 c64
  (
   .DEBUG(debug[6:0]),
   .DOT_CLOCK(vic_pclk),
   .EXT_RST(btn_rst),
   .C64_nRES(c64_nres),
   .VIC_R(vic_r),
   .VIC_G(vic_g),
   .VIC_B(vic_b),
   .VIC_HS(vic_hs),
   .VIC_VS(vic_vs),
   .VIC_DE(vic_de),
   .SID_MCLK(sid_mclk),
   .SID_SCLK(sid_sclk),
   .SID_LRCLK(sid_lrclk),
   .SID_SDOUT(sid_sdout),
   .SID_PDOUT(sid_pdout),
   .DATA_I(data_i),
   .DATA_O(data_o),
   .CLK_I(clk_i),
   .CLK_O(clk_o),
   .ATN(atn),
   .ARESETn(c64bus_ARESETn),
   .AWADDR(c64bus_AWADDR),
   .AWVALID(c64bus_AWVALID),
   .AWREADY(c64bus_AWREADY),
   .WDATA(c64bus_WDATA),
   .WREADY(c64bus_WREADY),
   .WSTRB(c64bus_WSTRB),
   .WVALID(c64bus_WVALID),
   .BRESP(c64bus_BRESP),
   .BVALID(c64bus_BVALID),
   .BREADY(c64bus_BREADY),
   .ARADDR(c64bus_ARADDR),
   .ARVALID(c64bus_ARVALID),
   .ARREADY(c64bus_ARREADY),
   .RDATA(c64bus_RDATA),
   .RRESP(c64bus_RRESP),
   .RVALID(c64bus_RVALID),
   .RREADY(c64bus_RREADY)
   );

//////////////////////////////////////////////////////////////////////
// Video upscaler: C64 VIC-II to HD video 720p

localparam [11:0] IN_WIDTH = 12'd418;
localparam [11:0] IN_HEIGHT = 12'd235;
localparam [11:0] OUT_WIDTH = 12'd1280;
localparam [11:0] OUT_HEIGHT = 12'd720;
localparam [2:0]  OUT_HSCALE = 2'd3;
localparam [2:0]  OUT_VSCALE = 2'd3;
localparam [11:0] OUT_WINDOW_WIDTH = (IN_WIDTH * OUT_HSCALE);
localparam [11:0] OUT_WINDOW_HEIGHT = (IN_HEIGHT * OUT_VSCALE);
localparam [11:0] OUT_LEFT = ((OUT_WIDTH - OUT_WINDOW_WIDTH) / 2);
localparam [11:0] OUT_RIGHT = (OUT_LEFT + OUT_WINDOW_WIDTH - 1);
localparam [11:0] OUT_TOP = ((OUT_HEIGHT - OUT_WINDOW_HEIGHT) / 2);
localparam [11:0] OUT_BOTTOM = (OUT_TOP + OUT_WINDOW_HEIGHT - 1);

video_scaler_axi video_scaler_axi
  (
   .RST(~vid_reset),

   .IN_PCLK(vic_pclk),
   .IN_PCE(1'b1),
   .IN_HS(vic_hs),
   .IN_VS(vic_vs),
   .IN_DE(vic_de),
   .IN_D({ vic_b, vic_g, vic_r }),

   .IN_WIDTH(IN_WIDTH),
   .IN_HEIGHT(IN_HEIGHT),

   .IN_HS_NEG(1'b1),
   .IN_VS_NEG(1'b1),

   .OUT_PCLK(dvi_pclk),
   .OUT_HBLANK_DCNT(dvi_hblank_dcnt),
   .OUT_VBLANK_DCNT(dvi_vblank_dcnt),
   .OUT_HS(dvi_hs),
   .OUT_VS(dvi_vs),
   .OUT_DE(dvi_de),
   .OUT_D({ dvi_b, dvi_g, dvi_r }),

   .OUT_WIDTH(OUT_WIDTH),
   .OUT_HEIGHT(OUT_HEIGHT),
   .OUT_LEFT(OUT_LEFT),
   .OUT_RIGHT(OUT_RIGHT),
   .OUT_TOP(OUT_TOP),
   .OUT_BOTTOM(OUT_BOTTOM),

   .FB_BASEADDR(FB_BASEADDR),

   .FB_AWADDR(fb_AWADDR),
   .FB_AWSIZE(fb_AWSIZE),
   .FB_AWLEN(fb_AWLEN),
   .FB_AWBURST(fb_AWBURST),
   .FB_AWCACHE(fb_AWCACHE),
   .FB_AWPROT(fb_AWPROT),
   .FB_AWVALID(fb_AWVALID),
   .FB_AWREADY(fb_AWREADY),
   .FB_WDATA(fb_WDATA),
   .FB_WSTRB(fb_WSTRB),
   .FB_WLAST(fb_WLAST),
   .FB_WVALID(fb_WVALID),
   .FB_WREADY(fb_WREADY),
   .FB_BRESP(fb_BRESP),
   .FB_BVALID(fb_BVALID),
   .FB_BREADY(fb_BREADY),
   .FB_ARADDR(fb_ARADDR),
   .FB_ARLEN(fb_ARLEN),
   .FB_ARSIZE(fb_ARSIZE),
   .FB_ARBURST(fb_ARBURST),
   .FB_ARCACHE(fb_ARCACHE),
   .FB_ARPROT(fb_ARPROT),
   .FB_ARVALID(fb_ARVALID),
   .FB_ARREADY(fb_ARREADY),
   .FB_RDATA(fb_RDATA),
   .FB_RRESP(fb_RRESP),
   .FB_RVALID(fb_RVALID),
   .FB_RREADY(fb_RREADY),

   .ERROR()
   );
   
//////////////////////////////////////////////////////////////////////
// HDMI transmitter

hdmi_tx hdmi
  (
   .PCLK(dvi_pclk),
   .RESET(vid_reset),

   .MODE(hdmi_mode),            // 0=DVI, 1=HDMI
   .AUD_EN(1'b1),

   .MCLK_DIV(3'd3),             // 24.576 / 6.144 -> /4
   .MCLK(sid_mclk),
   .FCLK(sid_lrclk),
   .PDINL(sid_pdout),
   .PDINR(sid_pdout),

   .R(dvi_r),
   .G(dvi_g),
   .B(dvi_b),
   .HS(dvi_hs),
   .VS(dvi_vs),
   .DE(dvi_de),
   .HBLANK_DCNT(dvi_hblank_dcnt),
   .VBLANK_DCNT(dvi_vblank_dcnt),

   .SEROUT(dvi_serout)
   );

OBUFDS
  #(.IOSTANDARD ("TMDS_33"))
obufds_inst
  (.O          (HDMI_CLK_P),
   .OB         (HDMI_CLK_N),
   .I          (dvi_pclk)
   );

serializer serializer
  (
   .clk_in(dvi_bclk),
   .clk_div_in(dvi_pclk),

   .io_reset(vid_reset),
   .clk_reset(1'b0),

   .data_out_from_device(dvi_serout),
   .data_out_to_pins_p(HDMI_DP),
   .data_out_to_pins_n(HDMI_DN)
   );

assign HDMI_OUT_EN = 1'b1;      // HDMI is a source

//////////////////////////////////////////////////////////////////////
// Board-level HMI

debounce sw0_db(.CLK(vic_pclk), .D(SW[0]), .Q(sw_db[0]));
debounce sw1_db(.CLK(vic_pclk), .D(SW[1]), .Q(sw_db[1]));
debounce sw2_db(.CLK(vic_pclk), .D(SW[2]), .Q(sw_db[2]));
debounce sw3_db(.CLK(vic_pclk), .D(SW[3]), .Q(sw_db[3]));

debounce btn0_db(.CLK(vic_pclk), .D(BTN[0]), .Q(btn_db[0]));
debounce btn1_db(.CLK(vic_pclk), .D(BTN[1]), .Q(btn_db[1]));
debounce btn2_db(.CLK(vic_pclk), .D(BTN[2]), .Q(btn_db[2]));
debounce btn3_db(.CLK(vic_pclk), .D(BTN[3]), .Q(btn_db[3]));

//////////////////////////////////////////////////////////////////////
// Audio CODEC interface

assign AC_BCLK = sid_sclk;
assign AC_MCLK = sid_mclk;
assign AC_MUTEN = 1'b1; // un-mute
assign AC_PBDAT = sid_sdout;
assign AC_PBLRC = sid_lrclk;

//////////////////////////////////////////////////////////////////////
// Pmod Headers

assign JA_N = 4'b0;
assign JA_P = 4'b0;

// IEC 01: c64soc IEC (Serial Port) Adapter
// Inputs and outputs are all inverted from 6-pin DIN levels
wire [3:0]  jb_n_i, jb_p_i;
wire [3:0]  jb_n_o, jb_p_o;
wire [3:0]  jb_n_t, jb_p_t;

assign data_i = ~jb_p_i[0];     // JB1 = DATA_I
assign clk_i = ~jb_n_i[0];      // JB2 = CLK_I
assign atn_i = ~jb_p_i[1];      // JB3 = ATN_I
assign rst_i = ~jb_n_i[1];      // JB4 = RST_I
assign jb_p_o[2] = ~data_o;     // JB7 = DATA_O
assign jb_n_o[2] = ~clk_o;      // JB8 = CLK_O
assign jb_p_o[3] = ~atn;        // JB9 = ATN_O
assign jb_n_o[3] = ~c64_nres;   // JB10= RST_O

// tristate when t = 1
assign jb_n_t = 4'b0011;
assign jb_p_t = 4'b0011;

IOBUF IOBUF_jb_n_0 (.O(jb_n_i[0]), .I(jb_n_o[0]), .T(jb_n_t[0]), .IO(JB_N[0]));
IOBUF IOBUF_jb_n_1 (.O(jb_n_i[1]), .I(jb_n_o[1]), .T(jb_n_t[1]), .IO(JB_N[1]));
IOBUF IOBUF_jb_n_2 (.O(jb_n_i[2]), .I(jb_n_o[2]), .T(jb_n_t[2]), .IO(JB_N[2]));
IOBUF IOBUF_jb_n_3 (.O(jb_n_i[3]), .I(jb_n_o[3]), .T(jb_n_t[3]), .IO(JB_N[3]));
IOBUF IOBUF_jb_p_0 (.O(jb_p_i[0]), .I(jb_p_o[0]), .T(jb_p_t[0]), .IO(JB_P[0]));
IOBUF IOBUF_jb_p_1 (.O(jb_p_i[1]), .I(jb_p_o[1]), .T(jb_p_t[1]), .IO(JB_P[1]));
IOBUF IOBUF_jb_p_2 (.O(jb_p_i[2]), .I(jb_p_o[2]), .T(jb_p_t[2]), .IO(JB_P[2]));
IOBUF IOBUF_jb_p_3 (.O(jb_p_i[3]), .I(jb_p_o[3]), .T(jb_p_t[3]), .IO(JB_P[3]));

assign JC_N = { debug[23], debug[21], debug[19], debug[17] };
assign JC_P = { debug[22], debug[20], debug[18], debug[16] };
assign JD_N = { debug[15], debug[13], debug[11], debug[9] };
assign JD_P = { debug[14], debug[12], debug[10], debug[8] };
assign JE = debug[7:0];

endmodule
