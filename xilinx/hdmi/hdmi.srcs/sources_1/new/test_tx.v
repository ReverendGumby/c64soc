`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/26/2019 11:20:03 AM
// Design Name: 
// Module Name: test_tx
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`define TP_WIDTH    12'd1280
`define TP_HEIGHT   12'd720

module test_tx(
    input         PCLK,
    input         MCLK,
    input [3:0]   SW,
    output        HDMI_OUT_EN,
    output        PHYRSTB,
    (* X_INTERFACE_PARAMETER = "POLARITY ACTIVE_HIGH" *)
    output        CLK_RESET,
    (* X_INTERFACE_PARAMETER = "POLARITY ACTIVE_HIGH" *)
    output        IO_RESET,
    output [29:0] SEROUT,
    output [15:0] DEBUG
    );

wire [11:0] x, y;
wire [11:0] hblank_dcnt, vblank_dcnt;
wire [7:0]  r, g, b;
wire [3:0]  swd;
wire        hs, vs, de;

// Reset generator

reg [7:0] rst_dcnt;
reg       rst;

initial begin
  rst_dcnt = 8'd100;
end

always @(posedge PCLK) begin
  if (rst_dcnt != 0)
    rst_dcnt <= rst_dcnt - 1'd1;
  rst <= rst_dcnt != 8'd0;
end

// Sync rst to MCLK
(* ASYNC_REG = "TRUE" *)
reg rst_mclk_s, rst_mclk;
always @(posedge MCLK) begin
  rst_mclk_s <= rst;
  rst_mclk <= rst_mclk_s;
end

// Video signals

video_timing video_timing
  (
   .CLK(PCLK),
   .RST(~rst),

   .WIDTH(`TP_WIDTH),
   .HEIGHT(`TP_HEIGHT),

   .X(x),
   .Y(y),

   .HS(hs),
   .VS(vs),
   .DE(de),

   .HBLANK_DCNT(hblank_dcnt),
   .VBLANK_DCNT(vblank_dcnt)
   );

video_test_pattern video_test_pattern
  (
   .CLK(PCLK),

   .WIDTH(`TP_WIDTH),
   .HEIGHT(`TP_HEIGHT),
   .MODE(3'd5),

   .X(x),
   .Y(y),
   .DE(de),
   
   .R(r),
   .G(g),
   .B(b)
   );

// Audio signals
// MCLK = 24.576 MHz = Fs * 512

reg signed [15:0] pdin;
reg [8:0] fclk_cnt;

always @(posedge MCLK) begin
  if (rst_mclk)
    fclk_cnt <= 9'd0;
  else
    fclk_cnt <= fclk_cnt + 1'd1;
end

wire fclk = fclk_cnt[8];

// 1 kHz sine tone
// From MATLAB code:
//   fs=48000; ft=1000; t=(0:fs/ft)/(fs/ft); floor(a*sinpi(2*t))
reg signed [15:0] ttab [0:47];
initial begin
  ttab[00] = 16'd0;
  ttab[01] = 16'd2138;
  ttab[02] = 16'd4240;
  ttab[03] = 16'd6269;
  ttab[04] = 16'd8192;
  ttab[05] = 16'd9973;
  ttab[06] = 16'd11585;
  ttab[07] = 16'd12998;
  ttab[08] = 16'd14188;
  ttab[09] = 16'd15136;
  ttab[10] = 16'd15825;
  ttab[11] = 16'd16243;
  ttab[12] = 16'd16384;
  ttab[13] = 16'd16243;
  ttab[14] = 16'd15825;
  ttab[15] = 16'd15136;
  ttab[16] = 16'd14188;
  ttab[17] = 16'd12998;
  ttab[18] = 16'd11585;
  ttab[19] = 16'd9973;
  ttab[20] = 16'd8192;
  ttab[21] = 16'd6269;
  ttab[22] = 16'd4240;
  ttab[23] = 16'd2138;
  ttab[24] = 16'd0;
  ttab[25] = -16'd2139;
  ttab[26] = -16'd4241;
  ttab[27] = -16'd6270;
  ttab[28] = -16'd8192;
  ttab[29] = -16'd9974;
  ttab[30] = -16'd11586;
  ttab[31] = -16'd12999;
  ttab[32] = -16'd14189;
  ttab[33] = -16'd15137;
  ttab[34] = -16'd15826;
  ttab[35] = -16'd16244;
  ttab[36] = -16'd16384;
  ttab[37] = -16'd16244;
  ttab[38] = -16'd15826;
  ttab[39] = -16'd15137;
  ttab[40] = -16'd14189;
  ttab[41] = -16'd12999;
  ttab[42] = -16'd11586;
  ttab[43] = -16'd9974;
  ttab[44] = -16'd8193;
  ttab[45] = -16'd6270;
  ttab[46] = -16'd4241;
  ttab[47] = -16'd2139;
end

reg [5:0] ttab_idx;
always @(posedge MCLK) begin
  if (rst_mclk)
    ttab_idx <= 0;
  else if (fclk_cnt == -9'd1)   // just before frame start
    ttab_idx <= ttab_idx == 6'd47 ? 0 : ttab_idx + 1'd1;
end

always @* pdin = ttab[ttab_idx];

// HMI

debounce #32 sw0_db(.CLK(PCLK), .D(SW[0]), .Q(mode));
debounce #32 sw1_db(.CLK(PCLK), .D(SW[1]), .Q(aud_en));
debounce #32 sw2_db(.CLK(PCLK), .D(SW[2]), .Q(swd[2]));
debounce #32 sw3_db(.CLK(PCLK), .D(SW[3]), .Q(swd[3]));

// The HDMI TX pipeline

hdmi_tx hdmi_tx
  (
   .PCLK(PCLK),
   .RESET(rst),

   .MODE(mode),
   .AUD_EN(aud_en),

   .MCLK_DIV(3'd3),             // 24.576 / 6.144 -> /4
   .MCLK(MCLK),
   .FCLK(fclk),
   .PDINL(pdinl),
   .PDINR(pdinr),

   .R(r),
   .G(g),
   .B(b),
   .HS(hs),
   .VS(vs),
   .DE(de),
   .HBLANK_DCNT(hblank_dcnt),
   .VBLANK_DCNT(vblank_dcnt),

   .SEROUT(SEROUT)
   );

assign HDMI_OUT_EN = 1'b1;      // HDMI is a source
assign PHYRSTB = 1'b1;          // Enable 125 MHz clock

assign CLK_RESET = 1'b0;
assign IO_RESET = rst;

assign DEBUG[4:0] = hdmi_tx.enc_mux_sel;
assign DEBUG[6] = hdmi_tx.audio_src.sample_src_valid;
assign DEBUG[7] = hdmi_tx.audio_src.sample_ecc_valid;

assign DEBUG[15:8] = hdmi_tx.audio_src.clk_regen_src.dbg_cts_bs;

endmodule
