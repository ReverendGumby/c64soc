`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/26/2019 04:03:42 PM
// Design Name: 
// Module Name: test_tx_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_tx_tb
  (
   output [9:0] enc0_q,
   output [9:0] enc1_q,
   output [9:0] enc2_q,
   output [15:0] debug
   );

wire [29:0] serout;
reg pclk, mclk;

// Clock generator

initial pclk = 1'b0;
always #6.734 begin :pclkgen    // 74.250 MHz
  pclk = !pclk;
end

initial mclk = 1'b0;
always #20.345 begin :mclkgen   // 24.576 MHz
  mclk = !mclk;
end

wire mode = 1'b1;
wire aud_en = 1'b1;

test_tx tx 
  (
   .PCLK(pclk),
   .MCLK(mclk),
   .SW({aud_en, mode}),
   .SEROUT(serout),
   .DEBUG(debug)
   );

assign enc0_q = { serout[27], serout[24], serout[21], serout[18], serout[15], serout[12], serout[9], serout[6], serout[3], serout[0] };
assign enc1_q = { serout[28], serout[25], serout[22], serout[19], serout[16], serout[13], serout[10], serout[7], serout[4], serout[1] };
assign enc2_q = { serout[29], serout[26], serout[23], serout[20], serout[17], serout[14], serout[11], serout[8], serout[5], serout[2] };

endmodule
