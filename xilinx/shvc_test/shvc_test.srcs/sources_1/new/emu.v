// Emulation system:
// - S-SMP / PS7 bridge
// - S-PPU

module emu
  (
   output [31:0] DEBUG,
   input         CLK, // master clock input: 21477273 Hz
   input         AUD_MCLK, // audio clock input: 24576000 Hz
   input         EXT_RES,
   output        EMU_nRES,
   input         BTN_KEY,
   input         SW_SRC,

   // Audio output
   output        AUD_SCLK,
   output        AUD_LRCLK,
   output        AUD_SDOUT,
   output [15:0] AUD_PDOUTL,
   output [15:0] AUD_PDOUTR,

   // AXI Lite slave bus interface
   input         EMUBUS_ARESETn,
   input [31:0]  EMUBUS_AWADDR,
   input         EMUBUS_AWVALID,
   output        EMUBUS_AWREADY,
   input [31:0]  EMUBUS_WDATA,
   output        EMUBUS_WREADY,
   input [3:0]   EMUBUS_WSTRB,
   input         EMUBUS_WVALID,
   output [1:0]  EMUBUS_BRESP,
   output        EMUBUS_BVALID,
   input         EMUBUS_BREADY,
   input [31:0]  EMUBUS_ARADDR,
   input         EMUBUS_ARVALID,
   output        EMUBUS_ARREADY,
   output [31:0] EMUBUS_RDATA,
   output [1:0]  EMUBUS_RRESP,
   output        EMUBUS_RVALID,
   input         EMUBUS_RREADY,

   // Video output
   input         VID_RESET,
   output        VID_PCE,
   output        VID_DE,
   output        VID_HS,
   output        VID_VS,
   output [7:0]  VID_R,
   output [7:0]  VID_G,
   output [7:0]  VID_B
   );

/* -----\/----- EXCLUDED -----\/-----
wire            emu_hold_req, emu_hold_ack;
wire            emubus_hold_req;
 -----/\----- EXCLUDED -----/\----- */

wire            emubus_apu_re, emubus_apu_we;
wire [7:0]      emubus_apu_d_o, emubus_apu_d_i;
wire [15:0]     emubus_apu_a_o;

wire [7:0]      apu_mon_sel;
wire            apu_mon_ready;
wire [31:0]     apu_mon_dout_smp, apu_mon_dout_dsp;
reg [31:0]      apu_mon_dout;
wire            apu_mon_valid_smp, apu_mon_valid_dsp;
reg             apu_mon_valid;
wire            apu_mon_ws;
wire [31:0]     apu_mon_din;

wire [15:0]     aud_pdoutl, aud_pdoutr;

wire            hblank, vblank;

wire [7:0]      emubus_io_pa;
wire [7:0]      emubus_io_d_i, emubus_io_d_o;
wire            emubus_io_npard, emubus_io_npawr;

//////////////////////////////////////////////////////////////////////
// Da music

reg        nRES;

wire [15:0] a, a_smp, a_reggen, ma;
wire [7:0]  di_dsp, do_smp, do_dsp, do_reggen;
wire [7:0]  mdo_dsp, do_ram;
wire        cken;
wire        rw_smp, rw_reggen;
wire        nCE, nWE, nOE;

reggen reggen
  (
   .nRES(nRES),
   .CLK(AUD_MCLK),
   .CKEN(cken),
   .KEY(BTN_KEY),
   .A(a_reggen),
   .DO(do_reggen),
   .RW(rw_reggen)
   );

s_smp s_smp
  (
   .nRES(nRES),
   .CLK(AUD_MCLK),
   .CKEN(cken),
   .A(a_smp),
   .DI(do_dsp),
   .DO(do_smp),
   .RW(rw_smp),
   .CPUI0(8'b0),
   .CPUI1(8'b0),
   .CPUI2(8'b0),
   .CPUI3(8'b0),
   .CPUIN(4'b0),
   .CPUO0(),
   .CPUO1(),
   .CPUO2(),
   .CPUO3(),
   .CPUON(),
   .MON_SEL(apu_mon_sel),
   .MON_ACK(apu_mon_ack_smp),
   .MON_READY(apu_mon_ready),
   .MON_DOUT(apu_mon_dout_smp),
   .MON_VALID(apu_mon_valid_smp),
   .MON_WS(apu_mon_ws),
   .MON_DIN(apu_mon_din)
   );

s_dsp s_dsp
  (
   .nRES(nRES),
   .CLK(AUD_MCLK),
   .CKEN(cken),
   .A(a),
   .DI(di_dsp),
   .DO(do_dsp),
   .RW(rw),
   .MA(ma),
   .MDI(do_ram),
   .MDO(mdo_dsp),
   .nCE(nCE),
   .nWE(nWE),
   .nOE(nOE),
   .PSOUTL(aud_pdoutl),
   .PSOUTR(aud_pdoutr),
   .MON_SEL(apu_mon_sel),
   .MON_ACK(apu_mon_ack_dsp),
   .MON_READY(apu_mon_ready),
   .MON_DOUT(apu_mon_dout_dsp),
   .MON_VALID(apu_mon_valid_dsp),
   .MON_WS(apu_mon_ws),
   .MON_DIN(apu_mon_din)
   );

dpram ram
  (
   .CLK(AUD_MCLK),

   .A(ma), 
   .DI(mdo_dsp), 
   .DO(do_ram), 
   .nCE(nCE), 
   .nWE(nWE),
   .nOE(nOE),

   .A2(emubus_apu_a_o),
   .DI2(emubus_apu_d_o),
   .DO2(emubus_apu_d_i),
   .nCE2(~(emubus_apu_re | emubus_apu_we)),
   .nWE2(~emubus_apu_we),
   .nOE2(~emubus_apu_re)
   );

always @* nRES = emu_nres;

assign a = SW_SRC ? a_reggen : a_smp;
assign rw = SW_SRC ? rw_reggen : rw_smp;
assign di_dsp = SW_SRC ? do_reggen : do_smp;

always @* begin
  apu_mon_valid = 1'b0;
  if (apu_mon_ack_smp) begin
    apu_mon_valid = apu_mon_valid_smp;
    apu_mon_dout = apu_mon_dout_smp;
  end
  else if (apu_mon_ack_dsp) begin
    apu_mon_valid = apu_mon_valid_dsp;
    apu_mon_dout = apu_mon_dout_dsp;
  end
  else begin
    apu_mon_valid = 1'b1;
    apu_mon_dout = 32'b0;
  end
end


//////////////////////////////////////////////////////////////////////
// EMU / PS7 bridge

emubus emubus
  (
   .CLK(CLK),
   .CLKEN(1'b1),
   .APU_MCLK(AUD_MCLK),

   .ARESETn(EMUBUS_ARESETn),
   .AWADDR(EMUBUS_AWADDR),
   .AWVALID(EMUBUS_AWVALID),
   .AWREADY(EMUBUS_AWREADY),
   .WDATA(EMUBUS_WDATA),
   .WREADY(EMUBUS_WREADY),
   .WSTRB(EMUBUS_WSTRB),
   .WVALID(EMUBUS_WVALID),
   .BRESP(EMUBUS_BRESP),
   .BVALID(EMUBUS_BVALID),
   .BREADY(EMUBUS_BREADY),
   .ARADDR(EMUBUS_ARADDR),
   .ARVALID(EMUBUS_ARVALID),
   .ARREADY(EMUBUS_ARREADY),
   .RDATA(EMUBUS_RDATA),
   .RRESP(EMUBUS_RRESP),
   .RVALID(EMUBUS_RVALID),
   .RREADY(EMUBUS_RREADY),

   .EXT_RES(EXT_RES),
   .CLK_RESET(clk_reset),
   .EMU_nRES(emu_nres),
   .RES_COLD(res_cold),

   .APU_RAM_RE(emubus_apu_re),
   .APU_RAM_WE(emubus_apu_we),
   .APU_RAM_D_I(emubus_apu_d_i),
   .APU_RAM_D_O(emubus_apu_d_o),
   .APU_RAM_A_O(emubus_apu_a_o),

   .APU_MON_SEL(apu_mon_sel),
   .APU_MON_READY(apu_mon_ready),
   .APU_MON_DOUT(apu_mon_dout),
   .APU_MON_VALID(apu_mon_valid),
   .APU_MON_WS(apu_mon_ws),
   .APU_MON_DIN(apu_mon_din),

   .IO_PA(emubus_io_pa),
   .IO_PA_OE(),
   .IO_D_I(emubus_io_d_i),
   .IO_D_O(emubus_io_d_o),
   .IO_D_OE(),
   .IO_nPARD(emubus_io_npard),
   .IO_nPAWR(emubus_io_npawr),

   .PPU_HBLANK(hblank),
   .PPU_VBLANK(vblank)
   );

/* -----\/----- EXCLUDED -----\/-----
// Merge hold requests.
assign emu_hold_req = emubus_hold_req | icd_sys_hold;
 -----/\----- EXCLUDED -----/\----- */

//////////////////////////////////////////////////////////////////////
// I2S audio output

i2s_serializer ser
  (
   .MCLK(AUD_MCLK),
   .OCLKEN(),
   .PDINL(aud_pdoutl),
   .PDINR(aud_pdoutr),
   .SCLK(AUD_SCLK),
   .LRCLK(AUD_LRCLK),
   .SDOUT(AUD_SDOUT)
   );

assign AUD_PDOUTL = aud_pdoutl;
assign AUD_PDOUTR = aud_pdoutr;


//////////////////////////////////////////////////////////////////////
// Video subsystem

wire [15:0] vaa, vab;
wire [7:0]  vda_i, vda_o, vdb_i, vdb_o;
wire        nvard, nvawr, nvbrd, nvbwr;

s_ppu ppu
  (
   .CLK_RESET(1'b0),
   .CLK(CLK),
   .CLKEN(1'b1),
   .RES_COLD(1'b0),
   .RESET(VID_RESET),
   .HOLD(1'b0),

   .MON_SEL(),
   .MON_READY(),
   .MON_DOUT(),
   .MON_VALID(),

   .ICD_COL(),
   .ICD_ROW(),
   .ICD_FRAME(),

   .CC(),
   .nCC(),
   .HBLANK(hblank),
   .VBLANK(vblank),
   .EXTLATCH(),

   .PA(emubus_io_pa),
   .D_I(emubus_io_d_o),
   .D_O(emubus_io_d_i),
   .D_OE(),
   .nPARD(emubus_io_npard),
   .nPAWR(emubus_io_npawr),

   .VAA(vaa),
   .VDA_I(vda_i),
   .VDA_O(vda_o),
   .nVARD(nvard),
   .nVAWR(nvawr),

   .VAB(vab),
   .VDB_I(vdb_i),
   .VDB_O(vdb_o),
   .nVBRD(nvbrd),
   .nVBWR(nvbwr),

   .PCE(VID_PCE), // pixel clock enable
   .DE(VID_DE), // data enable
   .HS(VID_HS), // horizontal sync
   .VS(VID_VS), // vertical sync
   .R(VID_R), // red component
   .G(VID_G), // green component
   .B(VID_B)  // blue component
   );

dpram #(.DWIDTH(8), .AWIDTH(15)) vrama
  (
   .CLK(CLK),

   .nCE(nvard & nvawr),
   .nWE(nvawr),
   .nOE(nvard),
   .A(vaa[14:0]),
   .DI(vda_o),
   .DO(vda_i),

   .nCE2(1'b1),
   .nWE2(1'b1),
   .nOE2(1'b1),
   .A2(),
   .DI2(),
   .DO2()
   );

dpram #(.DWIDTH(8), .AWIDTH(15)) vramb
  (
   .CLK(CLK),

   .nCE(nvbrd & nvbwr),
   .nWE(nvbwr),
   .nOE(nvbrd),
   .A(vab[14:0]),
   .DI(vdb_o),
   .DO(vdb_i),

   .nCE2(1'b1),
   .nWE2(1'b1),
   .nOE2(1'b1),
   .A2(),
   .DI2(),
   .DO2()
   );


//////////////////////////////////////////////////////////////////////
// Debug

//assign DEBUG[8] = 0;
//assign DEBUG[9] = s_dsp.sgenloop.voice.brr_out_next;
//assign DEBUG[10] = s_dsp.sgenloop.voice.interp_in_next;
//assign DEBUG[11] = s_dsp.sgenloop.voice.interp_play;
//assign DEBUG[12] = s_dsp.sgenloop.voice.interp.gos[1];
//assign DEBUG[13] = s_dsp.sgenloop.voice.brr_out_vsel[2];
//assign DEBUG[14] = s_dsp.sgenloop.voice.interp_in_vsel[2];
//assign DEBUG[15] = AUD_SDOUT;

endmodule
