module sram_32k_16
  (
   input         CLK,
   input         CLKEN,
   input         nCE,
   input         nOE,
   input         nWE,
   input [14:0]  A,
   input [15:0]  D_I,
   output [15:0] D_O,
   output        D_OE
   );

reg [15:0] mem [0:(1<<15)-1];
reg [15:0] d;

always @(posedge CLK) if (CLKEN) begin
  d <= mem[A];
  if (!nCE && !nWE)
    mem[A] <= D_I;
end

assign D_O = d;
assign D_OE = !nCE && !nOE && nWE;

endmodule
