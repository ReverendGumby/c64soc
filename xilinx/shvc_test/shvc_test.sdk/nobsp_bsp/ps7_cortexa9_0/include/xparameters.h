#ifndef XPARAMETERS_H   /* prevent circular inclusions */
#define XPARAMETERS_H   /* by using protection macros */

/* Definition for CPU ID */
#define XPAR_CPU_ID 0U

/* Definitions for peripheral PS7_CORTEXA9_0 */
#define XPAR_PS7_CORTEXA9_0_CPU_CLK_FREQ_HZ 650000000


/******************************************************************/

/* Canonical definitions for peripheral PS7_CORTEXA9_0 */
#define XPAR_CPU_CORTEXA9_0_CPU_CLK_FREQ_HZ 650000000


/******************************************************************/


/* Definitions for interface SYSCTL */
#define XPAR_SYSCTL_BASEADDR 0x43C00000
#define XPAR_SYSCTL_HIGHADDR 0x43C00FFF


/* Definitions for interface EMUBUS */
#define XPAR_EMUBUS_BASEADDR 0x78800000
#define XPAR_EMUBUS_HIGHADDR 0x7883FFFF


/* Definitions for peripheral PS7_DDR_0 */
#define XPAR_PS7_DDR_0_S_AXI_BASEADDR 0x00100000
#define XPAR_PS7_DDR_0_S_AXI_HIGHADDR 0x1FFFFFFF


/******************************************************************/

#endif  /* end of protection macro */
