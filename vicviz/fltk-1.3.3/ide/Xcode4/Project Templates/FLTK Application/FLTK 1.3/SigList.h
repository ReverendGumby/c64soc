//
//  SigList.h
//  vicviz-fltk
//
//  Created by Hunter, David on 1/11/15.
//
//

#ifndef __vicviz_fltk__SigList__
#define __vicviz_fltk__SigList__

#include <string>
#include <vector>

class SigList
{
public:
    SigList();
    SigList(std::initializer_list<std::string>);

    int add(const std::string &name);

    const std::string &name_of(int id) const;
    int id_of(const std::string &name) const;
    int size() const { return list.size(); }

private:
    std::vector<std::string> list;
};

#endif /* defined(__vicviz_fltk__SigList__) */
