//
//  VcdReader.h
//  vicviz-fltk
//
//  Created by Hunter, David on 1/8/15.
//
//

#ifndef __vicviz_fltk__VcdReader__
#define __vicviz_fltk__VcdReader__

#include <string>
#include <vector>
#include <unordered_map>
#include <iostream>
#include <fstream>

#include "VcdVector.h"
#include "SigList.h"

class VcdReader
{
public:
    VcdReader(const std::string &fn, const SigList &sl);
    
    typedef std::function<void()> file_shrunk_cb_t;
    void set_file_shrunk_cb(file_shrunk_cb_t cb) { file_shrunk_cb = cb; }
    
    const VcdVector *read();
    
private:
    bool read_definitions();
    int id_of(const char *code);
    
    void sync(size_t partial_read_len);
    
    std::string filename;
    std::ifstream input;
    const SigList &siglist;
    file_shrunk_cb_t file_shrunk_cb;
    
    std::unordered_map<std::string, int> definitions;

    struct stat stats;

    bool have_definitions;

    VcdVector vector;
    long int next_time;
    bool have_next_time;
    bool have_first_time;
};

#endif /* defined(__vicviz_fltk__VcdReader__) */
