//
//  SigList.cpp
//  vicviz-fltk
//
//  Created by Hunter, David on 1/11/15.
//
//

#include <algorithm>

#include "SigList.h"

SigList::SigList()
{
}

SigList::SigList(std::initializer_list<std::string> args)
: list(args)
{
}

int SigList::add(const std::string &name)
{
    int ret = list.size();
    list.push_back(name);
    return ret;
}

const std::string &SigList::name_of(int id) const
{
    static const std::string unknown {""};

    if (id >= list.size())
        return unknown;
    return list.at(id);
}

int SigList::id_of(const std::string &name) const
{
    auto i = std::find(list.begin(), list.end(), name);
    if (i == list.end())
        return -1;
    return i - list.begin();
}