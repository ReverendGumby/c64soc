//
//  VcdVector.h
//  vicviz-fltk
//
//  Created by Hunter, David on 1/8/15.
//
//

#ifndef __vicviz_fltk__VcdVector__
#define __vicviz_fltk__VcdVector__

#include <string>
#include <vector>
#include "SigList.h"

struct VcdSig
{
    char value;

    VcdSig() : value('x') {}
    //VcdSig(const std::string &v) : value(v) {}
    VcdSig(char v) : value(v) {}
    
    int value_int() const { return is_0() ? 0 : is_1() ? 1 : -1; }
    bool is_0() const { return value == '0'; }
    bool is_1() const { return value == '1'; }
    bool is_x() const { return !is_0() && !is_1(); }
};

class VcdVector
{
public:
    VcdVector(const SigList &sl);
    VcdVector(const VcdVector &that, const SigList &sl);
    
    long int time;
    std::vector<VcdSig> sigs;
    
    int bits_to_int(int msb_id, int lsb_id) const;
    
    const VcdSig &operator[](int id) const;
    
private:
    VcdSig undef {};
    const SigList &siglist;
};

#endif /* defined(__vicviz_fltk__VcdVector__) */
