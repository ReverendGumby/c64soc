//
//  VcdReader.cpp
//  vicviz-fltk
//
//  Created by Hunter, David on 1/8/15.
//
//

#include <stdexcept>
#include <iostream>

#include <sys/stat.h>

#include "VcdReader.h"

#define npos std::string::npos

VcdReader::VcdReader(const std::string &fn, const SigList &sl)
:filename(fn), input(fn.c_str()), siglist(sl), vector(sl)
{
    have_definitions = false;
    
    have_next_time = false;
    have_first_time = false;
    
    stats.st_size = 0;
    
    if (!input)
        std::cout << "Unable to open " << fn << std::endl;
    input.exceptions(std::ifstream::badbit);
}

std::vector<std::string> split(const std::string &in)
{
    std::vector<std::string> ret;
    
    size_t start = in.find_first_not_of(" ");
    size_t end = 0;
    while ((end = in.find_first_of(" ", start)) != npos)
    {
        ret.push_back(in.substr(start, end - start));
        start = in.find_first_not_of(" ", end);
    }
    if (start != npos)
        ret.push_back(in.substr(start));
    
    return ret;
}

const VcdVector *VcdReader::read()
{
    if (!read_definitions())
        return nullptr;
    
    const std::string val_start {"01xXzZbr"};

    if (have_next_time)
    {
        vector.time = next_time;
        have_next_time = false;
    }
    
    while (!have_next_time)
    {
        //auto pos = input.tellg();
        std::string line;
        std::getline(input, line);
        
        if (!input.good()) {
            input.clear();
            sync(line.size());
            return nullptr;
        }
        
        while (line.back() == '\r')
            line.pop_back();
        
        char start = line[0];
        if (start == '#')
        {
            next_time = std::stol(line.substr(1));
            if (!have_first_time) {
                have_first_time = true;
                vector.time = next_time;
            } else {
                have_next_time = true;
            }
        }
        else if (start != '\0' && val_start.find_first_of(start) != npos)
        {
            if (start == 'b' || start == 'r')
            {
                // No point.. can't consume it, anyway.
                continue;
            }
            char val = start;
            const char *code = &line[1];
            int i = id_of(code);
            if (i != -1)
                vector.sigs[i] = VcdSig{val};
        }
    }
    
    return &vector;
}

bool VcdReader::read_definitions()
{
    while (!have_definitions)
    {
        std::string line;
        std::getline(input, line);
        if (!input.good()) {
            input.clear();
            sync(line.size());
            break;
        }
        while (line.back() == '\r')
            line.pop_back();
        
        if (line.find("$enddefinitions") != npos)
        {
            if (definitions.size() != siglist.size())
                throw std::runtime_error("missing signal in VCD");
            have_definitions = true;
        }
        else if (line.find("$var") != npos)
        {
            // assumes all on one line:
            //   $var reg 1 *@ data $end
            //   $var wire 4 ) addr [3:0] $end
            std::vector<std::string> ls = split(line);
            if (ls.size() >= 6) {
                std::string &code = ls[3];
                std::string name;
                for (auto i = ls.begin() + 4; i < ls.end() - 1; i++)
                    name = name + *i;
                int sig_id = siglist.id_of(name);
                if (sig_id != -1)
                    definitions[code] = sig_id;
            }
        }
    }
    return have_definitions;
}

int VcdReader::id_of(const char *code)
{
    auto i = definitions.find(code);
    if (i == definitions.end())
        return -1;
    return i->second;
}

void VcdReader::sync(size_t partial_read_len)
{
    input.seekg(-partial_read_len, std::ios::cur);

    struct stat new_stats;
    stat(filename.c_str(), &new_stats);
    
    if (new_stats.st_size < stats.st_size) {
        std::cout << "File shrunk!\n";
        file_shrunk_cb();
    }
    
    stats = new_stats;
}
