//
//  VcdVector.cpp
//  vicviz-fltk
//
//  Created by Hunter, David on 1/8/15.
//
//

#include "VcdVector.h"

VcdVector::VcdVector(const SigList &sl)
: siglist(sl)
{
    sigs.assign(siglist.size(), undef);
}

VcdVector::VcdVector(const VcdVector &that, const SigList &sl)
: sigs(that.sigs), siglist(sl)
{
}

int VcdVector::bits_to_int(int msb_id, int lsb_id) const
{
    assert(msb_id < lsb_id);
    assert(msb_id != -1);
    assert(lsb_id != -1);

    int ret = 0;
    
    int b = lsb_id - msb_id;
    for (int i = msb_id; i <= lsb_id; i++, b--)
    {
        int v = sigs[i].value_int();
        if (v == -1)
            return -1;
        ret += v * (1 << b);
    }
    return ret;
}

const VcdSig &VcdVector::operator[](int id) const
{
    if (id == -1 || id >= sigs.size())
        return undef;
    return sigs[id];
}
