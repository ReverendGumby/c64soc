#include <string>
#include <functional>

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/fl_draw.H>

#include "VcdReader.h"
#include "SigList.h"

#include "main.h"

#define SCREEN_WIDTH  525
#define SCREEN_HEIGHT 264

#define WIN_COORD(c) ((c)*2)

struct Pixel
{
    int r, g, b;
    bool hs;
    bool vs;
};

class Input
{
public:
    Input(const std::string &fn);
    void reset();
    void on_input_restart(std::function<void()> cb);
    const Pixel *read();
    
private:
    void open_reader();
    
    std::string fn;
    VcdReader *reader { nullptr };
    SigList siglist;
    Pixel pixel;
    VcdSig pclk;
    std::function<void()> restart_cb;

    struct {
        int pclk;
        int r7, r0;
        int g7, g0;
        int b7, b0;
        int hs, vs;
    } vid;
};

class App;

class Screen : public Fl_Widget
{
public:
    Screen(App *app, int x, int y, int width, int height, const char *label = 0);
    
    virtual void draw();
    virtual int handle(int event);
    
    void pixel(const Pixel &p);
    void update();
    void clear();
    
private:
    void set_img_buf(int x, int y, int r, int g, int b);
    
    int wx() { return WIN_COORD(std::min(px, SCREEN_WIDTH-1)); }
    int wy() { return WIN_COORD(std::min(py, SCREEN_HEIGHT-1)); }
    
    App *app;
    
    int px;
    int py;
    bool hs;
    bool vs;
    
    bool pixel_was_set;
    int damage_y_first;
    int damage_y_last;
    
    uchar img_buf[WIN_COORD(SCREEN_HEIGHT)][WIN_COORD(SCREEN_WIDTH)][3];
};

class App
{
public:
    App(int argc, char **argv);
    
    int run() { return Fl::run(); }
    void handle_key(char key);
    
private:
    void idle();
    void on_input_restart();

    static void idle_cb(void *p) { static_cast<App *>(p)->idle(); }
    
    int fetch_and_render_vectors(int count);
    
    Screen *screen;
    Input *input;
    bool idle_set;
};

Input::Input(const std::string &fn)
: fn(fn),
  siglist {
    "pclk",
    "R[7]", "R[6]", "R[5]", "R[4]", "R[3]", "R[2]", "R[1]", "R[0]",
    "G[7]", "G[6]", "G[5]", "G[4]", "G[3]", "G[2]", "G[1]", "G[0]",
    "B[7]", "B[6]", "B[5]", "B[4]", "B[3]", "B[2]", "B[1]", "B[0]",
    "HS",
    "VS",
  }
{
    reader = nullptr;

    vid.pclk = siglist.id_of("pclk");
    vid.r7 = siglist.id_of("R[7]"), vid.r0 = siglist.id_of("R[0]");
    vid.g7 = siglist.id_of("G[7]"), vid.g0 = siglist.id_of("G[0]");
    vid.b7 = siglist.id_of("B[7]"), vid.b0 = siglist.id_of("B[0]");
    vid.hs = siglist.id_of("HS");
    vid.vs = siglist.id_of("VS");
    
    open_reader();
}

void Input::reset()
{
    open_reader();
}

void Input::on_input_restart(std::function<void()> cb)
{
    restart_cb = cb;
    reader->set_file_shrunk_cb(restart_cb);
}

void Input::open_reader()
{
    delete reader;
    reader = new VcdReader(fn, siglist);
    reader->set_file_shrunk_cb(restart_cb);
}

const Pixel *Input::read()
{
    for (;;) {
        const VcdVector *pv;
        if (!(pv = reader->read())) {
            return nullptr;
        }
        const VcdVector &v = *pv;
        const VcdSig &v_pclk = v[vid.pclk];
        bool pclk_posedge = pclk.is_0() && v_pclk.is_1();
        pclk = v_pclk;

        if (pclk_posedge) {
            pixel.r = v.bits_to_int(vid.r7, vid.r0);
            pixel.g = v.bits_to_int(vid.g7, vid.g0);
            pixel.b = v.bits_to_int(vid.b7, vid.b0);
            if (pixel.r == -1 || pixel.g == -1 || pixel.b == -1)
                pixel.r = 255, pixel.g = 0, pixel.b = 0;
            pixel.hs = v[vid.hs].is_1();
            pixel.vs = v[vid.vs].is_1();
            return &pixel;
        }
    }
}

Screen::Screen(App *in_app, int x, int y, int width, int height, const char *label)
: Fl_Widget(x, y, width, height, label)
{
    app = in_app;

    clear();
}

void Screen::draw()
{
    fl_draw_image(img_buf[0][0], 0, 0, w(), h());

    // Cursor
    fl_color(255, 255, 255);
    fl_rect(wx(), wy(), 2, 2);
}

int Screen::handle(int event)
{
    switch (event)
    {
        case FL_FOCUS:
            return 1;
        case FL_KEYBOARD:
            app->handle_key(Fl::event_key());
            return 1;
        default:
            return Fl_Widget::handle(event);
    }
}

void Screen::pixel(const Pixel &p)
{
    if (!pixel_was_set) {
        pixel_was_set = true;
        // Record y before incrementing, where the cursor is.
        damage_y_first = wy();
    }

    if (p.hs && !hs) {
        px = 0;
        py++;
    }
    hs = p.hs;
    
    if (p.vs && !vs) {
        py = 0;
    }
    vs = p.vs;
    
    int x = wx();
    int y = wy();
    
    set_img_buf(x+0, y+0, p.r, p.g, p.b);
    set_img_buf(x+1, y+0, p.r, p.g, p.b);
    set_img_buf(x+0, y+1, p.r, p.g, p.b);
    set_img_buf(x+1, y+1, p.r, p.g, p.b);
    
    damage_y_last = y;
    
    px++;
}

void Screen::update()
{
    if (pixel_was_set) {
        int y = std::min(damage_y_first, damage_y_last);
        int h = std::max(damage_y_first, damage_y_last) - y + 2;
        damage(1, 0, y, w(), h);
        //damage(1, 0, 0, w(), h());
        pixel_was_set = false;
    }
}

void Screen::clear()
{
    px = 0;
    py = 0;
    hs = true;
    vs = true;
    pixel_was_set = false;
    
    memset(img_buf[0][0], 90, sizeof(img_buf));

    damage(1, 0, 0, w(), h());
}

void Screen::set_img_buf(int x, int y, int r, int g, int b)
{
    img_buf[y][x][0] = r;
    img_buf[y][x][1] = g;
    img_buf[y][x][2] = b;
}

App::App(int argc, char **argv)
{
    int w = WIN_COORD(SCREEN_WIDTH);
    int h = WIN_COORD(SCREEN_HEIGHT);
    Fl_Window *win = new Fl_Window(w, h);
    screen = new Screen(this, 0, 0, w, h);
    win->add(screen);
    Fl::visual(FL_RGB);
    win->show(argc, argv);
    assert(Fl::focus() == screen);
    
    if (argc <= 1) {
        std::cout << "usage: " << argv[0] << " <dump.vcd>" << std::endl;
        exit(1);
    }
    
    input = new Input(argv[1]);
    input->on_input_restart(std::bind(&App::on_input_restart, this));
    
    Fl::add_idle(&App::idle_cb, this);
    idle_set = true;
}

void App::on_input_restart()
{
    screen->clear();
    input->reset();
}

void App::idle()
{
    int goal = SCREEN_WIDTH * 6;
    if (fetch_and_render_vectors(goal) > 0) {
        Fl::remove_idle(&App::idle_cb, this);
        idle_set = false;
        Fl::add_timeout(0.2, &App::idle_cb, this);
    } else if (!idle_set) {
        Fl::add_idle(&App::idle_cb, this);
        idle_set = true;
    }
}

int App::fetch_and_render_vectors(int count)
{
    while (count) {
        const Pixel *pp;
        if (!(pp = input->read()))
            break;
        screen->pixel(*pp);
        count --;
    }
    screen->update();
    return count;
}

void App::handle_key(char key)
{
    switch (key) {
        case 'r':
            screen->clear();
            input->reset();
            break;
        case 'q':
            exit(0);
        default:
            break;
    }
}

int main(int argc, char **argv)
{
    App app(argc, argv);
    return app.run();
}
