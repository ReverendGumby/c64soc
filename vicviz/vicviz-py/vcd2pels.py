import sys
import Verilog_VCD

class Vector:
    def __init__(self):
        self.sig = {}

    def __getattr__(self, name):
        if name in self.sig:
            return self.sig[name]
        if name in ('R', 'G', 'B'):
            v = 0
            for i in range(8):
                b = self.sig['%s[%d]' % (name[0], i)]
                if b not in ('0', '1'):
                    return b
                v = v + int(b) * 2 ** i
            return v
        return None

class Input:
    def __init__(self, fn):
        siglist = [
            'mos6567_tb.pclk',
            'mos6567_tb.R[7]',
            'mos6567_tb.R[6]',
            'mos6567_tb.R[5]',
            'mos6567_tb.R[4]',
            'mos6567_tb.R[3]',
            'mos6567_tb.R[2]',
            'mos6567_tb.R[1]',
            'mos6567_tb.R[0]',
            'mos6567_tb.G[7]',
            'mos6567_tb.G[6]',
            'mos6567_tb.G[5]',
            'mos6567_tb.G[4]',
            'mos6567_tb.G[3]',
            'mos6567_tb.G[2]',
            'mos6567_tb.G[1]',
            'mos6567_tb.G[0]',
            'mos6567_tb.B[7]',
            'mos6567_tb.B[6]',
            'mos6567_tb.B[5]',
            'mos6567_tb.B[4]',
            'mos6567_tb.B[3]',
            'mos6567_tb.B[2]',
            'mos6567_tb.B[1]',
            'mos6567_tb.B[0]',
            'mos6567_tb.HS',
            'mos6567_tb.VS',
            'mos6567_tb.DE',
        ]
        print 'Parsing VCD...',
        sys.stdout.flush()
        self.vcd = Verilog_VCD.parse_vcd(fn, siglist=siglist)
        print 'done'

        self.vector = Vector()
        self.pclk = None

        self.times = set()
        for sig in self.vcd.keys():
            for (t,v) in self.vcd[sig]['tv']:
                self.times.add(t)
        self.times = list(self.times)
        self.times.sort()
        print len(self.times), 'unique timestamps'

    def read_vector(self):
        if not self.times:
            return None
        
        time = self.times.pop(0)

        for sig in self.vcd.keys():
            name = self.vcd[sig]['nets'][0]['name']
            tv = self.vcd[sig]['tv']
            if len(tv):
                (t,v) = tv[0]
                if t == time:
                    self.vector.sig[name] = v
                    tv.pop(0)

        return self.vector
        
    def read(self):
        while True:
            vector = self.read_vector()
            if vector is None:
                return None
            old_pclk = self.pclk
            new_pclk = vector.pclk
            self.pclk = new_pclk
            if old_pclk == '0' and new_pclk == '1':
                r = vector.R
                g = vector.G
                b = vector.B
                if (type(r),type(g),type(b)) == (int,int,int):
                    color = (r,g,b)
                else:
                    color = (0,255,0)
                hs = vector.HS == '1'
                vs = vector.VS == '1'
                de = vector.DE == '1'
                return (color, de, hs, vs)

class Output:
    def __init__(self, fn):
        self.f = open(fn, 'w')
        self.pels = []

    def pixel(self, color, de, hs, vs):
        (r, g, b) = color
        pel = '{0:01b}{1:08b}{2:01b}{3:01b}{4:08b}00{5:08b}00'.format(de, b, hs, vs, g, r)
        print >> self.f, pel

    def close(self):
        self.f.close()

class App:
    def __init__(self, argv):
        if len(argv) <= 2:
            print >> sys.stderr, 'usage: <file.vcd> <file.txt>'
            sys.exit(1)
        self.input = Input(argv[1])
        self.output = Output(argv[2])

    def run(self):
        while True:
            d = self.input.read()
            if d is None:
                break
            (color, de, hs, vs) = d
            self.output.pixel(color, de, hs, vs)
        self.output.close()

app = App(sys.argv)
app.run()
