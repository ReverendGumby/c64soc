from Tkinter import *
import sys
import Verilog_VCD

class Vector:
    def __init__(self):
        self.sig = {}

    def __getattr__(self, name):
        if name in self.sig:
            return self.sig[name]
        if name in ('R', 'G', 'B'):
            v = 0
            for i in range(8):
                b = self.sig['%s[%d]' % (name[0], i)]
                if b not in ('0', '1'):
                    return b
                v = v + int(b) * 2 ** i
            return v
        return None

class Input:
    def __init__(self, fn):
        siglist = [
            'mos6567_tb.pclk',
            'mos6567_tb.R[7]',
            'mos6567_tb.R[6]',
            'mos6567_tb.R[5]',
            'mos6567_tb.R[4]',
            'mos6567_tb.R[3]',
            'mos6567_tb.R[2]',
            'mos6567_tb.R[1]',
            'mos6567_tb.R[0]',
            'mos6567_tb.G[7]',
            'mos6567_tb.G[6]',
            'mos6567_tb.G[5]',
            'mos6567_tb.G[4]',
            'mos6567_tb.G[3]',
            'mos6567_tb.G[2]',
            'mos6567_tb.G[1]',
            'mos6567_tb.G[0]',
            'mos6567_tb.B[7]',
            'mos6567_tb.B[6]',
            'mos6567_tb.B[5]',
            'mos6567_tb.B[4]',
            'mos6567_tb.B[3]',
            'mos6567_tb.B[2]',
            'mos6567_tb.B[1]',
            'mos6567_tb.B[0]',
            'mos6567_tb.HS',
            'mos6567_tb.VS',
        ]
        print 'Parsing VCD...',
        sys.stdout.flush()
        self.vcd = Verilog_VCD.parse_vcd(fn, siglist=siglist)
        print 'done'

        self.vector = Vector()

        self.times = set()
        for sig in self.vcd.keys():
            for (t,v) in self.vcd[sig]['tv']:
                self.times.add(t)
        self.times = list(self.times)
        self.times.sort()
        print len(self.times), 'unique timestamps'

    def read(self):
        if not self.times:
            return None
        
        time = self.times.pop(0)
        #if time % 100000 == 0:
        #    print time

        for sig in self.vcd.keys():
            name = self.vcd[sig]['nets'][0]['name']
            tv = self.vcd[sig]['tv']
            if len(tv):
                (t,v) = tv[0]
                if t == time:
                    self.vector.sig[name] = v
                    tv.pop(0)

        return self.vector
        

class Screen(Canvas):
    def __init__(self, master):
        self.width = 512
        self.height = 264
        (w,h) = self.coords(self.width, self.height)
        Canvas.__init__(self, master, width=w, height=h)

        self.pi = PhotoImage(width=w, height=h)
        self.pi.put("#000000", (w-1,h-1)) # allocate of full image rectangle

        self.create_image((0,0), anchor=NW, image=self.pi)

        self.x = 0
        self.y = 0
        self.hs = True
        self.vs = True

    def coords(self, x, y):
        return (x*2,y*2)

    def pixel(self, color, hs, vs):
        if hs and not self.hs:
            self.x = 0
            self.y = self.y + 1
        self.hs = hs

        if vs and not self.vs:
            self.y = 0
        self.vs = vs

        (x,y) = self.coords(self.x, self.y)
        self.pi.put("#%02x%02x%02x" % color, ['-to', x, y, x+2, y+2])

        self.x = self.x + 1

class App(Frame):
    def __init__(self, master, argv):
        self.master = master
        self.pclk = None

        if len(argv) <= 1:
            print >> sys.stderr, 'usage: <file.vcd>'
            sys.exit(1)
        self.input = Input(argv[1])

        frame = Frame(master)
        frame.pack()

        self.screen = Screen(frame)
        self.screen.pack()

        self.alarm()
        
    def idle(self):
        try:
            self.fetch_and_render_vectors(100)
        except KeyboardInterrupt:
            sys.exit(0)
        self.master.after_idle(self.alarm)

    def alarm(self):
        self.master.after(0, self.idle)

    def fetch_and_render_vectors(self, count):
        while count:
            vector = self.input.read()
            if vector is None:
                break
            if self.pclk == '0' and vector.pclk == '1':
                r = vector.R
                g = vector.G
                b = vector.B
                #print (r,g,b)
                if (type(r),type(g),type(b)) == (int,int,int):
                    color = (r,g,b)
                else:
                    color = (0,255,0)
                hs = vector.HS == '1'
                vs = vector.VS == '1'
                self.screen.pixel(color, hs, vs)
                count = count - 1
            self.pclk = vector.pclk

root = Tk()

app = App(root, sys.argv)
root.mainloop()

