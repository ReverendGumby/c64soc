1. In PlanAhead: Open Implemented Design. Select system block, right-click, Export Hardware for SDK
   [X] Include bitstream
   [X] Export Hardware
   [X] Launch SDK
2. In SDK, create Hello World project based on Standalone BSP
3. Create new Application Project.
   Project name: Zync_FSBL
   Hardware platform: system_hw_platform
   OS Platform: standalone
   [Next>]
   Select template Zync FSBL
4. (if using Ethernet) Overwrite Zync_FSBL/src/fsbl_hooks.c with same in zybo/zybo_base_system/source/ise/SDK/fsbl/fsbl_hooks.c
5. Select hello_world project, then Xilinx Tools > Create Zync Boot Image
   Create new BIF file. BIF file path: hello_world/bootimage/hello_world.bif
   Boot image partitions:
     [bootloader] Zync_FSBL/Debug/Zync_FSBL.elf
	 [datafile] system_hw_platform/system.bit
	 [datafile] hello_world/Debug/hello_world.elf
   Output path: hello_world/bootimage/output.mcs
   [Create Image]
6. Xilinx Tools > Program Flash
   Image File: hello_world/bootimage/output.mcs
   Flash Type: qspi_single
