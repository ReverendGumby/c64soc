import sys
from struct import pack, unpack

bits = int(sys.argv[2])
bcnt = bits / 8

data = []
while True:
    b = sys.stdin.read(1)
    if b == '':
        break
    data = data + [ord(b)]

if bits == 8:
    pck = "<1B"
    upck = "<B"
elif bits == 32:
    pck = "<4B"
    upck = "<I"

for i in range(0, len(data), bcnt):
    v = unpack(upck, pack(pck, *data[i:i+bcnt]))[0]
    print ("{0:x}".format(v))
