from __future__ import print_function
from collections import namedtuple, OrderedDict
import sys
import re
import argparse

class VOut:
    Region = namedtuple('VOut_Region', ['name', 'spat', 'epat', 'regs'])
    Reg = namedtuple('VOut_Reg', ['name', 'type', 'addr', 'addr_scale', 'off',
                                   'comment', 'sub_scale', 'subs'])
    Field = namedtuple('VOut_Field', ['name', 'msb', 'lsb'])
    Value = namedtuple('VOut_Value', ['name', 'value', 'base'])

    default_region = 'ALL'

    def __init__(self):
        self.regions = []
        self.add_region(VOut.default_region, None, None)

    def __str__(self):
        return str(self.regions)

    def add_region(self, name, spat, epat):
        r = VOut.Region(name, spat, epat, [])
        self.regions.append(r)

    def get_region(self, name):
        for region in self.regions:
            if name == region.name:
                return region
        # Hack to match to C64BUS_CTRL
        if name == 'C64BUS_CTRL':
            for region in self.regions:
                if name[-8:] == region.name[-8:]:
                    return region
        raise KeyError(f'{name} region does not exist')

    def get_reg(self, region, name):
        for reg in region.regs:
            if name == reg.name:
                return reg
        raise KeyError(f'{name} register does not exist')

class VParser:
    def __init__(self):
        self.out = VOut()
        self.cur_region = self.out.get_region(VOut.default_region)
        self.cur_reg_list = self.cur_region.regs
        self.cur_reg = None
        self.cur_scale = 0
        self.cur_off = 0
        self.reset()

    def reset(self):
        self.blk = ''
        self.build_blk = False
        self.tag = ''

    def input_file(self, fn):
        with open(fn, 'r') as f:
            self.fn = fn
            self.ln = 1
            for line in f:
                try:
                    self.input_line(line)
                except Exception as e:
                    print(f'{self.fn}:{self.ln}: {e}')
                    raise e
                self.ln += 1
        self.reset()

    def input_line(self, line):
        self.comment = None
        i = line.find('//``')
        if i >= 0:
            args = line[i+4:-1]
            line = line[:i]
            i = args.find('//') # Handle sub-comments
            if i >= 0:
                self.comment = args[i+3:]
                args = args[:i-1]
            args = args.split()
            self.tag = args[0]
            self.args = args[1:]
        else:
            self.args = []
            if self.build_blk:
                self.blk = self.blk + line
        self.line = re.sub(R'//.*', '', line)
        if self.tag:
            attr = 'tag_' + self.tag
            assert(hasattr(self, attr))
            self.tag = getattr(self, attr)()

    def new_reg(self, name, case, sub_scale=0):
        return self._add_reg(name, 'case', case, sub_scale)

    def new_reg_array(self, name, range):
        return self._add_reg(name, 'if-range', range, 0)

    def _add_reg(self, name, type, addr, sub_scale):
        addr_scale = self.cur_scale
        self.cur_reg = self.out.Reg(name, type, addr, addr_scale, self.cur_off,
                                    self.comment, sub_scale, [])
        self.cur_reg_list.append(self.cur_reg)

    def tag_REGION_START(self):
        self.case = ''
        return 'REGION_CONT'

    def tag_REGION_CONT(self):
        l = self.line.strip()
        if self.case:
            l = self.case + ' ' + l
        if l[-1] == ';':
            self.case = ''
            sr, er = None, None
            m = re.search(R"'h([\w_]+).*'h([\w_]+):", l)
            if m:
                sr, er = m.groups()
            else:
                m = re.search(R"'h([\w_]+):", l)
                if m:
                    sr = er = m.group(1)
            m = re.search(R": +.+ = (\w+)", l)
            if m:
                region = self.out.add_region(m.group(1), sr, er)
        else:
            self.case = l
        return self.tag

    def tag_REGION_END(self):
        del self.case
        return ''

    def tag_REGION_REGS(self):
        if self.args:
            name = self.args[0]
        else:
            m = re.search(R" == (\w+)", self.line)
            name = m.group(1)
        self.cur_region = self.out.get_region(name)
        self.cur_reg_list = self.cur_region.regs
        return 'REG_CONT'

    def tag_SUBREGS(self):
        m = re.search(R"\((\w+)(\[.*\])?.*\)", self.line)
        if m:
            name, sel = m.groups()
            if sel:
                m = re.search(R"(\d+):(\d+)", sel)
                if m:
                    self.cur_scale = 1 << int(m.group(2))
            if self.args:
                name = self.args[0] + '_' + name
                if len(self.args) >= 2:
                    m = re.search(R"'h([\w]+)", self.args[1])
                    assert(m)
                    self.cur_off = int(m.group(1), 16)
        else:
            assert(self.args)
            name = self.args[0]
        reg = self.out.get_reg(self.cur_region, name)
        self.cur_reg_list = reg.subs
        return 'REG_CONT'

    def tag_REG(self):
        # An explicit tag was given.
        return self.tag_REG_common(explicit=True)

    def tag_REG_CONT(self):
        return self.tag_REG_common(explicit=False)

    def tag_REG_common(self, explicit):
        self.tag = 'REG_CONT'
        name = None
        case = None
        data = False
        if self.args:
            name = self.args[0]
            if len(self.args) > 1 and self.args[1] == 'DATA':
                data = True
        m = re.search(R"'h([\w_]+)(?:,.+)?: *(.*)", self.line)
        if m:
            case, expr = m.groups()
            if not name:
                m = re.search(R"= (\w+)", expr)
                if m:
                    name = m.group(1)
            if data:
                mm = re.search(R"'([bdh])([\w_]+)", expr)
                base, val = mm.groups()
                val = self.out.Value('DATA', val, base)

        if m or explicit:
            self.new_reg(name, case)
            if data:
                self.cur_reg.subs.append(val)
        elif case:
            self.cur_reg = None
            self.case = case

        if re.search(R'\bbegin\b', self.line):
            return 'FIELD'
        elif re.search(R'\bendcase\b', self.line):
            self.cur_scale = 0
            self.cur_off = 0
            return self.tag_REG_END()
        return self.tag

    def tag_FIELD(self):
        if re.search(R'\bend\b', self.line):
            self.cur_reg = None
            return 'REG_CONT'
        m = re.search(R"\[(?:(\d+):)?(\d+)\] *<?= *([\w`]+)", self.line)
        if m:
            name = self.args[0] if self.args else m.group(3)
            if self.cur_reg is None:
                case = self.case
                sub_scale = 1 << int(m.group(2))
                self.new_reg(name, case, sub_scale)
            else:
                lsb = int(m.group(2))
                msb = int(m.group(1)) if m.group(1) else lsb
                field = self.out.Field(name, msb, lsb)
                self.cur_reg.subs.append(field)
        return self.tag

    def tag_NO_FIELD(self):
        return 'FIELD'

    def tag_REG_END(self):
        return ''

    def tag_REG_ARRAY(self):
        assert(self.args)
        name = self.args[0]
        m = re.search(R"'h([\w]+) && .*'h([\w]+)", self.line)
        assert(m)
        self.new_reg_array(name, m.groups())
        
        return 'REG_CONT'

    def tag_VALS(self):
        m = re.search(R"\((\w+)(\[.*\])?\)", self.line)
        if m:
            name = m.group(1)
            if self.args:
                name = self.args[0] + '_' + name
        else:
            assert(self.args)
            name = self.args[0]
        reg = self.out.get_reg(self.cur_region, name)
        self.cur_reg_list = reg.subs
        return 'VALS_CONT'

    def tag_VAL(self):
        assert(self.args)
        name = self.args[0]
        m = re.search(R"'([bdh])([\w_]+):", self.line)
        base, val = m.groups()
        val = self.out.Value(name, val, base)
        self.cur_reg_list.append(val)
        return 'VALS_CONT'

    def tag_VALS_CONT(self):
        if re.search(R'\bendcase\b', self.line):
            return ''
        return self.tag

def read_v(fns):
    p = VParser()
    for fn in fns:
        p.input_file(fn)
    return p.out

######################################################################

class CVOut(VOut):
    class Region(namedtuple('CVOut_Region', ['name', 'start', 'end', 'regs'])):
        @classmethod
        def _convert(cls, v):
            start, end = None, None
            if v.spat:
                pat = v.spat.replace('_', '')
                start = int(pat.replace('z', '0'), 16)
            if v.epat:
                pat = v.epat.replace('_', '')
                end = int(pat.replace('z', 'F'), 16)
            sub_scale = 4       # ARADDR[xx:2]
            regs = list(CVOut.convert_subs(v.regs, sub_scale, subregs=False))
            return cls(v.name, start, end, regs)

    class Reg(namedtuple('CVOut_Reg', ['name', 'addr', 'len', 'comment', 'subs'])):
        @classmethod
        def _convert(cls, v, scale):
            name = v.name.upper()
            rlen = 0
            scale = v.addr_scale or scale
            if v.type == 'case' and v.addr is not None:
                case = v.addr.replace('_', '')
                case = case.replace('x', '0') # for *_CTRL_ID
                reg_addr = (int(case, 16) + v.off) * scale
            elif v.type == 'if-range':
                start, end = [int(x, 16) for x in v.addr]
                reg_addr = (start + v.off) * scale
                rlen = (end - start + 1) * scale
            else:
                reg_addr = None
            sub_scale = v.sub_scale
            subs = list(CVOut.convert_subs(v.subs, sub_scale, subregs=True))
            return cls(name, reg_addr, rlen, v.comment, subs)

    class SubReg(Reg):
        pass

    class Field(namedtuple('CVOut_Field', VOut.Field._fields)):
        @classmethod
        def _convert(cls, v):
            name = v.name.upper()
            return cls(name, v.msb, v.lsb)

    class Value(namedtuple('CVOut_Value', VOut.Value._fields)):
        @classmethod
        def _convert(cls, v):
            name = v.name.upper()
            value = v.value.replace('_', '')
            base = 0
            if v.base == 'b':
                base = 2
            elif v.base == 'd':
                base = 10
            elif v.base == 'h':
                base = 16
            assert(base)
            value = int(value, base)
            return cls(name, value, base)

    @classmethod
    def convert_subs(cls, vs, scale, subregs):
        for v in vs:
            if isinstance(v, VOut.Reg):
                scls = cls.SubReg if subregs else cls.Reg
                yield scls._convert(v, scale)
            if isinstance(v, VOut.Field):
                yield cls.Field._convert(v)
            if isinstance(v, VOut.Value):
                yield cls.Value._convert(v)

    def __init__(self, v):
        self.regions = list([self.Region._convert(r) for r in v.regions])

def convert_v(v):
    return CVOut(v)

######################################################################

HI = namedtuple('hi', ['fn', 'lines'])

def read_hi(fn):
    lines = []
    with open(fn, 'r') as f:
        for line in f:
            lines.append(line)

    hi = HI(fn, lines)
    return hi

######################################################################

class HGen:
    class Base(namedtuple('HGen_Base', ['macro', 'lvl'])):
        def __add__(self, s):
            return HGen.Base(self.macro + s, self.lvl + 1)

    def __init__(self, f, cv):
        self.f = f
        self.cv = cv
        self.need_gap = False

    def print_hdr(self):
        self.print('// Auto-generated: Do not edit.')
        self.print('// {0} {1}'.format(sys.executable, ' '.join(sys.argv)))
        self.print()

    def input_line(self, line):
        if line[0:4] == '//``':
            cmd = line[4:-1]
            args = cmd.split()
            tag = 'tag_' + args[0]
            self.gapped = False
            getattr(self, tag)(args[1:])
        else:
            self.print(line, end='')

    def print(self, line='', end='\n'):
        if self.need_gap:
            self.print_gap()
        self.f.write(line + end)

    def print_cols(self, cols):
        tw = 4
        out = ''
        col = 0
        for c, s in cols:
            if c > 0 and col >= c:
                out = out + ' '
                col = col + 1
            else:
                while c > col:
                    w = 4 - (col % tw)
                    out = out + ' ' * w
                    col = col + w
            out = out + s
            col = col + len(s)
        self.print(out)

    def print_lvl(self, base, left, right, comment=None):
        right_col = 32 + base.lvl * 4
        comment_col = 48
        cols = [(0, left), (right_col, right)]
        if comment:
            cols = cols + [(comment_col, '// ' + comment)]
        self.print_cols(cols)

    def print_gap(self):
        self.need_gap = False
        if self.gapped:
            self.print()
        else:
            self.gapped = True

    def tag_PERIPHERAL(self, args):
        name = args[0]
        self.peripheral = name
        self.peripheral_base = name + '_BASE'
        base = HGen.Base(macro=name, lvl=0)
        self.print_lvl(base,
                       '#define ' + self.peripheral_base,
                       'XPAR_' + name + '_BASEADDR')

    def tag_BASE(self, args):
        width = args[0]
        if len(args) > 1:
            macro = args[1]
            region_name = args[2] if len(args) > 2 else macro
            macro = self.peripheral + '_' + macro
        else:
            macro = self.peripheral
            region_name = self.cv.default_region
        region = self.cv.get_region(region_name)
        base = HGen.Base(macro=macro, lvl=0)
        base_macro = base.macro + '_BASE'
        reg_macro = base.macro + '_REG'
        if region.start is not None:
            self.print_lvl(base,
                           '#define ' + base_macro,
                           '({1} + 0x{0:05X})'.format(
                               region.start, self.peripheral_base))
        self.print_lvl(base,
                       '#define {0}(off)'.format(reg_macro),
                        '(*((volatile uint{1}_t *)({0} + (off))))'.format(
                            base_macro, width))
        self.reg_macro = reg_macro
        self.region_name = region_name

    @classmethod
    def sr_to_repl(cls, sr):
        m = re.search(R"s/(.*)/(.*)/", sr)
        return re.compile(m.group(1)), m.group(2)

    def print_sub(self, base, sub, repls, recurse=True,
                  regs_only=False, fields_only=False):
        if isinstance(sub, self.cv.Reg):
            self.print_reg(base, sub, repls, recurse, regs_only, fields_only)
        if isinstance(sub, self.cv.Field) and not regs_only:
            self.print_field(base, sub, repls)
        if isinstance(sub, self.cv.Value) and not regs_only and not fields_only:
            self.print_value(base, sub, repls)

    def print_reg(self, base, reg, repls, recurse=True,
                  regs_only=False, fields_only=False):
        base = base + ('_' + reg.name)
        is_subreg = isinstance(reg, self.cv.SubReg)
        if is_subreg:
            faddr = lambda reg: '0x{0:X}'.format(reg.addr)
        else:
            faddr = lambda reg: '{0}(0x{1:03X})'.format(self.reg_macro, reg.addr)

        if recurse:
            self.need_gap = True
        if not fields_only:
            self.print_lvl(base, '#define ' + base.macro, faddr(reg), reg.comment)
            if reg.len:
                ln = base.macro + '_LEN'
                self.print_lvl(base, '#define ' + ln, f'0x{reg.len:X}')
        else:
            if reg.comment:
                self.print('// ' + reg.comment)
        if recurse:
            subs = reg.subs
            if len(subs) and isinstance(subs[0], CVOut.Reg):
                subs = sorted(subs, key=lambda r: r.addr)
            for sub in subs:
                self.print_sub(base, sub, repls, False, regs_only, fields_only)

    def print_field(self, base, field, repls=[]):
        fbase = base + ('_' + field.name)
        field_macro = fbase.macro
        for repl in repls:
            field_macro = repl[0].sub(repl[1], field_macro)
        w = field.msb - field.lsb + 1
        mask = (1 << w) - 1
        postfix = 'U' if field.msb >= 31 else ''
        self.print_lvl(fbase,
                       '#define ' + field_macro,
                       '({0}{1}<<{2})'.format(mask, postfix, field.lsb))
        if field.msb != field.lsb:
            self.print_lvl(fbase,
                           '#define ' + field_macro + '_S',
                           str(field.lsb))

    def print_value(self, base, v, repls=[]):
        vbase = base + ('_' + v.name)
        if v.base == 16:
            vstr = '0x{0:X}UL'.format(v.value)
        elif v.base == 10 or v.base == 2:
            vstr = str(v.value)
        self.print_lvl(vbase,
                       '#define ' + vbase.macro,
                       vstr)

    def tag_REGS(self, args):
        macro_name = self.peripheral
        base = self.reg_base = HGen.Base(macro=macro_name, lvl=0)
        repls = [self.sr_to_repl(sr) for sr in args]
        self.region = self.cv.get_region(self.region_name)
        regs = [reg for reg in self.region.regs if reg.addr is not None]
        for reg in regs:
            self.print_reg(base, reg, repls)

    def tag_SUBREG_FIELDS(self, args):
        reg_name = args[0]
        repls = [self.sr_to_repl(sr) for sr in args[1:]]
        base = HGen.Base(macro=self.reg_base.macro + '_' + reg_name, lvl=0)
        reg = self.cv.get_reg(self.region, reg_name)
        for sub in reg.subs:
            self.print_sub(base, sub, repls, True, fields_only=True)

    def tag_FIELDS(self, args):
        reg_name = args[0]
        base = self.reg_base + ('_' + reg_name)
        reg = self.cv.get_reg(self.region, reg_name)
        for sub in reg.subs:
            self.print_sub(base, sub, [], False, fields_only=True)

def gen_h(fn, cv, hi):
    with open(fn, 'w') as f:
        p = HGen(f, cv)
        p.print_hdr()
        ln = 1
        for line in hi.lines:
            try:
                p.input_line(line)
            except Exception as e:
                print(f'{hi.fn}:{ln}: {e}')
                raise e
            ln += 1

def get_args():
    parser = argparse.ArgumentParser(description='Autogenerate regs .h')
    parser.add_argument('hi_file', metavar='HI_FILE', type=str,
                        help='Path to read template header (c64bus.h.i)')
    parser.add_argument('h_file', metavar='H_FILE', type=str,
                        help='Path to write output header (c64bus.h)')
    parser.add_argument('v_file', metavar='V_FILE', type=str, nargs='+',
                        help='Path(s) to read Verilog input (c64bus.v)')
    return parser.parse_args()

def main():
    args = get_args()
    v = read_v(args.v_file)
    #print(str(v))
    cv = convert_v(v)
    #print(str(cv))
    hi = read_hi(args.hi_file)
    gen_h(args.h_file, cv, hi)

if __name__ == "__main__":
    main()
