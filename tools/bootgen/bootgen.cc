#include <iostream>
#include <fstream>
#include <stdint.h>
#include <string.h>
#include <string>
#include "boothdr.h"
#include "elf.h"

using namespace std;

constexpr int max_num_images = 14;
constexpr int max_num_parts = max_num_images * 3;

uint32_t offset_of(void* p, void* start)
{
    return reinterpret_cast<char*>(p) - reinterpret_cast<char*>(start);
}

struct {
    bootrom_header_t rom;
    image_header_t img[max_num_images];
    partition_header_t part[max_num_parts];

    uint32_t woff_of(void* p) { return offset_of(p, this) / 4; }
} header;

constexpr int iobuflen = 0x1000;
char iobuf[iobuflen];

fstream in, out;

void init_header()
{
    // Empty space should be 0xFF.
    memset(&header, ~0, sizeof(header));

    auto& rom = header.rom;
    for (int i = 0; i < 8; i++)
        rom.interrupt_table[i] = 0xeafffffe;
    rom.width_detection = 0xaa995566;
    rom.image_identification = rom.image_identification_value;
    rom.encryption_status = 0;
    rom.user10 = 0x01010000;
    rom._res14 = 0;
    rom.start_of_execution = 0;
    rom._res17 = 1; // If this is 0, SD won't boot!
    for (int i = 0; i < 19; i++)
        rom.user19[i] = 0;
    for (int i = 0; i < 512; i += 2) {
        rom.register_initialization[i + 0] = ~0;
        rom.register_initialization[i + 1] = 0;
    }

    auto& table = rom.image_header_table;
    table.version = 0x01020000;
    table.partition_header_woff = header.woff_of(&header.part[0]);
    table.first_image_header_woff = header.woff_of(&header.img[0]);
    table.auth_header_woff = 0;
    rom.user19[19] = offset_of(&table, &rom);
    rom.user19[20] = offset_of(&header.part[0], &rom);
}

void pack_img_name(image_header_t& img, const char* name)
{
    auto& oname = img.image_name;
    const int num_words = sizeof(oname) / sizeof(oname[0]);
    int namelen = strlen(name);
    int wi, ci;
    for (wi = 0, ci = 0; wi < num_words - 1 && ci < namelen; wi++) {
        uint32_t w = 0;
        for (ci = wi * 4; ci < wi * 4 + 4; ci++)
            w = (w << 8) | (ci < namelen ? name[ci] : 0);
        oname[wi] = w;
    }
    oname[wi++] = 0;
    for (; wi < num_words; wi++)
        oname[wi] = ~0;
}

void copy_to_out(int len, void (*filter)(int len) = nullptr)
{
    int read;
    for (int remain = len; remain; remain -= read) {
        read = std::min(remain, iobuflen);
        in.read(iobuf, read);
        if (!in)
            throw 1;
        if (filter)
            filter(read);
        out.write(iobuf, read);
    }
}

int copy_bin(partition_header_t& part)
{
    in.seekg(0, fstream::end);
    int in_len = in.tellg();
    in.seekg(0, fstream::beg);
    copy_to_out(in_len);

    part.attributes.destination_device = part.DD_PS;
    return in_len;
}

int copy_elf(partition_header_t& part)
{
    // Verify it's an ELF
    elf_header_t hdr;
    in.read(reinterpret_cast<char*>(&hdr), sizeof(hdr));
    if (hdr.e_ident.magic != hdr.magic_elf ||
        hdr.e_ident.clas != hdr.clas_32) {
        cerr << "ELF magic missing\n";
        throw 1;
    }

    // Find PT_LOAD segment
    elf_pgm_hdr_t pgm;
    int pgm_idx;
    for (pgm_idx = 0; pgm_idx < hdr.e_phnum; pgm_idx++) {
        in.seekg(hdr.e_phoff + pgm_idx * hdr.e_phentsize);
        in.read(reinterpret_cast<char*>(&pgm), sizeof(pgm));
        if (pgm.p_type == pgm.PT_LOAD)
            break;
    }
    if (pgm_idx == hdr.e_phnum) {
        cerr << "PT_LOAD segment missing\n";
        throw 1;
    }

    // Copy it out
    in.seekg(pgm.p_offset);
    int load_len = pgm.p_filesz;
    copy_to_out(load_len);
    
    part.attributes.destination_device = part.DD_PS;
    part.destination_load_addr = pgm.p_vaddr;
    part.destination_exe_addr = hdr.e_entry;
    return load_len;
}

void bit_swap(int len)
{
    // Bitstream is ordered for 8-bit streaming. FSBL does 32-bit little-endian
    // reads, so we must correct order for endianness.
    char *p = iobuf;
    for (; len >= 4; len -= 4, p += 4) {
        char b[4];
        for (int o = 0; o < 4; o++)
            b[o] = p[o];
        for (int o = 0; o < 4; o++)
            p[3 - o] = b[o];
    }
}

int copy_bit(partition_header_t& part)
{
    // Reference:
    // http://www.fpga-faq.com/FAQ_Pages/0026_Tell_me_about_bit_files.htm
    auto read_len = [](int width) {
        int ret = 0;
        for (int i = 0; i < width; i++)
            ret = (ret << 8) | in.get();
        return ret;
    };

    in.ignore(read_len(2)); // some sort of header
    in.ignore(read_len(2)); // "a"
    in.ignore(read_len(2)); // file name
    in.ignore(1);           // "b"
    in.ignore(read_len(2)); // part name
    in.ignore(1);           // "c"
    in.ignore(read_len(2)); // date
    in.ignore(1);           // "d"
    in.ignore(read_len(2)); // time
    in.ignore(1);           // "e"
    auto bs_len = read_len(4);
    copy_to_out(bs_len, bit_swap);

    part.attributes.destination_device = part.DD_PL;
    return bs_len;
}

uint32_t checksum(void* buf, int len)
{
    uint32_t sum = 0;
    uint32_t* beg = reinterpret_cast<uint32_t*>(buf);
    uint32_t* end = beg + len;
    for (uint32_t* p = beg; p < end; p++)
        sum += *p;
    return sum;
}

void copy_image(image_header_t& img, partition_header_t& part, const char* in_name)
{
    in.open(in_name, fstream::in | fstream::binary);
    if (!in) {
        cerr << "Unable to open " << in_name << "\n";
        throw 1;
    }

    const char* filename = strrchr(in_name, '/');
    if (filename == NULL)
        filename = in_name;
    else
        filename ++;
    const char* ext = filename;
    auto filename_len = strlen(filename);
    if (filename_len > 4) // ".ext"
        ext = ext + filename_len - 4;

    int (*copy)(partition_header_t& part);
    if (!strcmp(ext, ".elf"))
        copy = copy_elf;
    else if (!strcmp(ext, ".bit"))
        copy = copy_bit;
    else
        copy = copy_bin;

    auto out_start_pos = out.tellp();
    auto in_len = copy(part);
    in.close();

    auto tail = in_len % 64;
    if (tail) {
        auto pad = 64 - tail;
        memset(iobuf, ~0, pad);
        out.write(iobuf, pad);
    }

    pack_img_name(img, filename);

    auto in_wlen = (in_len + 3) / 4;
    part.partition_data_wlen = part.extracted_data_wlen = part.total_wlen = in_wlen;
    part.data_woff = out_start_pos / 4;
    part.attributes.partition_owner = part.PO_FSBL;
    part.section_count = 1;

    part.checksum = ~checksum(&part, (sizeof(part) - 4) / 4);
}

void bootgen(const char* out_name, int num_images, char** img_names)
{
    out.open(out_name, fstream::out | fstream::trunc | fstream::binary);
    if (!out) {
        cerr << "Unable to open " << out_name << "\n";
        throw 1;
    }

    out.write(reinterpret_cast<char*>(&header), sizeof(header)); // temporary placeholder

    init_header();
    auto& rom = header.rom;
    rom.image_header_table.image_header_count = num_images;

    for (int i = 0; i < num_images; i++) {
        const char* in_name = img_names[i];
        image_header_t& img = header.img[i];
        partition_header_t& part = header.part[i];
        img = image_header_t{};
        part = partition_header_t{};
        if (i < num_images - 1)
            img.next_woff = header.woff_of(&img + 1);
        img.first_partition_header_woff = header.woff_of(&part);
        img.partition_count = 1;
        part.image_header_woff = header.woff_of(&img);
        copy_image(img, part, in_name);
        if (i == 0) {
            rom.source_offset = part.data_woff * 4;
            rom.total_image_length = rom.image_length = part.partition_data_wlen * 4;
        }
    }
    {
        partition_header_t part = partition_header_t{};
        part.checksum = ~0;
        header.part[num_images] = part;
    }

    auto* rom_csum_start = &rom.width_detection;
    rom.checksum = ~checksum(rom_csum_start, offset_of(&rom.checksum, rom_csum_start) / 4);

    out.seekp(0, fstream::beg);
    out.write(reinterpret_cast<char*>(&header), sizeof(header));
    out.seekp(0, fstream::end);

    out.close();
}

int main(int argc, char *argv[])
{
    if (argc <= 2) {
        cerr << "Usage: " << argv[0] << " boot.bin [.elf | .bit ...]\n";
        return 1;
    }

    try {
        int num_images = argc - 2;
        const char* out_name = argv[1];
        bootgen(out_name, num_images, &argv[2]);
    }
    catch (int err) {
        return err;
    }
    return 0;
}
