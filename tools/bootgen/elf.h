#include <stdint.h>

// References:
// - binutils include/elf/external.h
// - https://refspecs.linuxfoundation.org/elf/elf.pdf

// ELF file header
struct elf_header_t
{
    union {
        struct {
            uint32_t magic;
            uint8_t clas;
        };
        uint8_t raw[16];
    } e_ident;
    uint16_t e_type;
    uint16_t e_machine;
    uint32_t e_version;
    uint32_t e_entry;
    uint32_t e_phoff;
    uint32_t e_shoff;
    uint32_t e_flags;
    uint16_t e_ehsize;
    uint16_t e_phentsize;
    uint16_t e_phnum;

    static constexpr uint32_t magic_elf = 0x464c457f; // '.ELF'
    static constexpr uint8_t clas_32 = 0x01; // 32-bit object
};

// Program header table entry
struct elf_pgm_hdr_t
{
    uint32_t p_type;                            // PT_*
    uint32_t p_offset;                          // segment file offset
    uint32_t p_vaddr;
    uint32_t p_paddr;
    uint32_t p_filesz;                          // segment size in file
    uint32_t p_memsz;
    uint32_t p_flags;

    static constexpr uint32_t PT_LOAD = 0x00000001; // Loadable program segment
};
