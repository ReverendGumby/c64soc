function [3:0] AxADDR_region(input [31:0] AxADDR);
  begin
    casez (AxADDR[18:0])
//``REGION_START
      19'h0_0zzz: AxADDR_region = EMUBUS_CTRL;
//``REGION_END
    endcase
  end
endfunction

always @* begin
  if (rregion == EMUBUS_CTRL) begin //``REGION_REGS
    casex (ARADDR[11:2])
      10'h22: begin
        rdata[10:0] = PPU_MON_SEL;
      end
      10'h22: begin
        rdata[8:3] = QPU_MON_SEL;
      end
    endcase
  end
end

always @(posedge CLK) begin
  if (MON_READY & mon_ready_d) begin // need 2xCLK to read OAM
    //``REGION_REGS EMUBUS_CTRL
    //``SUBREGS PPU_MON_SEL
    if (MON_SEL >= 9'h88 && MON_SEL <= 9'h10f) begin //``REG_ARRAY OAM1
    end
  end
end

always @(posedge CLK) begin
  if (MON_SEL[10:5] == (6'h01 + (BGIDX - 1)) && MON_READY) begin
    //``REGION_REGS EMUBUS_CTRL
    case (MON_SEL[4:1])         //``SUBREGS PPU 9'h40
      3'h04: begin              //``REG BGNSC_NBA
      end
      3'h04: MON_DOUT[9:0] <= hofs;  //``REG BGNHOFS
    endcase
  end
end

always @(posedge CLK) begin
  if (MON_SEL[8:4] == 5'h01 && MON_READY) begin
    //``REGION_REGS EMUBUS_CTRL
    case (MON_SEL[8:3])         //``SUBREGS QPU
      8'h11: begin              //``REG ROWCOL
      end
      8'h11: begin              //``REG FRAME
      end
    endcase
  end

  if (MON_READY) begin
    //``SUBREGS QPU_MON_SEL
    if (MON_SEL >= 8'h11 && MON_SEL <= 8'h21) begin //``REG_ARRAY PAL
    end
  end
end
