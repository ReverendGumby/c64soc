// Auto-generated: Do not edit.
// /usr/local/opt/python@3.11/bin/python3.11 gen_regs_h.py test-regs.h.i test-regs.h test-regs.v

// All addresses should be 0x88.

#define EMUBUS_BASE             XPAR_EMUBUS_BASEADDR

#define EMUBUS_CTRL_BASE        (EMUBUS_BASE + 0x00000)
#define EMUBUS_CTRL_REG(off)    (*((volatile uint32_t *)(EMUBUS_CTRL_BASE + (off))))

#define EMUBUS_PPU_MON_SEL          EMUBUS_CTRL_REG(0x088)
#define EMUBUS_PPU_MON_SEL_OAM1         0x88
#define EMUBUS_PPU_MON_SEL_OAM1_LEN     0x88
#define EMUBUS_PPU_MON_SEL_BGNSC_NBA    0x88
#define EMUBUS_PPU_MON_SEL_BGNHOFS      0x88

#define EMUBUS_QPU_MON_SEL          EMUBUS_CTRL_REG(0x088)
#define EMUBUS_QPU_MON_SEL_ROWCOL       0x88
#define EMUBUS_QPU_MON_SEL_FRAME        0x88
#define EMUBUS_QPU_MON_SEL_PAL          0x88
#define EMUBUS_QPU_MON_SEL_PAL_LEN      0x88

// Local Variables:
// compile-command: "python3 gen_regs_h.py test-regs.h.i test-regs.h test-regs.v"
// End:
