# This is meant to be used as follows:
# - Move board boot selection jumper to JTAG
# - Insert SD card with BOOT.bin
# - Start openocd
# - Start gdb -nx --command=.../load-fsbl.gdb

source ~/.gdbinit

# Load the FSBL into OCR (SRAM)
file obj/fsbl.elf
target extended-remote :3333
set pagination off
load
set pagination on
# Break just before the FSBL jumps into the app
b call_entry_point
# Start the FSBL, letting it load the app from SD into DDR
set $pc = 0
c

# Now it's safe to use app.elf, set breakpoints, and start debugging.

source .gdbinit
