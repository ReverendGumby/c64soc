proc nes_load_cart {fname addr} {
    set fin [open $fname]
    while { [gets $fin line] >= 0 } {
        puts [format %x $addr]
        mww $addr [scan $line %x]
        set addr [expr $addr + 4]
    }
    close $fin
}
