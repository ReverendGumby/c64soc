# This is meant to be used as follows:
# - Move board boot selection jumper to JTAG
# - Start openocd --file zybo.cfg --command zybo_load_init
# - Start gdb -nx --command=../dbg/load-app.gdb obj/.../foo.elf

source ~/.gdbinit

# Load the app into DDR
target extended-remote :3333
set pagination off
load
#set pagination on
# PC has been set to the app's entry point.

source .gdbinit
