module rp2a03_clkgen
  (
   input      CLK_RESET,
   input      CLK,
   input      HOLD,

   output reg CPU_CP1,
   output     CPU_CP1_POSEDGE,
   output     CPU_CP1_NEGEDGE,
   output reg CPU_CP2,
   output     CPU_CP2_POSEDGE,
   output     CPU_CP2_NEGEDGE,

   output     CPU_CP1_POSEDGE_GATED,
   output     CPU_CP1_NEGEDGE_GATED,
   output     CPU_CP2_GATED,
   output     CPU_CP2_NEGEDGE_GATED,
   output     HOLD_ACTIVE,

   output reg APU_CP1,
   output     APU_CP1_POSEDGE,
   output     APU_CP1_NEGEDGE,
   output reg APU_CP2,
   output     APU_CP2_POSEDGE,
   output     APU_CP2_NEGEDGE,

   output reg M2,
   output     M2_POSEDGE,
   output     M2_NEGEDGE
   );

//////////////////////////////////////////////////////////////////////
// CPU clock: two phases, CLK / 12

reg [3:0] cpu_clk_cnt;
reg       reset;

localparam [3:0] cpu_clk_cnt_init = 4'd0;

initial begin
  cpu_clk_cnt = cpu_clk_cnt_init;
  reset = 1'b1;
  CPU_CP1 = 1'b0;
  CPU_CP2 = 1'b0;
end

always @(negedge CLK)
  reset <= CLK_RESET;

//                                                            1 1
//                           cpu_clk_cnt: 0 1 2 3 4 5 6 7 8 9 0 1 0
// mainclock                        CLK:  \/\/\/\/\/\/\/\/\/\/\/\/\
//                                          _________
// CPU clock phase 1 = CLK / 12     CP1:  _/         \_____________
//                                                      _________
// CPU clock phase 2 = CLK / 12     CP2:  _____________/         \_

// cpu_clk_cnt moves on CLK falling edge, and all *_posedge/*_negedge
// are sampled on CLK rising edge.

always @(negedge CLK)
  if (reset)
    cpu_clk_cnt <= cpu_clk_cnt_init;
  else begin
    if (cpu_clk_cnt == 4'd11)
      cpu_clk_cnt <= 4'd0;
    else
      cpu_clk_cnt <= cpu_clk_cnt + 4'b0001;
  end

assign CPU_CP1_POSEDGE = ~reset & cpu_clk_cnt == 4'd0;
assign CPU_CP1_NEGEDGE = ~reset & cpu_clk_cnt == 4'd5;

assign CPU_CP2_POSEDGE = ~reset & cpu_clk_cnt == 4'd6;
assign CPU_CP2_NEGEDGE = ~reset & cpu_clk_cnt == 4'd11;

// The derived clocks, however, move on CLK rising edge.
always @(posedge CLK) begin
  if (reset) begin
    CPU_CP1 <= 1'b0;
    CPU_CP2 <= 1'b0;
  end
  else begin
    if (CPU_CP1_POSEDGE)
      CPU_CP1 <= 1'b1;
    else if (CPU_CP1_NEGEDGE)
      CPU_CP1 <= 1'b0;

    if (CPU_CP2_POSEDGE)
      CPU_CP2 <= 1'b1;
    else if (CPU_CP2_NEGEDGE)
      CPU_CP2 <= 1'b0;
  end
end

//////////////////////////////////////////////////////////////////////
// CPU clock hold
//
// HOLD transitions on CPU_CP2_NEGEDGE and applies to the following
// CP1_POSEDGE.

reg hold_dly;

initial
  hold_dly = 1'b0;

always @(posedge CLK) if (CPU_CP1_POSEDGE)
  hold_dly <= HOLD;

assign HOLD_ACTIVE = hold_dly;

// Gate clocks and clock enables by HOLD.
assign CPU_CP1_POSEDGE_GATED = CPU_CP1_POSEDGE && !HOLD;
assign CPU_CP1_NEGEDGE_GATED = CPU_CP1_NEGEDGE && !hold_dly;
assign CPU_CP2_GATED = CPU_CP2 && !hold_dly;
assign CPU_CP2_NEGEDGE_GATED = CPU_CP2_NEGEDGE && !hold_dly;

//////////////////////////////////////////////////////////////////////
// APU clock: two phases, CLK / 24

// APU clocks pulse once every other CPU CP1.

// CPU_CP1   CPU_CP2   APU_CP1    APU_CP2
// 1         0         1          0
// 0         1         0          0
// 1         0         0          1
// 0         1         0          0
// 1         0         1          0
// 0         1         0          0
// 1         0         0          1
// 0         1         0          0

reg apu_clk_cnt;

localparam apu_clk_cnt_init = 1'b0;

initial begin
  apu_clk_cnt = apu_clk_cnt_init;
  APU_CP1 = 1'b0;
  APU_CP2 = 1'b0;
end

always @(negedge CLK)
  if (reset)
    apu_clk_cnt <= apu_clk_cnt_init;
  else begin
    if (CPU_CP1_NEGEDGE_GATED)
      apu_clk_cnt <= ~apu_clk_cnt;
  end

assign APU_CP1_POSEDGE = CPU_CP1_POSEDGE_GATED & apu_clk_cnt;
assign APU_CP1_NEGEDGE = CPU_CP1_NEGEDGE_GATED & apu_clk_cnt;

assign APU_CP2_POSEDGE = CPU_CP1_POSEDGE_GATED & ~apu_clk_cnt;
assign APU_CP2_NEGEDGE = CPU_CP1_NEGEDGE_GATED & ~apu_clk_cnt;

always @(posedge CLK) begin
  if (reset) begin
    APU_CP1 <= 1'b0;
    APU_CP2 <= 1'b0;
  end
  else begin
    if (APU_CP1_POSEDGE)
      APU_CP1 <= 1'b1;
    else if (APU_CP1_NEGEDGE)
      APU_CP1 <= 1'b0;

    if (APU_CP2_POSEDGE)
      APU_CP2 <= 1'b1;
    else if (APU_CP2_NEGEDGE)
      APU_CP2 <= 1'b0;
  end
end

//////////////////////////////////////////////////////////////////////
// External M2 clock: CLK / 12 with duty cycle 7/12 = 58.33%
// (actual 2A03 has DC = 15/24; we're rounding down.)

initial
  M2 = 1'b0;

//                                       1 1
//      cpu_clk_cnt: 0 1 2 3 4 5 6 7 8 9 0 1 0
// mainclock   CLK:  \/\/\/\/\/\/\/\/\/\/\/\/\
//                             _____________
//              M2:  _________/             \_

assign M2_POSEDGE = cpu_clk_cnt == 4'd4;
assign M2_NEGEDGE = cpu_clk_cnt == 4'd11;

always @(posedge CLK) begin
  if (reset)
    M2 <= 1'b0;
  else begin
    if (M2_POSEDGE)
      M2 <= 1'b1;
    else if (M2_NEGEDGE)
      M2 <= 1'b0;
  end
end

endmodule
