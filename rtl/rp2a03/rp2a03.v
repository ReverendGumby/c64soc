module rp2a03
  (
   input            nRES,        // reset
   input            CLK_RESET,
   input            CLK,         // master clock input: 236250/11 KHz
   output           CP1,         // CPU clock phase 1
   output           CP1_POSEDGE,
   output           CP1_NEGEDGE,
   output           CP2,         // CPU clock phase 2
   output           CP2_POSEDGE,
   output           CP2_NEGEDGE,
   output           APU_CP1_POSEDGE,
   output           M2,          // External clock phase 2 (aka PHI2)
   output           M2_POSEDGE,
   output           M2_NEGEDGE,

   // (APU only) debug monitor
   input [4:0]      APU_MON_SEL,
   input            APU_MON_READY,
   output [7:0]     APU_MON_DOUT,
   output           APU_MON_VALID,

   output [31:0]    ICD_CPU_CYC,

   input            nIRQ,        // interrupt request
   input            nNMI,        // non-maskable interrupt

   output [15:0]    A_O,         // address bus, output
   output           A_OE,        //   output enable
   input [7:0]      D_I,         // data bus, input
   output [7:0]     D_O,         //   output data
   output           D_OE,        //   output enable
   output           RW,          // read / not write

   output [1:0]     INP,         // ctrl read strobes ($4016-7)
   output [2:0]     OUT,         // ctrl write strobes ($4016[0-2])

   input            RDY,         // ready
   output           SYNC,        // instruction sync
   input            HOLD,        // halt and release A,D,RW
   input [4:0]      CPU_REG_SEL, // register monitor
   output [7:0]     CPU_REG_DOUT,

   input            MCLK,        // CODEC master clock
   input            PDCLKEN,     // parallel data clock enable
   output [15:0]    PDOUT        // parallel data out
   );

wire [15:0] a_o_cpu, a_o_oamdma, a_o_dmcdma;
wire [7:0]  db_o_cpu, db_o_apu, db_o_oamdma;

reg [15:0] a;
reg [7:0]  db;

// Internal data / address bus
always @* begin
  db = 8'hxx;
  case ({db_oe_cpu, db_oe_apu, db_oe_oamdma})
    3'b100: db = db_o_cpu;
    3'b010: db = db_o_apu;
    3'b001: db = db_o_oamdma;
    default: ;
  endcase
end

always @* begin
  // dma overrides cpu.
  a = 16'hxxxx;
  casez ({a_oe_cpu, a_oe_oamdma, a_oe_dmcdma})
    3'b100: a = a_o_cpu;
    3'bz10: a = a_o_oamdma;
    3'bzz1: a = a_o_dmcdma;
    default: ;
  endcase
end

// Register select logic
wire [4:0] ra = a_o_cpu[4:0];
wire       rs = (a_o_cpu & 16'hFFE0) == 16'h4000;   // A is $4000 - $401F

// CPU A bus transitions one CLK after RW. When RW goes 0, A is stale
// for a single CLK. A memory that commits a write for that cycle
// would erroneously corrupt that stale address. To avoid this, mask
// RW=0 from cp2_negedge to cp1_posedge (+ 1 clk).
reg rw_mask, rw_mask_d1;
initial begin
  rw_mask = 1'b1;
  rw_mask_d1 = 1'b1;
end

always @(posedge CLK) begin
  if (cpu_cp2_negedge)
    rw_mask <= 1'b1;
  else if (cpu_cp1_posedge)
    rw_mask <= 1'b0;

  rw_mask_d1 <= rw_mask;
end

rp2a03_clkgen clkgen
  (
   .CLK(CLK),
   .CLK_RESET(CLK_RESET),
   .HOLD(HOLD),

   .CPU_CP1(cpu_cp1),
   .CPU_CP1_POSEDGE(cpu_cp1_posedge),
   .CPU_CP1_NEGEDGE(cpu_cp1_negedge),
   .CPU_CP2(cpu_cp2),
   .CPU_CP2_POSEDGE(cpu_cp2_posedge),
   .CPU_CP2_NEGEDGE(cpu_cp2_negedge),

   .CPU_CP1_POSEDGE_GATED(cpu_cp1_posedge_gated),
   .CPU_CP1_NEGEDGE_GATED(),
   .CPU_CP2_GATED(cpu_cp2_gated),
   .CPU_CP2_NEGEDGE_GATED(cpu_cp2_negedge_gated),
   .HOLD_ACTIVE(hold_active),

   .APU_CP1(apu_cp1),
   .APU_CP1_POSEDGE(apu_cp1_posedge),
   .APU_CP1_NEGEDGE(apu_cp1_negedge),
   .APU_CP2(apu_cp2),
   .APU_CP2_POSEDGE(apu_cp2_posedge),
   .APU_CP2_NEGEDGE(apu_cp2_negedge),

   .M2(M2),
   .M2_POSEDGE(M2_POSEDGE),
   .M2_NEGEDGE(M2_NEGEDGE)
   );

assign CP1 = cpu_cp1;
assign CP1_POSEDGE = cpu_cp1_posedge;
assign CP1_NEGEDGE = cpu_cp1_negedge;
assign CP2 = cpu_cp2;
assign CP2_POSEDGE = cpu_cp2_posedge;
assign CP2_NEGEDGE = cpu_cp2_negedge;
assign APU_CP1_POSEDGE = apu_cp1_posedge;

assign rdy = RDY && rdy_oamdma && rdy_dmcdma;

rp2a03_mos6502 mos6502
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP1_POSEDGE(CP1_POSEDGE),
   .CP1_NEGEDGE(CP1_NEGEDGE),
   .CP2_POSEDGE(CP2_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .ICD_CYC(ICD_CPU_CYC),
   .nIRQ(nIRQ),
   .nNMI(nNMI),
   .AEC(1'b1),
   .A_O(a_o_cpu),
   .A_OE(a_oe_cpu),
   .DB_I(D_I),
   .DB_O(db_o_cpu),
   .DB_OE(db_oe_cpu),
   .RW(rw_cpu),
   .RDY(rdy),
   .SYNC(SYNC),
   .SO(1'b0),
   .HOLD(HOLD),
   .CPU_REG_SEL(CPU_REG_SEL),
   .CPU_REG_DOUT(CPU_REG_DOUT)
   );

rp2a03_ctrl ctrl
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP2(cpu_cp2_gated),
   .CP2_NEGEDGE(cpu_cp2_negedge_gated),

   .RS(rs),
   .RA(ra),
   .DB_I(D_I),
   .RW(rw_cpu),

   .INP(INP),
   .OUT(OUT)
   );

rp2a03_oamdma oamdma
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP1_POSEDGE(cpu_cp1_posedge_gated),
   .CP2(cpu_cp2_gated),
   .CP2_NEGEDGE(cpu_cp2_negedge_gated),
   .APU_CP1_POSEDGE(apu_cp1_posedge),
   .APU_CP2_POSEDGE(apu_cp2_posedge),
   .HOLD_ACTIVE(hold_active),

   .RS(rs),
   .RA(ra),
   .RW_I(rw_cpu),

   .ACTIVE_I(dmcdma_active),
   .ACTIVE_O(oamdma_active),

   .A_O(a_o_oamdma),
   .A_OE(a_oe_oamdma),
   .DB_I(D_I),
   .DB_O(db_o_oamdma),
   .DB_OE(db_oe_oamdma),
   .RW_O(rw_oamdma),
   .RDY(rdy_oamdma)
   );

rp2a03_dmcdma dmcdma
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP1_POSEDGE(cpu_cp1_posedge_gated),
   .CP2_NEGEDGE(cpu_cp2_negedge_gated),
   .APU_CP1_POSEDGE(apu_cp1_posedge),
   .APU_CP2_POSEDGE(apu_cp2_posedge),
   .HOLD_ACTIVE(hold_active),

   .RS(rs),
   .RA(ra),
   .RW_I(rw_cpu),

   .RESTART(dmcdma_restart),
   .TRIGGER(dmcdma_trigger),
   .ACTIVE_I(oamdma_active),
   .ACTIVE_O(dmcdma_active),
   .READ(dmcdma_read),

   .A_O(a_o_dmcdma),
   .A_OE(a_oe_dmcdma),
   .DB_I(D_I),
   .RDY(rdy_dmcdma)
   );

wire [15:0] mixout;

rp2a03_apu apu
  (
   .nRES(nRES),
   .CLK(CLK),
   .CPU_CP1_POSEDGE(cpu_cp1_posedge_gated),
   .CPU_CP2(cpu_cp2_gated),
   .CPU_CP2_NEGEDGE(cpu_cp2_negedge_gated),
   .APU_CP1_POSEDGE(apu_cp1_posedge),
   .APU_CP1_NEGEDGE(apu_cp1_negedge),
   .APU_CP2_POSEDGE(apu_cp2_posedge),
   .APU_CP2_NEGEDGE(apu_cp2_negedge),
   .HOLD_ACTIVE(hold_active),

   .MON_SEL(APU_MON_SEL),
   .MON_READY(APU_MON_READY),
   .MON_DOUT(APU_MON_DOUT),
   .MON_VALID(APU_MON_VALID),

   .RS(rs),
   .RA(ra),
   .DB_I(D_I),
   .DB_O(db_o_apu),
   .DB_OE(db_oe_apu),
   .RW(rw_cpu),

   .DMCDMA_RESTART(dmcdma_restart),
   .DMCDMA_TRIGGER(dmcdma_trigger),
   .DMCDMA_READ(dmcdma_read),

   .MCLK(MCLK),
   .PDCLKEN(PDCLKEN),
   .PDOUT(PDOUT)
   );

assign A_O = a;
assign A_OE = a_oe_cpu | a_oe_oamdma | a_oe_dmcdma;
assign D_O = db;
assign D_OE = db_oe_apu | db_oe_cpu | db_oe_oamdma;
assign RW = (rw_cpu && rw_oamdma) || rw_mask_d1;

endmodule
