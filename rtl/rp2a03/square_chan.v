// References:
// - https://wiki.nesdev.com/w/index.php/APU

module rp2a03_square_chan
  (
   input        nRES,
   input        CLK,
   input        CPU_CP2_NEGEDGE,
   input        APU_CP1_POSEDGE,
   input        FRAME_CNT_E,
   input        FRAME_CNT_L,

   input        RSW_VOL,
   input        RSW_SWEEP,
   input        RSW_LO,
   input        RSW_HI,
   input        RSW_SND_CHN,
   input [7:0]  DB_I,

   input        INST,
   input        SND_CHN_EN,
   output       LEN,

   output [3:0] OUT
   );

reg [3:0]  vol;
reg [1:0]  dc;
reg        len_cnt_halt, const_vol, len_en;

// Registers modified only by CPU

always @(posedge CLK) begin
  if (!nRES) begin
    dc <= 2'b00;
    len_cnt_halt <= 1'b0;
    const_vol <= 1'b0;
    vol <= 4'd0;
    len_en <= 1'b0;
  end
  else begin
    if (CPU_CP2_NEGEDGE) begin
      if (RSW_VOL) begin
        dc <= DB_I[7:6];
        len_cnt_halt <= DB_I[5];
        const_vol <= DB_I[4];
        vol <= DB_I[3:0];
      end
      if (RSW_SND_CHN) begin
        len_en <= SND_CHN_EN;
      end
    end
  end
end

wire wrote_hi = CPU_CP2_NEGEDGE && RSW_HI;

// Envelope

reg [3:0] env_div_cnt, env_decay;
reg       env_start;

always @(posedge CLK) begin
  if (!nRES) begin
    env_start <= 1'b0;
    env_decay <= 4'd15;
    env_div_cnt <= 4'd0;
  end
  else begin
    if (wrote_hi)
      env_start <= 1'b1;
    else if (APU_CP1_POSEDGE && FRAME_CNT_E) begin
      if (env_start) begin
        env_start <= 1'b0;
        env_decay <= 4'd15;
        env_div_cnt <= vol;
      end
      else begin
        if (env_div_cnt)
          env_div_cnt[3:0] <= env_div_cnt - 1'd1;
        else begin
          env_div_cnt <= vol;
          if (env_decay)
            env_decay[3:0] <= env_decay - 1'd1;
          else if (len_cnt_halt)
            env_decay <= 4'd15;
        end
      end
    end    
  end
end

wire [3:0] env_out = const_vol ? vol : env_decay;

// Frequency sweep unit

reg [10:0] cur_freq;
reg [10:0] chg_freq;
reg [2:0]  sweep_len, sweep_shift, sweep_cnt;
reg        sweep_en, sweep_neg, sweep_reload;

wire [10:0] tgt_freq;
wire        tgt_freq_c;
wire        chg_pos_ovf;
wire        sweep_gated;

always @* begin
  chg_freq = cur_freq >> sweep_shift;
  if (sweep_neg)
    chg_freq[10:0] = -chg_freq - 1'b1 + INST;
end

assign {tgt_freq_c, tgt_freq} = cur_freq + chg_freq;
assign chg_pos_ovf = tgt_freq_c && !sweep_neg;

always @(posedge CLK) begin
  if (!nRES) begin
    sweep_en <= 1'b0;
    sweep_len <= 3'd0;
    sweep_neg <= 1'b0;
    sweep_shift <= 3'd0;
    sweep_reload <= 1'b0;
    sweep_cnt <= 3'd0;
    cur_freq <= 11'd0;
  end
  else begin
    if (CPU_CP2_NEGEDGE) begin
      if (RSW_LO)
        cur_freq[7:0] <= DB_I;
      if (RSW_HI)
        cur_freq[10:8] <= DB_I[2:0];
      if (RSW_SWEEP) begin
        sweep_en <= DB_I[7];
        sweep_len <= DB_I[6:4];
        sweep_neg <= DB_I[3];
        sweep_shift <= DB_I[2:0];
        sweep_reload <= 1'b1;
      end
    end
    else if (APU_CP1_POSEDGE && FRAME_CNT_L) begin
      if (!sweep_cnt && sweep_en && !sweep_gated && sweep_shift)
        cur_freq <= tgt_freq;
      if (!sweep_cnt || sweep_reload) begin
        sweep_cnt <= sweep_len;
        sweep_reload <= 1'b0;
      end
      else
        sweep_cnt[2:0] <= sweep_cnt - 1'd1;
    end
  end
end

assign sweep_gated = chg_pos_ovf | cur_freq < 11'd8;;

// Frequency counter

reg [10:0] freq_cnt;

always @(posedge CLK) begin
  if (!nRES) begin
    freq_cnt <= 11'd0;
  end
  else begin
    if (APU_CP1_POSEDGE) begin
      if (freq_cnt)
        freq_cnt[10:0] <= freq_cnt - 1'd1;
      else
        freq_cnt <= cur_freq;
    end
  end
end

wire freq_wrap = !freq_cnt;

// Sequencer

reg [2:0] seq_cnt;

always @(posedge CLK) begin
  if (!nRES || wrote_hi)
    seq_cnt <= 3'd0;
  else if (APU_CP1_POSEDGE) begin
    if (freq_wrap)
      seq_cnt[2:0] <= seq_cnt - 1'd1;
  end
end

reg [7:0] seq_lut [0:3];
initial begin
  seq_lut[0] = 8'b00000001;
  seq_lut[1] = 8'b00000011;
  seq_lut[2] = 8'b00001111;
  seq_lut[3] = 8'b11111100;
end

wire seq_gated = ~seq_lut[dc][seq_cnt];

// Length counter

reg [8:0] len_cnt;

`include "len_lut.vh"

wire [4:0] len_load = DB_I[7:3];
wire       len_gated;

always @(posedge CLK) begin
  if (!nRES)
    len_cnt <= -9'sd1;
  else if (wrote_hi)
    len_cnt <= len_lut[len_load];
  else if (APU_CP1_POSEDGE) begin
    if (!len_en)
      len_cnt <= -9'sd1;
    else if (FRAME_CNT_L && !len_cnt_halt)
      if (!len_gated)
        len_cnt <= len_cnt[7:0] - 1'd1;
  end
end

// Channel is silenced when length counter decrements from zero.
assign len_gated = len_cnt[8];

assign LEN = ~len_gated;

// Final channel output is envelope after all gates.
assign OUT = (sweep_gated | seq_gated | len_gated) ? 4'd0 : env_out;

endmodule
