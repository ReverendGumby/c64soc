//////////////////////////////////////////////////////////////////////
// Decimator: downsample by 14x
//
// Designed and realized using MATLAB Filter Designer. Parameters:
//   Structure: cascaded integrator-comb decimator
//   Rate change: 1/14
//   Sections: 2
//
// The filter is followed by a constant gain stage, reducing the
// output's amplitude to match the input's.

module rp2a03_filter_dec14
  (
   input             nRES,
   input             CLK,
   input             ICLKEN,
   input             OCLKEN,

   input [15:0]      IN, // UQ0.16
   output reg [15:0] OUT // UQ0.16
   );

reg [23:0] ins, sz1, sz2, ds1, oz1, oz2, outs; // UQ8.16

localparam OCLK_N = 3;
reg [OCLK_N-2:0] oclken_dly;
reg [OCLK_N-1:0] oclk;

always @(posedge CLK)
  oclken_dly[OCLK_N-2:0] <= { oclken_dly[OCLK_N-3:0], OCLKEN };

always @*
  oclk = { oclken_dly[OCLK_N-2:0], OCLKEN };

always @(posedge CLK) begin
  if (!nRES) begin
    ins <= 0;
    sz1 <= 0;
    sz2 <= 0;
  end
  else if (ICLKEN) begin
    ins <= IN;
    sz1 <= sz1 + ins;
    sz2 <= sz2 + sz1;
  end
end

always @(posedge CLK) begin
  if (!nRES) begin
    ds1 <= 0;
    oz1 <= 0;
  end
  else if (oclk[0]) begin
    ds1 <= sz2 - oz1;
    oz1 <= sz2;
  end
end

always @(posedge CLK) begin
  if (!nRES) begin
    outs <= 0;
    oz2 <= 0;
  end 
  else if (oclk[1]) begin
    outs <= ds1 - oz2;
    oz2 <= ds1;
  end
end

always @(posedge CLK) begin
  if (!nRES) begin
    OUT <= 0;
  end
  else if (oclk[2]) begin
    // Uncorrected decimator gain is 196 (4 * 49).
    // 1 / 49 ~= 167 * 2^-13
    OUT <= ((outs >> 2) * 29'd167) >> 13;
  end
end

endmodule
