// References:
// - https://wiki.nesdev.com/w/index.php/APU

module rp2a03_noise_chan
  (
   input        nRES,
   input        CLK,
   input        CPU_CP2_NEGEDGE,
   input        APU_CP1_POSEDGE,
   input        FRAME_CNT_E,
   input        FRAME_CNT_L,

   input        RSW_VOL,
   input        RSW_MP,
   input        RSW_LEN,
   input        RSW_SND_CHN,
   input [7:0]  DB_I,

   input        SND_CHN_EN,
   output       LEN,

   output [3:0] OUT
   );

reg [3:0]  vol, period;
reg        len_cnt_halt, const_vol, mode, len_en;

// Registers modified only by CPU

always @(posedge CLK) begin
  if (!nRES) begin
    len_cnt_halt <= 1'b0;
    const_vol <= 1'b0;
    vol <= 4'd0;
    mode <= 1'b0;
    period <= 4'd0;
    len_en <= 1'b0;
  end
  else begin
    if (CPU_CP2_NEGEDGE) begin
      if (RSW_VOL) begin
        len_cnt_halt <= DB_I[5];
        const_vol <= DB_I[4];
        vol <= DB_I[3:0];
      end
      if (RSW_MP) begin
        mode <= DB_I[7];
        period <= DB_I[3:0];
      end
      if (RSW_SND_CHN) begin
        len_en <= SND_CHN_EN;
      end
    end
  end
end

wire wrote_hi = CPU_CP2_NEGEDGE && RSW_LEN;

// Envelope

reg [3:0] env_div_cnt, env_decay;
reg       env_start;

always @(posedge CLK) begin
  if (!nRES) begin
    env_start <= 1'b0;
    env_decay <= 4'd15;
    env_div_cnt <= 4'd0;
  end
  else begin
    if (wrote_hi)
      env_start <= 1'b1;
    else if (APU_CP1_POSEDGE && FRAME_CNT_E) begin
      if (env_start) begin
        env_start <= 1'b0;
        env_decay <= 4'd15;
        env_div_cnt <= vol;
      end
      else begin
        if (env_div_cnt)
          env_div_cnt[3:0] <= env_div_cnt - 1'd1;
        else begin
          env_div_cnt <= vol;
          if (env_decay)
            env_decay[3:0] <= env_decay - 1'd1;
          else if (len_cnt_halt)
            env_decay <= 4'd15;
        end
      end
    end    
  end
end

wire [3:0] env_out = const_vol ? vol : env_decay;

// Frequency counter

reg [11:0] freq_lut [0:15];
reg [11:0] freq_cnt;

initial begin
  freq_lut[4'h0] = 12'd4;
  freq_lut[4'h1] = 12'd8;
  freq_lut[4'h2] = 12'd16;
  freq_lut[4'h3] = 12'd32;
  freq_lut[4'h4] = 12'd64;
  freq_lut[4'h5] = 12'd96;
  freq_lut[4'h6] = 12'd128;
  freq_lut[4'h7] = 12'd160;
  freq_lut[4'h8] = 12'd202;
  freq_lut[4'h9] = 12'd254;
  freq_lut[4'ha] = 12'd380;
  freq_lut[4'hb] = 12'd508;
  freq_lut[4'hc] = 12'd762;
  freq_lut[4'hd] = 12'd1016;
  freq_lut[4'he] = 12'd2034;
  freq_lut[4'hf] = 12'd4068;
end

always @(posedge CLK) begin
  if (!nRES) begin
    freq_cnt <= 12'd0;
  end
  else begin
    if (APU_CP1_POSEDGE) begin
      if (freq_cnt)
        freq_cnt[11:0] <= freq_cnt - 1'd1;
      else
        freq_cnt <= freq_lut[period];
    end
  end
end

wire freq_wrap = !freq_cnt;

// LFSR

reg [14:0] sr;

wire       sr_fb = mode ? sr[6] : sr[1];

always @(posedge CLK) begin
  if (!nRES)
    sr <= 15'h7fff;
  else if (APU_CP1_POSEDGE) begin
    if (freq_wrap)
      sr <= {sr[0] ^ sr_fb, sr[14:1]};
  end
end

wire sr_gated = sr[0];

// Length counter

reg [8:0] len_cnt;

`include "len_lut.vh"

wire [4:0] len_load = DB_I[7:3];
wire       len_gated;

always @(posedge CLK) begin
  if (!nRES)
    len_cnt <= -9'sd1;
  else if (wrote_hi)
    len_cnt <= len_lut[len_load];
  else if (APU_CP1_POSEDGE) begin
    if (!len_en)
      len_cnt <= -9'sd1;
    else if (FRAME_CNT_L && !len_cnt_halt)
      if (!len_gated)
        len_cnt <= len_cnt[7:0] - 1'd1;
  end
end

// Channel is silenced when length counter decrements from zero.
assign len_gated = len_cnt[8];

assign LEN = ~len_gated;

// Final channel output is envelope after all gates.
assign OUT = (sr_gated | len_gated) ? 4'd0 : env_out;

endmodule
