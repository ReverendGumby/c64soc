module rp2a03_ctrl
  (
   input            nRES,
   input            CLK,
   input            CP2,
   input            CP2_NEGEDGE,
  
   input            RS,
   input [4:0]      RA,
   input [7:0]      DB_I,
   input            RW,

   output reg [1:0] INP, // ctrl read strobes ($4016-7)
   output reg [2:0] OUT  // ctrl outputs ($4016[0-2])
   );

always @* begin
  INP = 2'b11;
  if (RS && RW && CP2) begin
    case (RA)
      5'h16: INP[0] = !CP2;     // $4016 JOY1
      5'h17: INP[1] = !CP2;     // $4017 JOY2
      default: ;
    endcase
  end
end

always @(posedge CLK) begin
  if (!nRES) begin
    OUT <= 3'b000;
  end
  else if (CP2_NEGEDGE) begin
    if (RS && !RW) begin
      case (RA)
        5'h16: OUT <= DB_I[2:0];  // $4016 JOY1
        5'h17: ;                  // $4017 JOY2
        default: ;
      endcase
    end
  end
end

endmodule
