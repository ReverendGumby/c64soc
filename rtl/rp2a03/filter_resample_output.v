//////////////////////////////////////////////////////////////////////
// Fractional-rate sample rate converter
//
// Designed and realized using MATLAB Filter Designer. Parameters:
//   Structure: direct-form FIR polyphase sample-rate converter
//   Order: 191
//   Rate change: 3/8

module rp2a03_filter_resample_output
  (
   input                    CLK,
   input                    ICLKEN,
   input                    OCLKEN,

   input signed [15:0]      IN, // Q0.16
   output reg signed [15:0] OUT // Q0.16
   );

reg signed [15:0] in_hold;      // input buffer, Q0.16
reg               in_hold_full;
reg signed [15:0] zram [0:63];  // delay line, Q0.16
reg signed [32:0] mac1, mac2, mac3, outs; // multiply-and-accumulate, Q1.32
reg [47:0]        pk_lut [0:63];// coefficient lookup table
reg [6:0]         maccnt;       // MAC counter
reg [47:0]        pk;           // current coefficient 3-tuple
reg signed [15:0] p1, p2, p3;   // current coefficients, Q0.16
reg signed [15:0] z;            // current sample from zram, Q0.16
reg [1:0]         macsel;       // MAC selector
reg [5:0]         zidx_in;      // index to where to put newest sample in zram
reg [5:0]         zidx_out;     // MAC zram fetch index

wire mac_start, mac_fetching_samples;

integer i;
initial begin
  in_hold = 0;
  in_hold_full = 1'b0;
  outs = 33'd0;
  maccnt = MACCNT_N;
  pk = 48'd0;
  for (i=0; i<64; i=i+1)
    zram[i] = 16'd0;
  z = 16'd0;
  macsel = 2'd1;                // somehow, this yields correct output
  zidx_in = 6'd0;
  zidx_out = 0;
end

// The coefficient lookup table contains 3-tuples:
//   pk[x][15:00] = p1[n]
//   pk[x][31:16] = p2[n]
//   pk[x][47:32] = p3[n]
initial begin
  pk_lut[0]  = 48'hfffeffff0000;
  pk_lut[1]  = 48'hfffafffbfffc;
  pk_lut[2]  = 48'h0000fffdfffb;
  pk_lut[3]  = 48'h000f000a0005;
  pk_lut[4]  = 48'h001200140013;
  pk_lut[5]  = 48'hfff20000000b;
  pk_lut[6]  = 48'hffceffd6ffe4;
  pk_lut[7]  = 48'hffe6ffd4ffcc;
  pk_lut[8]  = 48'h004100200000;
  pk_lut[9] =  48'h006f006e005c;
  pk_lut[10] = 48'h00000036005c;
  pk_lut[11] = 48'hff4bff80ffc0;
  pk_lut[12] = 48'hff52ff2dff2c;
  pk_lut[13] = 48'h00750000ff9b;
  pk_lut[14] = 48'h0179014500e8;
  pk_lut[15] = 48'h00b001300174;
  pk_lut[16] = 48'hfe76ff380000;
  pk_lut[17] = 48'hfd94fd89fddc;
  pk_lut[18] = 48'h0000fedefe08;
  pk_lut[19] = 48'h037802810147;
  pk_lut[20] = 48'h032a03e603fb;
  pk_lut[21] = 48'hfdf6000001d1;
  pk_lut[22] = 48'hf9a7fa78fc02;
  pk_lut[23] = 48'hfd18faf2f9c7;
  pk_lut[24] = 48'h067703490000;
  pk_lut[25] = 48'h0a480a680901;
  pk_lut[26] = 48'h000004e7086f;
  pk_lut[27] = 48'hefc3f496fa4d;
  pk_lut[28] = 48'hef64ec70ecc4;
  pk_lut[29] = 48'h0d1b0000f5f0;
  pk_lut[30] = 48'h3cba2cb91c66;
  pk_lut[31] = 48'h5d84564b4b04;
  pk_lut[32] = 48'h564b5d846000;
  pk_lut[33] = 48'h2cb93cba4b04;
  pk_lut[34] = 48'h00000d1b1c66;
  pk_lut[35] = 48'hec70ef64f5f0;
  pk_lut[36] = 48'hf496efc3ecc4;
  pk_lut[37] = 48'h04e70000fa4d;
  pk_lut[38] = 48'h0a680a48086f;
  pk_lut[39] = 48'h034906770901;
  pk_lut[40] = 48'hfaf2fd180000;
  pk_lut[41] = 48'hfa78f9a7f9c7;
  pk_lut[42] = 48'h0000fdf6fc02;
  pk_lut[43] = 48'h03e6032a01d1;
  pk_lut[44] = 48'h0281037803fb;
  pk_lut[45] = 48'hfede00000147;
  pk_lut[46] = 48'hfd89fd94fe08;
  pk_lut[47] = 48'hff38fe76fddc;
  pk_lut[48] = 48'h013000b00000;
  pk_lut[49] = 48'h014501790174;
  pk_lut[50] = 48'h0000007500e8;
  pk_lut[51] = 48'hff2dff52ff9b;
  pk_lut[52] = 48'hff80ff4bff2c;
  pk_lut[53] = 48'h00360000ffc0;
  pk_lut[54] = 48'h006e006f005c;
  pk_lut[55] = 48'h00200041005c;
  pk_lut[56] = 48'hffd4ffe60000;
  pk_lut[57] = 48'hffd6ffceffcc;
  pk_lut[58] = 48'h0000fff2ffe4;
  pk_lut[59] = 48'h00140012000b;
  pk_lut[60] = 48'h000a000f0013;
  pk_lut[61] = 48'hfffd00000005;
  pk_lut[62] = 48'hfffbfffafffb;
  pk_lut[63] = 48'hfffffffefffc;
end

// Input buffer - incoming sample arrives on ICLKEN, departs when
// delay line can safely accept a new sample.
always @(posedge CLK) begin
  if (ICLKEN) begin
    in_hold <= IN;
    in_hold_full <= 1'b1;
  end
  else if (!mac_fetching_samples)
    in_hold_full <= 1'b0;
end

// Delay line - circular buffer of incoming samples
always @(posedge CLK) begin
  if (in_hold_full && !mac_fetching_samples) begin
    zidx_in <= zidx_in + 1'd1;
    zram[zidx_in] <= in_hold;
  end
end

// Convolution engine 
// Every OCLKEN, runs three parallel 64-sample MACs
localparam [6:0] MACCNT_LAST_K = 7'd64;
localparam [6:0] MACCNT_SEL = MACCNT_LAST_K + 2;
localparam [6:0] MACCNT_N = MACCNT_SEL + 1;

assign mac_start = OCLKEN;

always @(posedge CLK) begin
  if (mac_start)
    maccnt <= 0;
  else if (maccnt < MACCNT_N)
    maccnt <= maccnt + 1'd1;
end

assign mac_fetching_samples = maccnt < MACCNT_LAST_K;

// Fetch coefficient from lookup table
always @(posedge CLK)
  if (maccnt < MACCNT_LAST_K)
    pk <= pk_lut[maccnt];

always @* begin
  p1 = pk[15:0];
  p2 = pk[31:16];
  p3 = pk[47:32];
end

// Fetch sample from delay line
always @(posedge CLK) begin
  if (mac_start)
    zidx_out <= zidx_in - 1'd1;
  else if (maccnt < MACCNT_LAST_K)
    zidx_out <= zidx_out - 1'd1;
end

always @(posedge CLK) begin
  if (maccnt < MACCNT_LAST_K)
    z <= zram[zidx_out];
end

// Triple convolute
always @(posedge CLK) begin
  if (maccnt == MACCNT_N) begin
    mac1 <= 0;
    mac2 <= 0;
    mac3 <= 0;
  end
  else if (maccnt < MACCNT_LAST_K + 1) begin
    mac1 <= mac1 + z * p1;
    mac2 <= mac2 + z * p2;
    mac3 <= mac3 + z * p3;
  end
end

// Select one of the MACs as output in round-robin fashion
always @(posedge CLK) begin
  if (maccnt == MACCNT_SEL) begin
    case (macsel)
      2'd0: begin
        outs <= mac1;
        macsel <= 2'd2;
      end
      2'd1: begin
        outs <= mac2;
        macsel <= 2'd0;
      end
      2'd2: begin
        outs <= mac3;
        macsel <= 2'd1;
      end
      default: outs <= 33'dx;
    endcase
  end
end

always @*
  OUT = outs >> 16; // scale Q1.32 down to Q0.16

endmodule
