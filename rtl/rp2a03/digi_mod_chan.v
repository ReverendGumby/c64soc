// References:
// - https://wiki.nesdev.com/w/index.php/APU

module rp2a03_digi_mod_chan
  (
   input        nRES,
   input        CLK,
   input        CPU_CP1_POSEDGE,
   input        CPU_CP2_NEGEDGE,
   input        APU_CP1_POSEDGE,
   input        APU_CP2_POSEDGE,

   input        RSW_FREQ,
   input        RSW_RAW,
   input        RSW_LEN,
   input        RSW_SND_CHN,
   input [7:0]  DB_I,

   input        SND_CHN_EN,
   output       LEN,

   output       DMA_RESTART,
   output       DMA_TRIGGER,
   input        DMA_READ,

   output [6:0] OUT
   );

reg [3:0]  period;
reg [7:0]  len_buf;
reg        irq_en, loop;
reg        len_en;
reg [2:0]  len_en_d;
reg        output_cycle;
reg        freq_wrap;

wire       len_start;
wire       len_restart;
wire       len_cnt_next_zero;

// Registers modified only by CPU

always @(posedge CLK) begin
  if (!nRES) begin
    irq_en <= 1'b0;
    loop <= 1'b0;
    period <= 4'd0;
    len_buf <= 8'h00;
  end
  else begin
    if (CPU_CP2_NEGEDGE) begin
      if (RSW_FREQ) begin
        irq_en <= DB_I[7];
        loop <= DB_I[6];
        period <= DB_I[3:0];
      end
      if (RSW_LEN)
        len_buf <= DB_I[7:0];
      end
  end
end

// Sample buffer

reg [7:0] sbuf;
reg       sbuf_loaded, sbuf_empty;
reg       dma_req, dma_req_d;

assign DMA_TRIGGER = dma_req & ~dma_req_d;

wire sbuf_empty_next = (sbuf_empty | output_cycle) & ~sbuf_loaded;

always @(posedge CLK) begin
  if (!nRES) begin
    sbuf <= 8'h00;
    sbuf_loaded <= 1'b0;
    sbuf_empty <= 1'b1;
    dma_req_d <= 1'b0;
  end
  else begin
    if (CPU_CP1_POSEDGE)
      dma_req_d <= dma_req;
    if (CPU_CP2_NEGEDGE && DMA_READ) begin
      sbuf <= DB_I;
      sbuf_loaded <= 1'b1;
    end

    if (APU_CP1_POSEDGE)
      sbuf_loaded <= 1'b0;
    if (APU_CP2_POSEDGE)
      sbuf_empty <= sbuf_empty_next;
  end
end

always @*
  dma_req = len_en_d[2] & sbuf_empty;

// Output unit

reg [7:0] sr;
reg [6:0] lvl_out;
reg [2:0] bits_remain;
reg       silence;

always @(posedge CLK) begin
  if (!nRES) begin
    lvl_out <= 7'd0;
    silence <= 1'b0;
    bits_remain <= 3'd7;
  end
  else begin
    if (CPU_CP2_NEGEDGE) begin
      if (RSW_RAW)
        lvl_out <= DB_I[6:0];
    end

    if (APU_CP2_POSEDGE & freq_wrap) begin
      if (!silence) begin
        if (sr[0] && lvl_out <= 7'd125)
          lvl_out <= lvl_out[6:0] + 2'd2;
        else if (!sr[0] && lvl_out >= 7'd2)
          lvl_out <= lvl_out[6:0] - 2'd2;
      end

      sr <= {1'b0, sr[7:1]};

      if (output_cycle) begin
        bits_remain <= 3'd7;
        silence <= sbuf_empty;
        if (!sbuf_empty)
          sr <= sbuf;
      end
      else
        bits_remain <= bits_remain[2:0] - 1'd1;
    end
  end
end

always @*
  output_cycle = freq_wrap & ~|bits_remain;

// Frequency counter

reg [7:0] freq_lut [0:15];
reg [9:0] freq_cnt;

initial begin
  freq_lut[4'h0] = 9'd213;
  freq_lut[4'h1] = 9'd189;
  freq_lut[4'h2] = 9'd169;
  freq_lut[4'h3] = 9'd159;
  freq_lut[4'h4] = 9'd142;
  freq_lut[4'h5] = 9'd126;
  freq_lut[4'h6] = 9'd112;
  freq_lut[4'h7] = 9'd106;
  freq_lut[4'h8] = 9'd84;
  freq_lut[4'h9] = 9'd79;
  freq_lut[4'ha] = 9'd70;
  freq_lut[4'hb] = 9'd63;
  freq_lut[4'hc] = 9'd52;
  freq_lut[4'hd] = 9'd41;
  freq_lut[4'he] = 9'd35;
  freq_lut[4'hf] = 9'd26;
end

always @(posedge CLK) begin
  if (!nRES) begin
    freq_cnt <= 10'd510; // first wrap on (CPU) cycle 1022 after reset
  end
  else begin
    if (APU_CP2_POSEDGE) begin
      if (!freq_cnt)
        freq_cnt <= freq_lut[period];
      else if (freq_cnt)
        freq_cnt[9:0] <= freq_cnt - 1'd1;
    end
  end
end

always @*
  freq_wrap = !freq_cnt;

// Length counter

reg [11:0] len_cnt, len_cnt_next;
reg        dma_restart;

assign len_start = len_en_d[1] & ~len_en_d[2];
assign len_restart = len_en_d[2] && !len_en && loop;

always @(posedge CLK) begin
  if (!nRES) begin
    len_en <= 1'b0;
  end
  else begin
    if (CPU_CP2_NEGEDGE) begin
      if (RSW_SND_CHN)
        len_en <= SND_CHN_EN;
      else if (len_cnt_next_zero & ~loop)
        len_en <= 1'b0;
    end
  end
end

always @* begin
  len_cnt_next = len_cnt;
  if (len_start || len_restart)
    len_cnt_next = {len_buf[7:0], 4'b0001};
  else if (len_en && sbuf_loaded)
    len_cnt_next = len_cnt[11:0] - 1'd1;
  else if (!len_en)
    len_cnt_next = 12'd0;
end

assign len_cnt_next_zero = |len_cnt & ~|len_cnt_next;

always @(posedge CLK) begin
  if (!nRES) begin
    len_cnt <= 12'd0;
    dma_restart <= 1'b0;
    len_en_d <= 3'b0;
  end
  else begin
    if (APU_CP1_POSEDGE) begin
      dma_restart <= 1'b0;
      len_cnt <= len_cnt_next;
      if (len_start || len_restart) begin
        dma_restart <= 1'b1;
      end
      len_en_d[0] <= len_en;
      len_en_d[2] <= len_en_d[1];
    end
    if (APU_CP2_POSEDGE)
      len_en_d[1] <= len_en_d[0];
  end
end

assign LEN = len_en;
assign DMA_RESTART = dma_restart;

// Final channel output is the output level.
assign OUT = lvl_out;

endmodule
