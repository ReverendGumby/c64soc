// Two-stage decimator
// Decimate 1.792 MHz by 14 down to 128 kHz, then by 8/3 down to 48 kHz

module rp2a03_filter
  (
   input         nRES,
   input         CLK,
   input         MCLK,
   input         SCLKEN,
   input         OCLKEN,

   input [15:0]  IN,
   output [15:0] OUT
   )
  ;

//////////////////////////////////////////////////////////////////////
// Fast sample clock enable generator
// fclk = MCLK * 7 / 96 = 1.792 MHz

reg [7:0] fclken_cnt;
initial
  fclken_cnt = 0;

wire [7:0] fclken_cnt_next = fclken_cnt + 3'd7;
wire fclken_cnt_wrap = fclken_cnt_next >= 8'd96;

always @(posedge MCLK) begin
  if (fclken_cnt_wrap)
    fclken_cnt <= fclken_cnt_next - 8'd96;
  else
    fclken_cnt <= fclken_cnt_next;
end

wire fclken = fclken_cnt_wrap;

//////////////////////////////////////////////////////////////////////
// Migrate input samples from CLK to MCLK domains

reg [15:0] decin;

wire [15:0] fifo_rdata;
wire        fifo_rempty;

initial
  decin <= 0;

fifo1 #(16, 2) infifo
  (
   .WRST_N(nRES),
   .WCLK(CLK),
   .WINC(SCLKEN),
   .WFULL(),
   .WDATA(IN),

   .RRST_N(nRES),
   .RCLK(MCLK),
   .RINC(fclken),
   .REMPTY(fifo_rempty),
   .RDATA(fifo_rdata)
   );

always @(posedge MCLK) if (fclken && !fifo_rempty)
  decin <= fifo_rdata;

//////////////////////////////////////////////////////////////////////
// Intermediate sample clock enable generator
// iclk = fclk / 14 = 128 KHz

reg iclken;
reg [3:0] iclken_cnt;
initial
  iclken_cnt = 0;

wire iclken_cnt_wrap = iclken_cnt == (4'd14 - 1'd1);

always @(posedge MCLK) if (fclken) begin
  if (iclken_cnt_wrap)
    iclken_cnt <= 0;
  else
    iclken_cnt <= iclken_cnt + 1'b1;
end

always @(posedge MCLK)
  iclken <= fclken && iclken_cnt_wrap;

//////////////////////////////////////////////////////////////////////
// Decimator: Drop filter output from fast rate (fclken) to
// intermediate sample rate (iclken)

wire [15:0] decout;

rp2a03_filter_dec14 dec
  (
   .nRES(nRES),
   .CLK(MCLK),
   .ICLKEN(fclken),
   .OCLKEN(iclken),
   .IN(decin),
   .OUT(decout)
   );

//////////////////////////////////////////////////////////////////////
// Final sample rate converter: from iclken to OCLKEN

// Convert unsigned to signed
wire signed [15:0] resin = $signed(decout);

rp2a03_filter_resample_output resout
  (
   .CLK(MCLK),
   .ICLKEN(iclken),
   .OCLKEN(OCLKEN),
   .IN(resin),
   .OUT(OUT)
   );

endmodule
