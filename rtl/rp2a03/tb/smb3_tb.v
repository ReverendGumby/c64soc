`timescale 1ns / 1ps

module smb3_tb();

initial begin
  $dumpfile("smb3_tb.vcd");
  $dumpvars;
end

reg [7:0]    d;
reg [4:0]    ra;
reg [8:0]    ps_cnt;
reg          res, clk;
reg          mclk;
reg          rw, rs;

wire [7:0]   d_o;
wire [15:0]  pdout;
wire         pdclken;

initial begin
  clk = 1;
  res = 1;
  mclk = 1;
  ps_cnt = 9'h0;
end

always #0.5 begin :fclkgen // 21.477272 MHz
  clk = !clk;
end

always #0.44744 begin :mclkgen // 24 MHz
  mclk = !mclk;
end

// Parallel sample output (MCLK / 512)
always @(posedge mclk) begin
  ps_cnt[8:0] <= ps_cnt + 1'b1;
end

assign pdclken = &ps_cnt;

//////////////////////////////////////////////////////////////////////

rp2a03_clkgen clkgen
  (
   .CLK_RESET(1'b0),
   .CLK(clk),
   .HOLD(1'b0),

   .CPU_CP1_POSEDGE(cpu_cp1_posedge),
   .CPU_CP2(cpu_cp2),
   .CPU_CP2_POSEDGE(cpu_cp2_posedge),
   .CPU_CP2_NEGEDGE(cpu_cp2_negedge),

   .APU_CP1(apu_cp1),
   .APU_CP1_POSEDGE(apu_cp1_posedge),
   .APU_CP1_NEGEDGE(apu_cp1_negedge),
   .APU_CP2(apu_cp2),
   .APU_CP2_POSEDGE(apu_cp2_posedge),
   .APU_CP2_NEGEDGE(apu_cp2_negedge)
   );

rp2a03_apu apu
  (
   .nRES(!res),
   .CLK(clk),
   .CPU_CP1_POSEDGE(cpu_cp1_posedge),
   .CPU_CP2(cpu_cp2),
   .CPU_CP2_NEGEDGE(cpu_cp2_negedge),
   .APU_CP1_POSEDGE(apu_cp1_posedge),
   .APU_CP1_NEGEDGE(apu_cp1_negedge),
   .APU_CP2_POSEDGE(apu_cp2_posedge),
   .APU_CP2_NEGEDGE(apu_cp2_negedge),
   .HOLD_ACTIVE(1'b0),

   .RS(rs),
   .RA(ra),
   .DB_I(d),
   .DB_O(d_o),
   .RW(rw),

   .MCLK(mclk),
   .PDCLKEN(pdclken),
   .PDOUT(pdout)
   );

//////////////////////////////////////////////////////////////////////

`include "../apu_reg_idx.vh"

task set_reg(input [4:0] r, input [7:0] v); 
  begin
    rw <= 1'b0;
    ra <= r;
    d <= v;
    while (!cpu_cp2_posedge)
      @(posedge clk) ;
    rs <= 1'b1;
    while (!cpu_cp2_negedge)
      @(posedge clk) ;
    rs <= 1'b0;
    @(posedge cpu_cp2_negedge) ;
    @(posedge cpu_cp2_negedge) ;
  end
endtask

task get_reg(input [4:0] r, input [7:0] v, input [7:0] m = 8'hff);
  begin
    rw <= 1'b1;
    ra <= r;
    while (!cpu_cp2_posedge)
      @(posedge clk) ;
    rs <= 1'b1;
    while (!cpu_cp2_negedge)
      @(posedge clk) ;
    assert ((d_o & m) == v);
    @(posedge cpu_cp2_negedge) ;
    @(posedge cpu_cp2_negedge) ;
  end
endtask

//////////////////////////////////////////////////////////////////////

localparam frame_tick = 89500;

initial begin
  d = 8'hxx;
  ra = 5'hxx;
  rs = 0;

  #80 @(posedge clk) res <= 0;

  // reset frame counter
  set_reg(REG_FRAME_CNT, 8'h80);

  // enable all sound channels except dmc
  set_reg(REG_SND_CHN, 8'b00001111);

  // Make the Mario jump sound
  set_reg(REG_SQ1_VOL,   8'h82);
  set_reg(REG_SQ1_SWEEP, 8'hA9);
  set_reg(REG_SQ1_LO,    8'h80);
  set_reg(REG_SQ1_HI,    8'h08);

  repeat (20) begin
    // reset frame counter, emit ticks
    set_reg(REG_FRAME_CNT, 8'hFF);
    #20 ;
  end

  $finish;
end


endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s smb3_tb -o smb3_tb.vvp -f apu.files smb3_tb.v && ./smb3_tb.vvp"
// End:
