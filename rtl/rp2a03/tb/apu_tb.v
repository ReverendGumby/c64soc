`timescale 1ns / 1ps

module apu_tb();

initial begin
  $dumpfile("apu_tb.vcd");
  $dumpvars;
end

reg [7:0]    d;
reg [4:0]    ra;
reg [8:0]    ps_cnt;
reg          res, clk;
reg          mclk;
reg          rw, rs;

wire [7:0]   d_o;
wire [15:0]  pdout;
wire         pdclken;

initial begin
  clk = 1;
  res = 1;
  mclk = 1;
  ps_cnt = 9'h0;
end

always #0.5 begin :fclkgen // 21.477272 MHz
  clk = !clk;
end

always #0.44744 begin :mclkgen // 24 MHz
  mclk = !mclk;
end

// Parallel sample output (MCLK / 512)
always @(posedge mclk) begin
  ps_cnt[8:0] <= ps_cnt + 1'b1;
end

assign pdclken = &ps_cnt;

//////////////////////////////////////////////////////////////////////

rp2a03_clkgen clkgen
  (
   .CLK_RESET(1'b0),
   .CLK(clk),
   .HOLD(1'b0),

   .CPU_CP1_POSEDGE(cpu_cp1_posedge),
   .CPU_CP2(cpu_cp2),
   .CPU_CP2_POSEDGE(cpu_cp2_posedge),
   .CPU_CP2_NEGEDGE(cpu_cp2_negedge),

   .APU_CP1(apu_cp1),
   .APU_CP1_POSEDGE(apu_cp1_posedge),
   .APU_CP1_NEGEDGE(apu_cp1_negedge),
   .APU_CP2(apu_cp2),
   .APU_CP2_POSEDGE(apu_cp2_posedge),
   .APU_CP2_NEGEDGE(apu_cp2_negedge)
   );

rp2a03_apu apu
  (
   .nRES(!res),
   .CLK(clk),
   .CPU_CP1_POSEDGE(cpu_cp1_posedge),
   .CPU_CP2(cpu_cp2),
   .CPU_CP2_NEGEDGE(cpu_cp2_negedge),
   .APU_CP1_POSEDGE(apu_cp1_posedge),
   .APU_CP1_NEGEDGE(apu_cp1_negedge),
   .APU_CP2_POSEDGE(apu_cp2_posedge),
   .APU_CP2_NEGEDGE(apu_cp2_negedge),
   .HOLD_ACTIVE(1'b0),

   .RS(rs),
   .RA(ra),
   .DB_I(d),
   .DB_O(d_o),
   .RW(rw),

   .MCLK(mclk),
   .PDCLKEN(pdclken),
   .PDOUT(pdout)
   );

//////////////////////////////////////////////////////////////////////

`include "../apu_reg_idx.vh"

task set_reg(input [4:0] r, input [7:0] v); 
  begin
    rw <= 1'b0;
    ra <= r;
    d <= v;
    while (!cpu_cp2_posedge)
      @(posedge clk) ;
    rs <= 1'b1;
    while (!cpu_cp2_negedge)
      @(posedge clk) ;
    rs <= 1'b0;
    @(posedge cpu_cp2_negedge) ;
    @(posedge cpu_cp2_negedge) ;
  end
endtask

task get_reg(input [4:0] r, input [7:0] v, input [7:0] m = 8'hff);
  begin
    rw <= 1'b1;
    ra <= r;
    while (!cpu_cp2_posedge)
      @(posedge clk) ;
    rs <= 1'b1;
    while (!cpu_cp2_negedge)
      @(posedge clk) ;
    assert ((d_o & m) == v);
    @(posedge cpu_cp2_negedge) ;
    @(posedge cpu_cp2_negedge) ;
  end
endtask

//////////////////////////////////////////////////////////////////////

localparam frame_tick = 89500;

initial begin
  d = 8'hxx;
  ra = 5'hxx;
  rs = 0;

  #80 @(posedge clk) res <= 0;

  // enable all sound channels except dmc
  set_reg(REG_SND_CHN, 8'b00001111);

  // square1, envelope decay, sweep on
  set_reg(REG_SQ1_VOL, 8'b10000000);
  set_reg(REG_SQ1_SWEEP, 8'b10010011);
  set_reg(REG_SQ1_LO, 8'h80);
  set_reg(REG_SQ1_HI, 8'h58);

  // triangle
  set_reg(REG_TRI_LO, 8'd139);
  set_reg(REG_TRI_HI, 8'h28);
  set_reg(REG_TRI_LINEAR, 8'b11000000);

  // noise
  set_reg(REG_NOISE_LEN, 8'h00);
  set_reg(REG_NOISE_MP, 8'b10000101);
  set_reg(REG_NOISE_VOL, 8'b00111111);

  // start 'em
  set_reg(REG_FRAME_CNT, 8'h80);

  #(frame_tick * 10) $finish;
end


endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s apu_tb -o apu_tb.vvp -f apu.files apu_tb.v && ./apu_tb.vvp"
// End:
