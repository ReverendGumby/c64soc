`timescale 1ns / 1ps

// BizHawk emulates this APU/CPU clock alignment on reset, such that the first CPU instruction is aligned with APU_CP2.
// file:///Users/dhunter/c64/visual2a03/index.html?graphics=f&reset0=0&reset1=4&logmore=res,apu_clk1,apu_clk2e,rdy&a=0&d=a9008d1440f0fe

module oamdma_tb();

initial begin
  $dumpfile("oamdma_tb.vcd");
  $dumpvars;
end

integer      cycle;
reg [7:0]    d, d_d;
reg          res, clk;
reg          hold;

wire [15:0]  a;
wire [7:0]   d_o_cpu, d_o_wram;

event        hold_req;

initial begin
  clk = 1;
  res = 1;
  hold = 0;
end

always #0.5 begin :clkgen
  clk = !clk;
end

always @(posedge clk)
  if (cp2_posedge & ~hold)
    cycle = cycle + 1;

always @hold_req begin
  @(posedge cp2_negedge) @(posedge clk) ;
  hold <= 1'b1;
  @(posedge cp1_posedge) force d = 8'haa;
  @(posedge cp2_negedge) @(posedge clk) ;
  @(posedge cp1_posedge) release d;
  hold <= 1'b0;
end

wire ncs_ram = !(cp2 && (a[14:12] == 3'h0 || a[14:12] == 3'h7));
wire ncs_ppu = !(a[14:13] == 2'h2);

rp2a03 cpu
  (
   .nRES(!res),
   .CLK_RESET(1'b0),
   .CLK(clk),
   .CP1(cp1),
   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2(cp2),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .APU_CP1_POSEDGE(apu_cp1_posedge),
   .nIRQ(1'b1),
   .nNMI(1'b1),
   .A_O(a),
   .A_OE(),
   .D_I(d),
   .D_O(d_o_cpu),
   .D_OE(d_oe_cpu),
   .RW(rw),
   .INP(),
   .OUT(),
   .RDY(1'b1),
   .SYNC(),
   .HOLD(hold)
   );

sram_16k wram
  (
   .CLK(clk),
   .CLKEN(1'b1),
   .nCE(ncs_ram),
   .nOE(1'b0),
   .nWE(rw),
   .A(a[10:0]),
   .D_I(d),
   .D_O(d_o_wram),
   .D_OE(d_oe_wram)
   );

always @* begin
  d = d_d;
  case ({d_oe_cpu, d_oe_wram})
    2'b10: d = d_o_cpu;
    2'b01: d = d_o_wram;
    default: ;
  endcase
end

// Emulate bus capacitance
always @(posedge clk)
  d_d <= d;

initial begin
  cpu.mos6502.s = 8'h00; // because we don't set it

  $readmemh("oamdma_ram.hex", wram.mem);

  #80 while (~(cp1_posedge & apu_cp1_posedge))
    @(posedge clk) ;
  cycle <= 0;
  #(2*12) res <= 0;

  // Test HOLD
  #6990 -> hold_req;

  #10000 $finish;
end

always @(posedge clk) begin
  if (cycle == 17 && cp2_negedge)
    assert(cpu.rdy == 1'b1);
  if (cycle == 17 && cp1_posedge)
    assert(cpu.rdy == 1'b0);
  if (cycle == 530 && cp1_posedge)
    assert(cpu.rdy == 1'b0 && cpu.A_O == 16'h2004);
  if (cycle == 530 && cp1_negedge)
    assert(cpu.rdy == 1'b1);
  if (cycle == 531 && cp1_posedge)
    assert(cpu.A_O == 16'h0005);
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s oamdma_tb -o oamdma_tb.vvp -f rp2a03.files oamdma_tb.v && ./oamdma_tb.vvp"
// End:
