module rom
  (
   input        CLK,
   input        CLKEN,
   input        nCE,
   input        nOE,
   input [14:0] A,
   output [7:0] D_O,
   output       D_OE
   );

reg [7:0] mem [0:(1<<15)-1];
reg [7:0] d;

always @(posedge CLK) if (CLKEN) begin
  d <= mem[A];
end

assign D_O = d;
assign D_OE = !nCE && !nOE;

endmodule
