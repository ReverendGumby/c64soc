// Indices for apu() rsr and rsw

localparam [4:0] REG_SQ1_VOL    = 5'h00; // $4000
localparam [4:0] REG_SQ1_SWEEP  = 5'h01; // $4001
localparam [4:0] REG_SQ1_LO     = 5'h02; // $4002
localparam [4:0] REG_SQ1_HI     = 5'h03; // $4003
localparam [4:0] REG_SQ2_VOL    = 5'h04; // $4004
localparam [4:0] REG_SQ2_SWEEP  = 5'h05; // $4005
localparam [4:0] REG_SQ2_LO     = 5'h06; // $4006
localparam [4:0] REG_SQ2_HI     = 5'h07; // $4007
localparam [4:0] REG_TRI_LINEAR = 5'h08; // $4008
localparam [4:0] REG_TRI_LO     = 5'h0A; // $400A
localparam [4:0] REG_TRI_HI     = 5'h0B; // $400B
localparam [4:0] REG_NOISE_VOL  = 5'h0C; // $400C
localparam [4:0] REG_NOISE_MP   = 5'h0E; // $400E
localparam [4:0] REG_NOISE_LEN  = 5'h0F; // $400F
localparam [4:0] REG_DMC_FREQ   = 5'h10; // $4010
localparam [4:0] REG_DMC_RAW    = 5'h11; // $4011
localparam [4:0] REG_DMC_START  = 5'h12; // $4012
localparam [4:0] REG_DMC_LEN    = 5'h13; // $4013
localparam [4:0] REG_SND_CHN    = 5'h15; // $4015
localparam [4:0] REG_FRAME_CNT  = 5'h17; // $4017 (JOY2 on read)
