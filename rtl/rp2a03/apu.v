module rp2a03_apu
  (
   input            nRES,
   input            CLK,
   input            CPU_CP1_POSEDGE,
   input            CPU_CP2,
   input            CPU_CP2_NEGEDGE,
   input            APU_CP1_POSEDGE,
   input            APU_CP1_NEGEDGE,
   input            APU_CP2_POSEDGE,
   input            APU_CP2_NEGEDGE,
   input            HOLD_ACTIVE,

   input [4:0]      MON_SEL,
   input            MON_READY,
   output reg [7:0] MON_DOUT,
   output reg       MON_VALID,

   input            RS,
   input [4:0]      RA,
   input [7:0]      DB_I,
   output reg [7:0] DB_O,
   output reg       DB_OE,
   input            RW,
  
   output           DMCDMA_RESTART,
   output           DMCDMA_TRIGGER,
   input            DMCDMA_READ,

   input            MCLK,
   input            PDCLKEN,
   output [15:0]    PDOUT
   );

`include "apu_reg_idx.vh"

// Clock notes
// -----------
// Sound blocks are driven by various clocks:
//
//   APU_CP1 (894886 Hz): all length cntr
//   CPU_CP1: triangle freq. cntr and sequencer

//////////////////////////////////////////////////////////////////////
// Register select logic

reg [23:0] rsr, rsw;            // one-hot register read/write selects

always @* begin
  rsr = 24'h0;
  rsw = 24'h0;
  rsr[RA] = RS & RW;
  rsw[RA] = RS & ~RW;
end

wire dmc_len, noi_len, tri_len, sq2_len, sq1_len;

always @* begin
  DB_O = 8'hxx;
  DB_OE = 1'b0;
  if (rsr[REG_SND_CHN] && CPU_CP2 && !HOLD_ACTIVE) begin
    DB_O = {3'b000, dmc_len, noi_len, tri_len, sq2_len, sq1_len};
    DB_OE = 1'b1;
  end
end

//////////////////////////////////////////////////////////////////////
// Debug monitor interface

always @(posedge CLK) begin
  MON_DOUT <= 8'h0;
  MON_VALID <= 1'b0;

  if (MON_READY) begin
    //``REGION_REGS NESBUS_CTRL
    case (MON_SEL[4:0])         //``SUBREGS APU
      5'h15: begin              //``REG SND_CHN
        MON_DOUT[4] <= dmc_len;
        MON_DOUT[3] <= noi_len;
        MON_DOUT[2] <= tri_len;
        MON_DOUT[1] <= sq2_len;
        MON_DOUT[0] <= sq1_len;
      end
    endcase
    MON_VALID <= 1'b1;
  end
end

//////////////////////////////////////////////////////////////////////
// Frame counter
//
// A LFSR designed to tick every 240 Hz, or ~ 'hE90 x APU_CPx.
// Taps are spaced 3726 - 3279 clocks apart and assert sequentially.

reg [15:0] frame_sr;
reg [5:0]  frame_tap;
reg        frame_wrote, frame_mode, frame_wrote_d;

wire frame_sr_fb = (frame_sr[14] ^ frame_sr[13]) | frame_tap[5];
wire frame_reset = frame_wrote_d | frame_tap[frame_mode ? 4 : 3];

initial begin
  frame_sr = 15'h0;
  frame_wrote_d <= 1'b0;
end

always @(posedge CLK) begin
  if (!nRES) begin
    frame_wrote <= 1'b0;
    frame_mode <= 1'b0;
  end
  else if (CPU_CP2_NEGEDGE) begin
    if (rsw[REG_FRAME_CNT]) begin
      frame_mode <= DB_I[7];
      frame_wrote <= 1'b1;
    end
  end
  else if (APU_CP1_POSEDGE) begin
    frame_wrote_d <= frame_wrote;

    if (frame_wrote)
      frame_wrote <= 1'b0;

    if (frame_reset)
      frame_sr <= 15'h7fff;
    else
      frame_sr <= {frame_sr[13:0], frame_sr_fb};
  end
end

always @* begin
  frame_tap[5] = frame_sr == 15'b000000000000000; // power-on reset
  frame_tap[4] = frame_sr == 15'b111000110000101;
  frame_tap[3] = frame_sr == 15'b000101000011111;
  frame_tap[2] = frame_sr == 15'b010110011010011;
  frame_tap[1] = frame_sr == 15'b011011000000011;
  frame_tap[0] = frame_sr == 15'b001000001100001;
end

reg frame_cnt_l, frame_cnt_e;

always @* begin
  frame_cnt_l = 1'b0;
  frame_cnt_e = 1'b0;

  if (frame_mode == 1'b0) begin
    frame_cnt_l = frame_tap[1] | frame_tap[3]; // every other tick
    frame_cnt_e = |frame_tap[3:0];             // every tick
  end
  else begin
    if (!frame_tap[3]) begin    // no clocks out in this tap
      frame_cnt_l = frame_tap[1] | frame_tap[4];
      frame_cnt_e = |frame_tap[4:0]; // every tick
    end
    if (frame_wrote_d) begin
      frame_cnt_l = 1'b1;
      frame_cnt_e = 1'b1;
    end
  end
end

//////////////////////////////////////////////////////////////////////
// Square wave channels

wire [3:0] sq1_out, sq2_out;

rp2a03_square_chan sq1
  (
   .nRES(nRES),
   .CLK(CLK),
   .CPU_CP2_NEGEDGE(CPU_CP2_NEGEDGE),
   .APU_CP1_POSEDGE(APU_CP1_POSEDGE),
   .FRAME_CNT_E(frame_cnt_e),
   .FRAME_CNT_L(frame_cnt_l),

   .RSW_VOL(rsw[REG_SQ1_VOL]),
   .RSW_SWEEP(rsw[REG_SQ1_SWEEP]),
   .RSW_LO(rsw[REG_SQ1_LO]),
   .RSW_HI(rsw[REG_SQ1_HI]),
   .RSW_SND_CHN(rsw[REG_SND_CHN]),
   .DB_I(DB_I),

   .INST(1'b0),
   .SND_CHN_EN(DB_I[0]),
   .LEN(sq1_len),

   .OUT(sq1_out)
   );

rp2a03_square_chan sq2
  (
   .nRES(nRES),
   .CLK(CLK),
   .CPU_CP2_NEGEDGE(CPU_CP2_NEGEDGE),
   .APU_CP1_POSEDGE(APU_CP1_POSEDGE),
   .FRAME_CNT_E(frame_cnt_e),
   .FRAME_CNT_L(frame_cnt_l),

   .RSW_VOL(rsw[REG_SQ2_VOL]),
   .RSW_SWEEP(rsw[REG_SQ2_SWEEP]),
   .RSW_LO(rsw[REG_SQ2_LO]),
   .RSW_HI(rsw[REG_SQ2_HI]),
   .RSW_SND_CHN(rsw[REG_SND_CHN]),
   .DB_I(DB_I),

   .INST(1'b1),
   .SND_CHN_EN(DB_I[1]),
   .LEN(sq2_len),

   .OUT(sq2_out)
   );

//////////////////////////////////////////////////////////////////////
// Triangle wave channel

wire [3:0] tri_out;

rp2a03_triangle_chan tric
  (
   .nRES(nRES),
   .CLK(CLK),
   .CPU_CP1_POSEDGE(CPU_CP1_POSEDGE),
   .CPU_CP2_NEGEDGE(CPU_CP2_NEGEDGE),
   .APU_CP1_POSEDGE(APU_CP1_POSEDGE),
   .FRAME_CNT_E(frame_cnt_e),
   .FRAME_CNT_L(frame_cnt_l),

   .RSW_LINEAR(rsw[REG_TRI_LINEAR]),
   .RSW_LO(rsw[REG_TRI_LO]),
   .RSW_HI(rsw[REG_TRI_HI]),
   .RSW_SND_CHN(rsw[REG_SND_CHN]),
   .DB_I(DB_I),

   .SND_CHN_EN(DB_I[2]),
   .LEN(tri_len),

   .OUT(tri_out)
   );

//////////////////////////////////////////////////////////////////////
// Noise channel

wire [3:0] noi_out;

rp2a03_noise_chan noi
  (
   .nRES(nRES),
   .CLK(CLK),
   .CPU_CP2_NEGEDGE(CPU_CP2_NEGEDGE),
   .APU_CP1_POSEDGE(APU_CP1_POSEDGE),
   .FRAME_CNT_E(frame_cnt_e),
   .FRAME_CNT_L(frame_cnt_l),

   .RSW_VOL(rsw[REG_NOISE_VOL]),
   .RSW_MP(rsw[REG_NOISE_MP]),
   .RSW_LEN(rsw[REG_NOISE_LEN]),
   .RSW_SND_CHN(rsw[REG_SND_CHN]),
   .DB_I(DB_I),

   .SND_CHN_EN(DB_I[3]),
   .LEN(noi_len),

   .OUT(noi_out)
   );

//////////////////////////////////////////////////////////////////////
// Digital Modulation Channel

wire [6:0] dmc_out;

rp2a03_digi_mod_chan dmc
  (
   .nRES(nRES),
   .CLK(CLK),
   .CPU_CP1_POSEDGE(CPU_CP1_POSEDGE),
   .CPU_CP2_NEGEDGE(CPU_CP2_NEGEDGE),
   .APU_CP1_POSEDGE(APU_CP1_POSEDGE),
   .APU_CP2_POSEDGE(APU_CP2_POSEDGE),

   .RSW_FREQ(rsw[REG_DMC_FREQ]),
   .RSW_RAW(rsw[REG_DMC_RAW]),
   .RSW_LEN(rsw[REG_DMC_LEN]),
   .RSW_SND_CHN(rsw[REG_SND_CHN]),
   .DB_I(DB_I),

   .SND_CHN_EN(DB_I[4]),
   .LEN(dmc_len),

   .DMA_RESTART(DMCDMA_RESTART),
   .DMA_TRIGGER(DMCDMA_TRIGGER),
   .DMA_READ(DMCDMA_READ),

   .OUT(dmc_out)
   );

//////////////////////////////////////////////////////////////////////
// Final mixed output

wire [15:0] mix;

// Linear approximation mixer, scaled up by 32768 (for -6dB of headroom)
// [https://www.nesdev.org/wiki/APU_Mixer]
assign mix = (sq1_out + sq2_out) * 9'd246 + tri_out * 10'd279 + noi_out * 9'd162 + dmc_out * 8'd110;

//////////////////////////////////////////////////////////////////////
// Digital filter drops sample rate

wire sclken = CPU_CP1_POSEDGE; // synthesis clock

rp2a03_filter filter
  (
   .nRES(nRES),
   .CLK(CLK),
   .MCLK(MCLK),
   .SCLKEN(sclken),
   .OCLKEN(PDCLKEN),
   .IN(mix),
   .OUT(PDOUT)
   );

endmodule
