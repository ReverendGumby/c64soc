// References:
// - https://wiki.nesdev.com/w/index.php/APU

module rp2a03_triangle_chan
  (
   input        nRES,
   input        CLK,
   input        CPU_CP1_POSEDGE,
   input        CPU_CP2_NEGEDGE,
   input        APU_CP1_POSEDGE,
   input        FRAME_CNT_E,
   input        FRAME_CNT_L,

   input        RSW_LINEAR,
   input        RSW_LO,
   input        RSW_HI,
   input        RSW_SND_CHN,
   input [7:0]  DB_I,

   input        SND_CHN_EN,
   output       LEN,

   output [3:0] OUT
   );

reg [10:0]      cur_freq;
reg [6:0]       linear_reload_cnt;
reg             control_flag, len_en;

// Registers modified only by CPU

always @(posedge CLK) begin
  if (!nRES) begin
    control_flag <= 1'b0;
    linear_reload_cnt <= 7'd0;
    cur_freq <= 11'd0;
    len_en <= 1'b0;
  end
  else begin
    if (CPU_CP2_NEGEDGE) begin
      if (RSW_LINEAR) begin
        control_flag <= DB_I[7];
        linear_reload_cnt <= DB_I[6:0];
      end
      if (RSW_LO)
        cur_freq[7:0] <= DB_I;
      if (RSW_HI)
        cur_freq[10:8] <= DB_I[2:0];
      if (RSW_SND_CHN) begin
        len_en <= SND_CHN_EN;
      end
    end
  end
end

wire len_cnt_halt = ~control_flag;
wire wrote_hi = CPU_CP2_NEGEDGE && RSW_HI;

// Frequency counter

reg [10:0] freq_cnt;

always @(posedge CLK) begin
  if (!nRES) begin
    freq_cnt <= 11'd0;
  end
  else begin
    if (CPU_CP1_POSEDGE) begin
      if (freq_cnt)
        freq_cnt[10:0] <= freq_cnt - 1'd1;
      else
        freq_cnt <= cur_freq;
    end
  end
end

wire freq_wrap = !freq_cnt;

// Linear counter

reg [6:0] linear_cnt;
reg       linear_reload_flag;

always @(posedge CLK) begin
  if (!nRES) begin
    linear_cnt <= -9'sd1;
    linear_reload_flag <= 1'b0;
  end
  else if (wrote_hi)
    linear_reload_flag <= 1'b1;
  else if (FRAME_CNT_E && APU_CP1_POSEDGE) begin
    if (linear_reload_flag)
      linear_cnt <= linear_reload_cnt;
    else if (linear_cnt)
      linear_cnt[6:0] <= linear_cnt - 1'd1;

    if (!control_flag)
      linear_reload_flag <= 1'b0;
  end
end

wire linear_gated = !linear_cnt;

// Length counter

reg [8:0] len_cnt;

`include "len_lut.vh"

wire [4:0] len_load = DB_I[7:3];
wire       len_gated;

always @(posedge CLK) begin
  if (!nRES)
    len_cnt <= -9'sd1;
  else if (wrote_hi)
    len_cnt <= len_lut[len_load];
  else if (APU_CP1_POSEDGE) begin
    if (!len_en)
      len_cnt <= -9'sd1;
    else if (FRAME_CNT_L && !len_cnt_halt)
      if (!len_gated)
        len_cnt <= len_cnt[7:0] - 1'd1;
  end
end

// Channel is silenced when length counter decrements from zero.
assign len_gated = len_cnt[8];

assign LEN = ~len_gated;

// Sequencer

reg [3:0] seq_out;
reg       seq_down;

always @(posedge CLK) begin
  if (!nRES) begin
    seq_out <= 4'd15;
    seq_down <= 1'b1;
  end
  else if (CPU_CP1_POSEDGE) begin
    if (freq_wrap & ~(linear_gated | len_gated)) begin
      if (seq_down) begin
        if (seq_out != 4'd0)
          seq_out[3:0] <= seq_out - 1'd1;
        else
          seq_down <= ~seq_down;
      end
      else begin
        if (seq_out != 4'd15)
          seq_out[3:0] <= seq_out + 1'd1;
        else
          seq_down <= ~seq_down;
      end
    end
  end
end

// Final channel output is sequencer.
assign OUT = seq_out;

endmodule
