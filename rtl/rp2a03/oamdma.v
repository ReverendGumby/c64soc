module rp2a03_oamdma
  (
   input         nRES,
   input         CLK,
   input         CP1_POSEDGE,
   input         CP2,
   input         CP2_NEGEDGE,
   input         APU_CP1_POSEDGE,
   input         APU_CP2_POSEDGE,
   input         HOLD_ACTIVE,

   input         RS,
   input [4:0]   RA,
   input         RW_I,

   input         ACTIVE_I,
   output        ACTIVE_O,

   output [15:0] A_O,
   output        A_OE,
   input [7:0]   DB_I,
   output [7:0]  DB_O,
   output        DB_OE,
   output        RW_O,
   output        RDY
   );

reg [15:0] a;
reg [7:0]  db_b;
reg [7:0]  page;
reg [7:0]  off;
reg        a_oe;
reg        trigger;
reg        cp1_posedge_d;
reg        rw_d;
reg        read;                // toggles every CP2 cycle
reg        active;
reg        leading;

//////////////////////////////////////////////////////////////////////
// Register logic

always @(posedge CLK) begin
  if (!nRES) begin
    page <= 8'hxx;
    trigger <= 1'b0;
  end
  else if (CP2_NEGEDGE) begin
    if (RS && !RW_I) begin
      case (RA)
        5'h14: begin              // $4014 OAMDMA
          page <= DB_I;
          trigger <= 1'b1;
        end
      endcase
    end
    else if (active)
      trigger <= 1'b0;
  end
end

//////////////////////////////////////////////////////////////////////
// DMA logic

always @*
  leading = active && !ACTIVE_I; // DMC preempts us

always @(posedge CLK) begin
  if (!nRES) begin
    active <= 1'b0;
  end
  else begin
    cp1_posedge_d <= CP1_POSEDGE;

    read <= (read | APU_CP1_POSEDGE) & ~APU_CP2_POSEDGE;

    if (CP2_NEGEDGE) begin
      rw_d <= RW_I;
      if (leading && read)
        db_b <= DB_I;
    end

    if (CP1_POSEDGE) begin
      if (leading) begin
        if (read) begin
          off[7:0] <= off + 8'h01;
        end
        else begin
          if (off == 8'h00)
            active <= 1'b0;
        end
      end
      else if (!active) begin
        if (trigger && !read && (rw_d || ACTIVE_I)) begin
          active <= 1'b1;
          off <= 8'h00;
        end
      end
    end
  end
end

always @(posedge CLK) if (cp1_posedge_d) begin
  a <= 16'hxxxx;
  if (leading) begin
    if (read)
      a <= {page, off};
    else
      a <= 16'h2004;            // PPU's OAMDATA
  end

  a_oe <= leading;
end

assign ACTIVE_O = active;
assign A_O = a;
assign A_OE = a_oe && !HOLD_ACTIVE;
assign DB_O = db_b;
assign DB_OE = leading && !read && CP2;
assign RW_O = !leading || read || HOLD_ACTIVE;
assign RDY = !(|trigger || active);

endmodule
