module rp2a03_dmcdma
  (
   input         nRES,
   input         CLK,
   input         CP1_POSEDGE,
   input         CP2_NEGEDGE,
   input         APU_CP1_POSEDGE,
   input         APU_CP2_POSEDGE,
   input         HOLD_ACTIVE,

   input         RS,
   input [4:0]   RA,
   input         RW_I,

   input         RESTART,
   input         TRIGGER,
   input         ACTIVE_I,
   output        ACTIVE_O,
   output        READ,

   output [15:0] A_O,
   output        A_OE,
   input [7:0]   DB_I,
   output        RDY
   );

`include "apu_reg_idx.vh"

reg [15:0] a;
reg [14:0] sa;
reg [7:0]  sa_buf;
reg        trigger;
reg        a_oe;
reg        cp1_posedge_d;
reg        rw_d;
reg        read;                // toggles every CP2 cycle
reg        active;
reg        leading;

//////////////////////////////////////////////////////////////////////
// Register logic

always @(posedge CLK) begin
  if (!nRES) begin
    sa_buf <= 8'hxx;
    sa <= 15'hxxxx;
  end
  else begin
    if (CP2_NEGEDGE) begin
      if (RS && !RW_I) begin
        case (RA)
          REG_DMC_START: begin
            sa_buf <= DB_I;
          end
        endcase
      end
    end
    if (APU_CP1_POSEDGE) begin
      if (RESTART)
        sa <= {1'b1, sa_buf, 6'b0}; // $C000 + (A * 64)
      else if (active)
        sa <= sa[14:0] + 1'd1;
    end
  end
end

//////////////////////////////////////////////////////////////////////
// DMA logic

always @*
  leading = active;

always @(posedge CLK) begin
  if (!nRES) begin
    trigger <= 1'b0;
    active <= 1'b0;
  end
  else begin
    cp1_posedge_d <= CP1_POSEDGE;

    read <= (read | APU_CP1_POSEDGE) & ~APU_CP2_POSEDGE;

    if (CP2_NEGEDGE && read)
      rw_d <= RW_I;

    if (CP1_POSEDGE) begin
      trigger <= trigger | TRIGGER;

      if (!active) begin
        if (trigger && !read && (rw_d || ACTIVE_I)) begin
          active <= 1'b1;
          trigger <= 1'b0;
        end
      end
      else if (leading) begin
        if (!read)
          active <= 1'b0;
      end
    end
  end
end

always @(posedge CLK) if (cp1_posedge_d) begin
  a <= 16'hxxxx;
  if (leading) begin
    if (read)
      a <= {1'b1, sa};
  end

  a_oe <= leading && read;
end

assign ACTIVE_O = active;
assign READ = active && read;
assign A_O = a;
assign A_OE = a_oe && !HOLD_ACTIVE;
assign RDY = !(TRIGGER || trigger || READ);

endmodule
