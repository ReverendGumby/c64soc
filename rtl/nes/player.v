module player
  (
   input            CLK,

   input [1:0]      INP, // pin 34, 17
   input [2:0]      OUT, // pin 43-45

   output reg [4:0] D_O,
   output reg       D_OE,

   output [1:0]     CTLR1_I,
   input [2:0]      CTLR1_O,
   output [1:0]     CTLR2_I,
   input [2:0]      CTLR2_O
   );

always @* begin
  D_OE = 1'b0;
  D_O = 8'hxx;
  if (~&INP) begin
    D_OE = 1'b1;
    if (!INP[0])
      D_O = {~CTLR1_O[2:1], 2'b00, ~CTLR1_O[0]};
    else if (!INP[1])
      D_O = {~CTLR2_O[2:1], 2'b00, ~CTLR2_O[0]};
  end
end

assign CTLR1_I = {OUT[0], INP[0]};
assign CTLR2_I = {OUT[0], INP[1]};

endmodule
