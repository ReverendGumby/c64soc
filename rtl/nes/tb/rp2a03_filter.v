module rp2a03_filter
  (
   input         nRES,
   input         CLK,
   input         MCLK,
   input         SCLKEN,
   input         OCLKEN,

   input [15:0]  IN,
   output [15:0] OUT
   )
  ;

assign OUT = IN;

endmodule
