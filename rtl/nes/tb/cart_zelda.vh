string fn = "Legend of Zelda, The (U) (PRG1) [!].nes.banks/";
task cart_load();
  $readmemh({fn, "bank00.hex"}, nes.cart.raom.bs.bank[8'h00].dpram.mem);
  $readmemh({fn, "bank01.hex"}, nes.cart.raom.bs.bank[8'h01].dpram.mem);
  $readmemh({fn, "bank02.hex"}, nes.cart.raom.bs.bank[8'h02].dpram.mem);
  $readmemh({fn, "bank03.hex"}, nes.cart.raom.bs.bank[8'h03].dpram.mem);
  $readmemh({fn, "bank04.hex"}, nes.cart.raom.bs.bank[8'h04].dpram.mem);
  $readmemh({fn, "bank05.hex"}, nes.cart.raom.bs.bank[8'h05].dpram.mem);
  $readmemh({fn, "bank06.hex"}, nes.cart.raom.bs.bank[8'h06].dpram.mem);
  $readmemh({fn, "bank07.hex"}, nes.cart.raom.bs.bank[8'h07].dpram.mem);
  $readmemh({fn, "bank08.hex"}, nes.cart.raom.bs.bank[8'h08].dpram.mem);
  $readmemh({fn, "bank09.hex"}, nes.cart.raom.bs.bank[8'h09].dpram.mem);
  $readmemh({fn, "bank0A.hex"}, nes.cart.raom.bs.bank[8'h0A].dpram.mem);
  $readmemh({fn, "bank0B.hex"}, nes.cart.raom.bs.bank[8'h0B].dpram.mem);
  $readmemh({fn, "bank0C.hex"}, nes.cart.raom.bs.bank[8'h0C].dpram.mem);
  $readmemh({fn, "bank0D.hex"}, nes.cart.raom.bs.bank[8'h0D].dpram.mem);
  $readmemh({fn, "bank0E.hex"}, nes.cart.raom.bs.bank[8'h0E].dpram.mem);
  $readmemh({fn, "bank0F.hex"}, nes.cart.raom.bs.bank[8'h0F].dpram.mem);
  $readmemh({fn, "bank10.hex"}, nes.cart.raom.bs.bank[8'h10].dpram.mem);
  $readmemh({fn, "bank11.hex"}, nes.cart.raom.bs.bank[8'h11].dpram.mem);
  $readmemh({fn, "bank12.hex"}, nes.cart.raom.bs.bank[8'h12].dpram.mem);
  $readmemh({fn, "bank13.hex"}, nes.cart.raom.bs.bank[8'h13].dpram.mem);
  $readmemh({fn, "bank14.hex"}, nes.cart.raom.bs.bank[8'h14].dpram.mem);
  $readmemh({fn, "bank15.hex"}, nes.cart.raom.bs.bank[8'h15].dpram.mem);
  $readmemh({fn, "bank16.hex"}, nes.cart.raom.bs.bank[8'h16].dpram.mem);
  $readmemh({fn, "bank17.hex"}, nes.cart.raom.bs.bank[8'h17].dpram.mem);
  $readmemh({fn, "bank18.hex"}, nes.cart.raom.bs.bank[8'h18].dpram.mem);
  $readmemh({fn, "bank19.hex"}, nes.cart.raom.bs.bank[8'h19].dpram.mem);
  $readmemh({fn, "bank1A.hex"}, nes.cart.raom.bs.bank[8'h1A].dpram.mem);
  $readmemh({fn, "bank1B.hex"}, nes.cart.raom.bs.bank[8'h1B].dpram.mem);
  $readmemh({fn, "bank1C.hex"}, nes.cart.raom.bs.bank[8'h1C].dpram.mem);
  $readmemh({fn, "bank1D.hex"}, nes.cart.raom.bs.bank[8'h1D].dpram.mem);
  $readmemh({fn, "bank1E.hex"}, nes.cart.raom.bs.bank[8'h1E].dpram.mem);
  $readmemh({fn, "bank1F.hex"}, nes.cart.raom.bs.bank[8'h1F].dpram.mem);

  nes.cart.raom.mmu.tt_set(8'h00, 32'h110000);
  nes.cart.raom.mmu.tt_set(8'h01, 32'h110001);
  nes.cart.raom.mmu.tt_set(8'h02, 32'h110002);
  nes.cart.raom.mmu.tt_set(8'h03, 32'h110003);
  nes.cart.raom.mmu.tt_set(8'h04, 32'h110004);
  nes.cart.raom.mmu.tt_set(8'h05, 32'h110005);
  nes.cart.raom.mmu.tt_set(8'h06, 32'h110006);
  nes.cart.raom.mmu.tt_set(8'h07, 32'h110007);
  nes.cart.raom.mmu.tt_set(8'h08, 32'h110008);
  nes.cart.raom.mmu.tt_set(8'h09, 32'h110009);
  nes.cart.raom.mmu.tt_set(8'h0A, 32'h11000A);
  nes.cart.raom.mmu.tt_set(8'h0B, 32'h11000B);
  nes.cart.raom.mmu.tt_set(8'h0C, 32'h11000C);
  nes.cart.raom.mmu.tt_set(8'h0D, 32'h11000D);
  nes.cart.raom.mmu.tt_set(8'h0E, 32'h11000E);
  nes.cart.raom.mmu.tt_set(8'h0F, 32'h11000F);
  nes.cart.raom.mmu.tt_set(8'h10, 32'h110010);
  nes.cart.raom.mmu.tt_set(8'h11, 32'h110011);
  nes.cart.raom.mmu.tt_set(8'h12, 32'h110012);
  nes.cart.raom.mmu.tt_set(8'h13, 32'h110013);
  nes.cart.raom.mmu.tt_set(8'h14, 32'h110014);
  nes.cart.raom.mmu.tt_set(8'h15, 32'h110015);
  nes.cart.raom.mmu.tt_set(8'h16, 32'h110016);
  nes.cart.raom.mmu.tt_set(8'h17, 32'h110017);
  nes.cart.raom.mmu.tt_set(8'h18, 32'h110018);
  nes.cart.raom.mmu.tt_set(8'h19, 32'h110019);
  nes.cart.raom.mmu.tt_set(8'h1A, 32'h11001A);
  nes.cart.raom.mmu.tt_set(8'h1B, 32'h11001B);
  nes.cart.raom.mmu.tt_set(8'h1C, 32'h11001C);
  nes.cart.raom.mmu.tt_set(8'h1D, 32'h11001D);
  nes.cart.raom.mmu.tt_set(8'h1E, 32'h11001E);
  nes.cart.raom.mmu.tt_set(8'h1F, 32'h11001F);
  nes.cart.raom.mmu.tt_set(8'h20, 32'h320000);
  nes.cart.raom.mmu.tt_set(8'h21, 32'h320001);

  // CPU reads cart. WRAM on boot
  //
  // This is how the initial RAM image was created:
  //
  // openssl rand -out secret.key 32
  // openssl aes-256-cbc -in hw/iec/01/iec-B_Mask.gbr -out ram.t -pass file:secret.key
  // dd if=ram.t of=wram.bin count=8 bs=1k skip=1
  // python tools/bin_to_memh.py mem 32 <wram.bin >rtl/nes/tb/prgram_init.hex
  //
  // Next, we ran the sim and let it initialize WRAM. It zero'd out
  // $6530..$7FFF, and wrote 0xFF's to a few bytes a bit before that.
  //
  // Finally, we saved WRAM to file, re-ran the sim, discovered that
  // it wanted $5A @ $6001 and $A5 @ $7FFF, and wrote those two
  // bytes. Now the sim skips WRAM init.
  //
  $readmemh({fn, "bank22-init.hex"}, nes.cart.raom.bs.bank[8'h22].dpram.mem);
  $readmemh({fn, "bank23-init.hex"}, nes.cart.raom.bs.bank[8'h23].dpram.mem);

  nes.cart.raom.mmu.tt_set(8'h22, 32'h210000);
  nes.cart.raom.mmu.tt_set(8'h23, 32'h210001);

  nes.cart.prg_rom_addr_max = 5'd16; // PRG ROM 128KB
  nes.cart.prg_ram_addr_max = 5'd12; // PRG RAM 8KB
  nes.cart.mapper_sel = 3'd1;        // MMC1
  nes.cart.chr_addr_max = 5'd12;     // CHR RAM 8KB
  nes.cart.chr_read_only = 1'b0;

endtask

task cart_run();
  // Skip the first few frames.
  @(negedge nes.mlb.ppu.res) #10 ;
  nes.mlb.ppu.video_counter.ROW = 9'h104;
  nes.mlb.ppu.video_counter.COL = 9'h153;
  #300 ;
  nes.mlb.ppu.video_counter.ROW = 9'd241;
  nes.mlb.ppu.video_counter.COL = 9'h0;
  #300 ;
  nes.mlb.ppu.video_counter.ROW = 9'h104;
  nes.mlb.ppu.video_counter.COL = 9'h153;
  #300 ;
  nes.mlb.ppu.video_counter.ROW = 9'd241;
  nes.mlb.ppu.video_counter.COL = 9'h0;

  #2000000 ;
endtask

task cart_save();
  $writememh({fn, "bank20.hex"}, nes.cart.raom.bs.bank[8'h20].dpram.mem);
  $writememh({fn, "bank21.hex"}, nes.cart.raom.bs.bank[8'h21].dpram.mem);
  $writememh({fn, "bank22.hex"}, nes.cart.raom.bs.bank[8'h22].dpram.mem);
  $writememh({fn, "bank23.hex"}, nes.cart.raom.bs.bank[8'h23].dpram.mem);
endtask
