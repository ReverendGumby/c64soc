`timescale 1ns / 1ps

module swap_tb();

reg         res;
reg         clk;
reg         loaded;

`include "cart_swap.vh"

initial begin
  $dumpfile("swap_tb.vcd");
  // Minimize dump scope for speed gains
  $dumpvars(1, nes);
  $dumpvars(0, nes.mlb);
  $dumpvars(0, nes.nesbus);
  $dumpvars(1, nes.cart);
  $dumpvars(1, nes.cart.mmc1);
  $dumpvars(1, nes.cart.raom);
  $dumpvars(0, nes.cart.raom.arb);
  $dumpvars(0, nes.cart.raom.mmu);

  clk = 1;
  res = 1;
  loaded = 0;
end

always #1 begin :clkgen
  clk = !clk;
end

initial begin
  #3 @(posedge clk) res = 0;
  #1 nes.nesbus.nes_usr_nres = 1;
  nes.nesbus.CART_nRES = 1;
  #2 ;
  cart_load();
  loaded = 1;

  cart_run();

  //cart_save();

  $finish();
end

// Pretend to swap in the required page
reg [7:0] tt_idx, tt_idx_off;
always @(posedge clk) if (nes.cp2) begin
  if (loaded && !nes.cart.prg_ready) begin
    #40 ;
    for (tt_idx = 8'h00; tt_idx < 8'h1C; tt_idx = tt_idx + 1)
      nes.cart.raom.mmu.tt_set(tt_idx, 32'h0);
    tt_idx = nes.cart.prg_sbba * 8'd4;
    for (tt_idx_off = 8'd0; tt_idx_off < 8'd4; tt_idx_off = tt_idx_off + 1)
      nes.cart.raom.mmu.tt_set(tt_idx + tt_idx_off, 32'h110000 | (tt_idx + tt_idx_off));
  end
end

nes nes
  (
   .CLK(clk),
   .EXT_RES(1'b0),

   .ARESETn(!res),
   .AWADDR(32'b0),
   .AWVALID(1'b0),
   .WDATA(32'b0),
   .WSTRB(4'b0),
   .WVALID(1'b0),
   .BREADY(1'b0),
   .ARADDR(32'b0),
   .ARVALID(1'b0),
   .RREADY(1'b0)
   );

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s swap_tb -o swap_tb.vvp -f nes_tb.files swap_tb.v && ./swap_tb.vvp"
// End:
