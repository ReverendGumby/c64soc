string fn = "NES Test Cart (Official Nintendo) (U) [!].nes.banks/";
task cart_load();
  $readmemh({fn, "bank00.hex"}, nes.cart.raom.bs.bank[8'h00].dpram.mem);
  $readmemh({fn, "bank01.hex"}, nes.cart.raom.bs.bank[8'h01].dpram.mem);
  $readmemh({fn, "bank02.hex"}, nes.cart.raom.bs.bank[8'h02].dpram.mem);
  $readmemh({fn, "bank03.hex"}, nes.cart.raom.bs.bank[8'h03].dpram.mem);
  $readmemh({fn, "bank04.hex"}, nes.cart.raom.bs.bank[8'h04].dpram.mem);
  $readmemh({fn, "bank05.hex"}, nes.cart.raom.bs.bank[8'h05].dpram.mem);
  $readmemh({fn, "bank06.hex"}, nes.cart.raom.bs.bank[8'h06].dpram.mem);
  $readmemh({fn, "bank07.hex"}, nes.cart.raom.bs.bank[8'h07].dpram.mem);
  $readmemh({fn, "bank08.hex"}, nes.cart.raom.bs.bank[8'h08].dpram.mem);
  $readmemh({fn, "bank09.hex"}, nes.cart.raom.bs.bank[8'h09].dpram.mem);

  nes.cart.raom.mmu.tt_set(8'h00, 32'h110000);
  nes.cart.raom.mmu.tt_set(8'h01, 32'h110001);
  nes.cart.raom.mmu.tt_set(8'h02, 32'h110002);
  nes.cart.raom.mmu.tt_set(8'h03, 32'h110003);
  nes.cart.raom.mmu.tt_set(8'h04, 32'h110004);
  nes.cart.raom.mmu.tt_set(8'h05, 32'h110005);
  nes.cart.raom.mmu.tt_set(8'h06, 32'h110006);
  nes.cart.raom.mmu.tt_set(8'h07, 32'h110007);
  nes.cart.raom.mmu.tt_set(8'h08, 32'h320000);
  nes.cart.raom.mmu.tt_set(8'h09, 32'h320001);

  nes.cart.prg_rom_addr_max = 5'd14; // PRG ROM 32KB
  nes.cart.mapper_sel = 3'd0;        // BYPASS
  nes.cart.chr_addr_max = 5'd12;     // CHR RAM 8KB
  nes.cart.chr_read_only = 1'b0;
endtask

task cart_run();
  #4500000 ;
endtask

task cart_save();
endtask
