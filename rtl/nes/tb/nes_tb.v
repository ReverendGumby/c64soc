`timescale 1ns / 1ps

module nes_tb();

reg         res;
reg         clk;

`include "cart_test.vh"

initial begin
  $dumpfile("nes_tb.vcd");
  // Minimize dump scope for speed gains
  $dumpvars(1, nes);
  $dumpvars(1, nes.mlb);
  $dumpvars(1, nes.mlb.player);
  $dumpvars(1, nes.mlb.wram);
  $dumpvars(1, nes.mlb.vram);
  $dumpvars(1, nes.mlb.cpu);
  $dumpvars(1, nes.mlb.ppu);
  $dumpvars(1, nes.mlb.cpu.mos6502);
  $dumpvars(1, nes.cart);
  $dumpvars(1, nes.cart.raom);

  clk = 1;
  res = 1;
end

always #1 begin :clkgen
  clk = !clk;
end

initial begin
  #3 @(posedge clk) res = 0;
  #1 nes.nesbus.nes_usr_nres = 1;
  nes.nesbus.CART_nRES = 1;
  #2 ;
  cart_load();

  cart_run();

  $writememh("wram.hex", nes.mlb.wram.mem);
  $writememh("vram.hex", nes.mlb.vram.mem);
  cart_save();

  $finish();
end

// Press and release A
// initial #3606790 nes.nesbus.ctlr1 <= 8'h01;
// initial #3669370 nes.nesbus.ctlr1 <= 8'h00;

nes nes
  (
   .CLK(clk),
   .EXT_RES(1'b0),

   .ARESETn(!res),
   .AWADDR(32'b0),
   .AWVALID(1'b0),
   .WDATA(32'b0),
   .WSTRB(4'b0),
   .WVALID(1'b0),
   .BREADY(1'b0),
   .ARADDR(32'b0),
   .ARVALID(1'b0),
   .RREADY(1'b0)
   );

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s nes_tb -o nes_tb.vvp -f nes_tb.files nes_tb.v && ./nes_tb.vvp"
// End:
