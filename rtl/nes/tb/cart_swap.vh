string fn = "swap.nes.banks/";
task cart_load();
  $readmemh({fn, "bank00.hex"}, nes.cart.raom.bs.bank[8'h00].dpram.mem);
  $readmemh({fn, "bank01.hex"}, nes.cart.raom.bs.bank[8'h01].dpram.mem);
  $readmemh({fn, "bank02.hex"}, nes.cart.raom.bs.bank[8'h02].dpram.mem);
  $readmemh({fn, "bank03.hex"}, nes.cart.raom.bs.bank[8'h03].dpram.mem);
  $readmemh({fn, "bank04.hex"}, nes.cart.raom.bs.bank[8'h04].dpram.mem);
  $readmemh({fn, "bank05.hex"}, nes.cart.raom.bs.bank[8'h05].dpram.mem);
  $readmemh({fn, "bank06.hex"}, nes.cart.raom.bs.bank[8'h06].dpram.mem);
  $readmemh({fn, "bank07.hex"}, nes.cart.raom.bs.bank[8'h07].dpram.mem);
  $readmemh({fn, "bank08.hex"}, nes.cart.raom.bs.bank[8'h08].dpram.mem);
  $readmemh({fn, "bank09.hex"}, nes.cart.raom.bs.bank[8'h09].dpram.mem);
  $readmemh({fn, "bank0A.hex"}, nes.cart.raom.bs.bank[8'h0A].dpram.mem);
  $readmemh({fn, "bank0B.hex"}, nes.cart.raom.bs.bank[8'h0B].dpram.mem);
  $readmemh({fn, "bank0C.hex"}, nes.cart.raom.bs.bank[8'h0C].dpram.mem);
  $readmemh({fn, "bank0D.hex"}, nes.cart.raom.bs.bank[8'h0D].dpram.mem);
  $readmemh({fn, "bank0E.hex"}, nes.cart.raom.bs.bank[8'h0E].dpram.mem);
  $readmemh({fn, "bank0F.hex"}, nes.cart.raom.bs.bank[8'h0F].dpram.mem);
  $readmemh({fn, "bank10.hex"}, nes.cart.raom.bs.bank[8'h10].dpram.mem);
  $readmemh({fn, "bank11.hex"}, nes.cart.raom.bs.bank[8'h11].dpram.mem);
  $readmemh({fn, "bank12.hex"}, nes.cart.raom.bs.bank[8'h12].dpram.mem);
  $readmemh({fn, "bank13.hex"}, nes.cart.raom.bs.bank[8'h13].dpram.mem);
  $readmemh({fn, "bank14.hex"}, nes.cart.raom.bs.bank[8'h14].dpram.mem);
  $readmemh({fn, "bank15.hex"}, nes.cart.raom.bs.bank[8'h15].dpram.mem);
  $readmemh({fn, "bank16.hex"}, nes.cart.raom.bs.bank[8'h16].dpram.mem);
  $readmemh({fn, "bank17.hex"}, nes.cart.raom.bs.bank[8'h17].dpram.mem);
  $readmemh({fn, "bank18.hex"}, nes.cart.raom.bs.bank[8'h18].dpram.mem);
  $readmemh({fn, "bank19.hex"}, nes.cart.raom.bs.bank[8'h19].dpram.mem);
  $readmemh({fn, "bank1A.hex"}, nes.cart.raom.bs.bank[8'h1A].dpram.mem);
  $readmemh({fn, "bank1B.hex"}, nes.cart.raom.bs.bank[8'h1B].dpram.mem);
  $readmemh({fn, "bank1C.hex"}, nes.cart.raom.bs.bank[8'h1C].dpram.mem);
  $readmemh({fn, "bank1D.hex"}, nes.cart.raom.bs.bank[8'h1D].dpram.mem);
  $readmemh({fn, "bank1E.hex"}, nes.cart.raom.bs.bank[8'h1E].dpram.mem);
  $readmemh({fn, "bank1F.hex"}, nes.cart.raom.bs.bank[8'h1F].dpram.mem);

  // Testbench will swap in 8'h00 - 8'h1B as needed

  nes.cart.raom.mmu.tt_set(8'h1C, 32'h11001C);
  nes.cart.raom.mmu.tt_set(8'h1D, 32'h11001D);
  nes.cart.raom.mmu.tt_set(8'h1E, 32'h11001E);
  nes.cart.raom.mmu.tt_set(8'h1F, 32'h11001F);

  nes.cart.raom.mmu.tt_set(8'h20, 32'h320000);
  nes.cart.raom.mmu.tt_set(8'h21, 32'h320001);

  nes.cart.raom.mmu.tt_set(8'h22, 32'h210000);
  nes.cart.raom.mmu.tt_set(8'h23, 32'h210001);

  nes.cart.prg_rom_addr_max = 5'd16; // PRG ROM 128KB
  nes.cart.prg_ram_addr_max = 5'd12; // PRG RAM 8KB
  nes.cart.mapper_sel = 3'd1;        // MMC1
  nes.cart.chr_addr_max = 5'd12;     // CHR RAM 8KB
  nes.cart.chr_read_only = 1'b0;

endtask

task cart_run();

  #12700 ;
endtask

task cart_save();
endtask
