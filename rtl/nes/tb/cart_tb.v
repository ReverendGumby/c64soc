`timescale 1ns / 1ps

module cart_tb();

reg         res;
reg         clk;

`include "cart_test.vh"

initial begin
  $dumpfile("cart_tb.vcd");
  $dumpvars;

  nes.mlb.vram.mem[0] = 8'h55;
  nes.mlb.vram.mem[1] = 8'haa;

  clk = 1;
  res = 1;
end

reg [31:0] awaddr, araddr, wdata;
reg [3:0]  wstrb;
reg        awvalid, wvalid, bready, arvalid, rready;

wire [31:0] rdata;
wire [1:0]  bresp;
wire        awready, wready, bvalid, arready, rvalid;

initial begin
  awaddr = 32'b0;
  awvalid = 1'b0;
  wdata = 32'b0;
  wstrb = 4'b0;
  wvalid = 1'b0;
  bready = 1'b0;
  araddr = 32'b0;
  arvalid = 1'b0;
  rready = 1'b0;
end

always #1 begin :clkgen
  clk = !clk;
end

task nesbus_write(input [31:0] a, input [31:0] v, input [3:0] s);
  begin
    awaddr <= a;
    awvalid <= 1'b1;
    wdata <= v;
    wstrb <= s;
    wvalid <= 1'b1;
    bready <= 1'b1;
    while (!(awready & wready & bvalid))
      @(posedge clk) ;
    awvalid <= 1'b0;
    wvalid <= 1'b0;
    bready <= 1'b0;
    @(posedge clk) ;
  end
endtask

task nesbus_write32(input [31:0] a, input [31:0] v);
  nesbus_write(a, v, 4'hf);
endtask

task nesbus_write8(input [31:0] a, input [7:0] v);
  nesbus_write(a, {4{v}}, 4'h1 << a[1:0]);
endtask

task nesbus_read32(input [31:0] a, output [31:0] v);
  begin
    araddr <= a;
    arvalid <= 1'b1;
    rready <= 1'b1;
    while (!arready)
      @(posedge clk) ;
    v = rdata;
    arvalid <= 1'b0;
    while (!rvalid)
      @(posedge clk) ;
    rready <= 1'b0;
    @(posedge clk) ;
  end
endtask

task nesbus_read8(input [31:0] a, output [7:0] v);
reg [31:0] tr;
  begin
    nesbus_read32(a, tr);
    v = tr >> (8 * a[1:0]);
  end
endtask

task nesbus_rmw32(input [31:0] a, input [31:0] v, input [31:0] m);
reg [7:0] tr;
  begin
    nesbus_read32(a, tr);
    tr = (tr & ~m) | (v & m);
    nesbus_write32(a, tr);
  end
endtask

task nesbus_rmw8(input [31:0] a, input [7:0] v, input [7:0] m);
reg [7:0] tr;
  begin
    nesbus_read8(a, tr);
    tr = (tr & ~m) | (v & m);
    nesbus_write8(a, tr);
  end
endtask

task bump();
reg [1:0] div;
  begin
    // Hold PPU CC clock divider for one CLK.
    @(negedge nes.mlb.ppu.clkgen.CLK) ;
    div = nes.mlb.ppu.clkgen.div4_clk;
    force nes.mlb.ppu.clkgen.div4_clk = div;
    @(negedge nes.mlb.ppu.clkgen.CLK) ;
    release nes.mlb.ppu.clkgen.div4_clk;
    //nes.mlb.ppu.clkgen.div4_clk[1:0] <= nes.mlb.ppu.clkgen.div4_clk - 1'b1;
  end
endtask

reg [7:0] nbr;
reg [31:0] nbr32;
initial begin
  #3 @(posedge clk) res = 0;
  #1 nesbus_rmw32(19'h50, 32'h4, 32'h4); // cart_nres
  cart_load();

  #1 nesbus_rmw32(19'h50, 32'h1, 32'h1); // nes_usr_nres
  @(posedge nes_nres) ;

  // Wait for CPU to poll PPUSTATUS.
  while (nes.mlb.A_I !== 16'h2002)
    @(posedge nes.mlb.CP2) ;
  // Skip to vbl_end and res2 set.
  #32 ;
  nes.mlb.ppu.video_counter.ROW = 9'h104;
  nes.mlb.ppu.video_counter.COL = 9'h153;
  #16 @(posedge clk) ;
  // Wait for CPU to poll PPUSTATUS.
  while (nes.mlb.A_I !== 16'h2002)
    @(posedge nes.mlb.CP2) ;
  // Skip to vbl_start.
  #32 ;
  nes.mlb.ppu.video_counter.ROW = 9'd241;
  nes.mlb.ppu.video_counter.COL = 9'h0;
  #16 @(posedge clk) ;
  // Wait for CPU to poll PPUSTATUS.
  while (nes.mlb.A_I !== 16'h2002)
    @(posedge nes.mlb.CP2) ;
  // Skip to just before pre-render row.
  nes.mlb.ppu.video_counter.ROW = 9'h104;
  nes.mlb.ppu.video_counter.COL = 9'h150;
  // Enable rendering.
  nes.mlb.ppu.bus_master.vac = 15'h0000;
  nes.mlb.ppu.reg_file.BKG_PAT = 1'b1;
  nes.mlb.ppu.reg_file.BKG_EN = 1'b1;

  // Read out the MMU translation table.
  for (int b = 0; b < 10; b++) begin
  reg [31:0] tt;
    nesbus_write32(19'h10c0, b);
    nesbus_read32(19'h10c4, tt);
    assert(tt != 0);
    $display("TT[%1d] = 0x%x", b, tt);
  end

  // Force cart PRG and CHR bus accesses to overlap at different
  // points.
  for (integer bumps = 0; bumps < 10; bumps ++)
    #1000 bump();

  $finish();
end

nes nes
  (
   .CLK(clk),
   .EXT_RES(1'b0),
   .NES_nRES(nes_nres),

   .ARESETn(!res),
   .AWADDR(awaddr),
   .AWVALID(awvalid),
   .AWREADY(awready),
   .WDATA(wdata),
   .WREADY(wready),
   .WSTRB(wstrb),
   .WVALID(wvalid),
   .BRESP(bresp),
   .BVALID(bvalid),
   .BREADY(bready),
   .ARADDR(araddr),
   .ARVALID(arvalid),
   .ARREADY(arready),
   .RRESP(),
   .RVALID(rvalid),
   .RREADY(rready),
   .RDATA(rdata)
   );

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s cart_tb -o cart_tb.vvp -f nes_tb.files cart_tb.v && ./cart_tb.vvp"
// End:
