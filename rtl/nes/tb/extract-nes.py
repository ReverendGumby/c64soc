# Reference: https://wiki.nesdev.com/w/index.php/INES

import sys
from struct import pack, unpack

def write_hex(fname, data):
    with open(fname, 'w') as f:
        for i in range(0, len(data), 4):
            v = unpack("<I", pack("<4B", *data[i:i+4]))[0]
            f.write("{0:08x}\n".format(v))

nes = sys.argv[1]
prghex = nes + '.prgrom.hex'
chahex = nes + '.charom.hex'

with open(nes, 'rb') as f:
    hdr = f.read(16)
    assert(hdr[0:4] == b'NES\x1A')
    prg_len = hdr[4] * 16384
    cha_len = hdr[5] * 8192

    prgrom = f.read(prg_len)
    charom = f.read(cha_len)

write_hex(prghex, prgrom)
write_hex(chahex, charom)
