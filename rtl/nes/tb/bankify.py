import sys
import os
import os.path

bank_size = 4096 // 4

def read_data(fname):
    data = []
    with open(fname, 'r') as f:
        for l in f.readlines():
            data.append(l)
    return data

def write_data(fname, data):
    with open(fname, 'w') as f:
        for l in data:
            f.write(l)

bank = 0

def write_banks(base, data):
    global bank
    if len(data) == 0:
        return
    for offset in range(0, len(data), bank_size):
        fname = base + 'bank{0:02X}.hex'.format(bank)
        write_data(fname, data[offset:offset+bank_size])
        bank = bank + 1

def bankify(fname, banks):
    data = read_data(fname)
    write_banks(banks, data)

nes = sys.argv[1]
prghex = nes + '.prgrom.hex'
chahex = nes + '.charom.hex'
banks = nes + '.banks/'

if not os.path.exists(banks):
    os.mkdir(banks)

bankify(prghex, banks)
bankify(chahex, banks)
