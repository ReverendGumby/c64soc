`timescale 1ns / 1ps

module nesbus_tb();

reg         res;
reg         clk;

`include "cart_test.vh"

initial begin
  $dumpfile("nesbus_tb.vcd");
  $dumpvars;

  nes.mlb.vram.mem[0] = 8'h55;
  nes.mlb.vram.mem[1] = 8'haa;

  clk = 1;
  res = 1;
end

reg [31:0] awaddr, araddr, wdata;
reg [3:0]  wstrb;
reg        awvalid, wvalid, bready, arvalid, rready;

wire [31:0] rdata;
wire [1:0]  bresp;
wire        awready, wready, bvalid, arready, rvalid;

initial begin
  awaddr = 32'b0;
  awvalid = 1'b0;
  wdata = 32'b0;
  wstrb = 4'b0;
  wvalid = 1'b0;
  bready = 1'b0;
  araddr = 32'b0;
  arvalid = 1'b0;
  rready = 1'b0;
end

always #1 begin :clkgen
  clk = !clk;
end

task nesbus_write(input [31:0] a, input [31:0] v, input [3:0] s);
  begin
    awaddr <= a;
    awvalid <= 1'b1;
    wdata <= v;
    wstrb <= s;
    wvalid <= 1'b1;
    bready <= 1'b1;
    while (!(awready & wready & bvalid))
      @(posedge clk) ;
    awvalid <= 1'b0;
    wvalid <= 1'b0;
    bready <= 1'b0;
  end
endtask

task nesbus_write32(input [31:0] a, input [31:0] v);
  nesbus_write(a, v, 4'hf);
endtask

task nesbus_write8(input [31:0] a, input [7:0] v);
  nesbus_write(a, {4{v}}, 4'h1 << a[1:0]);
endtask

task nesbus_read32(input [31:0] a, output [31:0] v);
  begin
    araddr <= a;
    arvalid <= 1'b1;
    rready <= 1'b1;
    while (!arready)
      @(posedge clk) ;
    v = rdata;
    arvalid <= 1'b0;
    while (!rvalid)
      @(posedge clk) ;
    rready <= 1'b0;
    @(posedge clk) ;
  end
endtask

task nesbus_read8(input [31:0] a, output [7:0] v);
reg [31:0] tr;
  begin
    nesbus_read32(a, tr);
    v = tr >> (8 * a[1:0]);
  end
endtask

task nesbus_rmw32(input [31:0] a, input [31:0] v, input [31:0] m);
reg [7:0] tr;
  begin
    nesbus_read32(a, tr);
    tr = (tr & ~m) | (v & m);
    nesbus_write32(a, tr);
  end
endtask

task nesbus_rmw8(input [31:0] a, input [7:0] v, input [7:0] m);
reg [7:0] tr;
  begin
    nesbus_read8(a, tr);
    tr = (tr & ~m) | (v & m);
    nesbus_write8(a, tr);
  end
endtask

reg [7:0] nbr, nbr2;
reg [31:0] nbr32;
initial begin
  #3 @(posedge clk) res = 0;
  #1 nesbus_rmw32(19'h50, 32'h4, 32'h4); // cart_nres
  cart_load();

  #1 nesbus_rmw32(19'h50, 32'h1, 32'h1); // nes_usr_nres
  @(posedge nes_nres) ;

  // Wait for CPU to poll PPUSTATUS.
  while (nes.mlb.A_I !== 16'h2002)
    @(posedge nes.mlb.CP2) ;
  // Skip to vbl_end and res2 set.
  #32 ;
  nes.mlb.ppu.video_counter.ROW = 9'h104;
  nes.mlb.ppu.video_counter.COL = 9'h153;
  #16 @(posedge clk) ;
  // Wait for CPU to poll PPUSTATUS.
  while (nes.mlb.A_I !== 16'h2002)
    @(posedge nes.mlb.CP2) ;
  // Skip to vbl_start.
  #32 ;
  nes.mlb.ppu.video_counter.ROW = 9'd241;
  nes.mlb.ppu.video_counter.COL = 9'h0;
  #16 @(posedge clk) ;
  // Wait for CPU to poll PPUSTATUS.
  while (nes.mlb.A_I !== 16'h2002)
    @(posedge nes.mlb.CP2) ;
  // Skip to just before pre-render row.
  nes.mlb.ppu.video_counter.ROW = 9'h104;
  nes.mlb.ppu.video_counter.COL = 9'h150;
  // Enable rendering.
  nes.mlb.ppu.bus_master.vac = 15'h0000;
  nes.mlb.ppu.reg_file.BKG_PAT = 1'b1;
  nes.mlb.ppu.reg_file.BKG_EN = 1'b1;
  // Wait until mid-render.
  #3500 @(posedge clk) ;

  // Read from VRAM via PPU bus
  nesbus_read8(19'h22000, nbr);
  assert(nbr == 8'h55);
  nesbus_read8(19'h22001, nbr);
  assert(nbr == 8'haa);

  // Read from CHAROM via PPU bus
  nesbus_read8(19'h20000, nbr);
  assert(nbr == 8'h80);
  nesbus_read8(19'h20010, nbr);
  assert(nbr == 8'h03);
  nesbus_read8(19'h20011, nbr);
  assert(nbr == 8'h07);
  nesbus_read8(19'h20012, nbr);
  assert(nbr == 8'h0f);
  nesbus_read8(19'h20013, nbr);
  assert(nbr == 8'h1f);

  // Read CHAROM from backing store directly
  nesbus_read32(19'h48000, nbr32);
  assert(nbr32 == 32'h00000080);
  nesbus_read32(19'h48010, nbr32);
  assert(nbr32 == 32'h1f0f0703);
  nesbus_read32(19'h48014, nbr32);
  assert(nbr32 == 32'h1fff7f3f);

  // Read from PRGROM via CPU bus
  nesbus_read8(19'h18000, nbr);
  assert(nbr == 8'h48);
  nesbus_read8(19'h18005, nbr);
  assert(nbr == 8'hb1);
  nesbus_read8(19'h1fffe, nbr);
  assert(nbr == 8'h70);
  nesbus_read8(19'h1ffff, nbr);
  assert(nbr == 8'h83);

  // Write and verify WRAM via CPU bus
  nesbus_read8(19'h100a1, nbr2);
  nesbus_write8(19'h100a0, 8'h55);
  nesbus_read8(19'h100a0, nbr);
  assert(nbr == 8'h55);
  nesbus_read8(19'h100a1, nbr);
  assert(nbr == nbr2);

  // Read PRGROM from backing store directly
  nesbus_read32(19'h40000, nbr32);
  assert(nbr32 == 32'h20068d48);
  nesbus_read32(19'h40004, nbr32);
  assert(nbr32 == 32'h8d00b1c8);
  nesbus_read32(19'h47fff, nbr32);
  assert(nbr32 == 32'h83708370);

  // nesbus read via CPU bus bordering CPU write
  #100 ;
  while (nes.nes_a != 16'h838C)
    @(posedge clk) ;
  // next two cycles will be read from and write to WRAM
  nesbus_read8(19'h18000, nbr);
  assert(nbr == 8'h48);

  // Write, read and verify backing store
  #100 ;
  nesbus_write32(19'h4a000, 32'h12345678);
  nesbus_read32(19'h4a000, nbr32);
  assert(nbr32 == 32'h12345678);
  nesbus_read32(19'h48010, nbr32);
  assert(nbr32 == 32'h1f0f0703);
  nesbus_read32(19'h48014, nbr32);
  assert(nbr32 == 32'h1fff7f3f);

  #100 ;
  $finish();
end

nes nes
  (
   .CLK(clk),
   .EXT_RES(1'b0),
   .NES_nRES(nes_nres),

   .ARESETn(!res),
   .AWADDR(awaddr),
   .AWVALID(awvalid),
   .AWREADY(awready),
   .WDATA(wdata),
   .WREADY(wready),
   .WSTRB(wstrb),
   .WVALID(wvalid),
   .BRESP(bresp),
   .BVALID(bvalid),
   .BREADY(bready),
   .ARADDR(araddr),
   .ARVALID(arvalid),
   .ARREADY(arready),
   .RRESP(),
   .RVALID(rvalid),
   .RREADY(rready),
   .RDATA(rdata)
   );

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s nesbus_tb -o nesbus_tb.vvp -f nes_tb.files nesbus_tb.v && ./nesbus_tb.vvp"
// End:
