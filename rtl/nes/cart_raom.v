// Emulate all of a cartridge's RAM and ROM.

module cart_raom #(parameter BS_AM = 15,
                   parameter BS_BANKS = 10,
                   parameter BS_BANK_AM = 11,
                   parameter PRG_AM = 14,
                   parameter CHR_AM = 8,
                   parameter SBB_AL = 14)
  (
   // Clocks / resets
   input                 CLK,
   input                 CTRL_nRES,

   // Debug
   output reg            DBG_PRGRAM_NES_WRITE,

   // Configuration
   input [4:0]           CFG_PRG_ROM_ADDR_MAX,
   input [4:0]           CFG_PRG_RAM_ADDR_MAX,
   input [4:0]           CFG_CHR_ADDR_MAX,

   // MMU translation table control interface
   input [7:0]           MMU_TT_IDX,
   input [31:0]          MMU_TT_DIN,
   output [31:0]         MMU_TT_DOUT,
   input                 MMU_TT_WE,

   // Backing store read interface
   input [BS_AM:2]       BS_ARADDR,
   input                 BS_ARVALID,
   output [31:0]         BS_RDATA,
   input                 BS_RREADY,
   output reg            BS_RVALID,

   // Backing store write interface
   input [BS_AM:2]       BS_AWADDR,
   input [3:0]           BS_WSTRB,
   input [31:0]          BS_WDATA,
   input                 BS_WVALID,
   output reg            BS_BVALID,

   // APU (for PRG) memory interface
   input [7:0]           PRG_D_I,
   output reg [7:0]      PRG_D_O,
   output reg            PRG_D_OE,
   input [PRG_AM:0]      PRG_A,
   input [PRG_AM:SBB_AL] PRG_SBBA0,
   input [PRG_AM:SBB_AL] PRG_SBBA1,
   output [1:0]          PRG_READY,

   input                 PRG_RnW,
   input                 PRG_ROM_EN,
   input                 PRG_RAM_EN,

   // PPU (for CHR) memory interface
   input [7:0]           CHR_D_I,
   output reg [7:0]      CHR_D_O,
   output reg            CHR_D_OE,
   input [CHR_AM:0]      CHR_A,
   input [CHR_AM:SBB_AL] CHR_SBBA0,
   input [CHR_AM:SBB_AL] CHR_SBBA1,
   input [CHR_AM:SBB_AL] CHR_SBBA2,
   input [CHR_AM:SBB_AL] CHR_SBBA3,
   input [CHR_AM:SBB_AL] CHR_SBBA4,
   input [CHR_AM:SBB_AL] CHR_SBBA5,
   output [5:0]          CHR_READY,

   input                 CHR_RnW,
   input                 CHR_EN
   );

wire      raom_we;

reg       bsri_rdata_req;

//////////////////////////////////////////////////////////////////////
// Cartridge backing store
//
// RAM/ROM contents are stored here, written by the NES (if RAM) or
// nesbus master.
//
// Store has two read/write ports. Both NES PPU (PRG) and APU (CHR)
// will share one port, while nesbus will use the other.

localparam BS_WIDTH = 32;
localparam BS_COLS = 4;

wire [BS_WIDTH-1:0] bs_rdata_a, bs_rdata_b;
wire [BS_WIDTH-1:0] bs_wdata_a, bs_wdata_b;
wire [BS_AM-2:0]    bs_addr_a, bs_addr_b;
wire [BS_COLS-1:0]  bs_we_a, bs_we_b; // write enables
wire                bs_en_a, bs_en_b; // port enables

cart_bs #(.BANKS(BS_BANKS), .WIDTH(BS_WIDTH), .AM(BS_AM-2),
          .BANK_AM(BS_BANK_AM-2), .COLS(BS_COLS)) bs
  (
   .CLK(CLK),

   .EN_A(bs_en_a),
   .RDATA_A(bs_rdata_a),
   .WDATA_A(bs_wdata_a),
   .ADDR_A(bs_addr_a),
   .WE_A(bs_we_a),

   .EN_B(bs_en_b),
   .RDATA_B(bs_rdata_b),
   .WDATA_B(bs_wdata_b),
   .ADDR_B(bs_addr_b),
   .WE_B(bs_we_b)
   );


//////////////////////////////////////////////////////////////////////
// NES bus arbitration
//
// Merge accesses for PRG and CHR. CHR has priority access.

wire [5:0]    nes_bop;

reg [BS_AM:0] nes_a;
reg [7:0]     nes_d_i;
reg [1:0]     nes_arb_bid_hold;

cart_arb #(.PRG_AM(PRG_AM), .CHR_AM(CHR_AM)) arb
  (
   .CLK(CLK),
   .PRG_A(PRG_A),
   .PRG_RnW(PRG_RnW),
   .PRG_ROM_EN(PRG_ROM_EN),
   .PRG_RAM_EN(PRG_RAM_EN),
   .CHR_A(CHR_A),
   .CHR_RnW(CHR_RnW),
   .CHR_EN(CHR_EN),
   .BOP(nes_bop)
   );

`include "cart_bop.vh"

wire [1:0] nes_arb_bid = get_bop_bid(nes_bop);
wire [1:0] nes_arb_cid = get_bop_cid(nes_bop);

always @* begin
  nes_a = {BS_AM+1{1'bx}};
  nes_d_i = {8{1'bx}};

  case (nes_arb_bid)
    BID_PRG: begin
      nes_a = PRG_A;
      nes_d_i = PRG_D_I;
    end
    BID_CHR: begin
      nes_a = CHR_A;
      nes_d_i = CHR_D_I;
    end
    default: ;
  endcase
end

assign bs_en_a = nes_arb_bid != BID_NONE;

always @(posedge CLK)
  nes_arb_bid_hold <= nes_arb_bid;


//////////////////////////////////////////////////////////////////////
// Address masking
//
// To emulate a RAM/ROM that's smaller than the maximum, with proper
// aliasing, mask the system address (A) by the configured address
// length.

reg [BS_AM:0] raom_a_en [1:CID_MAX];

wire [BS_AM:0] raom_a;

function [BS_AM:0] addr_mask(input [4:0] addr_max);
  addr_mask = {BS_AM+1{1'b1}} >> ((BS_AM > addr_max) ? (BS_AM - addr_max) : 0);
endfunction

always @(posedge CLK) begin
  raom_a_en[CID_PRG_ROM] <= addr_mask(CFG_PRG_ROM_ADDR_MAX);
  raom_a_en[CID_PRG_RAM] <= addr_mask(CFG_PRG_RAM_ADDR_MAX);
  raom_a_en[CID_CHR] <= addr_mask(CFG_CHR_ADDR_MAX);
end

assign raom_a = nes_a & raom_a_en[nes_arb_cid];


//////////////////////////////////////////////////////////////////////
// NES -> backing store memory translation

localparam MMU_BAM = BS_AM - BS_BANK_AM - 1;

wire [BS_AM:0] nrwi_bs_a;
wire [BS_AM:0] mmu_prg_sbba [0:1];
wire [BS_AM:0] mmu_chr_sbba [0:5];

assign mmu_prg_sbba[0] = {PRG_SBBA0, {SBB_AL{1'b1}}} & raom_a_en[CID_PRG_ROM];
assign mmu_prg_sbba[1] = {PRG_SBBA1, {SBB_AL{1'b1}}} & raom_a_en[CID_PRG_ROM];
assign mmu_chr_sbba[0] = {CHR_SBBA0, {SBB_AL{1'b1}}} & raom_a_en[CID_CHR];
assign mmu_chr_sbba[1] = {CHR_SBBA1, {SBB_AL{1'b1}}} & raom_a_en[CID_CHR];
assign mmu_chr_sbba[2] = {CHR_SBBA2, {SBB_AL{1'b1}}} & raom_a_en[CID_CHR];
assign mmu_chr_sbba[3] = {CHR_SBBA3, {SBB_AL{1'b1}}} & raom_a_en[CID_CHR];
assign mmu_chr_sbba[4] = {CHR_SBBA4, {SBB_AL{1'b1}}} & raom_a_en[CID_CHR];
assign mmu_chr_sbba[5] = {CHR_SBBA5, {SBB_AL{1'b1}}} & raom_a_en[CID_CHR];

cart_mmu #(.BAM(MMU_BAM), .BANKS(BS_BANKS)) mmu
  (
   .CLK(CLK),
   .nRES(CTRL_nRES),

   .TT_IDX(MMU_TT_IDX),
   .TT_DIN(MMU_TT_DIN),
   .TT_DOUT(MMU_TT_DOUT),
   .TT_WE(MMU_TT_WE),

   // Port 0: NES bus, post-arbitration
   .P0_BOP(nes_bop),
   .P0_RAOM_BA(raom_a[BS_AM:BS_BANK_AM+1]),
   .P0_BS_BA_VALID(),
   .P0_BS_BA(nrwi_bs_a[BS_AM:BS_BANK_AM+1]),

   // Ports 1-2: PRG switchable banks (for swap check)
   .P1_BOP(make_bop(BID_PRG, CID_PRG_ROM, OP_RD)),
   .P1_RAOM_BA(mmu_prg_sbba[0][BS_AM:BS_BANK_AM+1]),
   .P1_BS_BA_VALID(PRG_READY[0]),
   .P1_BS_BA(),

   .P2_BOP(make_bop(BID_PRG, CID_PRG_ROM, OP_RD)),
   .P2_RAOM_BA(mmu_prg_sbba[1][BS_AM:BS_BANK_AM+1]),
   .P2_BS_BA_VALID(PRG_READY[1]),
   .P2_BS_BA(),

   // Ports 2-8: CHR switchable banks (for swap check)
   .P3_BOP(make_bop(BID_CHR, CID_CHR, OP_RD)),
   .P3_RAOM_BA(mmu_chr_sbba[0][BS_AM:BS_BANK_AM+1]),
   .P3_BS_BA_VALID(CHR_READY[0]),
   .P3_BS_BA(),

   .P4_BOP(make_bop(BID_CHR, CID_CHR, OP_RD)),
   .P4_RAOM_BA(mmu_chr_sbba[1][BS_AM:BS_BANK_AM+1]),
   .P4_BS_BA_VALID(CHR_READY[1]),
   .P4_BS_BA(),

   .P5_BOP(make_bop(BID_CHR, CID_CHR, OP_RD)),
   .P5_RAOM_BA(mmu_chr_sbba[2][BS_AM:BS_BANK_AM+1]),
   .P5_BS_BA_VALID(CHR_READY[2]),
   .P5_BS_BA(),

   .P6_BOP(make_bop(BID_CHR, CID_CHR, OP_RD)),
   .P6_RAOM_BA(mmu_chr_sbba[3][BS_AM:BS_BANK_AM+1]),
   .P6_BS_BA_VALID(CHR_READY[3]),
   .P6_BS_BA(),

   .P7_BOP(make_bop(BID_CHR, CID_CHR, OP_RD)),
   .P7_RAOM_BA(mmu_chr_sbba[4][BS_AM:BS_BANK_AM+1]),
   .P7_BS_BA_VALID(CHR_READY[4]),
   .P7_BS_BA(),

   .P8_BOP(make_bop(BID_CHR, CID_CHR, OP_RD)),
   .P8_RAOM_BA(mmu_chr_sbba[5][BS_AM:BS_BANK_AM+1]),
   .P8_BS_BA_VALID(CHR_READY[5]),
   .P8_BS_BA()
   );

assign nrwi_bs_a[BS_BANK_AM:0] = raom_a[BS_BANK_AM:0];
assign bs_addr_a = nrwi_bs_a[BS_AM:2];


//////////////////////////////////////////////////////////////////////
// NES write interface
//
// The NES uses this to write the backing store.

reg [BS_WIDTH-1:0] nwi_data;
reg [BS_AM:2]      nwi_addr;
reg [3:0]          nwi_wstrb;

assign raom_we = get_bop_op(nes_bop) == OP_WR;

always @* begin
  nwi_data = {BS_WIDTH{1'bx}};
  nwi_addr = {BS_AM-2+1{1'bx}};
  nwi_wstrb = 4'b0;
  if (raom_we) begin
    nwi_wstrb[nrwi_bs_a[1:0]] = 1'b1;
    nwi_addr[BS_AM:2] = nrwi_bs_a[BS_AM:2];
    nwi_data = {4{nes_d_i}};
  end
end

assign bs_wdata_a = nwi_data;
assign bs_we_a = nwi_wstrb;

always @(posedge CLK)
  DBG_PRGRAM_NES_WRITE <= nes_arb_cid == CID_PRG_RAM && raom_we;


//////////////////////////////////////////////////////////////////////
// NES read interface
//
// The NES uses this to read the backing store.

wire [BS_AM:2]       nri_addr;
wire [BS_WIDTH-1:0]  nri_data;

reg [1:0]            nri_addr_hold;
reg [1:0]            nri_arb_bid_hold;
reg [7:0]            nri_dout;
reg [7:0]            nri_prg_dout;
reg [7:0]            nri_chr_dout;

assign nri_addr = nrwi_bs_a[BS_AM:2];
assign nri_data = bs_rdata_a;

always @(posedge CLK) begin
  nri_addr_hold <= nrwi_bs_a[1:0];
end

always @* begin
  case (nri_addr_hold)
    2'b00: nri_dout = nri_data[07:00];
    2'b01: nri_dout = nri_data[15:08];
    2'b10: nri_dout = nri_data[23:16];
    2'b11: nri_dout = nri_data[31:24];
    default: nri_dout = 8'hxx;
  endcase
end

always @(posedge CLK) begin
  if (nes_arb_bid_hold == BID_PRG) begin
    nri_prg_dout <= nri_dout;
  end
  if (nes_arb_bid_hold == BID_CHR) begin
    nri_chr_dout <= nri_dout;
  end
end

always @* begin
  PRG_D_O = 8'hxx;
  PRG_D_OE = 1'b0;
  if ((PRG_ROM_EN | PRG_RAM_EN) & PRG_RnW) begin
    PRG_D_O = nri_prg_dout;
    PRG_D_OE = 1'b1;
  end
end

always @* begin
  CHR_D_O = 8'hxx;
  CHR_D_OE = 1'b0;
  if (CHR_EN & CHR_RnW) begin
    CHR_D_O = nri_chr_dout;
    CHR_D_OE = 1'b1;
  end
end


/////////////////////////////////////////////////////////////////////
// Backing store write interface
//
// The nesbus master uses this to write the backing store.

reg [BS_WIDTH-1:0] bswi_data;
reg [BS_AM:2]      bswi_addr;
reg [3:0]          bswi_wstrb;
reg                bswi_we;

always @* begin
  bswi_we = 1'b0;
  bswi_wstrb = 4'h0;
  bswi_addr = {BS_AM-2+1{1'bx}};
  bswi_data = {BS_WIDTH{1'bx}};

  if (!bsri_rdata_req && BS_WVALID && !BS_BVALID) begin
    bswi_we = 1'b1;
    bswi_wstrb = BS_WSTRB;
    bswi_addr = BS_AWADDR;
    bswi_data = BS_WDATA;
  end
end

assign bs_wdata_b = bswi_data;
assign bs_we_b = bswi_wstrb;

always @(posedge CLK)
  BS_BVALID <= bswi_we;


//////////////////////////////////////////////////////////////////////
// Backing store read interface
//
// The nesbus master uses this to read the backing store.

wire [BS_WIDTH-1:0]  bsri_data;
wire [BS_AM:2]       bsri_addr;

reg                  bsri_rdata_ack;

initial begin
  bsri_rdata_req = 1'b0;
  bsri_rdata_ack = 1'b0;
end

assign bsri_addr = BS_ARADDR[BS_AM:2];
assign bsri_data = bs_rdata_b;

// Reads have priority over writes.
assign bs_addr_b = bsri_rdata_req ? bsri_addr : bswi_addr;
assign bs_en_b = bsri_rdata_req || bswi_we;

always @(posedge CLK) begin
  if (!bsri_rdata_ack)
    bsri_rdata_ack <= bsri_rdata_req;
  else if (bsri_rdata_ack)
    bsri_rdata_ack <= BS_ARVALID && BS_RREADY;

  bsri_rdata_req <= BS_ARVALID && BS_RREADY && !BS_RVALID;

  BS_RVALID <= bsri_rdata_req && bsri_rdata_ack;
end

assign BS_RDATA = bsri_data;


endmodule
