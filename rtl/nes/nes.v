// NES system:
// - main logic board
// - external cartridge emulation
// - NES / PS7 bridge

module nes
  (
   output [31:0] DEBUG,
   input         CLK, // master clock input: 236250/11 KHz
   input         EXT_RES,
   output        NES_nRES,

   // Video output
   output        PPU_PCE,
   output        PPU_DE,
   output        PPU_HS,
   output        PPU_VS,
   output [7:0]  PPU_R,
   output [7:0]  PPU_G,
   output [7:0]  PPU_B,

   // Audio output
   input         APU_MCLK,
   output        APU_SCLK,
   output        APU_LRCLK,
   output        APU_SDOUT,
   output [15:0] APU_PDOUT,

   // AXI Lite slave bus interface
   input         ARESETn,
   input [31:0]  AWADDR,
   input         AWVALID,
   output        AWREADY,
   input [31:0]  WDATA,
   output        WREADY,
   input [3:0]   WSTRB,
   input         WVALID,
   output [1:0]  BRESP,
   output        BVALID,
   input         BREADY,
   input [31:0]  ARADDR,
   input         ARVALID,
   output        ARREADY,
   output [31:0] RDATA,
   output [1:0]  RRESP,
   output        RVALID,
   input         RREADY
   );

wire            cp1_posedge;
wire            cp2;
wire            cp2_posedge;
wire            bclk_posedge;
wire            bclk_negedge;
wire            cc, ncc;

reg [7:0]       nes_d, nes_d_d;
wire [7:0]      nes_d_o, nes_d_oe;
reg [15:0]      nes_a;
wire [15:0]     nes_a_o;
wire            nes_a_oe;
wire            nes_rw, nes_rw_o;
reg [7:0]       nes_bd, nes_bd_d;
wire [7:0]      nes_bd_o;
reg [13:0]      nes_ba;
wire [13:0]     nes_ba_o;
wire            nes_bd_oe, nes_ba_oe;
wire            nes_nbread, nes_nbwrite;
wire [1:0]      nes_inp;
wire [2:0]      nes_out;
wire            nes_hold;

wire [1:0]      ctlr1_i, ctlr2_i;
wire [2:0]      ctlr1_o, ctlr2_o;

wire            cart_hold_req;
wire [11:2]     cart_ctrl_addr;
wire [3:0]      cart_ctrl_wstrb;
wire [31:0]     cart_ctrl_din;
wire [31:0]     cart_ctrl_dout;
wire [7:0]      cart_d_o;
wire [15:0]     cart_a_o;
wire            cart_d_oe;
wire [7:0]      cart_bd_o;

wire            nes_hold_req, nes_hold_ack;
wire            nesbus_hold_req;

wire            nesbus_bread;
wire            nesbus_bwrite;
wire [7:0]      nesbus_d_o;
wire [15:0]     nesbus_a_o;
wire            nesbus_d_oe, nesbus_a_oe;
wire [7:0]      nesbus_bd_o;
wire [13:0]     nesbus_ba_o;
wire            nesbus_bd_oe, nesbus_ba_oe;
wire [4:0]      cpu_reg_sel;
wire [7:0]      cpu_reg_dout;
wire            icd_sys_enable;
wire            icd_sys_force_halt;
wire            icd_sys_resume;
wire            icd_sys_hold;
wire [7:0]      icd_match_enable;
wire [7:0]      icd_match_trigger;
wire [2:0]      icd_match_sel;
wire [3:0]      icd_match_reg_sel;
wire [3:0]      icd_match_reg_wstrb;
wire [31:0]     icd_match_reg_din;
wire [31:0]     icd_match_reg_dout;
wire [8:2]      ppu_mon_sel;
wire            ppu_mon_ready;
wire [31:0]     ppu_mon_dout;
wire            ppu_mon_valid;
wire [4:0]      apu_mon_sel;
wire            apu_mon_ready;
wire [7:0]      apu_mon_dout;
wire            apu_mon_valid;

wire [17:2]     cart_bs_araddr;
wire            cart_bs_arvalid;
wire [31:0]     cart_bs_rdata;
wire            cart_bs_rready;
wire            cart_bs_rvalid;
wire [17:2]     cart_bs_awaddr;
wire [3:0]      cart_bs_wstrb;
wire [31:0]     cart_bs_wdata;
wire            cart_bs_wvalid;
wire            cart_bs_bvalid;

wire [15:0]     apu_pdout;
wire            apu_pdclken;

//////////////////////////////////////////////////////////////////////
// CPU address / data bus

always @* begin
  nes_a = 16'hxxxx;
  case ({nesbus_a_oe, nes_a_oe})
    2'b10: nes_a = nesbus_a_o;
    2'b01: nes_a = nes_a_o;
    default: ;
  endcase
end

always @* begin
  nes_d = nes_d_d;
  case ({nesbus_d_oe, cart_d_oe, |nes_d_oe})
    3'b100: nes_d = nesbus_d_o;
    3'b010: nes_d = cart_d_o;
    3'b001: nes_d = (nes_d_o & nes_d_oe) | (nes_d_d & ~nes_d_oe);
    default: ;
  endcase
end

// Emulate bus capacitance
always @(posedge CLK)
  nes_d_d <= nes_d;

assign nes_rw = nes_rw_o && nesbus_rw_o;
assign NES_nRES = nes_nres;

//////////////////////////////////////////////////////////////////////
// PPU address / data bus

always @* begin
  nes_ba = 14'hxxxx;
  case ({nesbus_ba_oe, nes_ba_oe})
    2'b10: nes_ba = nesbus_ba_o;
    2'b01: nes_ba = {1'b0, nes_ba_o};
    default: ;
  endcase
end

always @* begin
  nes_bd = nes_bd_d;
  case ({nesbus_bd_oe, cart_bd_oe, nes_bd_oe})
    3'b100: nes_bd = nesbus_bd_o;
    3'b010: nes_bd = cart_bd_o;
    3'b001: nes_bd = nes_bd_o;
    default: ;
  endcase
end

// Emulate bus capacitance
always @(posedge CLK)
  nes_bd_d <= nes_bd;

assign nes_nbread = nes_brw_oe ? nes_nbread_o : ~nesbus_bread;
assign nes_nbwrite = nes_brw_oe ? nes_nbwrite_o : ~nesbus_bwrite;

//////////////////////////////////////////////////////////////////////
// NES main logic board

nes_mlb mlb
  (
   .CLK_RESET(clk_reset),
   .CLK(CLK),
   .nRES(nes_nres),
   .RES_COLD(res_cold),

   .CP1(cp1),
   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2(cp2),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .APU_CP1_POSEDGE(apu_cp1_posedge),
   .M2(m2),
   .M2_POSEDGE(m2_posedge),
   .M2_NEGEDGE(m2_negedge),
   .CC(cc),
   .nCC(ncc),

   .RW_I(nes_rw),
   .RW_O(nes_rw_o),
   .D_I(nes_d),
   .D_O(nes_d_o),
   .D_OE(nes_d_oe),
   .A_I(nes_a),
   .A_O(nes_a_o),
   .A_OE(nes_a_oe),
   .PRGROM_nOE(prgrom_noe),
   .nBREAD_I(nes_nbread),
   .nBREAD_O(nes_nbread_o),
   .nBWRITE_I(nes_nbwrite),
   .nBWRITE_O(nes_nbwrite_o),
   .BRW_OE(nes_brw_oe),
   .BD_I(nes_bd),
   .BD_O(nes_bd_o),
   .BD_OE(nes_bd_oe),
   .BA_I(nes_ba),
   .BA_O(nes_ba_o),
   .BA_OE(nes_ba_oe),
   .VRAM_A10(vram_a10),
   .VRAM_nCEO(vram_nceo),
   .VRAM_nCEI(vram_ncei),
   .CHAROM_nCE(charom_nce),
   .nIRQ(nes_nirq),

   .INP(nes_inp),
   .OUT(nes_out),
   .PPU_PCE(PPU_PCE),
   .PPU_DE(PPU_DE),
   .PPU_HS(PPU_HS),
   .PPU_VS(PPU_VS),
   .PPU_R(PPU_R),
   .PPU_G(PPU_G),
   .PPU_B(PPU_B),
   .APU_MCLK(APU_MCLK),
   .APU_PDCLKEN(apu_pdclken),
   .APU_PDOUT(apu_pdout),

   .CTLR1_I(ctlr1_i),
   .CTLR1_O(ctlr1_o),
   .CTLR2_I(ctlr2_i),
   .CTLR2_O(ctlr2_o),

   .SYS_HOLD_REQ(nes_hold_req),
   .SYS_HOLD_ACK(nes_hold_ack),
   .CPU_REG_SEL(cpu_reg_sel),
   .CPU_REG_DOUT(cpu_reg_dout),
   .ICD_SYS_ENABLE(icd_sys_enable),
   .ICD_SYS_FORCE_HALT(icd_sys_force_halt),
   .ICD_SYS_RESUME(icd_sys_resume),
   .ICD_SYS_HOLD(icd_sys_hold),
   .ICD_MATCH_ENABLE(icd_match_enable),
   .ICD_MATCH_TRIGGER(icd_match_trigger),
   .ICD_MATCH_SEL(icd_match_sel),
   .ICD_MATCH_REG_SEL(icd_match_reg_sel),
   .ICD_MATCH_REG_WSTRB(icd_match_reg_wstrb),
   .ICD_MATCH_REG_DIN(icd_match_reg_din),
   .ICD_MATCH_REG_DOUT(icd_match_reg_dout),
   .PPU_MON_SEL(ppu_mon_sel),
   .PPU_MON_READY(ppu_mon_ready),
   .PPU_MON_DOUT(ppu_mon_dout),
   .PPU_MON_VALID(ppu_mon_valid),
   .APU_MON_SEL(apu_mon_sel),
   .APU_MON_READY(apu_mon_ready),
   .APU_MON_DOUT(apu_mon_dout),
   .APU_MON_VALID(apu_mon_valid)
   );

//////////////////////////////////////////////////////////////////////
// External cartridge emulation

cart cart
  (
   .CLK(CLK),
   .nRES(nes_nres),
   .RES_COLD(res_cold),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2(cp2),
   .CP2_NEGEDGE(cp2_negedge),
   .M2_NEGEDGE(m2_negedge),

   .RW(nes_rw),
   .D_I(nes_d),
   .D_O(cart_d_o),
   .D_OE(cart_d_oe),
   .A(nes_a[14:0] ),
   .PRGROM_nOE(prgrom_noe),
   .nBREAD(nes_nbread),
   .nBWRITE(nes_nbwrite),
   .BD_I(nes_bd),
   .BD_O(cart_bd_o),
   .BD_OE(cart_bd_oe),
   .BA(nes_ba[12:0]),
   .VRAM_A10(vram_a10),
   .VRAM_nCEO(vram_nceo),
   .VRAM_nCEI(vram_ncei),
   .CHAROM_nCE(charom_nce),
   .nIRQ(nes_nirq),

   .SYS_HOLD_REQ(cart_hold_req),
   .SYS_HOLD_ACK(nes_hold_ack),

   .CTRL_nRES(cart_nres),
   .CTRL_ADDR(cart_ctrl_addr),
   .CTRL_WSTRB(cart_ctrl_wstrb),
   .CTRL_DIN(cart_ctrl_din),
   .CTRL_DOUT(cart_ctrl_dout),

   .BS_ARADDR(cart_bs_araddr),
   .BS_ARVALID(cart_bs_arvalid),
   .BS_RDATA(cart_bs_rdata),
   .BS_RREADY(cart_bs_rready),
   .BS_RVALID(cart_bs_rvalid),
   .BS_AWADDR(cart_bs_awaddr),
   .BS_WSTRB(cart_bs_wstrb),
   .BS_WDATA(cart_bs_wdata),
   .BS_WVALID(cart_bs_wvalid),
   .BS_BVALID(cart_bs_bvalid)
   );

//////////////////////////////////////////////////////////////////////
// NES / PS7 bridge

nesbus nesbus
  (
   .CLK(CLK),
   .CP1_POSEDGE(cp1_posedge),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .APU_CP1_POSEDGE(apu_cp1_posedge),
   .CC(cc),
   .nCC(ncc),

   .ARESETn(ARESETn),
   .AWADDR(AWADDR),
   .AWVALID(AWVALID),
   .AWREADY(AWREADY),
   .WDATA(WDATA),
   .WREADY(WREADY),
   .WSTRB(WSTRB),
   .WVALID(WVALID),
   .BRESP(BRESP),
   .BVALID(BVALID),
   .BREADY(BREADY),
   .ARADDR(ARADDR),
   .ARVALID(ARVALID),
   .ARREADY(ARREADY),
   .RDATA(RDATA),
   .RRESP(RRESP),
   .RVALID(RVALID),
   .RREADY(RREADY),

   .CTLR1_I(ctlr1_i),
   .CTLR1_O(ctlr1_o),
   .CTLR2_I(ctlr2_i),
   .CTLR2_O(ctlr2_o),

   .EXT_RES(EXT_RES),
   .CLK_RESET(clk_reset),
   .NES_nRES(nes_nres),
   .CART_nRES(cart_nres),
   .RES_COLD(res_cold),
   .SYS_HOLD_REQ(nesbus_hold_req),
   .SYS_HOLD_ACK(nes_hold_ack),

   .RW_O(nesbus_rw_o),
   .D_I(nes_d),
   .D_O(nesbus_d_o),
   .D_OE(nesbus_d_oe),
   .A_O(nesbus_a_o),
   .A_OE(nesbus_a_oe),
   
   .BREAD(nesbus_bread),
   .BWRITE(nesbus_bwrite),
   .BD_I(nes_bd),
   .BD_O(nesbus_bd_o),
   .BD_OE(nesbus_bd_oe),
   .BA_O(nesbus_ba_o),
   .BA_OE(nesbus_ba_oe),

   .CPU_REG_SEL(cpu_reg_sel),
   .CPU_REG_DOUT(cpu_reg_dout),
   .ICD_SYS_ENABLE(icd_sys_enable),
   .ICD_SYS_FORCE_HALT(icd_sys_force_halt),
   .ICD_SYS_RESUME(icd_sys_resume),
   .ICD_SYS_HOLD(icd_sys_hold),
   .ICD_MATCH_ENABLE(icd_match_enable),
   .ICD_MATCH_TRIGGER(icd_match_trigger),
   .ICD_MATCH_SEL(icd_match_sel),
   .ICD_MATCH_REG_SEL(icd_match_reg_sel),
   .ICD_MATCH_REG_WSTRB(icd_match_reg_wstrb),
   .ICD_MATCH_REG_DIN(icd_match_reg_din),
   .ICD_MATCH_REG_DOUT(icd_match_reg_dout),

   .PPU_MON_SEL(ppu_mon_sel),
   .PPU_MON_READY(ppu_mon_ready),
   .PPU_MON_DOUT(ppu_mon_dout),
   .PPU_MON_VALID(ppu_mon_valid),

   .APU_MON_SEL(apu_mon_sel),
   .APU_MON_READY(apu_mon_ready),
   .APU_MON_DOUT(apu_mon_dout),
   .APU_MON_VALID(apu_mon_valid),

   .CART_CTRL_ADDR(cart_ctrl_addr),
   .CART_CTRL_WSTRB(cart_ctrl_wstrb),
   .CART_CTRL_DIN(cart_ctrl_din),
   .CART_CTRL_DOUT(cart_ctrl_dout),

   .CART_BS_ARADDR(cart_bs_araddr),
   .CART_BS_ARVALID(cart_bs_arvalid),
   .CART_BS_RDATA(cart_bs_rdata),
   .CART_BS_RREADY(cart_bs_rready),
   .CART_BS_RVALID(cart_bs_rvalid),
   .CART_BS_AWADDR(cart_bs_awaddr),
   .CART_BS_WSTRB(cart_bs_wstrb),
   .CART_BS_WDATA(cart_bs_wdata),
   .CART_BS_WVALID(cart_bs_wvalid),
   .CART_BS_BVALID(cart_bs_bvalid)
   );

// Merge hold requests.
assign nes_hold_req = nesbus_hold_req | icd_sys_hold | cart_hold_req;

//////////////////////////////////////////////////////////////////////
// I2S audio output

i2s_serializer ser
  (
   .MCLK(APU_MCLK),
   .OCLKEN(apu_pdclken),
   .PDIN(apu_pdout),
   .SCLK(APU_SCLK),
   .LRCLK(APU_LRCLK),
   .SDOUT(APU_SDOUT)
   );

assign APU_PDOUT = apu_pdout;

assign DEBUG[15:8] = cart.mmc3_6.ic;
assign DEBUG[7] = icd_match_trigger[1];
assign DEBUG[6] = nes_nirq;
assign DEBUG[5] = cart.mmc3_6.irq_clear;
assign DEBUG[4] = cart.mmc3_6.irq_reload;
assign DEBUG[3] = cart.mmc3_6.ba12_flt_posedge;
assign DEBUG[2] = mlb.ppu.col == 0;
assign DEBUG[1] = mlb.ppu.row == 0;
assign DEBUG[0] = cc;

endmodule
