module cart
  (
   // Clocks / resets
   input             CLK,
   input             nRES, // MLB reset
   input             RES_COLD,
   input             CP1_NEGEDGE,
   input             CP2,
   input             CP2_NEGEDGE,
   input             M2_NEGEDGE,

   // Cartridge port
   input             RW, // pin 14
   input [7:0]       D_I,
   output [7:0]      D_O,
   output            D_OE,
   input [14:0]      A,
   input             PRGROM_nOE, // pin 50
   input             nBREAD, // pin 21
   input             nBWRITE, // pin 56
   input [7:0]       BD_I,
   output [7:0]      BD_O,
   output            BD_OE,
   input [12:0]      BA,
   output reg        VRAM_A10, // pin 22
   input             VRAM_nCEO, // pin 58
   output            VRAM_nCEI, // pin 57
   input             CHAROM_nCE, // pin 65
   output reg        nIRQ, // pin 15

   // System control
   output            SYS_HOLD_REQ,
   input             SYS_HOLD_ACK,

   // Control register interface
   input             CTRL_nRES,
   input [11:2]      CTRL_ADDR, // control interface
   input [3:0]       CTRL_WSTRB,
   input [31:0]      CTRL_DIN,
   output reg [31:0] CTRL_DOUT,

   // Backing store read/write interface
   input [17:2]      BS_ARADDR,
   input             BS_ARVALID,
   output [31:0]     BS_RDATA,
   input             BS_RREADY,
   output            BS_RVALID,
   input [17:2]      BS_AWADDR,
   input [3:0]       BS_WSTRB,
   input [31:0]      BS_WDATA,
   input             BS_WVALID,
   output            BS_BVALID
   );

// Lowest bit of switchable bank base address (eg, MMC1 PRG0).
localparam SBB_AL = 12;

reg [2:0] mapper_sel;
reg [1:0] mirroring;
reg       snrom;
reg       serom;                // SEROM, SHROM, SH1ROM
reg       namcot;               // Namcot 108 family

//////////////////////////////////////////////////////////////////////
// Cartridge / expansion slot interface

assign VRAM_nCEI = VRAM_nCEO;


//////////////////////////////////////////////////////////////////////
// MMC1 mapper

wire [17:14] mmc1_prga, mmc1_prg0;
wire [16:12] mmc1_chra, mmc1_chr0, mmc1_chr1;
wire         mmc1_va10, mmc1_prgcs, mmc1_wramcs;

reg [17:0]   mmc1_prg_sbba0;
reg [16:0]   mmc1_chr_sbba0, mmc1_chr_sbba1;

mmc1 mmc1 
  (
   .CLK(CLK),
   .nRES(nRES),
   .RES_COLD(RES_COLD),
   .CP1_NEGEDGE(CP1_NEGEDGE),
   .CP2(CP2),
   .CP2_NEGEDGE(CP2_NEGEDGE),

   .RW(RW),
   .D(D_I),
   .A(A[14:13]),
   .PRGROM_nOE(PRGROM_nOE),
   .nBREAD(nBREAD),
   .nBWRITE(nBWRITE),
   .BA(BA[12:10]),
   .CHAROM_nCE(CHAROM_nCE),

   .PRG0(mmc1_prg0),
   .CHR0(mmc1_chr0),
   .CHR1(mmc1_chr1),

   .PRG_A(mmc1_prga),
   .CHR_A(mmc1_chra),
   .CIRAM_A10(mmc1_va10),
   .PRG_CS(mmc1_prgcs),
   .WRAM_CS(mmc1_wramcs)
   );

initial begin
  mmc1_prg_sbba0 = ~0;
  mmc1_chr_sbba0 = ~0;
  mmc1_chr_sbba1 = ~0;
end

always @* begin
  mmc1_prg_sbba0[17:14] = mmc1_prg0;
  mmc1_chr_sbba0[16:12] = mmc1_chr0;
  mmc1_chr_sbba1[16:12] = mmc1_chr1;
end


//////////////////////////////////////////////////////////////////////
// MMC3/6 mapper

wire [18:13] mmc3_6_prga, mmc3_6_prg0, mmc3_6_prg1;
wire [17:10] mmc3_6_chra, mmc3_6_chr0, mmc3_6_chr1, mmc3_6_chr2,
             mmc3_6_chr3, mmc3_6_chr4, mmc3_6_chr5;
wire         mmc3_6_va10, mmc3_6_prgcs, mmc3_6_wramcs;
wire         mmc3_6_nirq;

reg [18:0]   mmc3_6_prg_sbba0, mmc3_6_prg_sbba1;
reg [17:0]   mmc3_6_chr_sbba0, mmc3_6_chr_sbba1,
             mmc3_6_chr_sbba2, mmc3_6_chr_sbba3,
             mmc3_6_chr_sbba4, mmc3_6_chr_sbba5;

mmc3_6 mmc3_6
  (
   .CLK(CLK),
   .nRES(nRES),
   .RES_COLD(RES_COLD),
   .CP1_NEGEDGE(CP1_NEGEDGE),
   .CP2(CP2),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .M2_NEGEDGE(M2_NEGEDGE),

   .HOLD(SYS_HOLD_ACK),

   .RW(RW),
   .D(D_I),
   .A(A[14:13]),
   .A0(A[0]),
   .PRGROM_nOE(PRGROM_nOE),
   .nBREAD(nBREAD),
   .nBWRITE(nBWRITE),
   .BA(BA[12:10]),
   .CHAROM_nCE(CHAROM_nCE),
   .nIRQ(mmc3_6_nirq),

   .PRG0(mmc3_6_prg0),
   .PRG1(mmc3_6_prg1),
   .CHR0(mmc3_6_chr0),
   .CHR1(mmc3_6_chr1),
   .CHR2(mmc3_6_chr2),
   .CHR3(mmc3_6_chr3),
   .CHR4(mmc3_6_chr4),
   .CHR5(mmc3_6_chr5),

   .PRG_A(mmc3_6_prga),
   .CHR_A(mmc3_6_chra),
   .CIRAM_A10(mmc3_6_va10),
   .PRG_CS(mmc3_6_prgcs),
   .WRAM_CS(mmc3_6_wramcs)
   );

initial begin
  mmc3_6_prg_sbba0 = ~0;
  mmc3_6_prg_sbba1 = ~0;
  mmc3_6_chr_sbba0 = ~0;
  mmc3_6_chr_sbba1 = ~0;
  mmc3_6_chr_sbba2 = ~0;
  mmc3_6_chr_sbba3 = ~0;
  mmc3_6_chr_sbba4 = ~0;
  mmc3_6_chr_sbba5 = ~0;
end

always @* begin
  mmc3_6_prg_sbba0[18:13] = mmc3_6_prg0;
  mmc3_6_prg_sbba1[18:13] = mmc3_6_prg1;
  mmc3_6_chr_sbba0[17:10] = mmc3_6_chr0;
  mmc3_6_chr_sbba1[17:10] = mmc3_6_chr1;
  mmc3_6_chr_sbba2[17:10] = mmc3_6_chr2;
  mmc3_6_chr_sbba3[17:10] = mmc3_6_chr3;
  mmc3_6_chr_sbba4[17:10] = mmc3_6_chr4;
  mmc3_6_chr_sbba5[17:10] = mmc3_6_chr5;
end


//////////////////////////////////////////////////////////////////////
// UxROM mapper
//
// Reference: https://wiki.nesdev.org/w/index.php?title=UxROM
//
// Banks:
//   CPU $8000-$BFFF: (16K) switchable window
//   CPU $C000-$FFFF: (16K) fixed window of last 16K
//
// Bank select register:
//   Address: any address in PRGROM
//   Bits 7-0: bank select (A21-A14)

reg [21:0] uxrom_prga;
reg [7:0]  uxrom_bank;
reg [21:0] uxrom_prg_sbba0;

initial
  uxrom_prg_sbba0 = ~0;

always @(posedge CLK) if (CP2_NEGEDGE) begin
  if (~PRGROM_nOE & ~RW)
    uxrom_bank <= D_I;
end

always @* begin
  uxrom_prga[21:14] = A[14] ? 8'hff : uxrom_bank;
  uxrom_prga[13:0] = A[13:0];
end

always @*
  uxrom_prg_sbba0[21:14] = uxrom_bank;


//////////////////////////////////////////////////////////////////////
// PRGROM, CHARAM / CHAROM, PRG RAM / external WRAM

localparam PRG_AM = 18;      // 512K max.
localparam CHR_AM = 17;      // 256K max.
localparam BS_BANKS = 52;
localparam BS_BANK_AM = 11;     // 2 ^ (11 + 1) = 4K * 8bit

reg [PRG_AM:0] prg_a;
reg [CHR_AM:0] chr_a;

reg [PRG_AM:SBB_AL] prg_sbba0, prg_sbba1;
reg [CHR_AM:SBB_AL] chr_sbba0, chr_sbba1, chr_sbba2,
                    chr_sbba3, chr_sbba4, chr_sbba5;

reg [4:0]      prg_rom_addr_max;
reg [4:0]      prg_ram_addr_max;
reg [4:0]      chr_addr_max;
reg            chr_read_only;

reg            prgrom_nce;
reg            prgram_cs;
reg            prgram_nce;

wire [1:0]     prg_ready;
wire           prg_rnw = RW;
wire           prg_rom_en = ~prgrom_nce;
wire           prg_ram_en = ~(prgram_nce | ~prgram_cs);

wire [5:0]     chr_ready;
wire           chr_re = ~(CHAROM_nCE | nBREAD);
wire           chr_we = ~(CHAROM_nCE | (nBWRITE | chr_read_only));
wire           chr_rnw = nBWRITE | chr_read_only;
wire           chr_en = chr_re | chr_we;

reg [7:0]      mmu_tt_idx;
reg [31:0]     mmu_tt_din;
wire [31:0]    mmu_tt_dout;
reg            mmu_tt_we;

cart_raom #(.BS_AM(17), .BS_BANKS(BS_BANKS), .BS_BANK_AM(BS_BANK_AM),
            .PRG_AM(PRG_AM), .CHR_AM(CHR_AM), .SBB_AL(SBB_AL)
            ) raom
  (
   .CLK(CLK),
   .CTRL_nRES(CTRL_nRES),

   .DBG_PRGRAM_NES_WRITE(prgram_nes_write),

   .CFG_PRG_ROM_ADDR_MAX(prg_rom_addr_max),
   .CFG_PRG_RAM_ADDR_MAX(prg_ram_addr_max),
   .CFG_CHR_ADDR_MAX(chr_addr_max),

   .MMU_TT_IDX(mmu_tt_idx),
   .MMU_TT_DIN(mmu_tt_din),
   .MMU_TT_DOUT(mmu_tt_dout),
   .MMU_TT_WE(mmu_tt_we),

   .BS_ARADDR(BS_ARADDR),
   .BS_ARVALID(BS_ARVALID),
   .BS_RDATA(BS_RDATA),
   .BS_RREADY(BS_RREADY),
   .BS_RVALID(BS_RVALID),

   .BS_AWADDR(BS_AWADDR),
   .BS_WSTRB(BS_WSTRB),
   .BS_WDATA(BS_WDATA),
   .BS_WVALID(BS_WVALID),
   .BS_BVALID(BS_BVALID),

   .PRG_D_I(D_I),
   .PRG_D_O(D_O),
   .PRG_D_OE(D_OE),
   .PRG_A(prg_a),
   .PRG_SBBA0(prg_sbba0),
   .PRG_SBBA1(prg_sbba1),
   .PRG_READY(prg_ready),

   .PRG_RnW(prg_rnw),
   .PRG_ROM_EN(prg_rom_en),
   .PRG_RAM_EN(prg_ram_en),

   .CHR_D_I(BD_I),
   .CHR_D_O(BD_O),
   .CHR_D_OE(BD_OE),
   .CHR_A(chr_a),
   .CHR_SBBA0(chr_sbba0),
   .CHR_SBBA1(chr_sbba1),
   .CHR_SBBA2(chr_sbba2),
   .CHR_SBBA3(chr_sbba3),
   .CHR_SBBA4(chr_sbba4),
   .CHR_SBBA5(chr_sbba5),
   .CHR_READY(chr_ready),

   .CHR_RnW(chr_rnw),
   .CHR_EN(chr_en)
   );


//////////////////////////////////////////////////////////////////////
// PRG RAM / external WRAM write monitor

reg [31:0]     prgram_write_cnt;
reg        prgram_write_clear;

always @(posedge CLK) begin
  if (!nRES) begin
    prgram_write_cnt <= 1'd0;
  end
  else begin
    if (prgram_write_clear) begin
      prgram_write_cnt <= prgram_nes_write;
    end
    else if (prgram_nes_write) begin
      if (prgram_write_cnt + 1'd1 != 32'd0)
        prgram_write_cnt <= prgram_write_cnt + 1'd1;
    end
  end
end

//////////////////////////////////////////////////////////////////////
// Control register read interface

always @* begin                 //``REGION_REGS CART_CTRL
  CTRL_DOUT = 32'h0000;
  case (CTRL_ADDR)
    10'h000: begin              //``REG CART_BS
      CTRL_DOUT[7:0] = BS_BANKS;
      CTRL_DOUT[15:8] = BS_BANK_AM;
    end
    10'h001: begin              //``REG MMU_CFG
      CTRL_DOUT[7:0] = SBB_AL;
    end
    10'h010: begin              //``REG CHIP_CFG
      CTRL_DOUT[4:0] = prg_rom_addr_max;
      CTRL_DOUT[12:8] = chr_addr_max;
      CTRL_DOUT[15] = chr_read_only;
      CTRL_DOUT[20:16] = prg_ram_addr_max;
    end
    10'h014: begin              //``REG PRG_RAM_WRITE_CNT
      CTRL_DOUT[31:0] = prgram_write_cnt; //``NO_FIELD
    end
    10'h018: begin              //``REG SWAP0
      CTRL_DOUT[0] = SYS_HOLD_REQ;
      CTRL_DOUT[15:8] = prg_ready;
      CTRL_DOUT[23:16] = chr_ready;
    end
    10'h019: begin              //``REG SWAP1
      CTRL_DOUT[7:0] = prg_sbba0;
      CTRL_DOUT[15:8] = prg_sbba1;
    end
    10'h01a: begin              //``REG SWAP2
      CTRL_DOUT[7:0] = chr_sbba0;
      CTRL_DOUT[15:8] = chr_sbba1;
      CTRL_DOUT[23:16] = chr_sbba2;
      CTRL_DOUT[31:24] = chr_sbba3;
    end
    10'h01b: begin              //``REG SWAP3
      CTRL_DOUT[7:0] = chr_sbba4;
      CTRL_DOUT[15:8] = chr_sbba5;
    end
    10'h020: begin
      CTRL_DOUT[1:0] = mirroring;
    end
    10'h028: begin
      CTRL_DOUT[2:0] = mapper_sel;
    end
    10'h029: begin              //``REG MMC1_CFG
      CTRL_DOUT[0] = snrom;
      CTRL_DOUT[1] = serom;
    end
    10'h02a: begin              //``REG MMC3_6_CFG
      CTRL_DOUT[0] = namcot;
    end
    10'h030: begin
      CTRL_DOUT[7:0] = mmu_tt_idx;
    end
    10'h031: begin              //``REG MMU_TT_DAT
      CTRL_DOUT = mmu_tt_dout;
    end
    default: ;
  endcase
end                             //``REG_END


//////////////////////////////////////////////////////////////////////
// Control register write interface

always @(posedge CLK) begin
  mmu_tt_we <= 1'b0;
  prgram_write_clear <= 1'b0;
  if (!CTRL_nRES) begin
    prg_rom_addr_max <= 5'd14;   // 32KB
    chr_addr_max <= 5'd12;      // 8KB
    chr_read_only <= 1'b1;
    prg_ram_addr_max <= 5'd12;   // 8KB
    mirroring <= 2'b10;
    mapper_sel <= 3'd0;         // BYPASS
    snrom <= 1'b0;
    serom <= 1'b0;
    namcot <= 1'b0;
    mmu_tt_idx <= 8'h0;
    mmu_tt_din <= 32'h0;
  end
  else
    case (CTRL_ADDR)
      10'h010: begin
        if (CTRL_WSTRB[0])
          prg_rom_addr_max <= CTRL_DIN[4:0];
        if (CTRL_WSTRB[1]) begin
          chr_addr_max <= CTRL_DIN[12:8];
          chr_read_only <= CTRL_DIN[15];
        end
        if (CTRL_WSTRB[2])
          prg_ram_addr_max <= CTRL_DIN[20:16];
      end
      10'h014: begin
        if (CTRL_WSTRB[0])
          prgram_write_clear <= 1'b1;
      end
      10'h020: begin
        if (CTRL_WSTRB[0])
          mirroring <= CTRL_DIN[1:0];
      end
      10'h028: begin
        if (CTRL_WSTRB[0])
          mapper_sel <= CTRL_DIN[2:0];
      end
      10'h029: begin
        if (CTRL_WSTRB[0]) begin
          snrom <= CTRL_DIN[0];
          serom <= CTRL_DIN[1];
        end
      end
      10'h02a: begin
        if (CTRL_WSTRB[0]) begin
          namcot <= CTRL_DIN[0];
        end
      end
      10'h030: begin
        if (CTRL_WSTRB[0])
          mmu_tt_idx <= CTRL_DIN[7:0];
      end
      10'h031: begin
        if (CTRL_WSTRB[0]) mmu_tt_din[07:00] <= CTRL_DIN[07:00];
        if (CTRL_WSTRB[1]) mmu_tt_din[15:08] <= CTRL_DIN[15:08];
        if (CTRL_WSTRB[2]) mmu_tt_din[23:16] <= CTRL_DIN[23:16];
        if (CTRL_WSTRB[3]) mmu_tt_din[31:24] <= CTRL_DIN[31:24];
        mmu_tt_we <= |CTRL_WSTRB;
      end
      default: ;
    endcase
end


//////////////////////////////////////////////////////////////////////
// Hard-wired mirroring
//
// Control VRAM_A10 cartridge pin
// - Single-screen lower/upper: tied to VCC or GND
// - Vertical mirroring (horizontal arrangement): tied to BA[10]
// - Horizontal mirroring (vertical arrangement): tied to BA[11]

reg hard_va10;

always @* begin
  case (mirroring)              //``VALS
    2'b00: hard_va10 = 1'b0;    //``VAL LOWER
    2'b01: hard_va10 = 1'b1;    //``VAL UPPER
    2'b10: hard_va10 = BA[10];  //``VAL VERT
    2'b11: hard_va10 = BA[11];  //``VAL HORIZ
    default: hard_va10 = 1'bx;
  endcase
end


//////////////////////////////////////////////////////////////////////
// Mapper MUX

always @* begin
  prg_sbba0 = {PRG_AM-SBB_AL+1{1'b1}};
  prg_sbba1 = {PRG_AM-SBB_AL+1{1'b1}};
  chr_sbba0 = {CHR_AM-SBB_AL+1{1'b1}};
  chr_sbba1 = {CHR_AM-SBB_AL+1{1'b1}};
  chr_sbba2 = {CHR_AM-SBB_AL+1{1'b1}};
  chr_sbba3 = {CHR_AM-SBB_AL+1{1'b1}};
  chr_sbba4 = {CHR_AM-SBB_AL+1{1'b1}};
  chr_sbba5 = {CHR_AM-SBB_AL+1{1'b1}};
  prg_a = A;
  chr_a = BA;
  VRAM_A10 = hard_va10;
  prgrom_nce = PRGROM_nOE | ~RW;
  prgram_nce = 1'b1;
  prgram_cs = 1'b0;
  nIRQ = 1'b1;

  case (mapper_sel)             //``VALS
    3'd0: ;                     //``VAL BYPASS
    3'd1: begin                 //``VAL MMC1
      prg_sbba0 = mmc1_prg_sbba0[17:SBB_AL];
      chr_sbba0 = mmc1_chr_sbba0[16:SBB_AL];
      chr_sbba1 = mmc1_chr_sbba1[16:SBB_AL];
      prg_a[PRG_AM:14] = mmc1_prga;
      chr_a[CHR_AM:12] = mmc1_chra;
      VRAM_A10 = mmc1_va10;
      prgrom_nce = ~mmc1_prgcs;
      prgram_nce = snrom & mmc1_chra[16];
      prgram_cs = mmc1_wramcs;
      if (serom)
        prg_a[14] = A[14];
    end
    3'd2: begin                 //``VAL UXROM
      prg_a = uxrom_prga[PRG_AM:0];
      prg_sbba0 = uxrom_prg_sbba0[21:SBB_AL];
    end
    3'd4: begin                 //``VAL MMC3_6
      prg_sbba0 = mmc3_6_prg_sbba0[18:SBB_AL];
      prg_sbba1 = mmc3_6_prg_sbba1[18:SBB_AL];
      chr_sbba0 = mmc3_6_chr_sbba0[17:SBB_AL];
      chr_sbba1 = mmc3_6_chr_sbba1[17:SBB_AL];
      chr_sbba2 = mmc3_6_chr_sbba2[17:SBB_AL];
      chr_sbba3 = mmc3_6_chr_sbba3[17:SBB_AL];
      chr_sbba4 = mmc3_6_chr_sbba4[17:SBB_AL];
      chr_sbba5 = mmc3_6_chr_sbba5[17:SBB_AL];
      prg_a[PRG_AM:13] = mmc3_6_prga;
      chr_a[CHR_AM:10] = mmc3_6_chra;
      prgrom_nce = ~mmc3_6_prgcs;
      if (~namcot) begin
        VRAM_A10 = mmc3_6_va10;
        prgram_nce = 1'b0;
        prgram_cs = mmc3_6_wramcs;
        nIRQ = mmc3_6_nirq;
      end
    end
    default: begin
      prg_a = {PRG_AM+1{1'bx}};
      chr_a = {CHR_AM+1{1'bx}};
      VRAM_A10 = 1'bx;
    end
  endcase
end


//////////////////////////////////////////////////////////////////////
// System control

assign SYS_HOLD_REQ = ~&{prg_ready, chr_ready};


endmodule
