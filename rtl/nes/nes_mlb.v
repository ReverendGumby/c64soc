module nes_mlb
  (
   input         CLK_RESET,
   input         CLK,
   input         nRES,
   input         RES_COLD,

   output        CP1,
   output        CP1_POSEDGE,
   output        CP1_NEGEDGE,
   output        CP2,
   output        CP2_POSEDGE,
   output        CP2_NEGEDGE,
   output        APU_CP1_POSEDGE,
   output        M2,
   output        M2_POSEDGE,
   output        M2_NEGEDGE,
   output        CC,
   output        nCC,

   // CARTRIDGE PORT
   input         RW_I,
   output        RW_O, // pin 14
   input [7:0]   D_I,
   output [7:0]  D_O,
   output [7:0]  D_OE,
   input [15:0]  A_I,
   output [15:0] A_O,
   output        A_OE,
   output        PRGROM_nOE, // pin 50
   input         nBREAD_I,
   output        nBREAD_O, // pin 21
   input         nBWRITE_I,
   output        nBWRITE_O, // pin 56
   output        BRW_OE,
   input [7:0]   BD_I,
   output [7:0]  BD_O,
   output        BD_OE,
   input [13:0]  BA_I,
   output [13:0] BA_O,
   output        BA_OE,
   input         VRAM_A10, // pin 22
   output        VRAM_nCEO, // pin 58
   input         VRAM_nCEI, // pin 57
   output        CHAROM_nCE, // pin 65
   input         nIRQ, // pin 15

   // AUX PORT
   output [1:0]  INP,
   output [2:0]  OUT,
   output        PPU_PCE,
   output        PPU_DE,
   output        PPU_HS,
   output        PPU_VS,
   output [7:0]  PPU_R,
   output [7:0]  PPU_G,
   output [7:0]  PPU_B,
   input         APU_MCLK,
   input         APU_PDCLKEN,
   output [15:0] APU_PDOUT,

   // PLAYER PORT
   output [1:0]  CTLR1_I,
   input [2:0]   CTLR1_O,
   output [1:0]  CTLR2_I,
   input [2:0]   CTLR2_O,

   // nesbus interface
   input         SYS_HOLD_REQ,
   output reg    SYS_HOLD_ACK,
   input [4:0]   CPU_REG_SEL,
   output [7:0]  CPU_REG_DOUT,
   input         ICD_SYS_ENABLE,
   input         ICD_SYS_FORCE_HALT,
   input         ICD_SYS_RESUME,
   output        ICD_SYS_HOLD,
   input [7:0]   ICD_MATCH_ENABLE,
   output [7:0]  ICD_MATCH_TRIGGER,
   input [2:0]   ICD_MATCH_SEL,
   input [3:0]   ICD_MATCH_REG_SEL,
   input [3:0]   ICD_MATCH_REG_WSTRB,
   input [31:0]  ICD_MATCH_REG_DIN,
   output [31:0] ICD_MATCH_REG_DOUT,
   input [8:2]   PPU_MON_SEL,
   input         PPU_MON_READY,
   output [31:0] PPU_MON_DOUT,
   output        PPU_MON_VALID,
   input [4:0]   APU_MON_SEL,
   input         APU_MON_READY,
   output [7:0]  APU_MON_DOUT,
   output        APU_MON_VALID
   );

//////////////////////////////////////////////////////////////////////
// CPU complex

reg [7:0] d_o, d_oe;

wire [31:0] cpu_cyc;
wire [15:0] a, a_o_cpu;
wire [7:0]  d_o_cpu, d_o_wram, d_o_ppu;
wire [4:0]  d_o_player;
wire        rdy = 1'b1;
wire        nce_wram;

// The data bus is driven by at most one driver.
always @* begin
  d_o = 8'hxx;
  d_oe = 8'hff;
  case ({d_oe_ppu, d_oe_cpu, d_oe_wram, d_oe_player})
    4'b1000: d_o = d_o_ppu;
    4'b0100: d_o = d_o_cpu;
    4'b0010: d_o = d_o_wram;
    4'b0001: begin
      d_o[4:0] = d_o_player;
      d_oe[7:5] = 3'b0;
    end
    default: d_oe = 8'h00;
  endcase
end
assign D_O = d_o;
assign D_OE = d_oe;
assign d = D_OE ? d_o : D_I;

// The address bus is driven by CPU or external.
assign A_O = a_o_cpu;
assign A_OE = a_oe_cpu;
assign a = A_OE ? a_o_cpu : A_I;

rp2a03 cpu // U6
  (
   .nRES(nRES),
   .CLK_RESET(CLK_RESET),
   .CLK(CLK),
   .CP1(CP1),
   .CP1_POSEDGE(CP1_POSEDGE),
   .CP1_NEGEDGE(CP1_NEGEDGE),
   .CP2(CP2),
   .CP2_POSEDGE(CP2_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .APU_CP1_POSEDGE(APU_CP1_POSEDGE),
   .M2(M2),
   .M2_POSEDGE(M2_POSEDGE),
   .M2_NEGEDGE(M2_NEGEDGE),

   .APU_MON_SEL(APU_MON_SEL),
   .APU_MON_READY(APU_MON_READY),
   .APU_MON_DOUT(APU_MON_DOUT),
   .APU_MON_VALID(APU_MON_VALID),

   .ICD_CPU_CYC(cpu_cyc),

   .nIRQ(nIRQ),
   .nNMI(nnmi),

   .A_O(a_o_cpu),
   .A_OE(a_oe_cpu),
   .D_I(D_I),
   .D_O(d_o_cpu),
   .D_OE(d_oe_cpu),
   .RW(RW_O),

   .INP(INP),
   .OUT(OUT),

   .RDY(rdy),
   .SYNC(sync),
   .HOLD(SYS_HOLD_ACK),
   .CPU_REG_SEL(CPU_REG_SEL),
   .CPU_REG_DOUT(CPU_REG_DOUT),

   .MCLK(APU_MCLK),
   .PDCLKEN(APU_PDCLKEN),
   .PDOUT(APU_PDOUT)
   );

sram_16k wram // U1
  (
   .CLK(CLK),
   .CLKEN(1'b1),
   .nCE(nce_wram),
   .nOE(1'b0),
   .nWE(RW_I),
   .A(a[10:0]),
   .D_I(D_I),
   .D_O(d_o_wram),
   .D_OE(d_oe_wram)
   );

// Address decode logic
// When M2 = 1:
//   A              Chip selected
//   $0000-$1FFF    WRAM (U1)
//   $2000-$3FFF    PPU
//   $8000-$FFFF    PRGROM

wire [3:0] adl_1_y, adl_2_y;

lf74ls139 u3_1
  (
   .A(a[13]),
   .B(a[14]),
   .G(adl_2_y[1]),
   .Y(adl_1_y)
   );

lf74ls139 u3_2
  (
   .A(M2),
   .B(a[15]),
   .G(1'b0),
   .Y(adl_2_y)
   );

assign nce_wram = adl_1_y[0];
wire ppu_ncs = adl_1_y[1];
assign PRGROM_nOE = adl_2_y[3];

//////////////////////////////////////////////////////////////////////
// PPU complex

reg [7:0] bd_o;

wire [13:8] ba_o;
wire [7:0]  bd, bd_o_ppu, bd_o_vram;
wire        bd_oe_ppu, bd_oe_vram;
wire [31:0] ppu_icd_frame;
wire [8:0]  ppu_icd_col;
wire [8:0]  ppu_icd_row;

rp2c02 ppu // U5
  (
   .CLK_RESET(CLK_RESET),
   .CLK(CLK),
   .CC(CC),
   .nCC(nCC),
   .RES_COLD(RES_COLD),
   .HOLD(SYS_HOLD_ACK),

   .MON_SEL(PPU_MON_SEL),
   .MON_READY(PPU_MON_READY),
   .MON_DOUT(PPU_MON_DOUT),
   .MON_VALID(PPU_MON_VALID),

   .ICD_COL(ppu_icd_col),
   .ICD_ROW(ppu_icd_row),
   .ICD_FRAME(ppu_icd_frame),

   .RA(a[2:0]),
   .D_I(D_I),
   .D_O(d_o_ppu),
   .D_OE(d_oe_ppu),
   .RW(RW_I),
   .nCS(ppu_ncs),

   .AD_I(bd),
   .AD_O({ba_o[13:8], bd_o_ppu}),
   .AD_OE(bd_oe_ppu),
   .ALE(ale),
   .nR(nBREAD_O),
   .nW(nBWRITE_O),
   .ARW_OE(arw_oe_ppu),

   .nSYNC(nRES),
   .nVBL(nnmi),

   .PCE(PPU_PCE),
   .DE(PPU_DE),
   .HS(PPU_HS),
   .VS(PPU_VS),
   .PIX(),
   .R(PPU_R),
   .G(PPU_G),
   .B(PPU_B)
   );

// The video data bus is driven by at most one driver.
always @* begin
  case ({bd_oe_ppu, bd_oe_vram})
    2'b10: bd_o = bd_o_ppu;
    2'b01: bd_o = bd_o_vram;
    default: bd_o = 8'hxx;
  endcase
end
assign BD_O = bd_o;
assign BD_OE = bd_oe_ppu | bd_oe_vram;
assign bd = BD_OE ? bd_o : BD_I;

// 74LS373 (U2)
reg [7:0] u2_d;
always @(posedge CLK) if (ale)
    u2_d <= bd;

wire [7:0] ba = ale ? bd : u2_d;

wire [10:0] ba_vram = {VRAM_A10, BA_I[9:0]};

sram_16k vram // U4
  (
   .CLK(CLK),
   .CLKEN(1'b1),
   .nCE(VRAM_nCEI),
   .nOE(nBREAD_I),
   .nWE(nBWRITE_I),
   .A(ba_vram),
   .D_I(bd),
   .D_O(bd_o_vram),
   .D_OE(bd_oe_vram)
   );

assign CHAROM_nCE = BA_I[13];
assign VRAM_nCEO = ~BA_I[13];

assign BRW_OE = arw_oe_ppu;
assign BA_O[13:0] = {ba_o[13:8], ba};
assign BA_OE = arw_oe_ppu;

//////////////////////////////////////////////////////////////////////
// Controller port emulation

player player
  (
   .CLK(CLK),

   .INP(INP),
   .OUT(OUT),

   .D_O(d_o_player),
   .D_OE(d_oe_player),

   .CTLR1_I(CTLR1_I),
   .CTLR1_O(CTLR1_O),
   .CTLR2_I(CTLR2_I),
   .CTLR2_O(CTLR2_O)
   );

//////////////////////////////////////////////////////////////////////
// Additional logic, not original to the machine

initial
  SYS_HOLD_ACK = 1'b0;

// Synchonize system hold timing with the slowest entity: the CPU.
always @(posedge CLK) if (CP2_NEGEDGE)
  SYS_HOLD_ACK <= SYS_HOLD_REQ;

icd #(.NUM_MATCH(8)) icd
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP1_POSEDGE(CP1_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .CC(CC),
   .HOLD(SYS_HOLD_ACK),

   .CPU_A(a),
   .CPU_DB(D_I),
   .CPU_RW(RW_I),
   .CPU_RDY(rdy),
   .CPU_SYNC(sync),
   .CPU_CYC(cpu_cyc),
   .PPU_COL(ppu_icd_col),
   .PPU_ROW(ppu_icd_row),
   .PPU_FRAME(ppu_icd_frame[19:0]),

   .MATCH_ENABLE(ICD_MATCH_ENABLE),
   .MATCH_TRIGGER(ICD_MATCH_TRIGGER),
   .MATCH_SEL(ICD_MATCH_SEL),
   .MATCH_REG_SEL(ICD_MATCH_REG_SEL),
   .MATCH_REG_WSTRB(ICD_MATCH_REG_WSTRB),
   .MATCH_REG_DIN(ICD_MATCH_REG_DIN),
   .MATCH_REG_DOUT(ICD_MATCH_REG_DOUT),

   .SYS_ENABLE(ICD_SYS_ENABLE),
   .SYS_FORCE_HALT(ICD_SYS_FORCE_HALT),
   .SYS_RESUME(ICD_SYS_RESUME),
   .SYS_HOLD(ICD_SYS_HOLD)
   );

endmodule
