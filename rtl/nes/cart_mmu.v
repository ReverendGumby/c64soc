//////////////////////////////////////////////////////////////////////
// NES -> backing store memory translation

module cart_mmu #(parameter BAM = 3,
                  parameter BANKS = 10)
  (
   input             CLK,
   input             nRES,

   // Translation table control interface
   input [7:0]       TT_IDX,
   input [31:0]      TT_DIN,
   output reg [31:0] TT_DOUT,
   input             TT_WE,

   // Bus bank address interfaces
   input [5:0]       P0_BOP,
   input [BAM:0]     P0_RAOM_BA,
   output            P0_BS_BA_VALID,
   output [BAM:0]    P0_BS_BA,

   input [5:0]       P1_BOP,
   input [BAM:0]     P1_RAOM_BA,
   output            P1_BS_BA_VALID,
   output [BAM:0]    P1_BS_BA,

   input [5:0]       P2_BOP,
   input [BAM:0]     P2_RAOM_BA,
   output            P2_BS_BA_VALID,
   output [BAM:0]    P2_BS_BA,

   input [5:0]       P3_BOP,
   input [BAM:0]     P3_RAOM_BA,
   output            P3_BS_BA_VALID,
   output [BAM:0]    P3_BS_BA,

   input [5:0]       P4_BOP,
   input [BAM:0]     P4_RAOM_BA,
   output            P4_BS_BA_VALID,
   output [BAM:0]    P4_BS_BA,

   input [5:0]       P5_BOP,
   input [BAM:0]     P5_RAOM_BA,
   output            P5_BS_BA_VALID,
   output [BAM:0]    P5_BS_BA,

   input [5:0]       P6_BOP,
   input [BAM:0]     P6_RAOM_BA,
   output            P6_BS_BA_VALID,
   output [BAM:0]    P6_BS_BA,

   input [5:0]       P7_BOP,
   input [BAM:0]     P7_RAOM_BA,
   output            P7_BS_BA_VALID,
   output [BAM:0]    P7_BS_BA,

   input [5:0]       P8_BOP,
   input [BAM:0]     P8_RAOM_BA,
   output            P8_BS_BA_VALID,
   output [BAM:0]    P8_BS_BA
   );

localparam PORTS = 9;

`include "cart_bop.vh"

reg [1:0]           bids [0:BANKS-1];
reg [1:0]           cids [0:BANKS-1];
reg [BAM:0]         bases [0:BANKS-1];

integer             b;

task tt_set(input integer idx, input [31:0] dat);
  begin
    bids[idx] <= dat[17:16];
    cids[idx] <= dat[21:20];
    bases[idx] <= dat[BAM:0];
  end
endtask

always @(posedge CLK) begin
  if (~nRES) begin
    for (b = 0; b < BANKS; b = b + 1) begin
      bids[b] <= BID_NONE;
      cids[b] <= CID_NONE;
      bases[b] <= {BAM{1'b0}};
    end
  end
  else begin
    if (TT_WE) begin
      tt_set(TT_IDX, TT_DIN);
    end
  end
end

always @* begin
  TT_DOUT = 32'd0;
  TT_DOUT[17:16] = bids[TT_IDX];
  TT_DOUT[21:20] = cids[TT_IDX];
  TT_DOUT[BAM:0] = bases[TT_IDX];
end

generate
genvar p;
  for (p = 0; p < PORTS; p = p + 1) begin: port
  wire [BAM:0] base;
  wire [5:0]   bop;
  wire [1:0]   bid;
  wire [1:0]   cid;

  reg [BANKS-1:0] match;
  reg [BAM:0]     bank;
  reg             bank_valid;

    assign bid = get_bop_bid(bop);
    assign cid = get_bop_cid(bop);

    always @*
      for (b = 0; b < BANKS; b = b + 1)
        match[b] = { cids[b], bids[b], bases[b] } == { cid, bid, base };

    always @* begin
      bank_valid = 1'b0;
      bank = {BAM+1{1'b0}};
      for (b = 0; b < BANKS; b = b + 1)
        if (match[b]) begin
          bank_valid = 1'b1;
          bank = bank | b[BAM:0]; // OR uses fewer primitives
        end
    end
  end
endgenerate

assign port[0].bop = P0_BOP;
assign port[0].base = P0_RAOM_BA;
assign P0_BS_BA_VALID = port[0].bank_valid;
assign P0_BS_BA = port[0].bank;

assign port[1].bop = P1_BOP;
assign port[1].base = P1_RAOM_BA;
assign P1_BS_BA_VALID = port[1].bank_valid;
assign P1_BS_BA = port[1].bank;

assign port[2].bop = P2_BOP;
assign port[2].base = P2_RAOM_BA;
assign P2_BS_BA_VALID = port[2].bank_valid;
assign P2_BS_BA = port[2].bank;

assign port[3].bop = P3_BOP;
assign port[3].base = P3_RAOM_BA;
assign P3_BS_BA_VALID = port[3].bank_valid;
assign P3_BS_BA = port[3].bank;

assign port[4].bop = P4_BOP;
assign port[4].base = P4_RAOM_BA;
assign P4_BS_BA_VALID = port[4].bank_valid;
assign P4_BS_BA = port[4].bank;

assign port[5].bop = P5_BOP;
assign port[5].base = P5_RAOM_BA;
assign P5_BS_BA_VALID = port[5].bank_valid;
assign P5_BS_BA = port[5].bank;

assign port[6].bop = P6_BOP;
assign port[6].base = P6_RAOM_BA;
assign P6_BS_BA_VALID = port[6].bank_valid;
assign P6_BS_BA = port[6].bank;

assign port[7].bop = P7_BOP;
assign port[7].base = P7_RAOM_BA;
assign P7_BS_BA_VALID = port[7].bank_valid;
assign P7_BS_BA = port[7].bank;

assign port[8].bop = P8_BOP;
assign port[8].base = P8_RAOM_BA;
assign P8_BS_BA_VALID = port[8].bank_valid;
assign P8_BS_BA = port[8].bank;

endmodule
