module nesbus
  (
   // NES clocks
   input             CLK,
   input             CP1_POSEDGE,
   input             CP2_POSEDGE,
   input             CP2_NEGEDGE,
   input             APU_CP1_POSEDGE,
   input             CC,
   input             nCC,

   // AXI Lite slave bus interface
   input             ARESETn,
   input [31:0]      AWADDR,
   input             AWVALID,
   output reg        AWREADY,
   input [31:0]      WDATA,
   output reg        WREADY,
   input [3:0]       WSTRB,
   input             WVALID,
   output [1:0]      BRESP,
   output reg        BVALID,
   input             BREADY,
   input [31:0]      ARADDR,
   input             ARVALID,
   output reg        ARREADY,
   output reg [31:0] RDATA,
   output [1:0]      RRESP,
   output reg        RVALID,
   input             RREADY,

   // System control
   input             EXT_RES, // external (button) reset
   output reg        CLK_RESET, // reset all clkgen
   output reg        NES_nRES, // NES MLB reset
   output reg        CART_nRES, // cartridge emulation reset
   output reg        RES_COLD, // reset as if from first power-on
   output            SYS_HOLD_REQ, // halt system and take over memory bus
   input             SYS_HOLD_ACK,

   // Controller I/O
   input [1:0]       CTLR1_I, // pins 2, 3
   output [2:0]      CTLR1_O, // pins 4, 6, 7
   input [1:0]       CTLR2_I, // pins 2, 3
   output [2:0]      CTLR2_O, // pins 4, 6, 7

   // CPU memory bus
   output            RW_O,
   input [7:0]       D_I,
   output [7:0]      D_O,
   output            D_OE,
   output [15:0]     A_O,
   output            A_OE,

   // PPU memory bus
   output            BREAD,
   output            BWRITE,
   input [7:0]       BD_I,
   output [7:0]      BD_O,
   output            BD_OE,
   output [13:0]     BA_O,
   output            BA_OE,

   // CPU monitor and control interface
   output reg [4:0]  CPU_REG_SEL,
   input [7:0]       CPU_REG_DOUT,
   output reg        ICD_SYS_ENABLE,
   output reg        ICD_SYS_FORCE_HALT,
   output reg        ICD_SYS_RESUME,
   input             ICD_SYS_HOLD,
   output reg [7:0]  ICD_MATCH_ENABLE,
   input [7:0]       ICD_MATCH_TRIGGER,
   output reg [2:0]  ICD_MATCH_SEL,
   output reg [3:0]  ICD_MATCH_REG_SEL,
   output reg [3:0]  ICD_MATCH_REG_WSTRB,
   output [31:0]     ICD_MATCH_REG_DIN,
   input [31:0]      ICD_MATCH_REG_DOUT,

   // PPU debug monitor interface
   output reg [8:2]  PPU_MON_SEL,
   output            PPU_MON_READY,
   input [31:0]      PPU_MON_DOUT,
   input             PPU_MON_VALID,

   // APU debug monitor interface
   output reg [4:0]  APU_MON_SEL,
   output            APU_MON_READY,
   input [7:0]       APU_MON_DOUT,
   input             APU_MON_VALID,

   // Game cartridge emulator, control interface
   output reg [11:2] CART_CTRL_ADDR,
   output reg [3:0]  CART_CTRL_WSTRB,
   output [31:0]     CART_CTRL_DIN,
   input [31:0]      CART_CTRL_DOUT,

   // Game cartridge emulator, backing store interface
   output [17:2]     CART_BS_ARADDR,
   output            CART_BS_ARVALID,
   input [31:0]      CART_BS_RDATA,
   output            CART_BS_RREADY,
   input             CART_BS_RVALID,
   output [17:2]     CART_BS_AWADDR,
   output [3:0]      CART_BS_WSTRB,
   output [31:0]     CART_BS_WDATA,
   output reg        CART_BS_WVALID,
   input             CART_BS_BVALID
   );

reg         nes_usr_nres;

reg [7:0]   ctlr1;
reg [7:0]   ctlr2;

reg [7:0]   cpu_rdata;
reg         cpu_ractive, cpu_wactive;
reg         cpu_rvalid, cpu_wvalid;
reg         cpu_rreq, cpu_wreq;

reg [7:0]   ppu_rdata;
reg         ppu_ractive, ppu_wactive;
reg         ppu_rvalid, ppu_wvalid;
reg         ppu_rreq, ppu_wreq;

//////////////////////////////////////////////////////////////////////
// Register interface

// address range    size  bits   ussage
// -------------    ----  ----   ------
// 0_0000 - 0_0FFF   4K   8-32   nesbus control
// 0_1000 - 0_1FFF   4K   8-32   cartridge control
// 0_2000 - 0_FFFF  56K      -   (reserved)
// 1_0000 - 1_FFFF  64K      8   NES native CPU memory
// 2_0000 - 2_3FFF  16K      8   NES native PPU memory
// 2_4000 - 3_FFFF 112K      -   (reserved)
// 4_0000 - 7_FFFF 256K   8-32   cartridge backing store

localparam RRSVD       = 4'd0;
localparam NESBUS_CTRL = 4'd1;
localparam CPU_MEM     = 4'd2;
localparam PPU_MEM     = 4'd3;
localparam CART_CTRL   = 4'd4;
localparam CART_BS     = 4'd5;

function [3:0] AxADDR_region(input [31:0] AxADDR);
  begin
    casez (AxADDR[18:0])
//``REGION_START
      19'h0_0zzz: AxADDR_region = NESBUS_CTRL;
      19'h0_1zzz: AxADDR_region = CART_CTRL;
      19'h1_zzzz: AxADDR_region = CPU_MEM;
      19'h2_0zzz, 19'h2_1zzz, 19'h2_2zzz, 19'h2_3zzz:
        AxADDR_region = PPU_MEM;
      19'h4_zzzz, 19'h5_zzzz, 19'h6_zzzz, 19'h7_zzzz: 
        AxADDR_region = CART_BS;
//``REGION_END
      default:    AxADDR_region = RRSVD;
    endcase
  end
endfunction

reg [3:0] rregion, wregion;

always @* begin
  rregion = AxADDR_region(ARADDR);
  wregion = AxADDR_region(AWADDR);
end

reg [31:0] rdata;
reg        rvalid, bvalid;
reg        cart_rvalid;

always @* begin
  rdata = 32'h00000000;
  rvalid = 1'b1;
  if (rregion == NESBUS_CTRL) begin //``REGION_REGS
    casex (ARADDR[11:2])
      10'h0x: rdata = 32'h4d455331; //``REG ID DATA
      10'h10: begin             //``REG CTLR_MATRIX
        rdata[07:00] = ctlr1;   //``NO_FIELD
        rdata[15:08] = ctlr2;   //``NO_FIELD
      end
      10'h14: begin               //``REG RESETS
        rdata[0] = nes_usr_nres;  //``FIELD NES
        rdata[2] = CART_nRES;     //``FIELD CART
      end
      10'h15: begin             //``REG RESET_CTRL
        rdata[0] = RES_COLD;    //``FIELD COLD
      end
      10'h18: begin
        rdata[4:0] = CPU_REG_SEL;
      end
      10'h19: begin
        rdata[7:0] = CPU_REG_DOUT;
      end
      10'h1a: begin
        rdata[8:2] = PPU_MON_SEL;
      end
      10'h1b: begin
        rvalid = PPU_MON_VALID;
        rdata[31:0] = PPU_MON_DOUT;
      end
      10'h1c: begin                    //``REG ICD_CTRL
        rdata[0] = ICD_SYS_ENABLE;     //``FIELD SYS_ENABLE
        rdata[1] = ICD_SYS_FORCE_HALT; //``FIELD SYS_FORCE_HALT
        rdata[2] = ICD_SYS_RESUME;     //``FIELD SYS_RESUME
        rdata[8] = ICD_SYS_HOLD;       //``FIELD SYS_HOLD
        rdata[23:16] = ICD_MATCH_ENABLE;  //``FIELD MATCH_ENABLE
        rdata[31:24] = ICD_MATCH_TRIGGER; //``FIELD MATCH_TRIGGER
      end
      10'h1d: begin
        rdata[2:0] = ICD_MATCH_SEL;
      end
      10'h1e: begin
        rdata[3:0] = ICD_MATCH_REG_SEL;
      end
      10'h1f: rdata = ICD_MATCH_REG_DOUT;
      10'h24: begin
        rdata[4:0] = APU_MON_SEL;
      end
      10'h25: begin
        rvalid = APU_MON_VALID;
        rdata[7:0] = APU_MON_DOUT;
      end
      default: ;
    endcase
  end
  else if (rregion == CPU_MEM) begin
    rdata = {4{cpu_rdata}};
    rvalid = cpu_rvalid;
  end
  else if (rregion == PPU_MEM) begin
    rdata = {4{ppu_rdata}};
    rvalid = ppu_rvalid;
  end
  else if (rregion == CART_CTRL) begin
    rvalid = cart_rvalid;
    rdata = CART_CTRL_DOUT;
  end
  else if (rregion == CART_BS) begin
    rvalid = CART_BS_RVALID;
    rdata = CART_BS_RDATA;
  end
end

always @(posedge CLK) begin
  if (!ARESETn) begin
    ARREADY <= 1'b0;
    RVALID <= 1'b0;
  end
  else begin
    if (!ARREADY && ARVALID) begin
      if (rvalid) begin
        ARREADY <= 1'b1;
        RDATA <= rdata;
        RVALID <= 1'b1;
      end
    end
    else if (RVALID) begin
      ARREADY <= 1'b0;
      if (RREADY)
        RVALID <= 1'b0;
    end
  end
end

always @(posedge CLK) begin
  if (!ARESETn) begin
    cpu_rreq <= 1'b0;
  end
  else begin
    if (cpu_ractive || cpu_rvalid)
      cpu_rreq <= 1'b0;
    else if (!ARREADY && ARVALID && rregion == CPU_MEM)
      cpu_rreq <= 1'b1;
  end
end

always @(posedge CLK) begin
  if (!ARESETn) begin
    ppu_rreq <= 1'b0;
  end
  else begin
    if (ppu_ractive || ppu_rvalid)
      ppu_rreq <= 1'b0;
    else if (!ARREADY && ARVALID && rregion == PPU_MEM)
      ppu_rreq <= 1'b1;
  end
end

always @(posedge CLK) begin
  if (!ARESETn) begin
    CART_CTRL_ADDR <= 10'h000;
    cart_rvalid <= 1'b0;
  end
  else begin
    cart_rvalid <= 1'b0;
    if (!ARREADY && ARVALID && rregion == CART_CTRL) begin
      CART_CTRL_ADDR <= ARADDR[11:2];
      cart_rvalid <= 1'b1;
    end
    else if (!AWREADY && AWVALID && wregion == CART_CTRL)
      CART_CTRL_ADDR <= AWADDR[11:2];
  end
end

assign PPU_MON_READY = RREADY;
assign APU_MON_READY = RREADY;

assign CART_BS_ARADDR = ARADDR[17:2];
assign CART_BS_RREADY = RREADY;
assign CART_BS_ARVALID = ARVALID && rregion == CART_BS;

assign CART_BS_AWADDR = AWADDR[17:2];
assign CART_BS_WSTRB = WSTRB;
assign CART_BS_WDATA = WDATA;

assign RRESP = 2'b00;

always @* begin
  bvalid = 1'b1;
  if (wregion == CPU_MEM)
    bvalid = cpu_wvalid;
  else if (wregion == PPU_MEM)
    bvalid = ppu_wvalid;
  else if (wregion == CART_BS)
    bvalid = CART_BS_BVALID;
end

always @(posedge CLK) begin
  if (!ARESETn) begin
    AWREADY <= 1'b0;
    WREADY <= 1'b0;
    BVALID <= 1'b0;
  end
  else begin
    if (!AWREADY && AWVALID && !WREADY && WVALID && !BVALID) begin
      if (bvalid) begin
        AWREADY <= 1'b1;
        WREADY <= 1'b1;
        BVALID <= 1'b1;
      end
    end
    else if (BVALID) begin
      AWREADY <= 1'b0;
      WREADY <= 1'b0;
      if (BREADY)
        BVALID <= 1'b0;
    end
  end
end

assign BRESP = 2'b00;

always @(posedge CLK) begin
  if (!ARESETn) begin
    ctlr1 <= 8'h00;
    ctlr2 <= 8'h00;
    nes_usr_nres <= 1'b0;
    CART_nRES <= 1'b0;
    RES_COLD <= 1'b1;
    CART_CTRL_WSTRB <= 4'h0;
    CART_BS_WVALID <= 1'b0;
    ICD_MATCH_REG_WSTRB <= 4'h0;
    ICD_SYS_ENABLE <= 1'b0;
    ICD_SYS_FORCE_HALT <= 1'b0;
    ICD_SYS_RESUME <= 1'b0;
    ICD_MATCH_ENABLE <= 8'b0;
    ICD_MATCH_SEL <= 3'd0;
    ICD_MATCH_REG_SEL <= 4'd0;
    CPU_REG_SEL <= 5'h0;
    PPU_MON_SEL <= 7'h00;
    APU_MON_SEL <= 5'h0;
  end
  else begin
    CART_CTRL_WSTRB <= 4'h0;
    CART_BS_WVALID <= 1'b0;
    ICD_MATCH_REG_WSTRB <= 4'h0;
    if (!AWREADY && AWVALID && !WREADY && WVALID && !BVALID) begin
      if (wregion == NESBUS_CTRL) begin
        case (AWADDR[11:2])
          10'h10: begin
            if (WSTRB[0]) ctlr1 <= WDATA[07:00];
            if (WSTRB[1]) ctlr2 <= WDATA[15:08];
          end
          10'h14: begin
            if (WSTRB[0]) begin
              nes_usr_nres <= WDATA[0];
              CART_nRES <= WDATA[2];
            end
          end
          10'h15: begin
            if (WSTRB[0]) begin
              RES_COLD <= WDATA[0];
            end
          end
          10'h18: begin
            if (WSTRB[0]) begin
              CPU_REG_SEL <= WDATA[4:0];
            end
          end
          10'h1a: begin
            if (WSTRB[0]) PPU_MON_SEL[07:02] <= WDATA[07:02];
            if (WSTRB[1]) PPU_MON_SEL[08:08] <= WDATA[08:08];
          end
          10'h1c: begin
            if (WSTRB[0]) begin
              ICD_SYS_ENABLE <= WDATA[0];
              ICD_SYS_FORCE_HALT <= WDATA[1];
              ICD_SYS_RESUME <= WDATA[2];
            end
            if (WSTRB[2]) begin
              ICD_MATCH_ENABLE <= WDATA[23:16];
            end
          end
          10'h1d: begin
            if (WSTRB[0]) ICD_MATCH_SEL <= WDATA[2:0];
          end
          10'h1e: begin
            if (WSTRB[0]) ICD_MATCH_REG_SEL <= WDATA[3:0];
          end
          10'h1f: begin
            ICD_MATCH_REG_WSTRB <= WSTRB;
          end
          10'h24: begin
            if (WSTRB[0]) APU_MON_SEL <= WDATA[04:00];
          end
        endcase
      end
      else if (wregion == CART_CTRL) begin
        CART_CTRL_WSTRB <= WSTRB;
      end
      else if (wregion == CART_BS) begin
        CART_BS_WVALID <= WVALID;
      end
    end
  end
end

assign CART_CTRL_DIN = WDATA;
assign ICD_MATCH_REG_DIN = WDATA;

always @(posedge CLK) begin
  if (!ARESETn) begin
    cpu_wreq <= 1'b0;
  end
  else begin
    if (cpu_wactive || cpu_wvalid)
      cpu_wreq <= 1'b0;
    else if (!AWREADY && AWVALID && !WREADY && WVALID && !BVALID &&
             wregion == CPU_MEM) begin
      cpu_wreq <= 1'b1;
    end
  end
end

always @(posedge CLK) begin
  if (!ARESETn) begin
    ppu_wreq <= 1'b0;
  end
  else begin
    if (ppu_wactive || ppu_wvalid)
      ppu_wreq <= 1'b0;
    else if (!AWREADY && AWVALID && !WREADY && WVALID && !BVALID &&
             wregion == PPU_MEM) begin
      ppu_wreq <= 1'b1;
    end
  end
end

//////////////////////////////////////////////////////////////////////
// Controller I/O

// Buttons are normally open and read as 0. A 1 in ctlr_*[x] causes the
// button to read as 1. All eight buttons are latched into a shift
// register on CTLR*_I[1] falling edge. The LSB is inverted and drives
// CTLR*_O[0], and the register is shifted on CTLR*_I[0] rising
// edge. The register fills the shifted-in bit with 0.

reg [7:0] ctlr1_reg, ctlr2_reg;
reg       ctlr1_shift_d, ctlr2_shift_d;

wire      ctlr1_latch = CTLR1_I[1];
wire      ctlr2_latch = CTLR2_I[1];
wire      ctlr1_shift = !CTLR1_I[0];
wire      ctlr2_shift = !CTLR2_I[0];

always @(posedge CLK) begin
  if (!NES_nRES) begin
    ctlr1_reg <= 8'h00;
    ctlr2_reg <= 8'h00;
    ctlr1_shift_d <= 1'b0;
    ctlr2_shift_d <= 1'b0;
  end
  else begin
    if (ctlr1_latch)
      ctlr1_reg <= ctlr1;
    else if (!ctlr1_shift && ctlr1_shift_d)
      ctlr1_reg <= {1'b0, ctlr1_reg[7:1]};

    if (ctlr2_latch)
      ctlr2_reg <= ctlr2;
    else if (!ctlr2_shift && ctlr2_shift_d)
      ctlr2_reg <= {1'b0, ctlr2_reg[7:1]};
  end

  ctlr1_shift_d <= ctlr1_shift;
  ctlr2_shift_d <= ctlr2_shift;
end

assign CTLR1_O = {2'b11, ~ctlr1_reg[0]};
assign CTLR2_O = {2'b11, ~ctlr2_reg[0]};

//////////////////////////////////////////////////////////////////////
// Reset generator

reg [6:0] rst_cnt;
reg       nes_rst;

initial begin
  CLK_RESET = 1'b1;
  rst_cnt = ~0;
  nes_rst = 1'b1;
end

always @(posedge CLK) begin
  CLK_RESET <= ~ARESETn;

  if (!ARESETn || !nes_usr_nres || EXT_RES) begin
    rst_cnt <= 7'b0;
    nes_rst <= 1'b1;
  end
  else begin
    if (&rst_cnt)
      nes_rst <= 1'b0;
    else
      rst_cnt <= rst_cnt + 1'b1;
  end
end

// Ensure CPU and PPU come out of reset on CP1 rising and CC rising,
// respectively. (Those two clocks are guaranteed to be offset by a
// constant phase.) While this is less accurate than actual HW, it's
// more friendly for TAS movie playback.
always @(posedge CLK) begin
  if (nes_rst)
    NES_nRES <= 1'b0;
  else if (!NES_nRES && CP1_POSEDGE && APU_CP1_POSEDGE)
    NES_nRES <= 1'b1;
end

//////////////////////////////////////////////////////////////////////
// NES native CPU memory interface
//
// Transactions stall the machine for a clock cycle to master the bus.

reg [15:0] cpu_addr, cpu_a;
reg [7:0]  cpu_wdata, cpu_dout;
reg        cpu_hold;
reg        cpu_rw;
reg        cpu_master;

always @* begin
  cpu_hold = (cpu_ractive || cpu_wactive) & ~cpu_master;
end

always @(posedge CLK) if (CP1_POSEDGE) begin
  cpu_dout <= cpu_wdata;
  cpu_a <= cpu_addr;
  cpu_master <= cpu_hold & SYS_HOLD_ACK;
end

always @(posedge CLK) begin
  // Write cycle: Drive RW low from CP2 rising to CP1 rising
  if (CP1_POSEDGE)
    cpu_rw <= 1'b1;
  else if (CP2_POSEDGE)
    cpu_rw <= !cpu_wactive;
end

assign D_O = cpu_dout;
assign A_O = cpu_a;
assign RW_O = !cpu_master || cpu_rw;

assign D_OE = cpu_master && !cpu_rw;
assign A_OE = cpu_master;

always @(posedge CLK) begin
  if (!ARESETn) begin
    cpu_ractive <= 1'b0;
    cpu_rvalid <= 1'b0;
    cpu_wactive <= 1'b0;
    cpu_wvalid <= 1'b0;
  end
  else begin
    cpu_rvalid <= 1'b0;
    cpu_wvalid <= 1'b0;
    if (CP2_NEGEDGE) begin
      cpu_rdata <= D_I;
      if (!(cpu_hold | cpu_master)) begin
        if (cpu_rreq) begin
          cpu_ractive <= 1'b1;
          cpu_addr <= ARADDR[15:0];
        end
        else if (cpu_wreq) begin
          cpu_wactive <= 1'b1;
          cpu_addr <= AWADDR[15:0];
          case ({AWADDR[1:0], 1'b1})
            {2'b11, WSTRB[3]}: cpu_wdata <= WDATA[31:24];
            {2'b10, WSTRB[2]}: cpu_wdata <= WDATA[23:16];
            {2'b01, WSTRB[1]}: cpu_wdata <= WDATA[15:08];
            {2'b00, WSTRB[0]}: cpu_wdata <= WDATA[07:00];
            default: cpu_wdata <= 8'hxx;
          endcase
        end
      end
      else if (cpu_master) begin
        cpu_ractive <= 1'b0;
        cpu_wactive <= 1'b0;
        cpu_rvalid <= cpu_ractive;
        cpu_wvalid <= cpu_wactive;
      end
    end
  end
end

//////////////////////////////////////////////////////////////////////
// NES native PPU memory interface
//
// Transactions stall the machine for a CC clock cycle to master the bus.

reg [13:0] ppu_addr, ppu_a;
reg [7:0]  ppu_wdata, ppu_dout;
reg        ppu_hold;
reg        ppu_rw;
reg        ppu_master;

always @* begin
  ppu_hold = (ppu_ractive || ppu_wactive) & ~ppu_master;
end

always @* begin
  ppu_dout = ppu_wdata;
  ppu_a = ppu_addr;
end

// Because we transition ppu_hold on CC, we must wait 1 or more CC for
// it to take effect.
always @(posedge CLK) if (CC)
  ppu_master <= ppu_hold & SYS_HOLD_ACK;

always @(posedge CLK) begin
  // Write cycle: Drive RW low from CC falling to CC rising
  if (CC)
    ppu_rw <= 1'b1;
  else if (nCC)
    ppu_rw <= !ppu_wactive;
end

assign BD_O = ppu_dout;
assign BA_O = ppu_a;
assign BREAD = ppu_master & ppu_rw;
assign BWRITE = ppu_master & ~ppu_rw;

assign BD_OE = ppu_master && !ppu_rw;
assign BA_OE = ppu_master;

always @(posedge CLK) begin
  if (!ARESETn) begin
    ppu_ractive <= 1'b0;
    ppu_rvalid <= 1'b0;
    ppu_wactive <= 1'b0;
    ppu_wvalid <= 1'b0;
  end
  else begin
    ppu_rvalid <= 1'b0;
    ppu_wvalid <= 1'b0;
    if (CC) begin
/* -----\/----- EXCLUDED -----\/-----
      ppu_ractive <= 1'b0;
      ppu_wactive <= 1'b0;
 -----/\----- EXCLUDED -----/\----- */
      if (!(ppu_hold | ppu_master)) begin
        if (ppu_rreq) begin
          ppu_ractive <= 1'b1;
          ppu_addr <= ARADDR[13:0];
        end
        else if (ppu_wreq) begin
          ppu_wactive <= 1'b1;
          ppu_addr <= AWADDR[13:0];
          case ({AWADDR[1:0], 1'b1})
            {2'b11, WSTRB[3]}: ppu_wdata <= WDATA[31:24];
            {2'b10, WSTRB[2]}: ppu_wdata <= WDATA[23:16];
            {2'b01, WSTRB[1]}: ppu_wdata <= WDATA[15:08];
            {2'b00, WSTRB[0]}: ppu_wdata <= WDATA[07:00];
            default: ppu_wdata <= 8'hxx;
          endcase
        end
      end
      else if (ppu_master) begin
        ppu_ractive <= 1'b0;
        ppu_wactive <= 1'b0;
        ppu_rvalid <= ppu_ractive;
        ppu_wvalid <= ppu_wactive;
        if (ppu_ractive)
          ppu_rdata <= BD_I;
      end
    end
  end
end

assign SYS_HOLD_REQ = cpu_hold | ppu_hold;

endmodule
