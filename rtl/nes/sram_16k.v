module sram_16k
  (
   input        CLK,
   input        CLKEN,
   input        nCE,
   input        nOE,
   input        nWE,
   input [10:0] A,
   input [7:0]  D_I,
   output [7:0] D_O,
   output       D_OE
   );

reg [7:0] mem [0:(1<<11)-1];
reg [7:0] d;

// Initial RAM image matches BizHawk.

initial begin
  $readmemh("sram_16k.hex", mem);
end

always @(posedge CLK) if (CLKEN) begin
  d <= mem[A];
  if (!nCE && !nWE)
    mem[A] <= D_I;
end

assign D_O = d;
assign D_OE = !nCE && !nOE && nWE;

endmodule
