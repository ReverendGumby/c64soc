//////////////////////////////////////////////////////////////////////
// MMC1 mapper
//
// Reference: https://wiki.nesdev.org/w/index.php?title=MMC1
//
// Banks:
//   CPU $6000-$7FFF: (8K) PRG RAM bank (optional)
//   CPU $8000-$BFFF: (16K) switchable bank, or fixed to first 16K
//   CPU $C000-$FFFF: (16K) switchable bank, or fixed to last 16K
//   PPU $0000-$0FFF: (4K) switchable bank
//   PPU $1000-$1FFF: (4K) switchable bank

module mmc1
  (
   // Clocks / resets
   input              CLK,
   input              nRES, // MLB reset
   input              RES_COLD,
   input              CP1_NEGEDGE,
   input              CP2,
   input              CP2_NEGEDGE,

   // Cartridge inputs
   input              RW, // pin 14
   input [7:0]        D,
   input [14:13]      A,
   input              PRGROM_nOE, // pin 50
   input              nBREAD, // pin 21
   input              nBWRITE, // pin 56
   input [12:10]      BA,
   input              CHAROM_nCE, // pin 65

   // Switchable bank outputs
   output [17:14]     PRG0,
   output [16:12]     CHR0,
   output [16:12]     CHR1,

   // ROM / RAM outputs
   output reg [17:14] PRG_A, // remapped PRG ROM/RAM address
   output reg [16:12] CHR_A, // remapped CHR ROM/RAM address
   output reg         CIRAM_A10, // VRAM A[10]
   output             PRG_CS, // PRG ROM /ROMSEL
   output             WRAM_CS // WRAM (PRG RAM) +CE
   );

reg [5:0] load;           // shift register, +1b for full detect
reg [4:0] ctrl;
reg [4:0] chr0, chr1, prg0;
reg [4:0] prg0_d;
reg       last_rw;

localparam [5:0] load_reset = 6'h20;

wire [5:0] load_shift = {D[0], load[5:1]};
wire       load_sel = ~PRGROM_nOE & ~RW & last_rw;

always @(posedge CLK) if (CP2_NEGEDGE) begin
  if (~nRES & RES_COLD) begin
    load <= load_reset;
    ctrl <= 5'h0c;
    chr0 <= 5'h00;
    chr1 <= 5'h00;
    prg0 <= 5'h00;
  end
  else begin
    if (load_sel)
      if (D[7]) begin           // reset SR, $C000 = last bank
        load <= load_reset;
        ctrl <= ctrl | 5'h0c;
      end
      else begin                  // shift in
        if (load_shift[0]) begin  // load is full
          case (A[14:13])
            2'b00: ctrl <= load_shift[5:1];
            2'b01: chr0 <= load_shift[5:1];
            2'b10: chr1 <= load_shift[5:1];
            2'b11: prg0 <= load_shift[5:1];
            default: ;
          endcase
          load <= load_reset;
        end
        else
          load <= load_shift;
      end

    last_rw <= RW;
  end
end

// When a prg0 change causes the cart to hold (so the system can swap
// in the new bank), the hold take effects after a single CPU clock
// cycle elapses. If the CPU is executing code from the prg0 bank
// (say, Zelda @ $BFAF), the next instruction fetched could be from an
// invalid bank and fail. A delay here lets that cycle fetch from the
// known valid (old) bank.

always @(posedge CLK) if (CP2_NEGEDGE) begin
  prg0_d <= prg0;
end

always @* begin
  // PRG ROM bank mode
  PRG_A = 4'hx;
  case (ctrl[3:2])
    2'b00, 2'b01: PRG_A[17:14] = {prg0_d[3:1], A[14]};
    2'b10: begin
      case (A[14])
        1'b0: PRG_A = 4'h0;
        1'b1: PRG_A = prg0_d[3:0];
        default: ;
      endcase
    end
    2'b11: begin
      case (A[14])
        1'b0: PRG_A = prg0_d[3:0];
        1'b1: PRG_A = 4'hF;
        default: ;
      endcase
    end
    default: ;      
  endcase
end

always @* begin
  // CHR ROM/RAM bank mode
  CHR_A = 5'hxx;
  case (ctrl[4])
    1'b0: CHR_A[16:12] = {chr0[4:1], BA[12]};
    1'b1: begin
      case (BA[12])
        1'b0: CHR_A = chr0;
        1'b1: CHR_A = chr1;
        default: ;
      endcase
    end
    default: ;
  endcase
end

assign PRG0 = prg0;
assign CHR0 = chr0;
assign CHR1 = chr1;

always @* begin
  // Mirroring
  case (ctrl[1:0])
    2'b00: CIRAM_A10 = 1'b0;
    2'b01: CIRAM_A10 = 1'b1;
    2'b10: CIRAM_A10 = BA[10];
    2'b11: CIRAM_A10 = BA[11];
    default: CIRAM_A10 = 1'bx;
  endcase
end

assign PRG_CS = CP2 & ~(PRGROM_nOE | WRAM_CS | load_sel);
assign WRAM_CS = CP2 & PRGROM_nOE & A[14:13] == 2'b11;

endmodule
