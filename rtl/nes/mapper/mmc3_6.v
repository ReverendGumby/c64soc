//////////////////////////////////////////////////////////////////////
// MMC3/6 mapper
//
// Reference: https://www.nesdev.org/wiki/MMC3
//
// Banks:
//   CPU $6000-$7FFF: (8K) PRG RAM bank (optional)
//   CPU $8000-$9FFF: (8K) switchable bank, or fixed to the second-last bank
//   CPU $A000-$BFFF: (8K) switchable bank
//   CPU $C000-$DFFF: (8K) fixed to the second-last bank, or switchable bank
//   CPU $E000-$FFFF: (8K) fixed to the last bank
//   PPU $0000-$07FF (or $1000-$17FF): (2K) switchable bank
//   PPU $0800-$0FFF (or $1800-$1FFF): (2K) switchable bank
//   PPU $1000-$13FF (or $0000-$03FF): (1K) switchable bank
//   PPU $1400-$17FF (or $0400-$07FF): (1K) switchable bank
//   PPU $1800-$1BFF (or $0800-$0BFF): (1K) switchable bank
//   PPU $1C00-$1FFF (or $0C00-$0FFF): (1K) switchable bank

module mmc3_6
  (
   // Clocks / resets
   input              CLK,
   input              nRES, // MLB reset
   input              RES_COLD,
   input              CP1_NEGEDGE,
   input              CP2,
   input              CP2_NEGEDGE,
   input              M2_NEGEDGE,

   // System control
   input              HOLD,

   // Cartridge inputs/outputs
   input              RW, // pin 14
   input [7:0]        D,
   input [14:13]      A,
   input              A0,
   input              PRGROM_nOE, // pin 50
   input              nBREAD, // pin 21
   input              nBWRITE, // pin 56
   input [12:10]      BA,
   input              CHAROM_nCE, // pin 65
   output             nIRQ,

   // Switchable bank outputs
   output [18:13]     PRG0,
   output [18:13]     PRG1,
   output [17:10]     CHR0,
   output [17:10]     CHR1,
   output [17:10]     CHR2,
   output [17:10]     CHR3,
   output [17:10]     CHR4,
   output [17:10]     CHR5,

   // ROM outputs
   output reg [18:13] PRG_A, // remapped PRG ROM address
   output reg [17:10] CHR_A, // remapped CHR ROM address
   output reg         CIRAM_A10, // VRAM A[10]
   output             PRG_CS, // PRG ROM /ROMSEL
   output             WRAM_CS // WRAM (PRG RAM) +CE
   );

// Bank select ($8000-$9FFF & A[0]=0)
reg [2:0] bs_r;                 // bank register select
reg       bs_m;                 // [MMC6]
reg       bs_p;                 // PRG ROM bank mode
reg       bs_c;                 // CHR A12 inversion

// Bank data ($8000-$9FFF & A[0]=1)
reg [7:0] bd [0:7];
reg [7:0] bd_d [6:7];

// Mirroring ($A000-$BFFF & A[0]=0)
reg       m_m;                  // Nametable mirroring

// IRQ latch ($C000-$DFFF & A[0]=0)
reg [7:0] il;

// IRQ enable ($E000-$FFFF, IE = A[0])
reg       ie;

wire [2:0] reg_sel = {A[14:13], A0};
wire       reg_we = ~PRGROM_nOE & ~RW;

always @(posedge CLK) if (CP2_NEGEDGE) begin
  if (~nRES & RES_COLD) begin
    bs_r = 3'b0;
    bs_m = 1'b0;
    bs_p = 1'b0;
    bs_c = 1'b0;
    bd[0] = 8'b0;
    bd[1] = 8'b0;
    bd[2] = 8'b0;
    bd[3] = 8'b0;
    bd[4] = 8'b0;
    bd[5] = 8'b0;
    bd[6] = 8'b0;
    bd[7] = 8'b0;
    m_m = 1'b0;
    il = 8'b0;
    ie = 1'b0;
  end
  else begin
    if (reg_we)
      case (reg_sel)
        3'b000: {bs_c, bs_p, bs_m, bs_r} <= {D[7:5], D[2:0]};
        3'b001: bd[bs_r] <= D;
        3'b010: m_m <= D[0];
        3'b100: il <= D;
        3'b110: ie <= 1'b0;
        3'b111: ie <= 1'b1;
        default: ;
      endcase
  end
end

wire irq_reload = (reg_sel == 3'b101) && reg_we && CP2_NEGEDGE;
wire irq_clear = (reg_sel == 3'b110) && reg_we && CP2_NEGEDGE;

// When a PRG bank change causes the cart to hold (so the system can
// swap in the new bank), the hold take effects after a single CPU
// clock cycle elapses. If the CPU is executing code from the bank,
// the next instruction fetched could be from an invalid bank and
// fail. A delay here lets that cycle fetch from the known valid (old)
// bank.

always @(posedge CLK) if (!HOLD && CP2_NEGEDGE) begin
  bd_d[6] <= bd[6];
  bd_d[7] <= bd[7];
end

always @* begin
  // PRG ROM bank mode
  PRG_A = 6'hx;
  case (A[14:13])
    2'b00: PRG_A = bs_p ? 6'h3E : bd_d[6]; // $8000-$9FFF
    2'b01: PRG_A = bd_d[7];                // $A000-$BFFF
    2'b10: PRG_A = bs_p ? bd_d[6] : 6'h3E; // $C000-$DFFF
    2'b11: PRG_A = 6'h3F;                  // $E000-$FFFF
    default: ;
  endcase
end

always @* begin
  // CHR ROM bank mode
  CHR_A = 8'hx;
  casez ({bs_c ^ BA[12], BA[11:10]})
    3'b00z: CHR_A = {bd[0][7:1], BA[10]}; // $0000-$07FF
    3'b01z: CHR_A = {bd[1][7:1], BA[10]}; // $0800-$0FFF
    3'b100: CHR_A = bd[2];                // $1000-$03FF
    3'b101: CHR_A = bd[3];                // $1400-$07FF
    3'b110: CHR_A = bd[4];                // $1800-$0BFF
    3'b111: CHR_A = bd[5];                // $1C00-$0FFF
    default: ;
  endcase
end

assign PRG0 = bd[6];
assign PRG1 = bd[7];
assign CHR0 = bd[0];
assign CHR1 = bd[1];
assign CHR2 = bd[2];
assign CHR3 = bd[3];
assign CHR4 = bd[4];
assign CHR5 = bd[5];

always @* begin
  // Mirroring
  case (m_m)
    1'b0: CIRAM_A10 = BA[10];
    1'b1: CIRAM_A10 = BA[11];
    default: CIRAM_A10 = 1'bx;
  endcase
end

assign PRG_CS = CP2 & ~(PRGROM_nOE | WRAM_CS | reg_we);
assign WRAM_CS = CP2 & PRGROM_nOE & A[14:13] == 2'b11;

// Scanline counter, based on PPU A12 IRQ

reg [7:0] ic, ic_next;
reg       is;
reg [2:0] ba12_d;
reg       ba12_flt_d;

wire      ba12 = BA[12];

initial begin
  ic = 8'd0;
  is = 1'b0;
  ba12_d = 3'b0;
  ba12_flt_d = 1'b0;
end

always @(posedge CLK) if (!HOLD) begin
  if (M2_NEGEDGE)
    ba12_d <= {ba12_d[1:0], ba12};
  else
    ba12_d[0] <= ba12_d[0] | ba12;
end

wire ba12_flt = ba12 | |ba12_d;
wire ba12_flt_posedge = ba12_flt & ~ba12_flt_d;

always @*
  ic_next = (ic == 8'd0) ? il : ic - 1'd1;

always @(posedge CLK) if (!HOLD) begin
  ba12_flt_d <= ba12_flt;

  if (irq_reload)
    ic <= 8'd0;
  else if (ba12_flt_posedge)
    ic <= ic_next;

  // Emulate the "normal" or "new" behavior.
  if (irq_clear)
    is <= 1'b0;
  else if (ba12_flt_posedge & ~|ic_next & ie)
    is <= 1'b1;
end

assign nIRQ = ~(ie & is);

endmodule
