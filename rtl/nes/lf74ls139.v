module lf74ls139
  (
   input            A,
   input            B, 
   input            G,
   output reg [3:0] Y
   );

always @* begin
  casez ({G, B, A})
    3'b1??: Y = 4'b1111;
    3'b000: Y = 4'b1110;
    3'b001: Y = 4'b1101;
    3'b010: Y = 4'b1011;
    3'b011: Y = 4'b0111;
  endcase
end

endmodule
