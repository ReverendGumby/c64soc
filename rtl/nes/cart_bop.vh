// Requesting bus
localparam [1:0] BID_NONE = 2'd0;
localparam [1:0] BID_PRG = 2'd1; // from APU
localparam [1:0] BID_CHR = 2'd2; // from PPU

// Target chip / active chip select
localparam [1:0] CID_NONE = 2'd0;
localparam [1:0] CID_PRG_ROM = 2'd1;
localparam [1:0] CID_PRG_RAM = 2'd2;
localparam [1:0] CID_CHR = 2'd3; // CHR ROM or RAM
localparam [1:0] CID_MAX = CID_CHR;

// Bus operation
localparam [1:0] OP_NONE = 2'd0;
localparam [1:0] OP_RD = 2'd1;
localparam [1:0] OP_WR = 2'd2;

// Enumeration of valid bus / chip / operation combos
localparam [2:0] BCOID_IDLE = 3'd0;
localparam [2:0] BCOID_PRG_ROM_RD = 3'd1;
localparam [2:0] BCOID_PRG_RAM_RD = 3'd2;
localparam [2:0] BCOID_PRG_RAM_WR = 3'd3;
localparam [2:0] BCOID_CHR_RD = 3'd4;
localparam [2:0] BCOID_CHR_WR = 3'd5;
localparam [2:0] BCOID_MAX = BCOID_CHR_WR;

function [5:0] make_bop(input [1:0] bid, input [1:0] cid, input [1:0] op);
  make_bop = { bid, cid, op };
endfunction

function [1:0] get_bop_bid(input [5:0] bop);
  get_bop_bid = bop[5:4];
endfunction

function [1:0] get_bop_cid(input [5:0] bop);
  get_bop_cid = bop[3:2];
endfunction

function [1:0] get_bop_op(input [5:0] bop);
  get_bop_op = bop[1:0];
endfunction

