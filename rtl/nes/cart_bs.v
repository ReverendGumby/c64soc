//////////////////////////////////////////////////////////////////////
// Cartridge backing store
//
// RAM/ROM contents are stored here, written by the NES (if RAM) or
// nesbus master.
//
// Store is an array of banks. Upper address bits are bank selects.
//
// Each bank has two read/write ports.

module cart_bs #(parameter BANKS = 10,
                 parameter WIDTH = 32,
                 parameter AM = 12,
                 parameter BANK_AM = 9,
                 parameter COLS = 4)
  (
   input                  CLK,

   input                  EN_A,
   output reg [WIDTH-1:0] RDATA_A,
   input [WIDTH-1:0]      WDATA_A,
   input [AM:0]           ADDR_A,
   input [COLS-1:0]       WE_A,

   input                  EN_B,
   output reg [WIDTH-1:0] RDATA_B,
   input [WIDTH-1:0]      WDATA_B,
   input [AM:0]           ADDR_B,
   input [COLS-1:0]       WE_B
   );

function in_bank(input [AM:0] addr, input integer bank);
  in_bank = addr[AM:BANK_AM+1] == bank;
endfunction

wire [WIDTH-1:0] brdata_a [0:BANKS-1];
wire [WIDTH-1:0] brdata_b [0:BANKS-1];

reg [BANKS-1:0] en_a, en_b;
reg [BANKS-1:0] en_a_d, en_b_d;

genvar bi;
generate
  for (bi = 0; bi < BANKS; bi = bi + 1) begin :bank

    always @* begin
      en_a[bi] = EN_A && in_bank(ADDR_A, bi);
      en_b[bi] = EN_B && in_bank(ADDR_B, bi);
    end

    cart_dpram #(.WIDTH(WIDTH), .AM(BANK_AM), .COLS(COLS)) dpram
      (
       .CLK(CLK),

       .EN_A(en_a[bi]),
       .RDATA_A(brdata_a[bi]),
       .WDATA_A(WDATA_A),
       .ADDR_A(ADDR_A[BANK_AM:0]),
       .WE_A(WE_A),

       .EN_B(en_b[bi]),
       .RDATA_B(brdata_b[bi]),
       .WDATA_B(WDATA_B),
       .ADDR_B(ADDR_B[BANK_AM:0]),
       .WE_B(WE_B)
       );
  end
endgenerate

// Because RDATA is output on the following clock
always @(posedge CLK) begin
  en_a_d <= en_a;
  en_b_d <= en_b;
end

integer ba;
always @* begin
  RDATA_A = {WIDTH-1{1'bx}};
  RDATA_B = {WIDTH-1{1'bx}};
  for (ba = 0; ba < BANKS; ba = ba + 1) begin
    if (en_a_d[ba])
      RDATA_A = brdata_a[ba];
    if (en_b_d[ba])
      RDATA_B = brdata_b[ba];
  end
end

endmodule
