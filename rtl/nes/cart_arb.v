//////////////////////////////////////////////////////////////////////
// NES bus arbitration
//
// Merge accesses for PRG and CHR. CHR has priority access.

module cart_arb #(parameter PRG_AM = 8,
                  parameter CHR_AM = 8)
  (
   input            CLK,

   input [PRG_AM:0] PRG_A,
   input            PRG_RnW,
   input            PRG_ROM_EN,
   input            PRG_RAM_EN,
   input [CHR_AM:0] CHR_A,
   input            CHR_RnW,
   input            CHR_EN,

   output [5:0]     BOP
   );

`include "cart_bop.vh"

wire [BCOID_MAX:1] acts, news, reqs;

reg [5:0]         bop_lut [0:BCOID_MAX];

reg [PRG_AM+1:0]  prg_st, prg_st_last;
reg [CHR_AM+1:0]  chr_st, chr_st_last;
reg               prg_st_new, chr_st_new;

reg [BCOID_MAX:1] acts_last;
reg [BCOID_MAX:1] pends;
reg [BCOID_MAX:1] acks, acks_set, acks_clr;
reg [2:0]         bcoid;
reg [BCOID_MAX:1] bcoids; // one-hot of bcoid

initial begin
  bop_lut[BCOID_IDLE] = make_bop(BID_NONE, CID_NONE, OP_NONE);
  bop_lut[BCOID_PRG_ROM_RD] = make_bop(BID_PRG, CID_PRG_ROM, OP_RD);
  bop_lut[BCOID_PRG_RAM_RD] = make_bop(BID_PRG, CID_PRG_RAM, OP_RD);
  bop_lut[BCOID_PRG_RAM_WR] = make_bop(BID_PRG, CID_PRG_RAM, OP_WR);
  bop_lut[BCOID_CHR_RD] = make_bop(BID_CHR, CID_CHR, OP_RD);
  bop_lut[BCOID_CHR_WR] = make_bop(BID_CHR, CID_CHR, OP_WR);
end

initial begin
  pends = {BCOID_MAX{1'b0}};
  acks = {BCOID_MAX{1'b0}};
  bcoid = BCOID_IDLE;
end

// HACK: Delay two clocks for the data bus to go valid
// TODO: Remove the delay hacks
reg [1:0] prg_ram_we_d;
reg [1:0] chr_we_d;
wire      prg_ram_we = PRG_RAM_EN & ~PRG_RnW;
wire      chr_we = CHR_EN & ~CHR_RnW;
always @(posedge CLK) begin
  prg_ram_we_d <= {prg_ram_we_d[0], prg_ram_we};
  chr_we_d <= {chr_we_d[0], chr_we};
end

// Gather the active requestors.
assign acts[BCOID_PRG_ROM_RD] = PRG_ROM_EN & PRG_RnW;
assign acts[BCOID_PRG_RAM_RD] = PRG_RAM_EN & PRG_RnW;
assign acts[BCOID_PRG_RAM_WR] = &prg_ram_we_d & prg_ram_we;
assign acts[BCOID_CHR_RD] = CHR_EN & CHR_RnW;
assign acts[BCOID_CHR_WR] = &chr_we_d & chr_we;

always @(posedge CLK)
  acts_last <= acts;

// Track each bus's state (address, R/W). A change in state while the
// requestor is active (including the requestor going active) signals
// a request.
initial begin
  prg_st_last = {PRG_AM+2{1'b0}};
  chr_st_last = {CHR_AM+2{1'b0}};
end

always @(posedge CLK) begin
  prg_st_last <= prg_st;
  chr_st_last <= chr_st;
end

always @* begin
  prg_st = { PRG_RnW, PRG_A };
  chr_st = { CHR_RnW, CHR_A };
  // Simulation uses x's, so use !== to be safe
  prg_st_new = prg_st !== prg_st_last;
  chr_st_new = chr_st !== chr_st_last;
end

// Gather the new request signals.
assign news[BCOID_PRG_ROM_RD] = prg_st_new;
assign news[BCOID_PRG_RAM_RD] = prg_st_new;
assign news[BCOID_PRG_RAM_WR] = prg_st_new;
assign news[BCOID_CHR_RD] = chr_st_new;
assign news[BCOID_CHR_WR] = chr_st_new;

assign reqs = acts & (news | ~acts_last);

// Pend's are set on request, and cleared when the op completes.
always @* begin
  pends = reqs | (acts & ~acks);
end

// Ack's are set when the op completes, and cleared when the requestor
// deactivates, or a request does not immediately complete.
always @* begin
  bcoids = {BCOID_MAX{1'b0}};
  bcoids[bcoid] = 1'b1;
end

always @* begin
  acks_set = pends & bcoids;
  acks_clr = acks & (~acts | (reqs & ~bcoids));
end

integer aid;
always @(posedge CLK)
  acks <= (acks & ~acks_clr) | acks_set;

// Determine the next course of action.
always @* begin
  if (pends[BCOID_CHR_RD])
    bcoid = BCOID_CHR_RD;
  else if (pends[BCOID_CHR_WR])
    bcoid = BCOID_CHR_WR;
  else if (pends[BCOID_PRG_ROM_RD])
    bcoid = BCOID_PRG_ROM_RD;
  else if (pends[BCOID_PRG_RAM_RD])
    bcoid = BCOID_PRG_RAM_RD;
  else if (pends[BCOID_PRG_RAM_WR])
    bcoid = BCOID_PRG_RAM_WR;
  else
    bcoid = BCOID_IDLE;
end

assign BOP = bop_lut[bcoid];

endmodule
