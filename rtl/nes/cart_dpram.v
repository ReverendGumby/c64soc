//////////////////////////////////////////////////////////////////////
// Cartridge dual-port RAM
//
// Implement a dual-port, read-first RAM with byte write. Cartridge
// RAM/ROM contents for a single 4KB bank will be are stored here.

module cart_dpram #(parameter WIDTH = 32,
                    parameter AM = 9,
                    parameter COLS = 4
                    )
  (
   input                  CLK,

   input                  EN_A,
   output reg [WIDTH-1:0] RDATA_A,
   input [WIDTH-1:0]      WDATA_A,
   input [AM:0]           ADDR_A,
   input [COLS-1:0]       WE_A,

   input                  EN_B,
   output reg [WIDTH-1:0] RDATA_B,
   input [WIDTH-1:0]      WDATA_B,
   input [AM:0]           ADDR_B,
   input [COLS-1:0]       WE_B
   );

localparam SIZE_BYTES = 4096;

localparam SIZE = SIZE_BYTES / COLS;
localparam COL_WIDTH = WIDTH / COLS;

reg [WIDTH-1:0] mem [0:SIZE-1];

generate
  initial begin: zero_init
  integer ini;
    for (ini = 0; ini < SIZE; ini = ini + 1)
      mem[ini] = {WIDTH{1'b0}};
  end
endgenerate

always @(posedge CLK)
  if (EN_A)
    RDATA_A <= mem[ADDR_A];

always @(posedge CLK)
  if (EN_B)
    RDATA_B <= mem[ADDR_B];

generate
genvar i;
  for (i = 0; i < COLS; i = i + 1) begin: write
    always @(posedge CLK)
      if (EN_A)
        if (WE_A[i]) 
          mem[ADDR_A][(i+1)*COL_WIDTH-1:i*COL_WIDTH] <= WDATA_A[(i+1)*COL_WIDTH-1:i*COL_WIDTH];

    always @(posedge CLK)
      if (EN_B)
        if (WE_B[i]) 
          mem[ADDR_B][(i+1)*COL_WIDTH-1:i*COL_WIDTH] <= WDATA_B[(i+1)*COL_WIDTH-1:i*COL_WIDTH];
  end
endgenerate

endmodule
