// In-Circuit Debugger (not original to the NES)
// Match unit: triggers on a system state matching an externally provided template
// SYS control unit: halts system on match (or forced halt) by asserting SYS_HOLD

module icd #(parameter NUM_MATCH=8)
  (
   input                      nRES,
   input                      CLK,
   input                      CP1_POSEDGE,
   input                      CP2_NEGEDGE,
   input                      CC,
   input                      HOLD,

   // System state
   input [15:0]               CPU_A,
   input [7:0]                CPU_DB,
   input                      CPU_RW,
   input                      CPU_RDY,
   input                      CPU_SYNC,
   input [31:0]               CPU_CYC,
   input [8:0]                PPU_COL,
   input [8:0]                PPU_ROW,
   input [19:0]               PPU_FRAME,

   // Match units
   input [NUM_MATCH-1:0]      MATCH_ENABLE,
   output reg [NUM_MATCH-1:0] MATCH_TRIGGER,
   input [2:0]                MATCH_SEL,
   input [3:0]                MATCH_REG_SEL,
   input [3:0]                MATCH_REG_WSTRB,
   input [31:0]               MATCH_REG_DIN,
   output [31:0]              MATCH_REG_DOUT,

   // System control unit
   input                      SYS_ENABLE,
   input                      SYS_FORCE_HALT,
   input                      SYS_RESUME, // edge-triggered
   output reg                 SYS_HOLD
   );

reg [31:0] match_status [0:NUM_MATCH-1];
reg [63:0] match_din [0:NUM_MATCH-1];
reg [63:0] match_den [0:NUM_MATCH-1];
reg [95:0] match_dout [0:NUM_MATCH-1];
reg [31:0] match_rdata [0:NUM_MATCH-1];

reg [8:0]  ppu_col_cp1;
reg [8:0]  ppu_row_cp1;
reg [19:0] ppu_frame_cp1;

reg [31:0] state_h, state_l;
reg        resume_dly;

wire [63:0] match_out [0:NUM_MATCH-1];

// To help things look like the many SW emulators we're using as
// reference, report the PPU state at the START of the instruction
// (CP1+) instead of the END (CP2-). Because PPU_* update right on
// CP1+, delay capture by one CLK.
reg cp1_posedge_d;
always @(posedge CLK)
  cp1_posedge_d <= CP1_POSEDGE;

always @(posedge CLK) if (cp1_posedge_d) begin
  ppu_col_cp1 <= PPU_COL;
  ppu_row_cp1 <= PPU_ROW;
  ppu_frame_cp1 <= PPU_FRAME;
end

//``REGION_REGS NESBUS_CTRL
//``REG ICD_MATCH_REG_DL
always @* begin
  state_l[15:0] = CPU_A;
  state_l[23:16] = CPU_DB;
  state_l[24]    = CPU_RW;
  state_l[25]    = CPU_SYNC;
  state_l[31:26] = ppu_frame_cp1[19:14]; //``FIELD PPU_FRAME
end
//``REG_END
//``REG ICD_MATCH_REG_DH
always @* begin
  state_h[8:0]   = ppu_col_cp1;         //``FIELD PPU_COL
  state_h[17:9]  = ppu_row_cp1;         //``FIELD PPU_ROW
  state_h[31:18] = ppu_frame_cp1[13:0]; //``FIELD PPU_FRAME
end
//``REG_END

wire [63:0] state = {state_h, state_l};

// Match state isn't always valid on (CP2_NEGEDGE | CC). For instance,
// CPU state is only valid near CP2_NEGEDGE. So, at the expense of
// matching on every PPU_COL (which changes every CC), let's just test
// matching when CPU state is valid.

wire clken = CP2_NEGEDGE;

generate;
genvar i;
  for (i = 0; i < NUM_MATCH; i = i + 1) begin :match

    initial begin
      match_status[i] = 0;
      match_din[i] = 0;
      match_den[i] = 0;
    end

    //``REG ICD_MATCH_REG_STATUS
    initial begin
      match_status[i][3:0] = i[3:0];         //``FIELD MATCH_SEL
      match_status[i][7:4] = NUM_MATCH[3:0]; //``FIELD NUM_MATCH
    end
    //``REG_END

    // state      x0101
    // match_din  x0011
    // match_den  01111
    // ---------  -----
    // match_out  11001

    assign match_out[i] = ~((state ^ match_din[i]) & match_den[i]);

    always @(posedge CLK) begin
      if (!nRES || !MATCH_ENABLE[i]) begin
        MATCH_TRIGGER[i] <= 1'b0;
        match_dout[i] <= 96'b0;
      end
      else if (clken) begin
        if (!MATCH_TRIGGER[i] && CPU_RDY && &match_out[i] && !HOLD) begin
          MATCH_TRIGGER[i] <= 1'b1;
          match_dout[i][63:00] <= state;
          match_dout[i][95:64] <= CPU_CYC;
        end
      end
    end

    always @(posedge CLK) begin
      if (MATCH_SEL == i) begin
        casex (MATCH_REG_SEL)
          4'h2: begin
            if (MATCH_REG_WSTRB[0]) match_din[i][07:00] <= MATCH_REG_DIN[07:00];
            if (MATCH_REG_WSTRB[1]) match_din[i][15:08] <= MATCH_REG_DIN[15:08];
            if (MATCH_REG_WSTRB[2]) match_din[i][23:16] <= MATCH_REG_DIN[23:16];
            if (MATCH_REG_WSTRB[3]) match_din[i][31:24] <= MATCH_REG_DIN[31:24];
          end
          4'h3: begin
            if (MATCH_REG_WSTRB[0]) match_din[i][39:32] <= MATCH_REG_DIN[07:00];
            if (MATCH_REG_WSTRB[1]) match_din[i][47:40] <= MATCH_REG_DIN[15:08];
            if (MATCH_REG_WSTRB[2]) match_din[i][55:48] <= MATCH_REG_DIN[23:16];
            if (MATCH_REG_WSTRB[3]) match_din[i][63:56] <= MATCH_REG_DIN[31:24];
          end
          4'h4: begin
            if (MATCH_REG_WSTRB[0]) match_den[i][07:00] <= MATCH_REG_DIN[07:00];
            if (MATCH_REG_WSTRB[1]) match_den[i][15:08] <= MATCH_REG_DIN[15:08];
            if (MATCH_REG_WSTRB[2]) match_den[i][23:16] <= MATCH_REG_DIN[23:16];
            if (MATCH_REG_WSTRB[3]) match_den[i][31:24] <= MATCH_REG_DIN[31:24];
          end
          4'h5: begin
            if (MATCH_REG_WSTRB[0]) match_den[i][39:32] <= MATCH_REG_DIN[07:00];
            if (MATCH_REG_WSTRB[1]) match_den[i][47:40] <= MATCH_REG_DIN[15:08];
            if (MATCH_REG_WSTRB[2]) match_den[i][55:48] <= MATCH_REG_DIN[23:16];
            if (MATCH_REG_WSTRB[3]) match_den[i][63:56] <= MATCH_REG_DIN[31:24];
          end
        endcase
      end
    end

    always @* begin
      match_rdata[i] = 32'h00000000;
      casex (MATCH_REG_SEL)                          //``SUBREGS ICD
        4'h0: match_rdata[i] = match_status[i];      //``REG STATUS
        4'h2: match_rdata[i] = match_din[i][31:00];  //``REG DLIN
        4'h3: match_rdata[i] = match_din[i][63:32];  //``REG DHIN
        4'h4: match_rdata[i] = match_den[i][31:00];  //``REG DLEN
        4'h5: match_rdata[i] = match_den[i][63:32];  //``REG DHEN
        4'h6: match_rdata[i] = match_dout[i][31:00]; //``REG DLOUT
        4'h7: match_rdata[i] = match_dout[i][63:32]; //``REG DHOUT
        4'h8: match_rdata[i] = match_dout[i][95:64]; //``REG D2OUT
        default: ;
      endcase
    end

  end
endgenerate;

assign MATCH_REG_DOUT = match_rdata[MATCH_SEL];

initial SYS_HOLD = 1'b0;

always @(posedge CLK) begin
  resume_dly <= SYS_RESUME;

  if (!nRES || !SYS_ENABLE)
    SYS_HOLD <= 1'b0;
  else begin
    if (SYS_FORCE_HALT)
      SYS_HOLD <= 1'b1;
    else if (!resume_dly && SYS_RESUME)
      SYS_HOLD <= 1'b0;
    else if (MATCH_TRIGGER)
      SYS_HOLD <= 1'b1;
  end
end

endmodule
