module todclkgen
  (
   input      CLKIN,            // 8196721 Hz
   output reg CLKOUT            // 60 Hz
   );

reg [17:0] clk_cnt;

initial begin
  clk_cnt = 1'd0;
  CLKOUT = 1'b0;
end

always @(posedge CLKIN) begin
  if (clk_cnt == 18'd68305) begin
    clk_cnt <= 1'd0;
    CLKOUT <= ~CLKOUT;
  end else
    clk_cnt <= clk_cnt + 1'd1;
end

endmodule
