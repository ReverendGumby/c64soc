module cart
  (
   // C64 clocks
   input             CLK, // c64 DOT_CLOCK: 8.192 MHz
   input             CP1_POSEDGE,
   //input             CP2,
   input             BCLK_NEGEDGE,

   // Cartridge / expansion slot
   input             nRES, // C64 mainboard reset
   input             RW,
   output            nIRQ,
   input             BA,
   output            nDMA,
   input [7:0]       D_I,
   output [7:0]      D_O,
   output            D_OE,
   input [15:0]      A,
   output            nEXROM,
   output            nGAME,
   input             nROMH, // $A000 or $E000
   input             nROML, // $8000
   input             nIIO2, // $DF00
   input             nIIO1, // $DE00
   input             HOLD, // halt system and release A,D,RW

   // Control register interface
   input             CTRL_RSTn,
   input [11:2]      CTRL_ADDR, // control interface
   input [3:0]       CTRL_WSTRB,
   input [31:0]      CTRL_DIN,
   output reg [31:0] CTRL_DOUT,

   // Backing store read interface
   input [14:2]      BS_ARADDR,
   input             BS_ARVALID,
   output [31:0]     BS_RDATA,
   input             BS_RREADY,
   output reg        BS_RVALID,

   // Backing store write interface
   input [14:2]      BS_AWADDR,
   input [3:0]       BS_WSTRB,
   input [31:0]      BS_WDATA,
   input             BS_WVALID,
   output reg        BS_BVALID
   );


//////////////////////////////////////////////////////////////////////
// Cartridge backing store
//
// ROM contents are stored here, written by the c64bus master. It's
// divided into separate banks of virtual 8K ROM chips at fixed
// addresses:
//
// bank   bs address
// ----   ----------
// 0      $0000 - $1FFF
// 1      $2000 - $3FFF
// 2      $4000 - $5FFF
// 3      $6000 - $7FFF

localparam BANK_ADDR_LEN = 13;  // 8K
localparam BS_ADDR_LEN = 15;    // 32K
localparam BS_BANK_ADDR_LEN = BS_ADDR_LEN - BANK_ADDR_LEN;
localparam BS_WIDTH = 32;
localparam BS_SIZE = (1 << BS_ADDR_LEN) / (BS_WIDTH / 8);

reg [BS_WIDTH-1:0] bs [0:BS_SIZE-1];


//////////////////////////////////////////////////////////////////////
// Backing store write interface
//
// The c64bus master uses this to write the backing store.

always @(posedge CLK) begin
  BS_BVALID <= 1'b0;
  if (BS_WVALID && !BS_BVALID) begin
    BS_BVALID <= 1'b1;
    if (BS_WSTRB[0]) bs[BS_AWADDR][07:00] <= BS_WDATA[07:00];
    if (BS_WSTRB[1]) bs[BS_AWADDR][15:08] <= BS_WDATA[15:08];
    if (BS_WSTRB[2]) bs[BS_AWADDR][23:16] <= BS_WDATA[23:16];
    if (BS_WSTRB[3]) bs[BS_AWADDR][31:24] <= BS_WDATA[31:24];
  end
end


//////////////////////////////////////////////////////////////////////
// Virtual I/O device
//
// Emulates a device that responds to I/O chip select nIIO1
// ($DE00-$DEFF). Its purpose and behavior varies by cartridge
// type. Two main and independent functions are provided:
//
// 1. 'viod_bs': On cartridges with more than two banks, a write
// selects which bank is 'active', and thus addressed when nROML/nROMH
// are asserted. Outputs are 'viod_bs_bal' (write bits n-0) and
// 'viod_bs_bah' (TBD).

reg viod_bs;
reg [BS_BANK_ADDR_LEN-1:0] viod_bs_bah, viod_bs_bal;

always @(posedge CLK) if (BCLK_NEGEDGE) begin
  if (!nRES)
    viod_bs_bal <= 1'd0;
  else if (!HOLD && viod_bs) begin
    if (!nIIO1 && !RW)
      viod_bs_bal <= D_I[BS_BANK_ADDR_LEN-1:0];
  end
end

always @*
  viod_bs_bah = viod_bs_bal;    // TBD: fix this later

// 2. 'viod_nexrom_d7': nEXROM is set or cleared by a write: nEXROM =
// D7. It is asserted on reset to map in the cartridge. Output is
// 'viod_nexrom'.

reg viod_nexrom_d7;
reg viod_nexrom;

always @(posedge CLK) if (BCLK_NEGEDGE) begin
  if (!nRES)
    viod_nexrom <= 1'd0;
  else if (!HOLD && viod_nexrom_d7) begin
    if (!nIIO1 && !RW)
      viod_nexrom <= D_I[7];
  end
end


//////////////////////////////////////////////////////////////////////
// Cartridge control signal generator
//
// nEXROM and nGAME are driven by the cartridge.
//
// nEXROM is either set to a static value ('ccsg_nexrom_static') or
// driven by the virtual I/O device, depending on 'ccsg_dynamic'.
//
// nGAME is always set to 'ccsg_ngame_static'.

reg ccsg_enable;                // master enable for the cartridge
reg ccsg_dynamic;
reg ccsg_nexrom_static, ccsg_ngame_static;
reg ccsg_nexrom, ccsg_ngame;

always @* begin
  if (ccsg_enable) begin
    ccsg_nexrom = ccsg_dynamic ? viod_nexrom : ccsg_nexrom_static;
    ccsg_ngame = ccsg_ngame_static;
  end
  else begin
    ccsg_nexrom = 1'b1;
    ccsg_ngame = 1'b1;
  end
end


//////////////////////////////////////////////////////////////////////
// Backing store address generator
//
// Translates from cartridge chip selects (nROMH, nROML) to bank
// address. One address is generated for each chip select.
//
// There's two modes of operation:
//
// Static (bsag_dynamic = 0): nROML = bank 0, nROMH = bank 1.
//
// Dynamic (bsag_dynamic = 1): nROML = bank 'bal', where 'bal' is set
// by the virtual I/O device. Same for nROMH and 'bah'.

reg bsag_dynamic;
reg [BS_BANK_ADDR_LEN-1:0] bsag_ba_romh, bsag_ba_roml;

always @* begin
  if (bsag_dynamic) begin
    bsag_ba_romh = viod_bs_bah;
    bsag_ba_roml = viod_bs_bal;
  end
  else begin
    bsag_ba_romh = 1'd1;
    bsag_ba_roml = 1'd0;
  end
end


//////////////////////////////////////////////////////////////////////
// Control register read interface

always @* begin                 //``REGION_REGS CART_CTRL
  CTRL_DOUT = 32'h0000;
  case (CTRL_ADDR)
    10'h000: begin              //``REG CCSG0
      CTRL_DOUT[0] = ccsg_enable;
    end
    10'h001: begin              //``REG CCSG1
      CTRL_DOUT[0] = ccsg_ngame_static;
      CTRL_DOUT[1] = ccsg_nexrom_static;
      CTRL_DOUT[4] = ccsg_dynamic;
      CTRL_DOUT[16] = ccsg_ngame;
      CTRL_DOUT[17] = ccsg_nexrom;
    end
    10'h002: begin              //``REG BSAG0
      CTRL_DOUT[0] = bsag_dynamic;
    end
    10'h003: begin              //``REG BSAG1
      CTRL_DOUT[3:0] = bsag_ba_roml;
      CTRL_DOUT[7:4] = bsag_ba_romh;
    end
    10'h004: begin              //``REG VIOD0
      CTRL_DOUT[0] = viod_bs;
      CTRL_DOUT[19:16] = viod_bs_bal;
      CTRL_DOUT[23:20] = viod_bs_bah;
    end
    10'h005: begin              //``REG VIOD1
      CTRL_DOUT[0] = viod_nexrom_d7;
      CTRL_DOUT[3] = viod_nexrom;
    end
  endcase
end


//////////////////////////////////////////////////////////////////////
// Control register write interface

always @(posedge CLK or negedge CTRL_RSTn) begin
  if (!CTRL_RSTn) begin
    ccsg_enable <= 1'b0;
    ccsg_nexrom_static <= 1'b1;
    ccsg_ngame_static <= 1'b1;
    ccsg_dynamic <= 1'b0;
    bsag_dynamic <= 1'b0;
    viod_bs <= 1'b0;
    viod_nexrom_d7 <= 1'b0;
  end
  else
    case (CTRL_ADDR)
      10'h000: begin
        if (CTRL_WSTRB[0])
          ccsg_enable <= CTRL_DIN[0];
      end
      10'h001: begin
        if (CTRL_WSTRB[0]) begin
          ccsg_ngame_static <= CTRL_DIN[0];
          ccsg_nexrom_static <= CTRL_DIN[1];
          ccsg_dynamic <= CTRL_DIN[4];
        end
      end
      10'h002: begin
        if (CTRL_WSTRB[0])
          bsag_dynamic <= CTRL_DIN[0];
      end
      10'h004: begin
        if (CTRL_WSTRB[0])
          viod_bs <= CTRL_DIN[0];
      end
      10'h005: begin
        if (CTRL_WSTRB[0])
          viod_nexrom_d7 <= CTRL_DIN[0];
      end
    endcase
end


//////////////////////////////////////////////////////////////////////
// Backing store read interface
//
// Finally, a C64 bus read request is addressed to the backing store.

reg [BS_BANK_ADDR_LEN-1:0] bsri_bank;
wire [BS_ADDR_LEN-1:2]     bsri_addr;
wire [BS_ADDR_LEN-1:2]     bsri_addr_c64;
reg [BS_WIDTH-1:0]         bsri_rdata;
reg [7:0]                  bsri_dout;

always @* begin
  case ({nROMH, nROML})
    2'b10: bsri_bank = bsag_ba_roml;
    2'b01: bsri_bank = bsag_ba_romh;
    default: bsri_bank = {BS_BANK_ADDR_LEN{1'bx}};
  endcase
end

assign bsri_addr_c64 = { bsri_bank, A[BANK_ADDR_LEN-1:2] };

// The c64bus master and C64 bus share the backing store's read port.
// Since its timing requirements are fixed (bclk falling edge),
// priority is given to the C64 bus.

assign bsri_addr = BCLK_NEGEDGE ? bsri_addr_c64 : BS_ARADDR[BS_ADDR_LEN-1:2];

always @(posedge CLK) begin
  bsri_rdata <= bs[bsri_addr];
end

always @(posedge CLK) begin
  BS_RVALID <= !BCLK_NEGEDGE && BS_ARVALID && BS_RREADY;
end

assign BS_RDATA = bsri_rdata;

always @* begin
  case (A[1:0])
    2'b00: bsri_dout = bsri_rdata[07:00];
    2'b01: bsri_dout = bsri_rdata[15:08];
    2'b10: bsri_dout = bsri_rdata[23:16];
    2'b11: bsri_dout = bsri_rdata[31:24];
    default: bsri_dout = 8'hxx;
  endcase
end


//////////////////////////////////////////////////////////////////////
// Cartridge / expansion slot interface

assign nGAME = ccsg_ngame;
assign nEXROM = ccsg_nexrom;
assign nDMA = 1'b1;
assign nIRQ = 1'b1;

reg  hold_dly;
always @(posedge CLK) if (CP1_POSEDGE)
  hold_dly <= HOLD;

reg [7:0] dout;
reg       dout_en;
always @* begin
  dout = 8'hxx;
  dout_en = 1'b0;
  if (!hold_dly && ccsg_enable && RW)
    if (!nROML || !nROMH) begin
      dout = bsri_dout;
      dout_en = 1'b1;
    end
end

assign D_O = dout;
assign D_OE = dout_en;

endmodule
