module c64bus
  (
   // C64 clocks
   input             CLK, // c64 DOT_CLOCK: 8.192 MHz
   input             CP1_POSEDGE,
   input             CP2_NEGEDGE,
   input             BCLK_POSEDGE,

   // AXI Lite slave bus interface
   input             ARESETn,
   input [31:0]      AWADDR,
   input             AWVALID,
   output reg        AWREADY,
   input [31:0]      WDATA,
   output reg        WREADY,
   input [3:0]       WSTRB,
   input             WVALID,
   output [1:0]      BRESP,
   output reg        BVALID,
   input             BREADY,
   input [31:0]      ARADDR,
   input             ARVALID,
   output reg        ARREADY,
   output reg [31:0] RDATA,
   output [1:0]      RRESP,
   output reg        RVALID,
   input             RREADY,

   // Keyboard / joystick matrix
   output [7:0]      COL_I,
   input [7:0]       COL_O,
   output [7:0]      ROW_I,
   input [7:0]       ROW_O,
   output            nRESTORE,

   // C64 clocks
   // Resets
   input             EXT_RES, // external (button) reset
   output reg        C64_nRES, // C64 mainboard reset
   output reg        CART_nRES, // cartridge emulation reset

   // Memory bus
   output reg        HOLD, // halt system and take over memory bus
   output            RW_O,
   input [7:0]       D_I,
   output [7:0]      D_O,
   output            D_OE,
   output [15:0]     A_O,
   output            A_OE,

   // Chip select override
   output reg        CSO_EN,
   output reg        CSO_CHAREN,
   output reg        CSO_HIRAM,
   output reg        CSO_LORAM,
   output reg        CSO_GAME,
   output reg        CSO_EXROM,
   input             CSO_MON_CHAREN,
   input             CSO_MON_HIRAM,
   input             CSO_MON_LORAM,
   input             CSO_MON_GAME,
   input             CSO_MON_EXROM,

   // CPU monitor and control interface
   output reg [4:0]  CPU_REG_SEL,
   input [7:0]       CPU_REG_DOUT,
   output reg        ICD_CPU_ENABLE,
   output reg        ICD_CPU_FORCE_HALT,
   output reg        ICD_CPU_RESUME,
   input             ICD_CPU_CLR_RDY,
   output reg        ICD_MATCH_ENABLE,
   output reg [31:0] ICD_MATCH_DIN,
   output reg [31:0] ICD_MATCH_DEN,
   input             ICD_MATCH_TRIGGER,
   input [31:0]      ICD_MATCH_DOUT,

   // ROM cartridge emulator, control interface
   output reg [11:2] CART_CTRL_ADDR,
   output reg [3:0]  CART_CTRL_WSTRB,
   output [31:0]     CART_CTRL_DIN,
   input [31:0]      CART_CTRL_DOUT,

   // ROM cartridge emulator, backing store interface
   output [14:2]     CART_BS_ARADDR,
   output            CART_BS_ARVALID,
   input [31:0]      CART_BS_RDATA,
   output            CART_BS_RREADY,
   input             CART_BS_RVALID,
   output [14:2]     CART_BS_AWADDR,
   output [3:0]      CART_BS_WSTRB,
   output [31:0]     CART_BS_WDATA,
   output reg        CART_BS_WVALID,
   input             CART_BS_BVALID,

   // General-purpose outputs
   output reg [7:0]  GPO
   );

reg [7:0]   kbd_matrix [0:7];
reg [4:0]   joy_a;
reg [4:0]   joy_b;
reg         restore;
reg         c64_usr_nres;
reg         cso_enable;

reg [7:0]   mem_rdata;
reg         mem_ractive, mem_wactive;
reg         mem_rvalid, mem_wvalid;
reg         mem_rreq, mem_wreq;

//////////////////////////////////////////////////////////////////////
// Register interface

// address range    size  bits   ussage
// -------------    ----  ----   ------
// 0_0000 - 0_0FFF   4K   8-32   c64bus control
// 0_1000 - 0_1FFF   4K   8-32   ROM cartridge control
// 0_2000 - 0_FFFF  56K      -   (reserved)
// 1_0000 - 1_FFFF  64K      8   C64 native memory
// 2_0000 - 2_7FFF  32K   8-32   ROM cartridge backing store

localparam RRSVD       = 4'b0000;
localparam C64BUS_CTRL = 4'b0001;
localparam C64_MEM     = 4'b0010;
localparam CART_CTRL   = 4'b0100;
localparam CART_BS     = 4'b1000;

function [3:0] AxADDR_region(input [31:0] AxADDR);
  begin
    casez (AxADDR[17:0])
//``REGION_START
      18'h0_0zzz: AxADDR_region = C64BUS_CTRL;
      18'h0_1zzz: AxADDR_region = CART_CTRL;
      18'h1_zzzz: AxADDR_region = C64_MEM;
      18'h2_zzzz: AxADDR_region = CART_BS;
//``REGION_END
      default:    AxADDR_region = RRSVD;
    endcase
  end
endfunction

reg [3:0] rregion, wregion;

always @* begin
  rregion = AxADDR_region(ARADDR);
  wregion = AxADDR_region(AWADDR);
end

reg [31:0] rdata;
reg        rvalid, bvalid;
reg        cart_rvalid;

always @* begin
  rdata = 32'h00000000;
  rvalid = 1'b1;
  if (rregion == C64BUS_CTRL) begin //``REGION_REGS
    casex (ARADDR[11:2])
      10'h0x: rdata = 32'h0c640c64;   //``REG ID DATA
      10'h10: begin                   //``REG KBD_MATRIX_0
        rdata[07:00] = kbd_matrix[0]; //``NO_FIELD
        rdata[15:08] = kbd_matrix[1]; //``NO_FIELD
        rdata[23:16] = kbd_matrix[2]; //``NO_FIELD
        rdata[31:24] = kbd_matrix[3]; //``NO_FIELD
      end
      10'h11: begin                   //``REG KBD_MATRIX_1
        rdata[07:00] = kbd_matrix[4]; //``NO_FIELD
        rdata[15:08] = kbd_matrix[5]; //``NO_FIELD
        rdata[23:16] = kbd_matrix[6]; //``NO_FIELD
        rdata[31:24] = kbd_matrix[7]; //``NO_FIELD
      end
      10'h12: begin             //``REG JOY_MATRIX
        rdata[04:00] = joy_a;   //``NO_FIELD
        rdata[12:08] = joy_b;   //``NO_FIELD
      end
      10'h13: begin             //``REG MISC_KEYS
        rdata[0] = restore;
      end
      10'h14: begin               //``REG RESETS
        rdata[0] = c64_usr_nres;  //``FIELD C64
        rdata[2] = CART_nRES;     //``FIELD CART
      end
      10'h16: begin                 //``REG CSO
        rdata[0] = CSO_CHAREN;
        rdata[1] = CSO_HIRAM;
        rdata[2] = CSO_LORAM;
        rdata[3] = CSO_GAME;
        rdata[4] = CSO_EXROM;
        rdata[15] = cso_enable;
        rdata[16] = CSO_MON_CHAREN;
        rdata[17] = CSO_MON_HIRAM;
        rdata[18] = CSO_MON_LORAM;
        rdata[19] = CSO_MON_GAME;
        rdata[20] = CSO_MON_EXROM;
      end
      10'h18: begin
        rdata[4:0] = CPU_REG_SEL;
      end
      10'h19: begin
        rdata[7:0] = CPU_REG_DOUT;
      end
      10'h1c: begin                    //``REG ICD_CTRL
        rdata[0] = ICD_CPU_ENABLE;     //``FIELD CPU_ENABLE
        rdata[1] = ICD_CPU_FORCE_HALT; //``FIELD CPU_FORCE_HALT
        rdata[2] = ICD_CPU_RESUME;     //``FIELD CPU_RESUME
        rdata[8] = ICD_CPU_CLR_RDY;    //``FIELD CPU_CLR_RDY
        rdata[16] = ICD_MATCH_ENABLE;  //``FIELD MATCH_ENABLE
        rdata[24] = ICD_MATCH_TRIGGER; //``FIELD MATCH_TRIGGER
      end
      10'h1d: rdata = ICD_MATCH_DIN;
      10'h1e: rdata = ICD_MATCH_DEN;
      10'h1f: rdata = ICD_MATCH_DOUT;
      10'h20: rdata = GPO;
      default: ;
    endcase
  end
  else if (rregion == C64_MEM) begin
    rdata = {4{mem_rdata}};
    rvalid = mem_rvalid;
  end
  else if (rregion == CART_CTRL) begin
    rvalid = cart_rvalid;
    rdata = CART_CTRL_DOUT;
  end
  else if (rregion == CART_BS) begin
    rvalid = CART_BS_RVALID;
    rdata = CART_BS_RDATA;
  end
end

always @(posedge CLK or negedge ARESETn) begin
  if (!ARESETn) begin
    ARREADY <= 1'b0;
    RVALID <= 1'b0;
  end
  else begin
    if (!ARREADY && ARVALID) begin
      if (rvalid) begin
        ARREADY <= 1'b1;
        RDATA <= rdata;
        RVALID <= 1'b1;
      end
    end
    else if (RVALID) begin
      ARREADY <= 1'b0;
      if (RREADY)
        RVALID <= 1'b0;
    end
  end
end

always @(posedge CLK or negedge ARESETn) begin
  if (!ARESETn) begin
    mem_rreq <= 1'b0;
  end
  else begin
    if (mem_ractive || mem_rvalid)
      mem_rreq <= 1'b0;
    else if (!ARREADY && ARVALID && rregion == C64_MEM)
      mem_rreq <= 1'b1;
  end
end

always @(posedge CLK or negedge ARESETn) begin
  if (!ARESETn) begin
    CART_CTRL_ADDR <= 10'h000;
    cart_rvalid <= 1'b0;
  end
  else begin
    cart_rvalid <= 1'b0;
    if (!ARREADY && ARVALID && rregion == CART_CTRL) begin
      CART_CTRL_ADDR <= ARADDR[11:2];
      cart_rvalid <= 1'b1;
    end
    else if (!AWREADY && AWVALID && wregion == CART_CTRL)
      CART_CTRL_ADDR <= AWADDR[11:2];
  end
end

assign CART_BS_ARADDR = ARADDR[14:2];
assign CART_BS_RREADY = RREADY;
assign CART_BS_ARVALID = ARVALID && rregion == CART_BS;

assign CART_BS_AWADDR = AWADDR[14:2];
assign CART_BS_WSTRB = WSTRB;
assign CART_BS_WDATA = WDATA;

assign RRESP = 2'b00;

always @* begin
  bvalid = 1'b1;
  if (wregion == C64_MEM)
    bvalid = mem_wvalid;
  else if (wregion == CART_BS)
    bvalid = CART_BS_BVALID;
end

always @(posedge CLK or negedge ARESETn) begin
  if (!ARESETn) begin
    AWREADY <= 1'b0;
    WREADY <= 1'b0;
    BVALID <= 1'b0;
  end
  else begin
    if (!AWREADY && AWVALID && !WREADY && WVALID && !BVALID) begin
      if (bvalid) begin
        AWREADY <= 1'b1;
        WREADY <= 1'b1;
        BVALID <= 1'b1;
      end
    end
    else if (BVALID) begin
      AWREADY <= 1'b0;
      WREADY <= 1'b0;
      if (BREADY)
        BVALID <= 1'b0;
    end
  end
end

assign BRESP = 2'b00;

always @(posedge CLK or negedge ARESETn) begin
  if (!ARESETn) begin
    kbd_matrix[0] <= 8'h00;
    kbd_matrix[1] <= 8'h00;
    kbd_matrix[2] <= 8'h00;
    kbd_matrix[3] <= 8'h00;
    kbd_matrix[4] <= 8'h00;
    kbd_matrix[5] <= 8'h00;
    kbd_matrix[6] <= 8'h00;
    kbd_matrix[7] <= 8'h00;
    joy_a <= 5'h00;
    joy_b <= 5'h00;
    restore <= 1'b0;
    c64_usr_nres <= 1'b0;
    CART_nRES <= 1'b0;
    CART_CTRL_WSTRB <= 4'h0;
    CART_BS_WVALID <= 1'b0;
    ICD_CPU_ENABLE <= 1'b0;
    ICD_CPU_FORCE_HALT <= 1'b0;
    ICD_CPU_RESUME <= 1'b0;
    ICD_MATCH_ENABLE <= 1'b0;
    ICD_MATCH_DIN <= 32'b0;
    ICD_MATCH_DEN <= 32'b0;
    CPU_REG_SEL <= 5'h0;
    CSO_CHAREN <= 1'b0;
    CSO_HIRAM <= 1'b0;
    CSO_LORAM <= 1'b0;
    CSO_GAME <= 1'b0;
    CSO_EXROM <= 1'b0;
    cso_enable <= 1'b0;
    GPO <= 8'b0;
  end
  else begin
    CART_CTRL_WSTRB <= 4'h0;
    CART_BS_WVALID <= 1'b0;
    if (!AWREADY && AWVALID && !WREADY && WVALID && !BVALID) begin
      if (wregion == C64BUS_CTRL) begin
        case (AWADDR[11:2])
          10'h10: begin
            if (WSTRB[0]) kbd_matrix[0] <= WDATA[07:00];
            if (WSTRB[1]) kbd_matrix[1] <= WDATA[15:08];
            if (WSTRB[2]) kbd_matrix[2] <= WDATA[23:16];
            if (WSTRB[3]) kbd_matrix[3] <= WDATA[31:24];
          end
          10'h11: begin
            if (WSTRB[0]) kbd_matrix[4] <= WDATA[07:00];
            if (WSTRB[1]) kbd_matrix[5] <= WDATA[15:08];
            if (WSTRB[2]) kbd_matrix[6] <= WDATA[23:16];
            if (WSTRB[3]) kbd_matrix[7] <= WDATA[31:24];
          end
          10'h12: begin
            if (WSTRB[0]) joy_a <= WDATA[04:00];
            if (WSTRB[1]) joy_b <= WDATA[12:08];
          end
          10'h13: begin
            if (WSTRB[0]) restore <= WDATA[0];
          end
          10'h14: begin
            if (WSTRB[0]) begin
              c64_usr_nres <= WDATA[0];
              CART_nRES <= WDATA[2];
            end
          end
          10'h16: begin
            if (WSTRB[0]) begin
              CSO_CHAREN <= WDATA[0];
              CSO_HIRAM <= WDATA[1];
              CSO_LORAM <= WDATA[2];
              CSO_GAME <= WDATA[3];
              CSO_EXROM <= WDATA[4];
            end
            if (WSTRB[3]) begin
              cso_enable <= WDATA[15];
            end
          end
          10'h18: begin
            if (WSTRB[0]) begin
              CPU_REG_SEL <= WDATA[4:0];
            end
          end
          10'h1c: begin
            if (WSTRB[0]) begin
              ICD_CPU_ENABLE <= WDATA[0];
              ICD_CPU_FORCE_HALT <= WDATA[1];
              ICD_CPU_RESUME <= WDATA[2];
            end
            if (WSTRB[2]) begin
              ICD_MATCH_ENABLE <= WDATA[16];
            end
          end
          10'h1d: begin
            if (WSTRB[0]) ICD_MATCH_DIN[07:00] <= WDATA[07:00];
            if (WSTRB[1]) ICD_MATCH_DIN[15:08] <= WDATA[15:08];
            if (WSTRB[2]) ICD_MATCH_DIN[23:16] <= WDATA[23:16];
            if (WSTRB[3]) ICD_MATCH_DIN[31:24] <= WDATA[31:24];
          end
          10'h1e: begin
            if (WSTRB[0]) ICD_MATCH_DEN[07:00] <= WDATA[07:00];
            if (WSTRB[1]) ICD_MATCH_DEN[15:08] <= WDATA[15:08];
            if (WSTRB[2]) ICD_MATCH_DEN[23:16] <= WDATA[23:16];
            if (WSTRB[3]) ICD_MATCH_DEN[31:24] <= WDATA[31:24];
          end
          10'h20: begin
            if (WSTRB[0]) begin
              GPO[7:0] <= WDATA[7:0];
            end
          end
        endcase
      end
      else if (wregion == CART_CTRL) begin
        CART_CTRL_WSTRB <= WSTRB;
      end
      else if (wregion == CART_BS) begin
        CART_BS_WVALID <= WVALID;
      end
    end
  end
end

assign CART_CTRL_DIN = WDATA;

always @(posedge CLK or negedge ARESETn) begin
  if (!ARESETn) begin
    mem_wreq <= 1'b0;
  end
  else begin
    if (mem_wactive || mem_wvalid)
      mem_wreq <= 1'b0;
    else if (!AWREADY && AWVALID && !WREADY && WVALID && !BVALID &&
             wregion == C64_MEM) begin
      mem_wreq <= 1'b1;
    end
  end
end

//////////////////////////////////////////////////////////////////////
// Keyboard / joystick matrix

//           ROW (input)                                                     Joy B
// (output)  7       6       5       4       3       2       1       0
//      COL
//      7    STOP    Q       C=      SPACE   2       CTRL    <-      1
//      6    /       ^       =       RSHIFT  HOME    ;       *       £
//      5    ,       @       :       .       -       L       P       +
//      4    N       O       K       M       0       J       I       9       Fire
//      3    V       U       H       B       8       G       Y       7       Right
//      2    X       T       F       C       6       D       R       5       Left
//      1    LSHIFT  E       S       Z       4       A       W       3       Down
//      0    CRSR DN F5      F3      F1      F7      CRSR RT RETURN  DELETE  Up
// Joy A                             Fire/LP Right   Left    Down    Up

// ROW_I and COL_I are the inputs observed by CIA #1. They are
// normally HIGH (pulled up by CIA), and driven LOW by either us
// (e.g., keyboard/joystick activity) or the CIA itself (O=0). A
// shorted input overrides the CIA's output and forces it LOW. (Think
// wired-AND.)
//
// A 1 in kbd_matrix[x][y] shorts ROW[x] and COL[y]. ROW is
// pulled-up input and COL is normally high output, so ROW[x] goes
// low if COL[y] == 0 && kbd_matrix[x][y] == 1.
//
// COL[y]   kbd_matrix[x][y] => ROW[x]
// 1        0                   1
// 0        0                   1
// 1        1                   1
// 0        1                   0
//
// A 1 in joy_a[x] shorts ROW[x] to GND.
// A 1 in joy_b[x] shorts COL[x] to GND.

// Combine joystick and CIA outputs before applying to keyboard rows.
// Skip the keyboard columns, to avoid colliding with joystick B.
wire [7:0] row_ki = ROW_O & { 3'b111, ~joy_a[4:0] };

genvar i;
generate
  for (i = 0; i < 8; i = i + 1) begin: block_row
    assign ROW_I[i] = row_ki[i] & &(COL_O | ~kbd_matrix[i]);
  end
endgenerate

assign COL_I = COL_O & { 3'b111, ~joy_b[4:0] };

assign nRESTORE = !restore;

//////////////////////////////////////////////////////////////////////
// Reset generator

reg [4:0] rst_cnt;

always @(posedge CLK) begin
  if (!ARESETn || !c64_usr_nres || EXT_RES) begin
    rst_cnt <= 5'b0000;
    C64_nRES <= 1'b0;
  end
  else begin
    if (&rst_cnt)
      C64_nRES <= 1'b1;
    else
      rst_cnt <= rst_cnt + 1'b1;
  end
end

//////////////////////////////////////////////////////////////////////
// C64 native memory interface
//
// Transactions stall the machine for a clock cycle to master the bus.

reg [15:0] mem_addr, mem_a;
reg [7:0]  mem_wdata, mem_dout;
reg        mem_rw;
reg        mem_master;

always @* begin
  HOLD = mem_ractive || mem_wactive;
end

always @(posedge CLK) if (CP1_POSEDGE) begin
  mem_dout <= mem_wdata;
  mem_a <= mem_addr;
  mem_master <= HOLD;
end

always @(posedge CLK) if (BCLK_POSEDGE) begin
  // Write cycle: Drive RW low from CP2 rising to CP1 rising
  if (CP1_POSEDGE)
    mem_rw <= 1'b1;
  else /* CP2_POSEDGE */
    mem_rw <= !mem_wactive;
end

assign D_O = mem_dout;
assign A_O = mem_a;
assign RW_O = !mem_master || mem_rw;

assign D_OE = mem_master && !mem_rw;
assign A_OE = mem_master;

always @* CSO_EN = cso_enable && HOLD;

always @(posedge CLK or negedge ARESETn) begin
  if (!ARESETn) begin
    mem_ractive <= 1'b0;
    mem_rvalid <= 1'b0;
    mem_wactive <= 1'b0;
    mem_wvalid <= 1'b0;
  end
  else begin
    mem_rvalid <= 1'b0;
    mem_wvalid <= 1'b0;
    if (CP2_NEGEDGE) begin
      mem_ractive <= 1'b0;
      mem_wactive <= 1'b0;
      mem_rdata <= D_I;
      if (!HOLD) begin
        if (mem_rreq) begin
          mem_ractive <= 1'b1;
          mem_addr <= ARADDR[15:0];
        end
        else if (mem_wreq) begin
          mem_wactive <= 1'b1;
          mem_addr <= AWADDR[15:0];
          case ({AWADDR[1:0], 1'b1})
            {2'b11, WSTRB[3]}: mem_wdata <= WDATA[31:24];
            {2'b10, WSTRB[2]}: mem_wdata <= WDATA[23:16];
            {2'b01, WSTRB[1]}: mem_wdata <= WDATA[15:08];
            {2'b00, WSTRB[0]}: mem_wdata <= WDATA[07:00];
            default: mem_wdata <= 8'hxx;
          endcase
        end
      end
      else begin
        mem_rvalid <= mem_ractive;
        mem_wvalid <= mem_wactive;
      end
    end
  end
end

endmodule
