module main_ram
  (
   input        CLK,
   input        CLKEN,
   input        nCS,
   input        nWE,
   input [15:0] A,
   input [7:0]  DB_I,
   output [7:0] DB_O,
   output       DB_OE
   );

reg [7:0] mem [0:(1<<16)-1];
reg [7:0] d;

// This is how the initial RAM image was created:
//
// openssl rand -out secret.key 32
// openssl aes-256-cbc -in hw/iec/01/iec-B_Mask.gbr -out ram.t -pass file:secret.key
// dd if=ram.t of=main_ram.bin count=64 bs=1k skip=1
// python bin_to_memh.py mem 8 <main_ram.bin >main_ram.hex

initial begin
  $readmemh("main_ram.hex", mem);
end

always @(posedge CLK) if (CLKEN) begin
  d <= mem[A];
  if (!nCS && !nWE)
    mem[A] <= DB_I;
end

assign DB_O = d;
assign DB_OE = !nCS && nWE;

endmodule
