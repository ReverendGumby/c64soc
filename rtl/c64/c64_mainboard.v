module c64_mainboard
  (
   output [6:0]  DEBUG,
   input         DOT_CLOCK,
   input         nRES,

   output        CP1,
   output        CP1_POSEDGE,
   output        CP1_NEGEDGE,
   output        CP2,
   output        CP2_POSEDGE,
   output        CP2_NEGEDGE,
   output        BCLK,
   output        BCLK_POSEDGE,
   output        BCLK_NEGEDGE,

   // CN1: KEYBOARD
   input [7:0]   COL_I,
   output [7:0]  COL_O,
   input [7:0]   ROW_I,
   output [7:0]  ROW_O,
   input         nRESTORE,

   // CN2: USER PORT
   input         CNT1,
   input         SP1_I,
   output        SP1_O,
   output        ATN,
   input         CNT2,
   input         SP2_I,
   output        SP2_O,
   output        nPC2,
   input         PA2_I,
   output        PA2_O,
   input [7:0]   PB_I,
   output [7:0]  PB_O,
   input         nFLAG2,

   // CN3: CASSETTE
   output        CASS_MOTOR,
   input         CASS_RD,
   output        CASS_WRT,
   input         CASS_SENSE,

   // CN4: SERIAL BUS
   input         DATA_I,
   output        DATA_O,
   input         CLK_I,
   output        CLK_O,

   // CN5: AUDIO / VIDEO
   output [7:0]  VIC_R,
   output [7:0]  VIC_G,
   output [7:0]  VIC_B,
   output        VIC_HS,
   output        VIC_VS,
   output        VIC_DE,
   input         SID_MCLK,
   output        SID_SCLK,
   output        SID_LRCLK,
   output        SID_SDOUT,
   output [15:0] SID_PDOUT,

   // CN6: CARTRIDGE / EXPANSION
   input         RW_I,
   output        RW_O,
   input         EXP_nIRQ,
   output        BA,
   input         nDMA,
   input [7:0]   DB_I,
   output [7:0]  DB_O,
   output        DB_OE,
   input [15:0]  A_I,
   output [15:0] A_O,
   output        A_OE,
   input         nEXROM,
   input         nGAME,
   output        nROMH,
   output        nROML,
   output        nIIO2,
   output        nIIO1,

   // c64bus interface
   input         HOLD, // halt system and release A,D,RW
   input [4:0]   CPU_REG_SEL,
   output [7:0]  CPU_REG_DOUT,
   input         ICD_CPU_ENABLE,
   input         ICD_CPU_FORCE_HALT,
   input         ICD_CPU_RESUME,
   output        ICD_CPU_CLR_RDY,
   input         ICD_MATCH_ENABLE,
   input [31:0]  ICD_MATCH_DIN,
   input [31:0]  ICD_MATCH_DEN,
   output        ICD_MATCH_TRIGGER,
   output [31:0] ICD_MATCH_DOUT,
   // chip select override and monitor
   input         CSO_EN,
   input         CSO_CHAREN,
   input         CSO_HIRAM,
   input         CSO_LORAM,
   input         CSO_GAME,
   input         CSO_EXROM,
   output        CSO_MON_CHAREN,
   output        CSO_MON_HIRAM,
   output        CSO_MON_LORAM,
   output        CSO_MON_GAME,
   output        CSO_MON_EXROM
   );

localparam [3:0] BID_CPU    = 4'd0;
localparam [3:0] BID_RAM    = 4'd1;
localparam [3:0] BID_VIC    = 4'd2;
localparam [3:0] BID_SID    = 4'd3;
localparam [3:0] BID_CHAROM = 4'd4;
localparam [3:0] BID_BASIC  = 4'd5;
localparam [3:0] BID_KERNAL = 4'd6;
localparam [3:0] BID_CIA1   = 4'd7;
localparam [3:0] BID_CIA2   = 4'd8;
localparam [3:0] BID_COLOR  = 4'd9;
localparam NUM_BID = 10;

reg         nrestore_buf;
reg [15:0]  ao, va;
reg [7:0]   dbo;

wire        clk = DOT_CLOCK;
wire        aec;              // A bus driver: 1 = CPU, 0 = VIC
wire        rdy = BA && nDMA && !ICD_CPU_CLR_RDY;
wire        sync;
wire        nirq, nnmi;
wire        ncharen, nhiram, nloram;
wire        caec = aec && nDMA;
wire        nio;
wire        ncas;
wire        ncasram;
wire        nbasic, nkernal, ncharom, grw;
wire        nvic, nsid, ncolor, nclas, ncia1, ncia2;
wire        nva14, nva15;
wire        nexrom, ngame;
wire        btna_lp = ROW_I[4]; // CN9 (Control Port 1) pin 6, light pen
wire [7:0]  db_o[0:NUM_BID-1], db;
wire [NUM_BID-1:0] db_oe;
wire        va_oe, vd_oe, a_oe_cpu;
wire [15:0] a_o_cpu, a, ra;
wire [13:0] a_o_vic;
wire [3:0]  vd;
wire        vic_nirq, cia1_nirq, cia2_nirq;
wire        color_ncs;
wire        data_out, clk_out, atn_out;
wire [7:0]  p_i, p_o, p_oe;
wire [7:0]  cia2_pa_i, cia2_pa_o;

// The data bus is driven by at most one driver.
always @* begin
  case (db_oe)
    10'b0000000001: dbo = db_o[BID_CPU];
    10'b0000000010: dbo = db_o[BID_RAM];
    10'b0000000100: dbo = db_o[BID_VIC];
    10'b0000001000: dbo = db_o[BID_SID];
    10'b0000010000: dbo = db_o[BID_CHAROM];
    10'b0000100000: dbo = db_o[BID_BASIC];
    10'b0001000000: dbo = db_o[BID_KERNAL];
    10'b0010000000: dbo = db_o[BID_CIA1];
    10'b0100000000: dbo = db_o[BID_CIA2];
    10'b1000000000: dbo = db_o[BID_COLOR];
    default: dbo = 8'hxx;
  endcase
end
assign DB_O = dbo;
assign DB_OE = |db_oe;
assign db = DB_OE ? dbo : DB_I;

// The address bus is driven by CPU, VIC, or external.
// The lower 12 bits of A are driven by VIC when aec=0.
always @* begin
  case ({va_oe, a_oe_cpu})
    2'b10: ao = {4'hf, va[11:0]}; // replaces U26
    2'b01: ao = a_o_cpu;
    default: ao = 16'hxxxx;
  endcase
end
assign A_O = ao;
assign A_OE = va_oe | a_oe_cpu;
assign a = A_OE ? ao : A_I;

// VIC has its own (partial) address and data buses.
// va[15:14] don't actually touch VIC, and are driven by CIA2.
// va[13:0] are driven by VIC.
always @* begin
  va[15:14] = {!nva15, !nva14};
  va[13:0] = a_o_vic;
end

// db[3:0] are driven by color RAM when aec=1 (and other bits).
assign db_o[BID_COLOR] = {4'hf, vd};
assign db_oe[BID_COLOR] = !ncolor && vd_oe;

// Main RAM has its own address bus, driven by CPU or VIC.
assign ra = aec ? a : va; // replaces U13, U14, U25

// The CPU peripheral port drives the cassette port and controls the
// memory map.
assign p_i[7] = 1'b1;                    // doesn't actually exist
assign p_i[6] = 1'b1;                    // doesn't actually exist
assign p_i[5] = p_oe[5] ? p_o[5] : 1'b0; // pulled down
assign p_i[4] = p_oe[4] ? p_o[4] : CASS_SENSE;
assign p_i[3] = p_oe[3] ? p_o[3] : 1'b1;
assign p_i[2] = p_oe[2] ? p_o[2] : 1'b1;
assign p_i[1] = p_oe[1] ? p_o[1] : 1'b1;
assign p_i[0] = p_oe[0] ? p_o[0] : 1'b1;

assign CASS_MOTOR = p_i[5];
assign CASS_WRT = p_i[3];
assign ncharen = CSO_EN ? !CSO_CHAREN : p_i[2];
assign nhiram = CSO_EN ? !CSO_HIRAM : p_i[1];
assign nloram = CSO_EN ? !CSO_LORAM : p_i[0];

assign nexrom = CSO_EN ? !CSO_EXROM : nEXROM;
assign ngame = CSO_EN ? !CSO_GAME : nGAME;

assign CSO_MON_CHAREN = !ncharen;
assign CSO_MON_HIRAM = !nhiram;
assign CSO_MON_LORAM = !nloram;
assign CSO_MON_GAME = !ngame;
assign CSO_MON_EXROM = !nexrom;

assign nirq = vic_nirq && EXP_nIRQ && cia1_nirq;
assign nnmi = nrestore_buf && cia2_nirq;
assign ncas = 1'b0;             // always assert; bclk clocks RAM

always @(posedge clk) if (CP1)
  nrestore_buf <= nRESTORE;     // timer omitted

// U15: I/O chip selects
assign nvic   = nio || !(a[11:10] == 2'b00); // D000-D3FF: VIC
assign nsid   = nio || !(a[11:10] == 2'b01); // D400-D7FF: SID
assign ncolor = nio || !(a[11:10] == 2'b10); // D800-DBFF: color RAM
assign nclas  = nio || !(a[11:10] == 2'b11); // DC00-DFFF

assign ncia1 = nclas || !(a[9:8] == 2'b00); // DC00-DCFF: CIA#1
assign ncia2 = nclas || !(a[9:8] == 2'b01); // DD00-DDFF: CIA#2
assign nIIO1 = nclas || !(a[9:8] == 2'b10); // DE00-DEFF: Z80
assign nIIO2 = nclas || !(a[9:8] == 2'b11); // DF00-DFFF: DISK

assign color_ncs = aec && ncolor;

assign cia2_pa_i[7] = DATA_I;
assign cia2_pa_i[6] = CLK_I;
assign cia2_pa_i[5] = data_out;
assign cia2_pa_i[4] = clk_out;
assign cia2_pa_i[3] = atn_out;
assign cia2_pa_i[2] = PA2_I;
assign cia2_pa_i[1] = nva15;
assign cia2_pa_i[0] = nva14;

assign data_out = cia2_pa_o[5];
assign clk_out = cia2_pa_o[4];
assign atn_out = cia2_pa_o[3];
assign PA2_O = cia2_pa_o[2];
assign nva15 = cia2_pa_o[1];
assign nva14 = cia2_pa_o[0];

assign ATN = !atn_out;
assign DATA_O = !data_out & cia2_pa_o[7];
assign CLK_O = !clk_out & cia2_pa_o[6];

todclkgen todclkgen
  (
   .CLKIN(DOT_CLOCK),
   .CLKOUT(todclk)
   );

mos6526 cia1_u1
  (
   .nRES(nRES),
   .CLK(clk),
   .CP2(CP2),
   .CP2_POSEDGE(CP2_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .TOD(todclk),
   .DB_I(db),
   .DB_O(db_o[BID_CIA1]),
   .DB_OE(db_oe[BID_CIA1]),
   .RS(a[3:0]),
   .nCS(ncia1),
   .RW(RW_I),
   .nIRQ(cia1_nirq),
   .PA_I(COL_I),
   .PA_O(COL_O),
   .PB_I(ROW_I),
   .PB_O(ROW_O),
   .nPC(),
   .nFLAG(CASS_RD),
   .SP_I(SP1_I),
   .SP_O(SP1_O),
   .CNT(CNT1),
   .HOLD(HOLD)
   );

mos6526 cia2_u2
  (
   .DEBUG(DEBUG[6:4]),
   .nRES(nRES),
   .CLK(clk),
   .CP2(CP2),
   .CP2_POSEDGE(CP2_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .TOD(todclk),
   .DB_I(db),
   .DB_O(db_o[BID_CIA2]),
   .DB_OE(db_oe[BID_CIA2]),
   .RS(a[3:0]),
   .nCS(ncia2),
   .RW(RW_I),
   .nIRQ(cia2_nirq),
   .PA_I(cia2_pa_i),
   .PA_O(cia2_pa_o),
   .PB_I(PB_I),
   .PB_O(PB_O),
   .nPC(nPC2),
   .nFLAG(nFLAG2),
   .SP_I(SP2_I),
   .SP_O(SP2_O),
   .CNT(CNT2),
   .HOLD(HOLD)
   );

mos6510 cpu_u7
  (
   .nRES(nRES),
   .CLK(clk),
   .CP1_POSEDGE(CP1_POSEDGE),
   .CP1_NEGEDGE(CP1_NEGEDGE),
   .CP2_POSEDGE(CP2_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .nIRQ(nirq),
   .nNMI(nnmi),
   .AEC(caec),
   .A_O(a_o_cpu),
   .A_OE(a_oe_cpu),
   .P_I(p_i),
   .P_O(p_o),
   .P_OE(p_oe),
   .DB_I(db),
   .DB_O(db_o[BID_CPU]),
   .DB_OE(db_oe[BID_CPU]),
   .RW(RW_O),
   .RDY(rdy),
   .SYNC(sync),
   .SO(1'b1),
   .HOLD(HOLD),
   .CPU_REG_SEL(CPU_REG_SEL),
   .CPU_REG_DOUT(CPU_REG_DOUT)
   );

mos6567 vic_u19
  (
   .CLKIN(DOT_CLOCK),
   .BCLK(BCLK),
   .BCLK_POSEDGE(BCLK_POSEDGE),
   .BCLK_NEGEDGE(BCLK_NEGEDGE),
   .CP1(CP1),
   .CP1_POSEDGE(CP1_POSEDGE),
   .CP1_NEGEDGE(CP1_NEGEDGE),
   .CP2(CP2),
   .CP2_POSEDGE(CP2_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .A_I(a[5:0]),
   .A_O(a_o_vic),
   .A_OE(va_oe),
   .DB_I({vd, db[7:0]}),
   .DB_O(db_o[BID_VIC]),
   .DB_OE(db_oe[BID_VIC]),
   .RW(RW_I),
   .nCS(nvic),
   .BA(BA),
   .AEC(aec),
   .nIRQ(vic_nirq),
   .LP(btna_lp),
   .DE(VIC_DE),
   .HS(VIC_HS),
   .VS(VIC_VS),
   .R(VIC_R),
   .G(VIC_G),
   .B(VIC_B),
   .HOLD(HOLD)
   );

mos6581 sid_u18
  (
   .DEBUG(),
   .nRES(nRES),
   .CLK(clk),
   .CP2(CP2),
   .CP2_POSEDGE(CP2_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .BCLK_NEGEDGE(BCLK_NEGEDGE),
   .A(a[4:0]),
   .DB_I(db),
   .DB_O(db_o[BID_SID]),
   .DB_OE(db_oe[BID_SID]),
   .RW(RW_I),
   .nCS(nsid),
   .MCLK(SID_MCLK),
   .SCLK(SID_SCLK),
   .LRCLK(SID_LRCLK),
   .SDOUT(SID_SDOUT),
   .PDOUT(SID_PDOUT),
   .HOLD(HOLD)
   );

chargen charom_u5
  (
   .CLK(clk),
   .CLKEN(BCLK_NEGEDGE),
   .nCS(ncharom),
   .A(a[11:0]),
   .DB_O(db_o[BID_CHAROM]),
   .DB_OE(db_oe[BID_CHAROM])
   );

color_ram color_u6
  (
   .CLK(clk),
   .CLKEN(BCLK_NEGEDGE),
   .A(a[9:0]),
   .DB_I(db[3:0]),
   .DB_O(vd),
   .DB_OE(vd_oe),
   .nCS(color_ncs),
   .nWE(grw)
   );

c64_pla pla_u17
  (
   .I({
       ncas,                    // I[0]
       nloram,
       nhiram,
       ncharen,
       nva14,
       a[15],
       a[14],
       a[13],
       a[12],
       BA,
       !aec,
       RW_I,
       nEXROM,
       nGAME,
       va[13],
       va[12]                   // I[15]
       }),
   .O({
       ncasram,                 // O[0]
       nbasic,
       nkernal,
       ncharom,
       grw,
       nio,
       nROML,
       nROMH                    // O[7]
       })
   );

basic basic_u3
  (
   .CLK(clk),
   .CLKEN(BCLK_NEGEDGE),
   .nCS(nbasic),
   .A(a[12:0]),
   .DB_O(db_o[BID_BASIC]),
   .DB_OE(db_oe[BID_BASIC])
   );

kernal kernal_u4
  (
   .CLK(clk),
   .CLKEN(BCLK_NEGEDGE),
   .nCS(nkernal),
   .A(a[12:0]),
   .DB_O(db_o[BID_KERNAL]),
   .DB_OE(db_oe[BID_KERNAL])
   );

main_ram ram_u21              // plus U9, U22, U10, U23, U11, U24, U12
  (
   .CLK(clk),
   .CLKEN(BCLK_NEGEDGE),
   .A(ra),
   .DB_I(db),
   .DB_O(db_o[BID_RAM]),
   .DB_OE(db_oe[BID_RAM]),
   .nCS(ncasram),
   .nWE(RW_I)
   );

//////////////////////////////////////////////////////////////////////
// Additional logic, not original to the machine

icd icd
  (
   .CLK(clk),
   .CP1_POSEDGE(CP1_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .HOLD(HOLD),

   .A(a),
   .DB(db),
   .RW(RW_I),
   .RDY(rdy),
   .SYNC(sync),

   .MATCH_ENABLE(ICD_MATCH_ENABLE),
   .MATCH_DIN(ICD_MATCH_DIN),
   .MATCH_DEN(ICD_MATCH_DEN),
   .MATCH_TRIGGER(ICD_MATCH_TRIGGER),
   .MATCH_DOUT(ICD_MATCH_DOUT),

   .CPU_ENABLE(ICD_CPU_ENABLE),
   .CPU_FORCE_HALT(ICD_CPU_FORCE_HALT),
   .CPU_RESUME(ICD_CPU_RESUME),
   .CPU_CLR_RDY(ICD_CPU_CLR_RDY)
   );

// Hacking
assign DEBUG[0] = CP2;
assign DEBUG[1] = CP2_NEGEDGE;
assign DEBUG[2] = CLK_I;
assign DEBUG[3] = DATA_I;

endmodule
