// In-Circuit Debugger (not original to the C64)
// Match unit: triggers on a system state matching an externally provided template
// CPU control unit: halts CPU on match (or forced halt) by clearing RDY

module icd
  (
   input             CLK,
   input             CP1_POSEDGE,
   input             CP2_NEGEDGE,
   input             HOLD,

   // System state
   input [15:0]      A,
   input [7:0]       DB,
   input             RW,
   input             RDY,
   input             SYNC,

   // Match unit
   input             MATCH_ENABLE,
   input [31:0]      MATCH_DIN,
   input [31:0]      MATCH_DEN,
   output reg        MATCH_TRIGGER,
   output reg [31:0] MATCH_DOUT,

   // CPU control unit
   input             CPU_ENABLE,
   input             CPU_FORCE_HALT,
   input             CPU_RESUME, // edge-triggered
   output reg        CPU_CLR_RDY
   );

reg [31:0] state;
reg [31:0] match_out;
reg        resume_dly;

//``REGION_REGS C64BUS_CTRL
//``REG ICD_MATCH_D
always @* begin
  state[15:0]  = A;
  state[23:16] = DB;
  state[24]    = RW;
  state[25]    = SYNC;
  state[31:26] = 0;             //``NO_FIELD
end
//``REG_END

// state      x0101
// MATCH_DIN  x0011
// MATCH_DEN  01111
// ---------  -----
// match_out  11001

always @* begin
  match_out = ~((state ^ MATCH_DIN) & MATCH_DEN);
end

always @(posedge CLK) if (CP2_NEGEDGE) begin
  if (!MATCH_ENABLE) begin
    MATCH_TRIGGER <= 1'b0;
    MATCH_DOUT <= 32'b0;
  end
  else if (!MATCH_TRIGGER) begin
    if (RDY && &match_out && !HOLD) begin
      MATCH_TRIGGER <= 1'b1;
      MATCH_DOUT <= state;
    end
  end
end

always @(posedge CLK) if (CP1_POSEDGE) begin
  resume_dly <= CPU_RESUME;

  if (!CPU_ENABLE)
    CPU_CLR_RDY <= 1'b0;
  else begin
    if (CPU_FORCE_HALT)
      CPU_CLR_RDY <= 1'b1;
    else if (!resume_dly && CPU_RESUME)
      CPU_CLR_RDY <= 1'b0;
    else if (MATCH_TRIGGER)
      CPU_CLR_RDY <= 1'b1;
  end
end

endmodule
