import sys

data = []
while True:
    b = sys.stdin.read(1)
    if b == '':
        break
    data = data + [ord(b)]

for b in data:
    print ("{0:x}").format(b)
