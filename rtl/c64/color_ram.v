module color_ram
  (
   input        CLK,
   input        CLKEN,
   input        nCS,
   input        nWE,
   input [9:0]  A,
   input [3:0]  DB_I,
   output [3:0] DB_O,
   output       DB_OE
   );

reg [3:0] mem [0:(1<<10)-1];
reg [3:0] d;

always @(posedge CLK) if (CLKEN) begin
  d <= mem[A];
  if (!nCS && !nWE)
    mem[A] <= DB_I;
end

assign DB_O = d;
assign DB_OE = !nCS && nWE;

endmodule
