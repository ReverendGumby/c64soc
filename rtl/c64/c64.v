// C64 system:
// - mainboard
// - external ROM cartridge emulation
// - C64 / PS7 bridge

module c64
  (
   output [31:0] DEBUG,
   input         DOT_CLOCK,
   input         EXT_RST,
   output        C64_nRES,

   // Video output
   output [7:0]  VIC_R,
   output [7:0]  VIC_G,
   output [7:0]  VIC_B,
   output        VIC_HS,
   output        VIC_VS,
   output        VIC_DE,

   // Audio output
   input         SID_MCLK,
   output        SID_SCLK,
   output        SID_LRCLK,
   output        SID_SDOUT,
   output [15:0] SID_PDOUT,

   // Serial port (e.g., floppy drive)
   input         DATA_I,
   output        DATA_O,
   input         CLK_I,
   output        CLK_O,
   output        ATN,

   // AXI Lite slave bus interface
   input         ARESETn,
   input [31:0]  AWADDR,
   input         AWVALID,
   output        AWREADY,
   input [31:0]  WDATA,
   output        WREADY,
   input [3:0]   WSTRB,
   input         WVALID,
   output [1:0]  BRESP,
   output        BVALID,
   input         BREADY,
   input [31:0]  ARADDR,
   input         ARVALID,
   output        ARREADY,
   output [31:0] RDATA,
   output [1:0]  RRESP,
   output        RVALID,
   input         RREADY
   );

wire            cp1_posedge;
wire            cp2;
wire            cp2_posedge;
wire            bclk_posedge;
wire            bclk_negedge;

wire            c64_rstn;
wire            cart_rstn;

wire [7:0]      col, col_o;
wire [7:0]      row, row_o;
wire            nrestore;
wire            sp1 = sp1_o;    // TODO
wire            sp2 = sp2_o;    // TODO
wire            pa2 = pa2_o;    // TODO
wire [7:0]      pb_o;
wire [7:0]      pb = pb_o;      // TODO
wire            cass_sense = 1'b1;
reg [7:0]       c64_d;
wire [7:0]      c64_d_o;
reg [15:0]      c64_a;
wire [15:0]     c64_a_o;
wire            c64_d_oe, c64_a_oe;
wire            c64_rw, c64_rw_o;
wire            nexrom, ngame;

wire [11:2]     cart_ctrl_addr;
wire [3:0]      cart_ctrl_wstrb;
wire [31:0]     cart_ctrl_din;
wire [31:0]     cart_ctrl_dout;
wire [7:0]      cart_d_o;
wire [15:0]     cart_a_o;
wire            cart_d_oe;

wire            hold;
wire [7:0]      c64bus_d_o;
wire [15:0]     c64bus_a_o;
wire            c64bus_d_oe, c64bus_a_oe;
wire [4:0]      cpu_reg_sel;
wire [7:0]      cpu_reg_dout;
wire            icd_cpu_enable;
wire            icd_cpu_force_halt;
wire            icd_cpu_resume;
wire            icd_cpu_clr_rdy;
wire            icd_match_enable;
wire [31:0]     icd_match_din;
wire [31:0]     icd_match_den;
wire            icd_match_trigger;
wire [31:0]     icd_match_dout;
wire            cso_en;
wire            cso_charen;
wire            cso_hiram;
wire            cso_loram;
wire            cso_game;
wire            cso_exrom;
wire            cso_mon_charen;
wire            cso_mon_hiram;
wire            cso_mon_loram;
wire            cso_mon_game;
wire            cso_mon_exrom;

wire [14:2]     cart_bs_araddr;
wire            cart_bs_arvalid;
wire [31:0]     cart_bs_rdata;
wire            cart_bs_rready;
wire            cart_bs_rvalid;
wire [14:2]     cart_bs_awaddr;
wire [3:0]      cart_bs_wstrb;
wire [31:0]     cart_bs_wdata;
wire            cart_bs_wvalid;
wire            cart_bs_bvalid;

//////////////////////////////////////////////////////////////////////

always @* begin
  c64_a = 16'hxxxx;
  case ({c64bus_a_oe, c64_a_oe})
    2'b01: c64_a = c64_a_o;
    2'b10: c64_a = c64bus_a_o;
    default: ;
  endcase
end

always @* begin
  c64_d = 8'hxx;
  case ({c64bus_d_oe, cart_d_oe, c64_d_oe})
    3'b001: c64_d = c64_d_o;
    3'b010: c64_d = cart_d_o;
    3'b100: c64_d = c64bus_d_o;
    default: ;
  endcase
end

assign c64_rw = c64_rw_o && c64bus_rw_o;
assign C64_nRES = c64_rstn;

//////////////////////////////////////////////////////////////////////
// C64 mainboard

c64_mainboard c64_mb
  (
   .DEBUG(DEBUG),
   .DOT_CLOCK(DOT_CLOCK),
   .nRES(c64_rstn),
   .CP1(),
   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(),
   .CP2(cp2),
   .CP2_POSEDGE(),
   .CP2_NEGEDGE(cp2_negedge),
   .BCLK(),
   .BCLK_POSEDGE(bclk_posedge),
   .BCLK_NEGEDGE(bclk_negedge),
   .COL_I(col),
   .COL_O(col_o),
   .ROW_I(row),
   .ROW_O(row_o),
   .nRESTORE(nrestore),
   .CNT1(1'b0),
   .SP1_I(sp1),
   .SP1_O(sp1_o),
   .ATN(ATN),
   .CNT2(1'b0),
   .SP2_I(sp2),
   .SP2_O(sp2_o),
   .nPC2(),
   .PA2_I(pa2),
   .PA2_O(pa2_o),
   .PB_I(pb),
   .PB_O(pb_o),
   .nFLAG2(1'b1),
   .CASS_MOTOR(),
   .CASS_RD(1'b0),
   .CASS_WRT(),
   .CASS_SENSE(cass_sense),
   .DATA_I(DATA_I),
   .DATA_O(DATA_O),
   .CLK_I(CLK_I),
   .CLK_O(CLK_O),
   .VIC_R(VIC_R),
   .VIC_G(VIC_G),
   .VIC_B(VIC_B),
   .VIC_HS(VIC_HS),
   .VIC_VS(VIC_VS),
   .VIC_DE(VIC_DE),
   .SID_MCLK(SID_MCLK),
   .SID_SCLK(SID_SCLK),
   .SID_LRCLK(SID_LRCLK),
   .SID_SDOUT(SID_SDOUT),
   .SID_PDOUT(SID_PDOUT),
   .RW_I(c64_rw),
   .RW_O(c64_rw_o),
   .EXP_nIRQ(exp_nirq),
   .BA(c64_ba),
   .nDMA(ndma),
   .DB_I(c64_d),
   .DB_O(c64_d_o),
   .DB_OE(c64_d_oe),
   .A_I(c64_a),
   .A_O(c64_a_o),
   .A_OE(c64_a_oe),
   .nEXROM(nexrom),
   .nGAME(ngame),
   .nROMH(nromh),
   .nROML(nroml),
   .nIIO2(niio2),
   .nIIO1(niio1),
   .HOLD(hold),
   .CPU_REG_SEL(cpu_reg_sel),
   .CPU_REG_DOUT(cpu_reg_dout),
   .ICD_CPU_ENABLE(icd_cpu_enable),
   .ICD_CPU_FORCE_HALT(icd_cpu_force_halt),
   .ICD_CPU_RESUME(icd_cpu_resume),
   .ICD_CPU_CLR_RDY(icd_cpu_clr_rdy),
   .ICD_MATCH_ENABLE(icd_match_enable),
   .ICD_MATCH_DIN(icd_match_din),
   .ICD_MATCH_DEN(icd_match_den),
   .ICD_MATCH_TRIGGER(icd_match_trigger),
   .ICD_MATCH_DOUT(icd_match_dout),
   .CSO_EN(cso_en),
   .CSO_CHAREN(cso_charen),
   .CSO_HIRAM(cso_hiram),
   .CSO_LORAM(cso_loram),
   .CSO_GAME(cso_game),
   .CSO_EXROM(cso_exrom),
   .CSO_MON_CHAREN(cso_mon_charen),
   .CSO_MON_HIRAM(cso_mon_hiram),
   .CSO_MON_LORAM(cso_mon_loram),
   .CSO_MON_GAME(cso_mon_game),
   .CSO_MON_EXROM(cso_mon_exrom)
   );

//////////////////////////////////////////////////////////////////////
// External ROM cartridge emulation

cart cart
  (
   .CLK(DOT_CLOCK),
   .CP1_POSEDGE(cp1_posedge),
   .BCLK_NEGEDGE(bclk_negedge),
   .nRES(c64_rstn),
   .RW(c64_rw),
   .nIRQ(exp_nirq),
   .BA(c64_ba),
   .nDMA(ndma),
   .D_I(c64_d),
   .D_O(cart_d_o),
   .D_OE(cart_d_oe),
   .A(c64_a),
   .nEXROM(nexrom),
   .nGAME(ngame),
   .nROMH(nromh),
   .nROML(nroml),
   .nIIO2(niio2),
   .nIIO1(niio1),
   .HOLD(1'b0),          // cartridge SHOULD respond during HOLD
   .CTRL_RSTn(cart_rstn),
   .CTRL_ADDR(cart_ctrl_addr),
   .CTRL_WSTRB(cart_ctrl_wstrb),
   .CTRL_DIN(cart_ctrl_din),
   .CTRL_DOUT(cart_ctrl_dout),
   .BS_ARADDR(cart_bs_araddr),
   .BS_ARVALID(cart_bs_arvalid),
   .BS_RDATA(cart_bs_rdata),
   .BS_RREADY(cart_bs_rready),
   .BS_RVALID(cart_bs_rvalid),
   .BS_AWADDR(cart_bs_awaddr),
   .BS_WSTRB(cart_bs_wstrb),
   .BS_WDATA(cart_bs_wdata),
   .BS_WVALID(cart_bs_wvalid),
   .BS_BVALID(cart_bs_bvalid)
   );

//////////////////////////////////////////////////////////////////////
// C64 / PS7 bridge

c64bus c64bus
  (
   .CLK(DOT_CLOCK),
   .CP1_POSEDGE(cp1_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .BCLK_POSEDGE(bclk_posedge),

   .ARESETn(ARESETn),
   .AWADDR(AWADDR),
   .AWVALID(AWVALID),
   .AWREADY(AWREADY),
   .WDATA(WDATA),
   .WREADY(WREADY),
   .WSTRB(WSTRB),
   .WVALID(WVALID),
   .BRESP(BRESP),
   .BVALID(BVALID),
   .BREADY(BREADY),
   .ARADDR(ARADDR),
   .ARVALID(ARVALID),
   .ARREADY(ARREADY),
   .RDATA(RDATA),
   .RRESP(RRESP),
   .RVALID(RVALID),
   .RREADY(RREADY),

   .COL_I(col),
   .COL_O(col_o),
   .ROW_I(row),
   .ROW_O(row_o),
   .nRESTORE(nrestore),

   .EXT_RES(EXT_RST),
   .C64_nRES(c64_rstn),
   .CART_nRES(cart_rstn),

   .HOLD(hold),
   .RW_O(c64bus_rw_o),
   .D_I(c64_d),
   .D_O(c64bus_d_o),
   .D_OE(c64bus_d_oe),
   .A_O(c64bus_a_o),
   .A_OE(c64bus_a_oe),

   .CSO_EN(cso_en),
   .CSO_CHAREN(cso_charen),
   .CSO_HIRAM(cso_hiram),
   .CSO_LORAM(cso_loram),
   .CSO_GAME(cso_game),
   .CSO_EXROM(cso_exrom),
   .CSO_MON_CHAREN(cso_mon_charen),
   .CSO_MON_HIRAM(cso_mon_hiram),
   .CSO_MON_LORAM(cso_mon_loram),
   .CSO_MON_GAME(cso_mon_game),
   .CSO_MON_EXROM(cso_mon_exrom),
   
   .CPU_REG_SEL(cpu_reg_sel),
   .CPU_REG_DOUT(cpu_reg_dout),
   .ICD_CPU_ENABLE(icd_cpu_enable),
   .ICD_CPU_FORCE_HALT(icd_cpu_force_halt),
   .ICD_CPU_RESUME(icd_cpu_resume),
   .ICD_CPU_CLR_RDY(icd_cpu_clr_rdy),
   .ICD_MATCH_ENABLE(icd_match_enable),
   .ICD_MATCH_DIN(icd_match_din),
   .ICD_MATCH_DEN(icd_match_den),
   .ICD_MATCH_TRIGGER(icd_match_trigger),
   .ICD_MATCH_DOUT(icd_match_dout),

   .CART_CTRL_ADDR(cart_ctrl_addr),
   .CART_CTRL_WSTRB(cart_ctrl_wstrb),
   .CART_CTRL_DIN(cart_ctrl_din),
   .CART_CTRL_DOUT(cart_ctrl_dout),

   .CART_BS_ARADDR(cart_bs_araddr),
   .CART_BS_ARVALID(cart_bs_arvalid),
   .CART_BS_RDATA(cart_bs_rdata),
   .CART_BS_RREADY(cart_bs_rready),
   .CART_BS_RVALID(cart_bs_rvalid),
   .CART_BS_AWADDR(cart_bs_awaddr),
   .CART_BS_WSTRB(cart_bs_wstrb),
   .CART_BS_WDATA(cart_bs_wdata),
   .CART_BS_WVALID(cart_bs_wvalid),
   .CART_BS_BVALID(cart_bs_bvalid)
   );

endmodule
