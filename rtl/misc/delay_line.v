module delay_line
  #(parameter W=1,
    parameter L=1)
  (
   input          C,
   input          CE,
   input [W-1:0]  D,
   output [W-1:0] Q
   );

reg [L-1:0][W-1:0] dly;
always @(posedge C) if (CE)
  dly <= {dly[L-2:0], D};

assign Q = dly[L-1];

endmodule
