module debounce #(parameter DELAY=1)
  (
   input      CLK,
   input      D,
   output reg Q
   );

// Add 1 for synchronization
localparam DLY_LEN = DELAY + 1;

reg [DLY_LEN:0] dly;

always @(posedge CLK) begin
  dly[DLY_LEN:0] <= {D, dly[DLY_LEN:1]};

  if (dly[0] == |dly[DLY_LEN - 1:1] ||
      dly[0] == &dly[DLY_LEN - 1:1])
    Q <= dly[0];
end

endmodule
