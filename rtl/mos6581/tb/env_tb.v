`timescale 1us / 1ps

module env_tb
  (
   );

wire [7:0]  env;
wire [7:0]  cr, ad, sr;
wire [11:0] v1out;

reg [23:0]  v1osc;
reg         CLK, nRES;
reg         gate;
reg [3:0]   atk, dcy, stn, rls;
reg [11:0]  f;
reg [3:0]   wf;

assign cr = {wf, 3'b000, gate};
assign ad = {atk, dcy};
assign sr = {stn, rls};

mos6581_voice voice
  (
   .nRES(nRES),
   .CLK(CLK),
   .CLKEN(1'b1),
   .F(f),
   .CR(cr),
   .AD(ad),
   .SR(sr),
   .ENV(env),
   .OSC(),
   .OUT(v1out)
   );

initial begin
  f = 12'h400;
  wf = 4'b0001;
  atk = 4'd2;
  dcy = 4'd5;
  stn = 4'd8;
  rls = 4'd10;
end

initial begin
  CLK = 1'b1;
  nRES = 1'b0;
  gate = 1'b0;
  #10 @(posedge CLK) nRES = 1'b1;
  #100000 gate = 1'b1;
  #300000 gate = 1'b0;
  #1500000 $finish();
end

always #0.5 begin :clkgen
  CLK = !CLK;
end

endmodule
