function load_compare(fn)

    opt = delimitedTextImportOptions('NumVariables', 3, 'Delimiter', ' ', 'VariableNames', {'idx', 'want', 'got'}, 'VariableTypes', {'double', 'double', 'double'}, 'DataLines', [2 Inf], 'ConsecutiveDelimitersRule', 'join', 'LeadingDelimitersRule', 'ignore');
    t = readtable(fn, opt);

    figure
    hold on
    plot(t.idx, t.want)
    plot(t.idx, t.got)
    hold off

end
