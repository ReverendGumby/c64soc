// iverilog -grelative-include -s integrator_tb -o integrator_tb integrator_tb.v ../integrator.v ../vcr_kvg.v ../vcr_n_ids_term.v ../opamp_rev.v

`timescale 1ns / 1ps

module integrator_tb();

`include "integrator_tb_io.vh"

reg [15:0] vi;
reg [31:0] vddt_vw_2;
reg [19:0] idx;
reg [15:0] vo_exp;
reg        nres, clk, iclken;
wire [15:0] vo;

mos6581_integrator uut
  (
   .nRES(nres),
   .CLK(clk),
   .ICLKEN(iclken),
   .OCLKEN(oclken),

   .VI(vi),
   .VDDT_VW_2(vddt_vw_2),
   .VO(vo)
   );

initial begin
  $dumpfile("integrator_tb.vcd");
  $dumpvars;

  nres = 1'b0;
  vddt_vw_2 = 32'd377877541; // ((mf.kVddt - int32(mf.f0_dac(0 + 1))) ^ 2) / 2
  iclken = 1'b1;
  idx = 1'd0;
  clk = 1'b1;

  #100 @(posedge clk) nres = 1'b1;
end

always #10 begin :clkgen
  clk = !clk;
end

always @idx begin
  vi = in[idx];
  vo_exp = out[idx];
end

always @(posedge clk) if (nres) begin
  iclken <= ~iclken && oclken;

  if (oclken) begin
    $display("%6d %10d %10d", idx, vo_exp, vo);
    if (vo != vo_exp) begin
      //$display("MATCH FAILED!");
      //$finish;
    end

    idx = idx + 1'd1;
    if (idx == NUM_SAMPLES)
      $finish;
  end
end

endmodule
