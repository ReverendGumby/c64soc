import sys

i = 0
while True:
    l = sys.stdin.readline()
    if not l:
        break
    ns = l.split()
    for n in ns:
        print('  out[{0}] = {1}'.format(i, n))
        i += 1

