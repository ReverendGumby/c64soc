`timescale 1ns / 1ps

module serializer_tb
  (
   );

reg [23:0]  v1osc;
reg         CLK;
wire [15:0] serin;
wire        MCLK, SCLK, LRCLK, SDOUT;

mos6581_sample_converter #(12, 16) conv
  (
   .IN(v1osc[23:12]),
   .OUT(serin)
   );

mos6581_serializer ser
  (
   .CLK(CLK),
   .PDEN(1'b1),
   .PDIN(serin),
   .MCLK(MCLK),
   .SCLK(SCLK),
   .LRCLK(LRCLK),
   .SDOUT(SDOUT)
   );

initial begin
  CLK = 1'b1;
  v1osc = 12'h0;
end

always #(500/8.192) begin :clkgen
  CLK = !CLK;
end

always @(posedge CLK) begin
  v1osc <= v1osc + 16;
end

always @* begin
  if (v1osc[23:16] == 8'hff)
    $finish();
end

endmodule
