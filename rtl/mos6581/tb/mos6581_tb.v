`timescale 1us / 1ps

module mos6581_tb
  (
   );

wire        mclk, sclk, lrclk, sdout;
wire        CP2_POSEDGE, CP2_NEGEDGE, BCLK_NEGEDGE;

reg         MCLK, CLK, nRES;
reg         CP2;

mos6581 sid
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP2(CP2),
   .CP2_POSEDGE(CP2_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .BCLK_NEGEDGE(BCLK_NEGEDGE),
   .RW(1'b1),
   .nCS(1'b1),
   .MCLK(MCLK),
   .SCLK(sclk),
   .LRCLK(lrclk),
   .SDOUT(sdout),
   .HOLD(1'b0)
   );

initial begin
  force sid.reg_file.F1 = 16'd60000; // 3.66 kHz
  //force sid.reg_file.F1 = 16'd65; // 4 Hz
  force sid.reg_file.CR1 = 8'h20; // sawtooth
  //force sid.reg_file.CR1 = 8'h40; // pulse
  //force sid.reg_file.PW1 = 12'h800; // dc=50%
  force sid.reg_file.AD1 = 8'h00;
  force sid.reg_file.SR1 = 8'hf0;
  force sid.reg_file.FC = 11'd107; // ~680 Hz
  //force sid.reg_file.FCR1 = 8'h01; // f1
  force sid.reg_file.FCR2 = 8'h0f; // vol=15
end

initial begin
  clk_cnt = 3'b000;
  CLK = 1'b1;
  MCLK = 1'b1;
  nRES = 1'b0;
  #10 @(negedge CP2) nRES = 1'b1;
  #1000 force sid.reg_file.CR1[0] = 1'b1;
  //#300000 force sid.reg_file.CR1[0] = 1'b0;
  #2000000 $finish();
end

always #0.060 begin :clkgen
  CLK = !CLK;
end

always #0.020 begin :mclkgen
  MCLK = !MCLK;
end

reg [2:0] clk_cnt;

always @(negedge CLK)
  clk_cnt <= clk_cnt + 3'b001;

assign BCLK_NEGEDGE = clk_cnt[1:0] == 2'd2;
assign CP2_POSEDGE = clk_cnt[2:0] == 3'd4;
assign CP2_NEGEDGE = clk_cnt[2:0] == 3'd7;
always @(posedge CLK) CP2 <= clk_cnt[2] && clk_cnt[1:0] != 2'd3;

endmodule
