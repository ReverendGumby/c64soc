// iverilog -grelative-include -s svf_tb -o svf_tb svf_tb.v ../svf.v ../summer.v ../gain.v ../integrator.v ../vcr_kvg.v ../vcr_n_ids_term.v ../opamp_rev.v

`timescale 1ns / 1ps

module svf_tb();

`include "svf_tb_io_min.vh"

reg [15:0] in3;
reg [31:0] vddt_vw_2;
reg [19:0] idx;
reg [15:0] lp_exp, bp_exp, hp_exp;
reg [2:0]  iclken, ien;
reg        nres, clk, cclken;

wire [15:0] lp_got, bp_got, hp_got;

localparam IDX_END = NUM_SAMPLES;
//localparam IDX_END = 100;

mos6581_svf uut
  (
   .nRES(nres),
   .CLK(clk),
   .CCLKEN(cclken),
   .FC(11'd1600),
   .Q(4'd0),
   .IEN(ien),

   .ICLKEN(iclken),
   .IN3(in3),

   .LPCLKEN(lpclken),
   .LP(lp_got),
   .BPCLKEN(bpclken),
   .BP(bp_got),
   .HPCLKEN(hpclken),
   .HP(hp_got)
   );

initial begin
  $dumpfile("svf_tb.vcd");
  $dumpvars;

  nres = 1'b0;
  cclken = 1'b0;
  iclken = 3'b000;
  ien = 3'b100;
  idx = 1'd0;
  clk = 1'b1;

  #100 @(posedge clk) nres = 1'b1;
end

always #10 begin :clkgen
  clk = !clk;
end

always @idx begin
  in3 = in[idx];
  lp_exp = lp[idx];
  bp_exp = bp[idx];
  hp_exp = hp[idx];
end

always @(posedge clk) if (nres) begin
  cclken <= 1'b1;

  @(posedge clk) ;
  cclken <= 1'b0;
  iclken <= 3'b100;

  @(posedge clk) ;
  iclken <= 3'b0;

  while (!hpclken)
    @(negedge clk) ; 
  // ends mid-hpclken pulse
end

always @(posedge clk) if (nres) begin
  if (hpclken) begin
    $display("%6d / %6d %6d %4d / %6d %6d %4d / %6d %6d %4d", idx, 
             lp_exp, lp_got, $signed(lp_got - lp_exp),
             bp_exp, bp_got, $signed(bp_got - bp_exp),
             hp_exp, hp_got, $signed(hp_got - hp_exp));

    idx = idx + 1'd1;
    if (idx == IDX_END)
      $finish;
  end
end

endmodule
