module mos6581_wave
  (
   input         nRES,
   input         CLK,
   input         CLKEN,

   input [15:0]  F,
   input [11:0]  PW,

   input         SYNC,
   input         RING,
   input         TEST,
   input         WTRI,
   input         WSAW,
   input         WPUL,
   input         WNOI,

   input         SYNC_RM_IN,
   output        SYNC_RM_OUT,

   output [11:0] OUT            // UQ0.12
   );

wire [11:0] osc;

reg [23:0]  pa;
reg [22:0]  noi_lfsr;
reg [11:0]  wf_saw, wf_tri, wf_pul, wf_noi, wf_out;
reg [10:0]  tri_a, tri_b;
reg         noi_clk, noi_clk_dly;
reg         sync_rm_in_dly;

//////////////////////////////////////////////////////////////////////
// Oscillator

// Phase accumulator: increments by F every cycle.
always @(posedge CLK) if (CLKEN) begin
  if (!nRES)
    pa <= 24'h0;
  else begin
    // Hard sync: "Synchronizes the fundamental frequency of
    // Oscillator X with the fundamental frequency of Oscillator Y."
    // More specifically, rising edge of MSB of previous oscillator's
    // accumulator clears our accumulator.
    if (SYNC && SYNC_RM_IN && !sync_rm_in_dly)
      pa <= 24'h0;
    else
      pa[23:0] <= pa + F[15:0];
  end
end

// Oscillator output is upper 12 bits of phase accumulator.
// Frequency = F * CLK / 2^24 = F / 16.384 Hz
assign osc = pa[23:12];

always @(posedge CLK) if (CLKEN) begin
    sync_rm_in_dly <= SYNC_RM_IN;
end

// Both hard sync and ring modulation rely on the accumulator MSB.
assign SYNC_RM_OUT = pa[23];

//////////////////////////////////////////////////////////////////////
// Waveform Generator

// Sawtooth waveform: simply the oscillator output.
always @*
  wf_saw = osc;

// Triangle waveform: take the bitwise XOR of the oscillator MSB with
// each of the remaining bits. Then shift the result left, to recover
// amplitude.
//
// Ring modulation adds another bitwise XOR with the inverse of the
// previous voice's oscillator MSB.
always @* begin
  tri_a[10:0] = osc[10:0];
  tri_b[10:0] = {11{osc[11] ^ (RING * !SYNC_RM_IN)}};
end

always @* begin
  wf_tri[11:1] = tri_a ^ tri_b;
  wf_tri[0] = 1'b0;
end

// Pulse waveform: 0 when the oscillator is lower than PW, else
// full-scale. PW=12'h800 produces a perfect square wave. (TODO:
// confirm)
always @* begin
  wf_pul[11:0] = {12{osc >= PW}};
end

// Noise waveform: a 23-bit Fibonacci LFSR, polynomial
// x^22+x^17+1. Output the upper 12 bits of the register. Clock LFSR
// by an intermediate accumulator bit.
always @*
  noi_clk = pa[19]; // When F=$8000, LFSR shifts every $20 cycles.

always @(posedge CLK) if (CLKEN)
  noi_clk_dly <= noi_clk;

always @(posedge CLK) if (CLKEN) begin
  if (!nRES)
    noi_lfsr <= {23{1'b1}};
  else if (noi_clk && !noi_clk_dly) begin
    noi_lfsr[22:1] <= noi_lfsr[21:0];
    noi_lfsr[0] <= noi_lfsr[22] ^ noi_lfsr[17]; /* | TEST ? */
  end
end

// Bit scramble from http://oms.wmhost.com/misc/6581_wavegen.txt
always @* begin
  wf_noi[11] = noi_lfsr[20];
  wf_noi[10] = noi_lfsr[18];
  wf_noi[9] = noi_lfsr[14];
  wf_noi[8] = noi_lfsr[11];
  wf_noi[7] = noi_lfsr[9];
  wf_noi[6] = noi_lfsr[5];
  wf_noi[5] = noi_lfsr[2];
  wf_noi[4] = noi_lfsr[0];
  wf_noi[3:0] = 4'b0000;
end

// Waveform generator output. TODO, isn't really an AND. More like a
// mess.
always @(posedge CLK) if (CLKEN) begin
  if (!nRES)
    wf_out <= 12'h0;
  else if (WTRI || WSAW || WPUL || WNOI)
    wf_out <= ((WTRI ? wf_tri : 12'hfff) &
               (WSAW ? wf_saw : 12'hfff) &
               (WPUL ? wf_pul : 12'hfff) &
               (WNOI ? wf_noi : 12'hfff));
end

assign OUT = wf_out;

endmodule
