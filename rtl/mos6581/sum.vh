/* Defines the linear approximation interpolation function */
`include "interp2.vh"

localparam AW = IIW; // adder => interp2 input

reg [CSELW-1:0] csel;
reg [IW-1:0]    smux;
reg [NI-1:0]    ien, isum, ipost;
reg [OW-1:0]    out;
reg [AW-1:0]    sum;
reg             sdone, odone;

initial begin
  ien = 1'b0;
  ipost = 1'b0;
  odone = 1'b1;
end

always @* begin
  isum = (ICLKEN & IEN) & ~ipost;

  case (isum)
    7'b0000000: smux = 1'd0;
    7'b0000001: smux = I0;
    7'b0000010: smux = I1;
    7'b0000100: smux = I2;
    7'b0001000: smux = I3;
    7'b0010000: smux = I4;
    7'b0100000: smux = I5;
    7'b1000000: smux = I6;
    default:  smux = 'dx;
  endcase

  sdone = ~odone && ipost == ien;
end

always @(posedge CLK) begin
  if (CCLKEN) begin
    sum <= smux;
    ien <= IEN;
    ipost <= 1'b0;
    odone <= 1'b0;
  end
  else begin
    // Add input, set ipost
    sum <= sum + smux;
    ipost <= ipost | isum;

    // When sum is complete, output is ready
    if (sdone) begin
      odone <= 1'b1;
    end
  end
end

integer i;
always @* begin
  csel = CSEL0;
  for (i = 0; i < NI; i = i + 1)
    csel = csel + IEN[i];
end

always @*
  interp2(csel, sum, out);

assign O = out;
assign OCLKEN = sdone;
