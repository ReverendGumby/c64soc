reg [15:0] env_dac_lut [0:255];
initial begin
  env_dac_lut[0] = 16'd0;
  env_dac_lut[1] = 16'd2;
  env_dac_lut[2] = 16'd3;
  env_dac_lut[3] = 16'd4;
  env_dac_lut[4] = 16'd5;
  env_dac_lut[5] = 16'd6;
  env_dac_lut[6] = 16'd7;
  env_dac_lut[7] = 16'd9;
  env_dac_lut[8] = 16'd9;
  env_dac_lut[9] = 16'd11;
  env_dac_lut[10] = 16'd11;
  env_dac_lut[11] = 16'd13;
  env_dac_lut[12] = 16'd13;
  env_dac_lut[13] = 16'd15;
  env_dac_lut[14] = 16'd16;
  env_dac_lut[15] = 16'd18;
  env_dac_lut[16] = 16'd17;
  env_dac_lut[17] = 16'd19;
  env_dac_lut[18] = 16'd20;
  env_dac_lut[19] = 16'd21;
  env_dac_lut[20] = 16'd22;
  env_dac_lut[21] = 16'd23;
  env_dac_lut[22] = 16'd24;
  env_dac_lut[23] = 16'd26;
  env_dac_lut[24] = 16'd26;
  env_dac_lut[25] = 16'd28;
  env_dac_lut[26] = 16'd28;
  env_dac_lut[27] = 16'd30;
  env_dac_lut[28] = 16'd30;
  env_dac_lut[29] = 16'd32;
  env_dac_lut[30] = 16'd33;
  env_dac_lut[31] = 16'd35;
  env_dac_lut[32] = 16'd33;
  env_dac_lut[33] = 16'd35;
  env_dac_lut[34] = 16'd36;
  env_dac_lut[35] = 16'd37;
  env_dac_lut[36] = 16'd38;
  env_dac_lut[37] = 16'd39;
  env_dac_lut[38] = 16'd40;
  env_dac_lut[39] = 16'd42;
  env_dac_lut[40] = 16'd42;
  env_dac_lut[41] = 16'd44;
  env_dac_lut[42] = 16'd44;
  env_dac_lut[43] = 16'd46;
  env_dac_lut[44] = 16'd46;
  env_dac_lut[45] = 16'd48;
  env_dac_lut[46] = 16'd49;
  env_dac_lut[47] = 16'd51;
  env_dac_lut[48] = 16'd50;
  env_dac_lut[49] = 16'd52;
  env_dac_lut[50] = 16'd53;
  env_dac_lut[51] = 16'd54;
  env_dac_lut[52] = 16'd55;
  env_dac_lut[53] = 16'd56;
  env_dac_lut[54] = 16'd57;
  env_dac_lut[55] = 16'd59;
  env_dac_lut[56] = 16'd59;
  env_dac_lut[57] = 16'd61;
  env_dac_lut[58] = 16'd61;
  env_dac_lut[59] = 16'd63;
  env_dac_lut[60] = 16'd63;
  env_dac_lut[61] = 16'd65;
  env_dac_lut[62] = 16'd66;
  env_dac_lut[63] = 16'd68;
  env_dac_lut[64] = 16'd64;
  env_dac_lut[65] = 16'd66;
  env_dac_lut[66] = 16'd66;
  env_dac_lut[67] = 16'd68;
  env_dac_lut[68] = 16'd68;
  env_dac_lut[69] = 16'd70;
  env_dac_lut[70] = 16'd71;
  env_dac_lut[71] = 16'd73;
  env_dac_lut[72] = 16'd73;
  env_dac_lut[73] = 16'd74;
  env_dac_lut[74] = 16'd75;
  env_dac_lut[75] = 16'd77;
  env_dac_lut[76] = 16'd77;
  env_dac_lut[77] = 16'd79;
  env_dac_lut[78] = 16'd80;
  env_dac_lut[79] = 16'd82;
  env_dac_lut[80] = 16'd81;
  env_dac_lut[81] = 16'd83;
  env_dac_lut[82] = 16'd83;
  env_dac_lut[83] = 16'd85;
  env_dac_lut[84] = 16'd85;
  env_dac_lut[85] = 16'd87;
  env_dac_lut[86] = 16'd88;
  env_dac_lut[87] = 16'd90;
  env_dac_lut[88] = 16'd90;
  env_dac_lut[89] = 16'd91;
  env_dac_lut[90] = 16'd92;
  env_dac_lut[91] = 16'd94;
  env_dac_lut[92] = 16'd94;
  env_dac_lut[93] = 16'd96;
  env_dac_lut[94] = 16'd97;
  env_dac_lut[95] = 16'd99;
  env_dac_lut[96] = 16'd97;
  env_dac_lut[97] = 16'd98;
  env_dac_lut[98] = 16'd99;
  env_dac_lut[99] = 16'd101;
  env_dac_lut[100] = 16'd101;
  env_dac_lut[101] = 16'd103;
  env_dac_lut[102] = 16'd104;
  env_dac_lut[103] = 16'd106;
  env_dac_lut[104] = 16'd105;
  env_dac_lut[105] = 16'd107;
  env_dac_lut[106] = 16'd108;
  env_dac_lut[107] = 16'd110;
  env_dac_lut[108] = 16'd110;
  env_dac_lut[109] = 16'd112;
  env_dac_lut[110] = 16'd113;
  env_dac_lut[111] = 16'd115;
  env_dac_lut[112] = 16'd114;
  env_dac_lut[113] = 16'd115;
  env_dac_lut[114] = 16'd116;
  env_dac_lut[115] = 16'd118;
  env_dac_lut[116] = 16'd118;
  env_dac_lut[117] = 16'd120;
  env_dac_lut[118] = 16'd121;
  env_dac_lut[119] = 16'd123;
  env_dac_lut[120] = 16'd122;
  env_dac_lut[121] = 16'd124;
  env_dac_lut[122] = 16'd125;
  env_dac_lut[123] = 16'd127;
  env_dac_lut[124] = 16'd127;
  env_dac_lut[125] = 16'd129;
  env_dac_lut[126] = 16'd130;
  env_dac_lut[127] = 16'd132;
  env_dac_lut[128] = 16'd123;
  env_dac_lut[129] = 16'd125;
  env_dac_lut[130] = 16'd126;
  env_dac_lut[131] = 16'd128;
  env_dac_lut[132] = 16'd128;
  env_dac_lut[133] = 16'd130;
  env_dac_lut[134] = 16'd131;
  env_dac_lut[135] = 16'd133;
  env_dac_lut[136] = 16'd132;
  env_dac_lut[137] = 16'd134;
  env_dac_lut[138] = 16'd135;
  env_dac_lut[139] = 16'd137;
  env_dac_lut[140] = 16'd137;
  env_dac_lut[141] = 16'd139;
  env_dac_lut[142] = 16'd140;
  env_dac_lut[143] = 16'd141;
  env_dac_lut[144] = 16'd140;
  env_dac_lut[145] = 16'd142;
  env_dac_lut[146] = 16'd143;
  env_dac_lut[147] = 16'd145;
  env_dac_lut[148] = 16'd145;
  env_dac_lut[149] = 16'd147;
  env_dac_lut[150] = 16'd148;
  env_dac_lut[151] = 16'd150;
  env_dac_lut[152] = 16'd149;
  env_dac_lut[153] = 16'd151;
  env_dac_lut[154] = 16'd152;
  env_dac_lut[155] = 16'd154;
  env_dac_lut[156] = 16'd154;
  env_dac_lut[157] = 16'd156;
  env_dac_lut[158] = 16'd157;
  env_dac_lut[159] = 16'd158;
  env_dac_lut[160] = 16'd156;
  env_dac_lut[161] = 16'd158;
  env_dac_lut[162] = 16'd159;
  env_dac_lut[163] = 16'd161;
  env_dac_lut[164] = 16'd161;
  env_dac_lut[165] = 16'd163;
  env_dac_lut[166] = 16'd164;
  env_dac_lut[167] = 16'd165;
  env_dac_lut[168] = 16'd165;
  env_dac_lut[169] = 16'd167;
  env_dac_lut[170] = 16'd168;
  env_dac_lut[171] = 16'd170;
  env_dac_lut[172] = 16'd170;
  env_dac_lut[173] = 16'd172;
  env_dac_lut[174] = 16'd172;
  env_dac_lut[175] = 16'd174;
  env_dac_lut[176] = 16'd173;
  env_dac_lut[177] = 16'd175;
  env_dac_lut[178] = 16'd176;
  env_dac_lut[179] = 16'd178;
  env_dac_lut[180] = 16'd178;
  env_dac_lut[181] = 16'd180;
  env_dac_lut[182] = 16'd181;
  env_dac_lut[183] = 16'd182;
  env_dac_lut[184] = 16'd182;
  env_dac_lut[185] = 16'd184;
  env_dac_lut[186] = 16'd185;
  env_dac_lut[187] = 16'd187;
  env_dac_lut[188] = 16'd187;
  env_dac_lut[189] = 16'd189;
  env_dac_lut[190] = 16'd189;
  env_dac_lut[191] = 16'd191;
  env_dac_lut[192] = 16'd187;
  env_dac_lut[193] = 16'd189;
  env_dac_lut[194] = 16'd190;
  env_dac_lut[195] = 16'd192;
  env_dac_lut[196] = 16'd192;
  env_dac_lut[197] = 16'd194;
  env_dac_lut[198] = 16'd194;
  env_dac_lut[199] = 16'd196;
  env_dac_lut[200] = 16'd196;
  env_dac_lut[201] = 16'd198;
  env_dac_lut[202] = 16'd199;
  env_dac_lut[203] = 16'd200;
  env_dac_lut[204] = 16'd201;
  env_dac_lut[205] = 16'd202;
  env_dac_lut[206] = 16'd203;
  env_dac_lut[207] = 16'd205;
  env_dac_lut[208] = 16'd204;
  env_dac_lut[209] = 16'd206;
  env_dac_lut[210] = 16'd207;
  env_dac_lut[211] = 16'd209;
  env_dac_lut[212] = 16'd209;
  env_dac_lut[213] = 16'd211;
  env_dac_lut[214] = 16'd211;
  env_dac_lut[215] = 16'd213;
  env_dac_lut[216] = 16'd213;
  env_dac_lut[217] = 16'd215;
  env_dac_lut[218] = 16'd216;
  env_dac_lut[219] = 16'd217;
  env_dac_lut[220] = 16'd218;
  env_dac_lut[221] = 16'd219;
  env_dac_lut[222] = 16'd220;
  env_dac_lut[223] = 16'd222;
  env_dac_lut[224] = 16'd220;
  env_dac_lut[225] = 16'd222;
  env_dac_lut[226] = 16'd223;
  env_dac_lut[227] = 16'd225;
  env_dac_lut[228] = 16'd225;
  env_dac_lut[229] = 16'd227;
  env_dac_lut[230] = 16'd227;
  env_dac_lut[231] = 16'd229;
  env_dac_lut[232] = 16'd229;
  env_dac_lut[233] = 16'd231;
  env_dac_lut[234] = 16'd232;
  env_dac_lut[235] = 16'd233;
  env_dac_lut[236] = 16'd234;
  env_dac_lut[237] = 16'd235;
  env_dac_lut[238] = 16'd236;
  env_dac_lut[239] = 16'd238;
  env_dac_lut[240] = 16'd237;
  env_dac_lut[241] = 16'd239;
  env_dac_lut[242] = 16'd240;
  env_dac_lut[243] = 16'd242;
  env_dac_lut[244] = 16'd242;
  env_dac_lut[245] = 16'd244;
  env_dac_lut[246] = 16'd244;
  env_dac_lut[247] = 16'd246;
  env_dac_lut[248] = 16'd246;
  env_dac_lut[249] = 16'd248;
  env_dac_lut[250] = 16'd249;
  env_dac_lut[251] = 16'd250;
  env_dac_lut[252] = 16'd251;
  env_dac_lut[253] = 16'd252;
  env_dac_lut[254] = 16'd253;
  env_dac_lut[255] = 16'd255;
end
