module mos6581_summer #(parameter NI=5,
                        parameter IW=16,
                        parameter OW=16)
(
    input           nRES,
    input           CLK,
    input           CCLKEN,

    input [NI-1:0]  ICLKEN,
    input [NI-1:0]  IEN,
    input [IW-1:0]  I0,
    input [IW-1:0]  I1,
    input [IW-1:0]  I2,
    input [IW-1:0]  I3,
    input [IW-1:0]  I4,
    input [IW-1:0]  I5,
    input [IW-1:0]  I6,

    output          OCLKEN,
    output [OW-1:0] O
 );

/* Defines SCNT, IXDIQ, ix_lut, iy_lut, ... */
`include "summer.vh"

localparam CSEL0 = -2;          // csel 0 is for two inputs

`include "sum.vh"

endmodule
