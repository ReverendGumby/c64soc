module mos6581_filter
  (
   input                CLK,
   input                MCLK,
   input                SCLKEN,
   input                OCLKEN,

   input [10:0]         FC,
   input [7:0]          FCR1,
   input [7:0]          FCR2,

   input signed [16:0]  IN1,
   input signed [16:0]  IN2,
   input signed [16:0]  IN3,

   output signed [15:0] OUT // Q4.12
   );

localparam UFW = 16;            // unsigned sample
localparam SFW = 17;            // signed sample

wire [3:0]  res = FCR1[7:4];
wire        ex = FCR1[3];
wire        f3 = FCR1[2];
wire        f2 = FCR1[1];
wire        f1 = FCR1[0];
wire        c3off = FCR2[7];
wire        hp = FCR2[6];
wire        bp = FCR2[5];
wire        lp = FCR2[4];
wire [3:0]  vol = FCR2[3:0];

//////////////////////////////////////////////////////////////////////
// Intermediate sample clock enable generator
// iclk = SCLK / 16
//

reg iclken;
reg [3:0] iclken_cnt;
initial
  iclken_cnt = 0;

always @(posedge CLK) if (SCLKEN) begin
  iclken_cnt <= iclken_cnt + 1'b1;
end

always @(posedge CLK)
  iclken <= SCLKEN && &iclken_cnt;

//////////////////////////////////////////////////////////////////////
// Filter clock generator
//
// Since the filter has a feedback section, and the math operations
// require multiple delays, it requires multiple clocks per
// sample. Fortunately, CLK runs at many times the SCLK rate. So we
// just delay SCLKEN as many times as we need.

localparam FN = 8;
reg [FN-1:0] fclk;

initial
  fclk = {FN{1'b0}};

always @(posedge CLK)
  fclk <= {fclk[FN-2:0], SCLKEN};

//////////////////////////////////////////////////////////////////////
// svf: State Variable Filter

// Slot layout:
//          0    1    2    3    4    5    6    7    << fclk
// Input    EXT  IN1  IN2  IN3  ---  ---  ---  ---
// Output                  FBP  ---  ---  FLP  FHP

wire [UFW-1:0] flp, fbp, fhp;

mos6581_svf svf
  (
   .CLK(CLK),
   .CCLKEN(fclk[0]),
   .FC(FC),
   .Q(res),
   .IEN({f3, f2, f1}),
   .ICLKEN(fclk[3:1]),
   .IN1(IN1),
   .IN2(IN2),
   .IN3(IN3),
   .LPCLKEN(lpclken),
   .BPCLKEN(bpclken),
   .HPCLKEN(hpclken),
   .LP(flp),
   .BP(fbp),
   .HP(fhp)
   );

reg [UFW-1:0] in1b, in2b, in3b;
reg [UFW-1:0] flp1, fbp0, fbp1, fhp1;

initial begin
  in1b = 1'd0;
  in2b = 1'd0;
  in3b = 1'd0;
  flp1 = 1'd0;
  fbp0 = 1'd0;
  fhp1 = 1'd0;
end

always @(posedge CLK) begin
  // Delay filter inputs for input (as bypassed) to the next stage.
  if (fclk[1])
    in1b <= IN1;
  if (fclk[2])
    in2b <= IN2;
  if (fclk[3])
    in3b <= IN3;

  // Latch filter outputs and delay for input to the next stage.
  if (lpclken)
    flp1 <= flp;
  if (bpclken)
    fbp0 <= fbp;
  if (hpclken)
    fhp1 <= fhp;
  if (fclk[0])
    fbp1 <= fbp0;
end

//////////////////////////////////////////////////////////////////////
// mixer: Post-filter sum

// Slot layout:
// 0    1    2    3    4    5    6    7    << fclk
// EXT  IN1  IN2  IN3  FHP  FBP  FLP  ---

wire [UFW-1:0] sum;
wire           mixer_oclken;

reg [6:0]      mixer_ien;

always @* begin
  mixer_ien[0] = 1'b0;
  mixer_ien[1] = !f1;
  mixer_ien[2] = !f2;
  mixer_ien[3] = !f3 && !c3off;
  mixer_ien[4] = hp;
  mixer_ien[5] = bp;
  mixer_ien[6] = lp;
end

mos6581_mixer mixer
  (
   .CLK(CLK),
   .CCLKEN(fclk[7]),
   .IEN(mixer_ien),
   .ICLKEN(fclk[6:0]),
   .I0(1'd0),
   .I1(in1b),
   .I2(in2b),
   .I3(in3b),
   .I4(fhp1),
   .I5(fbp1),
   .I6(flp1),
   .OCLKEN(mixer_oclken),
   .O(sum)
   );

//////////////////////////////////////////////////////////////////////
// gain: Volume control

wire [UFW-1:0] preout;
wire           gain_oclken;

mos6581_gain gain
  (
   .CLK(CLK),
   .ICLKEN(mixer_oclken),
   .OCLKEN(gain_oclken),
   .VOL(vol),
   .I(sum),
   .O(preout)
   );

//////////////////////////////////////////////////////////////////////
// Decimator: Drop filter output from synthesis rate (SCLKEN) to
// intermediate sample rate (iclken)

wire [UFW-1:0] decout;

mos6581_filter_dec16 dec
  (
   .CLK(CLK),
   .ICLKEN(SCLKEN),
   .OCLKEN(iclken),
   .IN(preout),
   .OUT(decout)
   );

//////////////////////////////////////////////////////////////////////
// Final sample rate converter: from iclken to OCLKEN

// Sync iclken to MCLK
(* ASYNC_REG = "TRUE" *)
reg [2:0] mclk_iclken_dly;

always @(posedge MCLK) begin
  mclk_iclken_dly <= (mclk_iclken_dly << 1) | iclken;
end

wire mclk_iclken = mclk_iclken_dly[1] & !mclk_iclken_dly[2];

// Convert unsigned to signed
wire signed [15:0] resin = -16'sd32768 + $signed(decout);

mos6581_filter_resample_output resout
  (
   .CLK(MCLK),
   .ICLKEN(mclk_iclken),
   .OCLKEN(OCLKEN),
   .IN(resin),
   .OUT(OUT)
   );

endmodule
