module mos6581_reg_file
  (
   input             nRES,
   input             CLK,
   input             CP2,
   input             CP2_POSEDGE,
   input             CP2_NEGEDGE,
   input             BCLK_NEGEDGE,

   input [4:0]       A,           // address bus
   input [7:0]       DB_I,        // data bus
   output [7:0]      DB_O,
   output            DB_OE,
   input             RW,          // read / not write
   input             nCS,         // chip select
   input             HOLD,        // halt and release A,D,RW

   output reg [15:0] F1,
   output reg [11:0] PW1,
   output reg [7:0]  CR1,
   output reg [7:0]  AD1,
   output reg [7:0]  SR1,

   output reg [15:0] F2,
   output reg [11:0] PW2,
   output reg [7:0]  CR2,
   output reg [7:0]  AD2,
   output reg [7:0]  SR2,

   output reg [15:0] F3,
   output reg [11:0] PW3,
   output reg [7:0]  CR3,
   output reg [7:0]  AD3,
   output reg [7:0]  SR3,

   output reg [10:0] FC,
   output reg [7:0]  FCR1,
   output reg [7:0]  FCR2,

   input [7:0]       OSC3,
   input [7:0]       ENV3
   );

reg [4:0] addr_in;
reg [7:0] data_out;
reg [7:0] data_in;
reg       write;

always @(posedge CLK) if (CP2 && BCLK_NEGEDGE) begin
  write <= !nCS && !RW;
  data_in <= DB_I;
  addr_in <= A;
end

always @(posedge CLK) if (CP2_POSEDGE) begin
  if (!nRES) begin
    F1 <= 16'h0;
    PW1 <= 12'h0;
    CR1 <= 8'h0;
    AD1 <= 8'h0;
    SR1 <= 8'h0;
    F2 <= 16'h0;
    PW2 <= 12'h0;
    CR2 <= 8'h0;
    AD2 <= 8'h0;
    SR2 <= 8'h0;
    F3 <= 16'h0;
    PW3 <= 12'h0;
    CR3 <= 8'h0;
    AD3 <= 8'h0;
    SR3 <= 8'h0;
    FC <= 11'h0;
    FCR1 <= 8'h0;
    FCR2 <= 8'h0;
  end
  else begin
    if (write)
      case (addr_in)
        5'h00: F1[7:0] <= data_in;
        5'h01: F1[15:8] <= data_in;
        5'h02: PW1[7:0] <= data_in;
        5'h03: PW1[11:8] <= data_in[3:0];
        5'h04: CR1 <= data_in;
        5'h05: AD1 <= data_in;
        5'h06: SR1 <= data_in;
        5'h07: F2[7:0] <= data_in;
        5'h08: F2[15:8] <= data_in;
        5'h09: PW2[7:0] <= data_in;
        5'h0a: PW2[11:8] <= data_in[3:0];
        5'h0b: CR2 <= data_in;
        5'h0c: AD2 <= data_in;
        5'h0d: SR2 <= data_in;
        5'h0e: F3[7:0] <= data_in;
        5'h0f: F3[15:8] <= data_in;
        5'h10: PW3[7:0] <= data_in;
        5'h11: PW3[11:8] <= data_in[3:0];
        5'h12: CR3 <= data_in;
        5'h13: AD3 <= data_in;
        5'h14: SR3 <= data_in;
        5'h15: FC[2:0] <= data_in[2:0];
        5'h16: FC[10:3] <= data_in;
        5'h17: FCR1 <= data_in;
        5'h18: FCR2 <= data_in;
        default: ;
      endcase
  end
end

always @* begin
  data_out = 8'hff;
  case (addr_in)
    5'h00: data_out = F1[7:0];
    5'h01: data_out = F1[15:8];
    5'h02: data_out = PW1[7:0];
    5'h03: data_out[3:0] = PW1[11:8];
    5'h04: data_out = CR1;
    5'h05: data_out = AD1;
    5'h06: data_out = SR1;
    5'h07: data_out = F2[7:0];
    5'h08: data_out = F2[15:8];
    5'h09: data_out = PW2[7:0];
    5'h0a: data_out[3:0] = PW2[11:8];
    5'h0b: data_out = CR2;
    5'h0c: data_out = AD2;
    5'h0d: data_out = SR2;
    5'h0e: data_out = F3[7:0];
    5'h0f: data_out = F3[15:8];
    5'h10: data_out = PW3[7:0];
    5'h11: data_out[3:0] = PW3[11:8];
    5'h12: data_out = CR3;
    5'h13: data_out = AD3;
    5'h14: data_out = SR3;
    5'h15: data_out[2:0] = FC[2:0];
    5'h16: data_out = FC[10:3];
    5'h17: data_out = FCR1;
    5'h18: data_out = FCR2;
    5'h1b: data_out = OSC3;
    5'h1c: data_out = ENV3;
    default: ;
  endcase
end

assign DB_O = data_out;
assign DB_OE = !nCS && RW && CP2;

endmodule
