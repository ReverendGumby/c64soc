// Converts sample width and format
// In: IN_WIDTH bits, unsigned
// Out: OUT_WIDTH bits, two's complement (signed)
// Amplitude is preserved
module mos6581_sample_converter #(parameter IN_WIDTH=12, parameter OUT_WIDTH=16)
  (
   input [IN_WIDTH-1:0] IN,
   output reg [OUT_WIDTH-1:0] OUT
   );

integer in_signed, out_signed;

always @*
  in_signed <= IN - (1 << (IN_WIDTH - 1));

always @*
  out_signed <= in_signed << (OUT_WIDTH - IN_WIDTH);

always @*
  OUT <= out_signed[OUT_WIDTH-1:0];

endmodule
