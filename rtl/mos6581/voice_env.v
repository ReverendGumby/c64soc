// MOS6581 Voice Envelope Generator

module mos6581_voice_env
  (
   input        nRES,
   input        CLK,
   input        CLKEN,

   input [3:0]  ATK,
   input [3:0]  DCY,
   input [3:0]  STN,
   input [3:0]  RLS,
   input        GATE,
   
   output [7:0] ENV
   );

reg         gate_dly;
reg [14:0]  cnt_tmr, cnt_tmr_lut;
reg         cnt_tmr_match;
reg [4:0]   dcy_tmr, dcy_tmr_lut;
reg         dcy_tmr_match;
reg [4:0]   env_match_latches;
reg [3:0]   rate;
reg [7:0]   cnt;
reg [3:0]   adsr;               // 1-hot FSM

localparam [3:0] ADSR_ATK = 4'b1000;
localparam [3:0] ADSR_DCY = 4'b0100;
localparam [3:0] ADSR_STN = 4'b0010;
localparam [3:0] ADSR_RLS = 4'b0001;

//////////////////////////////////////////////////////////////////////

always @(posedge CLK) if (CLKEN) begin
  if (!nRES)
    gate_dly <= 1'b0;
  else
    gate_dly <= GATE;
end

// A 15-bit up-counter cnt_tmr acts as prescaler for clocking cnt.
// It is reset when it reaches cnt_tmr_lut or gate changes.
always @(posedge CLK) if (CLKEN) begin
  if (!nRES)
    cnt_tmr <= 15'h0;
  else begin
    if (GATE != gate_dly)
      cnt_tmr <= 15'h0;
    else if (dcy_tmr_match) // dcy_tmr divider
      if (cnt_tmr_match)
        cnt_tmr <= 15'h0;
      else if (adsr != ADSR_STN)
        cnt_tmr[14:0] <= cnt_tmr + 1'b1;
  end    
end

// rate reflects which of ATK, DCY, or RLS controls timing.
always @* begin
  case (adsr)
    ADSR_ATK: rate = ATK;
    ADSR_DCY: rate = DCY;
    ADSR_RLS: rate = RLS;
    default: rate = 4'hx;
  endcase
end

// cnt_tmr_lut is driven by the columns of a lookup table (LUT), where
// rate selects the row.
always @* begin
  case (rate)
    // Instead of a counter, actual Si uses a 15-bit LFSR, polynomial
    // x^13+x^14+1 (XNOR feedback). Its LUT enumerates register
    // states; on match, the register resets to all 0s. The comments
    // below include the matching LFSR states, LSB first.

    //                                 LFSR[0:15]        datasheet time
    4'd00: cnt_tmr_lut = 15'd7;     // 111111110000000   2 ms
    4'd01: cnt_tmr_lut = 15'd30;    // 100111111111111   8 ms
    4'd02: cnt_tmr_lut = 15'd61;    // 110000111111111   16 ms
    4'd03: cnt_tmr_lut = 15'd93;    // 111100110011111   24 ms
    4'd04: cnt_tmr_lut = 15'd147;   // 111111001111101   38 ms
    4'd05: cnt_tmr_lut = 15'd218;   // 010101010001100   56 ms
    4'd06: cnt_tmr_lut = 15'd265;   // 111111111110001   68 ms
    4'd07: cnt_tmr_lut = 15'd311;   // 100011111111010   80 ms
    4'd08: cnt_tmr_lut = 15'd390;   // 101101111011011   100 ms
    4'd09: cnt_tmr_lut = 15'd975;   // 101110111011111   250 ms
    4'd10: cnt_tmr_lut = 15'd1952;  // 111011011110011   500 ms
    4'd11: cnt_tmr_lut = 15'd3124;  // 111000100110010   800 ms
    4'd12: cnt_tmr_lut = 15'd3905;  // 111111011110001   1 s
    4'd13: cnt_tmr_lut = 15'd11718; // 101110000001000   3 s
    4'd14: cnt_tmr_lut = 15'd19530; // 010110111001000   5 s
    4'd15: cnt_tmr_lut = 15'd31249; // 001101101010111   8 s
    default: cnt_tmr_lut = 15'dx;
  endcase
end

always @*
  cnt_tmr_match <= cnt_tmr == cnt_tmr_lut;

// To do linear piecewise approximation of an exponential decay during
// the decay and release cycles, we divide the counting rate of
// cnt_tmr as env_cat drops below descending thresholds. The divisor
// is provided by dcy_tmr.

// env_match_latches track cnt passing the decay/release rate
// change thresholds.
always @(posedge CLK) if (CLKEN) begin
  if (!nRES)
    env_match_latches <= 5'b00000;
  else begin
    case (cnt)
      // Thresholds recovered from actual Si
      8'hff: env_match_latches <= 5'b00000;
      8'h5d: env_match_latches <= 5'b10000;
      8'h36: env_match_latches <= 5'b01000;
      8'h1a: env_match_latches <= 5'b00100;
      8'h0e: env_match_latches <= 5'b00010;
      8'h06: env_match_latches <= 5'b00001;
      8'h00: env_match_latches <= 5'b00000;
      default: ;
    endcase
  end
end

// A 5-bit up-counter dcy_tmr acts as prescaler for clocking cnt_tmr.
// It is reset when attacking (always /1) or it reaches dcy_tmr_lut.
always @(posedge CLK) if (CLKEN) begin
  if (!nRES)
    dcy_tmr <= 5'h0;
  else begin
    if (dcy_tmr_match)
      dcy_tmr <= 5'h0;
    else
      dcy_tmr[4:0] <= dcy_tmr + 1'b1;
  end
end

// dcy_tmr is driven by the columns of a lookup table (LUT), where
// env_match_latches selects the row.
always @* begin
  case (env_match_latches)
    // Instead of a counter, actual Si uses a 5-bit LFSR, polynomial
    // x^4+x^2+1 (XNOR feedback). Its LUT enumerates register states;
    // on match, the register resets to all 0s. The comments below
    // include the matching LFSR states, LSB first.

    //                                LFSR[0:4]
    5'b00000: dcy_tmr_lut = 5'd0;  // (n/a)
    5'b10000: dcy_tmr_lut = 5'd1;  // 11000
    5'b01000: dcy_tmr_lut = 5'd3;  // 01110
    5'b00100: dcy_tmr_lut = 5'd7;  // 00100
    5'b00010: dcy_tmr_lut = 5'd15; // 00010
    5'b00001: dcy_tmr_lut = 5'd29; // 00001
    default: dcy_tmr_lut = 5'dx;
  endcase
end

always @*
  dcy_tmr_match <= adsr == ADSR_ATK || dcy_tmr == dcy_tmr_lut;

// cnt is either incremented (attack) or decremented (decay/release)
// every cnt_tmr_match, or is held (sustain).
always @(posedge CLK) if (CLKEN) begin
  if (!nRES)
    cnt <= 8'h0;
  else begin
    if (dcy_tmr_match) begin
      if (cnt_tmr_match) begin
        case (adsr)
          ADSR_ATK: cnt[7:0] <= cnt + 1'b1;
          ADSR_STN: ;           // no change
          default:              // decay, release
            if (|cnt)           // only if > 0
              cnt[7:0] <= cnt - 1'b1;
        endcase
      end
    end
  end
end

// Attack, Decay, Sustain, Release
always @(posedge CLK) if (CLKEN) begin
  if (!nRES)
    adsr <= ADSR_RLS;
  else begin
    if (GATE) begin
      case (adsr)
        // Attack cycle begins when gate is asserted during release.
        ADSR_RLS: adsr <= ADSR_ATK;

        // Decay cycle begins when cnt reaches max.
        ADSR_ATK:
          if (cnt == 8'd255)
            adsr <= ADSR_DCY;

        // Sustain cycle begins when cnt reaches STN.
        ADSR_DCY:
          if (cnt == {STN, STN})
            adsr <= ADSR_STN;
      endcase
    end
    else /* if (!GATE) */ begin 
      // Release cycle begins whenever gate is deasserted, from any cycle.
      adsr <= ADSR_RLS;
    end
  end
end

// Envelope generator outputs
assign ENV = cnt;

endmodule
