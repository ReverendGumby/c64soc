#!/usr/bin/env python

# Convert coefficient array output from Mathematica to Verilog
#
# Run like this:
#   python xk.py

import sys
import math

def read_txt(fn):
    c = []
    with open(fn) as f:
        for line in f:
            strvals = line.strip('{ }\n').split(',')
            for s in strvals:
                try:
                    v = int(s)
                    c.append(v)
                except ValueError:
                    pass                
    return c


def write_v(reg_name, fn, c):
    num = len(c)
    ca = [abs(v) for v in c]
    bits = int(math.ceil(math.log(max(ca),2))) + 1
    
    with open(fn, 'w') as f:
        print >> f, 'reg signed [{0}:0] {1} [0:{2}];'.format(bits-1, reg_name, num-1)
        print >> f, 'initial begin'
        for i in range(num):
            v = c[i]
            b = bits
            if v < 0:
                v = -v
                b = -b
            print >> f, "  {0}[{1}] = {2}'sd{3};".format(reg_name, i, b, v)
        print >> f, 'end'

# For example, bk2.txt contains:
#
#  {1, 2, 3, 4, 5, 7, 8, 10, 12, 13, 16, 18, 20, 23, 25, 28, 31, 34, 37, 40, 44, 
#  47, 51, 55, 59, 63, 67, 72, 76, 81, 86, 91, 96, 101, 107, 112, 118, 124, 130,
#  :
#  291344, 291627, 291911, 292194}
#
# This produces bk2_lut.v containing:
#
#  reg signed [19:0] bk2_lut [0:2047];
#  initial begin
#    bk2[0] = 20'sd1;
#    :
#    bk2[2047] = 20'sd292194;

def convert(root):
    coeff = read_txt(root + '.txt')
    reg_name = root + '_lut'
    write_v(reg_name, reg_name + '.v', coeff)

#convert('ak1')
#convert('ak2')
#convert('bk1')
