// The 6581 waveform generator DAC has a non-linear response and a DC
// offset. This uses a LUT to mimic these properties.
//
// Model source: reSID [https://en.wikipedia.org/wiki/ReSID] dac.cc

module mos6581_wave_dac
  (
   input                nRES,
   input                CLK,
   input                CLKEN,

   input [11:0]         IN,     // digital: 12-bit code word
   output signed [12:0] OUT     // analog: Q1.12
   );

`include "wave_dac_lut.v"

reg [15:0] lutout;              // UQ4.12, range [0 .. 1)

reg signed [12:0] aoff = 13'sh380; // Q1.12, DC offset

always @(posedge CLK) if (CLKEN) begin
  if (!nRES)
    lutout <= 0;
  else
    lutout <= wave_dac_lut[IN];
end

// Subtract DC offset from LUT output to get DAC analog output.

assign OUT = $signed(lutout[12:0]) - aoff;

endmodule
