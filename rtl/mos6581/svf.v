// State Variable Filter: top level
//
// Model source: reSID [https://en.wikipedia.org/wiki/ReSID]
//   filter.h - Filter::solve_integrate_6581
//
// MATLAB Simulink model:
//   /doc/mos6581/design/svf/resid/svf.slx, svf.pdf
//   tables mf, m: /doc/mos6581/svf/resid/filter_data.m

module mos6581_svf #(parameter IW = 16,
                     parameter AW = 18, // (internal) adder width
                     parameter MW = 32, // multiplier width
                     parameter OW = 16)
(
   input           CLK,

   input           CCLKEN,
   input [10:0]    FC,
   input [3:0]     Q,
   input [2:0]     IEN,

   input [2:0]     ICLKEN,
   input [IW-1:0]  IN1,
   input [IW-1:0]  IN2,
   input [IW-1:0]  IN3,

   output          LPCLKEN,
   output          BPCLKEN,
   output          HPCLKEN,
   output [OW-1:0] LP,
   output [OW-1:0] BP,
   output [OW-1:0] HP
 );

reg [3:0]    q;
reg          cclken;

reg [15:0]   hp_nm1;
reg [15:0]   vddt_vw_2;

wire [15:0]  bp, lp, hp, bpq;

`include "vddt_vw_2_lut.vh"

initial begin
  vddt_vw_2 = 16'd0;
  q = 1'd0;
  hp_nm1 = 1'd0;
end

always @(posedge CLK) begin
  if (CCLKEN) begin
    vddt_vw_2 <= vddt_vw_2_lut[FC];
    q <= Q;
  end
end

mos6581_gain gain
  (
   .CLK(CLK),
   .ICLKEN(oclken_bpint),
   .OCLKEN(oclken_gain),
   .VOL(4'd15 - q),
   .I(bp),
   .O(bpq)
   );

mos6581_summer summer
  (
   .CLK(CLK),
   .CCLKEN(CCLKEN),
   .IEN({2'b11, IEN}),
   .ICLKEN({oclken_lpint, oclken_gain, ICLKEN}),
   .I0(IN1),
   .I1(IN2),
   .I2(IN3),
   .I3(bpq),
   .I4(lp),
   .OCLKEN(oclken_summer),
   .O(hp)
   );

mos6581_integrator bpint
  (
   .CLK(CLK),
   .ICLKEN(CCLKEN),
   .OCLKEN(oclken_bpint),
   .VI(hp_nm1),
   .VDDT_VW_2(vddt_vw_2),
   .VO(bp)
   );

mos6581_integrator lpint
  (
   .CLK(CLK),
   .ICLKEN(oclken_bpint),
   .OCLKEN(oclken_lpint),
   .VI(bp),
   .VDDT_VW_2(vddt_vw_2),
   .VO(lp)
   );

// Update prior sample regs
always @(posedge CLK) begin
  if (HPCLKEN) begin
    hp_nm1 <= hp;
  end
end

assign LP = lp;
assign LPCLKEN = oclken_lpint;
assign BP = bp;
assign BPCLKEN = oclken_bpint;
assign HP = hp;
assign HPCLKEN = oclken_summer;

endmodule
