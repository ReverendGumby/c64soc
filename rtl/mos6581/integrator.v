// State Variable Filter: integrator
//
// Model source: reSID [https://en.wikipedia.org/wiki/ReSID]
//   filter.h - Filter::solve_integrate_6581
//
// MATLAB Simulink model:
//   /doc/mos6581/design/svf/resid/integrator.slx, integrator.pdf
//   tables mf, m: /doc/mos6581/svf/resid/filter_data.m

module mos6581_integrator #(parameter IW = 16,
                            parameter AW = 18, // (internal) adder width
                            parameter MW = 32, // multiplier width
                            parameter OW = 16)
(
   input           CLK,
   input           ICLKEN,
   output          OCLKEN,

   input [IW-1:0]  VI,
   input [IW-1:0]  VDDT_VW_2,
   output [OW-1:0] VO
 );

localparam MF_KVDDT = 65535;    // mf.kVddt - analog rail @ full scale
localparam MF_N_SNAKE = 15;     // mf.n_snake

reg [IW-1:0] vi,                // vi == in
             vx_nm1,            // vx[n-1]
             vgst,              // Vgst
             vgdt,              // Vgdt
             vcr_in,
             vgs,               // Vgs
             vgd,               // Vgd
             or_in,
             vddt_vw_2;         // Vddt_Vw_2 = ((mf.kVddt - Vw) ^ 2 / 2) >> 16

wire [IW-1:0] kvg,              // kVg - VCR gate voltage
              igs,
              igd,
              vx;

reg signed [AW-1:0] vgs_s, vgd_s;

reg [MW-1:0] vgst_2,
             vgdt_2;            // Vgdt_2

reg signed [MW-1:0] n_n_snake, n_i_snake, n_i_vcr;
reg signed [MW-1:0] m_vc, vc, vc_s, vc_nm1, vo;

initial begin
  vi = 1'd0;
  vddt_vw_2 = 1'd0;
  vc_s = 1'd0;
  vc_nm1 = 1'd0;
  vx_nm1 = 1'd0;
end

always @(posedge CLK) begin
  if (ICLKEN) begin
    vi <= VI;
    vddt_vw_2 <= VDDT_VW_2;
  end
end

always @* begin
  vgst = MF_KVDDT - vx_nm1;
  vgst_2 = vgst * vgst;

  vgdt = MF_KVDDT - VI;
  vgdt_2 = vgdt * vgdt;

  // "Snake" current
  n_n_snake = $signed(vgst_2 - vgdt_2) >>> 15;
  n_i_snake = n_n_snake * MF_N_SNAKE;
end

// VCR gate voltage
always @*
  vcr_in = (vgdt_2 >> 17) + vddt_vw_2;

vcr_kvg vcr_kvg
  (
   .CSEL(1'd0),
   .CLK(CLK),
   .ICLKEN(ICLKEN),
   .OCLKEN(oclken_vcr),
   .I(vcr_in),
   .O(kvg)
   );

always @* begin
  // VCR voltages for EKV model table lookup
  vgs_s = kvg - vx_nm1;
  vgd_s = kvg - vi;
  vgs = vgs_s < 0 ? 0 : $unsigned(vgs_s);
  vgd = vgd_s < 0 ? 0 : $unsigned(vgd_s);
end

// VCR current
vcr_n_ids_term vcr_igs 
  (
   .CSEL(1'd0),
   .CLK(CLK),
   .ICLKEN(oclken_vcr),
   .OCLKEN(oclken_igs),
   .I(vgs),
   .O(igs)
   );

vcr_n_ids_term vcr_igd 
  (
   .CSEL(1'd0),
   .CLK(CLK),
   .ICLKEN(oclken_vcr),
   .OCLKEN(),
   .I(vgd),
   .O(igd)
   );

always @* begin
  n_i_vcr = $signed(igs - igd) << 15;

  // Change in capacitor charge
  m_vc = n_i_snake + n_i_vcr;
  vc = vc_nm1 - m_vc;
end

// vx = g(vc)
always @*
  or_in = $unsigned(vc >>> 15) + (1 << 15);

opamp_rev opamp_rev
  (
   .CSEL(1'd0),
   .CLK(CLK),
   .ICLKEN(oclken_igs),
   .OCLKEN(oclken_or),
   .I(or_in),
   .O(vx)
);

always @(posedge CLK) begin
  if (oclken_igs)
    vc_s <= vc >>> 14;
end

always @*
  vo = vx + vc_s;

// Update prior sample regs
always @(posedge CLK) begin
  if (OCLKEN) begin
    vc_nm1 <= vc;
    vx_nm1 <= vx;
  end
end

// vo will be valid by the OCLKEN=1 latch time.
assign VO = vo[OW-1:0];
assign OCLKEN = oclken_or;

endmodule
