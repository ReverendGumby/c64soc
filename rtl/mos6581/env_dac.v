// The 6581 envelope generator DAC has a non-linear response. This
// uses a LUT to mimic these properties.
//
// Model source: reSID [https://en.wikipedia.org/wiki/ReSID] dac.cc

module mos6581_env_dac
  (
   input               nRES,
   input               CLK,
   input               CLKEN,

   input [7:0]         IN, // digital: 8-bit code word
   output signed [8:0] OUT // analog: Q1.8
   );

`include "env_dac_lut.v"

reg [15:0] lutout;              // UQ8.8, range [0 .. 1)

always @(posedge CLK) if (CLKEN) begin
  if (!nRES)
    lutout <= 0;
  else
    lutout <= env_dac_lut[IN];
end

assign OUT = $signed(lutout[8:0]);

endmodule
