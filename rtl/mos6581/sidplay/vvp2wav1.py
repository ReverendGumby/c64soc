import sys

fout = open(sys.argv[1], 'wb')

for line in sys.stdin:
    try:
        v = int(line, 16)
        bv = chr((v >> 8) & 0xFF) + chr((v >> 0) & 0xFF)
        fout.write(bv)
    except ValueError:
        pass
