# Set to 1 for pre-filter (1 MHz) output, 2 for post-filter (48 kHz) output.
OUT := 1

ifeq ($(OUT),1)
SR := 1024k
endif
ifeq ($(OUT),2)
SR := 48k
endif

VVP := sidplay-$(SR).vvp
PCM := sidplay-$(SR).pcm
WAV := sidplay-$(SR).wav

all: sidplay

sidplay: $(WAV)

$(WAV): $(PCM)
	sox -r $(SR) -e signed -b 16 -c 1 -B -t raw $(PCM) $(WAV)

$(PCM): $(VVP)
	./$(VVP) | python vvp2wav1.py $@

SOURCES := \
	../filter.v ../filter_dec.v \
	../filter_resample_output.v ../reg_file.v \
	../gain.v ../mixer.v ../summer.v ../integrator.v \
	../vcr_kvg.v ../vcr_n_ids_term.v ../opamp_rev.v ../svf.v \
	../sample_converter.v ../serializer.v ../voice.v ../voice_env.v \
	../wave.v ../wave_dac.v ../env_dac.v \
	../mos6581.v \
	sidplay_tb.v

../gain.v: ../gain.vh
../mixer.v: ../mixer.vh

sidplay_tb.v: sidplay_song.vh

$(VVP): $(SOURCES)
	iverilog -DSIDPLAY_OUT_$(OUT) -grelative-include -s sidplay_tb -t vvp -o $@ $^

clean:
	rm -f $(VVP) $(PCM) $(WAV)
