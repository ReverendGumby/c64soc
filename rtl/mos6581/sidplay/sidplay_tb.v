`timescale 1us / 1ps

module sidplay_tb
  (
   );

wire        mclk, sclk, lrclk, sdout;
wire        CP2_POSEDGE, CP2_NEGEDGE, BCLK_NEGEDGE;

reg         MCLK, CLK, nRES;
reg         CP2;
reg         ncs, rw;
reg [4:0]   a;
reg [7:0]   db_i;

mos6581 sid
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP2(CP2),
   .CP2_POSEDGE(CP2_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .BCLK_NEGEDGE(BCLK_NEGEDGE),
   .A(a),
   .DB_I(db_i),
   .RW(rw),
   .nCS(ncs),
   .MCLK(MCLK),
   .SCLK(sclk),
   .LRCLK(lrclk),
   .SDOUT(sdout),
   .HOLD(1'b0)
   );

initial begin
  //$dumpfile("sidplay.vcd");
  //$dumpvars;

  clk_cnt = 3'b000;
  CLK = 1'b1;
  MCLK = 1'b1;
  nRES = 1'b0;
  ncs = 1'b1;
  rw = 1'b1;
  #10 @(negedge CP2) nRES = 1'b1;
  play();
  #10 $finish();
end

localparam [4:0] F1L  = 5'h00;
localparam [4:0] F1H  = 5'h01;
localparam [4:0] PW1L = 5'h02;
localparam [4:0] PW1H = 5'h03;
localparam [4:0] CR1  = 5'h04;
localparam [4:0] AD1  = 5'h05;
localparam [4:0] SR1  = 5'h06;
localparam [4:0] F2L  = 5'h07;
localparam [4:0] F2H  = 5'h08;
localparam [4:0] PW2L = 5'h09;
localparam [4:0] PW2H = 5'h0a;
localparam [4:0] CR2  = 5'h0b;
localparam [4:0] AD2  = 5'h0c;
localparam [4:0] SR2  = 5'h0d;
localparam [4:0] F3L  = 5'h0e;
localparam [4:0] F3H  = 5'h0f;
localparam [4:0] PW3L = 5'h10;
localparam [4:0] PW3H = 5'h11;
localparam [4:0] CR3  = 5'h12;
localparam [4:0] AD3  = 5'h13;
localparam [4:0] SR3  = 5'h14;
localparam [4:0] FCL  = 5'h15;
localparam [4:0] FCH  = 5'h16;
localparam [4:0] FCR1 = 5'h17;
localparam [4:0] FCR2 = 5'h18;

task play;
  begin
`include "sidplay_song.vh"
  end
endtask

task play_line;
input [4:0] addr;
input [7:0] data;
input cycles;
integer cycles;
  begin
    while (cycles > 1) begin
      @(negedge CP2) cycles = cycles - 1;
    end
    a = addr;
    db_i = data;
    ncs = 1'b0;
    rw = 1'b0;
    @(negedge CP2) ncs = 1'b1;
    rw = 1'b1;
  end
endtask

always #0.060 begin :clkgen
  CLK = !CLK;
end

always #0.020 begin :mclkgen
  MCLK = !MCLK;
end

reg [2:0] clk_cnt;

always @(negedge CLK)
  clk_cnt <= clk_cnt + 3'b001;

assign BCLK_NEGEDGE = clk_cnt[1:0] == 2'd2;
assign CP2_POSEDGE = clk_cnt[2:0] == 3'd4;
assign CP2_NEGEDGE = clk_cnt[2:0] == 3'd7;
always @(posedge CLK) CP2 <= clk_cnt[2] && clk_cnt[1:0] != 2'd3;

`ifdef SIDPLAY_OUT_1
reg signed [15:0] dout;
always @(posedge CLK) if (CP2_NEGEDGE) begin
  dout = sid.filter.IN1 + sid.filter.IN2 + sid.filter.IN3;
  $display("%h", dout);
end
`endif

`ifdef SIDPLAY_OUT_2
reg signed [15:0] dout;
always @(posedge MCLK) if (sid.filter.OCLKEN) begin
  dout = sid.filter.OUT;
  $display("%h", dout);
end
`endif

// Because I'm impatient
//initial #6500000 $finish();

endmodule
