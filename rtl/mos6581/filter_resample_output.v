//////////////////////////////////////////////////////////////////////
// Fractional-rate sample rate converter
//
// Designed and realized using MATLAB Filter Designer. Parameters:
//   Structure: direct-form FIR polyphase sample-rate converter
//   Order: 95
//   Rate change: 3/4

module mos6581_filter_resample_output
  (
   input                    CLK,
   input                    ICLKEN,
   input                    OCLKEN,

   input signed [15:0]      IN, // Q4.12
   output reg signed [15:0] OUT // Q4.12
   );

reg signed [15:0] in_hold;      // input buffer, Q4.12
reg               in_hold_full;
reg signed [15:0] zram [0:31];  // delay line, Q4.12
reg signed [31:0] mac1, mac2, mac3, outs; // multiply-and-accumulate, Q5.27
reg [47:0]        pk_lut [0:31];// coefficient lookup table
reg [5:0]         maccnt;       // MAC counter
reg [47:0]        pk;           // current coefficient 3-tuple
reg signed [15:0] p1, p2, p3;   // current coefficients, Q1.15
reg signed [15:0] z;            // current sample from zram, Q4.12
reg [1:0]         macsel;       // MAC selector
reg [4:0]         zidx_in;      // index to where to put newest sample in zram
reg [4:0]         zidx_out;     // MAC zram fetch index

wire mac_start, mac_fetching_samples;

integer i;
initial begin
  in_hold = 0;
  in_hold_full = 1'b0;
  outs = 32'd0;
  maccnt = MACCNT_N;
  pk = 48'd0;
  for (i=0; i<32; i=i+1)
    zram[i] = 16'd0;
  z = 16'd0;
  macsel = 2'd1;                // somehow, this yields correct output
  zidx_in = 4'd0;
  zidx_out = 0;
end

// The coefficient lookup table contains 3-tuples:
//   pk[x][15:00] = p1[n]
//   pk[x][31:16] = p2[n]
//   pk[x][47:32] = p3[n]
initial begin
  pk_lut[0]  = 48'hfffbfffe0000; 
  pk_lut[1]  = 48'h000a0000fffb; 
  pk_lut[2]  = 48'h000000120013; 
  pk_lut[3]  = 48'hffd4ffceffe4; 
  pk_lut[4]  = 48'h006e00410000; 
  pk_lut[5]  = 48'hff800000005c; 
  pk_lut[6]  = 48'h0000ff52ff2c; 
  pk_lut[7]  = 48'h0130017900e8; 
  pk_lut[8]  = 48'hfd89fe760000; 
  pk_lut[9] =  48'h02810000fe08; 
  pk_lut[10] = 48'h0000032a03fb; 
  pk_lut[11] = 48'hfaf2f9a7fc02; 
  pk_lut[12] = 48'h0a6806770000; 
  pk_lut[13] = 48'hf4960000086f; 
  pk_lut[14] = 48'h0000ef64ecc4; 
  pk_lut[15] = 48'h564b3cba1c66; 
  pk_lut[16] = 48'h3cba564b6000; 
  pk_lut[17] = 48'hef6400001c66; 
  pk_lut[18] = 48'h0000f496ecc4; 
  pk_lut[19] = 48'h06770a68086f; 
  pk_lut[20] = 48'hf9a7faf20000; 
  pk_lut[21] = 48'h032a0000fc02; 
  pk_lut[22] = 48'h0000028103fb; 
  pk_lut[23] = 48'hfe76fd89fe08; 
  pk_lut[24] = 48'h017901300000; 
  pk_lut[25] = 48'hff52000000e8; 
  pk_lut[26] = 48'h0000ff80ff2c; 
  pk_lut[27] = 48'h0041006e005c; 
  pk_lut[28] = 48'hffceffd40000; 
  pk_lut[29] = 48'h00120000ffe4; 
  pk_lut[30] = 48'h0000000a0013; 
  pk_lut[31] = 48'hfffefffbfffb;
end

// Input buffer - incoming sample arrives on ICLKEN, departs when
// delay line can safely accept a new sample.
always @(posedge CLK) begin
  if (ICLKEN) begin
    in_hold <= IN;
    in_hold_full <= 1'b1;
  end
  else if (!mac_fetching_samples)
    in_hold_full <= 1'b0;
end

// Delay line - circular buffer of incoming samples
always @(posedge CLK) begin
  if (in_hold_full && !mac_fetching_samples) begin
    zidx_in <= zidx_in + 1'd1;
    zram[zidx_in] <= in_hold;
  end
end

// Convolution engine 
// Every OCLKEN, runs three parallel 32-sample MACs
localparam [5:0] MACCNT_LAST_K = 6'd32;
localparam [5:0] MACCNT_SEL = MACCNT_LAST_K + 2;
localparam [5:0] MACCNT_N = MACCNT_SEL + 1;

assign mac_start = OCLKEN;

always @(posedge CLK) begin
  if (mac_start)
    maccnt <= 0;
  else if (maccnt < MACCNT_N)
    maccnt <= maccnt + 1'd1;
end

assign mac_fetching_samples = maccnt < MACCNT_LAST_K;

// Fetch coefficient from lookup table
always @(posedge CLK)
  if (maccnt < MACCNT_LAST_K)
    pk <= pk_lut[maccnt];

always @* begin
  p1 = pk[15:0];
  p2 = pk[31:16];
  p3 = pk[47:32];
end

// Fetch sample from delay line
always @(posedge CLK) begin
  if (mac_start)
    zidx_out <= zidx_in - 1'd1;
  else if (maccnt < MACCNT_LAST_K)
    zidx_out <= zidx_out - 1'd1;
end

always @(posedge CLK) begin
  if (maccnt < MACCNT_LAST_K)
    z <= zram[zidx_out];
end

// Triple convolute
always @(posedge CLK) begin
  if (maccnt == MACCNT_N) begin
    mac1 <= 0;
    mac2 <= 0;
    mac3 <= 0;
  end
  else if (maccnt < MACCNT_LAST_K + 1) begin
    mac1 <= mac1 + z * p1;
    mac2 <= mac2 + z * p2;
    mac3 <= mac3 + z * p3;
  end
end

// Select one of the MACs as output in round-robin fashion
always @(posedge CLK) begin
  if (maccnt == MACCNT_SEL) begin
    case (macsel)
      2'd0: begin
        outs <= mac1;
        macsel <= 2'd1;
      end
      2'd1: begin
        outs <= mac2;
        macsel <= 2'd2;
      end
      2'd2: begin
        outs <= mac3;
        macsel <= 2'd0;
      end
      default: outs <= 32'dx;
    endcase
  end
end

always @*
  OUT = outs >> 15; // scale Q5.27 down to Q4.12

endmodule
