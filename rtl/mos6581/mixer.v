module mos6581_mixer #(parameter NI=7,
                       parameter IW=16,
                       parameter OW=16)
(
    input           CLK,
    input           CCLKEN,

    input [NI-1:0]  ICLKEN,
    input [NI-1:0]  IEN,
    input [IW-1:0]  I0,
    input [IW-1:0]  I1,
    input [IW-1:0]  I2,
    input [IW-1:0]  I3,
    input [IW-1:0]  I4,
    input [IW-1:0]  I5,
    input [IW-1:0]  I6,

    output          OCLKEN,
    output [OW-1:0] O
 );

/* Defines SCNT, IXDIQ, ix_lut, iy_lut, ... */
`include "mixer.vh"

localparam CSEL0 = -1;          // csel 0 is for one input

`include "sum.vh"

endmodule
