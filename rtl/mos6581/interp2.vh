/*

interp2: Using liner approximation, model the voltage curve of an
NMOS-based inverter operating in the ohmic region.

Compared (first version) interp, this version supports 2-D ix*_lut,
for full per-curve customization.

To keep this code generic, parameters and the approx. LUTs are
computed by MATLAB and `include'd externally by the calling module.

Parameters:
  CSELW         csel (curve selector) register width
  IIW
  IOW
  SCNT
  IXDIW
  IXDIQ

LUTs:
  reg [IIW-1:0] ix_lut [0:CCNT-1][0:SCNT-1];
  reg signed [IIW:0] ixd_lut [0:CCNT-1][0:SCNT-1];
  reg [IOW-1:0] iy_lut [0:CCNT-1][0:SCNT-1];
  reg signed [IOW:0] iyd_lut [0:CCNT-1][0:SCNT-1];
  reg signed [IXDIW-1:0] ixdi_lut [0:CCNT-1][0:SCNT-1];

*/

localparam YXW = IIW + IOW - 5; // trial-n-error

localparam MXIW = IIW+YXW-1;

task interp2;
input [CSELW-1:0] csel;
input [IIW-1:0] in;
output [IOW-1:0] out;
integer         i, j, sel;
reg [0:SCNT-1]  cmp;
reg signed [IIW:0] ix, ixd, x;
reg signed [IOW:0] iy, iyd, mx;
reg signed [IIW+IOW-1:0] yxi;
reg signed [YXW:0] yx;          // Q(YXW+1).0
reg signed [IXDIW-1:0] ixdi;    // Q1.(IDXIQ)
reg signed [MXIW:0] mxi;        // Q(YXW+1).(IXDIQ)
  begin
    /*
     Comparators for input vs. ix_lut[]
     */
    for (j = 0; j < SCNT; j = j + 1) begin: gcmp
      cmp[j] = in < ix_lut[csel][j];
    end

    /*
     Find the ix_lut[][] pair that includes the input:
     ix_lut[][sel] = low side,
     ix_lut[][sel + 1] = high side
     */
    sel = SCNT - 1;
    for (i = 0; i < SCNT - 1; i = i + 1) begin: fsel
      if (cmp[i] != cmp[i + 1])
        sel = i;
    end

    /* Compute the interpolated output. */
    ix = ix_lut[csel][sel];
    iy = iy_lut[csel][sel];
    ixd = ixd_lut[csel][sel];
    iyd = iyd_lut[csel][sel];
    ixdi = ixdi_lut[csel][sel];

    x = in - ix;
    yxi = x * iyd;
    {yx} = yxi[YXW:0] + (ixd / 2);
    mxi = yx * ixdi;
    if (sel < SCNT - 1)
      mx = mxi[IOW+IXDIQ:IXDIQ];
    else
      mx = 1'b0;
    {out} = mx[IOW-1:0] + iy[IOW-1:0];
    /* $display("cmp=%x sel=%2d csel=%d ix=%d iy=%d x=%d ixd=%d iyd=%d yx=%d mxi=%d mx=%d",
             cmp, sel, csel, ix, iy, x, ixd, iyd, yx, mxi, mx); */
  end
endtask
