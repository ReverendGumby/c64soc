matches = [
    'LLRRR',
    'RLLLR',
    'RRLRR',
    'LLLRL',
    'RRRRL',
]

def trans(sr):
    r = ''
    for i in range(len(sr)):
        r = r + ('0' if sr[i] == 'R' else '1')
    return r

def shift(sr):
    if sr[2] + sr[4] in ['01', '10']:
        return '0' + sr[:4]
    return '1' + sr[:4]

i = 0
sr = '00000'
while True:
    sr = shift(sr)
    #print sr, i
    if sr == trans(matches[0]):
        print sr, i
        matches = matches[1:]
    if not matches or sr == '00000':
        break
    i = i + 1
