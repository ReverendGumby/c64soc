/* gain: Volume control

This stage scales the mixer output by the volume control.

We're using linear approximation to model the transistor's voltage
curves, instead of the very large lookup table used by resid.

The volume control register VOL selects the voltage curve. When VOL is
0, the curve is flat.

*/

module mos6581_gain #(parameter IW = 16,
                      parameter OW = 16)
(
   input           CLK,
   input           ICLKEN,
   output          OCLKEN,

   input [3:0]     VOL,
   input [IW-1:0]  I,
   output [OW-1:0] O
 );

localparam CSELW = 4;           // VOL is csel

/* Defines CNT, IXDIQ, ix, iy, ... */
`include "gain.vh"

/* Defines the linear approximation interpolation function */
`include "interp.vh"

reg [CSELW-1:0]     csel;
reg signed [IW-1:0] in;
reg [OW-1:0]        out;
reg                 oclken;

initial begin
  csel = {CSELW{1'b0}};
  in = {IW{1'b0}};
  oclken = 1'b0;
end

always @(posedge CLK) begin
  if (ICLKEN) begin
    csel <= VOL;
    in <= I;
  end
  // out will be valid by the oclken=1 latch time.
  oclken <= ICLKEN;
end

always @*
  interp(csel, in, out);

assign O = out;
assign OCLKEN = oclken;

endmodule
