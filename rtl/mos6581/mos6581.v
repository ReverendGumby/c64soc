// MOS6581: Sound Interface Device (SID)
//
// This model aims to faithfully reproduce the digital portions of the
// SID. The analog portions will have to be done digitally in this
// design, making the resulting audio notably cleaner than the
// original.
//
// Audio is output digitally via I2S. SID is master, supplying SCLK
// (positive) and LRCLK (positive).
//
// Sources:
//  https://en.wikipedia.org/wiki/MOS_Technology_SID#cite_note-11
//  http://www.oxyron.de/html/registers_sid.html (noise)
//  http://codebase64.org/doku.php?id=base:noise_waveform#the_noise-waveform
//  http://sid.kubarth.com

module mos6581
  (
   inout [31:0]  DEBUG,
   input         nRES,
   input         CLK,
   input         CP2,
   input         CP2_POSEDGE,
   input         CP2_NEGEDGE,
   input         BCLK_NEGEDGE,

   input [4:0]   A,             // address bus
   input [7:0]   DB_I,          // data bus, input
   output [7:0]  DB_O,          //   output
   output        DB_OE,         //   output enable
   input         RW,            // read / not write
   input         nCS,           // chip select

   input         MCLK,          // CODEC master clock
   output        SCLK,          // serial bit clock
   output        LRCLK,         // frame sync
   output        SDOUT,         // serial data out

   output [15:0] PDOUT,         // parallel data out

   input         HOLD           // halt and release A,D,RW
   );

wire [15:0] f1, f2, f3;
wire [11:0] pw1, pw2, pw3;
wire [7:0]  cr1, cr2, cr3;
wire [7:0]  ad1, ad2, ad3;
wire [7:0]  sr1, sr2, sr3;
wire [10:0] fc;
wire [7:0]  fcr1, fcr2;
wire [7:0]  osc3, env3;
wire        sync1, sync2, sync3;

wire        sclken = CP2_POSEDGE; // synthesis clock
wire [15:0] mixout;

wire signed [16:0] out1, out2, out3;

mos6581_reg_file reg_file
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP2(CP2),
   .CP2_POSEDGE(CP2_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .BCLK_NEGEDGE(BCLK_NEGEDGE),
   .A(A),
   .DB_I(DB_I),
   .DB_O(DB_O),
   .DB_OE(DB_OE),
   .RW(RW),
   .nCS(nCS),
   .HOLD(HOLD),
   .F1(f1),
   .PW1(pw1),
   .CR1(cr1),
   .AD1(ad1),
   .SR1(sr1),
   .F2(f2),
   .PW2(pw2),
   .CR2(cr2),
   .AD2(ad2),
   .SR2(sr2),
   .F3(f3),
   .PW3(pw3),
   .CR3(cr3),
   .AD3(ad3),
   .SR3(sr3),
   .FC(fc),
   .FCR1(fcr1),
   .FCR2(fcr2),
   .OSC3(osc3),
   .ENV3(env3)
   );

mos6581_voice voice1
  (
   .nRES(nRES),
   .CLK(CLK),
   .CLKEN(sclken),
   .F(f1),
   .PW(pw1),
   .CR(cr1),
   .AD(ad1),
   .SR(sr1),
   .SYNC_RM_IN(sync3),
   .SYNC_RM_OUT(sync1),
   .ENV(),
   .OSC(),
   .OUT(out1)
   );

mos6581_voice voice2
  (
   .nRES(nRES),
   .CLK(CLK),
   .CLKEN(sclken),
   .F(f2),
   .PW(pw2),
   .CR(cr2),
   .AD(ad2),
   .SR(sr2),
   .SYNC_RM_IN(sync1),
   .SYNC_RM_OUT(sync2),
   .ENV(),
   .OSC(),
   .OUT(out2)
   );

mos6581_voice voice3
  (
   .nRES(nRES),
   .CLK(CLK),
   .CLKEN(sclken),
   .F(f3),
   .PW(pw3),
   .CR(cr3),
   .AD(ad3),
   .SR(sr3),
   .SYNC_RM_IN(sync2),
   .SYNC_RM_OUT(sync3),
   .ENV(env3),
   .OSC(osc3),
   .OUT(out3)
   );

mos6581_filter filter
  (
   .CLK(CLK),
   .MCLK(MCLK),
   .SCLKEN(sclken),
   .OCLKEN(oclken),
   .FC(fc),
   .FCR1(fcr1),
   .FCR2(fcr2),
   .IN1(out1),
   .IN2(out2),
   .IN3(out3),
   .OUT(mixout)
   );

i2s_serializer ser
  (
   .MCLK(MCLK),
   .OCLKEN(oclken),
   .PDIN(mixout),
   .SCLK(SCLK),
   .LRCLK(LRCLK),
   .SDOUT(SDOUT)
   );

assign PDOUT = mixout;

endmodule
