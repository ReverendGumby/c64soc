module mos6581_voice
  (
   input                nRES,
   input                CLK,
   input                CLKEN,

   input [15:0]         F,
   input [11:0]         PW,
   input [7:0]          CR,
   input [7:0]          AD,
   input [7:0]          SR,

   input                SYNC_RM_IN,
   output               SYNC_RM_OUT,

   output [7:0]         OSC,
   output [7:0]         ENV,
   output signed [16:0] OUT
   );

wire        pa_msb;
wire [11:0] dout;

reg signed [20:0] pvout;
reg signed [31:0] svout;
reg signed [16:0] vout;

wire        gate = CR[0];
wire        sync = CR[1];
wire        ring = CR[2];
wire        test = CR[3];
wire        wtri = CR[4];
wire        wsaw = CR[5];
wire        wpul = CR[6];
wire        wnoi = CR[7];

wire [3:0]  atk = AD[7:4];
wire [3:0]  dcy = AD[3:0];
wire [3:0]  stn = SR[7:4];
wire [3:0]  rls = SR[3:0];

wire [11:0] wf_dout;

wire signed [12:0] wf_aout;     // Q1.12
wire signed [8:0]  env_aout;    // Q1.8

//////////////////////////////////////////////////////////////////////
// Digital section

// Waveform Generator
mos6581_wave wave
  (
   .nRES(nRES),
   .CLK(CLK),
   .CLKEN(CLKEN),
   .F(F),
   .PW(PW),
   .SYNC(sync),
   .RING(ring),
   .TEST(test),
   .WTRI(wtri),
   .WSAW(wsaw),
   .WPUL(wpul),
   .WNOI(wnoi),
   .SYNC_RM_IN(SYNC_RM_IN),
   .SYNC_RM_OUT(SYNC_RM_OUT),
   .OUT(wf_dout)
   );

// Envelope Generator
mos6581_voice_env env
  (
   .nRES(nRES),
   .CLK(CLK),
   .CLKEN(CLKEN),
   .ATK(atk),
   .DCY(dcy),
   .STN(stn),
   .RLS(rls),
   .GATE(gate),
   .ENV(ENV)
   );

assign OSC = wf_dout[11:4];     // upper 8 bits

//////////////////////////////////////////////////////////////////////
// Analog section

// Transform waveform and envelope generator outputs to the "analog"
// domain

// Range: [-896, 3199] = 'h[-380, C7F]
mos6581_wave_dac wave_dac
  (
   .nRES(nRES),
   .CLK(CLK),
   .CLKEN(CLKEN),
   .IN(wf_dout),
   .OUT(wf_aout)                // Q1.12
   );

// Range: [0, 255] = 'h[0, FF]
mos6581_env_dac env_dac
  (
   .nRES(nRES),
   .CLK(CLK),
   .CLKEN(CLKEN),
   .IN(ENV),
   .OUT(env_aout)               // Q1.8
   );

// (Pre-scaled) Voice output = Waveform Generator * Envelope Generator
// Range: [-228480, 815745] = 'h[-37C80, C7281]
always @(posedge CLK) if (CLKEN) begin
  pvout <= wf_aout * env_aout;
end

// The filter applies this to voice::output():
//   v1 = (voice1*f.voice_scale_s14 >> 18) + f.voice_DC;

wire signed [12:0] voice_scale_s14 = 13'sd2442;
wire signed [15:0] voice_DC = 16'sd27295;
always @(posedge CLK) if (CLKEN) begin
  // Range: [-557948160, 1992049290] = 'h[-21419D00, 76BC428A]
  svout <= pvout * voice_scale_s14;

  if (!nRES)
    vout <= voice_DC;
  else
    // Range: [25167, 34894] = 'h[624F, 884E]
    vout <= $signed(svout[31:18]) + voice_DC;
end

assign OUT = vout;

endmodule
