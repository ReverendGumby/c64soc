module test();
  
reg r;

initial begin
  r = 1'b1 & 1'bz;
  $display("%1b", r);
  $finish;
end

endmodule
