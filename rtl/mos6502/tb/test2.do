vlog ../mos6510.v +incdir+.. +define+BUS_CONFLICTS
#vlog ../mos6510.v +incdir+.. +define+BUS_CONFLICTS +define+PLA_OUTPUTS
vlog ram.sv
vlog test2.sv

vmap work

vsim test2

delete wave *
configure wave -signalnamewidth 1 -namecolwidth 150
env sim:/test2
#add wave nres
add wave cp1 rdy
add wave rw sync nirq
add wave -radix hex a d
#add wave -group sim -radix hex sync_sim rw_sim a_sim d_sim pc_sim p_sim

env sim:/test2/dut
#add wave sync_pre
#add wave sync0 resp clear_ir
add wave {t {t5 t4 t3 t2 clock2 clock1}}
add wave irqp intg

#add wave onebyte twocycle ins_done clock1_next
#add wave ins_done_pre twocycle_d2
#add wave vec0 vec1 add_pcl_db_pch brk_done
#add wave clear_ir fetch
#add wave short_circuit_idx_add
#add wave short_circuit_branch_add short_circuit_branch_add_hold

#add wave -radix hex pd
#add wave -radix hex ir

add wave -group reg -radix hex {pc {pch pcl}}
add wave -group reg -radix hex ac x y s pf

#add wave -radix hex dl dor
add wave -radix hex -group int_bus idb sb adl adh
#add wave -radix hex -group int_bus abl abh

add wave -group alu -radix hex ai bi
add wave -group alu addc
add wave -group alu sums ands eors ors srs
add wave -group alu -radix hex add
add wave -group alu -radix hex daa_adj daa_out cd_sums
add wave -group alu avo aco avr acr

#add wave branch_p branch_p_match branch_dont_take branch_offset_sign ins_done_branch
#add wave hcr daa dsa add_daa daa_d2 dsa_d2
#add wave sp_first sp_next sp_last sp_inc sp_dec sp_hold

#add wave pla_col

add wave rdy2_d2 i_pc

#configure wave -namecolwidth 210
run -all
wave zoom full
