`timescale 1us / 1ns

module fntest_tb();

reg        nRES;
reg        clk, cp1, cp1_posedge, cp2, cp2_posedge;
reg        nIRQ;
reg        nNMI;
reg        AEC;
wire [15:0] A, A_sim;
wire [7:0]  DB, DB_sim;
wire [7:0]  P, P_sim;
wire        RW, RW_sim;
reg         RDY;
wire        SYNC, SYNC_sim;
reg         SO;

mos6510 dut
  (
   .nRES(nRES),
   .CLK(clk),
   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .nIRQ(nIRQ),
   .nNMI(nNMI),
   .AEC(AEC),
   .A(A),
   .P(),
   .DB(DB),
   .RW(RW),
   .RDY(RDY),
   .SYNC(SYNC),
   .SO(SO),
   .HOLD(1'b0)
   );

mos6510_sim sim
  (
   .nRES(nRES),
   .CLK(cp1),
   .nIRQ(nIRQ),
   .nNMI(nNMI),
   .AEC(AEC),
   .A(A_sim),
   .P(P_sim),
   .DB(DB_sim),
   .RW(RW_sim),
   .RDY(RDY),
   .SYNC(SYNC_sim),
   .SO(SO)
   );

initial begin
  AEC = 1;
  clk = 1;
  cp1 = 0;
  cp1_posedge = 0;
  cp2 = 0;
  cp2_posedge = 0;
  nRES = 0;
  nIRQ = 1;
  nNMI = 1;
  RDY = 1;
  SO = 1;
end

initial forever begin :clkgen
  #0.125 clk = ~clk;
end

initial forever begin :cpgen
  @(negedge clk) cp1_posedge = 1;
  @(posedge clk) cp1 = 1;
  @(negedge clk) cp1_posedge = 0;
  @(posedge clk) cp1 = 0;
  @(negedge clk) cp2_posedge = 1;
  @(posedge clk) cp2 = 1;
  @(negedge clk) cp2_posedge = 0;
  @(posedge clk) cp2 = 0;
end

initial #9 nRES = 1;

initial #10 forever begin
  #47 ;
  @(posedge cp1) RDY = 0;
  #3 ;
  force AEC = 0;
  #2 ;
  @(posedge cp1) RDY = 1;
  release AEC;
end

initial forever begin
  @(posedge cp1) AEC = 0;
  @(posedge cp2) AEC = 1;
end

assign DB_sim = RW_sim ? DB : 8'hzz;

fntest_ram ram(.a(A[15:0]), .d(DB), .cs(1'b1), .rw(RW), .clk(cp2));

reg last_rdy;
always @(posedge cp1) last_rdy <= RDY;

reg [15:0] dut_pc;
initial #5 forever begin
  automatic reg   err = 0;
  #0.5 ;

  @(posedge cp2) #0.04 ;
  if (!last_rdy)
    continue;

  assert (SYNC_sim === SYNC)
    else begin
      $display("**** assert SYNC @%1t ns: sim %1b != dut %1b", $time, sim.SYNC, dut.SYNC);
      err = 1;
    end
  assert (RW_sim === RW)
    else begin
      $display("**** assert RW @%1t ns: sim %1b != dut %1b", $time, sim.RW, dut.RW);
      err = 1;
    end
  assert (A_sim === A)
    else begin
      $display("**** assert A @%1t ns: sim %04x != dut %04x", $time, sim.A, dut.A);
      err = 1;
    end
  assert (DB_sim === DB)
    else begin
      $display("**** assert DB @%1t ns: sim %02x != dut %02x", $time, sim.DB, dut.DB);
      err = 1;
    end
  assert (P_sim === P)
    else begin
      $display("**** assert P @%1t ns: sim %04x != dut %04x", $time, sim.P, dut.P);
      err = 1;
    end
  assert (dut.s === sim.s)
    else begin
      $display("**** assert s @%1t ns: sim %02x != dut %02x", $time, sim.s, dut.s);
      err = 1;
    end

  dut_pc = {dut.pch, dut.pcl};
  assert (dut_pc === sim.pc)
    else begin
      $display("**** assert pc @%1t ns: sim %04x != dut %04x", $time, sim.pc, dut_pc);
      err = 1;
    end
  assert (dut.ac === sim.ac)
    else begin
      $display("**** assert ac @%1t ns: sim %02x != dut %02x", $time, sim.ac, dut.ac);
      err = 1;
    end
  assert (dut.x === sim.x)
    else begin
      $display("**** assert x @%1t ns: sim %02x != dut %02x", $time, sim.x, dut.x);
      err = 1;
    end
  assert (dut.y === sim.y)
    else begin
      $display("**** assert y @%1t ns: sim %02x != dut %02x", $time, sim.y, dut.y);
      err = 1;
    end
  assert (dut.pf === sim.p)
    else begin
      $display("**** assert pf @%1t ns: sim %8b != dut %8b", $time, sim.p, dut.pf);
      err = 1;
    end

  if (err)
    $stop;
end

endmodule
