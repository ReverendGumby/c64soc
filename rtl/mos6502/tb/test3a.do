vlog ../mos6510.v +incdir+.. +define+BUS_CONFLICTS
#vlog ../mos6510.v +incdir+.. +define+BUS_CONFLICTS +define+PLA_OUTPUTS
vlog ram.sv
vlog test3a.sv

vmap work

vsim test3

delete wave *
configure wave -signalnamewidth 1 -namecolwidth 150
env sim:/test3
#add wave nres
add wave cp1 cp2 rdy aec
add wave rw sync
add wave -radix hex a d
#add wave -group sim -radix hex sync_sim rw_sim a_sim d_sim pc_sim p_sim

env sim:/test3/dut
#add wave sync_pre
#add wave sync0 resp clear_ir
add wave {t {t5 t4 t3 t2 clock2 clock1}}

#add wave onebyte twocycle ins_done clock1_next
#add wave ins_done_pre twocycle_d2
#add wave vec0 vec1 add_pcl_db_pch brk_done
#add wave clear_ir fetch
#add wave short_circuit_idx_add
#add wave short_circuit_branch_add short_circuit_branch_add_hold

#add wave -radix hex pd
#add wave -radix hex ir

add wave -group reg -radix hex {pc {pch pcl}}
add wave -group reg -radix hex ac x y s pf

#add wave -radix hex dl dor
add wave -radix hex -group int_bus idb sb adl adh
#add wave -radix hex -group int_bus abl abh

add wave -group alu -radix hex ai bi
add wave -group alu addc
add wave -group alu sums ands eors ors srs
add wave -group alu -radix hex add
add wave -group alu -radix hex daa_adj daa_out cd
add wave -group alu avo aco avr acr

add wave -group DPcontrol zero_adh zero_adl adl_pcl adh_pch s_adl add_adl add_sb sb_adh adl_abl adh_abh pcl_adl pch_adh i_pc sums db_add ndb_add adl_add sb_add sb_s one_addc zero_add dl_adl dl_adh dl_db pcl_db pch_db p_db ac_db sb_db x_sb y_sb s_sb sb_x sb_y daa dsa ac_sb sb_ac write_dor db0_c ir5_c acr_c db1_z dbz_z db2_i ir5_i db3_d ir5_d db6_v avr_v one_v zero_v db7_n

#add wave branch_p branch_p_match branch_dont_take branch_offset_sign ins_done_branch
#add wave hcr daa dsa add_daa daa_d2 dsa_d2
#add wave sp_first sp_next sp_last sp_inc sp_dec sp_hold

#add wave pla_col

add wave rdy2_d2 i_pc

#configure wave -namecolwidth 210
run -all
wave zoom full
