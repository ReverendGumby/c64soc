vmap work

proc compile {} {
    vlog ../mos6510.v +incdir+.. +define+BUS_CONFLICTS
#    vlog ../mos6510.v +incdir+.. +define+BUS_CONFLICTS +define+PLA_OUTPUTS
	vlog mos6510_sim.sv
	vlog fntest_ram.sv
	vlog fntest_tb.sv
}

proc recomp {} {
	compile
	restart -f
	run 50
}

compile
vsim fntest_tb

delete wave *
configure wave -signalnamewidth 1 -namecolwidth 150
env sim:/fntest_tb
#add wave nRES
add wave cp1 cp2
add wave RDY SYNC RW
add wave -radix hex A DB

env sim:/fntest_tb/dut
#add wave sync0 resp clear_ir
add wave {t {t5 t4 t3 t2 clock2 clock1}}

#add wave onebyte twocycle fntest_done clock1_next
#add wave fntest_done_pre twocycle_d2
#add wave vec0 vec1 add_pcl_db_pch brk_done
#add wave clear_ir fetch

#add wave -radix hex pd
add wave -radix hex ir
add wave -radix hex {pc {pch pcl}}
add wave -radix hex ac x y s pf

#add wave -radix hex dl dor
add wave -radix hex -group int_bus idb sb adl adh

add wave -group foo zp_t2 zp_t3 zp_ab addr_ab pc_ab pc_ab_dly i_pc cl_i_pc

add wave -group alu -radix hex ai bi
add wave -group alu addc
add wave -group alu sums ands eors ors srs
add wave -group alu -radix hex add
add wave -group alu -radix hex daa_adj daa_out cd_sums
add wave -group alu avo aco avr acr

#add wave branch_p branch_p_match branch_do_take branch_dont_take branch_taken branch_hold branch_offset_sign branch_overflow branch_done_sync ins_done_branch
#add wave hcr daa dsa add_daa daa_d2 dsa_d2
#add wave sp_first sp_next sp_last sp_inc sp_dec sp_hold

add wave pla_col

add wave -group DPcontrol zero_adh zero_adl adl_pcl adh_pch s_adl add_adl add_sb sb_adh adl_abl adh_abh pcl_adl pch_adh i_pc sums ands eors ors srs db_add ndb_add adl_add sb_add sb_s one_addc zero_add dl_adl dl_adh dl_db pcl_db pch_db p_db ac_db sb_db x_sb y_sb s_sb sb_x sb_y daa dsa ac_sb sb_ac write_dor db0_c ir5_c acr_c db1_z dbz_z db2_i ir5_i db3_d ir5_d db6_v avr_v one_v zero_v db7_n

env sim:/fntest_tb/sim
add wave -group sim RW SYNC
add wave -group sim -radix hex A DB P
add wave -group sim -radix hex pc
add wave -group sim -radix hex ac x y s
add wave -group sim p

#configure wave -namecolwidth 210
set RunLength "10 us"
run
wave zoom full
