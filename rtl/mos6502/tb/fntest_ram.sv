`timescale 1us / 1ns

module fntest_ram
  (
   input        CLK,
   input        CS,
   input        RW,
   input [15:0] A,
   inout [7:0]  D
   );

reg [7:0] ram [0:16'hffff] =
 '{
`include "../../../asm/fntest/ram_hex.sv"
   };


assign D = CS && RW ? ram[A] : 8'hzz;

always @(posedge CLK) #0.1 begin
  if (CS && !RW)
    ram[A] <= D;
end

endmodule
