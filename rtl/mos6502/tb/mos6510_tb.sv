module mos6510_tb();

reg        nRES;
reg        CLK;
reg        nIRQ;
reg        AEC;
wire [15:0] A;
wire [7:0] DB;
wire        RW;
reg         RDY;
wire        SYNC;
reg         SO;

mos6510 mos6510
  (
   .nRES(nRES),
   .CLK(CLK),
   .nIRQ(nIRQ),
   .AEC(AEC),
   .A(A),
   .P(),
   .DB(DB),
   .RW(RW),
   .RDY(RDY),
   .SYNC(SYNC),
   .SO(SO)
   );

initial begin
  AEC = 1;
  CLK = 1;
  nRES = 0;
  nIRQ = 1;
  RDY = 1;
  SO = 1;
end

always #1 begin :clkgen
  CLK = !CLK;
end

initial #9 nRES = 1;

wire rom_cs = RW && A[15:8] == 8'hFF;
rom rom(.A(A[7:0]), .D(DB), .CS(rom_cs));

wire ram_cs = !rom_cs;
ram ram(.A(A[15:0]), .D(DB), .CS(ram_cs), .RW(RW));

endmodule
