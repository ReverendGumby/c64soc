module mos6510_sim
  (
   input             nRES,                 // reset
   input             CLK,                  // clock
   input             nIRQ,                 // interrupt request
   input             nNMI,                 // non-maskable interrupt
   input             AEC,                  // address enable control
   output reg [15:0] A,                    // address bus
   inout [7:0]       P,                    // peripheral interface bus
   inout [7:0]       DB,                   // data bus
   output reg        RW,                   // read / not write
   input             RDY,                  // ready
   output reg        SYNC,                 // instruction sync
   input             SO                    // set overflow
   );

`define BRK      8'h00
`define ORA_INDX 8'h01
`define ORA_ZP   8'h05
`define ASL_ZP   8'h06
`define PHP      8'h08
`define ORA_IMM  8'h09
`define ASL_A    8'h0a
`define ORA_ABS  8'h0d
`define ASL_ABS  8'h0e
`define BPL      8'h10
`define ORA_INDY 8'h11
`define ORA_ZPX  8'h15
`define ASL_ZPX  8'h16
`define CLC      8'h18
`define ORA_ABSY 8'h19
`define ORA_ABSX 8'h1d
`define ASL_ABSX 8'h1e
`define JSR      8'h20
`define AND_INDX 8'h21
`define BIT_ZP   8'h24
`define AND_ZP   8'h25
`define ROL_ZP   8'h26
`define PLP      8'h28
`define AND_IMM  8'h29
`define ROL_A    8'h2a
`define BIT_ABS  8'h2c
`define AND_ABS  8'h2d
`define ROL_ABS  8'h2e
`define BMI      8'h30
`define AND_INDY 8'h31
`define AND_ZPX  8'h35
`define ROL_ZPX  8'h36
`define SEC      8'h38
`define AND_ABSY 8'h39
`define AND_ABSX 8'h3d
`define ROL_ABSX 8'h3e
`define RTI      8'h40
`define EOR_INDX 8'h41
`define EOR_ZP   8'h45
`define LSR_ZP   8'h46
`define PHA      8'h48
`define EOR_IMM  8'h49
`define LSR_A    8'h4a
`define JMP_ABS  8'h4c
`define EOR_ABS  8'h4d
`define LSR_ABS  8'h4e
`define BVC      8'h50
`define EOR_INDY 8'h51
`define EOR_ZPX  8'h55
`define LSR_ZPX  8'h56
`define CLI      8'h58
`define EOR_ABSY 8'h59
`define EOR_ABSX 8'h5d
`define LSR_ABSX 8'h5e
`define RTS      8'h60
`define ADC_INDX 8'h61
`define ADC_ZP   8'h65
`define ROR_ZP   8'h66
`define PLA      8'h68
`define ADC_IMM  8'h69
`define ROR_A    8'h6a
`define JMP_IND  8'h6c
`define ADC_ABS  8'h6d
`define ROR_ABS  8'h6e
`define BVS      8'h70
`define ADC_INDY 8'h71
`define ADC_ZPX  8'h75
`define ROR_ZPX  8'h76
`define SEI      8'h78
`define ADC_ABSY 8'h79
`define ADC_ABSX 8'h7d
`define ROR_ABSX 8'h7e
`define STA_INDX 8'h81
`define STY_ZP   8'h84
`define STA_ZP   8'h85
`define STX_ZP   8'h86
`define DEY      8'h88
`define TXA      8'h8a
`define STY_ABS  8'h8c
`define STA_ABS  8'h8d
`define STX_ABS  8'h8e
`define BCC      8'h90
`define STA_INDY 8'h91
`define STY_ZPX  8'h94
`define STA_ZPX  8'h95
`define STX_ZPY  8'h96
`define TYA      8'h98
`define STA_ABSY 8'h99
`define TXS      8'h9a
`define STA_ABSX 8'h9d
`define LDY_IMM  8'ha0
`define LDA_INDX 8'ha1
`define LDX_IMM  8'ha2
`define LDY_ZP   8'ha4
`define LDA_ZP   8'ha5
`define LDX_ZP   8'ha6
`define TAY      8'ha8
`define LDA_IMM  8'ha9
`define TAX      8'haa
`define LDY_ABS  8'hac
`define LDA_ABS  8'had
`define LDX_ABS  8'hae
`define BCS      8'hb0
`define LDA_INDY 8'hb1
`define LDY_ZPX  8'hb4
`define LDA_ZPX  8'hb5
`define LDX_ZPY  8'hb6
`define CLV      8'hb8
`define LDA_ABSY 8'hb9
`define TSX      8'hba
`define LDY_ABSX 8'hbc
`define LDA_ABSX 8'hbd
`define LDX_ABSY 8'hbe
`define CPY_IMM  8'hc0
`define CMP_INDX 8'hc1
`define CPY_ZP   8'hc4
`define CMP_ZP   8'hc5
`define DEC_ZP   8'hc6
`define INY      8'hc8
`define CMP_IMM  8'hc9
`define DEX      8'hca
`define CPY_ABS  8'hcc
`define CMP_ABS  8'hcd
`define DEC_ABS  8'hce
`define BNE      8'hd0
`define CMP_INDY 8'hd1
`define CMP_ZPX  8'hd5
`define DEC_ZPX  8'hd6
`define CLD      8'hd8
`define CMP_ABSY 8'hd9
`define CMP_ABSX 8'hdd
`define DEC_ABSX 8'hde
`define CPX_IMM  8'he0
`define SBC_INDX 8'he1
`define CPX_ZP   8'he4
`define SBC_ZP   8'he5
`define INC_ZP   8'he6
`define INX      8'he8
`define SBC_IMM  8'he9
`define NOP      8'hea
`define CPX_ABS  8'hec
`define SBC_ABS  8'hed
`define INC_ABS  8'hee
`define BEQ      8'hf0
`define SBC_INDY 8'hf1
`define SBC_ZPX  8'hf5
`define INC_ZPX  8'hf6
`define SED      8'hf8
`define SBC_ABSY 8'hf9
`define SBC_ABSX 8'hfd
`define INC_ABSX 8'hfe

`define OP_XXX 8'hxx
`define OP_ADC  8'h61
`define OP_AND  8'h21
`define OP_ASL  8'h06
`define OP_BIT  8'h24
`define OP_BRA  8'h10
`define OP_BRK  8'h00
`define OP_CLC  8'h18
`define OP_CLD  8'hd8
`define OP_CLI  8'h58
`define OP_CLV  8'hb8
`define OP_CMP  8'hc1
`define OP_CPX  8'he0
`define OP_CPY  8'hc0
`define OP_DEC  8'hc6
`define OP_DEX  8'hca
`define OP_DEY  8'h88
`define OP_EOR  8'h41
`define OP_INC  8'he6
`define OP_INX  8'he8
`define OP_INY  8'hc8
`define OP_JMP  8'h4c
`define OP_JSR  8'h20
`define OP_LDA  8'ha1
`define OP_LDX  8'ha2
`define OP_LDY  8'ha0
`define OP_LSR  8'h46
`define OP_NOP  8'hea
`define OP_ORA  8'h01
`define OP_PHA  8'h48
`define OP_PHP  8'h08
`define OP_PLA  8'h68
`define OP_PLP  8'h28
`define OP_ROL  8'h26
`define OP_ROR  8'h66
`define OP_RTI  8'h40
`define OP_RTS  8'h60
`define OP_SBC  8'he1
`define OP_SEC  8'h38
`define OP_SED  8'hf8
`define OP_SEI  8'h78
`define OP_STA  8'h81
`define OP_STX  8'h86
`define OP_STY  8'h84
`define OP_TAX  8'haa
`define OP_TAY  8'ha8
`define OP_TSX  8'hba
`define OP_TXA  8'h8a
`define OP_TXS  8'h9a
`define OP_TYA  8'h98

reg [7:0] opcode_op [0:255] =
 '{
   `OP_BRK, `OP_ORA, `OP_XXX, `OP_XXX, `OP_XXX, `OP_ORA, `OP_ASL, `OP_XXX,
   `OP_PHP, `OP_ORA, `OP_ASL, `OP_XXX, `OP_XXX, `OP_ORA, `OP_ASL, `OP_XXX,
   `OP_BRA, `OP_ORA, `OP_XXX, `OP_XXX, `OP_XXX, `OP_ORA, `OP_ASL, `OP_XXX,
   `OP_CLC, `OP_ORA, `OP_XXX, `OP_XXX, `OP_XXX, `OP_ORA, `OP_ASL, `OP_XXX,
   `OP_JSR, `OP_AND, `OP_XXX, `OP_XXX, `OP_BIT, `OP_AND, `OP_ROL, `OP_XXX,
   `OP_PLP, `OP_AND, `OP_ROL, `OP_XXX, `OP_BIT, `OP_AND, `OP_ROL, `OP_XXX,
   `OP_BRA, `OP_AND, `OP_XXX, `OP_XXX, `OP_XXX, `OP_AND, `OP_ROL, `OP_XXX,
   `OP_SEC, `OP_AND, `OP_XXX, `OP_XXX, `OP_XXX, `OP_AND, `OP_ROL, `OP_XXX,
   `OP_RTI, `OP_EOR, `OP_XXX, `OP_XXX, `OP_XXX, `OP_EOR, `OP_LSR, `OP_XXX,
   `OP_PHA, `OP_EOR, `OP_LSR, `OP_XXX, `OP_JMP, `OP_EOR, `OP_LSR, `OP_XXX,
   `OP_BRA, `OP_EOR, `OP_XXX, `OP_XXX, `OP_XXX, `OP_EOR, `OP_LSR, `OP_XXX,
   `OP_CLI, `OP_EOR, `OP_XXX, `OP_XXX, `OP_XXX, `OP_EOR, `OP_LSR, `OP_XXX,
   `OP_RTS, `OP_ADC, `OP_XXX, `OP_XXX, `OP_XXX, `OP_ADC, `OP_ROR, `OP_XXX,
   `OP_PLA, `OP_ADC, `OP_ROR, `OP_XXX, `OP_JMP, `OP_ADC, `OP_ROR, `OP_XXX,
   `OP_BRA, `OP_ADC, `OP_XXX, `OP_XXX, `OP_XXX, `OP_ADC, `OP_ROR, `OP_XXX,
   `OP_SEI, `OP_ADC, `OP_XXX, `OP_XXX, `OP_XXX, `OP_ADC, `OP_ROR, `OP_XXX,
   `OP_XXX, `OP_STA, `OP_XXX, `OP_XXX, `OP_STY, `OP_STA, `OP_STX, `OP_XXX,
   `OP_DEY, `OP_XXX, `OP_TXA, `OP_XXX, `OP_STY, `OP_STA, `OP_STX, `OP_XXX,
   `OP_BRA, `OP_STA, `OP_XXX, `OP_XXX, `OP_STY, `OP_STA, `OP_STX, `OP_XXX,
   `OP_TYA, `OP_STA, `OP_TXS, `OP_XXX, `OP_XXX, `OP_STA, `OP_XXX, `OP_XXX,
   `OP_LDY, `OP_LDA, `OP_LDX, `OP_XXX, `OP_LDY, `OP_LDA, `OP_LDX, `OP_XXX,
   `OP_TAY, `OP_LDA, `OP_TAX, `OP_XXX, `OP_LDY, `OP_LDA, `OP_LDX, `OP_XXX,
   `OP_BRA, `OP_LDA, `OP_XXX, `OP_XXX, `OP_LDY, `OP_LDA, `OP_LDX, `OP_XXX,
   `OP_CLV, `OP_LDA, `OP_TSX, `OP_XXX, `OP_LDY, `OP_LDA, `OP_LDX, `OP_XXX,
   `OP_CPY, `OP_CMP, `OP_XXX, `OP_XXX, `OP_CPY, `OP_CMP, `OP_DEC, `OP_XXX,
   `OP_INY, `OP_CMP, `OP_DEX, `OP_XXX, `OP_CPY, `OP_CMP, `OP_DEC, `OP_XXX,
   `OP_BRA, `OP_CMP, `OP_XXX, `OP_XXX, `OP_XXX, `OP_CMP, `OP_DEC, `OP_XXX,
   `OP_CLD, `OP_CMP, `OP_XXX, `OP_XXX, `OP_XXX, `OP_CMP, `OP_DEC, `OP_XXX,
   `OP_CPX, `OP_SBC, `OP_XXX, `OP_XXX, `OP_CPX, `OP_SBC, `OP_INC, `OP_XXX,
   `OP_INX, `OP_SBC, `OP_NOP, `OP_XXX, `OP_CPX, `OP_SBC, `OP_INC, `OP_XXX,
   `OP_BRA, `OP_SBC, `OP_XXX, `OP_XXX, `OP_XXX, `OP_SBC, `OP_INC, `OP_XXX,
   `OP_SED, `OP_SBC, `OP_XXX, `OP_XXX, `OP_XXX, `OP_SBC, `OP_INC, `OP_XXX
   };

`define AM_XXX   5'dx
`define AM_IMPL  5'd0
`define AM_A     5'd1
`define AM_IMM   5'd2
`define AM_ABS   5'd3
`define AM_ABSX  5'd4
`define AM_ABSY  5'd5
`define AM_ZP    5'd6
`define AM_ZPX   5'd7
`define AM_ZPY   5'd8
`define AM_REL   5'd9
`define AM_INDX  5'd10
`define AM_INDY  5'd11
`define AM_IND16 5'd12

reg [4:0] opcode_am [0:255] =
 '{
   `AM_IMPL, `AM_INDX, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ZP, `AM_ZP, `AM_XXX,
   `AM_IMPL, `AM_IMM, `AM_A, `AM_XXX, `AM_XXX, `AM_ABS, `AM_ABS, `AM_XXX,
   `AM_REL, `AM_INDY, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ZPX, `AM_ZPX, `AM_XXX,
   `AM_IMPL, `AM_ABSY, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ABSX, `AM_ABSX, `AM_XXX,
   `AM_IMM, `AM_INDX, `AM_XXX, `AM_XXX, `AM_ZP, `AM_ZP, `AM_ZP, `AM_XXX,
   `AM_IMPL, `AM_IMM, `AM_A, `AM_XXX, `AM_ABS, `AM_ABS, `AM_ABS, `AM_XXX,
   `AM_REL, `AM_INDY, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ZPX, `AM_ZPX, `AM_XXX,
   `AM_IMPL, `AM_ABSY, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ABSX, `AM_ABSX, `AM_XXX,
   `AM_IMPL, `AM_INDX, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ZP, `AM_ZP, `AM_XXX,
   `AM_IMPL, `AM_IMM, `AM_A, `AM_XXX, `AM_ABS, `AM_ABS, `AM_ABS, `AM_XXX,
   `AM_REL, `AM_INDY, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ZPX, `AM_ZPX, `AM_XXX,
   `AM_IMPL, `AM_ABSY, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ABSX, `AM_ABSX, `AM_XXX,
   `AM_IMPL, `AM_INDX, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ZP, `AM_ZP, `AM_XXX,
   `AM_IMPL, `AM_IMM, `AM_A, `AM_XXX, `AM_IND16, `AM_ABS, `AM_ABS, `AM_XXX,
   `AM_REL, `AM_INDY, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ZPX, `AM_ZPX, `AM_XXX,
   `AM_IMPL, `AM_ABSY, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ABSX, `AM_ABSX, `AM_XXX,
   `AM_XXX, `AM_INDX, `AM_XXX, `AM_XXX, `AM_ZP, `AM_ZP, `AM_ZP, `AM_XXX,
   `AM_IMPL, `AM_XXX, `AM_IMPL, `AM_XXX, `AM_ABS, `AM_ABS, `AM_ABS, `AM_XXX,
   `AM_REL, `AM_INDY, `AM_XXX, `AM_XXX, `AM_ZPX, `AM_ZPX, `AM_ZPY, `AM_XXX,
   `AM_IMPL, `AM_ABSY, `AM_IMPL, `AM_XXX, `AM_XXX, `AM_ABSX, `AM_XXX, `AM_XXX,
   `AM_IMM, `AM_INDX, `AM_IMM, `AM_XXX, `AM_ZP, `AM_ZP, `AM_ZP, `AM_XXX,
   `AM_IMPL, `AM_IMM, `AM_IMPL, `AM_XXX, `AM_ABS, `AM_ABS, `AM_ABS, `AM_XXX,
   `AM_REL, `AM_INDY, `AM_XXX, `AM_XXX, `AM_ZPX, `AM_ZPX, `AM_ZPY, `AM_XXX,
   `AM_IMPL, `AM_ABSY, `AM_IMPL, `AM_XXX, `AM_ABSX, `AM_ABSX, `AM_ABSY, `AM_XXX,
   `AM_IMM, `AM_INDX, `AM_XXX, `AM_XXX, `AM_ZP, `AM_ZP, `AM_ZP, `AM_XXX,
   `AM_IMPL, `AM_IMM, `AM_IMPL, `AM_XXX, `AM_ABS, `AM_ABS, `AM_ABS, `AM_XXX,
   `AM_REL, `AM_INDY, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ZPX, `AM_ZPX, `AM_XXX,
   `AM_IMPL, `AM_ABSY, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ABSX, `AM_ABSX, `AM_XXX,
   `AM_IMM, `AM_INDX, `AM_XXX, `AM_XXX, `AM_ZP, `AM_ZP, `AM_ZP, `AM_XXX,
   `AM_IMPL, `AM_IMM, `AM_IMPL, `AM_XXX, `AM_ABS, `AM_ABS, `AM_ABS, `AM_XXX,
   `AM_REL, `AM_INDY, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ZPX, `AM_ZPX, `AM_XXX,
   `AM_IMPL, `AM_ABSY, `AM_XXX, `AM_XXX, `AM_XXX, `AM_ABSX, `AM_ABSX, `AM_XXX
   };

reg [15:0]       pc;
reg [7:0]        ir, p, s, ac, x, y;
reg [7:0]        dor, m;
reg [8:0]        tmp;
reg [15:0]       ea;
reg              eac, op_store, hc;
reg              resp, resg, nmig, irqp, intg;
reg [7:0]        op;
reg [4:0]        am;
reg [15:0]       ab;
reg              rw_int;

`define p_c p[0]
`define p_z p[1]
`define p_i p[2]
`define p_d p[3]
`define p_b p[4]
`define p_v p[6]
`define p_n p[7]
initial p[5] = 1'b1;                            // unused
assign `p_b = !(resg || intg);

initial nmig = 0;
initial irqp = 0;
initial resp = 1'b1;
initial intg = 0;

assign RW = AEC ? (rw_int || resg) : 1'bz;
assign A = AEC ? ab : 16'hzzzz;

reg rdy0;
always @* begin
  if (CLK)
    rdy0 <= RDY || !rw_int;
end
wire clk = CLK || !rdy0;        // clock gated by RDY

wire p_sel = ab == 16'h0000 || ab == 16'h0001;

reg  db_oe;
always @* begin
  db_oe <= !rw_int && !CLK && !p_sel;
end
assign DB = db_oe ? dor : 8'hzz;

wire [7:0] ipdb;
wire [7:0] db = p_sel ? ipdb : DB;

always @(posedge CLK) begin
  resp <= !nRES;
  irqp <= !nIRQ;
  nmig <= 1'b0;
end

initial forever begin
  if (resp) begin
    @(posedge CLK) ;
    SYNC = 0;
    rw_int = 1'b1;
    resg = 1'b1;
  end
  else
    do_ins();
end

event do_ins_dly;

task do_ins();
  SYNC = 1;
  ab = pc;
  rw_int = 1'b1;
  if (nmig || (irqp && !`p_i)) begin
    intg = 1'b1;
  end
  
  @(posedge clk) ;
  SYNC = 0;
  if (resg || intg)
    ir = 8'h00;
  else
    ir = db;
  print_ins();
  if (!intg)
    pc = pc + 1;
  ab = pc;

  @(posedge clk) ;

  op = opcode_op[ir];
  am = opcode_am[ir];
  //$display("sim: ir=%02x op=%02x am=%d", ir, op, am);
  
  op_store = op == `OP_DEC || op == `OP_INC || op == `OP_STA ||
             op == `OP_STX || op == `OP_STY || op == `OP_ASL ||
             op == `OP_LSR || op == `OP_ROL || op == `OP_ROR;

  case (am)
    `AM_IMPL: begin
      case (op)
        `OP_BRK: begin
          if (!intg)
            pc = pc + 1;
          ab[15:8] = 8'h01;
          ab[7:0] = s;
          rw_int = 0;
          dor = pc[15:8];

          @(posedge clk) ;
          ab[7:0] = s - 1;
          dor = pc[7:0];
          
          @(posedge clk) ;
          ab[7:0] = s - 2;
          dor = p;
          
          @(posedge clk) ;
          rw_int = 1'b1;
          s = s - 3;
          ab[15:8] = 8'hff;
          ab[7:0] = resg ? 8'hfc : (nmig ? 8'hfa : 8'hfe);

          @(negedge clk) ;
          intg = 1'b0;

          @(posedge clk) ;
          if (nmig)
            nmig = 0;

          ea[7:0] = db;
          ab[0] = 1'b1;
          if (resg)
            resg = 0;
          `p_i = 1'b1;

          @(posedge clk) ;
          ea[15:8] = db;
          pc = ea;

        end
        `OP_CLC: begin
          `p_c = 0;
        end
        `OP_CLD: begin
          `p_d = 0;
        end
        `OP_CLI: begin
          `p_i = 0;
        end
        `OP_CLV: begin
          `p_v = 0;
        end
        `OP_DEX: begin
          -> do_ins_dly;
        end
        `OP_DEY: begin
          -> do_ins_dly;
        end
        `OP_INX: begin
          -> do_ins_dly;
        end
        `OP_INY: begin
          -> do_ins_dly;
        end
        `OP_NOP: begin
        end
        `OP_PHA, `OP_PHP: begin
          ab[15:8] = 8'h01;
          ab[7:0] = s;
          rw_int = 0;
          dor = ir == `PHA ? ac : p;

          @(posedge clk) ;
          rw_int = 1'b1;
          s = s - 1;
        end
        `OP_PLA, `OP_PLP: begin
          ab[15:8] = 8'h01;
          ab[7:0] = s;

          @(posedge clk) ;
          ab[7:0] = s + 1;
          s = s + 1;

          @(posedge clk) ;
          if (ir == `PLP)
            {p[7:6], p[3:0]} = {db[7:6], db[3:0]};
          else begin
            ac = db;
            `p_n = ac[7];
            `p_z = ac == 0;
          end
        end
        `OP_RTI: begin
          pc = pc + 1;
          ab[15:8] = 8'h01;
          ab[7:0] = s;

          @(posedge clk) ;
          ab[7:0] = s + 1;

          @(posedge clk) ;
          {p[7:6], p[3:0]} = {db[7:6], db[3:0]};
          ab[7:0] = s + 2;

          @(posedge clk) ;
          ea[7:0] = db;
          ab[7:0] = s + 3;
          s = s + 3;

          @(posedge clk) ;
          ea[15:8] = db;
          pc = ea;
          ab = pc;
        end
        `OP_RTS: begin
          // Cycle 3
          pc = pc + 1;
          ab[15:8] = 8'h01;
          ab[7:0] = s;

          // Cycle 4
          @(posedge clk) ;
          ab[7:0] = s + 1;

          // Cycle 5
          @(posedge clk) ;
          ea[7:0] = db;
          ab[7:0] = s + 2;
          s = s + 2;

          // Cycle 6
          @(posedge clk) ;
          ea[15:8] = db;
          pc = ea;
          ab = pc;

          // Cycle 7
          @(posedge clk) ;
          pc = pc + 1;
        end
        `OP_SEC: begin
          `p_c = 1'b1;
        end
        `OP_SED: begin
          `p_d = 1'b1;
        end
        `OP_SEI: begin
          `p_i = 1'b1;
        end
        `OP_TAX: begin
          x = ac;
          `p_z = x == 0;
          `p_n = x[7];
        end
        `OP_TAY: begin
          y = ac;
          `p_z = y == 0;
          `p_n = y[7];
        end
        `OP_TSX: begin
          x = s;
          `p_z = x == 0;
          `p_n = x[7];
        end
        `OP_TXA: begin
          ac = x;
          `p_z = ac == 0;
          `p_n = ac[7];          
        end
        `OP_TXS: begin
          s = x;
        end
        `OP_TYA: begin
          ac = y;
          `p_z = ac == 0;
          `p_n = ac[7];
        end
        `OP_XXX: $stop;
      endcase
    end
    `AM_IMM: begin
      pc = pc + 1;
    end
    `AM_ABS: begin
      ea[7:0] = db;
      pc = pc + 1;
      ab = pc;

      @(posedge clk) ;
      ea[15:8] = db;
      pc = pc + 1;
      ab = ea;
    end
    `AM_ABSX, `AM_ABSY: begin
      ea[7:0] = db;
      pc = pc + 1;
      ab = pc;

      @(posedge clk) ;
      ea[15:8] = db;
      pc = pc + 1;
      {eac, ea[7:0]} = ea[7:0] + (am == `AM_ABSX ? x : y);
      ab = ea;

      if (eac || op_store) begin
        @(posedge CLK) ;        // not blocked by RDY
        if (eac)
          ea = ea + 16'h0100;
        ab = ea;
        if (!RDY)
          @(posedge clk) ;
      end
    end
    `AM_ZP: begin
      ea[15:8] = 0;
      ea[7:0] = db;
      pc = pc + 1;
      ab = ea;
    end
    `AM_ZPX, `AM_ZPY: begin
      ea[15:8] = 0;
      ea[7:0] = db;
      pc = pc + 1;
      ab = ea;

      @(posedge clk) ;
      ea[7:0] = ea[7:0] + (am == `AM_ZPX ? x : y);
      ab = ea;
    end
    `AM_INDX: begin
      ab[15:8] = 0;
      ab[7:0] = db;
      pc = pc + 1;

      @(posedge clk) ;
      ea[7:0] = ab[7:0] + x;
      ab[7:0] = ea[7:0];
      
      @(posedge clk) ;
      ea[7:0] = db;
      ab[7:0] = ab[7:0] + 1;
      
      @(posedge clk) ;
      ea[15:8] = db;
      ab = ea;
    end
    `AM_INDY: begin
      ab[15:8] = 0;
      ab[7:0] = db;
      pc = pc + 1;

      @(posedge clk) ;
      ea[7:0] = db;
      ab[7:0] = ab[7:0] + 1;
      
      @(posedge clk) ;
      ea[15:8] = db;
      {eac, ea[7:0]} = ea[7:0] + y;
      ab = ea;

      if (eac || op_store) begin
        @(posedge CLK) ;        // not blocked by RDY
        if (eac)
          ea = ea + 16'h0100;
        ab = ea;
        if (!RDY)
          @(posedge clk) ;
      end
    end
    `AM_REL: begin
      assert (op == `OP_BRA);

      ea[7:0] = db;
      ea[15:8] = {8{db[7]}};
      pc = pc + 1;
      ab = pc;

      if (ir === `BPL && !`p_n ||
          ir === `BMI &&  `p_n ||
          ir === `BVC && !`p_v ||
          ir === `BVS &&  `p_v ||
          ir === `BCC && !`p_c ||
          ir === `BCS &&  `p_c ||
          ir === `BNE && !`p_z ||
          ir === `BEQ &&  `p_z) begin
        @(posedge clk) ;
        {eac, pc[7:0]} = ea[7:0] + pc[7:0];
        ab = pc;

        if (ea[15:8] + eac != 8'h00) begin
          @(posedge clk) ;
          pc[15:8] = pc[15:8] + ea[15:8] + eac;
        end
      end
    end
    `AM_IND16: begin
      assert (ir == `JMP_IND);
      ea[7:0] = db;
      pc = pc + 1;
      ab = pc;

      @(posedge clk) ;
      ea[15:8] = db;
      pc = pc + 1;
      ab = ea;

      @(posedge clk) ;
      ea[7:0] = db;
      ab[7:0] = ab[7:0] + 1;
      
      @(posedge clk) ;
      ea[15:8] = db;
      pc = ea;
    end
    `AM_XXX: $stop;
    default: ;
  endcase

  case (op)
    `OP_ADC, `OP_AND, `OP_ASL, `OP_BIT, `OP_CMP, `OP_CPX, `OP_CPY, `OP_DEC, 
    `OP_EOR, `OP_INC, `OP_LDA, `OP_LDX, `OP_LDY, `OP_LSR, `OP_ORA, `OP_ROL, 
    `OP_ROR, `OP_SBC: begin
      if (am == `AM_IMM) begin
        m = db;
      end
      else if (am == `AM_A)
        m = ac;
      else begin
        @(posedge clk);
        m = db;
      end
      dor = m;
    end
  endcase

  case (op)
    `OP_ADC, `OP_AND, `OP_CMP, `OP_CPX, `OP_CPY, `OP_EOR, `OP_ORA, `OP_SBC: begin
      -> do_ins_dly;
    end
    `OP_LSR, `OP_ROR: begin
      tmp[8] = `p_c;
      `p_c = m[0];
      -> do_ins_dly;
      if (am != `AM_A) begin
        rw_int = 0;
        @(posedge clk) ;
      end
    end
    `OP_ASL, `OP_ROL: begin
      -> do_ins_dly;
      if (am != `AM_A) begin
        rw_int = 0;
        @(posedge clk) ;
      end
    end
    `OP_BIT: begin
      `p_n = m[7];
      `p_v = m[6];
      // Extraneous flag change: corrected in next cycle
      `p_z = m == 0;
      -> do_ins_dly;
    end
    `OP_DEC, `OP_INC: begin
      -> do_ins_dly;
      rw_int = 0;
      @(posedge clk) ;
    end
    `OP_JMP: begin
      pc = ea;
    end
    `OP_JSR: begin
      assert (ir === `JSR);
      assert (am == `AM_IMM);

      // Cycle 3 (T2)
      ea[7:0] = db;
      ab[15:8] = 8'h01;
      ab[7:0] = s;
      s = ea[7:0];

      // Cycle 4 (T3)
      @(posedge clk) ;
      dor = pc[15:8];
      rw_int = 0;

      // Cycle 5 (T4)
      @(posedge clk) ;
      dor = pc[7:0];
      ab[7:0] = ab[7:0] - 1;

      // Cycle 6 (T5)
      @(posedge clk) ;
      rw_int = 1;
      m = ab[7:0] - 1;
      ab = pc;

      // Cycle 7 (T0)
      @(posedge clk) ;
      ea[15:8] = db;
      s = m; // original s - 2
      pc = ea;
    end

    `OP_LDA: begin
      ac = m;
      `p_n = ac[7];
      `p_z = ac == 0;
    end
    `OP_LDX: begin
      x = m;
      `p_n = x[7];
      `p_z = x == 0;
    end
    `OP_LDY: begin
      y = m;
      `p_n = y[7];
      `p_z = y == 0;
    end
    `OP_STA, `OP_STX, `OP_STY: begin
      m = op == `OP_STA ? ac : (op == `OP_STX ? x : y);
      dor = m;
      rw_int = 0;
    end
    `OP_XXX: $stop;
    default: ;
  endcase

  if (!rw_int) begin
    @(posedge clk) ;
  end    
endtask

// These instrutions execute during the next instruction fetch cycle.
always @do_ins_dly begin
  @(posedge clk) ;

  case (op)
    `OP_ADC: begin
      tmp = ac + m + `p_c;
      hc = ac[3:0] + m[3:0] + `p_c > 5'hf;
      if (`p_d && tmp[3:0] >= 4'd10) begin
        tmp = tmp + 8'h10;
        hc = 1'b1;
      end
      if (`p_d && tmp[7:4] >= 4'd10)
        tmp = tmp + 9'h100;
      `p_z = tmp[7:0] == 0;
      `p_n = tmp[7];
      `p_v = ac[7] == m[7] && tmp[7] != ac[7];
      `p_c = tmp[8];
      if (`p_d && hc)
        tmp[3:0] = tmp[3:0] - 4'd10;
      if (`p_d && `p_c)
        tmp[7:4] = tmp[7:4] - 4'd10;
      ac = tmp;
    end
    `OP_AND: begin
      ac = ac & m;
      `p_z = ac == 0;
      `p_n = ac[7];
    end
    `OP_ASL: begin
      tmp = {m[7:0], 1'b0};
      `p_n = tmp[7];
      `p_z = tmp[7:0] == 0;
      `p_c = tmp[8];
      m = tmp[7:0];
      if (am == `AM_A)
        ac = m;
      else begin
        dor = m;
      end
    end
    `OP_BIT: begin
      m = ac & m;
      `p_z = m == 0;
    end
    `OP_CMP, `OP_CPX, `OP_CPY: begin
      tmp = (op == `OP_CMP) ? ac : (op == `OP_CPX ? x : y);
      m = ~m;
      tmp = tmp + m + 9'b1;
      `p_n = tmp[7];
      `p_z = tmp[7:0] == 0;
      `p_c = tmp[8];
    end
    `OP_DEC: begin
      m = m - 1;
      `p_n = m[7];
      `p_z = m == 0;
      dor = m;
    end
    `OP_DEX: begin
      x = x - 1;
      `p_z = x == 0;
      `p_n = x[7];
    end
    `OP_DEY: begin
      y = y - 1;
      `p_z = y == 0;
      `p_n = y[7];
    end
    `OP_EOR: begin
      ac = ac ^ m;
      `p_z = ac == 0;
      `p_n = ac[7];
    end
    `OP_INC: begin
      m = m + 1;
      `p_n = m[7];
      `p_z = m == 0;
      dor = m;
    end
    `OP_INX: begin
      x = x + 1;
      `p_z = x == 0;
      `p_n = x[7];
    end
    `OP_INY: begin
      y = y + 1;
      `p_z = y == 0;
      `p_n = y[7];
    end
    `OP_LSR: begin
      tmp[7:0] = {1'b0, m[7:1]};
      `p_n = tmp[7];
      `p_z = tmp[7:0] == 0;
      m = tmp[7:0];
      if (am == `AM_A)
        ac = m;
      else begin
        dor = m;
      end
    end
    `OP_ORA: begin
      ac = ac | m;
      `p_n = ac[7];
      `p_z = ac == 0;
    end
    `OP_ROL: begin
      tmp = {m[7:0], `p_c};
      `p_n = tmp[7];
      `p_z = tmp[7:0] == 0;
      `p_c = tmp[8];
      m = tmp[7:0];
      if (am == `AM_A)
        ac = m;
      else begin
        dor = m;
      end
    end
    `OP_ROR: begin
      tmp[7:0] = {tmp[8], m[7:1]};
      `p_n = tmp[7];
      `p_z = tmp[7:0] == 0;
      m = tmp[7:0];
      if (am == `AM_A)
        ac = m;
      else begin
        dor = m;
      end
    end
    `OP_SBC: begin
      m = ~m;
      tmp = ac + m + `p_c;
      hc = ac[3:0] + m[3:0] + `p_c > 5'hf;
      `p_z = tmp[7:0] == 0;
      `p_n = tmp[7];
      `p_v = ac[7] == m[7] && tmp[7] != ac[7];
      `p_c = tmp[8];
      if (`p_d && !hc)
        tmp[3:0] = tmp[3:0] + 4'd10;
      if (`p_d && !`p_c)
        tmp[7:4] = tmp[7:4] + 4'd10;
      ac = tmp;
    end
  endcase
end

task print_ins();
  string s;
  case (ir)
    `BRK:       s = "BRK";
    `ORA_INDX:  s = "ORA_INDX";
    `ORA_ZP:    s = "ORA_ZP";
    `ASL_ZP:    s = "ASL_ZP";
    `PHP:       s = "PHP";
    `ORA_IMM:   s = "ORA_IMM";
    `ASL_A:     s = "ASL_A";
    `ORA_ABS:   s = "ORA_ABS";
    `ASL_ABS:   s = "ASL_ABS";
    `BPL:       s = "BPL";
    `ORA_INDY:  s = "ORA_INDY";
    `ORA_ZPX:   s = "ORA_ZPX";
    `ASL_ZPX:   s = "ASL_ZPX";
    `CLC:       s = "CLC";
    `ORA_ABSY:  s = "ORA_ABSY";
    `ORA_ABSX:  s = "ORA_ABSX";
    `ASL_ABSX:  s = "ASL_ABSX";
    `JSR:       s = "JSR";
    `AND_INDX:  s = "AND_INDX";
    `BIT_ZP:    s = "BIT_ZP";
    `AND_ZP:    s = "AND_ZP";
    `ROL_ZP:    s = "ROL_ZP";
    `PLP:       s = "PLP";
    `AND_IMM:   s = "AND_IMM";
    `ROL_A:     s = "ROL_A";
    `BIT_ABS:   s = "BIT_ABS";
    `AND_ABS:   s = "AND_ABS";
    `ROL_ABS:   s = "ROL_ABS";
    `BMI:       s = "BMI";
    `AND_INDY:  s = "AND_INDY";
    `AND_ZPX:   s = "AND_ZPX";
    `ROL_ZPX:   s = "ROL_ZPX";
    `SEC:       s = "SEC";
    `AND_ABSY:  s = "AND_ABSY";
    `AND_ABSX:  s = "AND_ABSX";
    `ROL_ABSX:  s = "ROL_ABSX";
    `RTI:       s = "RTI";
    `EOR_INDX:  s = "EOR_INDX";
    `EOR_ZP:    s = "EOR_ZP";
    `LSR_ZP:    s = "LSR_ZP";
    `PHA:       s = "PHA";
    `EOR_IMM:   s = "EOR_IMM";
    `LSR_A:     s = "LSR_A";
    `JMP_ABS:   s = "JMP_ABS";
    `EOR_ABS:   s = "EOR_ABS";
    `LSR_ABS:   s = "LSR_ABS";
    `BVC:       s = "BVC";
    `EOR_INDY:  s = "EOR_INDY";
    `EOR_ZPX:   s = "EOR_ZPX";
    `LSR_ZPX:   s = "LSR_ZPX";
    `CLI:       s = "CLI";
    `EOR_ABSY:  s = "EOR_ABSY";
    `EOR_ABSX:  s = "EOR_ABSX";
    `LSR_ABSX:  s = "LSR_ABSX";
    `RTS:       s = "RTS";
    `ADC_INDX:  s = "ADC_INDX";
    `ADC_ZP:    s = "ADC_ZP";
    `ROR_ZP:    s = "ROR_ZP";
    `PLA:       s = "PLA";
    `ADC_IMM:   s = "ADC_IMM";
    `ROR_A:     s = "ROR_A";
    `JMP_IND:   s = "JMP_IND";
    `ADC_ABS:   s = "ADC_ABS";
    `ROR_ABS:   s = "ROR_ABS";
    `BVS:       s = "BVS";
    `ADC_INDY:  s = "ADC_INDY";
    `ADC_ZPX:   s = "ADC_ZPX";
    `ROR_ZPX:   s = "ROR_ZPX";
    `SEI:       s = "SEI";
    `ADC_ABSY:  s = "ADC_ABSY";
    `ADC_ABSX:  s = "ADC_ABSX";
    `ROR_ABSX:  s = "ROR_ABSX";
    `STA_INDX:  s = "STA_INDX";
    `STY_ZP:    s = "STY_ZP";
    `STA_ZP:    s = "STA_ZP";
    `STX_ZP:    s = "STX_ZP";
    `DEY:       s = "DEY";
    `TXA:       s = "TXA";
    `STY_ABS:   s = "STY_ABS";
    `STA_ABS:   s = "STA_ABS";
    `STX_ABS:   s = "STX_ABS";
    `BCC:       s = "BCC";
    `STA_INDY:  s = "STA_INDY";
    `STY_ZPX:   s = "STY_ZPX";
    `STA_ZPX:   s = "STA_ZPX";
    `STX_ZPY:   s = "STX_ZPY";
    `TYA:       s = "TYA";
    `STA_ABSY:  s = "STA_ABSY";
    `TXS:       s = "TXS";
    `STA_ABSX:  s = "STA_ABSX";
    `LDY_IMM:   s = "LDY_IMM";
    `LDA_INDX:  s = "LDA_INDX";
    `LDX_IMM:   s = "LDX_IMM";
    `LDY_ZP:    s = "LDY_ZP";
    `LDA_ZP:    s = "LDA_ZP";
    `LDX_ZP:    s = "LDX_ZP";
    `TAY:       s = "TAY";
    `LDA_IMM:   s = "LDA_IMM";
    `TAX:       s = "TAX";
    `LDY_ABS:   s = "LDY_ABS";
    `LDA_ABS:   s = "LDA_ABS";
    `LDX_ABS:   s = "LDX_ABS";
    `BCS:       s = "BCS";
    `LDA_INDY:  s = "LDA_INDY";
    `LDY_ZPX:   s = "LDY_ZPX";
    `LDA_ZPX:   s = "LDA_ZPX";
    `LDX_ZPY:   s = "LDX_ZPY";
    `CLV:       s = "CLV";
    `LDA_ABSY:  s = "LDA_ABSY";
    `TSX:       s = "TSX";
    `LDY_ABSX:  s = "LDY_ABSX";
    `LDA_ABSX:  s = "LDA_ABSX";
    `LDX_ABSY:  s = "LDX_ABSY";
    `CPY_IMM:   s = "CPY_IMM";
    `CMP_INDX:  s = "CMP_INDX";
    `CPY_ZP:    s = "CPY_ZP";
    `CMP_ZP:    s = "CMP_ZP";
    `DEC_ZP:    s = "DEC_ZP";
    `INY:       s = "INY";
    `CMP_IMM:   s = "CMP_IMM";
    `DEX:       s = "DEX";
    `CPY_ABS:   s = "CPY_ABS";
    `CMP_ABS:   s = "CMP_ABS";
    `DEC_ABS:   s = "DEC_ABS";
    `BNE:       s = "BNE";
    `CMP_INDY:  s = "CMP_INDY";
    `CMP_ZPX:   s = "CMP_ZPX";
    `DEC_ZPX:   s = "DEC_ZPX";
    `CLD:       s = "CLD";
    `CMP_ABSY:  s = "CMP_ABSY";
    `CMP_ABSX:  s = "CMP_ABSX";
    `DEC_ABSX:  s = "DEC_ABSX";
    `CPX_IMM:   s = "CPX_IMM";
    `SBC_INDX:  s = "SBC_INDX";
    `CPX_ZP:    s = "CPX_ZP";
    `SBC_ZP:    s = "SBC_ZP";
    `INC_ZP:    s = "INC_ZP";
    `INX:       s = "INX";
    `SBC_IMM:   s = "SBC_IMM";
    `NOP:       s = "NOP";
    `CPX_ABS:   s = "CPX_ABS";
    `SBC_ABS:   s = "SBC_ABS";
    `INC_ABS:   s = "INC_ABS";
    `BEQ:       s = "BEQ";
    `SBC_INDY:  s = "SBC_INDY";
    `SBC_ZPX:   s = "SBC_ZPX";
    `INC_ZPX:   s = "INC_ZPX";
    `SED:       s = "SED";
    `SBC_ABSY:  s = "SBC_ABSY";
    `SBC_ABSX:  s = "SBC_ABSX";
    `INC_ABSX:  s = "INC_ABSX";
  endcase
  $display("[sim] %1t ns: ($%04X) %s", $time, pc, s);
endtask

reg [7:0] ddr, por;

always @* begin
  if (resp)
    ddr <= 8'h00;
  else if (!rw_int && p_sel && !CLK) begin
    if (ab == 16'h0000)
      ddr <= dor;
    else
      por <= dor;
  end
end

assign P[7] = ddr[7] ? por[7] : 1'bz;
assign P[6] = ddr[6] ? por[6] : 1'bz;
assign P[5] = ddr[5] ? por[5] : 1'bz;
assign P[4] = ddr[4] ? por[4] : 1'bz;
assign P[3] = ddr[3] ? por[3] : 1'bz;
assign P[2] = ddr[2] ? por[2] : 1'bz;
assign P[1] = ddr[1] ? por[1] : 1'bz;
assign P[0] = ddr[0] ? por[0] : 1'bz;

assign ipdb = ab == 16'h0000 ? ddr : P;

endmodule
