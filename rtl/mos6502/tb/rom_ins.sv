module rom
  (
   input        CS,
   input [8:0]  A,
   output [7:0] D
   );

reg [7:0] rom [0:511] =
 '{
`include "../../../asm/rom_ins_hex.sv"
   };

assign D = CS ? rom[A] : 8'hzz;

endmodule
