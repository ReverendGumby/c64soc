`timescale 1us / 1ns

// http://visual6502.org/JSSim/expert.html?graphics=false&logmore=DPControl&a=0000&d=a2ff9aa9ff48a9006810f830fe&a=fffe&d=0800
  
module test2();

wire [15:0] a;
wire [7:0]  d;
wire [7:0]  p;
reg         nres, nirq, nnmi;
reg         clk, cp1, cp1_posedge, cp2, cp2_posedge;

reg         aec;
wire        rw;
wire        sync;
reg         rdy;

mos6510 dut
  (
   .nRES(nres),
   .CLK(clk),
   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .nIRQ(nirq),
   .nNMI(nnmi),
   .AEC(aec),
   .A(a),
   .P(p),
   .DB(d),
   .RW(rw),
   .RDY(rdy),
   .SYNC(sync),
   .SO(1'b1),
   .HOLD(1'b0)
   );

initial begin
  aec = 1;
  clk = 1;
  cp1 = 0;
  cp1_posedge = 0;
  cp2 = 0;
  cp2_posedge = 0;
  nres = 0;
  nirq = 1;
  nnmi = 1;
  rdy = 1;
end

initial forever begin :clkgen
  #0.125 clk = ~clk;
end

initial forever begin :cpgen
  @(negedge clk) cp1_posedge = 1;
  @(posedge clk) cp1 = 1;
  @(negedge clk) cp1_posedge = 0;
  @(posedge clk) cp1 = 0;
  @(negedge clk) cp2_posedge = 1;
  @(posedge clk) cp2 = 1;
  @(negedge clk) cp2_posedge = 0;
  @(posedge clk) cp2 = 0;
end

initial begin
  #4 nres = 1;
end

wire rom_cs, ram_cs;

assign ram_cs = !rom_cs;
ram ram(.CLK(cp2), .A(a[15:0]), .D(d), .CS(ram_cs), .RW(rw));

reg [7:0] rom [0:255];
initial begin
  rom['h00:'h0c] =
   '{
     'ha2, 'hff,                // LDX #$FF
     'h9a,                      // TXS
     'ha9, 'hff,                // LDA #$FF
     'h48,                      // PHA
     'ha9, 'h00,                // LDA #$00
     'h68,                      // PLA
     'h10, 'hf8,                // BPL $FF03
     'h30, 'hfe                 // BMI $FF0B
     };
  
  rom['hfc:'hfd] = '{'h00, 'hff}; // rstvec
end

assign rom_cs = rw && a[15:8] == 8'hff;
assign d = rom_cs ? rom[a[7:0]] : 8'hzz;

reg [0:49] vec [0:20] =
  '{
//   sync  rw    a         d       pc        p                cycle
    {1'b1, 1'b1, 16'hff00, 8'ha2,  16'hff00, 8'bxx11x1xx}, // 0
    {1'b0, 1'b1, 16'hff01, 8'hff,  16'hff01, 8'bxx11x1xx}, // 1
    {1'b1, 1'b1, 16'hff02, 8'h9a,  16'hff02, 8'b1x11x10x}, // 2
    {1'b0, 1'b1, 16'hff03, 8'ha9,  16'hff03, 8'b1x11x10x}, // 3
    {1'b1, 1'b1, 16'hff03, 8'ha9,  16'hff03, 8'b1x11x10x}, // 4
    {1'b0, 1'b1, 16'hff04, 8'hff,  16'hff04, 8'b1x11x10x}, // 5
    {1'b1, 1'b1, 16'hff05, 8'h48,  16'hff05, 8'b1x11x10x}, // 6
    {1'b0, 1'b1, 16'hff06, 8'ha9,  16'hff06, 8'b1x11x10x}, // 7
    {1'b0, 1'b0, 16'h01ff, 8'hff,  16'hff06, 8'b1x11x10x}, // 8
    {1'b1, 1'b1, 16'hff06, 8'ha9,  16'hff06, 8'b1x11x10x}, // 9
    {1'b0, 1'b1, 16'hff07, 8'h00,  16'hff07, 8'b1x11x10x}, // 10
    {1'b1, 1'b1, 16'hff08, 8'h68,  16'hff08, 8'b0x11x11x}, // 11
    {1'b0, 1'b1, 16'hff09, 8'h10,  16'hff09, 8'b0x11x11x}, // 12
    {1'b0, 1'b1, 16'h01fe, 8'hzz,  16'hff09, 8'b0x11x11x}, // 13
    {1'b0, 1'b1, 16'h01ff, 8'hff,  16'hff09, 8'b0x11x11x}, // 14
    {1'b1, 1'b1, 16'hff09, 8'h10,  16'hff09, 8'b1x11x10x}, // 15
    {1'b0, 1'b1, 16'hff0a, 8'hf8,  16'hff0a, 8'b1x11x10x}, // 16
    {1'b1, 1'b1, 16'hff0b, 8'h30,  16'hff0b, 8'b1x11x10x}, // 17
    {1'b0, 1'b1, 16'hff0c, 8'hfe,  16'hff0c, 8'b1x11x10x}, // 18
    {1'b0, 1'b1, 16'hff0d, 8'hzz,  16'hff0d, 8'b1x11x10x}, // 19
    {1'b1, 1'b1, 16'hff0b, 8'h30,  16'hff0b, 8'b1x11x10x}  // 20
    };

integer     cycle = 0;
wire        sync_sim = vec[cycle][0];
wire        rw_sim =   vec[cycle][1];
wire [15:0] a_sim =    vec[cycle][2:17];
wire [7:0]  d_sim =    vec[cycle][18:25];
wire [15:0] pc_sim =   vec[cycle][26:41];
wire [7:0]  p_sim =    vec[cycle][42:49];

initial #12 forever begin
  automatic reg err = 0;
  automatic reg [15:0] dut_pc;
  
  @(negedge cp1) ;
  assert (sync_sim === sync)
    else begin
      $display("**** assert SYNC @%1t ns: sim %1b != dut %1b", $time, sync_sim, sync);
      err = 1;
    end
  assert (rw_sim === rw)
    else begin
      $display("**** assert RW @%1t ns: sim %1b != dut %1b", $time, rw_sim, rw);
      err = 1;
    end
  assert (a_sim === a)
    else begin
      $display("**** assert A @%1t ns: sim %04x != dut %04x", $time, a_sim, a);
      err = 1;
    end

  dut_pc = {dut.pch, dut.pcl};
  assert (pc_sim === dut_pc)
    else begin
      $display("**** assert pc @%1t ns: sim %04x != dut %04x", $time, pc_sim, dut_pc);
      err = 1;
    end
  assert (p_sim === dut.pf)
    else begin
      $display("**** assert pf @%1t ns: sim %8b != dut %8b", $time, p_sim, dut.pf);
      err = 1;
    end

  @(posedge cp2) #0.04 ;
  assert (d_sim === 8'hzz || d_sim === d)
    else begin
      $display("**** assert DB @%1t ns: sim %02x != dut %02x", $time, d_sim, d);
      err = 1;
    end

  if (err)
    $stop;

  cycle = cycle + 1;
  if (cycle == 21) begin
    $display("Pass!");
    $finish;
  end
end

endmodule
