`timescale 1ns / 1ns

module ram
  (
   input        CLK,
   input        CS,
   input        RW,
   input [15:0] A,
   inout [7:0]  D
   );

reg [7:0] ram [0:16'hffff];

assign D = CS && RW ? ram[A] : 8'hzz;

always @(posedge CLK) #40 begin
  if (CS && !RW)
    ram[A] <= D;
end

endmodule
