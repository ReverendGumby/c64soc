module rom
  (
   input        CS,
   input [7:0]  A,
   output [7:0] D
   );

reg [7:0] rom [0:255] =
 '{
   // 16'hFF00
   8'bxxx111xx, 8'h00,
   //8'hA9, 8'h00,                                // LDA #$00
   8'h8D, 8'h00, 8'h00,                         // STA $0000
   8'hEE, 8'h00, 8'h00,                         // INC $0000
   8'hD0, 8'hFB,                                // BNE -5
   8'h4C, 8'h0A, 8'hFF,                         // JMP $FF0A
   8'hEA,
   8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   // 16'hFF40
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   // 16'hFF80
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   // 16'hFFC0
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA, 8'hEA,
   8'hEA, 8'hEA, 8'h00, 8'hFF, 8'h00, 8'hFF, 8'h00, 8'hFF
   };

assign D = CS ? rom[A] : 8'hzz;

endmodule
