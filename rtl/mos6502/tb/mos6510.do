vmap work

vlog ../mos6510.sv +incdir+.. +define+PLA_OUTPUTS +define+BUS_CONFLICTS
vlog rom.sv
vlog ram.sv
vlog mos6510_tb.sv

vsim mos6510_tb

delete wave *
configure wave -signalnamewidth 1
env sim:/mos6510_tb
add wave nRES CLK RW SYNC
add wave -radix hex A DB
env sim:/mos6510_tb/mos6510
add wave sync0 resp clear_ir
add wave {t {t5 t4 t3 t2 clock2 clock1}}

#add wave onebyte twocycle pipeUNK35 ins_done clock1_next
#add wave vec0 vec1 add_pcl_db_pch brk_done
#add wave clear_ir fetch
#add wave -radix hex pd
add wave -radix hex ir
add wave -radix hex {pc {pch pcl}}

add wave -radix hex ac x y s pf
add wave -radix hex dl idb sb adl adh
add wave -radix hex ai bi add
add wave avr acr hc

add wave branch_p branch_p_match branch_do_take branch_dont_take branch_taken branch_hold branch_carry branch_overflow branch_done_sync ins_done_branch

add wave pla_col

add wave -group DPcontrol zero_adh zero_adl adl_pcl adh_pch s_adl add_adl add_sb sb_adh adl_abl adh_abh pcl_adl pch_adh i_pc sums db_add ndb_add adl_add sb_add sb_s one_addc zero_add dl_adl dl_adh dl_db pcl_db pch_db p_db ac_db sb_db x_sb y_sb s_sb sb_x sb_y daa dsa ac_sb sb_ac write_dor db0_c ir5_c acr_c db1_z dbz_z db2_i ir5_i db3_d ir5_d db6_v avr_v one_v zero_v db7_n

add wave SO so_latched so_last so_negedge


#configure wave -namecolwidth 210
run 100
wave zoom full
