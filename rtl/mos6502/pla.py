#!/usr/bin/env python

import sys
import re
import StringIO
from tokenize import *

class vec:
    def __init__(self, val=[]):
        self.v = []
        if type(val) is type(self):
            self.v = val.v[:]
        elif type(val) is str:
            for i in reversed(range(len(val))):
                self.v.append(val[i])
        elif type(val) is list:
            for i in reversed(range(len(val))):
                self.append(val[i])
        else:
            raise TypeError('unsupported type')
    def __str__(self):
        s = ''
        for i in reversed(range(len(self.v))):
            s = s + self.v[i]
        return s
    def __repr__(self):
        return 'vec(\'' + str(self) + '\')'
    def __len__(self):
        return len(self.v)
    def __getitem__(self, key):
        return self.v[key]
    def __setitem__(self, key, val):
        self.v[key] = val
    def __add__(self, other):
        return vec(self.v + other.v)
    def __cmp__(self, other):
        return self.v.__cmp__(other.v)
    def append(self, val):
        b = str(val)
        if len(b) != 1:
            raise TypeError("list element must be single char string")
        self.v = self.v + [ b ]
    def __invert__(self):
        ret = vec(self)
        for i in range(len(ret)):
            ret[i] = not_bin(ret[i])
        return ret
    def __and__(self, other):
        ret = vec(self)
        for i in range(len(ret)):
            ret[i] = and_bin(ret[i], other[i])
        return ret
    def __or__(self, other):
        ret = vec(self)
        for i in range(len(ret)):
            ret[i] = or_bin(ret[i], other[i])
        return ret

opcode_names = [
    'BRK',    'ORA IND,X', '(02)',   '(03)', '(04)',     'ORA ZP',   'ASL ZP',   '(07)',
    'PHP',    'ORA IMM',   'ASL A',  '(0B)', '(0C)',     'ORA ABS',  'ASL ABS',  '(0F)',
    'BPL',    'ORA IND,Y', '(12)',   '(13)', '(14)',     'ORA ZP,X', 'ASL ZP,X', '(17)',
    'CLC',    'ORA ABS,Y', '(1A)',   '(1B)', '(1C)',     'ORA ABS,X','ASL ABS,X','(1F)',
    'JSR',    'AND IND,X', '(22)',   '(23)', 'BIT ZP',   'AND ZP',   'ROL ZP',   '(27)',
    'PLP',    'AND IMM',   'ROL A',  '(2B)', 'BIT ABS',  'AND ABS',  'ROL ABS',  '(2F)',
    'BMI',    'AND IND,Y', '(32)',   '(33)', '(34)',     'AND ZP,X', 'ROL ZP,X', '(37)',
    'SEC',    'AND ABS,Y', '(3A)',   '(3B)', '(3C)',     'AND ABS,X','ROL ABS,X','(3F)',
    'RTI',    'EOR IND,X', '(42)',   '(43)', '(44)',     'EOR ZP',   'LSR ZP',   '(47)',
    'PHA',    'EOR IMM',   'LSR A',  '(4B)', 'JMP ABS',  'EOR ABS',  'LSR ABS',  '(4F)',
    'BVC',    'EOR IND,Y', '(52)',   '(53)', '(54)',     'EOR ZP,X', 'LSR ZP,X', '(57)',
    'CLI',    'EOR ABS,Y', '(5A)',   '(5B)', '(5C)',     'EOR ABS,X','LSR ABS,X','(5F)',
    'RTS',    'ADC IND,X', '(62)',   '(63)', '(64)',     'ADC ZP',   'ROR ZP',   '(67)',
    'PLA',    'ADC IMM',   'ROR A',  '(6B)', 'JMP IND',  'ADC ABS',  'ROR ABS',  '(6F)',
    'BVS',    'ADC IND,Y', '(72)',   '(73)', '(74)',     'ADC ZP,X', 'ROR ZP,X', '(77)',
    'SEI',    'ADC ABS,Y', '(7A)',   '(7B)', '(7C)',     'ADC ABS,X','ROR ABS,X','(7F)',
    '(80)',   'STA IND,X', '(82)',   '(83)', 'STY ZP',   'STA ZP',   'STX ZP',   '(87)',
    'DEY',    '(89)',      'TXA',    '(8B)', 'STY ABS',  'STA ABS',  'STX ABS',  '(8F)',
    'BCC',    'STA IND,Y', '(92)',   '(93)', 'STY ZP,X', 'STA ZP,X', 'STX ZP,Y', '(97)',
    'TYA',    'STA ABS,Y', 'TXS',    '(9B)', '(9C)',     'STA ABS,X','(9E)',     '(9F)',
    'LDY IMM','LDA IND,X', 'LDX IMM','(A3)', 'LDY ZP',   'LDA ZP',   'LDX ZP',   '(A7)',
    'TAY',    'LDA IMM',   'TAX',    '(AB)', 'LDY ABS',  'LDA ABS',  'LDX ABS',  '(AF)',
    'BCS',    'LDA IND,Y', '(B2)',   '(B3)', 'LDY ZP,X', 'LDA ZP,X', 'LDX ZP,Y', '(B7)',
    'CLV',    'LDA ABS,Y', 'TSX',    '(BB)', 'LDY ABS,X','LDA ABS,X','LDX ABS,Y','(BF)',
    'CPY IMM','CMP IND,X', '(C2)',   '(C3)', 'CPY ZP',   'CMP ZP',   'DEC ZP',   '(C7)',
    'INY',    'CMP IMM',   'DEX',    '(CB)', 'CPY ABS',  'CMP ABS',  'DEC ABS',  '(CF)',
    'BNE',    'CMP IND,Y', '(D2)',   '(D3)', '(D4)',     'CMP ZP,X', 'DEC ZP,X', '(D7)',
    'CLD',    'CMP ABS,Y', '(DA)',   '(DB)', '(DC)',     'CMP ABS,X','DEC ABS,X','(DF)',
    'CPX IMM','SBC IND,X', '(E2)',   '(E3)', 'CPX ZP',   'SBC ZP',   'INC ZP',   '(E7)',
    'INX',    'SBC IMM',   'NOP',    '(EB)', 'CPX ABS',  'SBC ABS',  'INC ABS',  '(EF)',
    'BEQ',    'SBC IND,Y', '(F2)',   '(F3)', '(F4)',     'SBC ZP,X', 'INC ZP,X', '(F7)',
    'SED',    'SBC ABS,Y', '(FA)',   '(FB)', '(FC)',     'SBC ABS,X','INC ABS,X','(FF)',
    ]

def err(why):
    print why
    sys.exit(1)

def read_pla_sv():
    global fets, output_names
    fets = []
    col_names = []
    with open('pla.sv') as f:
        for line in f:
            if 'initial fets' in line:
                p = re.compile('21\'b([01]+);')
                m = p.search(line)
                fets.append(vec(m.group(1)))
            elif 'pla_col[' in line:
                p = re.compile('(wire|assign) ([A-Za-z0-9_]+) = pla_col');
                m = p.search(line)
                col_names.append(m.group(2))
    output_names = col_names[:] + [ 'op_implied' ]

def hex_nibble_to_bin(v):
    if v.lower() == 'x':
        return 'xxxx'
    return '{0:04b}'.format(int(v, 16))

def hex_to_vec(v):
    if len(v) > 2 and v[0:2] == '0x':
        v = v[2:]
    if len(v) == 1:
        v = '0' + v
    return vec(hex_nibble_to_bin(v[0]) + hex_nibble_to_bin(v[1]))

def not_bin(v):
    if v == '0':
        return '1'
    elif v == '1':
        return '0'
    else:
        return 'x'

def or_bin(a, b):
    out = {
        '00':'0',
        '01':'1',
        '0x':'x',
        '10':'1',
        '11':'1',
        '1x':'1',
        'x0':'x',
        'x1':'1',
        'xx':'x'
        }
    return out[a + b]

def and_bin(a, b):
    out = {
        '00':'0',
        '01':'0',
        '0x':'0',
        '10':'0',
        '11':'1',
        '1x':'x',
        'x0':'0',
        'x1':'x',
        'xx':'x'
        }
    return out[a + b]

def compute_pla_inputs(ir, t):
    nir = ~ir
    irline3 = or_bin(ir[0], ir[1])
    rows = vec([t[5], t[4], t[3], t[2], nir[1], irline3, nir[0],
                ir[7], nir[7], ir[4], nir[4], ir[3], nir[3], ir[2],
                nir[2], ir[6], nir[6], ir[5], nir[5], t[0], t[1]])
    return rows

t_default = vec('000000')

def compute_pla_output(out_idx, ir, t=t_default, rows=None):
    if rows is None:
        rows = compute_pla_inputs(ir, t)
    if out_idx == 130:
        x_op_push_pull = compute_pla_output(121, ir, t, rows=rows)
        op_implied = not_bin(or_bin(or_bin(or_bin(x_op_push_pull, ir[0]), not_bin(ir[3])), ir[2]))
        return op_implied
    fet = fets[out_idx]
    cols = fet & rows;
    tmp = '0'
    for row in range(len(rows)):
        tmp = or_bin(tmp, cols[row])
    out = not_bin(tmp)
    if out_idx == 83 or out_idx == 90:
        nop_push_pull = not_bin(compute_pla_output(97, ir, t, rows=rows))
        out = and_bin(out, nop_push_pull)
    return out

def compute_pla_outputs(ir, t=t_default):
    rows = compute_pla_inputs(ir, t)
    outputs = []
    for col in range(len(output_names)):
        outputs.append(compute_pla_output(out_idx=col, ir=ir, t=t, rows=rows))
    return outputs

def get_opcodes_vec(output):
    output_idx = output_names.index(output)
    opcodes = vec()
    for op in range(256):
        op = hex_to_vec(hex(op))
        output = compute_pla_output(out_idx=output_idx, ir=op)
        opcodes.append(output)
    return opcodes

def list_opcodes_vec(opcodes):
    print 'MSD 0 1 2 3 4 5 6 7 8 9 A B C D E F LSD'
    for y in range(16):
        print ' {0:X} '.format(y),
        for x in range(16):
            op = y * 16 + x
            print ('X' if opcodes[op] == '1' else '.'),
        print
    print

    names = []
    for op in range(256):
        if opcodes[op] == '1':
            names.append(opcode_names[op])

    for i in range(len(names)):
        print '{0:12s}'.format(names[i]),
        if i % 4 == 3:
            print
    print
    
def convert_expression_to_python(expr):
    expr = expr.replace('&&', '&')
    expr = expr.replace('||', '|')
    expr = expr.replace('!', '~')

    t = []
    sio = StringIO.StringIO(expr)
    g = generate_tokens(sio.readline)
    for toknum, tokval, _, _, _ in g:
        if toknum == NAME:
            t.extend([
                (NAME, 'get_opcodes_vec'),
                (OP, '('),
                (STRING, repr(tokval)),
                (OP, ')')
                ])
        else:
            t.append((toknum, tokval))
    return untokenize(t)

def list_opcodes(expr):
    py_expr = convert_expression_to_python(expr)
    res = eval(py_expr)
    list_opcodes_vec(res)
    
def list_pla_outputs(opcode):
    opcode = opcode.lower()
    if len(opcode) == 2:
        opcode = hex_to_vec(opcode)
    else:
        opcode = vec(opcode)
    outputs = compute_pla_outputs(ir=opcode)

    blacklist = [ 'op_t0', 'op_t2', 'op_t3', 'op_t4' ]
    for i in range(len(outputs)):
        if outputs[i] == '1':
            name = output_names[i] 
            if name not in blacklist:
                print name

def usage():
    prg = sys.argv[0]
    print 'usage: %s OPCODE | OUTPUT'
    print
    print '  OPCODE - list PLA outputs that turn on for OPCODE'
    print '  OPCODE is binary (10100011) or hex (a3). x is okay'
    print
    print '  OUTPUT - list opcodes that turn on PLA OUTPUT'
    sys.exit(1)

if len(sys.argv) < 2:
    usage()
else:
    read_pla_sv()
    arg = sys.argv[1]
    if 'op_' in arg:
        list_opcodes(arg)
    else:
        list_pla_outputs(arg)
