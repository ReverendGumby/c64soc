// MOS 6502 emulation, behaving as near to silicon as I care to make it.
//
// Credit to:
// . http://www.visual6502.org
// . http://www.downloads.reactivemicro.com/Public/Electronics/CPU/6502%20Schematic.pdf
// . How MOS 6502 Illegal Opcodes really work. pagetable.com
// . Commodore 6510 datasheet, Nov 1982
// . Klaus Dormann, 6502 Functional Tests, (C) 2012. http://2m5.de
// . Sheer stubbornness

// module mos6502
//   (
//    input         nRES,                     // reset
//    input         CLK,                      // master clock
//    input         CP1_POSEDGE,              // clock phase 1
//    input         CP1_NEGEDGE,
//    input         CP2_POSEDGE,              // clock phase 2
//    input         CP2_NEGEDGE,
//    input         nIRQ,                     // interrupt request
//    input         nNMI,                     // non-maskable interrupt
//    input         AEC,                      // address enable control
//    output [15:0] A_O,                      // address bus, output
//    output        A_OE,                     //   output enable
//    input [7:0]   DB_I,                     // data bus, input
//    output [7:0]  DB_O,                     //   output data
//    output        DB_OE,                    //   output enable
//    output        RW,                       // read / not write
//    input         RDY,                      // ready
//    output reg    SYNC,                     // instruction sync
//    input         SO,                       // set overflow
//    input         HOLD,                     // halt and release A,D,RW
//    input [4:0]   CPU_REG_SEL,              // register monitor
//    output reg [7:0] CPU_REG_DOUT
//    );

// Processor status register friendly names
`define p_n pf[7]
`define p_v pf[6]
`define p_b pf[4]
`define p_d pf[3]
`define p_i pf[2]
`define p_z pf[1]
`define p_c pf[0]

wire cp1_posedge, cp1_negedge, cp2_posedge, cp2_negedge;
wire clear_ir;
wire twocycle, onebyte;
wire write_dor;
wire ins_done_pre;
wire branch_p_match, branch_dont_take, ins_done_branch;
wire short_circuit_branch_add, short_circuit_idx_add;

// All busses valid during cp2
reg       cp2, cp1_posedge_d1;
reg [7:0] idb, idb_drv;
reg [7:0] sb, sb_drv;
reg [7:0] adl;
reg [7:0] adh, adh_drv;
reg       rdy1, rdy2, rdy2_d2;
reg       resp, resp_d1, resg;
reg       irqp;
reg       nmip, nmil, nmig;
reg       intg;
reg [7:0] pd;
reg [7:0] ir;
reg       clock1, clock2, t2, t3, t4, t5;
reg [7:0] dor;
reg [7:0] dl;
reg [7:0] pcl, pch;
reg [7:0] pcls, pchs;                           // select registers
reg [7:0] abl, abh;
reg [7:0] pf, s, ac, x, y;
reg [7:0] s_pre;
reg [7:0] ai, bi, add, cd_sums, cd_ands, cd_eors, cd_ors, cd_srs, cd;
reg       addc, add_daa, avo, avr, aco, hco, hcr;
reg       ahc, dhc, afc, dfc, fc;
reg [7:0] daa_out, daa_adj;
reg       acr, acr_d1;
reg       vec1, brk_done;
reg       fetch;
reg       sync_pre, sync_pre_reg, sync0;
reg       ins_done;
reg       so_last;
reg       branch_p, branch_taken, branch_offset_sign;
reg       so_negedge;
reg       rw_int;

// HOLD transitions on CP2_NEGEDGE and applies to the following CP1_POSEDGE.
reg  hold_dly;
always @(posedge CLK) if (CP1_POSEDGE)
  hold_dly <= HOLD;

// Gate clocks and clock edges by HOLD (delayed).
initial begin
  cp2 <= 1'b0;
end

assign cp1_posedge = CP1_POSEDGE && !hold_dly;
assign cp1_negedge = CP1_NEGEDGE && !hold_dly;
assign cp2_posedge = CP2_POSEDGE && !hold_dly;
assign cp2_negedge = CP2_NEGEDGE && !hold_dly;

always @(posedge CLK)
  cp2 <= (cp2 && ~cp2_negedge) || cp2_posedge;
  
`ifdef MOS6502_ENABLE_ICD_CYC
always @(posedge CLK) if (cp1_posedge) begin
  // We want ICD_CYC = 8 on first instruction out of reset
  if (resp)
    ICD_CYC <= 32'd0;
  else
    ICD_CYC[31:0] <= ICD_CYC + 1'd1;
end
`endif

// abl/abh need to wait for CP1_POSEDGE-triggered logic.
always @(posedge CLK)
  cp1_posedge_d1 <= cp1_posedge;

// Also gate AEC by HOLD (delayed).
wire aec_int = AEC && !hold_dly;

//////////////////////////////////////////////////////////////////////
// pla: 21x130 decode ROM
wire irline3 = ir[0] || ir[1];
wire [0:20] pla_row = {  t5,    t4,     t3,     t2,    !ir[1],irline3, !ir[0],
                        ir[7], !ir[7],  ir[4], !ir[4],  ir[3], !ir[3],  ir[2],
                       !ir[2],  ir[6], !ir[6],  ir[5], !ir[5], clock1, clock2 };
wire [0:129] pla_col;

mos6502_pla pla
  (
   .ROW(pla_row),
   .COL(pla_col)
   );

// Friendly names for column outputs
wire op_push_pull;
wire op_sty_cpy_mem = pla_col[0];
wire op_t3_ind_y = pla_col[1];
wire op_t2_abs_y = pla_col[2];
wire op_t0_iny_dey = pla_col[3];
wire x_op_t0_tya = pla_col[4];
wire op_t0_cpy_iny = pla_col[5];
wire op_t2_idx_x_xy = pla_col[6];
wire op_xy = pla_col[7];
wire op_t2_ind_x = pla_col[8];
wire x_op_t0_txa = pla_col[9];
wire op_t0_dex = pla_col[10];
wire op_t0_cpx_inx = pla_col[11];
wire op_from_x = pla_col[12];
wire op_t0_txs = pla_col[13];
wire op_t0_ldx_tax_tsx = pla_col[14];
wire op_tp_dex = pla_col[15];
wire op_tp_inx = pla_col[16];
wire op_t0_tsx = pla_col[17];
wire op_tp_iny_dey = pla_col[18];
wire op_t0_ldy_mem = pla_col[19];
wire op_t0_tay_ldy_not_idx = pla_col[20];
wire op_t0_jsr = pla_col[21];
wire op_t5_brk = pla_col[22];
wire op_t0_php_pha = pla_col[23];
wire op_t4_rts = pla_col[24];
wire op_t3_plp_pla = pla_col[25];
wire op_t5_rti = pla_col[26];
wire op_ror = pla_col[27];
wire op_t2 = pla_col[28];
wire op_t0_eor = pla_col[29];
wire op_jmp = pla_col[30];
wire op_t2_abs = pla_col[31];
wire op_t0_ora = pla_col[32];
wire op_t2_adl_add = pla_col[33];
wire op_t0 = pla_col[34];
wire op_t2_stack = pla_col[35];
wire op_t3_stack_bit_jmp = pla_col[36];
wire op_t4_brk_jsr = pla_col[37];
wire op_t4_rti = pla_col[38];
wire op_t3_ind_x = pla_col[39];
wire op_t4_ind_y = pla_col[40];
wire op_t2_ind_y = pla_col[41];
wire op_t3_abs_idx = pla_col[42];
wire op_plp_pla = pla_col[43];
wire op_inc_nop = pla_col[44];
wire op_t4_ind_x = pla_col[45];
wire x_op_t3_ind_y = pla_col[46];
wire op_rti_rts = pla_col[47];
wire op_t2_jsr = pla_col[48];
wire op_t0_cpx_cpy_inx_iny = pla_col[49];
wire op_t0_cmp = pla_col[50];
wire op_t0_sbc = pla_col[51];
wire op_t0_adc_sbc = pla_col[52];
wire op_rol = pla_col[53];                      // was op_rol_ror
wire op_t3_jmp = pla_col[54];
wire op_shift = pla_col[55];    // same as op_asl_rol
wire op_t5_jsr = pla_col[56];
wire op_t2_stack_access = pla_col[57];
wire op_t0_tya = pla_col[58];
wire op_tp_ora_and_eor_adc = pla_col[59];
wire op_tp_adc_sbc = pla_col[60];
wire op_tp_shift_a = pla_col[61];
wire op_t0_txa = pla_col[62];
wire op_t0_pla = pla_col[63];
wire op_t0_lda = pla_col[64];
wire op_t0_acc = pla_col[65];
wire op_t0_tay = pla_col[66];
wire op_t0_shift_a = pla_col[67];
wire op_t0_tax = pla_col[68];
wire op_t0_bit = pla_col[69];
wire op_t0_and = pla_col[70];
wire op_t4_abs_idx = pla_col[71];
wire op_t5_ind_y = pla_col[72];
wire op_branch_done = pla_col[73];
wire op_t2_pha = pla_col[74];
wire op_t0_shift_right_a = pla_col[75];
wire op_shift_right = pla_col[76];
wire op_t2_brk = pla_col[77];
wire op_t3_jsr = pla_col[78];
wire op_sta_cmp = pla_col[79];
wire op_t2_branch = pla_col[80];
wire op_t2_zp_zp_idx = pla_col[81];
wire op_t2_ind = pla_col[82];
wire op_t2_abs_access = pla_col[83] && !op_push_pull;
wire op_t5_rts = pla_col[84];
wire op_t4 = pla_col[85];
wire op_t3 = pla_col[86];
wire op_t0_brk_rti = pla_col[87];
wire op_t0_jmp = pla_col[88];
wire op_t5_ind_x = pla_col[89];
wire op_t3_abs_idx_ind = pla_col[90] && !op_push_pull;
wire x_op_t4_ind_y = pla_col[91];
wire x_op_t3_abs_idx = pla_col[92];
wire op_t3_branch = pla_col[93];
wire op_brk_rti = pla_col[94];
wire op_jsr = pla_col[95];
wire x_op_jmp = pla_col[96];
assign op_push_pull = pla_col[97];
wire op_store = pla_col[98];
wire op_t4_brk = pla_col[99];
wire op_t2_php = pla_col[100];
wire op_t2_php_pha = pla_col[101];
wire op_t4_jmp = pla_col[102];
wire op_t5_rti_rts = pla_col[103];
wire xx_op_t5_jsr = pla_col[104];
wire op_t2_jmp_abs = pla_col[105];
wire x_op_t3_plp_pla = pla_col[106];
wire op_lsr_ror_dec_inc = pla_col[107];
wire op_asl_rol = pla_col[108]; // same as op_shift
wire op_t0_cli_sei = pla_col[109];
wire op_tp_bit = pla_col[110];
wire op_t0_clc_sec = pla_col[111];
wire op_t3_mem_zp_idx = pla_col[112];
wire x_op_tp_adc_sbc = pla_col[113];
wire x_op_t0_bit = pla_col[114];
wire op_t0_plp = pla_col[115];
wire x_op_t4_rti = pla_col[116];
wire op_tp_cmp = pla_col[117];
wire op_tp_cpx_cpy_abs = pla_col[118];
wire op_tp_asl_rol_a = pla_col[119];
wire op_tp_cpx_cpy_imm_zp = pla_col[120];
wire x_op_push_pull = pla_col[121];
wire op_t0_cld_sed = pla_col[122];
wire nop_branch_bit6 = pla_col[123];
wire op_t3_mem_abs = pla_col[124];
wire op_t2_mem_zp = pla_col[125];
wire op_t5_mem_ind_idx = pla_col[126];
wire op_t4_mem_abs_idx = pla_col[127];
wire nop_branch_bit7 = pla_col[128];
wire op_clv = pla_col[129];

// Technically inside PLA, but since it has some different rows,
// compute it outside.
wire op_implied = ~|{x_op_push_pull, ir[0], !ir[3], ir[2]};

`ifdef PLA_OUTPUTS
`include "pla_outputs.sv"
`endif

//////////////////////////////////////////////////////////////////////
// datapath control inputs
reg [2:0] zero_adl;
reg [7:0] zero_adh;
reg [7:0] add_sb;
reg       dl_db, dl_adl, dl_adh, adh_abh, adl_abl;
reg       adl_pcl, i_pc, pcl_db, pcl_adl, adh_pch, pch_db, pch_adh, sb_adh;
reg       sb_db, s_adl, sb_s, s_sb, ndb_add;
reg       db_add, adl_add, one_addc, daa, dsa, sums, ands, eors, ors, srs;
reg       add_adl, zero_add, sb_add, sb_ac, ac_db, ac_sb;
reg       sb_x, x_sb, sb_y, y_sb, p_db, db0_c, ir5_c, acr_c, db1_z, dbz_z;
reg       db2_i, ir5_i, db3_d, ir5_d, db6_v, avr_v, one_v, zero_v, db7_n;
reg       nrw, one_i;

// control pins

// RDY pin: halts processor when low. Stable during cp2.

// On Si: during cp2, moved onto notRdy0 and used by datapath. We
// register datapath on posedge cp2, so rdy2 needs to be stable
// earlier.
always @* begin
  rdy2 = RDY || !rw_int;        // use with cp2
end

always @(posedge CLK) if (cp2_posedge) begin
  rdy1 <= rdy2;                 // use with cp1
end

always @(posedge CLK) if (cp1_posedge) begin
  rdy2_d2 <= rdy1;
end

// RW pin: read / not write. Tri-stated (pulled high) when AEC low
// (undocumented in datasheet).
always @(posedge CLK) if (cp1_posedge) if (rdy1) begin
  rw_int <= !nrw;
end
assign RW = !aec_int || rw_int;

always @(posedge CLK) if (cp1_posedge) begin
  so_last <= SO;
end

// so_negedge goes high for 1 CLK on SO falling edge during cp1.
always @(posedge CLK) if (cp1_negedge) begin
  if (!SO && so_last)
    so_negedge <= 1'b1;
  else if (rdy1)
    so_negedge <= 1'b0;
end

// db: data bus
wire [7:0] db;

// idb: internal data bus
always @* begin
  idb_drv = 8'hff;                              // precharge MOSFETs
  if (dl_db && !cp2) idb_drv = idb_drv & dl;
  if (pch_db)        idb_drv = idb_drv & pch;
  if (pcl_db)        idb_drv = idb_drv & pcl;
  if (p_db)          idb_drv = idb_drv & pf;
  if (ac_db && !cp2) idb_drv = idb_drv & ac;
end

always @* begin
  idb = 8'hxx;
  case ({sb_db, sb_adh})
    2'b00:  idb = idb_drv;
    2'b01:  idb = idb_drv;
    2'b10:  idb = idb_drv & sb_drv;
    2'b11:  idb = idb_drv & sb_drv & adh_drv;
  endcase
end

// sb: "special" bus
always @* begin
  sb_drv = 8'hff;                               // precharge MOSFETs
  if (ac_sb)   sb_drv = sb_drv & ac;
  if (x_sb)    sb_drv = sb_drv & x;
  if (y_sb)    sb_drv = sb_drv & y;
  if (s_sb)    sb_drv = sb_drv & s;
  if (|add_sb) sb_drv = sb_drv & (add | ~add_sb);
end

always @* begin
  sb = 8'hxx;
  case ({sb_db, sb_adh})
    2'b00:  sb = sb_drv;
    2'b01:  sb = sb_drv & adh_drv;
    2'b10:  sb = sb_drv & idb_drv;
    2'b11:  sb = sb_drv & idb_drv & adh_drv;
  endcase
end

// adl / adh: internal address busses
always @* begin
  adl = 8'hff;                                  // precharge MOSFETs
  if (|zero_adl) adl = adl & ~zero_adl;         // open drain MOSFETs
  if (dl_adl)  adl = adl & dl;
  if (pcl_adl) adl = adl & pcl;
  if (s_adl)   adl = adl & s;
  if (add_adl) adl = adl & add;
end

always @* begin
  adh_drv = 8'hff;                              // precharge MOSFETs
  if (|zero_adh) adh_drv = adh_drv & ~zero_adh; // open drain MOSFETs
  if (dl_adh)  adh_drv = adh_drv & dl;
  if (pch_adh) adh_drv = adh_drv & pch;
end

always @* begin
  adh = 8'hxx;
  case ({sb_db, sb_adh})
    2'b00:  adh = adh_drv;
    2'b01:  adh = adh_drv & sb_drv;
    2'b10:  adh = adh_drv;
    2'b11:  adh = adh_drv & idb_drv & sb_drv;
  endcase
end

`ifdef CONFLICTS
`include "bus_conflicts.sv"
`endif

// interrupt and reset control
always @(posedge CLK) if (cp1_posedge) begin
  resp <= !nRES;
  irqp <= !nIRQ;
end

always @(posedge CLK) if (cp2_negedge) begin
  nmip <= !nNMI;
end

always @(posedge CLK) if (cp2_posedge) begin
  resp_d1 <= resp;
end

always @(posedge CLK) if (cp1_posedge) begin
  if (resp)
    resg <= resp_d1;
  else if (rdy1 && brk_done)
    resg <= 1'b0;
end

always @(posedge CLK) if (cp1_posedge) begin
  if (nmip && nmig)
    nmil <= 1'b1;
  else if (!nmip)
    nmil <= 1'b0;
end

always @(posedge CLK) if (cp1_posedge) begin
  if (nmip && !nmil)
    nmig <= 1'b1;
  else if (rdy1 && brk_done)
    nmig <= 1'b0;
end

reg intg_pre2, intg_pre;
always @(posedge CLK) if (cp1_negedge) begin
  if ((!clock1 || op_t2_branch) && (nmig || (irqp && !`p_i)))
    intg_pre2 <= 1'b1;
  else
    intg_pre2 <= intg;
end

always @* begin // must be evaluated during cp2
  if (rdy1 && brk_done)
    intg_pre = 1'b0;
  else
    intg_pre = intg_pre2;
end

always @(posedge CLK) if (cp2_negedge)
  intg <= intg_pre;

wire intr_pre = resg || intg_pre;
wire intr = resg || intg;

// pd: predecode register
assign clear_ir = !(fetch && !intr_pre);
always @(posedge CLK) if (cp2_negedge)
  pd <= clear_ir ? 8'h00 : db;

// predecode logic
wire pd_xxx010x1 = {pd[4:2], pd[0]} == 4'b010_1;
wire pd_1xx000x0 = {pd[7], pd[4:2], pd[0]} == 5'b1_000_0;
wire pd_0xx0xx0x = {pd[7], pd[4], pd[1]} == 3'b0_0_0;
wire pd_xxxx10x0 = {pd[3:2], pd[0]} == 3'b10_0;
assign onebyte = pd_xxxx10x0;
assign twocycle = pd_xxx010x1 || pd_1xx000x0 || (onebyte && !pd_0xx0xx0x);

// timing generation logic
always @* begin
  if (resp)
    sync_pre = 1'b0;
  else if (rdy2)
    sync_pre = !clock1 || branch_dont_take;
  else
    sync_pre = SYNC;
end

always @(posedge CLK) if (cp2_negedge)
  sync_pre_reg <= sync_pre;

reg short_circuit_branch_add_hold, short_circuit_branch_add_hold_pre;
always @* begin
  if (resp)
    short_circuit_branch_add_hold_pre = 1'b0;
  else if (rdy2_d2)
    short_circuit_branch_add_hold_pre = short_circuit_branch_add;
  else
    short_circuit_branch_add_hold_pre = short_circuit_branch_add_hold;
end

always @(posedge CLK) if (cp2_negedge)
  short_circuit_branch_add_hold <= short_circuit_branch_add_hold_pre;

always @(posedge CLK) if (cp2_negedge) begin
  if (resp)
    sync0 <= 1'b0;
  else
    sync0 <= sync_pre || short_circuit_branch_add_hold_pre && rdy2;
end

always @(posedge CLK) if (cp1_posedge) begin
  SYNC <= sync0;
end

reg onebyte_d1;
always @(posedge CLK) if (cp1_posedge) if (rdy1) begin
  onebyte_d1 <= onebyte;
end

always @* begin
  ins_done = brk_done || short_circuit_idx_add || (sync_pre_reg || ins_done_pre);
end

wire clock1_next = !resp && (sync0 || !(twocycle || ins_done));
always @(posedge CLK) if (cp1_posedge) if (rdy1) begin
  clock1 <= clock1_next;
end

always @(posedge CLK) if (cp1_posedge) begin
  clock2 <= clock1 | !rdy1;
end
  
always @(posedge CLK) if (cp1_posedge) if (rdy1) begin
  t2 <= !SYNC || ins_done;
  t3 <= t2 || ins_done;
  t4 <= t3 || ins_done;
  t5 <= t4 || ins_done;
end

// ir: instruction register
always @(posedge CLK) if (cp1_posedge) if (rdy1) begin
  fetch <= sync0;
end

always @(posedge CLK) if (cp1_posedge) if (rdy1) begin
  if (resg || intg && fetch)
    ir <= 8'h00;
  else if (fetch)
    ir <= pd;
end

// dbb: data bus tri-state buffer
wire db_oe = !rw_int && cp2;
assign DB_O = dor;
    
// dor: data output register
always @(posedge CLK) if (cp1_negedge) begin
  dor <= idb;
end

// dl: input data latch
always @(posedge CLK) if (cp2_negedge) begin
  dl <= db;
end

// pc: program ("P") counter
always @(posedge CLK) if (cp1_posedge) begin
  {pch, pcl} <= {pchs, pcls} + i_pc;
end

always @* begin
  if (adl_pcl)
    pcls = adl;
  else
    pcls = pcl;

  if (adh_pch)
    pchs = adh;
  else
    pchs = pch;
end

// abl / abh: address bus low / high register
always @(posedge CLK) if (cp1_posedge_d1) begin
  if (adl_abl)
    abl <= adl;
  if (adh_abh)
    abh <= adh;
end

assign A_O[7:0] = abl;
assign A_O[15:8] = abh;
assign A_OE = aec_int;

// s: stack pointer register
// inputs / drives on cp1, updates on cp2
always @(posedge CLK) if (cp1_negedge) begin
  if (sb_s)
    s_pre <= sb;
  else /* if (s_s) */
    s_pre <= s;
end

always @(posedge CLK) if (cp2_posedge) begin
  s <= s_pre;
end

// adder inputs
always @(posedge CLK) if (cp1_negedge) begin
  case ({sb_add, zero_add})
    2'b10: ai <= sb;
    2'b01: ai <= 8'h00;
    2'b00: ;
    default: begin
      ai <= 8'hxx;
`ifdef CONFLICTS
      if ($time > 14) begin
        $display("**** %1t ps: ai conflict: {adl_add, db_add, ndb_add}=%3b", $time, {adl_add, db_add, ndb_add});
        $stop;
      end
`endif
    end
  endcase

  case ({adl_add, db_add, ndb_add})
    3'b100: bi <= adl;
    3'b010: bi <= idb;
    3'b001: bi <= ~idb;
    3'b000: ;
    default: begin
      bi <= 8'hxx;
`ifdef CONFLICTS
      if ($time > 14) begin
        $display("**** %1t ps: bi conflict: {adl_add, db_add, ndb_add}=%3b", $time, {adl_add, db_add, ndb_add});
        $stop;
      end
`endif
    end
  endcase

  addc <= one_addc;
  add_daa <= daa;
end

// cd: arthimetic logic with decimal carry lookahead
always @* begin
  // adder is split into nibbles to facilitate decimal math
  // dhc, dfc: decimal add carry, 1 when nibble goes non-decimal
  // hco, aco: final carry outputs sum arithmetic and decimal results

  // low nibble adder
  {ahc, cd_sums[3:0]} = ai[3:0] + bi[3:0] + addc;
  dhc = add_daa && (cd_sums[3:0] > 4'd9); // decimal add: low nibble carry
  hco = ahc || dhc;

  // high nibble adder
  {afc, cd_sums[7:4]} = ai[7:4] + bi[7:4] + hco;
  dfc = add_daa && (cd_sums[7:4] > 4'd9);
  aco = afc || dfc;

  // overflow: sign (bit 7) is wrong
  avo = (ai[7] == bi[7]) && (bi[7] != cd_sums[7]);

  cd_ands = ai & bi;
  cd_eors = ai ^ bi;
  cd_ors  = ai | bi;
  cd_srs  = {1'b0, bi[7:1]};
end

always @* begin
  cd = 8'hxx;
  case ({sums, ands, eors, ors, srs})
    5'b10000: cd = cd_sums;
    5'b01000: cd = cd_ands;
    5'b00100: cd = cd_eors;
    5'b00010: cd = cd_ors;
    5'b00001: cd = cd_srs;
    5'b00000: ;
    default: begin
`ifdef CONFLICTS
      if ($time > 14) begin
        $display("**** %1t ps: adder op conflict: {sums, ands, eors, ors, srs}=%5b", $time, {sums, ands, eors, ors, srs});
        $stop;
      end
`endif
    end
  endcase
end

// adder outputs
always @(posedge CLK) if (cp2_posedge) begin
  if (rdy2_d2) begin
    add <= cd;
    acr <= aco;
    avr <= avo;
    hcr <= hco;
  end
end

// x index register
always @(posedge CLK) if (cp1_posedge) begin
  if (sb_x)
    x <= sb;
end

// y index register
always @(posedge CLK) if (cp1_posedge) begin
  if (sb_y)
    y <= sb;
end

// p: processor status register
always @(posedge CLK) if (cp1_posedge) begin
  case ({db0_c, ir5_c, acr_c})
    3'b100: `p_c <= idb[0];
    3'b010: `p_c <= ir[5];
    3'b001: `p_c <= acr;
    3'b000: ;
    default: `p_c <= 1'bx;
  endcase
  
  case ({db1_z, dbz_z})
    2'b10: `p_z <= idb[1];
    2'b01: `p_z <= ~|idb;
    2'b00: ;
    default: `p_z <= 1'bx;
  endcase

  case ({db2_i, ir5_i, one_i})
    3'b100: `p_i <= idb[2];
    3'b010: `p_i <= ir[5];
    3'b001: `p_i <= 1'b1;
    3'b000: ;
    default: `p_i <= 1'bx;
  endcase

`ifdef MOS6502_DISABLE_BCD
  `p_d <= 1'b0;
`else
  case ({db3_d, ir5_d})
    2'b10: `p_d <= idb[3];
    2'b01: `p_d <= ir[5];
    2'b00: ;
    default: `p_d <= 1'bx;
  endcase
`endif

  if (one_v)
    `p_v <= 1'b1;
  else
    case ({db6_v, avr_v, zero_v})
      3'b100: `p_v <= idb[6];
      3'b010: `p_v <= avr;
      3'b001: `p_v <= 1'b0;
      3'b000: ;
      default: `p_v <= 1'bx;
    endcase

  if (db7_n)
    `p_n <= idb[7];
end

always @* `p_b = !intr;
initial pf[5] = 1'b1;

// decimal adjust adders. Each nibble is adjusted separately to form a
// valid BCD integer. In short:
// - if adding: carry if > $9. Add $6 to adjust.
// - if subtracting: borrow if no carry from higher nibble. Add $A to adjust.
// hcr reflects carry to or borrow from upper nibble.

reg daa_d2, dsa_d2;
always @(posedge CLK) if (cp2_posedge) begin
  daa_d2 <= daa;
  dsa_d2 <= dsa;
end

function [3:0] daa_adj_nibble;
input cb; // carry / not borrow
input add, sub;
begin
  casez ({add, sub, cb})
    3'b00z: daa_adj_nibble = 4'd0;
    3'b100: daa_adj_nibble = 4'd0;
    3'b101: daa_adj_nibble = 4'd6; // add carry: + 6
    3'b011: daa_adj_nibble = 4'd0;
    3'b010: daa_adj_nibble = -4'd6; // sub borrow: - 6
    default: daa_adj_nibble = 4'dx;
  endcase
end
endfunction

always @* begin
  daa_adj[3:0] = daa_adj_nibble(hcr, daa_d2, dsa_d2);
  daa_adj[7:4] = daa_adj_nibble(acr, daa_d2, dsa_d2);

  // Special case if daa_adj is 0 allows x's in sb to propagate (because, x + 0 = x).
  // Enables loading registers with partially defined data.
  daa_out[3:0] = daa_adj[3:0] == 4'd0 ? sb[3:0] : sb[3:0] + daa_adj[3:0];
  daa_out[7:4] = daa_adj[7:4] == 4'd0 ? sb[7:4] : sb[7:4] + daa_adj[7:4];
end

// ac: accumulator
always @(posedge CLK) if (cp1_posedge) begin
  if (sb_ac)
    ac <= daa_out;
end

//////////////////////////////////////////////////////////////////////
// control logic
wire [2:0] cl_zero_adl;
wire [7:0] cl_zero_adh;
wire [7:0] cl_add_sb;
wire       cl_dl_db, cl_dl_adl, cl_dl_adh, cl_adh_abh, cl_adl_abl;
wire       cl_adl_pcl, cl_i_pc, cl_pcl_db, cl_pcl_adl, cl_adh_pch, cl_pch_db;
wire       cl_pch_adh, cl_sb_adh, cl_sb_db, cl_s_adl, cl_sb_s, cl_s_sb;
wire       cl_ndb_add, cl_db_add, cl_adl_add, cl_one_addc, cl_daa, cl_dsa;
wire       cl_sums, cl_ands, cl_eors, cl_ors, cl_srs, cl_add_adl, cl_zero_add;
wire       cl_sb_add, cl_sb_ac, cl_ac_db, cl_ac_sb, cl_sb_x, cl_x_sb, cl_sb_y;
wire       cl_y_sb, cl_p_db, cl_db0_c, cl_ir5_c, cl_acr_c, cl_db1_z, cl_dbz_z;
wire       cl_db2_i, cl_ir5_i, cl_db3_d, cl_ir5_d, cl_db6_v, cl_avr_v, cl_one_v;
wire       cl_zero_v, cl_db7_n, cl_nrw, cl_one_i;

always @* begin
  // branch processor status bit selection
  branch_p = 1'bx;
  case ({!nop_branch_bit7, !nop_branch_bit6})
    2'b00: branch_p = `p_n;                     // BPL / BMI
    2'b01: branch_p = `p_v;                     // BVC / BVS
    2'b10: branch_p = `p_c;                     // BCC / BCS
    2'b11: branch_p = `p_z;                     // BNE / BEQ
  endcase
end

assign branch_p_match = ir[5];                  // branch match value
assign branch_dont_take = op_t2_branch && !(branch_p == branch_p_match);
assign short_circuit_branch_add = op_t3_branch && (acr == branch_offset_sign);

wire vec0 = op_t5_brk && rdy2;
wire addr_ready;
reg  vec0_d1;

always @(posedge CLK) if (cp2_posedge) if (rdy2) begin
  vec0_d1 <= vec0;
end

always @(posedge CLK) if (cp1_posedge) if (rdy1) begin
  vec1 <= vec0_d1;
end

always @(posedge CLK) if (cp1_posedge) begin
  acr_d1 <= acr;
end

always @(posedge CLK) if (cp1_negedge)
  branch_offset_sign <= idb[7];

always @(posedge CLK) if (cp2_posedge) begin
  if (resp)
    brk_done <= 1'b1;
  else if (rdy2)
    brk_done <= vec1;
end

wire reg_xfer = op_t0_tay || op_t0_tsx || op_t0_txs || op_t0_tax || op_t0_tya || op_t0_txa;

// Read/modify/write instruction cycles
// T-2: read
// T-1: modify
// T0: write
wire rmw_ins = op_asl_rol || op_lsr_ror_dec_inc;
reg  rmw_read, rmw_modify;
always @(posedge CLK) if (cp1_posedge) if (rdy1) begin
  rmw_read <= addr_ready && rmw_ins;
  rmw_modify <= rmw_read;
end

// Zero page / zero page indexed address cycles
//
// T2: A = OP+1: fetch BAL, input BAL+idx to ALU, drive 0,BAL to AB
wire zp_t2_idx_x = op_t2_zp_zp_idx && op_t2_idx_x_xy && !op_xy;
wire zp_t2_idx_y = op_t2_zp_zp_idx && op_t2_idx_x_xy && op_xy;
wire zp_t2_idx = zp_t2_idx_x || zp_t2_idx_y;
wire zp_t2_no_idx = op_t2_zp_zp_idx && !op_t2_idx_x_xy;
wire zp_t2 = zp_t2_no_idx || zp_t2_idx;
// T3: A = 0,BAL: drive BAL+idx to ABL
wire zp_t3 = op_t3_mem_zp_idx;

// Absolute / absolute indexed address cycles
//
// T2: A = OP+1: fetch ADL, input ADL+idx to ALU, drive OP+2 to AB
wire abs_t2_idx_x = op_t2_abs_access && op_t2_idx_x_xy && !op_xy;
wire abs_t2_idx_y = op_t2_abs_y || op_t2_abs_access && op_t2_idx_x_xy && op_xy;
wire abs_t2_idx = abs_t2_idx_x || abs_t2_idx_y;
wire abs_t2_no_idx = op_t2_abs || vec1;
wire abs_t2 = abs_t2_no_idx || abs_t2_idx;
// T3: A = OP+2: drive ADH,ADL+idx to AB, fetch ADH, input ADH+1 to ALU
wire abs_t3_idx = op_t3_abs_idx;
wire abs_t3_no_idx = op_t3_mem_abs; // also op_t3_jmp
wire abs_t3 = abs_t3_no_idx || abs_t3_idx;
// T4: A = ADH,ADL+idx: drive ADH+1 to ABH
wire abs_t4 = op_t4_abs_idx;

// Indirect JMP address cycles
//
// T2: A = OP+1: fetch IAL, input IAL to ALU, drive OP+2 to AB
// T3: A = OP+2: drive IAH,IAL to AB, fetch IAH, input IAL+1 to ALU
// T4: A = IAH,IAL: fetch ADL, drive ADL to ALU
// T5/abs T3: A = IAH,IAL+1: fetch ADH, drive ADL,ADH to AB

// (ind,x): Indexed indirect address cycles
//
// T2: A = OP+1: fetch BAL, input BAL+X to ALU, drive 0,BAL to AB
wire ind_x_t2 = op_t2_ind_x;
// T3: A = 0,BAL: drive BAL+X to ABL, input (BAL+X)+1 to ALU
wire ind_x_t3 = op_t3_ind_x;
// T4: A = 0,BAL+X: fetch ADL, drive BAL+X+1 to ABL, input ADL to ALU
wire ind_x_t4 = op_t4_ind_x;
// T5: A = 0,BAL+X+1: fetch ADH, drive ADL,ADH to AB
wire ind_x_t5 = op_t5_ind_x;

// (ind),y: Indirect indexed address cycles
//
// T2: A = OP+1: fetch IAL, input IAL+1 to ALU, drive 0,IAL to AB
wire ind_y_t2 = op_t2_ind_y;
// T3: A = 0,IAL: fetch BAL, input BAL+Y to ALU, drive IAL+1 to ABL
wire ind_y_t3 = op_t3_ind_y;
// T4: A = 0,IAL+1: fetch BAH, input BAH+1 to ALU, drive BAH,BAL+Y to AB
wire ind_y_t4 = op_t4_ind_y;
// T5: A = BAH,BAL+Y: drive BAH+1 to ABH
wire ind_y_t5 = op_t5_ind_y;

wire abs_addr_ready = abs_t3_no_idx && !op_t3_jmp || abs_t4;
wire zp_addr_ready = zp_t2_no_idx || zp_t3;
wire ind_x_addr_ready = ind_x_t5;
wire ind_y_addr_ready = ind_y_t5;
assign addr_ready = abs_addr_ready || zp_addr_ready || ind_x_addr_ready || ind_y_addr_ready;

assign short_circuit_idx_add = !acr && (x_op_t3_abs_idx || x_op_t4_ind_y) && !(op_store || rmw_ins) && rdy1;

wire vec = vec0 || vec1;

// Stack access cycles
wire sp_first;                       // $01->abh, s->adl->abl
wire sp_next;                        // add->adl->abl
wire sp_last;                        // add->sb->s
wire sp_cycle = sp_first || sp_next;

wire sp_inc;                         // adl->bi, $00->ai, 1->addc
wire sp_dec;                         // adl->bi, $FF->sb->ai, 0->addc
wire sp_hold;                        // adl->bi, $00->ai, 0->addc

// Stack push operations (pull = 0)
//
// opcode cycle first   next    last    inc     dec     hold    idb
// ------ ----- -----   ----    ----    ---     ---     ----    ---
// PHA/P  T2    1       0       0       0       1       0       ac/p
//        T0    0       0       1       0       0       0       -
//
// JSR    T2    1       0       0       0       0       1       -
//        T3    0       1       0       0       1       0       pch
//        T4    0       1       0       0       1       0       pcl
//        T5    0       0       0       0       0       0       -
//        (T5 special case: add->sb, sb->db->bi, $00->ai, 0->addc)
//        T0    0       0       1       0       0       0       -
//
// BRK    T2    1       0       0       0       1       0       pch
//        T3    0       1       0       0       1       0       pcl
//        T4    0       1       0       0       1       0       p
//        T5    0       0       1       0       0       0       -

// Stack pull operations (pull = 1)
//
// opcode cycle first   next    last    inc     dec     hold    dl
// ------ ----- -----   ----    ----    ---     ---     ----    --
// PLA/P  T2    1       0       0       1       0       0       -
//        T3    0       1       1       0       0       0       -
//        T0    0       0       0       0       0       0       ac/p
//
// RTS    T2    1       0       0       1       0       0       -
//        T3    0       1       0       1       0       0       -
//        T4    0       1       1       0       0       0       pcl
//        T5    0       0       0       0       0       0       pch
//
// RTI    T2    1       0       0       1       0       0       -
//        T3    0       1       0       1       0       0       -
//        T4    0       1       0       1       0       0       p
//        T5    0       1       1       0       0       0       pcl
//        T0    0       0       0       0       0       0       pch

assign sp_first = op_t2_stack;
assign sp_next = op_t3_stack_bit_jmp && !(op_t3_mem_abs) || op_t4_brk_jsr || op_t4_rti || op_t4_rts || op_t5_rti;
assign sp_last = op_t0_php_pha || op_t0_jsr || op_t5_brk || op_t3_stack_bit_jmp && op_push_pull || op_t4_rts || op_t5_rti;

assign sp_inc = sp_cycle && !sp_last && (op_plp_pla || op_rti_rts);
assign sp_dec = sp_cycle && !sp_last && !sp_inc && !sp_hold;
assign sp_hold = op_t2_jsr;

wire ins_done_pre_rmw = rmw_modify;
wire rmw_write_dor = rmw_read || rmw_modify;

wire sp_ab = sp_cycle;
wire vec_ab = vec;
wire addr_ab = op_t0_jmp || op_t0_jsr || op_t5_rts || op_t0_brk_rti;
wire abs_ab = abs_t3 || abs_t4 || op_t4_jmp;
wire zp_ab = zp_t2 || zp_t3;
wire ind_x_ab = ind_x_t2 || ind_x_t3 || ind_x_t4 || ind_x_t5;
wire ind_y_ab = ind_y_t2 || ind_y_t3 || ind_y_t4 || ind_y_t5;
wire rmw_ab = rmw_write_dor;
wire pc_ab = !(sp_ab || vec_ab || addr_ab || abs_ab || zp_ab || ind_x_ab || ind_y_ab || rmw_ab);

reg  pc_ab_dly, cl_pch_db_dly, op_t0_rts;
always @(posedge CLK) if (cp1_posedge) if (rdy1) begin
  pc_ab_dly <= pc_ab;
  cl_pch_db_dly <= cl_pch_db;
  op_t0_rts <= op_t5_rts;
end

reg ror_sb7_one;
always @(posedge CLK) if (cp1_posedge) begin
  ror_sb7_one <= (op_t0_shift_a || rmw_read) && op_ror && `p_c;
end

assign op_t0_ora_and_eor = op_t0_ora || op_t0_and || op_t0_eor;

assign op_t0_cmp_cpx_cpy = op_t0_cmp || op_t0_cpx_cpy_inx_iny && !op_implied;
assign op_tp_cmp_cpx_cpy = op_tp_cmp || op_tp_cpx_cpy_abs || op_tp_cpx_cpy_imm_zp;

assign pull_p = op_t0_plp || op_t4_rti;

assign ins_done_pre = addr_ready && !rmw_ins || op_t3_branch || op_t2_jmp_abs || op_t4_jmp || ins_done_pre_rmw || op_t5_jsr || op_t5_rts || op_t5_rti || op_t2_php_pha || op_t3_plp_pla;
assign write_dor = addr_ready && op_store || sp_cycle && sp_dec && !op_t2_jsr || rmw_write_dor;

// control logic outputs to datapath
assign cl_zero_adl[0] = vec0;
assign cl_zero_adl[1] = vec && resg;
assign cl_zero_adl[2] = vec && !resg && nmig;
assign cl_zero_adh[0] = zp_t2 || ind_x_t2 || ind_y_t2;
assign cl_zero_adh[7:1] = {7{sp_first || zp_t2 || ind_x_t2 || ind_y_t2 || op_t2_jsr || op_t3_jsr}};
assign cl_add_sb[6:0] = {7{!rdy2 || abs_t4 || ind_y_t5 || op_tp_inx || op_tp_dex || op_tp_iny_dey || op_branch_done || op_t3_jmp || op_tp_adc_sbc || op_tp_cmp_cpx_cpy || op_tp_ora_and_eor_adc || rmw_modify || sp_last || op_t5_jsr || op_tp_bit || op_tp_shift_a}};
assign cl_add_sb[7] = cl_add_sb[0] && !ror_sb7_one;
assign cl_dl_db = abs_t2 || abs_t3_idx || ind_x_t4 || ind_y_t3 || ind_y_t4 || ((op_t0_ldx_tax_tsx || op_t0_tay_ldy_not_idx) && !reg_xfer) || op_t0_ldy_mem || op_t0_lda || op_t2_branch || op_t4_jmp || op_t0_adc_sbc || op_t0_cmp_cpx_cpy || op_t0_ora_and_eor || rmw_read || op_t2_jsr || op_t4_rts || op_t0_bit || pull_p || op_t0_pla || op_t5_rti;
assign cl_dl_adl = zp_t2 || ind_x_t2 || ind_y_t2;
assign cl_dl_adh = abs_t3 || ind_x_t5 || ind_y_t4 || addr_ab;
assign cl_adh_abh = !op_t3_branch && ((rdy2_d2 && acr_d1 && cl_sb_adh) || (rdy2 && (op_t2 || cl_adh_pch || op_t5_ind_x || op_t3_abs_idx_ind || x_op_t4_ind_y || op_t5_rts || op_t5_jsr))) || vec0;
assign cl_adl_abl = !(abs_t4 || ind_y_t5 || rmw_ab);
assign cl_adl_pcl = op_t0_brk_rti || op_t0_jmp || pc_ab || addr_ab;
assign cl_i_pc = !intr && (pc_ab_dly && !(onebyte_d1 || op_t3_branch || op_branch_done || addr_ab) || !clock2 || op_t0_rts);
assign cl_pcl_db = cl_pch_db_dly;
assign cl_pcl_adl = pc_ab && !(op_t3_branch);
assign cl_adh_pch = op_t0_brk_rti || op_t0_jmp || pc_ab || addr_ab;
assign cl_pch_db = op_t2_brk || op_t3_jsr;
assign cl_pch_adh = pc_ab && !(op_branch_done);
assign cl_sb_adh = op_t3_branch || op_branch_done || op_t5_ind_y || op_t4_abs_idx;
assign cl_sb_db = reg_xfer || op_t0_ldx_tax_tsx || op_t0_tay_ldy_not_idx || op_t0_ldy_mem || op_t0_lda || op_tp_iny_dey || op_tp_inx || op_tp_dex || op_t2_branch || op_t3_jmp || op_from_x || op_sty_cpy_mem || op_tp_adc_sbc || op_tp_ora_and_eor_adc || rmw_read && op_asl_rol || rmw_modify || op_t2_jsr || op_t5_jsr || op_tp_cmp_cpx_cpy || op_tp_bit || op_t3_jmp || op_t0_pla || op_t0_shift_a || op_tp_shift_a;
assign cl_s_adl = sp_first || op_t0_jsr;
assign cl_sb_s = sp_last || op_t2_jsr || op_t0_txs;
assign cl_s_sb = op_t0_tsx;
assign cl_ndb_add = op_t0_cpx_inx || op_t0_cpy_iny || op_t0_adc_sbc && op_t0_sbc || op_t0_cmp || op_t3_branch && !branch_offset_sign;
assign cl_db_add = abs_t2 || abs_t3_idx || ind_x_t4 || ind_y_t3 || ind_y_t4 || ind_y_t5 || op_t0_dex || op_t0_iny_dey && !op_t0_cpy_iny || op_t3_branch && branch_offset_sign || op_t4_jmp || op_t0_adc_sbc && !op_t0_sbc || op_t0_ora_and_eor || rmw_read || op_t5_jsr || op_t4_rts || op_t0_bit || op_t3_jmp || op_t5_rti || op_t0_shift_a;
assign cl_adl_add = (sp_inc || sp_dec || sp_hold) || op_t2_branch || zp_t2 || ind_x_t2 || ind_x_t3 || ind_y_t2;
assign cl_one_addc = op_t0_cpy_iny || op_t0_cpx_inx || rmw_read && op_lsr_ror_dec_inc && op_inc_nop || (op_t0_adc_sbc && `p_c) || abs_t3_idx || abs_t3_no_idx || ind_x_t3 || ind_y_t2 || ind_y_t4 || op_t0_adc_sbc && `p_c || op_t0_cmp || (op_t0_shift_a || rmw_read) && op_rol && `p_c || sp_inc || op_t3_jmp || op_t3_branch && !branch_offset_sign;
assign cl_daa = op_t0_adc_sbc && !op_t0_sbc && `p_d;
assign cl_dsa = op_t0_sbc && `p_d;
assign cl_sums = !(cl_ands || cl_eors || cl_ors || cl_srs);
assign cl_ands = op_t0_and || op_t0_bit;
assign cl_eors = op_t0_eor;
assign cl_ors = op_t0_ora;
assign cl_srs = op_t0_shift_right_a || rmw_read && op_shift_right || op_t0_shift_right_a;
assign cl_add_adl = sp_next || abs_t3 || op_t3_branch || zp_t3 || ind_x_t3 || ind_x_t4 || ind_x_t5 || ind_y_t3 || ind_y_t4 || op_t3_jsr || addr_ab && !op_t0_jsr || op_t4_jmp || op_t0_brk_rti;
assign cl_zero_add = !cl_sb_add;
assign cl_sb_add = sp_dec || op_t0_cpx_inx || op_t0_dex || op_t0_iny_dey || (rmw_read && op_lsr_ror_dec_inc && !op_inc_nop) || op_t0_cpy_iny || op_t2_branch || op_t3_branch || abs_t2_idx || zp_t2 || ind_x_t2 || ind_y_t3 || op_t0_adc_sbc || op_t0_cmp || op_t0_ora_and_eor || op_t0_shift_a || rmw_read && op_asl_rol || op_t0_bit;
assign cl_sb_ac = op_t0_lda || op_t0_tya || op_t0_txa || op_tp_adc_sbc || op_tp_ora_and_eor_adc || op_t0_pla || op_tp_shift_a;
assign cl_ac_db = op_sta_cmp && addr_ready || op_t2_pha;
assign cl_ac_sb = op_t0_tax || op_t0_tay || op_t0_acc && !op_t0_lda || op_t0_shift_a || op_t0_bit;
assign cl_sb_x = op_t0_ldx_tax_tsx || op_tp_inx || op_tp_dex;
assign cl_x_sb = op_t0_txs || op_t0_cpx_inx || op_t0_dex || op_t0_txa || abs_t2_idx_x || zp_t2_idx_x || ind_x_t2 || addr_ready && op_from_x;
assign cl_sb_y = op_t0_tay_ldy_not_idx || op_t0_ldy_mem || op_tp_iny_dey;
assign cl_y_sb = op_t0_iny_dey || op_t0_tya || op_t0_cpy_iny || abs_t2_idx_y || zp_t2_idx_y || ind_y_t3 || addr_ready && op_sty_cpy_mem;
assign cl_p_db = op_t4_brk || op_t2_php;
assign cl_db0_c = pull_p || rmw_read && op_shift_right || op_t0_shift_right_a;
assign cl_ir5_c = op_t0_clc_sec;
assign cl_acr_c = op_tp_adc_sbc || op_tp_cmp_cpx_cpy || op_tp_asl_rol_a || rmw_modify && op_asl_rol;
assign cl_db1_z = pull_p;
assign cl_db2_i = pull_p;
assign cl_ir5_i = op_t0_cli_sei;
assign cl_db3_d = pull_p;
assign cl_ir5_d = op_t0_cld_sed;
assign cl_db6_v = pull_p || op_t0_bit;
assign cl_avr_v = op_tp_adc_sbc;
assign cl_one_v = so_negedge;
assign cl_zero_v = op_clv;
assign cl_db7_n = (reg_xfer && !op_t0_txs) || op_t0_lda || op_t0_ldx_tax_tsx || op_t0_tay_ldy_not_idx || op_t0_ldy_mem || op_tp_iny_dey || op_tp_inx || op_tp_dex || pull_p || op_tp_adc_sbc || op_tp_cmp_cpx_cpy || op_tp_ora_and_eor_adc || rmw_modify || op_t0_bit || op_tp_shift_a || op_t0_pla;
assign cl_dbz_z = cl_db7_n && !cl_db1_z || op_tp_bit;
assign cl_nrw = !(resg /*|| other stuff*/ || !write_dor);
assign cl_one_i = vec1;

//////////////////////////////////////////////////////////////////////
// Datapath control inputs
always @(posedge CLK) if (cp2_posedge) begin
  zero_adl <= cl_zero_adl;
  zero_adh <= {8{rdy2}} & cl_zero_adh;
  add_sb <= cl_add_sb;
  dl_db <= rdy2 && cl_dl_db;
  dl_adl <= rdy2 && cl_dl_adl;
  dl_adh <= rdy2 && cl_dl_adh;
  adh_abh <= cl_adh_abh;
  adl_abl <= rdy2 && cl_adl_abl;
  adl_pcl <= rdy2 && cl_adl_pcl;
  i_pc <= rdy2_d2 && cl_i_pc;
  pcl_db <= rdy2 && cl_pcl_db;
  pcl_adl <= rdy2 && cl_pcl_adl;
  adh_pch <= rdy2 && cl_adh_pch;
  pch_db <= rdy2 && cl_pch_db;
  pch_adh <= rdy2 && cl_pch_adh;
  sb_adh <= cl_sb_adh;
  sb_db <= !rdy2 || cl_sb_db;
  s_adl <= rdy2 && cl_s_adl;
  sb_s <= rdy2 && cl_sb_s;
  s_sb <= rdy2 && cl_s_sb;
  ndb_add <= rdy2 && cl_ndb_add;
  db_add <= !rdy2 || cl_db_add;
  adl_add <= rdy2 && cl_adl_add;
  one_addc <= rdy2 && cl_one_addc;
  daa <= rdy2 && cl_daa;
  dsa <= rdy2 && cl_dsa;
  sums <= rdy2 && cl_sums;
  ands <= rdy2 && cl_ands;
  eors <= rdy2 && cl_eors;
  ors <= !rdy2 || cl_ors;
  srs <= rdy2 && cl_srs;
  add_adl <= rdy2 && cl_add_adl;
  zero_add <= !rdy2 || cl_zero_add;
  sb_add <= rdy2 && cl_sb_add;
  sb_ac <= cl_sb_ac;
  ac_db <= rdy2 && cl_ac_db;
  ac_sb <= cl_ac_sb;
  sb_x <= cl_sb_x;
  x_sb <= rdy2 && cl_x_sb;
  sb_y <= cl_sb_y;
  y_sb <= rdy2 && cl_y_sb;
  p_db <= rdy2 && cl_p_db;
  db0_c <= rdy2 && cl_db0_c;
  ir5_c <= rdy2 && cl_ir5_c;
  acr_c <= cl_acr_c;
  db1_z <= rdy2 && cl_db1_z;
  db2_i <= rdy2 && cl_db2_i;
  ir5_i <= rdy2 && cl_ir5_i;
  db3_d <= rdy2 && cl_db3_d;
  ir5_d <= rdy2 && cl_ir5_d;
  db6_v <= rdy2 && cl_db6_v;
  avr_v <= cl_avr_v;
  one_v <= rdy2 && cl_one_v;
  zero_v <= rdy2 && cl_zero_v;
  db7_n <= cl_db7_n;
  dbz_z <= cl_dbz_z;
  nrw <= rdy2 && cl_nrw;
  one_i <= rdy2 && cl_one_i;
end

//////////////////////////////////////////////////////////////////////
// Register monitor interface
always @* begin
  //``REGION_REGS C64BUS_CTRL
  CPU_REG_DOUT = 8'b0;
  case (CPU_REG_SEL)            //``SUBREGS
    5'h01: CPU_REG_DOUT = pcl;
    5'h02: CPU_REG_DOUT = pch;
    5'h03: begin                //``REG PF // Processor Flags
      //CPU_REG_DOUT = pf;
      CPU_REG_DOUT[0] = `p_c;   //``FIELD C
      CPU_REG_DOUT[1] = `p_z;   //``FIELD Z
      CPU_REG_DOUT[2] = `p_i;   //``FIELD I
      CPU_REG_DOUT[3] = `p_d;   //``FIELD D
      CPU_REG_DOUT[4] = `p_b;   //``FIELD B
      CPU_REG_DOUT[6] = `p_v;   //``FIELD V
      CPU_REG_DOUT[7] = `p_n;   //``FIELD N
    end
    5'h04: CPU_REG_DOUT = s;
    5'h05: CPU_REG_DOUT = ac;
    5'h06: CPU_REG_DOUT = x;
    5'h07: CPU_REG_DOUT = y;
    5'h08: CPU_REG_DOUT = ir;
    5'h09: CPU_REG_DOUT = abl;
    5'h0a: CPU_REG_DOUT = abh;
    5'h0b: CPU_REG_DOUT = dor;
    5'h0c: begin                //``REG TS // Timing State
      CPU_REG_DOUT[0] = clock1;
      CPU_REG_DOUT[1] = clock2;
      CPU_REG_DOUT[2] = t2;
      CPU_REG_DOUT[3] = t3;
      CPU_REG_DOUT[4] = t4;
      CPU_REG_DOUT[5] = t5;
    end
    5'h0d: begin                //``REG IS // Interrupt State
      CPU_REG_DOUT[0] = resp;
      CPU_REG_DOUT[1] = resg;
      CPU_REG_DOUT[2] = nmip;
      CPU_REG_DOUT[3] = nmig;
      CPU_REG_DOUT[4] = irqp;
    end
    5'h0e: begin                //``REG PS // Pin State
      CPU_REG_DOUT[0] = rdy1;   //``FIELD RDY
      CPU_REG_DOUT[1] = rw_int; //``FIELD RW
      CPU_REG_DOUT[2] = AEC;
      CPU_REG_DOUT[3] = SYNC;
      CPU_REG_DOUT[4] = so_last; //``FIELD SO
    end
  endcase
end
