// bus driver conflict detection and reporting
initial #14 forever begin
  automatic reg err = 0;

  // adl is always isolated
  case ({|zero_adl, dl_adl, pcl_adl, s_adl, add_adl})
    5'b10000: ;
    5'b01000: ;
    5'b00100: ;
    5'b00010: ;
    5'b00001: ;
    5'b00000: ;
    default: begin
      $display("**** %1t ps: adl conflict: {|zero_adl, dl_adl, pcl_adl, s_adl, add_adl}=%5b", $time, {|zero_adl, dl_adl, pcl_adl, s_adl, add_adl});
      err = 1;
    end
  endcase

  if (!sb_adh) begin
    // adh isolated
    case ({|zero_adh, pch_adh, dl_adh})
      3'b100: ;
      3'b010: ;
      3'b001: ;
      3'b000: ;
      default: begin
        $display("**** %1t ps: sb conflict: {|zero_adh, pch_adh, dl_adh}=%3b", $time, {|zero_adh, pch_adh, dl_adh});
        err = 1;
      end
    endcase
  end

  if (!sb_db) begin
    // idb isolated
    case ({dl_db, pch_db, pcl_db, p_db, ac_db})
      5'b10000: ;
      5'b01000: ;
      5'b00100: ;
      5'b00010: ;
      5'b00001: ;
      5'b00000: ;
      default: begin
        $display("**** %1t ps: idb conflict: {dl_db, pch_db, pcl_db, p_db, ac_db}=%5b", $time, {dl_db, pch_db, pcl_db, p_db, ac_db});
        err = 1;
      end
    endcase
  end

  if (!sb_db && !sb_adh) begin
    // sb isolated
    case ({ac_sb, x_sb, y_sb, s_sb, |add_sb})
      5'b10000: ;
      5'b01000: ;
      5'b00100: ;
      5'b00010: ;
      5'b00001: ;
      5'b00000: ;
      default: begin
        $display("**** %1t ps: sb conflict: {ac_sb, x_sb, y_sb, s_sb, |add_sb}=%5b", $time, {ac_sb, x_sb, y_sb, s_sb, |add_sb});
        err = 1;
      end
    endcase
  end

  if (sb_db && !sb_adh) begin
    // sb and idb joined
    case ({dl_db, pch_db, pcl_db, p_db, ac_db, ac_sb, x_sb, y_sb, s_sb, |add_sb})
      10'b1000000000: ;
      10'b0100000000: ;
      10'b0010000000: ;
      10'b0001000000: ;
      10'b0000100000: ;
      10'b0000010000: ;
      10'b0000001000: ;
      10'b0000000100: ;
      10'b0000000010: ;
      10'b0000000001: ;
      10'b0000000000: ;
      default: begin
        $display("**** %1t ps: idb/sb conflict: {dl_db, pch_db, pcl_db, p_db, ac_db, ac_sb, x_sb, y_sb, s_sb, |add_sb}=%10b", $time, {dl_db, pch_db, pcl_db, p_db, ac_db, ac_sb, x_sb, y_sb, s_sb, |add_sb});
        err = 1;
      end
    endcase
  end

  if (!sb_db && sb_adh) begin
    // sb and adh joined
    case ({ac_sb, x_sb, y_sb, s_sb, |add_sb, |zero_adh, pch_adh, dl_adh})
      8'b10000000: ;
      8'b01000000: ;
      8'b00100000: ;
      8'b00010000: ;
      8'b00001000: ;
      8'b00000100: ;
      8'b00000010: ;
      8'b00000001: ;
      8'b00000000: ;
      default: begin
        $display("**** %1t ps: sb/adh conflict: {ac_sb, x_sb, y_sb, s_sb, |add_sb, |zero_adh, pch_adh, dl_adh}=%08b", $time, {ac_sb, x_sb, y_sb, s_sb, |add_sb, |zero_adh, pch_adh, dl_adh});
        err = 1;
      end
    endcase
  end

  if (sb_db && sb_adh) begin
    // idb, sb and adh joined
    case ({dl_db, pch_db, pcl_db, p_db, ac_db, ac_sb, x_sb, y_sb, s_sb, |add_sb, |zero_adh, pch_adh, dl_adh})
      13'b1000000000000: ;
      13'b0100000000000: ;
      13'b0010000000000: ;
      13'b0001000000000: ;
      13'b0000100000000: ;
      13'b0000010000000: ;
      13'b0000001000000: ;
      13'b0000000100000: ;
      13'b0000000010000: ;
      13'b0000000001000: ;
      13'b0000000000100: ;
      13'b0000000000010: ;
      13'b0000000000001: ;
      13'b0000000000000: ;
      default: begin
        $display("**** %1t ps: idb/sb/adh conflict: {dl_db, pch_db, pcl_db, p_db, ac_db, ac_sb, x_sb, y_sb, s_sb, |add_sb, |zero_adh, pch_adh, dl_adh}=%13b", $time, {dl_db, pch_db, pcl_db, p_db, ac_db, ac_sb, x_sb, y_sb, s_sb, |add_sb, |zero_adh, pch_adh, dl_adh});
        err = 1;
      end
    endcase
  end

  if (err)
    $stop;

  @CLK;
end
