module rp2a03_mos6502
  (
   input         nRES,                     // reset
   input         CLK,                      // master clock
   input         CP1_POSEDGE,              // clock phase 1
   input         CP1_NEGEDGE,
   input         CP2_POSEDGE,              // clock phase 2
   input         CP2_NEGEDGE,
   output reg [31:0] ICD_CYC,              // clock cycles since reset
   input         nIRQ,                     // interrupt request
   input         nNMI,                     // non-maskable interrupt
   input         AEC,                      // address enable control
   output [15:0] A_O,                      // address bus, output
   output        A_OE,                     //   output enable
   input [7:0]   DB_I,                     // data bus, input
   output [7:0]  DB_O,                     //   output data
   output        DB_OE,                    //   output enable
   output        RW,                       // read / not write
   input         RDY,                      // ready
   output reg    SYNC,                     // instruction sync
   input         SO,                       // set overflow
   input         HOLD,                     // halt and release A,D,RW
   input [4:0]   CPU_REG_SEL,              // register monitor
   output reg [7:0] CPU_REG_DOUT
   );

`define MOS6502_DISABLE_BCD
`define MOS6502_ENABLE_ICD_CYC

`include "mos6502_core.vh"

assign db = DB_I;
assign DB_OE = db_oe;

endmodule
