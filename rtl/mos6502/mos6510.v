module mos6510
  (
   input         nRES,                     // reset
   input         CLK,                      // master clock
   input         CP1_POSEDGE,              // clock phase 1
   input         CP1_NEGEDGE,
   input         CP2_POSEDGE,              // clock phase 2
   input         CP2_NEGEDGE,
   input         nIRQ,                     // interrupt request
   input         nNMI,                     // non-maskable interrupt
   input         AEC,                      // address enable control
   output [15:0] A_O,                      // address bus, output
   output        A_OE,                     //   output enable
   input [7:0]   P_I,                      // peripheral interface bus, input
   output [7:0]  P_O,                      //   output data
   output [7:0]  P_OE,                     //   output enable
   input [7:0]   DB_I,                     // data bus, input
   output [7:0]  DB_O,                     //   output data
   output        DB_OE,                    //   output enable
   output        RW,                       // read / not write
   input         RDY,                      // ready
   output reg    SYNC,                     // instruction sync
   input         SO,                       // set overflow
   input         HOLD,                     // halt and release A,D,RW
   input [4:0]   CPU_REG_SEL,              // register monitor
   output reg [7:0] CPU_REG_DOUT
   );

`include "mos6502_core.vh"

// peripheral interface bus
reg [7:0] por;                  // peripheral output register
reg [7:0] ddr;                  // data direction register
wire [7:0] ipdb;                // internal peripheral data bus

wire p_sel = A_O[15:1] == 15'b0000_0000_0000_000;
wire ipab = A_O[0];

always @(posedge CLK) if (cp2_posedge) begin
  if (resp) begin
    ddr <= 8'h00;
    por <= 8'hxx;
  end else if (!rw_int && p_sel) begin
    if (ipab == 1'b0)
      ddr <= dor;
    else /* ipab == 1'b1 */
      por <= dor;
  end
end

assign P_OE = ddr;
assign P_O = por;
assign ipdb = ipab == 1'b0 ? ddr : P_I;

assign db = p_sel ? ipdb : DB_I;
assign DB_OE = db_oe && !p_sel;

endmodule
