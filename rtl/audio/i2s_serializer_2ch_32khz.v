// Produce an I2S serial stream.
//
// Input clock MCLK = 24.576 MHz
//
// Parallel input:
//   Sample width: 16 bits
//   Samples per frame: 2
//   Frame rate (LRCLK): 32 kHz
//
// Serial output:
//   Sample width: 16 bits
//   Frame width: 2 slots = 32 bits
//   Same sample output in both slots
//   SCLK = MCLK / 16           (1.536 MHz)
//   LRCLK = MCLK / 768         (32 kHz)
//   Sample bit order: MSB first
//   All clocks 50% duty cycle
//   LRCLK, SDOUT synchronous to SCLK falling edge
//   All outputs synchronous to MCLK
//
// Format aims to be compatible with SSM2603 default mode, with 16-bit
// data-word length (8'h07[3:2] = 2'b00) in 24-bit slots.

module i2s_serializer
  (
   input        MCLK,           // CODEC master clock

   output       OCLKEN,         // final output clock enable (frame start)

   // parallel sample input
   input [15:0] PDINL,          // data input, left channel
   input [15:0] PDINR,          // data input, right channel

   // I2S output
   output reg   SCLK,           // bit clock
   output reg   LRCLK,          // frame clock / left/right, 0=left
   output reg   SDOUT           // data output
   );

reg [9:0]  cnt;
reg [15:0] holdl, holdr;
reg [15:0] shift;
reg        sclk_falling, lrclk_edge, frame_end;

//////////////////////////////////////////////////////////////////////
// Clock generator

localparam [9:0] cnt_max = 10'd768;
wire cnt_wrap = cnt == (cnt_max - 1'd1);
wire right = cnt >= (cnt_max / 2);

initial begin
  cnt = 10'h0;
end

always @(posedge MCLK) begin
  if (cnt_wrap)
    cnt <= 10'd0;
  else
    cnt[9:0] <= cnt + 1'b1;
end

always @(posedge MCLK) begin
  // All clocks fall on cnt == 0.
  SCLK <= cnt[3];               // MCLK / 16
  LRCLK <= right;               // MCLK / 768
end

always @* begin
  sclk_falling = ~|cnt[3:0];
  lrclk_edge = cnt == 0 || cnt == (cnt_max / 2);
  frame_end = cnt_wrap;
end

assign OCLKEN = frame_end;

//////////////////////////////////////////////////////////////////////
// Parallel input

// Store most recent samples when enabled.

always @(posedge MCLK) if (OCLKEN) begin
  holdl <= PDINL;
  holdr <= PDINR;
end

//////////////////////////////////////////////////////////////////////
// Serializer

wire [15:0] hold = right ? holdr : holdl;

// Sample MSB starts one SCLK after LRCLK edge.

always @(posedge MCLK) begin
  // Reload shift register on LRCLK edge.
  if (lrclk_edge)
    shift <= hold;
  else if (sclk_falling) begin
    shift[15:1] <= shift[14:0];
    shift[0] <= 1'b0;
  end
end

always @(posedge MCLK)
  if (sclk_falling)
    SDOUT <= shift[15];

endmodule
