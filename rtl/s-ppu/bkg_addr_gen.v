//////////////////////////////////////////////////////////////////////
// Background video address generator

module s_ppu_bkg_addr_gen
  #(
    parameter BGIDX=1
    )
  (
   input             CLK,
   input             CC,
   input             nCC,
   input             RES,
   input             HOLD,

   // debug monitor
   input [10:0]      MON_SEL,
   output            MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input [8:0]       COL,
   input [8:0]       ROW,

   input [7:0]       IO_D_I,
   input [51:0]      IO_WS,

   input [2:0]       BGMODE,
   input             BGSIZE, // 16x16 tiles

   input             HINC,
   input             VINC,
   input             HCLR,
   input             VCLR,

   input             OPT_EN,
   input             OPT_FETCH,
   input             OPT_OSEL,
   input             OPT_DVALID,
   input [9:0]       OPT_OFFSET,
   input [2:1]       OPT_BGNEN,
   input             OPT_HV,

   output [15:0]     SCADDR,
   output [15:0]     CHBASE,
   output [2:0]      FHS,
   output [2:0]      FVS,
   output            THO,
   output            TVO
   );

`include "reg_idx.vh"

wire mon_write_bgnsc_nba, mon_write_bgnhofs, mon_write_bgnvofs;


//////////////////////////////////////////////////////////////////////
// Register interface

reg [5:0]  scba; // SC BASE ADDRESS
reg [1:0]  scs; // SC SIZE
reg [3:0]  nba;
reg [9:0]  hofs, vofs;
reg [7:0]  ofsl;

always @(posedge CLK) begin
  if (!HOLD) begin
    if (IO_WS[REG_BG1SC]) begin
      if (BGIDX == 1) begin
        scba <= IO_D_I[7:2];
        scs <= IO_D_I[1:0];
      end
    end
    if (IO_WS[REG_BG2SC]) begin
      if (BGIDX == 2) begin
        scba <= IO_D_I[7:2];
        scs <= IO_D_I[1:0];
      end
    end
    if (IO_WS[REG_BG3SC]) begin
      if (BGIDX == 3) begin
        scba <= IO_D_I[7:2];
        scs <= IO_D_I[1:0];
      end
    end
    if (IO_WS[REG_BG4SC]) begin
      if (BGIDX == 4) begin
        scba <= IO_D_I[7:2];
        scs <= IO_D_I[1:0];
      end
    end
    if (IO_WS[REG_BG12NBA]) begin
      if (BGIDX == 1)
        nba <= IO_D_I[3:0];
      if (BGIDX == 2)
        nba <= IO_D_I[7:4];
    end
    if (IO_WS[REG_BG34NBA]) begin
      if (BGIDX == 3)
        nba <= IO_D_I[3:0];
      if (BGIDX == 4)
        nba <= IO_D_I[7:4];
    end
    if (IO_WS[REG_BG1HOFS]) begin
      if (BGIDX == 1)
        hofs <= {IO_D_I[1:0], ofsl};
    end
    if (IO_WS[REG_BG1VOFS]) begin
      if (BGIDX == 1)
        vofs <= {IO_D_I[1:0], ofsl};
    end
    if (IO_WS[REG_BG2HOFS]) begin
      if (BGIDX == 2)
        hofs <= {IO_D_I[1:0], ofsl};
    end
    if (IO_WS[REG_BG2VOFS]) begin
      if (BGIDX == 2)
        vofs <= {IO_D_I[1:0], ofsl};
    end
    if (IO_WS[REG_BG3HOFS]) begin
      if (BGIDX == 3)
        hofs <= {IO_D_I[1:0], ofsl};
    end
    if (IO_WS[REG_BG3VOFS]) begin
      if (BGIDX == 3)
        vofs <= {IO_D_I[1:0], ofsl};
    end
    if (IO_WS[REG_BG4HOFS]) begin
      if (BGIDX == 4)
        hofs <= {IO_D_I[1:0], ofsl};
    end
    if (IO_WS[REG_BG4VOFS]) begin
      if (BGIDX == 4)
        vofs <= {IO_D_I[1:0], ofsl};
    end

    if (|IO_WS[REG_BG4VOFS:REG_BG1HOFS])
      ofsl <= IO_D_I;

  end
  if (mon_write_bgnsc_nba) begin
    if (MON_WS[0]) begin
      scs <= MON_DIN[1:0];
      scba <= MON_DIN[7:2];
    end
    if (MON_WS[1])
      nba <= MON_DIN[11:8];
  end
  else if (mon_write_bgnhofs) begin
    if (MON_WS[0])
      hofs[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      hofs[9:8] <= MON_DIN[9:8];
  end
  else if (mon_write_bgnvofs) begin
    if (MON_WS[0])
      vofs[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      vofs[9:8] <= MON_DIN[9:8];
  end
end


//////////////////////////////////////////////////////////////////////
// Video address base, counter

reg [16:0] vab;                 // video address base
reg [16:0] vac;                 // video address counter

// Sub-counters of va*
localparam HS = 0, HE = 6;      // H counter
localparam VS = 7, VE = 16;     // V counter
// Sub-sub-counters (for 8x8 tiles)
localparam HTS = 0, HTE = 4;    // H tile index
localparam VTS = 10, VTE = 14;  // V tile index
localparam H = 5;               // H name table selection
localparam V = 15;              // V name table selection
localparam FVS_ = 7, FVE = 9;   // fine V scroll

reg hinc_d, vinc_d, hclr_d, vclr_d;
always @(posedge CLK) if (CC) begin
  hinc_d <= HINC;
  vinc_d <= VINC;
  hclr_d <= HCLR;
  vclr_d <= VCLR;
end

reg vab_reset;
always @(posedge CLK) if (nCC)
  vab_reset <= hclr_d;

reg [16:0] vab_d;
always @(posedge CLK) if (nCC) begin
  vab_d <= vab;
end

wire opt_bgnen = ((BGIDX == 1) & OPT_BGNEN[1]) | ((BGIDX == 2) & OPT_BGNEN[2]);

always @* begin
  vab = vab_d;

  if (vab_reset) begin
    vab[HE:HS] = hofs[9:3];
    vab[VE:VS] = vofs[9:0];
  end

  if (OPT_EN & ((BGIDX == 1) | (BGIDX == 2))) begin
    if (OPT_DVALID & opt_bgnen) begin
      if (OPT_HV)
        vab[VE:VS] = OPT_OFFSET;
      else
        vab[HE:HS] = OPT_OFFSET[9:3];
    end
  end
end

// Zero or increment H/V counter portions.
reg [16:0] vac_next;
always @* begin
  vac_next = vac;

  if (hclr_d | hinc_d)
    vac_next[HE:HS] = {1'b0, COL[8:3]};

  if (vclr_d | vinc_d)
    vac_next[VE:VS] = {1'b0, ROW};

  if ((BGIDX == 3) & OPT_EN) begin
    vac_next[VE:VS] = 0;

    if (OPT_FETCH) begin
      vac_next[VTS] = OPT_OSEL;
    end
  end
end

always @(posedge CLK) if (nCC) begin
  vac <= vac_next;
end

// Sum the video address base and counter.
reg [16:0] vasi;
always @* begin
  vasi[HE:HS] = vab[HE:HS] + vac[HE:HS];
  vasi[VE:VS] = vab[VE:VS] + vac[VE:VS];
end

// Translate sum to support 16x16 tiles.
reg [16:0] vas;
reg        tho, tvo;
always @* begin
  vas = vasi;
  tho = 0;
  tvo = 0;
  if (BGSIZE) begin
    // Shift H/V portions right by one.
    vas[HE-1:HS] = vasi[HE:HS+1];
    vas[VE-1:VTS] = vasi[VE:VTS+1];
    // Use shifted-out bits for tile offset.
    tho = vasi[HS];
    tvo = vasi[VTS];
  end
end

// Apply screen size wrapping.
reg [1:0] scvh;
always @* begin
  case (scs)                        // screen size WxH
    2'b00: scvh = 2'b00;            // 1x1
    2'b01: scvh = {1'b0, vas[H]};   // 2x1
    2'b10: scvh = {1'b0, vas[V]};   // 1x2
    2'b11: scvh = {vas[V], vas[H]}; // 2x2
    default: scvh = 2'bxx;
  endcase
end

// Add the SCBA to get the final address.
assign SCADDR = {scba, 10'b0} + {4'b0, scvh, vas[VTE:VTS], vas[HTE:HTS]};
assign CHBASE = {nba, 12'b0};
assign FHS = hofs[2:0];
assign FVS = vas[FVE:FVS_];
assign THO = tho;
assign TVO = tvo;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_ack;
reg        mon_sel_bgnsc_nba, mon_sel_bgnhofs, mon_sel_bgnvofs;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  mon_ack = 1'b0;
  mon_sel_bgnsc_nba = 1'b0;
  mon_sel_bgnhofs = 1'b0;
  mon_sel_bgnvofs = 1'b0;

  if (MON_SEL[10:5] == (6'h01 + (BGIDX - 1))) begin
    mon_ack = 1'b1;

    //``REGION_REGS EMUBUS_CTRL
    case (MON_SEL[4:2])         //``SUBREGS PPU 9'h08
      3'h0: begin               //``REG BGNSC_NBA
        mon_sel_bgnsc_nba = 1'b1;
        mon_dout[1:0] = scs;
        mon_dout[7:2] = scba;
        mon_dout[11:8] = nba;
      end
      3'h1: begin               //``REG BGNHOFS
        mon_sel_bgnhofs = 1'b1;
        mon_dout[9:0] = hofs;  //``NO_FIELD
      end
      3'h2: begin               //``REG BGNVOFS
        mon_sel_bgnvofs = 1'b1;
        mon_dout[9:0] = vofs;  //``NO_FIELD
      end
      3'h5: mon_dout[16:0] = vab;
      3'h6: mon_dout[16:0] = vac;
      3'h7: mon_dout[16:0] = vas;
      default: begin
        mon_ack = 1'b0;
      end
    endcase
  end
end

// TODO: Add ofsl

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_bgnsc_nba = mon_write & mon_sel_bgnsc_nba;
assign mon_write_bgnhofs = mon_write & mon_sel_bgnhofs;
assign mon_write_bgnvofs = mon_write & mon_sel_bgnvofs;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK && MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end

assign MON_ACK = mon_ack;

endmodule
