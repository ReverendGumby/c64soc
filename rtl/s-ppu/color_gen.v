module s_ppu_color_gen
  (
   input        CLK,
   input        CC,

   input [3:0]  BRIGHT,

   input [14:0] I_RGB,
   input        BLANK,

   output [7:0] O_R,
   output [7:0] O_G,
   output [7:0] O_B
   );

reg [7:0] bright_mul_lut [0:15];

// Brightness multiplier: round(BRIGHT / 15 * 256) as fixed-point Q0.8
initial begin
    bright_mul_lut[ 1] = 8'd17;
    bright_mul_lut[ 2] = 8'd34;
    bright_mul_lut[ 3] = 8'd51;
    bright_mul_lut[ 4] = 8'd68;
    bright_mul_lut[ 5] = 8'd85;
    bright_mul_lut[ 6] = 8'd102;
    bright_mul_lut[ 7] = 8'd119;
    bright_mul_lut[ 8] = 8'd137;
    bright_mul_lut[ 9] = 8'd154;
    bright_mul_lut[10] = 8'd171;
    bright_mul_lut[11] = 8'd188;
    bright_mul_lut[12] = 8'd205;
    bright_mul_lut[13] = 8'd222;
    bright_mul_lut[14] = 8'd239;
end

function [7:0] scale (input [4:0] x, input [3:0] bright, input blank);
reg [7:0] x8;
reg [15:0] mul16;
  begin
    x8 = {x[4:0], x[4:2]};
    mul16 = 16'bx;

    if (blank | (bright == 4'd0))
      scale = 8'd0;
    else if (bright == 4'd15)
      scale = x8;
    else begin
      mul16 = {8'b0, x8};
      mul16 = mul16 * bright_mul_lut[bright];
      scale = mul16[15:8];
    end
  end
endfunction

wire [4:0] r5 = I_RGB[4:0];
wire [4:0] g5 = I_RGB[9:5];
wire [4:0] b5 = I_RGB[14:10];

assign O_R = scale(r5, BRIGHT, BLANK);
assign O_G = scale(g5, BRIGHT, BLANK);
assign O_B = scale(b5, BRIGHT, BLANK);

endmodule
