module s_ppu_oam
  (
   input             CLK,
   input             CC,
   input             HOLD,

   input [10:0]      MON_SEL,
   output            MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input [7:0]       AL,
   output [15:0]     DL_O,
   input [15:0]      DL_I,
   input             WEL,
   input [4:0]       AH,
   output [7:0]      DH_O,
   input [7:0]       DH_I,
   input             WEH
   );

reg [15:0] rbufl, drbufl, wbufl, dwbufl;
reg [7:0]  rbufh, drbufh, wbufh, dwbufh;
reg [7:0]  ptrl, dptrl;
reg [4:0]  ptrh, dptrh;

wire       mon_write_oaml, mon_write_oamh;


//////////////////////////////////////////////////////////////////////
// OAML: 256 x 16: OBJ V/H-pos, name, color, priority, H/V flip

reg [15:0] meml [0:255];

always @* ptrl = AL;
always @* wbufl = DL_I;

always @(posedge CLK) begin
  if (!HOLD) begin
    rbufl <= meml[ptrl];
    if (WEL)
      meml[ptrl] <= wbufl;
  end
end

always @(posedge CLK) begin
  drbufl <= meml[dptrl];
  if (mon_write_oaml)
    meml[dptrl] <= dwbufl;
end

assign DL_O = rbufl;


//////////////////////////////////////////////////////////////////////
// OAMH: 32 x 8: H-pos MSB, size

reg [7:0] memh [0:31];

always @* ptrh = AH;
always @* wbufh = DH_I;

always @(posedge CLK) begin
  rbufh <= memh[ptrh];
  if (WEH)
    memh[ptrh] <= wbufh;
end

always @(posedge CLK) begin
  drbufh <= memh[dptrh];
  if (mon_write_oamh)
    memh[dptrh] <= dwbufh;
end

assign DH_O = rbufh;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
wire       mon_sel_oaml, mon_sel_oamh;
wire       mon_write;
reg        mon_ready_d;

always @* dptrl = (MON_SEL[8:1] - 8'h98);
always @* dptrh = (MON_SEL[4:0] - 5'h10);

always @* begin
  dwbufl = 16'b0;
  case (MON_WS)
    4'b0011: dwbufl = MON_DIN[15:0];
    4'b1100: dwbufl = MON_DIN[31:16];
    default: ;
  endcase
end

always @* begin
  dwbufh = 8'b0;
  case (MON_WS)
    4'b0001: dwbufh = MON_DIN[7:0];
    4'b0010: dwbufh = MON_DIN[15:8];
    4'b0100: dwbufh = MON_DIN[23:16];
    4'b1000: dwbufh = MON_DIN[31:24];
    default: ;
  endcase
end

always @* begin
  mon_dout = 32'h0;

  if (mon_sel_oaml)
    mon_dout = {drbufl, drbufl};
  else if (mon_sel_oamh)
    mon_dout = {drbufh, drbufh, drbufh, drbufh};
end

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_oaml = mon_write & mon_sel_oaml;
assign mon_write_oamh = mon_write & mon_sel_oamh;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  MON_DOUT <= mon_dout;
  mon_ready_d <= MON_READY;

  if (mon_sel_oaml | mon_sel_oamh)
    MON_VALID <= MON_READY & mon_ready_d; // need 2xCLK to read OAM
end

//``REGION_REGS EMUBUS_CTRL
//``SUBREGS PPU_MON_SEL
assign mon_sel_oaml = (MON_SEL >= 11'h130 && MON_SEL <= 11'h32F); //``REG_ARRAY OAML
assign mon_sel_oamh = (MON_SEL >= 11'h330 && MON_SEL <= 11'h34F); //``REG_ARRAY OAMH
//``REG_END

assign MON_ACK = mon_sel_oaml | mon_sel_oamh;

endmodule
