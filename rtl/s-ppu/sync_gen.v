module s_ppu_sync_gen
  (
   input       CLK,
   input       CC,
   input       HOLD,

   input [8:0] COL,
   input [8:0] ROW,

   input       BLANK_I,

   output reg  PCE,
   output reg  DE,
   output reg  HSYNC,
   output reg  VSYNC,
   output reg  BLANK_O
   );

`include "timings.vh"

reg       visible;

/* verilator lint_off UNSIGNED */
wire      visible_row = ROW >= FIRST_ROW_RENDER && ROW <= LAST_ROW_RENDER_MAX;
wire      visible_col = COL >= FIRST_COL_VISIBLE && COL <= LAST_COL_VISIBLE;
/* verilator lint_on UNSIGNED */

always @* begin
  // Enable DE for visible region...
  visible = visible_row && visible_col;
  // plus right border...
  if (visible_row)
    visible = visible || (COL >= FIRST_COL_RIGHT && COL <= LAST_COL_RIGHT);
  // plus left border.
  if (visible_row)
    visible = visible || (COL >= FIRST_COL_LEFT && COL <= LAST_COL_LEFT);
end

always @(posedge CLK) if (CC) begin
  DE <= visible;
  HSYNC <= !(COL >= FIRST_COL_HSYNC && COL <= LAST_COL_HSYNC);
  VSYNC <= !(ROW >= FIRST_ROW_VSYNC && ROW <= LAST_ROW_VSYNC);
  BLANK_O <= BLANK_I || !(visible_row && visible_col);
end

always @* PCE = CC & ~HOLD;

endmodule
