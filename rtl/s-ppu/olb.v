// OLB (Object Line Buffer) stores a full row of pixels from rendered
// sprites. Sprite rendering writes to it on row N-1, and the renderer
// reads from it on row N.

module s_ppu_olb
  (
   input             CLK,
   input             CC,
   input             nCC,
   input             HOLD,

   input [10:0]      MON_SEL,
   output            MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input [7:1]       WAL,
   input [8:0]       WDL,
   input             WEL,
   input [7:1]       WAH,
   input [8:0]       WDH,
   input             WEH,
   input [7:0]       RA,
   output [8:0]      RD,
   input             RE
   );

reg         enal, enbl, enah, enbh;
reg [6:0]   ptral, ptrah, ptrbl, ptrbh;
reg [8:0]   rbufal, rbufah;
reg [8:0]   wbufal, wbufah, wbufbl, wbufbh;
reg         wenal, wenah, wenbl, wenbh;
wire        mren;
reg         mren_d;
reg [8:0]   mrbufl, mrbufh;
wire        dren, dwen;
wire [6:0]  dptr;
wire [8:0]  dwbufl, dwbufh;
reg [8:0]   drbufl, drbufh;
reg         rsel;
reg [8:0]   rdout;

//////////////////////////////////////////////////////////////////////
// OLB: 2 x 128 x 9
//
// 4 color + 3 palette + 2 pri. = 9 bits
//
// The array is split in half to handle writing two pixels at the same
// time, satisfying the sprite renderer's bandwidth requirement. And
// each half has independent addressing and control, to support those
// two pixels spanning two adjacent addresses (i.e., the sprite's
// H-position is odd).
//
// Write port is two pix wide, read port is one pix.
//
// Our implementation infers two true dual-port block RAMs. Dual-port
// is required because each port has only one address input, and this
// module must support simultaneous read (by main renderer) and write
// (by sprite renderer) at different addresses.
//
// - Port A: Module read interface and debug monitor (multiplexed)
// - Port B: Module write interface - unique address per RAM

reg [8:0] meml [0:127];         // even pixels
reg [8:0] memh [0:127];         // odd pixels

// RE launches on CC. RD should launch on next CC. So, read between
// the two clocks, on nCC.
assign mren = nCC & RE;

always @* begin
  enal = 1'b0;
  ptral = 7'b0;
  wenal = 1'b0;
  wbufal = 9'b0;
  wbufah = 9'b0;

  if (mren) begin
    enal = 1'b1;                // HOLD is handled by mren
    ptral = RA[7:1];
    wenal = 1'b0;
    wbufal = 9'bx;
    wbufah = 9'bx;
  end
  else if (dren | dwen) begin
    enal = 1'b1;
    ptral = dptr;
    wenal = dwen;
    wbufal = dwbufl;
    wbufah = dwbufh;
  end

  enah = enal;
  ptrah = ptral;
  wenah = wenal;
end

always @* begin
  enbl = CC | nCC;
  enbh = CC | nCC;
  ptrbl = WAL[7:1];
  ptrbh = WAH[7:1];
  wbufbl = WDL;
  wbufbh = WDH;
  wenbl = WEL;
  wenbh = WEH;
end

// meml RAM port A
always @(posedge CLK) begin
  if (enal) begin
    rbufal <= meml[ptral];
    if (wenal)
      meml[ptral] <= wbufal;
  end
end

// meml RAM port B
always @(posedge CLK) begin
  if (enbl) begin
    //rbufbl <= meml[ptrbl];
    if (wenbl)
      meml[ptrbl] <= wbufbl;
  end
end

// memh RAM port A
always @(posedge CLK) begin
  if (enah) begin
    rbufah <= memh[ptrah];
    if (wenah)
      memh[ptrah] <= wbufah;
  end
end

// memh RAM port B
always @(posedge CLK) begin
  if (enbh) begin
    //rbufbh <= memh[ptrbh];
    if (wenbh)
      memh[ptrbh] <= wbufbh;
  end
end

always @(posedge CLK) begin
  mren_d <= mren;

  if (mren_d) begin
    mrbufl <= rbufal;
    mrbufh <= rbufah;
  end
  else begin
    drbufl <= rbufal;
    drbufh <= rbufah;
  end
end

always @(posedge CLK) if (CC)
  rsel <= RA[0];

always @(posedge CLK) if (CC & RE) begin
  rdout <= (rsel == 1'b1) ? mrbufl : mrbufh;
end

assign RD = rdout;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
wire       mon_sel_olba;
wire       mon_write;
wire       mon_write_olba;
reg        mon_ready_d;

assign dren = mon_sel_olba;
assign dwen = mon_write_olba;
assign dptr = (MON_SEL[8:2] - 7'h00);

assign dwbufl = MON_DIN[8:0];
assign dwbufh = MON_DIN[17:9];

always @* begin
  mon_dout = 32'h0;

  mon_dout[8:0] = drbufl;
  mon_dout[17:9] = drbufh;
end

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_olba = mon_write & mon_sel_olba;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  MON_DOUT <= mon_dout;
  mon_ready_d <= MON_READY;

  if (mon_sel_olba) begin
    MON_VALID <= MON_READY & mon_ready_d; // need 2xCLK to read OLB
  end
end

//``REGION_REGS EMUBUS_CTRL
//``SUBREGS PPU_MON_SEL
/* verilator lint_off CMPCONST */
assign mon_sel_olba = (MON_SEL >= 11'h600 && MON_SEL <= 11'h7FF); //``REG_ARRAY OLB
/* verilator lint_on CMPCONST */
//``REG_END

assign MON_ACK = mon_sel_olba;

endmodule
