//////////////////////////////////////////////////////////////////////
// Timers and Status

module s_ppu_timer_status
  (
   input             CLK,
   input             CC,
   input             nCC,
   input             RES,
   input             HOLD,

   input [10:0]      MON_SEL,
   output            MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input [8:0]       COL,
   input [8:0]       ROW,
   input             FIELD,

   input [7:0]       IO_D_I,
   output reg [7:0]  IO_D_O,
   output reg [7:0]  IO_D_OE,
   input [63:52]     IO_RE,
   input [63:52]     IO_RS,
   input [51:0]      IO_WS,

   input             RANGE_OVER,
   input             TIME_OVER,
   input             EXTLATCH
   );

`include "reg_idx.vh"

wire        slhv_re, ophct_re, opvct_re, stat77_re, stat78_re;
wire        slhv_rs, ophct_rs, opvct_rs, stat77_rs, stat78_rs;
reg [8:0]   ophct, opvct;
reg         ophct_tog, opvct_tog;
wire        opt, ope;
reg         opl, opt_d;

wire        mon_write_ophvc, mon_write_stat7x;


//////////////////////////////////////////////////////////////////////
// Register interface

assign slhv_re = IO_RE[REG_SLHV];
assign ophct_re = IO_RE[REG_OPHCT];
assign opvct_re = IO_RE[REG_OPVCT];
assign stat77_re = IO_RE[REG_STAT77];
assign stat78_re = IO_RE[REG_STAT78];

assign slhv_rs = IO_RS[REG_SLHV];
assign ophct_rs = IO_RS[REG_OPHCT];
assign opvct_rs = IO_RS[REG_OPVCT];
assign stat77_rs = IO_RS[REG_STAT77];
assign stat78_rs = IO_RS[REG_STAT78];

always @* begin
  IO_D_O = 8'bx;
  IO_D_OE = 8'b0;
  if (ophct_re) begin
    if (ophct_tog) begin
      IO_D_O[0] = ophct[8];
      IO_D_OE = 8'b1;
    end
    else begin
      IO_D_O = ophct[7:0];
      IO_D_OE = ~0;
    end
  end
  if (opvct_re) begin
    if (opvct_tog) begin
      IO_D_O[0] = opvct[8];
      IO_D_OE = 8'b1;
    end
    else begin
      IO_D_O = opvct[7:0];
      IO_D_OE = ~0;
    end
  end
  if (stat77_re) begin
    IO_D_O[7] = TIME_OVER;
    IO_D_O[6] = RANGE_OVER;
    IO_D_O[5] = 1'b0; // Master/slave
    IO_D_O[3:0] = 4'd1;
    IO_D_OE = 8'b11101111;
  end
  if (stat78_re) begin
    IO_D_O[7] = FIELD;
    IO_D_O[6] = opl;
    IO_D_O[5] = 1'b0;
    IO_D_O[4] = 1'b0; // NTSC/60 Hz
    IO_D_O[3:0] = 4'd1;
    IO_D_OE = ~0;
  end
end


//////////////////////////////////////////////////////////////////////
// H/V counter latch

assign opt = ~EXTLATCH | slhv_rs;
assign ope = opt & ~opt_d;

always @(posedge CLK) begin
  if (!HOLD)
    opt_d <= opt;
end

always @(posedge CLK) begin
  if (RES) begin
    opl <= 1'b0;
    ophct <= 9'h1FF;
    opvct <= 9'h1FF;
    ophct_tog <= 1'b0;
    opvct_tog <= 1'b0;
  end
  else if (!HOLD) begin
    if (ope) begin
      opl <= 1'b1;
      ophct <= COL;
      opvct <= ROW;
    end
    if (stat78_rs) begin
      opl <= 1'b0;
      ophct_tog <= 1'b0;
      opvct_tog <= 1'b0;
    end
    if (ophct_rs)
      ophct_tog <= ~ophct_tog;
    if (opvct_rs)
      opvct_tog <= ~opvct_tog;
  end

  if (mon_write_ophvc) begin
    if (MON_WS[0])
      ophct[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      ophct[8] <= MON_DIN[8];
    if (MON_WS[2])
      opvct[7:0] <= MON_DIN[23:16];
    if (MON_WS[3])
      opvct[8] <= MON_DIN[24];
  end
  if (mon_write_stat7x) begin
    if (MON_WS[0]) begin
      opl <= MON_DIN[3];
      // TODO: ophct_tog, opvct_tog, opt_d...
    end
  end
end


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_ack;
reg        mon_sel_ophvc, mon_sel_stat7x;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  mon_ack = 1'b1;
  mon_sel_ophvc = 1'b0;
  mon_sel_stat7x = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL[10:2])        //``SUBREGS PPU
    9'h05: begin              //``REG OPHVC
      mon_sel_ophvc = 1'b1;
      mon_dout[8:0] = ophct;
      mon_dout[24:16] = opvct;
    end
    9'h06: begin              //``REG STAT7X
      mon_sel_stat7x = 1'b1;
      mon_dout[0] = RANGE_OVER;
      mon_dout[1] = TIME_OVER;
      mon_dout[2] = FIELD;
      mon_dout[3] = opl;
    end
    default: begin
      mon_ack = 1'b0;
    end
  endcase
end

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_ophvc = mon_write & mon_sel_ophvc;
assign mon_write_stat7x = mon_write & mon_sel_stat7x;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK && MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end

assign MON_ACK = mon_ack;

endmodule
