// Tile pattern data latch and shifter, 2x
//
// They come in pairs because PDR delivers two bits at a time.

module bkg_tile_latch_shifter
  #(parameter BIDX=0)
  (
   input         CLK,
   input         CC,

   input [1:0]   BMAX,
   
   input [15:0]  PDR,

   input         SHIFT,
   input         LOAD,
   input         FETCH_PT,
   input [1:0]   PT_BSEL,

   output [15:0] TILE0,
   output [15:0] TILE1
   );

reg [15:0] tile0, tile1; // shifters
reg [7:0]  tile0_buf, tile1_buf; // latches
reg [7:0]  tile0_in, tile1_in;
wire       hflip;

always @* begin
  if (BIDX == 0 || BIDX <= BMAX) begin
    tile0_in = tile0_buf;
    tile1_in = tile1_buf;
  end
  else begin
    tile0_in = 8'b0;
    tile1_in = 8'b0;
  end
end

always @(posedge CLK) if (CC) begin
  // Shift the shifters.
  if (SHIFT) begin
    tile0 <= {1'b1, tile0[15:1]};
    tile1 <= {1'b1, tile1[15:1]};
  end

  // Buffer PT data fetches, so we can load all data simultaneously
  // after the last PT data fetch.
  if (FETCH_PT & (PT_BSEL == BIDX)) begin
    tile0_buf <= PDR[7:0];
    tile1_buf <= PDR[15:8];
  end

  // Load buffered PTs and fetched PT.
  if (LOAD) begin
    tile0[15:8] <= tile0_in;
    tile1[15:8] <= tile1_in;
  end
end

assign TILE0 = tile0;
assign TILE1 = tile1;

endmodule

