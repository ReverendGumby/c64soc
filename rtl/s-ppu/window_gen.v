// Main/Sub Screen Window Signal Generator
//
// Outputs 6x window signals: 0-3 = BG1-4, 4 = OBJ, 5 = color
//
// A high signal hides the associated layer.

module s_ppu_window_gen
  (
   input             CLK,
   input             CC,
   input             nCC,
   input             RES,
   input             HOLD,

   input [10:0]      MON_SEL,
   output            MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input [8:0]       COL,

   input [7:0]       IO_D_I,
   input [51:0]      IO_WS,

   output [5:0]      WIN
   );

`include "reg_idx.vh"

wire mon_write_win0, mon_write_win1, mon_write_win2;


//////////////////////////////////////////////////////////////////////
// Registers

reg [5:0] w1sel_inv, w2sel_inv; // "IN/OUT"
reg [5:0] w1sel_ena, w2sel_ena; // "EN"
reg [7:0] w1lpos, w1rpos, w2lpos, w2rpos;
reg [1:0] wmlog [5:0];          // "Window Logic"

always @(posedge CLK) begin
  if (RES) begin
    /* if cold reset... */
  end
  else if (!HOLD) begin
    if (IO_WS[REG_W12SEL]) begin
      w1sel_inv[0] <= IO_D_I[0];
      w1sel_ena[0] <= IO_D_I[1];
      w2sel_inv[0] <= IO_D_I[2];
      w2sel_ena[0] <= IO_D_I[3];
      w1sel_inv[1] <= IO_D_I[4];
      w1sel_ena[1] <= IO_D_I[5];
      w2sel_inv[1] <= IO_D_I[6];
      w2sel_ena[1] <= IO_D_I[7];
    end
    if (IO_WS[REG_W34SEL]) begin
      w1sel_inv[2] <= IO_D_I[0];
      w1sel_ena[2] <= IO_D_I[1];
      w2sel_inv[2] <= IO_D_I[2];
      w2sel_ena[2] <= IO_D_I[3];
      w1sel_inv[3] <= IO_D_I[4];
      w1sel_ena[3] <= IO_D_I[5];
      w2sel_inv[3] <= IO_D_I[6];
      w2sel_ena[3] <= IO_D_I[7];
    end
    if (IO_WS[REG_WOBJSEL]) begin
      w1sel_inv[4] <= IO_D_I[0];
      w1sel_ena[4] <= IO_D_I[1];
      w2sel_inv[4] <= IO_D_I[2];
      w2sel_ena[4] <= IO_D_I[3];
      w1sel_inv[5] <= IO_D_I[4];
      w1sel_ena[5] <= IO_D_I[5];
      w2sel_inv[5] <= IO_D_I[6];
      w2sel_ena[5] <= IO_D_I[7];
    end
    if (IO_WS[REG_WH0])
      w1lpos <= IO_D_I;
    if (IO_WS[REG_WH1])
      w1rpos <= IO_D_I;
    if (IO_WS[REG_WH2])
      w2lpos <= IO_D_I;
    if (IO_WS[REG_WH3])
      w2rpos <= IO_D_I;
    if (IO_WS[REG_WBGLOG]) begin
      wmlog[0] <= IO_D_I[1:0];
      wmlog[1] <= IO_D_I[3:2];
      wmlog[2] <= IO_D_I[5:4];
      wmlog[3] <= IO_D_I[7:6];
    end
    if (IO_WS[REG_WOBJLOG]) begin
      wmlog[4] <= IO_D_I[1:0];
      wmlog[5] <= IO_D_I[3:2];
    end
  end

  if (mon_write_win0) begin
    if (MON_WS[0]) begin
      w1sel_inv <= MON_DIN[5:0];
      w1sel_ena[1:0] <= MON_DIN[7:6];
    end
    if (MON_WS[1]) begin
      w1sel_ena[5:2] <= MON_DIN[11:8];
      w2sel_inv[3:0] <= MON_DIN[15:12];
    end
    if (MON_WS[2]) begin
      w2sel_inv[5:4] <= MON_DIN[17:16];
      w2sel_ena <= MON_DIN[23:18];
    end
    if (MON_WS[3])
      w1lpos <= MON_DIN[31:24];
  end
  else if (mon_write_win1) begin
    if (MON_WS[0])
      w1rpos <= MON_DIN[7:0];
    if (MON_WS[1])
      w2lpos <= MON_DIN[15:8];
    if (MON_WS[2])
      w2rpos <= MON_DIN[23:16];
    if (MON_WS[3]) begin
      wmlog[0] <= MON_DIN[25:24];
      wmlog[1] <= MON_DIN[27:26];
      wmlog[2] <= MON_DIN[29:28];
      wmlog[3] <= MON_DIN[31:30];
    end
  end
  else if (mon_write_win2) begin
    if (MON_WS[0]) begin
      wmlog[4] <= MON_DIN[1:0];
      wmlog[5] <= MON_DIN[3:2];
    end
  end
end

//////////////////////////////////////////////////////////////////////
// Position comparators

function cmp(input [8:0] col, input [7:0] lpos, input [7:0] rpos);
  cmp = (lpos <= col[7:0]) & (col[7:0] <= rpos) & ~col[8];
endfunction

wire w1cmpo = cmp(COL, w1lpos, w1rpos);
wire w2cmpo = cmp(COL, w2lpos, w2rpos);


//////////////////////////////////////////////////////////////////////
// Enable, invert, combination logic

wire [5:0] w1login, w2login;
wire [5:0] win;

assign w1login = {6{w1cmpo}} & w1sel_ena ^ w1sel_inv;
assign w2login = {6{w2cmpo}} & w2sel_ena ^ w2sel_inv;

function combine(input w1, input w2, input [1:0] mlog);
  begin
    combine = 1'bx;
    case (mlog)
      2'd0: combine = w1 | w2;
      2'd1: combine = w1 & w2;
      2'd2: combine = w1 ^ w2;
      2'd3: combine = ~(w1 ^ w2);
    endcase
  end
endfunction

genvar i;
generate
  for (i = 0; i < 6; i = i + 1) begin :combine_wins
    assign win[i] = combine(w1login[i], w2login[i], wmlog[i]);
  end
endgenerate

assign WIN = win;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_ack;
reg        mon_sel_win0, mon_sel_win1, mon_sel_win2;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  mon_ack = 1'b1;
  mon_sel_win0 = 1'b0;
  mon_sel_win1 = 1'b0;
  mon_sel_win2 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL[10:2])        //``SUBREGS PPU
    9'h38: begin              //``REG WIN0
      mon_sel_win0 = 1'b1;
      mon_dout[5:0] = w1sel_inv;
      mon_dout[11:6] = w1sel_ena;
      mon_dout[17:12] = w2sel_inv;
      mon_dout[23:18] = w2sel_ena;
      mon_dout[31:24] = w1lpos;
    end
    9'h39: begin              //``REG WIN1
      mon_sel_win1 = 1'b1;
      mon_dout[7:0] = w1rpos;
      mon_dout[15:8] = w2lpos;
      mon_dout[23:16] = w2rpos;
      mon_dout[25:24] = wmlog[0]; //``FIELD WMLOG0
      mon_dout[27:26] = wmlog[1]; //``FIELD WMLOG1
      mon_dout[29:28] = wmlog[2]; //``FIELD WMLOG2
      mon_dout[31:30] = wmlog[3]; //``FIELD WMLOG3
    end
    9'h3A: begin                 //``REG WIN2
      mon_sel_win2 = 1'b1;
      mon_dout[1:0] = wmlog[4]; //``FIELD WMLOG4
      mon_dout[3:2] = wmlog[5]; //``FIELD WMLOG5
    end
    default: mon_ack = 1'b0;
  endcase
end

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_win0 = mon_write & mon_sel_win0;
assign mon_write_win1 = mon_write & mon_sel_win1;
assign mon_write_win2 = mon_write & mon_sel_win2;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK && MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end

assign MON_ACK = mon_ack;

endmodule
