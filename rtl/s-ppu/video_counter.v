module s_ppu_video_counter
  (
   input             CLK,
   input             CC,
   input             RES,

   input [10:0]      MON_SEL,
   output            MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input             RENDER_EN,

   output reg [8:0]  COL,
   output reg [8:0]  ROW,
   output [31:0]     FRAME,
   output            FIELD,
   output reg        DOT_SKIPPED,

   output            VBL_START,
   output            VBL_END
   );

reg [31:0] frame_cnt;
reg [8:0]  last_col, last_row;
reg        end_of_row;
reg        res_d;
reg        skip_dot;
reg        odd_frame;

wire       mon_write_rowcol, mon_write_frame;

`include "timings.vh"

initial begin
  last_row = LAST_ROW;
  last_col = LAST_COL;
end

always @*
  skip_dot = RENDER_EN && odd_frame && ROW == FIRST_ROW_POST_RENDER;
always @*
  end_of_row = (COL == last_col) || (COL == last_col - 1'd1 && skip_dot);

always @(posedge CLK) begin
  if (CC) begin
    if (RES) begin
      frame_cnt <= 32'd0;
      COL <= 9'd0;
      ROW <= 9'd0;
      DOT_SKIPPED <= 1'b0;
      odd_frame <= 1'b0;
    end
    else begin
      if (end_of_row) begin
        COL <= 9'd0;
        if (ROW == last_row) begin
          ROW <= 9'd0;
          odd_frame <= ~odd_frame;
        end
        else begin
          ROW <= ROW + 1'd1;
        end
        DOT_SKIPPED <= skip_dot;
      end
      else
        COL <= COL + 1'd1;

      // TODO: Honor bg_vdir_disp?
      if (ROW == (LAST_ROW_RENDER_MIN + 1'd1) && COL == FIRST_COL_RENDER)
        frame_cnt[31:0] <= frame_cnt + 1'd1;
    end
  end

  if (mon_write_rowcol) begin
    if (MON_WS[0])
      ROW[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      ROW[8] <= MON_DIN[8];
    if (MON_WS[2])
      COL[7:0] <= MON_DIN[23:16];
    if (MON_WS[3])
      COL[8] <= MON_DIN[24];
  end
  else if (mon_write_frame) begin
    if (MON_WS[0])
      frame_cnt[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      frame_cnt[15:8] <= MON_DIN[15:8];
    if (MON_WS[2])
      frame_cnt[23:16] <= MON_DIN[23:16];
    if (MON_WS[3])
      frame_cnt[31:24] <= MON_DIN[31:24];
  end
end

// Start of vertical blanking interval: set VBL flag.
assign VBL_START = ROW == LAST_ROW_POST_RENDER && COL == 9'd0;

// End of vertical blanking interval: clear VBL flag, RES2, frame,
// spr_overflow, and spr0_hit; and copy all vertical VRAM address bits
// from latch to counter (FV, VT, V).
assign VBL_END = ROW == LAST_ROW_PRE_RENDER;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_ack;
reg        mon_sel_rowcol, mon_sel_frame;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  mon_ack = 1'b1;
  mon_sel_rowcol = 1'b0;
  mon_sel_frame = 1'b0;
  mon_dout = 32'h0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL[10:2])        //``SUBREGS PPU
    9'h00: begin              //``REG ROWCOL
      mon_sel_rowcol = 1'b1;
      mon_dout[8:0] = ROW;
      mon_dout[24:16] = COL;
    end
    9'h01: begin              //``REG FRAME
      mon_sel_frame = 1'b1;
      mon_dout = frame_cnt;
    end
    default: begin
      mon_ack = 1'b0;
    end
  endcase
end

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_rowcol = mon_write & mon_sel_rowcol;
assign mon_write_frame = mon_write & mon_sel_frame;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK && MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end

assign MON_ACK = mon_ack;

assign FIELD = odd_frame;
assign FRAME = frame_cnt;

endmodule
