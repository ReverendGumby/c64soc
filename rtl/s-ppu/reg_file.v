module s_ppu_reg_file
  (
   input             CLK,
   input             RES_COLD,
   input             RES,
   input             HOLD,

   // external I/O data port
   input [7:0]       PA,
   input             nPARD,
   input             nPAWR,

   // internal I/O data buses
   output [63:52]    IO_RE, // read enable
   output [63:52]    IO_RS, // read strobe (strobed on nPARD negedge)
   output [51:0]     IO_WS // write strobe (strobed on nPAWR negedge)
   );

reg [63:0] rsel;                // one-hot register select
wire [51:0] io_we;
reg [63:52] io_re_d;
reg [51:0]  io_we_d;

always @* begin
  rsel = 64'h00;
  if (~|PA[7:6])                // Ignore PA > 'h3F
    rsel[PA[5:0]] = 1'b1;
end

genvar a;
generate
  for (a = 0; a <= 6'h3f; a = a + 1) begin :ioa
    if (a <= 6'h33) begin
      assign io_we[a] = ~nPAWR & rsel[a];
    end
    else begin
      assign IO_RE[a] = ~nPARD & rsel[a];
    end
  end
endgenerate

always @(posedge CLK) if (!HOLD) begin
  io_re_d <= IO_RE;
  io_we_d <= io_we;
end

assign IO_RS = ~IO_RE & io_re_d;
assign IO_WS = ~io_we & io_we_d;

endmodule
