// Sprite evaluation scans OAM for visible sprites and stores their
// indicies in OIM. It runs for COL=0..255.

module s_ppu_sprite_eval
  (
   input            CLK,
   input            CC,
   input            nCC,
   input            RES,
   input            HOLD,

   input [8:0]      COL,
   input [8:0]      ROW,

   input [7:0]      IO_D_I,
   output reg [7:0] IO_D_O,
   output reg       IO_D_OE,
   input [63:52]    IO_RE,
   input [63:52]    IO_RS,
   input [51:0]     IO_WS,

   // Renderer signals
   input            VBL_END,
   input            RENDER_EN,
   input            RENDERING,

   // Sprite registers
   input [2:0]      OBJSEL_SS,

   // OAM controls
   output [7:0]     OAM_AL,
   input [15:0]     OAM_DL_I,
   output [15:0]    OAM_DL_O,
   output           OAM_WEL,
   output [4:0]     OAM_AH,
   input [7:0]      OAM_DH_I,
   output [7:0]     OAM_DH_O,
   output           OAM_WEH,

   // OIM writer controls
   output           OIM_CLR,
   input            OIM_FULL,
   output [6:0]     OIM_WD,
   output           OIM_WE,

   // Sprite evaluation status
   output reg       RANGE_OVER,

   // Sprite renderer OAM reader controls
   input [7:0]      SPREN_OAMADD,
   input            SPREN_RE,
   output [1:0]     SPREN_OAMH
   );

`include "reg_idx.vh"
`include "timings.vh"

reg [7:0]  ptrl, dptrl;
reg [4:0]  ptrh, dptrh;
reg [8:0]  oamadd;              // OAM Address register
reg [8:0]  reload;              // Last value written to REG_OAMADD*
reg        oamaddh_pri;
wire       memlsel;
reg [7:0]  lbuf;
reg        bysel;
reg [7:0]  oam_al;
reg [4:0]  oam_ah;

wire       eval_en;
reg        eval_d;

reg        arel_io;
wire       ainc_io, ainc_ev;
wire       oam_we, oam_re;


//////////////////////////////////////////////////////////////////////
// Sprite evaluation timing

assign eval_en = RENDERING;

// All signals are one CC early.
/* verilator lint_off UNSIGNED */
wire ev_start = eval_en & (COL == LAST_COL);
wire eval = ev_start | eval_en & (COL >= FIRST_COL_SPREV &
                                  COL <= LAST_COL_SPREV);
wire ev_next = eval & ~ev_start & (COL[0] != FIRST_COL_SPREV[0]);
/* verilator lint_on UNSIGNED */

assign ainc_ev = ev_next & CC;

always @(posedge CLK) if (CC) begin
  eval_d <= eval;
end


//////////////////////////////////////////////////////////////////////
// OAM access control

always @(posedge CLK) begin
  if (~HOLD) begin
    if (arel_io)
      oamadd <= reload;
    if (ainc_io)
      oamadd <= oamadd + 1'd1;
    if (ainc_ev)
      oamadd <= oamadd + 9'd2;
  end

  if (CC) begin
    if (ev_start)
      oamadd <= oamaddh_pri ? {1'b0, reload[7:1], 1'b0} : 9'd0;
  end
end

always @* begin
  oam_al = oamadd[7:0];
  oam_ah = {oamadd[3:0], bysel};
  if (eval_d) begin
    oam_ah = oamadd[7:3];
  end
  else if (SPREN_RE) begin
    oam_al = SPREN_OAMADD;
    oam_ah = SPREN_OAMADD[7:3];
  end
end

assign OAM_AL = oam_al;
assign OAM_AH = oam_ah;
assign OAM_WEL = oam_we & memlsel;
assign OAM_WEH = oam_we & ~memlsel;


//////////////////////////////////////////////////////////////////////
// Sprite renderer OAM reader controls

reg       spr_sz, spr_x8;

// OAMH packs two bits per sprite in each byte. The OAML address tells
// us which two to select.
always @* begin
  case (oam_al[2:1])
    2'b00: {spr_sz, spr_x8} = OAM_DH_I[1:0];
    2'b01: {spr_sz, spr_x8} = OAM_DH_I[3:2];
    2'b10: {spr_sz, spr_x8} = OAM_DH_I[5:4];
    2'b11: {spr_sz, spr_x8} = OAM_DH_I[7:6];
    default: {spr_sz, spr_x8} = 2'bx;
  endcase
end

assign SPREN_OAMH = {spr_sz, spr_x8};


//////////////////////////////////////////////////////////////////////
// Sprite X in-range test
//
// A sprite is "X in-range" if any pixel could appear in a visible
// column. The sprite's X position, the sprite's size (large/small),
// and the OBJSEL size width (8/16/32/64) are all considered. X values
// are signed.
//
// ((0 < OBJ.H + OBJ.W) || (0 <= OBJ.H || OBJ.H == $100))

reg [8:0] spr_w;                // pixel width - 1
reg signed [8:0] first_x, last_x;
reg       x_in_range;

always @* begin
  // Sprite's pixel width is set by OAM (spr_sz) and OBJSEL.
  casez ({spr_sz, OBJSEL_SS})
    4'b0_00z, 4'b0_010:                 spr_w = 9'd7;
    4'b0_z11, 4'b0_1z0, 4'b1_000:       spr_w = 9'd15;
    4'b0_101, 4'b1_0z1, 4'b1_11z:       spr_w = 9'd31;
    4'b1_010, 4'b1_10z:                 spr_w = 9'd63;
    default: spr_w = 9'dx;
  endcase
end

always @* begin
  // When we perform this test, the OAML/H data contains the sprite's
  // position and size.
  first_x = {spr_x8, OAM_DL_I[7:0]};
  last_x = first_x + spr_w;

  x_in_range = (9'sd0 <= first_x) | (9'sd0 <= last_x) | (first_x == 9'sh100);
end


//////////////////////////////////////////////////////////////////////
// Sprite Y in-range test
//
// A sprite is "Y in-range" if it could appear on the current row. The
// current row, the sprite's Y position, the sprite's size
// (large/small), and the OBJSEL size height (8/16/32/64) are all
// considered.

reg [7:0] spr_h;
reg [7:0] first_y, last_y;
reg       y_in_range;

always @* begin
  // Sprite's pixel height is set by OAM (spr_sz) and OBJSEL.
  casez ({spr_sz, OBJSEL_SS})
    4'b0_00z, 4'b0_010:                 spr_h = 8'd7;
    4'b0_011, 4'b0_100, 4'b1_000:       spr_h = 8'd15;
    4'b0_1z1, 4'bz_110, 4'b1_0z1:       spr_h = 8'd31;
    4'b1_010, 4'b1_10z, 4'b1_111:       spr_h = 8'd63;
    default: spr_h = 8'dx;
  endcase
end

always @* begin
  first_y = OAM_DL_I[15:8];
  last_y = first_y + spr_h;

  y_in_range = ((first_y <= ROW[7:0]) | (last_y < first_y))
    & (ROW[7:0] <= last_y);
end


//////////////////////////////////////////////////////////////////////
// OIM writer control
//
// On evaluation start, OIM is cleared. For every sprite deemed
// visible, its index (OAMADDL[7:1]) is pushed to OIM.

wire      ev_store;
wire      oim_over = ev_store & OIM_FULL; // > 32 sprites

assign ev_store = x_in_range & y_in_range & ev_next;

assign OIM_CLR = ev_start;
assign OIM_WD = oamadd[7:1];
assign OIM_WE = ev_store;


//////////////////////////////////////////////////////////////////////
// Sprite evaluation status

always @(posedge CLK) begin
  if (RES) begin
    RANGE_OVER <= 1'b0;
  end
  else if (CC) begin
    if (VBL_END & RENDER_EN)
      RANGE_OVER <= 1'b0;
    else if (oim_over)
      RANGE_OVER <= 1'b1;
  end
end


//////////////////////////////////////////////////////////////////////
// Internal I/O data bus interface

initial begin
  bysel = 0;
end

wire re_rdoam = IO_RE[REG_RDOAM];
wire rs_rdoam = IO_RS[REG_RDOAM];

wire ws_oamaddl = IO_WS[REG_OAMADDL];
wire ws_oamaddh = IO_WS[REG_OAMADDH];
wire ws_oamdata = IO_WS[REG_OAMDATA];

always @(posedge CLK) begin
  if (RES) begin
    bysel <= 0;
  end
  else if (~HOLD) begin
    if (ws_oamaddl | ws_oamaddh)
      bysel <= 1'b0;
    else if (ws_oamdata | rs_rdoam)
      bysel <= ~bysel;
  end
end

assign memlsel = ~oamadd[8];
assign oam_re = re_rdoam;
assign oam_we = ws_oamdata & (~memlsel | bysel);

always @* begin
  IO_D_O = 8'hx;
  IO_D_OE = 1'b0;

  if (oam_re) begin
    if (memlsel)
      IO_D_O = bysel ? OAM_DL_I[15:8] : OAM_DL_I[7:0];
    else
      IO_D_O = OAM_DH_I;
    IO_D_OE = 1'b1;
  end
end

assign OAM_DL_O = {IO_D_I, lbuf};
assign OAM_DH_O = IO_D_I;

always @(posedge CLK) if (~HOLD) begin
  arel_io <= 1'b0;

  if (ws_oamaddl) begin
    reload[7:0] <= IO_D_I;
    arel_io <= 1'b1;
  end
  else if (ws_oamaddh) begin
    reload[8] <= IO_D_I[0];
    oamaddh_pri <= IO_D_I[7];
    arel_io <= 1'b1;
  end
  else if (ws_oamdata) begin
    if (~bysel)
      lbuf <= IO_D_I;
  end
end

assign ainc_io = (rs_rdoam | ws_oamdata) & bysel;

endmodule
