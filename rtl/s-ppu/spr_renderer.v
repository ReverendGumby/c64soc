// Rendering draws sprites listed in OIM to the OLB. A total of 34
// 8-pixel slices may be rendered during this time.

module s_ppu_spr_renderer
  (
   input             CLK,
   input             CC,
   input             nCC,
   input             RES,
   input             HOLD,

   input [10:0]      MON_SEL,
   output            MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input [8:0]       COL,
   input [8:0]       ROW,

   input [7:0]       IO_D_I,
   input [51:0]      IO_WS,

   // Renderer signals
   input             VBL_END,
   input             RENDER_EN,
   input             RENDERING,
   input             SPR_CLR,
   input             SPR_START,
   input             SPR_END,
   input             SPR_FETCH,
   input             SPR_FETCH_PTL,

   // Sprite registers
   output [2:0]      OBJSEL_BA,
   output [1:0]      OBJSEL_N,
   output [2:0]      OBJSEL_SS,

   // VRAM bus master
   output [8:0]      CHR,
   input [15:0]      PDR,
   output [2:0]      SPR_Y,

   // OIM reader controls
   input             OIM_EMPTY,
   input [6:0]       OIM_RD,
   output            OIM_RE,

   // OAM reader controls
   output [7:0]      SPREV_OAMADD,
   output            SPREV_RE,
   input [15:0]      SPREV_OAML,
   input [1:0]       SPREV_OAMH,

   // Renderer status
   output reg        TIME_OVER,

   // OLB controls
   input [7:0]       OLB_RA,
   output [7:1]      OLB_WAL,
   output [8:0]      OLB_WDL,
   output            OLB_WEL,
   output [7:1]      OLB_WAH,
   output [8:0]      OLB_WDH,
   output            OLB_WEH
   );

`include "reg_idx.vh"
`include "swab.vh"

reg        ren_done;
reg        ren_d;
reg        ren_d2; // aligned to SPR_FETCH

reg [4:0]  spr_cnt;             // sprite slice and OAM word counter
reg [2:0]  spr_sz_w;            // # slices - 1
reg [2:0]  spr_sz_h;            // # tile rows - 1
wire       spr_next;

wire       hflip, vflip;
wire       hflip_pdr;


//////////////////////////////////////////////////////////////////////
// Register interface

reg [7:0]  objsel;
wire       mon_write_spr_ren0;

always @(posedge CLK) begin
  if (!HOLD) begin
    if (IO_WS[REG_OBSEL]) begin
      objsel <= IO_D_I;
    end
  end

  if (mon_write_spr_ren0) begin
    if (MON_WS[0])
      objsel <= MON_DIN[7:0];
  end
end

assign OBJSEL_BA = objsel[2:0];
assign OBJSEL_N = objsel[4:3];
assign OBJSEL_SS = objsel[7:5];


//////////////////////////////////////////////////////////////////////
// Sprite renderer timing

reg       ren_act;
wire      ren_start = SPR_START & ~OIM_EMPTY;
wire      ren_end = SPR_END;
wire      ren_next = ren_start | ren_act & spr_next;
wire      ren_attrh = ren_act & spr_cnt[0];
wire      ren_attrl = ren_act & ~ren_attrh;

always @(posedge CLK) if (CC) begin
  ren_act <= (ren_act | ren_start)
    & ~(RES | ren_done | ren_end);
  ren_d <= ren_act;
  ren_d2 <= ren_d;
end


//////////////////////////////////////////////////////////////////////
// Sprite evaluation status

wire ren_over = ren_act & ren_end & ~ren_done; // > 34 slices

always @(posedge CLK) begin
  if (RES) begin
    TIME_OVER <= 1'b0;
  end
  else if (CC) begin
    if (VBL_END & RENDER_EN)
      TIME_OVER <= 1'b0;
    else if (ren_over)
      TIME_OVER <= 1'b1;
  end
end


//////////////////////////////////////////////////////////////////////
// OIM reader control

// OIM_RD is valid 1x CC after OIM_RE is asserted.

assign OIM_RE = ren_next;

always @* ren_done = spr_next & OIM_EMPTY;


//////////////////////////////////////////////////////////////////////
// OAM reader control

reg [15:0]  oaml_attrl;         // H-POS, V-POS
reg [15:0]  oaml_attrh;         // NAME, COLOR, OBJ and FLIP
reg [1:0]   oamh;               // {SZ, H-POS[8]}

assign SPREV_OAMADD = {OIM_RD, ren_attrh};
assign SPREV_RE = ren_act;

always @(posedge CLK) if (CC) begin
  if (ren_attrl) begin
    oaml_attrl <= SPREV_OAML;
    oamh <= SPREV_OAMH;
  end
  if (ren_attrh) begin
    oaml_attrh <= SPREV_OAML[15:0];
  end
end


//////////////////////////////////////////////////////////////////////
// Sprite slice (tile) size counter

always @(posedge CLK) if (CC) begin
  if (~ren_act | ren_next)
    spr_cnt <= 5'd0;
  else
    spr_cnt <= spr_cnt + 1'd1;
end

// Sprite's tile width is set by OAM and OBJSEL.
always @* begin
  casez ({oamh[1], OBJSEL_SS})
    4'b0_00z, 4'b0_010:                 spr_sz_w = 3'd0;
    4'b0_z11, 4'b0_1z0, 4'b1_000:       spr_sz_w = 3'd1;
    4'b0_101, 4'b1_0z1, 4'b1_11z:       spr_sz_w = 3'd3;
    4'b1_010, 4'b1_10z:                 spr_sz_w = 3'd7;
    default: spr_sz_w = 3'dx;
  endcase
end

// Move to next sprite when counter reaches the sprite's tile width.
assign spr_next = (spr_cnt[3:1] == spr_sz_w) & ren_attrh;


//////////////////////////////////////////////////////////////////////
// Sprite tile row calculation

reg [5:0]   slice_vpos;
reg [7:0]   vpos;
wire [2:0]  voff;

// Sprite's tile height is set by OAM and OBJSEL.
always @* begin
  casez ({oamh[1], OBJSEL_SS})
    4'b0_00z, 4'b0_010:                 spr_sz_h = 3'd0;
    4'b0_011, 4'b0_100, 4'b1_000:       spr_sz_h = 3'd1;
    4'b0_1z1, 4'bz_110, 4'b1_0z1:       spr_sz_h = 3'd3;
    4'b1_010, 4'b1_10z, 4'b1_111:       spr_sz_h = 3'd7;
    default: spr_sz_h = 3'dx;
  endcase
end

assign vflip = oaml_attrh[15];

always @* begin
  slice_vpos = ROW[5:0] - vpos[5:0];
  if (vflip)
    slice_vpos = {spr_sz_h, 3'd7} - slice_vpos;
end

assign voff = slice_vpos[5:3];


//////////////////////////////////////////////////////////////////////
// Sprite slice/tile column and CHR calculation
//
// Align all inputs to VRAM bus master

reg [2:0]   spr_sz_w_d;
reg [8:0]   hpos;
reg [2:0]   slice_x;
wire [5:0]  slice_hoff;
wire [8:0]  slice_hpos;
wire [8:0]  tile_sel;
reg [3:0]   tile_hoff_p, tile_hoff;
reg [8:0]   chr;

assign hflip = oaml_attrh[14];

always @* begin
  slice_x = tile_hoff[2:0];
  if (hflip)
    slice_x = spr_sz_w_d - tile_hoff[2:0];
end

always @(posedge CLK) if (CC) begin
  spr_sz_w_d <= spr_sz_w;

  tile_hoff_p <= spr_cnt[4:1];
  tile_hoff <= tile_hoff_p;

  hpos <= {oamh[0], oaml_attrl[7:0]};
  vpos <= oaml_attrl[15:8];
end

assign slice_hoff = {slice_x, 3'b0};
assign slice_hpos = hpos + {2'b0, slice_hoff};

assign tile_sel = oaml_attrh[8:0];

assign CHR = tile_sel + {2'b0, voff, tile_hoff};
assign SPR_Y = slice_vpos[2:0];


//////////////////////////////////////////////////////////////////////
// Tile pattern data latches
//
// These select the lower 4 color bits.

reg [7:0]  tile0_buf, tile1_buf, tile2_buf, tile3_buf;
reg [7:0]  tile2_buf_d, tile3_buf_d;
reg [15:0] pdr_in;
reg        fetch_d, fetch_ptl_d;
reg        fetch_d2, fetch_ptl_d2, fetch_ptl_d3, fetch_ptl_d4;
reg        fetch_d3, fetch_d4;

always @* begin
  // Pattern data is rendered LSB first if H-FLIP is set, MSB first
  // if clear.
  if (hflip_pdr)
    pdr_in = PDR;
  else
    pdr_in = { swab(PDR[15:8]), swab(PDR[7:0]) };
end

always @(posedge CLK) if (CC) begin
  fetch_d <= ren_d2;
  fetch_ptl_d <= SPR_FETCH_PTL;
end

always @(posedge CLK) if (nCC) begin
  fetch_d2 <= ren_d2;
  fetch_d3 <= fetch_d2;
  fetch_d4 <= fetch_d3;

  fetch_ptl_d2 <= SPR_FETCH_PTL;
  fetch_ptl_d3 <= fetch_ptl_d2;
  fetch_ptl_d4 <= fetch_ptl_d3;

  // Buffer data fetches, each of which delivers 2/4bpp. PDR
  // transitions from PTL to PTH data at (nCC & fetch_ptl_d).
  if (fetch_d3) begin
    if (~fetch_ptl_d4) begin
      tile0_buf <= pdr_in[7:0];
      tile1_buf <= pdr_in[15:8];
    end
    else begin
      tile2_buf_d <= pdr_in[7:0];
      tile3_buf_d <= pdr_in[15:8];
    end
  end
end

always @* begin
  tile2_buf = tile2_buf_d;
  tile3_buf = tile3_buf_d;
  if (fetch_ptl_d4) begin
    tile2_buf = pdr_in[7:0];
    tile3_buf = pdr_in[15:8];
  end
end


//////////////////////////////////////////////////////////////////////
// Attribute data latches
//
// Latches capture the priority, upper 3 color bits, tile H/V flip,
// and H-position data (including MSB from OAMH) of each slice.

reg [14:9] attrh_buf_p2, attrh_buf_p, attrh_buf;
reg [8:0]  hpos_buf_p2, hpos_buf_p, hpos_buf;

// Align attrh_buf/hpos_buf with tile*_buf.
always @(posedge CLK) begin
  if (nCC & SPR_FETCH) begin
    if (~SPR_FETCH_PTL) begin
      attrh_buf_p2 <= oaml_attrh[14:9];
      hpos_buf_p2 <= slice_hpos[8:0];
    end
  end

  if (CC & fetch_d) begin
    if (fetch_ptl_d) begin
      attrh_buf_p <= attrh_buf_p2;
      hpos_buf_p <= hpos_buf_p2;
    end
  end

  if (nCC & fetch_d2) begin
    if (~fetch_ptl_d2) begin
      attrh_buf <= attrh_buf_p;
      hpos_buf <= hpos_buf_p;
    end
  end
end

assign hflip_pdr = attrh_buf_p2[14];


//////////////////////////////////////////////////////////////////////
// Write out an 8-pixel slice to OLB
//
// Slice = [4 color + 3 palette + 2 pri. = 9 bits] * 8 pixels
// Bandwidth: 8 pix / 2x CC
wire        write;
reg         clear;
reg [7:1]   bclra;
reg [2:0]   boff;
wire [2:0]  boffl, boffr;
wire [8:0]  xl, xr;
wire [3:0]  bcl, bcr;           // color for 2 pixels
wire        pol, por;           // pixel opacity
reg [7:1]   bal, bah;
reg [8:0]   bdl, bdh;
reg         bwel, bweh;

// Clearing happens while reading. There's only 1/2x CC between
// reading the last pixel and writing the first pixel, leaving us just
// enough time to clear the last pixel.
always @(posedge CLK) if (CC | nCC) begin
  clear <= SPR_CLR & CC;
  bclra <= OLB_RA[7:1];
end

assign write = fetch_d4;

always @(posedge CLK) if (CC | nCC) begin
  if (~write)
    boff <= 3'd0;
  else
    boff <= boff + 3'd2;
end

// Gather tile data for two adjacent pixels from the correct H pos.
assign boffl = boff + 3'd0;
assign boffr = boff + 3'd1;

assign bcl = {tile3_buf[boffl],
              tile2_buf[boffl],
              tile1_buf[boffl],
              tile0_buf[boffl]};
assign bcr = {tile3_buf[boffr],
              tile2_buf[boffr],
              tile1_buf[boffr],
              tile0_buf[boffr]};

assign pol = |bcl;
assign por = |bcr;

assign xl = hpos_buf + {5'b0, boffl};
assign xr = hpos_buf + {5'b0, boffr};

always @* begin
  bal = 7'bx;
  bah = 7'bx;
  bdl = 9'bx;
  bdh = 9'bx;
  bwel = 1'b0;
  bweh = 1'b0;

  if (clear) begin
    bal = bclra;
    bah = bclra;
    bdl = 9'b0;
    bdh = 9'b0;
    bwel = 1'b1;
    bweh = 1'b1;
  end
  else if (write) begin
    bdl[8:4] = attrh_buf[13:9];
    bdh[8:4] = attrh_buf[13:9];

    if (xl[0] == 1'b0) begin
      // left pix. is @ even X -> low OLB
      bal = xl[7:1];
      bah = xr[7:1];
      bdl[3:0] = bcl;
      bdh[3:0] = bcr;
      bwel = pol & ~xl[8];
      bweh = por & ~xr[8];
    end
    else begin
      // right pix. is @ even X -> low OLB
      bal = xr[7:1];
      bah = xl[7:1];
      bdl[3:0] = bcr;
      bdh[3:0] = bcl;
      bwel = por & ~xr[8];
      bweh = pol & ~xl[8];
    end
  end
end

assign OLB_WAL = bal;
assign OLB_WDL = bdl;
assign OLB_WEL = bwel;
assign OLB_WAH = bah;
assign OLB_WDH = bdh;
assign OLB_WEH = bweh;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_ack;
reg        mon_sel_spr_ren0;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  mon_ack = 1'b1;
  mon_sel_spr_ren0 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL[10:2])        //``SUBREGS PPU
    9'h3C: begin              //``REG SPR_REN0
      mon_sel_spr_ren0 = 1'b1;
      mon_dout[7:0] = objsel;
    end
    default: mon_ack = 1'b0;
  endcase
end

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_spr_ren0 = mon_write & mon_sel_spr_ren0;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK && MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end

assign MON_ACK = mon_ack;

endmodule
