//////////////////////////////////////////////////////////////////////
// CPU-directed VRAM read / write

module s_ppu_vram_cpu_iface
  (
   input         CLK,
   input         CC,
   input         nCC,
   input         RES,
   input         HOLD,

   input [7:0]   IO_D_I,
   input [63:52] IO_RE,
   input [63:52] IO_RS,
   input [51:0]  IO_WS,

   output [15:0] VMADD,
   output [15:0] VMDATA,
   output        READ_EN,
   output        WRITE_EN,
   output        WRITE_HI
   );

`include "reg_idx.vh"

reg [15:0] vmadd, vmadd_remap, vmadd_next;
reg [15:0] vmdata;
reg [1:0]  vmain_incmode;       // 00=+1, 01=+32, 1x=+128
reg [1:0]  vmain_remap;
reg        vmain_incadd;        // inc. after 0=L, 1=H
reg        read_lt, write_lo_lt, write_hi_lt, set_addr_lt, inc_addr_lt;
reg        inc_addr;
reg        read_en, write_lo, write_hi;

wire       read_rdvraml = IO_RS[REG_RDVRAML];
wire       read_rdvramh = IO_RS[REG_RDVRAMH];
wire       write_vmaddl = IO_WS[REG_VMADDL];
wire       write_vmaddh = IO_WS[REG_VMADDH];
wire       write_vmdatal = IO_WS[REG_VMDATAL];
wire       write_vmdatah = IO_WS[REG_VMDATAH];

wire       inc_addr_read = (read_rdvram & (vmain_incadd == read_rdvramh));
wire       inc_addr_write = (write_vmdata & (vmain_incadd == write_vmdatah));

wire       write_vmadd = write_vmaddl | write_vmaddh;
wire       write_vmdata = write_vmdatal | write_vmdatah;
wire       read_rdvram = read_rdvraml | read_rdvramh;

always @* begin
  casez (vmain_incmode)
    2'b00:  vmadd_next = vmadd + 1'd1;
    2'b01:  vmadd_next = vmadd + 16'd32;
    2'b1z:  vmadd_next = vmadd + 16'd128;
    default: vmadd_next = 16'hx;
  endcase
end

always @* begin
  case (vmain_remap)
    2'b00: vmadd_remap = vmadd; // None
    2'b01: vmadd_remap = {vmadd[15:8], vmadd[4:0], vmadd[7:5]}; // 2bpp
    2'b10: vmadd_remap = {vmadd[15:9], vmadd[5:0], vmadd[8:6]}; // 4bpp
    2'b11: vmadd_remap = {vmadd[15:10], vmadd[6:0], vmadd[9:7]}; // 8bpp
    default: vmadd_remap = 16'hx;
  endcase
end

wire read_trg = write_vmadd | inc_addr_read;
wire write_lo_trg = write_vmdatal;
wire write_hi_trg = write_vmdatah;
wire set_addr_trg = write_vmadd;
wire inc_addr_trg = inc_addr_read | inc_addr_write;

always @(posedge CLK) if (!HOLD) begin
  read_lt <= (read_lt & ~CC) | read_trg;
  write_lo_lt <= (write_lo_lt & ~CC) | write_lo_trg;
  write_hi_lt <= (write_hi_lt & ~CC) | write_hi_trg;
  set_addr_lt <= (set_addr_lt & ~CC) | set_addr_trg;
  inc_addr_lt <= (inc_addr_lt & ~CC) | inc_addr_trg;
end

always @(posedge CLK) if (CC) begin
  read_en <= read_lt;
  write_lo <= write_lo_lt;
  write_hi <= write_hi_lt;
  inc_addr <= inc_addr_lt;
end

always @(posedge CLK) if (!HOLD) begin
  if (IO_WS[REG_VMAIN]) begin
    vmain_incadd <= IO_D_I[7];
    vmain_remap <= IO_D_I[3:2];
    vmain_incmode <= IO_D_I[1:0];
  end

  if (write_vmaddl)
    vmadd[7:0] <= IO_D_I;
  else if (write_vmaddh)
    vmadd[15:8] <= IO_D_I;
  else if (CC & inc_addr & ~set_addr_lt)
    vmadd <= vmadd_next;

  if (write_vmdatal)
    vmdata[7:0] <= IO_D_I;
  if (write_vmdatah)
    vmdata[15:8] <= IO_D_I;
end

assign VMADD = vmadd_remap;
assign VMDATA = vmdata;
assign READ_EN = read_en;
assign WRITE_EN = write_lo | write_hi;
assign WRITE_HI = write_hi;

endmodule
