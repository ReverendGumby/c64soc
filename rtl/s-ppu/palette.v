module s_ppu_palette
  (
   input             CLK,
   input             CC,
   input             nCC,
   input             RES,
   input             HOLD,

   input [10:0]      MON_SEL,
   output            MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input [7:0]       IO_D_I,
   output reg [7:0]  IO_D_O,
   output reg [7:0]  IO_D_OE,
   input [63:52]     IO_RE,
   input [63:52]     IO_RS,
   input [51:0]      IO_WS,

   input             RENDERING,
   input             HBLANK,

   input [7:0]       PD,
   output reg [14:0] RGB        // 5:5:5
   );

`include "reg_idx.vh"

reg [14:0] rbuf, drbuf, wbuf, dwbuf;
reg [7:0]  ptr, dptr, cgadd;
reg        sel_cgadd;
reg [7:0]  lbuf;
reg        bysel;

wire       pal_rs, pal_ws;

wire       mon_write_cgram;


//////////////////////////////////////////////////////////////////////
// CGRAM

reg [14:0] cgram [0:255];

always @* begin
  ptr = PD;
  if (sel_cgadd)
    ptr = cgadd;
end

always @(posedge CLK) begin
  if (!HOLD) begin
    rbuf <= cgram[ptr];
    if (pal_ws)
      cgram[ptr] <= wbuf;
  end
end

always @(posedge CLK) begin
  drbuf <= cgram[dptr];
  if (mon_write_cgram)
    cgram[dptr] <= dwbuf;
end


//////////////////////////////////////////////////////////////////////
// 5:5:5 RGB output

always @(posedge CLK) if (CC | nCC) begin
  RGB <= rbuf;
end


//////////////////////////////////////////////////////////////////////
// Internal I/O data bus interface

initial begin
  bysel = 0;
end

wire re_rdcgram = IO_RE[REG_RDCGRAM];
wire rs_rdcgram = IO_RS[REG_RDCGRAM];
wire ws_cgadd = IO_WS[REG_CGADD];
wire ws_cgdata = IO_WS[REG_CGDATA];

always @(posedge CLK) begin
  if (RES) begin
    bysel <= 0;
  end
  else if (!HOLD) begin
    if (ws_cgadd)
      bysel <= 1'b0;
    else if (ws_cgdata | rs_rdcgram)
      bysel <= ~bysel;
  end
end

assign pal_rs = rs_rdcgram & bysel;
assign pal_ws = ws_cgdata & bysel;

always @* begin
  IO_D_O = 8'hx;
  IO_D_OE = 8'b0;

  if (re_rdcgram) begin
    IO_D_O = bysel ? {1'b0, rbuf[14:8]} : rbuf[7:0];
    IO_D_OE = bysel ? 8'h7f : 8'hff;
  end
end

always @*
  wbuf = {IO_D_I[6:0], lbuf};

always @(posedge CLK) if (!HOLD) begin
  if (ws_cgadd)
    cgadd <= IO_D_I;
  else if (ws_cgdata) begin
    if (~bysel)
      lbuf <= IO_D_I;
  end

  if (pal_rs | pal_ws) begin
    cgadd <= cgadd + 1'd1;
  end
end

// Ensure visible pixel color lookup does not overlap with I/O access.
always @(posedge CLK) if (CC)
  sel_cgadd <= ~RENDERING | HBLANK;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
wire       mon_sel_cgram;
wire       mon_write;
reg        mon_ready_d;

// Array spans MON_SEL[10:0] == 11'h300 .. 4FF
always @*
  dptr = MON_SEL[8:1] - 8'h00;

always @* begin
  dwbuf = 15'b0;
  case (MON_WS)
    4'b0011: dwbuf = MON_DIN[14:0];
    4'b1100: dwbuf = MON_DIN[30:16];
    default: ;
  endcase
end

always @* begin
  mon_dout = 32'h0;

  if (mon_sel_cgram)
    mon_dout = {1'b0, drbuf, 1'b0, drbuf};
end

// TODO: Add cgadd, lbuf, bysel

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_cgram = mon_write & mon_sel_cgram;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  MON_DOUT <= mon_dout;
  mon_ready_d <= MON_READY;

  if (mon_sel_cgram)
    MON_VALID <= MON_READY & mon_ready_d; // need 2xCLK to read CGRAM
end

//``REGION_REGS EMUBUS_CTRL
//``SUBREGS PPU_MON_SEL
assign mon_sel_cgram = (MON_SEL >= 11'h400 && MON_SEL <= 11'h5FF); //``REG_ARRAY CGRAM
//``REG_END

assign MON_ACK = mon_sel_cgram;

endmodule
