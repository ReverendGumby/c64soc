// CT, title screen, pendulum

initial begin
  $readmemh("vram-mode7-5-dmp.hex", vram.mem);
  $readmemh("pal-mode7-5-dmp.hex", init_pal);
  $readmemh("oam-mode7-5-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'h02);
  set_reg(REG_BGMODE, 8'h07);
  set_reg2x(REG_M7HOFS, 16'h1F40);
  set_reg2x(REG_M7VOFS, 16'h00E5);
  set_reg(REG_M7SEL, 8'h80);
  set_reg2x(REG_M7A, 16'h0078);
  set_reg2x(REG_M7B, 16'h0040);
  set_reg2x(REG_M7C, 16'hFFC0);
  set_reg2x(REG_M7D, 16'h0078);
  set_reg2x(REG_M7X, 16'h0020);
  set_reg2x(REG_M7Y, 16'h1FC0);
  set_reg(REG_W12SEL, 8'h00);
  set_reg(REG_W34SEL, 8'h00);
  set_reg(REG_WOBJSEL, 8'h00);
  set_reg(REG_WH0, 8'h00);
  set_reg(REG_WH1, 8'h00);
  set_reg(REG_WH2, 8'h00);
  set_reg(REG_WH3, 8'h00);
  set_reg(REG_WBGLOG, 8'h00);
  set_reg(REG_WOBJLOG, 8'h00);
  set_reg(REG_TM, 8'h10);
  set_reg(REG_TS, 8'h01);
  set_reg(REG_TMW, 8'h00);
  set_reg(REG_TSW, 8'h00);
  set_reg(REG_CGWSEL, 8'h02);
  set_reg(REG_CGADSUB, 8'h21);
  set_reg(REG_COLDATA, 8'hE0);
  set_reg(REG_SETINI, 8'h00);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h00);
endtask
