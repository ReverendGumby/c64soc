// SNES Test Program, "princess flipping"

initial begin
  $readmemh("vram-mode0-5-dmp.hex", vram.mem);
  $readmemh("pal-mode0-5-dmp.hex", init_pal);
  $readmemh("oam-mode0-5-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'hA3);
  set_reg(REG_BGMODE, 8'h00);
  set_reg(REG_BG1SC, 8'h23);
  set_reg(REG_BG2SC, 8'h32);
  set_reg(REG_BG3SC, 8'h42);
  set_reg(REG_BG4SC, 8'h53);
  set_reg(REG_BG12NBA, 8'h00);
  set_reg(REG_BG34NBA, 8'h00);
  set_reg2x(REG_BG1HOFS, 16'h0300);
  set_reg2x(REG_BG1VOFS, 16'h0000);
  set_reg(REG_TM, 8'h11);
  set_reg(REG_CGWSEL, 8'h02);
  set_reg(REG_CGADSUB, 8'h20);
  set_reg(REG_COLDATA, 8'hFF);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h80);
endtask
