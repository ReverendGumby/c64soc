reg [7:0]   a;
reg [7:0]   d_i;
wire [7:0]  d_o;
reg         res, clk;
reg         npard, npawr;

initial begin
  clk = 1;
  res = 1;
  npard = 1;
  npawr = 1;
end

always #0.5 begin :clkgen
  clk = !clk;
end

//////////////////////////////////////////////////////////////////////

reg [7:0] bd_o;
reg [10:0] mon_sel;
reg       mon_ready;

wire [15:0] vaa, vab;
wire [7:0]  vda_i, vda_o, vdb_i, vdb_o;
wire        pce, de, hs, vs;
wire [7:0]  ppu_r, ppu_g, ppu_b;
wire [31:0] mon_dout;
wire        mon_valid;
wire        hblank;

s_ppu ppu
  (
   .CLK_RESET(1'b0),
   .CLK(clk),
   .CLKEN(1'b1),
   .RES_COLD(1'b1),
   .RESET(res),
   .HOLD(1'b0),

   .MON_SEL(mon_sel),
   .MON_READY(mon_ready),
   .MON_DOUT(mon_dout),
   .MON_VALID(mon_valid),

   .HBLANK(hblank),
   .VBLANK(),
   .EXTLATCH(1'b1),

   .PA(a),
   .D_I(d_i),
   .D_O(d_o),
   .D_OE(),
   .nPARD(npard),
   .nPAWR(npawr),

   .VAA(vaa),
   .VDA_I(vda_i),
   .VDA_O(vda_o),
   .nVARD(nvard),
   .nVAWR(nvawr),

   .VAB(vab),
   .VDB_I(vdb_i),
   .VDB_O(vdb_o),
   .nVBRD(nvbrd),
   .nVBWR(nvbwr),

   .PCE(pce),
   .DE(de),
   .HS(hs),
   .VS(vs),
   .R(ppu_r),
   .G(ppu_g),
   .B(ppu_b)
  );

sram_32k_16_split vram
  (
   .CLK(clk),
   .CLKEN(1'b1),
   .nCE((nvard & nvbrd) & (nvawr & nvbwr)),
   .nOE((nvard & nvbrd)),
   .nWEA(nvawr),
   .nWEB(nvbwr),
   .AA(vaa[14:0]),
   .DA_I(vda_o),
   .DA_O(vda_i),
   .DA_OE(),
   .AB(vab[14:0]),
   .DB_I(vdb_o),
   .DB_O(vdb_i),
   .DB_OE()
   );

initial begin
  mon_sel = 11'h000;
  mon_ready = 1'b0;
end

//////////////////////////////////////////////////////////////////////

`include "../reg_idx.vh"

task set_reg(input [5:0] r, input [7:0] v);
  begin
    #6 a <= r;
    d_i <= v;
    #3 npawr <= 1'b0;
    #3 npawr <= 1'b1;
  end
endtask

task set_reg16(input [5:0] r, input [15:0] v);
  begin
    #6 a <= r;
    d_i <= v[7:0];
    #3 npawr <= 1'b0;
    #3 npawr <= 1'b1;
    a <= r + 1;
    d_i <= v[15:8];
    #3 npawr <= 1'b0;
    #3 npawr <= 1'b1;
  end
endtask

task set_reg2x(input [5:0] r, input [15:0] v);
  begin
    #6 a <= r;
    d_i <= v[7:0];
    #3 npawr <= 1'b0;
    #3 npawr <= 1'b1;
    d_i <= v[15:8];
    #3 npawr <= 1'b0;
    #3 npawr <= 1'b1;
  end
endtask

task get_reg(input [5:0] r, input [7:0] v, input [7:0] m = 8'hff);
  begin
    #6 a <= r;
    d_i <= 8'hzz;
    #3 npard <= 1'b0;
    #3 npard <= 1'b1;
    assert ((d_o & m) == v);
  end
endtask

task get_reg16(input [5:0] r, input [15:0] v, input [15:0] m = 16'hffff);
  begin
  reg [15:0] d;
    #6 a <= r;
    d_i <= 8'hzz;
    #3 npard <= 1'b0;
    #3 npard <= 1'b1;
    a <= r + 1;
    d[7:0] = d_o;
    #3 npard <= 1'b0;
    #3 npard <= 1'b1;
    d[15:8] = d_o;
    assert ((d & m) == v);
  end
endtask

task get_reg2x(input [5:0] r, input [15:0] v, input [15:0] m = 16'hffff);
  begin
  reg [15:0] d;
    #6 a <= r;
    d_i <= 8'hzz;
    #3 npard <= 1'b0;
    #3 npard <= 1'b1;
    d[7:0] = d_o;
    #3 npard <= 1'b0;
    #3 npard <= 1'b1;
    d[15:8] = d_o;
    assert ((d & m) == v);
  end
endtask

task read_reg(input [5:0] r, output [7:0] o);
  begin
    #6 a <= r;
    d_i <= 8'hzz;
    #3 npard <= 1'b0;
    #3 npard <= 1'b1;
    o = d_o;
  end
endtask

task dma_set_reg(input [5:0] r, input [7:0] v);
  begin
    a <= r;
    d_i <= v;
    #4 npawr <= 1'b0;
    #4 npawr <= 1'b1;
  end
endtask

task clear_regs();
  // Skip REG_INIDISP; clearing it will enable rendering.
  set_reg(REG_OBSEL, 8'h00);
  for (int r = REG_BGMODE; r <= REG_BG34NBA; r++)
    set_reg(r, 8'h00);
  for (int r = REG_BG1HOFS; r <= REG_BG4VOFS; r++)
    set_reg2x(r, 16'h0000);
  set_reg(REG_M7SEL, 8'h00);
  for (int r = REG_M7A; r <= REG_M7Y; r++)
    set_reg2x(r, 16'h0000);
  for (int r = REG_W12SEL; r <= REG_SETINI; r++)
    set_reg(r, 8'h00);
  set_reg(REG_COLDATA, 8'hE0); // clr_fixed <= 0
endtask

//////////////////////////////////////////////////////////////////////

reg [14:0] init_pal [0:255];

task load_palette;
  begin
    set_reg(REG_CGADD, 8'h00);
    for (integer i = 0; i < 256; i ++) begin
    reg [14:0] p;
      p = init_pal[i];
      set_reg(REG_CGDATA, p[7:0]);
      set_reg(REG_CGDATA, {1'b0, p[14:8]});
    end
  end
endtask

task dump_palette;
  begin
    for (integer i = 0; i < 256; i++)
      $display("dump_palette: (%d) = 14'h%x", i, ppu.palette.cgram[i]);
  end
endtask

/* -----\/----- EXCLUDED -----\/-----
task dump_oam;
  for (integer p = 0; p < 256; p++)
    $display("dump_oam: (8'h%02x) = 8'h%02x", p[7:0], ppu.oam.pri[p]);
endtask

task dump_oam_sec;
  for (integer p = 0; p < 32; p++)
    $display("dump_oam_sec: (8'h%02x) = 8'h%02x", p[7:0], ppu.oam.sec[p]);
endtask
 -----/\----- EXCLUDED -----/\----- */

//////////////////////////////////////////////////////////////////////

reg [7:0] init_oam [0:543];

task load_oam;
  begin
    set_reg(REG_OAMADDL, 8'h00);
    set_reg(REG_OAMADDH, 8'h00);
    for (integer i = 0; i < 544; i ++) begin
      set_reg(REG_OAMDATA, init_oam[i]);
    end
  end
endtask

//////////////////////////////////////////////////////////////////////

`ifdef DUMP_PIC_HEX
integer fpic, pice;
initial begin
  fpic = $fopen(`DUMP_PIC_HEX, "w");
  pice = 0;
end
always @(posedge clk) begin
  if (pce && de) begin
    if (pice) begin
      pice = 0;
      $fwrite(fpic, "\n");
    end
    $fwrite(fpic, "%6x", {ppu_r, ppu_g, ppu_b});
  end
  if (!hs)
    pice = 1;
end
final
  $fclose(fpic);
`endif
