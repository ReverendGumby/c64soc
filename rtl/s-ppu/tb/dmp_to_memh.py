import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--count', type=int)
parser.add_argument('--width', type=int, default=2)
parser.add_argument('--seek', type=int)
args = parser.parse_args()

if args.seek:
    sys.stdin.buffer.read(args.seek)

data = []
while True:
    w = sys.stdin.buffer.read(args.width)
    if len(w) < args.width:
        break
    v = 0
    for i in range(args.width):
        v = v | (w[i] << (i * 8))
    data = data + [v]
    if args.count and len(data) == args.count:
        break

for w in data:
    print("{0:x}".format(w))
