initial begin
  $readmemh("vram-mode0-0-dmp.hex", vram.mem);
  $readmemh("pal-mode0-0-dmp.hex", init_pal);
end

task load_regs();
  set_reg(REG_BGMODE, 8'h00);
  set_reg(REG_BG1SC, 8'h61);
  set_reg(REG_BG2SC, 8'h70);
  set_reg(REG_BG3SC, 8'h60);
  set_reg(REG_BG4SC, 8'h60);
  set_reg(REG_BG12NBA, 8'h00);
  set_reg(REG_TM, 8'h03);
endtask
