// SMW, #2 Morton's Castle, Morton spinning/fading away

initial begin
  $readmemh("vram-mode7-4-dmp.hex", vram.mem);
  $readmemh("pal-mode7-4-dmp.hex", init_pal);
  $readmemh("oam-mode7-4-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'h03);
  set_reg(REG_BGMODE, 8'h07);
  set_reg2x(REG_M7HOFS, 16'h00A2);
  set_reg2x(REG_M7VOFS, 16'h0090);
  set_reg(REG_M7SEL, 8'h80);
  set_reg2x(REG_M7A, 16'h02EA);
  set_reg2x(REG_M7B, 16'hFFB7);
  set_reg2x(REG_M7C, 16'h0052);
  set_reg2x(REG_M7D, 16'h0349);
  set_reg2x(REG_M7X, 16'h0100);
  set_reg2x(REG_M7Y, 16'h0120);
  set_reg(REG_W12SEL, 8'h02);
  set_reg(REG_W34SEL, 8'h00);
  set_reg(REG_WOBJSEL, 8'h32);
  set_reg(REG_WH0, 8'hFF);
  set_reg(REG_WH1, 8'h00);
  set_reg(REG_WH2, 8'h00);
  set_reg(REG_WH3, 8'h00);
  set_reg(REG_WBGLOG, 8'h00);
  set_reg(REG_WOBJLOG, 8'h00);
  set_reg(REG_TM, 8'h15);
  set_reg(REG_TS, 8'h00);
  set_reg(REG_TMW, 8'h11);
  set_reg(REG_TSW, 8'h00);
  set_reg(REG_CGWSEL, 8'h20);
  set_reg(REG_CGADSUB, 8'h20);
  set_reg(REG_COLDATA, 8'hC0);
  set_reg(REG_COLDATA, 8'h37);
  set_reg(REG_SETINI, 8'h00);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h00);
endtask
