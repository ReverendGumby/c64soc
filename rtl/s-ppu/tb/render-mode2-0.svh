// SNES Test Program, "two distinct windows"

reg hdma_en = 0;

initial begin
  $readmemh("vram-mode2-0-dmp.hex", vram.mem);
  $readmemh("pal-mode2-0-dmp.hex", init_pal);
  $readmemh("oam-mode2-0-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'h02);
  set_reg(REG_BGMODE, 8'h02);
  set_reg(REG_BG1SC, 8'h60);
  set_reg(REG_BG2SC, 8'h68);
  set_reg(REG_BG12NBA, 8'h00);
  set_reg2x(REG_BG1HOFS, 16'h00FF);
  set_reg2x(REG_BG1VOFS, 16'h0000);
  set_reg2x(REG_BG2HOFS, 16'h0149);
  set_reg2x(REG_BG2VOFS, 16'h0000);
  set_reg(REG_W12SEL, 8'h0A);
  set_reg(REG_WOBJSEL, 8'h00);
  set_reg(REG_WH0, 8'h01);
  set_reg(REG_WH1, 8'h00);
  set_reg(REG_WH2, 8'h01);
  set_reg(REG_WH3, 8'h00);
  set_reg(REG_WBGLOG, 8'h00);
  set_reg(REG_WOBJLOG, 8'h00);
  set_reg(REG_TM, 8'h13);
  set_reg(REG_TMW, 8'h01);
  set_reg(REG_CGWSEL, 8'h02);
  set_reg(REG_CGADSUB, 8'h43);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h80);

  hdma_en = 1;
endtask

// Emulate H-DMA
reg [31:0] awhx [0:255];
initial begin
  awhx[9'd056] = 32'hAB966B56;
  awhx[9'd057] = 32'hAF926F52;
  awhx[9'd058] = 32'hB38E734E;
  awhx[9'd059] = 32'hB58C754C;
  awhx[9'd060] = 32'hB8897849;
  awhx[9'd061] = 32'hBA877A47;
  awhx[9'd062] = 32'hBC857C45;
  awhx[9'd063] = 32'hBD847D44;
  awhx[9'd064] = 32'hBF827F42;
  awhx[9'd065] = 32'hC0818041;
  awhx[9'd066] = 32'hC27F823F;
  awhx[9'd067] = 32'hC37E833E;
  awhx[9'd068] = 32'hC47D843D;
  awhx[9'd069] = 32'hC67B863B;
  awhx[9'd070] = 32'hC77A873A;
  awhx[9'd071] = 32'hC8798839;
  awhx[9'd072] = 32'hC9788938;
  awhx[9'd073] = 32'hCA778A37;
  awhx[9'd074] = 32'hCB768B36;
  awhx[9'd076] = 32'hCC758C35;
  awhx[9'd077] = 32'hCD748D34;
  awhx[9'd078] = 32'hCE738E33;
  awhx[9'd080] = 32'hCF728F32;
  awhx[9'd081] = 32'hD0719031;
  awhx[9'd083] = 32'hD1709130;
  awhx[9'd084] = 32'hD26F922F;
  awhx[9'd086] = 32'hD36E932E;
  awhx[9'd088] = 32'hD46D942D;
  awhx[9'd091] = 32'hD56C952C;
  awhx[9'd093] = 32'hD66B962B;
  awhx[9'd097] = 32'hD76A972A;
  awhx[9'd101] = 32'hD8699829;
  awhx[9'd122] = 32'hD76A972A;
  awhx[9'd124] = 32'hD66B962B;
  awhx[9'd128] = 32'hD56C952C;
  awhx[9'd130] = 32'hD46D942D;
  awhx[9'd133] = 32'hD36E932E;
  awhx[9'd135] = 32'hD26F922F;
  awhx[9'd137] = 32'hD1709130;
  awhx[9'd138] = 32'hD0719031;
  awhx[9'd140] = 32'hCF728F32;
  awhx[9'd141] = 32'hCE738E33;
  awhx[9'd143] = 32'hCD748D34;
  awhx[9'd144] = 32'hCC758C35;
  awhx[9'd145] = 32'hCB768B36;
  awhx[9'd147] = 32'hCA778A37;
  awhx[9'd148] = 32'hC9788938;
  awhx[9'd149] = 32'hC8798839;
  awhx[9'd150] = 32'hC77A873A;
  awhx[9'd151] = 32'hC67B863B;
  awhx[9'd152] = 32'hC47D843D;
  awhx[9'd153] = 32'hC37E833E;
  awhx[9'd154] = 32'hC27F823F;
  awhx[9'd155] = 32'hC0818041;
  awhx[9'd156] = 32'hBF827F42;
  awhx[9'd157] = 32'hBD847D44;
  awhx[9'd158] = 32'hBC857C45;
  awhx[9'd159] = 32'hBA877A47;
  awhx[9'd160] = 32'hB8897849;
  awhx[9'd161] = 32'hB58C754C;
  awhx[9'd162] = 32'hB38E734E;
  awhx[9'd163] = 32'hAF926F52;
  awhx[9'd164] = 32'hAB966B56;
  awhx[9'd165] = 32'h00010001;
end
wire [31:0] whx = awhx[ppu.video_counter.ROW];

always @(posedge hblank) begin
  if (whx !== 32'bx) begin
    set_reg(REG_WH0, whx[07:00]);
    set_reg(REG_WH1, whx[15:08]);
    set_reg(REG_WH2, whx[23:16]);
    set_reg(REG_WH3, whx[31:24]);
  end
end
