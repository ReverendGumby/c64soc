// FF6, battle screen, Sabin casts Aura cannon

initial begin
  $readmemh("vram-mode1-16x16tile-1-dmp.hex", vram.mem);
  $readmemh("pal-mode1-16x16tile-1-dmp.hex", init_pal);
  $readmemh("oam-mode1-16x16tile-1-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'h61);
  set_reg(REG_BGMODE, 8'h59);
  set_reg(REG_BG1SC, 8'h0C);
  set_reg(REG_BG2SC, 8'h61);
  set_reg(REG_BG3SC, 8'h54);
  set_reg(REG_BG12NBA, 8'h10);
  set_reg(REG_BG34NBA, 8'h05);
  set_reg2x(REG_BG1HOFS, 16'h006C);
  set_reg2x(REG_BG1VOFS, 16'h03B2);
  set_reg2x(REG_BG2HOFS, 16'h0000);
  set_reg2x(REG_BG2VOFS, 16'h0000);
  set_reg2x(REG_BG3HOFS, 16'h0100);
  set_reg2x(REG_BG3VOFS, 16'h0000);
  set_reg(REG_W12SEL, 8'h33);
  set_reg(REG_W34SEL, 8'hCC);
  set_reg(REG_WOBJSEL, 8'h33);
  set_reg(REG_WH0, 8'h08);
  set_reg(REG_WH1, 8'hF7);
  set_reg(REG_WH2, 8'hFF);
  set_reg(REG_WH3, 8'h00);
  set_reg(REG_WBGLOG, 8'h00);
  set_reg(REG_WOBJLOG, 8'h00);
  set_reg(REG_TM, 8'h17);
  set_reg(REG_TS, 8'h12);
  set_reg(REG_TMW, 8'h17);
  set_reg(REG_TSW, 8'h00);
  set_reg(REG_CGWSEL, 8'h02);
  set_reg(REG_CGADSUB, 8'h01);
  set_reg(REG_COLDATA, 8'hE0);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h80);
endtask
