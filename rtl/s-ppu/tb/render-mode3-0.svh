// SNES Test Program, "Look for color changes"

initial begin
  $readmemh("vram-mode3-0-dmp.hex", vram.mem);
  $readmemh("pal-mode3-0-dmp.hex", init_pal);
  $readmemh("oam-mode3-0-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'h02);
  set_reg(REG_BGMODE, 8'h03);
  set_reg(REG_BG1SC, 8'h7C);
  set_reg(REG_BG2SC, 8'h74);
  set_reg(REG_BG12NBA, 8'h60);
  set_reg2x(REG_BG1HOFS, 16'h0000);
  set_reg2x(REG_BG1VOFS, 16'h0020);
  set_reg2x(REG_BG2HOFS, 16'h0057);
  set_reg2x(REG_BG2VOFS, 16'h0000);
  set_reg(REG_WOBJSEL, 8'hA0);
  set_reg(REG_WH0, 8'h00);
  set_reg(REG_WH1, 8'h7B);
  set_reg(REG_WH2, 8'hFF);
  set_reg(REG_WH3, 8'hFF);
  set_reg(REG_WBGLOG, 8'h00);
  set_reg(REG_WOBJLOG, 8'h00);
  set_reg(REG_TM, 8'h11);
  set_reg(REG_TS, 8'h02);
  set_reg(REG_CGWSEL, 8'h22);
  set_reg(REG_CGADSUB, 8'h43);
  set_reg(REG_COLDATA, 8'hE0);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h80);
endtask
