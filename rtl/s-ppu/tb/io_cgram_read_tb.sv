`timescale 1ns / 1ps

module io_cgram_read_tb();

`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("io_cgram_read_tb.vcd");
  $dumpvars;
end

initial begin
  #8 @(posedge clk) res = 0;

  set_reg(REG_CGADD, 0);

  #4 ;
  for (integer i = 0; i < 256; i++) begin
  reg [7:0] ii;
  reg [15:0] v;
    ii = ~i;
    v = {1'b0, ii[6:0], ii[7:0]};
    ppu.palette.cgram[i] = v;
    get_reg2x(REG_RDCGRAM, v, 16'h7fff);
  end

  $finish();
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s io_cgram_read_tb -o io_cgram_read_tb.vvp -f clk_ppu_vram.files io_cgram_read_tb.sv && ./io_cgram_read_tb.vvp"
// End:
