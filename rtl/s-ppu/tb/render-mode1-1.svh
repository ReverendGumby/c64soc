initial begin
  $readmemh("vram-mode1-1-dmp.hex", vram.mem);
  $readmemh("pal-mode1-1-dmp.hex", init_pal);
  $readmemh("oam-mode1-1-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'h00);
  set_reg(REG_BGMODE, 8'h01);
  set_reg(REG_BG1SC, 8'h79);
  set_reg(REG_BG2SC, 8'h71);
  set_reg(REG_BG3SC, 8'h00);
  set_reg(REG_BG12NBA, 8'h23);
  set_reg(REG_BG34NBA, 8'h00);
  set_reg2x(REG_BG1HOFS, 16'h017C);
  set_reg2x(REG_BG1VOFS, 16'h0107);
  set_reg2x(REG_BG2HOFS, 16'h001D);
  set_reg2x(REG_BG2VOFS, 16'h0008);
  set_reg(REG_TM, 8'h13);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h80);
endtask
