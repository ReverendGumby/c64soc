`timescale 1ns / 1ps

module io_oam_read_tb();

`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("io_oam_read_tb.vcd");
  $dumpvars;
end

initial begin
  #8 @(posedge clk) res = 0;

  set_reg(REG_OAMADDL, 0);
  set_reg(REG_OAMADDH, 0);

  #4 ;
  for (integer i = 0; i < 256; i++) begin
  reg [7:0] ii;
  reg [15:0] v;
    ii = ~i;
    v = {2{ii[7:0]}};
    ppu.oam.meml[i] = v;
    get_reg2x(REG_RDOAM, v);
  end

  #4 ;
  for (integer i = 0; i < 32; i++) begin
  reg [7:0] v;
    v = ~i;
    ppu.oam.memh[i] = v;
    get_reg(REG_RDOAM, v);
  end

  $finish();
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s io_oam_read_tb -o io_oam_read_tb.vvp -f clk_ppu_vram.files io_oam_read_tb.sv && ./io_oam_read_tb.vvp"
// End:
