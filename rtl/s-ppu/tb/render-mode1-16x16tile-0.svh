initial begin
  $readmemh("vram-mode1-16x16tile-0-dmp.hex", vram.mem);
  $readmemh("pal-mode1-16x16tile-0-dmp.hex", init_pal);
  $readmemh("oam-mode1-16x16tile-0-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'h63);
  set_reg(REG_BGMODE, 8'h39);
  set_reg(REG_BG1SC, 8'h40);
  set_reg(REG_BG2SC, 8'h5C);
  set_reg(REG_BG3SC, 8'h58);
  set_reg(REG_BG12NBA, 8'h00);
  set_reg(REG_BG34NBA, 8'h05);
  set_reg2x(REG_BG1HOFS, 16'h002A);
  set_reg2x(REG_BG1VOFS, 16'h011F);
  set_reg2x(REG_BG2HOFS, 16'h0034);
  set_reg2x(REG_BG2VOFS, 16'h011F);
  set_reg(REG_TM, 8'h17);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h00);
endtask
