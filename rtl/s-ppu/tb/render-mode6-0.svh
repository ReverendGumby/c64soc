// SNES Test Program, "many Service Marios"

initial begin
  $readmemh("vram-mode6-0-dmp.hex", vram.mem);
  $readmemh("pal-mode6-0-dmp.hex", init_pal);
  $readmemh("oam-mode6-0-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'h02);
  set_reg(REG_BGMODE, 8'h06);
  set_reg(REG_BG1SC, 8'h50);
  set_reg(REG_BG2SC, 8'h5B);
  set_reg(REG_BG3SC, 8'h7C);
  set_reg(REG_BG12NBA, 8'h10);
  set_reg2x(REG_BG1HOFS, 16'h0000);
  set_reg2x(REG_BG1VOFS, 16'h0000);
  set_reg2x(REG_BG2HOFS, 16'h00FF);
  set_reg2x(REG_BG2VOFS, 16'h0050);
  set_reg(REG_WOBJSEL, 8'hA0);
  set_reg(REG_WH0, 8'h00);
  set_reg(REG_WH1, 8'hFD);
  set_reg(REG_WH2, 8'hFF);
  set_reg(REG_WH3, 8'hFF);
  set_reg(REG_WBGLOG, 8'h00);
  set_reg(REG_WOBJLOG, 8'h00);
  set_reg(REG_TM, 8'h11);
  set_reg(REG_TS, 8'h11);
  set_reg(REG_CGWSEL, 8'h00);
  set_reg(REG_CGADSUB, 8'h00);
  set_reg(REG_COLDATA, 8'hE0);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h80);
endtask
