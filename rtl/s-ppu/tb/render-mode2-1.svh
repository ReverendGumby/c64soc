// Jim Power, main menu

initial begin
  $readmemh("vram-mode2-1-dmp.hex", vram.mem);
  $readmemh("pal-mode2-1-dmp.hex", init_pal);
  $readmemh("oam-mode2-1-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'h63);
  set_reg(REG_BGMODE, 8'h32);
  set_reg(REG_BG1SC, 8'h58);
  set_reg(REG_BG2SC, 8'h54);
  set_reg(REG_BG3SC, 8'h50);
  set_reg(REG_BG12NBA, 8'h00);
  set_reg(REG_BG34NBA, 8'h04);
  set_reg2x(REG_BG1HOFS, 16'h03FF);
  set_reg2x(REG_BG1VOFS, 16'h0000);
  set_reg2x(REG_BG2HOFS, 16'h0236);
  set_reg2x(REG_BG2VOFS, 16'h0000);
  set_reg(REG_WOBJSEL, 8'h20);
  set_reg(REG_WH0, 8'h00);
  set_reg(REG_WH1, 8'hFF);
  set_reg(REG_WH2, 8'h00);
  set_reg(REG_WH3, 8'h00);
  set_reg(REG_WBGLOG, 8'h00);
  set_reg(REG_WOBJLOG, 8'h00);
  set_reg(REG_TM, 8'h17);
  set_reg(REG_TMW, 8'h01);
  set_reg(REG_CGWSEL, 8'h40);
  set_reg(REG_CGADSUB, 8'h07);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h00);
endtask
