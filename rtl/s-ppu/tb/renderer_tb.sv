`timescale 1ns / 1ps

module renderer_tb();

`include "../timings.vh"
`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("renderer_tb.vcd");
  $dumpvars;
end

//`include "render-mode0-5.svh"
//`include "render-mode1-16x16tile-1.svh"
//`include "render-mode2-1.svh"
//`include "render-mode3-0.svh"
//`include "render-mode4-0.svh"
//`include "render-mode5-0.svh"
//`include "render-mode6-0.svh"
`include "render-mode7-5.svh"

initial begin
  #8 @(posedge clk) res <= 0;

  // Skip to vbl_start, load palette + OAM, and enable rendering.
  #8 ;
  ppu.video_counter.ROW = 9'd241;
  ppu.video_counter.COL = 9'd0;
  #4 @(posedge clk) ;
  load_palette();
  load_oam();
  set_reg(REG_INIDISP, 8'h0F);

  // Initialize registers
  clear_regs();
  load_regs();

  // Skip to just before pre-render row.
  ppu.video_counter.ROW = 9'd260;
  ppu.video_counter.COL = 9'd339;
  #4 @(posedge clk) ;

  //#((16)*NUM_COLS*4) $finish();
  #((NUM_ROWS+2)*NUM_COLS*4) $finish();
end

integer fpic, pice;
initial begin
  fpic = $fopen("render.hex", "w");
  pice = 0;
end
always @(posedge clk) begin
  if (pce && de) begin
    if (pice) begin
      pice = 0;
      $fwrite(fpic, "\n");
    end
    $fwrite(fpic, "%x", ppu_r);
    $fwrite(fpic, "%x", ppu_g);
    $fwrite(fpic, "%x", ppu_b);
  end
  if (!hs)
    pice = 1;
end
final
  $fclose(fpic);

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s renderer_tb -o renderer_tb.vvp -f clk_ppu_vram.files renderer_tb.sv && ./renderer_tb.vvp && make render.png"
// End:
