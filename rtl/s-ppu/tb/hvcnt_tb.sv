`timescale 1ns / 1ps

module hvcnt_tb();

`include "../timings.vh"
`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("hvcnt_tb.vcd");
  $dumpvars;
end

`define ts ppu.timer_status

initial begin
reg [8:0] hct, vct;
  #8 @(posedge clk) res <= 0;

  // Skip to vbl_start and enable rendering.
  #8 ;
  ppu.video_counter.ROW = 9'd241;
  ppu.video_counter.COL = 9'd0;
  #4 @(posedge clk) ;
  set_reg(REG_INIDISP, 8'h00);

  // Counters reset to all 1s, latch cleared
  get_reg(REG_STAT78, 0, 8'h40); // reset read toggles
  get_reg(REG_OPHCT, ~8'b0, ~8'b0);
  get_reg(REG_OPHCT, 8'b1, 8'b1);
  get_reg(REG_OPVCT, ~8'b0, ~8'b0);
  get_reg(REG_OPVCT, 8'b1, 8'b1);

  // SW-controlled trigger
  get_reg(REG_SLHV, 0, 0);
  #8 ;
  hct = `ts.ophct;
  vct = `ts.opvct;
  assert(hct != 9'h1ff);
  assert(vct != 9'h1ff);

  // Counters are somewhere now
  get_reg(REG_OPHCT, hct[7:0], ~8'b0);
  get_reg(REG_OPHCT, hct[8], 8'b1);
  get_reg(REG_OPVCT, vct[7:0], ~8'b0);
  get_reg(REG_OPVCT, vct[8], 8'b1);

  // Re-read of SW-controlled trigger latches counters again
  #1364 ; // next row
  get_reg(REG_SLHV, 0, 0);
  #8 ;
  assert(`ts.ophct != hct);
  assert(`ts.opvct != vct);
  hct = `ts.ophct;
  vct = `ts.opvct;

  // Counters are somewhere else now
  get_reg(REG_OPHCT, hct[7:0], ~8'b0);
  get_reg(REG_OPHCT, hct[8], 8'b1);
  get_reg(REG_OPVCT, vct[7:0], ~8'b0);
  get_reg(REG_OPVCT, vct[8], 8'b1);

  // Latch is cleared on STAT78 read
  get_reg(REG_STAT78, 8'h40, 8'h40);
  get_reg(REG_STAT78, 0, 8'h40);

  #4 @(posedge clk) ;

  $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s hvcnt_tb -o hvcnt_tb.vvp -f clk_ppu_vram.files hvcnt_tb.sv && ./hvcnt_tb.vvp"
// End:
