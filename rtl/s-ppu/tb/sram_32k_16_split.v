module sram_32k_16_split
  (
   input        CLK,
   input        CLKEN,
   input        nCE,
   input        nOE,
   input        nWEA,
   input        nWEB,
   input [14:0] AA,
   input [7:0]  DA_I,
   output [7:0] DA_O,
   output       DA_OE,
   input [14:0] AB,
   input [7:0]  DB_I,
   output [7:0] DB_O,
   output       DB_OE
   );

reg [15:0] mem [0:(1<<15)-1];
reg [15:0] da, db;

always @(posedge CLK) if (CLKEN) begin
  da <= mem[AA][7:0];
  if (!nCE && !nWEA)
    mem[AA][7:0] <= DA_I;
end

always @(posedge CLK) if (CLKEN) begin
  db <= mem[AB][15:8];
  if (!nCE && !nWEB)
    mem[AB][15:8] <= DB_I;
end

assign DA_O = DA_OE ? da : 16'bx;
assign DA_OE = !nCE && !nOE && nWEA;

assign DB_O = DB_OE ? db : 16'bx;
assign DB_OE = !nCE && !nOE && nWEB;

endmodule
