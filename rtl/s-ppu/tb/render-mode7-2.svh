// FF5, asteroid falling from sky

reg hdma_en = 0;

initial begin
  $readmemh("vram-mode7-2-dmp.hex", vram.mem);
  $readmemh("pal-mode7-2-dmp.hex", init_pal);
  $readmemh("oam-mode7-2-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'h62);
  set_reg(REG_BGMODE, 8'h07);
  set_reg2x(REG_M7HOFS, 16'h0190);
  set_reg2x(REG_M7VOFS, 16'h01A8);
  set_reg(REG_M7SEL, 8'h80);
  set_reg2x(REG_M7A, 16'h0015);
  set_reg2x(REG_M7B, 16'hFF63);
  set_reg2x(REG_M7C, 16'h009D);
  set_reg2x(REG_M7D, 16'h0015);
  set_reg2x(REG_M7X, 16'h0210);
  set_reg2x(REG_M7Y, 16'h0218);
  set_reg(REG_TM, 8'h11);
  set_reg(REG_SETINI, 8'h00);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h80);

  hdma_en = 1;
endtask

always @(posedge hblank) begin
  // Emulate H-DMA
  if (hdma_en && ppu.video_counter.ROW < 250) begin
    set_reg2x(REG_M7A, 16'h0015);
    set_reg2x(REG_M7B, 16'hFF63);
    set_reg2x(REG_M7C, 16'h009D);
    set_reg2x(REG_M7D, 16'h0015);
  end
end
