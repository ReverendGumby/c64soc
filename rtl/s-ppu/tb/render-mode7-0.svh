initial begin
  $readmemh("vram-mode7-0-dmp.hex", vram.mem);
  $readmemh("pal-mode7-0-dmp.hex", init_pal);
  $readmemh("oam-mode7-0-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'h02);
  set_reg(REG_BGMODE, 8'h07);
  set_reg(REG_BG12NBA, 8'h10);
  set_reg2x(REG_M7HOFS, 16'h0000);
  set_reg2x(REG_BG1VOFS, 16'h0000);
  set_reg(REG_M7SEL, 8'h00);
  set_reg2x(REG_M7A, 16'hFF89);
  set_reg2x(REG_M7B, 16'h0061);
  set_reg2x(REG_M7C, 16'hFF8C);
  set_reg2x(REG_M7D, 16'hFF71);
  set_reg2x(REG_M7X, 16'h0080);
  set_reg2x(REG_M7Y, 16'h0080);
  set_reg(REG_TM, 8'h11);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h80);
endtask
