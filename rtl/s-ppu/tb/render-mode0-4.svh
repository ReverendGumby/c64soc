// SNES Test Program, "Look for colored windows"

initial begin
  $readmemh("vram-mode0-4-dmp.hex", vram.mem);
  $readmemh("pal-mode0-4-dmp.hex", init_pal);
  $readmemh("oam-mode0-4-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'h03);
  set_reg(REG_BGMODE, 8'hF0);
  set_reg(REG_BG1SC, 8'h40);
  set_reg(REG_BG2SC, 8'h50);
  set_reg(REG_BG3SC, 8'h60);
  set_reg(REG_BG4SC, 8'h70);
  set_reg(REG_BG12NBA, 8'h00);
  set_reg(REG_BG34NBA, 8'h00);
  set_reg2x(REG_BG1HOFS, 16'h0000);
  set_reg2x(REG_BG1VOFS, 16'h0000);
  set_reg(REG_W12SEL, 8'hFA);
  set_reg(REG_W34SEL, 8'hFA);
  set_reg(REG_WOBJSEL, 8'h00);
  set_reg(REG_WH0, 8'h06);
  set_reg(REG_WH1, 8'hFF);
  set_reg(REG_WH2, 8'h00);
  set_reg(REG_WH3, 8'hF9);
  set_reg(REG_WBGLOG, 8'h00);
  set_reg(REG_WOBJLOG, 8'h00);
  set_reg(REG_TM, 8'h1E);
  set_reg(REG_TMW, 8'h02);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h80);
endtask
