// FF5, first world map view (Boko center)

initial begin
  $readmemh("vram-mode7-1-dmp.hex", vram.mem);
  $readmemh("pal-mode7-1-dmp.hex", init_pal);
  $readmemh("oam-mode7-1-dmp.hex", init_oam);
end

task load_regs();
  set_reg(REG_OBSEL, 8'h03);
  set_reg(REG_BGMODE, 8'h07);
  set_reg(REG_BG1SC, 8'h00);
  set_reg(REG_BG2SC, 8'h50);
  set_reg(REG_BG3SC, 8'h58);
  set_reg(REG_BG12NBA, 8'h10);
  set_reg2x(REG_M7HOFS, 16'h0170);
  set_reg2x(REG_M7VOFS, 16'h00F0);
  set_reg2x(REG_BG2HOFS, 16'h00A0);
  set_reg2x(REG_BG2VOFS, 16'h0030);
  set_reg2x(REG_BG3HOFS, 16'h0170);
  set_reg2x(REG_BG3VOFS, 16'h00F0);
  set_reg(REG_M7SEL, 8'h00);
  set_reg2x(REG_M7A, 16'h0100);
  set_reg2x(REG_M7B, 16'h0000);
  set_reg2x(REG_M7C, 16'h0000);
  set_reg2x(REG_M7D, 16'h0100);
  set_reg2x(REG_M7X, 16'h01D8);
  set_reg2x(REG_M7Y, 16'h0168);
  set_reg(REG_TM, 8'h13);
  set_reg(REG_SETINI, 8'h00);
  set_reg(REG_OAMADDL, 8'h00);
  set_reg(REG_OAMADDH, 8'h80);
endtask
