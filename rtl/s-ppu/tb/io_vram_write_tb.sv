`timescale 1ns / 1ps

module io_vram_write_tb();

`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("io_vram_write.vcd");
  $dumpvars;
end

task test_cpu_io;
  set_reg(REG_VMAIN, 8'b10000000); // +1 on H
  set_reg(REG_VMADDL, 8'h80);
  set_reg(REG_VMADDH, 8'h00);

  #4 ;
  for (integer i = 0; i < 10; i++) begin
  reg [7:0] ii;
  reg [7:0] v;
    ii = i;
    v = ~ii;
    fork
      begin
        set_reg(i[0] ? REG_VMDATAH : REG_VMDATAL, v);
        #15 ;
      end
      begin
        check_vram_write(i, v);
      end
    join
  end
endtask

task test_dma_io_addr_once;
  set_reg(REG_VMAIN, 8'b10000000); // +1 on H
  set_reg(REG_VMADDL, 8'h80);
  set_reg(REG_VMADDH, 8'h00);

  @(posedge ppu.cc) ;
  fork
    begin
      for (integer i = 0; i < 10; i++) begin
      reg [7:0] ii;
      reg [7:0] v;
        ii = i;
        v = ~ii;
        dma_set_reg(i[0] ? REG_VMDATAH : REG_VMDATAL, v);
      end
    end
    begin
      #8 ; // first dma_set_reg done
      for (integer i = 0; i < 10; i++) begin
      reg [7:0] ii;
      reg [7:0] v;
        ii = i;
        v = ~ii;
        check_vram_write(i, v);
      end
    end
  join
endtask

task test_dma_io_addr_data;
  // Bug found in First Samurai: DMA uses 4-byte write to VMADDL,
  // VMADDH, VMDATAL, then VMDATAH. Address increment following
  // VMDATAH would be combined with the following VMADDL, causing all
  // words except the first to be written to the target address + 1.

  set_reg(REG_VMAIN, 8'b10000000); // +1 on H

  @(posedge ppu.cc) ;
  fork
    begin
      for (integer i = 0; i < 'h40; i += 'h8) begin
      reg [7:0] ii;
      reg [7:0] v;
        dma_set_reg(REG_VMADDL, 8'h80 + i[14:1]);
        dma_set_reg(REG_VMADDH, 8'h00);
        ii = i;
        v = ~ii;
        dma_set_reg(REG_VMDATAL, v);
        ii = i + 1;
        v = ~ii;
        dma_set_reg(REG_VMDATAH, v);
      end
    end
    begin
      #8 ; // first dma_set_reg done
      for (integer i = 0; i < 'h40; i += 'h8) begin
      reg [7:0] ii;
      reg [7:0] v;
        #16 ; // dma_set_reg(REG_VMDATAL) done
        ii = i;
        v = ~ii;
        check_vram_write(i, v);
        ii = i + 1;
        v = ~ii;
        check_vram_write(i + 1, v);
      end
    end
  join
endtask

task check_vram_write(integer i, reg [7:0] v);
integer start, now, delta;
//reg [15:0] saa, sab;
//reg [7:0] sda, sdb;
//reg       snvawr, snvbwr;
  @(negedge nvawr or negedge nvbwr) start = $time;
  @(posedge ppu.cc) /*saa = vaa; sab = vab; sda = vda_o; sdb = vdb_o*/;
  @(posedge clk) now = $time;
  delta = now - start;
/* -----\/----- EXCLUDED -----\/-----
  @(posedge clk) if (nvawr == 1'b1 & nvbwr == 1'b1)
    @(negedge nvawr or negedge nvbwr) ;
  snvawr = nvawr; snvbwr = nvbwr;
  @(posedge ppu.cc) saa = vaa; sab = vab; sda = vda_o; sdb = vdb_o;
  @(posedge nvawr or posedge nvbwr) now = $time;
  delta = now - start;
 -----/\----- EXCLUDED -----/\----- */

  $display("check_vram_write: %1t-%1t = %1t", now, start, delta);
  assert(delta == 4);
  if (~i[0]) begin
    assert(~nvawr & nvbwr);
    assert(vaa == ('h80 + i[14:1]));
    assert(vda_o == v);
  end
  else begin
    assert(nvawr & ~nvbwr);
    assert(vab == ('h80 + i[14:1]));
    assert(vdb_o == v);
  end
endtask

initial begin
  #8 @(posedge clk) res = 0;

  repeat (4) test_cpu_io();
  repeat (4) test_dma_io_addr_once();
  repeat (4) test_dma_io_addr_data();

  $finish();
end

always #5000 begin
  $display("Emergency stop");
  $finish(1);
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s io_vram_write_tb -o io_vram_write_tb.vvp -f clk_ppu_vram.files io_vram_write_tb.sv && ./io_vram_write_tb.vvp"
// End:
