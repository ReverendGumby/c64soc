`timescale 1ns / 1ps

module io_vram_read_tb();

`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("io_vram_read.vcd");
  $dumpvars;
end

initial begin
  // Set known values in VRAM.
  vram.mem['h80] = 16'hFF00;
  vram.mem['h81] = 16'hFE01;
  vram.mem['h82] = 16'hFD02;
  vram.mem['h83] = 16'hFC03;
  vram.mem['h84] = 16'hFB04;
  vram.mem['h85] = 16'hFA05;
  vram.mem['h86] = 16'hF906;
  vram.mem['h87] = 16'hF807;
  vram.mem['h88] = 16'hF708;
  vram.mem['h89] = 16'hF609;

  #8 @(posedge clk) res = 0;

  set_reg(REG_VMAIN, 8'b10000000); // +1 on H
  set_reg(REG_VMADDL, 8'h80);
  set_reg(REG_VMADDH, 8'h00);

  #4 ;
  for (integer i = 0; i < 10; i++) begin
  reg [7:0] ii;
  reg [15:0] v;
    if (i == 0)
      ii = i;
    else
      ii = (i - 1); // because of prefetch glitch
    v = {~ii, ii};
    get_reg16(REG_RDVRAML, v);
    fork
      //check_vram_read(i);
      #11 ;
    join
  end

  $finish();
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s io_vram_read_tb -o io_vram_read_tb.vvp -f clk_ppu_vram.files io_vram_read_tb.sv && ./io_vram_read_tb.vvp"
// End:
