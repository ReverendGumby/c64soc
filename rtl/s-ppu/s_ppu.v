// Ricoh 5C77 and 5C78, the SNES PPUs
//
// Credit to:
// - https://snes.nesdev.org/
// - https://problemkaputt.de/fullsnes.htm
// - SNES Development Manual, Book 1

module s_ppu
  (
   input         CLK_RESET,
   input         CLK,
   input         CLKEN,
   input         RES_COLD,
   input         RESET,
   input         HOLD,

   // debug monitor
   input [10:0]  MON_SEL,
   input         MON_READY,
   output [31:0] MON_DOUT,
   output        MON_VALID,
   input [3:0]   MON_WS,
   input [31:0]  MON_DIN,

   // ICD match interface
   output [8:0]  ICD_COL,
   output [8:0]  ICD_ROW,
   output [31:0] ICD_FRAME,

   // status / control signals
   output        CC, // for CPU H/V counters
   output        nCC,
   output        HBLANK,
   output        VBLANK,
   input         EXTLATCH,

   // CPU address / data bus (B-Bus)
   input [7:0]   PA,
   input [7:0]   D_I,
   output [7:0]  D_O,
   output        D_OE,
   input         nPARD,
   input         nPAWR,

   // VRAM address / data bus A, low byte
   output [15:0] VAA,
   input [7:0]   VDA_I,
   output [7:0]  VDA_O,
   output        nVARD,
   output        nVAWR,

   // VRAM address / data bus B, high byte
   output [15:0] VAB,
   input [7:0]   VDB_I,
   output [7:0]  VDB_O,
   output        nVBRD,
   output        nVBWR,

   // video output
   output        PCE, // pixel clock enable
   output        DE, // data enable
   output        HS, // horizontal sync
   output        VS, // vertical sync
   output [7:0]  R, // red component
   output [7:0]  G, // green component
   output [7:0]  B  // blue component
   );

wire [31:0] mon_dout_vc, mon_dout_ts, mon_dout_ren, mon_dout_bm, mon_dout_oam,
            mon_dout_oim, mon_dout_spren, mon_dout_olb, mon_dout_win,
            mon_dout_pal;
wire        mon_ack_vc, mon_ack_ts, mon_ack_ren, mon_ack_bm, mon_ack_oam,
            mon_ack_oim, mon_ack_spren, mon_ack_olb, mon_ack_win, mon_ack_pal;
wire        mon_valid_vc, mon_valid_ts, mon_valid_ren, mon_valid_bm,
            mon_valid_oam, mon_valid_oim, mon_valid_spren, mon_valid_olb,
            mon_valid_win, mon_valid_pal;
wire [3:0]  bright;
wire [2:0]  bgmode;
wire [3:0]  bgsize;
wire        render_en, rendering, field, dot_skipped;
wire        hinc, hclr, vclr, fetch;
wire [3:0]  vinc;
wire        fetch_nt, fetch_pt;
wire        spr_clr, spr_start, spr_end, spr_fetch, spr_fetch_ptl;
wire        vbl_start, vbl_end, ren_blank, sync_blank;
wire [31:0] frame;
wire [63:52] io_re, io_rs;
wire [51:0]  io_ws;
wire [7:0]  pd;
wire [8:0]  row, col, ren_row, ren_col;
wire [7:0]  io_d_o_tmr, io_d_o_bm, io_d_o_ren, io_d_o_oam,
            io_d_o_pal;
wire        io_d_oe_bm, io_d_oe_ren, io_d_oe_oam;
wire [7:0]  io_d_oe_tmr, io_d_oe_pal;
wire [7:0]  oam_al;
wire [15:0] oam_dl;
wire [4:0]  oam_ah;
wire [7:0]  oam_dh;
wire        oam_wel, oam_weh;
wire        oim_clr, oim_empty, oim_full;
wire [6:0]  oim_wd, oim_rd;
wire        oim_we, oim_re;
wire [15:0] sprev_oam_dl;
wire [7:0]  sprev_oam_dh;
wire [7:0]  spren_oamadd;
wire        spren_re;
wire [1:0]  spren_oamh;
wire [7:1]  olb_wal, olb_wah;
wire [8:0]  olb_wdl, olb_wdh;
wire        olb_wel, olb_weh;
wire [7:0]  olb_ra;
wire [8:0]  olb_rd;
wire        olb_re;
wire [5:0]  win;
wire [2:0]  objsel_ba;
wire [1:0]  objsel_n;
wire [2:0]  objsel_ss;
wire [8:0]  spr_chr;
wire [2:0]  spr_y;
wire        range_over, time_over;
wire [14:0] pal_rgb, ren_rgb;
wire        cc, ncc;

// Internal I/O read (PPU -> CPU) data bus
// Backed by the hypothetical "MDR"
// TODO: There's actually two MDRs, one each for PPU1 and PPU2
reg [7:0]   ior, mdr;

initial
  mdr = 8'hxx;

always @* begin
  ior = mdr;
  if (|io_d_oe_tmr)
    ior = (ior & ~io_d_oe_tmr) | (io_d_o_tmr & io_d_oe_tmr);
  if (io_d_oe_bm)
    ior = io_d_o_bm;
  if (io_d_oe_ren)
    ior = io_d_o_ren;
  if (io_d_oe_oam)
    ior = io_d_o_oam;
  if (|io_d_oe_pal)
    ior = (ior & ~io_d_oe_pal) | (io_d_o_pal & io_d_oe_pal);
end

always @(posedge CLK)
  mdr <= ior;

// Internal I/O write (CPU -> PPU) data bus
// Delay by one clock, so we can easily latch data on nPAWR falling edge.
reg [7:0] io_d_i;

always @(posedge CLK)
  io_d_i <= D_I;

s_ppu_clkgen clkgen
  (
   .CLK_RESET(CLK_RESET),
   .CLK(CLK),
   .CLKEN(CLKEN),
   .CLK_HOLD(HOLD),
   .CC(cc),
   .nCC(ncc)
   );

assign CC = cc;
assign nCC = ncc;

s_ppu_video_counter video_counter
  (
   .CLK(CLK),
   .CC(cc),
   .RES(RESET),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_vc),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_vc),
   .MON_VALID(mon_valid_vc),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .RENDER_EN(render_en),

   .COL(col),
   .ROW(row),
   .FRAME(frame),
   .FIELD(field),
   .DOT_SKIPPED(dot_skipped),

   .VBL_START(vbl_start),
   .VBL_END(vbl_end)
   );

s_ppu_reg_file reg_file
  (
   .CLK(CLK),
   .RES_COLD(RES_COLD),
   .RES(RESET),
   .HOLD(HOLD),

   .PA(PA),
   .nPARD(nPARD),
   .nPAWR(nPAWR),

   .IO_RE(io_re),
   .IO_RS(io_rs),
   .IO_WS(io_ws)
   );

s_ppu_timer_status timer_status
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),
   .RES(RESET),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_ts),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_ts),
   .MON_VALID(mon_valid_ts),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .COL(col),
   .ROW(row),
   .FIELD(field),

   .IO_D_I(io_d_i),
   .IO_D_O(io_d_o_tmr),
   .IO_D_OE(io_d_oe_tmr),
   .IO_RE(io_re),
   .IO_RS(io_rs),
   .IO_WS(io_ws),

   .RANGE_OVER(range_over),
   .TIME_OVER(time_over),
   .EXTLATCH(EXTLATCH)
   );

wire [15:0] vd_i = {VDB_I, VDA_I};
wire [15:0] par, pdr;
wire [1:0]  nt_bsel, pt_bsel, pt_bmax;
wire        nt_osel;
wire [11:0] bgfhs;

s_ppu_renderer renderer
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),
   .RES(RESET),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_ren),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_ren),
   .MON_VALID(mon_valid_ren),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .COL(col),
   .ROW(row),
   .FIELD(field),

   .BGFHS(bgfhs),
   .PAR(par),
   .PDR(pdr),

   .IO_D_I(io_d_i),
   .IO_D_O(io_d_o_ren),
   .IO_D_OE(io_d_oe_ren),
   .IO_WS(io_ws),

   .OLB_RA(olb_ra),
   .OLB_RD(olb_rd),
   .OLB_RE(olb_re),

   .WIN(win),

   .PAL_PD(pd),
   .PAL_RGB(pal_rgb),

   .BRIGHT(bright),
   .BGMODE(bgmode),
   .BGSIZE(bgsize),

   .RENDER_EN(render_en),
   .RENDERING(rendering),
   .HINC(hinc),
   .VINC(vinc),
   .HCLR(hclr),
   .VCLR(vclr),
   .FETCH(fetch),
   .FETCH_NT(fetch_nt),
   .FETCH_PT(fetch_pt),
   .SPR_CLR(spr_clr),
   .SPR_START(spr_start),
   .SPR_END(spr_end),
   .SPR_FETCH(spr_fetch),
   .SPR_FETCH_PTL(spr_fetch_ptl),
   .NT_BSEL(nt_bsel),
   .NT_OSEL(nt_osel),
   .PT_BSEL(pt_bsel),
   .PT_BMAX(pt_bmax),

   .HBLANK(HBLANK),
   .VBLANK(VBLANK),

   .SYNC_COL_O(ren_col),
   .SYNC_ROW_O(ren_row),
   .SYNC_BLANK(ren_blank),

   .RGB(ren_rgb)
   );

s_ppu_sync_gen sync_gen
  (
   .CLK(CLK),
   .CC(cc),
   .HOLD(HOLD),

   .COL(ren_col),
   .ROW(ren_row),
   //.DOT_SKIPPED(dot_skipped),

   .BLANK_I(ren_blank),

   .PCE(PCE),
   .DE(DE),
   .HSYNC(HS),
   .VSYNC(VS),
   .BLANK_O(sync_blank)
   );

s_ppu_vram_bus_master bus_master
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),
   .RES(RESET),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_bm),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_bm),
   .MON_VALID(mon_valid_bm),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .COL(col),
   .ROW(row),

   .IO_D_I(io_d_i),
   .IO_D_O(io_d_o_bm),
   .IO_D_OE(io_d_oe_bm),
   .IO_RE(io_re),
   .IO_RS(io_rs),
   .IO_WS(io_ws),

   .BGMODE(bgmode),
   .BGSIZE(bgsize),

   .RENDERING(rendering),
   .HINC(hinc),
   .VINC(vinc),
   .HCLR(hclr),
   .VCLR(vclr),
   .FETCH(fetch),
   .FETCH_NT(fetch_nt),
   .FETCH_PT(fetch_pt),
   .SPR_FETCH(spr_fetch),
   .SPR_FETCH_PTL(spr_fetch_ptl),
   .NT_BSEL(nt_bsel),
   .NT_OSEL(nt_osel),
   .PT_BSEL(pt_bsel),
   .PT_BMAX(pt_bmax),

   .BGFHS(bgfhs),
   .PAR(par),
   .PDR(pdr),

   .OBJSEL_BA(objsel_ba),
   .OBJSEL_N(objsel_n),
   .SPR_CHR(spr_chr),
   .SPR_Y(spr_y),

   .VAA(VAA),
   .VDA_I(VDA_I),
   .VDA_O(VDA_O),
   .nVARD(nVARD),
   .nVAWR(nVAWR),

   .VAB(VAB),
   .VDB_I(VDB_I),
   .VDB_O(VDB_O),
   .nVBRD(nVBRD),
   .nVBWR(nVBWR)
   );

s_ppu_oam oam
  (
   .CLK(CLK),
   .CC(cc),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_oam),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_oam),
   .MON_VALID(mon_valid_oam),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .AL(oam_al),
   .DL_I(sprev_oam_dl),
   .DL_O(oam_dl),
   .WEL(oam_wel),
   .AH(oam_ah),
   .DH_I(sprev_oam_dh),
   .DH_O(oam_dh),
   .WEH(oam_weh)
   );

s_ppu_oim oim
  (
   .CLK(CLK),
   .CC(cc),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_oim),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_oim),
   .MON_VALID(mon_valid_oim),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .CLR(oim_clr),
   .EMPTY(oim_empty),
   .FULL(oim_full),
   .WD(oim_wd),
   .WE(oim_we),
   .RD(oim_rd),
   .RE(oim_re)
   );

s_ppu_sprite_eval sprite_eval
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),
   .RES(RESET),
   .HOLD(HOLD),

   // TODO: add monitor

   .COL(col),
   .ROW(row),

   .IO_D_I(io_d_i),
   .IO_D_O(io_d_o_oam),
   .IO_D_OE(io_d_oe_oam),
   .IO_RE(io_re),
   .IO_RS(io_rs),
   .IO_WS(io_ws),

   .VBL_END(vbl_end),
   .RENDER_EN(render_en),
   .RENDERING(rendering),

   .OBJSEL_SS(objsel_ss),

   .OAM_AL(oam_al),
   .OAM_DL_I(oam_dl),
   .OAM_DL_O(sprev_oam_dl),
   .OAM_WEL(oam_wel),
   .OAM_AH(oam_ah),
   .OAM_DH_I(oam_dh),
   .OAM_DH_O(sprev_oam_dh),
   .OAM_WEH(oam_weh),

   .OIM_CLR(oim_clr),
   .OIM_FULL(oim_full),
   .OIM_WD(oim_wd),
   .OIM_WE(oim_we),

   .RANGE_OVER(range_over),

   .SPREN_OAMADD(spren_oamadd),
   .SPREN_RE(spren_re),
   .SPREN_OAMH(spren_oamh)
   );

s_ppu_spr_renderer spr_renderer
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),
   .RES(RESET),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_spren),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_spren),
   .MON_VALID(mon_valid_spren),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .COL(col),
   .ROW(row),

   .IO_D_I(io_d_i),
   .IO_WS(io_ws),

   .VBL_END(vbl_end),
   .RENDER_EN(render_en),
   .RENDERING(rendering),
   .SPR_CLR(spr_clr),
   .SPR_START(spr_start),
   .SPR_END(spr_end),
   .SPR_FETCH(spr_fetch),
   .SPR_FETCH_PTL(spr_fetch_ptl),

   .OBJSEL_BA(objsel_ba),
   .OBJSEL_N(objsel_n),
   .OBJSEL_SS(objsel_ss),

   .CHR(spr_chr),
   .PDR(pdr),
   .SPR_Y(spr_y),

   .OIM_EMPTY(oim_empty),
   .OIM_RD(oim_rd),
   .OIM_RE(oim_re),

   .SPREV_OAMADD(spren_oamadd),
   .SPREV_RE(spren_re),
   .SPREV_OAML(oam_dl),
   .SPREV_OAMH(spren_oamh),

   .TIME_OVER(time_over),

   .OLB_RA(olb_ra),
   .OLB_WAL(olb_wal),
   .OLB_WDL(olb_wdl),
   .OLB_WEL(olb_wel),
   .OLB_WAH(olb_wah),
   .OLB_WDH(olb_wdh),
   .OLB_WEH(olb_weh)
   );

s_ppu_olb olb
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_olb),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_olb),
   .MON_VALID(mon_valid_olb),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .WAL(olb_wal),
   .WDL(olb_wdl),
   .WEL(olb_wel),
   .WAH(olb_wah),
   .WDH(olb_wdh),
   .WEH(olb_weh),
   .RA(olb_ra),
   .RD(olb_rd),
   .RE(olb_re)
   );

s_ppu_window_gen win_gen
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),
   .RES(RESET),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_win),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_win),
   .MON_VALID(mon_valid_win),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .COL(col),

   .IO_D_I(io_d_i),
   .IO_WS(io_ws),

   .WIN(win)
   );

s_ppu_palette palette
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),
   .RES(RESET),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_pal),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_pal),
   .MON_VALID(mon_valid_pal),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .IO_D_I(io_d_i),
   .IO_D_O(io_d_o_pal),
   .IO_D_OE(io_d_oe_pal),
   .IO_RE(io_re),
   .IO_RS(io_rs),
   .IO_WS(io_ws),

   .RENDERING(rendering),
   .HBLANK(HBLANK),

   .PD(pd),
   .RGB(pal_rgb)
   );

s_ppu_color_gen color_gen
  (
   .CLK(CLK),
   .CC(cc),

   .BRIGHT(bright),

   .I_RGB(ren_rgb),
   .BLANK(sync_blank),

   .O_R(R),
   .O_G(G),
   .O_B(B)
   );

// Debug monitor interface
reg [31:0]  mon_dout;
reg         mon_valid;

always @* begin
  if (mon_ack_vc) begin
    mon_valid = mon_valid_vc;
    mon_dout = mon_dout_vc;
  end
  else if (mon_ack_ts) begin
    mon_valid = mon_valid_ts;
    mon_dout = mon_dout_ts;
  end
  else if (mon_ack_ren) begin
    mon_valid = mon_valid_ren;
    mon_dout = mon_dout_ren;
  end
  else if (mon_ack_bm) begin
    mon_valid = mon_valid_bm;
    mon_dout = mon_dout_bm;
  end
  else if (mon_ack_oam) begin
    mon_valid = mon_valid_oam;
    mon_dout = mon_dout_oam;
  end
  else if (mon_ack_oim) begin
    mon_valid = mon_valid_oim;
    mon_dout = mon_dout_oim;
  end
  else if (mon_ack_spren) begin
    mon_valid = mon_valid_spren;
    mon_dout = mon_dout_spren;
  end
  else if (mon_ack_olb) begin
    mon_valid = mon_valid_olb;
    mon_dout = mon_dout_olb;
  end
  else if (mon_ack_win) begin
    mon_valid = mon_valid_win;
    mon_dout = mon_dout_win;
  end
  else if (mon_ack_pal) begin
    mon_valid = mon_valid_pal;
    mon_dout = mon_dout_pal;
  end
  else begin
    mon_valid = 1'b1;
    mon_dout = 32'h0;
  end
end

assign MON_DOUT = mon_dout;
assign MON_VALID = mon_valid;

assign ICD_COL = col;
assign ICD_ROW = row;
assign ICD_FRAME = frame;

assign D_O = D_OE ? mdr : 8'hxx;
assign D_OE = ~nPARD & ~|PA[7:6];

endmodule
