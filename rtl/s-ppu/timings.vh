// We emulate the 5C77/5C78 in NTSC mode: 262 scanlines (224/239 visible),
// 341 pixels / scanline (256 visible).

localparam [8:0] NUM_ROWS = 9'd262;
localparam [8:0] NUM_COLS = 9'd341;

localparam [8:0] LAST_ROW = NUM_ROWS - 1'd1;
localparam [8:0] LAST_COL = NUM_COLS - 1'd1;

// References:
// - https://snes.nesdev.org/wiki/Timing
// - https://problemkaputt.de/fullsnes.htm#snestiminghvcounters
// Blanks filled in from NES timings

// 224/239 visible lines rendered ($2133.D2 selects)
localparam [8:0] FIRST_ROW_RENDER = 9'd1;
localparam [8:0] LAST_ROW_RENDER_MIN = 9'd224;
localparam [8:0] LAST_ROW_RENDER_MAX = 9'd239;
// post-render blanking
localparam [8:0] FIRST_ROW_POST_RENDER = LAST_ROW_RENDER_MAX + 9'd1;
localparam [8:0] LAST_ROW_POST_RENDER = 9'd241;
// VSYNC
localparam [8:0] FIRST_ROW_VSYNC = 9'd245;
localparam [8:0] LAST_ROW_VSYNC = 9'd247;
// back porch / pre-render blanking
localparam [8:0] FIRST_ROW_PRE_RENDER = 9'd247;
localparam [8:0] LAST_ROW_PRE_RENDER = 9'd0; // hidden, drawn as black

// 256 visible columns rendered
// values are for the start of the 22-cycle pixel render pipeline
//
// Rendering extends two tiles before the visible region
localparam [8:0] FIRST_COL_RENDER = 9'd0;
localparam [8:0] LAST_COL_RENDER = FIRST_COL_RENDER + 9'd271;
// BG renderers output the first visible pixel two tiles after start
localparam [8:0] FIRST_COL_VISIBLE = 9'd16;
localparam [8:0] LAST_COL_VISIBLE = FIRST_COL_VISIBLE + 9'd255;
// right border
localparam [8:0] FIRST_COL_RIGHT = 9'd272;
localparam [8:0] LAST_COL_RIGHT = 9'd279;
// HSYNC
localparam [8:0] FIRST_COL_HSYNC = 9'd292;
localparam [8:0] LAST_COL_HSYNC = 9'd316;
// left border
localparam [8:0] FIRST_COL_LEFT = 9'd8;
localparam [8:0] LAST_COL_LEFT = 9'd15;
// H-Blank region
localparam [8:0] FIRST_COL_HBLANK = 9'd274;
localparam [8:0] LAST_COL_HBLANK = LAST_COL;
// sprite evaluation
localparam [8:0] FIRST_COL_SPREV = 9'd0;
localparam [8:0] LAST_COL_SPREV = 9'd255;
// sprite rendering
localparam [8:0] FIRST_COL_SPREN = LAST_COL_RENDER + 9'd1;
localparam [8:0] LAST_COL_SPREN = 9'd339;
