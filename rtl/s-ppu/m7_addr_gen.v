//////////////////////////////////////////////////////////////////////
// Background Mode 7 video address(es) generator

module s_ppu_m7_addr_gen
  (
   input             CLK,
   input             CC,
   input             nCC,
   input             RES,
   input             HOLD,

   // debug monitor
   input [10:0]      MON_SEL,
   output            MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input [8:0]       COL,
   input [8:0]       ROW,

   input [7:0]       IO_D_I,
   output reg [7:0]  IO_D_O,
   output reg        IO_D_OE,
   input [63:52]     IO_RE,
   input [63:52]     IO_RS,
   input [51:0]      IO_WS,

   input             RENDERING,
   input             VNEXT,

   output [15:0]     SCADDR,
   input [7:0]       SCDATA,
   output [15:0]     CHADDR,
   output            OOB_TRANS
   );

`include "reg_idx.vh"
`include "timings.vh"

localparam ME = 26;

reg signed [12:0]   org_x, org_y;
reg signed [8:0]    screen_x, screen_y;
reg signed [ME:0]   vram0_x, vram0_y, vram_x, vram_xp, vram_y;

reg signed [15:0]   mpya;
reg signed [12:0]   mpyb;
reg signed [ME:0]   mpyc;

wire    mon_write_m7hofs, mon_write_m7vofs, mon_write_m7sel,
        mon_write_m7a, mon_write_m7b, mon_write_m7c, mon_write_m7d,
        mon_write_m7x, mon_write_m7y, mon_write_m7mpyc,
        mon_write_m7mpya, mon_write_m7mpyb;

//////////////////////////////////////////////////////////////////////
// Register interface

reg signed [12:0]   hofs, vofs, m7x, m7y;
reg signed [15:0]   m7a, m7b, m7c, m7d;
reg [1:0]           screen_over;
reg                 vflip, hflip;
reg [7:0]           lbuf;

always @(posedge CLK) begin
  if (!HOLD) begin
    if (IO_WS[REG_M7HOFS])
      hofs <= {IO_D_I[4:0], lbuf};
    if (IO_WS[REG_M7VOFS])
      vofs <= {IO_D_I[4:0], lbuf};
    if (IO_WS[REG_M7SEL]) begin
      screen_over <= IO_D_I[7:6];
      vflip <= IO_D_I[1];
      hflip <= IO_D_I[0];
    end
    if (IO_WS[REG_M7A])
      m7a <= {IO_D_I, lbuf};
    if (IO_WS[REG_M7B])
      m7b <= {IO_D_I, lbuf};
    if (IO_WS[REG_M7C])
      m7c <= {IO_D_I, lbuf};
    if (IO_WS[REG_M7D])
      m7d <= {IO_D_I, lbuf};
    if (IO_WS[REG_M7X])
      m7x <= {IO_D_I[4:0], lbuf};
    if (IO_WS[REG_M7Y])
      m7y <= {IO_D_I[4:0], lbuf};

    if (|IO_WS[REG_M7VOFS:REG_M7HOFS] | |IO_WS[REG_M7Y:REG_M7SEL])
      lbuf <= IO_D_I;
  end

  if (mon_write_m7hofs) begin
    if (MON_WS[0])
      hofs[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      hofs[12:8] <= MON_DIN[12:8];
  end
  else if (mon_write_m7vofs) begin
    if (MON_WS[0])
      vofs[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      vofs[12:8] <= MON_DIN[12:8];
  end
  else if (mon_write_m7sel) begin
    if (MON_WS[0]) begin
      hflip <= MON_DIN[0];
      vflip <= MON_DIN[1];
      screen_over <= MON_DIN[3:2];
    end
  end
  else if (mon_write_m7a) begin
    if (MON_WS[0])
      m7a[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      m7a[15:8] <= MON_DIN[15:8];
  end
  else if (mon_write_m7b) begin
    if (MON_WS[0])
      m7b[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      m7b[15:8] <= MON_DIN[15:8];
  end
  else if (mon_write_m7c) begin
    if (MON_WS[0])
      m7c[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      m7c[15:8] <= MON_DIN[15:8];
  end
  else if (mon_write_m7d) begin
    if (MON_WS[0])
      m7d[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      m7d[15:8] <= MON_DIN[15:8];
  end
  else if (mon_write_m7x) begin
    if (MON_WS[0])
      m7x[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      m7x[12:8] <= MON_DIN[12:8];
  end
  else if (mon_write_m7y) begin
    if (MON_WS[0])
      m7y[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      m7y[12:8] <= MON_DIN[12:8];
  end
end

always @* begin
  IO_D_O = 8'hx;
  IO_D_OE = 1'b0;

  if (IO_RE[REG_MPYL]) begin
    IO_D_O = mpyc[10:3];
    IO_D_OE = 1'b1;
  end
  else if (IO_RE[REG_MPYM]) begin
    IO_D_O = mpyc[18:11];
    IO_D_OE = 1'b1;
  end
  else if (IO_RE[REG_MPYH]) begin
    IO_D_O = mpyc[26:19];
    IO_D_OE = 1'b1;
  end
end


//////////////////////////////////////////////////////////////////////
// Address math sequencer

// X = N.0 occurs on nCC
// X = N.5 occurs on CC

wire xp0p0 = nCC & (COL == 9'd0);

wire xpnp0 = nCC & (COL <= 9'd336);
wire xpnp5 =  CC & (COL <= 9'd336);
wire xm4p0 = nCC & (COL == 9'd337);
wire xm3p5 =  CC & (COL == 9'd337);
wire xm3p0 = nCC & (COL == 9'd338);
wire xm2p5 =  CC & (COL == 9'd338);
wire xm2p0 = nCC & (COL == 9'd339);
wire xm1p5 =  CC & (COL == 9'd339);
wire xm1p0 = nCC & (COL == 9'd340);
wire xm0p5 =  CC & (COL == 9'd340);


//////////////////////////////////////////////////////////////////////
// Multiplier

always @(posedge CLK) begin
  if (nCC | CC) begin
    if (RENDERING) begin
      if (xm3p0 | xm1p5)
        mpyb <= org_x;
      else if (xm2p5 | xm2p0)
        mpyb <= org_y;
      else if (xpnp0 | xpnp5)
        mpyb <= {4'b0, screen_x};
      else if (xm1p0 | xm0p5)
        mpyb <= {4'b0, screen_y};

      if (xm3p0 | xpnp0)
        mpya <= m7a;
      else if (xm2p0 | xm1p0)
        mpya <= m7b;
      else if (xm1p5 | xpnp5)
        mpya <= m7c;
      else if (xm2p5 | xm0p5)
        mpya <= m7d;
    end
    else begin
      mpya <= m7a;
      // Left-shift by 3 to align with MPYL/M/H.
      mpyb <= {{3{m7b[15]}}, m7b[14:8], 3'b0};
    end
  end

  if (mon_write_m7mpya) begin
    if (MON_WS[0])
      mpya[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      mpya[15:8] <= MON_DIN[15:8];
  end
  else if (mon_write_m7mpyb) begin
    if (MON_WS[0])
      mpyb[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      mpyb[12:8] <= MON_DIN[12:8];
  end
end

always @(posedge CLK) begin
  if (!HOLD)
    mpyc <= mpya * mpyb;

  if (mon_write_m7mpyc) begin
    if (MON_WS[0])
      mpyc[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      mpyc[15:8] <= MON_DIN[15:8];
    if (MON_WS[2])
      mpyc[23:16] <= MON_DIN[23:16];
    if (MON_WS[3])
      mpyc[ME:24] <= MON_DIN[ME:24];
  end
end


//////////////////////////////////////////////////////////////////////
// Adder (and friends)

`define sign_extend(v, im, om) { {(om-im+1){v[im]}}, v[im-1:0] }

always @(posedge CLK) if (nCC | CC) begin
  if (RENDERING) begin
    if (xm4p0)
      org_x <= hofs - m7x;
    else if (xm3p5) begin
      org_y <= vofs - m7y;
      vram0_x <= 0;
      vram0_x[ME:8] <= `sign_extend(m7x, 12, ME-8);
    end
    else if (xm3p0) begin
      vram0_y <= 0;
      vram0_y[ME:8] <= `sign_extend(m7y, 12, ME-8);
    end
    else if (xm2p5 | xm1p5 | xm0p5)
      vram0_x[ME:6] <= vram0_x[ME:6] + mpyc[ME:6];
    else if (xm2p0 | xm1p0 | xp0p0)
      vram0_y[ME:6] <= vram0_y[ME:6] + mpyc[ME:6];
    else if (xpnp5)
      vram_xp <= vram0_x + mpyc;
    else if (xpnp0) begin
      vram_x <= vram_xp;        // align vram_x with vram_y
      vram_y <= vram0_y + mpyc;
    end
  end
end

always @* begin
  screen_x = COL;
  if (hflip)
    screen_x[7:0] = ~screen_x[7:0];
end

// By "design", VNEXT assertion (masked by mosaic) overlaps with
// screen_y being input to the multiplier. Use it to control when to
// use the recomputed or prior screen_y.

reg signed [8:0] screen_y_d, screen_y_next;

always @* begin
  screen_y_next = ROW + 9'd1;      // because it's used in the next row
  if (ROW == LAST_ROW)
    screen_y_next = 9'd0;
  if (vflip)
    screen_y_next[7:0] = ~screen_y_next[7:0];
end

always @* begin
  screen_y = screen_y_d;
  if (VNEXT)
    screen_y = screen_y_next;
end

always @(posedge CLK) if (CC)
  if (VNEXT)
    screen_y_d <= screen_y;


//////////////////////////////////////////////////////////////////////
// SCADDR generator

wire [2:0] fhs, fvs;
wire       oob;                 // coordinates are out of bounds

// Sub-counters of va*
localparam HS = 0, HE = 12;     // H counter
localparam VS = 13, VE = 25;    // V counter
// Sub-sub-counters (for 8x8 tiles)
localparam FHS_ = 0, FHE = 2;   // fine H scroll
localparam HTS = 3, HTE = 9;    // H tile index
localparam FVS_ = 13, FVE = 15; // fine V scroll
localparam VTS = 16, VTE = 22;  // V tile index

wire [25:0] vas;
assign vas[HE:HS] = {|vram_x[ME:20], vram_x[19:8]};
assign vas[VE:VS] = {|vram_y[ME:20], vram_y[19:8]};

// Compute the final address.
assign SCADDR = {2'b0, vas[VTE:VTS], vas[HTE:HTS]};
assign fhs = vas[FHE:FHS_];
assign fvs = vas[FVE:FVS_];
assign oob = |vas[HE:HTE+1] | |vas[VE:VTE+1];


//////////////////////////////////////////////////////////////////////
// CHADDR generator

reg [2:0] fhs_d, fvs_d;
reg       oob_d;
reg [2:0] chr_x, chr_y;
reg       chr_oob;
reg [7:0] chr_sel;

always @(posedge CLK) if (nCC) begin
  // Delay inputs to align with SC data arrival.
  fhs_d <= fhs;
  fvs_d <= fvs;
  oob_d <= oob;

  chr_x <= fhs_d;
  chr_y <= fvs_d;

  chr_sel <= (oob_d & screen_over[1]) ? 8'd0 : SCDATA;
  chr_oob <= oob_d;
end

// Compute the final address.
assign CHADDR = {2'b00, chr_sel, chr_y, chr_x};
assign OOB_TRANS = chr_oob & (screen_over == 2'b10);


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_ack;
reg        mon_sel_m7hofs, mon_sel_m7vofs, mon_sel_m7sel, mon_sel_m7a,
           mon_sel_m7b, mon_sel_m7c, mon_sel_m7d, mon_sel_m7x,
           mon_sel_m7y, mon_sel_m7mpyc, mon_sel_m7mpya, mon_sel_m7mpyb;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  mon_ack = 1'b1;
  mon_sel_m7hofs = 1'b0;
  mon_sel_m7vofs = 1'b0;
  mon_sel_m7sel = 1'b0;
  mon_sel_m7a = 1'b0;
  mon_sel_m7b = 1'b0;
  mon_sel_m7c = 1'b0;
  mon_sel_m7d = 1'b0;
  mon_sel_m7x = 1'b0;
  mon_sel_m7y = 1'b0;
  mon_sel_m7mpyc = 1'b0;
  mon_sel_m7mpya = 1'b0;
  mon_sel_m7mpyb = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL[10:2])          //``SUBREGS PPU
    9'h28: begin                //``REG M7HOFS
      mon_sel_m7hofs = 1'b1;
      mon_dout[12:0] = hofs;    //``NO_FIELD
    end
    9'h29: begin                //``REG M7VOFS
      mon_sel_m7vofs = 1'b1;
      mon_dout[12:0] = vofs;    //``NO_FIELD
    end
    9'h2A: begin                //``REG M7SEL
      mon_sel_m7sel = 1'b1;
      mon_dout[0] = hflip;
      mon_dout[1] = vflip;
      mon_dout[3:2] = screen_over; //``FIELD SCOVER
    end
    9'h2B: begin
      mon_sel_m7a = 1'b1;
      mon_dout[15:0] = m7a;
    end
    9'h2C: begin
      mon_sel_m7b = 1'b1;
      mon_dout[15:0] = m7b;
    end
    9'h2D: begin
      mon_sel_m7c = 1'b1;
      mon_dout[15:0] = m7c;
    end
    9'h2E: begin
      mon_sel_m7d = 1'b1;
      mon_dout[15:0] = m7d;
    end
    9'h2F: begin
      mon_sel_m7x = 1'b1;
      mon_dout[12:0] = m7x;
    end
    9'h30: begin
      mon_sel_m7y = 1'b1;
      mon_dout[12:0] = m7y;
    end
    9'h31: begin                //``REG M7MPYC
      mon_sel_m7mpyc = 1'b1;
      mon_dout[ME:0] = mpyc;    //``NO_FIELD
    end
    9'h32: begin                //``REG M7MPYA
      mon_sel_m7mpya = 1'b1;
      mon_dout[15:0] = mpya;    //``NO_FIELD
    end
    9'h33: begin                //``REG M7MPYB
      mon_sel_m7mpyb = 1'b1;
      mon_dout[12:0] = mpyb;    //``NO_FIELD
    end
    9'h37: mon_dout[25:0] = vas; //``REG M7VAS
    default: mon_ack = 1'b0;
  endcase
end

// TODO: Add lbuf

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_m7hofs = mon_write & mon_sel_m7hofs;
assign mon_write_m7vofs = mon_write & mon_sel_m7vofs;
assign mon_write_m7sel = mon_write & mon_sel_m7sel;
assign mon_write_m7a = mon_write & mon_sel_m7a;
assign mon_write_m7b = mon_write & mon_sel_m7b;
assign mon_write_m7c = mon_write & mon_sel_m7c;
assign mon_write_m7d = mon_write & mon_sel_m7d;
assign mon_write_m7x = mon_write & mon_sel_m7x;
assign mon_write_m7y = mon_write & mon_sel_m7y;
assign mon_write_m7mpyc = mon_write & mon_sel_m7mpyc;
assign mon_write_m7mpya = mon_write & mon_sel_m7mpya;
assign mon_write_m7mpyb = mon_write & mon_sel_m7mpyb;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK && MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end

assign MON_ACK = mon_ack;

endmodule
