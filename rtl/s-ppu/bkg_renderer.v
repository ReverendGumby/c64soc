module s_ppu_bkg_renderer
  (
   input            CLK,
   input            CC,
   input            nCC,

   input [2:0]      BGMODE,
   input [1:0]      BGIDX,
   input [1:0]      BMAX,
   input [8:0]      COL,
   input [8:0]      ROW,
   input            BKG_EN,
   input [2:0]      FHS,

   input [15:0]     PAR,
   input [15:0]     PDR,

   input            SHIFT,
   input            LOAD,
   input            FETCH_NT,
   input            FETCH_PT,
   input [1:0]      PT_BSEL,
   input            HNEXT,

   output reg [8:0] PD,
   output reg [8:0] COL_O,
   output reg [8:0] ROW_O
   );

`include "swab.vh"

//////////////////////////////////////////////////////////////////////
// Tile pattern data latches and shifters
// These select the lower 2-8 color bits.

wire [15:0] tile0, tile1, tile2, tile3, tile4, tile5, tile6, tile7;
reg [15:0] pdr_in;
reg        fetch_pt_rd_d;
reg [1:0]  pt_bsel_d;
reg        shift, load;
wire       hflip;

bkg_tile_latch_shifter #(0) tls01
  (
   .CLK(CLK),
   .CC(CC),
   .BMAX(BMAX),
   .PDR(pdr_in),
   .SHIFT(shift),
   .LOAD(load),
   .FETCH_PT(fetch_pt_rd_d),
   .PT_BSEL(pt_bsel_d),
   .TILE0(tile0),
   .TILE1(tile1)
   );

bkg_tile_latch_shifter #(1) tls23
  (
   .CLK(CLK),
   .CC(CC),
   .BMAX(BMAX),
   .PDR(pdr_in),
   .SHIFT(shift),
   .LOAD(load),
   .FETCH_PT(fetch_pt_rd_d),
   .PT_BSEL(pt_bsel_d),
   .TILE0(tile2),
   .TILE1(tile3)
   );

bkg_tile_latch_shifter #(2) tls45
  (
   .CLK(CLK),
   .CC(CC),
   .BMAX(BMAX),
   .PDR(pdr_in),
   .SHIFT(shift),
   .LOAD(load),
   .FETCH_PT(fetch_pt_rd_d),
   .PT_BSEL(pt_bsel_d),
   .TILE0(tile4),
   .TILE1(tile5)
   );

bkg_tile_latch_shifter #(3) tls67
  (
   .CLK(CLK),
   .CC(CC),
   .BMAX(BMAX),
   .PDR(pdr_in),
   .SHIFT(shift),
   .LOAD(load),
   .FETCH_PT(fetch_pt_rd_d),
   .PT_BSEL(pt_bsel_d),
   .TILE0(tile6),
   .TILE1(tile7)
   );

always @* begin
  // Pattern data is rendered LSB first if H-FLIP is set, MSB first
  // if clear.
  if (hflip)
    pdr_in = PDR;
  else
    pdr_in = { swab(PDR[15:8]), swab(PDR[7:0]) };
end

always @(posedge CLK) if (CC) begin
  fetch_pt_rd_d <= FETCH_PT;
  if (FETCH_PT) begin
    pt_bsel_d <= PT_BSEL;
  end
end


//////////////////////////////////////////////////////////////////////
// Name data latches and shifters
// Shifters output the priority and middle 3 color bits.
// Latches temporarily capture the above, plus tile H/V flip.

reg [7:0] name0, name1, name2, name3; // shifters
reg [15:10] name_buf;             // latches
reg       fetch_nt_rd_d;
reg [3:0] name_in;

always @(posedge CLK) if (CC) begin
  // Shift the shifters.
  if (shift) begin
    name0 <= {name_in[0], name0[7:1]};
    name1 <= {name_in[1], name1[7:1]};
    name2 <= {name_in[2], name2[7:1]};
    name3 <= {name_in[3], name3[7:1]};
  end

  // On NT data fetch, buffer it.
  fetch_nt_rd_d <= FETCH_NT;
  if (fetch_nt_rd_d)
    name_buf <= PAR[15:10];

  // Sample the bits to input to shifters.
  if (load) begin
    name_in <= name_buf[13:10];
  end
end

assign hflip = name_buf[14];


//////////////////////////////////////////////////////////////////////
// Pixel data
//
// Combine tile, name palette data, and BG index to get the pixel data
// (CGRAM entry) value. BGMODE controls how to combine them. Tile data
// 0 selects transparent pixels: output 'h00.

wire [3:0] fhs4 = {1'b0, FHS};
wire [7:0] pd_tile = {tile7[fhs4], tile6[fhs4], tile5[fhs4], tile4[fhs4],
                      tile3[fhs4], tile2[fhs4], tile1[fhs4], tile0[fhs4]};
wire [2:0] pd_color = {name2[FHS], name1[FHS], name0[FHS]};
wire       pd_pri = name3[FHS];
reg [7:0]  pd_px; // palette index
reg [7:0]  pd_in;

always @* begin
  pd_px = 8'bx;
  case (BMAX)
    2'b00: pd_px = {3'b0, pd_color, pd_tile[1:0]}; // 2bpp
    2'b01: pd_px = {1'b0, pd_color, pd_tile[3:0]}; // 4bpp
    2'b11: pd_px = pd_tile[7:0];                   // 8bpp
    default: ;
  endcase
end

always @* begin
  if (~|pd_tile)                // transparent
    pd_in = 8'h00;
  else begin
    pd_in = pd_px[7:0];
    if (BGMODE == 3'd0)
      pd_in[6:5] = BGIDX;
  end
end

reg [8:0] col [0:1];
reg [8:0] row [0:1];
reg       bkg_en, bkg_en_p;
reg       load_p, shift_p;
reg       hnext, hnext_p;

always @(posedge CLK) if (CC) begin
  // load needs a 2x CC delay to avoid overlap with fetch_pt_rd_d
  // Everything else needs to align with load.
  load_p <= LOAD;
  load <= load_p;

  shift_p <= SHIFT;
  shift <= shift_p;

  bkg_en_p <= BKG_EN;
  bkg_en <= bkg_en_p;

  hnext_p <= HNEXT;
  hnext <= hnext_p;

  // Pipeline delay is 3x CC.
  col[0] <= COL;
  row[0] <= ROW;
  col[1] <= col[0];
  row[1] <= row[0];

  if (hnext)
    PD <= bkg_en ? {pd_pri, pd_in} : 9'h0;
  COL_O <= col[1];
  ROW_O <= row[1];
end

endmodule
