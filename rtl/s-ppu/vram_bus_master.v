module s_ppu_vram_bus_master
  (
   input             CLK,
   input             CC,
   input             nCC,
   input             RES,
   input             HOLD,

   // debug monitor
   input [10:0]      MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input [8:0]       COL,
   input [8:0]       ROW,

   input [7:0]       IO_D_I,
   output reg [7:0]  IO_D_O,
   output reg        IO_D_OE,
   input [63:52]     IO_RE,
   input [63:52]     IO_RS,
   input [51:0]      IO_WS,

   input [2:0]       BGMODE,
   input [3:0]       BGSIZE,

   input             RENDERING,
   input             HINC,
   input [3:0]       VINC,
   input             HCLR,
   input             VCLR,
   input             FETCH,
   input             FETCH_NT,
   input             FETCH_PT,
   input             SPR_FETCH,
   input             SPR_FETCH_PTL,
   input [1:0]       NT_BSEL,
   input             NT_OSEL,
   input [1:0]       PT_BSEL,
   input [1:0]       PT_BMAX,

   output [11:0]     BGFHS,
   output [15:0]     PAR,
   output [15:0]     PDR,

   // Sprite renderer
   input [2:0]       OBJSEL_BA,
   input [1:0]       OBJSEL_N,
   input [8:0]       SPR_CHR,
   input [2:0]       SPR_Y,

   // VRAM address / data bus A, low byte
   output [15:0]     VAA,
   input [7:0]       VDA_I,
   output [7:0]      VDA_O,
   output            nVARD,
   output            nVAWR,

   // VRAM address / data bus B, low byte
   output [15:0]     VAB,
   input [7:0]       VDB_I,
   output [7:0]      VDB_O,
   output            nVBRD,
   output            nVBWR
   );

`include "reg_idx.vh"

reg [15:0]  par;


//////////////////////////////////////////////////////////////////////
// Offset-per-tile logic

wire        opt_en;
wire        opt_fetch;
wire        opt_osel;
reg         opt_fetch_d;
reg         opt_osel_d, opt_osel_d2;
reg         opt_dvalid;
reg [9:0]   opt_offset;
reg [2:1]   opt_bgnen;
reg         opt_hv;

assign opt_en = |BGMODE[2:1] & ~BGMODE[0]; // modes 2,4,6
assign opt_fetch = opt_en & FETCH_NT & (NT_BSEL == 2'd2 /*BG3*/);
assign opt_osel = NT_OSEL;

always @(posedge CLK) if (CC) begin  
  opt_fetch_d <= opt_fetch;
  opt_osel_d <= opt_osel;
end

always @(posedge CLK) if (nCC) begin
  opt_dvalid <= opt_fetch_d;
  opt_osel_d2 <= opt_osel_d;
end

always @* begin
  if (opt_dvalid) begin
    opt_offset = par[9:0];
    opt_bgnen = par[14:13];
    opt_hv = (BGMODE == 3'd4) ? par[15] : opt_osel_d2;
  end
  else begin
    opt_offset = 10'bx;
    opt_bgnen = 2'bx;
    opt_hv = 1'bx;
  end
end


//////////////////////////////////////////////////////////////////////
// Background video address generators

wire [15:0] bgscaddr [1:4];
wire [15:0] bgchbase [1:4];
wire [2:0]  bgfhs [1:4];
wire [2:0]  bgfvs [1:4];
wire        bgtho [1:4];
wire        bgtvo [1:4];
wire        mon_ack_bg [1:4];
wire [31:0] mon_dout_bg [1:4];
wire        mon_valid_bg [1:4];

genvar i;
generate
  for (i = 1; i <= 4; i = i + 1) begin :bg

    s_ppu_bkg_addr_gen #(.BGIDX(i)) bkg_addr_gen
      (
       .CLK(CLK),
       .CC(CC),
       .nCC(nCC),
       .RES(RES),
       .HOLD(HOLD),

       .MON_SEL(MON_SEL),
       .MON_ACK(mon_ack_bg[i]),
       .MON_READY(MON_READY),
       .MON_DOUT(mon_dout_bg[i]),
       .MON_VALID(mon_valid_bg[i]),
       .MON_WS(MON_WS),
       .MON_DIN(MON_DIN),

       .COL(COL),
       .ROW(ROW),

       .IO_D_I(IO_D_I),
       .IO_WS(IO_WS),

       .BGMODE(BGMODE),
       .BGSIZE(BGSIZE[i-1]),

       .HINC(HINC),
       .VINC(VINC[i-1]),
       .HCLR(HCLR),
       .VCLR(VCLR),

       .OPT_EN(opt_en),
       .OPT_FETCH(opt_fetch),
       .OPT_OSEL(opt_osel),
       .OPT_DVALID(opt_dvalid),
       .OPT_OFFSET(opt_offset),
       .OPT_BGNEN(opt_bgnen),
       .OPT_HV(opt_hv),

       .SCADDR(bgscaddr[i]),
       .CHBASE(bgchbase[i]),
       .FHS(bgfhs[i]),
       .FVS(bgfvs[i]),
       .THO(bgtho[i]),
       .TVO(bgtvo[i])
       );

    assign BGFHS[(i-1)*3+2:(i-1)*3] = bgfhs[i];

  end
endgenerate


//////////////////////////////////////////////////////////////////////
// Background Mode 7 video address generators

wire [7:0]  io_d_o_m7;
wire        io_d_oe_m7;

wire [15:0] m7scaddr, m7chaddr;
wire [2:0]  m7fvs;
wire        mon_ack_m7;
wire [31:0] mon_dout_m7;
wire        mon_valid_m7;

wire        m7 = (BGMODE == 3'd7);
wire        m7_fetch = m7 & FETCH;
wire        m7_rendering = m7 & RENDERING;
wire        m7_vnext = VCLR | VINC[0];
wire        m7_oob_trans;
wire        m7_zero_pdr;

s_ppu_m7_addr_gen m7_addr_gen
  (
   .CLK(CLK),
   .CC(CC),
   .nCC(nCC),
   .RES(RES),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_m7),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_m7),
   .MON_VALID(mon_valid_m7),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .COL(COL),
   .ROW(ROW),

   .IO_D_I(IO_D_I),
   .IO_D_O(io_d_o_m7),
   .IO_D_OE(io_d_oe_m7),
   .IO_RE(IO_RE),
   .IO_RS(IO_RS),
   .IO_WS(IO_WS),

   .RENDERING(m7_rendering),
   .VNEXT(m7_vnext),

   .SCADDR(m7scaddr),
   .SCDATA(PAR[7:0]),
   .CHADDR(m7chaddr),
   .OOB_TRANS(m7_oob_trans)
   );

assign m7_zero_pdr = m7_fetch & FETCH_PT & m7_oob_trans;


//////////////////////////////////////////////////////////////////////
// Picture address (SC DATA) register

reg        fetch_nt_d;
reg [9:0]  chr;                 // character code number
reg        bgsize, tho, tvo;
wire [9:0] chr_off;             // 16x16 tile offset
wire [9:0] tile_sel;
wire       hflip, vflip;

always @(posedge CLK) if (nCC) begin
  fetch_nt_d <= FETCH_NT;
  if (fetch_nt_d)
    par <= {VDB_I, VDA_I};
end

always @* begin
  {bgsize, tho, tvo} = 3'bx;
  case (NT_BSEL)
    2'b00: {bgsize, tho, tvo} = {BGSIZE[0], bgtho[1], bgtvo[1]};
    2'b01: {bgsize, tho, tvo} = {BGSIZE[1], bgtho[2], bgtvo[2]};
    2'b10: {bgsize, tho, tvo} = {BGSIZE[2], bgtho[3], bgtvo[3]};
    2'b11: {bgsize, tho, tvo} = {BGSIZE[3], bgtho[4], bgtvo[4]};
    default: ;
  endcase
  tho = tho ^ (bgsize & hflip);
  tvo = tvo ^ (bgsize & vflip);
end

assign chr_off = {5'b0, tvo, 3'b0, tho};
always @* begin
  chr = par[9:0] + chr_off;
end

assign PAR = par;
assign tile_sel = SPR_FETCH ? {1'b0, SPR_CHR} : chr[9:0];
assign hflip = par[14];
assign vflip = par[15];


//////////////////////////////////////////////////////////////////////
// Picture data (CHR DATA) data register

reg [15:0] pdr;
wire       fetch_pt;
reg        fetch_pt_rd_d;

assign fetch_pt = FETCH_PT | SPR_FETCH;

always @(posedge CLK) if (nCC) begin
  fetch_pt_rd_d <= fetch_pt;
  if (fetch_pt_rd_d)
    pdr <= m7_zero_pdr ? 16'b0 : {VDB_I, VDA_I};
end

assign PDR = pdr;


//////////////////////////////////////////////////////////////////////
// CPU-directed VRAM read / write

wire [15:0] vmadd;
wire [15:0] vmdata;
reg [15:0]  rdbuf;
wire        read_en, write_en, write_hi;

always @(posedge CLK) if (nCC) begin
  if (read_en & ~RENDERING)
    rdbuf <= {VDB_I, VDA_I};
end

s_ppu_vram_cpu_iface cpu_iface
  (
   .CLK(CLK),
   .CC(CC),
   .nCC(nCC),
   .RES(RES),
   .HOLD(HOLD),

   // TODO: add monitor

   .IO_D_I(IO_D_I),
   .IO_RE(IO_RE),
   .IO_RS(IO_RS),
   .IO_WS(IO_WS),

   .VMADD(vmadd),
   .VMDATA(vmdata),
   .READ_EN(read_en),
   .WRITE_EN(write_en),
   .WRITE_HI(write_hi)
   );


//////////////////////////////////////////////////////////////////////
// Video address / data bus output

reg [15:0] vaa, vab, m06rva, m7rvaa, m7rvab;
reg [15:0] scaddr;
reg [15:0] chbase, chaddr;
reg [2:0]  fvs;                 // fine V scroll
wire [2:0] pat_y;               // Y offset (V-FLIP applied)

reg [15:0]  choff;
wire        read, write, write_a, write_b;

always @* begin
  case (NT_BSEL)
    2'b00: scaddr = bgscaddr[1];
    2'b01: scaddr = bgscaddr[2];
    2'b10: scaddr = bgscaddr[3];
    2'b11: scaddr = bgscaddr[4];
    default: scaddr = 16'bx;
  endcase
end

always @* begin
  if (SPR_FETCH)
    chbase = {OBJSEL_BA[2:0], 13'b0};
  else
    case (NT_BSEL)
      2'b00: chbase = bgchbase[1];
      2'b01: chbase = bgchbase[2];
      2'b10: chbase = bgchbase[3];
      2'b11: chbase = bgchbase[4];
      default: chbase = 16'bx;
    endcase
end

always @* begin
  case (NT_BSEL)
    2'b00: fvs = bgfvs[1];
    2'b01: fvs = bgfvs[2];
    2'b10: fvs = bgfvs[3];
    2'b11: fvs = bgfvs[4];
    default: fvs = 3'bx;
  endcase
end

assign pat_y = vflip ? (3'd7 - fvs) : fvs;

always @* begin
  if (SPR_FETCH) begin
    choff = {4'b0, SPR_CHR[7:0], ~SPR_FETCH_PTL, SPR_Y}; // 4bpp
    choff[13:12] = OBJSEL_N + SPR_CHR[8];
  end
  else begin
    choff = {13'b0, pat_y};
    case (PT_BMAX)
      2'b00: choff[12:3] = tile_sel;                 // 2bpp
      2'b01: choff[13:3] = {tile_sel, PT_BSEL[0]};   // 4bpp
      2'b11: choff[14:3] = {tile_sel, PT_BSEL[1:0]}; // 8bpp
      default: ;
    endcase
  end
end

always @*
  chaddr = chbase + choff;

always @(posedge CLK) if (CC) begin
  m06rva <= scaddr;             // default to FETCH_NT
  if (fetch_pt)
    m06rva <= chaddr;

  m7rvaa <= m7scaddr;
  m7rvab <= m7chaddr;
end

always @* begin
  vaa = 16'hx;
  if (RENDERING)
    vaa = m7_fetch ? m7rvaa : m06rva;
  else if (read_en | write_en)
    vaa = vmadd;
end

always @*
  vab = (RENDERING & m7_fetch) ? m7rvab : vaa;

reg render_rd;
always @(posedge CLK) if (CC)
  render_rd <= FETCH | SPR_FETCH;

assign read = render_rd | read_en;
assign write = write_en & ~RENDERING;
assign write_a = write & ~write_hi;
assign write_b = write & write_hi;

assign VAA = vaa;
assign VDA_O = write_en ? vmdata[7:0] : 8'hx;
assign nVARD = ~read;
assign nVAWR = ~write_a;
assign VAB = vab;
assign VDB_O = write_en ? vmdata[15:8] : 8'hx;
assign nVBRD = ~read;
assign nVBWR = ~write_b;


//////////////////////////////////////////////////////////////////////
// Internal I/O read data bus interface

always @* begin
  IO_D_OE = 1'b1;

  if (io_d_oe_m7)
    IO_D_O = io_d_o_m7;
  else if (IO_RE[REG_RDVRAML])
    IO_D_O = rdbuf[7:0];
  else if (IO_RE[REG_RDVRAMH])
    IO_D_O = rdbuf[15:8];
  else begin
    IO_D_O = 8'hx;
    IO_D_OE = 1'b0;
  end
end


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

always @* begin
  if (mon_ack_bg[1]) begin
    MON_ACK = mon_ack_bg[1];
    MON_VALID = mon_valid_bg[1];
    MON_DOUT = mon_dout_bg[1];
  end
  else if (mon_ack_bg[2]) begin
    MON_ACK = mon_ack_bg[2];
    MON_VALID = mon_valid_bg[2];
    MON_DOUT = mon_dout_bg[2];
  end
  else if (mon_ack_bg[3]) begin
    MON_ACK = mon_ack_bg[3];
    MON_VALID = mon_valid_bg[3];
    MON_DOUT = mon_dout_bg[3];
  end
  else if (mon_ack_bg[4]) begin
    MON_ACK = mon_ack_bg[4];
    MON_VALID = mon_valid_bg[4];
    MON_DOUT = mon_dout_bg[4];
  end
  else if (mon_ack_m7) begin
    MON_ACK = mon_ack_m7;
    MON_VALID = mon_valid_m7;
    MON_DOUT = mon_dout_m7;
  end
  else begin
    MON_ACK = 1'b0;
    MON_VALID = 1'b0;
    MON_DOUT = 32'h0;
  end
end

endmodule
