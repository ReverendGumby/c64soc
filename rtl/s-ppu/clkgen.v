module s_ppu_clkgen
  (
   input  CLK_RESET,
   input  CLK,
   input  CLKEN,
   input  CLK_HOLD,

   output CC,
   output nCC
   );

reg [1:0] div4_clk;
reg       reset;

localparam [1:0] div4_clk_init = 2'b00;

initial begin
  div4_clk = div4_clk_init;
  reset = 1'b1;
end

always @(posedge CLK) if (CLKEN)
  reset <= CLK_RESET;

always @(posedge CLK) if (CLKEN)
  if (reset)
    div4_clk <= div4_clk_init;
  else if (~CLK_HOLD)
    div4_clk[1:0] <= div4_clk + 1'b1;

assign CC  = ~reset & ~CLK_HOLD & CLKEN & (div4_clk == 2'b00);
assign nCC = ~reset & ~CLK_HOLD & CLKEN & (div4_clk == 2'b10);

endmodule
