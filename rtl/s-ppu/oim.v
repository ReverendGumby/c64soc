// OIM (Object Index Memory) stores the sprite indicies that result
// from sprite evaluation. OIM has stack semantics (FILO) and separate
// read+write ports. It is addressed by an internal 5-bit register
// we'll call OIA.

module s_ppu_oim
  (
   input             CLK,
   input             CC,
   input             HOLD,

   input [10:0]      MON_SEL,
   output            MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input             CLR,
   output            EMPTY,
   output            FULL,
   input [6:0]       WD,
   input             WE,
   output [6:0]      RD,
   input             RE
   );

reg [6:0] rbuf, drbuf, wbuf;
reg [4:0] ptr, dptr;
reg [5:0] oia;

wire      mon_write_oims, mon_write_oima;


//////////////////////////////////////////////////////////////////////
// OIM: 32 x 7: OBJ index (OAML address / 2)

reg [6:0] mem [0:31];

always @* ptr = oia[4:0];
always @* wbuf = WD;

always @(posedge CLK) begin
  if (!HOLD) begin
    rbuf <= mem[ptr];
    if (WE)
      mem[ptr] <= wbuf;
  end
end

always @(posedge CLK) begin
  drbuf <= mem[dptr];
  if (mon_write_oima)
    mem[dptr] <= MON_DIN[6:0];
end

assign RD = rbuf;


//////////////////////////////////////////////////////////////////////
// OIA: stack pointer and status

always @(posedge CLK) begin
  if (CC) begin
    if (CLR)
      oia <= 6'd0;
    else if (WE & ~FULL)
      oia <= oia + 1'd1;
    else if (RE & ~EMPTY)
      oia <= oia - 1'd1;
  end

  if (mon_write_oims)
    if (MON_WS[0])
      oia <= MON_DIN[5:0];
end

assign EMPTY = ~|oia;
assign FULL = &oia[5];


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_ack;
reg        mon_sel_oims;
wire       mon_sel_oima;
wire       mon_write;
reg        mon_ready_d;

always @* dptr = (MON_SEL[4:0] - 5'h10);

always @* begin
  mon_dout = 32'h0;
  mon_ack = 1'b1;
  mon_sel_oims = 1'b0;

  if (mon_sel_oima) begin
    mon_dout = {4{1'b0, drbuf}};
  end
  else begin
    //``REGION_REGS EMUBUS_CTRL
    case (MON_SEL[10:2])        //``SUBREGS PPU
      9'h40: begin              //``REG OIMS
        mon_sel_oims = 1'b1;
        mon_dout[5:0] = oia;
        mon_dout[8] = EMPTY;
        mon_dout[9] = FULL;
      end
      default: mon_ack = 1'b0;
    endcase
  end
end

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_oims = mon_write & mon_sel_oims;
assign mon_write_oima = mon_write & mon_sel_oima;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  MON_DOUT <= mon_dout;
  mon_ready_d <= MON_READY;

  if (mon_sel_oims)
    MON_VALID <= MON_READY;
  else if (mon_sel_oima)
    MON_VALID <= MON_READY & mon_ready_d; // need 2xCLK to read OIM
end

//``REGION_REGS EMUBUS_CTRL
//``SUBREGS PPU_MON_SEL
assign mon_sel_oima = (MON_SEL >= 11'h110 && MON_SEL <= 11'h12F); //``REG_ARRAY OIM
//``REG_END

assign MON_ACK = mon_ack;

endmodule
