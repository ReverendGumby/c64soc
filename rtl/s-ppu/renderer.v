module s_ppu_renderer
  (
   input             CLK,
   input             CC,
   input             nCC,
   input             RES,
   input             HOLD,

   input [10:0]      MON_SEL,
   output            MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input [8:0]       COL,
   input [8:0]       ROW,
   input             FIELD,

   input [11:0]      BGFHS,
   input [15:0]      PAR,
   input [15:0]      PDR,

   input [7:0]       IO_D_I,
   output reg [7:0]  IO_D_O,
   output reg        IO_D_OE,
   input [51:0]      IO_WS,

   // OLB reader controls
   output [7:0]      OLB_RA,
   input [8:0]       OLB_RD,
   output            OLB_RE,

   input [5:0]       WIN,

   output [7:0]      PAL_PD,
   input [14:0]      PAL_RGB,

   output [3:0]      BRIGHT,
   output [2:0]      BGMODE,
   output [3:0]      BGSIZE,

   output            RENDER_EN,
   output            RENDERING,
   output            HINC,
   output [3:0]      VINC,
   output            HCLR,
   output            VCLR,
   output            FETCH,
   output            FETCH_NT,
   output            FETCH_PT,
   output            SPR_CLR,
   output            SPR_START,
   output            SPR_END,
   output            SPR_FETCH,
   output            SPR_FETCH_PTL,
   output [1:0]      NT_BSEL,
   output            NT_OSEL,
   output [1:0]      PT_BSEL,
   output [1:0]      PT_BMAX,

   output            HBLANK,
   output            VBLANK,

   output [8:0]      SYNC_COL_O,
   output [8:0]      SYNC_ROW_O,
   output            SYNC_BLANK,

   output reg [14:0] RGB
   //output reg [8:0]  COL_O,
   //output reg [8:0]  ROW_O
   );

`include "timings.vh"
`include "reg_idx.vh"

localparam [8:0] SPR_PDLY = 3;  // pipeline delay

wire       mon_write_ren0, mon_write_ren1, mon_write_ren2;


//////////////////////////////////////////////////////////////////////
// Registers

reg [3:0] inidisp_bright;
reg       inidisp_forced_blank;
reg [2:0] bgmode;
reg       bgmode_bg3_pri;
reg [3:0] bgmode_bg_size;       // 16x16 tiles
reg [3:0] mosaic_sz;
reg [3:0] mosaic_bkg_en;
reg       bg_vdir_disp;
reg [4:0] tm, ts;               // main/sub screen designation
reg [4:0] tmw, tsw;             // main/sub screen layer window enable
reg       clr_direct;           // direct color mode
reg       clr_add_sub;          // addend: 0=fixed, 1=subscreen
reg [1:0] clr_sub_win_rgn;      // subscreen color window transparent:
                                // 0=always, 1=inside, 2=outside, 3=never
reg [1:0] clr_main_win_rgn;     // main screen color window black
reg [5:0] clr_math_en;          // enable math by main screen source:
                                // 0-3=BG1-4, 4=OBJ, 5=back
reg       clr_half;             // 1/2 of color data designation
reg       clr_op_sub;           // color data addition/subtraction select
reg [14:0] clr_fixed;           // subscreen backdrop RGB color

always @(posedge CLK) begin
  if (RES) begin
    /* if cold reset... 
    bgmode <= 3'b111;
     */
    inidisp_forced_blank <= 1'b1;
    bg_vdir_disp <= 1'b0;
  end
  else if (!HOLD) begin
    if (IO_WS[REG_INIDISP]) begin
      inidisp_forced_blank <= IO_D_I[7];
      inidisp_bright <= IO_D_I[3:0];
    end
    if (IO_WS[REG_BGMODE]) begin
      bgmode <= IO_D_I[2:0];
      bgmode_bg3_pri <= IO_D_I[3];
      bgmode_bg_size <= IO_D_I[7:4];
    end
    if (IO_WS[REG_MOSAIC]) begin
      mosaic_sz <= IO_D_I[7:4];
      mosaic_bkg_en <= IO_D_I[3:0];
    end
    if (IO_WS[REG_SETINI]) begin
      bg_vdir_disp <= IO_D_I[2];
    end
    if (IO_WS[REG_TM]) begin
      tm <= IO_D_I[4:0];
    end
    if (IO_WS[REG_TS]) begin
      ts <= IO_D_I[4:0];
    end
    if (IO_WS[REG_TMW]) begin
      tmw <= IO_D_I[4:0];
    end
    if (IO_WS[REG_TSW]) begin
      tsw <= IO_D_I[4:0];
    end
    if (IO_WS[REG_CGWSEL]) begin
      clr_direct <= IO_D_I[0];
      clr_add_sub <= IO_D_I[1];
      clr_sub_win_rgn <= IO_D_I[5:4];
      clr_main_win_rgn <= IO_D_I[7:6];
    end
    if (IO_WS[REG_CGADSUB]) begin
      clr_math_en[5:0] <= IO_D_I[5:0];
      clr_half <= IO_D_I[6];
      clr_op_sub <= IO_D_I[7];
    end
    if (IO_WS[REG_COLDATA]) begin
      if (IO_D_I[5])            // red
        clr_fixed[4:0] <= IO_D_I[4:0];
      if (IO_D_I[6])            // green
        clr_fixed[9:5] <= IO_D_I[4:0];
      if (IO_D_I[7])            // blue
        clr_fixed[14:10] <= IO_D_I[4:0];
    end
  end

  if (mon_write_ren0) begin
    if (MON_WS[0]) begin
      inidisp_bright <= MON_DIN[3:0];
      inidisp_forced_blank <= MON_DIN[4];
      bgmode <= MON_DIN[7:5];
    end
    if (MON_WS[1]) begin
      bgmode_bg3_pri <= MON_DIN[8];
      bg_vdir_disp <= MON_DIN[9];
      tm <= MON_DIN[14:10];
      tmw[0] <= MON_DIN[15];
    end
    if (MON_WS[2]) begin
      tmw[4:1] <= MON_DIN[19:16];
      ts <= MON_DIN[24:20];
      tsw <= MON_DIN[29:25];
    end
  end
  else if (mon_write_ren1) begin
    if (MON_WS[0]) begin
      clr_direct <= MON_DIN[0];
      clr_add_sub <= MON_DIN[1];
      clr_sub_win_rgn <= MON_DIN[3:2];
      clr_main_win_rgn <= MON_DIN[5:4];
      clr_math_en[1:0] <= MON_DIN[7:6];
    end
    if (MON_WS[1]) begin
      clr_math_en[5:2] <= MON_DIN[11:8];
      clr_half <= MON_DIN[12];
      clr_op_sub <= MON_DIN[13];
      clr_fixed[1:0] <= MON_DIN[15:14];
    end
    if (MON_WS[2]) begin
      clr_fixed[14:2] <= MON_DIN[28:16];
    end
  end
  else if (mon_write_ren2) begin
    if (MON_WS[0]) begin
      mosaic_sz <= MON_DIN[3:0];
      mosaic_bkg_en <= MON_DIN[7:4];
    end
  end
end

initial
  IO_D_OE = 1'b0;

// Chip-global "rendering enabled" signal
assign RENDER_EN = ~inidisp_forced_blank;

// Registers shared with other blocks
assign BRIGHT = inidisp_bright;
assign BGMODE = bgmode;
assign BGSIZE = bgmode_bg_size;


//////////////////////////////////////////////////////////////////////
// Render control logic

/* verilator lint_off UNSIGNED */
wire visible_row = ROW >= FIRST_ROW_RENDER && 
     ROW <= (bg_vdir_disp ? LAST_ROW_RENDER_MAX : LAST_ROW_RENDER_MIN);
wire visible_col = (COL >= FIRST_COL_VISIBLE && COL <= LAST_COL_VISIBLE);
wire render_row = (ROW == LAST_ROW_PRE_RENDER) || visible_row;
wire render_col = (COL >= FIRST_COL_RENDER) & (COL <= LAST_COL_RENDER);
/* verilator lint_on UNSIGNED */

// Chip-global "rendering is happening" signal
assign RENDERING = RENDER_EN && render_row;

// Render-related counter triggers
// All signals are one CC early
wire last_row = ROW == LAST_ROW;
wire hvclr_en = render_row || last_row;
wire bkg_fetch = RENDERING && render_col;
wire hvwrap_col = COL == LAST_COL; // m7_addr_gen needs it here
wire [2:0] fcy;
assign HINC = FETCH & (fcy == 3'd7);
wire vinc = render_row & hvwrap_col;
assign HCLR = hvclr_en & hvwrap_col;
assign VCLR = hvclr_en & hvwrap_col & last_row;
assign FETCH = bkg_fetch;

// V-Blank starts at ROW 225 (240) and ends at ROW 0.
// H-Blank starts at COL 274 and ends at COL 0.
assign HBLANK = (COL >= FIRST_COL_HBLANK) & (COL <= LAST_COL_HBLANK);
assign VBLANK = ~render_row;

reg [1:0]  bg_bmax [0:3];       // BG color depth

always @* begin
  bg_bmax[0] = 2'b00;
  bg_bmax[1] = 2'b00;
  bg_bmax[2] = 2'b00;
  bg_bmax[3] = 2'b00;

  case (bgmode)
    3'd1, 3'd2: begin
      bg_bmax[0] = 2'b01;
      bg_bmax[1] = 2'b01;
    end
    3'd3: begin
      bg_bmax[0] = 2'b11;
      bg_bmax[1] = 2'b01;
    end
    3'd4: bg_bmax[0] = 2'b11;
    3'd5, 3'd6: bg_bmax[0] = 2'b01;
    default: ;
  endcase
end

// We have 8 cycles per dot to fetch all SC and CHR data.
// Each mode defines a VRAM access pattern.
reg [6:0]  ctm [0:7] [0:7];

initial begin
  // Mode 0: BG1 (SC CHR0) BG2 (SC CHR0) BG3 (SC CHR0) BG4 (SC CHR0)
  //            NO PS NS P N
  ctm[0][0] = 7'bx_xx_00_0_1;
  ctm[0][1] = 7'bx_00_00_1_0;
  ctm[0][2] = 7'bx_xx_01_0_1;
  ctm[0][3] = 7'bx_00_01_1_0;
  ctm[0][4] = 7'bx_xx_10_0_1;
  ctm[0][5] = 7'bx_00_10_1_0;
  ctm[0][6] = 7'bx_xx_11_0_1;
  ctm[0][7] = 7'bx_00_11_1_0;

  // Mode 1: BG1 (SC CHR0 CHR1) BG2 (SC CHR0 CHR1) BG3 (SC CHR0)
  //            NO PS NS P N
  ctm[1][0] = 7'bx_xx_00_0_1;
  ctm[1][1] = 7'bx_00_00_1_0;
  ctm[1][2] = 7'bx_01_00_1_0;
  ctm[1][3] = 7'bx_xx_01_0_1;
  ctm[1][4] = 7'bx_00_01_1_0;
  ctm[1][5] = 7'bx_01_01_1_0;
  ctm[1][6] = 7'bx_xx_10_0_1;
  ctm[1][7] = 7'bx_00_10_1_0;

  // Mode 2: BG1 (SC CHR0 CHR1) BG2 (SC CHR0 CHR1) BG3 (HOPT VOPT)
  //            NO PS NS P N
  ctm[2][0] = 7'bx_xx_00_0_1;
  ctm[2][1] = 7'bx_00_00_1_0;
  ctm[2][2] = 7'bx_01_00_1_0;
  ctm[2][3] = 7'bx_xx_01_0_1;
  ctm[2][4] = 7'bx_00_01_1_0;
  ctm[2][5] = 7'bx_01_01_1_0;
  ctm[2][6] = 7'b0_xx_10_0_1;   // H-offset-per-tile
  ctm[2][7] = 7'b1_xx_10_0_1;   // V-offset-per-tile

  // Mode 3: BG1 (SC CHR0 CHR1 CHR2 CHR3) BG2 (SC CHR0 CHR1)
  //            NO PS NS P N
  ctm[3][0] = 7'bx_xx_00_0_1;
  ctm[3][1] = 7'bx_00_00_1_0;
  ctm[3][2] = 7'bx_01_00_1_0;
  ctm[3][3] = 7'bx_10_00_1_0;
  ctm[3][4] = 7'bx_11_00_1_0;
  ctm[3][5] = 7'bx_xx_01_0_1;
  ctm[3][6] = 7'bx_00_01_1_0;
  ctm[3][7] = 7'bx_01_01_1_0;

  // Mode 4: BG1 (SC CHR0 CHR1 CHR2 CHR3) BG2 (SC CHR0) BG3 (OPT)
  //            NO PS NS P N
  ctm[4][0] = 7'bx_xx_00_0_1;
  ctm[4][1] = 7'bx_00_00_1_0;
  ctm[4][2] = 7'bx_01_00_1_0;
  ctm[4][3] = 7'bx_10_00_1_0;
  ctm[4][4] = 7'bx_11_00_1_0;
  ctm[4][5] = 7'bx_xx_01_0_1;
  ctm[4][6] = 7'bx_00_01_1_0;
  ctm[4][7] = 7'bx_xx_10_0_1;   // offset-per-tile

  // Mode 5: BG1 (SC CHR0 CHR1 CHR0+ CHR1+) BG2 (SC CHR0 CHR0+)
  // TODO: Fetch tile+1 for 512 horiz. mode
  //            NO PS NS P N
  ctm[5][0] = 7'bx_xx_00_0_1;
  ctm[5][1] = 7'bx_00_00_1_0;
  ctm[5][2] = 7'bx_01_00_1_0;
  ctm[5][3] = 7'bx_10_00_1_0;
  ctm[5][4] = 7'bx_11_00_1_0;
  ctm[5][5] = 7'bx_xx_01_0_1;
  ctm[5][6] = 7'bx_00_01_1_0;
  ctm[5][7] = 7'bx_01_01_1_0;

  // Mode 6: BG1 (SC CHR0 CHR1 CHR0+ CHR1+) BG3 (HOPT VOPT)
  // TODO: Fetch tile+1 for 512 horiz. mode
  //            NO PS NS P N
  ctm[6][0] = 7'bx_xx_00_0_1;
  ctm[6][1] = 7'bx_00_00_1_0;
  ctm[6][2] = 7'bx_01_00_1_0;
  ctm[6][3] = 7'bx_10_00_1_0;
  ctm[6][4] = 7'bx_11_00_1_0;
  ctm[6][5] = 7'bx_xx_xx_0_0;   // unused
  ctm[6][6] = 7'b0_xx_10_0_1;   // H-offset-per-tile
  ctm[6][7] = 7'b1_xx_10_0_1;   // V-offset-per-tile

  // Mode 7: BG1 (SC+CHR) x8
  //            NO PS NS P N
  ctm[7][0] = 7'bx_00_00_1_1;
  ctm[7][1] = 7'bx_00_00_1_1;
  ctm[7][2] = 7'bx_00_00_1_1;
  ctm[7][3] = 7'bx_00_00_1_1;
  ctm[7][4] = 7'bx_00_00_1_1;
  ctm[7][5] = 7'bx_00_00_1_1;
  ctm[7][6] = 7'bx_00_00_1_1;
  ctm[7][7] = 7'bx_00_00_1_1;
end

// Drive access pattern down to renderer.
// BGMODE selects an access pattern.
assign fcy = COL[2:0] - FIRST_COL_VISIBLE[2:0];
assign FETCH_NT = FETCH && ctm[bgmode][fcy][0];
assign FETCH_PT = FETCH && ctm[bgmode][fcy][1];
assign NT_BSEL = ctm[bgmode][fcy][3:2];
assign PT_BSEL = ctm[bgmode][fcy][5:4];
assign NT_OSEL = ctm[bgmode][fcy][6];
assign PT_BMAX = bg_bmax[NT_BSEL];

// Sprite renderer signals
/* verilator lint_off UNSIGNED */
wire spr_start_col = COL == FIRST_COL_SPREN - SPR_PDLY;
wire spr_end_col = COL == LAST_COL_SPREN + 1'd1 - SPR_PDLY;
wire spr_fetch_col = (COL >= FIRST_COL_SPREN) & (COL <= LAST_COL_SPREN);
wire spr_fetch_ptl_col = COL[0] == FIRST_COL_SPREN[0];
/* verilator lint_on UNSIGNED */
assign SPR_CLR = OLB_RE & OLB_RA[0];
assign SPR_START = RENDERING & spr_start_col;
assign SPR_END = RENDERING & spr_end_col;
assign SPR_FETCH = RENDERING & spr_fetch_col;
assign SPR_FETCH_PTL = SPR_FETCH & spr_fetch_ptl_col;


//////////////////////////////////////////////////////////////////////
// Mosaic counters

reg [3:0]   mosaic_vcnt, mosaic_hcnt;
wire        mosaic_hreset;
wire        mosaic_vzero, mosaic_hzero;
wire [3:0]  hnext;

always @(posedge CLK) if (CC) begin
  if (VCLR)
    mosaic_vcnt <= mosaic_sz;
  else if (vinc)
    if (mosaic_vzero)
      mosaic_vcnt <= mosaic_sz;
    else
      mosaic_vcnt <= mosaic_vcnt - 1'd1;
end

assign mosaic_vzero = ~|mosaic_vcnt;
assign VINC = {4{vinc}} & ({4{mosaic_vzero}} | ~mosaic_bkg_en);

always @(posedge CLK) if (CC) begin
  if (mosaic_hreset)
    mosaic_hcnt <= mosaic_sz;
  else if (visible_col)
    if (mosaic_hzero)
      mosaic_hcnt <= mosaic_sz;
    else
      mosaic_hcnt <= mosaic_hcnt - 1'd1;
end

assign mosaic_hreset = COL == FIRST_COL_VISIBLE;
assign mosaic_hzero = ~|mosaic_hcnt;
assign hnext = {4{mosaic_hreset | mosaic_hzero}} | ~mosaic_bkg_en;


//////////////////////////////////////////////////////////////////////
// Mode 0-6 background renderers

wire [8:0] bkg_col, bkg_row;
wire [8:0] bkg_m06_pd [1:4];
reg [3:0]  bkg_en_mask;

always @* begin
  bkg_en_mask = 4'b0011; // modes 2-6
  case (bgmode)
    3'd0:       bkg_en_mask = 4'b1111;
    3'd1:       bkg_en_mask = 4'b0111;
    3'd2, 3'd3,
    3'd4, 3'd5: bkg_en_mask = 4'b0011;
    3'd6, 3'd7: bkg_en_mask = 4'b0001;
    default: ;
  endcase
end

// Delay all loads until HINC, to align outputs of all BG renderers.
wire bkg_load = HINC;

genvar i;
generate
  for (i = 1; i <= 4; i = i + 1) begin :bg

  wire [1:0] bgidx = i[1:0] - 1;
  wire [1:0] bgn_bmax = bg_bmax[bgidx];
  wire       bgn_fetch = bkg_fetch && NT_BSEL == bgidx;
  wire       bgn_en = bkg_en_mask[bgidx];
  wire       bgn_hnext = hnext[bgidx];
  wire [8:0] bgn_col, bgn_row;
  wire [2:0] fhs = BGFHS[(i-1)*3+2:(i-1)*3];

    s_ppu_bkg_renderer bkg_renderer
      (
       .CLK(CLK),
       .CC(CC),
       .nCC(nCC),

       .BGMODE(bgmode),
       .BGIDX(bgidx),
       .BMAX(bgn_bmax),
       .COL(COL),
       .ROW(ROW),
       .BKG_EN(bgn_en),
       .FHS(fhs),

       .PAR(PAR),
       .PDR(PDR),

       .SHIFT(bkg_fetch),
       .LOAD(bkg_load),
       .FETCH_NT(bgn_fetch && FETCH_NT),
       .FETCH_PT(bgn_fetch && FETCH_PT),
       .PT_BSEL(PT_BSEL),
       .HNEXT(bgn_hnext),

       .PD(bkg_m06_pd[i]),
       .COL_O(bgn_col),
       .ROW_O(bgn_row)
       );

    if (i == 1) begin
      assign bkg_col = bgn_col;
      assign bkg_row = bgn_row;
    end

  end
endgenerate


//////////////////////////////////////////////////////////////////////
// Background Mode 7 renderer

wire       m7_fetch = bkg_fetch;
wire       m7_bkg_en = bkg_en_mask[0];
wire       m7_hnext = hnext[0];
wire [8:0] m7_pd;

s_ppu_m7_renderer m7_renderer
  (
   .CLK(CLK),
   .CC(CC),
   .nCC(nCC),

   .COL(COL),
   .ROW(ROW),
   .BKG_EN(m7_bkg_en),

   .PDR(PDR[15:8]),

   .SHIFT(bkg_fetch),
   .FETCH_PT(m7_fetch && FETCH_PT),
   .HNEXT(m7_hnext),

   .PD(m7_pd)
   );


//////////////////////////////////////////////////////////////////////
// Normal / Mode 7 background output MUX

wire [8:0] bkg_pd [1:4];

generate
  for (i = 1; i <= 4; i = i + 1) begin :bgmux
    if (i == 1)
      assign bkg_pd[i] = (bgmode == 3'd7) ? m7_pd : bkg_m06_pd[i];
    else
      assign bkg_pd[i] = bkg_m06_pd[i];
  end
endgenerate


//////////////////////////////////////////////////////////////////////
// Sprite line buffer input

reg [8:0] spr_col_p, spr_col;
reg [8:0] spr_row_p, spr_row;

wire [9:0] spr_pd;

always @(posedge CLK) if (CC) begin
  // Align OLB readout with BG out.
  spr_col_p <= COL;
  spr_row_p <= ROW;
  spr_col <= spr_col_p;
  spr_row <= spr_row_p;
end

assign OLB_RA = spr_col[7:0] - FIRST_COL_VISIBLE[7:0];
assign OLB_RE = (spr_col >= FIRST_COL_VISIBLE) & (spr_col <= LAST_COL_VISIBLE);

assign spr_pd = {OLB_RD[8:7], 1'b1, OLB_RD[6:0]};


//////////////////////////////////////////////////////////////////////
// Sprite / background pixel priority multiplexer

function [13:0] primux
  (
   input [2:0] bgmode,
   input       bgmode_bg3_pri,
   input [8:0] bg1_pd,
   input [8:0] bg2_pd,
   input [8:0] bg3_pd,
   input [8:0] bg4_pd,
   input [9:0] spr_pd
   );

reg [8:0] bkg_pd [1:4];
reg [7:0] bkg_px [1:4];         // pixel color
reg       bkg_po [1:4];         // pixel is opaque
reg       bkg_pp [1:4];         // tilemap priority is high

reg [7:0] spr_px;               // pixel color
reg       spr_po;               // pixel is opaque
reg [1:0] spr_pp;               // tilemap priority is high

reg       bkg3_hi_pri;
reg       pb1l, pb1h, pb2l, pb2h, pb3l, pb3hn, pb3hh, pb4l, pb4h;
reg       ps0, ps1, ps2, ps3;

reg [5:0] src;                  // source: 0-3=BG1-4, 4=OBJ, 5=back
reg [7:0] pd;

integer   i;

  begin
    bkg_pd[1] = bg1_pd;
    bkg_pd[2] = bg2_pd;
    bkg_pd[3] = bg3_pd;
    bkg_pd[4] = bg4_pd;

    for (i = 1; i <= 4; i = i + 1) begin :bgmux
      bkg_px[i] = bkg_pd[i][7:0];
      bkg_po[i] = |bkg_px[i];
      bkg_pp[i] = bkg_pd[i][8];
    end

    spr_px = spr_pd[7:0];
    spr_po = |spr_px[3:0];
    spr_pp = spr_pd[9:8];

    bkg3_hi_pri = (bgmode == 3'd1) & bgmode_bg3_pri;

    pb1l  = bkg_po[1] & ~bkg_pp[1];
    pb1h  = bkg_po[1] &  bkg_pp[1];
    pb2l  = bkg_po[2] & ~bkg_pp[2];
    pb2h  = bkg_po[2] &  bkg_pp[2];
    pb3l  = bkg_po[3] & ~bkg_pp[3];
    pb3hn = bkg_po[3] &  bkg_pp[3] & ~bkg3_hi_pri;
    pb3hh = bkg_po[3] &  bkg_pp[3] &  bkg3_hi_pri;
    pb4l  = bkg_po[4] & ~bkg_pp[4];
    pb4h  = bkg_po[4] &  bkg_pp[4];

    ps0 = spr_po & spr_pp == 2'b00;
    ps1 = spr_po & spr_pp == 2'b01;
    ps2 = spr_po & spr_pp == 2'b10;
    ps3 = spr_po & spr_pp == 2'b11;

    src = 6'b1_0_0000;          // backdrop
    if (bgmode <= 3'd1) begin   // Modes 0-1
      casez ({pb3hh, ps3, pb1h, pb2h, ps2, pb1l, pb2l, ps1, pb3hn, 
              pb4h, ps0, pb3l, pb4l})
        13'b0000000000001: src = 6'b0_0_1000;
        13'b000000000001?: src = 6'b0_0_0100;
        13'b00000000001??: src = 6'b0_1_0000;
        13'b0000000001???: src = 6'b0_0_1000;
        13'b000000001????: src = 6'b0_0_0100;
        13'b00000001?????: src = 6'b0_1_0000;
        13'b0000001??????: src = 6'b0_0_0010;
        13'b000001???????: src = 6'b0_0_0001;
        13'b00001????????: src = 6'b0_1_0000;
        13'b0001?????????: src = 6'b0_0_0010;
        13'b001??????????: src = 6'b0_0_0001;
        13'b01???????????: src = 6'b0_1_0000;
        13'b1????????????: src = 6'b0_0_0100;
        default: ;
      endcase
    end
    else begin                    // Modes 2-7
      casez ({ps3, pb1h, ps2, pb2h, ps1, pb1l, ps0, pb2l})
        8'b00000001: src = 6'b0_0_0010;
        8'b0000001?: src = 6'b0_1_0000;
        8'b000001??: src = 6'b0_0_0001;
        8'b00001???: src = 6'b0_1_0000;
        8'b0001????: src = 6'b0_0_0010;
        8'b001?????: src = 6'b0_1_0000;
        8'b01??????: src = 6'b0_0_0001;
        8'b1???????: src = 6'b0_1_0000;
        default: ;
      endcase
    end

    pd = 8'hx;
    case (src)
      6'b0_0_0001: pd = bkg_px[1];
      6'b0_0_0010: pd = bkg_px[2];
      6'b0_0_0100: pd = bkg_px[3];
      6'b0_0_1000: pd = bkg_px[4];
      6'b0_1_0000: pd = spr_px;
      6'b1_0_0000: pd = 8'd0;
      default: ;
    endcase

    primux = {src, pd};
  end
endfunction


//////////////////////////////////////////////////////////////////////
// Main Screen Circuit

wire [5:0] win;
wire [4:0] main_win;
wire [3:0] main_bkg_en;
wire       main_spr_en;
wire [8:0] main_bkg_pd [1:4];
wire [9:0] main_spr_pd;
wire [13:0] main_primux;
wire [7:0]  main_pd;
wire [5:0]  main_src;

delay_line #(6,19) windly(.C(CLK), .CE(CC), .D(WIN), .Q(win));

assign main_win = win[4:0] & tmw;

assign main_bkg_en = tm[3:0] & ~main_win[3:0];
assign main_spr_en = tm[4] & ~main_win[4];

assign main_bkg_pd[1] = main_bkg_en[0] ? bkg_pd[1] : 9'd0;
assign main_bkg_pd[2] = main_bkg_en[1] ? bkg_pd[2] : 9'd0;
assign main_bkg_pd[3] = main_bkg_en[2] ? bkg_pd[3] : 9'd0;
assign main_bkg_pd[4] = main_bkg_en[3] ? bkg_pd[4] : 9'd0;
assign main_spr_pd    = main_spr_en    ? spr_pd    : 10'd0;

assign main_primux = primux(bgmode, bgmode_bg3_pri,
                            main_bkg_pd[1], main_bkg_pd[2],
                            main_bkg_pd[3], main_bkg_pd[4],
                            main_spr_pd);

assign main_pd = main_primux[7:0];
assign main_src = main_primux[13:8];


//////////////////////////////////////////////////////////////////////
// Sub Screen Circuit

wire [4:0] sub_win;
wire [3:0] sub_bkg_en;
wire       sub_spr_en;
wire [8:0] sub_bkg_pd [1:4];
wire [9:0] sub_spr_pd;
wire [13:0] sub_primux;
wire [7:0]  sub_pd;
wire        sub_trans;          // subscreen backdrop is revealed

assign sub_win = win[4:0] & tsw;

assign sub_bkg_en = ts[3:0] & ~sub_win[3:0];
assign sub_spr_en = ts[4] & ~sub_win[4];

assign sub_bkg_pd[1] = sub_bkg_en[0] ? bkg_pd[1] : 9'd0;
assign sub_bkg_pd[2] = sub_bkg_en[1] ? bkg_pd[2] : 9'd0;
assign sub_bkg_pd[3] = sub_bkg_en[2] ? bkg_pd[3] : 9'd0;
assign sub_bkg_pd[4] = sub_bkg_en[3] ? bkg_pd[4] : 9'd0;
assign sub_spr_pd    = sub_spr_en    ? spr_pd    : 10'd0;

assign sub_primux = primux(bgmode, bgmode_bg3_pri,
                           sub_bkg_pd[1], sub_bkg_pd[2],
                           sub_bkg_pd[3], sub_bkg_pd[4],
                           sub_spr_pd);

assign sub_pd = sub_primux[7:0];
assign sub_trans = sub_primux[13];


//////////////////////////////////////////////////////////////////////
// Color palette lookup

reg         pal_sel;
reg [14:0]  main_rgb_p, sub_rgb;
reg         sub_trans_d;

assign PAL_PD = pal_sel ? main_pd : sub_pd;

always @(posedge CLK) begin
  if (nCC) begin
    pal_sel <= 1'b0;
    sub_trans_d <= sub_trans;
    sub_rgb <= sub_trans_d ? clr_fixed : PAL_RGB;
  end
  if (CC) begin
    pal_sel <= 1'b1;
    main_rgb_p <= PAL_RGB;
  end
end


//////////////////////////////////////////////////////////////////////
// Color math

reg [14:0]  main_rgb, clr_sub_rgb;
reg         clr_sub_trans;
wire [5:0]  clr_main_src;
wire        clr_main_src_masked;
reg         clr_op_en_p, clr_op_en;

wire        clr_main_win, clr_sub_win;
wire        clr_sub_dis;
reg         clr_win_p, clr_win;
wire [14:0] clr_in1, clr_in2;
wire        clr_op_half;
wire [14:0] clr_op_out;
reg [14:0]  clr_out_rgb;

// Align all inputs to nCC for logic below
always @(posedge CLK) begin
  if (nCC) begin
    main_rgb <= main_rgb_p;
    clr_sub_trans <= sub_trans_d;
    clr_op_en_p <= clr_main_src_masked;
    clr_op_en <= clr_op_en_p;
    clr_win_p <= win[5];
    clr_win <= clr_win_p;
  end
end

// Subscreen vs. fixed color input switch
always @* clr_sub_rgb = clr_add_sub ? sub_rgb : clr_fixed;

// Sprites using palettes 0-3 do not use color math.
assign clr_main_src = {main_src[5], main_src[4] & main_pd[6], main_src[3:0]};

function clr_screen_win(input [1:0] rgn, input win);
  begin
    clr_screen_win = 1'bx;
    case (rgn)
      2'd0: clr_screen_win = 1'b0;
      2'd1: clr_screen_win = ~win;
      2'd2: clr_screen_win = win;
      2'd3: clr_screen_win = 1'b1;
    endcase
  end
endfunction

assign clr_main_win = clr_screen_win(clr_main_win_rgn, clr_win);
assign clr_sub_win  = clr_screen_win(clr_sub_win_rgn,  clr_win);

assign clr_sub_dis = clr_sub_trans | clr_sub_win;

assign clr_in1 = clr_main_win ? 15'd0 : main_rgb;
assign clr_in2 = clr_sub_win  ? 15'd0 : clr_sub_rgb;

assign clr_main_src_masked = |(clr_math_en & clr_main_src);
assign clr_op_half = clr_half & ~(clr_add_sub & clr_sub_dis);

function [4:0] clr_math
  (
    input       en, 
    input       sub, 
    input       half, 
    input [4:0] in1, 
    input [4:0] in2
   );
  reg signed [6:0] a, b, c;
  begin
    a = {2'b00, in1};
    b = {2'b00, in2};
    c = a;

    if (sub)
      b = -b;
    if (en) begin

      c = a + b;
      if (half)
        c = c / 2;

      if (c < 7'sd0)
        c = 7'sd0;
      else if (c > 7'sd31)
        c = 7'sd31;
    end

    clr_math = c[4:0];
  end
endfunction

assign clr_op_out[4:0]   = clr_math(clr_op_en, clr_op_sub, clr_op_half,
                                    clr_in1[4:0],   clr_in2[4:0]);
assign clr_op_out[9:5]   = clr_math(clr_op_en, clr_op_sub, clr_op_half,
                                    clr_in1[9:5],   clr_in2[9:5]);
assign clr_op_out[14:10] = clr_math(clr_op_en, clr_op_sub, clr_op_half,
                                    clr_in1[14:10], clr_in2[14:10]);

always @(posedge CLK) if (CC) begin
  clr_out_rgb <= clr_op_out;
end


//////////////////////////////////////////////////////////////////////
// Final output

wire blank;

/* verilator lint_off UNSIGNED */
assign blank = inidisp_forced_blank |
               !visible_row |
               !(bkg_col >= FIRST_COL_VISIBLE &&
                 bkg_col <= LAST_COL_VISIBLE);
/* verilator lint_on UNSIGNED */

// These signals are 1x CC behind RGB, because sync_gen adds 1x CC.
delay_line #(9,2) dly_sync_col(.C(CLK), .CE(CC), .D(bkg_col), .Q(SYNC_COL_O));
delay_line #(9,2) dly_sync_row(.C(CLK), .CE(CC), .D(bkg_row), .Q(SYNC_ROW_O));
delay_line #(1,2) dly_sync_blank(.C(CLK), .CE(CC), .D(blank), .Q(SYNC_BLANK));

/* -----\/----- EXCLUDED -----\/-----
always @(posedge CLK) if (CC) begin
  SYNC_COL_O <= bkg_col;
  SYNC_ROW_O <= bkg_row;
  SYNC_BLANK <= blank;
end

 -----/\----- EXCLUDED -----/\----- */
always @(posedge CLK) if (CC) begin
  RGB <= clr_out_rgb;
  //COL_O <= SYNC_COL_O;
  //ROW_O <= SYNC_ROW_O;
end


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_ack;
reg        mon_sel_ren0, mon_sel_ren1, mon_sel_ren2;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  mon_ack = 1'b1;
  mon_sel_ren0 = 1'b0;
  mon_sel_ren1 = 1'b0;
  mon_sel_ren2 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL[10:2])        //``SUBREGS PPU
    9'h02: begin              //``REG REN0
      mon_sel_ren0 = 1'b1;
      mon_dout[3:0] = inidisp_bright;
      mon_dout[4] = inidisp_forced_blank;
      mon_dout[7:5] = bgmode;
      mon_dout[8] = bgmode_bg3_pri;
      mon_dout[9] = bg_vdir_disp;
      mon_dout[14:10] = tm;
      mon_dout[19:15] = tmw;
      mon_dout[24:20] = ts;
      mon_dout[29:25] = tsw;
    end
    9'h03: begin              //``REG REN1
      mon_sel_ren1 = 1'b1;
      mon_dout[0] = clr_direct;
      mon_dout[1] = clr_add_sub;
      mon_dout[3:2] = clr_sub_win_rgn;
      mon_dout[5:4] = clr_main_win_rgn;
      mon_dout[11:6] = clr_math_en;
      mon_dout[12] = clr_half;
      mon_dout[13] = clr_op_sub;
      mon_dout[28:14] = clr_fixed;
    end
    9'h04: begin              //``REG REN2
      mon_sel_ren2 = 1'b1;
      mon_dout[3:0] = mosaic_sz;
      mon_dout[7:4] = mosaic_bkg_en;
    end
    default: begin
      mon_ack = 1'b0;
    end
  endcase
end

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_ren0 = mon_write & mon_sel_ren0;
assign mon_write_ren1 = mon_write & mon_sel_ren1;
assign mon_write_ren2 = mon_write & mon_sel_ren2;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK && MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end

assign MON_ACK = mon_ack;

endmodule
