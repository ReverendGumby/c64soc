module s_ppu_m7_renderer
  (
   input            CLK,
   input            CC,
   input            nCC,

   input [8:0]      COL,
   input [8:0]      ROW,
   input            BKG_EN,

   input [7:0]      PDR,

   input            SHIFT,
   input            FETCH_PT,
   input            HNEXT,

   output reg [8:0] PD
   );

`include "swab.vh"

//////////////////////////////////////////////////////////////////////
// Tile pattern data latches and shifters
// These select all 8 color bits.

// Tune shifter depth to match our pipeline delay to that of the
// normal renderers.
localparam SMAX = 12;

reg [7:0]  tile [0:SMAX]; // shifter
reg [7:0]  tile0_buf; // latch
reg [7:0]  pdr_in;
reg        fetch_pt_rd_d;
reg        shift;
integer    i;

always @* begin
  pdr_in = PDR;
end

always @(posedge CLK) if (CC) begin
  // "Shift" out the pixel.
  if (shift) begin
    tile[0] <= tile0_buf;
    for (i = 0; i < SMAX; i = i + 1)
      tile[i + 1] <= tile[i];
  end

  fetch_pt_rd_d <= FETCH_PT;

  // Buffer PT data fetches.
  if (fetch_pt_rd_d)
    tile0_buf <= pdr_in;
end


//////////////////////////////////////////////////////////////////////
// Pixel data
//
// All data for a pixel is read at once, i.e., one PT fetch yields one
// pixel. Thus, tile data value is precisely the pixel data (CGRAM
// entry) value. Tile data 0 selects transparent pixels: output 'h00.

wire [7:0] pd_tile = tile[SMAX];
reg [7:0]  pd_px; // palette index
reg [7:0]  pd_in;

always @* begin
  pd_px = pd_tile[7:0];         // 8bpp
end

always @* begin
  if (~|pd_tile)                // transparent
    pd_in = 8'h00;
  else
    pd_in = pd_tile;
end

reg       bkg_en, bkg_en_p;
reg       shift_p;
reg       hnext, hnext_p;

always @(posedge CLK) if (CC) begin
  // TODO: figure this out.
  shift_p <= SHIFT;
  shift <= shift_p;

  bkg_en_p <= BKG_EN;
  bkg_en <= bkg_en_p;

  hnext_p <= HNEXT;
  hnext <= hnext_p;

  if (hnext)
    PD <= bkg_en ? {1'b0, pd_in} : 9'h0;
end

endmodule
