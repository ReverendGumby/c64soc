module sysctl
  (
   input             CLK,

   // AXI Lite slave bus interface
   input             ARESETn,
   input [31:0]      AWADDR,
   input             AWVALID,
   output reg        AWREADY,
   input [31:0]      WDATA,
   output reg        WREADY,
   input [3:0]       WSTRB,
   input             WVALID,
   output [1:0]      BRESP,
   output reg        BVALID,
   input             BREADY,
   input [31:0]      ARADDR,
   input             ARVALID,
   output reg        ARREADY,
   output reg [31:0] RDATA,
   output [1:0]      RRESP,
   output reg        RVALID,
   input             RREADY,

   // HMI input/outputs
   input [3:0]       HMI_SW,
   input [3:0]       HMI_BTN,
   output reg [3:0]  HMI_LED,

   // Video control
   output reg        VID_RESET,
   output reg        VID_HDMI_MODE,

   // Audio control
   output reg        AUD_MUTE,
   output reg        AUD_SDOUT_EN
   );

reg [31:0] rdata;

`include "version.vh"

reg [127:0] git_branch = `GIT_BRANCH;
reg [127:0] git_rev = `GIT_REV;

wire [31:0] timestamp;

// Timestamp is the bitstream generation time.
// See Xilinx doc. XAPP1232 for details.
USR_ACCESSE2 USR_ACCESS_7series_inst
  (
   .DATA(timestamp)
   );

always @* begin
  rdata = 32'h00000000;
  casex (ARADDR[8:2])
    7'h0x: rdata = 32'h05450545; //``REG ID DATA
    7'h10, 7'h11, 7'h12, 7'h13: begin //``REG BS_GIT_BRANCH
      case (ARADDR[3:2])
        2'b00: rdata = git_branch[0*32+31:0*32];
        2'b01: rdata = git_branch[1*32+31:1*32];
        2'b10: rdata = git_branch[2*32+31:2*32];
        2'b11: rdata = git_branch[3*32+31:3*32];
      endcase
    end
    7'h18, 7'h19, 7'h1a, 7'h1b: begin //``REG BS_GIT_REV
      case (ARADDR[3:2])
        2'b00: rdata = git_rev[0*32+31:0*32];
        2'b01: rdata = git_rev[1*32+31:1*32];
        2'b10: rdata = git_rev[2*32+31:2*32];
        2'b11: rdata = git_rev[3*32+31:3*32];
        default: ;
      endcase
    end
    7'h1c: rdata = timestamp;   //``REG BS_TIMESTAMP
    7'h20: begin                 //``REG HMI_IN
      rdata[3:0] = HMI_SW;
      rdata[11:8] = HMI_BTN;
    end
    7'h21: begin               //``REG HMI_OUT
      rdata[3:0] = HMI_LED;
    end
    7'h30: begin               //``REG VID
      rdata[0] = VID_RESET;
      rdata[1] = VID_HDMI_MODE;
    end
    7'h31: begin                //``REG AUD
      rdata[0] = AUD_MUTE;
      rdata[1] = AUD_SDOUT_EN;
    end
    default: ;
  endcase
end

always @(posedge CLK) begin
  if (!ARESETn) begin
    ARREADY <= 1'b0;
    RVALID <= 1'b0;
  end
  else begin
    if (!ARREADY && ARVALID) begin
      ARREADY <= 1'b1;
      RDATA <= rdata;
      RVALID <= 1'b1;
    end
    else if (RVALID) begin
      ARREADY <= 1'b0;
      if (RREADY)
        RVALID <= 1'b0;
    end
  end
end

always @(posedge CLK) begin
  if (!ARESETn) begin
    AWREADY <= 1'b0;
    WREADY <= 1'b0;
    BVALID <= 1'b0;
  end
  else begin
    if (!AWREADY && AWVALID && !WREADY && WVALID && !BVALID) begin
      AWREADY <= 1'b1;
      WREADY <= 1'b1;
      BVALID <= 1'b1;
    end
    else if (BVALID) begin
      AWREADY <= 1'b0;
      WREADY <= 1'b0;
      if (BREADY)
        BVALID <= 1'b0;
    end
  end
end

assign BRESP = 2'b00;

always @(posedge CLK) begin
  if (!ARESETn) begin
    HMI_LED <= 0;
    VID_RESET <= 1'b1;
    VID_HDMI_MODE <= 1'b0;
    AUD_MUTE <= 1'b1;
    AUD_SDOUT_EN <= 1'b0;
  end
  else begin
    if (!AWREADY && AWVALID && !WREADY && WVALID && !BVALID) begin
      case (AWADDR[8:2])
        7'h21: begin
          if (WSTRB[0]) HMI_LED <= WDATA[3:0];
        end
        7'h30: begin
          if (WSTRB[0]) begin
            VID_RESET <= WDATA[0];
            VID_HDMI_MODE <= WDATA[1];
          end
        end
        7'h31: begin
          if (WSTRB[0]) begin
            AUD_MUTE <= WDATA[0];
            AUD_SDOUT_EN <= WDATA[1];
          end
        end
      endcase
    end
  end
end
  
endmodule
