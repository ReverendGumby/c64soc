module player
  (
   input [2:1]  JPCLK,
   input [2:0]  JPOUT,

   output [1:0] JP1D, // 4016.D0-1
   output [4:0] JP2D, // 4017.D0-4

   output [1:0] CTLR1_I,
   input [2:0]  CTLR1_O,
   output [1:0] CTLR2_I,
   input [2:0]  CTLR2_O
   );

assign JP1D = {CTLR1_O[1:0]};
assign JP2D = {3'b000, CTLR2_O[1:0]};

assign CTLR1_I = {JPOUT[0], JPCLK[1]};
assign CTLR2_I = {JPOUT[0], JPCLK[2]};

endmodule
