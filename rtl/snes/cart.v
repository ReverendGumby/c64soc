// Cartridge emulation

module cart
  (
   // Clocks / resets
   input             CLK,
   input             nRES,
   input             CP2_NEGEDGE,

   // CPU memory bus (both A- and B-Bus) interface
   input [23:0]      A,
   input [7:0]       D_I,
   output [7:0]      D_O,
   output            D_OE,
   input             nWRAMSEL,
   input             nROMSEL,
   input             nWR,
   input             nRD,
   input [5:0]       PA,
   input             nPARD,
   input             nPAWR,

   // Control register interface
   input             CTRL_nRES,
   input [11:2]      CTRL_ADDR, // control interface
   input [3:0]       CTRL_WSTRB,
   input [31:0]      CTRL_DIN,
   output reg [31:0] CTRL_DOUT,

   // Address translation interface
   output [31:0]     CAT_ADDR,
   output            CAT_DREQ
   );

reg [31:0]     base_rom, base_ram;

reg [2:0]      mem_map;
reg            ram_enable;
reg [22:0]     rom_addr_mask;
reg [16:0]     ram_addr_mask;

reg [31:0]     ram_write_cnt;
reg            ram_write_clear;

reg [2:0]      mapper_sel;


//////////////////////////////////////////////////////////////////////
// Control register read interface

always @* begin                 //``REGION_REGS CART_CTRL
  CTRL_DOUT = 32'h0000;
  case (CTRL_ADDR)
    10'h000: begin
      CTRL_DOUT[2:0] = mem_map;
    end
    10'h001: CTRL_DOUT = base_rom;
    10'h002: CTRL_DOUT = base_ram;
    10'h010: begin              //``REG CHIP_CFG
      CTRL_DOUT[0] = ram_enable;
    end
    10'h011: begin                     //``REG ROM_ADDR_MASK
      CTRL_DOUT[22:0] = rom_addr_mask; //``NO_FIELD
    end
    10'h012: begin                     //``REG RAM_ADDR_MASK
      CTRL_DOUT[16:0] = ram_addr_mask; //``NO_FIELD
    end
    10'h014: CTRL_DOUT = ram_write_cnt;
    10'h028: begin
      CTRL_DOUT[2:0] = mapper_sel;
    end
    default: ;
  endcase
end                             //``REG_END


//////////////////////////////////////////////////////////////////////
// Control register write interface

initial begin
  // Tie lower mask bits high to simplify math.
  rom_addr_mask[18:0] = ~19'b0;
  ram_addr_mask[9:0] = ~10'b0;
end

always @(posedge CLK) begin
  ram_write_clear <= 1'b0;
  if (!CTRL_nRES) begin
    mem_map <= 3'd0;
    base_rom <= 32'h0;
    base_ram <= 32'h0;
    rom_addr_mask[22:19] <= 4'b0;
    ram_addr_mask[16:10] <= 7'b0;
    ram_enable <= 1'b0;
    mapper_sel <= 3'd0;         // BYPASS
  end
  else
    case (CTRL_ADDR)
      10'h000: begin
        if (CTRL_WSTRB[0])
          mem_map <= CTRL_DIN[2:0];
      end
      10'h001: begin
        if (CTRL_WSTRB[0]) base_rom[07:00] <= CTRL_DIN[07:00];
        if (CTRL_WSTRB[1]) base_rom[15:08] <= CTRL_DIN[15:08];
        if (CTRL_WSTRB[2]) base_rom[23:16] <= CTRL_DIN[23:16];
        if (CTRL_WSTRB[3]) base_rom[31:24] <= CTRL_DIN[31:24];
      end
      10'h002: begin
        if (CTRL_WSTRB[0]) base_ram[07:00] <= CTRL_DIN[07:00];
        if (CTRL_WSTRB[1]) base_ram[15:08] <= CTRL_DIN[15:08];
        if (CTRL_WSTRB[2]) base_ram[23:16] <= CTRL_DIN[23:16];
        if (CTRL_WSTRB[3]) base_ram[31:24] <= CTRL_DIN[31:24];
      end
      10'h010: begin
        if (CTRL_WSTRB[0])
          ram_enable <= CTRL_DIN[0];
      end
      10'h011: begin
        if (CTRL_WSTRB[2])
          rom_addr_mask[22:19] <= CTRL_DIN[22:19];
      end
      10'h012: begin
        if (&CTRL_WSTRB[2:1])
          ram_addr_mask[16:10] <= CTRL_DIN[16:10];
      end
      10'h014: begin
        if (CTRL_WSTRB[0])
          ram_write_clear <= 1'b1;
      end
      10'h028: begin
        if (CTRL_WSTRB[0])
          mapper_sel <= CTRL_DIN[2:0];
      end
      default: ;
    endcase
end


//////////////////////////////////////////////////////////////////////
// Address translation

reg [22:0]     rom_a;
reg [16:0]     ram_a;
reg            rom_ce, ram_ce;

reg [31:0]     addr;
reg            dreq;

always @* begin
  rom_a = 23'b0;
  ram_a = 17'b0;
  rom_ce = 1'b0;
  ram_ce = 1'b0;
  case (mem_map)                //``VALS
    3'h0: begin                 //``VAL LOROM
      rom_a[14:0] = A[14:0];
      rom_a[21:15] = A[22:16];
      ram_a[14:0] = A[14:0];
      rom_ce = ~nROMSEL & (A[15] | ~ram_enable);
      ram_ce = ~nROMSEL & ~A[15] & (A[22:20] == 3'h7);
    end
    3'h1: begin                 //``VAL HIROM
      rom_a[21:0] = A[21:0];
      ram_a[12:0] = A[12:0];
      ram_a[16:13] = A[19:16];
      rom_ce = ~nROMSEL;
      ram_ce = nROMSEL & &A[14:13] & (A[22:20] == 3'h3); // 3x/Bx:6000-7FFF
    end
    3'h5: begin                 //``VAL EXHIROM
      rom_a[21:0] = A[21:0];
      rom_a[22] = A[23];
      ram_a[12:0] = A[12:0];
      ram_a[16:13] = A[19:16];
      rom_ce = ~nROMSEL;
      ram_ce = nROMSEL & &A[14:13] & A[23]; // 80-BF:6000-7FFF
    end
    default: ;
  endcase
end

always @* begin
  addr = 32'b0;
  dreq = 1'b0;

  if (rom_ce) begin
    addr = base_rom + {9'b0, rom_a & rom_addr_mask};
    dreq = ~nRD;              // ignore writes
  end
  else if (ram_ce) begin
    addr = base_ram + {15'b0, ram_a & ram_addr_mask};
    dreq = ram_enable;
  end
end

assign CAT_ADDR = addr;
assign CAT_DREQ = dreq;

assign D_O = 8'bx;
assign D_OE = 1'b0; // reserved for mapper


//////////////////////////////////////////////////////////////////////
// RAM write monitor

wire ram_cpu_write = CP2_NEGEDGE & ram_enable & ram_ce & ~nWR;

always @(posedge CLK) begin
  if (!nRES) begin
    ram_write_cnt <= 32'd0;
  end
  else begin
    if (ram_write_clear) begin
      ram_write_cnt <= {31'd0, ram_cpu_write};
    end
    else if (ram_cpu_write) begin
      if (ram_write_cnt + 1'd1 != 32'd0)
        ram_write_cnt <= ram_write_cnt + 1'd1;
    end
  end
end


endmodule
