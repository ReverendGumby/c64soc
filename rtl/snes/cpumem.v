// CPU memory / PS7 memory (AXI_ACP) bridge
//
// Use system DDR as backing store for CPU WRAM and cartridge ROM.
//
// Caveat: DRAM is not consistently fast enough to meet strict timing.
//
// Clocks:
//
// - MCLK: the system clock. All ports except the AXI bus are on this
// clock.
//
// - ACLK: MCLK * N. Synchronous and phase-aligned to MCLK. The AXI
// bus runs at this rate.
//
// With AXI_ACP driven by the 2x system clock (43MHz), maximum timings
// from cpumem.atm_* (access timing measurement) are RIDLE=25 and
// WIDLE=29. This translates to maximum of 15 CPU clocks/cycle, far
// exceeding the fastest timing of 6 CPU clocks/cycle (needed by
// fast-ROM).
//
// Scope captures show these worst-case delays occur at a peak rate of
// 17.8kHz. This equates to an effective CPU clock slowdown of 0.75%.

module cpumem 
  (
   input         MCLK,
   input         CP1_POSEDGE,
   input         CP2_NEGEDGE,
   input         CP2_NEGEDGE_PRE,

   // Module status/control
   input [31:0]  BASE_WRAM,
   input         AXI_EN,
   output        CPU_READY,
   output        IDLE,

   // Monitor interface
   input [7:0]   MON_SEL,
   input         MON_WS,
   output [31:0] MON_DOUT,
   input [31:0]  MON_DIN,

   // CPU memory bus (both A- and B-Bus) interface
   input [23:0]  A,
   input [7:0]   D_I,
   output [7:0]  D_O,
   output        D_OE,
   input         nROMSEL,
   input         nWRAMSEL,
   input         nWR,
   input         nRD,
   input [7:0]   PA,
   input         nPARD,
   input         nPAWR,

   // Cartridge address translation interface
   input [31:0]  CAT_ADDR,
   input         CAT_DREQ,

   // AXI bus leader for ACP
   input         ACLK, // MCLK * N
   output [31:0] ARADDR,
   output [1:0]  ARBURST,
   output [3:0]  ARCACHE,
   output [2:0]  ARID,
   output [3:0]  ARLEN,
   output [1:0]  ARLOCK,
   output [2:0]  ARPROT,
   output [3:0]  ARQOS,
   input         ARREADY,
   output [2:0]  ARSIZE,
   output [4:0]  ARUSER,
   output        ARVALID,
   output [31:0] AWADDR,
   output [1:0]  AWBURST,
   output [3:0]  AWCACHE,
   output [2:0]  AWID,
   output [3:0]  AWLEN,
   output [1:0]  AWLOCK,
   output [2:0]  AWPROT,
   output [3:0]  AWQOS,
   input         AWREADY,
   output [2:0]  AWSIZE,
   output [4:0]  AWUSER,
   output        AWVALID,
   input [2:0]   BID,
   output        BREADY,
   input [1:0]   BRESP,
   input         BVALID,
   input [63:0]  RDATA,
   input [2:0]   RID,
   input         RLAST,
   output        RREADY,
   input [1:0]   RRESP,
   input         RVALID,
   output [63:0] WDATA,
   output [2:0]  WID,
   output        WLAST,
   input         WREADY,
   output [7:0]  WSTRB,
   output        WVALID
   );

wire        cp1_posedge_ne;     // negedge of CP1_POSEDGE
wire        cp2_negedge_pe;     // posedge of CP2_NEGEDGE
wire [16:0] wram_a;
wire [7:0]  dstrb;

reg         cp1_posedge_d, cp2_negedge_d;
reg [1:0]   rts, wts;
reg         axi_error, to_error;
reg [31:0]  addr, araddr, awaddr;
reg [63:0]  rdata;
reg [7:0]   wdata;
reg [31:0]  aaddr, baddr, maddr;
reg         arclken, awclken, brclken, bwclken;
wire        arreq, awreq, brreq, bwreq, mrreq, mwreq;
reg         adreq, bdreq;
reg         mrreq_mclk, mwreq_mclk, mclken_mclk;
reg [16:0]  wmadd;
wire        bsel;
wire        bas_wmdata, bas_wmaddl, bas_wmaddm, bas_wmaddh;
wire        bre_wmdata;
wire        bwe_wmdata, bwe_wmaddl, bwe_wmaddm, bwe_wmaddh;
wire        brs_wmdata, bws_wmdata;
reg         bwe_wmdata_d, bre_wmdata_d;
wire        bre_wmdata_pe;
reg         rreq, wreq;
reg [7:0]   dataw, mdataw;
reg [7:0]   d_o;
reg         mclken_d_mclk;
wire        mclken;

reg [31:0]  mon_dout;

reg [7:0]   atm_rnow, atm_rnow_d, atm_wnow, atm_wnow_d,
            atm_rdone_min, atm_rdone_max, 
            atm_wdone_min, atm_wdone_max;
reg         atm_rw_reset;
reg [31:0]  atm_not_ready_cnt;
reg         atm_not_ready_reset;

localparam [2:0] axi_id = 3'b000;
localparam [4:0] axi_user = 5'b00001; // enable ACP coherent R/W req

localparam [1:0] RTS_IDLE =  2'd0;
localparam [1:0] RTS_RADDR = 2'd1;
localparam [1:0] RTS_RDATA = 2'd2;
localparam [1:0] WTS_IDLE =  2'd0;
localparam [1:0] WTS_WADDR = 2'd1;
localparam [1:0] WTS_WDATA = 2'd2;
localparam [1:0] WTS_WWAIT = 2'd3;

initial begin
  rts = RTS_IDLE;
  wts = WTS_IDLE;
  axi_error = 1'b0;
  to_error = 1'b0;
  mrreq_mclk = 1'b0;
  mwreq_mclk = 1'b0;
  mclken_mclk = 1'b0;
  maddr = 32'b0;
  mdataw = 8'b0;

  atm_rnow_d = 8'd0;
  atm_wnow_d = 8'd0;
  atm_rdone_min = ~8'd0;
  atm_rdone_max = 8'd0;
  atm_wdone_min = ~8'd0;
  atm_wdone_max = 8'd0;

  atm_not_ready_cnt = 32'd0;
end


//////////////////////////////////////////////////////////////////////
// A-Bus interface

assign wram_a[15:0] = A[15:0];
assign wram_a[16] = A[16] & A[22];

always @* begin
  adreq = 1'b0;
  aaddr = 32'b0;

  if (~nWRAMSEL) begin
    aaddr = BASE_WRAM + {15'b0, wram_a};
    adreq = 1'b1;
  end
  else if (CAT_DREQ) begin
    aaddr = CAT_ADDR;
    adreq = CAT_DREQ;
  end
end

// Read request begins after CP1 rising edge, when address goes
// valid. Write request begins on CP2 falling edge, when both address
// and data are valid.

always @(posedge ACLK) begin
  cp1_posedge_d <= CP1_POSEDGE;
  cp2_negedge_d <= CP2_NEGEDGE;
end

assign cp1_posedge_ne = ~CP1_POSEDGE & cp1_posedge_d;
assign cp2_negedge_pe = CP2_NEGEDGE & ~cp2_negedge_d;

always @* begin
  arclken = cp1_posedge_ne;
  awclken = cp2_negedge_pe;
end

assign arreq = ~nRD & arclken & adreq;
assign awreq = ~nWR & awclken & adreq;


//////////////////////////////////////////////////////////////////////
// B-Bus interface

assign bsel = PA[7] & ~|PA[6:2];             // $2180-3
assign bas_wmdata = bsel & (PA[1:0] == 'h0);
assign bas_wmaddl = bsel & (PA[1:0] == 'h1);
assign bas_wmaddm = bsel & (PA[1:0] == 'h2);
assign bas_wmaddh = bsel & (PA[1:0] == 'h3);

// asserted while reading/writing
assign bre_wmdata = ~nPARD & bas_wmdata;
assign bwe_wmdata = ~nPAWR & bas_wmdata;
assign bwe_wmaddl = ~nPAWR & bas_wmaddl;
assign bwe_wmaddm = ~nPAWR & bas_wmaddm;
assign bwe_wmaddh = ~nPAWR & bas_wmaddh;

// strobes pulse after read/write
assign brs_wmdata = ~bre_wmdata & bre_wmdata_d;
assign bws_wmdata = ~bwe_wmdata & bwe_wmdata_d;

always @(posedge ACLK) begin
  // TODO: emulator reset
  // TODO: if (~HOLD) begin
  if (bwe_wmaddl)
    wmadd[7:0] <= D_I;
  else if (bwe_wmaddm)
    wmadd[15:8] <= D_I;
  else if (bwe_wmaddh)
    wmadd[16] <= D_I[0];
  else if (brs_wmdata | bws_wmdata)
    wmadd <= wmadd + 1'd1;

  if (MON_WS) begin
    case (MON_SEL)
      8'h10: wmadd <= MON_DIN[16:0];
      default: ;
    endcase
  end

  bre_wmdata_d <= bre_wmdata;
  bwe_wmdata_d <= bwe_wmdata;
end

always @* begin
  bdreq = 1'b0;
  baddr = 32'b0;

  if (bre_wmdata | bwe_wmdata) begin
    baddr = BASE_WRAM + {15'b0, wmadd};
    bdreq = 1'b1;
  end
end

// The WMDATA register, when selected, triggers reads and writes. Read
// request begins on nPARD rising edge (ASAP). Write request begins on
// CP2 falling edge, when data is valid. Address was previously stored
// in WMADD* registers.

assign bre_wmdata_pe = bre_wmdata & ~bre_wmdata_d;

always @* begin
  brclken = bre_wmdata_pe;
  bwclken = cp2_negedge_pe;
end

assign brreq = bre_wmdata & brclken & bdreq;
assign bwreq = bwe_wmdata & bwclken & bdreq;


//////////////////////////////////////////////////////////////////////
// Monitor-controlled memory interface
//
// For testing and loading/saving emulator state

always @(posedge MCLK) begin
  mrreq_mclk <= 1'b0;
  mwreq_mclk <= 1'b0;
  mclken_mclk <= 1'b0;

  if (MON_WS) begin
    case (MON_SEL)
      8'h08: begin
        mrreq_mclk <= MON_DIN[0];
        mwreq_mclk <= MON_DIN[1];
        mclken_mclk <= 1'b1;
      end
      8'h09: maddr <= MON_DIN;
      8'h0A: mdataw <= MON_DIN[7:0];
      default: ;
    endcase
  end
end

always @(posedge ACLK)
  mclken_d_mclk <= mclken_mclk;

assign mclken = mclken_mclk & ~mclken_d_mclk;
assign mrreq = mrreq_mclk & mclken;
assign mwreq = mwreq_mclk & mclken;


//////////////////////////////////////////////////////////////////////
// Merge down interfaces

always @* begin
  if (arreq | awreq) begin
    rreq = arreq;
    wreq = awreq;
    addr = aaddr;
    dataw = D_I;
  end
  else if (brreq | bwreq) begin
    rreq = brreq;
    wreq = bwreq;
    addr = baddr;
    dataw = D_I;
  end
  else if (mrreq | mwreq) begin
    rreq = mrreq;
    wreq = mwreq;
    addr = maddr;
    dataw = mdataw;
  end
  else begin
    rreq = 1'b0;
    wreq = 1'b0;
    addr = 32'bx;
    dataw = 8'bx;
  end
end


//////////////////////////////////////////////////////////////////////
// AXI access state machine

assign dstrb = (8'b1 << awaddr[2:0]);

wire ev_arstart = AXI_EN & rreq & (rts == RTS_IDLE);
wire ev_awstart = AXI_EN & wreq & (wts == WTS_IDLE);
wire ev_ardone = ARVALID & ARREADY;
wire ev_awdone = AWVALID & AWREADY;
wire ev_wdone = WVALID & WREADY;
wire ev_rdone = RVALID & RREADY;
wire ev_bdone = BVALID & BREADY;

always @(posedge ACLK) begin
  if (ev_arstart) begin
    rts <= RTS_RADDR;
    araddr <= addr;
  end
  if (ev_awstart) begin
    wts <= WTS_WADDR;
    wdata <= dataw;
    awaddr <= addr;
  end
  if (ev_ardone) begin
    rts <= RTS_RDATA;
  end
  if (ev_awdone) begin
    wts <= WTS_WDATA;
  end
  if (ev_wdone) begin
    wts <= WTS_WWAIT;
  end
  if (ev_rdone) begin
    if (RRESP != 2'b00)
      axi_error <= 1'b1;
    rdata <= RDATA;
    rts <= RTS_IDLE;
  end
  if (ev_bdone) begin
    if (BRESP != 2'b00)
      axi_error <= 1'b1;
    wts <= WTS_IDLE;
  end

  if (cp2_negedge_pe) begin
    if (rts != RTS_IDLE)
      to_error <= 1'b1;
    if (wts != WTS_IDLE)
      to_error <= 1'b1;
  end
end

// Access timing measurement
always @* begin
  atm_rnow = atm_rnow_d + 1'd1;
  if (rts == RTS_IDLE)
      atm_rnow = 8'd0;
end

always @* begin
  atm_wnow = atm_wnow_d + 1'd1;
  if (wts == WTS_IDLE)
      atm_wnow = 8'd0;
end

always @(posedge ACLK) begin
  atm_rnow_d <= atm_rnow;
  atm_wnow_d <= atm_wnow;
end

always @(posedge ACLK) begin
  if (ev_rdone) begin
    if (atm_rdone_min > atm_rnow)
      atm_rdone_min <= atm_rnow;
    if (atm_rdone_max < atm_rnow)
      atm_rdone_max <= atm_rnow;
  end
  if (ev_bdone) begin
    if (atm_wdone_min > atm_wnow)
      atm_wdone_min <= atm_wnow;
    if (atm_wdone_max < atm_wnow)
      atm_wdone_max <= atm_wnow;
  end

  if (atm_rw_reset) begin
    atm_rdone_min <= ~8'd0;
    atm_rdone_max <= 8'd0;
    atm_wdone_min <= ~8'd0;
    atm_wdone_max <= 8'd0;
  end
end

always @(posedge MCLK) begin
  if (~CPU_READY)
    atm_not_ready_cnt <= atm_not_ready_cnt + 1'd1;
  if (atm_not_ready_reset)
    atm_not_ready_cnt <= 32'd0;
end

always @* begin
  case (araddr[2:0])
    3'b000: d_o = rdata[(0*8+7):(0*8)];
    3'b001: d_o = rdata[(1*8+7):(1*8)];
    3'b010: d_o = rdata[(2*8+7):(2*8)];
    3'b011: d_o = rdata[(3*8+7):(3*8)];
    3'b100: d_o = rdata[(4*8+7):(4*8)];
    3'b101: d_o = rdata[(5*8+7):(5*8)];
    3'b110: d_o = rdata[(6*8+7):(6*8)];
    3'b111: d_o = rdata[(7*8+7):(7*8)];
    default: d_o = 8'hxx;
  endcase
end

always @* begin
  //``REGION_REGS EMUBUS_CTRL
  mon_dout = 32'b0;
  case (MON_SEL)                //``SUBREGS CPUMEM
    8'h01: begin                //``REG CTRL
      mon_dout[0] = AXI_EN;
    end
    8'h02: begin                //``REG STAT
      mon_dout[0] = axi_error;  //``FIELD AXI_ERROR
      mon_dout[1] = to_error;   //``FIELD TO_ERROR
      mon_dout[2] = CPU_READY;
      mon_dout[3] = IDLE;
    end
    8'h08: begin                //``REG MCMI
      mon_dout[0] = mrreq_mclk; //``FIELD MRREQ
      mon_dout[1] = mwreq_mclk; //``FIELD MWREQ
    end
    8'h09: mon_dout = maddr;
    8'h0A: mon_dout[7:0] = mdataw;

    8'h10: mon_dout[16:0] = wmadd;

    8'h20: mon_dout = araddr;
    8'h21: mon_dout[7:0] = d_o; //``REG RDATA // 8-bit read output
    8'h22: mon_dout = awaddr;
    8'h23: mon_dout[7:0] = wdata; //``REG // 8-bit write input

    8'h40: mon_dout[7:0] = atm_rdone_max;
    8'h41: mon_dout[7:0] = atm_rdone_min;
    8'h42: mon_dout[7:0] = atm_wdone_max;
    8'h43: mon_dout[7:0] = atm_wdone_min;

    8'h50: mon_dout = atm_not_ready_cnt;

    8'h80: mon_dout[1:0] = rts;  //``REG RTS // Read Transaction State
    8'h81: mon_dout[1:0] = wts;  //``REG WTS // Write Transaction State
    default: ;
  endcase
end

always @(posedge MCLK) begin
  atm_rw_reset <= 1'b0;
  atm_not_ready_reset <= 1'b0;
  case (MON_SEL)
    8'h40: atm_rw_reset <= MON_WS;
    8'h50: atm_not_ready_reset <= MON_WS;
    default: ;
  endcase
end

assign IDLE = (rts == RTS_IDLE) & (wts == WTS_IDLE);
assign CPU_READY = ~CP2_NEGEDGE_PRE | IDLE;

assign MON_DOUT = mon_dout;

assign D_O = d_o;

assign ARADDR = araddr;
assign ARBURST = 2'b01;         // INCR
assign ARCACHE = 4'b1111;       // Write-back read-allocate
assign ARID = axi_id;
assign ARLEN = 4'd0;            // 1 beat
assign ARLOCK = 2'b00;
assign ARPROT = 3'b000;
assign ARQOS = 4'b0;
assign ARSIZE = 3'd0;           // 1 byte
assign ARUSER = axi_user;
assign ARVALID = (rts == RTS_RADDR);

assign AWADDR = awaddr;
assign AWBURST = 2'b01;         // INCR
assign AWCACHE = 4'b1111;       // Write-back write-allocate
assign AWID = axi_id;
assign AWLEN = 4'd0;            // 1 beat
assign AWLOCK = 2'b00;
assign AWPROT = 3'b000;
assign AWQOS = 4'b0;
assign AWSIZE = 0;              // 1 byte
assign AWUSER = axi_user;
assign AWVALID = (wts == WTS_WADDR);

assign BREADY = (wts == WTS_WWAIT);

assign RREADY = (rts == RTS_RDATA);

assign WDATA = {8{wdata}};
assign WID = axi_id;
assign WLAST = 1'b1;
assign WSTRB = dstrb;
assign WVALID = (wts == WTS_WDATA);

endmodule
