`timescale 1us / 1ns

// CDC synchronizer between emubus AXI and APU debug monitor interfaces

module emubus_apu_mon
  (
   // emubus read/write interface
   input         ARESETn,
   input         EMUBUS_CLK,
   input [7:0]   EMUBUS_ADDR,
   input         EMUBUS_AVALID,
   output [31:0] EMUBUS_RDATA,
   input         EMUBUS_RREQ,
   output reg    EMUBUS_RVALID,
   input         EMUBUS_RREADY,
   input [31:0]  EMUBUS_WDATA,
   input         EMUBUS_WREQ,
   output reg    EMUBUS_BVALID,
   input         EMUBUS_BREADY,

   // APU debug monitor interface
   input         APU_MCLK,
   output [7:0]  APU_MON_SEL,
   output        APU_MON_READY,
   input [31:0]  APU_MON_DOUT,
   input         APU_MON_VALID,
   output        APU_MON_WS,
   output [31:0] APU_MON_DIN
   );


//////////////////////////////////////////////////////////////////////
// Clock domain crossing

// ARESETn
wire apu_mon_aresetn;
cdc_1bit_2ff aresetn_sync
  (
   .A_DATA(ARESETn),
   .B_CLK(APU_MCLK),
   .B_DATA(apu_mon_aresetn)
   );

// EMUBUS_A*
reg emubus_avalid_d;
initial
  emubus_avalid_d = 1'b0;
always @(posedge EMUBUS_CLK)
  emubus_avalid_d <= EMUBUS_AVALID;
wire emubus_avalid_posedge = EMUBUS_AVALID & ~emubus_avalid_d;

wire [7:0] apu_mon_abuf;

cdc_mcp #(.WIDTH(8)) a_mcp
  (
   .A_CLK(EMUBUS_CLK),
   .A_NEXT(emubus_avalid_posedge),
   .A_DATA(EMUBUS_ADDR),

   .B_CLK(APU_MCLK),
   .B_NEXT(),
   .B_ACK(),
   .B_DATA(apu_mon_abuf)
   );

// EMUBUS_R*
reg emubus_rreq_tog, emubus_rreq_d;
initial begin
  emubus_rreq_tog = 1'b0;
  emubus_rreq_d = 1'b0;
end
always @(posedge EMUBUS_CLK) begin
  emubus_rreq_d <= EMUBUS_RREQ;
  emubus_rreq_tog <= emubus_rreq_tog ^ (EMUBUS_RREQ & ~emubus_rreq_d);
end

reg [31:0] apu_mon_rbuf;
reg        apu_mon_rnext;
wire       apu_mon_rreq_tog, emubus_rnext;

cdc_1bit_2ff rreq_sync
  (
   .A_DATA(emubus_rreq_tog),
   .B_CLK(APU_MCLK),
   .B_DATA(apu_mon_rreq_tog)
   );

cdc_mcp #(.WIDTH(32)) r_mcp
  (
   .A_CLK(APU_MCLK),
   .A_NEXT(apu_mon_rnext),
   .A_DATA(apu_mon_rbuf),

   .B_CLK(EMUBUS_CLK),
   .B_NEXT(emubus_rnext),
   .B_ACK(),
   .B_DATA(EMUBUS_RDATA)
   );

always @(posedge EMUBUS_CLK)
  EMUBUS_RVALID <= (EMUBUS_RVALID | emubus_rnext) & (ARESETn & ~(EMUBUS_RREADY & EMUBUS_RVALID));

// EMUBUS_W*
reg emubus_wreq_d;
always @(posedge EMUBUS_CLK)
  emubus_wreq_d <= EMUBUS_WREQ;
wire emubus_wreq_posedge = EMUBUS_WREQ & ~emubus_wreq_d;

wire [31:0] apu_mon_wbuf;
wire        apu_mon_wnext;
wire        emubus_bvalid_tog;
reg         emubus_bvalid_tog_d;
reg         apu_mon_bvalid_tog;

cdc_mcp #(.WIDTH(32)) w_mcp
  (
   .A_CLK(EMUBUS_CLK),
   .A_NEXT(emubus_wreq_posedge),
   .A_DATA(EMUBUS_WDATA),

   .B_CLK(APU_MCLK),
   .B_NEXT(apu_mon_wnext),
   .B_ACK(),
   .B_DATA(apu_mon_wbuf)
   );

cdc_1bit_2ff bvalid_sync
  (
   .A_DATA(apu_mon_bvalid_tog),
   .B_CLK(EMUBUS_CLK),
   .B_DATA(emubus_bvalid_tog)
   );

wire emubus_bvalid_posedge = emubus_bvalid_tog ^ emubus_bvalid_tog_d;

always @(posedge EMUBUS_CLK) begin
  emubus_bvalid_tog_d <= emubus_bvalid_tog;
  EMUBUS_BVALID <= (EMUBUS_BVALID | emubus_bvalid_posedge) & (ARESETn & ~(EMUBUS_BREADY & EMUBUS_BVALID));
end


//////////////////////////////////////////////////////////////////////
// APU debug monitor leader

reg       apu_mon_write, apu_mon_read;
reg       apu_mon_rreq_tog_d;
reg       apu_mon_wreq;

initial begin
  apu_mon_write = 1'b0;
  apu_mon_read = 1'b0;
  apu_mon_bvalid_tog = 1'b0;
  apu_mon_rnext = 1'b0;
  apu_mon_wreq = 1'b0;
end

always @(posedge APU_MCLK) begin
  if (~apu_mon_aresetn) begin
    apu_mon_write <= 1'b0;
    apu_mon_read <= 1'b0;
    apu_mon_rreq_tog_d <= apu_mon_rreq_tog;
    apu_mon_rnext <= 1'b0;
    apu_mon_wreq <= 1'b0;
  end
  else begin
    apu_mon_rnext <= 1'b0;
    apu_mon_wreq <= apu_mon_wreq | apu_mon_wnext;

    if (1'b1) begin             // TODO: remove
      if (apu_mon_read & APU_MON_VALID) begin
        apu_mon_read <= 1'b0;
        apu_mon_rnext <= 1'b1;
      end
      else if (apu_mon_write & APU_MON_VALID) begin
        apu_mon_write <= 1'b0;
        apu_mon_bvalid_tog <= ~apu_mon_bvalid_tog;
      end
      else begin
        if (apu_mon_rreq_tog ^ apu_mon_rreq_tog_d) begin
          apu_mon_rreq_tog_d <= apu_mon_rreq_tog;
          apu_mon_read <= 1'b1;
        end
        else if (apu_mon_wreq) begin
          apu_mon_wreq <= 1'b0;
          apu_mon_write <= 1'b1;
        end
      end
    end
  end
end

always @*
  apu_mon_rbuf = APU_MON_DOUT;

assign APU_MON_READY = (apu_mon_read | apu_mon_write) & ~APU_MON_VALID;
assign APU_MON_DIN = apu_mon_wbuf;
assign APU_MON_SEL = apu_mon_abuf;
assign APU_MON_WS = apu_mon_write;

endmodule
