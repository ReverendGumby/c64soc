module emubus
  (
   // EMU clocks
   input             CLK,
   input             CLKEN,
   input             APU_MCLK,

   // AXI Lite slave bus interface
   input             ARESETn,
   input [31:0]      AWADDR,
   input             AWVALID,
   output reg        AWREADY,
   input [31:0]      WDATA,
   output reg        WREADY,
   input [3:0]       WSTRB,
   input             WVALID,
   output [1:0]      BRESP,
   output reg        BVALID,
   input             BREADY,
   input [31:0]      ARADDR,
   input             ARVALID,
   output reg        ARREADY,
   output reg [31:0] RDATA,
   output [1:0]      RRESP,
   output reg        RVALID,
   input             RREADY,

   // System control
   input             EXT_RES, // external (button) reset
   output reg        CLK_RESET, // reset all clkgen
   output reg        EMU_nRES, // EMU MLB reset
   output reg        CART_nRES, // cartridge emulation reset
   output reg        RES_COLD, // reset as if from first power-on
   output reg        SYS_HOLD_REQ, // full emulator hold
   input             SYS_HOLD_ACK,
   output reg        CPU_HOLD_REQ, // CPU+PPU-only hold
   input             CPU_HOLD_ACK,
   output reg        APU_HOLD_REQ, // APU-only hold
   input             APU_HOLD_ACK,
   output reg        LOCK_APU_TO_CPU_HOLD,

   // Controller I/O
   input [1:0]       CTLR1_I, // pins 2, 3
   output [2:0]      CTLR1_O, // pins 4, 5, ?
   input [1:0]       CTLR2_I, // pins 2, 3
   output [2:0]      CTLR2_O, // pins 4, 5, ?

   // APU RAM interface
   output            APU_RAM_RE,
   output            APU_RAM_WE,
   input [7:0]       APU_RAM_D_I,
   output [7:0]      APU_RAM_D_O,
   output [15:0]     APU_RAM_A_O,

   // APU debug monitor interface
   output [7:0]      APU_MON_SEL,
   output            APU_MON_READY,
   input [31:0]      APU_MON_DOUT,
   input             APU_MON_VALID,
   output            APU_MON_WS,
   output [31:0]     APU_MON_DIN,

   // CPU (non-core) debug monitor interface
   output reg [7:0]  CPU_MON_SEL,
   output            CPU_MON_READY,
   input [31:0]      CPU_MON_DOUT,
   input             CPU_MON_VALID,
   output reg [3:0]  CPU_MON_WS,
   output reg [31:0] CPU_MON_DIN,

   // CPU core debug monitor interface
   output reg [5:0]  CPU_CORE_MON_SEL,
   output reg        CPU_CORE_MON_WS,
   input [31:0]      CPU_CORE_MON_DOUT,
   output reg [31:0] CPU_CORE_MON_DIN,

   // PPU debug monitor interface
   output reg [10:0] PPU_MON_SEL,
   output            PPU_MON_READY,
   input [31:0]      PPU_MON_DOUT,
   input             PPU_MON_VALID,
   output reg [3:0]  PPU_MON_WS,
   output reg [31:0] PPU_MON_DIN,

   // CPU in-circuit debugger interface
   output reg        ICD_SYS_ENABLE,
   output reg        ICD_SYS_FORCE_HALT,
   output reg        ICD_SYS_RESUME,
   input             ICD_SYS_HOLD,
   output reg [7:0]  ICD_MATCH_ENABLE,
   input [7:0]       ICD_MATCH_TRIGGER,
   output reg [2:0]  ICD_MATCH_SEL,
   output reg [3:0]  ICD_MATCH_REG_SEL,
   output reg [3:0]  ICD_MATCH_REG_WSTRB,
   output [31:0]     ICD_MATCH_REG_DIN,
   input [31:0]      ICD_MATCH_REG_DOUT,

   // CPU memory bridge monitor interface
   output reg        CPUMEM_AXI_EN,
   output reg [31:0] CPUMEM_BASE_WRAM,
   output reg [31:0] CPUMEM_BASE_ROM,
   output reg [7:0]  CPUMEM_MON_SEL,
   output reg        CPUMEM_MON_WS,
   input [31:0]      CPUMEM_MON_DOUT,
   output reg [31:0] CPUMEM_MON_DIN,

   // Game cartridge emulator, control interface
   output reg [11:2] CART_CTRL_ADDR,
   output reg [3:0]  CART_CTRL_WSTRB,
   output [31:0]     CART_CTRL_DIN,
   input [31:0]      CART_CTRL_DOUT,

   // B-Bus (I/O bus) interface
   output [7:0]      IO_PA,
   output            IO_PA_OE,
   input [7:0]       IO_D_I,
   output [7:0]      IO_D_O,
   output            IO_D_OE,
   output            IO_nPARD,
   output            IO_nPAWR,

   // PPU signal interface
   input             PPU_HBLANK,
   input             PPU_VBLANK,

   // PPU VRAM monitor interface
   output [14:0]     PPU_VRAM_A_O,
   output            PPU_VRAM_RE,
   output            PPU_VRAMA_WE,
   input [7:0]       PPU_VRAMA_D_I,
   output [7:0]      PPU_VRAMA_D_O,
   output            PPU_VRAMB_WE,
   input [7:0]       PPU_VRAMB_D_I,
   output [7:0]      PPU_VRAMB_D_O
   );

reg         emu_usr_nres;

reg [31:0]  ctlr1;
reg [31:0]  ctlr2;

reg         cpu_mon_ready_read, cpu_mon_ready_write;

reg         ppu_mon_ready_read, ppu_mon_ready_write;

reg         apu_ram_avalid, apu_ram_aready;
wire        apu_ram_rvalid, apu_ram_bvalid;
reg         apu_ram_rreq, apu_ram_wreq;
reg [15:0]  apu_ram_addr;
wire [7:0]  apu_ram_rdata;
reg [7:0]   apu_ram_wdata;

reg         apu_mon_avalid;
wire        apu_mon_rvalid, apu_mon_bvalid;
reg         apu_mon_rreq, apu_mon_wreq;
reg [7:0]   apu_mon_sel;
wire [31:0] apu_mon_rdata;
reg [31:0]  apu_mon_wdata;

reg         io_avalid;
wire        io_rvalid, io_bvalid;
reg         io_rreq, io_wreq;
reg [7:0]   io_addr;
wire [7:0]  io_rdata;
reg [7:0]   io_wdata;

reg         ppu_vram_rvalid;
reg [15:0]  ppu_vram_wdata;
reg [1:0]   ppu_vram_wstrb;


//////////////////////////////////////////////////////////////////////
// Register interface

// address range    size  bits   ussage
// -------------    ----  ----   ------
// 0_0000 - 0_0FFF   4K   8-32   emubus control
// 0_1000 - 0_1FFF   4K   8-32   cartridge control
// 0_2000 - 0_FFFF  56K      -   (reserved)
// 1_0000 - 1_FFFF  64K      8   APU (SHVC-SOUND) memory
// 2_0000 - 2_FFFF  64K   8-16   PPU VRAM
// 3_0000 - 7_FFFF 320K      -   (reserved)

localparam RRSVD       = 4'd0;
localparam EMUBUS_CTRL = 4'd1;
localparam APU_MEM     = 4'd2;
localparam CART_CTRL   = 4'd3;
localparam PPU_VRAM    = 4'd4;

function [3:0] AxADDR_region(input [31:0] AxADDR);
  begin
    casez (AxADDR[18:0])
//``REGION_START
      19'h0_0zzz: AxADDR_region = EMUBUS_CTRL;
      19'h0_1zzz: AxADDR_region = CART_CTRL;
      19'h1_zzzz: AxADDR_region = APU_MEM;
      19'h2_zzzz: AxADDR_region = PPU_VRAM;
//``REGION_END
      default:    AxADDR_region = RRSVD;
    endcase
  end
endfunction

reg [3:0] rregion, wregion;

always @* begin
  rregion = AxADDR_region(ARADDR);
  wregion = AxADDR_region(AWADDR);
end

reg [31:0] rdata;
reg        rvalid, bvalid;
reg        cart_rvalid;

always @* begin
  rdata = 32'h00000000;
  rvalid = 1'b1;
  cpu_mon_ready_read = 1'b0;
  ppu_mon_ready_read = 1'b0;
  apu_mon_rreq = 1'b0;
  io_rreq = 1'b0;
  if (rregion == EMUBUS_CTRL) begin //``REGION_REGS
    /* verilator lint_off CASEX */
    casex (ARADDR[11:2])
      10'h0x: rdata = 32'h53485643; //``REG ID DATA
      10'h10: begin             //``REG CTLR1_MATRIX
        rdata[31:00] = ctlr1;   //``NO_FIELD
      end
      10'h11: begin             //``REG CTLR2_MATRIX
        rdata[31:00] = ctlr2;   //``NO_FIELD
      end
      10'h14: begin               //``REG RESETS
        rdata[0] = emu_usr_nres;  //``FIELD EMU
        rdata[2] = CART_nRES;     //``FIELD CART
      end
      10'h15: begin             //``REG RESET_CTRL
        rdata[0] = RES_COLD;    //``FIELD COLD
      end
      10'h16: begin
        rdata[7:0] = CPU_MON_SEL; //``FIELD CPU_MON_SEL
      end
      10'h17: begin
        rvalid = CPU_MON_VALID;
        cpu_mon_ready_read = ARVALID & ~ARREADY & ~CPU_MON_VALID;
        rdata[31:0] = CPU_MON_DOUT; //``FIELD CPU_MON_DATA
      end
      10'h18: begin
        rdata[5:0] = CPU_CORE_MON_SEL; //``FIELD CPU_CORE_MON_SEL
      end
      10'h19: begin
        rdata[31:0] = CPU_CORE_MON_DOUT; //``FIELD CPU_CORE_MON_DATA
      end
      10'h1a: begin
        rdata[10:0] = PPU_MON_SEL;
      end
      10'h1b: begin
        rvalid = PPU_MON_VALID;
        ppu_mon_ready_read = ARVALID & ~ARREADY & ~PPU_MON_VALID;
        rdata[31:0] = PPU_MON_DOUT; //``FIELD PPU_MON_DATA
      end
      10'h1c: begin                    //``REG ICD_CTRL
        rdata[0] = ICD_SYS_ENABLE;     //``FIELD SYS_ENABLE
        rdata[1] = ICD_SYS_FORCE_HALT; //``FIELD SYS_FORCE_HALT
        rdata[2] = ICD_SYS_RESUME;     //``FIELD SYS_RESUME
        rdata[8] = ICD_SYS_HOLD;       //``FIELD SYS_HOLD
        rdata[23:16] = ICD_MATCH_ENABLE;  //``FIELD MATCH_ENABLE
        rdata[31:24] = ICD_MATCH_TRIGGER; //``FIELD MATCH_TRIGGER
      end
      10'h1d: begin
        rdata[2:0] = ICD_MATCH_SEL;
      end
      10'h1e: begin
        rdata[3:0] = ICD_MATCH_REG_SEL;
      end
      10'h1f: rdata = ICD_MATCH_REG_DOUT; //``REG ICD_MATCH_REG_DATA
      10'h20: begin             //``REG HOLD
        rdata[0] = SYS_HOLD_REQ;
        rdata[1] = CPU_HOLD_REQ;
        rdata[2] = APU_HOLD_REQ;
        rdata[8] = LOCK_APU_TO_CPU_HOLD;
        rdata[16] = SYS_HOLD_ACK;
        rdata[17] = CPU_HOLD_ACK;
        rdata[18] = APU_HOLD_ACK;
      end
      10'h24: begin
        rdata[7:0] = APU_MON_SEL; //``FIELD APU_MON_SEL
      end
      10'h25: begin             //``REG APU_MON_DATA
        apu_mon_rreq = ARVALID & ~ARREADY;
        rvalid = apu_mon_rvalid;
        rdata = apu_mon_rdata;
      end
      10'h26: begin             //``REG IO_ADDR
        rdata[7:0] = io_addr; //``NO_FIELD
      end
      10'h27: begin             //``REG IO_DATA
        io_rreq = ARVALID & ~ARREADY;
        rvalid = io_rvalid;
        rdata[7:0] = io_rdata; //``NO_FIELD
      end
      10'h28: begin             //``REG PPU_STATUS
        rdata[0] = PPU_HBLANK;  //``FIELD HBLANK
        rdata[1] = PPU_VBLANK;  //``FIELD VBLANK
      end
      10'h30: rdata = CPUMEM_BASE_WRAM;
      10'h31: rdata = CPUMEM_BASE_ROM;
      10'h32: begin               //``REG CPUMEM_CTRL
        rdata[0] = CPUMEM_AXI_EN; //``FIELD AXI_EN
      end
      10'h34: begin
        rdata[7:0] = CPUMEM_MON_SEL;
      end
      10'h35: begin             //``REG CPUMEM_MON_DATA
        rdata = CPUMEM_MON_DOUT;
      end
      default: ;
    endcase
  end
  else if (rregion == APU_MEM) begin
    rdata = {4{apu_ram_rdata}};
    rvalid = apu_ram_rvalid;
  end
  else if (rregion == CART_CTRL) begin
    rvalid = cart_rvalid;
    rdata = CART_CTRL_DOUT;
  end
  else if (rregion == PPU_VRAM) begin
    rdata = {2{PPU_VRAMB_D_I, PPU_VRAMA_D_I}};
    rvalid = ppu_vram_rvalid;
  end
end

always @(posedge CLK) begin
  if (!ARESETn) begin
    ARREADY <= 1'b0;
    RVALID <= 1'b0;
  end
  else begin
    if (!ARREADY && ARVALID) begin
      if (rvalid) begin
        ARREADY <= 1'b1;
        RDATA <= rdata;
        RVALID <= 1'b1;
      end
    end
    else if (RVALID) begin
      ARREADY <= 1'b0;
      if (RREADY)
        RVALID <= 1'b0;
    end
  end
end

always @(posedge CLK) begin
  if (!ARESETn) begin
    CART_CTRL_ADDR <= 10'h000;
    cart_rvalid <= 1'b0;
  end
  else begin
    cart_rvalid <= 1'b0;
    if (!ARREADY && ARVALID && rregion == CART_CTRL) begin
      CART_CTRL_ADDR <= ARADDR[11:2];
      cart_rvalid <= 1'b1;
    end
    else if (!AWREADY && AWVALID && wregion == CART_CTRL)
      CART_CTRL_ADDR <= AWADDR[11:2];
  end
end

assign RRESP = 2'b00;

always @* begin
  bvalid = 1'b1;
  if (wregion == APU_MEM)
    bvalid = apu_ram_bvalid;
  if (wregion == EMUBUS_CTRL) begin
    if (AWADDR[11:2] == 10'h17)
      bvalid = cpu_mon_ready_write & CPU_MON_VALID;
    if (AWADDR[11:2] == 10'h1b)
      bvalid = ppu_mon_ready_write & PPU_MON_VALID;
    if (AWADDR[11:2] == 10'h25)
      bvalid = apu_mon_bvalid;
    if (AWADDR[11:2] == 10'h27)
      bvalid = io_bvalid;
  end
end

always @(posedge CLK) begin
  if (!ARESETn) begin
    AWREADY <= 1'b0;
    WREADY <= 1'b0;
    BVALID <= 1'b0;
  end
  else begin
    if (!AWREADY && AWVALID && !WREADY && WVALID && !BVALID) begin
      if (bvalid) begin
        AWREADY <= 1'b1;
        WREADY <= 1'b1;
        BVALID <= 1'b1;
      end
    end
    else if (BVALID) begin
      AWREADY <= 1'b0;
      WREADY <= 1'b0;
      if (BREADY)
        BVALID <= 1'b0;
    end
  end
end

assign BRESP = 2'b00;

initial begin
  CPU_MON_WS = 4'b0;
  CPU_CORE_MON_WS = 1'b0;
  CPUMEM_MON_WS = 1'b0;
  PPU_MON_WS = 4'b0;
  apu_ram_wreq = 1'b0;
  apu_mon_avalid = 1'b0;
  apu_mon_wreq = 1'b0;
  io_avalid = 1'b0;
  io_wreq = 1'b0;
end

always @(posedge CLK) begin
  cpu_mon_ready_write <= 1'b0;
  CPU_MON_WS <= 4'b0;
  CPU_CORE_MON_WS <= 1'b0;
  CPUMEM_MON_WS <= 1'b0;
  PPU_MON_WS <= 4'b0;
  ppu_mon_ready_write <= 1'b0;
  apu_ram_wreq <= 1'b0;
  apu_mon_avalid <= 1'b0;
  io_avalid <= 1'b0;
  ppu_vram_wstrb <= 2'b00;

  if (!ARESETn) begin
    ctlr1 <= 32'h0;
    ctlr2 <= 32'h0;
    emu_usr_nres <= 1'b0;
    CART_nRES <= 1'b0;
    RES_COLD <= 1'b1;
    SYS_HOLD_REQ <= 1'b0;
    CPU_HOLD_REQ <= 1'b0;
    APU_HOLD_REQ <= 1'b0;
    LOCK_APU_TO_CPU_HOLD <= 1'b0;
    CART_CTRL_WSTRB <= 4'h0;
    ICD_MATCH_REG_WSTRB <= 4'h0;
    ICD_SYS_ENABLE <= 1'b0;
    ICD_SYS_FORCE_HALT <= 1'b0;
    ICD_SYS_RESUME <= 1'b0;
    ICD_MATCH_ENABLE <= 8'b0;
    ICD_MATCH_SEL <= 3'd0;
    ICD_MATCH_REG_SEL <= 4'd0;
    CPU_MON_SEL <= 8'h0;
    CPU_CORE_MON_SEL <= 6'h0;
    apu_mon_sel <= 8'h0;
    apu_ram_wreq <= 1'b0;
    apu_mon_wreq <= 1'b0;
    io_addr <= 8'h0;
    io_wreq <= 1'b0;
    CPUMEM_AXI_EN <= 1'b1;
    CPUMEM_BASE_WRAM <= 32'h0;
    CPUMEM_BASE_ROM <= 32'h0;
    CPUMEM_MON_SEL <= 8'h0;
    PPU_MON_SEL <= 11'h0;
  end
  else begin
    CART_CTRL_WSTRB <= 4'h0;
    ICD_MATCH_REG_WSTRB <= 4'h0;
    if (!AWREADY && AWVALID && !WREADY && WVALID && !BVALID) begin
      if (wregion == EMUBUS_CTRL) begin
        case (AWADDR[11:2])
          10'h10: begin
            if (WSTRB[0]) ctlr1[07:00] <= WDATA[07:00];
            if (WSTRB[1]) ctlr1[15:08] <= WDATA[15:08];
            if (WSTRB[2]) ctlr1[23:16] <= WDATA[23:16];
            if (WSTRB[3]) ctlr1[31:24] <= WDATA[31:24];
          end
          10'h11: begin
            if (WSTRB[0]) ctlr2[07:00] <= WDATA[07:00];
            if (WSTRB[1]) ctlr2[15:08] <= WDATA[15:08];
            if (WSTRB[2]) ctlr2[23:16] <= WDATA[23:16];
            if (WSTRB[3]) ctlr2[31:24] <= WDATA[31:24];
          end
          10'h14: begin
            if (WSTRB[0]) begin
              emu_usr_nres <= WDATA[0];
              CART_nRES <= WDATA[2];
            end
          end
          10'h15: begin
            if (WSTRB[0]) begin
              RES_COLD <= WDATA[0];
            end
          end
          10'h16: begin
            if (WSTRB[0]) begin
              CPU_MON_SEL <= WDATA[7:0];
            end
          end
          10'h17: begin
            cpu_mon_ready_write <= 1'b1;
            CPU_MON_WS <= WSTRB;
            CPU_MON_DIN <= WDATA;
          end
          10'h18: begin
            if (WSTRB[0]) begin
              CPU_CORE_MON_SEL <= WDATA[5:0];
            end
          end
          10'h19: begin
            CPU_CORE_MON_WS <= WSTRB[0]; // only 32-bit writes expected
            CPU_CORE_MON_DIN <= WDATA;
          end
          10'h1a: begin
            if (WSTRB[0]) PPU_MON_SEL[07:00] <= WDATA[07:00];
            if (WSTRB[1]) PPU_MON_SEL[10:08] <= WDATA[10:08];
          end
          10'h1b: begin
            ppu_mon_ready_write <= 1'b1;
            PPU_MON_WS <= WSTRB;
            PPU_MON_DIN <= WDATA;
          end
          10'h1c: begin
            if (WSTRB[0]) begin
              ICD_SYS_ENABLE <= WDATA[0];
              ICD_SYS_FORCE_HALT <= WDATA[1];
              ICD_SYS_RESUME <= WDATA[2];
            end
            if (WSTRB[2]) begin
              ICD_MATCH_ENABLE <= WDATA[23:16];
            end
          end
          10'h1d: begin
            if (WSTRB[0]) ICD_MATCH_SEL <= WDATA[2:0];
          end
          10'h1e: begin
            if (WSTRB[0]) ICD_MATCH_REG_SEL <= WDATA[3:0];
          end
          10'h1f: begin
            ICD_MATCH_REG_WSTRB <= WSTRB;
          end
          10'h20: begin
            if (WSTRB[0]) begin
              SYS_HOLD_REQ <= WDATA[0];
              CPU_HOLD_REQ <= WDATA[1];
              APU_HOLD_REQ <= WDATA[2];
            end
            if (WSTRB[1]) begin
              LOCK_APU_TO_CPU_HOLD <= WDATA[8];
            end
          end
          10'h24: begin
            if (WSTRB[0]) begin
              apu_mon_avalid <= 1'b1;
              apu_mon_sel <= WDATA[07:00];
            end
          end
          10'h25: begin
            apu_mon_wreq <= 1'b1;
            apu_mon_wdata <= WDATA;
          end
          10'h26: begin
            if (WSTRB[0]) begin
              io_avalid <= 1'b1;
              io_addr <= WDATA[7:0];
            end
          end
          10'h27: begin
            if (WSTRB[0]) begin
              io_wreq <= 1'b1;
              io_wdata <= WDATA[7:0];
            end
          end
          10'h30: begin
            CPUMEM_BASE_WRAM <= WDATA;
          end
          10'h31: begin
            CPUMEM_BASE_ROM <= WDATA;
          end
          10'h32: begin
            if (WSTRB[0]) begin
              CPUMEM_AXI_EN <= WDATA[0];
            end
          end
          10'h34: begin
            CPUMEM_MON_SEL <= WDATA[7:0];
          end
          10'h35: begin
            CPUMEM_MON_WS <= WSTRB[0]; // only 32-bit writes expected
            CPUMEM_MON_DIN <= WDATA;
          end
          default: ;
        endcase
      end
      else if (wregion == APU_MEM) begin
        apu_ram_wreq <= apu_ram_wreq | apu_ram_avalid;
        case ({AWADDR[1:0], 1'b1})
          {2'b11, WSTRB[3]}: apu_ram_wdata <= WDATA[31:24];
          {2'b10, WSTRB[2]}: apu_ram_wdata <= WDATA[23:16];
          {2'b01, WSTRB[1]}: apu_ram_wdata <= WDATA[15:08];
          {2'b00, WSTRB[0]}: apu_ram_wdata <= WDATA[07:00];
          default: apu_ram_wdata <= 8'hxx;
        endcase
      end
      else if (wregion == CART_CTRL) begin
        CART_CTRL_WSTRB <= WSTRB;
      end
      else if (wregion == PPU_VRAM) begin
        // Narrowing from 32-bit AXI to 16-bit PPU VRAM
        if (AWADDR[1]) begin
          ppu_vram_wdata <= WDATA[31:16];
          ppu_vram_wstrb <= WSTRB[3:2];
        end
        else begin
          ppu_vram_wdata <= WDATA[15:0];
          ppu_vram_wstrb <= WSTRB[1:0];
        end
      end
    end
    if (cpu_mon_ready_write & CPU_MON_VALID)
      cpu_mon_ready_write <= 1'b0;
    if (ppu_mon_ready_write & PPU_MON_VALID)
      ppu_mon_ready_write <= 1'b0;
    if (io_bvalid)
      io_wreq <= 1'b0;
    if (apu_ram_bvalid)
      apu_ram_wreq <= 1'b0;
    if (apu_mon_bvalid)
      apu_mon_wreq <= 1'b0;
  end
end

assign CART_CTRL_DIN = WDATA;
assign ICD_MATCH_REG_DIN = WDATA;
assign CPU_MON_READY = cpu_mon_ready_read | cpu_mon_ready_write;
assign PPU_MON_READY = ppu_mon_ready_read | ppu_mon_ready_write;


//////////////////////////////////////////////////////////////////////
// Controller I/O

// Buttons are normally open and read as 0. A 1 in ctlr_*[x] causes
// the button to read as 1. All buttons are latched into a shift
// register on CTLR*_I[1] falling edge. The LSB is inverted and drives
// CTLR*_O[0], and the register is shifted on CTLR*_I[0] rising
// edge. The register fills the shifted-in bit with 0.

reg [31:0] ctlr1_reg, ctlr2_reg;
reg        ctlr1_shift_d, ctlr2_shift_d;

wire       ctlr1_latch = CTLR1_I[1];
wire       ctlr2_latch = CTLR2_I[1];
wire       ctlr1_shift = !CTLR1_I[0];
wire       ctlr2_shift = !CTLR2_I[0];

always @(posedge CLK) begin
  if (!EMU_nRES) begin
    ctlr1_reg <= 32'h00;
    ctlr2_reg <= 32'h00;
    ctlr1_shift_d <= 1'b0;
    ctlr2_shift_d <= 1'b0;
  end
  else begin
    if (ctlr1_latch)
      ctlr1_reg <= ctlr1;
    else if (!ctlr1_shift && ctlr1_shift_d)
      ctlr1_reg <= {1'b0, ctlr1_reg[31:1]};

    if (ctlr2_latch)
      ctlr2_reg <= ctlr2;
    else if (!ctlr2_shift && ctlr2_shift_d)
      ctlr2_reg <= {1'b0, ctlr2_reg[31:1]};
  end

  ctlr1_shift_d <= ctlr1_shift;
  ctlr2_shift_d <= ctlr2_shift;
end

assign CTLR1_O = {2'b11, ~ctlr1_reg[0]};
assign CTLR2_O = {2'b11, ~ctlr2_reg[0]};


//////////////////////////////////////////////////////////////////////
// Reset generator

reg [6:0] rst_cnt;
reg       emu_rst;

initial begin
  CLK_RESET = 1'b1;
  rst_cnt = ~0;
  emu_rst = 1'b1;
end

always @(posedge CLK) begin
  CLK_RESET <= ~ARESETn;

  if (!ARESETn || !emu_usr_nres || EXT_RES) begin
    rst_cnt <= 7'b0;
    emu_rst <= 1'b1;
  end
  else if (CLKEN) begin
    if (&rst_cnt)
      emu_rst <= 1'b0;
    else
      rst_cnt <= rst_cnt + 1'b1;
  end
end

always @(posedge CLK) begin
  if (emu_rst)
    EMU_nRES <= 1'b0;
  else if (!EMU_nRES)
    EMU_nRES <= 1'b1;
end


//////////////////////////////////////////////////////////////////////
// APU RAM interface

emubus_apu_ram apu_ram
  (
   .ARESETn(ARESETn),
   .EMUBUS_CLK(CLK),
   .EMUBUS_ADDR(apu_ram_addr),
   .EMUBUS_AVALID(apu_ram_avalid),
   .EMUBUS_RDATA(apu_ram_rdata),
   .EMUBUS_RREQ(apu_ram_rreq),
   .EMUBUS_RVALID(apu_ram_rvalid),
   .EMUBUS_RREADY(RREADY),
   .EMUBUS_WDATA(apu_ram_wdata),
   .EMUBUS_WREQ(apu_ram_wreq),
   .EMUBUS_BVALID(apu_ram_bvalid),
   .EMUBUS_BREADY(BREADY),

   // APU RAM bus interface
   .APU_MCLK(APU_MCLK),
   .APU_RAM_A_O(APU_RAM_A_O),
   .APU_RAM_D_I(APU_RAM_D_I),
   .APU_RAM_D_O(APU_RAM_D_O),
   .APU_RAM_RE(APU_RAM_RE),
   .APU_RAM_WE(APU_RAM_WE)
   );

initial begin
  apu_ram_avalid = 1'b0;
  apu_ram_aready = 1'b1;
end

always @(posedge CLK) begin
  if (!ARESETn) begin
    apu_ram_avalid <= 1'b0;
    apu_ram_aready <= 1'b1;
  end
  else begin
    if (~ARREADY & ARVALID & (rregion == APU_MEM)) begin
      apu_ram_addr <= ARADDR[15:0];
      apu_ram_avalid <= apu_ram_aready;
      apu_ram_aready <= 1'b0;
    end
    else if (~AWREADY & AWVALID & (wregion == APU_MEM)) begin
      apu_ram_addr <= AWADDR[15:0];
      apu_ram_avalid <= apu_ram_aready;
      apu_ram_aready <= 1'b0;
    end
    else begin
      apu_ram_avalid <= 1'b0;
      apu_ram_aready <= 1'b1;
    end
  end
end

initial
  apu_ram_rreq = 1'b0;

always @(posedge CLK) begin
  if (!ARESETn) begin
    apu_ram_rreq <= 1'b0;
  end
  else begin
    if (~RVALID & RREADY & (rregion == APU_MEM)) begin
      apu_ram_rreq <= apu_ram_rreq | apu_ram_avalid;
    end
    if (apu_ram_rvalid)
      apu_ram_rreq <= 1'b0;
  end
end


//////////////////////////////////////////////////////////////////////
// APU debug monitor interface

emubus_apu_mon apu_mon
  (
   .ARESETn(ARESETn),
   .EMUBUS_CLK(CLK),
   .EMUBUS_ADDR(apu_mon_sel),
   .EMUBUS_AVALID(apu_mon_avalid),
   .EMUBUS_RDATA(apu_mon_rdata),
   .EMUBUS_RREQ(apu_mon_rreq),
   .EMUBUS_RVALID(apu_mon_rvalid),
   .EMUBUS_RREADY(RREADY),
   .EMUBUS_WDATA(apu_mon_wdata),
   .EMUBUS_WREQ(apu_mon_wreq),
   .EMUBUS_BVALID(apu_mon_bvalid),
   .EMUBUS_BREADY(BREADY),

   // APU debug monitor interface
   .APU_MCLK(APU_MCLK),
   .APU_MON_SEL(APU_MON_SEL),
   .APU_MON_READY(APU_MON_READY),
   .APU_MON_DOUT(APU_MON_DOUT),
   .APU_MON_VALID(APU_MON_VALID),
   .APU_MON_WS(APU_MON_WS),
   .APU_MON_DIN(APU_MON_DIN)
   );


//////////////////////////////////////////////////////////////////////
// B-Bus I/O bus interface

emubus_io bbus_io
  (
   .CLK(CLK),
   .CLKEN(CLKEN),

   .ARESETn(ARESETn),
   .EMUBUS_ADDR(io_addr),
   .EMUBUS_AVALID(io_avalid),
   .EMUBUS_RDATA(io_rdata),
   .EMUBUS_RREQ(io_rreq),
   .EMUBUS_RVALID(io_rvalid),
   .EMUBUS_RREADY(RREADY),
   .EMUBUS_WDATA(io_wdata),
   .EMUBUS_WREQ(io_wreq),
   .EMUBUS_BVALID(io_bvalid),
   .EMUBUS_BREADY(BREADY),

   // I/O bus interface
   .IO_PA(IO_PA),
   .IO_PA_OE(IO_PA_OE),
   .IO_D_I(IO_D_I),
   .IO_D_O(IO_D_O),
   .IO_D_OE(IO_D_OE),
   .IO_nPARD(IO_nPARD),
   .IO_nPAWR(IO_nPAWR)
   );


//////////////////////////////////////////////////////////////////////
// PPU VRAM monitor interface

reg [14:0] ppu_vram_addr;
reg        ppu_vram_avalid;
wire       ppu_vram_rreq, ppu_vram_wreq;

always @(posedge CLK) begin
  if (!ARESETn) begin
    ppu_vram_avalid <= 1'b0;
    ppu_vram_rvalid <= 1'b0;
  end
  else begin
    if (ppu_vram_rreq) begin
      ppu_vram_addr <= ARADDR[15:1];
      ppu_vram_avalid <= 1'b1;
    end
    else if (ppu_vram_wreq) begin
      ppu_vram_addr <= AWADDR[15:1];
      ppu_vram_avalid <= 1'b1;
    end
    else begin
      ppu_vram_avalid <= 1'b0;
    end

    ppu_vram_rvalid <= ppu_vram_avalid & ppu_vram_rreq;
  end
end

assign ppu_vram_rreq = ARVALID & (rregion == PPU_VRAM);
assign ppu_vram_wreq = AWVALID & (wregion == PPU_VRAM);

assign PPU_VRAM_A_O = ppu_vram_addr;
assign PPU_VRAM_RE = ppu_vram_rreq & ppu_vram_avalid & ~ppu_vram_rvalid;
assign PPU_VRAMA_WE = ppu_vram_wreq & ppu_vram_wstrb[0];
assign PPU_VRAMA_D_O = ppu_vram_wdata[7:0];
assign PPU_VRAMB_WE = ppu_vram_wreq & ppu_vram_wstrb[1];
assign PPU_VRAMB_D_O = ppu_vram_wdata[15:8];


//////////////////////////////////////////////////////////////////////

endmodule
