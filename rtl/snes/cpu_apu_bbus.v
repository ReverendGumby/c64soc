`timescale 1us / 1ns

// CDC from CPU to APU B-Bus I/O interface. Here we place the I/O port
// in the CPU clock domain, because nPARD/nPAWR are only active for a
// few clocks.

module cpu_apu_bbus
  (
   // CPU B-Bus interface, as connected at APU pins
   input        CPU_CLK,
   input [1:0]  CPU_PA,
   input [7:0]  CPU_D_I, // CPU -> APU
   output [7:0] CPU_D_O,
   output       CPU_D_OE,
   input        CPU_CS,
   input        CPU_nCS,
   input        CPU_nPARD,
   input        CPU_nPAWR,

   // APU unwrapped I/O interface
   input        APU_CLK,
   output [3:0] APU_CPUIN,
   output [7:0] APU_CPUI0,
   output [7:0] APU_CPUI1,
   output [7:0] APU_CPUI2,
   output [7:0] APU_CPUI3,
   input [3:0]  APU_CPUON,
   input [7:0]  APU_CPUO0,
   input [7:0]  APU_CPUO1,
   input [7:0]  APU_CPUO2,
   input [7:0]  APU_CPUO3
   );

reg [3:0]   c2a_valid, a2c_valid;
wire [3:0]  c2a_ready, a2c_ready;
wire [3:0]  c2an;
reg [3:0]   c2an_d;
reg [7:0]   cpui [0:3];           // CPU -> APU
wire [7:0]  cpuo [0:3];          // APU -> CPU

initial begin
  c2a_valid = 4'b0;
  a2c_valid = 4'b0;
end

always @(posedge CPU_CLK) begin
  c2a_valid <= c2a_valid & c2a_ready;

  if (CPU_CS & ~CPU_nCS & ~CPU_nPAWR) begin
    cpui[CPU_PA] <= CPU_D_I;
    c2a_valid[CPU_PA] <= 1'b1;
  end
end

cdc_mcp_fb #(8) c2a0 (.A_CLK(CPU_CLK), .A_VALID(c2a_valid[0]),
                      .A_READY(c2a_ready[0]), .A_DATA(cpui[0]),
                      .B_CLK(APU_CLK), .B_NEXT(c2an[0]),
                      .B_DATA(APU_CPUI0));

cdc_mcp_fb #(8) c2a1 (.A_CLK(CPU_CLK), .A_VALID(c2a_valid[1]),
                      .A_READY(c2a_ready[1]), .A_DATA(cpui[1]),
                      .B_CLK(APU_CLK), .B_NEXT(c2an[1]),
                      .B_DATA(APU_CPUI1));

cdc_mcp_fb #(8) c2a2 (.A_CLK(CPU_CLK), .A_VALID(c2a_valid[2]),
                      .A_READY(c2a_ready[2]), .A_DATA(cpui[2]),
                      .B_CLK(APU_CLK), .B_NEXT(c2an[2]),
                      .B_DATA(APU_CPUI2));

cdc_mcp_fb #(8) c2a3 (.A_CLK(CPU_CLK), .A_VALID(c2a_valid[3]),
                      .A_READY(c2a_ready[3]), .A_DATA(cpui[3]),
                      .B_CLK(APU_CLK), .B_NEXT(c2an[3]),
                      .B_DATA(APU_CPUI3));

always @(posedge APU_CLK)
  c2an_d <= c2an;

assign APU_CPUIN = c2an_d;

always @(posedge APU_CLK) begin
  a2c_valid <= (a2c_valid & a2c_ready) | APU_CPUON;
end

cdc_mcp_fb #(8) a2c0 (.A_CLK(APU_CLK), .A_VALID(a2c_valid[0]),
                      .A_READY(a2c_ready[0]), .A_DATA(APU_CPUO0),
                      .B_CLK(CPU_CLK), .B_NEXT(), .B_DATA(cpuo[0]));

cdc_mcp_fb #(8) a2c1 (.A_CLK(APU_CLK), .A_VALID(a2c_valid[1]),
                      .A_READY(a2c_ready[1]), .A_DATA(APU_CPUO1),
                      .B_CLK(CPU_CLK), .B_NEXT(), .B_DATA(cpuo[1]));

cdc_mcp_fb #(8) a2c2 (.A_CLK(APU_CLK), .A_VALID(a2c_valid[2]),
                      .A_READY(a2c_ready[2]), .A_DATA(APU_CPUO2),
                      .B_CLK(CPU_CLK), .B_NEXT(), .B_DATA(cpuo[2]));

cdc_mcp_fb #(8) a2c3 (.A_CLK(APU_CLK), .A_VALID(a2c_valid[3]),
                      .A_READY(a2c_ready[3]), .A_DATA(APU_CPUO3),
                      .B_CLK(CPU_CLK), .B_NEXT(), .B_DATA(cpuo[3]));

assign CPU_D_O = CPU_D_OE ? cpuo[CPU_PA] : 8'hxx;
assign CPU_D_OE = CPU_CS & ~CPU_nCS & ~CPU_nPARD;

endmodule
