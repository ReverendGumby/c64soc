`timescale 1us / 1ns

// CDC synchronizer between emubus AXI and APU RAM interfaces

module emubus_apu_ram
  (
   // emubus read/write interface
   input         ARESETn,
   input         EMUBUS_CLK,
   input [15:0]  EMUBUS_ADDR,
   input         EMUBUS_AVALID,
   output [7:0]  EMUBUS_RDATA,
   input         EMUBUS_RREQ,
   output reg    EMUBUS_RVALID,
   input         EMUBUS_RREADY,
   input [7:0]   EMUBUS_WDATA,
   input         EMUBUS_WREQ,
   output reg    EMUBUS_BVALID,
   input         EMUBUS_BREADY,

   // APU RAM bus interface
   input         APU_MCLK,
   output [15:0] APU_RAM_A_O,
   input [7:0]   APU_RAM_D_I,
   output [7:0]  APU_RAM_D_O,
   output        APU_RAM_RE,
   output        APU_RAM_WE
   );


//////////////////////////////////////////////////////////////////////
// Clock domain crossing

// ARESETn
wire apu_ram_aresetn;
cdc_1bit_2ff aresetn_sync
  (
   .A_DATA(ARESETn),
   .B_CLK(APU_MCLK),
   .B_DATA(apu_ram_aresetn)
   );

// EMUBUS_A*
reg emubus_avalid_d;
initial
  emubus_avalid_d = 1'b0;
always @(posedge EMUBUS_CLK)
  emubus_avalid_d <= EMUBUS_AVALID;
wire emubus_avalid_posedge = EMUBUS_AVALID & ~emubus_avalid_d;

wire [15:0] apu_ram_abuf;

cdc_mcp #(.WIDTH(16)) a_mcp
  (
   .A_CLK(EMUBUS_CLK),
   .A_NEXT(emubus_avalid_posedge),
   .A_DATA(EMUBUS_ADDR),

   .B_CLK(APU_MCLK),
   .B_NEXT(),
   .B_ACK(),
   .B_DATA(apu_ram_abuf)
   );

// EMUBUS_R*
reg emubus_rreq_tog, emubus_rreq_d;
initial begin
  emubus_rreq_tog = 1'b0;
  emubus_rreq_d = 1'b0;
end
always @(posedge EMUBUS_CLK) begin
  emubus_rreq_d <= EMUBUS_RREQ;
  emubus_rreq_tog <= emubus_rreq_tog ^ (EMUBUS_RREQ & ~emubus_rreq_d);
end

reg [7:0] apu_ram_rbuf;
reg       apu_ram_rnext;
wire      apu_ram_rreq_tog, emubus_rnext;

cdc_1bit_2ff rreq_sync
  (
   .A_DATA(emubus_rreq_tog),
   .B_CLK(APU_MCLK),
   .B_DATA(apu_ram_rreq_tog)
   );

cdc_mcp #(.WIDTH(8)) r_mcp
  (
   .A_CLK(APU_MCLK),
   .A_NEXT(apu_ram_rnext),
   .A_DATA(apu_ram_rbuf),

   .B_CLK(EMUBUS_CLK),
   .B_NEXT(emubus_rnext),
   .B_ACK(),
   .B_DATA(EMUBUS_RDATA)
   );

always @(posedge EMUBUS_CLK)
  EMUBUS_RVALID <= (EMUBUS_RVALID | emubus_rnext) & (ARESETn & ~(EMUBUS_RREADY & EMUBUS_RVALID));

// EMUBUS_W*
reg emubus_wreq_d;
always @(posedge EMUBUS_CLK)
  emubus_wreq_d <= EMUBUS_WREQ;
wire emubus_wreq_posedge = EMUBUS_WREQ & ~emubus_wreq_d;

wire [7:0] apu_ram_wbuf;
wire       apu_ram_wnext;
wire       emubus_bvalid_tog;
reg        emubus_bvalid_tog_d;
reg        apu_ram_bvalid_tog;

cdc_mcp #(.WIDTH(8)) w_mcp
  (
   .A_CLK(EMUBUS_CLK),
   .A_NEXT(emubus_wreq_posedge),
   .A_DATA(EMUBUS_WDATA),

   .B_CLK(APU_MCLK),
   .B_NEXT(apu_ram_wnext),
   .B_ACK(),
   .B_DATA(apu_ram_wbuf)
   );

cdc_1bit_2ff bvalid_sync
  (
   .A_DATA(apu_ram_bvalid_tog),
   .B_CLK(EMUBUS_CLK),
   .B_DATA(emubus_bvalid_tog)
   );

wire emubus_bvalid_posedge = emubus_bvalid_tog ^ emubus_bvalid_tog_d;

always @(posedge EMUBUS_CLK) begin
  emubus_bvalid_tog_d <= emubus_bvalid_tog;
  EMUBUS_BVALID <= (EMUBUS_BVALID | emubus_bvalid_posedge) & (ARESETn & ~(EMUBUS_BREADY & EMUBUS_BVALID));
end


//////////////////////////////////////////////////////////////////////
// APU RAM leader

reg       apu_ram_write, apu_ram_read;
reg       apu_ram_rreq_tog_d;
reg       apu_ram_wreq;

initial begin
  apu_ram_write = 1'b0;
  apu_ram_read = 1'b0;
  apu_ram_bvalid_tog = 1'b0;
  apu_ram_rnext = 1'b0;
  apu_ram_wreq = 1'b0;
end

always @(posedge APU_MCLK) begin
  if (~apu_ram_aresetn) begin
    apu_ram_write <= 1'b0;
    apu_ram_read <= 1'b0;
    apu_ram_rreq_tog_d <= apu_ram_rreq_tog;
    apu_ram_rnext <= 1'b0;
    apu_ram_wreq <= 1'b0;
  end
  else begin
    apu_ram_rnext <= 1'b0;
    apu_ram_wreq <= apu_ram_wreq | apu_ram_wnext;

    if (1'b1) begin             // TODO: remove
      if (apu_ram_read) begin
        apu_ram_read <= 1'b0;
        apu_ram_rnext <= 1'b1;
      end
      else if (apu_ram_write) begin
        apu_ram_write <= 1'b0;
        apu_ram_bvalid_tog <= ~apu_ram_bvalid_tog;
      end
      else begin
        if (apu_ram_rreq_tog ^ apu_ram_rreq_tog_d) begin
          apu_ram_rreq_tog_d <= apu_ram_rreq_tog;
          apu_ram_read <= 1'b1;
        end
        else if (apu_ram_wreq) begin
          apu_ram_wreq <= 1'b0;
          apu_ram_write <= 1'b1;
        end
      end
    end
  end
end

always @*
  apu_ram_rbuf = APU_RAM_D_I;

assign APU_RAM_D_O = apu_ram_wbuf;
assign APU_RAM_A_O = apu_ram_abuf;
assign APU_RAM_RE = apu_ram_read;
assign APU_RAM_WE = apu_ram_write;

endmodule
