`timescale 1us / 1ns

// I/O bus leader for emubus

module emubus_io
  (
   input        CLK,
   input        CLKEN,

   // emubus read/write interface
   input        ARESETn,
   input [7:0]  EMUBUS_ADDR,
   input        EMUBUS_AVALID,
   output [7:0] EMUBUS_RDATA,
   input        EMUBUS_RREQ,
   output reg   EMUBUS_RVALID,
   input        EMUBUS_RREADY,
   input [7:0]  EMUBUS_WDATA,
   input        EMUBUS_WREQ,
   output reg   EMUBUS_BVALID,
   input        EMUBUS_BREADY,

   // I/O bus interface
   output [7:0] IO_PA,
   output       IO_PA_OE,
   input [7:0]  IO_D_I,
   output [7:0] IO_D_O,
   output       IO_D_OE,
   output       IO_nPARD,
   output       IO_nPAWR
   );

//////////////////////////////////////////////////////////////////////
// emubus read/write interface

// ARESETn
wire io_aresetn = ARESETn;

// EMUBUS_A*
reg emubus_avalid_d;
initial
  emubus_avalid_d = 1'b0;
always @(posedge CLK)
  emubus_avalid_d <= EMUBUS_AVALID;
wire emubus_avalid_posedge = EMUBUS_AVALID & ~emubus_avalid_d;

assign IO_PA = EMUBUS_ADDR;

// EMUBUS_R*
reg emubus_rreq_tog, emubus_rreq_d;
initial begin
  emubus_rreq_tog = 1'b0;
  emubus_rreq_d = 1'b0;
end
always @(posedge CLK) begin
  emubus_rreq_d <= EMUBUS_RREQ;
  emubus_rreq_tog <= emubus_rreq_tog ^ (EMUBUS_RREQ & ~emubus_rreq_d);
end

reg [7:0] io_rbuf, emubus_rdata;
reg       io_rnext;
wire      io_rreq_tog;
reg       emubus_rnext;

assign io_rreq_tog = emubus_rreq_tog;

always @(posedge CLK) begin
  emubus_rnext <= io_rnext;
  emubus_rdata <= io_rbuf;
end

assign EMUBUS_RDATA = emubus_rdata;

always @(posedge CLK)
  EMUBUS_RVALID <= (EMUBUS_RVALID | emubus_rnext) & (ARESETn & ~(EMUBUS_RREADY & EMUBUS_RVALID));

// EMUBUS_W*
reg emubus_wreq_d;
always @(posedge CLK)
  emubus_wreq_d <= EMUBUS_WREQ;
wire emubus_wreq_posedge = EMUBUS_WREQ & ~emubus_wreq_d;

reg [7:0] io_wbuf;
reg       io_wnext;
wire      emubus_bvalid_tog;
reg       emubus_bvalid_tog_d;
reg       io_bvalid_tog;

always @(posedge CLK) begin
  io_wnext <= emubus_wreq_posedge;
  io_wbuf <= EMUBUS_WDATA;
end

// EMUBUS_B*
assign emubus_bvalid_tog = io_bvalid_tog;

wire emubus_bvalid_posedge = emubus_bvalid_tog ^ emubus_bvalid_tog_d;

always @(posedge CLK) begin
  emubus_bvalid_tog_d <= emubus_bvalid_tog;
  EMUBUS_BVALID <= (EMUBUS_BVALID | emubus_bvalid_posedge) & (ARESETn & ~(EMUBUS_BREADY & EMUBUS_BVALID));
end


//////////////////////////////////////////////////////////////////////
// I/O bus leader

reg [2:0] io_ccnt;
wire      io_clken, io_clkp2;
reg       io_write, io_read;
reg       io_rreq_tog_d;
reg       io_wreq;

// Approximate bus clock = CLK/6
// TODO: Replace with real cp2 (but copy out to shvc_test)
initial
  io_ccnt = 3'd0;

always @(posedge CLK) if (CLKEN) begin
  if (io_clken)
    io_ccnt <= 3'd0;
  else
    io_ccnt <= io_ccnt + 1'd1;
end

assign io_clken = CLKEN & (io_ccnt == 3'd5);
assign io_clkp2 = (io_ccnt >= 3'd3); // Clock phase 2

initial begin
  io_write = 1'b0;
  io_read = 1'b0;
  io_bvalid_tog = 1'b0;
  io_rnext = 1'b0;
  io_wreq = 1'b0;
end

always @(posedge CLK) begin
  if (~io_aresetn) begin
    io_write <= 1'b0;
    io_read <= 1'b0;
    io_rreq_tog_d <= io_rreq_tog;
    io_rnext <= 1'b0;
    io_wreq <= 1'b0;
  end
  else begin
    io_rnext <= 1'b0;
    io_wreq <= io_wreq | io_wnext;

    if (io_clken) begin
      if (io_read) begin
        io_read <= 1'b0;
        io_rbuf <= IO_D_I;
        io_rnext <= 1'b1;
      end
      else if (io_write) begin
        io_write <= 1'b0;
        io_bvalid_tog <= ~io_bvalid_tog;
      end
      else begin
        if (io_rreq_tog ^ io_rreq_tog_d) begin
          io_rreq_tog_d <= io_rreq_tog;
          io_read <= 1'b1;
        end
        else if (io_wreq) begin
          io_wreq <= 1'b0;
          io_write <= 1'b1;
        end
      end
    end
  end
end

assign IO_nPARD = ~(io_clkp2 & io_read);
assign IO_nPAWR = ~(io_clkp2 & io_write);
assign IO_D_O = io_wbuf;
assign IO_PA_OE = io_read | io_write;
assign IO_D_OE = io_write;

endmodule
