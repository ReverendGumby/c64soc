`timescale 1us / 1ns

module cpumem_tb();

reg        nRES;
reg        clk, mclk, cpumem_clk;
wire       cp1_posedge, cp2_negedge, cp2_negedge_pre;

initial begin
  $dumpfile("cpumem_tb.vcd");
  $dumpvars();
end

wire [15:0] a;

reg [23:0]  cpu_a, cpu_a_idle;
reg [7:0]   cpu_d_i;
wire [7:0]  cpu_d_o;
reg         cpu_nwr, cpu_nrd;
reg         cpu_nromsel, cpu_nwramsel;

reg [31:0]  cat_addr;
reg         cat_dreq;

reg         cpumem_axi_en;
wire        cpumem_cpu_ready;
wire        cpumem_idle;

reg [7:0]   cpumem_mon_sel;
reg         cpumem_mon_ws;

wire [31:0] cpumem_araddr;
wire [1:0]  cpumem_arburst;
wire [3:0]  cpumem_arcache;
wire [3:0]  cpumem_arid;
wire [3:0]  cpumem_arlen;
wire [1:0]  cpumem_arlock;
wire [2:0]  cpumem_arprot;
wire [3:0]  cpumem_arqos;
wire        cpumem_arready;
wire [2:0]  cpumem_arsize;
wire [4:0]  cpumem_aruser;
wire        cpumem_arvalid;
wire [31:0] cpumem_awaddr;
wire [1:0]  cpumem_awburst;
wire [3:0]  cpumem_awcache;
wire [3:0]  cpumem_awid;
wire [3:0]  cpumem_awlen;
wire [1:0]  cpumem_awlock;
wire [2:0]  cpumem_awprot;
wire [3:0]  cpumem_awqos;
wire        cpumem_awready;
wire [2:0]  cpumem_awsize;
wire [4:0]  cpumem_awuser;
wire        cpumem_awvalid;
wire [3:0]  cpumem_bid;
wire        cpumem_bready;
wire [1:0]  cpumem_bresp;
wire        cpumem_bvalid;
wire [63:0] cpumem_rdata;
wire [3:0]  cpumem_rid;
wire        cpumem_rlast;
wire        cpumem_rready;
wire [1:0]  cpumem_rresp;
wire        cpumem_rvalid;
wire [63:0] cpumem_wdata;
wire [2:0]  cpumem_wid;
wire        cpumem_wlast;
wire        cpumem_wready;
wire [7:0]  cpumem_wstrb;
wire        cpumem_wvalid;

integer     bfm_latency;

event       ev_verify_write;

localparam [31:0] BASE_WRAM = 32'h0000_0000;
localparam [31:0] BASE_ROM = 32'h0002_0000;

cpumem cpumem
  (
   .MCLK(mclk),
   .CP1_POSEDGE(cp1_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .CP2_NEGEDGE_PRE(cp2_negedge_pre),

   // Module status/control
   .BASE_WRAM(BASE_WRAM),
   .AXI_EN(cpumem_axi_en),
   .CPU_READY(cpumem_cpu_ready),
   .IDLE(cpumem_idle),

   // Monitor interface
   .MON_SEL(cpumem_mon_sel),
   .MON_WS(cpumem_mon_ws),
   .MON_DOUT(),
   .MON_DIN(32'bz),

   // CPU memory bus interface
   .A(cpu_a),
   .D_I(cpu_d_i),
   .D_O(cpu_d_o),
   .D_OE(),
   .nROMSEL(cpu_nromsel),
   .nWRAMSEL(cpu_nwramsel),
   .nWR(cpu_nwr),
   .nRD(cpu_nrd),
   .PA(),
   .nPARD(1'b1),
   .nPAWR(1'b1),

   // Cartridge address translation interface
   .CAT_ADDR(cat_addr),
   .CAT_DREQ(cat_dreq),

   // AXI bus leader for ACP
   .ACLK(cpumem_clk),
   .ARADDR(cpumem_araddr),
   .ARBURST(cpumem_arburst),
   .ARCACHE(cpumem_arcache),
   .ARID(cpumem_arid[2:0]),
   .ARLEN(cpumem_arlen),
   .ARLOCK(cpumem_arlock),
   .ARPROT(cpumem_arprot),
   .ARQOS(cpumem_arqos),
   .ARREADY(cpumem_arready),
   .ARSIZE(cpumem_arsize),
   .ARUSER(cpumem_aruser),
   .ARVALID(cpumem_arvalid),
   .AWADDR(cpumem_awaddr),
   .AWBURST(cpumem_awburst),
   .AWCACHE(cpumem_awcache),
   .AWID(cpumem_awid[2:0]),
   .AWLEN(cpumem_awlen),
   .AWLOCK(cpumem_awlock),
   .AWPROT(cpumem_awprot),
   .AWQOS(cpumem_awqos),
   .AWREADY(cpumem_awready),
   .AWSIZE(cpumem_awsize),
   .AWUSER(cpumem_awuser),
   .AWVALID(cpumem_awvalid),
   .BID(cpumem_bid[2:0]),
   .BREADY(cpumem_bready),
   .BRESP(2'b00),
   .BVALID(cpumem_bvalid),
   .RDATA(cpumem_rdata),
   .RID(cpumem_rid[2:0]),
   .RLAST(cpumem_rlast),
   .RREADY(cpumem_rready),
   .RRESP(2'b00),
   .RVALID(cpumem_rvalid),
   .WDATA(cpumem_wdata),
   .WID(cpumem_wid),
   .WLAST(cpumem_wlast),
   .WREADY(cpumem_wready),
   .WSTRB(cpumem_wstrb),
   .WVALID(cpumem_wvalid)
   );

axi_slave_bfm #(32, 64) axi_slave_bfm
  (
   .ARESETn(nRES),
   .ACLK(cpumem_clk),

   .AWID(cpumem_awid),
   .AWADDR(cpumem_awaddr),
   .AWLEN({4'b0, cpumem_awlen}),
   .AWSIZE(cpumem_awsize),
   .AWBURST(cpumem_awburst),
   .AWVALID(cpumem_awvalid),
   .AWREADY(cpumem_awready),
   .WDATA(cpumem_wdata),
   .WSTRB(cpumem_wstrb),
   .WLAST(cpumem_wlast),
   .WVALID(cpumem_wvalid),
   .WREADY(cpumem_wready),
   .BID(cpumem_bid),
   .BVALID(cpumem_bvalid),
   .BREADY(cpumem_bready),

   .ARID(cpumem_arid),
   .ARADDR(cpumem_araddr),
   .ARLEN({4'b0, cpumem_arlen}),
   .ARSIZE(cpumem_arsize),
   .ARBURST(cpumem_arburst),
   .ARVALID(cpumem_arvalid),
   .ARREADY(cpumem_arready),
   .RID(cpumem_rid),
   .RDATA(cpumem_rdata),
   .RLAST(cpumem_rlast),
   .RVALID(cpumem_rvalid),
   .RREADY(cpumem_rready)
   );

initial begin
  clk = 1;
  nRES = 0;
end

initial begin
  cpu_a_idle = 24'hx;
  cpu_a = cpu_a_idle;
  cpu_d_i = 8'bx;
  cpu_nrd = 1'b1;
  cpu_nwr = 1'b1;
  cpu_nromsel = 1'b1;
  cpu_nwramsel = 1'b1;

  cpumem_axi_en = 1'b0;

  bfm_latency = 0;
end

always @bfm_latency
  axi_slave_bfm.latency = bfm_latency;

initial forever begin :clkgen
  #(250.0/24576.0) clk = ~clk;
end

localparam [1:0] mclk_ccnt_max = 2'd2;
reg [1:0] mclk_ccnt;
initial begin
  cpumem_clk = 'b0;
  mclk_ccnt = 2'd0;
  mclk = 'b0;
end
always @(posedge clk) begin
  cpumem_clk <= ~cpumem_clk;
  if (mclk_ccnt == mclk_ccnt_max) begin
    mclk_ccnt <= 2'd0;
    mclk <= ~mclk;
  end
  else
    mclk_ccnt <= mclk_ccnt + 1'd1;
end

localparam [7:0] cycle_ccnt = 8'd8;
reg [7:0] cp2_clkcnt;
initial
  cp2_clkcnt = 8'd0;
always @(posedge mclk) begin
  if (cpumem_cpu_ready) begin
    if (cp2_negedge_pre)
      cp2_clkcnt <= 8'd0;
    else
      cp2_clkcnt <= cp2_clkcnt + 1'd1;
  end
end
assign cp2_negedge_pre = cp2_clkcnt == (cycle_ccnt - 1'd1);
assign cp2_negedge = cp2_clkcnt == 8'd0;
assign cp1_posedge = cp2_clkcnt == 8'd1;

reg nromsel, nwramsel;

task decode_addr(input [23:0] a);
  begin
    nromsel = 'b1;
    nwramsel = 'b1;
    if ({a[22], a[15]} == 'b0) begin
      if (a[15:13] == 'b0)
        nwramsel = 'b0;
    end
    casez (a)
      24'bz0zzzzzz_0000000z_zzzzzzzz: nwramsel = 'b0;
      24'bz0zzzzzz_1zzzzzzz_zzzzzzzz: nromsel = 'b0;
      24'b010zzzzz_1zzzzzzz_zzzzzzzz: nromsel = 'b0;
      24'b0110zzzz_1zzzzzzz_zzzzzzzz: nromsel = 'b0;
      24'b01110zzz_1zzzzzzz_zzzzzzzz: nromsel = 'b0;
      24'b011110zz_1zzzzzzz_zzzzzzzz: nromsel = 'b0;
      24'b0111110z_1zzzzzzz_zzzzzzzz: nromsel = 'b0;
      24'b0111111z_zzzzzzzz_zzzzzzzz: nwramsel = 'b0;
      24'b11zzzzzz_zzzzzzzz_zzzzzzzz: nromsel = 'b0;
    endcase
  end
endtask

reg [22:0] rom_a;
reg        rom_ce;

always @* begin
  rom_a[14:0] = cpu_a[14:0];
  rom_a[21:15] = cpu_a[22:16];
  rom_a[22] = 1'b0;
  rom_ce = cpu_nromsel & cpu_a[15];

  cat_addr = BASE_ROM + {9'b0, rom_a};
  cat_dreq = ~cpu_nrd;
end

task cpu_write(input [23:0] a, input [7:0] v);
  begin
    decode_addr(a);

    while (~cp1_posedge)
      @(posedge mclk) ;

    cpu_a <= a;
    cpu_d_i <= 8'hxx;
    cpu_nwr <= 1'b0;
    cpu_nromsel <= nromsel;
    cpu_nwramsel <= nwramsel;

    while (~cp2_negedge_pre)
      @(posedge mclk) ;

    cpu_d_i <= v;
    while (~cp2_negedge)
      @(posedge mclk) ;

    cpu_a <= cpu_a_idle;
    cpu_d_i <= 8'hxx;
    cpu_nwr <= 1'b1;
    cpu_nromsel <= 1'b1;
    cpu_nwramsel <= 1'b1;
  end
endtask

task cpu_read(input [23:0] a, output [7:0] v);
  begin
    decode_addr(a);

    while (~cp1_posedge)
      @(posedge mclk) ;

    cpu_a <= a;
    cpu_nrd <= 1'b0;
    cpu_nromsel <= nromsel;
    cpu_nwramsel <= nwramsel;
    @(posedge mclk) ;

    while (~cp2_negedge)
      @(posedge mclk) ;

    v = cpu_d_o;

    while (~cp1_posedge)
      @(posedge mclk) ;

    cpu_a <= cpu_a_idle;
    cpu_nrd <= 1'b1;
    cpu_nromsel <= 1'b1;
    cpu_nwramsel <= 1'b1;
  end
endtask

task cpu_addr_to_axi_addr(input [23:0] cpu_addr, output [31:0] axi_addr);
  if (~cpu_addr[22] & ~|cpu_addr[15:13])
    axi_addr = BASE_WRAM + cpu_addr[12:0];
  else if (cpu_addr[23:17] == 7'h3F)
    axi_addr = BASE_WRAM + cpu_addr[16:0];
  else if (cpu_addr[15])
    axi_addr = BASE_ROM + { cpu_addr[23:16], cpu_addr[14:0] };
  else
    assert(0);
endtask

task read_verify(input [23:0] a, input [7:0] v);
reg [7:0] tr;
  cpu_read(a, tr);
  assert(v == tr);
endtask

task write_verify(input [23:0] a, input [7:0] v);
  write_verify_ex(a, v, v);
endtask

reg [31:0] write_axi_addr;
reg [7:0] write_v;
task write_verify_ex(input [23:0] a, input [7:0] wv, input [7:0] vv);
reg [31:0] axi_addr;
  cpu_addr_to_axi_addr(a, axi_addr);
  cpu_write(a, wv);
  write_axi_addr = axi_addr;
  write_v = vv;
  -> ev_verify_write;
endtask

always @ev_verify_write begin :verify_write_blk
reg [7:0] tr;
  // Write should complete by the next cycle.
  @(posedge mclk) ;
  while (~cp2_negedge_pre)
    @(posedge mclk) ;
  axi_slave_bfm.store_get_byte(write_axi_addr, tr);
  assert(write_v == tr);
end

task wait_for_cpumem_idle();
  while (~cpumem_idle)
    @(posedge mclk) ;
endtask

initial begin
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h00765, 8'h11);
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h00766, 8'h22);
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h00767, 8'h33);
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h08768, 8'h44);
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h10768, 8'h55);
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h20768, 8'h66);
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h40768, 8'h77);
end

initial begin
  #0.5 @(posedge cpumem_clk) nRES = 1;

  cpumem_axi_en <= 1'b1;

  repeat (4) @(posedge cpumem_clk) ;

  bfm_latency = 19;
  read_verify(24'h008765, 8'h11);
  read_verify(24'h008766, 8'h22);
  write_verify(24'h001234, 8'h34);
  write_verify(24'h7e1234, 8'h56);
  wait_for_cpumem_idle();

  bfm_latency = 5;
  write_verify(24'h7e1233, 8'h78);
  write_verify(24'h7f5678, 8'h9a);
  read_verify(24'h008765, 8'h11);
  write_verify_ex(24'h008767, 8'h80, 8'h33); // ROM write
  wait_for_cpumem_idle();

  bfm_latency = 15;
  read_verify(24'h018768, 8'h44);
  read_verify(24'h028768, 8'h55);
  read_verify(24'h048768, 8'h66);
  read_verify(24'h088768, 8'h77);
  repeat (4) @(posedge mclk) ;

  cpumem_mon_sel <= 8'h40;
  cpumem_mon_ws <= 1'b1;
  @(posedge mclk) ;
  cpumem_mon_sel <= 8'bx;
  cpumem_mon_ws <= 1'b0;
  repeat (4) @(posedge mclk) ;

  cpumem_mon_sel <= 8'h50;
  cpumem_mon_ws <= 1'b1;
  @(posedge mclk) ;
  cpumem_mon_sel <= 8'bx;
  cpumem_mon_ws <= 1'b0;
  repeat (4) @(posedge mclk) ;

  $finish;
end

always @(posedge cpumem.to_error or posedge cpumem.axi_error) begin
  $display("cpumem error");
  $finish(1);
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s cpumem_tb -o cpumem_tb.vvp -f snes.files ../../axi/axi_slave_bfm.v cpumem_tb.sv && ./cpumem_tb.vvp"
// End:
