`timescale 1us / 1ns

/* verilator lint_off INITIALDLY */

module emu_tb();

reg        nRES;
reg        clk, mclk, aud_mclk, cpumem_clk;
reg        ext_hold;

initial begin
`ifndef VERILATOR
  $dumpfile("emu_tb.vcd");
  $dumpvars();
`else
  $dumpfile("emu_tb.verilator.vcd");
  $dumpvars();
`endif
end

wire            vid_pce, vid_de, vid_hs, vid_vs;
wire [7:0]      vid_r;
wire [7:0]      vid_g;
wire [7:0]      vid_b;

wire            emu_nres;

wire        aud_sclk, aud_lrclk, aud_sdout;
wire [15:0] aud_pdoutl, aud_pdoutr;

wire [31:0] cpumem_araddr;
wire [1:0]  cpumem_arburst;
wire [3:0]  cpumem_arcache;
wire [3:0]  cpumem_arid;
wire [3:0]  cpumem_arlen;
wire [1:0]  cpumem_arlock;
wire [2:0]  cpumem_arprot;
wire [3:0]  cpumem_arqos;
wire        cpumem_arready;
wire [2:0]  cpumem_arsize;
wire [4:0]  cpumem_aruser;
wire        cpumem_arvalid;
wire [31:0] cpumem_awaddr;
wire [1:0]  cpumem_awburst;
wire [3:0]  cpumem_awcache;
wire [3:0]  cpumem_awid;
wire [3:0]  cpumem_awlen;
wire [1:0]  cpumem_awlock;
wire [2:0]  cpumem_awprot;
wire [3:0]  cpumem_awqos;
wire        cpumem_awready;
wire [2:0]  cpumem_awsize;
wire [4:0]  cpumem_awuser;
wire        cpumem_awvalid;
wire [3:0]  cpumem_bid;
wire        cpumem_bready;
wire [1:0]  cpumem_bresp;
wire        cpumem_bvalid;
wire [63:0] cpumem_rdata;
wire [3:0]  cpumem_rid;
wire        cpumem_rlast;
wire        cpumem_rready;
wire [1:0]  cpumem_rresp;
wire        cpumem_rvalid;
wire [63:0] cpumem_wdata;
wire [2:0]  cpumem_wid;
wire        cpumem_wlast;
wire        cpumem_wready;
wire [7:0]  cpumem_wstrb;
wire        cpumem_wvalid;

wire        emubus_clk = mclk;
`include "emubus.svh"

wire [31:0] debug;
wire        btn_rst = 'b0;
wire        btn_key = 'b0;
wire        sw_src = 'b0;

emu emu
  (
   .DEBUG(debug),
   .CPUMEM_CLK(cpumem_clk),
   .MCLK(mclk),
   .AUD_MCLK(aud_mclk),
   .EXT_HOLD(ext_hold),
   .EXT_RES(btn_rst),
   .EMU_nRES(emu_nres),
   .BTN_KEY(btn_key),
   .SW_SRC(sw_src),

   .VID_PCE(vid_pce),
   .VID_DE(vid_de),
   .VID_HS(vid_hs),
   .VID_VS(vid_vs),
   .VID_R(vid_r),
   .VID_G(vid_g),
   .VID_B(vid_b),

   .AUD_SCLK(aud_sclk),
   .AUD_LRCLK(aud_lrclk),
   .AUD_SDOUT(aud_sdout),
   .AUD_PDOUTL(aud_pdoutl),
   .AUD_PDOUTR(aud_pdoutr),

   .EMUBUS_ARESETn(nRES),
   .EMUBUS_AWADDR(emubus_awaddr),
   .EMUBUS_AWVALID(emubus_awvalid),
   .EMUBUS_AWREADY(emubus_awready),
   .EMUBUS_WDATA(emubus_wdata),
   .EMUBUS_WREADY(emubus_wready),
   .EMUBUS_WSTRB(emubus_wstrb),
   .EMUBUS_WVALID(emubus_wvalid),
   .EMUBUS_BRESP(emubus_bresp),
   .EMUBUS_BVALID(emubus_bvalid),
   .EMUBUS_BREADY(emubus_bready),
   .EMUBUS_ARADDR(emubus_araddr),
   .EMUBUS_ARVALID(emubus_arvalid),
   .EMUBUS_ARREADY(emubus_arready),
   .EMUBUS_RDATA(emubus_rdata),
   .EMUBUS_RRESP(emubus_rresp),
   .EMUBUS_RVALID(emubus_rvalid),
   .EMUBUS_RREADY(emubus_rready),

   .CPUMEM_ARADDR(cpumem_araddr),
   .CPUMEM_ARBURST(cpumem_arburst),
   .CPUMEM_ARCACHE(cpumem_arcache),
   .CPUMEM_ARID(cpumem_arid[2:0]),
   .CPUMEM_ARLEN(cpumem_arlen),
   .CPUMEM_ARLOCK(cpumem_arlock),
   .CPUMEM_ARPROT(cpumem_arprot),
   .CPUMEM_ARQOS(cpumem_arqos),
   .CPUMEM_ARREADY(cpumem_arready),
   .CPUMEM_ARSIZE(cpumem_arsize),
   .CPUMEM_ARUSER(cpumem_aruser),
   .CPUMEM_ARVALID(cpumem_arvalid),
   .CPUMEM_AWADDR(cpumem_awaddr),
   .CPUMEM_AWBURST(cpumem_awburst),
   .CPUMEM_AWCACHE(cpumem_awcache),
   .CPUMEM_AWID(cpumem_awid[2:0]),
   .CPUMEM_AWLEN(cpumem_awlen),
   .CPUMEM_AWLOCK(cpumem_awlock),
   .CPUMEM_AWPROT(cpumem_awprot),
   .CPUMEM_AWQOS(cpumem_awqos),
   .CPUMEM_AWREADY(cpumem_awready),
   .CPUMEM_AWSIZE(cpumem_awsize),
   .CPUMEM_AWUSER(cpumem_awuser),
   .CPUMEM_AWVALID(cpumem_awvalid),
   .CPUMEM_BID(cpumem_bid[2:0]),
   .CPUMEM_BREADY(cpumem_bready),
   .CPUMEM_BRESP(cpumem_bresp),
   .CPUMEM_BVALID(cpumem_bvalid),
   .CPUMEM_RDATA(cpumem_rdata),
   .CPUMEM_RID(cpumem_rid[2:0]),
   .CPUMEM_RLAST(cpumem_rlast),
   .CPUMEM_RREADY(cpumem_rready),
   .CPUMEM_RRESP(cpumem_rresp),
   .CPUMEM_RVALID(cpumem_rvalid),
   .CPUMEM_WDATA(cpumem_wdata),
   .CPUMEM_WID(cpumem_wid),
   .CPUMEM_WLAST(cpumem_wlast),
   .CPUMEM_WREADY(cpumem_wready),
   .CPUMEM_WSTRB(cpumem_wstrb),
   .CPUMEM_WVALID(cpumem_wvalid)
   );

axi_slave_bfm #(32, 64, 22) axi_slave_bfm
  (
   .ARESETn(nRES),
   .ACLK(cpumem_clk),

   .AWID(cpumem_awid),
   .AWADDR(cpumem_awaddr),
   .AWLEN({4'b0, cpumem_awlen}),
   .AWSIZE(cpumem_awsize),
   .AWBURST(cpumem_awburst),
   .AWVALID(cpumem_awvalid),
   .AWREADY(cpumem_awready),
   .WDATA(cpumem_wdata),
   .WSTRB(cpumem_wstrb),
   .WLAST(cpumem_wlast),
   .WVALID(cpumem_wvalid),
   .WREADY(cpumem_wready),
   .BID(cpumem_bid),
   .BVALID(cpumem_bvalid),
   .BREADY(cpumem_bready),

   .ARID(cpumem_arid),
   .ARADDR(cpumem_araddr),
   .ARLEN({4'b0, cpumem_arlen}),
   .ARSIZE(cpumem_arsize),
   .ARBURST(cpumem_arburst),
   .ARVALID(cpumem_arvalid),
   .ARREADY(cpumem_arready),
   .RID(cpumem_rid),
   .RDATA(cpumem_rdata),
   .RLAST(cpumem_rlast),
   .RVALID(cpumem_rvalid),
   .RREADY(cpumem_rready)
   );

initial begin
  clk = 1;
  aud_mclk = 1;
  nRES = 0;
  ext_hold = 0;
end

initial forever begin :clkgen
  #(250.0/(21477.272*3)) clk = ~clk;
end

initial forever begin :aud_mclkgen
  #(500.0/24576.0) aud_mclk = ~aud_mclk;
end

localparam [1:0] mclk_ccnt_max = 2'd2;
reg [1:0] mclk_ccnt;
initial begin
  cpumem_clk = 'b0;
  mclk_ccnt = 2'd0;
  mclk = 'b0;
end
always @(posedge clk) begin
  cpumem_clk <= ~cpumem_clk;
  if (mclk_ccnt == mclk_ccnt_max) begin
    mclk_ccnt <= 2'd0;
    mclk <= ~mclk;
  end
  else
    mclk_ccnt <= mclk_ccnt + 1'd1;
end

localparam [31:0] BASE_WRAM = 32'h0000_0000;
localparam [31:0] BASE_RAM = 32'h0002_0000;
localparam [31:0] BASE_ROM = 32'h0004_0000;
localparam [31:0] RAM_MASK = 32'h0001_ffff;
localparam [31:0] ROM_MASK = 32'h0007_ffff;

/* -----\/----- EXCLUDED -----\/-----
initial begin :wram_loader
reg [31:0] off;
  off = 0;
  while (off < 32'h20000) begin
    axi_slave_bfm.store_set_byte(BASE_WRAM + off, 8'b0);
    off = off + 1;
  end  
  $display("WRAM cleared");
end
 -----/\----- EXCLUDED -----/\----- */

initial begin :rom_loader
integer fin;
reg [31:0] off;
string     fn;
  //fn = "snes_tests.sfc";
  fn = "testprg.sfc";
  fin = $fopen(fn, "r");
  assert(fin != 0) else $finish;
  off = 0;
  while (!$feof(fin)) begin
    axi_slave_bfm.store_set_byte(BASE_ROM + off, $fgetc(fin));
    off = off + 1;
  end  
  $display("ROM loaded");
  $fclose(fin);
end

initial begin
  #5 @(posedge clk) nRES <= 1;
  
  emubus_write32({30'h30, 2'b0}, BASE_WRAM);

  emubus_rmw32(32'h50, 32'h4, 32'h4); // CART_nRES
  emubus_write32({30'h400, 2'b0}, 32'd0); // LOROM
  emubus_write32({30'h401, 2'b0}, BASE_ROM);
  emubus_write32({30'h402, 2'b0}, BASE_RAM);
  emubus_write32({30'h410, 2'b0}, 32'h0); // disable RAM
  emubus_write32({30'h411, 2'b0}, ROM_MASK);
  emubus_write32({30'h412, 2'b0}, RAM_MASK);

  #1 emubus_rmw32(32'h50, 32'h1, 32'h1); // emu_usr_nres
  @(posedge emu_nres) ;

  //#26250 ; // first VBLANK assertion
  //#3000 ;
  repeat (1150) #1000 ;

  //$writememh("vram.hex", emu.vram.mem);
  //$finish;
end

always @(negedge emu.cpu.nvbl) begin
  #1000 $finish;
end

// Print frame
reg [31:0] frame;
always @(posedge mclk) begin
`define vc emu.ppu.video_counter
  if (emu.ppu.cc) begin
    if (frame != `vc.FRAME) begin
      frame = `vc.FRAME;
      $display("Frame: ", frame);
    end
  end
`undef vc
end

/* -----\/----- EXCLUDED -----\/-----
// Log APU comms
genvar i;
generate
  for (i = 0; i < 4; i++) begin :cpuio
    always @(emu.s_smp.cpui[i])
      $display("[apu] %t: CPUI%1d=%x", $time, i, emu.s_smp.cpui[i]);
    always @(emu.s_smp.cpuo[i])
      $display("[apu] %t: CPUO%1d=%x", $time, i, emu.s_smp.cpuo[i]);
  end
endgenerate
 -----/\----- EXCLUDED -----/\----- */

/* -----\/----- EXCLUDED -----\/-----
// cputest loops on $4210 until VBLANK starts. This speeds things up.
always @(posedge mclk) begin
`define vc emu.ppu.video_counter
  if (emu.cpu_a == 24'h004210 && emu.cp2_posedge) begin // RDNMI
    if ((`vc.ROW > 0 && `vc.ROW < (`vc.FIRST_ROW_POST_RENDER - 1)) && 
        `vc.COL < `vc.LAST_COL_HSYNC) begin
      `vc.ROW <= (`vc.FIRST_ROW_POST_RENDER - 1);
      `vc.COL <= `vc.LAST_COL_HSYNC;
    end
    if (`vc.ROW > (`vc.FIRST_ROW_POST_RENDER) &&
        `vc.ROW < (`vc.NUM_ROWS - 1)) begin
      `vc.ROW <= (`vc.NUM_ROWS - 1);
      `vc.COL <= `vc.LAST_COL_HSYNC;
    end
  end
`undef vc
end

// Track test number
always @(posedge mclk) begin :test_num
reg [7:0] tnl;
  if (emu.cpu_a == 24'h000010 && emu.cp2_posedge && ~emu.cpu_rw)
    tnl <= emu.cpu_d_o;
  if (emu.cpu_a == 24'h000011 && emu.cp2_posedge && ~emu.cpu_rw) begin
    $display("test_num: %04x", {emu.cpu_d_o, tnl});
  end
end
 -----/\----- EXCLUDED -----/\----- */

always @(posedge emu.cpumem.to_error or posedge emu.cpumem.axi_error) begin
  $display("cpumem error");
  $finish(1);
end

/* -----\/----- EXCLUDED -----\/-----
// Hold testing
integer hold_cnt;
always @(posedge mclk)
  hold_cnt <= (ext_hold) ? 0 : hold_cnt + 1;
always @*
  ext_hold = hold_cnt == 169;
 -----/\----- EXCLUDED -----/\----- */

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s emu_tb -o emu_tb.vvp -f snes.files ../../axi/axi_slave_bfm.v emu_tb.sv && ./emu_tb.vvp"
// End:
