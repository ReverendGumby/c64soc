`timescale 1us / 1ns

module emubus_ppu_io_tb();

`include "../../s-ppu/reg_idx.vh"

reg        nRES;
reg        clk;                 // APU clock
reg        emu_clk;

wire       cken, rw;
wire [15:0] ma;
wire [7:0]  mdo_dsp, do_ram;
wire signed [15:0] psoutl, psoutr;
wire        nCE, nWE, nOE;
wire        sclken;

initial begin
  $dumpfile("emubus_ppu_io_tb.vcd");
  $dumpvars();
end

wire [15:0] a;
wire [7:0] do_dsp, do_smp;

wire [7:0]      emubus_io_pa;
wire [7:0]      emubus_io_d_i, emubus_io_d_o;
wire            emubus_io_npard, emubus_io_npawr;

wire            emubus_clk = emu_clk;
`include "emubus.svh"

s_ppu ppu
  (
   .CLK_RESET(1'b0),
   .CLK(emu_clk),
   .CLKEN(1'b1),
   .RES_COLD(1'b0),
   .RESET(~nRES),
   .HOLD(1'b0),

   .PA(emubus_io_pa),
   .D_I(emubus_io_d_o),
   .D_O(emubus_io_d_i),
   .D_OE(),
   .nPARD(emubus_io_npard),
   .nPAWR(emubus_io_npawr)
   );

emubus emubus
  (
   .CLK(emu_clk),
   .CLKEN(1'b1),
   .APU_MCLK(clk),

   .ARESETn(nRES),
   .AWADDR(emubus_awaddr),
   .AWVALID(emubus_awvalid),
   .AWREADY(emubus_awready),
   .WDATA(emubus_wdata),
   .WREADY(emubus_wready),
   .WSTRB(emubus_wstrb),
   .WVALID(emubus_wvalid),
   .BRESP(emubus_bresp),
   .BVALID(emubus_bvalid),
   .BREADY(emubus_bready),
   .ARADDR(emubus_araddr),
   .ARVALID(emubus_arvalid),
   .ARREADY(emubus_arready),
   .RDATA(emubus_rdata),
   .RRESP(emubus_rresp),
   .RVALID(emubus_rvalid),
   .RREADY(emubus_rready),

   .EXT_RES(EXT_RES),
   .CLK_RESET(clk_reset),
   .EMU_nRES(emu_nres),
   .RES_COLD(res_cold),

   .IO_PA(emubus_io_pa),
   .IO_D_I(emubus_io_d_i),
   .IO_D_O(emubus_io_d_o),
   .IO_D_OE(),
   .IO_nPARD(emubus_io_npard),
   .IO_nPAWR(emubus_io_npawr)
   );

initial begin
  clk = 1;
  emu_clk = 1;
  nRES = 0;
end

initial forever begin :clkgen
  #(500.0/24576.0) clk = ~clk;
end

initial forever begin :emu_clkgen
  #(500.0/21477.0) emu_clk = ~emu_clk;
end

task ppu_io_write(input [5:0] a, input [7:0] v);
  begin
    emubus_write8({19'h26, 2'b0}, a);
    emubus_write8({19'h27, 2'b0}, v);
  end
endtask

task ppu_io_read(input [5:0] a, output [7:0] v);
  begin
    emubus_write8({19'h26, 2'b0}, a);
    emubus_read8({19'h27, 2'b0}, v);
  end
endtask

task ppu_io_read_until(input [5:0] a, input [7:0] v, input [7:0] m);
reg [7:0] tr;
  begin
    ppu_io_read(a, tr);
    while ((tr & m) != v) begin
      emubus_read8({19'h27, 2'b0}, tr);
    end 
  end
endtask

initial begin :load
reg [7:0] tr;
  #3 @(posedge emu_clk) nRES = 1;

  #1 emubus_rmw32(19'h50, 32'h1, 32'h1); // emu_usr_nres
  @(posedge emu_nres) ;

  #10 ppu_io_write(REG_INIDISP, 8'h80);
  ppu_io_write(REG_BG1SC, 8'h60);

  ppu.palette.cgram['h44] = 'h12;
  ppu_io_write(REG_CGADD, 8'h44);
  ppu_io_read(REG_RDCGRAM, tr);
  #1 ;
  $display("REG_RDCGRAM = %x", tr[6:0]);

  $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2005 -grelative-include -s emubus_ppu_io_tb -o emubus_ppu_io_tb.vvp -f snes.files emubus_ppu_io_tb.v && ./emubus_ppu_io_tb.vvp"
// End:
