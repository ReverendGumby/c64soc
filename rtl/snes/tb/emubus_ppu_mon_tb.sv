`timescale 1us / 1ns

module emubus_ppu_mon_tb();

reg        nRES;
reg        emu_clk;
reg        hold;

wire       cken, rw;
wire [15:0] ma;
wire [7:0]  mdo_dsp, do_ram;
wire signed [15:0] psoutl, psoutr;
wire        nCE, nWE, nOE;
wire        sclken;

initial begin
  $dumpfile("emubus_ppu_mon_tb.vcd");
  $dumpvars();
end

wire [15:0] a;
wire [7:0] do_dsp, do_smp;

wire [7:0]      emubus_io_pa;
wire [7:0]      emubus_io_d_i, emubus_io_d_o;
wire            emubus_io_npard, emubus_io_npawr;

wire            emubus_clk = emu_clk;
`include "emubus.svh"

wire [10:0]     ppu_mon_sel;
wire            ppu_mon_ready;
wire [31:0]     ppu_mon_dout;
wire            ppu_mon_valid;
reg [3:0]       ppu_mon_ws;
reg [31:0]      ppu_mon_din;

s_ppu ppu
  (
   .CLK_RESET(1'b0),
   .CLK(emu_clk),
   .CLKEN(1'b1),
   .RES_COLD(1'b0),
   .RESET(~nRES),
   .HOLD(hold),

   .MON_SEL(ppu_mon_sel),
   .MON_READY(ppu_mon_ready),
   .MON_DOUT(ppu_mon_dout),
   .MON_VALID(ppu_mon_valid),
   .MON_WS(ppu_mon_ws),
   .MON_DIN(ppu_mon_din)
   );

emubus emubus
  (
   .CLK(emu_clk),
   .CLKEN(1'b1),
   .APU_MCLK(1'b0),

   .ARESETn(nRES),
   .AWADDR(emubus_awaddr),
   .AWVALID(emubus_awvalid),
   .AWREADY(emubus_awready),
   .WDATA(emubus_wdata),
   .WREADY(emubus_wready),
   .WSTRB(emubus_wstrb),
   .WVALID(emubus_wvalid),
   .BRESP(emubus_bresp),
   .BVALID(emubus_bvalid),
   .BREADY(emubus_bready),
   .ARADDR(emubus_araddr),
   .ARVALID(emubus_arvalid),
   .ARREADY(emubus_arready),
   .RDATA(emubus_rdata),
   .RRESP(emubus_rresp),
   .RVALID(emubus_rvalid),
   .RREADY(emubus_rready),

   .EXT_RES(EXT_RES),
   .CLK_RESET(clk_reset),
   .EMU_nRES(emu_nres),
   .RES_COLD(res_cold),

   .PPU_MON_SEL(ppu_mon_sel),
   .PPU_MON_READY(ppu_mon_ready),
   .PPU_MON_DOUT(ppu_mon_dout),
   .PPU_MON_VALID(ppu_mon_valid),
   .PPU_MON_WS(ppu_mon_ws),
   .PPU_MON_DIN(ppu_mon_din)
   );

initial begin
  emu_clk = 1;
  nRES = 0;
  hold = 0;
end

initial forever begin :emu_clkgen
  #(500.0/21477.0) emu_clk = ~emu_clk;
end

task ppu_mon_write8(input [10:0] a, input [7:0] v);
  begin
    emubus_write32({19'h1a, 2'b0}, a);
    emubus_write8({19'h1b, 2'b0}, v);
  end
endtask

task ppu_mon_write16(input [10:0] a, input [15:0] v);
  begin
    emubus_write32({19'h1a, 2'b0}, a);
    emubus_write16({19'h1b, 2'b0}, v);
  end
endtask

task ppu_mon_write32(input [10:0] a, input [31:0] v);
  begin
    emubus_write32({19'h1a, 2'b0}, a);
    emubus_write32({19'h1b, 2'b0}, v);
  end
endtask

task ppu_mon_read8(input [10:0] a, output [7:0] v);
  begin
    emubus_write32({19'h1a, 2'b0}, a);
    emubus_read8({19'h1b, 2'b0}, v);
    $display("%x = %x", a, v);
  end
endtask

task ppu_mon_read16(input [10:0] a, output [15:0] v);
  begin
    emubus_write32({19'h1a, 2'b0}, a);
    emubus_read16({19'h1b, 2'b0}, v);
    $display("%x = %x", a, v);
  end
endtask

task ppu_mon_read32(input [10:0] a, output [31:0] v);
  begin
    emubus_write32({19'h1a, 2'b0}, a);
    emubus_read32({19'h1b, 2'b0}, v);
    $display("%x = %x", a, v);
  end
endtask

initial begin :test
reg [31:0] tr;
  #3 @(posedge emu_clk) nRES = 1;

  #1 emubus_rmw32(19'h50, 32'h1, 32'h1); // emu_usr_nres
  @(posedge emu_nres) ;

  #1;
  hold <= 1;

  for (int a = 11'h000; a < 11'h110; a += 4)
    ppu_mon_read32(a[10:0], tr);

  ppu_mon_read8(11'h110, tr);
  ppu_mon_read8(11'h12f, tr);
  ppu_mon_read16(11'h130, tr);
  ppu_mon_read16(11'h132, tr);
  ppu_mon_read16(11'h32c, tr);
  ppu_mon_read16(11'h32e, tr);
  ppu_mon_read8(11'h330, tr);
  ppu_mon_read8(11'h331, tr);
  ppu_mon_read8(11'h34e, tr);
  ppu_mon_read8(11'h34f, tr);
  ppu_mon_read16(11'h400, tr);
  ppu_mon_read16(11'h402, tr);
  ppu_mon_read16(11'h5fc, tr);
  ppu_mon_read16(11'h5fe, tr);
  ppu_mon_read32(11'h600, tr);
  ppu_mon_read32(11'h7fc, tr);

  #1;
  for (int a = 11'h000; a < 11'h110; a += 4) begin
    tr = {32{1'b1}}; //{8{a[5:2]}};
    ppu_mon_write32(a[10:0], tr);
  end

  ppu_mon_write8(11'h110, tr);
  ppu_mon_write8(11'h12f, tr);
  ppu_mon_write16(11'h130, tr);
  ppu_mon_write16(11'h132, tr);
  ppu_mon_write16(11'h32c, tr);
  ppu_mon_write16(11'h32e, tr);
  ppu_mon_write8(11'h330, tr);
  ppu_mon_write8(11'h331, tr);
  ppu_mon_write8(11'h34e, tr);
  ppu_mon_write8(11'h34f, tr);
  ppu_mon_write16(11'h400, tr);
  ppu_mon_write16(11'h402, tr);
  ppu_mon_write16(11'h5fc, tr);
  ppu_mon_write16(11'h5fe, tr);
  ppu_mon_write32(11'h600, tr);
  ppu_mon_write32(11'h7fc, tr);

  #1 $finish;
end

initial #100 begin
  $display("Emergency stop!");
  $stop;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s emubus_ppu_mon_tb -o emubus_ppu_mon_tb.vvp -f snes.files emubus_ppu_mon_tb.sv && ./emubus_ppu_mon_tb.vvp"
// End:
