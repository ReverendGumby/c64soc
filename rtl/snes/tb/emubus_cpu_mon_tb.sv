`timescale 1us / 1ns

module emubus_cpu_mon_tb();

reg        nRES;
reg        emu_clk;
reg        hold;

wire       cken, rw;
wire [15:0] ma;
wire [7:0]  mdo_dsp, do_ram;
wire signed [15:0] psoutl, psoutr;
wire        nCE, nWE, nOE;
wire        sclken;

initial begin
  $dumpfile("emubus_cpu_mon_tb.vcd");
  $dumpvars();
end

wire            emubus_clk = emu_clk;
`include "emubus.svh"

wire [7:0]      cpu_mon_sel;
wire            cpu_mon_ready;
wire [31:0]     cpu_mon_dout;
wire            cpu_mon_valid;
reg [3:0]       cpu_mon_ws;
reg [31:0]      cpu_mon_din;

s_cpu cpu
  (
   .nRES(nRES),
   .MCLK(emu_clk),
   .HOLD(hold),

   .MON_SEL(cpu_mon_sel),
   .MON_READY(cpu_mon_ready),
   .MON_DOUT(cpu_mon_dout),
   .MON_VALID(cpu_mon_valid),
   .MON_WS(cpu_mon_ws),
   .MON_DIN(cpu_mon_din)
   );

emubus emubus
  (
   .CLK(emu_clk),
   .CLKEN(1'b1),
   .APU_MCLK(1'b0),

   .ARESETn(nRES),
   .AWADDR(emubus_awaddr),
   .AWVALID(emubus_awvalid),
   .AWREADY(emubus_awready),
   .WDATA(emubus_wdata),
   .WREADY(emubus_wready),
   .WSTRB(emubus_wstrb),
   .WVALID(emubus_wvalid),
   .BRESP(emubus_bresp),
   .BVALID(emubus_bvalid),
   .BREADY(emubus_bready),
   .ARADDR(emubus_araddr),
   .ARVALID(emubus_arvalid),
   .ARREADY(emubus_arready),
   .RDATA(emubus_rdata),
   .RRESP(emubus_rresp),
   .RVALID(emubus_rvalid),
   .RREADY(emubus_rready),

   .EXT_RES(EXT_RES),
   .CLK_RESET(clk_reset),
   .EMU_nRES(emu_nres),
   .RES_COLD(res_cold),

   .CPU_MON_SEL(cpu_mon_sel),
   .CPU_MON_READY(cpu_mon_ready),
   .CPU_MON_DOUT(cpu_mon_dout),
   .CPU_MON_VALID(cpu_mon_valid),
   .CPU_MON_WS(cpu_mon_ws),
   .CPU_MON_DIN(cpu_mon_din)
   );

initial begin
  emu_clk = 1;
  nRES = 0;
  hold = 0;
end

initial forever begin :emu_clkgen
  #(500.0/21477.0) emu_clk = ~emu_clk;
end

task cpu_mon_write(input [7:0] a, input [31:0] v);
  begin
    emubus_write32({19'h16, 2'b0}, a);
    emubus_write32({19'h17, 2'b0}, v);
  end
endtask

task cpu_mon_read(input [7:0] a, output [31:0] v);
  begin
    emubus_write32({19'h16, 2'b0}, a);
    emubus_read32({19'h17, 2'b0}, v);
    $display("%x = %x", a, v);
  end
endtask

initial begin :test
reg [31:0] tr;
  #3 @(posedge emu_clk) nRES = 1;

  #1 emubus_rmw32(19'h50, 32'h1, 32'h1); // emu_usr_nres
  @(posedge emu_nres) ;

  #1;
  hold <= 1;

  for (int a = 0; a < 9'h100; a += 4) begin
    cpu_mon_read(a[7:0], tr);
  end

  #1;
  for (int a = 0; a < 9'h100; a += 4) begin
    tr = {32{1'b1}}; //{8{a[5:2]}};
    cpu_mon_write(a[7:0], tr);
  end

  #1 $finish;
end

initial #100 begin
  $display("Emergency stop!");
  $stop;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s emubus_cpu_mon_tb -o emubus_cpu_mon_tb.vvp -f snes.files emubus_cpu_mon_tb.sv && ./emubus_cpu_mon_tb.vvp"
// End:
