`timescale 1us / 1ns

module cpumem_io_tb();

reg        nRES;
reg        clk, mclk, cpumem_clk;
wire       cp1_posedge, cp2_posedge, cp2_negedge, cp2_negedge_pre;

initial begin
  $dumpfile("cpumem_io_tb.vcd");
  $dumpvars();
end

wire [15:0] a;

reg [7:0]   cpu_pa;
reg [7:0]   cpu_d_i;
wire [7:0]  cpu_d_o;
reg         cpu_npawr, cpu_npard;

reg         cpumem_axi_en;
wire        cpumem_cpu_ready;

reg [7:0]   cpumem_mon_sel;
reg         cpumem_mon_ws;

wire [31:0] cpumem_araddr;
wire [1:0]  cpumem_arburst;
wire [3:0]  cpumem_arcache;
wire [3:0]  cpumem_arid;
wire [3:0]  cpumem_arlen;
wire [1:0]  cpumem_arlock;
wire [2:0]  cpumem_arprot;
wire [3:0]  cpumem_arqos;
wire        cpumem_arready;
wire [2:0]  cpumem_arsize;
wire [4:0]  cpumem_aruser;
wire        cpumem_arvalid;
wire [31:0] cpumem_awaddr;
wire [1:0]  cpumem_awburst;
wire [3:0]  cpumem_awcache;
wire [3:0]  cpumem_awid;
wire [3:0]  cpumem_awlen;
wire [1:0]  cpumem_awlock;
wire [2:0]  cpumem_awprot;
wire [3:0]  cpumem_awqos;
wire        cpumem_awready;
wire [2:0]  cpumem_awsize;
wire [4:0]  cpumem_awuser;
wire        cpumem_awvalid;
wire [3:0]  cpumem_bid;
wire        cpumem_bready;
wire [1:0]  cpumem_bresp;
wire        cpumem_bvalid;
wire [63:0] cpumem_rdata;
wire [3:0]  cpumem_rid;
wire        cpumem_rlast;
wire        cpumem_rready;
wire [1:0]  cpumem_rresp;
wire        cpumem_rvalid;
wire [63:0] cpumem_wdata;
wire [2:0]  cpumem_wid;
wire        cpumem_wlast;
wire        cpumem_wready;
wire [7:0]  cpumem_wstrb;
wire        cpumem_wvalid;

integer     bfm_latency;
reg         bfm_hold_read, bfm_hold_write;
wire        bfm_rready, bfm_rvalid, bfm_bready, bfm_bvalid;

event       ev_verify_write;

localparam [31:0] BASE_WRAM = 32'h0000_0000;

cpumem cpumem
  (
   .MCLK(mclk),
   .CP1_POSEDGE(cp1_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .CP2_NEGEDGE_PRE(cp2_negedge_pre),

   // Module status/control
   .BASE_WRAM(BASE_WRAM),
   .AXI_EN(cpumem_axi_en),
   .CPU_READY(cpumem_cpu_ready),

   // Monitor interface
   .MON_SEL(cpumem_mon_sel),
   .MON_WS(cpumem_mon_ws),
   .MON_DOUT(),
   .MON_DIN(32'bz),

   // CPU memory bus interface
   .A(),
   .D_I(cpu_d_i),
   .D_O(cpu_d_o),
   .D_OE(),
   .nROMSEL(1'b1),
   .nWRAMSEL(1'b1),
   .nWR(1'b1),
   .nRD(1'b1),
   .PA(cpu_pa),
   .nPARD(cpu_npard),
   .nPAWR(cpu_npawr),

   // AXI bus leader for ACP
   .ACLK(cpumem_clk),
   .ARADDR(cpumem_araddr),
   .ARBURST(cpumem_arburst),
   .ARCACHE(cpumem_arcache),
   .ARID(cpumem_arid[2:0]),
   .ARLEN(cpumem_arlen),
   .ARLOCK(cpumem_arlock),
   .ARPROT(cpumem_arprot),
   .ARQOS(cpumem_arqos),
   .ARREADY(cpumem_arready),
   .ARSIZE(cpumem_arsize),
   .ARUSER(cpumem_aruser),
   .ARVALID(cpumem_arvalid),
   .AWADDR(cpumem_awaddr),
   .AWBURST(cpumem_awburst),
   .AWCACHE(cpumem_awcache),
   .AWID(cpumem_awid[2:0]),
   .AWLEN(cpumem_awlen),
   .AWLOCK(cpumem_awlock),
   .AWPROT(cpumem_awprot),
   .AWQOS(cpumem_awqos),
   .AWREADY(cpumem_awready),
   .AWSIZE(cpumem_awsize),
   .AWUSER(cpumem_awuser),
   .AWVALID(cpumem_awvalid),
   .BID(cpumem_bid[2:0]),
   .BREADY(cpumem_bready),
   .BRESP(2'b00),
   .BVALID(cpumem_bvalid),
   .RDATA(cpumem_rdata),
   .RID(cpumem_rid[2:0]),
   .RLAST(cpumem_rlast),
   .RREADY(cpumem_rready),
   .RRESP(2'b00),
   .RVALID(cpumem_rvalid),
   .WDATA(cpumem_wdata),
   .WID(cpumem_wid),
   .WLAST(cpumem_wlast),
   .WREADY(cpumem_wready),
   .WSTRB(cpumem_wstrb),
   .WVALID(cpumem_wvalid)
   );

axi_slave_bfm #(32, 64) axi_slave_bfm
  (
   .ARESETn(nRES),
   .ACLK(cpumem_clk),

   .AWID(cpumem_awid),
   .AWADDR(cpumem_awaddr),
   .AWLEN({4'b0, cpumem_awlen}),
   .AWSIZE(cpumem_awsize),
   .AWBURST(cpumem_awburst),
   .AWVALID(cpumem_awvalid),
   .AWREADY(cpumem_awready),
   .WDATA(cpumem_wdata),
   .WSTRB(cpumem_wstrb),
   .WLAST(cpumem_wlast),
   .WVALID(cpumem_wvalid),
   .WREADY(cpumem_wready),
   .BID(cpumem_bid),
   .BVALID(bfm_bvalid),
   .BREADY(bfm_bready),

   .ARID(cpumem_arid),
   .ARADDR(cpumem_araddr),
   .ARLEN({4'b0, cpumem_arlen}),
   .ARSIZE(cpumem_arsize),
   .ARBURST(cpumem_arburst),
   .ARVALID(cpumem_arvalid),
   .ARREADY(cpumem_arready),
   .RID(cpumem_rid),
   .RDATA(cpumem_rdata),
   .RLAST(cpumem_rlast),
   .RVALID(bfm_rvalid),
   .RREADY(bfm_rready)
   );

initial begin
  clk = 1;
  nRES = 0;
end

initial begin
  cpu_pa = 8'bx;
  cpu_d_i = 8'bx;
  cpu_npard = 1'b1;
  cpu_npawr = 1'b1;

  cpumem_axi_en = 1'b0;

  bfm_latency = 0;
  bfm_hold_read = 1'b0;
  bfm_hold_write = 1'b0;
end

initial forever begin :clkgen
  #(250.0/24576.0) clk = ~clk;
end

localparam [1:0] mclk_ccnt_max = 2'd2;
reg [1:0] mclk_ccnt;
initial begin
  cpumem_clk = 'b0;
  mclk_ccnt = 2'd0;
  mclk = 'b0;
end
always @(posedge clk) begin
  cpumem_clk <= ~cpumem_clk;
  if (mclk_ccnt == mclk_ccnt_max) begin
    mclk_ccnt <= 2'd0;
    mclk <= ~mclk;
  end
  else
    mclk_ccnt <= mclk_ccnt + 1'd1;
end

localparam [7:0] cycle_ccnt = 8'd8;
reg [7:0] cp2_clkcnt;
initial
  cp2_clkcnt = 8'd0;
always @(posedge mclk) begin
  if (cpumem_cpu_ready) begin
    if (cp2_negedge_pre)
      cp2_clkcnt <= 8'd0;
    else
      cp2_clkcnt <= cp2_clkcnt + 1'd1;
  end
end
assign cp2_negedge_pre = cp2_clkcnt == (cycle_ccnt - 1'd1);
assign cp2_negedge = cp2_clkcnt == 8'd0;
assign cp1_posedge = cp2_clkcnt == 8'd1;
assign cp2_posedge = cp2_clkcnt == 8'd3;

reg nromsel, nwramsel;

task io_write(input [7:0] a, input [7:0] v);
  begin
    while (~cp1_posedge)
      @(posedge mclk) ;

    cpu_pa <= a;
    cpu_d_i <= 8'hxx;

    while (~cp2_posedge)
      @(posedge mclk) ;

    cpu_npawr <= 1'b0;
    while (~cp2_negedge_pre)
      @(posedge mclk) ;

    cpu_d_i <= v;
    while (~cp2_negedge)
      @(posedge mclk) ;

    cpu_pa <= 8'hxx;
    cpu_d_i <= 8'hxx;
    cpu_npawr <= 1'b1;
  end
endtask

task io_read(input [7:0] a, output [7:0] v);
  begin
    while (~cp1_posedge)
      @(posedge mclk) ;

    cpu_pa <= a;
    @(posedge mclk) ;

    while (~cp2_posedge)
      @(posedge mclk) ;

    cpu_npard <= 1'b0;
    while (~cp2_negedge)
      @(posedge mclk) ;

    v = cpu_d_o;

    while (~cp1_posedge)
      @(posedge mclk) ;

    cpu_pa <= 8'hxx;
    cpu_npard <= 1'b1;
  end
endtask

task wram_addr_to_axi_addr(input [16:0] io_addr, output [31:0] axi_addr);
  axi_addr = BASE_WRAM + io_addr;
endtask

reg [16:0] io_addr;
task set_addr(input [16:0] a);
  io_addr = a;
  io_write(8'h81, a[7:0]);
  io_write(8'h82, a[15:8]);
  io_write(8'h83, {7'b0, a[16]});
endtask

task read_verify();
reg [7:0] v, tr;
reg [31:0] axi_addr;
  wram_addr_to_axi_addr(io_addr, axi_addr);
  axi_slave_bfm.store_get_byte(axi_addr, v);

  io_read(8'h80, tr);
  assert(v == tr);
  io_addr += 1;
endtask

reg [31:0] write_axi_addr;
reg [7:0] write_v;
task write_verify(input [7:0] v);
reg [31:0] axi_addr;
  wram_addr_to_axi_addr(io_addr, axi_addr);
  write_axi_addr = axi_addr;
  write_v = v;

  io_write(8'h80, v);
  io_addr += 1;

  -> ev_verify_write;
endtask

always @ev_verify_write begin :verify_write_blk
reg [7:0] tr;
  // Write should complete by the next cycle.
  @(posedge mclk) ;
  while (~cp2_negedge_pre)
    @(posedge mclk) ;
  axi_slave_bfm.store_get_byte(write_axi_addr, tr);
  assert(write_v == tr);
end

always @(posedge cpumem_rready) begin
  bfm_hold_read <= 1'b1;
  repeat (bfm_latency) @(posedge cpumem_clk) ;
  bfm_hold_read <= 1'b0;
end

assign bfm_rready = cpumem_rready & ~bfm_hold_read;
assign cpumem_rvalid = bfm_rvalid & ~bfm_hold_read;

always @(posedge cpumem_bready) begin
  bfm_hold_write <= 1'b1;
  repeat (bfm_latency) @(posedge cpumem_clk) ;
  bfm_hold_write <= 1'b0;
end

assign bfm_bready = cpumem_bready & ~bfm_hold_write;
assign cpumem_bvalid = bfm_bvalid & ~bfm_hold_write;

initial begin
  axi_slave_bfm.store_set_byte(BASE_WRAM + 'h00765, 8'h11);
  axi_slave_bfm.store_set_byte(BASE_WRAM + 'h00766, 8'h22);
  axi_slave_bfm.store_set_byte(BASE_WRAM + 'h00767, 8'h33);
  axi_slave_bfm.store_set_byte(BASE_WRAM + 'h10768, 8'h44);
  axi_slave_bfm.store_set_byte(BASE_WRAM + 'h10769, 8'h55);
  axi_slave_bfm.store_set_byte(BASE_WRAM + 'h1076a, 8'h66);
  axi_slave_bfm.store_set_byte(BASE_WRAM + 'h1076b, 8'h77);
end

initial begin
  #0.5 @(posedge cpumem_clk) nRES = 1;

  cpumem_axi_en <= 1'b1;

  repeat (4) @(posedge cpumem_clk) ;

  bfm_latency = 18;
  set_addr(17'h00765);
  read_verify();
  read_verify();
  set_addr(17'h01234);
  write_verify(8'h34);
  set_addr(17'h11234);
  write_verify(8'h56);
  set_addr(17'h01234);
  read_verify();
  set_addr(17'h11234);
  read_verify();
  bfm_latency = 5;
  set_addr(17'h01233);
  write_verify(8'h78);
  set_addr(17'h15678);
  write_verify(8'h9a);
  set_addr(17'h00765);
  read_verify();
  bfm_latency = 15;
  set_addr(17'h10768);
  read_verify();
  read_verify();
  read_verify();
  read_verify();
  repeat (4) @(posedge mclk) ;

  cpumem_mon_sel <= 8'h40;
  cpumem_mon_ws <= 1'b1;
  @(posedge mclk) ;
  cpumem_mon_sel <= 8'bx;
  cpumem_mon_ws <= 1'b0;
  repeat (4) @(posedge mclk) ;

  cpumem_mon_sel <= 8'h50;
  cpumem_mon_ws <= 1'b1;
  @(posedge mclk) ;
  cpumem_mon_sel <= 8'bx;
  cpumem_mon_ws <= 1'b0;
  repeat (4) @(posedge mclk) ;

  $finish;
end

always @(posedge cpumem.to_error or posedge cpumem.axi_error) begin
  $display("cpumem error");
  $finish(1);
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s cpumem_io_tb -o cpumem_io_tb.vvp -f snes.files ../../axi/axi_slave_bfm.v cpumem_io_tb.sv && ./cpumem_io_tb.vvp"
// End:
