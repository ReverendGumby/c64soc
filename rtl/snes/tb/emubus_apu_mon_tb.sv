`timescale 1us / 1ns

module emubus_apu_mon_tb();

reg        nRES;
reg        clk;                 // APU clock
reg        emu_clk;
reg        hold;

wire       cken, rw;
wire [15:0] ma;
wire [7:0]  mdo_dsp, do_ram;
wire signed [15:0] psoutl, psoutr;
wire        nCE, nWE, nOE;
wire        sclken;

initial begin
  $dumpfile("emubus_apu_mon_tb.vcd");
  $dumpvars();
end

wire            emubus_clk = emu_clk;
`include "emubus.svh"

wire [15:0]     a;
wire [7:0]      do_dsp, do_smp;

wire [7:0]      apu_mon_sel;
wire            apu_mon_ready;
wire [31:0]     apu_mon_dout_smp, apu_mon_dout_dsp;
reg [31:0]      apu_mon_dout;
wire            apu_mon_valid_smp, apu_mon_valid_dsp;
reg             apu_mon_valid;
wire            apu_mon_ws;
wire [31:0]     apu_mon_din;

s_smp s_smp
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken),
   .HOLD(hold),
   .A(a),
   .DI(do_dsp),
   .DO(do_smp),
   .RW(rw),
   .MON_SEL(apu_mon_sel),
   .MON_ACK(apu_mon_ack_smp),
   .MON_READY(apu_mon_ready),
   .MON_DOUT(apu_mon_dout_smp),
   .MON_VALID(apu_mon_valid_smp),
   .MON_WS(apu_mon_ws),
   .MON_DIN(apu_mon_din)
   );

s_dsp dsp
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken),
   .HOLD(hold),
   .A(a),
   .DI(do_smp),
   .DO(do_dsp),
   .RW(rw),
   .MA(),
   .MDI(),
   .MDO(),
   .nCE(),
   .nWE(),
   .nOE(),
   .SCLKEN(sclken),
   .PSOUTL(psoutl),
   .PSOUTR(psoutr),
   .MON_SEL(apu_mon_sel),
   .MON_ACK(apu_mon_ack_dsp),
   .MON_READY(apu_mon_ready),
   .MON_DOUT(apu_mon_dout_dsp),
   .MON_VALID(apu_mon_valid_dsp),
   .MON_WS(apu_mon_ws),
   .MON_DIN(apu_mon_din)
   );

emubus emubus
  (
   .CLK(emu_clk),
   .CLKEN(1'b1),
   .APU_MCLK(clk),

   .ARESETn(nRES),
   .AWADDR(emubus_awaddr),
   .AWVALID(emubus_awvalid),
   .AWREADY(emubus_awready),
   .WDATA(emubus_wdata),
   .WREADY(emubus_wready),
   .WSTRB(emubus_wstrb),
   .WVALID(emubus_wvalid),
   .BRESP(emubus_bresp),
   .BVALID(emubus_bvalid),
   .BREADY(emubus_bready),
   .ARADDR(emubus_araddr),
   .ARVALID(emubus_arvalid),
   .ARREADY(emubus_arready),
   .RDATA(emubus_rdata),
   .RRESP(emubus_rresp),
   .RVALID(emubus_rvalid),
   .RREADY(emubus_rready),

   .EXT_RES(EXT_RES),
   .CLK_RESET(clk_reset),
   .EMU_nRES(emu_nres),
   .RES_COLD(res_cold),

   .APU_MON_SEL(apu_mon_sel),
   .APU_MON_READY(apu_mon_ready),
   .APU_MON_DOUT(apu_mon_dout),
   .APU_MON_VALID(apu_mon_valid),
   .APU_MON_WS(apu_mon_ws),
   .APU_MON_DIN(apu_mon_din)
   );

always @* begin
  apu_mon_valid = 1'b0;
  if (apu_mon_ack_smp) begin
    apu_mon_valid = apu_mon_valid_smp;
    apu_mon_dout = apu_mon_dout_smp;
  end
  else if (apu_mon_ack_dsp) begin
    apu_mon_valid = apu_mon_valid_dsp;
    apu_mon_dout = apu_mon_dout_dsp;
  end
  else begin
    apu_mon_valid = 1'b1;
    apu_mon_dout = 32'b0;
  end
end

initial begin
  clk = 1;
  emu_clk = 1;
  nRES = 0;
  hold = 0;
end

initial forever begin :clkgen
  #(500.0/24576.0) clk = ~clk;
end

initial forever begin :emu_clkgen
  #(500.0/21477.0) emu_clk = ~emu_clk;
end

task apu_mon_write(input [7:0] a, input [31:0] v);
  begin
    emubus_write32({19'h24, 2'b0}, a);
    emubus_write32({19'h25, 2'b0}, v);
  end
endtask

task apu_mon_read(input [7:0] a, output [31:0] v);
  begin
    emubus_write32({19'h24, 2'b0}, a);
    emubus_read32({19'h25, 2'b0}, v);
  end
endtask

initial begin :test
reg [31:0] tr, v0, v1;
  #3 @(posedge emu_clk) nRES = 1;
  hold <= 1;

  for (int a = 0; a < 9'h100; a += 1) begin
    tr = {32{1'b0}};
    apu_mon_write(a[7:0], tr);
    apu_mon_read(a[7:0], v0);
    tr = {32{1'b1}};
    apu_mon_write(a[7:0], tr);
    apu_mon_read(a[7:0], v1);
    $display("%x = %x %x", a, v0, v1);
  end

  #1 $finish;
end

initial #800 begin
  $display("Emergency stop!");
  $stop;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s emubus_apu_mon_tb -o emubus_apu_mon_tb.vvp -f snes.files emubus_apu_mon_tb.sv && ./emubus_apu_mon_tb.vvp"
// End:
