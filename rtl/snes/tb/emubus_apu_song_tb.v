`timescale 1us / 1ns

module emubus_apu_song_tb();

reg        nRES;
reg        clk;                 // APU clock
reg        emu_clk;
reg        emu_clken;

wire       cken, rw;
wire [15:0] ma;
wire [7:0]  mdo_dsp, do_ram;
wire signed [15:0] psoutl, psoutr;
wire        nCE, nWE, nOE;
wire        sclken;

initial begin
`ifdef VERILATOR
  $dumpfile("emubus_apu_song_tb.verilator.vcd");
  $dumpvars();
`else
  $dumpfile("emubus_apu_song_tb.vcd");
  $dumpvars();
`endif
end

wire [15:0] a;
wire [7:0] do_dsp, do_smp;

reg [31:0] awaddr, araddr, wdata;
reg [3:0]  wstrb;
reg        awvalid, wvalid, bready, arvalid, rready;

wire [31:0] rdata;
wire [1:0]  rresp, bresp;
wire        awready, wready, bvalid, arready, rvalid;

wire            emubus_apu_re, emubus_apu_we;
wire [7:0]      emubus_apu_d_o, emubus_apu_d_i;
wire [15:0]     emubus_apu_a_o;

wire [7:0]      apu_mon_sel;
wire            apu_mon_ready;
wire [31:0]     apu_mon_dout_smp, apu_mon_dout_dsp;
reg [31:0]      apu_mon_dout;
wire            apu_mon_valid_smp, apu_mon_valid_dsp;
reg             apu_mon_valid;
wire            apu_mon_ws;
wire [31:0]     apu_mon_din;

s_smp s_smp
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken),
   .HOLD(1'b0),
   .A(a),
   .DI(do_dsp),
   .DO(do_smp),
   .RW(rw),
   .MON_SEL(apu_mon_sel),
   .MON_ACK(apu_mon_ack_smp),
   .MON_READY(apu_mon_ready),
   .MON_DOUT(apu_mon_dout_smp),
   .MON_VALID(apu_mon_valid_smp),
   .MON_WS(apu_mon_ws),
   .MON_DIN(apu_mon_din)
   );

s_dsp dsp
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken),
   .HOLD(1'b0),
   .A(a),
   .DI(do_smp),
   .DO(do_dsp),
   .RW(rw),
   .MA(ma),
   .MDI(do_ram),
   .MDO(mdo_dsp),
   .nCE(nCE),
   .nWE(nWE),
   .nOE(nOE),
   .SCLKEN(sclken),
   .PSOUTL(psoutl),
   .PSOUTR(psoutr),
   .MON_SEL(apu_mon_sel),
   .MON_ACK(apu_mon_ack_dsp),
   .MON_READY(apu_mon_ready),
   .MON_DOUT(apu_mon_dout_dsp),
   .MON_VALID(apu_mon_valid_dsp),
   .MON_WS(apu_mon_ws),
   .MON_DIN(apu_mon_din)
   );

dpram #(.DWIDTH(8), .AWIDTH(16)) ram
  (
   .CLK(clk), 

   .A(ma),
   .DI(mdo_dsp),
   .DO(do_ram),
   .nCE(nCE),
   .nWE(nWE),
   .nOE(nOE),

   .A2(emubus_apu_a_o),
   .DI2(emubus_apu_d_o),
   .DO2(emubus_apu_d_i),
   .nCE2(~(emubus_apu_re | emubus_apu_we)),
   .nWE2(~emubus_apu_we),
   .nOE2(~emubus_apu_re)
   );

emubus emubus
  (
   .CLK(emu_clk),
   .CLKEN(emu_clken),
   .APU_MCLK(clk),

   .ARESETn(nRES),
   .AWADDR(awaddr),
   .AWVALID(awvalid),
   .AWREADY(awready),
   .WDATA(wdata),
   .WREADY(wready),
   .WSTRB(wstrb),
   .WVALID(wvalid),
   .BRESP(bresp),
   .BVALID(bvalid),
   .BREADY(bready),
   .ARADDR(araddr),
   .ARVALID(arvalid),
   .ARREADY(arready),
   .RDATA(rdata),
   .RRESP(rresp),
   .RVALID(rvalid),
   .RREADY(rready),

   .EXT_RES(EXT_RES),
   .CLK_RESET(clk_reset),
   .EMU_nRES(emu_nres),
   .RES_COLD(res_cold),

   .APU_RAM_RE(emubus_apu_re),
   .APU_RAM_WE(emubus_apu_we),
   .APU_RAM_D_I(emubus_apu_d_i),
   .APU_RAM_D_O(emubus_apu_d_o),
   .APU_RAM_A_O(emubus_apu_a_o),

   .APU_MON_SEL(apu_mon_sel),
   .APU_MON_READY(apu_mon_ready),
   .APU_MON_DOUT(apu_mon_dout),
   .APU_MON_VALID(apu_mon_valid),
   .APU_MON_WS(apu_mon_ws),
   .APU_MON_DIN(apu_mon_din)
   );

always @* begin
  apu_mon_valid = 1'b0;
  if (apu_mon_ack_smp) begin
    apu_mon_valid = apu_mon_valid_smp;
    apu_mon_dout = apu_mon_dout_smp;
  end
  else if (apu_mon_ack_dsp) begin
    apu_mon_valid = apu_mon_valid_dsp;
    apu_mon_dout = apu_mon_dout_dsp;
  end
  else begin
    apu_mon_valid = 1'b1;
    apu_mon_dout = 32'b0;
  end
end

initial begin
  clk = 1;
  emu_clk = 1;
  emu_clken = 0;
  nRES = 0;
end

initial begin
  awaddr = 32'b0;
  awvalid = 1'b0;
  wdata = 32'b0;
  wstrb = 4'b0;
  wvalid = 1'b0;
  bready = 1'b0;
  araddr = 32'b0;
  arvalid = 1'b0;
  rready = 1'b0;
end

initial forever begin :clkgen
  #(500.0/24576.0) clk = ~clk;
end

initial forever begin :emu_clkgen
  #(500.0/21477.0) emu_clk = ~emu_clk;
end

always @(posedge emu_clk)
  emu_clken <= ~emu_clken;

task emubus_write(input [31:0] a, input [31:0] v, input [3:0] s);
  begin
    awaddr <= a;
    awvalid <= 1'b1;
    wdata <= v;
    wstrb <= s;
    wvalid <= 1'b1;
    bready <= 1'b1;
    while (!(awready & wready & bvalid))
      @(posedge emu_clk) ;
    awvalid <= 1'b0;
    wvalid <= 1'b0;
    bready <= 1'b0;
    @(posedge emu_clk) ;
  end
endtask

task emubus_write32(input [31:0] a, input [31:0] v);
  emubus_write(a, v, 4'hf);
endtask

task emubus_write8(input [31:0] a, input [7:0] v);
  emubus_write(a, {4{v}}, 4'h1 << a[1:0]);
endtask

task emubus_read32(input [31:0] a, output [31:0] v);
  begin
    araddr <= a;
    arvalid <= 1'b1;
    rready <= 1'b1;
    while (!arready)
      @(posedge emu_clk) ;
    v = rdata;
    arvalid <= 1'b0;
    while (!rvalid)
      @(posedge emu_clk) ;
    rready <= 1'b0;
    @(posedge emu_clk) ;
  end
endtask

task emubus_read8(input [31:0] a, output [7:0] v);
reg [31:0] tr;
  begin
    emubus_read32(a, tr);
    v = tr >> (8 * a[1:0]);
  end
endtask

task emubus_rmw32(input [31:0] a, input [31:0] v, input [31:0] m);
reg [7:0] tr;
  begin
    emubus_read32(a, tr);
    tr = (tr & ~m) | (v & m);
    emubus_write32(a, tr);
  end
endtask

task emubus_rmw8(input [31:0] a, input [7:0] v, input [7:0] m);
reg [7:0] tr;
  begin
    emubus_read8(a, tr);
    tr = (tr & ~m) | (v & m);
    emubus_write8(a, tr);
  end
endtask

task apu_mon_write(input [7:0] a, input [31:0] v);
  begin
    emubus_write8({19'h24, 2'b0}, a);
    emubus_write32({19'h25, 2'b0}, v);
  end
endtask

task apu_mon_read(input [7:0] a, output [31:0] v);
  begin
    emubus_write8({19'h24, 2'b0}, a);
    emubus_read32({19'h25, 2'b0}, v);
  end
endtask

task apu_mon_read_until(input [7:0] a, input [31:0] v, input [31:0] m);
reg [31:0] tr;
  begin
    apu_mon_read(a, tr);
    while ((tr & m) != v) begin
      emubus_read32({19'h25, 2'b0}, tr);
    end 
  end
endtask

reg [7:0] ram_mem [0:16'hffff];
reg [7:0] dsp_regs_mem [0:7'h7f];

initial begin :load
integer i, dur;
reg [31:0] tr;
  #3 @(posedge emu_clk) nRES = 1;

  #1 emubus_rmw32(19'h50, 32'h1, 32'h1); // emu_usr_nres
  @(posedge emu_nres) ;

  // stall SMP and DSP
  $display("assert stalls");
  #10 apu_mon_write(8'h06, 32'h01);
  apu_mon_write(8'h40, 32'h01);
  apu_mon_read_until(8'h06, 32'h03, 32'h03);
  apu_mon_read_until(8'h40, 32'h03, 32'h03);

  $display("load DSP regs");
  $readmemh("dsp_regs.hex", dsp_regs_mem);
  for (i = 0; i < 8'h80; i = i + 1) begin
    apu_mon_write(8'h41, i[6:0]);
    apu_mon_write(8'h42, dsp_regs_mem[i]);
  end
  for (i = 0; i < 8'h80; i = i + 1) begin
    apu_mon_write(8'h41, i[6:0]);
    apu_mon_read(8'h42, tr);
    if (tr[7:0] !== dsp_regs_mem[i])
      $display("At 0x%x, wanted 0x%x, got 0x%x", i[6:0], dsp_regs_mem[i], tr[7:0]);
  end

  $display("load DSP RAM");
  $readmemh("ram.hex", ram_mem);
  for (i = 0; i < 65536; i = i + 1) begin
    emubus_write8(19'h10000 + i[15:0], ram_mem[i]);
  end
  for (i = 0; i < 65536; i = i + 1) begin
    emubus_read8(19'h10000 + i[15:0], tr[7:0]);
    if (tr[7:0] !== ram_mem[i])
      $display("At 0x%x, wanted 0x%x, got 0x%x", i[15:0], ram_mem[i], tr[7:0]);
  end

  $display("load CPU regs");
  apu_mon_write(8'h01, 32'h000291);   // ARC0: PF, PC[15:0]
  apu_mon_write(8'h02, 32'h012220FF); // ARC1: Y, X, AC, S
  apu_mon_write(8'h04, 32'h00);       // UAR0: DOR, AOR, IR = NOP

  apu_mon_write(8'h43, 32'h07058027); // SMP0: SPCR, SPT2, SPT1, SPT0

  $display("release stalls");
  apu_mon_write(8'h40, 32'h00);  // release DSP stall
  apu_mon_read_until(8'h40, 32'h00, 32'h03);

  apu_mon_write(8'h06, 8'hF0);  // reset Timing State, release SMP stall
  apu_mon_read_until(8'h06, 32'h00, 32'h03);

`ifdef VERILATOR
  dur = 500;
`else
  dur = 1;
`endif
  for (i = 0; i < dur; i = i + 1)
    #1000 ;
  $finish;
end

wire [7:0] mem0 = ram.mem[0];

endmodule

// Local Variables:
// compile-command: "iverilog -g2005 -grelative-include -s emubus_apu_song_tb -o emubus_apu_song_tb.vvp -f snes.files emubus_apu_song_tb.v && ./emubus_apu_song_tb.vvp"
// End:
