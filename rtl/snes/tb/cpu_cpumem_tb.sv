`timescale 1us / 1ns

module cpu_cpumem_tb();

reg        nRES;
reg        clk;
wire       cp1_posedge, cp1_negedge, cp2_posedge, cp2_negedge, cp2_negedge_pre;

initial begin
  $dumpfile("cpu_cpumem_tb.vcd");
  $dumpvars();
end

wire [15:0] a;

wire [23:0] cpu_a;
wire [7:0]  cpu_d, cpu_d_o;
wire        nromsel, nwramsel;
wire        nwr, nrd;

reg [31:0]  cat_addr;
reg         cat_dreq;

reg         cpumem_axi_en;
wire        cpumem_cpu_ready;

wire [31:0] cpumem_araddr;
wire [1:0]  cpumem_arburst;
wire [3:0]  cpumem_arcache;
wire [3:0]  cpumem_arid;
wire [3:0]  cpumem_arlen;
wire [1:0]  cpumem_arlock;
wire [2:0]  cpumem_arprot;
wire [3:0]  cpumem_arqos;
wire        cpumem_arready;
wire [2:0]  cpumem_arsize;
wire [4:0]  cpumem_aruser;
wire        cpumem_arvalid;
wire [31:0] cpumem_awaddr;
wire [1:0]  cpumem_awburst;
wire [3:0]  cpumem_awcache;
wire [3:0]  cpumem_awid;
wire [3:0]  cpumem_awlen;
wire [1:0]  cpumem_awlock;
wire [2:0]  cpumem_awprot;
wire [3:0]  cpumem_awqos;
wire        cpumem_awready;
wire [2:0]  cpumem_awsize;
wire [4:0]  cpumem_awuser;
wire        cpumem_awvalid;
wire [3:0]  cpumem_bid;
wire        cpumem_bready;
wire [1:0]  cpumem_bresp;
wire        cpumem_bvalid;
wire [63:0] cpumem_rdata;
wire [3:0]  cpumem_rid;
wire        cpumem_rlast;
wire        cpumem_rready;
wire [1:0]  cpumem_rresp;
wire        cpumem_rvalid;
wire [63:0] cpumem_wdata;
wire [2:0]  cpumem_wid;
wire        cpumem_wlast;
wire        cpumem_wready;
wire [7:0]  cpumem_wstrb;
wire        cpumem_wvalid;

localparam [31:0] BASE_WRAM = 32'h0000_0000;
localparam [31:0] BASE_ROM = 32'h0002_0000;

s_cpu cpu
  (
   .nRES(nRES),
   .MCLK(clk),
   .HOLD(~cpumem_cpu_ready),
   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .CP2_NEGEDGE_PRE(cp2_negedge_pre),

   .nIRQ(1'b1),
   .nNMI(1'b1),
   .nABORT(1'b1),
   .nML(),
   .MF(),
   .XF(),
   .RW(),
   .RDY(1'b1),
   .nVP(),
   .VDA(),
   .VPA(),

   .A(cpu_a),
   .D_I(cpu_d),
   .D_O(cpu_d_o),
   .nROMSEL(nromsel),
   .nWRAMSEL(nwramsel),
   .nWR(nwr),
   .nRD(nrd),

   .PA(),
   .nPARD(),
   .nPAWR(),

   .CC(1'b0),
   .HBLANK(1'b0),
   .VBLANK(1'b0),

   .CORE_MON_SEL(6'd0),
   .CORE_MON_WS(1'b0),
   .CORE_MON_DOUT(),
   .CORE_MON_DIN()
   );

cpumem cpumem
  (
   .MCLK(clk),
   .CP1_POSEDGE(cp1_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .CP2_NEGEDGE_PRE(cp2_negedge_pre),

   // Module status/control
   .BASE_WRAM(BASE_WRAM),
   .AXI_EN(cpumem_axi_en),
   .CPU_READY(cpumem_cpu_ready),

   // Monitor interface
   .MON_SEL(8'h00),
   .MON_WS(1'b0),
   .MON_DOUT(),
   .MON_DIN(32'bz),

   // CPU memory bus interface
   .A(cpu_a),
   .D_I(cpu_d_o),
   .D_O(cpu_d),
   .D_OE(),
   .nROMSEL(nromsel),
   .nWRAMSEL(nwramsel),
   .nWR(nwr),
   .nRD(nrd),
   .PA(),
   .nPARD(1'b1),
   .nPAWR(1'b1),

   // Cartridge address translation interface
   .CAT_ADDR(cat_addr),
   .CAT_DREQ(cat_dreq),

   // AXI bus leader for ACP
   .ACLK(clk),
   .ARADDR(cpumem_araddr),
   .ARBURST(cpumem_arburst),
   .ARCACHE(cpumem_arcache),
   .ARID(cpumem_arid[2:0]),
   .ARLEN(cpumem_arlen),
   .ARLOCK(cpumem_arlock),
   .ARPROT(cpumem_arprot),
   .ARQOS(cpumem_arqos),
   .ARREADY(cpumem_arready),
   .ARSIZE(cpumem_arsize),
   .ARUSER(cpumem_aruser),
   .ARVALID(cpumem_arvalid),
   .AWADDR(cpumem_awaddr),
   .AWBURST(cpumem_awburst),
   .AWCACHE(cpumem_awcache),
   .AWID(cpumem_awid[2:0]),
   .AWLEN(cpumem_awlen),
   .AWLOCK(cpumem_awlock),
   .AWPROT(cpumem_awprot),
   .AWQOS(cpumem_awqos),
   .AWREADY(cpumem_awready),
   .AWSIZE(cpumem_awsize),
   .AWUSER(cpumem_awuser),
   .AWVALID(cpumem_awvalid),
   .BID(cpumem_bid[2:0]),
   .BREADY(cpumem_bready),
   .BRESP(2'b00),
   .BVALID(cpumem_bvalid),
   .RDATA(cpumem_rdata),
   .RID(cpumem_rid[2:0]),
   .RLAST(cpumem_rlast),
   .RREADY(cpumem_rready),
   .RRESP(2'b00),
   .RVALID(cpumem_rvalid),
   .WDATA(cpumem_wdata),
   .WID(cpumem_wid),
   .WLAST(cpumem_wlast),
   .WREADY(cpumem_wready),
   .WSTRB(cpumem_wstrb),
   .WVALID(cpumem_wvalid)
   );

axi_slave_bfm #(32, 64) axi_slave_bfm
  (
   .ARESETn(nRES),
   .ACLK(clk),

   .AWID(cpumem_awid),
   .AWADDR(cpumem_awaddr),
   .AWLEN({4'b0, cpumem_awlen}),
   .AWSIZE(cpumem_awsize),
   .AWBURST(cpumem_awburst),
   .AWVALID(cpumem_awvalid),
   .AWREADY(cpumem_awready),
   .WDATA(cpumem_wdata),
   .WSTRB(cpumem_wstrb),
   .WLAST(cpumem_wlast),
   .WVALID(cpumem_wvalid),
   .WREADY(cpumem_wready),
   .BID(cpumem_bid),
   .BVALID(cpumem_bvalid),
   .BREADY(cpumem_bready),

   .ARID(cpumem_arid),
   .ARADDR(cpumem_araddr),
   .ARLEN({4'b0, cpumem_arlen}),
   .ARSIZE(cpumem_arsize),
   .ARBURST(cpumem_arburst),
   .ARVALID(cpumem_arvalid),
   .ARREADY(cpumem_arready),
   .RID(cpumem_rid),
   .RDATA(cpumem_rdata),
   .RLAST(cpumem_rlast),
   .RVALID(cpumem_rvalid),
   .RREADY(cpumem_rready)
   );

initial begin
  clk = 1;
  nRES = 0;
end

initial begin
  cpumem_axi_en = 1'b0;
end

initial forever begin :clkgen
  #(500.0/21477.27) clk <= ~clk;
end

reg [22:0] rom_a;
reg        rom_ce;

always @* begin
  rom_a[14:0] = cpu_a[14:0];
  rom_a[21:15] = cpu_a[22:16];
  rom_a[22] = 1'b0;
  rom_ce = nromsel & cpu_a[15];

  cat_addr = BASE_ROM + {9'b0, rom_a};
  cat_dreq = ~nrd;
end

task cpu_addr_to_axi_addr(input [23:0] cpu_addr, output [31:0] axi_addr);
  if (~cpu_addr[22] & ~|cpu_addr[15:13])
    axi_addr = BASE_WRAM + cpu_addr[12:0];
  else if (cpu_addr[23:17] == 7'h3F)
    axi_addr = BASE_WRAM + cpu_addr[16:0];
  else if (cpu_addr[15])
    axi_addr = BASE_ROM + { cpu_addr[23:16], cpu_addr[14:0] };
  else
    assert(0);
endtask

initial begin :rom_loader
integer fin;
reg [31:0] off;
  fin = $fopen("snes_tests_novblank.sfc", "r");
  assert(fin != 0) else $finish;
  off = 0;
  while (!$feof(fin)) begin
    axi_slave_bfm.store_set_byte(BASE_ROM + off, $fgetc(fin));
    off = off + 1;
  end  
  $display("ROM loaded");
  $fclose(fin);
end

initial begin
  #5 @(posedge clk) nRES <= 1;
  
  @(posedge cp2_negedge) cpumem_axi_en <= 1'b1;
  $display("cpumem enabled");

  //#200 ;
  #158010 ;

  $finish;
end

always @(posedge cpumem.to_error or posedge cpumem.axi_error) begin
  $display("cpumem error");
  $finish(1);
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s cpu_cpumem_tb -o cpu_cpumem_tb.vvp -f snes.files ../../axi/axi_slave_bfm.v cpu_cpumem_tb.sv && ./cpu_cpumem_tb.vvp"
// End:
