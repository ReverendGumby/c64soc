`timescale 1us / 1ns

module cpumem_mcmi_tb();

reg        nRES;
reg        clk, mclk, cpumem_clk;
wire       cp1_posedge, cp2_negedge, cp2_negedge_pre;

initial begin
  $dumpfile("cpumem_mcmi_tb.vcd");
  $dumpvars();
end

reg         cpumem_axi_en;
wire        cpumem_cpu_ready;
wire        cpumem_idle;

reg [7:0]   cpumem_mon_sel;
reg         cpumem_mon_ws;
wire [31:0] cpumem_mon_dout;
reg [31:0]  cpumem_mon_din;

wire [31:0] cpumem_araddr;
wire [1:0]  cpumem_arburst;
wire [3:0]  cpumem_arcache;
wire [3:0]  cpumem_arid;
wire [3:0]  cpumem_arlen;
wire [1:0]  cpumem_arlock;
wire [2:0]  cpumem_arprot;
wire [3:0]  cpumem_arqos;
wire        cpumem_arready;
wire [2:0]  cpumem_arsize;
wire [4:0]  cpumem_aruser;
wire        cpumem_arvalid;
wire [31:0] cpumem_awaddr;
wire [1:0]  cpumem_awburst;
wire [3:0]  cpumem_awcache;
wire [3:0]  cpumem_awid;
wire [3:0]  cpumem_awlen;
wire [1:0]  cpumem_awlock;
wire [2:0]  cpumem_awprot;
wire [3:0]  cpumem_awqos;
wire        cpumem_awready;
wire [2:0]  cpumem_awsize;
wire [4:0]  cpumem_awuser;
wire        cpumem_awvalid;
wire [3:0]  cpumem_bid;
wire        cpumem_bready;
wire [1:0]  cpumem_bresp;
wire        cpumem_bvalid;
wire [63:0] cpumem_rdata;
wire [3:0]  cpumem_rid;
wire        cpumem_rlast;
wire        cpumem_rready;
wire [1:0]  cpumem_rresp;
wire        cpumem_rvalid;
wire [63:0] cpumem_wdata;
wire [2:0]  cpumem_wid;
wire        cpumem_wlast;
wire        cpumem_wready;
wire [7:0]  cpumem_wstrb;
wire        cpumem_wvalid;

integer     bfm_latency;

localparam [31:0] BASE_WRAM = 32'h0000_0000;
localparam [31:0] BASE_ROM = 32'h0002_0000;

cpumem cpumem
  (
   .MCLK(mclk),
   .CP1_POSEDGE(cp1_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .CP2_NEGEDGE_PRE(cp2_negedge_pre),

   // Module status/control
   .BASE_WRAM(BASE_WRAM),
   .AXI_EN(cpumem_axi_en),
   .CPU_READY(cpumem_cpu_ready),
   .IDLE(cpumem_idle),

   // Monitor interface
   .MON_SEL(cpumem_mon_sel),
   .MON_WS(cpumem_mon_ws),
   .MON_DOUT(cpumem_mon_dout),
   .MON_DIN(cpumem_mon_din),

   // CPU memory bus interface
   .A(),
   .D_I(),
   .D_O(),
   .D_OE(),
   .nROMSEL(1'b1),
   .nWRAMSEL(1'b1),
   .nWR(1'b1),
   .nRD(1'b1),
   .PA(),
   .nPARD(1'b1),
   .nPAWR(1'b1),

   // Cartridge address translation interface
   .CAT_ADDR(),
   .CAT_DREQ(1'b0),

   // AXI bus leader for ACP
   .ACLK(cpumem_clk),
   .ARADDR(cpumem_araddr),
   .ARBURST(cpumem_arburst),
   .ARCACHE(cpumem_arcache),
   .ARID(cpumem_arid[2:0]),
   .ARLEN(cpumem_arlen),
   .ARLOCK(cpumem_arlock),
   .ARPROT(cpumem_arprot),
   .ARQOS(cpumem_arqos),
   .ARREADY(cpumem_arready),
   .ARSIZE(cpumem_arsize),
   .ARUSER(cpumem_aruser),
   .ARVALID(cpumem_arvalid),
   .AWADDR(cpumem_awaddr),
   .AWBURST(cpumem_awburst),
   .AWCACHE(cpumem_awcache),
   .AWID(cpumem_awid[2:0]),
   .AWLEN(cpumem_awlen),
   .AWLOCK(cpumem_awlock),
   .AWPROT(cpumem_awprot),
   .AWQOS(cpumem_awqos),
   .AWREADY(cpumem_awready),
   .AWSIZE(cpumem_awsize),
   .AWUSER(cpumem_awuser),
   .AWVALID(cpumem_awvalid),
   .BID(cpumem_bid[2:0]),
   .BREADY(cpumem_bready),
   .BRESP(2'b00),
   .BVALID(cpumem_bvalid),
   .RDATA(cpumem_rdata),
   .RID(cpumem_rid[2:0]),
   .RLAST(cpumem_rlast),
   .RREADY(cpumem_rready),
   .RRESP(2'b00),
   .RVALID(cpumem_rvalid),
   .WDATA(cpumem_wdata),
   .WID(cpumem_wid),
   .WLAST(cpumem_wlast),
   .WREADY(cpumem_wready),
   .WSTRB(cpumem_wstrb),
   .WVALID(cpumem_wvalid)
   );

axi_slave_bfm #(32, 64) axi_slave_bfm
  (
   .ARESETn(nRES),
   .ACLK(cpumem_clk),

   .AWID(cpumem_awid),
   .AWADDR(cpumem_awaddr),
   .AWLEN({4'b0, cpumem_awlen}),
   .AWSIZE(cpumem_awsize),
   .AWBURST(cpumem_awburst),
   .AWVALID(cpumem_awvalid),
   .AWREADY(cpumem_awready),
   .WDATA(cpumem_wdata),
   .WSTRB(cpumem_wstrb),
   .WLAST(cpumem_wlast),
   .WVALID(cpumem_wvalid),
   .WREADY(cpumem_wready),
   .BID(cpumem_bid),
   .BVALID(cpumem_bvalid),
   .BREADY(cpumem_bready),

   .ARID(cpumem_arid),
   .ARADDR(cpumem_araddr),
   .ARLEN({4'b0, cpumem_arlen}),
   .ARSIZE(cpumem_arsize),
   .ARBURST(cpumem_arburst),
   .ARVALID(cpumem_arvalid),
   .ARREADY(cpumem_arready),
   .RID(cpumem_rid),
   .RDATA(cpumem_rdata),
   .RLAST(cpumem_rlast),
   .RVALID(cpumem_rvalid),
   .RREADY(cpumem_rready)
   );

initial begin
  clk = 1;
  nRES = 0;
end

initial begin
  cpumem_axi_en = 1'b0;

  cpumem_mon_ws = 1'b0;

  bfm_latency = 0;
end

always @bfm_latency
  axi_slave_bfm.latency = bfm_latency;

initial forever begin :clkgen
  #(250.0/24576.0) clk = ~clk;
end

localparam [1:0] mclk_ccnt_max = 2'd2;
reg [1:0] mclk_ccnt;
initial begin
  cpumem_clk = 'b0;
  mclk_ccnt = 2'd0;
  mclk = 'b0;
end
always @(posedge clk) begin
  cpumem_clk <= ~cpumem_clk;
  if (mclk_ccnt == mclk_ccnt_max) begin
    mclk_ccnt <= 2'd0;
    mclk <= ~mclk;
  end
  else
    mclk_ccnt <= mclk_ccnt + 1'd1;
end

localparam [7:0] cycle_ccnt = 8'd8;
reg [7:0] cp2_clkcnt;
initial
  cp2_clkcnt = 8'd0;
always @(posedge mclk) begin
  if (cpumem_cpu_ready) begin
    if (cp2_negedge_pre)
      cp2_clkcnt <= 8'd0;
    else
      cp2_clkcnt <= cp2_clkcnt + 1'd1;
  end
end
assign cp2_negedge_pre = cp2_clkcnt == (cycle_ccnt - 1'd1);
assign cp2_negedge = cp2_clkcnt == 8'd0;
assign cp1_posedge = cp2_clkcnt == 8'd1;

task mon_read(input [7:0] sel, output [31:0] dout);
  cpumem_mon_sel <= sel;
  @(posedge mclk) ;
  dout = cpumem_mon_dout;
  cpumem_mon_sel <= 8'bx;
  cpumem_mon_din <= 32'bx;
  @(posedge mclk) ;
endtask

task mon_write(input [7:0] sel, input [31:0] din);
  cpumem_mon_sel <= sel;
  cpumem_mon_din <= din;
  cpumem_mon_ws <= 1'b1;
  @(posedge mclk) ;
  cpumem_mon_sel <= 8'bx;
  cpumem_mon_din <= 32'bx;
  cpumem_mon_ws <= 1'b0;
  @(posedge mclk) ;
endtask

task read_verify(input [31:0] a, input [7:0] v);
reg [31:0] tr;
  mon_write(8'h09 /* MADDR */, a);
  mon_write(8'h08 /* MCMI */, 32'h1 /* MRREQ */);
  wait_for_cpumem_idle();
  mon_read(8'h21 /* RDATA */, tr);
  repeat (4) @(posedge mclk) ;
  
  assert(v == tr[7:0]);
endtask

task write_verify(input [31:0] a, input [7:0] v);
reg [7:0] tr;
  mon_write(8'h09 /* MADDR */, a);
  mon_write(8'h0A /* MDATAW */, {24'b0, v});
  mon_write(8'h08 /* MCMI */, 32'h2 /* MWREQ */);
  repeat (4) @(posedge mclk) ;
  
  wait_for_cpumem_idle();
  axi_slave_bfm.store_get_byte(a, tr);
  assert(v == tr);
endtask

task wait_for_cpumem_idle();
  while (~cpumem_idle)
    @(posedge mclk) ;
endtask

initial begin
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h00765, 8'h11);
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h00766, 8'h22);
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h00767, 8'h33);
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h08768, 8'h44);
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h10768, 8'h55);
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h20768, 8'h66);
  axi_slave_bfm.store_set_byte(BASE_ROM + 'h40768, 8'h77);
end

initial begin
  #0.5 @(posedge cpumem_clk) nRES = 1;

  cpumem_axi_en <= 1'b1;

  repeat (4) @(posedge cpumem_clk) ;

  bfm_latency = 19;
  read_verify(BASE_ROM + 'h00765, 8'h11);
  read_verify(BASE_ROM + 'h00766, 8'h22);
  write_verify(BASE_WRAM + 'h01234, 8'h34);
  write_verify(BASE_WRAM + 'h12345, 8'h56);
  wait_for_cpumem_idle();

  $finish;
end

always @(posedge cpumem.to_error or posedge cpumem.axi_error) begin
  $display("cpumem error");
  $finish(1);
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s cpumem_mcmi_tb -o cpumem_mcmi_tb.vvp -f snes.files ../../axi/axi_slave_bfm.v cpumem_mcmi_tb.sv && ./cpumem_mcmi_tb.vvp"
// End:
