reg [31:0] emubus_awaddr, emubus_araddr, emubus_wdata;
reg [3:0]  emubus_wstrb;
reg        emubus_awvalid, emubus_wvalid, emubus_bready, 
           emubus_arvalid, emubus_rready;

wire [31:0] emubus_rdata;
wire [1:0]  emubus_rresp, emubus_bresp;
wire        emubus_awready, emubus_wready, emubus_bvalid, 
            emubus_arready, emubus_rvalid;

initial begin
  emubus_awaddr = 32'b0;
  emubus_awvalid = 1'b0;
  emubus_wdata = 32'b0;
  emubus_wstrb = 4'b0;
  emubus_wvalid = 1'b0;
  emubus_bready = 1'b0;
  emubus_araddr = 32'b0;
  emubus_arvalid = 1'b0;
  emubus_rready = 1'b0;
end

task emubus_write(input [31:0] a, input [31:0] v, input [3:0] s);
  begin
    emubus_awaddr <= a;
    emubus_awvalid <= 1'b1;
    emubus_wdata <= v;
    emubus_wstrb <= s;
    emubus_wvalid <= 1'b1;
    emubus_bready <= 1'b1;
    while (!(emubus_awready & emubus_wready & emubus_bvalid))
      @(posedge emubus_clk) ;
    emubus_awvalid <= 1'b0;
    emubus_wvalid <= 1'b0;
    emubus_bready <= 1'b0;
    @(posedge emubus_clk) ;
  end
endtask

task emubus_write32(input [31:0] a, input [31:0] v);
  emubus_write(a, v, 4'hf);
endtask

task emubus_write8(input [31:0] a, input [7:0] v);
  emubus_write(a, {4{v}}, 4'h1 << a[1:0]);
endtask

task emubus_write16(input [31:0] a, input [15:0] v);
  emubus_write(a, {2{v}}, 4'h3 << (a[1] * 2));
endtask

task emubus_read32(input [31:0] a, output [31:0] v);
  begin
    emubus_araddr <= a;
    emubus_arvalid <= 1'b1;
    emubus_rready <= 1'b1;
    while (!emubus_arready)
      @(posedge emubus_clk) ;
    v = emubus_rdata;
    emubus_arvalid <= 1'b0;
    while (!emubus_rvalid)
      @(posedge emubus_clk) ;
    emubus_rready <= 1'b0;
    @(posedge emubus_clk) ;
  end
endtask

task emubus_read8(input [31:0] a, output [7:0] v);
reg [31:0] tr;
  begin
    emubus_read32(a, tr);
    tr = tr >> (8 * a[1:0]);
    v = tr[7:0];
  end
endtask

task emubus_read16(input [31:0] a, output [15:0] v);
reg [31:0] tr;
  begin
    emubus_read32(a, tr);
    tr = tr >> (16 * a[0]);
    v = tr[15:0];
  end
endtask

task emubus_rmw32(input [31:0] a, input [31:0] v, input [31:0] m);
reg [31:0] tr;
  begin
    emubus_read32(a, tr);
    tr = (tr & ~m) | (v & m);
    emubus_write32(a, tr);
  end
endtask

task emubus_rmw8(input [31:0] a, input [7:0] v, input [7:0] m);
reg [7:0] tr;
  begin
    emubus_read8(a, tr);
    tr = (tr & ~m) | (v & m);
    emubus_write8(a, tr);
  end
endtask
