`timescale 1us / 1ns

module emubus_ppu_vram_tb();

reg        nRES;
reg        emu_clk;

initial begin
  $dumpfile("emubus_ppu_vram_tb.vcd");
  $dumpvars();
end

wire [14:0]     emubus_ppu_vram_a_o;
wire            emubus_ppu_vram_re;
wire            emubus_ppu_vrama_we;
wire [7:0]      emubus_ppu_vrama_d_i, emubus_ppu_vrama_d_o;
wire            emubus_ppu_vramb_we;
wire [7:0]      emubus_ppu_vramb_d_i, emubus_ppu_vramb_d_o;

wire            emubus_clk = emu_clk;
`include "emubus.svh"

emubus emubus
  (
   .CLK(emu_clk),
   .CLKEN(1'b1),
   .APU_MCLK(1'b0),

   .ARESETn(nRES),
   .AWADDR(emubus_awaddr),
   .AWVALID(emubus_awvalid),
   .AWREADY(emubus_awready),
   .WDATA(emubus_wdata),
   .WREADY(emubus_wready),
   .WSTRB(emubus_wstrb),
   .WVALID(emubus_wvalid),
   .BRESP(emubus_bresp),
   .BVALID(emubus_bvalid),
   .BREADY(emubus_bready),
   .ARADDR(emubus_araddr),
   .ARVALID(emubus_arvalid),
   .ARREADY(emubus_arready),
   .RDATA(emubus_rdata),
   .RRESP(emubus_rresp),
   .RVALID(emubus_rvalid),
   .RREADY(emubus_rready),

   .EXT_RES(1'b0),
   .EMU_nRES(emu_nres),

   .PPU_VRAM_A_O(emubus_ppu_vram_a_o),
   .PPU_VRAM_RE(emubus_ppu_vram_re),
   .PPU_VRAMA_WE(emubus_ppu_vrama_we),
   .PPU_VRAMA_D_I(emubus_ppu_vrama_d_i),
   .PPU_VRAMA_D_O(emubus_ppu_vrama_d_o),
   .PPU_VRAMB_WE(emubus_ppu_vramb_we),
   .PPU_VRAMB_D_I(emubus_ppu_vramb_d_i),
   .PPU_VRAMB_D_O(emubus_ppu_vramb_d_o)
   );

dpram #(.DWIDTH(8), .AWIDTH(15)) vrama
  (
   .CLK(emu_clk),

   .nCE(1'b1),
   .nWE(1'b1),
   .nOE(1'b1),
   .A(),
   .DI(),
   .DO(),

   .nCE2(~(emubus_ppu_vram_re | emubus_ppu_vrama_we)),
   .nWE2(~emubus_ppu_vrama_we),
   .nOE2(~emubus_ppu_vram_re),
   .A2(emubus_ppu_vram_a_o),
   .DI2(emubus_ppu_vrama_d_o),
   .DO2(emubus_ppu_vrama_d_i)
   );

dpram #(.DWIDTH(8), .AWIDTH(15)) vramb
  (
   .CLK(emu_clk),

   .nCE(1'b1),
   .nWE(1'b1),
   .nOE(1'b1),
   .A(),
   .DI(),
   .DO(),

   .nCE2(~(emubus_ppu_vram_re | emubus_ppu_vramb_we)),
   .nWE2(~emubus_ppu_vramb_we),
   .nOE2(~emubus_ppu_vram_re),
   .A2(emubus_ppu_vram_a_o),
   .DI2(emubus_ppu_vramb_d_o),
   .DO2(emubus_ppu_vramb_d_i)
   );

initial begin
  emu_clk = 1;
  nRES = 0;
end

initial forever begin :emu_clkgen
  #(500.0/21477.0) emu_clk = ~emu_clk;
end

task read8_verify(input [15:0] a, input [7:0] v);
reg [7:0] tr;
  emubus_read8(19'h20000 + a, tr);
  assert(v == tr);
endtask

task write8_verify(input [15:0] a, input [7:0] v);
  emubus_write8(19'h20000 + a, v);
  if (a[0])
    assert(vramb.mem[a[15:1]] == v);
  else
    assert(vrama.mem[a[15:1]] == v);
endtask

task read16_verify(input [15:0] a, input [15:0] v);
reg [15:0] tr;
  emubus_read16(19'h20000 + a, tr);
  assert(v == tr);
endtask

task write16_verify(input [15:0] a, input [15:0] v);
  emubus_write16(19'h20000 + a, v);
  assert(vrama.mem[a[15:1]] == v[7:0]);
  assert(vramb.mem[a[15:1]] == v[15:8]);
endtask

initial begin
  @(posedge emu_clk) nRES = 1;

  #1 emubus_rmw32(19'h50, 32'h1, 32'h1); // emu_usr_nres
  @(posedge emu_nres) ;

  @(posedge emu_clk) ;
  write8_verify(16'h4A8B, 8'h55);
  write8_verify(16'h4A87, 8'h77);
  write8_verify(16'h4A88, 8'h88);
  write8_verify(16'h4A89, 8'h99);
  write8_verify(16'h4A8A, 8'hAA);
  write8_verify(16'h4A86, 8'h55);
  read8_verify(16'h4A86, 8'h55);
  read8_verify(16'h4A87, 8'h77);
  read8_verify(16'h4A88, 8'h88);
  read8_verify(16'h4A89, 8'h99);
  read8_verify(16'h4A8A, 8'hAA);
  read8_verify(16'h4A8B, 8'h55);

  write16_verify(16'h2510, 16'h5566);
  write16_verify(16'h2512, 16'h7788);
  read16_verify(16'h2510, 16'h5566);
  read16_verify(16'h2512, 16'h7788);

  $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s emubus_ppu_vram_tb -o emubus_ppu_vram_tb.vvp -f snes.files ../../axi/axi_slave_bfm.v emubus_ppu_vram_tb.sv && ./emubus_ppu_vram_tb.vvp"
// End:
