// Emulation system:
// - CPU memory / PS7 memory (AXI_ACP) bridge
// - CPU subsystem: S-CPU, in-circuit debugger
// - Video subsystem: S-PPU
// - Audio subsystem: S-SMP
// - EMU / PS7 bridge

module emu
  (
   output [31:0] DEBUG,
   input         CPUMEM_CLK, // AXI_ACP clock input
   input         MCLK, // master clock input: 21477272 Hz
   input         AUD_MCLK, // audio clock input: 24576000 Hz
   input         EXT_HOLD,
   input         EXT_RES,
   output        EMU_nRES,
   input         BTN_KEY,
   input         SW_SRC,

   // Video output
   output        VID_PCE,
   output        VID_DE,
   output        VID_HS,
   output        VID_VS,
   output [7:0]  VID_R,
   output [7:0]  VID_G,
   output [7:0]  VID_B,

   // Audio output
   output        AUD_SCLK,
   output        AUD_LRCLK,
   output        AUD_SDOUT,
   output [15:0] AUD_PDOUTL,
   output [15:0] AUD_PDOUTR,

   // AXI Lite slave bus interface
   input         EMUBUS_ARESETn,
   input [31:0]  EMUBUS_AWADDR,
   input         EMUBUS_AWVALID,
   output        EMUBUS_AWREADY,
   input [31:0]  EMUBUS_WDATA,
   output        EMUBUS_WREADY,
   input [3:0]   EMUBUS_WSTRB,
   input         EMUBUS_WVALID,
   output [1:0]  EMUBUS_BRESP,
   output        EMUBUS_BVALID,
   input         EMUBUS_BREADY,
   input [31:0]  EMUBUS_ARADDR,
   input         EMUBUS_ARVALID,
   output        EMUBUS_ARREADY,
   output [31:0] EMUBUS_RDATA,
   output [1:0]  EMUBUS_RRESP,
   output        EMUBUS_RVALID,
   input         EMUBUS_RREADY,

   // AXI_ACP master bus interface for CPU memory
   output [31:0] CPUMEM_ARADDR,
   output [1:0]  CPUMEM_ARBURST,
   output [3:0]  CPUMEM_ARCACHE,
   output [2:0]  CPUMEM_ARID,
   output [3:0]  CPUMEM_ARLEN,
   output [1:0]  CPUMEM_ARLOCK,
   output [2:0]  CPUMEM_ARPROT,
   output [3:0]  CPUMEM_ARQOS,
   input         CPUMEM_ARREADY,
   output [2:0]  CPUMEM_ARSIZE,
   output [4:0]  CPUMEM_ARUSER,
   output        CPUMEM_ARVALID,
   output [31:0] CPUMEM_AWADDR,
   output [1:0]  CPUMEM_AWBURST,
   output [3:0]  CPUMEM_AWCACHE,
   output [2:0]  CPUMEM_AWID,
   output [3:0]  CPUMEM_AWLEN,
   output [1:0]  CPUMEM_AWLOCK,
   output [2:0]  CPUMEM_AWPROT,
   output [3:0]  CPUMEM_AWQOS,
   input         CPUMEM_AWREADY,
   output [2:0]  CPUMEM_AWSIZE,
   output [4:0]  CPUMEM_AWUSER,
   output        CPUMEM_AWVALID,
   input [2:0]   CPUMEM_BID,
   output        CPUMEM_BREADY,
   input [1:0]   CPUMEM_BRESP,
   input         CPUMEM_BVALID,
   input [63:0]  CPUMEM_RDATA,
   input [2:0]   CPUMEM_RID,
   input         CPUMEM_RLAST,
   output        CPUMEM_RREADY,
   input [1:0]   CPUMEM_RRESP,
   input         CPUMEM_RVALID,
   output [63:0] CPUMEM_WDATA,
   output [2:0]  CPUMEM_WID,
   output        CPUMEM_WLAST,
   input         CPUMEM_WREADY,
   output [7:0]  CPUMEM_WSTRB,
   output        CPUMEM_WVALID
   );

wire            cp1_posedge, cp1_negedge, cp2_posedge, cp2_negedge,
                cp2_negedge_pre;
wire            cc, ncc;

wire            clk_reset, res_cold;
wire            cart_nres;

wire            cpumem_axi_en;
wire [31:0]     cpumem_base_wram, cpumem_base_rom;
wire            cpumem_cpu_ready;
reg [23:0]      cpumem_a;
wire [7:0]      cpumem_d_i, cpumem_d_o;
reg             cpumem_nromsel, cpumem_nwramsel;
reg             cpumem_nrd, cpumem_nwr;
wire [7:0]      cpumem_mon_sel;
wire            cpumem_mon_ws;
wire [31:0]     cpumem_mon_dout, cpumem_mon_din;

wire [31:0]     cart_cat_addr;
wire            cart_cat_dreq;
wire [11:2]     cart_ctrl_addr;
wire [3:0]      cart_ctrl_wstrb;
wire [31:0]     cart_ctrl_din;
wire [31:0]     cart_ctrl_dout;
wire [7:0]      cart_d_o;
wire            cart_d_oe;

wire            cpu_rdy;
wire [23:0]     cpu_a;
reg [7:0]       cpu_d;
wire            cpu_d_oe;
wire [7:0]      cpu_d_i, cpu_d_o;
wire            cpu_nromsel, cpu_nwramsel;
wire            cpu_nwr, cpu_nrd;
wire            cpu_rw, cpu_vda, cpu_vpa;
wire [7:0]      cpu_io_pa;
wire            cpu_io_npard, cpu_io_npawr;
wire [7:0]      cpu_mon_sel;
wire [31:0]     cpu_mon_din, cpu_mon_dout;
wire [3:0]      cpu_mon_ws;
wire            cpu_mon_ready, cpu_mon_valid;
wire [5:0]      cpu_core_mon_sel;
wire [31:0]     cpu_core_mon_din, cpu_core_mon_dout;
wire            cpu_core_mon_ws;

wire [10:0]     ppu_mon_sel;
wire            ppu_mon_ready;
wire [31:0]     ppu_mon_dout, ppu_mon_din;
wire            ppu_mon_valid;
wire [3:0]      ppu_mon_ws;
wire [31:0]     ppu_icd_frame;
wire [8:0]      ppu_icd_col;
wire [8:0]      ppu_icd_row;

reg [7:0]       io_pa;
wire            io_npard, io_npawr;

wire            sys_hold_req, cpu_hold_req, apu_hold_req;
wire            sys_hold_ack, cpu_hold_ack, apu_hold_ack;
wire            emubus_sys_hold_req, emubus_cpu_hold_req, emubus_apu_hold_req;
wire            emubus_lock_apu_to_cpu_hold;
reg             apu_hold_req_d;

wire            icd_sys_enable;
wire            icd_sys_force_halt;
wire            icd_sys_resume;
wire            icd_sys_hold;
wire [7:0]      icd_match_enable;
wire [7:0]      icd_match_trigger;
wire [2:0]      icd_match_sel;
wire [3:0]      icd_match_reg_sel;
wire [3:0]      icd_match_reg_wstrb;
wire [31:0]     icd_match_reg_din;
wire [31:0]     icd_match_reg_dout;

wire            apu_nres;
wire [15:0]     apu_a, apu_ma;
wire [7:0]      apu_do_smp, apu_do_dsp;
wire            apu_rw;
wire [7:0]      apu_mdo_dsp, apu_do_ram;
wire            apu_cken;
wire            apu_nce, apu_nwe, apu_noe;
wire [7:0]      apu_cpui0, apu_cpui1, apu_cpui2, apu_cpui3;
wire [7:0]      apu_cpuo0, apu_cpuo1, apu_cpuo2, apu_cpuo3;
wire [3:0]      apu_cpuin, apu_cpuon;
wire [15:0]     apu_psoutl, apu_psoutr;

wire [7:0]      ppu_io_d_o;
wire            ppu_io_d_oe;

wire [2:1]      jpclk;
wire [2:0]      jpout;
wire [1:0]      jp1d;
wire [4:0]      jp2d;
wire [7:0]      jpio, cpu_jpio_o;
wire [1:0]      ctlr1_s2c, ctlr2_s2c;
wire [2:0]      ctlr1_c2s, ctlr2_c2s;

wire            emubus_apu_re, emubus_apu_we;
wire [7:0]      emubus_apu_d_o, emubus_apu_d_i;
wire [15:0]     emubus_apu_a_o;

wire [7:0]      apu_mon_sel;
wire            apu_mon_ack_smp, apu_mon_ack_dsp;
wire            apu_mon_ready;
wire [31:0]     apu_mon_dout_smp, apu_mon_dout_dsp;
reg [31:0]      apu_mon_dout;
wire            apu_mon_valid_smp, apu_mon_valid_dsp;
reg             apu_mon_valid;
wire            apu_mon_ws;
wire [31:0]     apu_mon_din;

wire [14:0]     emubus_ppu_vram_a_o;
wire            emubus_ppu_vram_re;
wire            emubus_ppu_vrama_we;
wire [7:0]      emubus_ppu_vrama_d_i, emubus_ppu_vrama_d_o;
wire            emubus_ppu_vramb_we;
wire [7:0]      emubus_ppu_vramb_d_i, emubus_ppu_vramb_d_o;

wire [15:0]     aud_pdoutl, aud_pdoutr;
wire            aud_apu_hold_req, aud_apu_hold_ack;
reg             apu_hold_req_mclk;

wire            hblank, vblank;

wire [7:0]      cpu_apu_io_d_o;
wire            cpu_apu_io_d_oe;


//////////////////////////////////////////////////////////////////////
// Emulator system hold

assign sys_hold_req = emubus_sys_hold_req | EXT_HOLD;
assign cpu_hold_req = sys_hold_req | emubus_cpu_hold_req |
                      ~cpumem_cpu_ready | icd_sys_hold;
assign apu_hold_req = sys_hold_req | emubus_apu_hold_req |
                      (emubus_lock_apu_to_cpu_hold & cpu_hold_req);

assign sys_hold_ack = cpu_hold_ack & apu_hold_ack;
assign cpu_hold_ack = cpu_hold_req; // takes effect immediately


//////////////////////////////////////////////////////////////////////
// CPU memory / PS7 memory (AXI_ACP) bridge

cpumem cpumem
  (
   .MCLK(MCLK),
   .CP1_POSEDGE(cp1_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .CP2_NEGEDGE_PRE(cp2_negedge_pre),

   // Module status/control
   .BASE_WRAM(cpumem_base_wram),
   .AXI_EN(cpumem_axi_en),
   .CPU_READY(cpumem_cpu_ready),
   .IDLE(),

   // CPU memory bus interface
   .A(cpumem_a),
   .D_I(cpumem_d_i),
   .D_O(cpumem_d_o),
   .D_OE(),
   .nROMSEL(cpumem_nromsel),
   .nWRAMSEL(cpumem_nwramsel),
   .nWR(cpumem_nwr),
   .nRD(cpumem_nrd),
   .PA(io_pa),
   .nPARD(io_npard),
   .nPAWR(io_npawr),

   // Cartridge address translation interface
   .CAT_ADDR(cart_cat_addr),
   .CAT_DREQ(cart_cat_dreq),

   // Monitor interface
   .MON_SEL(cpumem_mon_sel),
   .MON_WS(cpumem_mon_ws),
   .MON_DOUT(cpumem_mon_dout),
   .MON_DIN(cpumem_mon_din),

   // AXI bus leader for ACP
   .ACLK(CPUMEM_CLK),
   .ARADDR(CPUMEM_ARADDR),
   .ARBURST(CPUMEM_ARBURST),
   .ARCACHE(CPUMEM_ARCACHE),
   .ARID(CPUMEM_ARID),
   .ARLEN(CPUMEM_ARLEN),
   .ARLOCK(CPUMEM_ARLOCK),
   .ARPROT(CPUMEM_ARPROT),
   .ARQOS(CPUMEM_ARQOS),
   .ARREADY(CPUMEM_ARREADY),
   .ARSIZE(CPUMEM_ARSIZE),
   .ARUSER(CPUMEM_ARUSER),
   .ARVALID(CPUMEM_ARVALID),
   .AWADDR(CPUMEM_AWADDR),
   .AWBURST(CPUMEM_AWBURST),
   .AWCACHE(CPUMEM_AWCACHE),
   .AWID(CPUMEM_AWID),
   .AWLEN(CPUMEM_AWLEN),
   .AWLOCK(CPUMEM_AWLOCK),
   .AWPROT(CPUMEM_AWPROT),
   .AWQOS(CPUMEM_AWQOS),
   .AWREADY(CPUMEM_AWREADY),
   .AWSIZE(CPUMEM_AWSIZE),
   .AWUSER(CPUMEM_AWUSER),
   .AWVALID(CPUMEM_AWVALID),
   .BID(CPUMEM_BID),
   .BREADY(CPUMEM_BREADY),
   .BRESP(CPUMEM_BRESP),
   .BVALID(CPUMEM_BVALID),
   .RDATA(CPUMEM_RDATA),
   .RID(CPUMEM_RID),
   .RLAST(CPUMEM_RLAST),
   .RREADY(CPUMEM_RREADY),
   .RRESP(CPUMEM_RRESP),
   .RVALID(CPUMEM_RVALID),
   .WDATA(CPUMEM_WDATA),
   .WID(CPUMEM_WID),
   .WLAST(CPUMEM_WLAST),
   .WREADY(CPUMEM_WREADY),
   .WSTRB(CPUMEM_WSTRB),
   .WVALID(CPUMEM_WVALID)
   );

always @* begin
  cpumem_a = cpu_a;
  cpumem_nrd = cpu_nrd;
  cpumem_nwr = cpu_nwr;
  cpumem_nromsel = cpu_nromsel;
  cpumem_nwramsel = cpu_nwramsel;
end

assign cpumem_d_i = cpu_d;


//////////////////////////////////////////////////////////////////////
// Game cartridge emulator

cart cart
  (
   .CLK(MCLK),
   .nRES(EMU_nRES),
   .CP2_NEGEDGE(cp2_negedge),

   // CPU memory bus interface
   .A(cpu_a),
   .D_I(cpu_d_i),
   .D_O(cart_d_o),
   .D_OE(cart_d_oe),
   .nROMSEL(cpu_nromsel),
   .nWRAMSEL(cpu_nwramsel),
   .nWR(cpu_nwr),
   .nRD(cpu_nrd),
   .PA(io_pa[5:0]),
   .nPARD(io_npard),
   .nPAWR(io_npawr),

   .CTRL_nRES(cart_nres),
   .CTRL_ADDR(cart_ctrl_addr),
   .CTRL_WSTRB(cart_ctrl_wstrb),
   .CTRL_DIN(cart_ctrl_din),
   .CTRL_DOUT(cart_ctrl_dout),

   .CAT_ADDR(cart_cat_addr),
   .CAT_DREQ(cart_cat_dreq)
   );


//////////////////////////////////////////////////////////////////////
// S-CPU

s_cpu cpu
  (
   .nRES(EMU_nRES),
   .MCLK(MCLK),
   .HOLD(cpu_hold_ack),
   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .CP2_NEGEDGE_PRE(cp2_negedge_pre),

   .MON_SEL(cpu_mon_sel),
   .MON_READY(cpu_mon_ready),
   .MON_DOUT(cpu_mon_dout),
   .MON_VALID(cpu_mon_valid),
   .MON_WS(cpu_mon_ws),
   .MON_DIN(cpu_mon_din),

   .nIRQ(1'b1),
   .nNMI(1'b1),
   .nABORT(1'b1),
   .nML(),
   .MF(),
   .XF(),
   .RW(cpu_rw),
   .RDY(1'b1),
   .nVP(),
   .VDA(cpu_vda),
   .VPA(cpu_vpa),

   .A(cpu_a),
   .D_I(cpu_d_i),
   .D_O(cpu_d_o),
   .D_OE(cpu_d_oe),
   .nROMSEL(cpu_nromsel),
   .nWRAMSEL(cpu_nwramsel),
   .nWR(cpu_nwr),
   .nRD(cpu_nrd),

   .PA(cpu_io_pa),
   .nPARD(cpu_io_npard),
   .nPAWR(cpu_io_npawr),

   .JPCLK(jpclk),
   .JPOUT(jpout),
   .JP1D(jp1d),
   .JP2D(jp2d),
   .JPIO_I(jpio),
   .JPIO_O(cpu_jpio_o),

   .CC(cc),
   .HBLANK(hblank),
   .VBLANK(vblank),

   .CORE_MON_SEL(cpu_core_mon_sel),
   .CORE_MON_WS(cpu_core_mon_ws),
   .CORE_MON_DOUT(cpu_core_mon_dout),
   .CORE_MON_DIN(cpu_core_mon_din)
   );

always @* begin
  cpu_d = cpumem_d_o;
  if (cpu_d_oe)
    cpu_d = cpu_d_o;
  else if (cart_d_oe)
    cpu_d = cart_d_o;
  else if (ppu_io_d_oe)
    cpu_d = ppu_io_d_o;
  else if (cpu_apu_io_d_oe)
    cpu_d = cpu_apu_io_d_o;
end

assign cpu_d_i = cpu_d;

always @* begin
  io_pa = cpu_io_pa;
end

assign io_npard = cpu_io_npard;
assign io_npawr = cpu_io_npawr;

assign jpio = cpu_jpio_o; // no external drivers (yet)


//////////////////////////////////////////////////////////////////////
// CPU in-circuit debugger

icd #(.NUM_MATCH(2)) icd
  (
   .nRES(EMU_nRES),
   .CLK(MCLK),
   .CP1_POSEDGE(cp1_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .HOLD(cpu_hold_ack),

   .CPU_A(cpu_a),
   .CPU_DB(cpu_d_i),
   .CPU_RW(cpu_rw),
   .CPU_RDY(1'b1),
   .CPU_SYNC(cpu_vda & cpu_vpa),
   .CPU_CYC(/*cpu_cyc*/32'b0),
   .PPU_COL(ppu_icd_col),
   .PPU_ROW(ppu_icd_row),
   .PPU_FRAME(ppu_icd_frame),

   .MATCH_ENABLE(icd_match_enable[1:0]),
   .MATCH_TRIGGER(icd_match_trigger[1:0]),
   .MATCH_SEL(icd_match_sel),
   .MATCH_REG_SEL(icd_match_reg_sel),
   .MATCH_REG_WSTRB(icd_match_reg_wstrb),
   .MATCH_REG_DIN(icd_match_reg_din),
   .MATCH_REG_DOUT(icd_match_reg_dout),

   .SYS_ENABLE(icd_sys_enable),
   .SYS_FORCE_HALT(icd_sys_force_halt),
   .SYS_RESUME(icd_sys_resume),
   .SYS_HOLD(icd_sys_hold)
   );


//////////////////////////////////////////////////////////////////////
// Controller I/O

player player
  (
   .JPCLK(jpclk),
   .JPOUT(jpout),
   .JP1D(jp1d),
   .JP2D(jp2d),

   .CTLR1_I(ctlr1_s2c),
   .CTLR1_O(ctlr1_c2s),
   .CTLR2_I(ctlr2_s2c),
   .CTLR2_O(ctlr2_c2s)
   );


//////////////////////////////////////////////////////////////////////
// Audio subsystem

s_smp s_smp
  (
   .nRES(apu_nres),
   .CLK(AUD_MCLK),
   .CKEN(apu_cken),
   .HOLD(aud_apu_hold_ack),
   .A(apu_a),
   .DI(apu_do_dsp),
   .DO(apu_do_smp),
   .RW(apu_rw),
   .CPUI0(apu_cpui0),
   .CPUI1(apu_cpui1),
   .CPUI2(apu_cpui2),
   .CPUI3(apu_cpui3),
   .CPUIN(apu_cpuin),
   .CPUO0(apu_cpuo0),
   .CPUO1(apu_cpuo1),
   .CPUO2(apu_cpuo2),
   .CPUO3(apu_cpuo3),
   .CPUON(apu_cpuon),
   .MON_SEL(apu_mon_sel),
   .MON_ACK(apu_mon_ack_smp),
   .MON_READY(apu_mon_ready),
   .MON_DOUT(apu_mon_dout_smp),
   .MON_VALID(apu_mon_valid_smp),
   .MON_WS(apu_mon_ws),
   .MON_DIN(apu_mon_din)
   );

s_dsp s_dsp
  (
   .nRES(apu_nres),
   .CLK(AUD_MCLK),
   .CKEN(apu_cken),
   .HOLD(aud_apu_hold_ack),
   .A(apu_a),
   .DI(apu_do_smp),
   .DO(apu_do_dsp),
   .RW(apu_rw),
   .MA(apu_ma),
   .MDI(apu_do_ram),
   .MDO(apu_mdo_dsp),
   .nCE(apu_nce),
   .nWE(apu_nwe),
   .nOE(apu_noe),
   .SCLKEN(),
   .PSOUTL(apu_psoutl),
   .PSOUTR(apu_psoutr),
   .MON_SEL(apu_mon_sel),
   .MON_ACK(apu_mon_ack_dsp),
   .MON_READY(apu_mon_ready),
   .MON_DOUT(apu_mon_dout_dsp),
   .MON_VALID(apu_mon_valid_dsp),
   .MON_WS(apu_mon_ws),
   .MON_DIN(apu_mon_din)
   );

assign aud_apu_hold_ack = aud_apu_hold_req; // takes effect immediately

dpram #(.DWIDTH(8), .AWIDTH(16)) apu_ram
  (
   .CLK(AUD_MCLK),

   .nCE(apu_nce | aud_apu_hold_ack),
   .nWE(apu_nwe),
   .nOE(apu_noe),
   .A(apu_ma),
   .DI(apu_mdo_dsp),
   .DO(apu_do_ram),

   .nCE2(~(emubus_apu_re | emubus_apu_we)),
   .nWE2(~emubus_apu_we),
   .nOE2(~emubus_apu_re),
   .A2(emubus_apu_a_o),
   .DI2(emubus_apu_d_o),
   .DO2(emubus_apu_d_i)
   );

always @* begin
  apu_mon_valid = 1'b0;
  if (apu_mon_ack_smp) begin
    apu_mon_valid = apu_mon_valid_smp;
    apu_mon_dout = apu_mon_dout_smp;
  end
  else if (apu_mon_ack_dsp) begin
    apu_mon_valid = apu_mon_valid_dsp;
    apu_mon_dout = apu_mon_dout_dsp;
  end
  else begin
    apu_mon_valid = 1'b1;
    apu_mon_dout = 32'b0;
  end
end

cdc_1bit_2ff apu_reset_sync
  (
   .A_DATA(EMU_nRES),
   .B_CLK(AUD_MCLK),
   .B_DATA(apu_nres)
   );

// Register apu_hold_req to MCLK for CDC.
always @(posedge MCLK)
  apu_hold_req_mclk <= apu_hold_req;

cdc_1bit_2ff apu_hold_req_sync
  (
   .A_DATA(apu_hold_req_mclk),
   .B_CLK(AUD_MCLK),
   .B_DATA(aud_apu_hold_req)
   );

cdc_1bit_2ff apu_hold_ack_sync
  (
   .A_DATA(aud_apu_hold_ack),
   .B_CLK(MCLK),
   .B_DATA(apu_hold_ack)
   );

cpu_apu_bbus cpu_apu_bbus
  (
   .CPU_CLK(MCLK),
   .CPU_PA(io_pa[1:0]),
   .CPU_D_I(cpu_d),
   .CPU_D_O(cpu_apu_io_d_o),
   .CPU_D_OE(cpu_apu_io_d_oe),
   .CPU_CS(io_pa[6]),
   .CPU_nCS(io_pa[7]),
   .CPU_nPARD(io_npard),
   .CPU_nPAWR(io_npawr),

   .APU_CLK(AUD_MCLK),
   .APU_CPUI0(apu_cpui0),
   .APU_CPUI1(apu_cpui1),
   .APU_CPUI2(apu_cpui2),
   .APU_CPUI3(apu_cpui3),
   .APU_CPUIN(apu_cpuin),
   .APU_CPUO0(apu_cpuo0),
   .APU_CPUO1(apu_cpuo1),
   .APU_CPUO2(apu_cpuo2),
   .APU_CPUO3(apu_cpuo3),
   .APU_CPUON(apu_cpuon)
   );

// Full-scale audio is not uncommon. Add some headroom for comfort.
assign aud_pdoutl = $signed(apu_psoutl) >>> 2;
assign aud_pdoutr = $signed(apu_psoutr) >>> 2;

//////////////////////////////////////////////////////////////////////
// EMU / PS7 bridge

emubus emubus
  (
   .CLK(MCLK),
   .CLKEN(1'b1),
   .APU_MCLK(AUD_MCLK),

   .ARESETn(EMUBUS_ARESETn),
   .AWADDR(EMUBUS_AWADDR),
   .AWVALID(EMUBUS_AWVALID),
   .AWREADY(EMUBUS_AWREADY),
   .WDATA(EMUBUS_WDATA),
   .WREADY(EMUBUS_WREADY),
   .WSTRB(EMUBUS_WSTRB),
   .WVALID(EMUBUS_WVALID),
   .BRESP(EMUBUS_BRESP),
   .BVALID(EMUBUS_BVALID),
   .BREADY(EMUBUS_BREADY),
   .ARADDR(EMUBUS_ARADDR),
   .ARVALID(EMUBUS_ARVALID),
   .ARREADY(EMUBUS_ARREADY),
   .RDATA(EMUBUS_RDATA),
   .RRESP(EMUBUS_RRESP),
   .RVALID(EMUBUS_RVALID),
   .RREADY(EMUBUS_RREADY),

   .EXT_RES(EXT_RES),
   .CLK_RESET(clk_reset),
   .EMU_nRES(EMU_nRES),
   .CART_nRES(cart_nres),
   .RES_COLD(res_cold),
   .SYS_HOLD_REQ(emubus_sys_hold_req),
   .SYS_HOLD_ACK(sys_hold_ack),
   .CPU_HOLD_REQ(emubus_cpu_hold_req),
   .CPU_HOLD_ACK(cpu_hold_ack),
   .APU_HOLD_REQ(emubus_apu_hold_req),
   .APU_HOLD_ACK(apu_hold_ack),
   .LOCK_APU_TO_CPU_HOLD(emubus_lock_apu_to_cpu_hold),

   .CPU_MON_SEL(cpu_mon_sel),
   .CPU_MON_READY(cpu_mon_ready),
   .CPU_MON_DOUT(cpu_mon_dout),
   .CPU_MON_VALID(cpu_mon_valid),
   .CPU_MON_WS(cpu_mon_ws),
   .CPU_MON_DIN(cpu_mon_din),

   .CPU_CORE_MON_SEL(cpu_core_mon_sel),
   .CPU_CORE_MON_WS(cpu_core_mon_ws),
   .CPU_CORE_MON_DOUT(cpu_core_mon_dout),
   .CPU_CORE_MON_DIN(cpu_core_mon_din),

   .PPU_MON_SEL(ppu_mon_sel),
   .PPU_MON_READY(ppu_mon_ready),
   .PPU_MON_DOUT(ppu_mon_dout),
   .PPU_MON_VALID(ppu_mon_valid),
   .PPU_MON_WS(ppu_mon_ws),
   .PPU_MON_DIN(ppu_mon_din),

   .ICD_SYS_ENABLE(icd_sys_enable),
   .ICD_SYS_FORCE_HALT(icd_sys_force_halt),
   .ICD_SYS_RESUME(icd_sys_resume),
   .ICD_SYS_HOLD(icd_sys_hold),
   .ICD_MATCH_ENABLE(icd_match_enable),
   .ICD_MATCH_TRIGGER(icd_match_trigger),
   .ICD_MATCH_SEL(icd_match_sel),
   .ICD_MATCH_REG_SEL(icd_match_reg_sel),
   .ICD_MATCH_REG_WSTRB(icd_match_reg_wstrb),
   .ICD_MATCH_REG_DIN(icd_match_reg_din),
   .ICD_MATCH_REG_DOUT(icd_match_reg_dout),

   .CPUMEM_AXI_EN(cpumem_axi_en),
   .CPUMEM_BASE_WRAM(cpumem_base_wram),
   .CPUMEM_BASE_ROM(cpumem_base_rom),
   .CPUMEM_MON_SEL(cpumem_mon_sel),
   .CPUMEM_MON_WS(cpumem_mon_ws),
   .CPUMEM_MON_DOUT(cpumem_mon_dout),
   .CPUMEM_MON_DIN(cpumem_mon_din),

   .CART_CTRL_ADDR(cart_ctrl_addr),
   .CART_CTRL_WSTRB(cart_ctrl_wstrb),
   .CART_CTRL_DIN(cart_ctrl_din),
   .CART_CTRL_DOUT(cart_ctrl_dout),

   .CTLR1_I(ctlr1_s2c),
   .CTLR1_O(ctlr1_c2s),
   .CTLR2_I(ctlr2_s2c),
   .CTLR2_O(ctlr2_c2s),

   .APU_RAM_RE(emubus_apu_re),
   .APU_RAM_WE(emubus_apu_we),
   .APU_RAM_D_I(emubus_apu_d_i),
   .APU_RAM_D_O(emubus_apu_d_o),
   .APU_RAM_A_O(emubus_apu_a_o),

   .APU_MON_SEL(apu_mon_sel),
   .APU_MON_READY(apu_mon_ready),
   .APU_MON_DOUT(apu_mon_dout),
   .APU_MON_VALID(apu_mon_valid),
   .APU_MON_WS(apu_mon_ws),
   .APU_MON_DIN(apu_mon_din),

   .IO_PA(),
   .IO_PA_OE(),
   .IO_D_I(8'hxx),
   .IO_D_O(),
   .IO_D_OE(),
   .IO_nPARD(),
   .IO_nPAWR(),

   .PPU_HBLANK(hblank),
   .PPU_VBLANK(vblank),

   .PPU_VRAM_A_O(emubus_ppu_vram_a_o),
   .PPU_VRAM_RE(emubus_ppu_vram_re),
   .PPU_VRAMA_WE(emubus_ppu_vrama_we),
   .PPU_VRAMA_D_I(emubus_ppu_vrama_d_i),
   .PPU_VRAMA_D_O(emubus_ppu_vrama_d_o),
   .PPU_VRAMB_WE(emubus_ppu_vramb_we),
   .PPU_VRAMB_D_I(emubus_ppu_vramb_d_i),
   .PPU_VRAMB_D_O(emubus_ppu_vramb_d_o)
   );


//////////////////////////////////////////////////////////////////////
// I2S audio output

i2s_serializer ser
  (
   .MCLK(AUD_MCLK),
   .OCLKEN(),
   .PDINL(aud_pdoutl),
   .PDINR(aud_pdoutr),
   .SCLK(AUD_SCLK),
   .LRCLK(AUD_LRCLK),
   .SDOUT(AUD_SDOUT)
   );

assign AUD_PDOUTL = aud_pdoutl;
assign AUD_PDOUTR = aud_pdoutr;


//////////////////////////////////////////////////////////////////////
// Video subsystem

wire [15:0] vaa, vab;
wire [7:0]  vda_i, vda_o, vdb_i, vdb_o;
wire        nvard, nvawr, nvbrd, nvbwr;

s_ppu ppu
  (
   .CLK_RESET(1'b0),
   .CLK(MCLK),
   .CLKEN(1'b1),
   .RES_COLD(1'b0),
   .RESET(~EMU_nRES),
   .HOLD(cpu_hold_ack),

   .MON_SEL(ppu_mon_sel),
   .MON_READY(ppu_mon_ready),
   .MON_DOUT(ppu_mon_dout),
   .MON_VALID(ppu_mon_valid),
   .MON_WS(ppu_mon_ws),
   .MON_DIN(ppu_mon_din),

   .ICD_COL(ppu_icd_col),
   .ICD_ROW(ppu_icd_row),
   .ICD_FRAME(ppu_icd_frame),

   .CC(cc),
   .nCC(ncc),
   .HBLANK(hblank),
   .VBLANK(vblank),
   .EXTLATCH(jpio[7]),

   .PA(io_pa),
   .D_I(cpu_d),
   .D_O(ppu_io_d_o),
   .D_OE(ppu_io_d_oe),
   .nPARD(io_npard),
   .nPAWR(io_npawr),

   .VAA(vaa),
   .VDA_I(vda_i),
   .VDA_O(vda_o),
   .nVARD(nvard),
   .nVAWR(nvawr),

   .VAB(vab),
   .VDB_I(vdb_i),
   .VDB_O(vdb_o),
   .nVBRD(nvbrd),
   .nVBWR(nvbwr),

   .PCE(VID_PCE), // pixel clock enable
   .DE(VID_DE), // data enable
   .HS(VID_HS), // horizontal sync
   .VS(VID_VS), // vertical sync
   .R(VID_R), // red component
   .G(VID_G), // green component
   .B(VID_B)  // blue component
   );

dpram #(.DWIDTH(8), .AWIDTH(15)) vrama
  (
   .CLK(MCLK),

   .nCE(nvard & nvawr),
   .nWE(nvawr),
   .nOE(nvard),
   .A(vaa[14:0]),
   .DI(vda_o),
   .DO(vda_i),

   .nCE2(~(emubus_ppu_vram_re | emubus_ppu_vrama_we)),
   .nWE2(~emubus_ppu_vrama_we),
   .nOE2(~emubus_ppu_vram_re),
   .A2(emubus_ppu_vram_a_o),
   .DI2(emubus_ppu_vrama_d_o),
   .DO2(emubus_ppu_vrama_d_i)
   );

dpram #(.DWIDTH(8), .AWIDTH(15)) vramb
  (
   .CLK(MCLK),

   .nCE(nvbrd & nvbwr),
   .nWE(nvbwr),
   .nOE(nvbrd),
   .A(vab[14:0]),
   .DI(vdb_o),
   .DO(vdb_i),

   .nCE2(~(emubus_ppu_vram_re | emubus_ppu_vramb_we)),
   .nWE2(~emubus_ppu_vramb_we),
   .nOE2(~emubus_ppu_vram_re),
   .A2(emubus_ppu_vram_a_o),
   .DI2(emubus_ppu_vramb_d_o),
   .DO2(emubus_ppu_vramb_d_i)
   );


//////////////////////////////////////////////////////////////////////
// Debug

//assign DEBUG[0] = cp1_posedge;
//assign DEBUG[1] = cp2_negedge;
//assign DEBUG[2] = cpu.slow_cpu;
//assign DEBUG[3] = cpu.refresh_en;
//assign DEBUG[4] = cpu.fast_cpu;
//assign DEBUG[5] = cpu.slowck_sel;
//assign DEBUG[6] = hblank;
//assign DEBUG[7] = vblank;
//assign DEBUG[8] = cpu.cpu_gate_active;
//assign DEBUG[9] = cpu.slow_refresh_en;
//assign DEBUG[10] = cpu.ocio.dmac.aboe;
//assign DEBUG[11] = cpu.ocio.dmac.rwoe;
//assign DEBUG[12] = sys_hold_req;
//assign DEBUG[15:8] = ppu.renderer.m7_renderer.PDR;

endmodule
