// In-Circuit Debugger (not original to the SNES)
// Match unit: triggers on a system state matching an externally provided template
// SYS control unit: halts system on match (or forced halt) by asserting SYS_HOLD

/* verilator lint_off WIDTHTRUNC */

module icd #(parameter NUM_MATCH=8)
  (
   input                      nRES,
   input                      CLK,
   input                      CP1_POSEDGE,
   input                      CP2_NEGEDGE,
   input                      HOLD,

   // System state
   input [23:0]               CPU_A,
   input [7:0]                CPU_DB,
   input                      CPU_RW,
   input                      CPU_RDY,
   input                      CPU_SYNC,
   input [31:0]               CPU_CYC,
   input [8:0]                PPU_COL,
   input [8:0]                PPU_ROW,
   input [31:0]               PPU_FRAME,

   // Match units
   input [NUM_MATCH-1:0]      MATCH_ENABLE,
   output reg [NUM_MATCH-1:0] MATCH_TRIGGER,
   input [2:0]                MATCH_SEL,
   input [3:0]                MATCH_REG_SEL,
   input [3:0]                MATCH_REG_WSTRB,
   input [31:0]               MATCH_REG_DIN,
   output [31:0]              MATCH_REG_DOUT,

   // System control unit
   input                      SYS_ENABLE,
   input                      SYS_FORCE_HALT,
   input                      SYS_RESUME, // edge-triggered
   output reg                 SYS_HOLD
   );

reg [31:0] match_status [0:NUM_MATCH-1];
reg [95:0] match_din [0:NUM_MATCH-1];
reg [95:0] match_den [0:NUM_MATCH-1];
reg [127:0] match_dout [0:NUM_MATCH-1];
reg [31:0] match_rdata [0:NUM_MATCH-1];

reg [8:0]  ppu_col_cp1;
reg [8:0]  ppu_row_cp1;
reg [31:0] ppu_frame_cp1;

reg [31:0] state2, state1, state0;
reg        resume_dly;

wire [95:0] match_out [0:NUM_MATCH-1];

// To help things look like the many SW emulators we're using as
// reference, report the PPU state at the START of the instruction
// (CP1+) instead of the END (CP2-). Because PPU_* update right on
// CP1+, delay capture by one CLK.
reg cp1_posedge_d;
always @(posedge CLK) if (~HOLD)
  cp1_posedge_d <= CP1_POSEDGE;

always @(posedge CLK) if (~HOLD & cp1_posedge_d) begin
  ppu_col_cp1 <= PPU_COL;
  ppu_row_cp1 <= PPU_ROW;
  ppu_frame_cp1 <= PPU_FRAME;
end

//``REGION_REGS EMUBUS_CTRL
//``REG ICD_MATCH_REG_D0
always @* begin
  state0[23:0] = CPU_A;
  state0[31:24] = CPU_DB;
end
//``REG_END
//``REG ICD_MATCH_REG_D1
always @* begin
  state1[0]     = CPU_RW;
  state1[1]     = CPU_SYNC;
  state1[13:2]  = 12'b0;        //``NO_FIELD
  state1[22:14] = ppu_col_cp1;  //``FIELD PPU_COL
  state1[31:23] = ppu_row_cp1;  //``FIELD PPU_ROW
end
//``REG ICD_MATCH_REG_D2
always @* begin
  state2[31:0]  = ppu_frame_cp1; //``FIELD PPU_FRAME
end
//``REG_END

wire [95:0] state = {state2, state1, state0};

// Match state is valid near CP2_NEGEDGE.
wire state_valid = CP2_NEGEDGE;

generate;
genvar i;
  for (i = 0; i < NUM_MATCH; i = i + 1) begin :match

    initial begin
      match_status[i] = 0;
      match_din[i] = 0;
      match_den[i] = 0;
    end

    //``REG ICD_MATCH_REG_STATUS
    initial begin
      match_status[i][3:0] = i[3:0];         //``FIELD MATCH_SEL
      match_status[i][7:4] = NUM_MATCH[3:0]; //``FIELD NUM_MATCH
    end
    //``REG_END

    // state      x0101
    // match_din  x0011
    // match_den  01111
    // ---------  -----
    // match_out  11001

    assign match_out[i] = ~((state ^ match_din[i]) & match_den[i]);

    always @(posedge CLK) begin
      if (!nRES || !MATCH_ENABLE[i]) begin
        MATCH_TRIGGER[i] <= 1'b0;
        match_dout[i] <= 128'b0;
      end
      else if (state_valid) begin
        if (!MATCH_TRIGGER[i] && CPU_RDY && &match_out[i] && !HOLD) begin
          MATCH_TRIGGER[i] <= 1'b1;
          match_dout[i][95:00] <= state;
          match_dout[i][127:96] <= CPU_CYC;
        end
      end
    end

    always @(posedge CLK) begin
      if (MATCH_SEL == i) begin
        casez (MATCH_REG_SEL)
          4'h2: begin
            if (MATCH_REG_WSTRB[0]) match_din[i][07:00] <= MATCH_REG_DIN[07:00];
            if (MATCH_REG_WSTRB[1]) match_din[i][15:08] <= MATCH_REG_DIN[15:08];
            if (MATCH_REG_WSTRB[2]) match_din[i][23:16] <= MATCH_REG_DIN[23:16];
            if (MATCH_REG_WSTRB[3]) match_din[i][31:24] <= MATCH_REG_DIN[31:24];
          end
          4'h3: begin
            if (MATCH_REG_WSTRB[0]) match_din[i][39:32] <= MATCH_REG_DIN[07:00];
            if (MATCH_REG_WSTRB[1]) match_din[i][47:40] <= MATCH_REG_DIN[15:08];
            if (MATCH_REG_WSTRB[2]) match_din[i][55:48] <= MATCH_REG_DIN[23:16];
            if (MATCH_REG_WSTRB[3]) match_din[i][63:56] <= MATCH_REG_DIN[31:24];
          end
          4'h4: begin
            if (MATCH_REG_WSTRB[0]) match_din[i][71:64] <= MATCH_REG_DIN[07:00];
            if (MATCH_REG_WSTRB[1]) match_din[i][79:72] <= MATCH_REG_DIN[15:08];
            if (MATCH_REG_WSTRB[2]) match_din[i][87:80] <= MATCH_REG_DIN[23:16];
            if (MATCH_REG_WSTRB[3]) match_din[i][95:88] <= MATCH_REG_DIN[31:24];
          end
          4'h6: begin
            if (MATCH_REG_WSTRB[0]) match_den[i][07:00] <= MATCH_REG_DIN[07:00];
            if (MATCH_REG_WSTRB[1]) match_den[i][15:08] <= MATCH_REG_DIN[15:08];
            if (MATCH_REG_WSTRB[2]) match_den[i][23:16] <= MATCH_REG_DIN[23:16];
            if (MATCH_REG_WSTRB[3]) match_den[i][31:24] <= MATCH_REG_DIN[31:24];
          end
          4'h7: begin
            if (MATCH_REG_WSTRB[0]) match_den[i][39:32] <= MATCH_REG_DIN[07:00];
            if (MATCH_REG_WSTRB[1]) match_den[i][47:40] <= MATCH_REG_DIN[15:08];
            if (MATCH_REG_WSTRB[2]) match_den[i][55:48] <= MATCH_REG_DIN[23:16];
            if (MATCH_REG_WSTRB[3]) match_den[i][63:56] <= MATCH_REG_DIN[31:24];
          end
          4'h8: begin
            if (MATCH_REG_WSTRB[0]) match_den[i][71:64] <= MATCH_REG_DIN[07:00];
            if (MATCH_REG_WSTRB[1]) match_den[i][79:72] <= MATCH_REG_DIN[15:08];
            if (MATCH_REG_WSTRB[2]) match_den[i][87:80] <= MATCH_REG_DIN[23:16];
            if (MATCH_REG_WSTRB[3]) match_den[i][95:88] <= MATCH_REG_DIN[31:24];
          end
          default: ;
        endcase
      end
    end

    always @* begin
      match_rdata[i] = 32'h00000000;
      casez (MATCH_REG_SEL)                          //``SUBREGS ICD
        4'h0: match_rdata[i] = match_status[i];      //``REG STATUS
        4'h2: match_rdata[i] = match_din[i][31:00];  //``REG D0IN
        4'h3: match_rdata[i] = match_din[i][63:32];  //``REG D1IN
        4'h4: match_rdata[i] = match_din[i][95:64];  //``REG D2IN
        4'h6: match_rdata[i] = match_den[i][31:00];  //``REG D0EN
        4'h7: match_rdata[i] = match_den[i][63:32];  //``REG D1EN
        4'h8: match_rdata[i] = match_den[i][95:64];  //``REG D2EN
        4'hA: match_rdata[i] = match_dout[i][31:00]; //``REG D0OUT
        4'hB: match_rdata[i] = match_dout[i][63:32]; //``REG D1OUT
        4'hC: match_rdata[i] = match_dout[i][95:64]; //``REG D2OUT
        4'hD: match_rdata[i] = match_dout[i][127:96]; //``REG D3OUT
        default: ;
      endcase
    end

  end
endgenerate;

assign MATCH_REG_DOUT = match_rdata[MATCH_SEL];

initial SYS_HOLD = 1'b0;

always @(posedge CLK) begin
  resume_dly <= SYS_RESUME;

  if (!nRES || !SYS_ENABLE)
    SYS_HOLD <= 1'b0;
  else begin
    if (SYS_FORCE_HALT)
      SYS_HOLD <= 1'b1;
    else if (!resume_dly && SYS_RESUME)
      SYS_HOLD <= 1'b0;
    else if (MATCH_TRIGGER)
      SYS_HOLD <= 1'b1;
  end
end

endmodule
