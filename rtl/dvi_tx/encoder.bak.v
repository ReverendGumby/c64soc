module encoder
  (
   input            clk,

   input [7:0]      D,
   input [1:0]      C,
   input            DE,

   output reg [9:0] q_out
   );

//////////////////////////////////////////////////////////////////////
// Phase 1: Sample inputs.

reg [7:0] d_p1;
reg [1:0] c_p1;
reg       de_p1;

// Sample and hold inputs on pclk_posedge.
always @(posedge pclk) begin
    d_p1 <= D;
    c_p1 <= C;
    de_p1 <= DE;
  end
end

//////////////////////////////////////////////////////////////////////
// Phase 2: Count the ones in D.

reg [7:0] d_p2[-1:7];
reg [5:0] d_cnt_p2[-1:7];       // N1(D), one-hot: { 0, 1,... 4, 5+ }

task [5:0] inc_cnt
  (
   output reg [5:0] cnt1,
   input [5:0] cnt0,
   input d
   );
  begin
    if (d && !cnt0[5])
      cnt1 <= cnt0 << 1;
    else
      cnt1 <= cnt0;
  end
endtask

always @* begin
  d_cnt_p2[-1] <= 5'b00001;
  d_p2[-1] <= d_p1;
  c_p2[-1] <= c_p1;
  de_p2[-1] <= de_p1;
end

genvar integer i;
generate
  for (i = 0; i <= 7; i = i + 1) begin
    always @(posedge pclk) begin
      inc_cnt(d_cnt_p2[i], d_cnt_p2[i-1], d_p2[i]);
      d_p2[i] <= d_p2[i-1];
      c_p2[i] <= c_p2[i-1];
      de_p2[i] <= de_p2[i-1];
    end
  end
endgenerate

reg [5:0] d_cnt;
reg [7:0] d_p2o;
reg [1:0] c_p2o;
reg       de_p2o;

always @* begin
  d_cnt <= d_cnt_p2[7];
  d_p2o <= d_p2[7];
  c_p2o <= c_p2[7];
  de_p2o <= de_p2[7];
end

//////////////////////////////////////////////////////////////////////
// Phase 3: Minimize transitions.

reg [8:0] q_m_p3[0:8];
reg [5:0] q_m_cnt_p3[0:7];      // N1(q_m), one-hot
reg [1:0] c_p3[-1:8];
reg       de_p3[-1:8];

reg [9:0] q_spec;

always @(posedge clk) begin
  // q_m[8]: indicates XNOR versus XOR.
  q_m_p3[0][8] <= d_cnt[5] || d_cnt[4] && d_p2o[0] == 1'b0;
  q_m_p3[0][7:0] <= d_p2o[0];
end

always @* begin
  q_m_cnt_p3[0] <= 5'b00001 << q_m_p3[0][8];
  c_p3[-1] <= c_p2o;
  de_p3[-1] <= de_p2o;
end

genvar integer j;
generate
  for (j = 1; j <= 7; j = j + 1) begin
    always @(posedge pclk) begin
      q_m_p3[j][j-1:0] <= q_m_p3[j-1][j-1:0];
      // If q_m[8] is 1: q_m[N] = q_m[N-1] XOR D[N]
      // If q_m[8] is 0: q_m[N] = q_m[N-1] XNOR D[N]
      q_m_p3[j][j] <= q_m_p3[j-1][j-1] ^ q_m_p3[j-1][j] ^ ~q_m_p3[j-1][8];
      q_m_p3[j][8:j+1] <= q_m_p3[j-1][8:j+1];
    end
  end

  // count q_m[7:0] in the next clk after its computed
  for (j = 1; j <= 8; j = j + 1) begin
    always @(posedge pclk) begin
      inc_cnt(q_m_cnt_p3[j], q_m_cnt_p3[j-1], q_m_p3[j][j-1]);
    end
  end

  for (j = 0; j <= 8; j = j + 1) begin
    always @(posedge clk) begin
      c_p3[j] <= c_p3[j-1];
      de_p3[j] <= de_p3[j-1];
    end
  end
endgenerate

always @(posedge clk) begin
  q_m_p3[8] <= q_m_p3[7];
end

reg [8:0] q_m_p3o;
reg [5:0] q_m_cnt;
reg [1:0] c_p3o;
reg       de_p3o;

always @* begin
  q_m_p3o <= q_m_p3[8];
  q_m_cnt <= q_m_cnt_p3[8];
  c_p3o <= c_p3[8];
  de_p3o <= de_p3[8];
end

//////////////////////////////////////////////////////////////////////
// Phase 4: Balance DC offset for final serialized output.

`define CNTM 15
reg [`CNTM:-`CNTM] cnt, cnt_next; // N1(q) - N0(q), one-hot
reg [8:0]          q_m_cnt;       // N1(q_m[7:0]), one-hot
reg                q_inv;         // invert q[7:0]
reg                q_next;

// Count 8 bits at q_m input on bcnt[1:8]. By the time we reach bcnt[9],
// q_m_cnt will be valid.
always @(posedge bclk) begin
  if (bcnt[1])
    q_m_cnt <= { q_m[9], ~q_m[9] }; // reset
  else if (|bcnt[8:2] && q_m[9])
    q_m_cnt <= q_m_cnt << 1;
end

// Determine whether we should invert q_m or not, based on how many 1s
// it has and how many 1s we've sent cumulatively. (q[9] is included
// in the cumulative count courtesy of cnt_next.)
always @(posedge bclk) begin
  if (bcnt[9]) begin
    // cnt_next is used because we need the 9th bit added in.
    if (cnt_next[0] || q_m_cnt[4])
      q_inv <= q_m_inv;         // q[9] = ~q_m[8]
    else if ((|cnt_next[`CNTM:1] && |q_m_cnt[8:5]) ||
             (|cnt_next[-1:-`CNTM] && |q_m_cnt[3:0]))
      q_inv <= 1'b1;
    else
      q_inv <= 1'b0;
  end
end

// q: Final serialized output.
always @(posedge bclk) begin
  q <= q_next;
end

always @* begin
  if (!de_p3) begin
    q_next <= q_spec[0];
  end
  else begin
    if (|bcnt[7:0])
      // q_m[7:0] is passed verbatim or inverted.
      q_next <= q_m[0] ^ q_inv;
    else if (bcnt[8])
      q_next <= q_m[0];
    else if (bcnt[9])
      q_next <= q_inv;
    else
      q_next <= 1'bx;
  end
end

always @(posedge bclk) begin
  if (bcnt[9]) begin
    c0_p3 <= c0_p2;
    c1_p3 <= c1_p2;
    de_p3 <= de_p2;

    // If !DE, special words are output in phase 4.
    case ({c1_p2, c0_p2})
      2'b00: q_spec <= 10'b1101010100;
      2'b01: q_spec <= 10'b0010101011;
      2'b10: q_spec <= 10'b0101010100;
      2'b11: q_spec <= 10'b1010101011;
      default: q_spec <= 10'bxxxxxxxxxx;
    endcase
  end
  else begin
    q_spec <= q_spec >> 1;
  end
end

//////////////////////////////////////////////////////////////////////
// Phase 5: Compute the output DC offset as feedback for phase 4.

always @* begin
  if (!de_p3)
    // reset: cnt_next = 0; cnt_next[0] = 1
    cnt_next <= { {`CNTM{1'b0}}, 1'b1, {`CNTM{1'b0}} };
  else if (q_next)
    cnt_next <= cnt << 1;
  else
    cnt_next <= cnt >> 1;
end

always @(posedge bclk) begin
  cnt <= cnt_next;
end

/***********************************************************************
 * Here's the DVI spec. encode algorithm.

always @(posedge clk) begin
  q_out = 10'bxxxxxxxxxx;

  if (DE) begin
    if (cnt == 0 || N1(q_m[7:0]) == N0(q_m[7:0])) begin
      q_out[9] = ~q_m[8];
      q_out[8] = q_m[8];
      q_out[7:0] = q_m[8] ? q_m[7:0] : ~q_m[7:0];
      if (q_m[8] == 1'b0)
        cnt = cnt + (N0(q_m[7:0]) - N1(q_m[7:0]));
      else
        cnt = cnt + (N1(q_m[7:0]) - N0(q_m[7:0]));
    end
    else if ((cnt > 0 && N1(q_m[7:0]) > N0(q_m[7:0])) ||
             (cnt < 0 && N0(q_m[7:0]) > N1(q_m[7:0]))) begin
      q_out[9] = 1'b1;
      q_out[8] = q_m[8];
      q_out[7:0] = ~q_m[7:0];
      cnt = cnt + 2 * q_m[8] + (N0(q_m[7:0]) - N1(q_m[7:0]));
    end
    else begin
      q_out[9] = 1'b0;
      q_out[8] = q_m[8];
      q_out[7:0] = q_m[7:0];
      cnt = cnt - 2 * $unsigned(~q_m[8]) + (N1(q_m[7:0]) - N0(q_m[7:0]));
    end
  end
  else begin
    cnt = 0;
    case ({C1, C0})
      2'b00: q_out = 10'b1101010100;
      2'b01: q_out = 10'b0010101011;
      2'b10: q_out = 10'b0101010100;
      2'b11: q_out = 10'b1010101011;
    endcase
  end
end
**********************************************************************/

endmodule
