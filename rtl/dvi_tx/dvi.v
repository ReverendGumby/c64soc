module dvi
  (
   input        PCLK,
   input        RESET,

   input [7:0]  R,
   input [7:0]  G,
   input [7:0]  B,
   input        HS,
   input        VS,
   input        DE,

   // Output to TMDS serializer
   output [29:0] SEROUT
   );

wire [9:0] enc0_q, enc1_q, enc2_q;

encoder enc0
  (
   .CLK(PCLK),

   .D(B),
   .C0(HS),
   .C1(VS),
   .DE(DE),

   .Q_OUT(enc0_q)
   );

encoder enc1
  (
   .CLK(PCLK),

   .D(G),
   .C0(1'b0),
   .C1(1'b0),
   .DE(DE),

   .Q_OUT(enc1_q)
   );

encoder enc2
  (
   .CLK(PCLK),

   .D(R),
   .C0(1'b0),
   .C1(1'b0),
   .DE(DE),

   .Q_OUT(enc2_q)
   );

assign SEROUT = {
                 enc2_q[9], enc1_q[9], enc0_q[9],
                 enc2_q[8], enc1_q[8], enc0_q[8],
                 enc2_q[7], enc1_q[7], enc0_q[7],
                 enc2_q[6], enc1_q[6], enc0_q[6],
                 enc2_q[5], enc1_q[5], enc0_q[5],
                 enc2_q[4], enc1_q[4], enc0_q[4],
                 enc2_q[3], enc1_q[3], enc0_q[3],
                 enc2_q[2], enc1_q[2], enc0_q[2],
                 enc2_q[1], enc1_q[1], enc0_q[1],
                 enc2_q[0], enc1_q[0], enc0_q[0]
                 };

endmodule
