`timescale 1ns / 1ps

module dvi_tb
  (
   );

reg clk;
initial clk = 1'b0;
always #20 begin :clkgen
  clk = !clk;
end

reg reset;
initial begin
  reset = 1'b1;
  #2400 reset = 1'b0;
end

reg [0:30] pel;

integer f;
integer ret;
initial begin
  f = $fopen("../../../../../rtl/dvi_tx/tb/pels.txt", "r");
  if (!f)
    f = $fopen("../../../../../../rtl/dvi_tx/tb/pels.txt", "r");
  if (!f)
    $display("open error");
  else begin
    @(negedge reset) ;
    pel = 0;
    while (1) begin
      if ($feof(f))
        $stop;
      @(negedge clk) ;
      ret = $fscanf(f, "%31b\n", pel);
      if (ret != 1)
        $stop;
    end
  end
end

wire       de = pel[0];
wire [7:0] r = pel[1:8];
wire       hs = pel[9];
wire       vs = pel[10];
wire [7:0] g = pel[11:18];
wire [7:0] b = pel[21:28];

wire [29:0] q_sim;
wire [2:0] qp, qn;

dvi dvi
  (
   .CLK(clk),
   .RESET(reset),

   .R(r),
   .G(g),
   .B(b),
   .HS(hs),
   .VS(vs),
   .DE(de),

   .QP(qp),
   .QN(qn)
   );

dvi_sim dvi_sim
  (
   .CLK(clk),

   .R(r),
   .G(g),
   .B(b),
   .HS(hs),
   .VS(vs),
   .DE(de),

   .Q_OUT(q_sim)
   );

`define ENC_DELAY 18
reg [29:0] qdly [0:`ENC_DELAY];
integer   i;
always @(posedge clk) begin
  qdly[0] <= q_sim;
  for (i = 1; i <= `ENC_DELAY; i = i + 1)
    qdly[i] <= qdly[i-1];
end
wire [29:0] q_sim_delay[2:0];
assign q_sim_delay[2] = qdly[`ENC_DELAY][29:20];
assign q_sim_delay[1] = qdly[`ENC_DELAY][19:10];
assign q_sim_delay[0] = qdly[`ENC_DELAY][09:00];

`define SER_DELAY 8
reg [9+`SER_DELAY:0] sreg[0:2];
always @(posedge clk) begin
  for (i = 0; i <= 2; i = i + 1)
    sreg[i][9+`SER_DELAY:0+`SER_DELAY] <= q_sim_delay[i];
end
always #4 begin
  for (i = 0; i <= 2; i = i + 1)
    sreg[i] <= sreg[i] >> 1;
end
wire [2:0] qp_sim = {sreg[2][0], sreg[1][0], sreg[0][0]};

reg compare_start;
initial begin
  compare_start = 1'b0;
  @(negedge reset) ;
  #1000 compare_start = 1'b1;
end

always #4 begin
  if (compare_start && qp_sim != qp)
    $stop;
end

endmodule
