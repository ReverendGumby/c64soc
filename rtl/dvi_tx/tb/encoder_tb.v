`timescale 1ns / 1ps

module encoder_tb
  (
   );

reg clk;
initial clk = 1'b0;
always #20 begin :clkgen
  clk = !clk;
end

reg [0:30] pel;

integer f;
integer ret;
initial begin
  f = $fopen("../../../../../rtl/dvi_tx/tb/pels.txt", "r");
  if (!f)
    f = $fopen("../../../../../../rtl/dvi_tx/tb/pels.txt", "r");
  if (!f)
    $display("open error");
  else begin
    #100 ;                      // FPGA startup
    pel = 0;
    while (1) begin
      if ($feof(f))
        $stop;
      @(negedge clk) ;
      ret = $fscanf(f, "%31b\n", pel);
      if (ret != 1)
        $stop;
    end
  end
end

wire       de = pel[0];
wire [7:0] r = pel[1:8];
wire       hs = pel[9];
wire       vs = pel[10];

wire [9:0] q_sim;
wire [9:0] q;

encoder encoder
  (
   .CLK(clk),

   .D(r),
   .C0(hs),
   .C1(vs),
   .DE(de),

   .Q_OUT(q)
   );

encoder_sim encoder_sim
  (
   .CLK(clk),

   .D(r),
   .C0(hs),
   .C1(vs),
   .DE(de),

   .Q_OUT(q_sim)
   );

`define REAL_DELAY 18
reg [9:0] sreg [0:`REAL_DELAY];
integer   i;
always @(posedge clk) begin
  sreg[0] <= q_sim;
  for (i = 1; i <= `REAL_DELAY; i = i + 1)
    sreg[i] <= sreg[i-1];
end
wire [9:0] q_sim_delay = sreg[`REAL_DELAY];

reg compare_start;
initial compare_start = 1'b0;
always @(negedge clk) begin
  if (q_sim !== 1'bx && q !== 1'bx)
    compare_start = ~compare_start;
  if (compare_start && q_sim_delay != q)
    $stop;
end

endmodule
