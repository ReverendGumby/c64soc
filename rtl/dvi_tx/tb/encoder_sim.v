module encoder_sim
  (
   input            CLK,

   input [7:0]      D,
   input            C0,
   input            C1,
   input            DE,

   output reg [9:0] Q_OUT
   );

reg [9:0] q_m;
// cnt = { -8, -6, -4, -2, 0, 2, 4, 6, 8 }
integer   cnt;

function integer N1;
input [7:0]         D;
integer             n;
  begin
    N1 = 0;
    for (n = 0; n < 8; n = n + 1)
      if (D[n])
        N1 = N1 + 1;
  end
endfunction

function integer N0;
input [7:0] D;
  begin
    N0 = 8 - N1(D);
  end
endfunction

initial begin
  cnt = 0;
end

always @(posedge CLK) begin
  q_m = 9'bxxxxxxxxx;
  
  if (N1(D) > 4 || (N1(D) == 4 && D[0] == 1'b0)) begin
    q_m[0] = D[0];
    q_m[1] = ~(q_m[0] ^ D[1]);
    q_m[2] = ~(q_m[1] ^ D[2]);
    q_m[3] = ~(q_m[2] ^ D[3]);
    q_m[4] = ~(q_m[3] ^ D[4]);
    q_m[5] = ~(q_m[4] ^ D[5]);
    q_m[6] = ~(q_m[5] ^ D[6]);
    q_m[7] = ~(q_m[6] ^ D[7]);
    q_m[8] = ~1'b1;
  end
  else begin
    q_m[0] = D[0];
    q_m[1] = q_m[0] ^ D[1];
    q_m[2] = q_m[1] ^ D[2];
    q_m[3] = q_m[2] ^ D[3];
    q_m[4] = q_m[3] ^ D[4];
    q_m[5] = q_m[4] ^ D[5];
    q_m[6] = q_m[5] ^ D[6];
    q_m[7] = q_m[6] ^ D[7];
    q_m[8] = 1'b1;
  end

  Q_OUT = 10'bxxxxxxxxxx;

  if (DE) begin
    if (cnt == 0 || N1(q_m[7:0]) == N0(q_m[7:0])) begin
      Q_OUT[9] = ~q_m[8];
      Q_OUT[8] = q_m[8];
      Q_OUT[7:0] = q_m[8] ? q_m[7:0] : ~q_m[7:0];
      if (q_m[8] == 1'b0)
        cnt = cnt + (N0(q_m[7:0]) - N1(q_m[7:0]));
      else
        cnt = cnt + (N1(q_m[7:0]) - N0(q_m[7:0]));
    end
    else if ((cnt > 0 && N1(q_m[7:0]) > N0(q_m[7:0])) ||
             (cnt < 0 && N0(q_m[7:0]) > N1(q_m[7:0]))) begin
      Q_OUT[9] = 1'b1;
      Q_OUT[8] = q_m[8];
      Q_OUT[7:0] = ~q_m[7:0];
      cnt = cnt + 2 * q_m[8] + (N0(q_m[7:0]) - N1(q_m[7:0]));
    end
    else begin
      Q_OUT[9] = 1'b0;
      Q_OUT[8] = q_m[8];
      Q_OUT[7:0] = q_m[7:0];
      cnt = cnt - 2 * $unsigned(~q_m[8]) + (N1(q_m[7:0]) - N0(q_m[7:0]));
    end
  end
  else begin
    cnt = 0;
    case ({C1, C0})
      2'b00: Q_OUT = 10'b1101010100;
      2'b01: Q_OUT = 10'b0010101011;
      2'b10: Q_OUT = 10'b0101010100;
      2'b11: Q_OUT = 10'b1010101011;
    endcase
  end
end

endmodule
