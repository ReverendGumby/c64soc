module dvi_sim
  (
   input         CLK,
   input [7:0]   R,
   input [7:0]   G,
   input [7:0]   B,
   input         HS,
   input         VS,
   input         DE,
   output [29:0] Q_OUT
   );

encoder_sim enc0_sim
  (
   .CLK(CLK),

   .D(b),
   .C0(hs),
   .C1(vs),
   .DE(de),

   .Q_OUT(Q_OUT[9:0])
   );

encoder_sim enc1_sim
  (
   .CLK(CLK),

   .D(g),
   .C0(1'b0),
   .C1(1'b0),
   .DE(de),

   .Q_OUT(Q_OUT[19:10])
   );

encoder_sim enc2_sim
  (
   .CLK(CLK),

   .D(r),
   .C0(1'b0),
   .C1(1'b0),
   .DE(de),

   .Q_OUT(Q_OUT[29:20])
   );

endmodule
