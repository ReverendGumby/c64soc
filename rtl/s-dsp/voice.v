`timescale 1us / 1ns

/* verilator lint_off IMPLICIT */
/* verilator lint_off WIDTH */

module s_dsp_voice
  (
   input               CLK,
   input               nRES,
   input               CKEN,

   // debug monitor
   input [7:0]         MON_SEL,
   output reg          MON_ACK,
   input               MON_READY,
   output reg [31:0]   MON_DOUT,
   output reg          MON_VALID,
   input               MON_WS,
   input [31:0]        MON_DIN,

   // Global counter
   input [15:0]        GLB_CNT,

   // Global registers
   input [7:0]         DIR,
   input [7:0]         KON,
   input [7:0]         KOF,
   input [7:0]         NON,
   input [7:0]         PMON,

   // Noise input
   input signed [14:0] NOISE_SIN,

   // Voice pipeline control signals
   input [4:0]         SGLCNT,
   input [1:0]         CCCNT,
   input [11:0]        VPS,

   // Register file interface
   output [6:0]        RA,
   output              RRS,
   output              RWS,
   input [7:0]         RDO,
   output [7:0]        RDI,

   // Memory interface
   output [15:0]       MA,
   output              MRS,
   input [7:0]         MDO,

   // Sample output
   output              VML,
   output              VMR,
   output [15:0]       VOUTL,
   output [15:0]       VOUTR
   );

wire [11:0] ps = VPS;           // pipeline strobes

wire mon_write_voice0, mon_write_voice1, mon_write_voice3;


//////////////////////////////////////////////////////////////////////
// Register file access

reg [3:0]   ra;
reg         rrs, rws;
reg [7:0]   rdi;

wire        rg;                 // Read GAIN or ADSR2?
reg [7:0]   envx;
wire [7:0]  outx;

reg [2:0]   rvsel;

always @* begin
  case ({SGLCNT, CCCNT})
    {5'd17, 2'd2}, {5'd21, 2'd1}, {5'd21, 2'd2}, {5'd22, 2'd1},
    {5'd30, 2'd2}, {5'd31, 2'd1}, {5'd0,  2'd0}, {5'd1,  2'd0},
    {5'd2,  2'd0}: rvsel = 3'd0;

    {5'd20, 2'd2}, {5'd0,  2'd1}, {5'd0,  2'd2}, {5'd1,  2'd1},
    {5'd1,  2'd2}, {5'd2,  2'd1}, {5'd3,  2'd0}, {5'd4,  2'd0},
    {5'd5,  2'd0}: rvsel = 3'd1;

    {5'd31, 2'd2}, {5'd3,  2'd1}, {5'd3,  2'd2}, {5'd4,  2'd1},
    {5'd4,  2'd2}, {5'd5,  2'd1}, {5'd6,  2'd0}, {5'd7,  2'd0},
    {5'd8,  2'd0}: rvsel = 3'd2;

    {5'd2,  2'd2}, {5'd6,  2'd1}, {5'd6,  2'd2}, {5'd7,  2'd1},
    {5'd7,  2'd2}, {5'd8,  2'd1}, {5'd9,  2'd0}, {5'd10, 2'd0},
    {5'd11, 2'd0}: rvsel = 3'd3;

    {5'd5,  2'd2}, {5'd9,  2'd1}, {5'd9,  2'd2}, {5'd10, 2'd1},
    {5'd10, 2'd2}, {5'd11, 2'd1}, {5'd12, 2'd0}, {5'd13, 2'd0},
    {5'd14, 2'd0}: rvsel = 3'd4;

    {5'd8,  2'd2}, {5'd12, 2'd1}, {5'd12, 2'd2}, {5'd13, 2'd1},
    {5'd13, 2'd2}, {5'd14, 2'd1}, {5'd15, 2'd0}, {5'd16, 2'd0},
    {5'd17, 2'd0}: rvsel = 3'd5;

    {5'd11, 2'd2}, {5'd15, 2'd1}, {5'd15, 2'd2}, {5'd16, 2'd1},
    {5'd16, 2'd2}, {5'd17, 2'd1}, {5'd18, 2'd0}, {5'd19, 2'd0},
    {5'd20, 2'd0}: rvsel = 3'd6;

    {5'd14, 2'd2}, {5'd18, 2'd1}, {5'd18, 2'd2}, {5'd19, 2'd1},
    {5'd19, 2'd2}, {5'd20, 2'd1}, {5'd21, 2'd0}, {5'd22, 2'd0},
    {5'd23, 2'd0}: rvsel = 3'd7;

    default: rvsel = 3'dx;
  endcase
end

always @* begin
  rrs = 1'b0;
  rws = 1'b0;
  ra = 4'hx;
  rdi = 8'hx;

  if (nRES) begin
    rrs = 1'b1;

    if (ps[0])            ra = 4'h4; // V0SRCN (but one ps[0] early)
    else if (ps[2])       ra = 4'h2; // V0PITCHL
    else if (ps[3])       ra = 4'h5; // V0ADSR1
    else if (ps[4])       ra = 4'h3; // V0PITCHH
    else if (ps[7] & ~rg) ra = 4'h6; // V0ADSR2
    else if (ps[7] &  rg) ra = 4'h7; // V0GAIN
    else if (ps[8])       ra = 4'h0; // V0VOLL
    else if (ps[9])       ra = 4'h1; // V0VOLR
    else begin
      rrs = 1'b0;
      if (ps[10]) begin
        ra = 4'h8;              // V0ENVX
        rws = 1'b1;
        rdi = envx;
      end
      else if (ps[11]) begin
        ra = 4'h9;              // V0OUTX
        rws = 1'b1;
        rdi = outx;
      end
    end
  end
end

assign RA = {rvsel, ra};
assign RRS = rrs;
assign RWS = rws;
assign RDI = rdi;

reg [7:0]   srcn, srcnd;
reg [13:0]  pitch;
reg [7:0]   adsr1, adsr2_gain, voll, volr;

integer     i;

initial begin
  pitch = 0;
end

always @(posedge CLK) begin
  if (~nRES) begin
    pitch <= 0;
  end
  else if (CKEN) begin
    if (ps[0]) begin
      srcn <= RDO;
      srcnd <= srcn;
    end
    if (ps[2])
      pitch[7:0] <= RDO;
    if (ps[3])
      adsr1 <= RDO;
    if (ps[4])
      pitch[13:8] <= RDO[5:0];
    if (ps[7])
      adsr2_gain <= RDO;
    if (ps[8])
      voll <= RDO;
    if (ps[9])
      volr <= RDO;
  end

  if (mon_write_voice0) begin
    srcn <= MON_DIN[7:0];
    srcnd <= MON_DIN[15:8];
    pitch <= MON_DIN[29:16];
  end
  if (mon_write_voice1) begin
    adsr1 <= MON_DIN[7:0];
    adsr2_gain <= MON_DIN[15:8];
    voll <= MON_DIN[23:16];
    volr <= MON_DIN[31:24];
  end
end

assign rg = ~adsr1[7];


//////////////////////////////////////////////////////////////////////
// Memory access

wire [15:0] brr_ma;
reg [15:0]  ma;
wire        brr_mrs;
reg         mrs;

always @* begin
  mrs = 1'b0;
  ma = 16'hx;

  if (brr_mrs) begin
    mrs = brr_mrs;
    ma = brr_ma;
  end
end

assign MA = ma;
assign MRS = mrs;


//////////////////////////////////////////////////////////////////////
// BRR decoder

reg [2:0] brr_vsel;
wire rd_srcnl, rd_srcnh, rd_hdr, rd_dta0, rd_dta1;
wire brr_play, brr_bend;
wire signed [14:0] brr_sout, brr_soutmx;
wire [1:0]         brr_soutmx_rs;
wire               brr_out_reset, brr_out_full;

wire [31:0]        mon_dout_brrdec;
wire               mon_ack_brrdec;
wire               mon_valid_brrdec;

always @* begin
  casez ({SGLCNT, CCCNT})
    {5'd0,  2'd1}, {5'd0,  2'd2},
    {5'd1,  2'dz}, {5'd2,  2'dz}, {5'd3,  2'd0}: brr_vsel = 3'd1;
    {5'd3,  2'd1}, {5'd3,  2'd2},
    {5'd4,  2'dz}, {5'd5,  2'dz}, {5'd6,  2'd0}: brr_vsel = 3'd2;
    {5'd6,  2'd1}, {5'd6,  2'd2},
    {5'd7,  2'dz}, {5'd8,  2'dz}, {5'd9,  2'd0}: brr_vsel = 3'd3;
    {5'd9,  2'd1}, {5'd9,  2'd2},
    {5'd10, 2'dz}, {5'd11, 2'dz}, {5'd12, 2'd0}: brr_vsel = 3'd4;
    {5'd12, 2'd1}, {5'd12, 2'd2},
    {5'd13, 2'dz}, {5'd14, 2'dz}, {5'd15, 2'd0}: brr_vsel = 3'd5;
    {5'd15, 2'd1}, {5'd15, 2'd2},
    {5'd16, 2'dz}, {5'd17, 2'dz}, {5'd18, 2'd0}: brr_vsel = 3'd6;
    {5'd18, 2'd1}, {5'd18, 2'd2},
    {5'd19, 2'dz}, {5'd20, 2'dz}, {5'd21, 2'd0}: brr_vsel = 3'd7;
    default: brr_vsel = 3'd0;
  endcase
end

assign rd_srcnl = ps[2];
assign rd_srcnh = ps[3];
assign rd_hdr = ps[5];
assign rd_dta0 = ps[6];
assign rd_dta1 = ps[8];

s_dsp_brrdec brrdec
  (
   .CLK(CLK),
   .nRES(nRES),
   .CKEN(CKEN),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_brrdec),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_brrdec),
   .MON_VALID(mon_valid_brrdec),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .KON(KON),
   .DIR(DIR),

   .VSEL(brr_vsel),

   .SRCN(srcnd),

   .MA(brr_ma),
   .MRS(brr_mrs),
   .MDO(MDO),

   .RD_SRCNL(rd_srcnl),
   .RD_SRCNH(rd_srcnh),
   .RD_HDR(rd_hdr),
   .RD_DTA0(rd_dta0),
   .RD_DTA1(rd_dta1),

   .BEND(brr_bend),

   .OUT_RESET(brr_out_reset),
   .OUT_FULL(brr_out_full),
   .OUT_PLAY(brr_play),
   .OUT_NEXT(brr_out_next),
   .OUT_SAMP(brr_sout),
   .OUT_SAMPMX_RS(brr_soutmx_rs),
   .OUT_SAMPMX(brr_soutmx)
   );


//////////////////////////////////////////////////////////////////////
// Decoded sample ring buffer

wire [2:0]         interp_in_next;
wire               interp_play;
reg [2:0]          interp_vsel;
wire signed [14:0] decbuf_out_samp;
wire [3:0]         decbuf_out_samp_rs_int;

wire [31:0]        mon_dout_decbuf;
wire               mon_ack_decbuf;
wire               mon_valid_decbuf;

s_dsp_decbuf decbuf
  (
   .CLK(CLK),
   .nRES(nRES),
   .CKEN(CKEN),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_decbuf),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_decbuf),
   .MON_VALID(mon_valid_decbuf),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .IN_RESET(brr_out_reset),
   .IN_FULL(brr_out_full),
   .IN_PLAY(brr_play),
   .IN_NEXT(brr_out_next),
   .IN_VSEL(brr_vsel),
   .IN_SAMP(brr_sout),

   .OUT_PLAY(interp_play),
   .OUT_NEXT(interp_in_next),
   .OUT_VSEL(interp_vsel),
   .OUT_SAMP_RS_DEC(brr_soutmx_rs),
   .OUT_SAMP_RS_INT(decbuf_out_samp_rs_int),
   .OUT_SAMP(decbuf_out_samp)
   );

assign brr_soutmx = decbuf_out_samp;


//////////////////////////////////////////////////////////////////////
// Gaussian interpolator

wire signed [14:0] interp_voutm1;
wire signed [14:0] interp_sout;
wire               interp_go;

wire [31:0]        mon_dout_interp;
wire               mon_ack_interp;
wire               mon_valid_interp;

always @* begin
  casez ({SGLCNT, CCCNT})
    {5'd1,  2'd2}, {5'd2,  2'dz}, {5'd3,  2'dz},
    {5'd4,  2'd0}, {5'd4,  2'd1}: interp_vsel = 3'd1;
    {5'd4,  2'd2}, {5'd5,  2'dz}, {5'd6,  2'dz},
    {5'd7,  2'd0}, {5'd7,  2'd1}: interp_vsel = 3'd2;
    {5'd7,  2'd2}, {5'd8,  2'dz}, {5'd9,  2'dz},
    {5'd10, 2'd0}, {5'd10, 2'd1}: interp_vsel = 3'd3;
    {5'd10, 2'd2}, {5'd11, 2'dz}, {5'd12, 2'dz},
    {5'd13, 2'd0}, {5'd13, 2'd1}: interp_vsel = 3'd4;
    {5'd13, 2'd2}, {5'd14, 2'dz}, {5'd15, 2'dz},
    {5'd16, 2'd0}, {5'd16, 2'd1}: interp_vsel = 3'd5;
    {5'd16, 2'd2}, {5'd17, 2'dz}, {5'd18, 2'dz},
    {5'd19, 2'd0}, {5'd19, 2'd1}: interp_vsel = 3'd6;
    {5'd19, 2'd2}, {5'd20, 2'dz}, {5'd21, 2'dz},
    {5'd22, 2'd0}, {5'd22, 2'd1}: interp_vsel = 3'd7;
    default: interp_vsel = 3'd0;
  endcase
end

assign interp_adj_pitch = ps[7];
assign interp_go = ps[8];

s_dsp_interp interp
  (
   .CLK(CLK),
   .nRES(nRES),
   .CKEN(CKEN),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_interp),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_interp),
   .MON_VALID(mon_valid_interp),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .PMON(PMON),
   .NON(NON),

   .VSEL(interp_vsel),

   .PITCH(pitch),
   .PLAY(interp_play),
   .ADJ_PITCH(interp_adj_pitch),
   .GO(interp_go),

   .IN_NEXT(interp_in_next),
   .IN_SAMP_RS(decbuf_out_samp_rs_int),
   .IN_SAMP(decbuf_out_samp),

   .VOUTM1(interp_voutm1),
   .OUT(interp_sout)
   );


//////////////////////////////////////////////////////////////////////
// Envelope generator

reg [2:0]   env_vsel;
wire [10:0] env_out;
wire        env_update;

wire [31:0] mon_dout_env;
wire        mon_ack_env;
wire        mon_valid_env;

always @* begin
  casez (SGLCNT)
    5'd0,  5'd1,  5'd2:     env_vsel = 3'd1;
    5'd3,  5'd4,  5'd5:     env_vsel = 3'd2;
    5'd6,  5'd7,  5'd8:     env_vsel = 3'd3;
    5'd9,  5'd10, 5'd11:    env_vsel = 3'd4;
    5'd12, 5'd13, 5'd14:    env_vsel = 3'd5;
    5'd15, 5'd16, 5'd17:    env_vsel = 3'd6;
    5'd18, 5'd19, 5'd20:    env_vsel = 3'd7;
    default: env_vsel = 3'd0;
  endcase
end

assign env_update = ps[8];
assign env_write = ps[10];

s_dsp_env env
  (
   .CLK(CLK),
   .nRES(nRES),
   .CKEN(CKEN),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_env),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_env),
   .MON_VALID(mon_valid_env),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .GLB_CNT(GLB_CNT),

   .KON(KON),
   .KOF(KOF),

   .VSEL(env_vsel),

   .ADSR1(adsr1),
   .ADSR2_GAIN(adsr2_gain),

   .PLAY(brr_play),
   .BEND(brr_bend),
   .UPDATE(env_update),

   .OUT(env_out)
   );

// ENVX is needed after env has advanced to the next voice.
always @(posedge CLK) begin
  if (CKEN) begin
    if (env_update)
      envx <= {1'b0, env_out[10:4]};
  end

  if (mon_write_voice3) begin
    envx <= MON_DIN[13:6];
  end
end


//////////////////////////////////////////////////////////////////////
// Final envelope scale and pan

wire env_apply, voll_apply, volr_apply;
wire signed [14:0] vout;

wire [31:0]        mon_dout_sp;
wire               mon_ack_sp;
wire               mon_valid_sp;

assign sp_non = NON[interp_vsel];
assign env_apply = ps[7];
assign voll_apply = ps[9];
assign volr_apply = ps[10];

s_dsp_scale_pan scale_pan
  (
   .CLK(CLK),
   .nRES(nRES),
   .CKEN(CKEN),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_sp),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_sp),
   .MON_VALID(mon_valid_sp),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .NON(sp_non),
   .VOLL(voll),
   .VOLR(volr),

   .ENV_APPLY(env_apply),
   .VOLL_APPLY(voll_apply),
   .VOLR_APPLY(volr_apply),

   .BSIN(interp_sout),
   .NSIN(NOISE_SIN),
   .ENV(env_out),

   .VML(VML),
   .VMR(VMR),
   .VOUT(vout),
   .OUTL(VOUTL),
   .OUTR(VOUTR)
   );

assign interp_voutm1 = vout;
assign outx = vout[14:7];


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout, mon_dout_voice;
reg        mon_ack_voice, mon_valid_voice;
reg        mon_sel_voice0, mon_sel_voice1, mon_sel_voice3;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  mon_ack_voice = 1'b1;
  mon_sel_voice0 = 1'b0;
  mon_sel_voice1 = 1'b0;
  mon_sel_voice3 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'h4d: begin                //``REG VOICE0
      mon_sel_voice0 = 1'b1;
      mon_dout[7:0] = srcn;
      mon_dout[15:8] = srcnd;
      mon_dout[29:16] = pitch;
    end
    8'h4e: begin                //``REG VOICE1
      mon_sel_voice1 = 1'b1;
      mon_dout[7:0] = adsr1;
      mon_dout[15:8] = adsr2_gain;
      mon_dout[23:16] = voll;
      mon_dout[31:24] = volr;
    end
    8'h50: begin                //``REG VOICE3
      mon_sel_voice3 = 1'b1;
      mon_dout[13:6] = envx;
    end
    default: mon_ack_voice = 1'b0;
  endcase
end

assign mon_write = MON_READY & MON_WS & ~mon_valid_voice;
assign mon_write_voice0 = mon_write & mon_sel_voice0;
assign mon_write_voice1 = mon_write & mon_sel_voice1;
assign mon_write_voice3 = mon_write & mon_sel_voice3;

always @(posedge CLK) begin
  mon_valid_voice <= 1'b0;
  if (mon_ack_voice & MON_READY) begin
    mon_valid_voice <= 1'b1;
    mon_dout_voice <= mon_dout;
  end
end

always @* begin
  MON_ACK = 1'b1;
  if (mon_ack_brrdec) begin
    MON_VALID = mon_valid_brrdec;
    MON_DOUT = mon_dout_brrdec;
  end
  else if (mon_ack_decbuf) begin
    MON_VALID = mon_valid_decbuf;
    MON_DOUT = mon_dout_decbuf;
  end
  else if (mon_ack_interp) begin
    MON_VALID = mon_valid_interp;
    MON_DOUT = mon_dout_interp;
  end
  else if (mon_ack_env) begin
    MON_VALID = mon_valid_env;
    MON_DOUT = mon_dout_env;
  end
  else if (mon_ack_sp) begin
    MON_VALID = mon_valid_sp;
    MON_DOUT = mon_dout_sp;
  end
  else if (mon_ack_voice) begin
    MON_VALID = mon_valid_voice;
    MON_DOUT = mon_dout_voice;
  end
  else begin
    MON_ACK = 1'b0;
    MON_VALID = 1'b0;
    MON_DOUT = 32'b0;
  end
end


endmodule
