`timescale 1us / 1ns

module s_dsp_echo
  (
   input                CLK,
   input                nRES,
   input                CKEN,

   // debug monitor
   input [7:0]          MON_SEL,
   output reg           MON_ACK,
   input                MON_READY,
   output reg [31:0]    MON_DOUT,
   output reg           MON_VALID,
   input                MON_WS,
   input [31:0]         MON_DIN,

   // Global registers
   input                ECEN,

   // Echo pipeline control signals
   input [21:0]         EPS,
   input                CLR_STALLED,

   // Register file interface
   output [6:0]         RA,
   output               RRS,
   input [7:0]          RDO,

   // Memory interface
   output [15:0]        MA,
   output               MRS,
   output               MWS,
   output [7:0]         MDI,
   input [7:0]          MDO,

   // Sample input
   input                VML,
   input                VMR,
   input signed [15:0]  VOUTL,
   input signed [15:0]  VOUTR,

   // Sample output
   output               EML,
   output               EMR,
   output signed [15:0] EOUTL,
   output signed [15:0] EOUTR
   );

wire signed [15:0] moutl, moutr;
wire signed [14:0] finl, finr;
wire signed [15:0] foutl, foutr;

wire mon_write_echo0, mon_write_echo1, mon_write_echo2, mon_write_echo3;

//////////////////////////////////////////////////////////////////////
// Control signals

wire peboll, pebohl, pebolr, pebohr, pebill, pebihl, pebilr, pebihr,
     pefb, peon, pedl, pesa, pecen, pptr, pimax, pmgol, pmgor, pffc,
     psil, psir, pfgol, pfgor;

assign peboll = EPS[0];         // [RdEchoLeft.lsb]
assign pebohl = EPS[1];         // [RdEchoLeft.msb]
assign pebolr = EPS[2];         // [RdEchoRight.lsb]
assign pebohr = EPS[3];         // [RdEchoRight.msb]

assign pebill = EPS[4];         // [WrEchoLeft.lsb]
assign pebihl = EPS[5];         // [WrEchoLeft.msb]
assign pebilr = EPS[6];         // [WrEchoRight.lsb]
assign pebihr = EPS[7];         // [WrEchoRight.msb] (inc buf idx)

assign pefb   = EPS[8];         // load EFB
assign peon   = EPS[9];         // load EON
assign pedl   = EPS[10];        // load EDL
assign pesa   = EPS[11];        // load ESA
assign pecen  = EPS[12];        // load ECEN (FLG.6)

assign pptr   = EPS[13];        // apply ESA
assign pimax  = EPS[14];        // apply EDL
assign pmgol  = EPS[15];        // echo mix out left
assign pmgor  = EPS[16];        // echo mix out right

assign pffc   = EPS[17];        // FFCx
assign psil   = EPS[18];        // FIR in left
assign psir   = EPS[19];        // FIR int right
assign pfgol  = EPS[20];        // FIR out left
assign pfgor  = EPS[21];        // FIR out right


//////////////////////////////////////////////////////////////////////
// Register file access

reg [7:0]   ffc, efb, eon, esa;
reg [3:0]   edl;
reg [6:0]   ra;
reg         rrs;
reg         fcl;
reg [2:0]   fcrs, fcls;

initial begin
  efb = 8'd0;
  eon = 8'd0;
  edl = 4'd0;
  esa = 8'd0;
  fcrs = 3'd0;
  fcls = 3'd0;
  fcl = 1'b0;
end

always @* begin
  rrs = 1'b1;
  ra = 7'hx;

  if (pffc)             ra = {fcrs, 4'hF}; // FFC0-7
  else if (pefb)        ra = 7'h0D;
  else if (peon)        ra = 7'h4D;
  else if (pedl)        ra = 7'h7D;
  else if (pesa)        ra = 7'h6D;
  else
    rrs = 1'b0;
end

always @(posedge CLK) begin
  if (~nRES) begin
    efb <= 8'd0;
    eon <= 8'd0;
    edl <= 4'd0;
    esa <= 8'd0;
    fcrs <= 3'd0;
    fcls <= 3'd0;
    fcl <= 1'b0;
  end
  else if (CKEN) begin
    fcls <= fcrs;
    fcl <= pffc;
    if (pffc) begin
      ffc <= RDO;
      fcrs <= fcrs + 1'd1;
    end
    if (pefb)
      efb <= RDO;
    if (peon)
      eon <= RDO;
    if (pedl)
      edl <= RDO[3:0];
    if (pesa)
      esa <= RDO;
  end

  if (mon_write_echo0) begin
    efb <= MON_DIN[7:0];
    eon <= MON_DIN[15:8];
    edl <= MON_DIN[19:16];
    esa <= MON_DIN[31:24];
  end
  if (mon_write_echo1) begin
    fcrs <= MON_DIN[2:0];
    fcls <= MON_DIN[6:4];
    fcl <= MON_DIN[8];
  end
end

assign RA = ra;
assign RRS = rrs;


//////////////////////////////////////////////////////////////////////
// Mixer

wire signed [15:0] efbinl, efbinr;

wire [31:0]        mon_dout_mixer;
wire               mon_ack_mixer;
wire               mon_valid_mixer;

s_dsp_echo_mixer mixer
  (
   .CLK(CLK),
   .nRES(nRES),
   .CKEN(CKEN),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_mixer),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_mixer),
   .MON_VALID(mon_valid_mixer),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .EON(eon),
   .EFB(efb),

   .VML(VML),
   .VMR(VMR),
   .GOL(pmgol),
   .GOR(pmgor),

   .EFBINL(efbinl),
   .EFBINR(efbinr),
   .VOUTL(VOUTL),
   .VOUTR(VOUTR),

   .SOUTL(moutl),
   .SOUTR(moutr)
   );


//////////////////////////////////////////////////////////////////////
// FIR filter (stereo)

wire [31:0]        mon_dout_fir;
wire               mon_ack_fir;
wire               mon_valid_fir;

s_dsp_echo_fir fir
  (
   .CLK(CLK),
   .nRES(nRES),
   .CKEN(CKEN),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_fir),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_fir),
   .MON_VALID(mon_valid_fir),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .SIL(psil),
   .SIR(psir),
   .CL(fcl),
   .CS(fcls),

   .FFC(ffc),
   .SINL(finl),
   .SINR(finr),

   .SOUTL(foutl),
   .SOUTR(foutr)
   );

assign efbinl = foutl;
assign efbinr = foutr;
assign EOUTL = foutl;
assign EOUTR = foutr;
assign EML = pfgol;
assign EMR = pfgor;


//////////////////////////////////////////////////////////////////////
// Echo buffer

wire [15:0] ebinl, ebinr;
reg [15:2]  buf_ptr;
reg [12:0]  idx, idxp1, idx_max;
reg [15:0]  eboutl, eboutr;
reg [7:0]   buf_din;
wire [7:0]  mdo;
reg         ece;
wire        buf_side, buf_off;

initial begin
  idx = 13'd0;
  eboutl = 16'd0;
  eboutr = 16'd0;
  ece = 1'b0;
end

// Use CLR_STALLED to give the pipeline a chance to stabilize (read
// and apply ESA, etc.), in the likely case that the registers have
// changed during the stall.

assign mdo = CLR_STALLED ? 8'h0 : MDO;

always @(posedge CLK) begin
  if (~nRES) begin
    idx <= 13'd0;
  end
  else if (CKEN) begin
    if (pecen)
      ece <= ~(ECEN | CLR_STALLED);
    if (pimax) begin
      if (idx == 0 || CLR_STALLED)
        idx_max <= {edl, 9'b0};
    end
    if (pptr) begin
      buf_ptr <= {esa, 6'd0} + idx;
    end
    if (pebihr) begin
      if (idxp1 >= idx_max || CLR_STALLED)
        idx <= 13'd0;
      else
        idx <= idxp1;
    end
    if (peboll)
      eboutl[7:0] <= mdo;
    if (pebohl)
      eboutl[15:8] <= mdo;
    if (pebolr)
      eboutr[7:0] <= mdo;
    if (pebohr)
      eboutr[15:8] <= mdo;
  end

  if (mon_write_echo1) begin
    idx <= MON_DIN[21:9];
    ece <= MON_DIN[22];
  end
  if (mon_write_echo2) begin
    idx_max <= MON_DIN[12:0];
    buf_ptr[15:2] <= MON_DIN[26:13];
  end
  if (mon_write_echo3) begin
    eboutl <= MON_DIN[15:0];
    eboutr <= MON_DIN[31:16];
  end
end

always @*
  idxp1 = idx + 1'd1;

always @* begin
  buf_din = 8'hx;
  if (pebill)
    buf_din = ebinl[7:0];
  else if (pebihl)
    buf_din = ebinl[15:8];
  else if (pebilr)
    buf_din = ebinr[7:0];
  else if (pebihr)
    buf_din = ebinr[15:8];
end

assign buf_side = pebolr | pebohr | pebilr | pebihr;
assign buf_off = pebohl | pebohr | pebihl | pebihr;

assign ebinl = {moutl[15:1], 1'b0};
assign ebinr = {moutr[15:1], 1'b0};
assign finl = eboutl[15:1];
assign finr = eboutr[15:1];

assign MA = {buf_ptr, buf_side, buf_off};
assign MRS = peboll | pebohl | pebolr | pebohr;
assign MWS = ece & (pebill | pebihl | pebilr | pebihr);
assign MDI = buf_din;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout, mon_dout_echo;
reg        mon_ack_echo, mon_valid_echo;
reg        mon_sel_echo0, mon_sel_echo1, mon_sel_echo2, mon_sel_echo3;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  mon_ack_echo = 1'b1;
  mon_sel_echo0 = 1'b0;
  mon_sel_echo1 = 1'b0;
  mon_sel_echo2 = 1'b0;
  mon_sel_echo3 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'hd7: begin                //``REG ECHO0
      mon_sel_echo0 = 1'b1;
      mon_dout[7:0] = efb;
      mon_dout[15:8] = eon;
      mon_dout[19:16] = edl;
      mon_dout[31:24] = esa;
    end
    8'hd8: begin                //``REG ECHO1
      mon_sel_echo1 = 1'b1;
      mon_dout[2:0] = fcrs;
      mon_dout[6:4] = fcls;
      mon_dout[8] = fcl;
      mon_dout[21:9] = idx;
      mon_dout[22] = ece;
    end
    8'hd9: begin                //``REG ECHO2
      mon_sel_echo2 = 1'b1;
      mon_dout[12:0] = idx_max;
      mon_dout[26:13] = buf_ptr[15:2];
    end
    8'hda: begin                //``REG ECHO3
      mon_sel_echo3 = 1'b1;
      mon_dout[15:0] = eboutl;
      mon_dout[31:16] = eboutr;
    end
    default: mon_ack_echo = 1'b0;
  endcase
end

assign mon_write = MON_READY & MON_WS & ~mon_valid_echo;
assign mon_write_echo0 = mon_write & mon_sel_echo0;
assign mon_write_echo1 = mon_write & mon_sel_echo1;
assign mon_write_echo2 = mon_write & mon_sel_echo2;
assign mon_write_echo3 = mon_write & mon_sel_echo3;

always @(posedge CLK) begin
  mon_valid_echo <= 1'b0;
  if (mon_ack_echo & MON_READY) begin
    mon_valid_echo <= 1'b1;
    mon_dout_echo <= mon_dout;
  end
end

always @* begin
  MON_ACK = 1'b1;
  if (mon_ack_mixer) begin
    MON_VALID = mon_valid_mixer;
    MON_DOUT = mon_dout_mixer;
  end
  else if (mon_ack_fir) begin
    MON_VALID = mon_valid_fir;
    MON_DOUT = mon_dout_fir;
  end
  else if (mon_ack_echo) begin
    MON_VALID = mon_valid_echo;
    MON_DOUT = mon_dout_echo;
  end
  else begin
    MON_ACK = 1'b0;
    MON_VALID = 1'b0;
    MON_DOUT = 32'b0;
  end
end


endmodule
