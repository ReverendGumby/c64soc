`timescale 1us / 1ns

/* verilator lint_off WIDTH */

module s_dsp_interp
  (
   input                CLK,
   input                nRES,
   input                CKEN,

   // debug monitor
   input [7:0]          MON_SEL,
   output reg           MON_ACK,
   input                MON_READY,
   output reg [31:0]    MON_DOUT,
   output reg           MON_VALID,
   input                MON_WS,
   input [31:0]         MON_DIN,

   input [7:0]          PMON,
   input [7:0]          NON,

   input [2:0]          VSEL,

   input [13:0]         PITCH,
   input                PLAY,
   input                ADJ_PITCH,
   input                GO,

   output [2:0]         IN_NEXT,
   output [3:0]         IN_SAMP_RS,
   input signed [14:0]  IN_SAMP,

   input signed [14:0]  VOUTM1,
   output signed [14:0] OUT
   );

reg signed [16:0] smp, acc;
reg signed [25:0] mula, mulb, prd;
reg signed [15:0] csmp, sout;
reg [14:0]        pcnt;
wire [15:0]       pcnt_next;
reg [2:0]         next;         // how many new samples needed
reg [5:0]         gos;

wire              mon_write_interp0, mon_write_interp1, mon_write_interp2;

`include "math.vh"


//////////////////////////////////////////////////////////////////////
// Context store

localparam CTX_WIDTH = 15 + 16;

wire       ctx_swap;
wire [CTX_WIDTH-1:0] ctx_in, ctx_out;
wire [14:0]          ctx_pcnt;
wire signed [15:0]   ctx_sout;

wire [31:0]          mon_dout_ctx;
wire                 mon_ack_ctx;
wire                 mon_valid_ctx;

assign ctx_in = { pcnt, sout };
assign { ctx_pcnt, ctx_sout } = ctx_out;

s_dsp_context_mem #(.WIDTH(CTX_WIDTH), .MON_BASE(8'hbf)) ctx
  (
   .CLK(CLK), .nRES(nRES), .CKEN(CKEN),
   .MON_SEL(MON_SEL), .MON_ACK(mon_ack_ctx), .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_ctx), .MON_VALID(mon_valid_ctx),
   .MON_WS(MON_WS), .MON_DIN(MON_DIN),
   .VSEL(VSEL),
   .SWAP(ctx_swap),
   .IN(ctx_in), .OUT(ctx_out)
   );


//////////////////////////////////////////////////////////////////////
// Weight LUT

reg [10:0] tbl [0:511];

`include "interp_tbl.vh"


//////////////////////////////////////////////////////////////////////
// Pitch accumulator

reg signed [24:0] padj;
reg [24:0]        pstep_mod;
reg [13:0]        pstep, pstep_next;

initial begin
  sout = 0;
  pcnt = 0;
end

wire pmon = PMON[VSEL] & ~NON[VSEL];

function [13:0] pclamp(input [14:0] in);
  begin
    if (in > 15'h3fff)
      in = 15'h3fff;
    pclamp = in[13:0];
  end
endfunction

always @* begin
  padj = 25'd0;
  if (pmon)
    padj = VOUTM1 >>> 4;
  padj = padj + 25'h400;                // 11 bits
  pstep_mod = (PITCH * padj) >>> 10;    // 15 bits
  pstep_next = pclamp(pstep_mod[14:0]); // 14 bits
end

assign pcnt_next = pcnt + pstep;

always @(posedge CLK) begin
  if (~nRES) begin
    pcnt <= 0;
    gos <= 0;
  end
  else if (CKEN) begin
    if (ctx_swap) begin
      pcnt <= ctx_pcnt;
    end
    if (ADJ_PITCH) begin
      pstep <= pstep_next;
    end
    if (gos[4]) begin
      if (PLAY)
        pcnt <= pcnt_next;
      else
        pcnt <= 0;
    end

    gos <= {gos[4:0], GO};
  end

  if (mon_write_interp0) begin
    pcnt <= MON_DIN[14:0];
    pstep <= MON_DIN[28:15];
  end
  if (mon_write_interp1)
    gos <= MON_DIN[22:17];
end

always @* begin
  next = 0;
  if (PLAY)
    next = pcnt_next[15:12] - pcnt[14:12];
end

assign ctx_swap = gos[5];

assign IN_NEXT = GO * next;


//////////////////////////////////////////////////////////////////////
// Output computation

wire [7:0] idx;
reg [8:0]  tbl_idx;
reg [10:0] tbl_out;
wire [3:0] macs;

assign idx = pcnt_next[11:4];
assign macs = gos[4:1];

always @* begin
  tbl_idx = 9'd0;
  if (macs[0])
    tbl_idx = 9'h0ff - idx;
  else if (macs[1])
    tbl_idx = 9'h1ff - idx;
  else if (macs[2])
    tbl_idx = 9'h100 + idx;
  else if (macs[3])
    tbl_idx = 9'h000 + idx;
end

assign IN_SAMP_RS = gos[3:0];

always @(posedge CLK)
  tbl_out <= tbl[tbl_idx];

always @*
  mula = IN_SAMP;

always @*
  mulb = tbl_out;

always @(posedge CLK) begin
  prd <= mula * mulb;
  smp <= acc + (prd >>> 10);
end

always @* begin
  csmp = clamp(smp);
end

always @(posedge CLK) begin
  if (~nRES) begin
    acc <= 0;
    sout <= 0;
  end
  else if (CKEN) begin
    if (GO)
      acc <= 17'd0;
    else if (|macs)
      acc <= smp;

    if (ctx_swap)
      sout <= ctx_sout;
    else if (gos[4])
      sout <= csmp[15:1];
  end

  if (mon_write_interp1)
    acc <= MON_DIN[16:0];
  if (mon_write_interp2)
    sout <= MON_DIN[15:0];
end

assign OUT = sout;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout, mon_dout_interp;
reg        mon_ack_interp, mon_valid_interp;
reg        mon_sel_interp0, mon_sel_interp1, mon_sel_interp2;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  mon_ack_interp = 1'b1;
  mon_sel_interp0 = 1'b0;
  mon_sel_interp1 = 1'b0;
  mon_sel_interp2 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'hc8: begin                //``REG INTERP0
      mon_sel_interp0 = 1'b1;
      mon_dout[14:0] = pcnt;
      mon_dout[28:15] = pstep;
    end
    8'hc9: begin                //``REG INTERP1
      mon_sel_interp1 = 1'b1;
      mon_dout[16:0] = acc;
      mon_dout[22:17] = gos;
    end
    8'hca: begin                //``REG INTERP2
      mon_sel_interp2 = 1'b1;
      mon_dout[15:0] = sout;
    end
    default: mon_ack_interp = 1'b0;
  endcase
end

assign mon_ack_ctx = (MON_SEL >= 8'hbf && MON_SEL <= 8'hc6); //``REG_ARRAY INTERP_CTX

assign mon_write = MON_READY & MON_WS & ~mon_valid_interp;
assign mon_write_interp0 = mon_write & mon_sel_interp0;
assign mon_write_interp1 = mon_write & mon_sel_interp1;
assign mon_write_interp2 = mon_write & mon_sel_interp2;

always @(posedge CLK) begin
  mon_valid_interp <= 1'b0;
  if (mon_ack_interp & MON_READY) begin
    mon_valid_interp <= 1'b1;
    mon_dout_interp <= mon_dout;
  end
end

always @* begin
  MON_ACK = 1'b1;
  if (mon_ack_ctx) begin
    MON_VALID = mon_valid_ctx;
    MON_DOUT = mon_dout_ctx;
  end
  else if (mon_ack_interp) begin
    MON_VALID = mon_valid_interp;
    MON_DOUT = mon_dout_interp;
  end
  else begin
    MON_ACK = 1'b0;
    MON_VALID = 1'b0;
    MON_DOUT = 32'b0;
  end
end


endmodule
