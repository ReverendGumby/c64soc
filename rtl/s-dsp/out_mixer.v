`timescale 1us / 1ns

/* verilator lint_off IMPLICIT */
/* verilator lint_off WIDTH */

module s_dsp_out_mixer
  (
   input                CLK,
   input                nRES,
   input                CKEN,

   // debug monitor
   input [7:0]          MON_SEL,
   output reg           MON_ACK,
   input                MON_READY,
   output reg [31:0]    MON_DOUT,
   output reg           MON_VALID,
   input                MON_WS,
   input [31:0]         MON_DIN,

   // Global registers
   input                MUTE,
   input [7:0]          MVOLL,
   input [7:0]          EVOLL,
   input [7:0]          MVOLR,
   input [7:0]          EVOLR,

   // Control signals
   input                VML,
   input                VMR,
   input                EML,
   input                EMR,
   input                GOL,
   input                GOR,

   // Voice, echo inputs
   input signed [15:0]  VOUTL,
   input signed [15:0]  VOUTR,
   input signed [15:0]  EOUTL,
   input signed [15:0]  EOUTR,

   // Sample output
   output signed [15:0] SOUTL,
   output signed [15:0] SOUTR
   );

`include "math.vh"

reg signed [25:0] omul;
reg signed [25:0] imac, mul1, mul2;
reg signed [16:0] sum;
reg signed [25:0] omac;
reg signed [24:0] suml, sumr;
reg signed [15:0] soutl, soutr;

wire              mon_write_omix0, mon_write_omix1, mon_write_omix2;

always @* begin
  omul = (mul1 * mul2) >>> 7;
  sum = imac[16:0] + omul[16:0];
  omac = clamp(sum);
end

initial begin
  suml = 25'd0;
  sumr = 25'd0;
  soutl = 16'd0;
  soutr = 16'd0;
end

always @* begin
  mul1 = 25'd0;
  mul2 = 25'd1 << 7;            // negate the built-in right shift
  imac = 25'd0;
  if (VML) begin
    mul1 = VOUTL;
    imac = suml;
  end
  if (VMR) begin
    mul1 = VOUTR;
    imac = sumr;
  end
  if (EML) begin
    mul1 = EOUTL;
    mul2 = $signed(EVOLL);
    imac = suml;
  end
  if (EMR) begin
    mul1 = EOUTR;
    mul2 = $signed(EVOLR);
    imac = sumr;
  end
  if (GOL) begin
    mul1 = suml;
    mul2 = $signed(MVOLL);
  end
  if (GOR) begin
    mul1 = sumr;
    mul2 = $signed(MVOLR);
  end
  if (MUTE) begin
    mul1 = 25'd0;
    imac = 25'd0;
  end
end

always @(posedge CLK) begin
  if (~nRES) begin
    suml <= 25'd0;
    sumr <= 25'd0;
    soutl <= 16'd0;
    soutr <= 16'd0;
  end
  else if (CKEN) begin
    if (VML) begin
      suml <= omac;
    end
    if (VMR) begin
      sumr <= omac;
    end
    if (EML) begin
      suml <= omac;
    end
    if (EMR) begin
      sumr <= omac;
    end
    if (GOL) begin
      soutl <= omac;
      suml <= 25'd0;
    end
    if (GOR) begin
      soutr <= omac;
      sumr <= 25'd0;
    end
  end

  if (mon_write_omix0) begin
    suml <= MON_DIN[24:0];
  end
  if (mon_write_omix1) begin
    sumr <= MON_DIN[24:0];
  end
  if (mon_write_omix2) begin
    soutl <= MON_DIN[15:0];
    soutr <= MON_DIN[31:16];
  end
end

assign SOUTL = soutl[15:0];
assign SOUTR = soutr[15:0];
  

//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout, mon_dout_omix;
reg        mon_ack_omix, mon_valid_omix;
reg        mon_sel_omix0, mon_sel_omix1, mon_sel_omix2;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  mon_ack_omix = 1'b1;
  mon_sel_omix0 = 1'b0;
  mon_sel_omix1 = 1'b0;
  mon_sel_omix2 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'heb: begin                //``REG OMIX0
      mon_sel_omix0 = 1'b1;
      mon_dout[24:0] = suml;
    end
    8'hec: begin                //``REG OMIX1
      mon_sel_omix1 = 1'b1;
      mon_dout[24:0] = sumr;
    end
    8'hed: begin                //``REG OMIX2
      mon_sel_omix2 = 1'b1;
      mon_dout[15:0] = soutl;
      mon_dout[31:16] = soutr;
    end
    default: mon_ack_omix = 1'b0;
  endcase
end

assign mon_write = MON_READY & MON_WS & ~mon_valid_omix;
assign mon_write_omix0 = mon_write & mon_sel_omix0;
assign mon_write_omix1 = mon_write & mon_sel_omix1;
assign mon_write_omix2 = mon_write & mon_sel_omix2;

always @(posedge CLK) begin
  mon_valid_omix <= 1'b0;
  if (mon_ack_omix & MON_READY) begin
    mon_valid_omix <= 1'b1;
    mon_dout_omix <= mon_dout;
  end
end

always @* begin
  MON_ACK = 1'b1;
  if (mon_ack_omix) begin
    MON_VALID = mon_valid_omix;
    MON_DOUT = mon_dout_omix;
  end
  else begin
    MON_ACK = 1'b0;
    MON_VALID = 1'b0;
    MON_DOUT = 32'b0;
  end
end


endmodule
