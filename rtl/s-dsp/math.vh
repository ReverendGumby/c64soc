// Math on audio sample utility code

function signed [15:0] clamp(input signed [16:0] in);
  begin
    if (in > 17'sh7fff)
      in = 17'sh7fff;
    else if (in < -17'sh8000)
      in = -17'sh8000;
    clamp = in[15:0];
  end
endfunction

