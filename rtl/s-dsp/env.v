`timescale 1us / 1ns

/* verilator lint_off WIDTH */

module s_dsp_env
  (
   input             CLK,
   input             nRES,
   input             CKEN,

   // debug monitor
   input [7:0]       MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input             MON_WS,
   input [31:0]      MON_DIN,

   // Global counter
   input [15:0]      GLB_CNT,

   // Global registers
   input [7:0]       KON,
   input [7:0]       KOF,

   // Voice pipeline control signals
   input [2:0]       VSEL,

   // Voice registers
   input [7:0]       ADSR1,
   input [7:0]       ADSR2_GAIN,

   // Control signals
   input             PLAY,
   input             BEND,
   input             UPDATE,

   // Envelope output
   output [10:0]     OUT
   );

reg [10:0] envelope;
reg [1:0]  adsr_st;

localparam [1:0] ADSR_ST_RLS = 3'd0;
localparam [1:0] ADSR_ST_ATK = 3'd1;
localparam [1:0] ADSR_ST_DCY = 3'd2;
localparam [1:0] ADSR_ST_STN = 3'd3;

wire [3:0] A = ADSR1[3:0];
wire [2:0] D = ADSR1[6:4];
wire [2:0] S = ADSR2_GAIN[7:5];
wire [4:0] R = ADSR2_GAIN[4:0];

wire eam_adsr = ADSR1[7];
wire eam_direct = ~eam_adsr & ~ADSR2_GAIN[7];
wire eam_gain = ~eam_adsr & ADSR2_GAIN[7];

localparam [1:0] GAIN_LIN_DEC = 2'b00;
localparam [1:0] GAIN_EXP_DEC = 2'b01;
localparam [1:0] GAIN_LIN_INC = 2'b10;
localparam [1:0] GAIN_EXP_INC = 2'b11;

wire [1:0] gain_mode = ADSR2_GAIN[6:5];
wire [4:0] gain_rate = ADSR2_GAIN[4:0];

wire       mon_write_env0;

initial begin
  envelope = 0;
  adsr_st = ADSR_ST_RLS;
end


//////////////////////////////////////////////////////////////////////
// Context store

localparam CTX_WIDTH = 11 + 2;

reg        ctx_swap;
wire [CTX_WIDTH-1:0] ctx_in, ctx_out;
wire [10:0]          ctx_envelope;
wire [1:0]           ctx_adsr_st;

wire [31:0]          mon_dout_ctx;
wire                 mon_ack_ctx;
wire                 mon_valid_ctx;

always @(posedge CLK) begin
  if (CKEN)
    ctx_swap <= UPDATE;

  if (mon_write_env0)
    ctx_swap <= MON_DIN[13];
end

assign ctx_in = { envelope, adsr_st };
assign { ctx_envelope, ctx_adsr_st } = ctx_out;

s_dsp_context_mem #(.WIDTH(CTX_WIDTH), .MON_BASE(8'hcb)) ctx
  (
   .CLK(CLK), .nRES(nRES), .CKEN(CKEN),
   .MON_SEL(MON_SEL), .MON_ACK(mon_ack_ctx), .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_ctx), .MON_VALID(mon_valid_ctx),
   .MON_WS(MON_WS), .MON_DIN(MON_DIN),
   .VSEL(VSEL),
   .SWAP(ctx_swap),
   .IN(ctx_in), .OUT(ctx_out)
   );


//////////////////////////////////////////////////////////////////////
// Event counter

reg [4:0] ev_rate;
wire      env_ev;

always @* begin
  ev_rate = 5'd31;
  if (eam_gain && adsr_st != ADSR_ST_RLS)
    ev_rate = gain_rate;
  else if (eam_adsr)
    case (adsr_st)
      ADSR_ST_ATK: ev_rate = A * 2 + 1;
      ADSR_ST_DCY: ev_rate = D * 2 + 16;
      ADSR_ST_STN: ev_rate = R;
      default: ;
    endcase
end

s_dsp_ev_ctr ev_ctr(.CLK(CLK), .GLB_CNT(GLB_CNT), .RATE(ev_rate), .EV(env_ev));


//////////////////////////////////////////////////////////////////////
// Envelope output

reg [1:0] ev_gain_mode;

always @* begin
  ev_gain_mode = gain_mode;
  if (eam_adsr) begin
    case (adsr_st)
      ADSR_ST_ATK: ev_gain_mode = GAIN_LIN_INC;
      ADSR_ST_DCY, ADSR_ST_STN: ev_gain_mode = GAIN_EXP_DEC;
      default: ;
    endcase
  end
end

reg [10:0] envelope_next, envelope_stn;

always @* begin :envelope_next_comp
reg signed [12:0] n;
  n = envelope;

  if (adsr_st == ADSR_ST_RLS) begin
    if (BEND)
      n = 0;
    else
      n = n - 8;
  end
  else begin
    if (eam_direct)
      n = {2'b0, ADSR2_GAIN[6:0], 4'b0};
    else
      case (ev_gain_mode)
        GAIN_LIN_DEC:
          n = n - 13'sd32;
        GAIN_EXP_DEC: begin
          n = n - 13'sd1;
          n = n - (n >>> 8);
        end
        GAIN_LIN_INC:
          n = n + ((eam_adsr && A == 4'hF) ? 13'sd1024 : 13'sd32);
        GAIN_EXP_INC:
          n = n + ((n < 13'sh600) ? 13'sd32 : 13'sd8);
        default: n = 13'sdx;
      endcase
  end

  if (n > 13'sd2047)
    n = 13'sd2047;
  else if (n < 13'sd0)
    n = 13'sd0;

  envelope_next = n[10:0];
end

always @* envelope_stn = {S, 8'hff};

wire kon = KON[VSEL];
wire kof = KOF[VSEL];

always @(posedge CLK) begin
  if (~nRES) begin
    envelope <= 0;
    adsr_st <= ADSR_ST_RLS;
  end
  else if (CKEN) begin
    if (ctx_swap) begin
      envelope <= ctx_envelope;
      adsr_st <= ctx_adsr_st;
    end
    if (UPDATE) begin
      if (kon) begin
        adsr_st <= ADSR_ST_ATK;
        envelope <= 0;
      end
      else begin
        if (kof | BEND)
          adsr_st <= ADSR_ST_RLS;
      end

      if (PLAY & env_ev)
        case (adsr_st)
          ADSR_ST_ATK: begin
            if (envelope != envelope_next)
              envelope <= envelope_next;
            else if (eam_adsr) begin
              if (envelope_next > envelope_stn)
                adsr_st <= ADSR_ST_DCY;
              else
                adsr_st <= ADSR_ST_STN;
            end
          end
          ADSR_ST_DCY: begin
            if (envelope_next > envelope_stn)
              envelope <= envelope_next;
            else
              adsr_st <= ADSR_ST_STN;
          end
          ADSR_ST_STN, ADSR_ST_RLS: begin
            envelope <= envelope_next;
          end
          default: ;
        endcase
    end
  end

  if (mon_write_env0) begin
    envelope <= MON_DIN[10:0];
    adsr_st <= MON_DIN[12:11];
  end
end

assign OUT = envelope;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout, mon_dout_env;
reg        mon_ack_env, mon_valid_env;
reg        mon_sel_env0;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  mon_ack_env = 1'b1;
  mon_sel_env0 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'hd4: begin                //``REG ENV0
      mon_sel_env0 = 1'b1;
      mon_dout[10:0] = envelope;
      mon_dout[12:11] = adsr_st;
      mon_dout[13] = ctx_swap;
    end
    default: mon_ack_env = 1'b0;
  endcase
end

assign mon_ack_ctx = (MON_SEL >= 8'hcb && MON_SEL <= 8'hd2); //``REG_ARRAY ENV_CTX

assign mon_write = MON_READY & MON_WS & ~mon_valid_env;
assign mon_write_env0 = mon_write & mon_sel_env0;

always @(posedge CLK) begin
  mon_valid_env <= 1'b0;
  if (mon_ack_env & MON_READY) begin
    mon_valid_env <= 1'b1;
    mon_dout_env <= mon_dout;
  end
end

always @* begin
  MON_ACK = 1'b1;
  if (mon_ack_ctx) begin
    MON_VALID = mon_valid_ctx;
    MON_DOUT = mon_dout_ctx;
  end
  else if (mon_ack_env) begin
    MON_VALID = mon_valid_env;
    MON_DOUT = mon_dout_env;
  end
  else begin
    MON_ACK = 1'b0;
    MON_VALID = 1'b0;
    MON_DOUT = 32'b0;
  end
end


endmodule
