`timescale 1us / 1ns

module s_dsp_reg_file
  (
   input             nRES, // reset
   input             CLK, // clock
   input             HOLD,
   input             ICLKEN,
   input             CCKEN,

   // debug monitor
   input [7:0]       MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input             MON_WS,
   input [31:0]      MON_DIN,

   // S-SMP port
   input [6:0]       SA,
   input             SRS,
   input             SWS,
   input [7:0]       SRDI,
   output reg [7:0]  SRDO,

   // Internal port
   input [6:0]       IA,
   input             IRS,
   input             IWS,
   input [7:0]       IRDI,
   output reg [7:0]  IRDO,

   output            KON_WRITTEN,
   input             KON_READ,
   output [7:5]      FLG
   );

reg [7:0] file [0:127];
reg       kon_written;
reg [7:5] flg_quick;

reg [6:0] addr1, addr2;
reg [7:0] rdat1, rdat2;
reg [7:0] wdat1, wdat2;
reg       ws1, ws2;

reg [7:0]  drdo, drdi;
reg [6:0]  da;
reg        drs, drsd, dws;
reg        da_ws;

wire       mon_write_drs, mon_write_drd, mon_write_regfile;

integer   i;

initial begin
  //for (i = 0; i < 128; i = i + 1)
  //  file[i] = 0;

  kon_written = 1'b1;
end

// Model as dual-port block RAM with two read/write ports:
//
// Port 1: S-SMP, debug monitor
// Port 2: Internal

always @(posedge CLK) begin
  rdat1 <= file[addr1];

  if (ws1) begin
    file[addr1] <= wdat1;
  end
end

always @(posedge CLK) begin
  rdat2 <= file[addr2];

  if (ws2) begin
    file[addr2] <= wdat2;
  end
end

always @(posedge CLK) begin
  if (~nRES) begin
    //kon_written <= 1'b1;
    flg_quick <= 3'b111;
  end
  else begin
    if (ICLKEN & KON_READ)
      kon_written <= 1'b0;

    if (~HOLD & ws1) begin
      if (addr1 == 7'h4C)
        kon_written <= 1'b1;
      if (addr1 == 7'h6C)
        flg_quick <= wdat1[7:5];
    end
  end

  if (mon_write_regfile) begin
    kon_written <= MON_DIN[0];
    flg_quick <= MON_DIN[3:1];
  end
end

always @(posedge CLK) begin
  drsd <= drs;

  if (drsd)
    drdo <= rdat1;
  else
    SRDO <= rdat1;

  drs <= mon_write_drs;
  dws <= mon_write_drd;

  if (mon_write_drs)
    da <= MON_DIN[6:0];
  if (mon_write_drd) begin
    drdi <= MON_DIN[7:0];
  end
end

always @* begin
  if (drs | dws) begin
    addr1 = da;
    wdat1 = drdi;
    ws1 = dws;
  end
  else begin
    addr1 = SA;
    wdat1 = SRDI;
    ws1 = ~HOLD & SWS;
  end
end

always @* addr2 = IA;
always @* IRDO = rdat2;
always @* wdat2 = IRDI;
always @* ws2 = ~HOLD & IWS;

//assign ENDX = file['h7C];
assign KON_WRITTEN = kon_written;
assign FLG = flg_quick;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_sel_drs, mon_sel_drd, mon_sel_regfile;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  MON_ACK = 1'b1;
  mon_sel_drs = 1'b0;
  mon_sel_drd = 1'b0;
  mon_sel_regfile = 1'b0;
  da_ws = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'h41: begin                //``REG DRS // DSP Register Select
      mon_sel_drs = 1'b1;
      mon_dout[6:0] = da;
    end
    8'h42: begin                //``REG DRD // DSP Register Data
      mon_sel_drd = 1'b1;
      mon_dout[7:0] = drdo;     //``NO_FIELD
    end
    8'h43: begin                //``REG REGFILE
      mon_sel_regfile = 1'b1;
      mon_dout[0] = kon_written;
      mon_dout[3:1] = flg_quick;
    end
    default: MON_ACK = 1'b0;
  endcase
end

assign mon_write = MON_READY & MON_WS & ~MON_VALID;
assign mon_write_drs = mon_write & mon_sel_drs;
assign mon_write_drd = mon_write & mon_sel_drd;
assign mon_write_regfile = mon_write & mon_sel_regfile;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK & MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end


endmodule
