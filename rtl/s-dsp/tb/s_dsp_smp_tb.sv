// S-SMP drives S-DSP.

`timescale 1us / 1ps

module s_dsp_smp_tb();

reg        nRES;
reg        clk;

integer    duration_sec, fade_ms;

wire       cken, rw;
wire [15:0] MA;
wire [7:0]  mdo_dsp_sim, do_ram;
wire signed [15:0] psoutl, psoutr;
wire        nCE, nWE, nOE;
wire        sclken;

initial begin
`ifdef VERILATOR
  $dumpfile("s_dsp_smp_tb.verilator.vcd");
  //$dumpvars();
`else
  $dumpfile("s_dsp_smp_tb.vcd");
  $dumpvars();
`endif
end

wire [15:0] a;
wire [7:0] do_dsp, do_smp;

s_smp smp
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken),
   .HOLD(1'b0),
   .A(a),
   .DI(do_dsp),
   .DO(do_smp),
   .RW(rw),
   .MON_SEL(8'b0),
   .MON_ACK(),
   .MON_READY(1'b0),
   .MON_DOUT(),
   .MON_VALID(),
   .MON_WS(1'b0),
   .MON_DIN(32'b0),

   .CPUI0(),
   .CPUI1(),
   .CPUI2(),
   .CPUI3(),
   .CPUIN(4'h0),
   .CPUO0(),
   .CPUO1(),
   .CPUO2(),
   .CPUO3(),
   .CPUON()
   );

s_dsp dsp
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken),
   .HOLD(1'b0),
   .A(a),
   .DI(do_smp),
   .DO(do_dsp),
   .RW(rw),
   .MA(MA),
   .MDI(do_ram),
   .MDO(mdo_dsp_sim),
   .nCE(nCE),
   .nWE(nWE),
   .nOE(nOE),
   .SCLKEN(sclken),
   .PSOUTL(psoutl),
   .PSOUTR(psoutr),
   .MON_SEL(8'b0),
   .MON_ACK(),
   .MON_READY(1'b0),
   .MON_DOUT(),
   .MON_VALID(),
   .MON_WS(1'b0),
   .MON_DIN(32'b0)
   );

ram ram(.CLK(clk), .A(MA[15:0]), .DI(mdo_dsp_sim), .DO(do_ram), .nCE(nCE), 
        .nWE(nWE), .nOE(nOE));

task read_from_file(input string path);
integer fin, code;
reg [7:0] tmp [0:4];
string    tmps;
  fin = $fopen(path, "r");
  assert(fin != 0) else $finish;

  code = $fseek(fin, 'h25, 0);
  code = $fread(tmp, fin, 0, 2);
  smp.pc = {tmp[1], tmp[0]};
  code = $fread(smp.ac, fin);
  code = $fread(smp.x, fin);
  code = $fread(smp.y, fin);
  code = $fread(smp.p, fin);
  code = $fread(smp.s, fin);
  smp.aord = smp.pc;

  code = $fseek(fin, 'ha9, 0);
  code = $fread(tmp, fin, 0, 3);
  tmps = {tmp[0], tmp[1], tmp[2]};
  duration_sec = tmps.atoi();
  code = $fread(tmp, fin, 0, 5);
  tmps = {tmp[0], tmp[1], tmp[2], tmp[3], tmp[4]};
  fade_ms = tmps.atoi();
  $display("duration: %d", duration_sec + int'(fade_ms / 1000.0));

  code = $fseek(fin, 'h100, 0);
  ram.read_from_file(fin);

  code = $fread(dsp.rf.file, fin);

  $fclose(fin);
endtask

initial begin
  clk = 1;
  nRES = 1;

  read_from_file("song.spc");

  repeat (3) @(posedge cken) ;

  smp.cpui[0] = ram.ram['hf4];
  smp.cpui[1] = ram.ram['hf5];
  smp.cpui[2] = ram.ram['hf6];
  smp.cpui[3] = ram.ram['hf7];
  smp.spt0 = ram.ram['hfa];
  smp.spt1 = ram.ram['hfb];
  smp.spt2 = ram.ram['hfc];
  smp.spcr = ram.ram['hf1] & ~8'h30;

  dsp.rf.flg_quick = dsp.rf.file[7'h6c][7:5];

  // Avoid premature echo writes if they're enabled at start.
  if (dsp.rf.flg_quick[5] == 1'b0)
    dsp.sgenloop.clr_stalled = 1'b1;
end

initial forever begin :clkgen
  #(500.0/24576.0) clk = ~clk;
end

initial begin :main
integer dur;
integer i;

  #1 ;

`ifdef VERILATOR
  dur = duration_sec != 0 ? duration_sec : 1;
`else
  dur = 1;
`endif
  for (i = 0; i < dur; i = i + 1) begin
    #(1e6) ;
    $display("elapsed: %d", i + 1);
  end
  $finish;
end

integer fout, fout2;
reg     vml_sync;
reg [15:0] out;
initial begin
  fout = $fopen("s_dsp_smp_tb.raw", "w+b");
  fout2 = $fopen("s_dsp_smp_tb_8ch.raw", "w+b");
  vml_sync = 0;
end
always @(posedge clk) begin
  if (sclken) begin
    $fwrite(fout, "%c%c", psoutl[15:8], psoutl[7:0]);
    $fwrite(fout, "%c%c", psoutr[15:8], psoutr[7:0]);
  end
  if (dsp.sgenloop.CKEN) begin
    if (~vml_sync && dsp.sgenloop.voice.interp.VSEL == 3'd0)
      vml_sync = 1;
    if (vml_sync && dsp.sgenloop.vml) begin
      out = dsp.sgenloop.voice.scale_pan.out[15:0];
      $fwrite(fout2, "%c%c", out[15:8], out[7:0]);
    end
  end
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s s_dsp_smp_tb -o s_dsp_smp_tb.vvp -f s_dsp.files ../../s-smp/sp_timer.v ../../s-smp/s_smp.v ../../s-smp/tb/ram.sv s_dsp_smp_tb.sv && ./s_dsp_smp_tb.vvp"
// End:
