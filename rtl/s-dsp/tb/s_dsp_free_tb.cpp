// Verilated -*- C++ -*-
// DESCRIPTION: main() calling loop, created with Verilator --main

#include "verilated.h"
#include "Vs_dsp_free_tb.h"

//======================

int main(int argc, char** argv, char**) {
    // Setup context, defaults, and parse command line
    Verilated::debug(0);
    const std::unique_ptr<VerilatedContext> contextp{new VerilatedContext};
    contextp->traceEverOn(true);
    contextp->commandArgs(argc, argv);

    // Construct the Verilated model, from Vtop.h generated from Verilating
    const std::unique_ptr<Vs_dsp_free_tb> topp{new Vs_dsp_free_tb{contextp.get()}};

    // Simulate until $finish
    while (!contextp->gotFinish()) {
        // Evaluate model
        topp->eval();
        // Advance time
        if (!topp->eventsPending()) break;
        contextp->time(topp->nextTimeSlot());
    }

    if (!contextp->gotFinish()) {
        VL_DEBUG_IF(VL_PRINTF("+ Exiting without $finish; no events left\n"););
    }

    // Final model cleanup
    topp->final();
    return 0;
}

// Local Variables:
// compile-command: "verilator +1364-2005ext+v -DSYNTHESIS --cc --exe s_dsp_free_tb.cpp --build -j 0 --timing --assert --trace --top-module s_dsp_free_tb --relative-includes -F s_dsp.files ../../s-smp/tb/ram.sv reggen.v s_dsp_free_tb.sv"
// End:
