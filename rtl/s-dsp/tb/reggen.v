`timescale 1us / 1ns

module reggen
  (
   input             nRES,
   input             CLK,
   input             CKEN,

   input             KEY,
   output reg [15:0] A,
   output reg [7:0]  DO,
   output reg        RW
   );

reg cken_d, key_d;
reg [3:0] update;
reg [8:0] cnt;

initial key_d = 0;
initial update = 0;
initial cnt = 0;
initial RW = 1'b1;

always @(posedge CLK)
  cken_d <= CKEN;

always @(posedge CLK) if (CKEN & ~cken_d) begin
  if (update[0]) begin
    A <= 16'h00F2;
    DO <= KEY ? 8'h4C : 8'h5C;
    RW <= 1'b0;
    update <= update << 1;
  end
  else if (update[1]) begin
    A <= 16'h00F3;
    DO <= 8'hFF;
    RW <= 1'b0;
    cnt <= ~0;
    update <= update << 1;
  end
  else if (update[2]) begin
    RW <= 1'b1;
    if (|cnt)
      cnt <= cnt - 1'd1;
    else
      update <= update << 1;
  end
  else if (update[3]) begin
    DO <= 8'h00;
    RW <= 1'b0;
    update <= 0;
  end
  else begin
    A <= 16'h0000;
    DO <= 8'h00;
    RW <= 1'b1;
    if (key_d != KEY)
      update <= 4'b1;
    key_d <= KEY;
  end
end

endmodule
