// Start S-DSP with initial state, then let it run freely.

`timescale 1us / 1ps

module s_dsp_free_tb();

reg        nRES;
reg        clk;

wire       cken, rw;
wire [15:0] MA;
wire [7:0]  mdo_dsp_sim, do_ram;
wire signed [15:0] psoutl, psoutr;
wire        nCE, nWE, nOE;
wire        sclken;

initial begin
`ifdef VERILATOR
  $dumpfile("s_dsp_free_tb.verilator.vcd");
  $dumpvars();
`else
  $dumpfile("s_dsp_free_tb.vcd");
  $dumpvars();
`endif
end

wire [15:0] a;
wire [7:0] do_smp;

reggen reggen
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken),
   .KEY(btn_key),
   .A(a),
   .DO(do_smp),
   .RW(rw)
   );

s_dsp dsp
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken),
   .HOLD(1'b0),
   .A(a),
   .DI(do_smp),
   .DO(),
   .RW(rw),
   .MA(MA),
   .MDI(do_ram),
   .MDO(mdo_dsp_sim),
   .nCE(nCE),
   .nWE(nWE),
   .nOE(nOE),
   .SCLKEN(sclken),
   .PSOUTL(psoutl),
   .PSOUTR(psoutr),
   .MON_SEL(8'b0),
   .MON_ACK(),
   .MON_READY(1'b0),
   .MON_DOUT(),
   .MON_VALID(),
   .MON_WS(1'b0),
   .MON_DIN(32'b0)
   );

ram ram(.CLK(clk), .A(MA[15:0]), .DI(mdo_dsp_sim), .DO(do_ram), .nCE(nCE), 
        .nWE(nWE), .nOE(nOE));

task read_from_file(input string path);
integer fin, code;
reg [7:0] tmp [0:4];
string    tmps;
  fin = $fopen(path, "r");
  assert(fin != 0) else $finish;

  code = $fseek(fin, 'h25, 0);
  code = $fread(tmp, fin, 0, 2);
  //smp_sim.pc = {tmp[1], tmp[0]};
  code = $fread(tmp, fin, 0, 1);
  code = $fread(tmp, fin, 0, 1);
  code = $fread(tmp, fin, 0, 1);
  code = $fread(tmp, fin, 0, 1);
  code = $fread(tmp, fin, 0, 1);
  //smp_sim.ab = smp_sim.pc;

  code = $fseek(fin, 'ha9, 0);
  code = $fread(tmp, fin, 0, 3);
  tmps = {tmp[0], tmp[1], tmp[2]};
  //duration_sec = tmps.atoi();
  code = $fread(tmp, fin, 0, 5);
  tmps = {tmp[0], tmp[1], tmp[2], tmp[3], tmp[4]};
  //fade_ms = tmps.atoi();
  //$display("duration: %d", duration_sec + int'(fade_ms / 1000.0));

  code = $fseek(fin, 'h100, 0);
  ram.read_from_file(fin);

  code = $fread(dsp.rf.file, fin);

  $fclose(fin);
endtask

initial begin
  read_from_file("song.spc");

  dsp.rf.flg_quick = dsp.rf.file[7'h6c][7:5];

  clk = 1;
  nRES = 0;

  repeat (8 * 5) @(posedge clk) ;
  nRES = 1;
end

initial forever begin :clkgen
  #(500.0/24576.0) clk = ~clk;
end

reg btn_key;
initial begin
  btn_key = 1'b0;
  //#100 btn_key = 1'b1;
  //#900 btn_key = 1'b0;
end

initial begin :main
integer dur;
integer i;
`ifdef VERILATOR
  dur = 1000;
`else
  dur = 5;
`endif
  for (i = 0; i < dur; i = i + 1)
    #1000 ;
  $finish;
end

integer fout, fout2;
reg     vml_sync;
reg [15:0] out;
initial begin
  fout = $fopen("s_dsp_free_tb.raw", "w+b");
  fout2 = $fopen("s_dsp_free_tb_8ch.raw", "w+b");
  vml_sync = 0;
end
always @(posedge clk) begin
  if (sclken) begin
    $fwrite(fout, "%c%c", psoutl[15:8], psoutl[7:0]);
    $fwrite(fout, "%c%c", psoutr[15:8], psoutr[7:0]);
  end
  if (dsp.sgenloop.CKEN) begin
    if (~vml_sync && dsp.sgenloop.voice.interp.VSEL == 3'd0)
      vml_sync = 1;
    if (vml_sync && dsp.sgenloop.vml) begin
      out = dsp.sgenloop.voice.scale_pan.out[15:0];
      $fwrite(fout2, "%c%c", out[15:8], out[7:0]);
    end
  end
end

/* -----\/----- EXCLUDED -----\/-----
always @(posedge clk) begin
  if (dsp.sgenloop.CKEN) begin
    if (dsp.sgenloop.voice.brr_out_next && dsp.sgenloop.voice.brr_vsel == 3'd7)
      $display("brr_sout     = %d", dsp.sgenloop.voice.brr_sout);
    if (dsp.sgenloop.voice.interp.GO && dsp.sgenloop.voice.interp.VSEL == 3'd7)
      $display("interp IN0:3 = %d, %d, %d, %d", dsp.sgenloop.voice.interp.IN0, dsp.sgenloop.voice.interp.IN1, dsp.sgenloop.voice.interp.IN2, dsp.sgenloop.voice.interp.IN3);
    if (dsp.sgenloop.voice.interp.gos[1] && dsp.sgenloop.voice.interp.VSEL == 3'd7)
      $display("interp_sout  = %d", dsp.sgenloop.voice.interp_sout);
    if (dsp.sgenloop.vml && dsp.sgenloop.voice.interp.VSEL == 3'd7)
      $display("voutl        = %d", dsp.sgenloop.voutl);
  end
end
 -----/\----- EXCLUDED -----/\----- */

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s s_dsp_free_tb -o s_dsp_free_tb.vvp -f s_dsp.files ../../s-smp/tb/ram.sv reggen.v s_dsp_free_tb.sv && ./s_dsp_free_tb.vvp"
// End:
