`timescale 1us / 1ns

module s_dsp_sram_if
  (
   input             CLK,
   input             CCKEN,
   input             HOLD,

   // debug monitor
   input [7:0]       MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input             MON_WS,
   input [31:0]      MON_DIN,

   // S-SMP memory port
   input [15:0]      A, // address bus (from S-SMP)
   input [7:0]       DI, // data bus (from S-SMP)
   input             RW, // R/W (from S-SMP)

   // SRAM interface
   output [15:0]     MA, // SRAM address bus
   input [7:0]       MDI, // SRAM data bus (from RAM)
   output [7:0]      MDO, // SRAM data bus (to RAM)
   output            nCE, // SRAM chip enable
   output            nWE, // SRAM write enable
   output            nOE, // SRAM output enable

   // Internal (S-DSP) memory port
   input [15:0]      IMA,
   input             IMRS,
   input             IMWS,
   output [7:0]      IMDO,
   input [7:0]       IMDI
   );

reg [15:0] mad_a, ima;
reg        mad_noe, mad_nwe, mad_nce;
reg [7:0]  mad_do, imdi;
reg        imrs, imws;
reg        mad_ssmp;            // S-SMP owns the SRAM A/D bus
wire       mad_sdsp;            // S-DSP owns the SRAM A/D bus
wire       mad_doe;

wire       mon_write_sram0;

// S-SMP bus is delayed by 1x CLK. Delay ours to match.
always @(posedge CLK) begin
  if (~HOLD) begin
    mad_ssmp <= CCKEN;
    ima <= IMA;
    imdi <= IMDI;
    imrs <= IMRS;
    imws <= IMWS;
  end

  if (mon_write_sram0) begin
    ima <= MON_DIN[15:0];
    imdi <= MON_DIN[23:16];
    imrs <= MON_DIN[24];
    imws <= MON_DIN[25];
    mad_ssmp <= MON_DIN[26];
  end
end

assign mad_sdsp = ~mad_ssmp;

assign mad_doe = ~(nWE | nCE);
assign MDO = mad_doe ? mad_do : 8'hxx;

always @* begin
  if (mad_ssmp) begin
    mad_a = A;
    mad_do = DI;
    mad_noe = ~RW;
    mad_nwe = RW;
    mad_nce = 0;
  end
  else /*if (mad_sdsp)*/ begin
    mad_a = ima;
    mad_do = imdi;
    mad_noe = ~imrs;
    mad_nwe = ~imws;
    mad_nce = ~(imrs | imws);
  end
end

assign MA = mad_a;
assign nOE = mad_noe;
assign nWE = mad_nwe;
assign nCE = mad_nce;

assign IMDO = MDI;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_sel_sram0;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  MON_ACK = 1'b1;
  mon_sel_sram0 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'h47: begin                //``REG SRAM0
      mon_sel_sram0 = 1'b1;
      mon_dout[15:0] = ima;
      mon_dout[23:16] = imdi;
      mon_dout[24] = imrs;
      mon_dout[25] = imws;
      mon_dout[26] = mad_ssmp;
    end
    default: begin
      MON_ACK = 1'b0;
    end
  endcase
end

assign mon_write = MON_READY & MON_WS & ~MON_VALID;
assign mon_write_sram0 = mon_write & mon_sel_sram0;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK & MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end


endmodule
