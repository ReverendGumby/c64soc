`timescale 1us / 1ns

/* verilator lint_off IMPLICIT */
/* verilator lint_off WIDTH */

module s_dsp_scale_pan
  (
   input                CLK,
   input                nRES,
   input                CKEN,

   // debug monitor
   input [7:0]          MON_SEL,
   output reg           MON_ACK,
   input                MON_READY,
   output reg [31:0]    MON_DOUT,
   output reg           MON_VALID,
   input                MON_WS,
   input [31:0]         MON_DIN,

   // Voice registers
   input                NON,
   input signed [7:0]   VOLL,
   input signed [7:0]   VOLR,

   // Control signals
   input                ENV_APPLY,
   input                VOLL_APPLY,
   input                VOLR_APPLY,

   // Sample, envelope inputs
   input signed [14:0]  BSIN,
   input signed [14:0]  NSIN,
   input [10:0]         ENV,

   // Sample output
   output               VML,
   output               VMR,
   output signed [14:0] VOUT,
   output signed [15:0] OUTL,
   output signed [15:0] OUTR
   );

wire signed [25:0] sin_mul;
reg signed [24:0] out, outl, outr;
reg               vml, vmr;

wire              mon_write_sp0, mon_write_sp1;

assign sin_mul = NON ? NSIN : BSIN;

initial begin
  vml = 1'b0;
  vmr = 1'b0;
  out = 25'd0;
  outl = 16'd0;
  outr = 16'd0;
end

always @(posedge CLK) begin
  if (~nRES) begin
    vml <= 1'b0;
    vmr <= 1'b0;
    out <= 25'd0;
    outl <= 16'd0;
    outr <= 16'd0;
  end
  else if (CKEN) begin
    if (ENV_APPLY) begin
      out <= $signed(sin_mul * ENV) >>> 11;
    end
    if (VOLL_APPLY) begin
      outl <= $signed(out * VOLL) >>> 6;
    end
    if (VOLR_APPLY) begin
      outr <= $signed(out * VOLR) >>> 6;
    end
    vml <= VOLL_APPLY;
    vmr <= VOLR_APPLY;
  end

  if (mon_write_sp0) begin
    vml <= MON_DIN[0];
    vmr <= MON_DIN[1];
    out <= MON_DIN[16:2];
  end
  if (mon_write_sp1) begin
    outl <= MON_DIN[15:0];
    outr <= MON_DIN[31:16];
  end
end

assign VML = vml;
assign VMR = vmr;
assign VOUT = out[14:0];
assign OUTL = outl[15:0];
assign OUTR = outr[15:0];
  

//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout, mon_dout_sp;
reg        mon_ack_sp, mon_valid_sp;
reg        mon_sel_sp0, mon_sel_sp1;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  mon_ack_sp = 1'b1;
  mon_sel_sp0 = 1'b0;
  mon_sel_sp1 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'hd5: begin                //``REG SP0
      mon_sel_sp0 = 1'b1;
      mon_dout[0] = vml;
      mon_dout[1] = vmr;
      mon_dout[16:2] = out;
    end
    8'hd6: begin                //``REG SP1
      mon_sel_sp1 = 1'b1;
      mon_dout[15:0] = outl;
      mon_dout[31:16] = outr;
    end
    default: mon_ack_sp = 1'b0;
  endcase
end

assign mon_write = MON_READY & MON_WS & ~mon_valid_sp;
assign mon_write_sp0 = mon_write & mon_sel_sp0;
assign mon_write_sp1 = mon_write & mon_sel_sp1;

always @(posedge CLK) begin
  mon_valid_sp <= 1'b0;
  if (mon_ack_sp & MON_READY) begin
    mon_valid_sp <= 1'b1;
    mon_dout_sp <= mon_dout;
  end
end

always @* begin
  MON_ACK = 1'b1;
  if (mon_ack_sp) begin
    MON_VALID = mon_valid_sp;
    MON_DOUT = mon_dout_sp;
  end
  else begin
    MON_ACK = 1'b0;
    MON_VALID = 1'b0;
    MON_DOUT = 32'b0;
  end
end


endmodule
