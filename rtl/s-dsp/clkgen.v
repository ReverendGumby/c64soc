`timescale 1us / 1ns

module s_dsp_clkgen
  (
   input             CLK,
   input             nRES_I,
   input             HOLD,
   output            nRES_O,
   output            ICLKEN,
   output [1:0]      CCCNT,
   output            CCKEN,

   // debug monitor
   input [7:0]       MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input             MON_WS,
   input [31:0]      MON_DIN
   );

wire    mon_write_clkgen;

// iclken = 3.072MHz = CLK / 8

reg [2:0] idivcnt;

initial begin
  idivcnt = 0;
end

always @(posedge CLK) begin
  if (~HOLD)
    idivcnt <= idivcnt + 1'd1;

  if (mon_write_clkgen)
    idivcnt <= MON_DIN[2:0];
end

assign ICLKEN = ~HOLD & &idivcnt;

// SRAM bus cycle counter

reg [1:0] cccnt;

initial
  cccnt = 2'd2;

wire cccnt_wrap = cccnt == 2'd2;

always @(posedge CLK) begin
  if (ICLKEN) begin
    if (cccnt_wrap)
      cccnt <= 0;
    else
      cccnt <= cccnt + 1'd1;
  end

  if (mon_write_clkgen)
    cccnt <= MON_DIN[4:3];
end

assign CCCNT = cccnt;
assign CCKEN = cccnt == 2'd0;

// Internal reset: synchronize deassertion with CCKEN

reg nres;
initial nres = 0;

always @(posedge CLK) begin
  nres <= (nres | (ICLKEN & cccnt_wrap)) & nRES_I;

  if (mon_write_clkgen)
    nres <= MON_DIN[5];
end

// Ignore reset during HOLD
assign nRES_O = nres | HOLD;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_sel_clkgen;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  MON_ACK = 1'b1;
  mon_sel_clkgen = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'h44: begin                //``REG CLKGEN
      mon_sel_clkgen = 1'b1;
      mon_dout[2:0] = idivcnt;
      mon_dout[4:3] = cccnt;
      mon_dout[5] = nres;
    end
    default: begin
      MON_ACK = 1'b0;
    end
  endcase
end

assign mon_write = MON_READY & MON_WS & ~MON_VALID;
assign mon_write_clkgen = mon_write & mon_sel_clkgen;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK & MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end


endmodule
