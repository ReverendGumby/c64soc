`timescale 1us / 1ns

// Uses the Mednafen algorithm described in Anomie's S-DSP Doc

module s_dsp_glb_ctr
  (
   input             nRES,
   input             CLK,
   input             SCLKEN,

   // debug monitor
   input [7:0]       MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input             MON_WS,
   input [31:0]      MON_DIN,

   output reg [15:0] CNT
   );

wire mon_write_gc;
reg [15:0] cntf, cnt_next;

initial begin
  CNT = 0;
end

// Global counter

always @* begin
  cntf = CNT;
  if (~|CNT[2:0])
    cntf[2:0] = CNT[2:0] ^ 3'b101;
  if (~|CNT[4:3])
    cntf[4:3] = CNT[4:3] ^ 2'b11;
end

always @* begin
  cnt_next = cntf - 16'h29;
end

always @(posedge CLK) begin
  if (~nRES) begin
    CNT <= 0;
  end
  else if (SCLKEN) begin
    CNT <= cnt_next;
  end

  if (mon_write_gc)
    CNT <= MON_DIN[15:0];
end


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_sel_gc;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  MON_ACK = 1'b1;
  mon_sel_gc = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'h48: begin                //``REG GLB_CTR
      mon_sel_gc = 1'b1;
      mon_dout[15:0] = CNT;
    end
    default: MON_ACK = 1'b0;
  endcase
end

assign mon_write = MON_READY & MON_WS & ~MON_VALID;
assign mon_write_gc = mon_write & mon_sel_gc;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK & MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end


endmodule
