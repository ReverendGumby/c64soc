module s_dsp_sp_timer
(
 input        CLK,
 input        CLKEN,
 input        TICK,
 input        EN,
 input        CLR,
 input [7:0]  T,
 output [3:0] C
 );

reg [7:0] ti;
reg [3:0] c;
reg       en_d;

initial begin
  c = 0;
  ti = 0;
end

wire en_negedge = !EN && en_d;

always @(posedge CLK) if (CLKEN) begin
  en_d <= EN;

  if (en_negedge || CLR)
    c <= 0;
  if (en_negedge)
    ti <= 0;

  if (TICK) begin
    if (EN) begin
      if (ti + 1 == T) begin
        ti <= 0;
        c <= c + 1;
      end
      else
        ti <= ti + 1;
    end
  end
end

assign C = c;

endmodule

