`timescale 1us / 1ns

module s_dsp_smp_if
  (
   input             CLK,
   input             nRES,
   input             HOLD,
   input             CCKEN,
   input             ICLKEN,

   // debug monitor
   input [7:0]       MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input             MON_WS,
   input [31:0]      MON_DIN,

   // S-SMP memory bus
   input [15:0]      A, // address bus (from S-SMP)
   input [7:0]       DI, // data bus (from S-SMP)
   output [7:0]      DO, // data bus (to S-SMP)
   input             RW, // R/W (from S-SMP)

   // SRAM memory bus
   input [7:0]       MDI, // SRAM data bus (from RAM)

   // Register file interface
   output [6:0]      SPDA,
   output            SPDDRS,
   output            SPDDWS,
   input [7:0]       SPDDO,
   output [7:0]      SPDDI
   );

wire      mon_write_smp0, mon_write_smp1;
wire      sp_sel;

// S-SMP address / data bus

reg [7:0] d_o_d, d_o, spdb;
reg       ccken_d;

wire [7:0] d_i = DI;
wire       d_oe = RW;

always @(posedge CLK) begin
  if (~HOLD)
    ccken_d <= CCKEN;

  if (mon_write_smp0)
    ccken_d <= MON_DIN[8];
end

always @(posedge CLK) begin
  if (~HOLD & ccken_d)
    d_o_d <= d_o;     // Hold data for S-SMP while we're accessing RAM

  if (mon_write_smp0)
    d_o_d <= MON_DIN[7:0];
end

always @* begin
  d_o = d_o_d;

  if (ccken_d) begin
    if (sp_sel)
      d_o = spdb;
    else
      d_o = MDI;
  end
end

assign DO = d_oe ? d_o : 8'hzz;

// S-DSP Access Registers @ $F2-$F3

assign sp_sel = A[15:4] == 'h00f && A[3:1] == 'b001;
wire sp_dspaddr = sp_sel & ~A[0]; // $F2
wire sp_dspdata = sp_sel &  A[0]; // $F3

reg [6:0] spda;
reg [7:0] spdd;
reg       spddrs, spddws, spdro;

initial begin
  spda = 0;
  spdro = 0;
end

always @(posedge CLK) begin
  if (~nRES) begin
    spda <= 0;
    spdro <= 0;
  end
  else begin
    if (ICLKEN & CCKEN & !RW) begin
      if (sp_dspaddr) begin
        spda <= d_i[6:0];
        spdro <= d_i[7];
      end
    end

    if (mon_write_smp1) begin
      {spdro, spda} <= MON_DIN[7:0];
    end
  end
end

always @* begin
  spdb = 8'h00;
  if (sp_dspaddr)
    spdb = {spdro, spda};
  else if (sp_dspdata)
    spdb = SPDDO;
end

always @* begin
  spdd = 8'hxx;
  spddrs = 0;
  spddws = 0;
  if (ccken_d & sp_dspdata) begin
    spddrs = RW;
    spddws = ~spdro & ~RW;
    spdd = d_i;
  end
end

assign SPDA = spda;
assign SPDDRS = spddrs;
assign SPDDWS = spddws;
assign SPDDI = spdd;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_sel_smp0, mon_sel_smp1;
wire       mon_write;

wire [7:0] spdrs = {spdro, spda};

always @* begin
  mon_dout = 32'b0;
  MON_ACK = 1'b1;
  mon_sel_smp0 = 1'b0;
  mon_sel_smp1 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'h45: begin                //``REG SMP0
      mon_sel_smp0 = 1'b1;
      mon_dout[7:0] = d_o_d;
      mon_dout[8] = ccken_d;
    end
    8'h46: begin                //``REG SMP1
      mon_sel_smp1 = 1'b1;
      mon_dout[7:0] = spdrs; //``FIELD SPDRS // SPC DSP Register Select
    end
    default: begin
      MON_ACK = 1'b0;
    end
  endcase
end

assign mon_write = MON_READY & MON_WS & ~MON_VALID;
assign mon_write_smp0 = mon_write & mon_sel_smp0;
assign mon_write_smp1 = mon_write & mon_sel_smp1;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK & MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end


endmodule
