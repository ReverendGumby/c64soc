`timescale 1us / 1ns

/* verilator lint_off IMPLICIT */
/* verilator lint_off WIDTH */

module s_dsp_echo_mixer
  (
   input                CLK,
   input                nRES,
   input                CKEN,

   // debug monitor
   input [7:0]          MON_SEL,
   output reg           MON_ACK,
   input                MON_READY,
   output reg [31:0]    MON_DOUT,
   output reg           MON_VALID,
   input                MON_WS,
   input [31:0]         MON_DIN,

   // Global registers
   input [7:0]          EON,
   input [7:0]          EFB,

   // Control signals
   input                VML,
   input                VMR,
   input                GOL,
   input                GOR,

   // Voice inputs
   input signed [15:0]  EFBINL,
   input signed [15:0]  EFBINR,
   input signed [15:0]  VOUTL,
   input signed [15:0]  VOUTR,

   // Sample output
   output signed [15:0] SOUTL,
   output signed [15:0] SOUTR
   );

`include "math.vh"

reg signed [25:0] omul;
reg signed [25:0] imac, mul1, mul2;
reg signed [16:0] sum;
reg signed [15:0] omac;
reg signed [24:0] suml, sumr;
reg signed [15:0] soutl, soutr;
reg [2:0]         vsel;
wire              eon;

wire              mon_write_emix0, mon_write_emix1, mon_write_emix2;

always @* begin
  omul = (mul1 * mul2) >>> 7;
  sum = imac[16:0] + omul[16:0];
  omac = clamp(sum);
end

initial begin
  suml = 25'd0;
  sumr = 25'd0;
  vsel = 3'd0;
  soutl = 16'd0;
  soutr = 16'd0;
end

assign eon = EON[vsel];

always @* begin
  mul1 = 25'd0;
  mul2 = 25'd1 << 7;            // negate the built-in right shift
  imac = 25'd0;
  if (VML & eon) begin
    mul1 = VOUTL;
    imac = suml;
  end
  if (VMR & eon) begin
    mul1 = VOUTR;
    imac = sumr;
  end
  if (GOL) begin
    mul1 = EFBINL;
    mul2 = $signed(EFB);
    imac = suml;
  end
  if (GOR) begin
    mul1 = EFBINR;
    mul2 = $signed(EFB);
    imac = sumr;
  end
end

always @(posedge CLK) begin
  if (~nRES) begin
    suml <= 25'd0;
    sumr <= 25'd0;
    vsel <= 3'd0;
    soutl <= 16'd0;
    soutr <= 16'd0;
  end
  else if (CKEN) begin
    if (VML) begin
      if (eon)
        suml <= omac;
    end
    if (VMR) begin
      if (eon)
        sumr <= omac;
      vsel <= vsel + 1'd1;
    end
    if (GOL) begin
      soutl <= {omac[15:1], 1'b0};
      suml <= 25'd0;
    end
    if (GOR) begin
      soutr <= {omac[15:1], 1'b0};
      sumr <= 25'd0;
      vsel <= 3'd0;
    end
  end

  if (mon_write_emix0) begin
    suml <= MON_DIN[24:0];
    vsel <= MON_DIN[27:25];
  end
  if (mon_write_emix1) begin
    sumr <= MON_DIN[24:0];
  end
  if (mon_write_emix2) begin
    soutl <= MON_DIN[15:0];
    soutr <= MON_DIN[31:16];
  end
end

assign SOUTL = soutl[15:0];
assign SOUTR = soutr[15:0];
  

//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout, mon_dout_emix;
reg        mon_ack_emix, mon_valid_emix;
reg        mon_sel_emix0, mon_sel_emix1, mon_sel_emix2;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  mon_ack_emix = 1'b1;
  mon_sel_emix0 = 1'b0;
  mon_sel_emix1 = 1'b0;
  mon_sel_emix2 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'hdb: begin                //``REG EMIX0
      mon_sel_emix0 = 1'b1;
      mon_dout[24:0] = suml;
      mon_dout[27:25] = vsel;
    end
    8'hdc: begin                //``REG EMIX1
      mon_sel_emix1 = 1'b1;
      mon_dout[24:0] = sumr;
    end
    8'hdd: begin                //``REG EMIX2
      mon_sel_emix2 = 1'b1;
      mon_dout[15:0] = soutl;
      mon_dout[31:16] = soutr;
    end
    default: mon_ack_emix = 1'b0;
  endcase
end

assign mon_write = MON_READY & MON_WS & ~mon_valid_emix;
assign mon_write_emix0 = mon_write & mon_sel_emix0;
assign mon_write_emix1 = mon_write & mon_sel_emix1;
assign mon_write_emix2 = mon_write & mon_sel_emix2;

always @(posedge CLK) begin
  mon_valid_emix <= 1'b0;
  if (mon_ack_emix & MON_READY) begin
    mon_valid_emix <= 1'b1;
    mon_dout_emix <= mon_dout;
  end
end

always @* begin
  MON_ACK = 1'b1;
  if (mon_ack_emix) begin
    MON_VALID = mon_valid_emix;
    MON_DOUT = mon_dout_emix;
  end
  else begin
    MON_ACK = 1'b0;
    MON_VALID = 1'b0;
    MON_DOUT = 32'b0;
  end
end


endmodule
