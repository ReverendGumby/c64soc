`timescale 1us / 1ns

/* verilator lint_off WIDTH */

module s_dsp_decbuf
  (
   input                CLK,
   input                nRES,
   input                CKEN,

   // debug monitor
   input [7:0]          MON_SEL,
   output reg           MON_ACK,
   input                MON_READY,
   output reg [31:0]    MON_DOUT,
   output reg           MON_VALID,
   input                MON_WS,
   input [31:0]         MON_DIN,

   input                IN_RESET,
   output               IN_FULL,
   output               IN_PLAY,
   input                IN_NEXT,
   input [2:0]          IN_VSEL,
   input signed [14:0]  IN_SAMP,

   output               OUT_PLAY,
   input [2:0]          OUT_VSEL,
   input [2:0]          OUT_NEXT,
   input [1:0]          OUT_SAMP_RS_DEC,
   input [3:0]          OUT_SAMP_RS_INT,
   output signed [14:0] OUT_SAMP
   );

reg [3:0]  deccnt [0:7];
reg [3:0]  intcnt [0:7];
reg [14:0] mem [0:(8*12-1)];
reg [7:0]  play;

reg [3:0]  deccnt_in, deccntp1;
reg [3:0]  intcnt_in, intcnt_out;
reg [14:0] mem_rbuf, mem_wbuf;
reg [6:0]  mem_rptr, mem_wptr;
reg        mem_we;
wire [6:0] in_idx;

wire       mon_write_decbuf0, mon_write_decbuf1, mon_write_decbuf2,
           mon_write_mem;
wire [7:0] mon_idx_mem;

integer    i, j;

function [6:0] memidx(input [2:0] v, input [3:0] i);
  memidx = {i, v};
endfunction

function [3:0] bufidxpn(input [3:0] idx, input [3:0] add);
reg [4:0] idxp;
  begin
    idxp = idx + add;
    bufidxpn = idxp % 4'd12;
  end
endfunction

initial begin
  for (i = 0; i < 8; i = i + 1) begin
    deccnt[i] = 4'd0;
    intcnt[i] = 4'd0;
    for (j = 0; j < 12; j = j + 1) begin
      mem[memidx(i, j)] = 15'd0;
    end
    play = 8'b0;
  end
end

always @* begin
  deccnt_in = deccnt[IN_VSEL];
  intcnt_in = intcnt[IN_VSEL];

  intcnt_out = intcnt[OUT_VSEL];
end

assign in_idx = memidx(IN_VSEL, deccnt_in);

always @(posedge CLK) begin
  if (~nRES) begin
    for (i = 0; i < 8; i = i + 1) begin
      deccnt[i] <= 4'd0;
      intcnt[i] <= 4'd0;
      play <= 8'b0;
    end
  end
  else if (CKEN) begin
    if (IN_RESET) begin
      deccnt[IN_VSEL] <= 4'd0;
      intcnt[IN_VSEL] <= 4'd0;
      play[IN_VSEL] <= 1'b0;
    end

    if (IN_NEXT) begin
      deccnt[IN_VSEL] <= deccntp1;

      if (~play[IN_VSEL] && (deccntp1 == 4'd0))
        play[IN_VSEL] <= 1'b1;
    end

    if (|OUT_NEXT) begin
      intcnt[OUT_VSEL] <= bufidxpn(intcnt_out, OUT_NEXT);
    end
  end

  if (mon_write_decbuf0)
    play <= MON_DIN[7:0];
  if (mon_write_decbuf1) begin
    deccnt[0] <= MON_DIN[3:0];
    deccnt[1] <= MON_DIN[7:4];
    deccnt[2] <= MON_DIN[11:8];
    deccnt[3] <= MON_DIN[15:12];
    deccnt[4] <= MON_DIN[19:16];
    deccnt[5] <= MON_DIN[23:20];
    deccnt[6] <= MON_DIN[27:24];
    deccnt[7] <= MON_DIN[31:28];
  end
  if (mon_write_decbuf2) begin
    intcnt[0] <= MON_DIN[3:0];
    intcnt[1] <= MON_DIN[7:4];
    intcnt[2] <= MON_DIN[11:8];
    intcnt[3] <= MON_DIN[15:12];
    intcnt[4] <= MON_DIN[19:16];
    intcnt[5] <= MON_DIN[23:20];
    intcnt[6] <= MON_DIN[27:24];
    intcnt[7] <= MON_DIN[31:28];
  end
end

always @* begin
  deccntp1 = bufidxpn(deccnt_in, 4'd1);
end

always @* begin
  mem_rptr = out_idx;
  if (mon_sel_mem)
    mem_rptr = mon_idx_mem[6:0];
end

always @* begin
  mem_wptr = 0;
  mem_wbuf = 0;
  mem_we = 0;

  if (nRES & CKEN & IN_NEXT) begin
    mem_wptr = in_idx;
    mem_wbuf = IN_SAMP;
    mem_we = 1'b1;
  end
  else if (mon_write_mem) begin
    mem_wptr = mon_idx_mem[6:0];
    mem_wbuf = MON_DIN[14:0];
    mem_we = 1'b1;
  end
end  

always @(posedge CLK) begin
  mem_rbuf <= mem[mem_rptr];
  if (mem_we)
    mem[mem_wptr] <= mem_wbuf;
end

assign IN_FULL = deccnt_in[3:2] == intcnt_in[3:2];
assign IN_PLAY = play[IN_VSEL];
assign OUT_PLAY = play[OUT_VSEL];


//////////////////////////////////////////////////////////////////////
// Sample output to decoder or interpolator

reg [14:0] out_samp;
reg [6:0]  out_idx;

always @* begin
  out_idx = 4'dx;
  if (OUT_SAMP_RS_DEC[0])
    out_idx = memidx(IN_VSEL, bufidxpn(deccnt_in, 4'd10));
  else if (OUT_SAMP_RS_DEC[1])
    out_idx = memidx(IN_VSEL, bufidxpn(deccnt_in, 4'd11));
  else if (OUT_SAMP_RS_INT[0])
    out_idx = memidx(OUT_VSEL, bufidxpn(intcnt_out, 4'd0));
  else if (OUT_SAMP_RS_INT[1])
    out_idx = memidx(OUT_VSEL, bufidxpn(intcnt_out, 4'd1));
  else if (OUT_SAMP_RS_INT[2])
    out_idx = memidx(OUT_VSEL, bufidxpn(intcnt_out, 4'd2));
  else if (OUT_SAMP_RS_INT[3])
    out_idx = memidx(OUT_VSEL, bufidxpn(intcnt_out, 4'd3));
end

always @(posedge CLK) begin
  if (CKEN) begin
    if (|OUT_SAMP_RS_DEC | |OUT_SAMP_RS_INT)
      out_samp <= mem_rbuf;
  end

  if (mon_write_decbuf0)
    out_samp <= MON_DIN[22:8];
end

assign OUT_SAMP = out_samp;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout, mon_dout_decbuf;
reg        mon_ack_decbuf, mon_valid_decbuf;
reg        mon_sel_decbuf0, mon_sel_decbuf1, mon_sel_decbuf2;
wire       mon_sel_mem;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  mon_ack_decbuf = 1'b1;
  mon_sel_decbuf0 = 1'b0;
  mon_sel_decbuf1 = 1'b0;
  mon_sel_decbuf2 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'h5c: begin                //``REG DECBUF0
      mon_sel_decbuf0 = 1'b1;
      mon_dout[7:0] = play;
      mon_dout[22:8] = out_samp;
    end
    8'h5d: begin                //``REG DECBUF1
      mon_sel_decbuf1 = 1'b1;
      mon_dout[3:0] = deccnt[0]; //``FIELD DECCNT0
      mon_dout[7:4] = deccnt[1]; //``FIELD DECCNT1
      mon_dout[11:8] = deccnt[2]; //``FIELD DECCNT2
      mon_dout[15:12] = deccnt[3]; //``FIELD DECCNT3
      mon_dout[19:16] = deccnt[4]; //``FIELD DECCNT4
      mon_dout[23:20] = deccnt[5]; //``FIELD DECCNT5
      mon_dout[27:24] = deccnt[6]; //``FIELD DECCNT6
      mon_dout[31:28] = deccnt[7]; //``FIELD DECCNT7
    end
    8'h5e: begin                //``REG DECBUF2
      mon_sel_decbuf2 = 1'b1;
      mon_dout[3:0] = intcnt[0]; //``FIELD INTCNT0
      mon_dout[7:4] = intcnt[1]; //``FIELD INTCNT1
      mon_dout[11:8] = intcnt[2]; //``FIELD INTCNT2
      mon_dout[15:12] = intcnt[3]; //``FIELD INTCNT3
      mon_dout[19:16] = intcnt[4]; //``FIELD INTCNT4
      mon_dout[23:20] = intcnt[5]; //``FIELD INTCNT5
      mon_dout[27:24] = intcnt[6]; //``FIELD INTCNT6
      mon_dout[31:28] = intcnt[7]; //``FIELD INTCNT7
    end
    default: begin
      if (mon_sel_mem)
        mon_dout[14:0] = mem_rbuf; //``NO_FIELD
      else
        mon_ack_decbuf = 1'b0;
    end
  endcase
end

assign mon_sel_mem = (MON_SEL >= 8'h5f && MON_SEL <= 8'hbe); //``REG_ARRAY DECBUF_MEM
assign mon_idx_mem = MON_SEL - 8'h5f;

assign mon_write = MON_READY & MON_WS & ~mon_valid_decbuf;
assign mon_write_decbuf0 = mon_write & mon_sel_decbuf0;
assign mon_write_decbuf1 = mon_write & mon_sel_decbuf1;
assign mon_write_decbuf2 = mon_write & mon_sel_decbuf2;
assign mon_write_mem = mon_write & mon_sel_mem;

always @(posedge CLK) begin
  mon_valid_decbuf <= 1'b0;
  if (mon_ack_decbuf & MON_READY) begin
    mon_valid_decbuf <= 1'b1;
    mon_dout_decbuf <= mon_dout;
  end
end

always @* begin
  MON_ACK = 1'b1;
  if (mon_ack_decbuf) begin
    MON_VALID = mon_valid_decbuf;
    MON_DOUT = mon_dout_decbuf;
  end
  else begin
    MON_ACK = 1'b0;
    MON_VALID = 1'b0;
    MON_DOUT = 32'b0;
  end
end


endmodule
