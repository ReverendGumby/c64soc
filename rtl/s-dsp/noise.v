`timescale 1us / 1ns

/* verilator lint_off WIDTH */

module s_dsp_noise
  (
   input                CLK,
   input                nRES,
   input                CKEN,

   // debug monitor
   input [7:0]          MON_SEL,
   output reg           MON_ACK,
   input                MON_READY,
   output reg [31:0]    MON_DOUT,
   output reg           MON_VALID,
   input                MON_WS,
   input [31:0]         MON_DIN,

   input [15:0]         GLB_CNT,
   input [4:0]          FLGN,
   input                SCLKEN,
   output signed [14:0] OUT
   );

reg [14:0] lfsr;
wire ev;
wire mon_write_noise;

s_dsp_ev_ctr ev_ctr(.CLK(CLK), .GLB_CNT(GLB_CNT), .RATE(FLGN), .EV(ev));

initial begin
  lfsr = 15'h4000;
end

always @(posedge CLK) begin
  if (~nRES) begin
    lfsr <= 15'h4000;
  end
  else if (CKEN) begin
    if (SCLKEN & ev) begin
      lfsr <= {lfsr[1] ^ lfsr[0], lfsr[14:1]};
    end
  end

  if (mon_write_noise)
    lfsr <= MON_DIN[14:0];
end

assign OUT = lfsr;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_sel_noise;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  MON_ACK = 1'b1;
  mon_sel_noise = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'h4c: begin                //``REG NOISE
      mon_sel_noise = 1'b1;
      mon_dout[14:0] = lfsr;
    end
    default: MON_ACK = 1'b0;
  endcase
end

assign mon_write = MON_READY & MON_WS & ~MON_VALID;
assign mon_write_noise = mon_write & mon_sel_noise;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK & MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end


endmodule
