`timescale 1us / 1ns

/* verilator lint_off IMPLICIT */
/* verilator lint_off WIDTH */

module s_dsp_echo_fir
  (
   input                CLK,
   input                nRES,
   input                CKEN,

   // debug monitor
   input [7:0]          MON_SEL,
   output reg           MON_ACK,
   input                MON_READY,
   output reg [31:0]    MON_DOUT,
   output reg           MON_VALID,
   input                MON_WS,
   input [31:0]         MON_DIN,

   // Control signals
   input                SIL, // input, left
   input                SIR, // input, right
   input                CL, // load coefficient
   input [2:0]          CS, // coefficient select

   // Coefficient, sample inputs
   input [7:0]          FFC,
   input signed [14:0]  SINL,
   input signed [14:0]  SINR,

   // Sample outputs
   output signed [15:0] SOUTL,
   output signed [15:0] SOUTR
   );

`include "math.vh"

reg signed [16:0] omacl, omacr;
reg signed [14:0] sl [0:7], sr [0:7];
reg signed [16:0] suml, sumr;
reg signed [15:0] soutl, soutr;
reg [2:0]         sp;           // ptr to oldest sample
wire [2:0]        rsp;          // read ptr
wire [2:0]        wsp;          // write ptr
wire              ff, go;
integer           i;

wire [10:0]       mon_write_efir;

// sl, sr buffer layout
// sp increments
// 0 S[t+1]  <- first sp @ t+0
// 1 S[t+2]  <- first sp @ t+1
// 2 S[t+3]
// 3 S[t+4]
// 4 S[t+5]
// 5 S[t+6]
// 6 S[t+7]
// 7 S[t+0]

assign rsp = sp + CS;
assign wsp = sp - 1'd1;

function [16:0] mac
  (
   input signed [16:0] sum,
   input signed [7:0]  c,
   input signed [14:0] s,
   input               ff,
   input               go
   );

reg signed [25:0] imac, mul1, mul2;
reg signed [25:0] omul;

  begin
    mul1 = 25'd0;
    mul2 = 25'd1 << 6;            // negate the built-in right shift
    imac = 25'd0;
    if (ff) begin
      mul1 = c;
      mul2 = s;
      imac = sum;
    end
    if (go) begin
      imac = clamp(sum);
    end

    omul = (mul1 * mul2) >>> 6;
    mac = imac[16:0] + omul[16:0];
  end
endfunction

always @*
  omacl = mac(suml, FFC, sl[rsp], ff, go);

always @*
  omacr = mac(sumr, FFC, sr[rsp], ff, go);

assign ff = CL;
assign go = CL & (CS == 3'd7);

initial begin
  suml = 17'd0;
  sumr = 17'd0;
  soutl = 16'd0;
  soutr = 16'd0;
  for (i = 0; i < 8; i = i + 1) begin
    sl[i] = 15'd0;
    sr[i] = 15'd0;
  end
  sp = 3'd0;
end

always @(posedge CLK) begin
  if (~nRES) begin
    suml <= 17'd0;
    sumr <= 17'd0;
    soutl <= 16'd0;
    soutr <= 16'd0;
    for (i = 0; i < 8; i = i + 1) begin
      sl[i] <= 15'd0;
      sr[i] <= 15'd0;
    end
    sp <= 3'd0;
  end
  else if (CKEN) begin
    if (SIL) begin
      sl[wsp] <= SINL;
    end
    if (SIR) begin
      sr[wsp] <= SINR;
    end
    if (ff & ~go) begin
      suml <= omacl;
      sumr <= omacr;
    end
    if (go) begin
      soutl <= {omacl[15:1], 1'b0};
      soutr <= {omacr[15:1], 1'b0};
      suml <= 17'd0;
      sumr <= 17'd0;
      sp <= sp + 1'd1;
    end
  end

  if (mon_write_efir[0]) begin
    suml <= MON_DIN[16:0];
    sp <= MON_DIN[19:17];
  end
  if (mon_write_efir[1]) begin
    sumr <= MON_DIN[16:0];
  end
  if (mon_write_efir[2]) begin
    sl[0] <= MON_DIN[14:0];
    sl[1] <= MON_DIN[30:16];
  end
  if (mon_write_efir[3]) begin
    sl[2] <= MON_DIN[14:0];
    sl[3] <= MON_DIN[30:16];
  end
  if (mon_write_efir[4]) begin
    sl[4] <= MON_DIN[14:0];
    sl[5] <= MON_DIN[30:16];
  end
  if (mon_write_efir[5]) begin
    sl[6] <= MON_DIN[14:0];
    sl[7] <= MON_DIN[30:16];
  end
  if (mon_write_efir[6]) begin
    sr[0] <= MON_DIN[14:0];
    sr[1] <= MON_DIN[30:16];
  end
  if (mon_write_efir[7]) begin
    sr[2] <= MON_DIN[14:0];
    sr[3] <= MON_DIN[30:16];
  end
  if (mon_write_efir[8]) begin
    sr[4] <= MON_DIN[14:0];
    sr[5] <= MON_DIN[30:16];
  end
  if (mon_write_efir[9]) begin
    sr[6] <= MON_DIN[14:0];
    sr[7] <= MON_DIN[30:16];
  end
  if (mon_write_efir[10]) begin
    soutl <= MON_DIN[15:0];
    soutr <= MON_DIN[31:16];
  end
end

assign SOUTL = soutl[15:0];
assign SOUTR = soutr[15:0];
  

//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout, mon_dout_efir;
reg        mon_ack_efir, mon_valid_efir;
reg [10:0] mon_sel_efir;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  mon_ack_efir = 1'b1;
  mon_sel_efir = 13'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'hde: begin                //``REG EFIR0
      mon_sel_efir[0] = 1'b1;   //``NO_FIELD
      mon_dout[16:0] = suml;
      mon_dout[19:17] = sp;
    end
    8'hdf: begin                //``REG EFIR1
      mon_sel_efir[1] = 1'b1;   //``NO_FIELD
      mon_dout[16:0] = sumr;
    end
    8'he0: begin                //``REG EFIR2
      mon_sel_efir[2] = 1'b1;   //``NO_FIELD
      mon_dout[14:0] = sl[0];   //``FIELD SL0
      mon_dout[30:16] = sl[1];  //``FIELD SL1
    end
    8'he1: begin                //``REG EFIR3
      mon_sel_efir[3] = 1'b1;   //``NO_FIELD
      mon_dout[14:0] = sl[2];   //``FIELD SL2
      mon_dout[30:16] = sl[3];  //``FIELD SL3
    end
    8'he2: begin                //``REG EFIR4
      mon_sel_efir[4] = 1'b1;   //``NO_FIELD
      mon_dout[14:0] = sl[4];   //``FIELD SL4
      mon_dout[30:16] = sl[5];  //``FIELD SL5
    end
    8'he3: begin                //``REG EFIR5
      mon_sel_efir[5] = 1'b1;   //``NO_FIELD
      mon_dout[14:0] = sl[6];   //``FIELD SL6
      mon_dout[30:16] = sl[7];  //``FIELD SL7
    end
    8'he4: begin                //``REG EFIR6
      mon_sel_efir[6] = 1'b1;   //``NO_FIELD
      mon_dout[14:0] = sr[0];   //``FIELD SR0
      mon_dout[30:16] = sr[1];  //``FIELD SR1
    end
    8'he5: begin                //``REG EFIR7
      mon_sel_efir[7] = 1'b1;   //``NO_FIELD
      mon_dout[14:0] = sr[2];   //``FIELD SR2
      mon_dout[30:16] = sr[3];  //``FIELD SR3
    end
    8'he6: begin                //``REG EFIR8
      mon_sel_efir[8] = 1'b1;   //``NO_FIELD
      mon_dout[14:0] = sr[4];   //``FIELD SR4
      mon_dout[30:16] = sr[5];  //``FIELD SR5
    end
    8'he7: begin                //``REG EFIR9
      mon_sel_efir[9] = 1'b1;   //``NO_FIELD
      mon_dout[14:0] = sr[6];   //``FIELD SR6
      mon_dout[30:16] = sr[7];  //``FIELD SR7
    end
    8'he8: begin                //``REG EFIR10
      mon_sel_efir[10] = 1'b1;  //``NO_FIELD
      mon_dout[15:0] = soutl;
      mon_dout[31:16] = soutr;
    end
    default: mon_ack_efir = 1'b0;
  endcase
end

assign mon_write = MON_READY & MON_WS & ~mon_valid_efir;
assign mon_write_efir = {11{mon_write}} & mon_sel_efir;

always @(posedge CLK) begin
  mon_valid_efir <= 1'b0;
  if (mon_ack_efir & MON_READY) begin
    mon_valid_efir <= 1'b1;
    mon_dout_efir <= mon_dout;
  end
end

always @* begin
  MON_ACK = 1'b1;
  if (mon_ack_efir) begin
    MON_VALID = mon_valid_efir;
    MON_DOUT = mon_dout_efir;
  end
  else begin
    MON_ACK = 1'b0;
    MON_VALID = 1'b0;
    MON_DOUT = 32'b0;
  end
end


endmodule
