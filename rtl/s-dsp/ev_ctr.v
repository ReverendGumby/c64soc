`timescale 1us / 1ns

// Uses the Mednafen algorithm described in Anomie's S-DSP Doc

module s_dsp_ev_ctr
  (
   input        CLK,
   input [15:0] GLB_CNT,
   input [4:0]  RATE,
   output       EV
   );

reg [15:0] mask_tbl [0:31];
reg [15:0] xor_tbl [0:31];

reg [15:0] ev_mask, ev_xor;

initial begin
  mask_tbl[00] = 16'h0000;
  mask_tbl[01] = 16'hFFE0;
  mask_tbl[02] = 16'h3FF8;
  mask_tbl[03] = 16'h1FE7;
  mask_tbl[04] = 16'h7FE0;
  mask_tbl[05] = 16'h1FF8;
  mask_tbl[06] = 16'h0FE7;
  mask_tbl[07] = 16'h3FE0;
  mask_tbl[08] = 16'h0FF8;
  mask_tbl[09] = 16'h07E7;
  mask_tbl[10] = 16'h1FE0;
  mask_tbl[11] = 16'h07F8;
  mask_tbl[12] = 16'h03E7;
  mask_tbl[13] = 16'h0FE0;
  mask_tbl[14] = 16'h03F8;
  mask_tbl[15] = 16'h01E7;
  mask_tbl[16] = 16'h07E0;
  mask_tbl[17] = 16'h01F8;
  mask_tbl[18] = 16'h00E7;
  mask_tbl[19] = 16'h03E0;
  mask_tbl[20] = 16'h00F8;
  mask_tbl[21] = 16'h0067;
  mask_tbl[22] = 16'h01E0;
  mask_tbl[23] = 16'h0078;
  mask_tbl[24] = 16'h0027;
  mask_tbl[25] = 16'h00E0;
  mask_tbl[26] = 16'h0038;
  mask_tbl[27] = 16'h0007;
  mask_tbl[28] = 16'h0060;
  mask_tbl[29] = 16'h0018;
  mask_tbl[30] = 16'h0020;
  mask_tbl[31] = 16'h0000;

  xor_tbl[00] = 16'hFFFF;
  xor_tbl[01] = 16'h0000;
  xor_tbl[02] = 16'h3E08;
  xor_tbl[03] = 16'h1D04;
  xor_tbl[04] = 16'h0000;
  xor_tbl[05] = 16'h1E08;
  xor_tbl[06] = 16'h0D04;
  xor_tbl[07] = 16'h0000;
  xor_tbl[08] = 16'h0E08;
  xor_tbl[09] = 16'h0504;
  xor_tbl[10] = 16'h0000;
  xor_tbl[11] = 16'h0608;
  xor_tbl[12] = 16'h0104;
  xor_tbl[13] = 16'h0000;
  xor_tbl[14] = 16'h0208;
  xor_tbl[15] = 16'h0104;
  xor_tbl[16] = 16'h0000;
  xor_tbl[17] = 16'h0008;
  xor_tbl[18] = 16'h0004;
  xor_tbl[19] = 16'h0000;
  xor_tbl[20] = 16'h0008;
  xor_tbl[21] = 16'h0004;
  xor_tbl[22] = 16'h0000;
  xor_tbl[23] = 16'h0008;
  xor_tbl[24] = 16'h0004;
  xor_tbl[25] = 16'h0000;
  xor_tbl[26] = 16'h0008;
  xor_tbl[27] = 16'h0004;
  xor_tbl[28] = 16'h0000;
  xor_tbl[29] = 16'h0008;
  xor_tbl[30] = 16'h0000;
  xor_tbl[31] = 16'h0000;
end

always @(posedge CLK)
  ev_mask <= mask_tbl[RATE];

always @(posedge CLK)
  ev_xor <= xor_tbl[RATE];

assign EV = ((GLB_CNT & ev_mask) ^ ev_xor) == 0;

endmodule
