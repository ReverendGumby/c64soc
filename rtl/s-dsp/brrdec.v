`timescale 1us / 1ns

/* verilator lint_off WIDTH */

module s_dsp_brrdec
  (
   input                CLK,
   input                nRES,
   input                CKEN,

   // debug monitor
   input [7:0]          MON_SEL,
   output reg           MON_ACK,
   input                MON_READY,
   output reg [31:0]    MON_DOUT,
   output reg           MON_VALID,
   input                MON_WS,
   input [31:0]         MON_DIN,

   // Global registers
   input [7:0]          KON,
   input [7:0]          DIR,

   // Voice pipeline control signals
   input [2:0]          VSEL,

   // Voice registers
   input [7:0]          SRCN,

   // Memory interface
   output [15:0]        MA,
   output               MRS,
   input [7:0]          MDO,
   
   // Decode control signals
   input                RD_SRCNL,
   input                RD_SRCNH,
   input                RD_HDR,
   input                RD_DTA0,
   input                RD_DTA1,

   // Decode outputs
   output               BEND,

   output               OUT_RESET,
   input                OUT_FULL,
   input                OUT_PLAY,
   output               OUT_NEXT,
   output signed [14:0] OUT_SAMP,
   output [1:0]         OUT_SAMPMX_RS,
   input signed [14:0]  OUT_SAMPMX
   );

`include "math.vh"

reg [7:0] hdr, dat;
wire      rd_dta;
reg       dec_en, dec_loaded;
wire      dec_active, dec_reset;
wire      loop, bend;
reg [1:0] decs;
reg       rd_dta1;

wire      mon_write_brrdec0, mon_write_brrdec1;


//////////////////////////////////////////////////////////////////////
// Context store

localparam CTX_WIDTH = 4 + 1 + 1 + 1 +
           1 + 16;

reg        ctx_swap;
wire [CTX_WIDTH-1:0] ctx_in, ctx_out;
wire [3:0]           ctx_bbcnt;
wire                 ctx_srcn0_load, ctx_srcn1_load, ctx_srcn_inc;
wire                 ctx_dec_loaded;
wire [15:0]          ctx_srcnc;

wire [31:0]          mon_dout_ctx;
wire                 mon_ack_ctx;
wire                 mon_valid_ctx;

always @*
  ctx_swap = decs[1] & rd_dta1;

assign ctx_in = { bbcnt, srcn0_load, srcn1_load, srcn_inc,
                  dec_loaded, srcnc };
assign { ctx_bbcnt, ctx_srcn0_load, ctx_srcn1_load, ctx_srcn_inc,
         ctx_dec_loaded, ctx_srcnc } = ctx_out;

s_dsp_context_mem #(.WIDTH(CTX_WIDTH), .MON_BASE(8'h51)) ctx
  (
   .CLK(CLK), .nRES(nRES), .CKEN(CKEN),
   .MON_SEL(MON_SEL), .MON_ACK(mon_ack_ctx), .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_ctx), .MON_VALID(mon_valid_ctx),
   .MON_WS(MON_WS), .MON_DIN(MON_DIN),
   .VSEL(VSEL),
   .SWAP(ctx_swap),
   .IN(ctx_in), .OUT(ctx_out)
   );


//////////////////////////////////////////////////////////////////////
// Block pointer loader

reg [3:0]  bbcnt, bbcntp1;
reg        srcn0_load, srcn1_load, srcn_inc;

assign rd_dta = RD_DTA0 | RD_DTA1;

initial begin
  bbcnt = 4'd1;
  srcn0_load = 1'b0;
  srcn1_load = 1'b0;
  srcn_inc = 1'b0;
  rd_dta1 = 1'b0;
end

wire kon = KON[VSEL];

always @(posedge CLK) begin
  if (~nRES) begin
    bbcnt <= 4'd1;
    srcn0_load <= 1'b0;
    srcn1_load <= 1'b0;
    srcn_inc <= 1'b0;
    rd_dta1 <= 1'b0;
  end
  else if (CKEN) begin
    rd_dta1 <= (rd_dta1 & ~RD_DTA0) | RD_DTA1;

    if (ctx_swap) begin
      bbcnt <= ctx_bbcnt;
      srcn0_load <= ctx_srcn0_load;
      srcn1_load <= ctx_srcn1_load;
      srcn_inc <= ctx_srcn_inc;
    end
    if (RD_SRCNH) begin
      srcn_inc <= 1'b0;
      if (srcn0_load | srcn1_load) begin
        srcn0_load <= 1'b0;
        srcn1_load <= 1'b0;
        bbcnt <= 4'd1;
      end
    end
    if (dec_active & rd_dta) begin
      bbcnt <= bbcntp1;
    end
    if (RD_DTA1) begin
      if (kon)
        srcn0_load <= 1'b1;
      else if (dec_active && bbcnt == 4'd8) begin
        if (bend)
          srcn1_load <= 1'b1;
        else
          srcn_inc <= 1'b1;
      end
    end
  end

  if (mon_write_brrdec0) begin
    bbcnt <= MON_DIN[3:0];
    srcn0_load <= MON_DIN[4];
    srcn1_load <= MON_DIN[5];
    srcn_inc <= MON_DIN[6];
    rd_dta1 <= MON_DIN[7];
  end
end

always @* begin
  bbcntp1 = bbcnt + 1'd1;
  if (bbcntp1 == 4'd9)
    bbcntp1 = 4'd1;
end


//////////////////////////////////////////////////////////////////////
// Block buffer loader

wire [3:0] left_shift;
wire [1:0] dec_flt;

always @(posedge CLK) begin
  if (~nRES) begin
    hdr <= 0;
    dat <= 0;
  end
  else if (CKEN) begin
    if (RD_HDR)
      hdr <= MDO;
    if (rd_dta)
      dat <= MDO;
  end

  if (mon_write_brrdec0) begin
    hdr <= MON_DIN[15:8];
    dat <= MON_DIN[23:16];
  end
end

assign left_shift = hdr[7:4];
assign dec_flt = hdr[3:2];
assign loop = hdr[1];
assign bend = hdr[0];

//assign PLAY = dec_loaded;
assign BEND = OUT_PLAY & bend & ~loop;


//////////////////////////////////////////////////////////////////////
// BRR shifter

reg signed [16:0] brrss;        // shifted sample
reg signed [15:0] brrs;         // encoded sample
wire              sp;           // sample is which nibble

always @* begin
  brrs = $signed(dat[(7 - 4 * sp)-:4]);

  if (left_shift <= 4'd12)
    brrss = (brrs << left_shift) >>> 1;
  else
    brrss = $signed({brrs[3], 11'b0});
end  


//////////////////////////////////////////////////////////////////////
// BRR filter

reg signed [16:0] brrfs1, brrfs2;
reg signed [15:0] brrfsc;
reg signed [14:0] brrfsm1, brrfsm2;
reg signed [7:0]  m1, m2;
reg [1:0]         s1, s2;

function [16:0] samp_muldiv;
input signed [14:0] x;
input signed [7:0]  m;
input [1:0]         s;
reg signed [23:0]   ex, exm;
reg [3:0]           sf;
  begin
    ex = x;
    exm = ex * m;
    sf = s + 3'd4;
    samp_muldiv = exm >>> sf;
  end
endfunction

always @* begin
  m1 = 8'sdx;
  m2 = 8'sdx;
  s1 = 2'dx;
  s2 = 2'dx;
  case (dec_flt)
    2'd0: { m1, s1, m2, s2 } = { 8'sd0,   2'd0, 8'sd0,  2'd0 };
    2'd1: { m1, s1, m2, s2 } = { 8'sd15,  2'd0, 8'sd0,  2'd0 };
    2'd2: { m1, s1, m2, s2 } = { 8'sd61,  2'd1, 8'sd15, 2'd0 };
    2'd3: { m1, s1, m2, s2 } = { 8'sd115, 2'd2, 8'sd13, 2'd0 };
    default: ;
  endcase
end

always @(posedge CLK) begin
  brrfs1 <= brrss + samp_muldiv(brrfsm1, m1, s1);
  brrfs2 <= brrfs1 - samp_muldiv(brrfsm2, m2, s2);
  brrfsc <= clamp(brrfs2);
end

reg rd_brrfsm1, rd_brrfsm2;
reg lt_brrfsm1, lt_brrfsm2;

always @(posedge CLK) begin
  if (CKEN)
    rd_brrfsm1 <= RD_SRCNH;

  if (mon_write_brrdec0)
    rd_brrfsm1 <= MON_DIN[24];
end

always @* rd_brrfsm2 = RD_HDR;
always @* lt_brrfsm1 = rd_brrfsm2;
always @* lt_brrfsm2 = RD_DTA0;

assign OUT_SAMPMX_RS = { rd_brrfsm2, rd_brrfsm1 };

always @(posedge CLK) begin
  if (CKEN) begin
    if (lt_brrfsm1)
      brrfsm1 <= OUT_SAMPMX;
    else if (lt_brrfsm2) begin
      brrfsm1 <= OUT_SAMPMX;
      brrfsm2 <= brrfsm1;
    end
    else if (|decs) begin
/* -----\/----- EXCLUDED -----\/-----
      if (dec_active && VSEL == 3'd7)
        $display("brrs=%d ls=%d brrss=%d brrfs=%d ", brrs, left_shift, brrss, brrfsc);
 -----/\----- EXCLUDED -----/\----- */
      brrfsm1 <= OUT_SAMP;
      brrfsm2 <= brrfsm1;
    end
  end

  if (mon_write_brrdec0) begin
    brrfsm1 <= MON_DIN[25];
    brrfsm2 <= MON_DIN[26];
  end
end


//////////////////////////////////////////////////////////////////////
// Decoded sample ring buffer output

reg        dec_buf_full;
wire       dec_adv;

always @(posedge CLK) if (CKEN)
  decs <= {decs[0], rd_dta};

always @(posedge CLK) begin
  if (~nRES) begin
    dec_buf_full <= 1'b0;
  end
  else if (CKEN) begin
    if (RD_SRCNL)
      dec_buf_full <= dec_loaded & OUT_FULL;
  end

  if (mon_write_brrdec0)
    dec_buf_full <= MON_DIN[27];
end

always @* dec_loaded = OUT_PLAY;

assign sp = decs[1];
assign dec_active = ~dec_buf_full;
assign dec_reset = RD_SRCNL & kon;
assign dec_adv = dec_active & |decs;
assign OUT_NEXT = dec_adv;
assign OUT_RESET = dec_reset;
assign OUT_SAMP = brrfsc[14:0];  // truncate very -ve / +ve values


//////////////////////////////////////////////////////////////////////
// Memory access

reg [15:0] srcnc;
reg [15:0] ma;
reg [15:2] ste;
reg        mrs;

initial begin
  srcnc = 0;
end

always @* ste = {DIR, 6'b0} + {6'b0, SRCN};

always @* begin
  mrs = 1'b0;
  ma = 16'hx;

  if (nRES) begin
    mrs = 1'b1;

    if (RD_SRCNL)         ma = {ste, srcn1_load, 1'b0};
    else if (RD_SRCNH)    ma = {ste, srcn1_load, 1'b1};
    else if (RD_HDR)      ma = srcnc;
    else if (RD_DTA0)     ma = srcnc + bbcnt;
    else if (RD_DTA1)     ma = srcnc + bbcnt;
    else
      mrs = 1'b0;
  end
end

always @(posedge CLK) begin
  if (~nRES) begin
    srcnc <= 0;
  end
  else if (CKEN) begin
    if (ctx_swap) begin
      srcnc <= ctx_srcnc;
    end

    if (srcn0_load | srcn1_load) begin
      if (RD_SRCNL)
        srcnc[7:0] <= MDO;
      if (RD_SRCNH)
        srcnc[15:8] <= MDO;
    end
    if (RD_SRCNH & srcn_inc)
      srcnc <= srcnc + 4'd9;
  end

  if (mon_write_brrdec1)
    srcnc <= MON_DIN[15:0];
end

assign MA = ma;
assign MRS = mrs;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout, mon_dout_brrdec;
reg        mon_ack_brrdec, mon_valid_brrdec;
reg        mon_sel_brrdec0, mon_sel_brrdec1;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  mon_ack_brrdec = 1'b1;
  mon_sel_brrdec0 = 1'b0;
  mon_sel_brrdec1 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'h5a: begin                //``REG BRRDEC0
      mon_sel_brrdec0 = 1'b1;
      mon_dout[3:0] = bbcnt;
      mon_dout[4] = srcn0_load;
      mon_dout[5] = srcn1_load;
      mon_dout[6] = srcn_inc;
      mon_dout[7] = rd_dta1;
      mon_dout[15:8] = hdr;
      mon_dout[23:16] = dat;
      mon_dout[24] = rd_brrfsm1;
      mon_dout[25] = brrfsm1;
      mon_dout[26] = brrfsm2;
      mon_dout[27] = dec_buf_full;
    end
    8'h5b: begin                //``REG BRRDEC1
      mon_sel_brrdec1 = 1'b1;
      mon_dout[15:0] = srcnc;
    end
    default: mon_ack_brrdec = 1'b0;
  endcase
end

assign mon_ack_ctx = (MON_SEL >= 8'h51 && MON_SEL <= 8'h58); //``REG_ARRAY BRRDEC_CTX

assign mon_write = MON_READY & MON_WS & ~mon_valid_brrdec;
assign mon_write_brrdec0 = mon_write & mon_sel_brrdec0;
assign mon_write_brrdec1 = mon_write & mon_sel_brrdec1;

always @(posedge CLK) begin
  mon_valid_brrdec <= 1'b0;
  if (mon_ack_brrdec & MON_READY) begin
    mon_valid_brrdec <= 1'b1;
    mon_dout_brrdec <= mon_dout;
  end
end

always @* begin
  MON_ACK = 1'b1;
  if (mon_ack_ctx) begin
    MON_VALID = mon_valid_ctx;
    MON_DOUT = mon_dout_ctx;
  end
  else if (mon_ack_brrdec) begin
    MON_VALID = mon_valid_brrdec;
    MON_DOUT = mon_dout_brrdec;
  end
  else begin
    MON_ACK = 1'b0;
    MON_VALID = 1'b0;
    MON_DOUT = 32'b0;
  end
end


endmodule
