`timescale 1us / 1ns

/* verilator lint_off PINMISSING */

module s_dsp
  (
   input         nRES, // reset
   input         CLK, // 24.576MHz clock
   output        CKEN, // CPU clock enable (1.024MHz)
   input         HOLD, // clock stall (emulator control)
   input [15:0]  A, // address bus (from S-SMP)
   input [7:0]   DI, // data bus (from S-SMP)
   output [7:0]  DO, // data bus (to S-SMP)
   input         RW, // R/W (from S-SMP)
   output [15:0] MA, // SRAM address bus
   input [7:0]   MDI, // SRAM data bus (from RAM)
   output [7:0]  MDO, // SRAM data bus (to RAM)
   output        nCE, // SRAM chip enable
   output        nWE, // SRAM write enable
   output        nOE, // SRAM output enable
   output        SCLKEN, // sample clock enable
   output [15:0] PSOUTL, // parallel sample out (L)
   output [15:0] PSOUTR, // parallel sample out (R)

   // debug monitor
   input [7:0]   MON_SEL,
   output reg    MON_ACK,
   input         MON_READY,
   output [31:0] MON_DOUT,
   output        MON_VALID,
   input         MON_WS,
   input [31:0]  MON_DIN
   );

// Clock enables

wire [1:0] cccnt;
wire       nres;
wire       iclken;
wire       ccken;

wire [31:0] mon_dout_clkgen;
wire        mon_ack_clkgen;
wire        mon_valid_clkgen;

s_dsp_clkgen clkgen
  (
   .CLK(CLK),
   .nRES_I(nRES),
   .HOLD(HOLD),
   .nRES_O(nres),
   .ICLKEN(iclken),
   .CCCNT(cccnt),
   .CCKEN(ccken),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_clkgen),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_clkgen),
   .MON_VALID(mon_valid_clkgen),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN)
   );

assign CKEN = ccken;

// S-SMP address / data bus, peripherals

wire [6:0] spda;
wire [7:0] spddi, spddo;
wire       spddrs, spddws;
wire [31:0] mon_dout_si;
wire        mon_ack_si;
wire        mon_valid_si;

s_dsp_smp_if smp
  (
   .CLK(CLK),
   .nRES(nres),
   .HOLD(HOLD),
   .ICLKEN(iclken),
   .CCKEN(ccken),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_si),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_si),
   .MON_VALID(mon_valid_si),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .A(A),
   .DI(DI),
   .DO(DO),
   .RW(RW),

   .MDI(MDI),

   .SPDA(spda),
   .SPDDRS(spddrs),
   .SPDDWS(spddws),
   .SPDDO(spddo),
   .SPDDI(spddi)
   );

// DSP Register File

wire [31:0] mon_dout_rf;
wire        mon_ack_rf;
wire        mon_valid_rf;
wire [6:0]  ira;
wire [7:0]  irdi, irdo;
wire        irrs, irws;
wire        kon_written, kon_read;
wire [7:5]  flg;

s_dsp_reg_file rf
  (
   .CLK(CLK),
   .nRES(nres),
   .HOLD(HOLD),
   .ICLKEN(iclken),
   .CCKEN(ccken),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_rf),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_rf), 
   .MON_VALID(mon_valid_rf), 
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .SA(spda), 
   .SRS(spddrs), 
   .SWS(spddws), 
   .SRDI(spddi), 
   .SRDO(spddo),

   .IA(ira),
   .IRS(irrs),
   .IWS(irws),
   .IRDI(irdi),
   .IRDO(irdo),

   .KON_WRITTEN(kon_written),
   .KON_READ(kon_read),
   .FLG(flg)
   );

// SRAM address / data and control

wire [31:0] mon_dout_sr;
wire        mon_ack_sr;
wire        mon_valid_sr;
wire [15:0] ima;
wire [7:0]  imdo, imdi;
wire        imrs, imws;

s_dsp_sram_if sram
  (
   .CLK(CLK),
   .CCKEN(ccken),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_sr),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_sr),
   .MON_VALID(mon_valid_sr),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .A(A),
   .DI(DI),
   .RW(RW),
   .MA(MA),
   .MDI(MDI),
   .MDO(MDO),
   .nCE(nCE),
   .nWE(nWE),
   .nOE(nOE),

   .IMA(ima),
   .IMRS(imrs),
   .IMWS(imws),
   .IMDO(imdo),
   .IMDI(imdi)
   );

// Global counter

wire [31:0] mon_dout_gc;
wire        mon_ack_gc;
wire        mon_valid_gc;
wire [15:0] glb_cnt;            // envelope event counter
wire        sclken;             // sample rate clock enable

s_dsp_glb_ctr glb_ctr
  (
   .CLK(CLK),
   .nRES(nres),
   .SCLKEN(sclken),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_gc),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_gc),
   .MON_VALID(mon_valid_gc),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .CNT(glb_cnt)
   );

// Sample generation loop

wire [31:0] mon_dout_sl;
wire        mon_ack_sl;
wire        mon_valid_sl;

s_dsp_sgenloop sgenloop
  (
   .CLK(CLK),
   .nRES(nres),
   .CKEN(iclken),
   .CCCNT(cccnt),
   .SCLKEN(sclken),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_sl),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_sl),
   .MON_VALID(mon_valid_sl),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .GLB_CNT(glb_cnt),

   .RA(ira),
   .RRS(irrs),
   .RWS(irws),
   .RDO(irdo),
   .RDI(irdi),
   .KON_WRITTEN(kon_written),
   .KON_READ(kon_read),
   .FLG(flg),

   .MA(ima),
   .MRS(imrs),
   .MWS(imws),
   .MDO(imdo),
   .MDI(imdi),

   .PSOUTL(PSOUTL),
   .PSOUTR(PSOUTR)
   );

assign SCLKEN = sclken;


//////////////////////////////////////////////////////////////////////
// Register monitor interface

reg [31:0]  mon_dout;
reg         mon_valid;

always @* begin
  MON_ACK = 1'b1;
  if (mon_ack_clkgen) begin
    mon_valid = mon_valid_clkgen;
    mon_dout = mon_dout_clkgen;
  end
  else if (mon_ack_si) begin
    mon_valid = mon_valid_si;
    mon_dout = mon_dout_si;
  end
  else if (mon_ack_rf) begin
    mon_valid = mon_valid_rf;
    mon_dout = mon_dout_rf;
  end
  else if (mon_ack_sr) begin
    mon_valid = mon_valid_sr;
    mon_dout = mon_dout_sr;
  end
  else if (mon_ack_gc) begin
    mon_valid = mon_valid_gc;
    mon_dout = mon_dout_gc;
  end
  else if (mon_ack_sl) begin
    mon_valid = mon_valid_sl;
    mon_dout = mon_dout_sl;
  end
  else begin
    MON_ACK = 1'b0;
    mon_valid = 1'b0;
    mon_dout = 32'h0;
  end
end

assign MON_DOUT = mon_dout;
assign MON_VALID = mon_valid;


endmodule
