// References: 
// [1] https://problemkaputt.de/fullsnes.htm#snesapulowleveltimings
// [2] https://gist.github.com/nyanpasu64/a0d916ce6924912a7116682bf778e9a0
// sglcnt numbers from [1], regfile timing vs CCCNT from [2]

module s_dsp_sgenloop
  (
   input             CLK,
   input             nRES,
   input             CKEN,
   input [1:0]       CCCNT,
   output            SCLKEN,

   // debug monitor
   input [7:0]       MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input             MON_WS,
   input [31:0]      MON_DIN,

   // Global counter
   input [15:0]      GLB_CNT,

   // Register file interface
   output [6:0]      RA,
   output            RRS,
   output            RWS,
   input [7:0]       RDO,
   output [7:0]      RDI,
   input             KON_WRITTEN,
   output            KON_READ,
   input [7:5]       FLG,

   // Memory interface
   output [15:0]     MA,
   output            MRS,
   output            MWS,
   input [7:0]       MDO,
   output [7:0]      MDI,

   // Sample output
   output [15:0]     PSOUTL,
   output [15:0]     PSOUTR
   );

wire mon_write_dtcs, mon_write_sl0, mon_write_sl1, mon_write_sl2;

//////////////////////////////////////////////////////////////////////
// Loop counter

reg [5:0] sglcnt;
reg       stall, stalled;
reg       clr_stalled;
wire      gcken;
wire      sclken;

initial begin
  sglcnt = 0;
  stall = 'b0;
  stalled = 'b0;
end

assign gcken = CKEN & ~stalled;

always @(posedge CLK) begin
  if (~nRES) begin
    sglcnt <= 0;
    // Reset shouldn't conflict with stalling.
    //stall <= 'b0;
    //stalled <= 'b0;
    //clr_stalled <= 'b0;
  end
  else if (CKEN & (CCCNT == 2'd2)) begin
    sglcnt <= sglcnt + 1'd1;

    if (&sglcnt) begin
      stalled <= stall;
      clr_stalled <= stalled;
    end
  end

  if (mon_write_dtcs)
    stall <= MON_DIN[0];
  if (mon_write_sl0)
    sglcnt <= MON_DIN[5:0];
end

assign sclken = gcken && sglcnt[4:0] == 5'd29 & CCCNT == 2'd0; // global counter update time

assign SCLKEN = sclken;


//////////////////////////////////////////////////////////////////////
// Global operations

reg [7:0] pmon, non, dir, kon, kof, mvoll, mvolr, evoll, evolr;
reg [4:0] flgn;
reg       gpmon, gnon, gdir, gkon_clr, gkon_rd, gkof, gflgn;
reg       gmvoll, gevoll, goutl, gmvolr, gevolr, goutr;

initial begin
  kon = 0;
  kof = 0;
  mvoll = 0;
  evoll = 0;
  mvolr = 0;
  evolr = 0;
end

always @* begin
  gpmon = 0;
  gnon = 0;
  gdir = 0;
  gkon_clr = 0;
  gkon_rd = 0;
  gkof = 0;
  gflgn = 0;
  gmvoll = 0;
  gevoll = 0;
  goutl = 0;
  gmvolr = 0;
  gevolr = 0;
  goutr = 0;

  if (~stalled)
    case ({sglcnt[4:0], CCCNT})
      {5'd26, 2'd0}: gmvoll = 'b1;
      {5'd26, 2'd1}: gevoll = 'b1;
      {5'd26, 2'd2}: goutl = 'b1;
      {5'd27, 2'd0}: gmvolr = 'b1;
      {5'd27, 2'd1}: gevolr = 'b1;
      {5'd27, 2'd2}: begin goutr = 'b1; gpmon = 'b1; end
      {5'd28, 2'd0}: gnon = 'b1;
      {5'd28, 2'd2}: begin gdir = 'b1; gkon_clr = sglcnt[5]; end
      {5'd29, 2'd2}: gkon_rd = sglcnt[5] & KON_WRITTEN;
      {5'd30, 2'd0}: gkof = sglcnt[5];
      {5'd30, 2'd1}: gflgn = 'b1;
      default: ;
    endcase
end

always @(posedge CLK) begin
  if (gcken) begin
    if (gpmon)
      pmon <= {RDO[7:1], 1'b0};   // V0 never modulates pitch
    if (gnon)
      non <= RDO;
    if (gdir)
      dir <= RDO;
    if (gkon_clr)
      kon <= 8'h00;
    if (gkon_rd)
      kon <= RDO;
    if (gkof)
      kof <= RDO;
    if (gflgn)
      flgn <= RDO[4:0];
    if (gmvoll)
      mvoll <= RDO;
    if (gevoll)
      evoll <= RDO;
    if (gmvolr)
      mvolr <= RDO;
    if (gevolr)
      evolr <= RDO;
  end

  if (mon_write_sl0) begin
    pmon <= MON_DIN[15:8];
    non <= MON_DIN[23:16];
    dir <= MON_DIN[31:24];
  end
  if (mon_write_sl1) begin
    kon <= MON_DIN[7:0];
    kof <= MON_DIN[15:8];
    mvoll <= MON_DIN[23:16];
    mvolr <= MON_DIN[31:24];
  end
  if (mon_write_sl2) begin
    evoll <= MON_DIN[7:0];
    evolr <= MON_DIN[15:8];
    flgn <= MON_DIN[20:16];
  end
end

assign KON_READ = gkon_rd;


//////////////////////////////////////////////////////////////////////
// Noise generator

wire signed [14:0] noise_sout;

wire [31:0] mon_dout_noise;
wire        mon_ack_noise;
wire        mon_valid_noise;

s_dsp_noise noise
  (
   .CLK(CLK),
   .nRES(nRES),
   .CKEN(gcken),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_noise),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_noise),
   .MON_VALID(mon_valid_noise),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .GLB_CNT(GLB_CNT),
   .FLGN(flgn),
   .SCLKEN(sclken),
   .OUT(noise_sout)
   );


//////////////////////////////////////////////////////////////////////
// Voice pipeline

reg [11:0] vps;
wire [6:0] vra;
wire       vrrs, vrws;
wire [15:0] vma;
wire        vmrs;
wire       vml, vmr;
wire signed [15:0] voutl, voutr;

wire [31:0] mon_dout_voice;
wire        mon_ack_voice;
wire        mon_valid_voice;

always @* begin
  vps[0] = 0;                // VxSRCN
  vps[1] = 0;                // just before vs2a
  vps[2] = 0;                // [VxSRCN/DIR.lsb], VxPITCHL
  vps[3] = 0;                // [VxSRCN/DIR.msb], VxADSR1
  vps[4] = 0;                // VxPITCHH
  vps[5] = 0;                // [VxBRR.1st.hdr]
  vps[6] = 0;                // [VxBRR.1st.dta0]
  vps[7] = 0;                // VxADSR2 (+ env apply, update)
  vps[8] = 0;                // [VxBRR.2nd.dta1], VxVOLL (+ interp.go)
  vps[9] = 0;                // VxVOLR, ENDX.0
  vps[10] = 0;               // VxENVX
  vps[11] = 0;               // VxOUTX

  if (~stalled) case ({sglcnt[4:0], CCCNT})
    //                       110000000000
    //                       109876543210
    {5'd0,  2'd0}: vps = 12'b001000000010;
    {5'd0,  2'd1}: vps = 12'b000000000100;
    {5'd0,  2'd2}: vps = 12'b000000001000;
    {5'd1,  2'd0}: vps = 12'b010000000000;
    {5'd1,  2'd1}: vps = 12'b000000110000;
    {5'd1,  2'd2}: vps = 12'b000011000000;
    {5'd2,  2'd0}: vps = 12'b100000000000;
    {5'd2,  2'd1}: vps = 12'b000100000000;
    {5'd2,  2'd2}: vps = 12'b000000000001;
    {5'd3,  2'd0}: vps = 12'b001000000010;
    {5'd3,  2'd1}: vps = 12'b000000000100;
    {5'd3,  2'd2}: vps = 12'b000000001000;
    {5'd4,  2'd0}: vps = 12'b010000000000;
    {5'd4,  2'd1}: vps = 12'b000000110000;
    {5'd4,  2'd2}: vps = 12'b000011000000;
    {5'd5,  2'd0}: vps = 12'b100000000000;
    {5'd5,  2'd1}: vps = 12'b000100000000;
    {5'd5,  2'd2}: vps = 12'b000000000001;
    {5'd6,  2'd0}: vps = 12'b001000000010;
    {5'd6,  2'd1}: vps = 12'b000000000100;
    {5'd6,  2'd2}: vps = 12'b000000001000;
    {5'd7,  2'd0}: vps = 12'b010000000000;
    {5'd7,  2'd1}: vps = 12'b000000110000;
    {5'd7,  2'd2}: vps = 12'b000011000000;
    {5'd8,  2'd0}: vps = 12'b100000000000;
    {5'd8,  2'd1}: vps = 12'b000100000000;
    {5'd8,  2'd2}: vps = 12'b000000000001;
    {5'd9,  2'd0}: vps = 12'b001000000010;
    {5'd9,  2'd1}: vps = 12'b000000000100;
    {5'd9,  2'd2}: vps = 12'b000000001000;
    {5'd10, 2'd0}: vps = 12'b010000000000;
    {5'd10, 2'd1}: vps = 12'b000000110000;
    {5'd10, 2'd2}: vps = 12'b000011000000;
    {5'd11, 2'd0}: vps = 12'b100000000000;
    {5'd11, 2'd1}: vps = 12'b000100000000;
    {5'd11, 2'd2}: vps = 12'b000000000001;
    {5'd12, 2'd0}: vps = 12'b001000000010;
    {5'd12, 2'd1}: vps = 12'b000000000100;
    {5'd12, 2'd2}: vps = 12'b000000001000;
    {5'd13, 2'd0}: vps = 12'b010000000000;
    {5'd13, 2'd1}: vps = 12'b000000110000;
    {5'd13, 2'd2}: vps = 12'b000011000000;
    {5'd14, 2'd0}: vps = 12'b100000000000;
    {5'd14, 2'd1}: vps = 12'b000100000000;
    {5'd14, 2'd2}: vps = 12'b000000000001;
    {5'd15, 2'd0}: vps = 12'b001000000010;
    {5'd15, 2'd1}: vps = 12'b000000000100;
    {5'd15, 2'd2}: vps = 12'b000000001000;
    {5'd16, 2'd0}: vps = 12'b010000000000;
    {5'd16, 2'd1}: vps = 12'b000000110000;
    {5'd16, 2'd2}: vps = 12'b000011000000;
    {5'd17, 2'd0}: vps = 12'b100000000000;
    {5'd17, 2'd1}: vps = 12'b000100000000;
    {5'd17, 2'd2}: vps = 12'b000000000001;
    {5'd18, 2'd0}: vps = 12'b001000000010;
    {5'd18, 2'd1}: vps = 12'b000000000100;
    {5'd18, 2'd2}: vps = 12'b000000001000;
    {5'd19, 2'd0}: vps = 12'b010000000000;
    {5'd19, 2'd1}: vps = 12'b000000110000;
    {5'd19, 2'd2}: vps = 12'b000011000000;
    {5'd20, 2'd0}: vps = 12'b100000000000;
    {5'd20, 2'd1}: vps = 12'b000100000000;
    {5'd20, 2'd2}: vps = 12'b000000000001;
    {5'd21, 2'd0}: vps = 12'b001000000010;
    {5'd21, 2'd1}: vps = 12'b000000000100;
    {5'd21, 2'd2}: vps = 12'b000000001000;
    {5'd22, 2'd0}: vps = 12'b010000000000;
    {5'd22, 2'd1}: vps = 12'b000000010000;
    {5'd22, 2'd2}: vps = 12'b000000000000;
    {5'd23, 2'd0}: vps = 12'b100000000000;
    {5'd23, 2'd1}: vps = 12'b000000000000;
    {5'd23, 2'd2}: vps = 12'b000000000000;
    {5'd24, 2'd0}: vps = 12'b000000000000;
    {5'd24, 2'd1}: vps = 12'b000000000000;
    {5'd24, 2'd2}: vps = 12'b000000000000;
    {5'd25, 2'd0}: vps = 12'b000000000000;
    {5'd25, 2'd1}: vps = 12'b000000100000;
    {5'd25, 2'd2}: vps = 12'b000001000000;
    {5'd26, 2'd0}: vps = 12'b000000000000;
    {5'd26, 2'd1}: vps = 12'b000000000000;
    {5'd26, 2'd2}: vps = 12'b000000000000;
    {5'd27, 2'd0}: vps = 12'b000000000000;
    {5'd27, 2'd1}: vps = 12'b000000000000;
    {5'd27, 2'd2}: vps = 12'b000000000000;
    {5'd28, 2'd0}: vps = 12'b000000000000;
    {5'd28, 2'd1}: vps = 12'b000000000000;
    {5'd28, 2'd2}: vps = 12'b000000000000;
    {5'd29, 2'd0}: vps = 12'b000000000000;
    {5'd29, 2'd1}: vps = 12'b000000000000;
    {5'd29, 2'd2}: vps = 12'b000000000000;
    {5'd30, 2'd0}: vps = 12'b000000000000;
    {5'd30, 2'd1}: vps = 12'b000000000000;
    {5'd30, 2'd2}: vps = 12'b000010000000;
    {5'd31, 2'd0}: vps = 12'b000000000000;
    {5'd31, 2'd1}: vps = 12'b000100000000;
    {5'd31, 2'd2}: vps = 12'b000000000001;
    default: ;
  endcase
end

s_dsp_voice voice
  (
   .CLK(CLK),
   .nRES(nRES),
   .CKEN(gcken),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_voice),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_voice),
   .MON_VALID(mon_valid_voice),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .GLB_CNT(GLB_CNT),

   .DIR(dir),
   .KON(kon),
   .KOF(kof | {8{FLG[7]}}),
   .NON(non),
   .PMON(pmon),

   .NOISE_SIN(noise_sout),

   .SGLCNT(sglcnt[4:0]),
   .CCCNT(CCCNT),
   .VPS(vps),

   .RA(vra),
   .RRS(vrrs),
   .RWS(vrws),
   .RDO(RDO),
   .RDI(RDI),

   .MA(vma),
   .MRS(vmrs),
   .MDO(MDO),

   .VML(vml),
   .VMR(vmr),
   .VOUTL(voutl),
   .VOUTR(voutr)
   );


//////////////////////////////////////////////////////////////////////
// Echo pipeline

wire signed [15:0] eoutl, eoutr;
reg [21:0]         eps;
wire [6:0]         era;
wire               errs;
wire [15:0]        ema;
wire [7:0]         emdi;
wire               emrs, emws;
wire               eml, emr;

wire [31:0]        mon_dout_echo;
wire               mon_ack_echo;
wire               mon_valid_echo;

always @* begin
  eps[0] = 0;                   // [RdEchoLeft.lsb]
  eps[1] = 0;                   // [RdEchoLeft.msb]
  eps[2] = 0;                   // [RdEchoRight.lsb]
  eps[3] = 0;                   // [RdEchoRight.msb]

  eps[4] = 0;                   // [WrEchoLeft.lsb]
  eps[5] = 0;                   // [WrEchoLeft.msb]
  eps[6] = 0;                   // [WrEchoRight.lsb]
  eps[7] = 0;                   // [WrEchoRight.msb] (inc buf idx)

  eps[8] = 0;                   // load EFB
  eps[9] = 0;                   // load EON
  eps[10] = 0;                  // load EDL
  eps[11] = 0;                  // load ESA
  eps[12] = 0;                  // load ECEN (FLG.6)

  eps[13] = 0;                  // apply ESA
  eps[14] = 0;                  // apply EDL
  eps[15] = 0;                  // echo mix out left
  eps[16] = 0;                  // echo mix out right

  eps[17] = 0;                  // FFCx
  eps[18] = 0;                  // FIR in left
  eps[19] = 0;                  // FIR int right
  eps[20] = 0;                  // FIR out left
  eps[21] = 0;                  // FIR out right

  if (~stalled) case ({sglcnt[4:0], CCCNT})
    {5'd22, 2'd0}: eps = 22'b00000_0001_00000_0000_0000;
    {5'd22, 2'd1}: eps = 22'b00000_0000_00000_0000_0001;
    {5'd22, 2'd2}: eps = 22'b00001_0000_00000_0000_0010;
    {5'd23, 2'd0}: eps = 22'b00010_0000_00000_0000_0000;
    {5'd23, 2'd1}: eps = 22'b00001_0000_00000_0000_0100;
    {5'd23, 2'd2}: eps = 22'b00001_0000_00000_0000_1000;
    {5'd24, 2'd0}: eps = 22'b00101_0000_00000_0000_0000;
    {5'd24, 2'd1}: eps = 22'b00001_0000_00000_0000_0000;
    {5'd24, 2'd2}: eps = 22'b00001_0000_00000_0000_0000;
    {5'd25, 2'd0}: eps = 22'b00001_0000_00000_0000_0000;
    {5'd25, 2'd1}: eps = 22'b00001_0000_00000_0000_0000;
    {5'd25, 2'd2}: eps = 22'b00000_0000_00000_0000_0000;
    {5'd26, 2'd0}: eps = 22'b01000_0000_00000_0000_0000;
    {5'd26, 2'd1}: eps = 22'b00000_0000_00000_0000_0000;
    {5'd26, 2'd2}: eps = 22'b00000_0000_00001_0000_0000;
    {5'd27, 2'd0}: eps = 22'b10000_0000_00000_0000_0000;
    {5'd28, 2'd1}: eps = 22'b00000_0100_00010_0000_0000;
    {5'd28, 2'd2}: eps = 22'b00000_1000_00000_0000_0000;
    {5'd29, 2'd0}: eps = 22'b00000_0000_10100_0000_0000;
    {5'd29, 2'd1}: eps = 22'b00000_0010_01000_0001_0000;
    {5'd29, 2'd2}: eps = 22'b00000_0000_00000_0010_0000;
    {5'd30, 2'd0}: eps = 22'b00000_0000_10000_0000_0000;
    {5'd30, 2'd1}: eps = 22'b00000_0000_00000_0100_0000;
    {5'd30, 2'd2}: eps = 22'b00000_0000_00000_1000_0000;
    default: ;
  endcase
end

s_dsp_echo echo
  (
   .CLK(CLK),
   .nRES(nRES),
   .CKEN(gcken),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_echo),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_echo),
   .MON_VALID(mon_valid_echo),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .ECEN(FLG[5]),

   .EPS(eps),
   .CLR_STALLED(clr_stalled),

   .RA(era),
   .RRS(errs),
   .RDO(RDO),

   .MA(ema),
   .MRS(emrs),
   .MWS(emws),
   .MDI(emdi),
   .MDO(MDO),

   .VML(vml),
   .VMR(vmr),
   .VOUTL(voutl),
   .VOUTR(voutr),

   .EML(eml),
   .EMR(emr),
   .EOUTL(eoutl),
   .EOUTR(eoutr)
   );


//////////////////////////////////////////////////////////////////////
// Register file access

reg [6:0] ra;
reg       rrs, rws;

always @* begin
  if (~nRES) begin
    rrs = 1'b0;
    rws = 1'b0;
    ra = 7'hx;
  end
  else begin
    rrs = 1'b1;
    rws = 1'b0;
    ra = 7'hx;

    if (gkon_rd)          ra = 7'h4C;
    else if (gkof)        ra = 7'h5C;
    else if (gpmon)       ra = 7'h2D;
    else if (gnon)        ra = 7'h3D;
    else if (gflgn)       ra = 7'h6C;
    else if (gdir)        ra = 7'h5D;
    else if (gmvoll)      ra = 7'h0C;
    else if (gevoll)      ra = 7'h2C;
    else if (gmvolr)      ra = 7'h1C;
    else if (gevolr)      ra = 7'h3C;
    else if (vrrs)        ra = vra;
    else if (vrws) begin
      ra = vra;
      rrs = 1'b0;
      rws = vrws;
    end
    else if (errs)        ra = era;
    else
      rrs = 1'b0;
  end
end

assign RA = ra;
assign RRS = rrs;
assign RWS = rws;


//////////////////////////////////////////////////////////////////////
// Memory access

reg [15:0] ma;
reg        mrs, mws;
reg [7:0]  mdi;

always @* begin
  ma = 16'hx;
  mrs = 1'b0;
  mws = 1'b0;
  mdi = 8'hx;

  if (vmrs) begin
    ma = vma;
    mrs = vmrs;
  end
  else if (emrs | emws) begin
    ma = ema;
    mrs = emrs;
    mws = emws;
    mdi = emdi;
  end
end

assign MA = ma;
assign MRS = mrs;
assign MWS = mws;
assign MDI = mdi;


//////////////////////////////////////////////////////////////////////
// Final mix and sample output

wire [31:0]        mon_dout_mixer;
wire               mon_ack_mixer;
wire               mon_valid_mixer;

s_dsp_out_mixer out_mixer
  (
   .CLK(CLK),
   .nRES(nRES),
   .CKEN(gcken),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_mixer),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_mixer),
   .MON_VALID(mon_valid_mixer),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .MUTE(FLG[6]),
   .MVOLL(mvoll),
   .EVOLL(evoll),
   .MVOLR(mvolr),
   .EVOLR(evolr),

   .VML(vml),
   .VMR(vmr),
   .EML(eml),
   .EMR(emr),
   .GOL(goutl),
   .GOR(goutr),

   .VOUTL(voutl),
   .VOUTR(voutr),
   .EOUTL(eoutl),
   .EOUTR(eoutr),

   .SOUTL(PSOUTL),
   .SOUTR(PSOUTR)
   );


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout, mon_dout_sl;
reg        mon_ack_sl, mon_valid_sl;
reg        mon_sel_dtcs, mon_sel_sl0, mon_sel_sl1, mon_sel_sl2;
wire       mon_write;

always @* begin
  mon_dout = 32'b0;
  mon_ack_sl = 1'b1;
  mon_sel_dtcs = 1'b0;
  mon_sel_sl0 = 1'b0;
  mon_sel_sl1 = 1'b0;
  mon_sel_sl2 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL)                //``SUBREGS APU
    8'h40: begin                //``REG DTCS // DSP Timing Control / Status
      mon_sel_dtcs = 1'b1;
      mon_dout[0] = stall;
      mon_dout[1] = stalled;
    end
    8'h49: begin                //``REG SL0
      mon_sel_sl0 = 1'b1;
      mon_dout[5:0] = sglcnt;
      mon_dout[15:8] = pmon;
      mon_dout[23:16] = non;
      mon_dout[31:24] = dir;
    end
    8'h4a: begin                //``REG SL1
      mon_sel_sl1 = 1'b1;
      mon_dout[7:0] = kon;
      mon_dout[15:8] = kof;
      mon_dout[23:16] = mvoll;
      mon_dout[31:24] = mvolr;
    end
    8'h4b: begin                //``REG SL2
      mon_sel_sl2 = 1'b1;
      mon_dout[7:0] = evoll;
      mon_dout[15:8] = evolr;
      mon_dout[20:16] = flgn;
    end
    default: mon_ack_sl = 1'b0;
  endcase
end

assign mon_write = MON_READY & MON_WS & ~mon_valid_sl;
assign mon_write_dtcs = mon_write & mon_sel_dtcs;
assign mon_write_sl0 = mon_write & mon_sel_sl0;
assign mon_write_sl1 = mon_write & mon_sel_sl1;
assign mon_write_sl2 = mon_write & mon_sel_sl2;

always @(posedge CLK) begin
  mon_valid_sl <= 1'b0;
  if (mon_ack_sl & MON_READY) begin
    mon_valid_sl <= 1'b1;
    mon_dout_sl <= mon_dout;
  end
end

always @* begin
  MON_ACK = 1'b1;
  if (mon_ack_noise) begin
    MON_VALID = mon_valid_noise;
    MON_DOUT = mon_dout_noise;
  end
  else if (mon_ack_voice) begin
    MON_VALID = mon_valid_voice;
    MON_DOUT = mon_dout_voice;
  end
  else if (mon_ack_echo) begin
    MON_VALID = mon_valid_echo;
    MON_DOUT = mon_dout_echo;
  end
  else if (mon_ack_mixer) begin
    MON_VALID = mon_valid_mixer;
    MON_DOUT = mon_dout_mixer;
  end
  else if (mon_ack_sl) begin
    MON_VALID = mon_valid_sl;
    MON_DOUT = mon_dout_sl;
  end
  else begin
    MON_ACK = 1'b0;
    MON_VALID = 1'b0;
    MON_DOUT = 32'b0;
  end
end


endmodule
