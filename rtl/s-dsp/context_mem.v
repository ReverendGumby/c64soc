`timescale 1us / 1ns

module s_dsp_context_mem
  #(
    parameter       WIDTH=1,
    parameter [2:0] INIT_VSEL=3'd0,
    parameter [7:0] MON_BASE=8'h0
    )
  (
   input              CLK,
   input              nRES,
   input              CKEN,

   // debug monitor
   input [7:0]        MON_SEL,
   input              MON_ACK,
   input              MON_READY,
   output reg [31:0]  MON_DOUT,
   output reg         MON_VALID,
   input              MON_WS,
   input [31:0]       MON_DIN,

   input [2:0]        VSEL,
   input              SWAP,

   input [WIDTH-1:0]  IN,
   output [WIDTH-1:0] OUT
   );

reg [WIDTH-1:0] mem [0:7];
reg [WIDTH-1:0] mem_rbuf, mem_wbuf;
reg [2:0]       ptr, ptrp1, mem_rptr, mem_wptr;
reg             mem_we;
wire [7:0]      mon_sel_off;
wire [2:0]      mon_vsel;
wire            mon_sel_mem;
wire            mon_write;
integer         i;

initial begin
  for (i = 0; i < 8; i = i + 1)
    mem[i] = {WIDTH{1'b0}};
end

always @*
  ptr = VSEL;

always @*
  ptrp1 = ptr + 1'd1;

always @* begin
  mem_rptr = ptrp1;
  if (mon_sel_mem)
    mem_rptr = mon_vsel;
end

always @* begin
  mem_wptr = 0;
  mem_wbuf = 0;
  mem_we = 0;

  if (nRES & CKEN & SWAP) begin
    mem_wptr = ptr;
    mem_wbuf = IN;
    mem_we = 1'b1;
  end
  else if (mon_write & mon_sel_mem) begin
    mem_wptr = mon_vsel;
    mem_wbuf = MON_DIN[WIDTH-1:0];
    mem_we = 1'b1;
  end
end

always @(posedge CLK) begin
  mem_rbuf <= mem[mem_rptr];
  if (mem_we)
    mem[mem_wptr] <= mem_wbuf;
end

// Assumption: OUT only needs to be valid on CKEN & SWAP
assign OUT = mem_rbuf;

assign mon_sel_off = MON_SEL - MON_BASE;
assign mon_vsel = mon_sel_off[2:0];
assign mon_sel_mem = MON_ACK;
assign mon_write = MON_ACK & MON_READY & MON_WS & ~MON_VALID;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK & MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= 0;
    MON_DOUT[WIDTH-1:0] <= mem_rbuf;
  end
end

endmodule
