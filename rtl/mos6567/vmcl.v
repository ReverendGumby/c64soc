module mos6567_vmcl
  (
   input         PCLK,
   input         BCLK_POSEDGE,
   input         BCLK_NEGEDGE,
   input [11:0]  DATA_IN,
   input         ENABLE_IN,
   input         SHIFT,

   output [11:0] DATA_OUT
   );

// Array upper index is rounded up from 39 to next power of 2, to
// silence synthesis warning Xst:3035.
reg [11:0] line[63:0];

reg [5:0]  idx;
reg        enable, enable_d1;
reg        shift_d1;
reg [11:0] line_out;

initial begin
  idx <= 6'd0;
end

integer i;
always @(posedge PCLK) if (BCLK_POSEDGE) begin
  if (shift_d1) begin
    if (idx == 6'd39)
      idx <= 6'd0;
    else if (idx < 6'd39)
      idx <= idx + 6'd1;
    else
      idx <= 6'dx;
  end
end

always @(posedge PCLK) if (BCLK_NEGEDGE) begin
  enable <= ENABLE_IN;
  shift_d1 <= SHIFT;
  //data <= DATA_IN;
  if (enable_d1)
    line[idx] <= DATA_IN;
end

always @(posedge PCLK) if (BCLK_POSEDGE) begin
  enable_d1 <= enable;
  line_out <= line[idx];
end

assign DATA_OUT = enable_d1 ? DATA_IN : line_out;

endmodule
