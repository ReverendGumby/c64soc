`include "mob_bus.vh"

module mos6567_mob_data_seq
  (
   input                     PCLK,
   input                     CLKEN, // @(negedge PCLK)
   input                     CCLK_POSEDGE,
   input                     CCLK_NEGEDGE,
   input                     BCLK_POSEDGE,
   input                     BCLK_NEGEDGE,

   inout [`MOB_BUS_BITS-1:0] MOB_BUS,

   input [8:0]               RASTER,
   input [6:0]               CYCLE,
   input [8:0]               X,
   input [7:0]               DATA_IN,
   input [3:0]               MM0,
   input [3:0]               MM1,
   output reg                OUT_CLKEN,
   output reg [8:0]          OUT_X
   );

wire mxe = `MOB_BUS_MXE(MOB_BUS, 0);
wire mxxe = `MOB_BUS_MXXE(MOB_BUS, 0);
wire mxye = `MOB_BUS_MXYE(MOB_BUS, 0);
wire mxmc = `MOB_BUS_MXMC(MOB_BUS, 0);
wire mxdp = `MOB_BUS_MXDP(MOB_BUS, 0);
wire [7:0] mxy = `MOB_BUS_MXY(MOB_BUS, 0);
wire [8:0] mxx = `MOB_BUS_MXX(MOB_BUS, 0);
wire [3:0] mxc = `MOB_BUS_MXC(MOB_BUS, 0);

// Along with MCBASE, VIC-Article.txt describes MC, the lower bits of
// the s-access address. Since MC follows a fixed pattern
// (MCBASE+[0,1,2]) and is never read back, we've replaced it with a
// single fixed adder in addr_generator.

reg [5:0] mcbase;
reg       dma_request;
reg       expansion;
reg       display;
reg [5:0] pix;                  // 1'fg/bg + 1'alpha + 4'color

wire dma_grant;
reg  mxx_match;
wire mxy_match = {1'b0, mxy} == RASTER[8:0];
wire dma_start = (CYCLE == 7'd57-1 || CYCLE == 7'd58-1) &&
                  mxe && mxy_match;
reg  raster_end;

reg        data_in_valid;
reg [23:0] shift_reg;
reg [7:0]  shift_reg_in;
reg        shift_reg_in_valid;
reg        shift_data_in_go;
reg        shift_cnt;
wire       shift_cnt_max;
reg        shift_data_out;
reg        shift_data_out_go;
reg [1:0]  pix_cnt;
reg [1:0]  pix_cnt_max;
wire       pix_code_reg_go;
reg [1:0]  pix_code_reg;
reg        alpha;
reg [3:0]  color;

assign `MOB_BUS_DMAR(MOB_BUS, 0) = dma_request;
assign `MOB_BUS_MC(MOB_BUS, 0) = mcbase;
assign `MOB_BUS_PIX(MOB_BUS, 0) = pix;

assign dma_grant = `MOB_BUS_DMAG(MOB_BUS, 0);

initial begin
  shift_data_out <= 1'b0;
  dma_request <= 1'b0;
end

reg clken_dly1;                 // @(posedge PCLK)
always @(posedge PCLK)
  clken_dly1 <= CLKEN;

reg clken_dly2;                 // @(negedge PCLK)
always @(negedge PCLK)
  clken_dly2 <= clken_dly1;

always @(posedge PCLK) if (CCLK_NEGEDGE) begin
  if (!mxye)
    expansion <= 1'b1;
  else if (dma_start)
    expansion <= 1'b0;
  else if (CYCLE == 7'd58-1)
    expansion <= ~expansion;
end

always @(posedge PCLK) if (CCLK_POSEDGE) raster_end <= CYCLE == 7'd60-1;

always @(posedge PCLK) if (CCLK_NEGEDGE) begin
  if (CYCLE == 7'd14) begin
    if (expansion)
      mcbase <= mcbase + 6'h02;
  end
  else if (CYCLE == 7'd15) begin
    if (expansion)
      mcbase <= mcbase + 6'h01;
  end
  else if (CYCLE == 7'd16) begin
    if (mcbase == 6'h3f) begin
      dma_request <= 1'b0;
      /* VIC-Article says display is reset here. It doesn't work: last
         MOB raster isn't displayed. */
      //display <= 1'b0;
    end
  end
  else if (dma_start && !dma_request) begin
    dma_request <= 1'b1;
    mcbase <= 6'h00;
  end
  else if (raster_end) begin
    if (dma_request && mxy_match)
      display <= 1'b1;
    else if (!dma_request)
      display <= 1'b0;
  end
end

always @(posedge PCLK) if (CLKEN)
  mxx_match <= mxx == X;

always @(posedge PCLK) if (clken_dly1) begin
  if (display && mxx_match)
    shift_data_out <= 1'b1;
  else if (raster_end)
    shift_data_out <= 1'b0;
end

// shift_reg shifts out a bit every /
// pix_code_reg gets a new pixel every \
//
// mxxe mxxc 01230123
// 0    0    XXXXXXXX
// 0    1    X/X/X/X/
// 1    0    X X X X 
// 1    1    X / X / 

assign shift_cnt_max = mxxe;

always @(posedge PCLK) if (CLKEN) begin
  if (raster_end)
    shift_cnt <= 1'b0;
  else if (shift_data_out) begin
    if (shift_cnt == shift_cnt_max)
      shift_cnt <= 1'b0;
    else
      shift_cnt <= shift_cnt + 1'b1;
  end
end

// dma_grant is high during bclk CYCLE n. DATA_IN is valid at n+1.
always @(posedge PCLK) if (BCLK_POSEDGE)
  data_in_valid <= dma_grant;

always @(negedge PCLK) if (BCLK_POSEDGE) begin
  shift_reg_in_valid <= data_in_valid;
  shift_reg_in <= DATA_IN;
end

always @*
  shift_data_in_go <= shift_reg_in_valid && BCLK_POSEDGE;

always @(negedge PCLK)
  shift_data_out_go <= shift_data_out && shift_cnt == 2'b00;

always @(posedge PCLK) begin
  if (shift_data_in_go)         // gated by CLKEN
    shift_reg <= { shift_reg[15:0], shift_reg_in };
  else if (clken_dly1 && shift_data_out_go)
    shift_reg <= shift_reg << 1;
end

always @* begin
  case ({mxxe, mxmc})
    2'b00: pix_cnt_max <= 2'b00;
    2'b01: pix_cnt_max <= 2'b01;
    2'b10: pix_cnt_max <= 2'b01;
    2'b11: pix_cnt_max <= 2'b11;
    default: pix_cnt_max <= 2'bxx;
  endcase
end

always @(posedge PCLK) if (clken_dly1) begin
  if (raster_end)
    pix_cnt <= 2'b00;
  else if (shift_data_out) begin
    if (pix_cnt == pix_cnt_max)
      pix_cnt <= 2'b00;
    else
      pix_cnt <= pix_cnt + 2'b01;
  end
end

assign pix_code_reg_go = shift_data_out && pix_cnt == 2'b00;

always @(negedge PCLK) if (clken_dly2) begin
  if (!shift_data_out)
    pix_code_reg <= 2'b00;
  else if (pix_code_reg_go) begin
    if (mxmc)
      pix_code_reg <= shift_reg[23:22];
    else
      pix_code_reg <= { shift_reg[23], 1'b0 };
  end
end

always @* begin 
  case (pix_code_reg)
    2'b00: { alpha, color } <= { 1'b0, 4'b0000 };
    2'b01: { alpha, color } <= { 1'b1, MM0 };
    2'b10: { alpha, color } <= { 1'b1, mxc };
    2'b11: { alpha, color } <= { 1'b1, MM1 };
    default: { alpha, color } <= { 1'bx, 4'bxxxx };
  endcase
end

always @* begin
  pix[5] <= ~mxdp;              // fg(1)/bg(0): mxdp=0 is fg
  pix[4] <= alpha;
  pix[3:0] <= color;
end

reg [8:0] x_dly;
always @(negedge PCLK) begin
  x_dly <= X;
end

always @(posedge PCLK) begin
  OUT_X <= x_dly;
  OUT_CLKEN <= clken_dly1;
end

endmodule
