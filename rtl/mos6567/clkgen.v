module mos6567_clkgen
  (
   input            PCLK,

   output reg       CP1,
   output           CP1_POSEDGE,
   output           CP1_NEGEDGE,
   output reg       CP2,
   output           CP2_POSEDGE,
   output           CP2_NEGEDGE,

   output reg       BCLK,
   output           BCLK_POSEDGE,
   output           BCLK_NEGEDGE,

   output reg       CCLK, 
   output           CCLK_POSEDGE, 
   output           CCLK_NEGEDGE
   );

reg [2:0] clk_cnt;

initial begin
  clk_cnt = 3'b000;
end

//                               clk_cnt: 0 1 2 3 4 5 6 7 0
// pixel clock                      PCLK: \/\/\/\/\/\/\/\/\
//                                          ___     ___
// bus clock = PCLK / 4             BCLK: _/   \___/   \___
//                                        _         _______
// cycle clock = PCLK / 8           CCLK:  \_______/
//                                          _____        
// CPU clock phase 1 = PCLK / 8     CP1:  _/     \_________
//                                                  _____
// CPU clock phase 2 = PCLK / 8     CP2:  _________/     \_

// clk_cnt moves on PCLK falling edge, and all *_posedge/*_negedge are
// sampled on PCLK rising edge.

always @(negedge PCLK)
  clk_cnt <= clk_cnt + 3'b001;

assign BCLK_POSEDGE = clk_cnt[1:0] == 2'd0;
assign BCLK_NEGEDGE = clk_cnt[1:0] == 2'd2;

assign CCLK_POSEDGE = clk_cnt[2:0] == 3'd4;
assign CCLK_NEGEDGE = clk_cnt[2:0] == 3'd0;

assign CP1_POSEDGE = clk_cnt[2:0] == 3'd0;
assign CP1_NEGEDGE = clk_cnt[2:0] == 3'd3;

assign CP2_POSEDGE = clk_cnt[2:0] == 3'd4;
assign CP2_NEGEDGE = clk_cnt[2:0] == 3'd7;

// The derived clocks, however, move on PCLK rising edge.
always @(posedge PCLK) BCLK <= ~clk_cnt[1];
always @(posedge PCLK) CCLK <= clk_cnt[2];
always @(posedge PCLK) CP1 <= ~clk_cnt[2] && clk_cnt[1:0] != 2'd3;
always @(posedge PCLK) CP2 <= clk_cnt[2] && clk_cnt[1:0] != 2'd3;

endmodule
