module mos6567_gfx_data_seq
  (
   input            PCLK,
   input            CLKEN,      // @(negedge PCLK)
   input            CCLK_POSEDGE,
   input            CCLK_NEGEDGE,

   input [8:0]      RASTER,
   input [6:0]      CYCLE,
   input [8:0]      X,
   input            BAD_LINE,
   input            VERTICAL,

   input [11:0]     C_DATA_IN,
   input [7:0]      G_DATA_IN,

   input [2:0]      XSCROLL,
   input            ECM,
   input            BMM,
   input            MCM,
   input [3:0]      B0C,
   input [3:0]      B1C,
   input [3:0]      B2C,
   input [3:0]      B3C,

   output reg [9:0] VC,
   output reg [2:0] RC,

   output           DISPLAY_STATE,

   output reg [5:0] PIX, // 1'fg/bg + 1'alpha + 4'color
   output reg       OUT_CLKEN,
   output reg [8:0] OUT_X
   );

reg [9:0] vcbase, vc;

reg [11:0] c_data;
reg [11:0] c_data_dly;
reg [11:0] c_data_dly2;
wire       mc_flag;
reg        mc_flag_dly;
reg        mc_flag_dly2;

wire raster_end = CYCLE == 7'd59;
wire g_data_in_valid = CYCLE >= 7'd16 && CYCLE <= 7'd55;

reg       x_match;
reg [7:0] shift_reg;
reg [7:0] shift_reg_in;
reg       shift_reg_in_valid;
reg       shift_data_in_go;
reg       pix_cnt;
reg       pix_cnt_max;
wire      pix_code_reg_go;
reg [1:0] pix_code_reg;
reg       fg;
reg [3:0] color, color_masked;

reg       display_logic_state;
`define DISPLAY 1'b1
`define IDLE    1'b0

initial begin
  display_logic_state = `IDLE;
  vc = 10'd0;
  vcbase = 10'd0;
  RC = 3'd0;
end

reg clken_dly1;                 // @(posedge PCLK)
always @(posedge PCLK)
  clken_dly1 <= CLKEN;

reg clken_dly2;                 // @(negedge PCLK)
always @(negedge PCLK)
  clken_dly2 <= clken_dly1;

always @(posedge PCLK) if (CCLK_NEGEDGE) begin
  if (BAD_LINE)
    display_logic_state <= `DISPLAY;

  if (RASTER == 9'd0 && CYCLE == 7'd13-1)
    vcbase <= 10'd0;

  if (CYCLE == 7'd14-1) begin
    vc <= vcbase;
    if (BAD_LINE)
      RC <= 3'd0;
  end
  else if (CYCLE >= 7'd15 && CYCLE <= 7'd54 &&
           display_logic_state == `DISPLAY) begin
    vc <= vc + 10'd1;
  end
  else if (CYCLE == 7'd58-1) begin
    if (RC == 8'd7) begin
      vcbase <= vc;
      if (!BAD_LINE)
        display_logic_state <= `IDLE;
    end
  end
  else if (CYCLE == 7'd59-1) begin
    if (display_logic_state == `DISPLAY)
      RC <= RC + 3'd1;
  end
end

// addr_generator needs VC valid during g-access when in bitmap modes.
always @(posedge PCLK) if (CCLK_POSEDGE)
  VC <= vc;

assign DISPLAY_STATE = display_logic_state == `DISPLAY;
                     
// g-access starts @CCLK_NEGEDGE, ends @CCLK_POSEDGE. Data on
// G_DATA_IN is valid for 1/2 cclk after. shift_reg can pick it up at
// any PCLK, so latch shift_reg_in here.
always @(negedge PCLK) if (CCLK_POSEDGE) begin
  shift_reg_in_valid <= g_data_in_valid;
  shift_reg_in <= G_DATA_IN;
end

// shift_reg shifts out a bit every /
// pix_code_reg gets a new pixel every \
//
// mc_flag   01230123
// 0         XXXXXXXX
// 1         X/X/X/X/

always @(negedge PCLK) if (clken_dly1) begin
  x_match <= X[2:0] == XSCROLL;
end

always @*
  shift_data_in_go = shift_reg_in_valid && x_match;

always @(posedge PCLK) if (clken_dly2) begin
  if (shift_data_in_go)
    shift_reg <= shift_reg_in;
  else
    shift_reg <= shift_reg << 1;
end

// In idle state, video matrix data (normally read in the c-accesses)
// is all "0" bits.
wire [11:0] c_data_masked = DISPLAY_STATE ? C_DATA_IN : 12'h000;

assign mc_flag = g_data_in_valid && MCM && (BMM || c_data_masked[11]);

// C_DATA_IN is valid 1/2 cclk before shift_reg_in is valid. Delay it
// so they are aligned.
always @(posedge PCLK) if (CCLK_POSEDGE) begin
  c_data_dly <= c_data_masked;
  mc_flag_dly <= mc_flag;
end

// Align c_data changes with the first bit out of shift_reg, such that
// c_data is in sync with pix_code_reg. That means we need to:

// 1. Simultaneously with shift_reg, c_data_dly => c_data_dly2
always @(posedge PCLK) if (clken_dly2) begin
  if (shift_data_in_go) begin
    c_data_dly2 <= c_data_dly;
    mc_flag_dly2 <= mc_flag_dly;
  end
end

// 2. 1/2 PCLK later, c_data_dly2 => c_data
always @(negedge PCLK) if (OUT_CLKEN) begin
  c_data <= c_data_dly2;
end

always @(negedge PCLK) if (OUT_CLKEN) begin
  pix_cnt_max <= mc_flag_dly;
end

always @(posedge PCLK) if (clken_dly2) begin
  if (shift_data_in_go)
    pix_cnt <= 1'b0;
  else begin
    if (pix_cnt == pix_cnt_max)
      pix_cnt <= 1'b0;
    else
      pix_cnt <= pix_cnt + 1'b1;
  end
end
assign pix_code_reg_go = pix_cnt == 1'b0;
  
always @(negedge PCLK) if (OUT_CLKEN) begin
  if (pix_code_reg_go) begin
    if (mc_flag_dly2)
      pix_code_reg <= shift_reg[7:6];
    else
      pix_code_reg <= {2{shift_reg[7]}};
  end
end

wire [2:0] mode = {ECM, BMM, MCM};
always @* begin 
  if (mode == 3'b000) begin
    case (pix_code_reg[1])
      1'b0: color <= B0C;
      1'b1: color <= c_data[11:8];
      default: color <= 4'bx;
    endcase
  end
  else if (mode == 3'b001) begin
    case (pix_code_reg)
      2'b00: color <= B0C;
      2'b01: color <= B1C;
      2'b10: color <= B2C;
      2'b11: color <= { 1'b0, c_data[10:8] };
      default: color <= 4'bx;
    endcase
  end
  else if (mode == 3'b010) begin
    case (pix_code_reg[1])
      1'b0: color <= c_data[3:0];
      1'b1: color <= c_data[7:4];
      default: color <= 4'bx;
    endcase
  end
  else if (mode == 3'b011) begin
    case (pix_code_reg)
      2'b00: color <= B0C;
      2'b01: color <= c_data[7:4];
      2'b10: color <= c_data[3:0];
      2'b11: color <= c_data[11:8];
      default: color <= 4'bx;
    endcase
  end
  else if (mode == 3'b100) begin
    case (pix_code_reg[1])
      1'b0: case (c_data[7:6])
              2'b00: color <= B0C;
              2'b01: color <= B1C;
              2'b10: color <= B2C;
              2'b11: color <= B3C;
              default: color <= 4'bx;
            endcase
      1'b1: color <= c_data[11:8];
      default: color <= 4'bx;
    endcase
  end
  else if (mode == 3'b101 ||
           mode == 3'b110 ||
           mode == 3'b111) begin
    color <= 4'h0;
  end
  else
    color <= 4'bx;
end

always @* begin
  // We only output data if border.vertical is not set. Otherwise we
  // output the background color.
  if (VERTICAL) begin
    fg <= 1'b0;
    color_masked <= B0C;
  end
  else begin
    fg <= pix_code_reg[1];
    color_masked <= color;
  end
end

// At this point in the pipeline, X is ahead by 1.5*PCLK.
always @* begin
  PIX[5] <= fg;
  PIX[4] <= 1'b1;               // alpha
  PIX[3:0] <= color_masked;
end

reg [8:0] x_dly;
always @(negedge PCLK) begin
  x_dly <= X;
end

always @(posedge PCLK) begin
  OUT_X <= x_dly;
  OUT_CLKEN <= clken_dly1;
end

endmodule
