// Design based on "The MOS 6567/6569 video controller (VIC-II) and
// its application in the Commodore 64", Christian Bauer,
// www.cebix.net, 28.Aug.1996

// Analog video outputs (SYNC+LUM, COLOR) aren't feasible in an
// FPGA. Also, we're targetting a digital video display (VGA,
// HDMI). We output digital baseband video (RGB, H/V sync).

// Signals specific to Dynamic RAM (RAS, CAS, multiplexed Address) are
// not required in this design and have been replaced by a simple "bus
// clock" BCLK.

// The original device synthesized the CPU clock phase 2 (CP2) from
// CLKIN by dividing by 8. We also synthesize the phase 1 (CP1).

// Clocking resources are kept to a minimum by distributing the
// fastest clock (CLKIN) thru the design and expressing edges of
// derived clocks as clock enables. This makes timing closure
// achievable.

// TODO: Confirm conformance to X/Y position decodes in 6567-13.GIF

`include "mob_bus.vh"

module mos6567
  (
   input         CLKIN,          // pixel clock
                  
   output        BCLK,           // bus clock
   output        BCLK_POSEDGE,
   output        BCLK_NEGEDGE,
   output        CP1,            // CPU clock phase 1
   output        CP1_POSEDGE,
   output        CP1_NEGEDGE,
   output        CP2,            // CPU clock phase 2
   output        CP2_POSEDGE,
   output        CP2_NEGEDGE,

   input [5:0]   A_I,            // address bus, input
   output [13:0] A_O,            //   output
   output        A_OE,           //   output enable
   input [11:0]  DB_I,           // data bus, input
   output [7:0]  DB_O,           //   output (only lower 8 bits)
   output        DB_OE,          //   output enable
   input         RW,             // read / not write
   input         nCS,            // chip select

   output        BA,             // bus available
   output        AEC,            // CPU address bus enable

   output        nIRQ,           // interrupt request

   input         LP,             // light pen

   // digital baseband video output
   output        DE,             // data enable
   output        HS,             // horizontal sync
   output        VS,             // vertical sync
   output [7:0]  R,              // red component
   output [7:0]  G,              // green component
   output [7:0]  B,              // blue component

   input         HOLD            // halt and release A,D,RW
   );

wire pclk = CLKIN;

wire bclk, bclk_posedge, bclk_negedge;
wire bclk_posedge_ungated, bclk_negedge_ungated;
wire cclk, cclk_posedge, cclk_negedge;
wire cclk_posedge_ungated, cclk_negedge_ungated;

wire [7:0] reg_data_out;
reg [7:0]  reg_data_in;
wire       reg_select = !nCS;
reg        reg_read;
reg        reg_write;

wire [`MOB_BUS_BITS-1:0] mob_bus[7:0];

wire [7:0] mxm;
wire [7:0] mxd;
wire       mxm_res;
wire       mxd_res;

wire [8:0] raster;
wire [8:0] raster_cmp;
wire [6:0] cycle;
wire [8:0] x;

wire       ecm, bmm, den, rsel, mcm, csel;
wire [2:0] yscroll, xscroll;

wire       irq;
wire       erst, irst, irst_set, irst_res;
wire       elp, ilp, ilp_set, ilp_res;
wire       embc, imbc, imbc_set, imbc_res;
wire       emmc, immc, immc_set, immc_res;

wire [13:10] vm;
wire [13:11] cb;

wire [3:0]   ec;
wire [3:0]   b0c;
wire [3:0]   b1c;
wire [3:0]   b2c;
wire [3:0]   b3c;
wire [3:0]   mm0;
wire [3:0]   mm1;

wire [9:0]   vc;
wire [2:0]   rc;
wire         c_access_req;

wire [11:0]  vmcl_out;
wire         vmcl_enable_in;
wire         vmcl_shift;

reg [11:0]   data_in;
reg          data_in_valid;
wire [13:0]  addr_out;
reg [5:0]    addr_in;
wire         bad_line;

wire [5:0]   gfx_pix;
wire [8:0]   gfx_x;

wire [3:0]   mux_pix;
wire [8:0]   mux_x;

wire         vertical;
wire [3:0]   border_pix;
wire [8:0]   border_x;
wire         blank;

mos6567_clkgen clkgen
  (
   .PCLK(pclk),

   .CP1(CP1),
   .CP1_POSEDGE(CP1_POSEDGE),
   .CP1_NEGEDGE(CP1_NEGEDGE),
   .CP2(CP2),
   .CP2_POSEDGE(CP2_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),

   .BCLK(bclk),
   .BCLK_POSEDGE(bclk_posedge_ungated),
   .BCLK_NEGEDGE(bclk_negedge_ungated),

   .CCLK(cclk),
   .CCLK_POSEDGE(cclk_posedge_ungated),
   .CCLK_NEGEDGE(cclk_negedge_ungated)
   );

reg clken;
always @(negedge pclk)
  clken <= !HOLD;

assign bclk_posedge = bclk_posedge_ungated && clken;
assign bclk_negedge = bclk_negedge_ungated && clken;
assign cclk_posedge = cclk_posedge_ungated && clken;
assign cclk_negedge = cclk_negedge_ungated && clken;

assign BCLK = bclk;
assign BCLK_POSEDGE = bclk_posedge_ungated;
assign BCLK_NEGEDGE = bclk_negedge_ungated;

mos6567_reg_file reg_file
  (
   .PCLK(pclk),
   .BCLK_POSEDGE(bclk_posedge_ungated),
   .ADDR(addr_in),
   .DATA_IN(reg_data_in),
   .READ(reg_read),
   .WRITE(reg_write),
   .DATA_OUT(reg_data_out),

   .COMPRESS_MOB_BUS(`COMPRESS_MOB_BUS(mob_bus)),

   .MXM(mxm),
   .MXM_RES(mxm_res),
   .MXD(mxd),
   .MXD_RES(mxd_res),

   .RASTER(raster),
   .RASTER_CMP(raster_cmp),

   .ECM(ecm),
   .BMM(bmm),
   .DEN(den),
   .RSEL(rsel),
   .YSCROLL(yscroll),
   .RES(),
   .MCM(mcm),
   .CSEL(csel),
   .XSCROLL(xscroll),
   .VM(vm),
   .CB(cb),

   .EC(ec),
   .B0C(b0c),
   .B1C(b1c),
   .B2C(b2c),
   .B3C(b3c),
   .MM0(mm0),
   .MM1(mm1),

   .IRQ(irq),
   .ERST(erst),
   .IRST(irst),
   .IRST_RES(irst_res),
   .EMBC(embc),
   .IMBC(imbc),
   .IMBC_RES(imbc_res),
   .EMMC(emmc),
   .IMMC(immc),
   .IMMC_RES(immc_res),
   .ELP(elp),
   .ILP(ilp),
   .ILP_RES(ilp_res)
   );

assign ilp_set = LP;

mos6567_interrupt interrupt
  (
   .PCLK(pclk),
   .BCLK_POSEDGE(bclk_posedge_ungated),

   .ERST(erst),
   .IRST_SET(irst_set),
   .IRST_RES(irst_res),
   .IRST(irst),

   .EMBC(embc),
   .IMBC_SET(imbc_set),
   .IMBC_RES(imbc_res),
   .IMBC(imbc),

   .EMMC(emmc),
   .IMMC_SET(immc_set),
   .IMMC_RES(immc_res),
   .IMMC(immc),

   .ELP(elp),
   .ILP_SET(ilp_set),
   .ILP_RES(ilp_res),
   .ILP(ilp),

   .IRQ(irq)
   );

mos6567_raster_counter raster_counter
  (
   .PCLK(pclk),
   .CLKEN(!HOLD),
   .CCLK_POSEDGE(cclk_posedge),
   .CCLK_NEGEDGE(cclk_negedge),
   .CP2_NEGEDGE(CP2_NEGEDGE),

   .RASTER_CMP(raster_cmp),

   .RASTER(raster),
   .CYCLE(cycle),
   .X(x),

   .RASTER_MATCH(irst_set)
   );

mos6567_addr_generator addr_generator
  (
   .PCLK(pclk),
   .CCLK(cclk),
   .CCLK_POSEDGE(cclk_posedge),
   .CCLK_NEGEDGE(cclk_negedge),
   .BCLK_POSEDGE(bclk_posedge),
   .BCLK_NEGEDGE(bclk_negedge),

   .RASTER(raster),
   .CYCLE(cycle),
   .ECM(ecm),
   .BMM(bmm),
   .VM(vm),
   .CB(cb),
   .DEN(den),
   .YSCROLL(yscroll),

   .DATA_IN(data_in[7:0]),

   .VMCL_DATA(vmcl_out[7:0]),
   .VMCL_ENABLE_IN(vmcl_enable_in),
   .VMCL_SHIFT(vmcl_shift),

   .DISPLAY_STATE(display_state),
   .VC(vc),
   .RC(rc),

   .COMPRESS_MOB_BUS(`COMPRESS_MOB_BUS(mob_bus)),

   .BAD_LINE(bad_line),
   .ADDR(addr_out),
   .BA(ba),
   .AEC(aec)
   );

mos6567_vmcl vmcl
  (
   .PCLK(pclk),
   .BCLK_POSEDGE(bclk_posedge),
   .BCLK_NEGEDGE(bclk_negedge),
   .DATA_IN(data_in),
   .ENABLE_IN(vmcl_enable_in),
   .SHIFT(vmcl_shift),

   .DATA_OUT(vmcl_out)
   );

mos6567_gfx_data_seq gfx_data_seq
  (
   .PCLK(pclk),
   .CLKEN(clken),
   .CCLK_POSEDGE(cclk_posedge),
   .CCLK_NEGEDGE(cclk_negedge),

   .RASTER(raster),
   .CYCLE(cycle),
   .X(x),
   .BAD_LINE(bad_line),
   .VERTICAL(vertical),

   .C_DATA_IN(vmcl_out),
   .G_DATA_IN(data_in[7:0]),

   .XSCROLL(xscroll),
   .ECM(ecm),
   .BMM(bmm),
   .MCM(mcm),
   .B0C(b0c),
   .B1C(b1c),
   .B2C(b2c),
   .B3C(b3c),

   .VC(vc),
   .RC(rc),

   .DISPLAY_STATE(display_state),

   .PIX(gfx_pix),
   .OUT_CLKEN(gfx_clken),
   .OUT_X(gfx_x)
   );

genvar i;
generate
  for (i = 0; i <= 7; i = i + 1) begin: mob
    mos6567_mob_data_seq mob_data_seq
    (
     .PCLK(pclk),
     .CLKEN(clken),
     .CCLK_POSEDGE(cclk_posedge),
     .CCLK_NEGEDGE(cclk_negedge),
     .BCLK_POSEDGE(bclk_posedge),
     .BCLK_NEGEDGE(bclk_negedge),

     .MOB_BUS(mob_bus[i]),

     .RASTER(raster),
     .CYCLE(cycle),
     .X(x),
     .DATA_IN(data_in[7:0]),
     .MM0(mm0),
     .MM1(mm1),

     .OUT_X(),
     .OUT_CLKEN()
     );
  end
endgenerate

mos6567_mux mux
  (
   .PCLK(pclk),
   .CLKEN(gfx_clken),

   .MXM(mxm),
   .MXM_RES(mxm_res),
   .MXD(mxd),
   .MXD_RES(mxd_res),

   .GFX_PIX(gfx_pix),
   .COMPRESS_MOB_BUS(`COMPRESS_MOB_BUS(mob_bus)),
   .X(gfx_x),

   .IMBC_SET(imbc_set),
   .IMMC_SET(immc_set),

   .OUT_PIX(mux_pix),
   .OUT_CLKEN(mux_clken),
   .OUT_X(mux_x)
   );

mos6567_border border
  (
   .PCLK(pclk),
   .CLKEN(mux_clken),

   .RASTER(raster),
   .CYCLE(cycle),
   .X(mux_x),

   .DEN(den),
   .CSEL(csel),
   .RSEL(rsel),

   .IN_PIX(mux_pix),
   .EC(ec),

   .VERTICAL(vertical),
   .OUT_CLKEN(border_clken),
   .OUT_PIX(border_pix),
   .OUT_X(border_x)
   );

mos6567_color_gen color_gen
  (
   .X(border_x),
   .PIX(border_pix),
   .BLANK(blank),

   .RED(R),
   .GREEN(G),
   .BLUE(B)
   );

mos6567_sync_gen sync_gen
  (
   .PCLK(pclk),
   .CLKEN(border_clken),

   .RASTER(raster),
   .X(border_x),

   .HSYNC(HS),
   .VSYNC(VS),
   .BLANK(blank)
   );

reg  hold_dly;
always @(posedge pclk) if (CP1_POSEDGE)
  hold_dly <= HOLD;

assign nIRQ = !irq;
assign DB_O[7:0] = reg_data_out;
assign DB_OE = reg_select && RW;
assign A_O = addr_out;
assign A_OE = !(aec || hold_dly);
assign BA = ba || hold_dly;
assign AEC = aec || hold_dly;
assign DE = !blank;

always @(posedge pclk) if (bclk_posedge_ungated && !hold_dly) begin
  data_in[11:8] <= DB_I[11:8];
  data_in[7:0] <= data_in_valid ? DB_I[7:0] : 8'hff;
end

always @(posedge pclk) if (bclk_negedge_ungated) begin // approx. where CAS falls
  addr_in <= A_I[5:0];
  reg_read <= reg_select && RW;
  reg_write <= reg_select && !RW;
  data_in_valid <= !aec && RW;
  // DB should be latched no later than CP2 negedge.
  reg_data_in <= DB_I[7:0];
end

endmodule
