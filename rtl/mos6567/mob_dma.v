/* MOB accesses follow this sequence:
 *
 * CCLK   cycle  t   BA   DMA_GRANT   access
 * 0      n-4    --  0    0           ---       START 0=>1
 * 0      n-3    t0  1    0           :
 * 0      n-2    t1  :    :           :         NEXT 0=>1
 * 0      n-1    t2  :    :           :
 * 0      n      t3  1    0           p-access
 * 1      n      t3  1    1           s-access
 * 0      n+1    t4  1    1           s-access
 * 1      n+1    t4  1    1           s-access
 * 0      n+2    --  0    0           ---
 * 
 * If START is set, then on @CCLK_NEGEDGE, t begins counting t0 up to
 * t4. BA and DMA_GRANT (s-access) are masked by DMA_REQUEST. FETCH_P
 * always happens at the p-access. */

module mos6567_mob_dma
  (
   input  PCLK,
   input  CCLK,
   input  CCLK_NEGEDGE,

   input  DMA_REQUEST,
   input  START,
   
   output NEXT,                 // input to next module's START
   output BA,
   output ACTIVE,               // p- and s-access (would) happen here
   output FETCH_P,
   output DMA_GRANT
   );

reg [4:0] t;
reg       req;

always @(posedge PCLK) if (CCLK_NEGEDGE) begin
  t <= t << 1 | START;
end

assign BA = DMA_REQUEST && |t[4:0];
assign NEXT = t[1];
assign ACTIVE = t[3] || t[4];
assign FETCH_P = t[3] && !CCLK;
assign DMA_GRANT = DMA_REQUEST && ACTIVE && !FETCH_P;

endmodule
