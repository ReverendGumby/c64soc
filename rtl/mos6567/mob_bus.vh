// MOB signal bus

`define MOB_BUS_BITS           40
`define MOB_BUS_IDX(i, m)      ((i)*`MOB_BUS_BITS+(m))
`define MOB_BUS_RANGE(i, m, n) (`MOB_BUS_IDX(i, m+(n)-1)):`MOB_BUS_IDX(i, m)

`define MOB_BUS_MXX(bus, i)    bus[`MOB_BUS_RANGE(i, 0, 9)]
`define MOB_BUS_MXY(bus, i)    bus[`MOB_BUS_RANGE(i, 9, 8)]
`define MOB_BUS_MXC(bus, i)    bus[`MOB_BUS_RANGE(i, 17, 4)]
`define MOB_BUS_MXE(bus, i)    bus[`MOB_BUS_IDX(i, 21)]
`define MOB_BUS_MXYE(bus, i)   bus[`MOB_BUS_IDX(i, 22)]
`define MOB_BUS_MXDP(bus, i)   bus[`MOB_BUS_IDX(i, 23)]
`define MOB_BUS_MXMC(bus, i)   bus[`MOB_BUS_IDX(i, 24)]
`define MOB_BUS_MXXE(bus, i)   bus[`MOB_BUS_IDX(i, 25)]
`define MOB_BUS_DMAR(bus, i)   bus[`MOB_BUS_IDX(i, 26)]
`define MOB_BUS_DMAG(bus, i)   bus[`MOB_BUS_IDX(i, 27)]
`define MOB_BUS_MC(bus, i)     bus[`MOB_BUS_RANGE(i, 28, 6)]
`define MOB_BUS_PIX(bus, i)    bus[`MOB_BUS_RANGE(i, 34, 6)]

// Helper to compress bus arrays

`define COMPRESS_MOB_BUS(busa) { busa[7], busa[6], busa[5], busa[4], busa[3], busa[2], busa[1], busa[0] }
