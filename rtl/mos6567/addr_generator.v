// next line (abbreviated):
// 
// Cycl-# 6                   1 1 1 1 1 1 1 1 1 1 |5 5 5 5 5 5 5 6 6 6 6 6 6
//        5 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 |3 4 5 6 7 8 9 0 1 2 3 4 5 1
//         _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _| _ _ _ _ _ _ _ _ _ _ _ _ _ _
//     ø0 _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ |_ _ _ _ _ _ _ _ _ _ _ _ _ _
//        __                                      |
//    IRQ   ______________________________________|____________________________
//                              __________________|________
//     BA ______________________                  |        ____________________
//                               _ _ _ _ _ _ _ _ _| _ _ _ _ _ _ _
//    AEC _______________________ _ _ _ _ _ _ _ _ |_ _ _ _ _ _ _ ______________
//                                                |
//    VIC ss3sss4sss5sss6sss7sssr r r r r g g g g |g g g i i i i 0sss1sss2sss3s
//   6510                        x x x x x x x x x| x x x x X X X
//                                                |
// Graph.                      |===========0102030|7383940=============
//                                                |
// X coo. \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\|\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//        1111111111111111111111111110000000000000|1111111111111111111111111111
//        999aaaabbbbccccddddeeeeffff0000111122223|344445555666677778888889999a
//        48c048c048c048c048c048c048c048c048c048c0|c048c048c048c048c048ccc048c0

`include "mob_bus.vh"

module mos6567_addr_generator
  (
   input                       PCLK,
   input                       CCLK,
   input                       CCLK_POSEDGE,
   input                       CCLK_NEGEDGE,
   input                       BCLK_POSEDGE,
   input                       BCLK_NEGEDGE,

   input [8:0]                 RASTER,
   input [6:0]                 CYCLE,
   input                       ECM,
   input                       BMM,
   input [13:10]               VM,
   input [13:11]               CB,
   input                       DEN,
   input [2:0]                 YSCROLL,

   input [7:0]                 DATA_IN,

   input [7:0]                 VMCL_DATA,
   output reg                  VMCL_ENABLE_IN,
   output reg                  VMCL_SHIFT,

   input                       DISPLAY_STATE,
   input [9:0]                 VC,
   input [2:0]                 RC,

   inout [`MOB_BUS_BITS*8-1:0] COMPRESS_MOB_BUS,

   output reg                  BAD_LINE,
   output reg [13:0]           ADDR,
   output reg                  BA,
   output                      AEC
   );

reg [2:0] access;
`define i_access    3'd0        // idle access
`define c_access    3'd1        // video matrix + color RAM
`define g_access    3'd2        // char. generator or bitmap
`define p_access    3'd3        // MOB data pointer
`define s_access    3'd4        // MOB data
`define r_access    3'd5        // DRAM refresh

wire [5:0] mcbase[7:0];
reg [7:0]  mob_dma_start;
wire [7:0] mob_dma_next;
wire [7:0] mob_ba;
wire [7:0] dma_request;
wire [7:0] dma_grant;
wire [7:0] mob_active;
wire [7:0] mob_fetch_p;

// den_set_window_start: DEN was set for at least one CCLK on the
// first line of the visible window (RASTER $30).
reg den_set_window_start;
initial den_set_window_start = 1'b0;
always @(posedge PCLK) if (CCLK_POSEDGE) begin
  if (RASTER == 9'h030) begin
    if (DEN)
      den_set_window_start <= 1'b1;
  end
  else if (RASTER == 9'h02f /*guess*/) begin
    den_set_window_start <= 1'b0;
  end
end

always @(posedge PCLK) if (CCLK_NEGEDGE) begin
  BAD_LINE <= (RASTER >= 9'h030 && RASTER <= 9'h0f7) &&
              RASTER[2:0] == YSCROLL && den_set_window_start;
end

always @* begin
  access = `i_access;
  if (!CCLK && CYCLE >= 7'd11 && CYCLE <= 7'd15)
    access = `r_access;
  else if (CCLK && CYCLE >= 7'd15 && CYCLE <= 7'd54 && 
           BAD_LINE && DISPLAY_STATE)
    access = `c_access;
  else if (!CCLK && CYCLE >= 7'd16 && CYCLE <= 7'd55)
    access = `g_access;
  else if (|mob_fetch_p)
    access = `p_access;
  else if (|dma_grant)
    access = `s_access;
end

always @* begin
  BA <= !((BAD_LINE && CYCLE >= 7'd12 && CYCLE <= 7'd54) // c-access
          || |mob_ba);                                   // s-access
end

/* c-access: Video matrix + color RAM */

always @* begin
  VMCL_ENABLE_IN <= access == `c_access;
  VMCL_SHIFT <= access == `g_access;
end

/* g-access: Char. generator or bitmap */

// ECM BMM MCM | 13 | 12 | 11 | 10 |  9 |  8 |  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 |
//             +----+----+----+----+----+----+----+----+----+----+----+----+----+----+
//  0   0   0  |CB13|CB12|CB11| D7 | D6 | D5 | D4 | D3 | D2 | D1 | D0 | RC2| RC1| RC0|
//  0   0   1  |CB13|CB12|CB11| D7 | D6 | D5 | D4 | D3 | D2 | D1 | D0 | RC2| RC1| RC0|
//  0   1   0  |CB13| VC9| VC8| VC7| VC6| VC5| VC4| VC3| VC2| VC1| VC0| RC2| RC1| RC0|
//  0   1   1  |CB13| VC9| VC8| VC7| VC6| VC5| VC4| VC3| VC2| VC1| VC0| RC2| RC1| RC0|
//  1   0   0  |CB13|CB12|CB11|  0 |  0 | D5 | D4 | D3 | D2 | D1 | D0 | RC2| RC1| RC0|
//  1   0   1  |CB13|CB12|CB11|  0 |  0 | D5 | D4 | D3 | D2 | D1 | D0 | RC2| RC1| RC0|
//  1   1   0  |CB13| VC9| VC8|  0 |  0 | VC5| VC4| VC3| VC2| VC1| VC0| RC2| RC1| RC0|
//  1   1   1  |CB13| VC9| VC8|  0 |  0 | VC5| VC4| VC3| VC2| VC1| VC0| RC2| RC1| RC0|

reg [13:0] g_addr_pre;
always @* begin
  if (DISPLAY_STATE)
    case (BMM)
      2'b0: g_addr_pre <= { CB[13:11], VMCL_DATA[7:0], RC[2:0] };
      2'b1: g_addr_pre <= { CB[13], VC[9:0], RC[2:0] };
      default: g_addr_pre <= 14'hxxxx;
    endcase
  else
    g_addr_pre <= 14'h3fff;
end

wire [13:0] g_addr;
assign g_addr[13:11] = g_addr_pre[13:11];
assign g_addr[10:9] = ECM ? 2'b00 : g_addr_pre[10:9];
assign g_addr[8:0] = g_addr_pre[8:0];

/* MOB DMA sequencers */

/* These generate timing for MOB data fetches, which happen at
   specific cycles on every raster. 'DMA' refers to the s-access
   portion (and accompanying BA assertion). The first is kicked off
   (mob_dma_start 0=>1) by a specific cycle, the next by the first
   after a fixed delay, and so on. */

genvar i;
generate
  for (i = 0; i <= 7; i = i + 1) begin: mob
    assign mcbase[i] = `MOB_BUS_MC(COMPRESS_MOB_BUS, i);
    assign dma_request[i] = `MOB_BUS_DMAR(COMPRESS_MOB_BUS, i);

    assign `MOB_BUS_DMAG(COMPRESS_MOB_BUS, i) = dma_grant[i];

    mos6567_mob_dma mob_dma
           (
            .PCLK(PCLK),
            .CCLK(CCLK),
            .CCLK_NEGEDGE(CCLK_NEGEDGE),

            .DMA_REQUEST(dma_request[i]),
            .START(mob_dma_start[i]),

            .NEXT(mob_dma_next[i]),
            .BA(mob_ba[i]),
            .ACTIVE(mob_active[i]),
            .FETCH_P(mob_fetch_p[i]),
            .DMA_GRANT(dma_grant[i])
            );
  end
endgenerate

// mob_ba[0] can go low in cycle 57.
always @* mob_dma_start[0] <= CYCLE == 7'd56;
always @* mob_dma_start[7:1] <= mob_dma_next[6:0];

/* p-access: MOB pointer */

reg [2:0]  p_mob_num;
integer j;
always @* begin
  p_mob_num = 3'dx;
  for (j = 0; j < 8; j = j + 1) begin
    if (mob_active[j]) begin
      p_mob_num = j[2:0];
    end
  end
end

wire [13:0] p_addr = { VM[13:10], 7'h7f, p_mob_num[2:0] };

// p-access data goes here:
wire        p_access;
reg         mp_fetch;
reg [7:0]   mp, mp_out;
reg [5:0]   mc, mc_out;

assign p_access = access == `p_access;

// mp_fetch: high on bclk following p-access, when DATA_IN holds mp.
always @(posedge PCLK) if (BCLK_POSEDGE) begin
  mp_fetch <= p_access;
end

always @(posedge PCLK) if (BCLK_NEGEDGE) begin
  if (mp_fetch)
    mp <= DATA_IN;
end

always @* mp_out <= mp_fetch ? DATA_IN : mp;

always @(posedge PCLK) if (BCLK_NEGEDGE) begin
  if (p_access)
    mc <= mcbase[p_mob_num];
  else
    mc <= mc + 6'd1;
end

always @(posedge PCLK) if (BCLK_POSEDGE) mc_out <= mc;

/* s-access: MOB data */
wire [13:0] s_addr = { mp_out[7:0], mc_out[5:0] };

/* r-access: DRAM refresh */
reg [7:0] ref;
initial ref = 8'hff;

always @(posedge PCLK) if (CCLK_NEGEDGE) begin
  if (RASTER == 9'd0 && CYCLE == 7'd10)
    ref <= 8'hff;
  else if (access == `r_access)
    ref <= ref - 8'd1;
end

/* address output */
always @* begin
  case (access)
    `i_access: ADDR <= 14'h3fff;
    `c_access: ADDR <= { VM[13:10], VC[9:0] };
    `g_access: ADDR <= g_addr;
    `p_access: ADDR <= p_addr;
    `s_access: ADDR <= s_addr;
    `r_access: ADDR <= { 6'h3f, ref[7:0] };
    default: ADDR <= 14'hxxxx;
  endcase
end

/* After BA goes low, three CP2 pulses occur, then AEC stays low. */
reg ba_d1, ba_d2, ba_d3, ba_d4;
always @(posedge PCLK) if (CCLK_POSEDGE) begin
  ba_d1 <= BA;
  ba_d2 <= BA || ba_d1;
  ba_d3 <= BA || ba_d2;
  ba_d4 <= BA || ba_d3;
end

assign AEC = CCLK && ba_d4;

endmodule
