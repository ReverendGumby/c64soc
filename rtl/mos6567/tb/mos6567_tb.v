`timescale 1ns / 1ps

module mos6567_tb
  (
   output       DE,             // data enable
   output       HS,             // horizontal sync
   output       VS,             // vertical sync
   output [7:0] R,              // red component
   output [7:0] G,              // green component
   output [7:0] B               // blue component
   );

wire [13:0] A;
wire        BA;
wire        AEC;
wire [11:0] DB;
wire        RW;
reg [11:0]  db_rom;
wire [7:0]  db_cg;
wire [11:0] db_vm;

reg         cs_set_reg = 1'b0;
reg [11:0]  db_set_reg;
reg [13:0]  a_set_reg;
reg         rw_set_reg;

reg cp2, pclk;

mos6567 mos6567
  (
   .CLKIN(pclk),
   .CP2(cp2),

   .A(A),
   .DB(DB),
   .RW(RW),
   .nCS(~cs_set_reg),

   .BA(BA),
   .AEC(AEC),

   .DE(DE),
   .HS(HS),
   .VS(VS),
   .R(R),
   .G(G),
   .B(B)
   );

chargen chargen
  (
   .A(A[11:0]),
   .D(db_cg)
   );

vm vm
  (
   .A(A[10:0]),
   .D(db_vm)
   );

initial begin
  cp2 = 1'b0;
  pclk = 1'b1;
end

always #500 begin :cp2gen
  cp2 = !cp2;
end

always #62.5 begin :pclkgen
  pclk = !pclk;
end

always @* begin
  if (A[13:12] == 3'b00)
    db_rom <= db_vm;
  else if (A[13:12] == 3'b01)
    db_rom <= { 4'b0000, db_cg };
  else if (A == 14'h3fff)
    db_rom <= 12'h055;
  else
    db_rom <= 12'hxxx;
end
assign #40 DB = cs_set_reg ? db_set_reg : db_rom;
assign A = cs_set_reg ? a_set_reg : 14'hzzzz;
assign RW = cs_set_reg ? rw_set_reg : 1'b1;

/* -----\/----- EXCLUDED -----\/-----
initial begin
  force mos6567.reg_file.csel = 1'b1;
  force mos6567.reg_file.rsel = 1'b1;
  force mos6567.reg_file.xscroll = 3'd0;
  force mos6567.reg_file.yscroll = 3'd3;

  force mos6567.reg_file.ecm = 1'b0;
  force mos6567.reg_file.bmm = 1'b0;
  force mos6567.reg_file.mcm = 1'b0;
  force mos6567.reg_file.ec = 4'he; // light blue
  force mos6567.reg_file.b0c = 4'h6; // blue
  force mos6567.reg_file.b1c = 4'h2;
  force mos6567.reg_file.b2c = 4'h3;
  force mos6567.reg_file.b3c = 4'h4;

  force mos6567.reg_file.vm = 4'b0000;
  force mos6567.reg_file.cb = 3'b010;

  force mos6567.reg_file.mxy[0] = 8'h50;
  force mos6567.reg_file.mxx[0] = 9'h0a0;
  force mos6567.reg_file.mxc[0] = 4'h0;
  force mos6567.reg_file.mxxe[0] = 1'b1;
  force mos6567.reg_file.mxye[0] = 1'b1;
  force mos6567.reg_file.mxmc[0] = 1'b0;

  force mos6567.reg_file.mxy[1] = 8'h58;
  force mos6567.reg_file.mxx[1] = 9'h0a8;
  force mos6567.reg_file.mxc[1] = 4'h1;
  force mos6567.reg_file.mxxe[1] = 1'b1;
  force mos6567.reg_file.mxye[1] = 1'b1;
  force mos6567.reg_file.mxmc[1] = 1'b0;

  force mos6567.reg_file.mxy[2] = 8'h60;
  force mos6567.reg_file.mxx[2] = 9'h0b0;
  force mos6567.reg_file.mxc[2] = 4'h2;
  force mos6567.reg_file.mxxe[2] = 1'b1;
  force mos6567.reg_file.mxye[2] = 1'b1;
  force mos6567.reg_file.mxmc[2] = 1'b0;

  force mos6567.reg_file.mxy[3] = 8'h68;
  force mos6567.reg_file.mxx[3] = 9'h0b8;
  force mos6567.reg_file.mxc[3] = 4'h3;
  force mos6567.reg_file.mxxe[3] = 1'b1;
  force mos6567.reg_file.mxye[3] = 1'b1;
  force mos6567.reg_file.mxmc[3] = 1'b0;

  force mos6567.reg_file.mxy[4] = 8'h70;
  force mos6567.reg_file.mxx[4] = 9'h0c0;
  force mos6567.reg_file.mxc[4] = 4'h4;
  force mos6567.reg_file.mxxe[4] = 1'b1;
  force mos6567.reg_file.mxye[4] = 1'b1;
  force mos6567.reg_file.mxmc[4] = 1'b0;

  force mos6567.reg_file.mxy[5] = 8'h78;
  force mos6567.reg_file.mxx[5] = 9'h0c8;
  force mos6567.reg_file.mxc[5] = 4'h5;
  force mos6567.reg_file.mxxe[5] = 1'b1;
  force mos6567.reg_file.mxye[5] = 1'b1;
  force mos6567.reg_file.mxmc[5] = 1'b0;

  force mos6567.reg_file.mxy[6] = 8'h80;
  force mos6567.reg_file.mxx[6] = 9'h0d0;
  force mos6567.reg_file.mxc[6] = 4'h7;
  force mos6567.reg_file.mxxe[6] = 1'b1;
  force mos6567.reg_file.mxye[6] = 1'b1;
  force mos6567.reg_file.mxmc[6] = 1'b0;

  force mos6567.reg_file.mxy[7] = 8'h88;
  force mos6567.reg_file.mxx[7] = 9'h0d8;
  force mos6567.reg_file.mxc[7] = 4'h8;
  force mos6567.reg_file.mxxe[7] = 1'b1;
  force mos6567.reg_file.mxye[7] = 1'b1;
  force mos6567.reg_file.mxmc[7] = 1'b0;

  force mos6567.reg_file.mxdp = 8'b11111111;
  force mos6567.reg_file.mxe = 8'b11111111;
end
 -----/\----- EXCLUDED -----/\----- */

/*
always #1040 begin
  //force mos6567.reg_file.xscroll = mos6567.reg_file.xscroll + 3'd1;
end

always #274560 begin
  force mos6567.reg_file.yscroll = mos6567.reg_file.yscroll + 3'd1;
  //force mos6567.reg_file.xscroll = mos6567.reg_file.xscroll + 3'd1;
end
*/

task set_reg(input [5:0] r, input [7:0] v); 
  begin
    @(posedge AEC) cs_set_reg = 1'b1;
    rw_set_reg = 1'b0;
    a_set_reg = { 8'b01000000, r };
    db_set_reg = { 4'b0000, v };
    @(negedge cp2) #40 cs_set_reg = 1'b0;
  end
endtask

task get_reg(input [5:0] r); 
  begin
    @(posedge AEC) cs_set_reg = 1'b1;
    rw_set_reg = 1'b1;
    a_set_reg = { 8'b01000000, r };
    db_set_reg = 12'hzzz;
    @(negedge cp2) #40 cs_set_reg = 1'b0;
  end
endtask

reg ecm = 1'b0;
reg bmm = 1'b0;
reg mcm = 1'b0;
reg [3:0] bg;

initial begin
  #2000 ;
  get_reg(6'h11);
  get_reg(6'h16);
  set_reg(6'h11, {1'b0, ecm, bmm, 5'b11011});
  set_reg(6'h16, {3'b000, mcm, 4'b1000});

  get_reg(6'h11);
  get_reg(6'h16);

  set_reg(6'h20, 8'h0e);
  set_reg(6'h21, 8'h06);
  set_reg(6'h22, 8'h02);
  set_reg(6'h23, 8'h03);
  set_reg(6'h24, 8'h04);

  set_reg(6'h18, 4'b00000100);

  set_reg(6'h01, 8'h50);
  set_reg(6'h00, 9'ha0);
  set_reg(6'h10, 8'b00000000);
  set_reg(6'h27, 8'h00);
  set_reg(6'h1d, 8'b11111111);
  set_reg(6'h17, 8'b11111111);
  set_reg(6'h1c, 8'b00000000);

  set_reg(6'h03, 8'h58);
  set_reg(6'h02, 8'ha8);
  set_reg(6'h28, 8'h01);

  set_reg(6'h05, 8'h60);
  set_reg(6'h04, 8'hb0);
  set_reg(6'h29, 8'h02);

  set_reg(6'h07, 8'h68);
  set_reg(6'h06, 8'hb8);
  set_reg(6'h2a, 8'h03);

  set_reg(6'h09, 8'h70);
  set_reg(6'h08, 8'hc0);
  set_reg(6'h2b, 8'h04);

  set_reg(6'h0b, 8'h78);
  set_reg(6'h0a, 8'hc8);
  set_reg(6'h2c, 8'h05);

  set_reg(6'h0d, 8'h80);
  set_reg(6'h0c, 8'hd0);
  set_reg(6'h2d, 8'h07);

  set_reg(6'h0f, 8'h88);
  set_reg(6'h0e, 8'hd8);
  set_reg(6'h2e, 8'h08);

  set_reg(6'h1b, 8'b11111111);
  set_reg(6'h15, 8'b11111111);

//  bg = 4'he;
//  while (1) begin
//    bg = bg + 4'h1;
//    set_reg(6'h20, { 4'b0000, bg });
//  end
end

endmodule
