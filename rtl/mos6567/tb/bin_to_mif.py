import sys

while True:
    b = sys.stdin.read(1)
    if b == '':
        break
    print "{0:08b}".format(ord(b))
