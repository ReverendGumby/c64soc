import sys

name = sys.argv[1]
width = sys.argv[2]

data = []
while True:
    b = sys.stdin.read(1)
    if b == '':
        break
    data = data + [ord(b)]

print "reg [{0}:0] ".format(int(width)-1) + name + "[" + str(len(data) - 1) + ":0];"
print "initial begin"

i = 0
for b in data:
    print ("  {0}[{1}] = " + width + "'b{2:0" + width + "b};").format(name, i, b)
    i = i + 1

print "end"
