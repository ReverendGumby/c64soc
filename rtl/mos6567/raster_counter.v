// We emulate the 6567R8: NTSC, 263 lines (235 visible), 65
// cycles/line, 418 visible pixels/line.

module mos6567_raster_counter
  (
   input            PCLK,
   input            CLKEN,
   input            CCLK_POSEDGE, 
   input            CCLK_NEGEDGE,
   input            CP2_NEGEDGE,

   input [8:0]      RASTER_CMP,

   output reg [8:0] RASTER, // a.k.a. "y"
   output reg [6:0] CYCLE,
   output reg [8:0] X,
  
   output           RASTER_MATCH
   );

initial begin
  //X = 9'h19c;
  CYCLE = 7'd65;
  RASTER = 9'h108;
end

// 'CYCLE' counts cclk cycles in a line, 1 - 65.
always @(posedge PCLK) if (CCLK_NEGEDGE) begin
  if (CYCLE == 7'd65)
    CYCLE <= 7'd1;
  else
    CYCLE <= CYCLE + 7'd1;
end

// X counts pixels in a line, $000 - $1ff (511). 8 pixels are emitted
// per CYCLE of cclk. It pauses for one CYCLE at the right border end.
// It is $000 on the rising edge of cclk in CYCLE 13.
reg hold_x;
initial hold_x = 1'b0;
always @*
  hold_x <= CYCLE == 7'd63;

always @(negedge PCLK) if (CLKEN) begin
  if (CYCLE == 7'd13 && CCLK_POSEDGE)
    // cclk edge has already passed, so X should now be $000 + 1.
    X <= 9'h001;
  else if (!hold_x)
    X <= X + 9'd1;
end

// RASTER (y) counts $000 - $107 (263).
// It increments one cycle before the first X coordinate of a line, 412 ($19c).
// It resets to 0 one cclk after exceeding its upper limit.
reg [8:0] first_x = 9'h19c;
wire vinc = X >= first_x - 8 && X < first_x;
always @(posedge PCLK) if (CCLK_POSEDGE) begin
  if (RASTER == 9'h108)
    RASTER <= 9'h000;
  else if (vinc)
    RASTER <= RASTER + 9'd1;
end

// Clocking is chosen such that (ERST=1) nIRQ falls when X == first_x.
reg raster_match_new, raster_match_dly;
always @(posedge PCLK) if (CP2_NEGEDGE && CLKEN) begin
  raster_match_new <= RASTER == RASTER_CMP;
  raster_match_dly <= raster_match_new;
end

assign RASTER_MATCH = raster_match_new && !raster_match_dly;

endmodule
