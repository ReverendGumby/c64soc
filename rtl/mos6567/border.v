module mos6567_border
  (
   input            PCLK,
   input            CLKEN,      // @(posedge PCLK)

   input [8:0]      RASTER,
   input [6:0]      CYCLE,
   input [8:0]      X,

   input            DEN,
   input            CSEL,
   input            RSEL,

   input [3:0]      IN_PIX,
   input [3:0]      EC,

   output reg       VERTICAL,

   output reg       OUT_CLKEN,
   output reg [3:0] OUT_PIX,
   output reg [8:0] OUT_X
   );

reg main, main_dly;

/* VERTICAL has to go backwards in the pipeline by 1*PCLK. So we
   match up to X-1, then delay main by 1*PCLK. */

wire left =  X == (CSEL ? 9'h018-1 : 9'h01f-1);
wire right = X == (CSEL ? 9'h158-1 : 9'h14f-1);

wire top =    RASTER == (RSEL ? 9'h033 : 9'h037);
wire bottom = RASTER == (RSEL ? 9'h0fb : 9'h0f7);

wire vertical_cycle = CYCLE == 7'd63;

initial begin
  main <= 1'b1;
  VERTICAL <= 1'b1;
end

reg clken_dly;
always @(negedge PCLK)
  clken_dly <= CLKEN;

// Since left/right change @(posedge PCLK), trigger on @(negedge PCLK)
// to ensure VERTICAL moves first.
always @(negedge PCLK) if (CLKEN) begin
  if ((vertical_cycle || left) && bottom)
    VERTICAL <= 1'b1;
  else if ((vertical_cycle || left) && top && DEN)
    VERTICAL <= 1'b0;
end

// Trigger on @(posedge PCLK) to ensure main moves after VERTICAL.
always @(posedge PCLK) if (clken_dly) begin
  if (right)
    main <= 1'b1;
  else if (left && !VERTICAL)
    main <= 1'b0;
end

// Delay main by another 1*PCLK. Now we've effectively matched up with
// X.
always @(posedge PCLK) if (clken_dly)
  main_dly <= main;

always @(negedge PCLK) if (OUT_CLKEN) begin
  OUT_PIX <= main_dly ? EC : IN_PIX;
end

// Our total pipeline delay (from IN_PIX to OUT_PIX) is 1*PCLK.
always @(posedge PCLK) begin
  OUT_CLKEN <= CLKEN;
  OUT_X <= X;
end

endmodule
