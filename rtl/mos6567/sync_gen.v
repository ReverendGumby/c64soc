module mos6567_sync_gen
  (
   input       PCLK,
   input       CLKEN,           // @(posedge PCLK)

   input [8:0] RASTER,
   input [8:0] X,

   output reg  HSYNC,
   output reg  VSYNC,
   output reg  BLANK
   );

reg vert;                       // 1 = vert. blanking region
reg horiz;                      // 1 = horiz. blanking region

reg clken_dly;
always @(negedge PCLK)
  clken_dly <= CLKEN;

// At this point in the pipeline, X is ahead by 2.5*PCLK.

// VIC-Article.txt says there's 418 pixels/line. It also says visible
// X is $1E9 - $18C, but that would yield 420 pixels/line. So I pulled
// each edge in by one pixel.

`define last_visible_x  9'h18b  // VIC-Article: $18C
`define first_visible_x 9'h1ea  // VIC-Article: $1E9

`define horiz_start_x   (`last_visible_x + 1)
`define horiz_end_x     (`first_visible_x)

// X holds for 8*PCLK at 9'h18c, and that's exactly the last pixel
// before blanking. If we compare X with 9'h18d, we would effectively
// delay the start of blanking by 8*PCLK. No bueno. So instead we
// compare with (X-1), then delay by 1*PCLK.

wire horiz_start = X == `horiz_start_x - 1;
wire horiz_end   = X == `horiz_end_x - 1;

// TODO: Can we eliminiate even this convoluted delay?
reg horiz_dly;

wire vert_x = X == 9'h1a0;
wire vert_start = RASTER == 8'd13;
wire vert_end   = RASTER == 8'd40 + 1;

initial begin
  vert <= 1'b0;
  horiz <= 1'b0;
  horiz_dly <= 1'b0;
end

always @(posedge PCLK) if (clken_dly) begin
  if (vert_x)
    if (vert_start)
      vert <= 1'b1;
    else if (vert_end)
      vert <= 1'b0;
end

always @(posedge PCLK) if (clken_dly) begin
  if (horiz_start)
    horiz_dly <= 1'b1;
  else if (horiz_end)
    horiz_dly <= 1'b0;
end

always @(posedge PCLK) if (clken_dly)
  horiz <= horiz_dly;

// Opposing edge PCLK triggering lines us up with color_gen's final
// delay of 2.5*PCLK.
always @(negedge PCLK) if (CLKEN) begin
  HSYNC <= !horiz;
  VSYNC <= !vert;
end

always @(negedge PCLK)
  BLANK <= !CLKEN || (horiz || vert);

endmodule
