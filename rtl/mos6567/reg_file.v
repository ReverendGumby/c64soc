`include "mob_bus.vh"

module mos6567_reg_file
  (
   input                       PCLK,
   input                       BCLK_POSEDGE,
   input [5:0]                 ADDR,
   input [7:0]                 DATA_IN,
   input                       READ,
   input                       WRITE,
   output reg [7:0]            DATA_OUT,

   inout [`MOB_BUS_BITS*8-1:0] COMPRESS_MOB_BUS,

   input [7:0]                 MXM,
   output reg                  MXM_RES,
   input [7:0]                 MXD,
   output reg                  MXD_RES,

   input [8:0]                 RASTER,
   output reg [8:0]            RASTER_CMP,

   output reg                  ECM,
   output reg                  BMM,
   output reg                  DEN,
   output reg                  RSEL,
   output reg [2:0]            YSCROLL,
   output reg                  RES,
   output reg                  MCM,
   output reg                  CSEL,
   output reg [2:0]            XSCROLL,
   output reg [13:10]          VM,
   output reg [13:11]          CB,

   output reg [3:0]            EC,
   output reg [3:0]            B0C,
   output reg [3:0]            B1C,
   output reg [3:0]            B2C,
   output reg [3:0]            B3C,
   output reg [3:0]            MM0,
   output reg [3:0]            MM1,

   input                       IRQ,
   output reg                  ERST,
   input                       IRST,
   output reg                  IRST_RES,
   output reg                  EMBC,
   input                       IMBC,
   output reg                  IMBC_RES,
   output reg                  EMMC,
   input                       IMMC,
   output reg                  IMMC_RES,
   output reg                  ELP,
   input                       ILP,
   output reg                  ILP_RES
   );

reg [8:0] mxx[7:0];
reg [7:0] mxy[7:0];
reg [7:0] mxe;
reg [7:0] mxye;
reg [7:0] mxdp;
reg [7:0] mxmc;
reg [7:0] mxxe;
reg [3:0] mxc[7:0];

reg [7:0] mxx8;                 // { mxx[7][8], ... mxx[0][8] }

genvar i;
generate
  for (i = 0; i <= 7; i = i + 1) begin: mob
    assign `MOB_BUS_MXX(COMPRESS_MOB_BUS, i) = mxx[i];
    assign `MOB_BUS_MXY(COMPRESS_MOB_BUS, i) = mxy[i];
    assign `MOB_BUS_MXC(COMPRESS_MOB_BUS, i) = mxc[i];
    assign `MOB_BUS_MXE(COMPRESS_MOB_BUS, i) = mxe[i];
    assign `MOB_BUS_MXYE(COMPRESS_MOB_BUS, i) = mxye[i];
    assign `MOB_BUS_MXDP(COMPRESS_MOB_BUS, i) = mxdp[i];
    assign `MOB_BUS_MXMC(COMPRESS_MOB_BUS, i) = mxmc[i];
    assign `MOB_BUS_MXXE(COMPRESS_MOB_BUS, i) = mxxe[i];

    always @*
      mxx[i][8] <= mxx8[i];
  end
endgenerate

initial begin
  ECM = 1'b0;
  BMM = 1'b0;
  DEN = 1'b1;
  MCM = 1'b0;
  YSCROLL = 3'b000;
  VM[13:10] = 4'b0000;
  CB[13:11] = 3'b000;
  mxe = 8'h00;
end

always @* begin
  case (ADDR)
    6'h00: DATA_OUT <= mxx[0][7:0];
    6'h01: DATA_OUT <= mxy[0];
    6'h02: DATA_OUT <= mxx[1][7:0];
    6'h03: DATA_OUT <= mxy[1];
    6'h04: DATA_OUT <= mxx[2][7:0];
    6'h05: DATA_OUT <= mxy[2];
    6'h06: DATA_OUT <= mxx[3][7:0];
    6'h07: DATA_OUT <= mxy[3];
    6'h08: DATA_OUT <= mxx[4][7:0];
    6'h09: DATA_OUT <= mxy[4];
    6'h0a: DATA_OUT <= mxx[5][7:0];
    6'h0b: DATA_OUT <= mxy[5];
    6'h0c: DATA_OUT <= mxx[6][7:0];
    6'h0d: DATA_OUT <= mxy[6];
    6'h0e: DATA_OUT <= mxx[7][7:0];
    6'h0f: DATA_OUT <= mxy[7];
    6'h10: DATA_OUT <= mxx8;
    6'h11: DATA_OUT <= { RASTER[8], ECM, BMM, DEN, RSEL, YSCROLL[2:0] };
    6'h12: DATA_OUT <= RASTER[7:0];
    6'h13: DATA_OUT <= 8'hff;     // LPX
    6'h14: DATA_OUT <= 8'hff;     // LPY
    6'h15: DATA_OUT <= mxe;
    6'h16: DATA_OUT <= { 2'b11, RES, MCM, CSEL, XSCROLL[2:0] };
    6'h17: DATA_OUT <= mxye;
    6'h18: DATA_OUT <= { VM[13:10], CB[13:11], 1'b1 };
    6'h19: DATA_OUT <= { IRQ, 3'b111, ILP, IMMC, IMBC, IRST };
    6'h1a: DATA_OUT <= { 4'b1111, ELP, EMMC, EMBC, ERST };
    6'h1b: DATA_OUT <= mxdp;
    6'h1c: DATA_OUT <= mxmc;
    6'h1d: DATA_OUT <= mxxe;
    6'h1e: DATA_OUT <= MXM;
    6'h1f: DATA_OUT <= MXD;
    6'h20: DATA_OUT <= { 4'b1111, EC };
    6'h21: DATA_OUT <= { 4'b1111, B0C };
    6'h22: DATA_OUT <= { 4'b1111, B1C };
    6'h23: DATA_OUT <= { 4'b1111, B2C };
    6'h24: DATA_OUT <= { 4'b1111, B3C };
    6'h25: DATA_OUT <= { 4'b1111, MM0 };
    6'h26: DATA_OUT <= { 4'b1111, MM1 };
    6'h27: DATA_OUT <= { 4'b1111, mxc[0] };
    6'h28: DATA_OUT <= { 4'b1111, mxc[1] };
    6'h29: DATA_OUT <= { 4'b1111, mxc[2] };
    6'h2a: DATA_OUT <= { 4'b1111, mxc[3] };
    6'h2b: DATA_OUT <= { 4'b1111, mxc[4] };
    6'h2c: DATA_OUT <= { 4'b1111, mxc[5] };
    6'h2d: DATA_OUT <= { 4'b1111, mxc[6] };
    6'h2e: DATA_OUT <= { 4'b1111, mxc[7] };
    default: DATA_OUT <= 8'hff;
  endcase
end

always @(posedge PCLK) if (BCLK_POSEDGE) begin
  MXM_RES <= READ && ADDR == 6'h1e;
  MXD_RES <= READ && ADDR == 6'h1f;
end

always @(posedge PCLK) if (BCLK_POSEDGE) begin
  { ILP_RES, IMMC_RES, IMBC_RES, IRST_RES } <= 4'b0000;
  if (WRITE)
    case (ADDR)
      6'h00: mxx[0][7:0] <= DATA_IN;
      6'h01: mxy[0] <= DATA_IN;
      6'h02: mxx[1][7:0] <= DATA_IN;
      6'h03: mxy[1] <= DATA_IN;
      6'h04: mxx[2][7:0] <= DATA_IN;
      6'h05: mxy[2] <= DATA_IN;
      6'h06: mxx[3][7:0] <= DATA_IN;
      6'h07: mxy[3] <= DATA_IN;
      6'h08: mxx[4][7:0] <= DATA_IN;
      6'h09: mxy[4] <= DATA_IN;
      6'h0a: mxx[5][7:0] <= DATA_IN;
      6'h0b: mxy[5] <= DATA_IN;
      6'h0c: mxx[6][7:0] <= DATA_IN;
      6'h0d: mxy[6] <= DATA_IN;
      6'h0e: mxx[7][7:0] <= DATA_IN;
      6'h0f: mxy[7] <= DATA_IN;
      6'h10: mxx8 <= DATA_IN;
      6'h11: { RASTER_CMP[8], ECM, BMM, DEN, RSEL, YSCROLL[2:0] } <= DATA_IN;
      6'h12: RASTER_CMP[7:0] <= DATA_IN;
      6'h15: mxe <= DATA_IN;
      6'h16: { RES, MCM, CSEL, XSCROLL[2:0] } <= DATA_IN[5:0];
      6'h17: mxye <= DATA_IN;
      6'h18: { VM[13:10], CB[13:11] } <= DATA_IN[7:1];
      6'h19: { ILP_RES, IMMC_RES, IMBC_RES, IRST_RES } <= DATA_IN[3:0];
      6'h1a: { ELP, EMMC, EMBC, ERST } <= DATA_IN[3:0];
      6'h1b: mxdp <= DATA_IN;
      6'h1c: mxmc <= DATA_IN;
      6'h1d: mxxe <= DATA_IN;
      6'h20: EC <= DATA_IN[3:0];
      6'h21: B0C <= DATA_IN[3:0];
      6'h22: B1C <= DATA_IN[3:0];
      6'h23: B2C <= DATA_IN[3:0];
      6'h24: B3C <= DATA_IN[3:0];
      6'h25: MM0 <= DATA_IN[3:0];
      6'h26: MM1 <= DATA_IN[3:0];
      6'h27: mxc[0] <= DATA_IN[3:0];
      6'h28: mxc[1] <= DATA_IN[3:0];
      6'h29: mxc[2] <= DATA_IN[3:0];
      6'h2a: mxc[3] <= DATA_IN[3:0];
      6'h2b: mxc[4] <= DATA_IN[3:0];
      6'h2c: mxc[5] <= DATA_IN[3:0];
      6'h2d: mxc[6] <= DATA_IN[3:0];
      6'h2e: mxc[7] <= DATA_IN[3:0];
      default: ;
    endcase
end

endmodule
