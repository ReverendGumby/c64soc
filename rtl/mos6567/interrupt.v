module mos6567_interrupt
  (
   input      PCLK,
   input      BCLK_POSEDGE,

   input      ERST,
   input      IRST_SET,
   input      IRST_RES,
   output reg IRST,

   input      EMBC,
   input      IMBC_SET,
   input      IMBC_RES,
   output reg IMBC,

   input      EMMC,
   input      IMMC_SET,
   input      IMMC_RES,
   output reg IMMC,

   input      ELP,
   input      ILP_SET,
   input      ILP_RES,
   output reg ILP,

   output     IRQ
   );

function int_set_res
  (
   input intr,
   input set,
   input res
   );
  begin
    if (res)
      int_set_res = 1'b0;
    else if (set)
      int_set_res = 1'b1;
    else
      int_set_res = intr;
  end
endfunction

// _SET/_RES signals can be as narrow as 1 PCLK, so latch them every PCLK.
reg pirst, pimbc, pimmc, pilp;

initial begin
  pirst <= 1'b0;
  pimbc <= 1'b0;
  pimmc <= 1'b0;
  pilp <= 1'b0;
end

always @(posedge PCLK) begin
  pirst <= int_set_res(pirst, IRST_SET, IRST_RES);
  pimbc <= int_set_res(pimbc, IMBC_SET, IMBC_RES);
  pimmc <= int_set_res(pimmc, IMMC_SET, IMMC_RES);
  pilp  <= int_set_res(pilp,  ILP_SET,  ILP_RES);
end

// The outside world wants interrupt signals to move with BCLK.
always @(posedge PCLK) if (BCLK_POSEDGE) begin
  IRST <= pirst;
  IMBC <= pimbc;
  IMMC <= pimmc;
  ILP  <= pilp;
end

assign IRQ = (ERST && IRST) || (EMBC && IMBC) || (EMMC && IMMC) || (ELP && ILP);

endmodule
