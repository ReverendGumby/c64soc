module mos6567_color_gen
  (
   input [8:0]      X,
   input [3:0]      PIX,
   input            BLANK,

   output reg [7:0] RED,
   output reg [7:0] GREEN,
   output reg [7:0] BLUE
   );

// Colors from www.c64-wiki.com/index.php/Color
always @* begin
  if (BLANK)
    { RED, GREEN, BLUE } <= { 8'd000, 8'd000, 8'd000 };
  else
    case (PIX)
      4'h0: { RED, GREEN, BLUE } <= { 8'd000, 8'd000, 8'd000 }; // black
      4'h1: { RED, GREEN, BLUE } <= { 8'd255, 8'd255, 8'd255 }; // white
      4'h2: { RED, GREEN, BLUE } <= { 8'd136, 8'd000, 8'd000 }; // red
      4'h3: { RED, GREEN, BLUE } <= { 8'd170, 8'd255, 8'd238 }; // cyan
      4'h4: { RED, GREEN, BLUE } <= { 8'd204, 8'd068, 8'd204 }; // violet
      4'h5: { RED, GREEN, BLUE } <= { 8'd000, 8'd204, 8'd085 }; // green
      4'h6: { RED, GREEN, BLUE } <= { 8'd000, 8'd000, 8'd170 }; // blue
      4'h7: { RED, GREEN, BLUE } <= { 8'd238, 8'd238, 8'd119 }; // yellow
      4'h8: { RED, GREEN, BLUE } <= { 8'd221, 8'd136, 8'd085 }; // orange
      4'h9: { RED, GREEN, BLUE } <= { 8'd102, 8'd068, 8'd000 }; // brown
      4'ha: { RED, GREEN, BLUE } <= { 8'd255, 8'd119, 8'd119 }; // light red
      4'hb: { RED, GREEN, BLUE } <= { 8'd051, 8'd051, 8'd051 }; // gray 1
      4'hc: { RED, GREEN, BLUE } <= { 8'd119, 8'd119, 8'd119 }; // gray 2
      4'hd: { RED, GREEN, BLUE } <= { 8'd170, 8'd255, 8'd102 }; // light green
      4'he: { RED, GREEN, BLUE } <= { 8'd000, 8'd136, 8'd255 }; // light blue
      4'hf: { RED, GREEN, BLUE } <= { 8'd187, 8'd187, 8'd187 }; // gray 3
      default: { RED, GREEN, BLUE } <= { 8'dx, 8'dx, 8'dx };
    endcase
end

endmodule
