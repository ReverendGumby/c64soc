`include "mob_bus.vh"

module mos6567_mux
  (
   input                       PCLK,
   input                       CLKEN, // @(posedge PCLK)

   input                       MXM_RES,
   output reg [7:0]            MXM,
   input                       MXD_RES,
   output reg [7:0]            MXD,

   // 1'fg/bg + 1'alpha + 4'color
   input [5:0]                 GFX_PIX,
   inout [`MOB_BUS_BITS*8-1:0] COMPRESS_MOB_BUS,
   input [8:0]                 X,

   output reg                  IMBC_SET,
   output reg                  IMMC_SET,

   output reg [3:0]            OUT_PIX,
   output reg                  OUT_CLKEN,
   output reg [8:0]            OUT_X
   );

wire [5:0]               mob_pix[7:0];

wire [7:0]               mob_alpha;
reg [5:0]                mob_pri_pix;

genvar i;
generate
  for (i = 0; i <= 7; i = i + 1) begin: mob
    assign mob_pix[i] = `MOB_BUS_PIX(COMPRESS_MOB_BUS, i);
    assign mob_alpha[i] = mob_pix[i][4];
  end
endgenerate

initial begin
  MXM <= 8'h00;
  MXD <= 8'h00;
end

reg clken_dly;
always @(negedge PCLK)
  clken_dly <= CLKEN;

// First come MOB priorities: 0 is highest, 7 lowest.
always @(posedge PCLK) if (clken_dly) begin
  casex (mob_alpha)
    8'bxxxxxxx1: mob_pri_pix <= mob_pix[0];
    8'bxxxxxx10: mob_pri_pix <= mob_pix[1];
    8'bxxxxx100: mob_pri_pix <= mob_pix[2];
    8'bxxxx1000: mob_pri_pix <= mob_pix[3];
    8'bxxx10000: mob_pri_pix <= mob_pix[4];
    8'bxx100000: mob_pri_pix <= mob_pix[5];
    8'bx1000000: mob_pri_pix <= mob_pix[6];
    default:     mob_pri_pix <= mob_pix[7];
  endcase
end

// Next comes gfx vs. MOBs. Highest to lowest:
// 1. Non-transparent foreground MOB (fg/bg=1, alpha=1)
// 2. Foreground gfx                 (fg/bg=1)
// 3. Non-transparent background MOB (fg/bg=0, alpha=1)
// 4. Background gfx                 (fg/bg=0)

wire mob_pri_fg = mob_pri_pix[5];
wire mob_pri_alpha = mob_pri_pix[4];
wire gfx_fg = GFX_PIX[5];

wire mob_fg = mob_pri_fg && mob_pri_alpha;
wire mob_bg = !mob_pri_fg && mob_pri_alpha;

always @(negedge PCLK) if (OUT_CLKEN) begin
  casex ({mob_bg, gfx_fg, mob_fg})
    3'bxx1:  OUT_PIX <= mob_pri_pix[3:0];
    3'bx10:  OUT_PIX <= GFX_PIX[3:0];
    3'b100:  OUT_PIX <= mob_pri_pix[3:0];
    default: OUT_PIX <= GFX_PIX[3:0];
  endcase
end

// MOB-MOB collision detector
reg [2:0] mob_alpha_count;
integer j;
always @* begin
  mob_alpha_count = 0;
  for (j = 0; j < 8; j = j + 1)
    mob_alpha_count = mob_alpha_count + mob_alpha[j];
end

always @(negedge PCLK) if (OUT_CLKEN) begin
  IMMC_SET <= 1'b0;
  if (MXM_RES)
    MXM <= 8'h00;
  else if (mob_alpha_count > 1) begin
    // Only set interrupt if MXM was empty.
    IMMC_SET <= !MXM;
    MXM <= MXM | mob_alpha;
  end
end

// MOB-gfx collision detector
always @(negedge PCLK) if (OUT_CLKEN) begin
  IMBC_SET <= 1'b0;
  if (MXD_RES)
    MXD <= 8'h00;
  else if (|(mob_alpha) && gfx_fg) begin
    IMBC_SET <= !MXD;
    MXD <= MXD | mob_alpha;
  end
end

always @(posedge PCLK) begin
  OUT_X <= X;
  OUT_CLKEN <= clken_dly;
end

endmodule
