module mos6526_tmr
  (
   input             nRES,
   input             CLK,
   input             CP2,
   input             CP2_POSEDGE,
   input             CP2_NEGEDGE,

   input [15:0]      TLATCH, // timer latch (PxL/H in datasheet)
   input             START, // 1=start, 0=stop
   input             OUTMODE, // 1=toggle, 0=pulse
   input             LOAD, // 1=force load (strobe)
   input [1:0]       INMODE, // input: 00=CP2, 01=CNT, 10=TA, 11=CNT*TA

   input             CNT, // sync'd CNT pin input
   input             CNT_POSEDGE,
   input             OTHER_UNDER, // underflow on "other" timer

   output reg [15:0] TCNT, // timer counter
   output reg        OUT,
   output reg        UNDER // underflow
   );

reg        tclk;
reg        START_dly;
reg        started;

always @* begin
  case (INMODE)
    2'b00: tclk = 1'b1; // CP2
    2'b01: tclk = CNT_POSEDGE;
    2'b10: tclk = OTHER_UNDER;
    2'b11: tclk = OTHER_UNDER && CNT;
    default: tclk = 1'bx;
  endcase
end

always @(negedge nRES or posedge CLK) begin
  if (!nRES)
    START_dly <= 1'b0;
  else if (CP2_NEGEDGE)
    START_dly <= START;
end

always @*
  started = START && !START_dly;

always @(negedge nRES or posedge CLK) begin
  if (!nRES) begin
    UNDER <= 1'b0;
    OUT <= 1'b0;
  end
  else if (CP2_NEGEDGE) begin
    if (started) begin
      UNDER <= 1'b0;
      OUT <= OUTMODE ? 1'b1 : 1'b0;
    end
    else if (UNDER) begin
      UNDER <= 1'b0;
      OUT <= OUTMODE ? ~OUT : 1'b1;
    end
    else begin
      OUT <= OUTMODE ? OUT : 1'b0;
      if (START)
        UNDER <= TCNT == 16'h0000;
    end
  end
end

always @(negedge nRES or posedge CLK) begin
  if (!nRES) begin
    TCNT <= 16'h0000;
  end
  else if (CP2_NEGEDGE) begin
    if (UNDER || LOAD)
      TCNT <= TLATCH;
    else if (START && tclk && TCNT)
      TCNT <= TCNT - 1'd1;
  end
end

endmodule
