module mos6526
  (
   output [2:0] DEBUG,
   input        nRES,
   input        CLK,
   input        CP2,
   input        CP2_POSEDGE,
   input        CP2_NEGEDGE,
   input        TOD,

   input [7:0]  DB_I,
   output [7:0] DB_O,
   output       DB_OE,
   input [3:0]  RS,
   input        nCS,
   input        RW,
   output       nIRQ,

   input [7:0]  PA_I,
   output [7:0] PA_O,           // open-drain
   input [7:0]  PB_I,
   output [7:0] PB_O,           // open-drain
   output       nPC,
   input        nFLAG,

   input        SP_I,
   output       SP_O,           // open-drain
   input        CNT,

   input        HOLD            // halt
   );

//////////////////////////////////////////////////////////////////////
// Input buffers

wire       sp_in;

reg [7:0] PA_reg, PB_reg; // sync'd PA/PB
reg       SP_reg;         // sync'd SP
reg       CNT_reg;        // sync'd CNT
reg       nFLAG_reg;      // sync'd nFLAG
reg       TOD_reg;        // sync'd TOD

reg       CNT_dly, CNT_posedge;

// Port input setup time is referenced to the CP2 posedge
// immediately preceding readout. Assumed to be a transparent latch.
// Assumed to apply to other inputs as well.
always @(posedge CLK) if (CP2_NEGEDGE) begin
  PA_reg <= PA_I;
  PB_reg <= PB_I;
  CNT_reg <= CNT;
  SP_reg <= sp_in;
  nFLAG_reg <= nFLAG;
  TOD_reg <= TOD;
end

always @(posedge CLK) if (CP2_NEGEDGE) begin
  CNT_dly <= CNT_reg;
end

always @* begin
  CNT_posedge = CNT && !CNT_dly;
end

//////////////////////////////////////////////////////////////////////
// Register file

reg [7:0]  PRA, PRB;       // Peripheral Data output Register
reg [7:0]  DDRA, DDRB;     // Data Direction Register
reg [15:0] TPA, TPB;       // Timer Latch (PxL/H in datasheet)
reg [7:0]  SDR;            // Serial Data Register

// ICR: Interrupt Control Register
// reading ICR gives INT DATA
reg        ID_TA;          // TIMER A underflow
reg        ID_TB;          // TIMER A underflow
reg        ID_ALRM;        // TOD alarm
reg        ID_SP;          // serial port full/empty
reg        ID_FLG;         // nFLAG asserted
reg        ID_IR;          // Interrupt Request: OR of all interrupts
reg        ID_read;        // ICR was read

// writing ICR affects INT MASK
reg        IM_TA;
reg        IM_TB;
reg        IM_ALRM;
reg        IM_SP;
reg        IM_FLG;

// CRA: Control Register for Timer A
reg        CRA_START;      // 1=start, 0=stop
reg        CRA_PBON;       // PB6: 1=TA, 0=PRB6
reg        CRA_OUTMODE;    // 1=toggle, 0=pulse
reg        CRA_RUNMODE;    // 1=one-shot, 0=continuous
reg        CRA_LOAD;       // 1=force load (strobe)
reg        CRA_INMODE;     // input: 1=CNT, 0=CP2
reg        CRA_SPMODE;     // SP dir: 1=out, 0=in
reg        CRA_TODIN;      // TOD pin Hz: 1=50, 0=60

// CRB: Control Register for Timer B
reg        CRB_START;      // 1=start, 0=stop
reg        CRB_PBON;       // PB7: 1=TB, 0=PRB7
reg        CRB_OUTMODE;    // 1=toggle, 0=pulse
reg        CRB_RUNMODE;    // 1=one-shot, 0=continuous
reg        CRB_LOAD;       // 1=force load (strobe)
reg [1:0]  CRB_INMODE;     // input: 00=CP2, 01=CNT, 10=TA, 11=CNT*TA
reg        CRB_ALARM;      // TOD writes 1=ALARM, 0=TOD clock

wire [15:0] TA, TB;        // Timer Counter
wire        TAOUT, TBOUT;  // Timer outputs on PB
wire        TA_under, TB_under; // timer underflow

//////////////////////////////////////////////////////////////////////
// Register file bus interface

reg [7:0] dbo;             // register readout to DB

wire [7:0] tod_dbo;

wire       read = !nCS && RW;
wire       write = !nCS && !RW;

// TOD clock / alarm registers: 4'h8 - 4'hB
wire       tod_reg_sel = {RS[3:2], 2'b00} == 4'h8;

always @(negedge nRES or posedge CLK) begin
  if (!nRES) begin
    PRA <= 8'h00;
    PRB <= 8'h00;
    DDRA <= 8'h00;
    DDRB <= 8'h00;
    TPA <= 16'hffff;
    TPB <= 16'Hffff;
    SDR <= 8'h00;
    IM_TA <= 1'b0;
    IM_TB <= 1'b0;
    IM_ALRM <= 1'b0;
    IM_SP <= 1'b0;
    CRA_START <= 1'b0;
    CRA_PBON <= 1'b0;
    CRA_OUTMODE <= 1'b0;
    CRA_RUNMODE <= 1'b0;
    CRA_LOAD <= 1'b0;
    CRA_INMODE <= 1'b0;
    CRA_SPMODE <= 1'b0;
    CRA_TODIN <= 1'b0;
    CRB_START <= 1'b0;
    CRB_PBON <= 1'b0;
    CRB_OUTMODE <= 1'b0;
    CRB_RUNMODE <= 1'b0;
    CRB_LOAD <= 1'b0;
    CRB_INMODE <= 2'b00;
    CRB_ALARM <= 1'b0;
  end
  else if (CP2_NEGEDGE) begin
    CRA_LOAD <= 1'b0;
    CRB_LOAD <= 1'b0;
    
    if (write) begin
      case (RS)
        4'h0: PRA <= DB_I;
        4'h1: PRB <= DB_I;
        4'h2: DDRA <= DB_I;
        4'h3: DDRB <= DB_I;
        4'h4: TPA[7:0] <= DB_I;
        4'h5: begin
          TPA[15:8] <= DB_I; 
          CRA_LOAD <= !CRA_START; // load latch if timer stopped
        end
        4'h6: TPB[7:0] <= DB_I;
        4'h7: begin
          TPB[15:8] <= DB_I;
          CRB_LOAD <= !CRB_START; // load latch if timer stopped
        end
        // 4'h8 - 4'hb: TOD
        4'hc: SDR <= DB_I;
        4'hd:                  // INT MASK
          if (!DB_I[7]) begin    // clear mask bits
            IM_TA   <= IM_TA   && !DB_I[0];
            IM_TB   <= IM_TB   && !DB_I[1];
            IM_ALRM <= IM_ALRM && !DB_I[2];
            IM_SP   <= IM_SP   && !DB_I[3];
            IM_FLG  <= IM_FLG  && !DB_I[4];
          end
          else begin           // set mask bits
            IM_TA   <= IM_TA   || DB_I[0];
            IM_TB   <= IM_TB   || DB_I[1];
            IM_ALRM <= IM_ALRM || DB_I[2];
            IM_SP   <= IM_SP   || DB_I[3];
            IM_FLG  <= IM_FLG  || DB_I[4];
          end
        4'he: begin            // CRA
          CRA_START <= DB_I[0];
          CRA_PBON <= DB_I[1];
          CRA_OUTMODE <= DB_I[2];
          CRA_RUNMODE <= DB_I[3];
          CRA_LOAD <= DB_I[4];
          CRA_INMODE <= DB_I[5];
          CRA_SPMODE <= DB_I[6];
          CRA_TODIN <= DB_I[7];
        end
        4'hf: begin            // CRB
          CRB_START <= DB_I[0];
          CRB_PBON <= DB_I[1];
          CRB_OUTMODE <= DB_I[2];
          CRB_RUNMODE <= DB_I[3];
          CRB_LOAD <= DB_I[4];
          CRB_INMODE <= DB_I[6:5];
          CRB_ALARM <= DB_I[7];
        end
        default: ;
      endcase
    end
    else begin
      // Stop one-shot timers on underflow.
      if (CRA_START && CRA_RUNMODE && TA_under)
        CRA_START <= 1'b0;
      if (CRB_START && CRB_RUNMODE && TB_under)
        CRB_START <= 1'b0;
    end
  end
end

always @* begin
  dbo = 8'h00;
  case (RS)
    4'h0: dbo = PA_reg;
    4'h1: dbo = PB_reg;
    4'h2: dbo = DDRA;
    4'h3: dbo = DDRB;
    4'h4: dbo = TA[7:0];
    4'h5: dbo = TA[15:8];
    4'h6: dbo = TB[7:0];
    4'h7: dbo = TB[15:8];
    4'h8: dbo = tod_dbo;
    4'h9: dbo = tod_dbo;
    4'ha: dbo = tod_dbo;
    4'hb: dbo = tod_dbo;
    4'hc: dbo = SDR;
    4'hd: begin
      dbo[0] = ID_TA;
      dbo[1] = ID_TB;
      dbo[2] = ID_ALRM;
      dbo[3] = ID_SP;
      dbo[4] = ID_FLG;
      dbo[7] = ID_IR;
    end
    4'he: begin
      dbo[0] = CRA_START;
      dbo[1] = CRA_PBON;
      dbo[2] = CRA_OUTMODE;
      dbo[3] = CRA_RUNMODE;
      dbo[5] = CRA_INMODE;
      dbo[6] = CRA_SPMODE;
      dbo[7] = CRA_TODIN;
    end
    4'hf: begin
      dbo[0] = CRB_START;
      dbo[1] = CRB_PBON;
      dbo[2] = CRB_OUTMODE;
      dbo[3] = CRB_RUNMODE;
      dbo[6:5] = CRB_INMODE;
      dbo[7] = CRB_ALARM;
    end
  endcase
end

assign DB_O = dbo;
assign DB_OE = read;

always @(negedge nRES or posedge CLK) begin
  if (!nRES) begin
    ID_read <= 1'b0;
  end
  else if (CP2_NEGEDGE && !HOLD) begin
    ID_read <= 1'b0;
    if (read) begin
      case (RS)
        4'hd: ID_read <= 1'b1;    // read ICR
        default: ;
      endcase      
    end
  end
end

//////////////////////////////////////////////////////////////////////
// Peripheral Data Inputs / Outputs

wire [7:0] pa_o, pa_oe;
wire [7:0] pb_o, pb_oe;

assign pa_o = PRA;
assign pa_oe = DDRA;

assign pb_o[7] = PRB[7];
assign pb_o[6] = CRA_PBON ? TAOUT : PRB[6];
assign pb_o[5] = CRB_PBON ? TBOUT : PRB[5];
assign pb_o[4:0] = PRB[4:0];

assign pb_oe[7] = DDRB[7];
assign pb_oe[6] = DDRB[6] || CRA_PBON;
assign pb_oe[5] = DDRB[5] || CRB_PBON;
assign pb_oe[4:0] = DDRB[4:0];

assign PA_O = pa_o | ~pa_oe;
assign PB_O = pb_o | ~pb_oe;

//////////////////////////////////////////////////////////////////////
// Interval Timers

mos6526_tmr tmr_a
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP2(CP2),
   .CP2_POSEDGE(CP2_POSEDGE && !HOLD),
   .CP2_NEGEDGE(CP2_NEGEDGE && !HOLD),
   .TLATCH(TPA),
   .START(CRA_START),
   .OUTMODE(CRA_OUTMODE),
   //.RUNMODE(CRA_RUNMODE),
   .LOAD(CRA_LOAD),
   .INMODE({1'b0, CRA_INMODE}),
   .CNT(CNT_reg),
   .CNT_POSEDGE(CNT_posedge),
   .OTHER_UNDER(1'b0),
   .TCNT(TA),
   .OUT(TAOUT),
   .UNDER(TA_under)
   );

mos6526_tmr tmr_b
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP2(CP2),
   .CP2_POSEDGE(CP2_POSEDGE && !HOLD),
   .CP2_NEGEDGE(CP2_NEGEDGE && !HOLD),
   .TLATCH(TPB),
   .START(CRB_START),
   .OUTMODE(CRB_OUTMODE),
   //.RUNMODE(CRB_RUNMODE),
   .LOAD(CRB_LOAD),
   .INMODE(CRB_INMODE),
   .CNT(CNT_reg),
   .CNT_POSEDGE(CNT_posedge),
   .OTHER_UNDER(TA_under),
   .TCNT(TB),
   .OUT(TBOUT),
   .UNDER(TB_under)
   );

//////////////////////////////////////////////////////////////////////
// Time Of Day (TOD) / alarm clock

mos6526_tod tod
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP2(CP2),
   .CP2_POSEDGE(CP2_POSEDGE && !HOLD),
   .CP2_NEGEDGE(CP2_NEGEDGE && !HOLD),
   .TODCLK(TOD_reg),
   .HOLD(HOLD),

   .TODIN(CRA_TODIN),
   .ALARM(CRB_ALARM),

   .RS(RS[1:0]),
   .READ(read && tod_reg_sel),
   .WRITE(write && tod_reg_sel),
   .DB_I(DB_I),
   .DB_O(tod_dbo),

   .ALARM_INT(TOD_alarm)
   );

//////////////////////////////////////////////////////////////////////
// Interrupts

always @(posedge CLK) begin
  if (!nRES) begin
    ID_TA <= 1'b0;
    ID_TB <= 1'b0;
    ID_ALRM <= 1'b0;
    ID_SP <= 1'b0;
    ID_FLG <= 1'b0;
  end
  else if (CP2_NEGEDGE) begin
    if (TA_under)
      ID_TA <= 1'b1;
    else if (ID_read)
      ID_TA <= 1'b0;

    if (TB_under)
      ID_TB <= 1'b1;
    else if (ID_read)
      ID_TB <= 1'b0;

    if (TOD_alarm)
      ID_ALRM <= 1'b1;
    else if (ID_read)
      ID_ALRM <= 1'b0;

    // TODO: ID_SP
    // TODO: ID_FLG
  end
end

always @*
  ID_IR = (ID_TA && IM_TA) ||
          (ID_TB && IM_TB) ||
          (ID_ALRM && IM_ALRM) || 
          (ID_SP && IM_SP) ||
          (ID_FLG && IM_FLG);

assign nIRQ = !ID_IR;

// TODO
assign nPC = 1'b1;
assign SP_O = 1'bx;

// Hacking
assign DEBUG[0] = PA_reg[6];    // CLK
assign DEBUG[1] = PA_reg[7];    // DATA
assign DEBUG[2] = DB_OE && RS == 4'h0; // reading PA

endmodule
