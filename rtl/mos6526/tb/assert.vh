task assert(input test);
  begin
    if (test !== 1)
      begin
        $display("ASSERTION FAILED in %m @%1t ns", $time);
        $finish;
      end
  end
endtask

task assert_eq(input integer lhs, input integer rhs);
  begin
    if (lhs !== rhs) begin
      $display("ASSERTION FAILED in %m @%1t ns: 0x%x !== 0x%x",
               $time, lhs, rhs);
      $finish();
    end
  end
endtask

task assert_neq(input integer lhs, input integer rhs);
  begin
    if (lhs === rhs) begin
      $display("ASSERTION FAILED in %m @%1t ns: 0x%x === 0x%x",
               $time, lhs, rhs);
      $finish();
    end
  end
endtask
