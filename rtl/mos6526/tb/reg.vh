task set_reg(input [5:0] r, input [7:0] v); 
  begin
    @(posedge AEC) cs_set_reg = 1'b1;
    rw_set_reg = 1'b0;
    a_set_reg = { 8'b01000000, r };
    db_set_reg = { 4'b0000, v };
    @(negedge cp2) #40 cs_set_reg = 1'b0;
  end
endtask

task get_reg(input [5:0] r); 
  begin
    @(posedge AEC) cs_set_reg = 1'b1;
    rw_set_reg = 1'b1;
    a_set_reg = { 8'b01000000, r };
    db_set_reg = 12'hzzz;
    @(negedge cp2) #40 cs_set_reg = 1'b0;
  end
endtask

