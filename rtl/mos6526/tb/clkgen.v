`timescale 1us / 1ps

module clkgen
  (
   output reg CLK,
   output     BCLK,
   output     BCLK_POSEDGE,
   output     BCLK_NEGEDGE,
   output     CP1,
   output     CP1_POSEDGE,
   output     CP1_NEGEDGE,
   output     CP2,
   output     CP2_POSEDGE,
   output     CP2_NEGEDGE
   );

mos6567_clkgen vicclkgen
  (
   .PCLK(CLK),

   .CP1(CP1),
   .CP1_POSEDGE(CP1_POSEDGE),
   .CP1_NEGEDGE(CP1_NEGEDGE),
   .CP2(CP2),
   .CP2_POSEDGE(CP2_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),

   .BCLK(bclk),
   .BCLK_POSEDGE(BCLK_POSEDGE),
   .BCLK_NEGEDGE(BCLK_NEGEDGE),

   .CCLK(),
   .CCLK_POSEDGE(),
   .CCLK_NEGEDGE()
   );

initial begin
  CLK = 1'b1;
end

always #0.0625 begin :clkgen
  CLK = !CLK;
end

endmodule
