module todclkgen
  (
   input      CLKIN,            // 8000000 Hz
   output reg CLKOUT            // a LOT faster than 60 Hz
   );

reg [17:0] clk_cnt;

initial begin
  clk_cnt = 1'd0;
  CLKOUT = 1'b0;
end

always @(posedge CLKIN) begin
  // If we really wanted 60 Hz out, we'd use 18'd66666 here. But then
  // the test would take FOREVER.
  if (clk_cnt == 18'd15) begin
    clk_cnt <= 1'd0;
    CLKOUT <= ~CLKOUT;
  end else
    clk_cnt <= clk_cnt + 1'd1;
end

endmodule
