`timescale 1us / 1ps

module mos6526_tod_tb
  (
   output       cp2,
   output [3:0] a,
   output [7:0] db,
   output       rw,
   output       nirq
   );

`include "assert.vh"

reg             nres;
wire [7:0] db_i, db_o;

localparam [3:0] RS_10THS = 4'h8;
localparam [3:0] RS_SEC = 4'h9;
localparam [3:0] RS_MIN = 4'hA;
localparam [3:0] RS_HR = 4'hB;
localparam [3:0] RS_CRA = 4'hE;
localparam [3:0] RS_CRB = 4'hF;

localparam CRA_TODIN = 1<<7;
localparam CRB_ALARM = 1<<7;

clkgen clkgen
  (
   .CLK(clk),
   .BCLK(bclk),
   .BCLK_POSEDGE(bclk_posedge),
   .BCLK_NEGEDGE(bclk_negedge),
   .CP1(),
   .CP1_POSEDGE(),
   .CP1_NEGEDGE(),
   .CP2(cp2),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge)
   );

todclkgen todclkgen
  (
   .CLKIN(clk),
   .CLKOUT(todclk)
   );

regger #(4) regger
  (
   .CP2(cp2),
   .nCS(ncs),
   .A(a),
   .DB_I(db_o),
   .DB_O(db_i),
   .RW(rw)
   );

mos6526 cia
  (
   .nRES(nres),
   .CLK(clk),
   .CP2(cp2),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .TOD(todclk),
   .DB_I(db_i),
   .DB_O(db_o),
   .DB_OE(db_oe),
   .RS(a),
   .nCS(ncs),
   .RW(rw),
   .nIRQ(nirq),
   .PA_I(8'hff),
   .PA_O(),
   .PB_I(8'hff),
   .PB_O(),
   .nPC(),
   .nFLAG(1'b1),
   .SP_I(1'b1),
   .SP_O(),
   .CNT(),
   .HOLD(1'b0)
   );

assign db = db_oe ? db_o : db_i;

task test_tod_reset;
  begin
    nres = 1'b0;
    #10 @(negedge cp2) nres = 1'b1;

    // TOD should be stopped on reset.
    assert_eq(cia.tod.stop, 1'b1);
    assert_eq(cia.tod.latch, 1'b0);
    assert_eq(cia.tod.ALARM, 1'b0);

    regger.get_assert(RS_HR, 8'h01);
    assert_eq(cia.tod.latch, 1'b1); // latch on read HR
    regger.get_assert(RS_MIN, 8'h00);
    assert_eq(cia.tod.latch, 1'b1);
    regger.get_assert(RS_SEC, 8'h00);
    assert_eq(cia.tod.latch, 1'b1);
    regger.get_assert(RS_10THS, 8'h00);
    assert_eq(cia.tod.latch, 1'b0); // unlatch on read 10THS
  end
endtask

// Set the TOD clock to 'then', wait for it to tick, then confirm it
// advanced to 'now'.
task test_tod_tick(integer then, integer now);
  begin
    $display("%m: %x -> %x", then, now);

    assert_eq(cia.tod.ALARM, 1'b0);
    regger.set(RS_HR, then[31:24]);
    assert_eq(cia.tod.stop, 1'b1);
    regger.set(RS_MIN, then[23:16]);
    assert_eq(cia.tod.stop, 1'b1);
    regger.set(RS_SEC, then[15:8]);
    assert_eq(cia.tod.stop, 1'b1);
    regger.set(RS_10THS, then[7:0]);
    assert_eq(cia.tod.stop, 1'b0);

    assert_eq(cia.tod.tod_hr, then[31:24]);
    assert_eq(cia.tod.tod_min, then[23:16]);
    assert_eq(cia.tod.tod_sec, then[15:8]);
    assert_eq(cia.tod.tod_10ths, then[7:0]);

    @(negedge cia.tod.jiffy) @(posedge cp2) ;

    regger.get_assert(RS_HR, now[31:24]);
    regger.get_assert(RS_MIN, now[23:16]);
    regger.get_assert(RS_SEC, now[15:8]);
    regger.get_assert(RS_10THS, now[7:0]);
  end
endtask

// Validate TOD start/stop behavior on register writes
task test_tod_stop;
reg [7:0] v;
  begin
    $display("%m: TOD is running");
    assert_eq(cia.tod.ALARM, 1'b0);
    assert_eq(cia.tod.stop, 1'b0);
    v = cia.tod.tod_10ths;
    @(negedge cia.tod.jiffy) @(posedge cp2) ;
    assert(cia.tod.tod_10ths !== v);

    $display("%m: stops on writing HR");
    regger.set(RS_HR, 8'h01);
    v = cia.tod.tod_10ths;
    assert_eq(cia.tod.stop, 1'b1);

    $display("%m: TOD stays stops for multiple jiffies");
    @(negedge cia.tod.jiffy) @(posedge cp2) ;
    assert_eq(cia.tod.stop, 1'b1);
    assert_eq(cia.tod.tod_10ths, v);
    @(negedge cia.tod.jiffy) @(posedge cp2) ;
    assert_eq(cia.tod.stop, 1'b1);
    assert_eq(cia.tod.tod_10ths, v);

    $display("%m: TOD restarts on writing 10THS");
    regger.set(RS_10THS, 8'h02);
    assert_eq(cia.tod.stop, 1'b0);
    @(negedge cia.tod.jiffy) @(posedge cp2) ;
    assert_eq(cia.tod.tod_10ths, 8'h03);
  end
endtask

task test_tod_alarm;
  begin
    nres = 1'b0;
    #10 @(negedge cp2) nres = 1'b1;

    $display("%m: Set alarm to T");
    regger.set(RS_CRB, CRB_ALARM);
    regger.set(RS_HR, 8'h12);
    regger.set(RS_MIN, 8'h34);
    regger.set(RS_SEC, 8'h56);
    regger.set(RS_10THS, 8'h07);

    assert_eq(cia.tod.alarm_hr, 8'h12);
    assert_eq(cia.tod.alarm_min, 8'h34);
    assert_eq(cia.tod.alarm_sec, 8'h56);
    assert_eq(cia.tod.alarm_10ths, 8'h07);

    assert_neq(cia.tod.tod_hr, 8'h12); // TOD didn't change
    assert_eq(cia.tod.stop, 1'b1);     // and didn't start

    $display("%m: Set TOD to be T-2");
    regger.set(RS_CRB, 0);
    regger.set(RS_HR, 8'h12);
    regger.set(RS_MIN, 8'h34);
    regger.set(RS_SEC, 8'h56);
    regger.set(RS_10THS, 8'h05);

    $display("%m: At T-1, ALARM int. clear");
    @(negedge cia.tod.jiffy) @(posedge cp2) ;
    assert_eq(cia.tod.tod_10ths, 8'h06);
    assert_eq(cia.tod.ALARM_INT, 1'b0);

    $display("%m: At T+0, ALARM int. set");
    @(negedge cia.tod.jiffy) @(posedge cp2) ;
    assert_eq(cia.tod.tod_10ths, 8'h07);
    assert_eq(cia.tod.ALARM_INT, 1'b1);

    $display("%m: At T+1, ALARM int. clear");
    @(negedge cia.tod.jiffy) @(posedge cp2) ;
    assert_eq(cia.tod.tod_10ths, 8'h08);
    assert_eq(cia.tod.ALARM_INT, 1'b0);

  end
endtask

initial begin
  $dumpfile("mos6526_tod_tb.vcd");
  $dumpvars;

  force cia.CRA_TODIN = 1'b0; // todclk/6

  test_tod_reset();

  test_tod_tick('h01000000, 'h01000001);
  test_tod_tick('h01000009, 'h01000100);
  test_tod_tick('h01000909, 'h01001000);
  test_tod_tick('h01005909, 'h01010000);
  test_tod_tick('h01095909, 'h01100000);
  test_tod_tick('h01595909, 'h02000000);
  test_tod_tick('h09595909, 'h10000000);
  test_tod_tick('h12595909, 'h81000000);
  test_tod_tick('h92595909, 'h01000000);

  test_tod_stop();
  test_tod_alarm();

  $finish();
end

endmodule
