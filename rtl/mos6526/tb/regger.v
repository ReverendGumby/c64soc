`timescale 1ns / 1ps

module regger #(parameter AW=4,
                parameter DBW=8)
  (
   input                CP2,

   output reg           nCS,
   output reg [AW-1:0]  A,
   input [DBW-1:0]      DB_I,
   output reg [DBW-1:0] DB_O,
   output reg           RW
   );

`include "assert.vh"

initial begin
  nCS = 1'b1;
  A = {AW{1'bx}};
  DB_O = {DBW{1'bx}};
  RW = 1'bx;
end

task set(input [AW-1:0] r, input [DBW-1:0] v); 
  begin
    @(posedge CP2) nCS = 1'b0;
    RW = 1'b0;
    A = r;
    DB_O = v;
    @(negedge CP2) #40 nCS = 1'b1;
    RW = 1'bx;
    A = {AW{1'bx}};
    DB_O = {DBW{1'bx}};
  end
endtask

task get(input [AW-1:0] r, output [DBW-1:0] av);
  begin
    @(posedge CP2) nCS = 1'b0;
    RW = 1'b1;
    A = r;
    @(negedge CP2) av = DB_I;
    #40 nCS = 1'b1;
    RW = 1'bx;
    A = {AW{1'bx}};
  end
endtask

task get_assert(input [AW-1:0] r, input [DBW-1:0] ev);
  reg [DBW-1:0] av;
  begin
    get(r, av);
    if (ev !== {DBW{1'bx}}) assert_eq(av, ev);
  end
endtask

endmodule
