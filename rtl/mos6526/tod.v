module mos6526_tod
  (
   input            nRES,
   input            CLK,
   input            CP2,
   input            CP2_POSEDGE,
   input            CP2_NEGEDGE,
   input            TODCLK,
   input            HOLD, // halt

   // Clock / alarm control
   input            TODIN,      // TOD freq: 1 = 50 Hz, 0 = 60 Hz
   input            ALARM,      // reg. writes: 1 = ALARM, 0 = TOD

   // Register interface
   input [1:0]      RS,
   input            READ,
   input            WRITE,
   input [7:0]      DB_I,
   output reg [7:0] DB_O,

   // Interrupt
   output           ALARM_INT
   );

//////////////////////////////////////////////////////////////////////
// Jiffy (0.1s) clock counter

reg stop, latch;
reg jiffy, todclk_dly;
reg [2:0] clk_cnt;

reg [7:0] tod_10ths;            // TOD clock, 0.1 secs
reg [7:0] tod_sec;              //       "    seconds (BCD)
reg [7:0] tod_min;              //       "    minutes (BCD)
reg [7:0] tod_hr;               //       "    hours (BCD), PM (bit 7)
reg [7:0] alarm_10ths;          // TOD ALARM, 0.1 secs
reg [7:0] alarm_sec;            //       "    seconds (BCD)
reg [7:0] alarm_min;            //       "    minutes (BCD)
reg [7:0] alarm_hr;             //       "    hours (BCD), PM (bit 7)

initial begin
  clk_cnt = 3'd0;
end

always @(posedge CLK) if (CP2_NEGEDGE) begin
  todclk_dly <= TODCLK;
end
wire todclk_posedge = !todclk_dly && TODCLK;

always @(posedge CLK) if (CP2_NEGEDGE) begin
  jiffy <= 1'b0;
  if (todclk_posedge) begin
    if (clk_cnt == (TODIN ? 3'd4 : 3'd5)) begin
      clk_cnt <= 3'd0;
      jiffy <= 1'b1;
    end else
      clk_cnt <= clk_cnt + 1'd1;
  end
end

// Carries
wire tod_10ths_ci = jiffy;
wire tod_10ths_co = tod_10ths_ci && tod_10ths[3:0] == 4'h9;
wire tod_sec_1s_ci = tod_10ths_co;
wire tod_sec_1s_co = tod_sec_1s_ci && tod_sec[3:0] == 4'h9;
wire tod_sec_10s_ci = tod_sec_1s_co;
wire tod_sec_10s_co = tod_sec_10s_ci && tod_sec[6:4] == 3'h5;
wire tod_min_1s_ci = tod_sec_10s_co;
wire tod_min_1s_co = tod_min_1s_ci && tod_min[3:0] == 4'h9;
wire tod_min_10s_ci = tod_min_1s_co;
wire tod_min_10s_co = tod_min_10s_ci && tod_min[6:4] == 3'h5;
wire tod_hr_1s_ci = tod_min_10s_co;
wire tod_hr_1s_co = tod_hr_1s_ci && tod_hr[3:0] == 4'h9;
wire tod_hr_10s_ci = tod_hr_1s_co;
wire tod_hr_10s_co = tod_hr_1s_ci && tod_hr[5:0] == 5'h12;
wire tod_hr_pm_ci = tod_hr_10s_co;

// Gate to update clock
wire update = !HOLD && !stop;

always @(negedge nRES or posedge CLK) begin
  if (!nRES) begin
    tod_10ths <= 8'h00;
    tod_sec <= 8'h00;
    tod_min <= 8'h00;
    tod_hr <= 8'h01;
    alarm_10ths <= 8'h00;
    alarm_sec <= 8'h00;
    alarm_min <= 8'h00;
    alarm_hr <= 8'h00;
    stop <= 1'b1;
  end
  else if (CP2_NEGEDGE) begin
    if (ALARM && WRITE) begin
      case (RS)
        2'h0: alarm_10ths <= DB_I[3:0];
        2'h1: alarm_sec <= DB_I[6:0];
        2'h2: alarm_min <= DB_I[6:0];
        2'h3: begin
          alarm_hr[7] <= DB_I[7];
          alarm_hr[4:0] <= DB_I[4:0];
        end
        default: ;
      endcase
    end
    else if (!ALARM) begin
      if (WRITE && RS == 2'h0) begin
        tod_10ths[3:0] <= DB_I[3:0];
        stop <= 1'b0;
      end
      else if (update) begin
        tod_10ths[3:0] <= tod_10ths_co ? 4'h0 : tod_10ths[3:0] + tod_10ths_ci;
      end

      if (WRITE && RS == 2'h1) begin
        tod_sec <= DB_I[6:0];
      end
      else if (update) begin
        tod_sec[3:0] <= tod_sec_1s_co ? 4'h0 : tod_sec[3:0] + tod_sec_1s_ci;
        tod_sec[6:4] <= tod_sec_10s_co ? 3'h0 : tod_sec[6:4] + tod_sec_10s_ci;
      end

      if (WRITE && RS == 2'h2) begin
        tod_min <= DB_I[6:0];
      end
      else if (update) begin
        tod_min[3:0] <= tod_min_1s_co ? 4'h0 : tod_min[3:0] + tod_min_1s_ci;
        tod_min[6:4] <= tod_min_10s_co ? 3'h0 : tod_min[6:4] + tod_min_10s_ci;
      end

      if (WRITE && RS == 2'h3) begin
        tod_hr[7] <= DB_I[7];
        tod_hr[4:0] <= DB_I[4:0];
        stop <= 1'b1;
      end
      else if (update) begin
        if (tod_hr_10s_co) begin // PM <-> AM
          tod_hr[4:0] <= 5'h01;
          tod_hr[7] <= tod_hr[7] + tod_hr_pm_ci;
        end
        else begin
          tod_hr[3:0] <= tod_hr_1s_co ? 4'h0 : tod_hr[3:0] + tod_hr_1s_ci;
          tod_hr[4] <= tod_hr[4] + tod_hr_10s_ci;
        end
      end
    end
  end
end

reg [7:0] tod_10ths_l;          // TOD clock, latched copy
reg [7:0] tod_sec_l;
reg [7:0] tod_min_l;
reg [7:0] tod_hr_l;

always @(negedge nRES or posedge CLK) begin
  if (!nRES) begin
    tod_10ths_l <= 8'h0;
    tod_sec_l <= 8'h0;
    tod_min_l <= 8'h0;
    tod_hr_l <= 8'h0;
    latch <= 1'b0;
  end
  else if (CP2_NEGEDGE && !HOLD) begin
    if (READ) begin
      case (RS)
        2'h0: latch <= 1'b0;  // TOD_10THS
        2'h3: begin           // TOD_HR
          tod_10ths_l <= tod_10ths;
          tod_sec_l <= tod_sec;
          tod_min_l <= tod_min;
          tod_hr_l <= tod_hr;
          latch <= 1'b1;
        end
        default: ;
      endcase      
    end
  end
end

always @* begin
  DB_O = 8'h00;
  case (RS)
    2'h0: DB_O = latch ? tod_10ths_l : tod_10ths;
    2'h1: DB_O = latch ? tod_sec_l : tod_sec;
    2'h2: DB_O = latch ? tod_min_l : tod_min;
    2'h3: DB_O = latch ? tod_hr_l : tod_hr;
  endcase
end

//////////////////////////////////////////////////////////////////////
// Alarm interrupt
//
// The alarm interrupt is asserted only on the first clock that the
// TOD and ALARM registers match. The next interrupt is not asserted
// until after they no longer match. This ensures that the interrupt
// latch in the ICR can be cleared (on read) during the 100 ms that
// TOD == ALARM.

wire alarm_match = tod_10ths == alarm_10ths &&
                   tod_sec == alarm_sec &&
                   tod_min == alarm_min &&
                   tod_hr == alarm_hr;

reg  alarm_match_dly;

always @(posedge CLK) if (CP2_NEGEDGE)
  alarm_match_dly <= alarm_match;

assign ALARM_INT = alarm_match & !alarm_match_dly;

endmodule
