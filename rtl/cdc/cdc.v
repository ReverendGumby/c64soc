`timescale 1us / 1ns

module cdc_1bit_2ff
  (
   input              A_DATA,
   input              B_CLK,
   output             B_DATA
   );

(* ASYNC_REG = "TRUE" *)
reg [1:0] data_sync;

always @(posedge B_CLK)
  data_sync <= {data_sync[0], A_DATA};

assign B_DATA = data_sync[1];

endmodule

//////////////////////////////////////////////////////////////////////

module cdc_nbit_2ff
  #(parameter WIDTH = 1)
  (
   input [WIDTH-1:0]  A_DATA,
   input              B_CLK,
   output [WIDTH-1:0] B_DATA
   );

(* ASYNC_REG = "TRUE" *)
reg [WIDTH-1:0] data_sync0, data_sync1;

always @(posedge B_CLK) begin
  data_sync1 <= data_sync0;
  data_sync0 <= A_DATA;
end

assign B_DATA = data_sync1;

endmodule

//////////////////////////////////////////////////////////////////////

module cdc_mcp
  #(parameter WIDTH=1)
  (
   // Clock domain A: input side
   input                  A_CLK,
   input                  A_NEXT, // pulse
   input [WIDTH-1:0]      A_DATA,

   // Clock domain B: output side
   input                  B_CLK,
   output                 B_NEXT, // pulse
   output                 B_ACK, // toggle
   output reg [WIDTH-1:0] B_DATA
   );

reg [WIDTH-1:0] a_data_buf;
reg             b_next_tog_d;
wire            b_next_tog;
reg             a_next_tog;
reg             a_next_d;

initial begin
  b_next_tog_d = 1'b0;
  a_next_tog = 1'b0;
  a_next_d = 1'b0;
end

always @(posedge A_CLK) begin
  a_data_buf <= A_DATA;
  a_next_d <= A_NEXT;
  a_next_tog <= a_next_tog ^ A_NEXT;
end

cdc_1bit_2ff next_sync
  (
   .A_DATA(a_next_tog),
   .B_CLK(B_CLK),
   .B_DATA(b_next_tog)
   );

always @(posedge B_CLK) begin
  b_next_tog_d <= b_next_tog;
  if (B_NEXT)
    B_DATA <= a_data_buf;
end

assign B_NEXT = b_next_tog_d ^ b_next_tog;
assign B_ACK = b_next_tog;

endmodule

//////////////////////////////////////////////////////////////////////

module cdc_mcp_fb
  #(parameter WIDTH=1)
  (
   // Clock domain A: input side
   input              A_CLK,
   input              A_VALID, // request to send
   output             A_READY, // ready to send
   input [WIDTH-1:0]  A_DATA,

   // Clock domain B: output side
   input              B_CLK,
   output             B_NEXT, // pulse
   output [WIDTH-1:0] B_DATA
   );

reg             a_busy;
wire            a_next;
wire            a_ack;
wire            a_ack_tog, b_ack_tog;
reg             a_ack_tog_d;

initial begin
  a_busy = 1'b0;
  a_ack_tog_d = 1'b0;
end

always @(posedge A_CLK) begin
  if (a_next)
    a_busy <= 1'b1;
  else if (a_ack)
    a_busy <= 1'b0;
end

assign a_next = A_VALID & A_READY;
assign A_READY = ~a_busy;

cdc_mcp #(WIDTH) data_sync
  (
   .A_CLK(A_CLK),
   .A_NEXT(a_next),
   .A_DATA(A_DATA),
   .B_CLK(B_CLK),
   .B_NEXT(B_NEXT),
   .B_ACK(b_ack_tog),
   .B_DATA(B_DATA)
   );

cdc_1bit_2ff ack_sync
  (
   .A_DATA(b_ack_tog),
   .B_CLK(A_CLK),
   .B_DATA(a_ack_tog)
   );

always @(posedge A_CLK)
  a_ack_tog_d <= a_ack_tog;

assign a_ack = a_ack_tog_d ^ a_ack_tog;

endmodule
