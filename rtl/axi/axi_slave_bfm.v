/* verilator lint_off WIDTHTRUNC */

module axi_slave_bfm #(parameter ASIZE=32,
                       parameter DSIZE=32,
                       parameter STORE_LOG=19)
  (
   // AXI common signals
   input               ARESETn,
   input               ACLK,

   // AXI slave write channel
   input [3:0]         AWID,
   input [ASIZE-1:0]   AWADDR,
   input [7:0]         AWLEN,
   input [2:0]         AWSIZE,
   input [1:0]         AWBURST,
   input               AWVALID,
   output              AWREADY,
   input [DSIZE-1:0]   WDATA,
   input [DSIZE/8-1:0] WSTRB,
   input               WLAST,
   input               WVALID,
   output              WREADY,
   output reg [3:0]    BID,
   output              BVALID,
   input               BREADY,

   // AXI slave read channel
   input [3:0]         ARID,
   input [ASIZE-1:0]   ARADDR,
   input [7:0]         ARLEN,
   input [2:0]         ARSIZE,
   input [1:0]         ARBURST,
   input               ARVALID,
   output              ARREADY,
   output reg [3:0]    RID,
   output [DSIZE-1:0]  RDATA,
   output              RLAST,
   output              RVALID,
   input               RREADY
   );

localparam DBYTES = DSIZE / 8;
localparam STORE_SIZE = (1 << STORE_LOG) / DBYTES;
reg [DSIZE-1:0] store [0:STORE_SIZE - 1];
integer         latency = 0;

//////////////////////////////////////////////////////////////////////
// Test infrastructure

task store_get(input [ASIZE-1:0] addr, output [DSIZE-1:0] data);
reg [STORE_LOG-1:0] store_addr;
  begin
    store_addr = (addr / DBYTES) % STORE_SIZE;
    data = store[store_addr];
  end
endtask

task store_get_byte(input [ASIZE-1:0] addr, output [7:0] data);
reg [DSIZE-1:0]     store_data;
integer             byteoff;
  begin
    byteoff = addr % DBYTES;
    store_get(addr, store_data);
    store_data = store_data >> (byteoff * 8);
    data = store_data[7:0];
  end
endtask

task store_set(input [ASIZE-1:0] addr, input [DSIZE-1:0] data);
reg [STORE_LOG-1:0] store_addr;
  begin
    store_addr = (addr / DBYTES) % STORE_SIZE;
    store[store_addr] = data;
  end
endtask

task store_set_byte(input [ASIZE-1:0] addr, input [7:0] data);
reg [DSIZE-1:0]     store_data;
reg [DSIZE-1:0]     new_data;
reg [DSIZE-1:0]     mask;
integer             byteoff;
  begin
    byteoff = addr % DBYTES;
    new_data = {DSIZE{1'b0}};
    mask = {DSIZE{1'b0}};
    new_data[7:0] = data;
    mask[7:0] = 8'hFF;
    new_data = new_data << (byteoff * 8);
    mask = mask << (byteoff * 8);
    store_get(addr, store_data);
    store_data = (store_data & ~mask) | new_data;
    store_set(addr, store_data);
  end
endtask

//////////////////////////////////////////////////////////////////////
// Write channel

reg [3:0]       wid;
reg [ASIZE-1:0] waddr;
//reg [7:0]       wlen;
reg [2:0]       wsize;
//reg [1:0]       wburst;

reg [DSIZE-1:0]   wdata;
reg [DSIZE/8-1:0] wstrb;
reg               wlast;
reg               awvalid, wvalid;

reg               bvalid;
integer           wcnt;

wire [DSIZE-1:0]  wmask;
wire [STORE_LOG-1:0] store_waddr = (waddr / DBYTES) % STORE_SIZE;

always @(posedge ACLK or negedge ARESETn) begin
  if (!ARESETn) begin
    awvalid <= 1'b0;
    wvalid <= 1'b0;
    bvalid <= 1'b0;
    BID <= 4'bxxxx;
  end
  else begin
    if (AWVALID && AWREADY) begin
      // Save new address
      wid <= AWID;
      waddr <= AWADDR;
      //wlen <= AWLEN;
      wsize <= AWSIZE;
      //wburst <= AWBURST;
      awvalid <= AWVALID;
    end
    else if (wvalid) begin
      // Compute next address
      waddr <= waddr + (1 << wsize);
      if (wlast)
        awvalid <= 1'b0;
    end

    if (WVALID && WREADY) begin
      // Save new data to holding
      wdata <= WDATA;
      wstrb <= WSTRB;
      wlast <= WLAST;
      wvalid <= WVALID;
    end
    else if (WREADY) begin
      // Prepare for new data
      wvalid <= 1'b0;
    end

    if (!bvalid) begin
      // Check for end of burst
      if (wvalid && wlast) begin
        bvalid <= 1'b1;
        BID <= wid;
        wcnt <= 0;
      end
    end
    else if (bvalid && ~BVALID) begin
      wcnt <= wcnt + 1;
    end
    else if (BVALID && BREADY) begin
      // Prepare for new burst
      bvalid <= 1'b0;
      BID <= 4'bxxxx;
      wcnt <= 0;
    end
  end
end

// Expand write strobe to DSIZE-wide data mask
genvar wb;
generate
  for (wb = 0; wb < DBYTES; wb = wb + 1) begin: mask
    assign wmask[7+wb*8:wb*8] = {8{wstrb[wb]}};
  end
endgenerate

always @(posedge ACLK) begin
  // Move data from holding to storage
  if (wvalid)
    store[store_waddr] <= (store[store_waddr] & ~wmask) | (wdata & wmask);
end

assign AWREADY = !awvalid;
assign WREADY = !(wvalid && !awvalid); // else data will be written
assign BVALID = bvalid && (wcnt >= latency);

//////////////////////////////////////////////////////////////////////
// Read channel

reg [3:0]       rid;
reg [ASIZE-1:0] raddr;
reg [7:0]       rlen;
reg [2:0]       rsize;
//reg [1:0]       rburst;

reg             arvalid;
integer         rcnt;

wire [STORE_LOG-1:0] store_raddr = (raddr / DBYTES) % STORE_SIZE;

always @(posedge ACLK or negedge ARESETn) begin
  if (!ARESETn) begin
    arvalid <= 1'b0;
    rlen <= 8'd0;
    RID <= 4'bxxxx;
  end
  else begin
    if (ARVALID && ARREADY) begin
      // Save new address
      RID <= ARID;
      raddr <= ARADDR;
      rlen <= ARLEN;
      rsize <= ARSIZE;
      //rburst <= ARBURST;
      arvalid <= ARVALID;
      rcnt <= 0;
    end
    else if (arvalid && ~RVALID) begin
      rcnt <= rcnt + 1;
    end
    else if (RVALID && RREADY) begin
      // Compute next address
      if (!RLAST) begin
        rlen <= rlen - 8'd1;
        raddr <= raddr + (1 << rsize);
        rcnt <= 0;
      end
      else begin
        arvalid <= 1'b0;
        RID <= 4'bxxxx;
      end
    end
  end
end

assign ARREADY = !arvalid;
assign RVALID = arvalid && (rcnt >= latency);
assign RLAST = rlen == 8'd0;
assign RDATA = store[store_raddr];

endmodule
