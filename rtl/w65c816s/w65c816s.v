// Deviations from original W65C816S:
// - MX pin split into separate M, X pins
// - A[23:16] added, instead of being mux'd onto D_O

`timescale 1us / 1ns

module w65c816s
  (
   input         RESB, // reset (active-low)
   input         CLK, // main clock
   input         CP1_POSEDGE, // clock phase 1
   input         CP1_NEGEDGE,
   input         CP2_POSEDGE, // clock phase 2
   input         CP2_NEGEDGE,
   input         HOLD, // halt all activity (except monitor)
   input         IRQB, // interrupt request (active-low)
   input         NMIB, // non-maskable interrupt (active-low)
   input         ABORTB, // abort input (active-low)
   output        E, // emulation status
   output        MLB, // memory lock (active-low)
   output        M, // memory select status
   output        X, // index select status
   output [23:0] A, // address bus, output
   input [7:0]   D_I, // data bus, input
   output [7:0]  D_O, // data bus, output
   output        RWB, // read / not write
   input         RDYIN, // ready, input
   output        WAITN, // active-low "WAI and no interrupt"
   output        VPB, // vector pull (active-low)
   output        VDA, // valid data address
   output        VPA, // valid program address

   // debug monitor
   input [5:0]   MON_SEL,
   input         MON_WS,
   output [31:0] MON_DOUT,
   input [31:0]  MON_DIN
   );

`include "w65c816s_core.vh"

assign db = D_I;
assign D_O = dor;
assign A = {bar, aor};

endmodule
