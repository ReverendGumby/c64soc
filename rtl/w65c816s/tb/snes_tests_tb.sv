// snes_tests_tb: Run gilyon's SNES 65C816 basic test ROM
//
// ROM source: https://github.com/gilyon/snes-tests

`timescale 1us / 1ns

/* verilator lint_off INITIALDLY */
/* verilator lint_off ZERODLY */

module snes_tests_tb();

initial begin
  $timeformat(-6, 0, " us", 1);

`ifndef VERILATOR
  $dumpfile("snes_tests_tb.vcd");
  $dumpvars();
`else
  $dumpfile("snes_tests_tb.verilator.vcd");
  $dumpvars();
`endif
end

reg       clk, res;
reg [2:0] ccnt;
reg       cp1_posedge, cp1_negedge, cp2_posedge, cp2_negedge;
reg [7:0] d;
reg       check = 0;
reg       sync_d;

wire [23:0] a;
wire [7:0]  ba;
wire [7:0]  do_dut;
wire        e, mlb, m, x, rw, vpb, vda, vpa;

wire [23:0] a_sim;
wire [7:0]  do_sim;
wire        e_sim, mlb_sim, m_sim, x_sim, rw_sim, rdy_sim,
            vpb_sim, vda_sim, vpa_sim;

wire [7:0]  do_ram, do_rom;
wire        loram_nce, loram_nwe, loram_noe, lorom_nce;
wire [16:0] loram_a;
wire [21:0] lorom_a;

w65c816s dut
  (
   .RESB(~res),
   .CLK(clk),
   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .HOLD(1'b0),
   .IRQB(1'b1),
   .NMIB(1'b1),
   .ABORTB(1'b1),
   .E(e),
   .MLB(mlb),
   .M(m),
   .X(x),
   .A(a),
   .D_I(d),
   .D_O(do_dut),
   .RWB(rw),
   .RDYIN(1'b1),
   .WAITN(),
   .VPB(vpb),
   .VDA(vda),
   .VPA(vpa),
   .MON_SEL(6'd0),
   .MON_WS(1'b0),
   .MON_DOUT(),
   .MON_DIN()
   );

w65c816s_sim sim
  (
   .RESB(~res),
   .CLK(clk),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .IRQB(1'b1),
   .NMIB(1'b1),
   .ABORTB(1'b1),
   .E(e_sim),
   .MLB(mlb_sim),
   .M(m_sim),
   .X(x_sim),
   .A(a_sim),
   .D_I(d),
   .D_O(do_sim),
   .RWB(rw_sim),
   .RDYIN(rdy_sim),
   .WAITN(rdy_sim),
   .VPB(vpb_sim),
   .VDA(vda_sim),
   .VPA(vpa_sim),
   .HOLD(1'b0)
   );

ram #(17, 8) ram(.CLK(clk), .A(loram_a), .DI(do_dut), .DO(do_ram),
                 .nCE(loram_nce), .nWE(loram_nwe), .nOE(loram_noe));

rom #(18, 8) rom(.CLK(clk), .A(lorom_a[17:0]), .DO(do_rom),
                 .nCE(lorom_nce));

initial begin
integer fin;
  fin = $fopen("snes_tests.sfc", "r");
  assert(fin != 0) else $finish;
  rom.read_from_file(fin);
  $fclose(fin);
end

initial begin
  clk = 1;
  ccnt = 0;
  cp1_posedge = 0;
  cp1_negedge = 0;
  cp2_posedge = 0;
  cp2_negedge = 0;
  res = 1;
end

initial forever begin :clkgen
  #0.125 clk = ~clk;
end

always @(posedge clk)
  ccnt <= ccnt + 'b1;

always @* begin
  cp1_posedge = ccnt == 3'd7;
  cp1_negedge = ccnt == 3'd0;
  cp2_posedge = ccnt == 3'd2;
  cp2_negedge = ccnt == 3'd6;
end

assign ba[7:0] = a[23:16];

assign loram_a[15:0] = a[15:0];
assign loram_a[16] = a[16] & ba[6];
assign loram_nce = ~((~ba[6] & ~|a[15:13]) | (~ba[7] & &ba[6:1]));
assign loram_nwe = rw;
assign loram_noe = ~rw;

assign lorom_a[14:0] = a[14:0];
assign lorom_a[21:15] = a[22:16];
assign lorom_nce = (~ba[7] & &ba[6:1]) | ~a[15];

always @* begin
  d = do_dut;
  if (rw) begin
    if (~loram_nce)
      d = do_ram;
    else if (~lorom_nce)
      d = do_rom;
  end
end

always @(posedge clk) if (cp2_negedge)
  sync_d <= sim.sync;

wire sync_negedge = sync_d & ~sim.sync;

initial #0 begin
  sim.print_ins_enable = 1;

  #32 res <= 0;

  @(posedge clk) check = 1;

  #1145000 ;

  $finish;
end

event err;

always @(negedge clk) if (check & cp2_posedge) begin
  assert (sim.sync === dut.sync)
    else begin
      $display("**** assert SYNC @%t: sim %1b != dut %1b", $time, sim.sync, dut.sync);
      -> err;
    end
  assert (rw_sim === rw)
    else begin
      $display("**** assert RW @%t: sim %1b != dut %1b", $time, rw_sim, rw);
      -> err;
    end
  assert (a_sim === a)
    else begin
      $display("**** assert A @%t: sim %04x != dut %04x", $time, a_sim, a);
      -> err;
    end
  assert (rw || do_sim === do_dut)
    else begin
      $display("**** assert DO @%t: sim %02x != dut %02x", $time, do_sim, do_dut);
      -> err;
    end
end

always @(negedge clk) if (check & cp2_negedge) begin
  if (sync_negedge) begin
    assert (sim.ac === dut.ac)
      else begin
        $display("**** assert ac @%t: sim %8b != dut %8b", $time, sim.ac, dut.ac);
        -> err;
      end
    assert (sim.x === dut.x)
      else begin
        $display("**** assert x @%t: sim %8b != dut %8b", $time, sim.x, dut.x);
        -> err;
      end
    assert (sim.y === dut.y)
      else begin
        $display("**** assert y @%t: sim %8b != dut %8b", $time, sim.y, dut.y);
        -> err;
      end
    assert (sim.d === dut.d)
      else begin
        $display("**** assert d @%t: sim %8b != dut %8b", $time, sim.d, dut.d);
        -> err;
      end
    assert (sim.s === dut.s)
      else begin
        $display("**** assert s @%t: sim %8b != dut %8b", $time, sim.s, dut.s);
        -> err;
      end
    assert (sim.p === dut.p)
      else begin
        $display("**** assert p @%t: sim %8b != dut %8b", $time, sim.p, dut.p);
        -> err;
      end
    assert (sim.pbr === dut.pbr)
      else begin
        $display("**** assert p @%t: sim %8b != dut %8b", $time, sim.pbr, dut.pbr);
        -> err;
      end
    assert (sim.dbr === dut.dbr)
      else begin
        $display("**** assert p @%t: sim %8b != dut %8b", $time, sim.dbr, dut.dbr);
        -> err;
      end
  end
end

always @err begin
  check = 0;
  #20 $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s snes_tests_tb -o snes_tests_tb.vvp ../w65c816s.v w65c816s_sim.sv ram.sv rom.sv snes_tests_tb.sv && ./snes_tests_tb.vvp"
// End:
