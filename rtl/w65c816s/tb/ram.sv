`timescale 1us / 1ns

module ram
  #(parameter AW,
    parameter DW)
  (
   input           CLK,
   input           nCE,
   input           nWE,
   input           nOE,
   input [AW-1:0]  A,
   input [DW-1:0]  DI,
   output [DW-1:0] DO
   );

reg [DW-1:0] ram [0:((1 << AW) - 1)];
reg [DW-1:0] dor;

task read_from_file(input integer fin);
integer code;
  code = $fread(ram, fin);
endtask

task write_to_file(input integer fout);
int i;
  for (i = 0; i < $size(ram); i++) begin :write_loop
    $fwrite(fout, "%c", ram[i]);
  end
endtask

task set(input reg [AW-1:0] a, input reg [DW-1:0] v);
  ram[a] = v;
endtask

task get(input reg [AW-1:0] a, output reg [DW-1:0] v);
  v = ram[a];
endtask

always @(posedge CLK)
  dor <= ram[A];

assign DO = ~(nCE | nOE) ? dor : {DW{1'bz}};

always @(negedge CLK) begin
  if (~(nCE | nWE)) begin
    //$display("ram[%x] <= %x", A, D);
    ram[A] <= DI;
  end
end

endmodule
