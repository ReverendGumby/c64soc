// On interrupt, status bit D shall be cleared and I set. (Hey, that's
// just like reset.)

// PC saved to stack shall be the address of the instruction that was
// preempted (replaced with BRK) by the interrupt.

`timescale 1us / 1ns

module nmi_tb();

initial begin
  $timeformat(-6, 0, " us", 1);

  $dumpfile("nmi_tb.vcd");
  $dumpvars();
end

reg       clk, res, nmi;
reg       cp1_posedge, cp1_negedge, cp2_posedge, cp2_negedge;
reg [7:0] din;

wire [23:0] a;
wire [7:0]  dout;
wire        e, mlb, m, x, rw, vpb, vda, vpa;

w65c816s dut
  (
   .RESB(~res),
   .CLK(clk),
   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .HOLD(1'b0),
   .IRQB(1'b1),
   .NMIB(~nmi),
   .ABORTB(1'b1),
   .E(e),
   .MLB(mlb),
   .M(m),
   .X(x),
   .A(a),
   .D_I(din),
   .D_O(dout),
   .RWB(rw),
   .RDYIN(1'b1),
   .WAITN(),
   .VPB(vpb),
   .VDA(vda),
   .VPA(vpa)
   );

initial begin
  cp1_posedge = 0;
  cp1_negedge = 0;
  cp2_posedge = 0;
  cp2_negedge = 0;
  res = 1;
  clk = 1;
  nmi = 0;
end

initial forever begin :clkgen
  #0.125 clk = ~clk;
end

initial forever begin :cpgen
  @(posedge clk) cp1_posedge <= 0;
  @(posedge clk) cp1_negedge <= 1;
  @(posedge clk) cp1_negedge <= 0; cp2_posedge <= 1;
  @(posedge clk) cp2_posedge <= 0;
  @(posedge clk) ;
  @(posedge clk) ;
  @(posedge clk) cp2_negedge <= 1;
  @(posedge clk) cp2_negedge <= 0; cp1_posedge <= 1;
end

wire cp2 = dut.cp2;

task next_cycle();
  while (~cp1_posedge) @(posedge clk) ;
  @(posedge clk) ;
endtask

always @* begin
  casez (a)
    24'h00ffea:   din = 8'h00;
    24'h00ffeb:   din = 8'h81;
    24'h00fffc:   din = 8'h00;
    24'h00fffd:   din = 8'h80;
    24'h008000:   din = 8'h18;  // CLC
    24'h008001:   din = 8'hfb;  // XCE
    24'h008002:   din = 8'h58;  // CLI
    24'h008003:   din = 8'ha2;  // LDX #$FF
    24'h008004:   din = 8'hff;  // "
    24'h008005:   din = 8'h9a;  // TXS
    24'h008006:   din = 8'ha2;  // LDX #$FF  <-- NMI here
    24'h008007:   din = 8'hff;  // "
    24'h008008:   din = 8'h86;  // STX $04
    24'h008009:   din = 8'h04;  // "
    24'h008100:   din = 8'h40;  // RTI
    24'h008zzz:   din = 8'hea;  // NOP
    default: din = 8'hx;
  endcase
end

task assert_nmi();
endtask

initial #0 begin
  // Exit and complete reset
  next_cycle();
  next_cycle();
  res = 0;
  repeat (8) next_cycle();
  assert(a[23:0] == 24'h008000);

  // Assert NMI just before "NMI here"
  repeat (8) next_cycle();
  assert(a[23:0] == 24'h008005);
  nmi = 1;

  // Wait for interrupt to start
  repeat (4) next_cycle();

  // (cheat) Clear all unknowns in P
  dut.p = 8'b10110001;

  // Pretending to fetch BRK instead of next instruction
  assert(a[23:0] == 24'h008008);
  assert(rw == 'b1);
  assert({vda, vpa} == 'b11);
  next_cycle();

  assert(a[23:0] == 24'h008008);
  assert(rw == 'b1);
  assert({vda, vpa} == 'b00);
  next_cycle();

  // Push PBR
  assert(a[23:0] == 24'h0000ff);
  assert(dout == 8'h00);
  assert(rw == 'b0);
  assert({vda, vpa} == 'b10);
  next_cycle();

  // Push PCH
  assert(a[23:0] == 24'h0000fe);
  assert(dout == 8'h80);
  assert(rw == 'b0);
  assert({vda, vpa} == 'b10);
  next_cycle();

  // Push PCL
  assert(a[23:0] == 24'h0000fd);
  assert(dout == 8'h08);
  assert(rw == 'b0);
  assert({vda, vpa} == 'b10);
  next_cycle();

  // Push P
  assert(a[23:0] == 24'h0000fc);
  //assert(dout == 8'b??);
  assert(rw == 'b0);
  assert({vda, vpa} == 'b10);
  next_cycle();

  // Fetch NMI vector
  assert(a == 24'h00FFEA);
  assert(rw == 'b1);
  assert({vda, vpa} == 'b10);
  assert(vpb == 'b0);
  next_cycle();

  assert(a == 24'h00FFEB);
  assert({vda, vpa} == 'b10);
  assert(vpb == 'b0);
  next_cycle();

  // Fetching opcode at NMI vector
  assert(a == 24'h008100);
  assert({vda, vpa} == 'b11);
  next_cycle();

  // BRK has retired
  assert(a == 24'h008101);
  assert(dut.p[3] == 'b0);
  assert(dut.p[2] == 'b1);
  next_cycle();

  $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s nmi_tb -o nmi_tb.vvp ../w65c816s.v nmi_tb.sv && ./nmi_tb.vvp"
// End:
