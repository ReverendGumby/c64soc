`timescale 1us / 1ns

module one_op_tb();

initial begin
  $timeformat(-6, 0, " us", 1);

  //$dumpfile("one_op_tb.vcd");
  //$dumpvars();
end

reg       clk, res, irq;
reg       cp1_posedge, cp1_negedge, cp2_posedge, cp2_negedge;
reg       hold, be, be_next;
reg       short_blk;
reg       rdy1;
reg       do_wai, do_stp;
reg [23:0] end_a;

wire [23:0] a;
wire [7:0]  d, do_ram, do_dut;
wire        e, mlb, m, x, rw, vpb, vda, vpa;
wire        nce, nwe, noe;

w65c816s dut
  (
   .RESB(~res),
   .CLK(clk),
   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .HOLD(1'b0),
   .IRQB(~irq),
   .NMIB(1'b1),
   .ABORTB(1'b1),
   .E(e),
   .MLB(mlb),
   .M(m),
   .X(x),
   .A(a),
   .D_I(do_ram),
   .D_O(do_dut),
   .RWB(rw),
   .RDYIN(1'b1),
   .WAITN(),
   .VPB(vpb),
   .VDA(vda),
   .VPA(vpa),
   .MON_SEL(6'd0),
   .MON_WS(1'b0),
   .MON_DOUT(),
   .MON_DIN()
   );

ram #(24, 8) ram(.CLK(clk), .A(a), .DI(do_dut), .DO(do_ram), .nCE(nce),
                 .nWE(nwe), .nOE(noe));

initial begin
  clk = 1;
  cp1_posedge = 0;
  cp1_negedge = 0;
  cp2_posedge = 0;
  cp2_negedge = 0;
  res = 0;
  irq = 0;
  be_next = 0;
  be = 0;
  hold = 1;
  short_blk = 0;
  do_wai = 0;
  do_stp = 0;
end

initial forever begin :clkgen
  #0.125 clk = ~clk;
end

initial forever begin :cpgen
  @(posedge clk) cp1_posedge <= 0;
  @(posedge clk) cp1_negedge <= 1;
  @(posedge clk) cp1_negedge <= 0; cp2_posedge <= 1;
  @(posedge clk) cp2_posedge <= 0;
  @(posedge clk) ;
  @(posedge clk) ;
  @(posedge clk) cp2_negedge <= 1;
  @(posedge clk) cp2_negedge <= 0; cp1_posedge <= 1;
end

assign d = rw ? do_ram : do_dut;
assign nce = ~be | ~(vda | vpa);
assign nwe = ~be | rw;
assign noe = ~be | ~rw;

always @* dut.stall = hold;

wire [7:0] bc = { vda, vpa, ~vpb, rw, e, m, x, ~mlb };

struct packed {
reg [15:0] pc, s, a, d, x, y;
reg [7:0]  p, dbr, pbr;
reg        e;
} regs;

task test_start();
  res <= 'b0;
  irq <= 'b0;

  if (short_blk) begin
    short_blk = 0;
    {dut.b, dut.a} = 0;
    while (dut.ac != 16'hffff)
      test_next_cycle();
    test_next_cycle();
  end

  dut.ir = 8'hxx;

  while (~dut.stalled)
    test_next_cycle();

  dut.pc = regs.pc;
  dut.s = regs.s;
  {dut.b, dut.a} = regs.a;
  dut.d = regs.d;
  dut.p = regs.p;
  dut.x = regs.x;
  dut.y = regs.y;
  dut.dbr = regs.dbr;
  dut.pbr = regs.pbr;
  dut.e = regs.e;

  dut.aor = dut.pc;
  dut.bar = dut.pbr;

  force dut.t_reset = 1'b1;
  be_next <= 1;
  test_next_cycle();

  release dut.t_reset;
  hold <= 0;

  while (dut.stalled)
    test_next_cycle();
endtask

task test_next_cycle();
  while (~cp2_negedge)
    @(posedge clk) ;
  be <= be_next;
  while (~cp2_posedge)
    @(posedge clk) ;
  @(posedge clk) ;
  @(posedge clk) ;
endtask

task test_end();
  if (dut.ir == 8'hcb) begin
    do_wai = 1;
    // Doing a WAI
  end
  if (dut.ir == 8'hdb) begin
    do_stp = 1;
    // Doing a STP
  end

  if (~do_stp) begin
    be_next <= 0;
    hold <= 1;
    test_next_cycle();

    if ((vda & vpa) == 1'b0 && (dut.ir & 8'hef) == 8'h44) begin
      // MVN/MVP tests stop at 100 cycles, regardless of whether the
      // move has completed and advanced to the next instrution.
      short_blk = 1;
    end
    else if (do_wai) begin
      // Recover from WAI
      irq <= 'b1;
      test_next_cycle();
      test_next_cycle();
      irq <= 'b0;
      dut.intg = 'b0; // clear pending interrupt
    end
    else
      assert(vda & vpa == 1'b1);    // SYNC indicates last opcode ended

    end_a = a;
    test_next_cycle();
  end

  regs.pc = dut.pc;
  regs.s = dut.s;
  regs.a = {dut.b, dut.a};
  regs.d = dut.d;
  regs.p = dut.p;
  regs.x = dut.x;
  regs.y = dut.y;
  regs.dbr = dut.dbr;
  regs.pbr = dut.pbr;
  regs.e = dut.e;

  assert(end_a[15:0] == regs.pc);
  assert(end_a[23:16] == regs.pbr);

  if (do_stp) begin
    // Recover from STP
    do_stp = 0;
    res <= 'b1;
    test_next_cycle();
    test_next_cycle();
    res <= 'b0;
    repeat (7)
      test_next_cycle();
    hold <= 1;
    test_next_cycle();          // let BRK retire and touch flags
  end
endtask

`include "FILENAME"

initial #0 begin
  //dut.print_ins_enable = 1;
  dut.resp = 0;
  dut.respd = 0;
  //dut.t = 0;
  `OP_TEST();
  $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s one_op_tb -o one_op_tb.vvp ../w65c816s.v ram.sv one_op_tb.sv && ./one_op_tb.vvp"
// End:
