// WDC W65C816S simulation
//
// Credit to:
// . https://snes.nesdev.org/wiki/65C816
// . http://www.westerndesigncenter.com/wdc/documentation/w65c816s.pdf
// . http://www.6502.org/tutorials/65c816opcodes.html

`timescale 1us / 1ns

/* verilator lint_off WIDTHEXPAND */
/* verilator lint_off ZERODLY */

module w65c816s_sim
  (
   input         RESB, // reset (active-low)
   input         CLK, // main clock
   input         CP2_POSEDGE, // clock phase 2
   input         CP2_NEGEDGE,
   input         IRQB, // interrupt request (active-low)
   input         NMIB, // non-maskable interrupt (active-low)
   input         ABORTB, // abort input (active-low)
   output        E, // emulation status
   output        MLB, // memory lock (active-low)
   output        M, // memory select status
   output        X, // index select status
   output [23:0] A, // bank and address bus
   input [7:0]   D_I, // data bus, input
   output [7:0]  D_O, // data bus, output
   output        RWB, // read / not write
   input         RDYIN, // ready, input
   output        WAITN, // active-low "WAI and no interrupt"
   output        VPB, // vector pull (active-low)
   output        VDA, // valid data address
   output        VPA, // valid program address
   input         HOLD
   );

// emulation mode status bits
`define p_c p[0]    // Carry
`define p_z p[1]    // Zero
`define p_i p[2]    // IRQ Disable
`define p_d p[3]    // Decimal
`define p_b p[4]    // Break
`define p_v p[6]    // oVerflow
`define p_n p[7]    // Negative

// native mode status bits
`define p_x p[4]    // indeX register select: 1 = 8-bit, 0 = 16-bit
`define p_m p[5]    // Memory/accumulator select: 1 = 8-bit, 0 = 16-bit

string    opcode_st [0:255];
reg [6:0] opcode_op [0:255];
reg [4:0] opcode_am [0:255];
reg [4:0] opcode_cy [0:255];
`include "w65c816s_sim_opcodes.svh"

reg [15:0] pc, s, d, x, y;
reg [7:0]  ir, ir_d;
reg [7:0]  a, b, p, dbr, pbr;
reg [7:0]  ba, dor;
reg [15:0] m;
reg [16:0] tmp;
reg [23:0] ea, npc;
reg [15:0] ac_out;
reg        e;
reg        cp2;
reg        sync;
reg        eac, op_store, op_rmw;
reg [2:0]  hc;
reg        resp, irqp, nmip;
reg        resg, irqg, nmig, abtg, copg;
wire       nmie;
reg        nmil;
reg [6:0]  op, op_d;
reg [4:0]  am, am_d;
reg [15:0] ab;
reg        rw_int, res_int;
reg        wai_act, wai_act_d;
reg        branch_take;
reg        ml, vp, vpa, vda;
reg        opw, amw, idxw, opw_d, idxw_d;
reg [15:0] amm, ams, idxm, idxs, idxm_d, idxs_d;
reg        origop;
integer    cycles, cy_extra;

wire [15:0] ac = {b, a};

assign RWB = rw_int || resg;
assign A = {ba, ab};
assign D_O = dor;

assign MLB = ~ml;
assign M = `p_m;
assign X = `p_x;
assign E = e;

assign VPB = ~vp;
assign VDA = sync | vda;
assign VPA = sync | vpa;

assign WAITN = ~(wai_act_d & ~((nmie | nmig) | irqp)) | resp;
wire rdy_int = RDYIN & WAITN;

wire [7:0] db = D_I;

initial cp2 = 'b0;

always @(posedge CLK)
  cp2 <= (cp2 | CP2_POSEDGE) & ~CP2_NEGEDGE;

task automatic wait_clk(int cnt = 1);
  for (int i = 0; i < cnt; i++) begin
    do begin
      @(posedge CLK) ;
    end while (!(CP2_NEGEDGE & rdy_int));
  end
endtask

initial begin
  {resg, irqg, nmig, abtg, copg} = 0;
  {irqp, nmip} = 0;
  resp = 1;
  //e = 1;
  //rw_int = 1;
  res_int = 0;
  ml = 0;
  //vp = 0;
  //vpa = 0;
  //vda = 0;
  wai_act = 0;
  wai_act_d = 0;
end

always @(posedge CLK) if (CP2_NEGEDGE) begin
  resp <= ~RESB;
  irqp <= ~IRQB;
  nmip <= ~NMIB;
end

assign nmie = nmip & ~nmil;

always @(posedge CLK) if (!HOLD & CP2_NEGEDGE) begin
  if (nmip & nmig)
    nmil <= 1'b1;
  else if (!nmip)
    nmil <= 1'b0;
end

always @(posedge CLK) if (!HOLD & CP2_NEGEDGE) begin
  if (nmie)
    nmig <= 1'b1;

  if (irqp && !`p_i)
    irqg <= 1'b1;
end

// WAI affects WAITN on CP2 rising edge
always @(posedge CLK) if (CP2_POSEDGE)
  wai_act_d <= wai_act;

initial cycles = 0;
always @(posedge CLK) begin
  if (CP2_NEGEDGE)
    cycles <= cycles + 1;
end

always #0 begin
  if (HOLD) begin
    sync <= ~res_int;
    wait_clk();
    ir = 8'hxx;
  end
  else begin
    if (resp | res_int) begin
      wait_clk();
      sync <= 0;
      rw_int <= 1'b1;
      wai_act <= 0;
      resg <= 1'b1;
      res_int = res_int & ~resp;

      if (resp) begin
        e = 'b1;
        fixup_regs();
        d = 0;
        dbr = 0;
        pbr = 0;
        vp <= 'b0;
        vpa <= 0;
        vda <= 0;
      end
    end
    else
      do_ins();
  end
end

function [15:0] get_vec();
  get_vec[15:4] = {11'h7ff, e};
  if (resg)
    get_vec[3:0] = 4'hc;
  else if (abtg)
    get_vec[3:0] = 4'h8;
  else if (nmig)
    get_vec[3:0] = 4'ha;
  else if (irqg)
    get_vec[3:0] = 4'he;
  else if (copg)
    get_vec[3:0] = 4'h4;
  else if (~e)
    get_vec[3:0] = 4'h6;
  else // BRK, emulation mode
    get_vec[3:0] = 4'he;
endfunction

task fixup_regs();
  if (e) begin
    // emulation mode restrictions
    s[15:8] = 8'h01;
    p[5:4] = 2'b11;               // Set `p_x (`p_b), `p_m (reserved)
  end

  if (`p_x) begin
    // narrow index regs
    x[15:8] = 8'b0;
    y[15:8] = 8'b0;
  end
endtask

event do_retire;

task do_ins();
integer   cy_opstart, cy_want, cy_got;
  cy_opstart = cycles;
  cy_extra = 0;
  sync <= 1;
  ml <= 'b0;
  vp <= 'b0;
  vpa <= 'b1;
  vda <= 'b0;
  ab <= pc;
  ba <= pbr;
  rw_int <= 1'b1;

  wait_clk();
  sync <= 0;
  if (resg | irqg | nmig)
    ir = `BRK;
  else
    ir = db;
  print_ins();
  if (!(resg | irqg | nmig))
    pc = pc + 1;
  ab <= pc;

  _do_ins();

  if (ir != `WAI && ir != `STP) begin
    cy_want = 32'(opcode_cy[ir]) + cy_extra;
    cy_got = cycles - cy_opstart;
    assert (cy_got == cy_want)
      else
        $fatal(1, "Cycles wanted %1d got %1d", cy_want, cy_got);
  end
endtask

task add_s(input signed [7:0] i);
  if (e && origop)
    s[7:0] = s[7:0] + i;
  else
    s = s + 16'(i);
endtask

task add_ea(input signed [7:0] i);
  if (am == `AM_DP || am == `AM_DPX || am == `AM_DPY || am == `AM_IMM ||
      am == `AM_SR)
    ea[15:0] = ea[15:0] + 16'(i);
  else
    ea = ea + 24'(i);
endtask

task _do_ins();
  op = opcode_op[ir];
  am = opcode_am[ir];
  //$display("sim: ir=%02x op=%02x am=%d", ir, op, am);

  if (am != `AM_BLK && (ir != `BRK && ir != `COP && ir != `WAI)) begin
    if (am == `AM_IMPL || am == `AM_A)
      vpa <= 'b0;

    wait_clk();
  end

  origop = 'b1;
  case (op)
    `OP_BRA, `OP_BRL, `OP_JML, `OP_JSL, `OP_MVN, `OP_MVP, `OP_PEA, `OP_PEI, 
    `OP_PER, `OP_PHB, `OP_PHD, `OP_PHK, `OP_PLB, `OP_PLD, `OP_REP, `OP_RTL, 
    `OP_SEP, `OP_TCD, `OP_TCS, `OP_TDC, `OP_TRB, `OP_TSB, `OP_TSC, `OP_TXY, 
    `OP_TYX, `OP_WAI, `OP_WDM, `OP_XBA, `OP_XCE:
      origop = 'b0;
    default: ;
  endcase

  op_store = op == `OP_DEC || op == `OP_INC || op == `OP_STA ||
             op == `OP_STX || op == `OP_STY || op == `OP_STZ ||
             op == `OP_ASL || op == `OP_LSR || op == `OP_ROL ||
             op == `OP_ROR;
  op_rmw = am != `AM_A &&
           (op == `OP_ASL || op == `OP_DEC || op == `OP_INC ||
            op == `OP_LSR || op == `OP_ROL || op == `OP_ROR ||
            op == `OP_TRB || op == `OP_TSB);

  case (op)
    `OP_CPX, `OP_CPY, `OP_DEX, `OP_DEY, `OP_INX, `OP_INY, `OP_LDX, `OP_LDY,
    `OP_PHX, `OP_PHY, `OP_PLX, `OP_PLY, `OP_STX, `OP_STY:
      opw = ~`p_x;
    `OP_PHB, `OP_PHK, `OP_PHP, `OP_PLB, `OP_PLP, `OP_REP, `OP_SEP:
      opw = 'b0;
    `OP_PHD, `OP_PEA, `OP_PEI, `OP_PER, `OP_PLD:
      opw = 'b1;
    default: opw = ~`p_m;
  endcase

  idxw = ~`p_x;
  idxm = {(idxw * 8'hff), 8'hff};
  idxs = idxm ^ (idxm >> 1);

  if (op == `OP_BRA || op == `OP_BRL)
    branch_take = ir === `BRA_REL ||
                  ir === `BPL_REL && !`p_n ||
                  ir === `BMI_REL &&  `p_n ||
                  ir === `BVC_REL && !`p_v ||
                  ir === `BVS_REL &&  `p_v ||
                  ir === `BCC_REL && !`p_c ||
                  ir === `BCS_REL &&  `p_c ||
                  ir === `BNE_REL && !`p_z ||
                  ir === `BEQ_REL &&  `p_z ||
                  ir === `BRL_LREL;

  case (am)
    `AM_A: begin
      m = ac;
    end
    `AM_ABS, `AM_ABSX, `AM_ABSY: begin
      ea[7:0] = db;
      pc = pc + 1;
      ab <= pc;
      wait_clk();

      ea[15:8] = db;
      ea[23:16] = dbr;
      pc = pc + 1;
      vpa <= 'b0;
      if (am == `AM_ABSX || am == `AM_ABSY) begin
        {eac, ea[7:0]} = ea[7:0] + (am == `AM_ABSX ? x[7:0] : y[7:0]);
        {ba, ab} <= ea;
        if (eac | op_store | idxw) begin
          if (!op_store)
            cy_extra += 1;
          wait_clk();

          if (eac)
            ea = ea + 16'h0100;
        end
        ea = ea + ((am == `AM_ABSX ? x : y) & idxm & ~24'hff);
      end
    end
    `AM_AIND: begin
      ea[7:0] = db;
      pc = pc + 1;
      ab <= pc;
      wait_clk();

      ea[15:8] = db;
      ea[23:16] = 8'b0;
      pc = pc + 1;
      {ba, ab} <= ea;
      vpa <= 'b0;
      vda <= 'b1;
      wait_clk();

      ea[7:0] = db;
      ab[15:0] <= ab[15:0] + 1;
      wait_clk();

      ea[15:8] = db;
      if (ir == `JML_AIND) begin
        ab[15:0] <= ab[15:0] + 1;
        wait_clk();

        ea[23:16] = db;
      end
      else
        ea[23:16] = pbr;
    end
    `AM_AINDX: begin
      ea[7:0] = db;
      ea[23:16] = pbr;
      pc = pc + 1;

      if (ir == `JSR_AINDX) begin
        npc = ea;

        ea = s;
        add_s(-1);
        {ba, ab} <= ea;
        rw_int <= 0;
        vda <= 'b1;
        vpa <= 'b0;
        dor <= pc[15:8];
        wait_clk();

        ea = s;
        add_s(-1);
        {ba, ab} <= ea;
        dor <= pc[7:0];
        wait_clk();

        ea = npc;
        {ba, ab} <= ea;
        rw_int <= 'b1;
        vda <= 'b0;
        vpa <= 'b1;
      end

      ab <= pc;
      wait_clk();

      ea[15:8] = db;
      pc = pc + 1;
      vpa <= 'b0;
      wait_clk();
      
      ea[15:0] = ea[15:0] + x;
      {ba, ab} <= ea;
      vda <= 'b1;
      wait_clk();

      ea[7:0] = db;
      ab[15:0] <= ab[15:0] + 1;
      wait_clk();

      ea[15:8] = db;
      ea[23:16] = pbr;
    end
    `AM_BLK: ;
    `AM_LABS, `AM_LABSX: begin
      if (ir != `JSL_LABS) begin
        ea[7:0] = db;
        pc = pc + 1;
        ab <= pc;
        wait_clk();

        ea[15:8] = db;
        pc = pc + 1;
        ab <= pc;
        wait_clk();

        ea[23:16] = db;
        pc = pc + 1;
        if (am == `AM_LABSX)
          ea = ea + x;
        vpa <= 'b0;
      end
    end
    `AM_DP, `AM_DPX, `AM_DPY: begin
      ea = d + db;
      ea[23:16] = 8'b0;
      pc = pc + 1;
      vpa <= 'b0;
      if (d[7:0] != 0) begin
        cy_extra += 1;
        wait_clk();
      end

      if (am == `AM_DPX || am == `AM_DPY) begin
        ea = ea + ((am == `AM_DPX ? x : y) & idxm);
        ea[23:16] = 8'h0;
        wait_clk();
      end

      if (e && origop && d[7:0] == 0)
        ea[15:8] = d[15:8];
    end
    `AM_IMM: begin
      ea = {pbr, pc};
      pc = pc + 1;
    end
    `AM_IMPL: ;
    `AM_IND: begin
      ea = d + db;
      ea[23:16] = 8'b0;
      pc = pc + 1;
      vpa <= 'b0;
      if (d[7:0] != 0) begin
        cy_extra += 1;
        wait_clk();
      end

      {ba, ab} <= ea;
      vda <= 'b1;
      wait_clk();

      ea[7:0] = db;
      ab[15:0] <= ab[15:0] + 1;
      wait_clk();

      ea[15:8] = db;
      ea[23:16] = dbr;
    end
    `AM_INDYI: begin
      ea = d + db;
      if (e && origop && d[7:0] == 0)
        ea[15:8] = d[15:8];
      ea[23:16] = 8'b0;
      pc = pc + 1;
      vpa <= 'b0;
      if (d[7:0] != 0) begin
        cy_extra += 1;
        wait_clk();
      end

      {ba, ab} <= ea;
      vda <= 'b1;
      wait_clk();

      ea[7:0] = db;
      ab[15:0] <= ab[15:0] + 1;
      wait_clk();

      ea[15:8] = db;
      ea[23:16] = dbr;
      {eac, ea[7:0]} = ea[7:0] + y[7:0];
      {ba, ab} <= ea;
      vda <= 'b0;
      if (eac | op_store | idxw) begin
        if (!op_store)
          cy_extra += 1;
        wait_clk();

        if (eac)
          ea = ea + 16'h0100;
      end
      ea = ea + (y & idxm & ~24'hff);
    end
    `AM_LIND, `AM_LINDYI: begin
      ea = d + db;
      ea[23:16] = 8'b0;
      pc = pc + 1;
      vpa <= 'b0;
      if (d[7:0] != 0) begin
        cy_extra += 1;
        wait_clk();
      end

      {ba, ab} <= ea;
      vda <= 'b1;
      wait_clk();

      ea[7:0] = db;
      ab[15:0] <= ab[15:0] + 1;
      wait_clk();

      ea[15:8] = db;
      ab[15:0] <= ab[15:0] + 1;
      wait_clk();

      ea[23:16] = db;
      if (am == `AM_LINDYI)
        ea = ea + (y & idxm);
    end
    `AM_REL: begin
      assert (op == `OP_BRA);

      pc = pc + 1;
      ea[7:0] = db;
      ea[15:8] = {8{db[7]}};
      ea[23:16] = pbr;

      if (branch_take) begin
        vpa <= 'b0;
        cy_extra += 1;
        wait_clk();

        if (e) begin
          {eac, ea[7:0]} = ea[7:0] + pc[7:0];

          if (ea[15:8] + eac != 8'h00) begin
            cy_extra += 1;
            wait_clk();

            ea[15:8] = ea[15:8] + pc[15:8] + eac;
          end
          else
            ea[15:8] = pc[15:8];
        end
        else begin
          ea[15:0] = ea[15:0] + pc;
        end
      end
    end
    `AM_LREL: begin
      assert (op == `OP_BRL || op == `OP_PER);

      pc = pc + 1;
      ea[7:0] = db;
      ab <= pc;
      wait_clk();

      pc = pc + 1;
      ea[15:8] = db;
      vpa <= 'b0;
      wait_clk();

      ea[15:0] = ea[15:0] + pc;
    end
    `AM_SR: begin
      ea = s + db;
      ea[23:16] = 8'b0;
      pc = pc + 1;
      vpa <= 'b0;
      wait_clk();
    end
    `AM_SRINDY: begin
      ea = s + db;
      ea[23:16] = 8'b0;
      pc = pc + 1;
      vpa <= 'b0;
      wait_clk();

      {ba, ab} <= ea;
      vda <= 'b1;
      wait_clk();

      ea[7:0] = db;
      ab[15:0] <= ab[15:0] + 1;
      wait_clk();

      ea[15:8] = db;
      ea[23:16] = dbr;
      ea = ea + (y & idxm);
      vda <= 'b0;
      wait_clk();
    end
    `AM_XIIND: begin
      ea = d + db;
      ea[23:16] = 8'b0;
      pc = pc + 1;
      vpa <= 'b0;
      if (d[7:0] != 0) begin
        cy_extra += 1;
        wait_clk();
      end

      ea[15:0] = ea[15:0] + x;
      if (e && origop && d[7:0] == 0)
        ea[15:8] = d[15:8];
      wait_clk();

      {ba, ab} <= ea;
      vda <= 'b1;
      wait_clk();

      ea[7:0] = db;
      ab[15:0] <= ab[15:0] + 1;
      wait_clk();

      ea[15:8] = db;
      ea[23:16] = dbr;
    end
    default: $finish(1); // shouldn't get here
  endcase

  case (op)
    `OP_ADC, `OP_AND, `OP_ASL, `OP_BIT, `OP_CMP, `OP_CPX, `OP_CPY, `OP_DEC,
    `OP_EOR, `OP_INC, `OP_LDA, `OP_LDX, `OP_LDY, `OP_LSR, `OP_ORA,
    `OP_ROL, `OP_ROR, `OP_SBC, `OP_TRB, `OP_TSB: begin
      if (am == `AM_A)
        m = ac;
      else begin
        ml <= op_rmw;
        if (am != `AM_IMM) begin
          {ba, ab} <= ea;
          vda <= 'b1;
          wait_clk();
        end

        m[7:0] = db;
        if (opw) begin
          if (am == `AM_IMM)
            pc = pc + 1;
          add_ea(1);
          {ba, ab} <= ea;
          cy_extra += 1;
          wait_clk();

          m[15:8] = db;
        end
      end
    end
    `OP_BRK, `OP_COP: begin
      pc = pc + 1;
      copg = op == `OP_COP;

      vpa <= ~(resg | irqg | nmig);
      vda <= 'b0;
      wait_clk();

      rw_int <= 0;
      vpa <= 'b0;
      vda <= 'b1;

      if (~e) begin
        cy_extra += 1;
        ea = s;
        add_s(-1);
        {ba, ab} <= ea;
        dor <= pbr;
        wait_clk();
      end

      ea = s;
      add_s(-1);
      {ba, ab} <= ea;
      dor <= pc[15:8];
      wait_clk();

      ea = s;
      add_s(-1);
      {ba, ab} <= ea;
      dor <= pc[7:0];
      wait_clk();

      ea = s;
      add_s(-1);
      {ba, ab} <= ea;
      dor <= p;
      wait_clk();

      vp <= 'b1;
      rw_int <= 'b1;
      ab <= get_vec();
      wait_clk();

      ea[7:0] = db;
      ab <= ab + 1;
      wait_clk();

      ea[15:8] = db;
    end
    `OP_JSR: begin
      if (ir == `JSR_ABS) begin
        pc = pc - 1;
        npc = ea;

        vda <= 'b0;
        wait_clk();

        ea = s;
        add_s(-1);
        {ba, ab} <= ea;
        rw_int <= 0;
        vda <= 'b1;
        dor <= pc[15:8];
        wait_clk();

        ea = s;
        add_s(-1);
        {ba, ab} <= ea;
        dor <= pc[7:0];
        wait_clk();

        ea = npc;
      end
    end
    `OP_JSL: begin
      ea[7:0] = db;
      pc = pc + 1;
      ab <= pc;
      wait_clk();

      ea[15:8] = db;
      npc = ea;

      pc = pc + 1;
      ea = s;
      add_s(-1);
      {ba, ab} <= ea;
      rw_int <= 0;
      vpa <= 'b0;
      vda <= 'b1;
      dor <= pbr;
      wait_clk();

      rw_int <= 'b1;
      vda <= 'b0;
      wait_clk();

      {ba, ab} <= {pbr, pc};
      vpa <= 'b1;
      wait_clk();

      npc[23:16] = db;
      ea = s;
      add_s(-1);
      {ba, ab} <= ea;
      rw_int <= 0;
      vda <= 'b1;
      vpa <= 'b0;
      dor <= pc[15:8];
      wait_clk();

      ea = s;
      add_s(-1);
      {ba, ab} <= ea;
      dor <= pc[7:0];
      wait_clk();

      ea = npc;
    end
    `OP_MVN, `OP_MVP: begin
    reg [15:0] dir;
      dir = (op == `OP_MVP) ? -16'd1 : 16'd1;
      ab <= pc;
      vpa <= 'b1;
      wait_clk();

      dbr = db;
      pc = pc + 1;
      ab <= pc;
      wait_clk();

      {ba, ab} <= {db, x};
      vpa <= 'b0;
      vda <= 'b1;
      wait_clk();

      if (idxw)
        x = x + dir;
      else
        x[7:0] = x[7:0] + dir[7:0];
      {ba, ab} <= {dbr, y};
      dor <= db;
      rw_int <= 'b0;
      wait_clk();

      if (idxw)
        y = y + dir;
      else
        y[7:0] = y[7:0] + dir[7:0];
      vda <= 'b0;
      rw_int <= 'b1;
      wait_clk();

      if (ac != 0)
        pc = pc - 2;
      else
        pc = pc + 1;
      {b, a} = ac - 1;
      wait_clk();
    end
    `OP_PHA, `OP_PHB, `OP_PHD, `OP_PHK, `OP_PHP, `OP_PHX, `OP_PHY,
      `OP_PEA, `OP_PEI, `OP_PER: begin
      case (op)
        `OP_PHA: m = ac;
        `OP_PHB: m = dbr;
        `OP_PHD: m = d;
        `OP_PHK: m = pbr;
        `OP_PHP: m = p;
        `OP_PHX: m = x;
        `OP_PHY: m = y;
        `OP_PEA, `OP_PER: m = ea[15:0];
        `OP_PEI: begin
          {ba, ab} <= ea;
          vda <= 'b1;
          wait_clk();

          m[7:0] = db;
          add_ea(1);
          {ba, ab} <= ea;
          wait_clk();

          m[15:8] = db;
        end
        default: $finish(1); // shouldn't get here
      endcase
      ea = s;
      add_s(-1);
      {ba, ab} <= ea;
      rw_int <= 0;
      vda <= 'b1;
      if (opw) begin
        case (op)
          `OP_PHA, `OP_PHB, `OP_PHK, `OP_PHP, `OP_PHX, `OP_PHY:
            cy_extra += 1;
          default: ;
        endcase
        dor <= m[15:8];
        wait_clk();

        ea = s;
        add_s(-1);
        {ba, ab} <= ea;
      end
      dor <= m[7:0];
      wait_clk();
    end
    `OP_PLA, `OP_PLB, `OP_PLD, `OP_PLP, `OP_PLX, `OP_PLY: begin
      add_s(1);
      ea = s;
      wait_clk();

      {ba, ab} <= ea;
      vda <= 'b1;
      wait_clk();

      m[7:0] = db;

      if (opw) begin
        case (op)
          `OP_PLA, `OP_PLB, `OP_PLP, `OP_PLX, `OP_PLY:
            cy_extra += 1;
          default: ;
        endcase
        add_s(1);
        ea = s;
        {ba, ab} <= ea;
        wait_clk();

        m[15:8] = db;
      end
      case (op)
        `OP_PLA: begin
          a = m[7:0];
          if (opw)
            b = m[15:8];
        end
        `OP_PLB: dbr = m[7:0];
        `OP_PLD: d = m;
        `OP_PLP: begin
          p = m[7:0];
          if (e)
            p[5:4] = 2'b11;
        end
        `OP_PLX: begin
          x[7:0] = m[7:0];
          if (opw)
            x[15:8] = m[15:8];
        end
        `OP_PLY: begin
          y[7:0] = m[7:0];
          if (opw)
            y[15:8] = m[15:8];
        end
        default: $finish(1); // shouldn't get here
      endcase
    end
    `OP_RTS, `OP_RTL, `OP_RTI: begin
      wait_clk();

      vda <= 'b1;
      if (op == `OP_RTI) begin
        add_s(1);
        ea = s;
        {ba, ab} <= ea;
        wait_clk();

        m[7:0] = db;
      end

      add_s(1);
      ea = s;
      {ba, ab} <= ea;
      wait_clk();

      npc[7:0] = db;
      add_s(1);
      ea = s;
      {ba, ab} <= ea;
      wait_clk();

      npc[15:8] = db;
      if (e && op == `OP_RTI) begin
        cy_extra -= 1;
        npc[23:16] = pbr;
      end
      else begin
        if (op == `OP_RTL || op == `OP_RTI) begin
          add_s(1);
          ea = s;
          {ba, ab} <= ea;
        end
        else
          vda <= 'b0;
        wait_clk();

        if (op == `OP_RTL || op == `OP_RTI)
          npc[23:16] = db;
        else
          npc[23:16] = pbr;
      end
        
      ea = npc;
      if (op != `OP_RTI)
        ea[15:0] = ea[15:0] + 1;
    end
    `OP_REP, `OP_SEP: begin
      m[7:0] = db;
      vpa <= 'b0;
      wait_clk();
    end
    `OP_STA, `OP_STX, `OP_STY, `OP_STZ: begin
      case (op)
        `OP_STA: m = ac;
        `OP_STX: m = x;
        `OP_STY: m = y;
        `OP_STZ: m = 0;
        default: ;
      endcase

      {ba, ab} <= ea;
      vda <= 'b1;
      dor <= m[7:0];
      rw_int <= 0;
      wait_clk();

      if (opw) begin
        add_ea(1);
        {ba, ab} <= ea;
        dor <= m[15:8];
        cy_extra += 1;
        wait_clk();
      end
    end
    `OP_WDM: begin
      pc = pc + 1;
    end
    `OP_XBA: begin
      wait_clk();
    end
    default: ;
  endcase

  if (op_rmw) begin
    do_alu(op, opw, ac, m, m);
    vda <= 'b0;
    if (e) begin
      rw_int <= 0;
      dor <= db;
    end
    wait_clk();

    vda <= 'b1;
    rw_int <= 0;
    if (opw) begin
      dor <= m[15:8];
      cy_extra += 1;
      wait_clk();

      add_ea(-1);
      {ba, ab} <= ea;
    end
    dor <= m[7:0];
    wait_clk();
  end

  case (op)
    `OP_BRA, `OP_BRL: begin
      if (branch_take)
        pc = ea[15:0];
    end
    `OP_JMP, `OP_JML, `OP_JSR, `OP_JSL, `OP_RTS, `OP_RTL: begin
      pc = ea[15:0];
      if (am == `AM_LABS || ir == `JML_AIND || ir == `JSL_LABS ||
          ir == `RTL || ir == `RTI)
        pbr = ea[23:16];
    end
    `OP_RTI: begin
      pc = ea[15:0];
      pbr = ea[23:16];
    end
    `OP_BRK, `OP_COP: begin
      pc = ea[15:0];
      pbr = 0;
      {resg, nmig, irqg} <= 0;
    end
    `OP_WAI: begin
      // Cycle 2 (just after SYNC falls)
      vpa <= 'b0;
      wai_act <= 1;
      wait_clk();

      // Cycle 3
      wai_act <= 0;
      wait_clk();
    end
    `OP_STP: begin
      res_int = 'b1;
      wait_clk();
    end
    default: ;
  endcase

  -> do_retire;
endtask

// These statements execute during the next instruction fetch cycle.
always @do_retire begin
  ir_d = ir;
  op_d = op;
  am_d = am;

  opw_d = opw;

  amw = ~`p_m;
  amm = {(amw * 8'hff), 8'hff};
  ams = amm ^ (amm >> 1);

  idxw_d = idxw;
  idxm_d = idxm;
  idxs_d = idxs;

  wait_clk();
  case (op_d)
    `OP_BRA, `OP_BRL,
    `OP_JMP, `OP_JML, `OP_JSR, `OP_JSL,
    `OP_MVN, `OP_MVP,
    `OP_NOP, `OP_PHA, `OP_PHB, `OP_PHD, `OP_PHK, `OP_PHP,
    `OP_PHX, `OP_PHY, `OP_PEA, `OP_PEI, `OP_PER,
    `OP_RTS, `OP_RTL,
    `OP_STA, `OP_STP, `OP_STX, `OP_STY, `OP_STZ, 
    `OP_WAI, `OP_WDM:
      ;
    `OP_ADC, `OP_AND, `OP_EOR, `OP_ORA, `OP_SBC: begin
      ac_out = ac;
      do_alu(op_d, opw_d, ac, m, ac_out);
      {b, a} = ac_out;
    end
    `OP_ASL, `OP_DEC, `OP_INC, `OP_LSR, `OP_ROL, `OP_ROR, `OP_TRB,
    `OP_TSB: begin
      if (am_d == `AM_A) begin
        ac_out = ac;
        do_alu(op_d, opw_d, ac, m, ac_out);
        {b, a} = ac_out;
      end
    end
    `OP_BIT: begin
      if (opw_d) begin
        if (am_d != `AM_IMM)
          {`p_n, `p_v} = m[15:14];
        `p_z = (ac & m) == 0;
      end
      else begin
        if (am_d != `AM_IMM)
          {`p_n, `p_v} = m[7:6];
        `p_z = (ac[7:0] & m[7:0]) == 0;
      end
    end
    `OP_BRK, `OP_COP: begin
      `p_d = 0;
      `p_i = 1;
    end
    `OP_CLC: begin
      `p_c = 0;
    end
    `OP_CLD: begin
      `p_d = 0;
    end
    `OP_CLI: begin
      `p_i = 0;
    end
    `OP_CLV: begin
      `p_v = 0;
    end
    `OP_CMP: begin
      do_alu(op_d, opw_d, ac, m, m);
    end
    `OP_CPX: begin
      do_alu(`OP_CMP, opw_d, x, m, m);
    end
    `OP_CPY: begin
      do_alu(`OP_CMP, opw_d, y, m, m);
    end
    `OP_DEX: begin
      do_alu(`OP_DEC, opw_d, 16'bx, x, x);
    end
    `OP_DEY: begin
      do_alu(`OP_DEC, opw_d, 16'bx, y, y);
    end
    `OP_INX: begin
      do_alu(`OP_INC, opw_d, 16'bx, x, x);
    end
    `OP_INY: begin
      do_alu(`OP_INC, opw_d, 16'bx, y, y);
    end
    `OP_LDA: begin
      ac_out = ac;
      do_alu(`OP_LDA, opw_d, 16'bx, m, ac_out);
      {b, a} = ac_out;
    end
    `OP_LDX: begin
      do_alu(`OP_LDA, opw_d, 16'bx, m, x);
    end
    `OP_LDY: begin
      do_alu(`OP_LDA, opw_d, 16'bx, m, y);
    end
    `OP_PLA, `OP_PLB, `OP_PLD, `OP_PLX, `OP_PLY: begin
      `p_z = (opw_d ? m : m[7:0]) == 0;
      `p_n = opw_d ? m[15] : m[7];
    end
    `OP_PLP: ;                  // just so we can catch p_x changing
    `OP_SEC: begin
      `p_c = 1'b1;
    end
    `OP_SED: begin
      `p_d = 1'b1;
    end
    `OP_REP: begin
      if (e)
        m[5:4] = 2'b00;
      p = p & ~m[7:0];
    end
    `OP_RTI: begin
      p = m[7:0];
      if (e)
        p[5:4] = 2'b11;
    end
    `OP_SEP: begin
      p = p | m[7:0];
    end
    `OP_SEI: begin
      `p_i = 1'b1;
    end
    `OP_TAX: begin
      x[7:0] = a;
      if (idxw_d)
        x[15:8] = b;
      `p_z = (x & idxm_d) == 0;
      `p_n = |(x & idxs_d);
    end
    `OP_TAY: begin
      y[7:0] = a;
      if (idxw_d)
        y[15:8] = b;
      `p_z = (y & idxm_d) == 0;
      `p_n = |(y & idxs_d);
    end
    `OP_TCD: begin
      d = ac;
      `p_z = ac == 0;
      `p_n = ac[15];
    end
    `OP_TDC: begin
      {b, a} = d;
      `p_z = d == 0;
      `p_n = d[15];
    end
    `OP_TCS: begin
      if (e)
        s[7:0] = a;
      else
        s = ac;
    end
    `OP_TSC: begin
      {b, a} = s;
      `p_z = s == 0;
      `p_n = s[15];
    end
    `OP_TSX: begin
      x[7:0] = s[7:0];
      if (idxw_d)
        x[15:8] = s[15:8];
      `p_z = (x & idxm_d) == 0;
      `p_n = |(x & idxs_d);
    end
    `OP_TXA: begin
      a = x[7:0];
      if (amw)
        b = x[15:8];
      `p_z = (x & amm) == 0;
      `p_n = |(x & ams);
    end
    `OP_TXS: begin
      s[7:0] = x[7:0];
      if (~e)
        s[15:8] = x[15:8];
    end
    `OP_TXY: begin
      y[7:0] = x[7:0];
      if (idxw_d)
        y[15:8] = x[15:8];
      `p_z = (y & idxm_d) == 0;
      `p_n = |(y & idxs_d);
    end
    `OP_TYA: begin
      a = y[7:0];
      if (amw)
        b = y[15:8];
      `p_z = (y & amm) == 0;
      `p_n = |(y & ams);
    end
    `OP_TYX: begin
      x[7:0] = y[7:0];
      if (idxw_d)
        x[15:8] = y[15:8];
      `p_z = (x & idxm_d) == 0;
      `p_n = |(x & idxs_d);
    end
    `OP_XBA: begin
      {b, a} = {a, b};
      `p_z = a == 0;
      `p_n = a[7];
    end
    `OP_XCE: begin
      {`p_c, e} = {e, `p_c};
    end
    default: $finish(1); // shouldn't get here
  endcase

  fixup_regs();
end

task automatic do_alu
  (
    input [6:0]  op,
    input        w,             // 0=8-bit, 1=16-bit
    input [15:0] a,
    input [15:0] b,
    inout [15:0] o            // inout to preserve [15:8] on 8-bit ALU
   );
reg [15:0] rm = {(w * 8'hff), 8'hff};
reg [15:0] rs = rm ^ (rm >> 1);
  case (op)
    `OP_ADC: begin
      tmp[4:0] = a[3:0] + b[3:0] + `p_c;
      hc[0] = tmp[4] | (`p_d && tmp[3:0] >= 4'd10);
      tmp[8:4] = a[7:4] + b[7:4] + hc[0];
      hc[1] = tmp[8] | (`p_d && tmp[7:4] >= 4'd10);
      if (amw) begin
        tmp[12:8] = a[11:8] + b[11:8] + hc[1];
        hc[2] = tmp[12] | (`p_d && tmp[11:8] >= 4'd10);
        tmp[16:12] = a[15:12] + b[15:12] + hc[2];
        `p_c = tmp[16] | (`p_d && tmp[15:12] >= 4'd10);
        `p_v = a[15] == b[15] && tmp[15] != a[15];
        if (`p_d && hc[0])
          tmp[3:0] = tmp[3:0] + 4'd6;
        if (`p_d && hc[1])
          tmp[7:4] = tmp[7:4] + 4'd6;
        if (`p_d && hc[2])
          tmp[11:8] = tmp[11:8] + 4'd6;
        if (`p_d && `p_c)
          tmp[15:12] = tmp[15:12] + 4'd6;
        `p_z = tmp[15:0] == 0;
        `p_n = tmp[15];
        o = tmp[15:0];
      end
      else begin
        `p_c = hc[1];
        `p_v = a[7] == b[7] && tmp[7] != a[7];
        if (`p_d && hc[0])
          tmp[3:0] = tmp[3:0] + 4'd6;
        if (`p_d && `p_c)
          tmp[7:4] = tmp[7:4] + 4'd6;
        `p_z = tmp[7:0] == 0;
        `p_n = tmp[7];
        o[7:0] = tmp[7:0];
      end
    end
    `OP_AND: begin
      o[7:0] = a[7:0] & b[7:0];
      if (w)
        o[15:8] = a[15:8] & b[15:8];
      `p_z = (o & rm) == 0;
      `p_n = |(o & rs);
    end
    `OP_ASL: begin
      if (w)
        {`p_c, o[15:0]} = {b[15:0], 1'b0};
      else
        {`p_c, o[7:0]} = {b[7:0], 1'b0};
      `p_z = (o & rm) == 0;
      `p_n = |(o & rs);
    end
    `OP_CMP: begin
      b = ~b;
      if (w)
        {`p_c, o} = a + b + 17'b1;
      else
        {`p_c, o[7:0]} = a[7:0] + b[7:0] + 9'b1;
      `p_z = (o & rm) == 0;
      `p_n = |(o & rs);
    end
    `OP_DEC: begin
      if (w)
        o[15:0] = b[15:0] - 1;
      else
        o[7:0] = b[7:0] - 1;
      `p_z = (o & rm) == 0;
      `p_n = |(o & rs);
    end
    `OP_EOR: begin
      o[7:0] = a[7:0] ^ b[7:0];
      if (w)
        o[15:8] = a[15:8] ^ b[15:8];
      `p_z = (o & rm) == 0;
      `p_n = |(o & rs);
    end
    `OP_INC: begin
      if (w)
        o[15:0] = b[15:0] + 1;
      else
        o[7:0] = b[7:0] + 1;
      `p_z = (o & rm) == 0;
      `p_n = |(o & rs);
    end
    `OP_LDA: begin
      o[7:0] = b[7:0];
      if (w)
        o[15:8] = b[15:8];
      `p_z = (o & rm) == 0;
      `p_n = |(o & rs);
    end
    `OP_LSR: begin
      if (w)
        {o[15:0], `p_c} = {1'b0, b[15:0]};
      else
        {o[7:0], `p_c} = {1'b0, b[7:0]};
      `p_z = (o & rm) == 0;
      `p_n = |(o & rs);
    end
    `OP_ORA: begin
      o[7:0] = a[7:0] | b[7:0];
      if (w)
        o[15:8] = a[15:8] | b[15:8];
      `p_z = (o & rm) == 0;
      `p_n = |(o & rs);
    end
    `OP_ROL: begin
      if (w)
        {`p_c, o[15:0]} = {b[15:0], `p_c};
      else
        {`p_c, o[7:0]} = {b[7:0], `p_c};
      `p_z = (o & rm) == 0;
      `p_n = |(o & rs);
    end
    `OP_ROR: begin
      if (w)
        {o[15:0], `p_c} = {`p_c, b[15:0]};
      else
        {o[7:0], `p_c} = {`p_c, b[7:0]};
      `p_z = (o & rm) == 0;
      `p_n = |(o & rs);
    end
    `OP_SBC: begin
      b = ~b;
      tmp[4:0] = a[3:0] + b[3:0] + `p_c;
      hc[0] = tmp[4];
      tmp[8:4] = a[7:4] + b[7:4] + hc[0];
      hc[1] = tmp[8];
      if (amw) begin
        tmp[12:8] = a[11:8] + b[11:8] + hc[1];
        hc[2] = tmp[12];
        tmp[16:12] = a[15:12] + b[15:12] + hc[2];
        `p_c = tmp[16];
        `p_v = a[15] == b[15] && tmp[15] != a[15];
        if (`p_d && ~hc[0])
          tmp[3:0] = tmp[3:0] - 4'd6;
        if (`p_d && ~hc[1])
          tmp[7:4] = tmp[7:4] - 4'd6;
        if (`p_d && ~hc[2])
          tmp[11:8] = tmp[11:8] - 4'd6;
        if (`p_d && ~`p_c)
          tmp[15:12] = tmp[15:12] - 4'd6;
        `p_z = tmp[15:0] == 0;
        `p_n = tmp[15];
        o = tmp[15:0];
      end
      else begin
        `p_c = hc[1];
        `p_v = a[7] == b[7] && tmp[7] != a[7];
        if (`p_d && ~hc[0])
          tmp[3:0] = tmp[3:0] - 4'd6;
        if (`p_d && ~`p_c)
          tmp[7:4] = tmp[7:4] - 4'd6;
        `p_z = tmp[7:0] == 0;
        `p_n = tmp[7];
        o[7:0] = tmp[7:0];
      end
    end
    `OP_TRB: begin
      o[7:0] = ~a[7:0] & b[7:0];
      `p_z = ~|(a[7:0] & b[7:0]);
      if (w) begin
        o[15:8] = ~a[15:8] & b[15:8];
        `p_z &= ~|(a[15:8] & b[15:8]);
      end
    end
    `OP_TSB: begin
      o[7:0] = a[7:0] | b[7:0];
      `p_z = ~|(a[7:0] & b[7:0]);
      if (w) begin
        o[15:8] = a[15:8] | b[15:8];
        `p_z &= ~|(a[15:8] & b[15:8]);
      end
    end
    default: begin
      $display("do_alu: op='h%x", op);
      $finish(1); // shouldn't get here
    end
  endcase
endtask

reg print_ins_enable;
initial print_ins_enable = 0;
task print_ins();
  if (print_ins_enable)
    $display("[sim] %t: ($%02X%04X) %s", $time, pbr, pc, opcode_st[ir]);
endtask

endmodule
