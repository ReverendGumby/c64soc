#!/usr/bin/env python3

import sys
import json
import re


def bc_fromstr(s):
    s = re.sub(r'[-w]', '0', s)
    s = re.sub(r'[dpvremxl]', '1', s)
    return s


def header(f, fnin):
    f.write(f"// Automatically generated from {fnin}\n")
    f.write("\n")


def test(f, t):
    j = json.dumps(t)
    name = re.sub(r' ', '_', t['name'])
    name = 'op_test_' + name
    initial = t['initial']
    final = t['final']
    cycles = t['cycles']

    f.write(f"// {j}\n")
    f.write(f"task {name}();\n")
    f.write("reg [7:0] v;\n")
    f.write(f"  $display(\"{name}\");\n")
    f.write("\n")
    f.write(f"  regs.pc = 16'h{initial['pc']:04X};\n")
    f.write(f"  regs.s = 16'h{initial['s']:04X};\n")
    f.write(f"  regs.p = 8'h{initial['p']:02X};\n")
    f.write(f"  regs.a = 16'h{initial['a']:04X};\n")
    f.write(f"  regs.x = 16'h{initial['x']:04X};\n")
    f.write(f"  regs.y = 16'h{initial['y']:04X};\n")
    f.write(f"  regs.dbr = 8'h{initial['dbr']:02X};\n")
    f.write(f"  regs.d = 16'h{initial['d']:04X};\n")
    f.write(f"  regs.pbr = 8'h{initial['pbr']:02X};\n")
    f.write(f"  regs.e = 1'b{initial['e']};\n")
    for ram in initial['ram']:
        (a, v) = ram
        f.write(f"  ram.set(24'h{a:06X}, 8'h{v:02X});\n")
    f.write("  test_start();\n")
    f.write("\n")
    for cycle in cycles:
        (a, d, bc) = cycle
        bc = bc_fromstr(bc)
        if a is None: # WAI, STP
            pass # TODO
        else:
            f.write(f"  assert(a == 24'h{a:06X});\n")
            if d is None:
                f.write(f"  assert(d === 8'hzz);\n")
            else:
                f.write(f"  assert(d == 8'h{d:02X});\n")
            f.write(f"  assert(bc == 8'b{bc:s});\n")
        fn = 'test_end' if cycle is cycles[-1] else 'test_next_cycle'
        f.write(f"  {fn}();\n")
        f.write("\n")
    f.write(f"  assert(regs.pc == 16'h{final['pc']:04X});\n")
    f.write(f"  assert(regs.s == 16'h{final['s']:04X});\n")
    f.write(f"  assert(regs.p == 8'h{final['p']:02X});\n")
    f.write(f"  assert(regs.a == 16'h{final['a']:04X});\n")
    f.write(f"  assert(regs.x == 16'h{final['x']:04X});\n")
    f.write(f"  assert(regs.y == 16'h{final['y']:04X});\n")
    f.write(f"  assert(regs.dbr == 8'h{final['dbr']:02X});\n")
    f.write(f"  assert(regs.d == 16'h{final['d']:04X});\n")
    f.write(f"  assert(regs.pbr == 8'h{final['pbr']:02X});\n")
    f.write(f"  assert(regs.e == 1'b{final['e']});\n")
    for ram in final['ram']:
        (a, v) = ram
        f.write(f"  ram.get(24'h{a:06X}, v);\n")
        f.write(f"  assert(v == 8'h{v:02X});\n")
    f.write("endtask\n")
    f.write("\n")


def footer(f, t, num):
    name = re.sub(r' ', '_', t['name'])
    name = 'op_test_' + name
    op = name[0:12]
    f.write(f"task {op}();\n")
    for t in range(1, num + 1):
        f.write(f"  {op}_{t}();\n")
    f.write("endtask\n")


(fnin, fnout, max_tests) = sys.argv[1:4]
with open(fnin, 'r') as f:
    tests = json.load(f)
max_tests = int(max_tests)
num_tests = len(tests)
if num_tests > max_tests:
    num_tests = max_tests
    tests = tests[:num_tests]
with open(fnout, 'w') as f:
    header(f, fnin)
    for t in tests:
        test(f, t)
    footer(f, tests[0], num_tests)
