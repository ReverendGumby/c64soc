`timescale 1us / 1ns

module rom
  #(parameter AW, 
    parameter DW)
  (
   input           CLK,
   input           nCE,
   input [AW-1:0]  A,
   output [DW-1:0] DO
   );

reg [DW-1:0] rom [0:((1 << AW) - 1)];
reg [DW-1:0] dor;

task read_from_file(input integer fin);
integer code;
  code = $fread(rom, fin);
endtask

task get(input reg [AW-1:0] a, output reg [DW-1:0] v);
  v = rom[a];
endtask

always @(posedge CLK)
  dor <= rom[A];

assign DO = ~nCE ? dor : {DW{1'bz}};

endmodule
