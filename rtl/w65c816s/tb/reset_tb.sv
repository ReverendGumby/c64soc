`timescale 1us / 1ns

module reset_tb();

initial begin
  $timeformat(-6, 0, " us", 1);

  $dumpfile("reset_tb.vcd");
  $dumpvars();
end

reg       clk, res;
reg       cp1_posedge, cp1_negedge, cp2_posedge, cp2_negedge;
reg [7:0] din;

wire [23:0] a;
wire [7:0]  dout;
wire        e, mlb, m, x, rw, vpb, vda, vpa;

w65c816s dut
  (
   .RESB(~res),
   .CLK(clk),
   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .HOLD(1'b0),
   .IRQB(1'b1),
   .NMIB(1'b1),
   .ABORTB(1'b1),
   .E(e),
   .MLB(mlb),
   .M(m),
   .X(x),
   .A(a),
   .D_I(din),
   .D_O(dout),
   .RWB(rw),
   .RDYIN(1'b1),
   .WAITN(),
   .VPB(vpb),
   .VDA(vda),
   .VPA(vpa)
   );

initial begin
  cp1_posedge = 0;
  cp1_negedge = 0;
  cp2_posedge = 0;
  cp2_negedge = 0;
  res = 1;
  clk = 1;
end

initial forever begin :clkgen
  #0.125 clk = ~clk;
end

initial forever begin :cpgen
  @(posedge clk) cp1_posedge <= 0;
  @(posedge clk) cp1_negedge <= 1;
  @(posedge clk) cp1_negedge <= 0; cp2_posedge <= 1;
  @(posedge clk) cp2_posedge <= 0;
  @(posedge clk) ;
  @(posedge clk) ;
  @(posedge clk) cp2_negedge <= 1;
  @(posedge clk) cp2_negedge <= 0; cp1_posedge <= 1;
end

wire cp2 = dut.cp2;

task next_cycle();
  while (~cp1_posedge) @(posedge clk) ;
  @(posedge clk) ;
endtask

always @* begin
  casez (a)
    24'h00fffc:   din = 8'h00;
    24'h00fffd:   din = 8'h80;
    24'h008zzz:   din = 8'hea;
    default: din = 8'hx;
  endcase
end

task assert_reset();
  assert(dut.d == 0);
  assert(dut.dbr == 0);
  assert(dut.pbr == 0);
  assert(dut.s[15:8] == 8'h01);
  assert(dut.x[15:8] == 8'h00);
  assert(dut.y[15:8] == 8'h00);
  assert(dut.p[5] == 'b1);
  assert(dut.p[4] == 'b1);
  assert(e == 'b1);
  assert(m == 'b1);
  assert(x == 'b1);
  assert(rw == 'b1);
  assert(vda == 'b0);
  assert(vpa == 'b0);
  assert(vpb == 'b1);
endtask

initial #0 begin
  // Reset must be asserted for a minimum of 2 cycles.
  next_cycle();
  next_cycle();

  // Reset initialization
  assert_reset();

  repeat (2) begin
    // De-assert reset
    res = 0;
    next_cycle();

    // Pretending to fetch BRK
    assert({vda, vpa} == 'b11);
    next_cycle();

    assert({vda, vpa} == 'b00);
    next_cycle();

    // 3x stack accesses, but no write
    assert(a[23:8] == 16'h0001);
    assert(rw == 'b1);
    assert({vda, vpa} == 'b10);
    next_cycle();

    assert(a[23:8] == 16'h0001);
    assert(rw == 'b1);
    assert({vda, vpa} == 'b10);
    next_cycle();

    assert(a[23:8] == 16'h0001);
    assert(rw == 'b1);
    assert({vda, vpa} == 'b10);
    next_cycle();

    // Fetch reset vector
    assert(a == 24'h00FFFC);
    assert(rw == 'b1);
    assert({vda, vpa} == 'b10);
    assert(vpb == 'b0);
    next_cycle();

    assert(a == 24'h00FFFD);
    assert({vda, vpa} == 'b10);
    assert(vpb == 'b0);
    next_cycle();

    // Fetching opcode at reset vector
    assert(a == 24'h008000);
    assert({vda, vpa} == 'b11);
    next_cycle();

    // BRK has retired
    assert(a == 24'h008001);
    assert(dut.p[3] == 'b0);
    assert(dut.p[2] == 'b1);
    next_cycle();

    // Assert reset
    res = 1;
    next_cycle();

    repeat (5) begin
      assert_reset();
      next_cycle();
    end
  end

  $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s reset_tb -o reset_tb.vvp ../w65c816s.v reset_tb.sv && ./reset_tb.vvp"
// End:
