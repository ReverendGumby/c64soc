`timescale 1us / 1ns

module wai_tb();

// References: http://forum.6502.org/viewtopic.php?f=4&t=7732

initial begin
  $timeformat(-6, 0, " us", 1);

  $dumpfile("wai_tb.vcd");
  $dumpvars();
end

reg       clk, res;
reg       cp1_posedge, cp1_negedge, cp2_posedge, cp2_negedge;
reg [7:0] din;
reg       irq, nmi;
reg       hold;
reg       disable_irq;

wire [23:0] a;
wire [7:0]  dout;
wire        e, mlb, m, x, rw, waitn, vpb, vda, vpa;

w65c816s dut
  (
   .RESB(~res),
   .CLK(clk),
   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .HOLD(1'b0),
   .IRQB(~irq),
   .NMIB(~nmi),
   .ABORTB(1'b1),
   .E(e),
   .MLB(mlb),
   .M(m),
   .X(x),
   .A(a),
   .D_I(din),
   .D_O(dout),
   .RWB(rw),
   .RDYIN(1'b1),
   .WAITN(waitn),
   .VPB(vpb),
   .VDA(vda),
   .VPA(vpa),
   .MON_SEL(6'd0),
   .MON_WS(1'b0),
   .MON_DOUT(),
   .MON_DIN()
   );

initial begin
  clk = 1;
  cp1_posedge = 0;
  cp1_negedge = 0;
  cp2_posedge = 0;
  cp2_negedge = 0;
  res = 0;
  irq = 0;
  nmi = 0;
  hold = 1;
  disable_irq = 1'b0;
end

initial forever begin :clkgen
  #0.125 clk = ~clk;
end

initial forever begin :cpgen
  @(posedge clk) cp1_posedge <= 0;
  @(posedge clk) cp1_negedge <= 1;
  @(posedge clk) cp1_negedge <= 0; cp2_posedge <= 1;
  @(posedge clk) cp2_posedge <= 0;
  @(posedge clk) ;
  @(posedge clk) ;
  @(posedge clk) cp2_negedge <= 1;
  @(posedge clk) cp2_negedge <= 0; cp1_posedge <= 1;
end

always @* dut.stall = hold;

wire cp2 = dut.cp2;

task next_ph2();
  while (~cp2_posedge) @(posedge clk) ;
  @(posedge clk) ;
endtask

task next_cycle();
  while (~cp2_negedge) @(posedge clk) ;
  @(posedge clk) ;
  @(posedge clk) ;
endtask

always @* begin
  casez (a)
    24'h00ffea:   din = 8'h00; // NMI vector
    24'h00ffeb:   din = 8'h90;
    24'h00ffee:   din = 8'h00; // IRQ vector
    24'h00ffef:   din = 8'h90;
    24'h008000:   din = 8'hcb; // WAI
    24'h00800z:   din = 8'hea; // NOP
    24'h009zzz:   din = 8'hea; // NOP
    default: din = 8'hx;
  endcase
end

task test_start;
  dut.ir = 8'hxx;

  while (~dut.stalled)
    next_ph2();

  // Setup DUT to fetch WAI opcode.
  dut.pc = 16'h8000;
  dut.s = 16'h01ff;
  {dut.b, dut.a} = 0;
  dut.p = 0;
  dut.p[2] = disable_irq;
  dut.x = 0;
  dut.y = 0;
  dut.dbr = 0;
  dut.pbr = 8'h00;
  dut.e = 1'b0;

  dut.aor = dut.pc;
  dut.bar = dut.pbr;

  force dut.t_reset = 1'b1;
  next_ph2();

  release dut.t_reset;
  hold <= 0;

  while (dut.stalled)
    next_cycle();
endtask

task test_cy1;
  // Cycle 1: Fetch WAI
  assert(a == 24'h008000);
  assert(rw == 'b1);
  assert(waitn == 'b1);
  assert({vda, vpa} == 'b11);
  next_cycle();
endtask

task test_cy2_waitn_falls;
  // Cycle 2, phase 1: Idle bus, A advances
  assert(a == 24'h008001);
  assert(rw == 'b1);
  assert(waitn == 'b1);
  assert({vda, vpa} == 'b00);
  next_ph2();

  // Cycle 2, phase 2: WAITN falls
  assert(waitn == 1'b0);
  next_cycle();
endtask

task test_cy2_stalled;
  // Cycle 2: WAITN is low, nothing happens
  assert(a == 24'h008001);
  assert(rw == 'b1);
  assert(waitn == 'b0);
  assert({vda, vpa} == 'b00);
  next_cycle();
endtask

task test_cy2_active;
  // Cycle 2: WAITN is high, cycle completes
  assert(a == 24'h008001);
  assert(rw == 'b1);
  assert(waitn == 'b1);
  assert({vda, vpa} == 'b00);
  next_cycle();
endtask

task test_cy3;
  // Cycle 3: identical to cycle 2
  test_cy2_active();
endtask

task test_int_taken;
  // Pretending to fetch BRK
  assert(a == 24'h008001);
  assert(rw == 'b1);
  assert(waitn == 'b1);
  assert({vda, vpa} == 'b11);
  next_cycle();

  // Idle cycle (native mode only)
  assert(a == 24'h008001);
  assert(rw == 'b1);
  assert(waitn == 'b1);
  assert({vda, vpa} == 'b00);
  next_cycle();

  // Push PBR to stack
  assert(a == 24'h0001ff);
  assert(dout == 8'h00);
  assert(rw == 'b0);
  assert({vda, vpa} == 'b10);
  next_cycle();
endtask

task test_int_not_taken;
  // Cycle 1: Fetch next instruction
  assert(a == 24'h008001);
  assert(rw == 'b1);
  assert(waitn == 'b1);
  assert({vda, vpa} == 'b11);
  next_cycle();
endtask

task test_end;
  // Wait for next SYNC
  hold <= 1;
  while (!({vda, vpa} == 'b11))
    next_ph2();
  next_ph2();
endtask

initial #0 begin
  //dut.print_ins_enable = 1;
  dut.resp = 0;
  dut.respd = 0;

  $display("WAI stalls, IRQ resumes");
  disable_irq = 1'b0;
  test_start();
  test_cy1();
  test_cy2_waitn_falls();
  test_cy2_stalled();
  test_cy2_stalled();
  irq <= 'b1;               // Assert IRQ to terminate WAI
  next_cycle();
  test_cy2_active();
  test_cy3();
  test_int_taken();
  irq <= 'b0;
  test_end();

  $display("WAI stalls, NMI resumes");
  disable_irq = 1'b0;
  test_start();
  test_cy1();
  test_cy2_waitn_falls();
  test_cy2_stalled();
  test_cy2_stalled();
  nmi <= 'b1;               // Assert NMI to terminate WAI
  next_cycle();
  nmi <= 'b0;
  test_cy2_active();
  test_cy3();
  test_int_taken();
  test_end();

  $display("WAI with pending IRQ");
  disable_irq = 1'b0;
  test_start();
  irq <= 'b1;               // Assert IRQ to prevent WAI from stalling
  test_cy1();
  test_cy2_active();
  test_cy3();
  test_int_taken();
  irq <= 'b0;
  test_end();

  $display("WAI with pending masked IRQ");
  disable_irq = 1'b1;
  test_start();
  irq <= 'b1;               // Assert IRQ to prevent WAI from stalling
  test_cy1();
  test_cy2_active();
  test_cy3();
  test_int_not_taken();
  irq <= 'b0;
  test_end();

  $display("WAI with pending NMI");
  disable_irq = 1'b0;
  test_start();
  nmi <= 'b1;               // Assert NMI to prevent WAI from stalling
  test_cy1();
  nmi <= 'b0;
  test_cy2_active();
  test_cy3();
  test_int_taken();
  test_end();

  $display("Done!");
  next_cycle();
  $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s wai_tb -o wai_tb.vvp ../w65c816s.v wai_tb.sv && ./wai_tb.vvp"
// End:
