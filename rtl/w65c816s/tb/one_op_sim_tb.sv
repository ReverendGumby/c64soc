`timescale 1us / 1ns

module one_op_sim_tb();

initial begin
  $timeformat(-6, 0, " us", 1);

  //$dumpfile("one_op_sim_tb.vcd");
  //$dumpvars();
end

reg       clk, res, irq;
reg       cp2_posedge, cp2_negedge;
reg       hold, be, be_next;
reg       short_blk;
reg       do_wai, do_stp;

wire [23:0] a;
wire [7:0]  d, do_ram, do_dut;
wire        e, mlb, m, x, rw, vpb, vda, vpa;
wire        nce, nwe, noe;

w65c816s_sim dut
  (
   .RESB(~res),
   .CLK(clk),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .IRQB(~irq),
   .NMIB(1'b1),
   .ABORTB(1'b1),
   .E(e),
   .MLB(mlb),
   .M(m),
   .X(x),
   .A(a),
   .D_I(do_ram),
   .D_O(do_dut),
   .RWB(rw),
   .RDYIN(1'b1),
   .WAITN(),
   .VPB(vpb),
   .VDA(vda),
   .VPA(vpa),
   .HOLD(hold)
   );

ram #(24, 8) ram(.CLK(clk), .A(a), .DI(do_dut), .DO(do_ram), .nCE(nce),
                 .nWE(nwe), .nOE(noe));

initial begin
  clk = 1;
  cp2_posedge = 0;
  cp2_negedge = 0;
  res = 0;
  irq = 0;
  be_next = 0;
  be = 0;
  hold = 1;
  short_blk = 0;
  do_wai = 0;
  do_stp = 0;
end

initial forever begin :clkgen
  #0.125 clk = ~clk;
end

initial forever begin :cpgen
  @(posedge clk) ;
  @(posedge clk) ;
  @(posedge clk) cp2_posedge <= 1;
  @(posedge clk) cp2_posedge <= 0;
  @(posedge clk) ;
  @(posedge clk) ;
  @(posedge clk) cp2_negedge <= 1;
  @(posedge clk) cp2_negedge <= 0;
end

assign d = rw ? do_ram : do_dut;
assign nce = ~be | ~(vda | vpa);
assign nwe = ~be | rw;
assign noe = ~be | ~rw;

wire [7:0] bc = { vda, vpa, ~vpb, rw, e, m, x, ~mlb };

struct packed {
reg [15:0] pc, s, a, d, x, y;
reg [7:0]  p, dbr, pbr;
reg        e;
} regs;

task test_start();
  res <= 'b0;
  irq <= 'b0;

  if (short_blk) begin
    short_blk = 0;
    {dut.b, dut.a} = 0;
    while (dut.ac != 16'hffff)
      test_next_cycle();
    test_next_cycle();
  end

  test_next_cycle();

  dut.pc = regs.pc;
  dut.s = regs.s;
  {dut.b, dut.a} = regs.a;
  dut.d = regs.d;
  dut.p = regs.p;
  dut.x = regs.x;
  dut.y = regs.y;
  dut.dbr = regs.dbr;
  dut.pbr = regs.pbr;
  dut.e = regs.e;

  dut.fixup_regs();

  dut.ab = dut.pc;

  hold <= 0;
  be_next <= 1;

  test_next_cycle();
endtask

task test_next_cycle();
  while (~cp2_negedge)
    @(posedge clk) ;
  be <= be_next;
  while (~cp2_posedge)
    @(posedge clk) ;
  @(posedge clk) ;
  @(posedge clk) ;
endtask

task test_end();
  if (dut.ir == 8'hcb) begin
    do_wai = 1;
    // Doing a WAI
  end
  if (dut.ir == 8'hdb) begin
    do_stp = 1;
    // Doing a STP
  end

  if (~do_stp) begin
    be_next <= 0;
    hold <= 1;
    test_next_cycle();

    if ((vda & vpa) == 1'b0 && (dut.ir & 8'hef) == 8'h44) begin
      // MVN/MVP tests stop at 100 cycles, regardless of whether the
      // move has completed and advanced to the next instrution.
      short_blk = 1;
    end
    else if (do_wai) begin
      // Recover from WAI
      irq <= 'b1;
      test_next_cycle();
      test_next_cycle();
      irq <= 'b0;
    end
    else
      assert(vda & vpa == 1'b1);    // SYNC indicates last opcode ended

    test_next_cycle();
  end

  regs.pc = dut.pc;
  regs.s = dut.s;
  regs.a = {dut.b, dut.a};
  regs.d = dut.d;
  regs.p = dut.p;
  regs.x = dut.x;
  regs.y = dut.y;
  regs.dbr = dut.dbr;
  regs.pbr = dut.pbr;
  regs.e = dut.e;

  if (do_stp) begin
    // Recover from STP
    do_stp = 0;
    res <= 'b1;
    test_next_cycle();
    test_next_cycle();
    res <= 'b0;
    repeat (7)
      test_next_cycle();
    hold <= 1;
    test_next_cycle();          // let BRK retire and touch flags
  end
endtask

`include "FILENAME"

initial #0 begin
  dut.print_ins_enable = 1;
  `OP_TEST();
  $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s one_op_sim_tb -o one_op_sim_tb.vvp w65c816s_sim.sv ram.sv one_op_sim_tb.sv && ./one_op_sim_tb.vvp"
// End:
