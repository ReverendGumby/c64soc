// WDC W65C816S synthesizable emulation
//
// Credit to:
// . https://snes.nesdev.org/wiki/65C816
// . http://www.westerndesigncenter.com/wdc/documentation/w65c816s.pdf
// . http://www.6502.org/tutorials/65c816opcodes.html
//
// TODO:
// - Emulation mode (E=0)
// - Opcodes: STP
// - Ports: ABORTB
//
// Core ports:
//   input         RESB         // reset (active-low)
//   input         CLK          // main clock
//   input         CP1_POSEDGE  // clock phase 1
//   input         CP1_NEGEDGE
//   input         CP2_POSEDGE  // clock phase 2
//   input         CP2_NEGEDGE
//   input         HOLD         // halt all activity (except monitor)
//   input         IRQB         // interrupt request (active-low)
//   input         NMIB         // non-maskable interrupt (active-low)
//   input         ABORTB       // abort input (active-low)
//   output        E            // emulation status
//   output        MLB          // memory lock (active-low)
//   output        M            // memory select status
//   output        X            // index select status
//   output        RWB          // read / not write
//   input         RDYIN        // ready, input
//   output        WAITN        // active-low "WAI and no interrupt"
//   output        VPB          // vector pull (active-low)
//   output        VDA          // valid data address
//   output        VPA          // valid program address
//
//   output [7:0]  bar          // bank address (address bits [23:16])
//   output [15:0] aor          // address bus bits [15:0]
//   input [7:0]   db           // data bus
//   output [7:0]  dor          // data output register
//
//   input [5:0]   MON_SEL      // debug monitor
//   input         MON_WS
//   output [31:0] MON_DOUT
//   input [31:0]  MON_DIN

`include "w65c816s_defs.vh"

// emulation mode status bits
`define p_c p[0]    // Carry
`define p_z p[1]    // Zero
`define p_i p[2]    // IRQ Disable
`define p_d p[3]    // Decimal
`define p_b p[4]    // Break
`define p_v p[6]    // oVerflow
`define p_n p[7]    // Negative

// native mode status bits
`define p_x p[4]    // indeX register select: 1 = 8-bit, 0 = 16-bit
`define p_m p[5]    // Memory/accumulator select: 1 = 8-bit, 0 = 16-bit

wire        cp1p, cp2p, cp2n;
reg         cp2;
reg         rdy1, rdy2;
reg         sync;
wire        sync_next;
wire        t_reset;
reg         t_reset_pend;
reg [7:0]   t;
reg [6:0]   am_t;
reg         am_start, am_nostart, am_done, am_done_d, am_done_d2;
reg         op_t0;
reg [7:1]   op_tp;
wire [7:0]  op_t;
reg         op_start, op_done;
reg         stall, stalled, resuming;
wire        stalling;
reg [3:0]   ts;
reg         resp, nmip, irqp;
reg         resg, nmig;
wire        nmie;
reg         respd, nmil;
reg         intg_pre2, intg_pre, intg;
wire        copg;
wire        intr_pre, intr;
reg [2:0]   intr_id;
reg         vec1;
reg [4:0]   intr_vec;
reg         rwb_ext, mlb_ext, vpb_ext, vda_ext, vpa_ext;

reg [15:0]  pc, d, x, y, s, z;
wire [7:0]  pcl, pch, dl, dh, xl, xh, yl, yh, sl, sh;
reg         ac_z, dl_z;
reg [7:0]   a, b, p, dbr, pbr;
wire [15:0] ac;
reg         e;
reg [15:0]  upc, npc, aor, idl, idld;
reg [7:0]   bar, dor;
wire [7:0]  db;
reg [7:0]   ir;
wire        rw_int;
reg         waitn0;

reg [15:0]  idb, sb;
reg [23:0]  ab, abp;

reg [15:0]  aao;
reg [7:0]   aab;

reg [15:0]  aip, bip, ibip;
reg [15:0]  ai, bi;
reg         addc;
reg [16:0]  co, cod;
reg         cco, cvo, ccod, cvod;
reg         add16, accoh, daa, das;
reg         ors, ands, eors, asls, rols, lsrs, rors;

reg         bra_p;
wire        bra_p_match, bra_skip;

wire [7:0]  pd;
reg         pd_am_nostart;
reg         ins_onebyte;
wire        ins_done, clear_ir;
wire        brk_done;

reg         am_abs, am_a, am_absx, am_absy, am_labs, am_labsx, am_aind,
            am_aindx, am_dp, am_sr, am_dpx, am_dpy, am_ind, am_lind, am_srindy,
            am_xiind, am_indyi, am_lindyi, am_impl, am_rel, am_lrel, am_blk,
            am_imm;

reg         op_a, op_x, op_y, op_s, op_p, op_d, op_dbr, op_pbr;
reg         op_ld, op_st, op_asl, op_rol, op_lsr, op_ror, op_dec, op_inc,
            op_and, op_or, op_eor, op_sbc, op_adc, op_cmp, op_bit, op_tr_x,
            op_tr_y, op_tr_a, op_tr_s, op_tr_c, op_tcd, op_xba, op_trb, op_tsb,
            op_push, op_pull, op_pea, op_jmp, op_jsr, op_rts, op_rtl, op_rti,
            op_clx_sex, op_bra;
wire        op_store, op_rmw;
wire        op_math;
reg         op_16bit;
wire        op_load_store;
reg         op_load_store0, op_load_store1;
reg         op_brk_done, op_pc_inc, op_set_n, op_set_z,
            op_am_page_wrap, op_am_page_wrap_d, op_push_pea_pre,
            op_push_pea_hi, op_push_pea_lo, op_pull_rts_pre, op_pull_rts_go,
            op_pull_rts_last, op_pull_rts_post;

wire        cl_pc_inc, cl_ab_aor, cl_abb_ba;
reg         cl_pc_dec, cl_a_out, cl_abl_aor, cl_abh_aor;
reg         cl_waitn0;
reg         cl_t_skip, cl_store_dor;
reg         cl_abl_pcl, cl_abh_pch, cl_sbl_pcl, cl_sbh_pch,
            cl_abl_aorl, cl_abh_aorh;
reg         cl_db_idl, cl_zero_idlh, cl_db_idlh;
reg         cl_idb_ba, cl_bar_ba;
reg         cl_idb_a, cl_idb_b, cl_idb_ac, cl_idb_x, cl_idb_y, cl_idb_s,
            cl_idb_d, cl_idb_z, cl_idb_dbr, cl_idb_pbr;
reg         cl_idb_p, cl_cvo_v, cl_zero_v,
            cl_zero_d, cl_ir5_d, cl_ir5_i, cl_one_i, cl_cco_c, cl_ir5_c;
wire        cl_idb7_n, cl_idb15_n, cl_idblz_z, cl_idbz_z;
reg         cl_idb6_v, cl_idb14_v, cl_xchg_ce;
reg [4:0]   cl_idb_rs;
reg         cl_sb_s;
reg [4:0]   cl_sb_rs;
reg [4:0]   cl_abl_rs, cl_abh_rs, cl_abb_rs;
reg [7:0]   cl_zero_abh;
reg         cl_vec_abl;
reg         cl_one_vpa, cl_one_vda, cl_one_ml;
reg         cl_aa_inc, cl_aa_dec;
reg         cl_use_ai_rs;
reg [4:0]   cl_ai_rs, cl_ail_rs, cl_aih_rs, cl_bi_rs;
reg         cl_not_bi, cl_sign_extend_bil, cl_one_addc, cl_alu_16bit, cl_ccoh,
            cl_daa_das;
reg         cl_ors, cl_ands, cl_eors, cl_asls, cl_rols, cl_lsrs, cl_rors;


//////////////////////////////////////////////////////////////////////
// Clocking

assign cp1p = !HOLD & CP1_POSEDGE;
//assign cp1n = CP1_NEGEDGE;
assign cp2p = !HOLD & CP2_POSEDGE;
assign cp2n = !HOLD & CP2_NEGEDGE;

initial begin
  cp2 = 0;
  t_reset_pend = 'b0;
  t = 'b0;
  am_t = 'b0;
  rdy1 = 'b1;
  waitn0 = 0;
  sync = 0;
  stall = 'b0;
  stalled = 'b0;
  resuming = 'b0;
end

always @(posedge CLK) begin
  cp2 <= (cp2 | cp2p) & ~cp2n;

  if ((MON_SEL == 6'h20) & MON_WS)
    cp2 <= MON_DIN[0];
end

// WAI affects WAITN on CP2 rising edge
always @(posedge CLK) begin
  if (cp2p)
    waitn0 <= cl_waitn0 & ~resp;

  if ((MON_SEL == 6'h21) & MON_WS)
    waitn0 <= MON_DIN[11];
end

// Instruction sync
always @(posedge CLK) begin
  if (cp2n & rdy2) begin
    if (~(stalling & sync))
      sync <= sync_next & RESB;
  end

  if ((MON_SEL == 6'h11) & MON_WS)
    sync <= MON_DIN[3];
end

// Timing should reset to where stall resumes: just after sync deasserts.
assign t_reset = (MON_SEL == 6'h10) & MON_WS & |MON_DIN[7:4];

always @(posedge CLK) begin
  if (stalled & t_reset)
    t_reset_pend <= 'b1;
  else if (cp2n & resuming)
    t_reset_pend <= 'b0;
end

// Instruction cycle counter
always @(posedge CLK) begin
  if (~stalled & cp2n & rdy2) begin
    if (ins_done)
      t <= 'b1;
    else begin
      t <= t << (cl_t_skip ? 2 : 1);
    end
  end

  if (t_reset)
    t <= 'b1;
  else if ((MON_SEL == 6'h24) & MON_WS)
    t <= MON_DIN[7:0];
end

// Address mode cycle counter (instruction phase 1)
always @(posedge CLK) begin
  if (cp2n & rdy2) begin
    if (resp | (stalling | stalled)) begin
      am_t <= 'b0;
      am_done_d <= 'b0;
      am_done_d2 <= 'b0;
      am_nostart <= 'b0;
    end
    else begin
      am_done_d <= am_done & ~t[0];
      am_done_d2 <= am_done_d & ~am_start & ~t[0];
      am_nostart <= pd_am_nostart & t[0];

      if (am_start)
        am_t <= 'b1;
      else if (am_done_d)
        am_t <= 'b0;
      else
        am_t <= am_t << (cl_t_skip ? 2 : 1);
    end
  end

  if (t_reset)
    am_t <= 'b0;
  else if ((MON_SEL == 6'h20) & MON_WS) begin
    am_nostart <= MON_DIN[2];
    am_done_d <= MON_DIN[3];
    am_done_d2 <= MON_DIN[4];
  end
  else if ((MON_SEL == 6'h24) & MON_WS)
    am_t <= MON_DIN[14:8];
end

// Operation cycle counter (instruction phase 2)
always @* begin
  op_t0 = op_start;

  if (t_reset)
    op_t0 = 'b0;
end

reg unused;
always @(posedge CLK) begin
  if (~stalled & cp2n & rdy2) begin
    if (t[0] | resp)
      op_tp <= 'b0;
    else
      {op_tp, unused} <= op_t << (cl_t_skip ? 2 : 1);
  end

  if (t_reset)
    op_tp <= 'b0;
  else if ((MON_SEL == 6'h24) & MON_WS)
    op_tp <= MON_DIN[22:16];
end

assign op_t = {op_tp, op_t0};

// Timing control
assign stalling = stall & ~stalled & (~|t | sync);

always @(posedge CLK) begin
  if (cp2n) begin
    if (stalling) begin
      stalled <= 1'b1;
    end
    else if (~stall & stalled & ~resuming) begin
      stalled <= 1'b0;
      resuming <= 1'b1;
    end
    else if (resuming) begin
      resuming <= 1'b0;
    end
  end

  if ((MON_SEL == 6'h10) & MON_WS)
    stall <= MON_DIN[0];
end

// Encode t for monitor
always @* begin
  case (t)
    8'b0:               ts = 4'd0;
    8'b1 << 0:          ts = 4'd1;
    8'b1 << 1:          ts = 4'd2;
    8'b1 << 2:          ts = 4'd3;
    8'b1 << 3:          ts = 4'd4;
    8'b1 << 4:          ts = 4'd5;
    8'b1 << 5:          ts = 4'd6;
    8'b1 << 6:          ts = 4'd7;
    8'b1 << 7:          ts = 4'd8;
    default:            ts = 4'd15;
  endcase
end


//////////////////////////////////////////////////////////////////////
// Reset and interrupts

initial begin
  {resg, nmig} = 0;
  {irqp, nmip} = 0;
  resp = 1;
  respd = 1;
  intg_pre2 = 0;
  intg = 0;
end

always @(posedge CLK) begin
  if (cp2n) begin
    resp <= ~RESB;
    irqp <= ~IRQB;
  end
  if (cp1p) begin
    nmip <= ~NMIB;
  end
  if (cp2p) begin
    respd <= resp;
  end

  if ((MON_SEL == 6'h20) & MON_WS) begin
    resp <= MON_DIN[5];
    nmip <= MON_DIN[6];
    irqp <= MON_DIN[7];
    respd <= MON_DIN[11];
  end
end

always @(posedge CLK) begin
  if (cp2n) begin
    if (resp)
      resg <= respd;
    else if (rdy1 && brk_done)
      resg <= 1'b0;
  end

  if ((MON_SEL == 6'h20) & MON_WS)
    resg <= MON_DIN[8];
end

always @(posedge CLK) begin
  if (cp2n) begin
    if (nmip && nmig)
      nmil <= 1'b1;
    else if (!nmip)
      nmil <= 1'b0;
  end

  if ((MON_SEL == 6'h20) & MON_WS)
    nmil <= MON_DIN[12];
end

assign nmie = nmip & ~nmil;

always @(posedge CLK) begin
  if (cp2n) begin
    if (nmie)
      nmig <= 1'b1;
    else if (rdy1 && brk_done)
      nmig <= 1'b0;
  end

  if ((MON_SEL == 6'h20) & MON_WS)
    nmig <= MON_DIN[9];
end

always @(posedge CLK) begin
  if (cp1p & rdy1) begin
    if (ins_done && (nmig || (irqp && !`p_i)))
      intg_pre2 <= 1'b1;
    else
      intg_pre2 <= intg;
  end

  if ((MON_SEL == 6'h20) & MON_WS)
    intg_pre2 <= MON_DIN[13];
end

always @* begin // must be evaluated during cp2
  if (rdy1 && brk_done)
    intg_pre = 1'b0;
  else
    intg_pre = intg_pre2;
end

always @(posedge CLK) begin
  if (cp2n & rdy2)
    intg <= intg_pre;

  if ((MON_SEL == 6'h20) & MON_WS)
    intg <= MON_DIN[14];
end

assign copg = (ir == `COP);

assign intr_pre = resg || intg_pre;
assign intr = resg || intg;

always @(posedge CLK) begin
  if (cp2n & rdy2) begin
    vec1 <= cl_vec_abl & ~vec1;
  end

  if ((MON_SEL == 6'h20) & MON_WS)
    vec1 <= MON_DIN[15];
end

assign brk_done = vec1;

always @(posedge CLK) begin
  if (cp2n & rdy2 & sync) begin
    if (resg)
      intr_id <= `INTR_ID_RES;
    else if (nmig)
      intr_id <= `INTR_ID_NMI;
    else if (intg)              // irqp
      intr_id <= `INTR_ID_IRQ;
    else
      intr_id <= `INTR_ID_SW;
  end

  if ((MON_SEL == 6'h20) & MON_WS)
    intr_id <= MON_DIN[18:16];
end

always @* begin
  intr_vec[4] = e;
  case (intr_id)
    `INTR_ID_RES:
      intr_vec[3:0] = 4'hc;
    `INTR_ID_NMI:
      intr_vec[3:0] = 4'ha;
    `INTR_ID_IRQ:
      intr_vec[3:0] = 4'he;
    `INTR_ID_SW: begin
      if (copg)
        intr_vec[3:0] = 4'h4;
      else if (~e)
        intr_vec[3:0] = 4'h6;
      else // BRK, emulation mode
        intr_vec[3:0] = 4'he;
    end
    default: intr_vec[3:0] = 4'hx;
  endcase
  intr_vec[0] = vec1;
end


//////////////////////////////////////////////////////////////////////
// External interface

always @* rdy2 = RDYIN & WAITN; // use with cp2n

always @(posedge CLK) begin
  if (cp2n)
    rdy1 <= rdy2;               // use with cp1*, cp2p

  if ((MON_SEL == 6'h21) & MON_WS)
    rdy1 <= MON_DIN[10];
end

// Memory-related signals aligned with aor/bar/dor
always @(posedge CLK) begin
  if (cp1p & rdy1) begin
    rwb_ext <= rw_int | resg;

    mlb_ext <= ~cl_one_ml;

    vpb_ext <= ~cl_vec_abl;
    vda_ext <= sync | cl_one_vda;
    vpa_ext <= sync | cl_one_vpa;
  end

  if ((MON_SEL == 6'h20) & MON_WS) begin
    rwb_ext <= MON_DIN[19];
    mlb_ext <= MON_DIN[20];
    vpb_ext <= MON_DIN[21];
    vda_ext <= MON_DIN[22];
    vpa_ext <= MON_DIN[23];
  end
end

assign RWB = rwb_ext;
assign MLB = mlb_ext;
assign VPB = vpb_ext;
assign VDA = vda_ext;
assign VPA = vpa_ext;

assign M = `p_m;
assign X = `p_x;
assign E = e;

assign WAITN = ~(waitn0 & ~((nmie | nmig) | irqp)) | resp;


//////////////////////////////////////////////////////////////////////
// Registers

// pc: program ("P") counter
always @(posedge CLK) begin
  if (cp2p & rdy1 & ~resp)
    pc <= npc;

  if ((MON_SEL == 6'h01) & MON_WS)
    pc <= MON_DIN[15:0];
end

assign pcl = pc[7:0];
assign pch = pc[15:8];

// "updated" pc (change part or all)
always @(posedge CLK) begin
  if (cp1p & rdy1) begin
    upc <= pc;

    if (cl_abl_pcl)
      upc[7:0] <= ab[7:0];
    if (cl_abh_pch)
      upc[15:8] <= ab[15:8];

    if (cl_sbl_pcl)
      upc[7:0] <= sb[7:0];
    if (cl_sbh_pch)
      upc[15:8] <= sb[15:8];
  end

  if ((MON_SEL == 6'h25) & MON_WS)
    upc <= MON_DIN[15:0];
end

// "next" pc (increment / decrement)
always @* begin
  npc = upc;

  if (~(stalling | stalled)) begin
    if (cl_pc_inc)
      npc = upc + 1;
    else if (cl_pc_dec)
      npc = upc - 1;
  end
end

// aor: address bus output register
// register latches on cp1p
always @(posedge CLK) begin
  if (cp1p & rdy1) begin
    if (cl_abl_aor)
      aor[7:0] <= ab[7:0];
    else if (cl_abh_aor)
      aor[15:8] <= ab[15:8];
    else if (cl_ab_aor)
      aor <= ab[15:0];
  end

  if ((MON_SEL == 6'h08) & MON_WS)
    aor[15:0] <= MON_DIN[23:8];
end

// bar: bank address register (i.e., aor[23:16])
// register latches on cp1p
always @(posedge CLK) begin
  if (cp1p & rdy1) begin
    if (cl_idb_ba)
      bar <= idb[7:0];
    else if (cl_bar_ba)
      ;
    else if (cl_abb_ba)
      bar <= ab[23:16];
  end

  if ((MON_SEL == 6'h08) & MON_WS)
    bar[7:0] <= MON_DIN[31:24];
end

// dor: data output register
// register latches on cp1p
always @(posedge CLK) begin
  if (cp1p & rdy1) begin
    dor <= sb[7:0];
  end

  if ((MON_SEL == 6'h09) & MON_WS)
    dor <= MON_DIN[7:0];
end

// idl: input data transparent latch
// latch open during cp2
always @* begin
  idl = idld;
  if (cp2) begin
    if (cl_db_idl)
      idl[7:0] = db;

    if (cl_zero_idlh)
      idl[15:8] = 8'b0;
    if (cl_db_idlh)
      idl[15:8] = db;
  end
end

always @(posedge CLK) begin
  if (!HOLD)
    idld <= idl;

  if ((MON_SEL == 6'h26) & MON_WS)
    idld <= MON_DIN[15:0];
end

// a: accumulator, lower 8 bits
always @(posedge CLK) begin
  if (cp2n & rdy2) begin
    if (cl_idb_a | cl_idb_ac)
      a <= idb[7:0];
  end

  if ((MON_SEL == 6'h02) & MON_WS)
    a <= MON_DIN[23:16];
end

// b: accumulator, upper 8 bits
always @(posedge CLK) begin
  if (cp2n & rdy2) begin
    if (cl_idb_ac)
      b <= idb[15:8];
    if (cl_idb_b)
      b <= idb[7:0];
  end

  if ((MON_SEL == 6'h02) & MON_WS)
    b <= MON_DIN[31:24];
end

assign ac = {b, a};

always @(posedge CLK) begin
  if (cp2n & rdy2)
    ac_z <= ac == 16'd0;

  if ((MON_SEL == 6'h20) & MON_WS)
    ac_z <= MON_DIN[25];
end

// d: direct register
always @(posedge CLK) begin
  if (!HOLD & resp)
    d <= 16'b0;
  else if (cp2n & rdy2) begin
    if (cl_idb_d)
      d <= idb;
  end

  if ((MON_SEL == 6'h03) & MON_WS)
    d <= MON_DIN[15:0];
end

assign dl = d[7:0];
assign dh = d[15:8];

always @(posedge CLK) begin
  if (cp2n & rdy2)
    dl_z <= dl == 8'd0;

  if ((MON_SEL == 6'h20) & MON_WS)
    dl_z <= MON_DIN[26];
end

// x: index register
always @(posedge CLK) begin
  if (cp2n & rdy2) begin
    if (cl_idb_x)
      x <= idb;
  end

  if ((cp2n | cp2p) & rdy2) begin // cp2p for same cycle as `p_x / e change
    if (`p_x | e)      // e for same cycle as e change
      x[15:8] <= 8'h00;
  end

  if ((MON_SEL == 6'h03) & MON_WS)
    x <= MON_DIN[31:16];
end

assign xl = x[7:0];
assign xh = x[15:8];

// y: index register
always @(posedge CLK) begin
  if (cp2n & rdy2) begin
    if (cl_idb_y)
      y <= idb;
  end

  if ((cp2n | cp2p) & rdy2) begin // cp2p for same cycle as `p_x / e change
    if (`p_x | e)      // e for same cycle as e change
      y[15:8] <= 8'h00;
  end

  if ((MON_SEL == 6'h04) & MON_WS)
    y <= MON_DIN[15:0];
end

assign yl = y[7:0];
assign yh = y[15:8];

// s: stack pointer
always @(posedge CLK) begin
  if (cp2n & rdy2) begin
    if (cl_idb_s)
      s <= idb;
    if (cl_sb_s)
      s <= sb;
  end

  if ((cp2n | cp2p) & rdy2) begin // cp2p for same cycle as e change
    if (e)
      s[15:8] <= 8'h01;
  end

  if ((MON_SEL == 6'h02) & MON_WS)
    s <= MON_DIN[15:0];
end

assign sl = s[7:0];
assign sh = s[15:8];

// dbr: data bank register
always @(posedge CLK) begin
  if (!HOLD & resp)
    dbr <= 8'b0;
  else if (cp2n & rdy2) begin
    if (cl_idb_dbr)
      dbr <= idb[7:0];
  end

  if ((MON_SEL == 6'h04) & MON_WS)
    dbr <= MON_DIN[23:16];
end

// pbr: program bank register
always @(posedge CLK) begin
  if (!HOLD & resp)
    pbr <= 8'b0;
  else if (cp2n & rdy2) begin
    if (cl_idb_pbr)
      pbr <= idb[7:0];
  end

  if ((MON_SEL == 6'h04) & MON_WS)
    pbr <= MON_DIN[31:24];
end

// z: internal temporary register
always @(posedge CLK) begin
  if (cp2n & rdy2) begin
    if (cl_idb_z)
      z <= idb;
  end

  if ((MON_SEL == 6'h09) & MON_WS)
    z <= MON_DIN[23:8];
end

// p: processor status flags
always @(posedge CLK) begin
  if (cp2n & rdy2) begin
    if (cl_idb_p)
      p <= idb[7:0];

    if (cl_idb7_n)
      `p_n <= idb[7];
    if (cl_idb15_n)
      `p_n <= idb[15];

    if (cl_cvo_v)
      `p_v <= cvo;
    if (cl_zero_v)
      `p_v <= 0;
    if (cl_idb6_v)
      `p_v <= idb[6];
    if (cl_idb14_v)
      `p_v <= idb[14];

    if (cl_zero_d)
      `p_d <= 1'b0;
    if (cl_ir5_d)
      `p_d <= ir[5];

    if (cl_ir5_i)
      `p_i <= ir[5];
    if (cl_one_i)
      `p_i <= 1'b1;

    if (cl_idblz_z)
      `p_z <= ~|idb[7:0];
    if (cl_idbz_z)
      `p_z <= ~|idb;

    if (cl_cco_c)
      `p_c <= cco;
    if (cl_ir5_c)
      `p_c <= ir[5];
    if (cl_xchg_ce)
      `p_c <= e;
  end

  if ((cp2n | cp2p) & rdy2) begin // cp2p for same cycle as e change
    if (e)
      {`p_x, `p_m} <= 2'b11;
  end

  if ((MON_SEL == 6'h01) & MON_WS)
    p <= MON_DIN[23:16];
end

// e: emulation mode (vs. native mode)
always @(posedge CLK) begin
  if (!HOLD & resp)
    e <= 'b1;
  else if (cp2n & rdy2) begin
    if (cl_xchg_ce)
      e <= `p_c;
  end

  if ((MON_SEL == 6'h01) & MON_WS)
    e <= MON_DIN[24];
end


//////////////////////////////////////////////////////////////////////
// Register selection

localparam [4:0] rs_0   = 5'h00; // all zeros
localparam [4:0] rs_ac  = 5'h01;
localparam [4:0] rs_x   = 5'h02;
localparam [4:0] rs_y   = 5'h03;
localparam [4:0] rs_p   = 5'h04;
localparam [4:0] rs_s   = 5'h05;
localparam [4:0] rs_d   = 5'h06;
localparam [4:0] rs_pc  = 5'h07;
localparam [4:0] rs_aao = 5'h08;
localparam [4:0] rs_idl = 5'h09;
localparam [4:0] rs_aab = 5'h0a;
localparam [4:0] rs_co  = 5'h0b;
localparam [4:0] rs_aor = 5'h0c;
localparam [4:0] rs_dbr = 5'h0d;
localparam [4:0] rs_pbr = 5'h0e;
localparam [4:0] rs_1   = 5'h0f; // all ones
localparam [4:0] rs_z   = 5'h10;
localparam [4:0] rs_b   = 5'h11;
localparam [4:0] rs_xh  = 5'h12;
localparam [4:0] rs_yh  = 5'h13;
localparam [4:0] rs_e   = 5'h14;
localparam [4:0] rs_sh  = 5'h15;
localparam [4:0] rs_dh  = 5'h16;
localparam [4:0] rs_pch = 5'h17;
localparam [4:0] rs_aaoh= 5'h18;
localparam [4:0] rs_idlh= 5'h19;
localparam [4:0] rs_zh  = 5'h1a;
localparam [4:0] rs_coh = 5'h1b;
localparam [4:0] rs_aorh= 5'h1c;

// Synonyms for 8-bit selectors
localparam [4:0] rs_a   = rs_ac;
localparam [4:0] rs_xl  = rs_x;
localparam [4:0] rs_yl  = rs_y;
localparam [4:0] rs_dl  = rs_d;
localparam [4:0] rs_zl  = rs_z;
localparam [4:0] rs_pcl = rs_pc;

// Synonyms for 16-bit selectors
localparam [4:0] rs_c   = rs_ac;

`define regmux8(rs, q) \
  case (rs) \
    rs_0:   q = 0; \
    rs_ac:  q = a; \
    rs_x:   q = xl; \
    rs_y:   q = yl; \
    rs_p:   q = p; \
    rs_s:   q = sl; \
    rs_d:   q = dl; \
    rs_pc:  q = pcl; \
    rs_aao: q = aao[7:0]; \
    rs_idl: q = idld[7:0]; \
    rs_aab: q = aab; \
    rs_co:  q = co[7:0]; \
    rs_aor: q = aor[7:0]; \
    rs_dbr: q = dbr; \
    rs_pbr: q = pbr; \
    rs_1:   q = 8'hff; \
    rs_z:   q = z[7:0]; \
    rs_b:   q = b; \
    rs_xh:  q = xh; \
    rs_yh:  q = yh; \
    rs_e:   q = {7'b0, e}; \
    rs_sh:  q = sh; \
    rs_dh:  q = dh; \
    rs_pch: q = pch; \
    rs_aaoh:q = aao[15:8]; \
    rs_idlh:q = idld[15:8]; \
    rs_zh:  q = z[15:8]; \
    rs_coh: q = co[15:8]; \
    rs_aorh:q = aor[15:8]; \
    default:q = 8'hxx; \
  endcase

`define regmux16(rs, q) \
  case (rs) \
    rs_0:   q = 0; \
    rs_ac:  q = ac; \
    rs_x:   q = x; \
    rs_y:   q = y; \
    rs_p:   q = {8'b0, p}; \
    rs_s:   q = s; \
    rs_d:   q = d; \
    rs_pc:  q = pc; \
    rs_aao: q = aao; \
    rs_idl: q = idld; \
    rs_aab: q = {8'b0, aab}; \
    rs_co:  q = co[15:0]; \
    rs_aor: q = aor; \
    rs_dbr: q = {8'b0, dbr}; \
    rs_pbr: q = {8'b0, pbr}; \
    rs_1:   q = 16'hffff; \
    rs_z:   q = z; \
    rs_b:   q = {8'b0, b}; \
    rs_xh:  q = {8'b0, xh}; \
    rs_yh:  q = {8'b0, yh}; \
    rs_e:   q = {8'b0, 7'b0, e}; \
    rs_sh:  q = {8'b0, sh}; \
    rs_dh:  q = {8'b0, dh}; \
    rs_pch: q = {8'b0, pch}; \
    rs_aaoh:q = {8'b0, aao[15:8]}; \
    rs_idlh:q = {8'b0, idld[15:8]}; \
    rs_zh:  q = {8'b0, z[15:8]}; \
    rs_coh: q = {8'b0, co[15:8]}; \
    rs_aorh:q = {8'b0, aor[15:8]}; \
    default:q = 16'hxxxx; \
  endcase


//////////////////////////////////////////////////////////////////////
// Internal buses

// idb: internal data bus
always @*
  `regmux16 (cl_idb_rs, idb);

// sb: special bus
always @*
  `regmux16 (cl_sb_rs, sb);

// ab: (internal) address bus
always @*
  `regmux8 (cl_abl_rs, abp[7:0]);
always @*
  `regmux8 (cl_abh_rs, abp[15:8]);
always @*
  `regmux8 (cl_abb_rs, abp[23:16]);

always @* begin
  ab = abp;
  ab[15:8] = ab[15:8] & ~cl_zero_abh;
  if (cl_vec_abl)
    ab[4:0] = intr_vec;
end


//////////////////////////////////////////////////////////////////////
// 24-bit address adder
// Used to increment/decrement effective address

always @(posedge CLK) begin
  if (cp2p & rdy1) begin
    if (cl_aa_inc)
      {aab, aao} <= {bar, aor} + 24'd1;
    else if (cl_aa_dec)
      {aab, aao} <= {bar, aor} - 24'd1;
    else
      {aab, aao} <= {bar, aor};
  end

  if ((MON_SEL == 6'h24) & MON_WS)
    aab <= MON_DIN[31:24];
  else if ((MON_SEL == 6'h26) & MON_WS)
    aao <= MON_DIN[31:16];
end

//////////////////////////////////////////////////////////////////////
// ALU

// Inputs

always @* begin
  if (cl_use_ai_rs)
    `regmux16 (cl_ai_rs, aip)
  else begin
    `regmux8 (cl_ail_rs, aip[7:0])
    `regmux8 (cl_aih_rs, aip[15:8])
  end
end

always @*
  `regmux16 (cl_bi_rs, bip);

always @* begin
  ibip = bip;
  if (cl_not_bi)
    ibip = ~ibip;
  if (cl_sign_extend_bil)
    ibip[15:8] = {8{ibip[7]}};
end

always @(posedge CLK) begin
  if (cp2n & rdy2) begin
    ai <= aip;
    bi <= ibip;
    addc <= cl_one_addc;
    add16 <= cl_alu_16bit;
    accoh <= cl_ccoh;
    daa <= cl_daa_das & op_adc;
    das <= cl_daa_das & op_sbc;
    ors <= cl_ors;
    ands <= cl_ands;
    eors <= cl_eors;
    asls <= cl_asls;
    rols <= cl_rols;
    lsrs <= cl_lsrs;
    rors <= cl_rors;
  end

  if ((MON_SEL == 6'h20) & MON_WS) begin
    addc <= MON_DIN[27];
    add16 <= MON_DIN[28];
    accoh <= MON_DIN[29];
    daa <= MON_DIN[30];
    das <= MON_DIN[31];
  end
  else if ((MON_SEL == 6'h21) & MON_WS) begin
    ors <= MON_DIN[0];
    ands <= MON_DIN[1];
    eors <= MON_DIN[2];
    asls <= MON_DIN[3];
    rols <= MON_DIN[4];
    lsrs <= MON_DIN[5];
    rors <= MON_DIN[6];
  end
  else if ((MON_SEL == 6'h27) & MON_WS) begin
    ai <= MON_DIN[15:0];
    bi <= MON_DIN[31:16];
  end
end

// Maths
task alu_add(inout [16:0] o, inout c, inout v);
reg [4:0] hhsum, hlsum, lhsum, llsum;
  begin
    llsum = ai[3:0] + bi[3:0] + {3'b0, addc};
    llsum[4] = llsum[4] | (daa & (llsum[3:0] >= 4'd10));
    lhsum = ai[7:4] + bi[7:4] + {3'b0, llsum[4]};
    lhsum[4] = lhsum[4] | (daa & (lhsum[3:0] >= 4'd10));
    hlsum = ai[11:8] + bi[11:8] + {3'b0, lhsum[4]};
    hlsum[4] = hlsum[4] | (daa & (hlsum[3:0] >= 4'd10));
    hhsum = ai[15:12] + bi[15:12] + {3'b0, hlsum[4]};
    hhsum[4] = hhsum[4] | (daa & (hhsum[3:0] >= 4'd10));

    if (~accoh)
      c = add16 ? hhsum[4] : lhsum[4];
    v = add16 ?
        (ai[15] == bi[15]) & (ai[15] != hhsum[3]) :
        (ai[7] == bi[7]) & (ai[7] != lhsum[3]);

    if (daa) begin
      if (llsum[4])
        llsum[3:0] = llsum[3:0] + 4'd6;
      if (lhsum[4])
        lhsum[3:0] = lhsum[3:0] + 4'd6;
      if (hlsum[4])
        hlsum[3:0] = hlsum[3:0] + 4'd6;
      if (hhsum[4])
        hhsum[3:0] = hhsum[3:0] + 4'd6;
    end
    else if (das) begin
      if (~llsum[4])
        llsum[3:0] = llsum[3:0] - 4'd6;
      if (~lhsum[4])
        lhsum[3:0] = lhsum[3:0] - 4'd6;
      if (~hlsum[4])
        hlsum[3:0] = hlsum[3:0] - 4'd6;
      if (~hhsum[4])
        hhsum[3:0] = hhsum[3:0] - 4'd6;
    end

    o[16:0] = {8'b0, lhsum[4:0], llsum[3:0]};
    if (add16)
      o[16:8] = {hhsum[4:0], hlsum[3:0]};
  end
endtask

always @* begin
  co = cod;
  cco = ccod;
  cvo = cvod;

  if (ors)
    co[15:0] = ai | bi;
  else if (ands)
    co[15:0] = ai & bi;
  else if (eors)
    co[15:0] = ai ^ bi;
  else if (asls | rols) begin
    if (add16)
      {cco, co[15:0]} = {ai, addc & rols};
    else
      {cco, co[7:0]} = {ai[7:0], addc & rols};
  end
  else if (lsrs | rors) begin
    if (add16)
      {co[15:0], cco} = {addc & rors, ai};
    else
      {co[7:0], cco} = {addc & rors, ai[7:0]};
  end
  else
    alu_add(co, cco, cvo);
end

always @(posedge CLK) begin
  if (cp1p & rdy1) begin
    cod <= co;
    ccod <= cco;
    cvod <= cvo;
  end

  if ((MON_SEL == 6'h21) & MON_WS) begin
    ccod <= MON_DIN[7];
    cvod <= MON_DIN[8];
  end
  else if ((MON_SEL == 6'h28) & MON_WS)
    cod <= MON_DIN[16:0];
end

//////////////////////////////////////////////////////////////////////
// Branch decision

always @* begin
  case (ir)
    `BPL_REL, `BMI_REL:     bra_p = `p_n;
    `BVC_REL, `BVS_REL:     bra_p = `p_v;
    `BCC_REL, `BCS_REL:     bra_p = `p_c;
    `BNE_REL, `BEQ_REL:     bra_p = `p_z;
    `BRA_REL:               bra_p = 1'b1;
    default: bra_p = 1'bx;
  endcase
end

assign bra_p_match = (ir == `BRA_REL) ? 1'b1 : ir[5];
assign bra_skip = bra_p ^ bra_p_match;


//////////////////////////////////////////////////////////////////////
// Instruction decode and control logic

assign ins_done = op_done | resp;
assign sync_next = ins_done | (t_reset_pend & ~resuming);

// pd: predecode register
assign clear_ir = intr_pre | resp;
assign pd = clear_ir ? `BRK : db;

always @* begin
  // pd_am_nostart for empty addressing modes, ie. that are done at
  // t[0]. Needs to be valid when pd holds opcode.
  pd_am_nostart = 0;

  if (sync) begin
    casez (pd)
      // am_impl
      8'bzzzz1011, 8'bzzzz1000, 8'b1zzz1010,
      `BRK, `COP, `RTI, `WDM, `RTS, `PHY, `PLY, `PEA, `PEI, `PER,
      // am_imm involving 8/16-bit accumulator
      8'bzzz01001,
      // am_imm involving 8/16-bit index
      `LDY_IMM, `LDX_IMM, `CPY_IMM, `CPX_IMM,
      // am_imm involving 8-bit value
      `REP_IMM, `SEP_IMM,
      // am_a
      `ASL_A, `INC_A, `ROL_A, `DEC_A, `LSR_A, `ROR_A,
      // am_rel, am_lrel
      8'bzzz10000, `BRA_REL, `BRL_LREL,
      // am_blk
      `MVP_BLK, `MVN_BLK,
      // These instructions interleave address mode phase with stack access.
      `JSR_AINDX, `JSL_LABS:
        pd_am_nostart = 'b1;
      default: ;
    endcase
  end
end

always @(posedge CLK) begin
  if (cp2n & rdy2) begin
    if (resp)
      ir <= 8'h00;
    else if (~stall & sync)
      ir <= pd;
  end

  if ((MON_SEL == 6'h08) & MON_WS)
    ir <= MON_DIN[7:0];
end

always @* begin
  am_abs = 0;             // Absolute = a
  am_a = 0;               // Accumulator = A
  am_absx = 0;            // Absolute Indexed with X = a,x
  am_absy = 0;            // Absolute Indexed with Y = a,y
  am_labs = 0;            // Absolute Long = al
  am_labsx = 0;           // Absolute Long Indexed with X = al,x
  am_aind = 0;            // Absolute Indirect = (a)
  am_aindx = 0;           // Absolute Indexed Indirect = (a,x)
  am_dp = 0;              // Direct = d
  am_sr = 0;              // Stack Relative = d,s
  am_dpx = 0;             // X-Indexed Direct Page = d,x
  am_dpy = 0;             // Y-Indexed Direct Page = d,y
  am_ind = 0;             // Direct Indirect = (d)
  am_lind = 0;            // Direct Indirect Long = [d]
  am_srindy = 0;          // Stack Relative Indirect Indexed = (d,s),y
  am_xiind = 0;           // Direct Indexed Indirect = (d,x)
  am_indyi = 0;           // Direct Indirect Indexed = (d),y
  am_lindyi = 0;          // Direct Indirect Long Indexed = [d],y
  am_impl = 0;            // Implied
  am_rel = 0;             // Program Counter Relative = r
  am_lrel = 0;            // Program Counter Relative Long = rl
  am_blk = 0;             // Block Move = xyc
  am_imm = 0;             // Immediate = #

  case (ir)
    `TSB_ABS, `ORA_ABS, `ASL_ABS, `TRB_ABS, `JSR_ABS, `BIT_ABS, `AND_ABS,
    `ROL_ABS, `JMP_ABS, `EOR_ABS, `LSR_ABS, `ADC_ABS, `ROR_ABS, `STY_ABS,
    `STA_ABS, `STX_ABS, `STZ_ABS, `LDY_ABS, `LDA_ABS, `LDX_ABS, `CPY_ABS,
    `CMP_ABS, `DEC_ABS, `CPX_ABS, `SBC_ABS, `INC_ABS:
      am_abs = 'b1;
    `ASL_A, `INC_A, `ROL_A, `DEC_A, `LSR_A, `ROR_A:
      am_a = 'b1;
    `ORA_ABSY, `AND_ABSY, `EOR_ABSY, `ADC_ABSY, `STA_ABSY, `LDA_ABSY, `LDX_ABSY,
    `CMP_ABSY, `SBC_ABSY:
      am_absy = 'b1;
    `ORA_ABSX, `ASL_ABSX, `BIT_ABSX, `AND_ABSX, `ROL_ABSX, `EOR_ABSX, `LSR_ABSX,
    `ADC_ABSX, `ROR_ABSX, `STA_ABSX, `STZ_ABSX, `LDY_ABSX, `LDA_ABSX, `CMP_ABSX,
    `DEC_ABSX, `SBC_ABSX, `INC_ABSX:
      am_absx = 'b1;
    `ORA_LABS, `JSL_LABS, `AND_LABS, `EOR_LABS, `JMP_LABS, `ADC_LABS,
    `STA_LABS, `LDA_LABS, `CMP_LABS, `SBC_LABS:
      am_labs = 'b1;
    `ORA_LABSX, `AND_LABSX, `EOR_LABSX, `ADC_LABSX, `STA_LABSX, `LDA_LABSX,
    `CMP_LABSX, `SBC_LABSX:
      am_labsx = 'b1;
    `JMP_AIND, `JML_AIND:
      am_aind = 'b1;
    `JMP_AINDX, `JSR_AINDX:
      am_aindx = 'b1;
    `TSB_DP, `ORA_DP, `ASL_DP, `TRB_DP, `BIT_DP, `AND_DP, `ROL_DP, `EOR_DP,
    `LSR_DP, `STZ_DP, `ADC_DP, `ROR_DP, `STY_DP, `STA_DP, `STX_DP, `LDY_DP,
    `LDA_DP, `LDX_DP, `CPY_DP, `CMP_DP, `DEC_DP, `CPX_DP, `SBC_DP, `INC_DP:
      am_dp = 'b1;
    `ORA_SR, `AND_SR, `EOR_SR, `ADC_SR, `STA_SR, `LDA_SR, `CMP_SR, `SBC_SR:
      am_sr = 'b1;
    `ORA_DPX, `ASL_DPX, `BIT_DPX, `AND_DPX, `ROL_DPX, `EOR_DPX, `LSR_DPX,
    `STZ_DPX, `ADC_DPX, `ROR_DPX, `STY_DPX, `STA_DPX, `LDY_DPX, `LDA_DPX,
    `CMP_DPX, `DEC_DPX, `SBC_DPX, `INC_DPX:
      am_dpx = 'b1;
    `STX_DPY, `LDX_DPY:
      am_dpy = 'b1;
    `ORA_IND, `AND_IND, `EOR_IND, `ADC_IND, `STA_IND, `LDA_IND, `CMP_IND,
    `SBC_IND:
      am_ind = 'b1;
    `ORA_LIND, `AND_LIND, `EOR_LIND, `ADC_LIND, `STA_LIND, `LDA_LIND, `CMP_LIND,
    `SBC_LIND:
      am_lind = 'b1;
    `ORA_SRINDY, `AND_SRINDY, `EOR_SRINDY, `ADC_SRINDY, `STA_SRINDY,
    `LDA_SRINDY, `CMP_SRINDY, `SBC_SRINDY:
      am_srindy = 'b1;
    `ORA_XIIND, `AND_XIIND, `EOR_XIIND, `ADC_XIIND, `STA_XIIND, `LDA_XIIND,
    `CMP_XIIND, `SBC_XIIND:
      am_xiind = 'b1;
    `ORA_INDYI, `AND_INDYI, `EOR_INDYI, `ADC_INDYI, `STA_INDYI, `LDA_INDYI,
    `CMP_INDYI, `SBC_INDYI:
      am_indyi = 'b1;
    `ORA_LINDYI, `AND_LINDYI, `EOR_LINDYI, `ADC_LINDYI, `STA_LINDYI,
    `LDA_LINDYI, `CMP_LINDYI, `SBC_LINDYI:
      am_lindyi = 'b1;
    `BPL_REL, `BMI_REL, `BVC_REL, `BVS_REL, `BRA_REL, `BCC_REL, `BCS_REL,
    `BNE_REL, `BEQ_REL:
      am_rel = 'b1;
    `BRL_LREL:
      am_lrel = 'b1;
    `MVP_BLK, `MVN_BLK:
      am_blk = 'b1;
    `ORA_IMM, `AND_IMM, `EOR_IMM, `ADC_IMM, `BIT_IMM, `LDY_IMM, `LDX_IMM,
    `LDA_IMM, `CPY_IMM, `REP_IMM, `CMP_IMM, `CPX_IMM, `SEP_IMM, `SBC_IMM:
      am_imm = 'b1;
    default:
      am_impl = 'b1;
  endcase
end

always @* begin
  // am_start aligns with cp1 of sync, so am_t[0] is on the first cycle
  // after the opcode.
  am_start = ~pd_am_nostart & t[0];
end

always @* ins_onebyte = am_impl | am_a;

always @* begin
  op_a = 0;
  op_x = 0;
  op_y = 0;
  op_s = 0;
  op_p = 0;
  op_d = 0;
  op_dbr = 0;
  op_pbr = 0;

  case (ir)
    `ORA_XIIND, `ORA_SR, `ORA_DP, `ORA_LIND, `ORA_IMM,
    `ORA_ABS, `ORA_LABS, `ORA_INDYI, `ORA_IND, `ORA_SRINDY,
    `ORA_DPX, `ORA_LINDYI, `ORA_ABSY, `ORA_ABSX, `ORA_LABSX,
    `AND_XIIND, `AND_SR, `AND_DP, `AND_LIND, `AND_IMM,
    `AND_ABS, `AND_LABS, `AND_INDYI, `AND_IND, `AND_SRINDY,
    `AND_DPX, `AND_LINDYI, `AND_ABSY, `AND_ABSX, `AND_LABSX,
    `EOR_XIIND, `EOR_SR, `EOR_DP, `EOR_LIND, `EOR_IMM,
    `EOR_ABS, `EOR_LABS, `EOR_INDYI, `EOR_IND, `EOR_SRINDY,
    `EOR_DPX, `EOR_LINDYI, `EOR_ABSY, `EOR_ABSX, `EOR_LABSX,
    `ADC_XIIND, `ADC_SR, `ADC_DP, `ADC_LIND, `ADC_IMM,
    `ADC_ABS, `ADC_LABS, `ADC_INDYI, `ADC_IND, `ADC_SRINDY,
    `ADC_DPX, `ADC_LINDYI, `ADC_ABSY, `ADC_ABSX, `ADC_LABSX,
    `STA_XIIND, `STA_SR, `STA_DP, `STA_LIND,
    `STA_ABS, `STA_LABS, `STA_INDYI, `STA_IND, `STA_SRINDY,
    `STA_DPX, `STA_LINDYI, `STA_ABSY, `STA_ABSX, `STA_LABSX,
    `LDA_XIIND, `LDA_SR, `LDA_DP, `LDA_LIND, `LDA_IMM,
    `LDA_ABS, `LDA_LABS, `LDA_INDYI, `LDA_IND, `LDA_SRINDY,
    `LDA_DPX, `LDA_LINDYI, `LDA_ABSY, `LDA_ABSX, `LDA_LABSX,
    `CMP_XIIND, `CMP_SR, `CMP_DP, `CMP_LIND, `CMP_IMM,
    `CMP_ABS, `CMP_LABS, `CMP_INDYI, `CMP_IND, `CMP_SRINDY,
    `CMP_DPX, `CMP_LINDYI, `CMP_ABSY, `CMP_ABSX, `CMP_LABSX,
    `SBC_XIIND, `SBC_SR, `SBC_DP, `SBC_LIND, `SBC_IMM,
    `SBC_ABS, `SBC_LABS, `SBC_INDYI, `SBC_IND, `SBC_SRINDY,
    `SBC_DPX, `SBC_LINDYI, `SBC_ABSY, `SBC_ABSX, `SBC_LABSX,
    `ASL_A, `INC_A, `ROL_A, `DEC_A, `LSR_A, `ROR_A,
    `PHA, `PLA, `TAX, `TAY, `TCS:                               op_a = 'b1;

    `STX_DP, `STX_ABS, `STX_DPY,
    `LDX_IMM, `LDX_DP, `LDX_ABS, `LDX_DPY, `LDX_ABSY,
    `CPX_IMM, `CPX_DP, `CPX_ABS,
    `DEX, `INX, `TXA, `TXS, `TXY, `PHX, `PLX:                   op_x = 'b1;

    `STY_DP, `STY_ABS, `STY_DPX,
    `LDY_IMM, `LDY_DP, `LDY_ABS, `LDY_DPX, `LDY_ABSX,
    `CPY_IMM, `CPY_DP, `CPY_ABS,
    `DEY, `INY, `TYA, `TYX,       `PHY, `PLY:                   op_y = 'b1;

    `TSX, `TSC:                                                 op_s = 'b1;
    `PHP, `PLP:                                                 op_p = 'b1;
    `TDC, `PHD, `PLD:                                           op_d = 'b1;
    `PHB, `PLB:                                                 op_dbr = 'b1;
    `PHK:                                                       op_pbr = 'b1;
    default: ;
  endcase
end

always @* begin
  op_tr_x = 0;
  op_tr_y = 0;
  op_tr_a = 0;
  op_tr_s = 0;
  op_tr_c = 0;
  op_tcd = 0;
  op_xba = 0;
  op_ld = 0;
  op_st = 0;
  op_asl = 0;
  op_rol = 0;
  op_lsr = 0;
  op_ror = 0;
  op_dec = 0;
  op_inc = 0;
  op_and = 0;
  op_or = 0;
  op_eor = 0;
  op_sbc = 0;
  op_adc = 0;
  op_cmp = 0;
  op_bit = 0;
  op_trb = 0;
  op_tsb = 0;
  op_push = 0;
  op_pull = 0;
  op_pea = 0;
  op_jmp = 0;
  op_jsr = 0;
  op_rts = 0;
  op_rtl = 0;
  op_rti = 0;
  op_clx_sex = 0;
  op_bra = 0;

  case (ir)
    `XCE: ;
    `TAX, `TSX, `TYX:                                           op_tr_x = 'b1;
    `TAY, `TXY:                                                 op_tr_y = 'b1;
    `TXA, `TYA:                                                 op_tr_a = 'b1;
    `TCS, `TXS:                                                 op_tr_s = 'b1;
    `TDC, `TSC:                                                 op_tr_c = 'b1;
    `TCD:                                                       op_tcd = 'b1;
    `XBA:                                                       op_xba = 'b1;
    `TSB_DP, `TSB_ABS:                                          op_tsb = 'b1;
    `TRB_DP, `TRB_ABS:                                          op_trb = 'b1;
    `PHP, `PHD, `PHA, `PHK, `PHY, `PHB, `PHX:                   op_push = 'b1;
    `PLP, `PLD, `PLA, `PLY, `PLB, `PLX:                         op_pull = 'b1;
    `PER, `PEI, `PEA:                                           op_pea = 'b1;
    `JMP_ABS, `JMP_LABS, `JMP_AIND, `JMP_AINDX, `JML_AIND:      op_jmp = 'b1;
    `JSR_ABS, `JSR_AINDX, `JSL_LABS:                            op_jsr = 'b1;
    `RTS:                                                       op_rts = 'b1;
    `RTL:                                                       op_rtl = 'b1;
    `RTI:                                                       op_rti = 'b1;
    `ASL_DP, `ASL_A, `ASL_ABS, `ASL_DPX, `ASL_ABSX:             op_asl = 'b1;
    `ORA_XIIND, `ORA_SR, `ORA_DP, `ORA_LIND, `ORA_IMM,
    `ORA_ABS, `ORA_LABS, `ORA_INDYI, `ORA_IND, `ORA_SRINDY,
    `ORA_DPX, `ORA_LINDYI, `ORA_ABSY, `ORA_ABSX, `ORA_LABSX:    op_or = 'b1;
    `BIT_DP, `BIT_ABS, `BIT_DPX, `BIT_ABSX, `BIT_IMM:           op_bit = 'b1;
    `ROL_DP, `ROL_A, `ROL_ABS, `ROL_DPX, `ROL_ABSX:             op_rol = 'b1;
    `AND_XIIND, `AND_SR, `AND_DP, `AND_LIND, `AND_IMM,
    `AND_ABS, `AND_LABS, `AND_INDYI, `AND_IND, `AND_SRINDY,
    `AND_DPX, `AND_LINDYI, `AND_ABSY, `AND_ABSX, `AND_LABSX:    op_and = 'b1;
    `LSR_DP, `LSR_A, `LSR_ABS, `LSR_DPX, `LSR_ABSX:             op_lsr = 'b1;
    `EOR_XIIND, `EOR_SR, `EOR_DP, `EOR_LIND, `EOR_IMM,
    `EOR_ABS, `EOR_LABS, `EOR_INDYI, `EOR_IND, `EOR_SRINDY,
    `EOR_DPX, `EOR_LINDYI, `EOR_ABSY, `EOR_ABSX, `EOR_LABSX:    op_eor = 'b1;
    `ROR_DP, `ROR_A, `ROR_ABS, `ROR_DPX, `ROR_ABSX:             op_ror = 'b1;
    `ADC_XIIND, `ADC_SR, `ADC_DP, `ADC_LIND, `ADC_IMM,
    `ADC_ABS, `ADC_LABS, `ADC_INDYI, `ADC_IND, `ADC_SRINDY,
    `ADC_DPX, `ADC_LINDYI, `ADC_ABSY, `ADC_ABSX, `ADC_LABSX:    op_adc = 'b1;
    `STA_XIIND, `STA_SR, `STA_DP, `STA_LIND,
    `STA_ABS, `STA_LABS, `STA_INDYI, `STA_IND, `STA_SRINDY,
    `STA_DPX, `STA_LINDYI, `STA_ABSY, `STA_ABSX, `STA_LABSX,
    `STX_DP, `STX_ABS, `STX_DPY,
    `STY_DP, `STY_ABS, `STY_DPX,
    `STZ_DP, `STZ_DPX, `STZ_ABS, `STZ_ABSX:                     op_st = 'b1;
    `LDA_XIIND, `LDA_SR, `LDA_DP, `LDA_LIND, `LDA_IMM,
    `LDA_ABS, `LDA_LABS, `LDA_INDYI, `LDA_IND, `LDA_SRINDY,
    `LDA_DPX, `LDA_LINDYI, `LDA_ABSY, `LDA_ABSX, `LDA_LABSX,
    `LDX_IMM, `LDX_DP, `LDX_ABS, `LDX_DPY, `LDX_ABSY,
    `LDY_IMM, `LDY_DP, `LDY_ABS, `LDY_DPX, `LDY_ABSX:           op_ld = 'b1;
    `DEC_A, `DEC_DP, `DEC_ABS, `DEC_DPX, `DEC_ABSX,
    `DEX, `DEY:                                                 op_dec = 'b1;
    `CMP_XIIND, `CMP_SR, `CMP_DP, `CMP_LIND, `CMP_IMM,
    `CMP_ABS, `CMP_LABS, `CMP_INDYI, `CMP_IND, `CMP_SRINDY,
    `CMP_DPX, `CMP_LINDYI, `CMP_ABSY, `CMP_ABSX, `CMP_LABSX,
    `CPX_IMM, `CPX_DP, `CPX_ABS,
    `CPY_IMM, `CPY_DP, `CPY_ABS:                                op_cmp = 'b1;
    `INC_A, `INC_DP, `INC_ABS, `INC_DPX, `INC_ABSX,
    `INX, `INY:                                                 op_inc = 'b1;
    `SBC_XIIND, `SBC_SR, `SBC_DP, `SBC_LIND, `SBC_IMM,
    `SBC_ABS, `SBC_LABS, `SBC_INDYI, `SBC_IND, `SBC_SRINDY,
    `SBC_DPX, `SBC_LINDYI, `SBC_ABSY, `SBC_ABSX, `SBC_LABSX:    op_sbc = 'b1;
    `BPL_REL, `BMI_REL, `BVC_REL, `BVS_REL, `BRA_REL, `BCC_REL,
    `BCS_REL, `BNE_REL, `BEQ_REL:                               op_bra = 'b1;
    `CLC, `CLI, `CLV, `CLD, `SEC, `SEI, `SED:                   op_clx_sex = 'b1;
    default: ;
  endcase
end

assign op_math = (op_asl | op_rol | op_lsr | op_ror |
                  op_dec | op_inc | op_and | op_or | op_eor |
                  op_adc | op_sbc | op_cmp | op_bit);

assign op_load_store = ~(op_jmp | op_jsr);
assign op_store = op_st;
assign op_rmw = ~(am_a | am_impl) &
                (op_asl | op_rol | op_lsr | op_ror | op_dec | op_inc |
                 op_trb | op_tsb);

always @* begin
  op_16bit = ~`p_m;

  if ((op_tr_x | op_tr_y) |
      ((op_ld | op_st | op_cmp | op_dec | op_inc | op_push | op_pull) &
       (op_x | op_y)))
    op_16bit = ~`p_x;
  else if (op_xba | ((op_push | op_pull) & (op_p | op_dbr | op_pbr)) /* | ...*/)
    op_16bit = 'b0;
  else if (op_tr_s | op_tr_c | op_tcd |
           ((op_push | op_pull) & op_d) | op_pea/* | ...*/)
    op_16bit = 'b1;
  else if ((ir == `REP_IMM) | (ir == `SEP_IMM))
    op_16bit = 'b0;
end

always @* begin
  // 16-bit load/store adds an extra cycle for the MSB between
  // instruction phases 1 and 2.
  op_load_store0 = 0;
  op_load_store1 = 0;
  if (~t[0]) begin
    if (op_load_store) begin
      if (am_imm) begin
        op_load_store0 = t[1];
        op_load_store1 = op_16bit & t[2];
      end
      else begin
        op_load_store0 = am_done_d;
        op_load_store1 = op_16bit & am_done_d2;
      end
    end
  end

  op_start = ~stalled & am_nostart & ~(am_imm & op_16bit);
  if (~t[1]) begin
    op_start = am_done_d;
    if (op_load_store) begin
      if (am_imm)
        op_start = op_16bit ? t[2] : t[1];
      else
        op_start = op_16bit ? am_done_d2 : am_done_d;
    end
  end
end

assign cl_pc_inc = ~intr & (t[0] | (t[1] & ~ins_onebyte) | op_pc_inc);
assign cl_ab_aor = ~resp & (cl_a_out | t[0] | t[1]);
assign cl_abb_ba = cl_ab_aor;
assign cl_idb7_n = op_set_n & ~op_16bit;
assign cl_idb15_n = op_set_n & op_16bit;
assign cl_idblz_z = op_set_z & ~op_16bit;
assign cl_idbz_z = op_set_z & op_16bit;

always @* begin
  am_done = 0;

  op_done = 0;
  op_brk_done = 0;
  op_pc_inc = 0;
  op_set_n = 0;
  op_set_z = 0;
  op_am_page_wrap = 0;
  op_push_pea_pre = 0;
  op_push_pea_hi = 0;
  op_push_pea_lo = 0;
  op_pull_rts_pre = 0;
  op_pull_rts_go = 0;
  op_pull_rts_last = 0;
  op_pull_rts_post = 0;

  cl_pc_dec = 0;
  cl_a_out = 0;
  cl_abl_aor = 0;
  cl_abh_aor = 0;
  cl_waitn0 = 0;
  cl_t_skip = 0;
  cl_store_dor = 0;
  cl_abl_pcl = 0;
  cl_abh_pch = 0;
  cl_sbl_pcl = 0;
  cl_sbh_pch = 0;
  cl_abl_aorl = 0;
  cl_abh_aorh = 0;
  cl_db_idl = ~(stalling);
  cl_zero_idlh = t[1];
  cl_db_idlh = 0;
  cl_idb_ba = 0;
  cl_bar_ba = 0;
  cl_idb_a = 0;
  cl_idb_b = 0;
  cl_idb_ac = 0;
  cl_idb_x = 0;
  cl_idb_y = 0;
  cl_idb_s = 0;
  cl_idb_d = 0;
  cl_idb_z = 0;
  cl_idb_p = 0;
  cl_idb_dbr = 0;
  cl_idb_pbr = 0;
  cl_cvo_v = 0;
  cl_zero_v = 0;
  cl_zero_d = 0;
  cl_ir5_d = 0;
  cl_ir5_i = 0;
  cl_one_i = 0;
  cl_cco_c = 0;
  cl_ir5_c = 0;
  cl_idb6_v = 0;
  cl_idb14_v = 0;
  cl_xchg_ce = 0;
  cl_idb_rs = rs_idl;
  cl_sb_s = 0;
  cl_sb_rs = rs_0;
  cl_abl_rs = rs_pcl;
  cl_abh_rs = rs_pch;
  cl_abb_rs = rs_pbr;
  cl_zero_abh = 0;
  cl_vec_abl = 0;
  cl_use_ai_rs = ~0;
  cl_ai_rs = rs_0;
  cl_ail_rs = rs_0;
  cl_aih_rs = rs_0;
  cl_bi_rs = rs_co;
  cl_one_vpa = t[1] & ~ins_onebyte;
  cl_one_vda = 0;
  cl_one_ml = 0;
  cl_aa_inc = 0;
  cl_aa_dec = 0;
  cl_not_bi = 0;
  cl_sign_extend_bil = 0;
  cl_one_addc = 0;
  cl_alu_16bit = ~0;
  cl_ccoh = 0;
  cl_daa_das = 0;
  cl_ors = 0;
  cl_ands = 0;
  cl_eors = 0;
  cl_asls = 0;
  cl_rols = 0;
  cl_lsrs = 0;
  cl_rors = 0;

  if (am_impl | am_imm)
    ;
  else if (am_abs) begin
    if (am_t[0]) begin
      cl_idb_z = ~0;
      {cl_a_out, cl_one_vpa} = ~0;
    end
    if (am_t[1]) begin
      {cl_a_out, cl_one_vpa} = ~0;
      op_pc_inc = op_load_store; // don't ++PC for JSR
      am_done = ~0;
    end
    if (am_t[2]) begin
      cl_abl_rs = rs_z;
      cl_abh_rs = rs_idl;
      if (op_load_store) begin
        cl_abb_rs = rs_dbr;
        {cl_a_out, cl_one_vda} = ~0;
      end
    end
  end
  else if (am_absx | am_absy) begin
    if (am_t[0]) begin
      cl_ai_rs = am_absx ? rs_x : rs_y;
      cl_bi_rs = rs_idl;
      cl_alu_16bit = 0;
      {cl_a_out, cl_one_vpa} = ~0;
    end
    if (am_t[1]) begin
      {cl_a_out, cl_one_vpa} = ~0;
      op_pc_inc = ~0;
      {cl_t_skip, am_done} = {2{~(cco | (op_store | op_rmw) | ~`p_x)}};
      cl_ccoh = ~0;
    end
    if (am_t[2]) begin
      op_am_page_wrap = ~0;
      cl_use_ai_rs = 0;
      cl_ail_rs = rs_aorh;
      cl_aih_rs = rs_dbr;
      cl_bi_rs = am_absx ? rs_xh : rs_yh;
      cl_one_addc = cco;
      cl_abl_rs = rs_co;
      cl_abh_rs = rs_idl;
      cl_abb_rs = rs_dbr;
      {cl_a_out} = ~0;
      am_done = ~0;
    end
    if (am_t[3]) begin
      if (op_am_page_wrap_d) begin
        cl_abl_rs = rs_aor;
        cl_abh_rs = rs_co;
        cl_abb_rs = rs_coh;
      end
      else begin
        cl_abl_rs = rs_co;
        cl_abh_rs = rs_idl;
        cl_abb_rs = rs_dbr;
      end
      {cl_a_out, cl_one_vda} = ~0;
    end
  end
  else if (am_aind) begin
    if (am_t[0]) begin
      cl_ai_rs = rs_0;
      cl_bi_rs = rs_idl;
      {cl_a_out, cl_one_vpa} = ~0;
    end
    if (am_t[1]) begin
      {cl_a_out, cl_one_vpa} = ~0;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
      op_pc_inc = ~0;
    end
    if (am_t[2]) begin
      cl_abl_rs = rs_idl;
      cl_abh_rs = rs_idlh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      {cl_aa_inc} = ~0;
      cl_idb_z = ~0;
    end
    if (am_t[3]) begin
      cl_abl_rs = rs_aao;
      cl_abh_rs = rs_aaoh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      {cl_aa_inc} = ~0;
      cl_t_skip = ~(ir == `JML_AIND);
      am_done = cl_t_skip;
    end
    if (am_t[4]) begin
      cl_abl_rs = rs_aao;
      cl_abh_rs = rs_aaoh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
      am_done = ~0;
    end
    if (am_t[5]) begin
      cl_abl_rs = rs_z;
      cl_abh_rs = rs_idl;
      cl_abb_rs = rs_pbr;
      if (ir == `JML_AIND) begin
        cl_abb_rs = rs_idlh;
        cl_idb_rs = rs_idlh;
        cl_idb_pbr = ~0;
      end
      else begin
      end
    end
  end
  else if (am_aindx) begin
    if (am_t[0]) begin
      cl_ai_rs = rs_0;
      cl_bi_rs = rs_idl;
      {cl_a_out, cl_one_vpa} = ~0;
    end
    if (am_t[1]) begin
      {cl_a_out, cl_one_vpa} = ~0;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
      op_pc_inc = ~0;
    end
    if (am_t[2]) begin
      cl_db_idl = 0;
      cl_ai_rs = rs_x;
      cl_bi_rs = rs_idl;
    end
    if (am_t[3]) begin
      cl_abl_rs = rs_co;
      cl_abh_rs = rs_coh;
      cl_abb_rs = rs_pbr;
      {cl_a_out, cl_one_vda} = ~0;
      {cl_aa_inc} = ~0;
    end
    if (am_t[4]) begin
      cl_abl_rs = rs_aao;
      cl_abh_rs = rs_aaoh;
      cl_abb_rs = rs_aab;
      {cl_a_out, cl_one_vda} = ~0;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
      am_done = ~0;
    end
    if (am_t[5]) begin
      cl_abl_rs = rs_idl;
      cl_abh_rs = rs_idlh;
      cl_abb_rs = rs_pbr;
    end
  end
  else if (am_dp) begin
    if (am_t[0]) begin
      cl_ai_rs = rs_d;
      cl_bi_rs = rs_idl;
      {cl_a_out, cl_one_vpa} = ~0;
      cl_t_skip = dl_z;
      am_done = cl_t_skip;
    end
    if (am_t[1]) begin
      am_done = ~0;
    end
    if (am_t[2]) begin
      cl_abl_rs = rs_co;
      cl_abh_rs = rs_coh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
    end
  end
  else if (am_dpx | am_dpy) begin
    if (am_t[0]) begin
      cl_ai_rs = rs_d;
      cl_bi_rs = rs_idl;
      {cl_a_out, cl_one_vpa} = ~0;
      cl_t_skip = dl_z;
    end
    if (am_t[2]) begin
      cl_ai_rs = rs_co;
      cl_bi_rs = am_dpx ? rs_x : rs_y;
      am_done = ~0;
    end
    if (am_t[3]) begin
      cl_abl_rs = rs_co;
      cl_abh_rs = rs_coh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
    end
  end
  else if (am_ind) begin
    if (am_t[0]) begin
      cl_t_skip = dl_z;
    end
    if (am_t[1]) begin
      cl_db_idl = 0;
      cl_ai_rs = rs_d;
      cl_bi_rs = rs_idl;
    end
    if (am_t[2]) begin
      cl_abl_rs = dl_z ? rs_idl : rs_co;
      cl_abh_rs = dl_z ? rs_dh : rs_coh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      {cl_aa_inc} = ~0;
    end
    if (am_t[3]) begin
      cl_abl_rs = rs_aao;
      cl_abh_rs = rs_aaoh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
      am_done = ~0;
    end
    if (am_t[4]) begin
      cl_abl_rs = rs_idl;
      cl_abh_rs = rs_idlh;
      cl_abb_rs = rs_dbr;
      {cl_a_out, cl_one_vda} = ~0;
    end
  end
  else if (am_indyi) begin
    if (am_t[0]) begin
      cl_t_skip = dl_z;
    end
    if (am_t[1]) begin
      cl_db_idl = 0;
      cl_ai_rs = rs_d;
      cl_bi_rs = rs_idl;
    end
    if (am_t[2]) begin
      cl_ai_rs = rs_y;
      cl_bi_rs = rs_idl;
      cl_alu_16bit = 0;
      cl_abl_rs = dl_z ? rs_idl : rs_co;
      cl_abh_rs = dl_z ? rs_dh : rs_coh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      {cl_aa_inc} = ~0;
    end
    if (am_t[3]) begin
      cl_abl_rs = rs_aao;
      cl_abh_rs = rs_aaoh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      {cl_t_skip, am_done} = {2{~(cco | (op_store | op_rmw) | ~`p_x)}};
      cl_ccoh = ~0;
    end
    if (am_t[4]) begin
      op_am_page_wrap = ~0;
      cl_use_ai_rs = 0;
      cl_ail_rs = rs_aorh;
      cl_aih_rs = rs_dbr;
      cl_bi_rs = rs_yh;
      cl_one_addc = cco;
      cl_abl_rs = rs_co;
      cl_abh_rs = rs_idl;
      cl_abb_rs = rs_dbr;
      {cl_a_out} = ~0;
      am_done = ~0;
    end
    if (am_t[5]) begin
      if (op_am_page_wrap_d) begin
        cl_abl_rs = rs_aor;
        cl_abh_rs = rs_co;
        cl_abb_rs = rs_coh;
      end
      else begin
        cl_abl_rs = rs_co;
        cl_abh_rs = rs_idl;
        cl_abb_rs = rs_dbr;
      end
      {cl_a_out, cl_one_vda} = ~0;
    end
  end
  else if (am_labs | am_labsx) begin
    if (am_t[0]) begin
      cl_ai_rs = am_labsx ? rs_x : rs_0;
      cl_bi_rs = rs_idl;
      cl_alu_16bit = 0;
      {cl_a_out, cl_one_vpa} = ~0;
    end
    if (am_t[1]) begin
      cl_idb_rs = rs_co;
      cl_idb_z = ~0;
      cl_ccoh = ~0;
      {cl_a_out, cl_one_vpa} = ~0;
      op_pc_inc = ~0;
    end
    if (am_t[2]) begin
      cl_ai_rs = am_labsx ? rs_xh : rs_0;
      cl_bi_rs = rs_idl;
      cl_one_addc = cco;
      {cl_a_out, cl_one_vpa} = ~0;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
      op_pc_inc = ~0;
      am_done = ~0;
    end
    if (am_t[3]) begin
      cl_abl_rs = rs_z;
      cl_abh_rs = rs_co;
      cl_abb_rs = rs_coh;
      {cl_a_out, cl_one_vda} = ~0;
    end
  end
  else if (am_lind | am_lindyi) begin
    if (am_t[0]) begin
      cl_t_skip = dl_z;
    end
    if (am_t[1]) begin
      cl_db_idl = 0;
      cl_ai_rs = rs_d;
      cl_bi_rs = rs_idl;
    end
    if (am_t[2]) begin
      cl_ai_rs = am_lindyi ? rs_y : rs_0;
      cl_bi_rs = rs_idl;
      cl_alu_16bit = 0;
      cl_abl_rs = dl_z ? rs_idl : rs_co;
      cl_abh_rs = dl_z ? rs_dh : rs_coh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      {cl_aa_inc} = ~0;
    end
    if (am_t[3]) begin
      cl_ccoh = ~0;
      cl_idb_rs = rs_co;
      cl_idb_z = ~0;
      cl_abl_rs = rs_aao;
      cl_abh_rs = rs_aaoh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      {cl_aa_inc} = ~0;
    end
    if (am_t[4]) begin
      cl_ai_rs = am_lindyi ? rs_yh : rs_0;
      cl_bi_rs = rs_idl;
      cl_one_addc = cco;
      cl_abl_rs = rs_aao;
      cl_abh_rs = rs_aaoh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
      am_done = ~0;
    end
    if (am_t[5]) begin
      cl_abl_rs = rs_z;
      cl_abh_rs = rs_co;
      cl_abb_rs = rs_coh;
      {cl_a_out, cl_one_vda} = ~0;
    end
  end
  else if (am_sr) begin
    if (am_t[1]) begin
      cl_ai_rs = rs_s;
      cl_bi_rs = rs_idl;
      cl_db_idl = 0;
      am_done = ~0;
    end
    if (am_t[2]) begin
      cl_abl_rs = rs_co;
      cl_abh_rs = rs_coh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
    end
  end
  else if (am_srindy) begin
    if (am_t[1]) begin
      cl_db_idl = 0;
      cl_ai_rs = rs_s;
      cl_bi_rs = rs_idl;
    end
    if (am_t[2]) begin
      cl_ai_rs = rs_y;
      cl_bi_rs = rs_idl;
      cl_alu_16bit = 0;
      cl_abl_rs = rs_co;
      cl_abh_rs = rs_coh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      {cl_aa_inc} = ~0;
    end
    if (am_t[3]) begin
      cl_ccoh = ~0;
      cl_idb_rs = rs_co;
      cl_idb_z = ~0;
      cl_abl_rs = rs_aao;
      cl_abh_rs = rs_aaoh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
    end
    if (am_t[4]) begin
      cl_use_ai_rs = 0;
      cl_ail_rs = rs_idlh;
      cl_aih_rs = rs_dbr;
      cl_bi_rs = rs_yh;
      cl_one_addc = cco;
      cl_db_idl = 0;
      am_done = ~0;
    end
    if (am_t[5]) begin
      cl_abl_rs = rs_z;
      cl_abh_rs = rs_co;
      cl_abb_rs = rs_coh;
      {cl_a_out, cl_one_vda} = ~0;
    end
  end
  else if (am_xiind) begin
    if (am_t[0]) begin
      cl_t_skip = dl_z;
    end
    if (am_t[1]) begin
      cl_db_idl = 0;
      cl_ai_rs = rs_d;
      cl_bi_rs = rs_idl;
    end
    if (am_t[2]) begin
      if (dl_z) begin
        cl_db_idl = 0;
        cl_use_ai_rs = 0;
        cl_ail_rs = rs_idl;
        cl_aih_rs = rs_dh;
      end
      else
        cl_ai_rs = rs_co;
      cl_bi_rs = rs_x;
    end
    if (am_t[3]) begin
      cl_abl_rs = rs_co;
      cl_abh_rs = rs_coh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      {cl_aa_inc} = ~0;
    end
    if (am_t[4]) begin
      cl_abl_rs = rs_aao;
      cl_abh_rs = rs_aaoh;
      cl_abb_rs = rs_0;
      {cl_a_out, cl_one_vda} = ~0;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
      am_done = ~0;
    end
    if (am_t[5]) begin
      cl_abl_rs = rs_idl;
      cl_abh_rs = rs_idlh;
      cl_abb_rs = rs_dbr;
      {cl_a_out, cl_one_vda} = ~0;
    end
  end

  if (op_load_store0) begin
    // Final cycle of addressing mode is driving out the address.
    cl_aa_inc = op_16bit;
    cl_one_ml = op_rmw;
  end
  if (op_load_store1) begin
    if (am_imm) begin
      {op_pc_inc, cl_one_vpa} = ~0;
    end
    else begin
      cl_abl_rs = rs_aao;
      cl_abh_rs = rs_aaoh;
      cl_abb_rs = rs_aab;
      cl_bar_ba = (am_sr | am_dp | am_dpx | am_dpy); // hold ba
      cl_one_vda = ~0;
    end
    cl_a_out = ~0;
    cl_one_ml = op_rmw;
  end

  if (op_store) begin
    if (op_load_store0 | op_load_store1) begin
      {cl_store_dor, cl_one_vda} = ~0;
    end
  end
  else if (op_16bit) begin
    if (op_load_store1) begin
      cl_db_idl = 0;
      cl_db_idlh = ~0;
    end
  end

  // LDA/LDX/LDY/ADC/SBC/CMP mem
  if (~(am_a | am_impl) & (op_ld | op_and | op_or | op_eor |
                           op_adc | op_sbc | op_cmp)) begin
    if (op_t[0]) begin
      cl_bi_rs = rs_idl;
      if (op_a)         cl_ai_rs = rs_ac;
      if (op_x)         cl_ai_rs = rs_x;
      if (op_y)         cl_ai_rs = rs_y;
      if (op_ld)        cl_ai_rs = rs_0;

      if (op_or)                        cl_ors = ~0;
      if (op_and)                       cl_ands = ~0;
      if (op_eor)                       cl_eors = ~0;
      if (op_adc | op_sbc)              cl_one_addc = `p_c;
      if (op_cmp)                       cl_one_addc = ~0;
      if (op_adc | op_sbc)              cl_daa_das = `p_d;
      if (op_sbc | op_cmp)              cl_not_bi = ~0;

      cl_alu_16bit = op_16bit;
      op_done = ~0;
    end
    if (op_t[1]) begin
      cl_idb_rs = rs_co;
      if (~op_cmp) begin
        if (op_a) begin
          cl_idb_ac = ~`p_m;
          cl_idb_a = `p_m;
        end
        if (op_x)       cl_idb_x = ~0;
        if (op_y)       cl_idb_y = ~0;
      end
      {op_set_n, op_set_z} = ~0;
      if (op_asl | op_rol | op_lsr | op_ror | op_sbc | op_adc | op_cmp) begin
        cl_cco_c = ~0;
      end
      if (op_sbc | op_adc) begin
        {cl_cvo_v} = ~0;
      end
    end
  end

  // STA/STX/STY mem
  if (~(am_a | am_impl) & op_store & (op_st)) begin
    if (op_load_store0) begin
      if (op_a)         cl_sb_rs = rs_a;
      if (op_x)         cl_sb_rs = rs_xl;
      if (op_y)         cl_sb_rs = rs_yl;
    end
    if (op_load_store1) begin
      if (op_a)         cl_sb_rs = rs_b;
      if (op_x)         cl_sb_rs = rs_xh;
      if (op_y)         cl_sb_rs = rs_yh;
    end
    op_done = op_t[0];
  end

  // ASL/ROL/LSR/ROR/DEC/INC mem (rmw)
  if (op_rmw & op_math) begin
    if (op_t[0]) begin
      cl_ai_rs = rs_idl;
      cl_bi_rs = rs_0;

      if (op_asl)                       cl_asls = ~0;
      if (op_rol)                       cl_rols = ~0;
      if (op_lsr)                       cl_lsrs = ~0;
      if (op_ror)                       cl_rors = ~0;
      if (op_rol | op_ror)              cl_one_addc = `p_c;
      if (op_dec)                       cl_not_bi = ~0;
      if (op_inc)                       cl_one_addc = ~0;

      cl_alu_16bit = op_16bit;
    end
    if (op_t[1]) begin
      cl_idb_rs = rs_co;
      {op_set_n, op_set_z} = ~0;
      cl_cco_c = ~(op_dec | op_inc);
      cl_one_ml = op_rmw;
    end
    if (op_t[2]) begin
      cl_sb_rs = op_16bit ? rs_coh : rs_co;
      {cl_store_dor, cl_one_vda} = ~0;
      cl_one_ml = op_rmw;
      cl_aa_dec = op_16bit;
      op_done = ~op_16bit;
    end
    if (op_t[3] & op_16bit) begin
      cl_sb_rs = rs_co;
      cl_abl_rs = rs_aao;
      cl_abh_rs = rs_aaoh;
      cl_abb_rs = rs_aab;
      {cl_a_out, cl_store_dor, cl_one_vda} = ~0;
      cl_one_ml = op_rmw;
      op_done = ~0;
    end
  end

  // ASL/ROL/LSR/ROR/DEC/INC A, DEX/DEY/INX/INY
  if ((am_a | am_impl) & op_math) begin
    if (op_t[0]) begin
      cl_bi_rs = rs_0;
      if (op_a)         cl_ai_rs = rs_ac;
      if (op_x)         cl_ai_rs = rs_x;
      if (op_y)         cl_ai_rs = rs_y;

      if (op_asl)                       cl_asls = ~0;
      if (op_rol)                       cl_rols = ~0;
      if (op_lsr)                       cl_lsrs = ~0;
      if (op_ror)                       cl_rors = ~0;
      if (op_rol | op_ror)              cl_one_addc = `p_c;
      if (op_dec)                       cl_not_bi = ~0;
      if (op_inc)                       cl_one_addc = ~0;

      cl_alu_16bit = op_16bit;
      op_done = ~0;
    end
    if (op_t[1]) begin
      cl_idb_rs = rs_co;
      if (op_a) begin
        cl_idb_ac = ~`p_m;
        cl_idb_a = `p_m;
      end
      if (op_x)       cl_idb_x = ~0;
      if (op_y)       cl_idb_y = ~0;
      {op_set_n, op_set_z} = ~0;
      cl_cco_c = ~(op_dec | op_inc);
    end
  end

  // BIT mem
  if (op_bit) begin
    if (op_t[0]) begin
      cl_ai_rs = rs_idl;
      cl_bi_rs = rs_ac;
      cl_ands = ~0;
      cl_alu_16bit = op_16bit;
      if (~am_imm) begin
        op_set_n = ~0;
        cl_idb6_v = ~op_16bit;
        cl_idb14_v = op_16bit;
      end
      op_done = ~0;
    end
    if (op_t[1]) begin
      cl_idb_rs = rs_co;
      op_set_z = ~0;
    end
  end

  // TRB/TSB mem (rmw)
  if (op_trb | op_tsb) begin
    if (op_t[1]) begin
      cl_ai_rs = rs_idl;
      cl_bi_rs = rs_ac;
      if (op_trb)       {cl_not_bi, cl_ands} = ~0;
      if (op_tsb)       cl_ors = ~0;
      cl_alu_16bit = op_16bit;
      cl_db_idl = 0;
      cl_one_ml = op_rmw;
    end
    if (op_t[2]) begin
      cl_sb_rs = op_16bit ? rs_coh : rs_co;
      {cl_store_dor, cl_one_vda} = ~0;
      cl_one_ml = op_rmw;
      cl_aa_dec = op_16bit;
      cl_db_idl = 0;
      op_done = ~op_16bit;
    end
    if (op_t[3] & op_16bit) begin
      cl_sb_rs = rs_co;
      cl_abl_rs = rs_aao;
      cl_abh_rs = rs_aaoh;
      cl_abb_rs = rs_aab;
      {cl_a_out, cl_store_dor, cl_one_vda} = ~0;
      cl_one_ml = op_rmw;
      cl_db_idl = 0;
      op_done = ~0;
    end
    if ((op_t[2] & ~op_16bit) | (op_t[3] & op_16bit)) begin
      cl_ai_rs = rs_idl;
      cl_bi_rs = rs_ac;
      cl_ands = ~0;
      cl_alu_16bit = op_16bit;
      cl_db_idl = 0;
    end
    if ((op_t[3] & ~op_16bit) | (op_t[4] & op_16bit)) begin
      cl_idb_rs = rs_co;
      op_set_z = ~0;
    end
  end

  // Register pushes
  if (op_push) begin
    if (op_t[0]) begin
      cl_t_skip = e;
      op_push_pea_pre = ~0;
      cl_t_skip = ~op_16bit;
    end
    if (op_t[1]) begin
      op_push_pea_hi = ~0;
      if (op_a)     cl_sb_rs = rs_b;
      if (op_x)     cl_sb_rs = rs_xh;
      if (op_y)     cl_sb_rs = rs_yh;
      if (op_d)     cl_sb_rs = rs_dh;
    end
    if (op_t[2]) begin
      op_push_pea_lo = ~0;
      if (op_a)     cl_sb_rs = rs_a;
      if (op_x)     cl_sb_rs = rs_xl;
      if (op_y)     cl_sb_rs = rs_yl;
      if (op_p)     cl_sb_rs = rs_p;
      if (op_d)     cl_sb_rs = rs_dl;
      if (op_dbr)   cl_sb_rs = rs_dbr;
      if (op_pbr)   cl_sb_rs = rs_pbr;
      op_done = ~0;
    end
  end

  // Register pulls
  if (op_pull) begin
    if (op_t[1]) begin
      op_pull_rts_pre = ~0;
    end
    if (op_t[2]) begin
      op_pull_rts_go = ~0;
      op_pull_rts_last = ~op_16bit;
      cl_t_skip = ~op_16bit;
      op_done = cl_t_skip;
    end
    if (op_t[3]) begin
      op_pull_rts_go = ~0;
      op_pull_rts_last = ~0;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
      op_done = ~0;
    end
    if (op_t[4]) begin
      op_pull_rts_post = ~0;
      if (op_a) begin
        cl_idb_ac = ~`p_m;
        cl_idb_a = `p_m;
      end
      if (op_x)     cl_idb_x = ~0;
      if (op_y)     cl_idb_y = ~0;
      if (op_p)     cl_idb_p = ~0;
      if (op_d)     cl_idb_d = ~0;
      if (op_dbr)   cl_idb_dbr = ~0;
      if (op_pbr)   cl_idb_pbr = ~0;
      if (op_a | op_x | op_y | op_d | op_dbr)
        {op_set_n, op_set_z} = ~0;
      cl_db_idl = 0;
    end
  end

  // Transfers
  if (op_tr_x | op_tr_y) begin
    if (op_t[1]) begin
      if (op_a)         cl_idb_rs = rs_ac;
      if (op_x)         cl_idb_rs = rs_x;
      if (op_s)         cl_idb_rs = rs_s;
      if (op_y)         cl_idb_rs = rs_y;

      if (op_tr_x)      cl_idb_x = ~0;
      if (op_tr_y)      cl_idb_y = ~0;
      {op_set_n, op_set_z} = ~0;
    end
    op_done = op_t[0];
  end
  if (op_tr_a) begin
    if (op_t[1]) begin
      if (op_x)         cl_idb_rs = rs_x;
      if (op_y)         cl_idb_rs = rs_y;
      cl_idb_ac = ~`p_m;
      cl_idb_a = `p_m;
      {op_set_n, op_set_z} = ~0;
    end
    op_done = op_t[0];
  end
  if (op_tr_c) begin
    if (op_t[1]) begin
      if (op_d)         cl_idb_rs = rs_d;
      if (op_s)         cl_idb_rs = rs_s;
      cl_idb_ac = ~0;
      {op_set_n, op_set_z} = ~0;
    end
    op_done = op_t[0];
  end
  if (op_tr_s) begin
    if (op_t[1]) begin
      if (op_a)         cl_idb_rs = rs_c;
      if (op_x)         cl_idb_rs = rs_x;
      cl_idb_s = ~0;
    end
    op_done = op_t[0];
  end
  if (op_tcd) begin
    if (op_t[1]) begin
      cl_idb_rs = rs_c;
      cl_idb_d = ~0;
      {op_set_n, op_set_z} = ~0;
    end
    op_done = op_t[0];
  end

  // Branches
  if (op_bra) begin
    if (op_t[0]) begin
      op_done = bra_skip;
    end
    if (op_t[1] & ~bra_skip) begin
      cl_db_idl = 0;
      cl_ai_rs = rs_pc;
      cl_bi_rs = rs_idl;
      cl_sign_extend_bil = ~0;
      op_done = ~0;
    end
    if (op_t[2]) begin
      cl_abl_rs = rs_co;
      cl_abh_rs = rs_coh;
      {cl_a_out, cl_abl_pcl, cl_abh_pch} = ~0;
    end
  end

  // JMP
  if (op_jmp) begin
    op_done = am_done & ~op_t[0];
    if (op_t[0]) begin
      {cl_a_out, cl_abl_pcl, cl_abh_pch} = ~0;
      if (am_labs) begin
        cl_idb_rs = rs_coh;
        cl_idb_pbr = ~0;
      end
    end
  end

  // JSR abs/aindx
  if (op_jsr & (am_abs | am_aindx)) begin
    if (op_t[0]) begin
      op_push_pea_pre = ~0;
      if (am_aindx)
        cl_idb_z = ~0;
      else
        cl_db_idl = 0;
    end
    if (op_t[1]) begin
      cl_sb_rs = rs_pch;
      op_push_pea_hi = ~0;
      cl_db_idl = 0;
    end
    if (op_t[2]) begin
      cl_sb_rs = rs_pcl;
      op_push_pea_hi = ~0;
      cl_db_idl = 0;
      op_done = am_abs;
    end
    if (op_t[3] & am_abs) begin
      cl_abl_rs = rs_z;
      cl_abh_rs = rs_idl;
      {cl_a_out, cl_abl_pcl, cl_abh_pch} = ~0;
    end
    // JSR abs ends above. JSR aindx continues below.
    if (op_t[3] & am_aindx) begin
      {cl_a_out, cl_one_vpa} = ~0;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
      op_pc_inc = ~0;
    end
    if (op_t[4]) begin
      cl_db_idl = 0;
      cl_ai_rs = rs_x;
      cl_bi_rs = rs_idl;
    end
    if (op_t[5]) begin
      cl_abl_rs = rs_co;
      cl_abh_rs = rs_coh;
      cl_abb_rs = rs_pbr;
      {cl_a_out, cl_one_vda} = ~0;
      {cl_aa_inc} = ~0;
    end
    if (op_t[6]) begin
      cl_abl_rs = rs_aao;
      cl_abh_rs = rs_aaoh;
      cl_abb_rs = rs_pbr;
      {cl_a_out, cl_one_vda} = ~0;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
      op_done = ~0;
    end
    if (op_t[7]) begin
      cl_abl_rs = rs_idl;
      cl_abh_rs = rs_idlh;
      cl_abb_rs = rs_pbr;
      {cl_a_out, cl_abl_pcl, cl_abh_pch} = ~0;
    end
  end

  // JSL labs
  if (op_jsr & am_labs) begin
    if (op_t[1]) begin
      {cl_a_out, cl_one_vpa} = ~0;
      op_pc_inc = ~0;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
      cl_idb_z = ~0;
      op_push_pea_pre = ~0;
    end
    if (op_t[2]) begin
      cl_sb_rs = rs_pbr;
      op_push_pea_hi = ~0;
    end
    if (op_t[4]) begin
      {cl_a_out, cl_one_vpa} = ~0;
      cl_idb_pbr = ~0;
    end
    if (op_t[5]) begin
      cl_sb_rs = rs_pch;
      op_push_pea_hi = ~0;
      cl_db_idl = 0;
    end
    if (op_t[6]) begin
      cl_sb_rs = rs_pcl;
      op_push_pea_hi = ~0;
      cl_db_idl = 0;
      op_done = ~0;
    end
    if (op_t[7]) begin
      cl_abl_rs = rs_z;
      cl_abh_rs = rs_zh;
      cl_abb_rs = rs_pbr;
      {cl_a_out, cl_abl_pcl, cl_abh_pch} = ~0;
    end
  end

  // RTS/RTL/RTI
  if (op_rts | op_rtl | op_rti) begin
    if (op_t[1]) begin
      op_pull_rts_pre = ~0;
      cl_t_skip = ~op_rti;
    end
    if (op_t[2]) begin
      op_pull_rts_go = ~0;
      cl_idb_z = ~0;
    end
    if (op_t[3]) begin
      op_pull_rts_go = ~0;
    end
    if (op_t[4]) begin
      op_pull_rts_go = ~0;
      op_pull_rts_last = op_rts;
      cl_db_idl = 0;
      cl_db_idlh = ~0;
    end
    if (op_t[5]) begin
      // sb transfers the new PC from idl during cp1.
      cl_sb_rs = rs_idl;
      {cl_sbl_pcl, cl_sbh_pch} = ~0;
      op_pc_inc = ~op_rti;
      if (~op_rts) begin
        op_pull_rts_go = ~0;
        op_pull_rts_last = ~0;
        // idb transfers the new BA from idl during cp2.
        cl_idb_pbr = ~0;
      end
      op_done = ~0;
    end
    if (op_t[6]) begin
      op_pull_rts_post = ~0;
      {cl_a_out} = ~0;
      if (op_rti) begin
        cl_idb_rs = rs_z;
        cl_idb_p = ~0;
      end
    end
  end

  // CLC/SEC/CLI/SEI/CLV/CLD/SED
  if (op_clx_sex) begin
    cl_ir5_c = (ir == `CLC || ir == `SEC);
    cl_ir5_i = (ir == `CLI || ir == `SEI);
    cl_zero_v = (ir == `CLV);
    cl_ir5_d = (ir == `CLD || ir == `SED);
    op_done = op_t[0];
  end

  case (ir)
    `BRK, `COP: begin
      if (op_t[0]) begin
        if (~intr)
          {cl_a_out, cl_one_vpa} = ~0;
        op_pc_inc = ~0;
        op_push_pea_pre = ~0;
        cl_t_skip = e;
      end
      if (op_t[1]) begin
        cl_sb_rs = rs_pbr;
        op_push_pea_hi = ~0;
      end
      if (op_t[2]) begin
        cl_sb_rs = rs_pch;
        op_push_pea_hi = ~0;
      end
      if (op_t[3]) begin
        cl_sb_rs = rs_pcl;
        op_push_pea_hi = ~0;
      end
      if (op_t[4]) begin
        cl_sb_rs = rs_p;
        op_push_pea_lo = ~0;
      end
      if (op_t[5]) begin
        cl_abh_rs = rs_1;
        cl_abl_rs = rs_1;
        cl_abb_rs = rs_0;
        {cl_a_out, cl_vec_abl, cl_one_vda} = ~0;
        cl_ai_rs = rs_0;
        cl_bi_rs = rs_idl;
      end
      if (op_t[6]) begin
        cl_abh_rs = rs_1;
        cl_abl_rs = rs_1;
        cl_abb_rs = rs_0;
        {cl_a_out, cl_vec_abl, cl_one_vda} = ~0;
        cl_idb_rs = rs_0;
        cl_idb_pbr = ~0;
        op_done = ~0;
      end
      if (op_t[7]) begin
        cl_abl_rs = rs_co;
        cl_abh_rs = rs_idl;
        cl_abb_rs = rs_pbr;
        {cl_abl_pcl, cl_abh_pch} = ~0;
        {cl_zero_d, cl_one_i} = ~0;
      end
    end
    `BRL_LREL: begin
      if (op_t[0]) begin
        {cl_a_out, cl_one_vpa} = ~0;
        op_pc_inc = ~0;
      end
      if (op_t[1]) begin
        {cl_a_out, cl_one_vpa} = ~0;
        cl_db_idl = 0;
        cl_db_idlh = ~0;
        cl_ai_rs = rs_pc;
        cl_bi_rs = rs_idl;
        op_pc_inc = ~0;
      end
      if (op_t[2]) begin
        op_done = ~0;
      end
      if (op_t[3]) begin
        cl_abl_rs = rs_co;
        cl_abh_rs = rs_coh;
        {cl_a_out, cl_abl_pcl, cl_abh_pch} = ~0;
      end
    end
    `MVP_BLK, `MVN_BLK: begin
      if (op_t[0]) begin
        cl_idb_dbr = ~0;
        op_pc_inc = ~0;
      end
      if (op_t[1]) begin
        {cl_a_out, cl_one_vpa} = ~0;
        op_pc_inc = ac_z;
      end
      if (op_t[2]) begin
        cl_abl_rs = rs_xl;
        cl_abh_rs = rs_xh;
        cl_abb_rs = rs_idl;
        {cl_a_out, cl_one_vda} = ~0;
      end
      if (op_t[3]) begin
        cl_sb_rs = rs_idl;
        cl_abl_rs = rs_yl;
        cl_abh_rs = rs_yh;
        cl_abb_rs = rs_dbr;
        {cl_a_out, cl_store_dor, cl_one_vda} = ~0;
        cl_ai_rs = rs_x;
        cl_bi_rs = rs_0;
        if (ir == `MVP_BLK)
          cl_not_bi = ~0;
        else
          cl_one_addc = ~0;
      end
      if (op_t[4]) begin
        cl_idb_rs = rs_co;
        cl_idb_x = ~0;
        cl_ai_rs = rs_y;
        cl_bi_rs = rs_0;
        if (ir == `MVP_BLK)
          cl_not_bi = ~0;
        else
          cl_one_addc = ~0;
        cl_pc_dec = ~ac_z;
      end
      if (op_t[5]) begin
        cl_idb_rs = rs_co;
        cl_idb_y = ~0;
        cl_ai_rs = rs_ac;
        cl_bi_rs = rs_0;
        cl_not_bi = ~0;
        cl_pc_dec = ~ac_z;
        op_done = ~0;
      end
      if (op_t[6]) begin
        cl_idb_rs = rs_co;
        cl_idb_ac = ~0;
      end
    end
    `NOP: begin
      op_done = op_t[0];
    end
    `PEA: begin
      if (op_t[0]) begin
        {cl_a_out, cl_one_vpa} = ~0;
        op_pc_inc = ~0;
      end
      if (op_t[1]) begin
        {cl_a_out, cl_one_vpa} = ~0;
        cl_db_idl = 0;
        cl_db_idlh = ~0;
        op_pc_inc = ~0;
        op_push_pea_pre = ~0;
      end
      if (op_t[2]) begin
        op_push_pea_hi = ~0;
        cl_sb_rs = rs_idlh;
        cl_db_idl = 0;
      end
      if (op_t[3]) begin
        op_push_pea_lo = ~0;
        cl_sb_rs = rs_idl;
        op_done = ~0;
      end
    end
    `PEI: begin
      if (op_t[0]) begin
        cl_ai_rs = rs_d;
        cl_bi_rs = rs_idl;
        {cl_a_out, cl_one_vpa} = ~0;
        cl_t_skip = dl_z;
        op_pc_inc = ~0;
      end
      if (op_t[2]) begin
        cl_abl_rs = rs_co;
        cl_abh_rs = rs_coh;
        cl_abb_rs = rs_0;
        {cl_a_out, cl_one_vda} = ~0;
        cl_aa_inc = ~0;
      end
      if (op_t[3]) begin
        cl_abl_rs = rs_aao;
        cl_abh_rs = rs_aaoh;
        cl_abb_rs = rs_0;
        {cl_a_out, cl_one_vda} = ~0;
        cl_db_idl = 0;
        cl_db_idlh = ~0;
        op_push_pea_pre = ~0;
      end
      if (op_t[4]) begin
        op_push_pea_hi = ~0;
        cl_sb_rs = rs_idlh;
        cl_db_idl = 0;
      end
      if (op_t[5]) begin
        op_push_pea_lo = ~0;
        cl_sb_rs = rs_idl;
        op_done = ~0;
      end
    end
    `PER: begin
      if (op_t[0]) begin
        {cl_a_out, cl_one_vpa} = ~0;
        op_pc_inc = ~0;
      end
      if (op_t[1]) begin
        {cl_a_out, cl_one_vpa} = ~0;
        cl_db_idl = 0;
        cl_db_idlh = ~0;
        cl_ai_rs = rs_pc;
        cl_bi_rs = rs_idl;
        op_pc_inc = ~0;
      end
      if (op_t[2]) begin
        cl_idb_rs = rs_co;
        cl_idb_z = ~0;
        op_push_pea_pre = ~0;
      end
      if (op_t[3]) begin
        op_push_pea_hi = ~0;
        cl_sb_rs = rs_zh;
        cl_db_idl = 0;
      end
      if (op_t[4]) begin
        op_push_pea_lo = ~0;
        cl_sb_rs = rs_zl;
        op_done = ~0;
      end
    end
    `REP_IMM, `SEP_IMM: begin
      if (op_t[1]) begin
        cl_db_idl = 0;
        cl_ai_rs = rs_p;
        cl_bi_rs = rs_idl;
        if (ir == `REP_IMM)
          {cl_not_bi, cl_ands} = ~0;
        else
          {cl_ors} = ~0;
        {cl_alu_16bit, op_done} = ~0;
      end
      if (op_t[2]) begin
        cl_idb_rs = rs_co;
        cl_idb_p = ~0;
      end
    end
    `WAI: begin
      cl_waitn0 = op_t[0];
      op_done = op_t[1];
    end
    `WDM: begin
      if (op_t[0]) begin
        {op_pc_inc, op_done} = ~0;
      end
    end
    `XBA: begin
      if (op_t[1]) begin
        cl_ai_rs = rs_b;
        cl_bi_rs = rs_0;
        cl_alu_16bit = ~0;
        cl_idb_rs = rs_a;
        cl_idb_b = ~0;
      end
      if (op_t[2]) begin
        cl_idb_rs = rs_co;
        cl_idb_a = ~0;
        {op_set_n, op_set_z} = ~0;
      end
      op_done = op_t[1];
    end
    `XCE: begin
      if (op_t[1]) begin
        cl_xchg_ce = ~0;
      end
      op_done = op_t[0];
    end
    default: ;
  endcase

  // Push cycles
  if (op_push_pea_pre) begin
    cl_ai_rs = rs_s;
    cl_bi_rs = rs_0;
    {cl_not_bi} = ~0;
  end
  if (op_push_pea_hi) begin
    cl_ai_rs = rs_co;
    cl_bi_rs = rs_0;
    {cl_not_bi} = ~0;
  end
  if (op_push_pea_hi | op_push_pea_lo) begin
    cl_abl_rs = rs_s;
    cl_abh_rs = rs_sh;
    cl_abb_rs = rs_0;
    {cl_a_out, cl_store_dor, cl_one_vda} = ~0;
    cl_idb_rs = rs_co;
    cl_idb_s = ~0;
  end

  // Pull cycles
  if (op_pull_rts_pre) begin
    cl_ai_rs = rs_s;
    cl_bi_rs = rs_0;
    cl_one_addc = ~0;
  end
  if (op_pull_rts_go) begin
    if (~op_pull_rts_last) begin
      cl_ai_rs = rs_co;
      cl_bi_rs = rs_0;
      cl_one_addc = ~0;
    end
    cl_abl_rs = rs_co;
    cl_abh_rs = rs_coh;
    cl_abb_rs = rs_0;
    {cl_a_out, cl_one_vda} = ~0;
  end
  if (op_pull_rts_post) begin
    cl_sb_rs = rs_co;
    cl_sb_s = ~0;
  end
end

always @(posedge CLK) begin
  if (cp2n & rdy2)
    op_am_page_wrap_d <= op_am_page_wrap;

  if ((MON_SEL == 6'h21) & MON_WS)
    op_am_page_wrap_d <= MON_DIN[9];
end

assign rw_int = ~cl_store_dor;


//////////////////////////////////////////////////////////////////////
// Register monitor interface

reg [31:0] mon_dout;

always @* begin
  //``REGION_REGS EMUBUS_CTRL
  mon_dout = 32'b0;
  case (MON_SEL)                //``SUBREGS CPU_CORE
    6'h01: begin                //``REG ARC0
      mon_dout[15:0] = pc;
      mon_dout[16] = `p_c;      //``FIELD PF_C
      mon_dout[17] = `p_z;      //``FIELD PF_Z
      mon_dout[18] = `p_i;      //``FIELD PF_I
      mon_dout[19] = `p_d;      //``FIELD PF_D
      mon_dout[20] = `p_x;      //``FIELD PF_X_B
      mon_dout[21] = `p_m;      //``FIELD PF_M
      mon_dout[22] = `p_v;      //``FIELD PF_V
      mon_dout[23] = `p_n;      //``FIELD PF_N
      mon_dout[24] = e;
    end
    6'h02: begin                //``REG ARC1
      mon_dout[15:0] = s;
      mon_dout[31:16] = ac;
    end
    6'h03: begin                //``REG ARC2
      mon_dout[15:0] = d;
      mon_dout[31:16] = x;
    end
    6'h04: begin                //``REG ARC3
      mon_dout[15:0] = y;
      mon_dout[23:16] = dbr;
      mon_dout[31:24] = pbr;
    end
    6'h08: begin                //``REG UAR0
      mon_dout[7:0] = ir;
      mon_dout[23:8] = aor;
      mon_dout[31:24] = bar;
    end
    6'h09: begin                //``REG UAR1
      mon_dout[7:0] = dor;
      mon_dout[23:8] = z;
    end
    6'h10: begin                //``REG CTCS // CPU Timing Control / Status
      mon_dout[0] = stall;
      mon_dout[1] = stalled;
      mon_dout[7:4] = ts;       //``FIELD TS // Timing State
    end
    6'h11: begin                //``REG CPS // Pin State
      mon_dout[1] = rw_int;     //``FIELD RW
      mon_dout[3] = sync;
      mon_dout[4] = RDYIN;
      mon_dout[4] = WAITN;
    end
    6'h20: begin                //``REG INT0
      mon_dout[0] = cp2;
      mon_dout[1] = t_reset_pend;
      mon_dout[2] = am_nostart;
      mon_dout[3] = am_done_d;
      mon_dout[4] = am_done_d2;
      mon_dout[5] = resp;
      mon_dout[6] = nmip;
      mon_dout[7] = irqp;
      mon_dout[8] = resg;
      mon_dout[9] = nmig;
      mon_dout[10] = 1'b0;      //``NO_FIELD (spare)
      mon_dout[11] = respd;
      mon_dout[12] = nmil;
      mon_dout[13] = intg_pre2;
      mon_dout[14] = intg;
      mon_dout[15] = vec1;
      mon_dout[18:16] = intr_id[2:0];
      mon_dout[19] = rwb_ext;
      mon_dout[20] = mlb_ext;
      mon_dout[21] = vpb_ext;
      mon_dout[22] = vda_ext;
      mon_dout[23] = vpa_ext;
      mon_dout[24] = 1'b0;      //``NO_FIELD (spare)
      mon_dout[25] = ac_z;
      mon_dout[26] = dl_z;
      mon_dout[27] = addc;
      mon_dout[28] = add16;
      mon_dout[29] = accoh;
      mon_dout[30] = daa;
      mon_dout[31] = das;
    end
    6'h21: begin                //``REG INT1
      mon_dout[0] = ors;
      mon_dout[1] = ands;
      mon_dout[2] = eors;
      mon_dout[3] = asls;
      mon_dout[4] = rols;
      mon_dout[5] = lsrs;
      mon_dout[6] = rors;
      mon_dout[7] = ccod;
      mon_dout[8] = cvod;
      mon_dout[9] = op_am_page_wrap_d;
      mon_dout[10] = rdy1;
      mon_dout[11] = waitn0;
    end
    6'h24: begin                //``REG INT4
      mon_dout[7:0] = t[7:0];
      mon_dout[14:8] = am_t[6:0];
      mon_dout[22:16] = op_tp[7:1];
      mon_dout[31:24] = aab[7:0];
    end
    6'h25: begin                //``REG INT5
      mon_dout[15:0] = upc;
      mon_dout[31:16] = npc;
    end
    6'h26: begin                //``REG INT6
      mon_dout[15:0] = idld;
      mon_dout[31:16] = aao;
    end
    6'h27: begin                //``REG INT7
      mon_dout[15:0] = ai;
      mon_dout[31:16] = bi;
    end
    6'h28: begin                //``REG INT8
      mon_dout[16:0] = cod;
    end
    default: ;
  endcase
end

assign MON_DOUT = mon_dout;

// EOF
