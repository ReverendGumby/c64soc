// Multiply and divide unit
// Informed second-pass implementation
// Reference: https://github.com/rgalland/SNES_S-CPU_Schematics/

module s_cpu_muldiv
  (
    input             nRES,
    input             CLK,
    input             CP2_NEGEDGE,
    input             HOLD,

    input [7:0]       MON_SEL,
    output reg        MON_ACK,
    input             MON_READY,
    output reg [31:0] MON_DOUT,
    output reg        MON_VALID,
    input [3:0]       MON_WS,
    input [31:0]      MON_DIN,

    input [7:0]       D_I,
    input             WRMPYA,
    input             WRMPYB,
    input             WRDIVL,
    input             WRDIVH,
    input             WRDIVB,

    output [15:0]     DIV,
    output [15:0]     MPY
   );

reg [7:0]       mpya;
reg [15:0]      num;
reg [22:0]      shifter;        // mpy.a/denom. latch and shifter
reg [15:0]      prodrem;        // product / remainder
reg [15:0]      mpyquot;        // mpy.b / quotient
reg             mulnotdiv;
reg [4:0]       st;

wire [16:0]     madd, dsub;
wire [22:0]     shifter_next_mul, shifter_next_div;
wire [15:0]     prodrem_next_mul, prodrem_next_div;
wire [15:0]     mpyquot_next_mul, mpyquot_next_div;
wire            zdiv;

wire            mon_write_muldiv0, mon_write_muldiv1, mon_write_muldiv2,
                mon_write_muldiv3;

//////////////////////////////////////////////////////////////////////
// Multiplier combinatorial logic

assign madd = prodrem + shifter[15:0];
assign shifter_next_mul = {shifter[21:0], 1'b0};
assign prodrem_next_mul = mpyquot[0] ? madd[15:0] : prodrem;
assign mpyquot_next_mul = {1'b0, mpyquot[15:1]};


//////////////////////////////////////////////////////////////////////
// Divider combinatorial logic

assign dsub = prodrem - shifter[15:0];
assign zdiv = ~dsub[16] & ~|shifter[22:16];
assign shifter_next_div = {1'b0, shifter[22:1]};
assign prodrem_next_div = zdiv ? dsub[15:0] : prodrem;
assign mpyquot_next_div = {mpyquot[14:0], zdiv};


//////////////////////////////////////////////////////////////////////
// Register interface / state machine

initial begin
  mpya = 8'd0;
  mpyquot = 16'd0;
  num = 16'd0;
  shifter = 23'd0;
  prodrem = 16'd0;
end

always @(posedge CLK) begin
  if (~nRES) begin
    st <= 5'd0;
  end
  else if (CP2_NEGEDGE) begin
    if (WRMPYA)
      mpya <= D_I;
    if (WRMPYB) begin
      // "The unsigned multiplication registers will replace the value
      // at RDDIVL with WRMPYB, and RDDIVH with zero."
      prodrem <= 16'd0;
      mpyquot <= {8'b0, D_I};
      shifter <= {15'b0, mpya};
      st <= 5'd8;
      mulnotdiv <= 1'b1;
    end
    if (WRDIVL)
      num[7:0] <= D_I;
    if (WRDIVH)
      num[15:8] <= D_I;
    if (WRDIVB) begin
      shifter <= {D_I, 15'b0};
      prodrem <= num;
      st <= 5'd16;
      mulnotdiv <= 1'b0;
    end

    if (|st) begin
      st <= st - 1'd1;
      if (mulnotdiv) begin
        shifter <= shifter_next_mul;
        prodrem <= prodrem_next_mul;
        mpyquot <= mpyquot_next_mul;
      end
      else begin
        shifter <= shifter_next_div;
        prodrem <= prodrem_next_div;
        mpyquot <= mpyquot_next_div;
      end
    end
  end  

  if (mon_write_muldiv0) begin
    if (MON_WS[0])
      mpya <= MON_DIN[7:0];
    if (MON_WS[2])
      num[7:0] <= MON_DIN[23:16];
    if (MON_WS[3])
      num[15:8] <= MON_DIN[31:24];
  end
  if (mon_write_muldiv1) begin
    if (MON_WS[0])
      shifter[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      shifter[15:8] <= MON_DIN[15:8];
    if (MON_WS[2])
      shifter[22:16] <= MON_DIN[22:16];
  end
  if (mon_write_muldiv2) begin
    if (MON_WS[0])
      prodrem[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      prodrem[15:8] <= MON_DIN[15:8];
    if (MON_WS[2])
      mpyquot[7:0] <= MON_DIN[23:16];
    if (MON_WS[3])
      mpyquot[15:8] <= MON_DIN[31:24];
  end
  if (mon_write_muldiv3) begin
    if (MON_WS[0])
      st <= MON_DIN[4:0];
    if (MON_WS[1]) begin
      mulnotdiv <= MON_DIN[8];
    end
  end
end

assign MPY = prodrem;
assign DIV = mpyquot;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_sel_muldiv0, mon_sel_muldiv1, mon_sel_muldiv2, mon_sel_muldiv3;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  MON_ACK = 1'b1;
  mon_sel_muldiv0 = 1'b0;
  mon_sel_muldiv1 = 1'b0;
  mon_sel_muldiv2 = 1'b0;
  mon_sel_muldiv3 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL[7:2])         //``SUBREGS CPU
    6'h18: begin              //``REG MULDIV0
      mon_sel_muldiv0 = 1'b1;
      mon_dout[7:0] = mpya;
      mon_dout[31:16] = num;
    end
    6'h19: begin              //``REG MULDIV1
      mon_sel_muldiv1 = 1'b1;
      mon_dout[22:0] = shifter;
    end
    6'h1A: begin              //``REG MULDIV2
      mon_sel_muldiv2 = 1'b1;
      mon_dout[15:0] = prodrem;  //``FIELD MPY
      mon_dout[31:16] = mpyquot; //``FIELD DIV
    end
    6'h1B: begin              //``REG MULDIV3
      mon_sel_muldiv3 = 1'b1;
      mon_dout[4:0] = st;
      mon_dout[8] = mulnotdiv;
    end
    default: begin
      MON_ACK = 1'b0;
    end
  endcase
end

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_muldiv0 = mon_write & mon_sel_muldiv0;
assign mon_write_muldiv1 = mon_write & mon_sel_muldiv1;
assign mon_write_muldiv2 = mon_write & mon_sel_muldiv2;
assign mon_write_muldiv3 = mon_write & mon_sel_muldiv3;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK & MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end

endmodule
