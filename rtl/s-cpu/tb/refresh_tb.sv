`timescale 1us / 1ns

module refresh_tb();

reg        nRES;
reg        mclk;
wire       cp1_posedge, cp2_posedge, cp2_negedge;
wire       cp1p, cp2p;

wire [23:0] a;
reg [7:0]   din;

reg [1:0]   cc_cnt;
wire        cc;
reg         hblank, vblank;

initial begin
  $dumpfile("refresh_tb.vcd");
  $dumpvars();
end

s_cpu cpu
  (
   .nRES(nRES),
   .MCLK(mclk),
   .HOLD(1'b0),
   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .CP2_NEGEDGE_PRE(),

   .nIRQ(1'b1),
   .nNMI(1'b1),
   .nABORT(1'b1),
   .nML(),
   .MF(),
   .XF(),
   .RW(),
   .RDY(1'b1),
   .nVP(),
   .VDA(),
   .VPA(),

   .A(a),
   .D_I(din),
   .D_O(),
   .nROMSEL(nromsel),
   .nWRAMSEL(nwramsel),
   .nWR(nwr),
   .nRD(nrd),

   .PA(),
   .nPARD(),
   .nPAWR(),

   .CC(cc),
   .HBLANK(hblank),
   .VBLANK(vblank),

   .CORE_MON_SEL(6'd0),
   .CORE_MON_WS(1'b0),
   .CORE_MON_DOUT(),
   .CORE_MON_DIN()
   );

initial begin
  mclk = 1;
  nRES = 0;
  cc_cnt = 0;
  hblank = 1;
  vblank = 1;
end

initial forever begin :mclkgen
  #(500.0/21477.27) mclk <= ~mclk;
end

always @(posedge mclk) begin :ccgen
  cc_cnt <= cc_cnt + 1;
end

assign cc = cc_cnt == 3;

initial forever begin :hgen
  while (~nRES)
    @(negedge cc) ;
  hblank <= 1'b0;
  repeat (274) @(negedge cc) ;
  hblank <= 1'b1;
  repeat (67) @(negedge cc) ;
end

initial forever begin :vgen
  while (~nRES)
    @(negedge cc) ;
  vblank <= 1'b0;
  repeat (260) @(negedge hblank) ;
  vblank <= 1'b1;
end

always @* begin
  casez (a)
    24'h008000:   din = 8'hea;
    24'h008001:   din = 8'h4c;
    24'h008002:   din = 8'h00;
    24'h008003:   din = 8'h80;
    24'h00fffc:   din = 8'h00;
    24'h00fffd:   din = 8'h80;
    default: din = 8'hx;
  endcase
end

//////////////////////////////////////////////////////////////////////

// Use ungated slow clock for when refresh stalls DMA
assign cp1p = cpu.slowck_sel ? cpu.clkgen.slow_cp1_posedge : cp1_posedge;
assign cp2p = cpu.slowck_sel ? cpu.clkgen.slow_cp2_posedge : cp2_posedge;

task next_cp1p();
  @(posedge mclk) ;
  while (~cp1p)
    @(posedge mclk) ;
endtask  

task next_cp2p();
  @(posedge mclk) ;
  while (~cp2p)
    @(posedge mclk) ;
endtask  

task go_to_hpos(input [8:0] c);
  do
    @cpu.ocio.gen.hcnt ;
  while (cpu.ocio.gen.hcnt != c);
  @(posedge mclk) ;
endtask

task assert_refresh(reg en);
  if (cpu.clkgen.cpu_act)
    assert(cpu.clkgen.CPU_GATE_ACTIVE == en);
  if (en) begin
    assert(cpu.nRD == 'b1);
    assert(cpu.nWR == 'b1);
    assert(cpu.nPARD == 'b1);
    assert(cpu.nPAWR == 'b1);
  end
endtask

task test_refresh_on_off(input [8:0] on, input [8:0] off);
  go_to_hpos(on - 1);
  assert_refresh(0);
  @(posedge mclk) ;
  assert(cpu.refresh_en == 0);
  go_to_hpos(on);
  assert_refresh(0);
  @(posedge mclk) ;
  assert(cpu.refresh_en == 1);
  next_cp1p();
  @(posedge mclk) ;
  assert_refresh(1);
  next_cp2p();
  if (cpu.clkgen.cpu_act)
    assert(cpu.clkgen.cycle_ccnt == 'd8);
  @(posedge mclk) ;
  assert_refresh(1);

  go_to_hpos(off - 1);
  assert_refresh(1);
  @(posedge mclk) ;
  assert(cpu.refresh_en == 1);
  go_to_hpos(off);
  assert_refresh(1);
  @(posedge mclk) ;
  assert(cpu.refresh_en == 0);
  next_cp1p();
  @(posedge mclk) ;
  assert_refresh(0);
  next_cp2p();
  @(posedge mclk) ;
  assert_refresh(0);
endtask

//////////////////////////////////////////////////////////////////////

task test_norm_clk();
  test_refresh_on_off(128, 138);
endtask

task test_fast_clk();
  go_to_hpos(0);
  force cpu.fast_cpu = 1;

  test_refresh_on_off(128, 138);

  go_to_hpos(340);
  release cpu.fast_cpu;
endtask

task test_slow_clk();
  go_to_hpos(0);
  force cpu.slow_cpu = 1;

  test_refresh_on_off(128, 139);

  go_to_hpos(340);
  release cpu.slow_cpu;
endtask

task test_gpdma();
  go_to_hpos(0);

  // Start GP-DMA
`define dmac cpu.ocio.dmac
  `dmac.dreg[0][0] = 8'h04;
  `dmac.dreg[0][1] = 8'h18;
  `dmac.dreg[0][2] = 8'h2A;
  `dmac.dreg[0][3] = 8'hCD;
  `dmac.dreg[0][4] = 8'h00;
  `dmac.dreg[0][5] = 8'h7F; // Runs from H=1..261
  `dmac.dreg[0][6] = 8'h00;
  next_cp2p();
  force `dmac.MSET = 8'h01;
  next_cp2p();
  release `dmac.MSET;

  test_refresh_on_off(128, 138);

  go_to_hpos(340);
endtask

//////////////////////////////////////////////////////////////////////

initial begin
  repeat (2) next_cp2p();
  nRES <= 1'b1;
  repeat (2) next_cp2p();

  repeat (10) test_norm_clk();
  repeat (10) test_fast_clk();
  repeat (10) test_slow_clk();
  repeat (10) test_gpdma();

  #50 $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s refresh_tb -o refresh_tb.vvp -f s-cpu.files refresh_tb.sv && ./refresh_tb.vvp"
// End:
