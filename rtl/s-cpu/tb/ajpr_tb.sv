`timescale 1us / 1ns

module ajpr_tb();

reg        nRES, hold;
reg        clk, mclk;
wire [2:1] jpclk;
wire       jpout;
reg [1:0]  jp1d, jp2d;
reg        vblank;
reg        ajpr_en;
wire       ajpr_busy;
wire [15:0] joy1, joy2, joy3, joy4;

initial begin
  $dumpfile("ajpr_tb.vcd");
  $dumpvars();
end

s_cpu_clkgen clkgen
  (
   .CLK_RESET(1'b0),
   .CLK(mclk),

   .SLOWCK_SEL(1'b0),
   .CLK_HOLD(1'b0),

   .SLOW_CP1_POSEDGE(slow_cp1_posedge)
   );

s_cpu_ajpr ajpr
  (
   .nRES(nRES),
   .CLK(mclk),
   .SLOW_CP1_POSEDGE(slow_cp1_posedge),
   .HOLD(hold),

   .JPCLK(jpclk),
   .JPOUT(jpout),
   .JP1D(jp1d),
   .JP2D(jp2d),

   .VBLANK(vblank),

   .AJPR_EN(ajpr_en),
   .AJPR_BUSY(ajpr_busy),
   .JOY1(joy1),
   .JOY2(joy2),
   .JOY3(joy3),
   .JOY4(joy4)
   );

initial begin
  clk = 1;
  nRES = 0;
  hold = 0;
end

initial forever begin :ckgen
  #(250.0/(21477.272*3)) clk = ~clk;
end

localparam [1:0] mclk_ccnt_max = 2'd2;
reg [1:0] mclk_ccnt;
initial begin
  mclk_ccnt = 2'd0;
  mclk = 'b0;
end
always @(posedge clk) begin
  if (mclk_ccnt == mclk_ccnt_max) begin
    mclk_ccnt <= 2'd0;
    mclk <= ~mclk;
  end
  else
    mclk_ccnt <= mclk_ccnt + 1'd1;
end

task next_tick();
  @(posedge mclk) ;
  while (~ajpr.tick)
    @(posedge mclk) ;
  @(posedge mclk) ;
endtask  

initial begin
  ajpr_en <= 1'b1;
  vblank <= 1'b0;
  jp1d <= 2'bxx;
  jp2d <= 2'bxx;

  repeat (2) @(posedge slow_cp1_posedge) ;
  nRES <= 1'b1;

  // cycle 0
  next_tick();
  next_tick();
  assert(~ajpr_busy & &jpclk & ~jpout);

  // cycle 1
  vblank <= 1'b1;
  next_tick();
  assert(ajpr_busy & &jpclk & jpout);

  // cycle 2
  next_tick();
  assert(ajpr_busy & &jpclk & jpout);

  // cycle 3
  next_tick();
  assert(ajpr_busy & &jpclk & ~jpout);

  // cycles 4 - 33
  jp1d <= 2'b01;
  jp2d <= 2'b10;
  repeat (15) begin
    next_tick();
    assert(ajpr_busy & ~|jpclk & ~jpout);

    next_tick();
    assert(ajpr_busy & &jpclk & ~jpout);

    jp1d <= ~jp1d;
    jp2d <= ~jp2d;
  end

  // cycle 34
  next_tick();
  assert(~ajpr_busy & ~|jpclk & ~jpout);
  assert(joy1 == ~16'hAAAA);
  assert(joy2 == ~16'h5555);
  assert(joy3 == ~16'h5555);
  assert(joy4 == ~16'hAAAA);

  // cycle 35
  next_tick();
  assert(~ajpr_busy & &jpclk & ~jpout);

  jp1d <= 2'bxx;
  jp2d <= 2'bxx;

  repeat (10) begin
    next_tick();
    assert(~ajpr_busy & &jpclk & ~jpout);
  end

  $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s ajpr_tb -o ajpr_tb.vvp -f s-cpu.files ajpr_tb.sv && ./ajpr_tb.vvp"
// End:
