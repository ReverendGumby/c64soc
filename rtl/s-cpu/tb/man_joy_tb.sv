`timescale 1us / 1ns

module man_joy_tb();

reg        nRES;
reg        clk, mclk;
reg [12:0] ocio_ra;
reg [7:0]  ocio_d_i;
wire [7:0] ocio_d_o;
wire       ocio_d_oe;
reg        ocio_nrd, ocio_nwr;
wire [2:1] jpclk;
wire [2:0] jpout;
reg [1:0]  jp1d;
reg [4:0]  jp2d;
reg        vblank;
reg [23:0] jpreg, jpreg_in;

initial begin
  $dumpfile("man_joy_tb.vcd");
  $dumpvars();
end

s_cpu_clkgen clkgen
  (
   .CLK_RESET(1'b0),
   .CLK(mclk),

   .SLOWCK_SEL(1'b0),
   .CLK_HOLD(1'b0),

   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge)
   );

s_cpu_ocio ocio
  (
   .nRES(nRES),
   .CLK(mclk),
   .CP1_POSEDGE(cp1_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .HOLD(1'b0),
   .SLOWCK_SEL(),

   .RA(ocio_ra),
   .D_I(ocio_d_i),
   .D_O(ocio_d_o),
   .D_OE(ocio_d_oe),
   .nRD(ocio_nrd),
   .nWR(ocio_nwr),

   .JPCLK(jpclk),
   .JPOUT(jpout),
   .JP1D(jp1d),
   .JP2D(jp2d),
   .JPIO_I(),
   .JPIO_O()
   );

initial begin
  clk = 1;
  nRES = 0;
end

initial forever begin :ckgen
  #(250.0/(21477.272*3)) clk = ~clk;
end

localparam [1:0] mclk_ccnt_max = 2'd2;
reg [1:0] mclk_ccnt;
initial begin
  mclk_ccnt = 2'd0;
  mclk = 'b0;
end
always @(posedge clk) begin
  if (mclk_ccnt == mclk_ccnt_max) begin
    mclk_ccnt <= 2'd0;
    mclk <= ~mclk;
  end
  else
    mclk_ccnt <= mclk_ccnt + 1'd1;
end

task next_mclk();
  @(posedge mclk) ;
endtask

task next_cp1_posedge_start();
  while (~cp1_posedge)
    @(posedge mclk) ;
endtask  

task next_cp1_posedge_end();
  next_cp1_posedge_start();
  next_mclk();
endtask  

task next_cp1_posedge();
  @(posedge mclk) ;
  next_cp1_posedge_end();
endtask

task set_reg(input [15:0] r, input [7:0] v);
  begin
    next_cp1_posedge_start();

    ocio_ra <= r[12:0];
    ocio_d_i <= v;
    ocio_nwr <= 1'b0;

    next_cp1_posedge_end();
    next_cp1_posedge_start();

    ocio_ra <= 13'hx;
    ocio_d_i <= 8'hx;
    ocio_nwr <= 1'b1;
  end
endtask

task get_verify_reg(input [15:0] r, input [7:0] v, input [7:0] m);
  begin
    next_cp1_posedge_start();

    ocio_ra <= r[12:0];
    ocio_nrd <= 1'b0;

    next_cp1_posedge_end();
    next_cp1_posedge_start();

    ocio_ra <= 13'hx;
    ocio_nrd <= 1'b1;
    assert((ocio_d_o & m) == (v & m));
  end
endtask

always @(posedge jpout[0]) begin
  jpreg <= jpreg_in;
end

always @(posedge jpclk) begin
  jpreg <= jpreg >> 1;
end

always @* begin
  jp1d[0] = ~jpreg[0];
  jp2d[0] = ~jpreg[0];
end

initial begin
  ocio_ra = 4'hx;
  ocio_d_i = 8'hx;
  ocio_nrd = 1'b1;
  ocio_nwr = 1'b1;

  jpreg_in = 24'hff0010; // Start is pressed

  repeat (2) @(posedge cp2_negedge) ;
  nRES <= 1'b1;

  set_reg(16'h4016, 8'h01);
  next_cp1_posedge();
  set_reg(16'h4016, 8'h00);
  next_cp1_posedge();

  for (int i = 0; i < 24; i++) begin :ver
    get_verify_reg(16'h4016, {7'bx, jpreg_in[i]}, 8'h01);
    next_cp1_posedge();
  end

  $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s man_joy_tb -o man_joy_tb.vvp -f s-cpu.files man_joy_tb.sv && ./man_joy_tb.vvp"
// End:
