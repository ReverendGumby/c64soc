`timescale 1us / 1ns

//`define TEST_HOLD

module dmac_tb();

reg        nRES, hold;
reg        clk, mclk;
reg [1:0]  cc_cnt;
wire       cc;
wire       slowck_sel, cpu_gate_active;
wire       refresh_en, slow_refresh_en;
wire       cp1_posedge, cp1_negedge, cp2_posedge, cp2_negedge;
wire       slow_cp1_posedge;
wire       slow_gate_active;
reg [12:0] ocio_ra;
reg [7:0]  ocio_d_i, ocio_d_i_mask;
wire [7:0] ocio_d_o;
wire       ocio_d_oe;
reg        ocio_nrd, ocio_nwr;
wire [23:0] ocio_a_o;
wire [7:0] ocio_pa_o;
wire       ocio_a_oe, ocio_pa_oe;
wire       ocio_nwr_o, ocio_nrd_o, ocio_npard_o, ocio_npawr_o;
reg        hblank, vblank;
reg [23:0] a;
wire [7:0] pa;
wire       nrd, nwr, npard, npawr;
reg [7:0]  bo;
reg [23:0] hdma_table_cur [0:7];
reg [7:0]  hdma_table [0:'hffffff];
reg [23:0] hdma_indir_data [0:7];
reg [7:0]  hdma_mode [0:7];
integer    hdma_bc [0:7];
reg [7:0]  ntrl [0:7];
integer    dma_active;
integer    hdma_cyc;
reg        hdma_active;
reg [7:0]  hdmaen;
reg [7:0]  hdma_xfer;
reg [7:0]  hdma_done;

initial begin
  $timeformat(-9, 0, " ns", 1);

  $dumpfile("dmac_tb.vcd");
  $dumpvars();
end

s_cpu_clkgen clkgen
  (
   .CLK_RESET(1'b0),
   .CLK(mclk),

   .SLOWCK_SEL(slowck_sel),
   .CLK_HOLD(hold),
   .FAST_CPU(1'b0),
   .SLOW_CPU(1'b0),
   .REFRESH_EN(refresh_en),

   .CP1_POSEDGE(cp1_posedge),
   .CP1_NEGEDGE(cp1_negedge),
   .CP2_POSEDGE(cp2_posedge),
   .CP2_NEGEDGE(cp2_negedge),

   .CPU_GATE_ACTIVE(cpu_gate_active),

   .SLOW_CP1_POSEDGE(slow_cp1_posedge),
   .SLOW_GATE_ACTIVE(slow_gate_active),
   .SLOW_REFRESH_EN(slow_refresh_en)
   );

s_cpu_ocio ocio
  (
   .nRES(nRES),
   .CLK(mclk),
   .CP1_POSEDGE(cp1_posedge),
   .CP2_NEGEDGE(cp2_negedge),
   .SLOW_CP1_POSEDGE(slow_cp1_posedge),
   .HOLD(hold),
   .SLOWCK_SEL(slowck_sel),
   .REFRESH_EN(refresh_en),
   .SLOW_GATE_ACTIVE(slow_gate_active),
   .SLOW_REFRESH_EN(slow_refresh_en),

   .RA(ocio_ra),
   .D_I(ocio_d_i ^ ocio_d_i_mask),
   .D_O(ocio_d_o),
   .D_OE(ocio_d_oe),
   .nRD(ocio_nrd),
   .nWR(ocio_nwr),

   .CC(cc),
   .HBLANK(hblank),
   .VBLANK(vblank),

   .A_O(ocio_a_o),
   .A_OE(ocio_a_oe),
   .nWR_O(ocio_nwr_o),
   .nRD_O(ocio_nrd_o),
   .PA_O(ocio_pa_o),
   .PA_OE(ocio_pa_oe),
   .nPARD_O(ocio_npard_o),
   .nPAWR_O(ocio_npawr_o)
   );

`define dreg ocio.dmac.dreg[ch]

`define DMAC_DMAPx `dreg['h0]
`define DMAC_BBADx `dreg['h1]
`define DMAC_A1TxL `dreg['h2]
`define DMAC_A1TxH `dreg['h3]
`define DMAC_A1TBx `dreg['h4]
`define DMAC_DASxL `dreg['h5]
`define DMAC_DASxH `dreg['h6]
`define DMAC_DASBx `dreg['h7]
`define DMAC_A2AxL `dreg['h8]
`define DMAC_A2AxH `dreg['h9]
`define DMAC_NTRLx `dreg['hA]

`define INDIR hdma_mode[ch][6]

always @* begin
  a = 24'bx;
  if (~ocio_nwr | ~ocio_nrd)
    a = {11'h2, ocio_ra};
  else if (ocio_a_oe)
    a = ocio_a_o;
end

assign pa = ocio_pa_oe ? ocio_pa_o : 8'bx;

assign nrd = ocio_nrd & ocio_nrd_o;
assign nwr = ocio_nwr & ocio_nwr_o;
assign npard = ocio_npard_o;
assign npawr = ocio_npawr_o;

`define REG_MDMAEN  16'h420B
`define REG_HDMAEN  16'h420C

`define REG_DMAP0   16'h4300
`define REG_BBAD0   16'h4301
`define REG_A1T0L   16'h4302
`define REG_A1T0H   16'h4303
`define REG_A1B0    16'h4304
`define REG_DAS0L   16'h4305
`define REG_DAS0H   16'h4306
`define REG_DASB0   16'h4307
`define REG_A2A0L   16'h4308
`define REG_A2A0H   16'h4309
`define REG_NTRL0   16'h430A

initial begin
  clk = 1;
  nRES = 0;
  cc_cnt = 0;
  hold = 0;
end

initial forever begin :ckgen
  #(250.0/(21477.272*3)) clk = ~clk;
end

localparam [1:0] mclk_ccnt_max = 2'd2;
reg [1:0] mclk_ccnt;
initial begin
  mclk_ccnt = 2'd0;
  mclk = 'b0;
end
always @(posedge clk) begin
  if (mclk_ccnt == mclk_ccnt_max) begin
    mclk_ccnt <= 2'd0;
    mclk <= ~mclk;
  end
  else
    mclk_ccnt <= mclk_ccnt + 1'd1;
end

integer hold_cnt = 0;
integer hold_luck = 0;
initial ocio_d_i_mask = 0;
`ifdef TEST_HOLD
always @(posedge mclk) begin
  if (cp1_posedge) begin
    if (++hold_luck == 3) begin
      hold <= 1'b1;
      ocio_d_i_mask <= 8'bx;
      hold_cnt = 5;
      hold_luck = 0;
    end
  end
  else if (hold_cnt) begin
    if (--hold_cnt == 0) begin
      hold <= 1'b0;
      ocio_d_i_mask <= 8'b0;
    end
  end
end
`endif

always @(posedge mclk) begin :ccgen
  cc_cnt <= cc_cnt + 1;
end

assign cc = cc_cnt == 3;

task next_mclk();
  @(posedge mclk) ;
  while (hold)
    @(posedge mclk) ;
endtask

task next_cp1_posedge_start();
  while (~cp1_posedge)
    @(posedge mclk) ;
endtask  

task next_cp1_posedge_end();
  next_cp1_posedge_start();
  next_mclk();
endtask  

task next_cp1_posedge();
  @(posedge mclk) ;
  next_cp1_posedge_end();
endtask

task set_reg(input [15:0] r, input [7:0] v);
  begin
    next_cp1_posedge_start();

    ocio_ra <= r[12:0];
    ocio_d_i <= v;
    ocio_nwr <= 1'b0;

    next_cp1_posedge_end();
    next_cp1_posedge_start();

    ocio_ra <= 13'hx;
    ocio_d_i <= 8'hx;
    ocio_nwr <= 1'b1;
  end
endtask

task get_verify_reg(input [15:0] r, input [7:0] v);
  begin
    next_cp1_posedge_start();

    ocio_ra <= r[12:0];
    ocio_nrd <= 1'b0;

    next_cp1_posedge_end();
    next_cp1_posedge_start();

    ocio_ra <= 13'hx;
    ocio_nrd <= 1'b1;
    assert(ocio_d_o == v);
  end
endtask

task drive_hdma_table(int ch);
  ocio_d_i <= hdma_table[hdma_table_cur[ch]];
endtask

initial begin
  hdma_bc[0] = 1;
  hdma_bc[1] = 2;
  hdma_bc[2] = 2;
  hdma_bc[3] = 4;
  hdma_bc[4] = 4;
  hdma_bc[5] = 4;
  hdma_bc[6] = 2;
  hdma_bc[7] = 4;
end

function [7:0] hdma_boff(reg [2:0] mode, integer bc);
  hdma_boff = 8'd0;
  case (mode)
    3'd0: ;
    3'd1: hdma_boff[0] = bc[0];
    3'd2: ;
    3'd3: hdma_boff[0] = bc[1];
    3'd4: hdma_boff[1:0] = bc[1:0];
    3'd5: hdma_boff[0] = bc[0];
    3'd6: ;
    3'd7: hdma_boff[0] = bc[1];
  endcase
endfunction

task go_to_hpos(input [8:0] c);
  do
    @ocio.gen.hcnt ;
  while (ocio.gen.hcnt != c);
  @(posedge mclk) ;
endtask

//////////////////////////////////////////////////////////////////////

wire cpu_act = clkgen.cpu_act;

task do_dma_idle();
  assert(~slowck_sel);
  assert(~cpu_gate_active | cpu_act);
  assert(~ocio_pa_oe);
endtask

task do_dma_enter();
  assert(slowck_sel);
  assert(~cpu_gate_active | cpu_act);
  assert(~ocio_pa_oe);
  assert(~ocio_a_oe);
  assert(nrd & nwr);
  assert(npard & npawr);
endtask

task do_dma_exit();
  assert(~slowck_sel);
  assert(cpu_gate_active);
  assert(nrd & nwr);
  assert(npard & npawr);
endtask

//////////////////////////////////////////////////////////////////////
// General-purpose DMA test

task gpdma_test();
  $display("GP-DMA Test");

  dma_active = 0;

  // 2 identical channels
  // - 9 bytes long to verify B-bus offset starts at 0
  set_reg(16'h10 + `REG_DMAP0, 8'h04);
  set_reg(16'h10 + `REG_BBAD0, 8'h18);
  set_reg(16'h10 + `REG_A1T0L, 8'h2A);
  set_reg(16'h10 + `REG_A1T0H, 8'hCD);
  set_reg(16'h10 + `REG_A1B0,  8'h00);
  set_reg(16'h10 + `REG_DAS0L, 8'h09);
  set_reg(16'h10 + `REG_DAS0H, 8'h00);
  set_reg(16'h10 + `REG_DASB0, 8'hxx);
  set_reg(16'h10 + `REG_A2A0L, 8'hxx);
  set_reg(16'h10 + `REG_A2A0H, 8'hxx);
  set_reg(16'h10 + `REG_NTRL0, 8'hxx);

  set_reg(16'h40 + `REG_DMAP0, 8'h04);
  set_reg(16'h40 + `REG_BBAD0, 8'h18);
  set_reg(16'h40 + `REG_A1T0L, 8'h2A);
  set_reg(16'h40 + `REG_A1T0H, 8'hCD);
  set_reg(16'h40 + `REG_A1B0,  8'h00);
  set_reg(16'h40 + `REG_DAS0L, 8'h09);
  set_reg(16'h40 + `REG_DAS0H, 8'h00);
  set_reg(16'h40 + `REG_DASB0, 8'hxx);
  set_reg(16'h40 + `REG_A2A0L, 8'hxx);
  set_reg(16'h40 + `REG_A2A0H, 8'hxx);
  set_reg(16'h40 + `REG_NTRL0, 8'hxx);

  do_dma_idle();

  set_reg(`REG_MDMAEN, 8'h12);
  dma_active++;

  // Reference: https://snesdev.mesen.ca/wiki/index.php?title=SNES_Timing#DMA

  // After $420b is written, the CPU gets one more CPU cycle before
  // the pause (on a standard "STA $420b", this would be the opcode
  // fetch for the next instruction). Now, after the pause, wait 2-8
  // master cycles to reach a whole multiple of 8 master cycles since
  // reset.
  next_mclk();
  do_dma_enter();

  for (int j = 0; j < 2; j++) begin
    $display("GP-DMA Transfer %1d", j);

    // Then perform the DMA: 8 master cycles overhead...
    next_cp1_posedge();
    assert(cpu_gate_active);
    assert(nrd & nwr);
    assert(npard & npawr);

    // ... and 8 master cycles per byte per channel, ...
    for (int i = 0; i < 9; i++) begin
      bo = i % 4;
      $display(i, bo);

      next_cp1_posedge();

      assert(a == 24'h00CD2A + i);
      assert(pa == 8'h18 + bo);
      assert(slowck_sel);
      assert(ocio_pa_oe);
      assert(~nrd & nwr);
      assert(npard & ~npawr);
      assert(~ocio_d_oe);
    end
  end

  // ... and an extra 8 master cycles overhead for the whole thing.
  // Then wait 2-8 master cycles to reach a whole number of CPU clock
  // cycles since the pause...
  next_cp1_posedge();
  do_dma_exit();
  // ... and only then resume S-CPU execution.
  next_cp1_posedge();
  do_dma_idle();
  dma_active--;

  get_verify_reg(16'h10 + `REG_A1T0L, 8'h33);
  get_verify_reg(16'h10 + `REG_A1T0H, 8'hCD);
  get_verify_reg(16'h10 + `REG_DAS0L, 8'h00);
  get_verify_reg(16'h10 + `REG_DAS0H, 8'h00);

  get_verify_reg(16'h40 + `REG_A1T0L, 8'h33);
  get_verify_reg(16'h40 + `REG_A1T0H, 8'hCD);
  get_verify_reg(16'h40 + `REG_DAS0L, 8'h00);
  get_verify_reg(16'h40 + `REG_DAS0H, 8'h00);

  #2;
  $display("GP-DMA Test Done\n");
endtask


//////////////////////////////////////////////////////////////////////
// H-DMA emulation

// TODO: Figure out what's actually happening on HW, and align this.

initial begin
  hdma_active = 0;
  hdma_cyc = 0;
end

always @(negedge cp1_posedge) begin
  if (hdma_cyc) begin
    if (~hdma_active)
      hdma_active <= 1;
    hdma_cyc --;
  end
  else if (hdma_active)
    hdma_active <= 0;
end

// For all active channels, the HDMA registers are initialized at
// about V=0 H=6. The overhead is ~18 master cycles, plus 8 master
// cycles for each channel set for direct HDMA and 24 master cycles
// for each channel set for indirect HDMA.

task hdma_init_count_cyc(output integer c);
  c = 1;
  for (int ch = 0; ch < 8; ch++) begin
    if (~hdmaen[ch])
      continue;
    c += 1;
    if (`INDIR)
      c += 2;
  end
endtask


task do_hdma_init();
  {hblank, vblank} <= 'b11;
  repeat (16) next_mclk();
  {hblank, vblank} <= 'b00;
  hdma_init_count_cyc(hdma_cyc);
  if (dma_active++ == 0) begin
    next_cp1_posedge();
    if (hdmaen)
      do_dma_enter();
  end
  else begin
    // Mesen2 says there's always an extra cycle here
    next_cp1_posedge();
    assert(cpu_gate_active);
  end
  next_cp1_posedge();

  $display("  H-DMA Initialization");
  if (hdmaen)
    assert(cpu_gate_active);

  hdma_done = 0;
  hdma_xfer = 0;

  for (int ch = 0; ch < 8; ch++) begin
    ntrl[ch] = `DMAC_NTRLx; // in case of mid-frame enable
    if (~hdmaen[ch])
      continue;

    // Initialize channel registers
    ntrl[ch] = 1;
    do_hdma_load_ch(ch);
  end

  if (--dma_active == 0) begin
    if (hdmaen)
      do_dma_exit();

    next_cp1_posedge();
    do_dma_idle();
  end
endtask


task hdma_row_count_cyc(output integer c);
  c = 1;
  if (hdmaen & ~hdma_done) begin
    for (int ch = 0; ch < 8; ch++) begin
      if (hdmaen[ch] & ~hdma_done[ch])
        c += hdma_bc[hdma_mode[ch][2:0]];
    end
    for (int ch = 0; ch < 8; ch++) begin
      if (~hdmaen[ch])
        continue;
      c += 1;
      if (`INDIR)
        c += 2;
    end
  end
endtask


// The actual HDMA transfer begins at dot 278 of the scanline
// [shortly after HBLANK assertion], for every visible scanline.
task do_hdma_row();
reg hdma_all_done;

  hblank <= 'b1;
  hdma_row_count_cyc(hdma_cyc);

  hdma_all_done = (hdmaen & ~hdma_done) == 0;
  if (dma_active++ == 0) begin
    next_cp1_posedge();
    if (~hdma_all_done) begin
      do_dma_enter();
    end
  end
  else begin
    // Mesen2 says there's always an extra cycle here
    next_cp1_posedge();
    assert(cpu_gate_active);
  end
  // For each scanline during which HDMA is active (i.e. at least
  // one channel has not yet terminated for the frame), there are
  // ~18 master cycles overhead.
  next_cp1_posedge();
  if (~hdma_all_done)
    assert(cpu_gate_active);

  if (~hdma_all_done) begin
    for (int ch = 0; ch < 8; ch++) begin
      if (hdmaen[ch] & ~hdma_done[ch])
        do_hdma_xfer_ch(ch);
    end
    for (int ch = 0; ch < 8; ch++) begin
      if (hdmaen[ch] & ~hdma_done[ch])
        do_hdma_load_ch(ch);
    end
  end
  else if (dma_active > 1) begin
    //$display("    idle cycle");
    assert(nrd & nwr);
    assert(npard & npawr);
  end

  hblank <= 'b0;
  if (--dma_active == 0) begin
    next_cp1_posedge();
    if (~hdma_all_done) begin
      do_dma_idle();
    end
  end
endtask


task do_hdma_xfer_ch(int ch);
reg [23:0] ea;
  if (hdma_xfer[ch] == 1) begin
    //$display("    Channel %1d xfer", ch);

    // Then 8 cycles per byte transferred are used.
    for (int i = 0; i < hdma_bc[hdma_mode[ch][2:0]]; i++) begin
      ea = `INDIR ? hdma_indir_data[ch] : hdma_table_cur[ch];
      //$display("      Byte %1d  %x", i, ea);
      assert(a == ea);
      assert(pa == `DMAC_BBADx + hdma_boff(hdma_mode[ch][2:0], i));
      assert(slowck_sel);
      assert(ocio_a_oe);
      assert(~nrd & nwr);
      assert(ocio_pa_oe);
      assert(npard & ~npawr);
      assert(~ocio_d_oe);

      if (~`INDIR) begin
        drive_hdma_table(ch);
        hdma_table_cur[ch] = hdma_table_cur[ch] + 1;
      end
      else
        hdma_indir_data[ch] = hdma_indir_data[ch] + 1;

      next_cp1_posedge();
    end

    ocio_d_i <= 8'hx;
  end
endtask


task do_hdma_load_ch(int ch);
logic latch;
  //$display("    Channel %1d load", ch);

  // Each active channel incurs another 8 master cycles overhead
  // for every scanline, whether or not a transfer actually
  // occurs.

  ntrl[ch][6:0] = ntrl[ch][6:0] - 1;
  hdma_xfer[ch] = (ntrl[ch][7] == 1 /* repeat mode */);
  latch = ntrl[ch][6:0] == 0;

  // When line count (post-decrement) reaches 0, read and latch new
  // info from the table into registers. (The NTRLx read cycle always
  // occurs.)
  //$display("      Table line count / repeat %s", 
  //         latch ? "(latched)" : "(ignored)");
  assert(a == hdma_table_cur[ch]);
  assert(slowck_sel);
  assert(ocio_a_oe);
  assert(~nrd & nwr);
  assert(npard & npawr);
  assert(~ocio_d_oe);
  drive_hdma_table(ch);

  if (latch) begin
    ntrl[ch] = hdma_table[hdma_table_cur[ch]];
    hdma_table_cur[ch] = hdma_table_cur[ch] + 1;

    // If a new indirect address is required, 16 master cycles are
    // taken to load it.
    if (`INDIR) begin
      //$display("      Table address low");
      next_cp1_posedge();

      hdma_indir_data[ch][7:0] = hdma_table[hdma_table_cur[ch]];
      assert(a == hdma_table_cur[ch]);
      assert(slowck_sel);
      assert(ocio_a_oe);
      assert(~nrd & nwr);
      assert(ocio_pa_oe);
      assert(npard & npawr);
      assert(~ocio_d_oe);
      drive_hdma_table(ch);
      hdma_table_cur[ch] = hdma_table_cur[ch] + 1;
      //$display("      Table address high");
      next_cp1_posedge();

      hdma_indir_data[ch][15:8] = hdma_table[hdma_table_cur[ch]];
      assert(a == hdma_table_cur[ch]);
      assert(slowck_sel);
      assert(ocio_a_oe);
      assert(~nrd & nwr);
      assert(ocio_pa_oe);
      assert(npard & npawr);
      assert(~ocio_d_oe);
      drive_hdma_table(ch);
      hdma_table_cur[ch] = hdma_table_cur[ch] + 1;
    end

    // Reading $00 into NTRLx deactivates this channel for the rest of
    // the frame.
    if (ntrl[ch] == 0)
      hdma_done[ch] = 1;

    // Do a transfer on the next H-blank.
    hdma_xfer[ch] = 1;
  end

  next_cp1_posedge();
  ocio_d_i <= 8'hx;
endtask


//////////////////////////////////////////////////////////////////////
// H-DMA Direct Test

task hdma_direct_test();
  $display("H-DMA Direct Test #1");

  // [from Bubsy] change BG1HOFS, BG2HOFS at several lines
  // CH7: mode 2, direct, A-Bus (table) $000F2E, B-Bus $210F
  hdma_mode[7] = 8'h02;
  hdma_table_cur[7] = 24'h000F2E;
  hdma_table[24'h000F2E + 0]  = 8'h18;
  hdma_table[24'h000F2E + 1]  = 8'h30;
  hdma_table[24'h000F2E + 2]  = 8'h00;
  hdma_table[24'h000F2E + 3]  = 8'h3F;
  hdma_table[24'h000F2E + 4]  = 8'h30;
  hdma_table[24'h000F2E + 5]  = 8'h00;
  hdma_table[24'h000F2E + 6]  = 8'h60;
  hdma_table[24'h000F2E + 7]  = 8'h60;
  hdma_table[24'h000F2E + 8]  = 8'h00;
  hdma_table[24'h000F2E + 9]  = 8'h01;
  hdma_table[24'h000F2E + 10] = 8'h90;
  hdma_table[24'h000F2E + 11] = 8'h00;
  hdma_table[24'h000F2E + 12] = 8'h00;
  // CH1: mode 2, direct, A-Bus (table) $001122, B-Bus $210D
  hdma_mode[1] = 8'h02;
  hdma_table_cur[1] = 24'h001122;
  hdma_table[24'h000F2E + 500] = 8'h68;
  hdma_table[24'h000F2E + 501] = 8'h58;
  hdma_table[24'h000F2E + 502] = 8'h04;
  hdma_table[24'h000F2E + 503] = 8'h69;
  hdma_table[24'h000F2E + 504] = 8'h58;
  hdma_table[24'h000F2E + 505] = 8'h04;
  hdma_table[24'h000F2E + 506] = 8'h01;
  hdma_table[24'h000F2E + 507] = 8'h68;
  hdma_table[24'h000F2E + 508] = 8'h19;
  hdma_table[24'h000F2E + 509] = 8'h00;

  dma_active = 0;

  set_reg(16'h10 + `REG_DMAP0, 8'h02);
  set_reg(16'h10 + `REG_BBAD0, 8'h0D);
  set_reg(16'h10 + `REG_A1T0L, 8'h22);
  set_reg(16'h10 + `REG_A1T0H, 8'h11);
  set_reg(16'h10 + `REG_A1B0,  8'h00);
  set_reg(16'h10 + `REG_DAS0L, 8'hxx);
  set_reg(16'h10 + `REG_DAS0H, 8'hxx);
  set_reg(16'h10 + `REG_DASB0, 8'hxx);
  set_reg(16'h10 + `REG_A2A0L, 8'hxx);
  set_reg(16'h10 + `REG_A2A0H, 8'hxx);
  set_reg(16'h10 + `REG_NTRL0, 8'hxx);

  set_reg(16'h70 + `REG_DMAP0, 8'h02);
  set_reg(16'h70 + `REG_BBAD0, 8'h0F);
  set_reg(16'h70 + `REG_A1T0L, 8'h2E);
  set_reg(16'h70 + `REG_A1T0H, 8'h0F);
  set_reg(16'h70 + `REG_A1B0,  8'h00);
  set_reg(16'h70 + `REG_DAS0L, 8'hxx);
  set_reg(16'h70 + `REG_DAS0H, 8'hxx);
  set_reg(16'h70 + `REG_DASB0, 8'hxx);
  set_reg(16'h70 + `REG_A2A0L, 8'hxx);
  set_reg(16'h70 + `REG_A2A0H, 8'hxx);
  set_reg(16'h70 + `REG_NTRL0, 8'hxx);

  hdmaen = 8'h82;
  set_reg(`REG_HDMAEN, hdmaen);

  do_hdma_init();
  do_hdma_verify_regs(1);

  for (int row = 0; row <= 224; row++) begin
    $display("  Row %03d", row);
    do_hdma_row();
    do_hdma_verify_regs();
  end

  #2;
  $display("H-DMA Direct Test Done\n");
endtask


task do_hdma_verify_regs(logic init = 0);
  for (int ch = 0; ch < 8; ch++) begin
    if (~hdmaen[ch])
      continue;

    get_verify_reg(ch * 16 + `REG_A2A0L, hdma_table_cur[ch][7:0]);
    get_verify_reg(ch * 16 + `REG_A2A0H, hdma_table_cur[ch][15:8]);
    get_verify_reg(ch * 16 + `REG_NTRL0, ntrl[ch]);
    if (`INDIR && !init) begin
      get_verify_reg(ch * 16 + `REG_DAS0L, hdma_indir_data[ch][7:0]);
      get_verify_reg(ch * 16 + `REG_DAS0H, hdma_indir_data[ch][15:8]);
    end
  end
endtask


//////////////////////////////////////////////////////////////////////
// H-DMA Indirect Test

task hdma_indirect_test();
  $display("H-DMA Indirect Test");

  // [from FF6] Flame-powered game title effect
  // CH2: mode 3, indirect, A-Bus (table) $7E78E4 (data) $7Exxxx, B-Bus $210D-0E
  hdma_mode[2] = 8'h43;
  hdma_table_cur[2] = 24'h7E78E4;
  hdma_table[24'h7E78E4 + 0]  = 8'hE4;
  hdma_table[24'h7E78E4 + 1]  = 8'h00;
  hdma_table[24'h7E78E4 + 2]  = 8'h3D;
  hdma_table[24'h7E78E4 + 3]  = 8'hFB;
  hdma_table[24'h7E78E4 + 4]  = 8'h90;
  hdma_table[24'h7E78E4 + 5]  = 8'h3E;
  hdma_table[24'h7E78E4 + 6]  = 8'h00;
  hdma_indir_data[2][23:16] = 8'h7E;

  // CH3: mode 3, indirect, A-Bus (table) $7E78EB (data) $7Exxxx, B-Bus $210F-10
  hdma_mode[3] = 8'h43;
  hdma_table_cur[3] = 24'h7E78EB;
  hdma_table[24'h7E78E4 + 7]  = 8'hE4;
  hdma_table[24'h7E78E4 + 8]  = 8'h80;
  hdma_table[24'h7E78E4 + 9]  = 8'h40;
  hdma_table[24'h7E78E4 + 10] = 8'hFB;
  hdma_table[24'h7E78E4 + 11] = 8'h10;
  hdma_table[24'h7E78E4 + 12] = 8'h42;
  hdma_table[24'h7E78E4 + 13] = 8'h00;
  hdma_indir_data[3][23:16] = 8'h7E;

  // CH4: mode 3, indirect, A-Bus (table) $7E78F2 (data) $7Exxxx, B-Bus $2111-12
  hdma_mode[4] = 8'h43;
  hdma_table_cur[4] = 24'h7E78F2;
  hdma_table[24'h7E78E4 + 14] = 8'hE4;
  hdma_table[24'h7E78E4 + 15] = 8'h00;
  hdma_table[24'h7E78E4 + 16] = 8'h44;
  hdma_table[24'h7E78E4 + 17] = 8'hFB;
  hdma_table[24'h7E78E4 + 18] = 8'h90;
  hdma_table[24'h7E78E4 + 19] = 8'h45;
  hdma_table[24'h7E78E4 + 20] = 8'h00;
  hdma_table[24'h7E78E4 + 21] = 8'hAA; // guard bytes
  hdma_table[24'h7E78E4 + 22] = 8'hAA; // guard bytes
  hdma_indir_data[4][23:16] = 8'h7E;

  dma_active = 0;

  set_reg(16'h20 + `REG_DMAP0, 8'h43);
  set_reg(16'h20 + `REG_BBAD0, 8'h0D);
  set_reg(16'h20 + `REG_A1T0L, 8'hE4);
  set_reg(16'h20 + `REG_A1T0H, 8'h78);
  set_reg(16'h20 + `REG_A1B0,  8'h7E);
  set_reg(16'h20 + `REG_DAS0L, 8'hxx);
  set_reg(16'h20 + `REG_DAS0H, 8'hxx);
  set_reg(16'h20 + `REG_DASB0, 8'h7E);
  set_reg(16'h20 + `REG_A2A0L, 8'hxx);
  set_reg(16'h20 + `REG_A2A0H, 8'hxx);
  set_reg(16'h20 + `REG_NTRL0, 8'hxx);

  set_reg(16'h30 + `REG_DMAP0, 8'h43);
  set_reg(16'h30 + `REG_BBAD0, 8'h0F);
  set_reg(16'h30 + `REG_A1T0L, 8'hEB);
  set_reg(16'h30 + `REG_A1T0H, 8'h78);
  set_reg(16'h30 + `REG_A1B0,  8'h7E);
  set_reg(16'h30 + `REG_DAS0L, 8'hxx);
  set_reg(16'h30 + `REG_DAS0H, 8'hxx);
  set_reg(16'h30 + `REG_DASB0, 8'h7E);
  set_reg(16'h30 + `REG_A2A0L, 8'hxx);
  set_reg(16'h30 + `REG_A2A0H, 8'hxx);
  set_reg(16'h30 + `REG_NTRL0, 8'hxx);

  set_reg(16'h40 + `REG_DMAP0, 8'h43);
  set_reg(16'h40 + `REG_BBAD0, 8'h11);
  set_reg(16'h40 + `REG_A1T0L, 8'hF2);
  set_reg(16'h40 + `REG_A1T0H, 8'h78);
  set_reg(16'h40 + `REG_A1B0,  8'h7E);
  set_reg(16'h40 + `REG_DAS0L, 8'hxx);
  set_reg(16'h40 + `REG_DAS0H, 8'hxx);
  set_reg(16'h40 + `REG_DASB0, 8'h7E);
  set_reg(16'h40 + `REG_A2A0L, 8'hxx);
  set_reg(16'h40 + `REG_A2A0H, 8'hxx);
  set_reg(16'h40 + `REG_NTRL0, 8'hxx);

  hdmaen = 8'h1C;
  set_reg(`REG_HDMAEN, hdmaen);

  do_hdma_init();
  do_hdma_verify_regs(1);

  for (int row = 0; row <= 224; row++) begin
    $display("  Row %03d", row);
    do_hdma_row();
    do_hdma_verify_regs();
  end

  #2;
  $display("H-DMA Indirect Test #1 Done\n");
endtask


task hdma_indirect_test2();
  $display("H-DMA Indirect Test #2");

  // [from FF6] Battle screen
  // CH0: mode 3, indirect, A-Bus (table) $C2D1F2 (data) $7Exxxx, B-Bus $210D-0E
  hdma_mode[0] = 8'h43;
  hdma_table_cur[0] = 24'hC2D1F2;
  hdma_table[24'hC2D1F2 + 0]  = 8'h40;
  hdma_table[24'hC2D1F2 + 1]  = 8'hF5;
  hdma_table[24'hC2D1F2 + 2]  = 8'h43;
  hdma_table[24'hC2D1F2 + 3]  = 8'h40;
  hdma_table[24'hC2D1F2 + 4]  = 8'hF5;
  hdma_table[24'hC2D1F2 + 5]  = 8'h43;
  hdma_table[24'hC2D1F2 + 6]  = 8'h17;
  hdma_table[24'hC2D1F2 + 7]  = 8'hF5;
  hdma_table[24'hC2D1F2 + 8]  = 8'h43;
  hdma_table[24'hC2D1F2 + 9]  = 8'hC9;
  hdma_table[24'hC2D1F2 + 10] = 8'h51;
  hdma_table[24'hC2D1F2 + 11] = 8'h46;
  hdma_table[24'hC2D1F2 + 12] = 8'h00;
  hdma_table[24'hC2D1F2 + 13] = 8'hA0; // guard bytes
  hdma_table[24'hC2D1F2 + 14] = 8'h75; // guard bytes
  hdma_indir_data[0][23:16] = 8'h7E;

  // CH1: mode 3, indirect, A-Bus (table) $C2D225 (data) $7Exxxx, B-Bus $210F-10
  hdma_mode[1] = 8'h43;
  hdma_table_cur[1] = 24'hC2D225;
  hdma_table[24'hC2D225 + 0]  = 8'hA0;
  hdma_table[24'hC2D225 + 1]  = 8'h75;
  hdma_table[24'hC2D225 + 2]  = 8'h47;
  hdma_table[24'hC2D225 + 3]  = 8'hA0;
  hdma_table[24'hC2D225 + 4]  = 8'h75;
  hdma_table[24'hC2D225 + 5]  = 8'h47;
  hdma_table[24'hC2D225 + 6]  = 8'hA0;
  hdma_table[24'hC2D225 + 7]  = 8'h75;
  hdma_table[24'hC2D225 + 8]  = 8'h47;
  hdma_table[24'hC2D225 + 9]  = 8'hA0;
  hdma_table[24'hC2D225 + 10] = 8'h75;
  hdma_table[24'hC2D225 + 11] = 8'h47;
  hdma_table[24'hC2D225 + 12] = 8'h97;
  hdma_table[24'hC2D225 + 13] = 8'h75;
  hdma_table[24'hC2D225 + 14] = 8'h47;
  hdma_table[24'hC2D225 + 15] = 8'hC9;
  hdma_table[24'hC2D225 + 16] = 8'hD1;
  hdma_table[24'hC2D225 + 17] = 8'h49;
  hdma_table[24'hC2D225 + 18] = 8'h00;
  hdma_table[24'hC2D225 + 19] = 8'hA0; // guard bytes
  hdma_table[24'hC2D225 + 20] = 8'hF5; // guard bytes
  hdma_indir_data[1][23:16] = 8'h7E;

  // CH2: mode 3, indirect, A-Bus (table) $C2D24B (data) $7Exxxx, B-Bus $2111-12
  hdma_mode[2] = 8'h43;
  hdma_table_cur[2] = 24'hC2D24B;
  hdma_table[24'hC2D24B + 0]  = 8'h40;
  hdma_table[24'hC2D24B + 1]  = 8'hF5;
  hdma_table[24'hC2D24B + 2]  = 8'h4A;
  hdma_table[24'hC2D24B + 3]  = 8'h40;
  hdma_table[24'hC2D24B + 4]  = 8'hF5;
  hdma_table[24'hC2D24B + 5]  = 8'h4A;
  hdma_table[24'hC2D24B + 6]  = 8'h17;
  hdma_table[24'hC2D24B + 7]  = 8'hF5;
  hdma_table[24'hC2D24B + 8]  = 8'h4A;
  hdma_table[24'hC2D24B + 9]  = 8'hC9;
  hdma_table[24'hC2D24B + 10] = 8'h51;
  hdma_table[24'hC2D24B + 11] = 8'h4D;
  hdma_table[24'hC2D24B + 12] = 8'h00;
  hdma_table[24'hC2D24B + 13] = 8'hE4; // guard bytes
  hdma_table[24'hC2D24B + 14] = 8'hF5; // guard bytes
  hdma_indir_data[2][23:16] = 8'h7E;

  // CH3: mode 4, indirect, A-Bus (table) $C2D12E (data) $7Exxxx, B-Bus $2105-08
  hdma_mode[3] = 8'h44;
  hdma_table_cur[3] = 24'hC2D12E;
  hdma_table[24'hC2D12E + 0]  = 8'h64;
  hdma_table[24'hC2D12E + 1]  = 8'h6F;
  hdma_table[24'hC2D12E + 2]  = 8'h89;
  hdma_table[24'hC2D12E + 3]  = 8'h33;
  hdma_table[24'hC2D12E + 4]  = 8'h6F;
  hdma_table[24'hC2D12E + 5]  = 8'h89;
  hdma_table[24'hC2D12E + 6]  = 8'h49;
  hdma_table[24'hC2D12E + 7]  = 8'h73;
  hdma_table[24'hC2D12E + 8]  = 8'h89;
  hdma_table[24'hC2D12E + 9]  = 8'h00;
  hdma_indir_data[3][23:16] = 8'h7E;

  // CH4: mode 4, indirect, A-Bus (table) $C2D138 (data) $7Exxxx, B-Bus $212F-32
  hdma_mode[4] = 8'h44;
  hdma_table_cur[4] = 24'hC2D138;
  hdma_table[24'hC2D138 + 0]  = 8'hF0;
  hdma_table[24'hC2D138 + 1]  = 8'h93;
  hdma_table[24'hC2D138 + 2]  = 8'h89;
  hdma_table[24'hC2D138 + 3]  = 8'hF0;
  hdma_table[24'hC2D138 + 4]  = 8'h53;
  hdma_table[24'hC2D138 + 5]  = 8'h8B;
  hdma_table[24'hC2D138 + 6]  = 8'h00;
  hdma_indir_data[4][23:16] = 8'h7E;

  // CH5: mode 4, indirect, A-Bus (table) $C2D13F (data) $7Exxxx, B-Bus $2126-29
  hdma_mode[5] = 8'h44;
  hdma_table_cur[5] = 24'hC2D13F;
  hdma_table[24'hC2D13F + 0]  = 8'hCC;
  hdma_table[24'hC2D13F + 1]  = 8'h1F;
  hdma_table[24'hC2D13F + 2]  = 8'h9F;
  hdma_table[24'hC2D13F + 3]  = 8'hCC;
  hdma_table[24'hC2D13F + 4]  = 8'h4F;
  hdma_table[24'hC2D13F + 5]  = 8'hA0;
  hdma_table[24'hC2D13F + 6]  = 8'hC8;
  hdma_table[24'hC2D13F + 7]  = 8'h7F;
  hdma_table[24'hC2D13F + 8]  = 8'h9C;
  hdma_table[24'hC2D13F + 9]  = 8'h00;
  hdma_table[24'hC2D13F + 10] = 8'hF0; // guard bytes
  hdma_table[24'hC2D13F + 11] = 8'h32; // guard bytes
  hdma_indir_data[5][23:16] = 8'h7E;

  // CH6: mode 4, indirect, A-Bus (table) $C2D0F4 (data) $7Exxxx, B-Bus $2109-0C
  hdma_mode[6] = 8'h44;
  hdma_table_cur[6] = 24'hC2D0F4;
  hdma_table[24'hC2D0F4 + 0]  = 8'h64;
  hdma_table[24'hC2D0F4 + 1]  = 8'h7B;
  hdma_table[24'hC2D0F4 + 2]  = 8'h89;
  hdma_table[24'hC2D0F4 + 3]  = 8'h33;
  hdma_table[24'hC2D0F4 + 4]  = 8'h7B;
  hdma_table[24'hC2D0F4 + 5]  = 8'h89;
  hdma_table[24'hC2D0F4 + 6]  = 8'h49;
  hdma_table[24'hC2D0F4 + 7]  = 8'h7F;
  hdma_table[24'hC2D0F4 + 8]  = 8'h89;
  hdma_table[24'hC2D0F4 + 9]  = 8'h00;
  hdma_indir_data[6][23:16] = 8'h7E;

  // CH7: mode 4, indirect, A-Bus (table) $C2D0FE (data) $7Exxxx, B-Bus $212A-2D
  hdma_mode[7] = 8'h44;
  hdma_table_cur[7] = 24'hC2D0FE;
  hdma_table[24'hC2D0FE + 0]  = 8'h04;
  hdma_table[24'hC2D0FE + 1]  = 8'h87;
  hdma_table[24'hC2D0FE + 2]  = 8'h89;
  hdma_table[24'hC2D0FE + 3]  = 8'h60;
  hdma_table[24'hC2D0FE + 4]  = 8'h8B;
  hdma_table[24'hC2D0FE + 5]  = 8'h89;
  hdma_table[24'hC2D0FE + 6]  = 8'h33;
  hdma_table[24'hC2D0FE + 7]  = 8'h8B;
  hdma_table[24'hC2D0FE + 8]  = 8'h89;
  hdma_table[24'hC2D0FE + 9]  = 8'h44;
  hdma_table[24'hC2D0FE + 10] = 8'h8F;
  hdma_table[24'hC2D0FE + 11] = 8'h89;
  hdma_table[24'hC2D0FE + 12] = 8'h05;
  hdma_table[24'hC2D0FE + 13] = 8'h87;
  hdma_table[24'hC2D0FE + 14] = 8'h89;
  hdma_table[24'hC2D0FE + 15] = 8'h00;
  hdma_table[24'hC2D0FE + 16] = 8'h64; // guard bytes
  hdma_table[24'hC2D0FE + 17] = 8'h7B; // guard bytes
  hdma_indir_data[7][23:16] = 8'h7E;

  dma_active = 0;

  set_reg(16'h00 + `REG_DMAP0, 8'h43);
  set_reg(16'h00 + `REG_BBAD0, 8'h0D);
  set_reg(16'h00 + `REG_A1T0L, 8'hF2);
  set_reg(16'h00 + `REG_A1T0H, 8'hD1);
  set_reg(16'h00 + `REG_A1B0,  8'hC2);
  set_reg(16'h00 + `REG_DAS0L, 8'hxx);
  set_reg(16'h00 + `REG_DAS0H, 8'hxx);
  set_reg(16'h00 + `REG_DASB0, 8'h7E);
  set_reg(16'h00 + `REG_A2A0L, 8'hxx);
  set_reg(16'h00 + `REG_A2A0H, 8'hxx);
  set_reg(16'h00 + `REG_NTRL0, 8'hxx);

  set_reg(16'h10 + `REG_DMAP0, 8'h43);
  set_reg(16'h10 + `REG_BBAD0, 8'h0F);
  set_reg(16'h10 + `REG_A1T0L, 8'h25);
  set_reg(16'h10 + `REG_A1T0H, 8'hD2);
  set_reg(16'h10 + `REG_A1B0,  8'hC2);
  set_reg(16'h10 + `REG_DAS0L, 8'hxx);
  set_reg(16'h10 + `REG_DAS0H, 8'hxx);
  set_reg(16'h10 + `REG_DASB0, 8'h7E);
  set_reg(16'h10 + `REG_A2A0L, 8'hxx);
  set_reg(16'h10 + `REG_A2A0H, 8'hxx);
  set_reg(16'h10 + `REG_NTRL0, 8'hxx);

  set_reg(16'h20 + `REG_DMAP0, 8'h43);
  set_reg(16'h20 + `REG_BBAD0, 8'h11);
  set_reg(16'h20 + `REG_A1T0L, 8'h4B);
  set_reg(16'h20 + `REG_A1T0H, 8'hD2);
  set_reg(16'h20 + `REG_A1B0,  8'hC2);
  set_reg(16'h20 + `REG_DAS0L, 8'hxx);
  set_reg(16'h20 + `REG_DAS0H, 8'hxx);
  set_reg(16'h20 + `REG_DASB0, 8'h7E);
  set_reg(16'h20 + `REG_A2A0L, 8'hxx);
  set_reg(16'h20 + `REG_A2A0H, 8'hxx);
  set_reg(16'h20 + `REG_NTRL0, 8'hxx);

  set_reg(16'h30 + `REG_DMAP0, 8'h44);
  set_reg(16'h30 + `REG_BBAD0, 8'h05);
  set_reg(16'h30 + `REG_A1T0L, 8'h2E);
  set_reg(16'h30 + `REG_A1T0H, 8'hD1);
  set_reg(16'h30 + `REG_A1B0,  8'hC2);
  set_reg(16'h30 + `REG_DAS0L, 8'hxx);
  set_reg(16'h30 + `REG_DAS0H, 8'hxx);
  set_reg(16'h30 + `REG_DASB0, 8'h7E);
  set_reg(16'h30 + `REG_A2A0L, 8'hxx);
  set_reg(16'h30 + `REG_A2A0H, 8'hxx);
  set_reg(16'h30 + `REG_NTRL0, 8'hxx);

  set_reg(16'h40 + `REG_DMAP0, 8'h44);
  set_reg(16'h40 + `REG_BBAD0, 8'h2F);
  set_reg(16'h40 + `REG_A1T0L, 8'h38);
  set_reg(16'h40 + `REG_A1T0H, 8'hD1);
  set_reg(16'h40 + `REG_A1B0,  8'hC2);
  set_reg(16'h40 + `REG_DAS0L, 8'hxx);
  set_reg(16'h40 + `REG_DAS0H, 8'hxx);
  set_reg(16'h40 + `REG_DASB0, 8'h7E);
  set_reg(16'h40 + `REG_A2A0L, 8'hxx);
  set_reg(16'h40 + `REG_A2A0H, 8'hxx);
  set_reg(16'h40 + `REG_NTRL0, 8'hxx);

  set_reg(16'h50 + `REG_DMAP0, 8'h44);
  set_reg(16'h50 + `REG_BBAD0, 8'h26);
  set_reg(16'h50 + `REG_A1T0L, 8'h3F);
  set_reg(16'h50 + `REG_A1T0H, 8'hD1);
  set_reg(16'h50 + `REG_A1B0,  8'hC2);
  set_reg(16'h50 + `REG_DAS0L, 8'hxx);
  set_reg(16'h50 + `REG_DAS0H, 8'hxx);
  set_reg(16'h50 + `REG_DASB0, 8'h7E);
  set_reg(16'h50 + `REG_A2A0L, 8'hxx);
  set_reg(16'h50 + `REG_A2A0H, 8'hxx);
  set_reg(16'h50 + `REG_NTRL0, 8'hxx);

  set_reg(16'h60 + `REG_DMAP0, 8'h44);
  set_reg(16'h60 + `REG_BBAD0, 8'h09);
  set_reg(16'h60 + `REG_A1T0L, 8'hF4);
  set_reg(16'h60 + `REG_A1T0H, 8'hD0);
  set_reg(16'h60 + `REG_A1B0,  8'hC2);
  set_reg(16'h60 + `REG_DAS0L, 8'hxx);
  set_reg(16'h60 + `REG_DAS0H, 8'hxx);
  set_reg(16'h60 + `REG_DASB0, 8'h7E);
  set_reg(16'h60 + `REG_A2A0L, 8'hxx);
  set_reg(16'h60 + `REG_A2A0H, 8'hxx);
  set_reg(16'h60 + `REG_NTRL0, 8'hxx);

  set_reg(16'h70 + `REG_DMAP0, 8'h44);
  set_reg(16'h70 + `REG_BBAD0, 8'h2A);
  set_reg(16'h70 + `REG_A1T0L, 8'hFE);
  set_reg(16'h70 + `REG_A1T0H, 8'hD0);
  set_reg(16'h70 + `REG_A1B0,  8'hC2);
  set_reg(16'h70 + `REG_DAS0L, 8'hxx);
  set_reg(16'h70 + `REG_DAS0H, 8'hxx);
  set_reg(16'h70 + `REG_DASB0, 8'h7E);
  set_reg(16'h70 + `REG_A2A0L, 8'hxx);
  set_reg(16'h70 + `REG_A2A0H, 8'hxx);
  set_reg(16'h70 + `REG_NTRL0, 8'hxx);

  hdmaen = 8'hFF;
  set_reg(`REG_HDMAEN, hdmaen);

  do_hdma_init();
  do_hdma_verify_regs(1);

  for (int row = 0; row <= 224; row++) begin
    $display("  Row %03d", row);
    do_hdma_row();
    do_hdma_verify_regs();
  end

  #2;
  $display("H-DMA Indirect Test #2 Done\n");
endtask


//////////////////////////////////////////////////////////////////////
// H-DMA Mid-frame Disable Tesst
//
// FF6 does this on start, frame 97.

task hdma_midframe_disable_test();
  $display("H-DMA Mid-frame Disable Test");

  // [from Bubsy] change BG1HOFS, BG2HOFS at several lines
  // CH7: mode 2, direct, A-Bus (table) $000F2E, B-Bus $210F
  hdma_mode[7] = 8'h02;
  hdma_table_cur[7] = 24'h000F2E;
  hdma_table[24'h000F2E + 0]  = 8'h18;
  hdma_table[24'h000F2E + 1]  = 8'h30;
  hdma_table[24'h000F2E + 2]  = 8'h00;
  hdma_table[24'h000F2E + 3]  = 8'h3F;
  hdma_table[24'h000F2E + 4]  = 8'h30;
  hdma_table[24'h000F2E + 5]  = 8'h00;
  hdma_table[24'h000F2E + 6]  = 8'h60;
  hdma_table[24'h000F2E + 7]  = 8'h60;
  hdma_table[24'h000F2E + 8]  = 8'h00;
  hdma_table[24'h000F2E + 9]  = 8'h01;
  hdma_table[24'h000F2E + 10] = 8'h90;
  hdma_table[24'h000F2E + 11] = 8'h00;
  hdma_table[24'h000F2E + 12] = 8'h00;
  // CH1: mode 2, direct, A-Bus (table) $001122, B-Bus $210D
  hdma_mode[1] = 8'h02;
  hdma_table_cur[1] = 24'h001122;
  hdma_table[24'h000F2E + 500] = 8'h68;
  hdma_table[24'h000F2E + 501] = 8'h58;
  hdma_table[24'h000F2E + 502] = 8'h04;
  hdma_table[24'h000F2E + 503] = 8'h69;
  hdma_table[24'h000F2E + 504] = 8'h58;
  hdma_table[24'h000F2E + 505] = 8'h04;
  hdma_table[24'h000F2E + 506] = 8'h01;
  hdma_table[24'h000F2E + 507] = 8'h68;
  hdma_table[24'h000F2E + 508] = 8'h19;
  hdma_table[24'h000F2E + 509] = 8'h00;

  dma_active = 0;

  set_reg(16'h10 + `REG_DMAP0, 8'h02);
  set_reg(16'h10 + `REG_BBAD0, 8'h0D);
  set_reg(16'h10 + `REG_A1T0L, 8'h22);
  set_reg(16'h10 + `REG_A1T0H, 8'h11);
  set_reg(16'h10 + `REG_A1B0,  8'h00);
  set_reg(16'h10 + `REG_DAS0L, 8'hxx);
  set_reg(16'h10 + `REG_DAS0H, 8'hxx);
  set_reg(16'h10 + `REG_DASB0, 8'hxx);
  set_reg(16'h10 + `REG_A2A0L, 8'hxx);
  set_reg(16'h10 + `REG_A2A0H, 8'hxx);
  set_reg(16'h10 + `REG_NTRL0, 8'hxx);

  set_reg(16'h70 + `REG_DMAP0, 8'h02);
  set_reg(16'h70 + `REG_BBAD0, 8'h0F);
  set_reg(16'h70 + `REG_A1T0L, 8'h2E);
  set_reg(16'h70 + `REG_A1T0H, 8'h0F);
  set_reg(16'h70 + `REG_A1B0,  8'h00);
  set_reg(16'h70 + `REG_DAS0L, 8'hxx);
  set_reg(16'h70 + `REG_DAS0H, 8'hxx);
  set_reg(16'h70 + `REG_DASB0, 8'hxx);
  set_reg(16'h70 + `REG_A2A0L, 8'hxx);
  set_reg(16'h70 + `REG_A2A0H, 8'hxx);
  set_reg(16'h70 + `REG_NTRL0, 8'hxx);

  hdmaen = 8'h82;
  set_reg(`REG_HDMAEN, hdmaen);

  do_hdma_init();
  do_hdma_verify_regs(1);

  for (int row = 0; row <= 10; row++) begin
    if (row == 5) begin
      $display("  Disabling H-DMA");
      hdmaen = 8'h00;
      set_reg(`REG_HDMAEN, hdmaen);
    end

    $display("  Row %03d", row);
    do_hdma_row();
    do_hdma_verify_regs();
  end

  #2;
  $display("H-DMA Mid-frame Disable Test Done\n");
endtask


//////////////////////////////////////////////////////////////////////
// H-DMA Mid-frame Enable Tesst
//
// Aladdin does this on IRQ, around scanline 8.

task hdma_midframe_enable_test();
  $display("H-DMA Mid-frame Enable Test");

  // [from Aladdin] change BG2HOFS at several lines
  // CH7: mode 2, direct, A-Bus (table) $7EF480, B-Bus $210F
  hdma_mode[4] = 8'h02;
  hdma_table_cur[4] = 24'h7EF480;
  hdma_table[24'h7EF480 + 0]  = 8'h01;
  hdma_table[24'h7EF480 + 1]  = 8'h31;
  hdma_table[24'h7EF480 + 2]  = 8'h00;
  hdma_table[24'h7EF480 + 3]  = 8'h04;
  hdma_table[24'h7EF480 + 4]  = 8'h31;
  hdma_table[24'h7EF480 + 5]  = 8'h00;
  hdma_table[24'h7EF480 + 6]  = 8'h10;
  hdma_table[24'h7EF480 + 7]  = 8'h18;
  hdma_table[24'h7EF480 + 8]  = 8'h00;
  hdma_table[24'h7EF480 + 9]  = 8'h11;
  hdma_table[24'h7EF480 + 10] = 8'h00;
  hdma_table[24'h7EF480 + 11] = 8'h00;
  hdma_table[24'h7EF480 + 12] = 8'h7F;
  hdma_table[24'h7EF480 + 13] = 8'h00;
  hdma_table[24'h7EF480 + 14] = 8'h00;
  hdma_table[24'h7EF480 + 15] = 8'h3C;
  hdma_table[24'h7EF480 + 16] = 8'h00;
  hdma_table[24'h7EF480 + 17] = 8'h00;
  hdma_table[24'h7EF480 + 18] = 8'h00;
  hdma_table[24'h7EF480 + 19] = 8'h00; // guard bytes
  hdma_table[24'h7EF480 + 20] = 8'h00; // guard bytes

  dma_active = 0;

  set_reg(16'h40 + `REG_DMAP0, 8'h02);
  set_reg(16'h40 + `REG_BBAD0, 8'h0F);
  set_reg(16'h40 + `REG_A1T0L, 8'h80);
  set_reg(16'h40 + `REG_A1T0H, 8'hF4);
  set_reg(16'h40 + `REG_A1B0,  8'h7E);
  set_reg(16'h40 + `REG_DAS0L, 8'hxx);
  set_reg(16'h40 + `REG_DAS0H, 8'hxx);
  set_reg(16'h40 + `REG_DASB0, 8'hxx);
  set_reg(16'h40 + `REG_A2A0L, 8'h80);
  set_reg(16'h40 + `REG_A2A0H, 8'hF4);
  set_reg(16'h40 + `REG_NTRL0, 8'h01);

  hdmaen = 8'h00;
  set_reg(`REG_HDMAEN, hdmaen);

  do_hdma_init();
  do_hdma_verify_regs(1);

  for (int row = 0; row <= 20; row++) begin
    if (row == 9) begin
      $display("  Enabling H-DMA");
      hdmaen = 8'h10;
      set_reg(`REG_HDMAEN, hdmaen);
    end

    $display("  Row %03d", row);
    do_hdma_row();
    do_hdma_verify_regs();
  end

  #2;
  $display("H-DMA Mid-frame Enable Test Done\n");
endtask


//////////////////////////////////////////////////////////////////////
// Overlapping General-purpose DMA and H-DMA

task overlap_test();
  $display("H-DMA over GP-DMA test");

  // A multi-row direct H-DMA that disables itself on ROW=4
  hdma_mode[7] = 8'h02;
  hdma_table_cur[7] = 24'h000F2E;
  hdma_table[24'h000F2E + 0]  = 8'h01;
  hdma_table[24'h000F2E + 1]  = 8'h30;
  hdma_table[24'h000F2E + 2]  = 8'h00;
  hdma_table[24'h000F2E + 3]  = 8'h01;
  hdma_table[24'h000F2E + 4]  = 8'h40;
  hdma_table[24'h000F2E + 5]  = 8'h00;
  hdma_table[24'h000F2E + 6]  = 8'h01;
  hdma_table[24'h000F2E + 7]  = 8'h50;
  hdma_table[24'h000F2E + 8]  = 8'h00;
  hdma_table[24'h000F2E + 9]  = 8'h01;
  hdma_table[24'h000F2E + 10] = 8'h60;
  hdma_table[24'h000F2E + 11] = 8'h00;
  hdma_table[24'h000F2E + 12] = 8'h00;
  hdma_table[24'h000F2E + 13] = 8'hAA;
  hdma_table[24'h000F2E + 14] = 8'hAA;

  dma_active = 0;

  set_reg(16'h70 + `REG_DMAP0, 8'h02);
  set_reg(16'h70 + `REG_BBAD0, 8'h0F);
  set_reg(16'h70 + `REG_A1T0L, 8'h2E);
  set_reg(16'h70 + `REG_A1T0H, 8'h0F);
  set_reg(16'h70 + `REG_A1B0,  8'h00);
  set_reg(16'h70 + `REG_DAS0L, 8'hxx);
  set_reg(16'h70 + `REG_DAS0H, 8'hxx);
  set_reg(16'h70 + `REG_DASB0, 8'hxx);
  set_reg(16'h70 + `REG_A2A0L, 8'hxx);
  set_reg(16'h70 + `REG_A2A0H, 8'hxx);
  set_reg(16'h70 + `REG_NTRL0, 8'hxx);

  hdmaen = 8'h80;
  set_reg(`REG_HDMAEN, hdmaen);

  // One channel GP-DMA
  set_reg(16'h10 + `REG_DMAP0, 8'h01);
  set_reg(16'h10 + `REG_BBAD0, 8'h18);
  set_reg(16'h10 + `REG_A1T0L, 8'h2A);
  set_reg(16'h10 + `REG_A1T0H, 8'hCD);
  set_reg(16'h10 + `REG_A1B0,  8'h00);
  set_reg(16'h10 + `REG_DAS0L, 8'h00);
  set_reg(16'h10 + `REG_DAS0H, 8'h01);
  set_reg(16'h10 + `REG_DASB0, 8'hxx);
  set_reg(16'h10 + `REG_A2A0L, 8'hxx);
  set_reg(16'h10 + `REG_A2A0H, 8'hxx);
  set_reg(16'h10 + `REG_NTRL0, 8'hxx);

  set_reg(`REG_MDMAEN, 8'h02);
  dma_active++;

  next_mclk();
  do_dma_enter();

  $display("  GP-DMA Transfer");

  next_cp1_posedge();
  assert(cpu_gate_active);
  assert(nrd & nwr);
  assert(npard & npawr);

  fork
    begin
      for (int i = 0; i < 'h100; i++) begin
        bo = i % 2;

        next_cp1_posedge_start();
        while (hdma_active) begin
          next_cp1_posedge_end();
          next_cp1_posedge_start();
        end
        next_cp1_posedge_end();

        //$display("    %3d %2d  %X", i, bo, 24'h00CD2A + i[23:0]);
        assert(a == 24'h00CD2A + i);
        assert(pa == 8'h18 + bo);
        assert(slowck_sel);
        assert(ocio_pa_oe);
        assert(~nrd & nwr);
        assert(npard & ~npawr);
        assert(~ocio_d_oe);
      end
    end
    begin
      repeat (10) next_cp1_posedge();
      do_hdma_init();
      for (int j = 0; j < 10; j++) begin
        // Timing adjusted to ensure we interrupt GP-DMA at every
        // transaction position offset.
        repeat (10) next_cp1_posedge();
        do_hdma_row();
      end
    end
  join

  next_cp1_posedge();
  do_dma_exit();
  next_cp1_posedge();
  do_dma_idle();
  dma_active--;

  get_verify_reg(16'h10 + `REG_A1T0L, 8'h2A);
  get_verify_reg(16'h10 + `REG_A1T0H, 8'hCE);
  get_verify_reg(16'h10 + `REG_DAS0L, 8'h00);
  get_verify_reg(16'h10 + `REG_DAS0H, 8'h00);

  do_hdma_verify_regs();

  #2;
endtask


//////////////////////////////////////////////////////////////////////
// GP-DMA starting near DRAM Refresh

task refresh_test_off(input [8:0] off);
reg [8:0] hpos;

  hpos = 9'd90 + off;

  $display("GP-DMA starting near DRAM refresh H=%d Test", hpos);

  dma_active = 0;

  // Set H=0
  hdmaen = 8'h00;
  {hblank, vblank} <= 'b11;
  repeat (16) next_mclk();
  {hblank, vblank} <= 'b00;

  // Wait for H=(hpos)
  // H=94: refresh starts 1x CPU_CP1/2 cycle after DMA ends
  // H=95: cpu_gate_active remains asserted after DMA ends
  // H=97: refresh starts right after DMA releases SLOWCK_SEL
  // H=99: refresh ends, DMA does last byte
  // H=101...
  // H=105: refresh ends, DMA does first byte
  // H=107: refresh ends, DMA does channel overhead (idle cycle)
  // H=109: refresh starts right after DMA asserts SLOWCK_SEL
  // H=112-121: refresh extends the CPU->slowck switch
  // H=122-123: cpu_gate_active remains asserted from refresh into DMA
  go_to_hpos(hpos);

  $display("Configuring transfer");
  set_reg(16'h10 + `REG_DMAP0, 8'h04);
  set_reg(16'h10 + `REG_BBAD0, 8'h18);
  set_reg(16'h10 + `REG_A1T0L, 8'h2A);
  set_reg(16'h10 + `REG_A1T0H, 8'hCD);
  set_reg(16'h10 + `REG_A1B0,  8'h00);
  set_reg(16'h10 + `REG_DAS0L, 8'h04);
  set_reg(16'h10 + `REG_DAS0H, 8'h00);

  do_dma_idle();

  set_reg(`REG_MDMAEN, 8'h02);
  dma_active++;

  next_mclk();
  do_dma_enter();

  $display("GP-DMA Transfer");

  // Then perform the DMA: 8 master cycles overhead...
  next_cp1_posedge();
  assert(cpu_gate_active);
  assert(nrd & nwr);
  assert(npard & npawr);

  // ... and 8 master cycles per byte per channel, ...
  for (int i = 0; i < 4; i++) begin
    bo = i % 4;

    next_cp1_posedge();

    assert(a == 24'h00CD2A + i);
    assert(pa == 8'h18 + bo);
    assert(slowck_sel);
    assert(ocio_pa_oe);
    assert(~nrd & nwr);
    assert(npard & ~npawr);
    assert(~ocio_d_oe);
  end

  // ... and an extra 8 master cycles overhead for the whole thing.
  // Then wait 2-8 master cycles to reach a whole number of CPU clock
  // cycles since the pause...
  next_cp1_posedge();
  do_dma_exit();
  // ... and only then resume S-CPU execution.
  next_cp1_posedge();
  do_dma_idle();
  dma_active--;

  get_verify_reg(16'h10 + `REG_A1T0L, 8'h2E);
  get_verify_reg(16'h10 + `REG_A1T0H, 8'hCD);
  get_verify_reg(16'h10 + `REG_DAS0L, 8'h00);
  get_verify_reg(16'h10 + `REG_DAS0H, 8'h00);

  #2;
  $display("GP-DMA starting near DRAM refresh H=%d Test Done\n", hpos);
endtask

task refresh_test();
  for (reg [8:0] off = 9'd0; off <= 9'd40; off ++)
    refresh_test_off(off);
endtask


//////////////////////////////////////////////////////////////////////

initial begin
  ocio_ra = 4'hx;
  ocio_d_i = 8'hx;
  ocio_nrd = 1'b1;
  ocio_nwr = 1'b1;
  hblank = 1'b0;
  vblank = 1'b0;

  repeat (2) @(posedge cp2_negedge) ;
  nRES <= 1'b1;

  force refresh_en = 1'b0;
  gpdma_test();
  hdma_direct_test();
  hdma_indirect_test();
  hdma_indirect_test2();
  hdma_midframe_disable_test();
  hdma_midframe_enable_test();
  overlap_test();

  release refresh_en;
  refresh_test();

  $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s dmac_tb -o dmac_tb.vvp -f s-cpu.files dmac_tb.sv && ./dmac_tb.vvp"
// End:
