`timescale 1us / 1ns

module muldiv_tb();

reg        nRES, hold;
reg        clk, mclk;
wire       cp2_negedge;

reg [7:0]       d_i;
reg             wrmpya, wrmpyb, wrdivl, wrdivh, wrdivb;
wire [15:0]     div, mpy;

initial begin
  $dumpfile("muldiv_tb.vcd");
  $dumpvars();
end

s_cpu_clkgen clkgen
  (
   .CLK_RESET(1'b0),
   .CLK(mclk),

   .SLOWCK_SEL(1'b0),
   .CLK_HOLD(1'b0),

   .CP2_NEGEDGE(cp2_negedge)
   );

s_cpu_muldiv muldiv
  (
   .nRES(nRES),
   .CLK(mclk),
   .CP2_NEGEDGE(cp2_negedge),
   .HOLD(hold),

   .D_I(d_i),
   .WRMPYA(wrmpya),
   .WRMPYB(wrmpyb),
   .WRDIVL(wrdivl),
   .WRDIVH(wrdivh),
   .WRDIVB(wrdivb),

   .DIV(div),
   .MPY(mpy)
   );

initial begin
  clk = 1;
  nRES = 0;
  hold = 0;
end

initial forever begin :ckgen
  #(250.0/(21477.272*3)) clk = ~clk;
end

localparam [1:0] mclk_ccnt_max = 2'd2;
reg [1:0] mclk_ccnt;
initial begin
  mclk_ccnt = 2'd0;
  mclk = 'b0;
end
always @(posedge clk) begin
  if (mclk_ccnt == mclk_ccnt_max) begin
    mclk_ccnt <= 2'd0;
    mclk <= ~mclk;
  end
  else
    mclk_ccnt <= mclk_ccnt + 1'd1;
end

task next_cp2();
  @(posedge mclk) ;
  while (~cp2_negedge)
    @(posedge mclk) ;
  @(posedge mclk) ;
endtask  

task test_mul(input [7:0] a, input [7:0] b);
reg [15:0] c;
  begin
    c = a * b;

    d_i <= a;
    wrmpya <= 1'b1;
    next_cp2();
    wrmpya <= 1'b0;

    d_i <= b;
    wrmpyb <= 1'b1;
    next_cp2();
    wrmpyb <= 1'b0;
    d_i <= 8'bx;

    repeat (8) next_cp2();

    assert(muldiv.mpya == a);
    assert(mpy == c);

    repeat (2) next_cp2();
  end
endtask

task test_div(input [15:0] n, input [7:0] d);
reg [15:0] q;
reg [15:0] r;
  begin
    if (d == 0) begin
      q = 65535;
      r = n;
    end
    else begin
      q = n / d;
      r = n % d;
    end
    
    d_i <= n[7:0];
    wrdivl <= 1'b1;
    next_cp2();
    wrdivl <= 1'b0;

    d_i <= n[15:8];
    wrdivh <= 1'b1;
    next_cp2();
    wrdivh <= 1'b0;

    d_i <= d;
    wrdivb <= 1'b1;
    next_cp2();
    wrdivb <= 1'b0;
    d_i <= 8'bx;

    repeat (16) next_cp2();

    assert(muldiv.num == n);
    assert(div == q);
    assert(mpy == r);

    repeat (2) next_cp2();
  end
endtask

task test_wrdivlh_write_during_div();
reg [15:0] n;
reg [7:0]  d;
reg [15:0] q;
reg [7:0]  r;
  begin
    n = 16'h7FFF;
    d = 8'h0F;
    q = 16'h0888;
    r = 8'h07;

    d_i <= n[7:0];
    wrdivl <= 1'b1;
    next_cp2();
    wrdivl <= 1'b0;

    d_i <= n[15:8];
    wrdivh <= 1'b1;
    next_cp2();
    wrdivh <= 1'b0;

    d_i <= d;
    wrdivb <= 1'b1;
    next_cp2();
    wrdivb <= 1'b0;
    d_i <= 8'bx;

    // WRDIVL/H registers are latched into the remainder (RDMPYL/H) on
    // WRDIVB write, and then not used again during the operation.
    assert(mpy == n);

    // Inverting WRDIVL/H here should have no effect on the result.
    d_i <= ~n[7:0];
    wrdivl <= 1'b1;
    next_cp2();
    wrdivl <= 1'b0;

    d_i <= ~n[15:8];
    wrdivh <= 1'b1;
    next_cp2();
    wrdivh <= 1'b0;

    repeat (14) next_cp2();

    assert(muldiv.num == ~n);
    assert(div == q);
    assert(mpy == r);

    repeat (2) next_cp2();
  end
endtask

initial begin
  d_i <= 8'bx;
  wrmpya <= 1'b0;
  wrmpyb <= 1'b0;
  wrdivl <= 1'b0;
  wrdivh <= 1'b0;
  wrdivb <= 1'b0;

  repeat (2) next_cp2();
  nRES <= 1'b1;
  repeat (2) next_cp2();

  test_mul(0, 10);
  test_mul(10, 0);
  test_mul(10, 11);
  test_mul(11, 10);
  test_mul(65534, 65535);
  test_mul(65535, 65535);

  test_div(0, 1);
  test_div(1, 1);
  test_div(10, 10);
  test_div(5, 10);
  test_div(256, 255);
  test_div(65535, 255);
  test_div(100, 0);

  test_wrdivlh_write_during_div();

  #2 $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s muldiv_tb -o muldiv_tb.vvp -f s-cpu.files muldiv_tb.sv && ./muldiv_tb.vvp"
// End:
