`timescale 1us / 1ns

//////////////////////////////////////////////////////////////////////
// Automatic Joy Port Read
//
// Reference: https://tasvideos.org/Forum/Topics/14077?CurrentPage=1&Highlight=347214#347214
// And then tweaked to toggle clock a full 16 times.
//
// (1 cycle = 16 slow clocks)
// Cycle 0: Detect VBlank (every 32 slow clocks)
// Cycle 1: Set Autopoll busy, Raise latch, zero autopoll result registers.
// Cycle 3: Lower latch.
// Cycle 4: Shift the autopoll registers one place to left and read new LSB
//          from controllers, lower clock.
// Cycle 5: Raise clock.
// Cycle 6: Shift the autopoll registers one place to left and read new LSB
//          from controllers, lower clock.
// Cycle 7: Raise clock.
// ...
// Cycle 32: Shift the autopoll registers one place to left and read new LSB
//           from controllers, lower clock.
// Cycle 33: Raise clock.
// Cycle 34: Shift the autopoll registers one place to left and read new LSB
//           from controllers, lower clock, clear autopoll busy.
// Cycle 35: Raise clock.

module s_cpu_ajpr
  (
   input             nRES,
   input             CLK,
   input             SLOW_CP1_POSEDGE,
   input             HOLD,

   input [7:0]       MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   output reg [2:1]  JPCLK,
   output reg        JPOUT,
   input [1:0]       JP1D,
   input [1:0]       JP2D,

   input             VBLANK,

   input             AJPR_EN,
   output reg        AJPR_BUSY,
   output reg [15:0] JOY1,
   output reg [15:0] JOY2,
   output reg [15:0] JOY3,
   output reg [15:0] JOY4
   );

reg [3:0] ccnt;
reg       vblank_d;
wire      tick;
reg [5:0] cycle;
wire      cycle0;
wire      cycle1;
wire      go;
wire      done;
wire      run;
wire      latch;
wire      shift;

wire        mon_write_ajpr_st, mon_write_joy12, mon_write_joy34;


//////////////////////////////////////////////////////////////////////

always @(posedge CLK) begin
  if (~nRES)
    ccnt <= 4'd0;
  else if (SLOW_CP1_POSEDGE) begin
    ccnt <= ccnt + 1'd1;
  end

  if (mon_write_ajpr_st & MON_WS[0])
    ccnt <= MON_DIN[3:0];
end

assign tick = SLOW_CP1_POSEDGE & &ccnt;
assign cycle0 = cycle == 6'd0;
assign cycle1 = cycle == 6'd1;
assign go = cycle0 & AJPR_EN & VBLANK & ~vblank_d;
assign done = (cycle == 6'd33);
assign run = (~(cycle0 | cycle1) | (go | AJPR_BUSY)) & ~done;

assign latch = run & (cycle <= 6'd1);
assign shift = (cycle >= 6'd3) & (cycle <= 6'd33) & cycle[0];

always @(posedge CLK) begin
  if (~nRES) begin
    cycle <= 6'd0;
    vblank_d <= 1'b0;
    AJPR_BUSY <= 1'b0;
    JPCLK <= 2'b11;
    JPOUT <= 1'b0;
  end
  else if (tick) begin
    if (cycle0)
      vblank_d <= VBLANK;

    if (cycle0 | run)
      cycle <= cycle + 1'd1;
    else
      cycle <= 6'd0;

    if (latch) begin
      JOY1 <= 0;
      JOY2 <= 0;
      JOY3 <= 0;
      JOY4 <= 0;
    end
    else if (shift) begin
      JOY1 <= {JOY1[14:0], ~JP1D[0]};
      JOY2 <= {JOY2[14:0], ~JP2D[0]};
      JOY3 <= {JOY3[14:0], ~JP1D[1]};
      JOY4 <= {JOY4[14:0], ~JP2D[1]};
    end

    AJPR_BUSY <= run;
    JPCLK <= {2{~shift}};
    JPOUT <= latch;
  end

  if (mon_write_ajpr_st) begin
    if (MON_WS[0])
      cycle[3:0] <= MON_DIN[7:4];
    if (MON_WS[1]) begin
      cycle[5:4] <= MON_DIN[9:8];
      vblank_d <= MON_DIN[10];
      AJPR_BUSY <= MON_DIN[11];
      JPCLK <= MON_DIN[13:12];
      JPOUT <= MON_DIN[14];
    end
  end
  if (mon_write_joy12) begin
    if (MON_WS[0])
      JOY1[07:00] <= MON_DIN[07:00];
    if (MON_WS[1])
      JOY1[15:08] <= MON_DIN[15:08];
    if (MON_WS[2])
      JOY2[07:00] <= MON_DIN[23:16];
    if (MON_WS[3])
      JOY2[15:08] <= MON_DIN[31:24];
  end
  if (mon_write_joy34) begin
    if (MON_WS[0])
      JOY3[07:00] <= MON_DIN[07:00];
    if (MON_WS[1])
      JOY3[15:08] <= MON_DIN[15:08];
    if (MON_WS[2])
      JOY4[07:00] <= MON_DIN[23:16];
    if (MON_WS[3])
      JOY4[15:08] <= MON_DIN[31:24];
  end
end


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_sel_ajpr_st, mon_sel_joy12, mon_sel_joy34;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  MON_ACK = 1'b1;
  mon_sel_ajpr_st = 1'b0;
  mon_sel_joy12 = 1'b0;
  mon_sel_joy34 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL[7:2])         //``SUBREGS CPU
    6'h08: begin              //``REG AJPR0
      mon_sel_ajpr_st = 1'b1;
      mon_dout[3:0] = ccnt;
      mon_dout[9:4] = cycle;
      mon_dout[10] = vblank_d;
      mon_dout[11] = AJPR_BUSY;
      mon_dout[13:12] = JPCLK;
      mon_dout[14] = JPOUT;
    end
    6'h09: begin              //``REG AJPR1
      mon_sel_joy12 = 1'b1;
      mon_dout[15:00] = JOY1;
      mon_dout[31:16] = JOY2;
    end
    6'h0A: begin              //``REG AJPR2
      mon_sel_joy34 = 1'b1;
      mon_dout[15:00] = JOY3;
      mon_dout[31:16] = JOY4;
    end
    default: begin
      MON_ACK = 1'b0;
    end
  endcase
end

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_ajpr_st = mon_write & mon_sel_ajpr_st;
assign mon_write_joy12 = mon_write & mon_sel_joy12;
assign mon_write_joy34 = mon_write & mon_sel_joy34;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK & MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end

endmodule
