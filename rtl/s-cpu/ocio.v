`timescale 1us / 1ns

module s_cpu_ocio
  (
   input         nRES,
   input         CLK,
   input         CP1_POSEDGE,
   input         CP2_NEGEDGE,
   input         SLOW_CP1_POSEDGE,
   input         HOLD,
   output        SLOWCK_SEL,
   input         SLOW_CPU,
   output        REFRESH_EN,
   input         SLOW_GATE_ACTIVE,
   input         SLOW_REFRESH_EN,

   input [7:0]   MON_SEL,
   output reg    MON_ACK,
   input         MON_READY,
   output [31:0] MON_DOUT,
   output        MON_VALID,
   input [3:0]   MON_WS,
   input [31:0]  MON_DIN,

   input [12:0]  RA,
   input [7:0]   D_I,
   output [7:0]  D_O,
   output        D_OE,
   input         nRD,
   input         nWR,

   output        MEMSEL,
   output        NCS_JOY,

   output [2:1]  JPCLK,
   output [2:0]  JPOUT,
   input [1:0]   JP1D, // 4016.D0-1
   input [4:0]   JP2D, // 4017.D0-4
   input [7:0]   JPIO_I,
   output [7:0]  JPIO_O,

   input         CC,
   input         HBLANK,
   input         VBLANK,

   output [23:0] A_O,
   output        A_OE,
   output        nWR_O,
   output        nRD_O,
   output [7:0]  PA_O,
   output        PA_OE,
   output        nPARD_O,
   output        nPAWR_O,

   output        nVBL,
   output        nHVT
   );

wire            ncs_joy, ncs_gen, ncs_dma;
wire [31:0]     mon_dout_joy, mon_dout_gen, mon_dout_dmac;
wire            mon_ack_joy, mon_ack_gen, mon_ack_dmac;
wire            mon_valid_joy, mon_valid_gen, mon_valid_dmac;
wire [7:0]      joy_d_o;
wire [7:0]      joy_d_oe;
wire [7:0]      gen_d_o;
wire            gen_d_oe;
wire [7:0]      dmac_d_o;
wire            dmac_d_oe;
reg [7:0]       d_o;
wire [7:0]      dma_mset, dma_hen;
wire            vbl_start, vbl_end, hbl_start;
wire            ajpr_en, ajpr_busy;
wire [15:0]     joy1, joy2, joy3, joy4;


//////////////////////////////////////////////////////////////////////
// Joypad I/O: $40xx-$41xx

assign ncs_joy = RA[12:9] != 4'h0;
assign NCS_JOY = ncs_joy | (nRD & nWR);

s_cpu_ocio_joy joy
  (
   .nRES(nRES),
   .CLK(CLK),
   .SLOW_CP1_POSEDGE(SLOW_CP1_POSEDGE),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_joy),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_joy),
   .MON_VALID(mon_valid_joy),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .RA(RA[8:0]),
   .D_I(D_I),
   .D_O(joy_d_o),
   .D_OE(joy_d_oe),
   .nRD(ncs_joy | nRD),
   .nWR(ncs_joy | nWR),

   .JPOUT(JPOUT),
   .JPCLK(JPCLK),
   .JP1D(JP1D),
   .JP2D(JP2D),

   .VBLANK(VBLANK),

   .AJPR_EN(ajpr_en),
   .AJPR_BUSY(ajpr_busy),
   .JOY1(joy1),
   .JOY2(joy2),
   .JOY3(joy3),
   .JOY4(joy4)
   );


//////////////////////////////////////////////////////////////////////
// General I/O: $42xx

assign ncs_gen = RA[12:8] != 5'h02;

s_cpu_ocio_gen gen
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP1_POSEDGE(CP1_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .SLOW_CP1_POSEDGE(SLOW_CP1_POSEDGE),
   .HOLD(HOLD),
   .SLOW_CPU(SLOW_CPU),
   .REFRESH_EN(REFRESH_EN),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_gen),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_gen),
   .MON_VALID(mon_valid_gen),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .RA(RA[7:0]),
   .D_I(D_I),
   .D_O(gen_d_o),
   .D_OE(gen_d_oe),
   .nRD(ncs_gen | nRD),
   .nWR(ncs_gen | nWR),

   .MEMSEL(MEMSEL),

   .JPIO_I(JPIO_I),
   .JPIO_O(JPIO_O),

   .CC(CC),
   .HBLANK(HBLANK),
   .VBLANK(VBLANK),

   .DMA_MSET(dma_mset),
   .DMA_HEN(dma_hen),
   .VBL_START(vbl_start),
   .VBL_END(vbl_end),
   .HBL_START(hbl_start),

   .AJPR_EN(ajpr_en),
   .AJPR_BUSY(ajpr_busy),
   .JOY1(joy1),
   .JOY2(joy2),
   .JOY3(joy3),
   .JOY4(joy4),

   .nVBL(nVBL),
   .nHVT(nHVT)
   );


//////////////////////////////////////////////////////////////////////
// MDMA / HDMA: $43xx

assign ncs_dma = RA[12:8] != 5'h03;

s_cpu_dmac dmac
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP1_POSEDGE(CP1_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .HOLD(HOLD),
   .SLOWCK_SEL(SLOWCK_SEL),
   .SLOW_GATE_ACTIVE(SLOW_GATE_ACTIVE),
   .SLOW_REFRESH_EN(SLOW_REFRESH_EN),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_dmac),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_dmac),
   .MON_VALID(mon_valid_dmac),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .RA(RA[7:0]),
   .D_I(D_I),
   .D_O(dmac_d_o),
   .D_OE(dmac_d_oe),
   .nRD(ncs_dma | nRD),
   .nWR(ncs_dma | nWR),

   .MSET(dma_mset),
   .HEN(dma_hen),
   .HBLANK(HBLANK),
   .VBLANK(VBLANK),
   .VBL_START(vbl_start),
   .VBL_END(vbl_end),
   .HBL_START(hbl_start),

   .A_O(A_O),
   .A_OE(A_OE),
   .nWR_O(nWR_O),
   .nRD_O(nRD_O),
   .PA_O(PA_O),
   .PA_OE(PA_OE),
   .nPARD_O(nPARD_O),
   .nPAWR_O(nPAWR_O)
   );


//////////////////////////////////////////////////////////////////////
// Data bus
// TODO: Handle multi-bit D_OE

always @* begin
  case (1'b1)
    |joy_d_oe: d_o = joy_d_o;
    gen_d_oe: d_o = gen_d_o;
    dmac_d_oe: d_o = dmac_d_o;
    default: d_o = 8'hxx;
  endcase
end

assign D_O = d_o;
assign D_OE = dmac_d_oe | gen_d_oe | |joy_d_oe;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0]  mon_dout;
reg         mon_valid;

always @* begin
  if (mon_ack_joy) begin
    MON_ACK = mon_ack_joy;
    mon_valid = mon_valid_joy;
    mon_dout = mon_dout_joy;
  end
  else if (mon_ack_gen) begin
    MON_ACK = mon_ack_gen;
    mon_valid = mon_valid_gen;
    mon_dout = mon_dout_gen;
  end
  else if (mon_ack_dmac) begin
    MON_ACK = mon_ack_dmac;
    mon_valid = mon_valid_dmac;
    mon_dout = mon_dout_dmac;
  end
  else begin
    MON_ACK = 1'b0;
    mon_valid = 1'b0;
    mon_dout = 32'h0;
  end
end

assign MON_DOUT = mon_dout;
assign MON_VALID = mon_valid;

endmodule
