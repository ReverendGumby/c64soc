`timescale 1us / 1ns

module s_cpu_clkgen
  (
   input             CLK_RESET,
   input             CLK, // main clock

   // debug monitor
   input [7:0]       MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input             SLOWCK_SEL, // sync CP1/2 to SLOWCK
   input             CLK_HOLD, // holds CP2, SLOWCK
   input             FAST_CPU, // request fast CP1/2 rate
   input             SLOW_CPU, // request slow CP1/2 rate
   input             REFRESH_EN, // DRAM refresh

   output            CP1_POSEDGE, // clock phase 1
   output            CP1_NEGEDGE,
   output            CP2,
   output            CP2_POSEDGE, // clock phase 2
   output            CP2_NEGEDGE,
   output            CP2_NEGEDGE_PRE,

   output            CPU_GATE_ACTIVE, // CPU clocks below are gated

   output            CPU_CP1_POSEDGE_GATED,
   output            CPU_CP1_NEGEDGE_GATED,
   output            CPU_CP2_POSEDGE_GATED,
   output            CPU_CP2_NEGEDGE_GATED,

   // Slow clock = CLK / 8
   output            SLOW_CP1_POSEDGE,
   output            SLOW_GATE_ACTIVE, // slow clocks are gated
   output            SLOW_REFRESH_EN // refresh_en sync'd to slow clock
   );

reg     slowck_act;
wire    mon_write_clkgen;


//////////////////////////////////////////////////////////////////////
// Clock reset

reg       reset;

initial
  reset = 1'b1;

always @(posedge CLK)
  reset <= CLK_RESET;


//////////////////////////////////////////////////////////////////////
// CPU clock: two phases, CP1 and CP2

reg [3:0] cp2_clkcnt;
reg [3:0] cycle_ccnt, cycle_ccnt_next;
wire      cpu_cp2_negedge, cpu_cp1_posedge, cpu_cp1_negedge, cpu_cp2_posedge,
          cpu_cp2_negedge_pre;

localparam [3:0] cp2_clkcnt_init = 4'd0;

localparam [3:0] fast_cycle_ccnt = 4'd6; // fast-ROM, MMIO, "internal" cycles
localparam [3:0] norm_cycle_ccnt = 4'd8; // normal ROM, expansion, WRAM
localparam [3:0] joyp_cycle_ccnt = 4'd12; // joypad I/O

initial begin
  cp2_clkcnt = cp2_clkcnt_init;
  cycle_ccnt = norm_cycle_ccnt;
end

always @* begin
  if (FAST_CPU & ~REFRESH_EN)
    cycle_ccnt_next = fast_cycle_ccnt;
  else if (SLOW_CPU & ~REFRESH_EN)
    cycle_ccnt_next = joyp_cycle_ccnt;
  else
    cycle_ccnt_next = norm_cycle_ccnt;
end

always @(posedge CLK) begin
  if (CP1_NEGEDGE & ~slowck_act)    // preserve rate when on slow clock
    cycle_ccnt <= cycle_ccnt_next;

  if (mon_write_clkgen & MON_WS[0])
    cycle_ccnt <= MON_DIN[3:0];
end

always @(posedge CLK) begin
  if (reset)
    cp2_clkcnt <= cp2_clkcnt_init;
  else if (~CLK_HOLD) begin
    if (cpu_cp2_negedge_pre)
      cp2_clkcnt <= 4'd0;
    else
      cp2_clkcnt <= cp2_clkcnt + 1'd1;
  end

  if (mon_write_clkgen & MON_WS[0])
    cp2_clkcnt <= MON_DIN[7:4];
end

assign cpu_cp2_negedge_pre = cp2_clkcnt == (cycle_ccnt - 1'd1);

assign cpu_cp2_negedge = ~CLK_HOLD && cp2_clkcnt == 4'd0;
assign cpu_cp1_posedge = ~CLK_HOLD && cp2_clkcnt == 4'd1;
assign cpu_cp1_negedge = ~CLK_HOLD && cp2_clkcnt == 4'd2;
assign cpu_cp2_posedge = ~CLK_HOLD && cp2_clkcnt == 4'd3;


//////////////////////////////////////////////////////////////////////
// CPU clock gate
//
// The CPU clock is gated (1) while the combined CPU clock is switched
// to the slow clock and (2) during DRAM refresh.

wire      cpu_gate;
reg       cpu_gate_dly;
reg       cpu_act;
reg       cpu_refresh_en;

always @(posedge CLK) begin
  if (reset)
    cpu_refresh_en <= 1'b0;
  else if (cpu_cp2_negedge)
    cpu_refresh_en <= REFRESH_EN;

  if (mon_write_clkgen & MON_WS[2])
    cpu_refresh_en <= MON_DIN[16];
end

assign cpu_gate = ~cpu_act | cpu_refresh_en;

initial
  cpu_gate_dly = 1'b0;

always @(posedge CLK) begin
  if (cpu_cp1_posedge)
    cpu_gate_dly <= cpu_gate;

  if (mon_write_clkgen & MON_WS[1])
    cpu_gate_dly <= MON_DIN[12];
end

assign CPU_GATE_ACTIVE = cpu_gate_dly;

assign CPU_CP1_POSEDGE_GATED = cpu_cp1_posedge & ~cpu_gate;
assign CPU_CP1_NEGEDGE_GATED = cpu_cp1_negedge & ~cpu_gate_dly;
assign CPU_CP2_POSEDGE_GATED = cpu_cp2_posedge & ~cpu_gate_dly;
assign CPU_CP2_NEGEDGE_GATED = cpu_cp2_negedge & ~cpu_gate_dly;


//////////////////////////////////////////////////////////////////////
// Slow clock = MCLK / 8
//
// This drives DMA timing (and probably other stuff).

reg [3:0] slow_ccnt;
wire      slow_cp2_negedge, slow_cp1_posedge, slow_cp1_negedge,
          slow_cp2_posedge, slow_cp2_negedge_pre;

localparam [3:0] slow_clkcnt = 4'd8;
localparam [3:0] slow_clkcnt_init = 4'd0;

initial begin
  slow_ccnt = slow_clkcnt_init;
end

always @(posedge CLK) begin
  if (reset)
    slow_ccnt <= slow_clkcnt_init;
  else if (~CLK_HOLD) begin
    if (slow_cp2_negedge_pre)
      slow_ccnt <= 4'd0;
    else
      slow_ccnt <= slow_ccnt + 1'd1;
  end

  if (mon_write_clkgen & MON_WS[1])
    slow_ccnt <= MON_DIN[11:8];
end

assign slow_cp2_negedge_pre = slow_ccnt == (slow_clkcnt - 1'd1);

assign slow_cp2_negedge = ~CLK_HOLD && slow_ccnt == 4'd0;
assign slow_cp1_posedge = ~CLK_HOLD && slow_ccnt == 4'd1;
assign slow_cp1_negedge = ~CLK_HOLD && slow_ccnt == 4'd2;
assign slow_cp2_posedge = ~CLK_HOLD && slow_ccnt == 4'd3;

assign SLOW_CP1_POSEDGE = slow_cp1_posedge;


//////////////////////////////////////////////////////////////////////
// Slow clock gate
//
// The slow clock is gated during DRAM refresh.
//
// Note that this is input to the combined clock, so that DMA halts
// when gated.

wire      slow_gate;
reg       slow_gate_dly;
reg       slow_refresh_en;

always @(posedge CLK) begin
  if (reset)
    slow_refresh_en <= 1'b0;
  else if (slow_cp2_negedge)
    slow_refresh_en <= REFRESH_EN;

  if (mon_write_clkgen & MON_WS[2])
    slow_refresh_en <= MON_DIN[18];
end

assign slow_gate = slow_refresh_en;

initial
  slow_gate_dly = 1'b0;

always @(posedge CLK) begin
  if (slow_cp1_posedge)
    slow_gate_dly <= slow_gate;

  if (mon_write_clkgen & MON_WS[2])
    slow_gate_dly <= MON_DIN[17];
end

wire slow_cp1_posedge_gated = slow_cp1_posedge & ~slow_gate;
wire slow_cp1_negedge_gated = slow_cp1_negedge & ~slow_gate_dly;
wire slow_cp2_posedge_gated = slow_cp2_posedge & ~slow_gate_dly;
wire slow_cp2_negedge_gated = slow_cp2_negedge & ~slow_gate_dly;

assign SLOW_GATE_ACTIVE = slow_gate_dly;
assign SLOW_REFRESH_EN = slow_refresh_en;


//////////////////////////////////////////////////////////////////////
// Combined CPU clock and SLOWCK: final CP1/CP2
//
// When SLOWCK_SEL is asserted, this clock is synchronized to SLOWCK,
// else to the CPU clock. Switching between the two clocks is
// glitchless; MCLK are inserted to pad.

wire      slowck_switch;
reg       cp2;

assign slowck_switch = (SLOWCK_SEL & ~slowck_act) |
                       (~SLOWCK_SEL & slowck_act);

always @(posedge CLK) begin
  if (reset) begin
    cpu_act <= 1'b1;
    slowck_act <= 1'b0;
  end
  else if (~CLK_HOLD) begin
    // CPU clock -> SLOWCK
    if (SLOWCK_SEL & ~slowck_act) begin
      if (cpu_act) begin
        if (cpu_cp2_negedge)
          cpu_act <= ~SLOWCK_SEL;
      end
      else begin
        if (slow_cp2_negedge_gated)
          slowck_act <= SLOWCK_SEL;
      end
    end

    // SLOWCK -> CPU clock
    if (~SLOWCK_SEL & ~cpu_act) begin
      if (slowck_act) begin
        if (slow_cp2_negedge_gated)
          slowck_act <= SLOWCK_SEL;
      end
      else begin
        if (cpu_cp2_negedge)
          cpu_act <= ~SLOWCK_SEL;
      end
    end
  end

  if (mon_write_clkgen & MON_WS[1]) begin
    cpu_act <= MON_DIN[13];
    slowck_act <= MON_DIN[14];
  end
end

initial
  cp2 = 1'b0;

always @(posedge CLK) begin
  if (reset)
    cp2 <= 1'b0;
  else begin
    cp2 <= (cp2 | CP2_POSEDGE) & ~CP2_NEGEDGE;
  end

  if (mon_write_clkgen & MON_WS[1])
    cp2 <= MON_DIN[15];
end

assign CP1_POSEDGE = (cpu_act & cpu_cp1_posedge) |
                     (slowck_act & slow_cp1_posedge_gated);
assign CP1_NEGEDGE = (cpu_act & cpu_cp1_negedge) |
                     (slowck_act & slow_cp1_negedge_gated);
assign CP2_POSEDGE = (cpu_act & cpu_cp2_posedge) |
                     (slowck_act & slow_cp2_posedge_gated);
assign CP2_NEGEDGE = (cpu_act & cpu_cp2_negedge) |
                     (slowck_act & slow_cp2_negedge_gated);
assign CP2_NEGEDGE_PRE = (cpu_act & cpu_cp2_negedge_pre) |
                         (slowck_act & slow_cp2_negedge_pre);

assign CP2 = cp2;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_sel_clkgen;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  MON_ACK = 1'b1;
  mon_sel_clkgen = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL[7:2])         //``SUBREGS CPU
    6'h00: begin              //``REG CLKGEN
      mon_sel_clkgen = 1'b1;
      mon_dout[3:0] = cycle_ccnt;
      mon_dout[7:4] = cp2_clkcnt;
      mon_dout[11:8] = slow_ccnt;
      mon_dout[12] = cpu_gate_dly;
      mon_dout[13] = cpu_act;
      mon_dout[14] = slowck_act;
      mon_dout[15] = cp2;
      mon_dout[16] = cpu_refresh_en;
      mon_dout[17] = slow_gate_dly;
      mon_dout[18] = slow_refresh_en;
    end
    default: begin
      MON_ACK = 1'b0;
    end
  endcase
end

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_clkgen = mon_write & mon_sel_clkgen;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK & MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end

endmodule
