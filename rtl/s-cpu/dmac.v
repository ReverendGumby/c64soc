//////////////////////////////////////////////////////////////////////
// General-purpose DMA and H-DMA controller
//
// References:
// - https://problemkaputt.de/fullsnes.htm#snesdmaandhdmastartenableregisters
// - https://snesdev.mesen.ca/wiki/index.php?title=SNES_Timing#DMA

`timescale 1us / 1ns

module s_cpu_dmac
  (
   input             nRES,
   input             CLK,
   input             CP1_POSEDGE,
   input             CP2_NEGEDGE,
   input             HOLD,
   output            SLOWCK_SEL,
   input             SLOW_GATE_ACTIVE,
   input             SLOW_REFRESH_EN,

   input [7:0]       MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input [7:0]       RA,
   input [7:0]       D_I,
   output [7:0]      D_O,
   output            D_OE,
   input             nRD,
   input             nWR,

   input [7:0]       MSET,
   input [7:0]       HEN,
   input             HBLANK,
   input             VBLANK,
   input             VBL_START,
   input             VBL_END,
   input             HBL_START,

   output [23:0]     A_O,
   output            A_OE,
   output            nWR_O,
   output            nRD_O,
   output [7:0]      PA_O,
   output            PA_OE,
   output            nPARD_O,
   output            nPAWR_O
   );

genvar          a;
wire [3:0]      ca, cra;
reg [2:0]       acap, aca;
wire [7:0]      cenp, cenclr;
reg [7:0]       cen;
reg [7:0]       hcdis_pre, hcdis;
wire [7:0]      hcact, hcxfer, hcload, hchbls;
reg [7:0]       men;
reg [7:0]       hen, hen_d;
wire            xfer;
wire            gpdma, hdma_pre;
reg             hdma;
reg             active_pre, active;
reg             post_vbl_end;
wire [7:0]      acen;
wire            ch_active_pre, hdma_ch_active_pre, gpdma_ch_active_pre;
reg             ch_active;
wire            halldone;
wire            hstart_pre, hstop, gprestart;
reg             hstartl, hstart, gprestart_d;
wire            hdma_active, hdma_active_pre;
wire            done_pre, gpdma_done_pre, hdma_done_pre;
wire            ntrlz_pre;
reg             ntrlz;
wire            hload_done_pre, hdata_done_pre;
reg             done;
wire            gpstore_a1t, gpstore_das;
reg [1:0]       gptpos;
wire            cs_dmac;
reg [2:0]       hst, hstn;
wire            hinit, htbl, hterm;
wire            hntrl, hdasl, hdash, hdas;
wire            hxfer_set, hxfer_clr;
reg [7:0]       hxfer;
wire            hload, hload_next, hload_last;
wire            hload_start, hload_end;
wire            hdata;
wire            hstore_a2a, hstore_ntrl, hstore_das;
reg [2:0]       tunit_bc, hbc;
wire [2:0]      hbcn;
wire ['hA:'h0]  rsel, wsel;
reg [7:0]       dl, d_o;
reg [7:0]       dreg [0:7]['h0:'hB];
wire [7:0]      dmap, bbad, ntrl;
wire [23:0]     a1t, a2a, das;
wire [15:0]     a2ar;
reg [15:0]      dasn;
wire [15:0]     bc;
wire            b2a, ind_tbl, ind_data_pre, ind_data;
wire [1:0]      astep;
reg [1:0]       eas;
wire [1:0]      tpos;
wire [2:0]      tunit, tunit_pre;
wire [15:0]     ap;
wire [23:0]     abad;
reg [15:0]      an, bcn;
reg [7:0]       boff;
wire [7:0]      ntrlm1;
reg [7:0]       ntrln;
wire            ntrln_latch;

wire            mon_write_dmac_ch_reg0123, mon_write_dmac_ch_reg4567,
                mon_write_dmac_ch_reg89AB, mon_write_dmac_st0,
                mon_write_dmac_int0, mon_write_dmac_int1;


//////////////////////////////////////////////////////////////////////
// Read/write registers

`define REG_DMAPx   4'h0
`define REG_BBADx   4'h1
`define REG_A1TxL   4'h2
`define REG_A1TxH   4'h3
`define REG_A1Bx    4'h4
`define REG_DASxL   4'h5
`define REG_DASxH   4'h6
`define REG_DASBx   4'h7
`define REG_A2AxL   4'h8
`define REG_A2AxH   4'h9
`define REG_NTRLx   4'hA
`define REG_UNUSEDx 4'hB

assign ca = RA[7:4];            // channel address
assign cra = RA[3:0];           // register address

assign cs_dmac = ca < 'd8;

generate
  for (a = 'h0; a <= 'hA; a = a + 1) begin :rsela
    assign rsel[a] = ~nRD & cs_dmac & (cra == a);
  end

  for (a = 'h0; a <= 'hA; a = a + 1) begin :wsela
    assign wsel[a] = ~nWR & cs_dmac & (cra == a);
  end
endgenerate

always @* begin
  d_o = 8'hxx;
  if (|rsel & ~ca[3])
    d_o = dreg[ca[2:0]][cra];
end

assign D_O = d_o;
assign D_OE = |rsel;

generate
endgenerate

always @(posedge CLK) begin
  if (CP2_NEGEDGE)
    dl <= D_I;

  if (mon_write_dmac_int0 & MON_WS[1])
    dl <= MON_DIN[15:8];
end

// Write back register updates
always @(posedge CLK) begin
  if (~nRES) begin
  end
  else if (!HOLD) begin
    if (CP1_POSEDGE) begin
      if (gpstore_a1t) begin
        dreg[aca][`REG_A1TxH] <= an[15:8];
        dreg[aca][`REG_A1TxL] <= an[7:0];
      end
      if (gpstore_das) begin
        dreg[aca][`REG_DASxH] <= bcn[15:8];
        dreg[aca][`REG_DASxL] <= bcn[7:0];
      end
      if (hstore_a2a) begin
        dreg[aca][`REG_A2AxH] <= an[15:8];
        dreg[aca][`REG_A2AxL] <= an[7:0];
      end
      if (hstore_das) begin
        dreg[aca][`REG_DASxH] <= dasn[15:8];
        dreg[aca][`REG_DASxL] <= dasn[7:0];
      end
      if (hstore_ntrl) begin
        dreg[aca][`REG_NTRLx] <= ntrln;
      end
    end
    if (|wsel & ~ca[3])
      dreg[ca[2:0]][cra] <= D_I;
  end

  if (mon_write_dmac_ch_reg0123) begin
    if (MON_WS[0])
      dreg[mon_sel_dmac_ch_sel]['h0] <= MON_DIN[07:00];
    if (MON_WS[1])
      dreg[mon_sel_dmac_ch_sel]['h1] <= MON_DIN[15:08];
    if (MON_WS[2])
      dreg[mon_sel_dmac_ch_sel]['h2] <= MON_DIN[23:16];
    if (MON_WS[3])
      dreg[mon_sel_dmac_ch_sel]['h3] <= MON_DIN[31:24];
  end
  if (mon_write_dmac_ch_reg4567) begin
    if (MON_WS[0])
      dreg[mon_sel_dmac_ch_sel]['h4] <= MON_DIN[07:00];
    if (MON_WS[1])
      dreg[mon_sel_dmac_ch_sel]['h5] <= MON_DIN[15:08];
    if (MON_WS[2])
      dreg[mon_sel_dmac_ch_sel]['h6] <= MON_DIN[23:16];
    if (MON_WS[3])
      dreg[mon_sel_dmac_ch_sel]['h7] <= MON_DIN[31:24];
  end
  if (mon_write_dmac_ch_reg89AB) begin
    if (MON_WS[0])
      dreg[mon_sel_dmac_ch_sel]['h8] <= MON_DIN[07:00];
    if (MON_WS[1])
      dreg[mon_sel_dmac_ch_sel]['h9] <= MON_DIN[15:08];
    if (MON_WS[2])
      dreg[mon_sel_dmac_ch_sel]['hA] <= MON_DIN[23:16];
    if (MON_WS[3])
      dreg[mon_sel_dmac_ch_sel]['hB] <= MON_DIN[31:24];
  end
end


//////////////////////////////////////////////////////////////////////
// DMAC active state

always @(posedge CLK) begin
  if (~nRES) begin
    active <= 1'b0;
  end
  else if (CP1_POSEDGE) begin
    active <= active_pre;
  end

  if (mon_write_dmac_st0 & MON_WS[0])
    active <= MON_DIN[0];
end

always @* active_pre = |cenp;

always @(posedge CLK) begin
  if (!HOLD)
    post_vbl_end <= (post_vbl_end | VBL_END) & ~(HBL_START | ~nRES);

  if (mon_write_dmac_int0 & MON_WS[0])
    post_vbl_end <= MON_DIN[1];
end

// Controller sub-states
assign gpdma = active & ~hdma;
assign hdma_pre = ((hstart & ~halldone) | hdma) & ~hstop;
assign xfer = active & (gpdma | hdata);

// Sub-state transition events
assign hstart_pre = (VBL_END | HBL_START) & ~VBLANK;
assign hstop = hdma & (hload_end | halldone);
assign halldone = ~|(HEN & ~hcdis);

always @(posedge CLK) begin
  if (~nRES) begin
    hstartl <= 1'b0;
    hstart <= 1'b0;
    hdma <= 1'b0;
  end
  else begin
    if (!HOLD)
      hstartl <= (hstartl & ~CP1_POSEDGE) | hstart_pre;
    if (CP1_POSEDGE) begin
      hstart <= hstartl;
      hdma <= hdma_pre;
    end
  end

  if (mon_write_dmac_int0 & MON_WS[0]) begin
    hstartl <= MON_DIN[2];
    hstart <= MON_DIN[3];
  end
  if (mon_write_dmac_st0 & MON_WS[0])
    hdma <= MON_DIN[2];
end

assign gprestart = (hstart & halldone) | (hstop & |men);

always @(posedge CLK) begin
  if (CP1_POSEDGE)
    gprestart_d <= gprestart;

  if (mon_write_dmac_int0 & MON_WS[0])
    gprestart_d <= MON_DIN[4];
end

assign SLOWCK_SEL = active_pre | hdma;


//////////////////////////////////////////////////////////////////////
// Channel enables

assign cenclr = done_pre ? acen : 8'b0;

// Low-priority (general-purpose DMA) channel enables
// DMA begins one CPU cycle after MSET write
always @(posedge CLK) begin
  if (~nRES) begin
    men <= 8'b0;
  end
  else if (CP1_POSEDGE) begin
    men <= (men | MSET) & ~cenclr;
  end

  if (mon_write_dmac_int0 & MON_WS[2])
    men <= MON_DIN[23:16];
end

// High-priority (H-DMA) channel enables
always @* begin
  hen = hen_d & ~cenclr;
  if (hstart)
    hen = hen | hchbls;
  if (hload_start)
    hen = hen | hcload;
end

always @(posedge CLK) begin
  if (~nRES) begin
    hen_d <= 8'b0;
  end
  else if (CP1_POSEDGE) begin
    hen_d <= hen;
  end

  if (mon_write_dmac_int0 & MON_WS[3])
    hen_d <= MON_DIN[31:24];
end

// Combined (general-purpose and H-DMA) channel enables
assign cenp = hdma_pre ? hen : men;

always @(posedge CLK) begin
  if (~nRES) begin
    cen <= 8'b0;
  end
  else if (CP1_POSEDGE) begin
    cen <= cenp;
  end

  if (mon_write_dmac_int1 & MON_WS[0])
    cen <= MON_DIN[7:0];
end

// H-DMA active channel enables
always @* begin
  if (VBL_START) begin
    hcdis_pre = ~8'b0;
  end
  else if (VBL_END) begin
    hcdis_pre = 8'b0;
  end
  else if (CP1_POSEDGE & hterm) begin
    hcdis_pre = hcdis | cenclr;
  end
  else
    hcdis_pre = hcdis;
end

always @(posedge CLK) begin
  if (~nRES)
    hcdis <= 8'b0;
  else if (!HOLD)
    hcdis <= hcdis_pre;

  if (mon_write_dmac_st0 & MON_WS[1])
    hcdis <= MON_DIN[15:8];
end

// H-DMA channels pending transfer on H-blank
always @(posedge CLK) begin
  if (~nRES)
    hxfer <= 8'b0;
  else begin
    if (VBL_START)
      hxfer <= 8'b0;
    else if (CP1_POSEDGE) begin
      hxfer[aca] <= (hxfer[aca] | hxfer_set) & ~hxfer_clr;
    end
  end

  if (mon_write_dmac_st0 & MON_WS[2])
    hxfer <= MON_DIN[23:16];
end

assign hxfer_set = hntrl & (ntrln[7] | ntrln_latch);
assign hxfer_clr = hntrl & ~ntrln[7] & (~ntrln_latch | hterm);

// Active H-DMA channels
assign hcact = HEN & ~hcdis_pre;
// ... that are eligible for transfer or load
assign hcxfer = hcact & hxfer;
assign hcload = hcact;
// Select which of these two sets to process at V-Blank end / H-Blank start.
assign hchbls = |hcxfer ? hcxfer : hcload;


//////////////////////////////////////////////////////////////////////
// Active channel select

always @* begin
  casez (cenp)
    default:     acap = 3'd0;
    8'bzzzzzz10: acap = 3'd1;
    8'bzzzzz100: acap = 3'd2;
    8'bzzzz1000: acap = 3'd3;
    8'bzzz10000: acap = 3'd4;
    8'bzz100000: acap = 3'd5;
    8'bz1000000: acap = 3'd6;
    8'b10000000: acap = 3'd7;
  endcase
end

assign acen = {8{active}} & (8'b1 << aca);

assign ch_active_pre = hdma_pre ? hdma_ch_active_pre : gpdma_ch_active_pre;
assign done_pre = gpdma_done_pre | hdma_done_pre;

always @(posedge CLK) begin
  if (~nRES) begin
    ch_active <= 1'b0;
    done <= 1'b0;
    aca <= 3'd0;
  end
  else if (CP1_POSEDGE) begin
    ch_active <= ch_active_pre;
    done <= done_pre;
    aca <= acap;
  end

  if (mon_write_dmac_st0 & MON_WS[0]) begin
    ch_active <= MON_DIN[4];
    done <= MON_DIN[5];
  end
  if (mon_write_dmac_int0 & MON_WS[0])
    aca <= MON_DIN[7:5];
end


//////////////////////////////////////////////////////////////////////
// Active channel status

assign dmap = dreg[aca][`REG_DMAPx];
assign bbad = dreg[aca][`REG_BBADx];
assign a1t = {dreg[aca][`REG_A1Bx], dreg[aca][`REG_A1TxH], dreg[aca][`REG_A1TxL]};
assign das = {dreg[aca][`REG_DASBx], dreg[aca][`REG_DASxH], dreg[aca][`REG_DASxL]};
assign a2ar = {dreg[aca][`REG_A2AxH], dreg[aca][`REG_A2AxL]};
assign ntrl = dreg[aca][`REG_NTRLx];

assign b2a = dmap[7];
assign ind_tbl = dmap[6];
assign astep = dmap[4:3];
assign tunit = dmap[2:0];

// This data is needed before the channel becomes active.
assign tunit_pre = dreg[acap][`REG_DMAPx][2:0];

// A1T->A2A happens on the first H-DMA load cycle.
assign a2a = (hinit & hntrl) ? a1t : {a1t[23:16], a2ar};


//////////////////////////////////////////////////////////////////////
// General-purpose DMA states

assign gpstore_a1t = gpdma & ch_active;
assign gpstore_das = gpdma & ch_active;

always @(posedge CLK) begin
  if (CP1_POSEDGE) begin
    if (gpdma & ~gprestart_d) begin
      if (~ch_active)
        gptpos <= 2'd0;
      else
        gptpos <= gptpos + 1'd1;
    end
  end

  if (mon_write_dmac_int1 & MON_WS[1])
    gptpos <= MON_DIN[9:8];
end

assign gpdma_ch_active_pre = ((active & active_pre) &
                              ~(done_pre & ~gprestart_d) &
                              ~hstart);


//////////////////////////////////////////////////////////////////////
// H-DMA states

assign hdma_active = hdma & active;
assign hdma_active_pre = hdma_pre & active_pre;

localparam [2:0] HST_IDLE   = 3'd0;
localparam [2:0] HST_DATA   = 3'd1;
localparam [2:0] HST_NTRL   = 3'd2;
localparam [2:0] HST_DASL   = 3'd3;
localparam [2:0] HST_DASH   = 3'd4;

// Loading phase starts immediately after all transfers complete.
wire hxfer_done = xfer & hdma & (cen == acen) & hdata_done_pre;
wire hxfer_skip = (hdma_pre & ~hdma_active & ~|hcxfer) | hinit;
assign hload = hst >= HST_NTRL;
assign hload_last = hload & (cen == acen);
assign hload_start = ~hload & ~halldone & hxfer_done;
assign hload_end = hload_last & hload_done_pre;

always @* begin
  if (~hdma_pre)
    hstn = HST_IDLE;
  else begin
    hstn = hst;
    case (hst)
      HST_IDLE:
        if (hdma_active_pre)
          hstn = hxfer_skip ? HST_NTRL : HST_DATA;
      HST_DATA:
        if (hload_start)
          hstn = HST_NTRL;
      HST_NTRL:
        hstn = (ind_tbl & ntrln_latch) ? HST_DASL :
               (hload_last ? HST_IDLE : HST_NTRL);
      HST_DASL:
        hstn = HST_DASH;
      HST_DASH:
        hstn = hload_last ? HST_IDLE : HST_NTRL;
      default: ;
    endcase
  end
end

always @(posedge CLK) begin
  if (CP1_POSEDGE)
    hst <= hstn;

  if (mon_write_dmac_int1 & MON_WS[1])
    hst <= MON_DIN[12:10];
end

wire hdata_pre = ~(hst == HST_DATA) & (hstn == HST_DATA);

assign htbl = (hst >= HST_NTRL) & (hst <= HST_DASH);
assign hntrl = hst == HST_NTRL;
assign hdata = hst == HST_DATA;
assign hdasl = hst == HST_DASL;
assign hdash = hst == HST_DASH;
assign hdas = hdasl | hdash;

always @(posedge CLK) begin
  if (CP1_POSEDGE) begin
    if (hdata_pre | hdata_done_pre)
      hbc <= tunit_bc;
    else if (hdata)
      hbc <= hbcn;
  end

  if (mon_write_dmac_int1 & MON_WS[1])
    hbc <= MON_DIN[15:13];
end

assign hbcn = hbc - 1'd1;
assign hinit = hdma & post_vbl_end;
assign ind_data_pre = ind_tbl & (hdash | (hdata & ~done));
assign ind_data = ind_tbl & hdata;

assign hstore_a2a = hdma_active & ~ind_data & ~(hntrl & ~ntrln_latch);
assign hstore_ntrl = hntrl;
assign hstore_das = (hdas | hdata);

always @* begin
  casez (tunit_pre)
    3'b000:             tunit_bc = 3'd1;
    3'b001, 3'bz10:     tunit_bc = 3'd2;
    default:            tunit_bc = 3'd4;
  endcase
end

assign ntrlz_pre = ntrln_latch & ~|ntrln[7:0];

always @(posedge CLK) begin
  if (~nRES)
    ntrlz <= 1'b0;
  else if (CP1_POSEDGE & hntrl)
    ntrlz <= ntrlz_pre;

  if (mon_write_dmac_int0 & MON_WS[0])
    ntrlz <= MON_DIN[0];
end

assign hdma_ch_active_pre = active_pre;

assign hdata_done_pre = hdata & (hbc == 3'd1);
assign hload_done_pre = ((hst == HST_NTRL) & ~(ind_tbl & ntrln_latch)) | (hst == HST_DASH);
assign hdma_done_pre = hdata_done_pre | hload_done_pre;

assign hterm = hload_done_pre & ((hntrl & ntrlz_pre) | (hdash & ntrlz));


//////////////////////////////////////////////////////////////////////
// Register update computation

always @* begin
  eas = astep;
  if (hdma) begin
    eas = 2'b00; // +1
  end
end

assign ap = ind_data ? das[15:0] : (gpdma ? a1t[15:0] : a2a[15:0]);

always @* begin
  an = 16'bx;
  case (eas)
    2'b00:  an = ap + 1'd1;
    2'b10:  an = ap - 1'd1;
    2'b01,
    2'b11:  an = ap;
    default: ;
  endcase
end

always @* begin
  if (hdasl)
    dasn = {das[15:8], dl};
  else if (hdash)
    dasn = {dl, das[7:0]};
  else
    dasn = an;
end

// B-bus offset is based on how many bytes have been transferred since
// the channel transaction started. (H-DMA transactions begin on each
// H-Blank, GP-DMA on MSET write.)
assign tpos = hdma ? ~hbcn[1:0] : gptpos;

always @* begin
  boff = 8'd0;
  casez (tunit)
    3'b000: ;                      // 1-address
    3'b001: boff[0] = tpos[0];     // 2-address
    3'b010: ;                      // 1-address (write twice)
    3'b011: boff[0] = tpos[1];     // 2-address (write twice)
    3'b100: boff[1:0] = tpos[1:0]; // 4-address
    default: ;
  endcase
end

assign bc = hdma ? {13'b0, hbc[2:0]} : das[15:0];

always @* begin
  bcn = bc - 1'd1;
end

assign gpdma_done_pre = ch_active & xfer & gpdma & (bcn == 16'd0);

assign ntrlm1 = {ntrl[7], ntrl[6:0] - 7'd1};
assign ntrln_latch = hinit | ~|ntrlm1[6:0];

always @* begin
  ntrln = ntrlm1;
  if (ntrln_latch)
    ntrln = dl;
end


//////////////////////////////////////////////////////////////////////
// A- and B-Bus leader

reg [23:0] a_o;
reg [7:0]  pa_o;

assign abad = ind_data ? das : (gpdma ? a1t : a2a);

always @* begin
  a_o = 24'bx;
  pa_o = 8'bx;
  if (ch_active) begin
    a_o = abad;
    pa_o = bbad + boff;
  end
end

wire aboe = active;
wire rwoe = ~SLOW_GATE_ACTIVE & ch_active;
wire awr = xfer & b2a;
wire ard = (xfer & ~b2a) | (hinit | htbl);
wire bwr = xfer & ~b2a;
wire brd = xfer & b2a;

assign A_O = a_o;
assign PA_O = pa_o;
assign A_OE = aboe;
assign PA_OE = aboe;
assign nWR_O = ~(rwoe & awr);
assign nRD_O = ~(rwoe & ard);
assign nPARD_O = ~(rwoe & brd);
assign nPAWR_O = ~(rwoe & bwr);


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout;
reg        mon_sel_dmac_ch_reg0123, mon_sel_dmac_ch_reg4567,
           mon_sel_dmac_ch_reg89AB, mon_sel_dmac_st0, mon_sel_dmac_int0,
           mon_sel_dmac_int1;
reg [2:0]  mon_sel_dmac_ch_sel;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  MON_ACK = 1'b1;
  mon_sel_dmac_ch_sel = 3'dx;
  mon_sel_dmac_ch_reg0123 = 1'b0;
  mon_sel_dmac_ch_reg4567 = 1'b0;
  mon_sel_dmac_ch_reg89AB = 1'b0;
  mon_sel_dmac_st0 = 1'b0;
  mon_sel_dmac_int0 = 1'b0;
  mon_sel_dmac_int1 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL[7:2])         //``SUBREGS CPU
    6'h20: begin              //``REG DMAC_CH0_REG0123
      mon_sel_dmac_ch_sel = 3'd0;
      mon_sel_dmac_ch_reg0123 = 1'b1;
    end
    6'h21: begin              //``REG DMAC_CH0_REG4567
      mon_sel_dmac_ch_sel = 3'd0;
      mon_sel_dmac_ch_reg4567 = 1'b1;
    end
    6'h22: begin              //``REG DMAC_CH0_REG89AB
      mon_sel_dmac_ch_sel = 3'd0;
      mon_sel_dmac_ch_reg89AB = 1'b1;
    end
    6'h23: begin              //``REG DMAC_CH1_REG0123
      mon_sel_dmac_ch_sel = 3'd1;
      mon_sel_dmac_ch_reg0123 = 1'b1;
    end
    6'h24: begin
      mon_sel_dmac_ch_sel = 3'd1;
      mon_sel_dmac_ch_reg4567 = 1'b1;
    end
    6'h25: begin
      mon_sel_dmac_ch_sel = 3'd1;
      mon_sel_dmac_ch_reg89AB = 1'b1;
    end
    6'h26: begin
      mon_sel_dmac_ch_sel = 3'd2;
      mon_sel_dmac_ch_reg0123 = 1'b1;
    end
    6'h27: begin
      mon_sel_dmac_ch_sel = 3'd2;
      mon_sel_dmac_ch_reg4567 = 1'b1;
    end
    6'h28: begin
      mon_sel_dmac_ch_sel = 3'd2;
      mon_sel_dmac_ch_reg89AB = 1'b1;
    end
    6'h29: begin
      mon_sel_dmac_ch_sel = 3'd3;
      mon_sel_dmac_ch_reg0123 = 1'b1;
    end
    6'h2A: begin
      mon_sel_dmac_ch_sel = 3'd3;
      mon_sel_dmac_ch_reg4567 = 1'b1;
    end
    6'h2B: begin
      mon_sel_dmac_ch_sel = 3'd3;
      mon_sel_dmac_ch_reg89AB = 1'b1;
    end
    6'h2C: begin
      mon_sel_dmac_ch_sel = 3'd4;
      mon_sel_dmac_ch_reg0123 = 1'b1;
    end
    6'h2D: begin
      mon_sel_dmac_ch_sel = 3'd4;
      mon_sel_dmac_ch_reg4567 = 1'b1;
    end
    6'h2E: begin
      mon_sel_dmac_ch_sel = 3'd4;
      mon_sel_dmac_ch_reg89AB = 1'b1;
    end
    6'h2F: begin
      mon_sel_dmac_ch_sel = 3'd5;
      mon_sel_dmac_ch_reg0123 = 1'b1;
    end
    6'h30: begin
      mon_sel_dmac_ch_sel = 3'd5;
      mon_sel_dmac_ch_reg4567 = 1'b1;
    end
    6'h31: begin
      mon_sel_dmac_ch_sel = 3'd5;
      mon_sel_dmac_ch_reg89AB = 1'b1;
    end
    6'h32: begin
      mon_sel_dmac_ch_sel = 3'd6;
      mon_sel_dmac_ch_reg0123 = 1'b1;
    end
    6'h33: begin
      mon_sel_dmac_ch_sel = 3'd6;
      mon_sel_dmac_ch_reg4567 = 1'b1;
    end
    6'h34: begin
      mon_sel_dmac_ch_sel = 3'd6;
      mon_sel_dmac_ch_reg89AB = 1'b1;
    end
    6'h35: begin
      mon_sel_dmac_ch_sel = 3'd7;
      mon_sel_dmac_ch_reg0123 = 1'b1;
    end
    6'h36: begin
      mon_sel_dmac_ch_sel = 3'd7;
      mon_sel_dmac_ch_reg4567 = 1'b1;
    end
    6'h37: begin
      mon_sel_dmac_ch_sel = 3'd7;
      mon_sel_dmac_ch_reg89AB = 1'b1;
    end
    6'h38: begin              //``REG DMAC_ST0
      mon_sel_dmac_st0 = 1'b1;
      mon_dout[0] = active;
      mon_dout[1] = gpdma;
      mon_dout[2] = hdma;
      mon_dout[3] = xfer;
      mon_dout[4] = ch_active;
      mon_dout[5] = done;
      mon_dout[15:8] = hcdis;
      mon_dout[23:16] = hxfer;
    end
    6'h39: begin                //``REG DMAC_BL0
      mon_dout[23:0] = A_O;
      mon_dout[31:24] = PA_O;
    end
    6'h3A: begin                //``REG DMAC_BL1
      mon_dout[0] = A_OE;
      mon_dout[1] = PA_OE;
      mon_dout[2] = nWR_O;
      mon_dout[3] = nRD_O;
      mon_dout[4] = nPAWR_O;
      mon_dout[5] = nPARD_O;
    end
    6'h3C: begin                //``REG DMAC_INT0
      mon_sel_dmac_int0 = 1'b1;
      mon_dout[0] = ntrlz;
      mon_dout[1] = post_vbl_end;
      mon_dout[2] = hstartl;
      mon_dout[3] = hstart;
      mon_dout[4] = gprestart_d;
      mon_dout[7:5] = aca;
      mon_dout[15:8] = dl;
      mon_dout[23:16] = men;
      mon_dout[31:24] = hen_d;
    end
    6'h3D: begin                //``REG DMAC_INT1
      mon_sel_dmac_int1 = 1'b1;
      mon_dout[7:0] = cen;
      mon_dout[9:8] = gptpos;
      mon_dout[12:10] = hst;
      mon_dout[15:13] = hbc;
    end
    default: begin
      MON_ACK = 1'b0;
    end
  endcase

  if (mon_sel_dmac_ch_reg0123) begin
    mon_dout[07:00] = dreg[mon_sel_dmac_ch_sel]['h0];
    mon_dout[15:08] = dreg[mon_sel_dmac_ch_sel]['h1];
    mon_dout[23:16] = dreg[mon_sel_dmac_ch_sel]['h2];
    mon_dout[31:24] = dreg[mon_sel_dmac_ch_sel]['h3];
  end
  if (mon_sel_dmac_ch_reg4567) begin
    mon_dout[07:00] = dreg[mon_sel_dmac_ch_sel]['h4];
    mon_dout[15:08] = dreg[mon_sel_dmac_ch_sel]['h5];
    mon_dout[23:16] = dreg[mon_sel_dmac_ch_sel]['h6];
    mon_dout[31:24] = dreg[mon_sel_dmac_ch_sel]['h7];
  end
  if (mon_sel_dmac_ch_reg89AB) begin
    mon_dout[07:00] = dreg[mon_sel_dmac_ch_sel]['h8];
    mon_dout[15:08] = dreg[mon_sel_dmac_ch_sel]['h9];
    mon_dout[23:16] = dreg[mon_sel_dmac_ch_sel]['hA];
    mon_dout[31:24] = dreg[mon_sel_dmac_ch_sel]['hB];
  end
end

assign mon_write = MON_READY & |MON_WS & ~MON_VALID;
assign mon_write_dmac_ch_reg0123 = mon_write & mon_sel_dmac_ch_reg0123;
assign mon_write_dmac_ch_reg4567 = mon_write & mon_sel_dmac_ch_reg4567;
assign mon_write_dmac_ch_reg89AB = mon_write & mon_sel_dmac_ch_reg89AB;
assign mon_write_dmac_st0 = mon_write & mon_sel_dmac_st0;
assign mon_write_dmac_int0 = mon_write & mon_sel_dmac_int0;
assign mon_write_dmac_int1 = mon_write & mon_sel_dmac_int1;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK & MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end

endmodule
