// SNES S-CPU / 5A22

`timescale 1us / 1ns

module s_cpu
  (
   input         nRES, // reset (active-low)
   input         MCLK, // main clock
   output        CP1_POSEDGE, // clock phase 1
   output        CP1_NEGEDGE,
   output        CP2_POSEDGE, // clock phase 2
   output        CP2_NEGEDGE,
   output        CP2_NEGEDGE_PRE,
   input         HOLD, // clock stall

   // debug monitor (excluding the CPU core)
   input [7:0]   MON_SEL,
   input         MON_READY,
   output [31:0] MON_DOUT,
   output        MON_VALID,
   input [3:0]   MON_WS,
   input [31:0]  MON_DIN,

   // CPU core
   input         nIRQ, // interrupt request (active-low)
   input         nNMI, // non-maskable interrupt (active-low)
   input         nABORT, // abort input (active-low)
   output        nML, // memory lock (active-low)
   output        MF, // memory select status
   output        XF, // index select status
   output        RW, // read / not write
   input         RDY, // ready, input
   output        nVP, // vector pull (active-low)
   output        VDA, // valid data address
   output        VPA, // valid program address

   // A-Bus interface
   output [23:0] A, // address bus, output
   input [7:0]   D_I, // data bus, input
   output [7:0]  D_O, // data bus, output
   output        D_OE, // data bus, output enable
   output        nROMSEL, // ROM chip select
   output        nWRAMSEL, // WRAM chip select
   output        nWR, // write (active-low)
   output        nRD, // read (active-low)

   // B-Bus interface
   output [7:0]  PA, // address bus, output
   output        nPARD, // read (active-low)
   output        nPAWR, // write (active-low)

   // Controller I/O
   output [2:1]  JPCLK,
   output [2:0]  JPOUT,
   input [1:0]   JP1D, // 4016.D0-1
   input [4:0]   JP2D, // 4017.D0-4
   input [7:0]   JPIO_I,
   output [7:0]  JPIO_O,

   // Video I/O
   input         CC, // PPU pixel clock
   input         HBLANK,
   input         VBLANK,

   // CPU core debug monitor
   input [5:0]   CORE_MON_SEL,
   input         CORE_MON_WS,
   output [31:0] CORE_MON_DOUT,
   input [31:0]  CORE_MON_DIN
   );

wire        cp2;
wire        slowck_sel, cpu_rw_hold;
wire        slow_cpu, fast_cpu;
wire        cpu_gate_active;
wire        cpu_cp1_posedge_gated, cpu_cp1_negedge_gated,
            cpu_cp2_posedge_gated, cpu_cp2_negedge_gated;
wire        refresh_en, slow_refresh_en;
wire        slow_cp1_posedge;
wire        slow_gate_active;
wire        memsel, ncs_joy;

reg [7:0]   d;

wire [23:0] cpu_a;
wire [7:0]  cpu_d_o;
wire        cpu_d_oe;
wire        cpu_rw;
wire        cpu_nrd, cpu_nwr;
wire [7:0]  ba;
wire        nce_sys, nce_loram, nce_io, nce_rom, nce_ram;
wire        nce_mem1, nce_mem2;
wire        cpu_int;         // CPU internal cycle
wire        cpu_npard, cpu_npawr;
wire        nio_int;         // on-chip I/O cycle, active-low
wire        nio_ext;         // external (B-Bus) I/O cycle, active-low

wire [7:0]  ocio_d_o;
wire        ocio_d_oe;
wire        ocio_nrd, ocio_nwr;

wire [23:0] ocio_a_o;
wire        ocio_a_oe, ocio_nwr_o, ocio_nrd_o;
wire [7:0]  ocio_pa_o;
wire        ocio_pa_oe, ocio_npard_o, ocio_npawr_o;

wire        nvbl, nhvt;

wire [31:0] mon_dout_clkgen, mon_dout_ocio;
wire        mon_ack_clkgen, mon_ack_ocio;
wire        mon_valid_clkgen, mon_valid_ocio;

//////////////////////////////////////////////////////////////////////
// Internal busses

always @* begin
  case ({ocio_d_oe, cpu_d_oe})
    2'b00: d = D_I;
    2'b01: d = cpu_d_o;
    2'b10: d = ocio_d_o;
    default: d = 8'hxx;
  endcase
end

assign D_O = cpu_d_o;
assign D_OE = cpu_d_oe;


//////////////////////////////////////////////////////////////////////
// Clocks and related

s_cpu_clkgen clkgen
  (
   .CLK_RESET(1'b0),
   .CLK(MCLK),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_clkgen),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_clkgen),
   .MON_VALID(mon_valid_clkgen),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .SLOWCK_SEL(slowck_sel),
   .CLK_HOLD(HOLD),
   .FAST_CPU(fast_cpu),
   .SLOW_CPU(slow_cpu),
   .REFRESH_EN(refresh_en),

   .CP1_POSEDGE(CP1_POSEDGE),
   .CP1_NEGEDGE(CP1_NEGEDGE),
   .CP2(cp2),
   .CP2_POSEDGE(CP2_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .CP2_NEGEDGE_PRE(CP2_NEGEDGE_PRE),

   .CPU_GATE_ACTIVE(cpu_gate_active),
   .CPU_CP1_POSEDGE_GATED(cpu_cp1_posedge_gated),
   .CPU_CP1_NEGEDGE_GATED(cpu_cp1_negedge_gated),
   .CPU_CP2_POSEDGE_GATED(cpu_cp2_posedge_gated),
   .CPU_CP2_NEGEDGE_GATED(cpu_cp2_negedge_gated),

   .SLOW_CP1_POSEDGE(slow_cp1_posedge),
   .SLOW_GATE_ACTIVE(slow_gate_active),
   .SLOW_REFRESH_EN(slow_refresh_en)
   );

assign cpu_rw_hold = cpu_gate_active;


//////////////////////////////////////////////////////////////////////
// CPU core

w65c816s s_cpu_w65c816s
  (
   .RESB(nRES),
   .CLK(MCLK),
   .CP1_POSEDGE(cpu_cp1_posedge_gated),
   .CP1_NEGEDGE(cpu_cp1_negedge_gated),
   .CP2_POSEDGE(cpu_cp2_posedge_gated),
   .CP2_NEGEDGE(cpu_cp2_negedge_gated),
   .HOLD(HOLD),
   .IRQB(nIRQ & nhvt),
   .NMIB(nNMI & nvbl),
   .ABORTB(nABORT),
   .E(),
   .MLB(nML),
   .M(MF),
   .X(XF),
   .A(cpu_a),
   .D_I(d),
   .D_O(cpu_d_o),
   .RWB(cpu_rw),
   .RDYIN(1'b1), // TODO: active DMA deasserts RDYIN
   .WAITN(),
   .VPB(nVP),
   .VDA(VDA),
   .VPA(VPA),

   .MON_SEL(CORE_MON_SEL),
   .MON_WS(CORE_MON_WS),
   .MON_DOUT(CORE_MON_DOUT),
   .MON_DIN(CORE_MON_DIN)
   );

assign cpu_int = ~(VDA | VPA);
assign cpu_nrd = ~cpu_rw | cpu_int | cpu_rw_hold;
assign cpu_nwr = cpu_rw | cpu_int | cpu_rw_hold;
assign cpu_d_oe = ~cpu_nwr;

assign RW = cpu_rw | cpu_rw_hold;


//////////////////////////////////////////////////////////////////////
// Address decode

assign ba[7:0] = A[23:16];

// Full 24-bit address space decode
assign nce_sys = ba[6] | A[15];          // 00-3F:0000-7FFF: system area
                                         // 80-BF:0000-7FFF:    "    "
assign nce_loram = nce_sys | |A[15:13];  //       0000-1FFF: low WRAM
assign nce_io = nce_sys | ~^A[14:13];    //       2000-5FFF: I/O
assign nce_rom = ~(nce_sys & nce_ram);   // 00-3F:8000-FFFF: ROM
                                         // 40-7D:0000-FFFF: ROM
                                         // 80-BF:8000-FFFF:  "
                                         // C0-FF:0000-FFFF:  "
assign nce_ram = ba[7] | ~&ba[6:1];      // 7E-7F:0000-FFFF: WRAM

assign nce_mem1 = nce_rom | ba[7];  // Memory-1: ROM in banks 00h-7Fh
assign nce_mem2 = nce_rom | ~ba[7]; // Memory-2: ROM in banks 80h-FFh

assign A = ocio_a_oe ? ocio_a_o : cpu_a;
assign nROMSEL = nce_rom;
assign nWRAMSEL = nce_loram & nce_ram;
assign nWR = cpu_nwr & ocio_nwr_o;
assign nRD = cpu_nrd & ocio_nrd_o;

// I/O address decode
assign nio_ext = nce_io | A[14:8] != 7'h21; //    2100-21FF: B-Bus I/O 
assign nio_int = nce_io | ~A[14];           //    4000-5FFF: on-chip I/O

assign cpu_npard = cpu_nrd | nio_ext;
assign cpu_npawr = cpu_nwr | nio_ext;

assign nPARD = ~cp2 | (cpu_npard & ocio_npard_o);
assign nPAWR = ~cp2 | (cpu_npawr & ocio_npawr_o);
assign PA = ocio_pa_oe ? ocio_pa_o : A[7:0];

// CPU clock waitstate selection
assign slow_cpu = ~cpu_int & ~ncs_joy;
assign fast_cpu = cpu_int | (~nce_mem2 & memsel);


//////////////////////////////////////////////////////////////////////
// On-chip I/O

s_cpu_ocio ocio
  (
   .nRES(nRES),
   .CLK(MCLK),
   .CP1_POSEDGE(CP1_POSEDGE),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .SLOW_CP1_POSEDGE(slow_cp1_posedge),
   .HOLD(HOLD),
   .SLOWCK_SEL(slowck_sel),
   .SLOW_CPU(slow_cpu),
   .REFRESH_EN(refresh_en),
   .SLOW_GATE_ACTIVE(slow_gate_active),
   .SLOW_REFRESH_EN(slow_refresh_en),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_ocio),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_ocio),
   .MON_VALID(mon_valid_ocio),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .RA(A[12:0]),
   .D_I(d),
   .D_O(ocio_d_o),
   .D_OE(ocio_d_oe),
   .nRD(ocio_nrd),
   .nWR(ocio_nwr),

   .MEMSEL(memsel),
   .NCS_JOY(ncs_joy),

   .JPOUT(JPOUT),
   .JPCLK(JPCLK),
   .JP1D(JP1D),
   .JP2D(JP2D),
   .JPIO_I(JPIO_I),
   .JPIO_O(JPIO_O),

   .CC(CC),
   .HBLANK(HBLANK),
   .VBLANK(VBLANK),

   .A_O(ocio_a_o),
   .A_OE(ocio_a_oe),
   .nWR_O(ocio_nwr_o),
   .nRD_O(ocio_nrd_o),
   .PA_O(ocio_pa_o),
   .PA_OE(ocio_pa_oe),
   .nPARD_O(ocio_npard_o),
   .nPAWR_O(ocio_npawr_o),

   .nVBL(nvbl),
   .nHVT(nhvt)
   );

assign ocio_nrd = cpu_nrd | nio_int;
assign ocio_nwr = cpu_nwr | nio_int;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0]  mon_dout;
reg         mon_valid;

always @* begin
  if (mon_ack_clkgen) begin
    mon_valid = mon_valid_clkgen;
    mon_dout = mon_dout_clkgen;
  end
  else if (mon_ack_ocio) begin
    mon_valid = mon_valid_ocio;
    mon_dout = mon_dout_ocio;
  end
  else begin
    mon_valid = 1'b1;
    mon_dout = 32'h0;
  end
end

assign MON_DOUT = mon_dout;
assign MON_VALID = mon_valid;

endmodule
