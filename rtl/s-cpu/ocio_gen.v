// General I/O: $42xx

`timescale 1us / 1ns

module s_cpu_ocio_gen
  (
   input             nRES,
   input             CLK,
   input             CP1_POSEDGE,
   input             CP2_NEGEDGE,
   input             SLOW_CP1_POSEDGE,
   input             HOLD,
   input             SLOW_CPU,
   output            REFRESH_EN,

   input [7:0]       MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input [7:0]       RA,
   input [7:0]       D_I,
   output [7:0]      D_O,
   output            D_OE,
   input             nRD,
   input             nWR,

   output reg        MEMSEL,

   input [7:0]       JPIO_I,
   output [7:0]      JPIO_O,

   input             CC,
   input             HBLANK,
   input             VBLANK,

   output reg [7:0]  DMA_MSET,
   output reg [7:0]  DMA_HEN,
   output            VBL_START,
   output            VBL_END,
   output            HBL_START,

   output reg        AJPR_EN,
   input             AJPR_BUSY,
   input [15:0]      JOY1,
   input [15:0]      JOY2,
   input [15:0]      JOY3,
   input [15:0]      JOY4,

   output            nVBL,
   output            nHVT
   );

localparam [3:0] CPU_VER = 4'd1; // I guess?

wire            vbl_flag;
reg             vbl_en, vbl_flag_d, vbl_set, vbl_clear_flags, vbl_buf;
reg             cc_d, vblank_d, hblank_d;
wire            vbl_start, vbl_end, hbl_start, hbl_end;
reg [1:0]       hvt_mode;
wire            hvt_en, hvt_flag;
reg             hvt_match, hvt_flag_d, hvt_set, hvt_clear_flags, hvt_buf;

wire            wrmpya, wrmpyb, wrdivl, wrdivh, wrdivb;
wire [15:0]     div, mpy;

wire [7:0]      jpi;
reg [7:0]       jpo;

reg [8:0]       hcnt, vcnt, htime, vtime;

wire ['h0D:'h00]    wsel;
wire ['h1F:'h10]    rsel;
genvar              a;
reg [7:0]           d_o;

wire [31:0]         mon_dout_muldiv;
wire                mon_ack_muldiv;
wire                mon_valid_muldiv;
wire                mon_write_gen0, mon_write_gen1, mon_write_gen2, mon_write_gen3;


//////////////////////////////////////////////////////////////////////
// Write-only registers

`define REG_NMITIMEN 	'h00   // 4200h
`define REG_WRIO     	'h01
`define REG_WRMPYA   	'h02
`define REG_WRMPYB   	'h03
`define REG_WRDIVL   	'h04
`define REG_WRDIVH   	'h05
`define REG_WRDIVB   	'h06
`define REG_HTIMEL   	'h07
`define REG_HTIMEH   	'h08
`define REG_VTIMEL   	'h09
`define REG_VTIMEH   	'h0A
`define REG_MDMAEN   	'h0B
`define REG_HDMAEN   	'h0C
`define REG_MEMSEL   	'h0D

generate
  for (a = `REG_NMITIMEN; a <= `REG_MEMSEL; a = a + 1) begin :wsela
    assign wsel[a] = ~nWR & (RA == a);
  end
endgenerate

initial begin
  htime = 9'h1ff;
  vtime = 9'h1ff;
end

always @(posedge CLK) begin
  DMA_MSET <= 8'b0;

  if (!HOLD) begin
    if (~nRES) begin
      DMA_HEN <= 8'b0;
      AJPR_EN <= 1'b0;
      vbl_en <= 1'b0;
      hvt_mode <= 2'b0;
      jpo <= 8'hff;
      MEMSEL <= 1'b0;
    end
    else begin
      case (1'b1)
        wsel[`REG_NMITIMEN]: begin
          AJPR_EN <= D_I[0];
          vbl_en <= D_I[7];
          hvt_mode <= D_I[5:4];
        end
        wsel[`REG_MDMAEN]: begin
          DMA_MSET <= D_I;
        end
        wsel[`REG_HDMAEN]: begin
          DMA_HEN <= D_I;
        end
        wsel[`REG_WRIO]: begin
          jpo <= D_I;
        end
        wsel[`REG_HTIMEL]: begin
          htime[7:0] <= D_I;
        end
        wsel[`REG_HTIMEH]: begin
          htime[8] <= D_I[0];
        end
        wsel[`REG_VTIMEL]: begin
          vtime[7:0] <= D_I;
        end
        wsel[`REG_VTIMEH]: begin
          vtime[8] <= D_I[0];
        end
        wsel[`REG_MEMSEL]: begin
          MEMSEL <= D_I[0];
        end
        default: ;
      endcase
    end
  end

  if (mon_write_gen0) begin
    if (MON_WS[1]) begin
      AJPR_EN <= MON_DIN[8];
      vbl_en <= MON_DIN[9];
      hvt_mode <= MON_DIN[11:10];
      MEMSEL <= MON_DIN[12];
    end
  end
  if (mon_write_gen1) begin
    if (MON_WS[0])
      DMA_MSET <= MON_DIN[7:0];
    if (MON_WS[1])
      DMA_HEN <= MON_DIN[15:8];
  end
  if (mon_write_gen2) begin
    if (MON_WS[1])
      jpo <= MON_DIN[15:8];
  end
  if (mon_write_gen3) begin
    if (MON_WS[0])
      htime[7:0] <= MON_DIN[7:0];
    if (MON_WS[1])
      htime[8] <= MON_DIN[8];
    if (MON_WS[2])
      vtime[7:0] <= MON_DIN[23:16];
    if (MON_WS[3])
      vtime[8] <= MON_DIN[24];
  end
end


//////////////////////////////////////////////////////////////////////
// Read-only registers

`define REG_RDNMI       'h10   // 4210h
`define REG_TIMEUP  	'h11
`define REG_HVBJOY  	'h12
`define REG_RDIO    	'h13
`define REG_RDDIVL  	'h14
`define REG_RDDIVH  	'h15
`define REG_RDMPYL  	'h16
`define REG_RDMPYH  	'h17
`define REG_JOY1L   	'h18
`define REG_JOY1H   	'h19
`define REG_JOY2L   	'h1A
`define REG_JOY2H   	'h1B
`define REG_JOY3L   	'h1C
`define REG_JOY3H   	'h1D
`define REG_JOY4L   	'h1E
`define REG_JOY4H   	'h1F

generate
  for (a = `REG_RDNMI; a <= `REG_JOY4H; a = a + 1) begin : rsela
    assign rsel[a] = ~nRD & (RA == a);
  end
endgenerate

// TODO: Open-bus the unused bits
always @* begin
  case (1'b1)
    rsel[`REG_RDNMI]:   d_o = {vbl_buf, 3'b0, CPU_VER};
    rsel[`REG_TIMEUP]:  d_o = {hvt_buf, 7'b0};
    rsel[`REG_HVBJOY]:  d_o = {VBLANK, HBLANK, 5'b0, AJPR_BUSY};
    rsel[`REG_RDIO]:    d_o = jpi;
    rsel[`REG_RDDIVL]:  d_o = div[7:0];
    rsel[`REG_RDDIVH]:  d_o = div[15:8];
    rsel[`REG_RDMPYL]:  d_o = mpy[7:0];
    rsel[`REG_RDMPYH]:  d_o = mpy[15:8];
    rsel[`REG_JOY1L]:   d_o = JOY1[7:0];
    rsel[`REG_JOY1H]:   d_o = JOY1[15:8];
    rsel[`REG_JOY2L]:   d_o = JOY2[7:0];
    rsel[`REG_JOY2H]:   d_o = JOY2[15:8];
    rsel[`REG_JOY3L]:   d_o = JOY3[7:0];
    rsel[`REG_JOY3H]:   d_o = JOY3[15:8];
    rsel[`REG_JOY4L]:   d_o = JOY4[7:0];
    rsel[`REG_JOY4H]:   d_o = JOY4[15:8];
    default:            d_o = 8'hxx;
  endcase
end

assign D_O = d_o;
assign D_OE = |rsel;


//////////////////////////////////////////////////////////////////////
// Joypad I/O (JPIO)
// 8 bidirectional pins. Drives weak high, strong low.

assign JPIO_O = jpo;
assign jpi = JPIO_I & jpo;


//////////////////////////////////////////////////////////////////////
// Video blanking interval flag

initial begin
  vbl_en = 1'b0;
  vbl_flag_d = 1'b0;
  vbl_set = 1'b0;
  vbl_clear_flags = 1'b0;
end

// Latching VBLANK on the falling edge of HBLANK makes vbl_start/end
// last a full scanline, 
assign vbl_start = VBLANK & ~vblank_d;
assign vbl_end = ~VBLANK & vblank_d;

always @(posedge CLK) begin
  if (!HOLD)
    vblank_d <= VBLANK;

  if (mon_write_gen0) begin
    if (MON_WS[1])
      vblank_d <= MON_DIN[13];
  end
end

always @(posedge CLK) begin
  if (!HOLD) begin
    if (~nRES)
      vbl_set <= 1'b0;
    else begin
      if (vbl_start)
        vbl_set <= 1'b1;
      else
        vbl_set <= 1'b0;
    end
  end

  if (mon_write_gen0) begin
    if (MON_WS[1])
      vbl_set <= MON_DIN[14];
  end
end

always @(posedge CLK) begin
  if (!HOLD) begin
    if (~nRES)
      vbl_clear_flags <= 1'b0;
    else
      vbl_clear_flags <= vbl_end;
  end

  if (mon_write_gen0) begin
    if (MON_WS[1])
      vbl_clear_flags <= MON_DIN[15];
  end
end

// To prevent RDNMI read from hiding a simultaneous vbl_set set, mask
// clear-on-read when the read indicates vbl_buf is clear.
wire vbl_clr = (~nRES) | vbl_clear_flags | (rsel[`REG_RDNMI] & vbl_buf);

assign vbl_flag = (vbl_flag_d | vbl_set) & ~vbl_clr;

always @(posedge CLK) begin
  vbl_flag_d <= vbl_flag;

  if (~rsel[`REG_RDNMI]) begin
    vbl_buf <= vbl_flag;
  end

  if (mon_write_gen0) begin
    if (MON_WS[0])
      vbl_buf <= MON_DIN[4];
    if (MON_WS[2])
      vbl_flag_d <= MON_DIN[17];
  end
end

assign VBL_START = vbl_start;
assign VBL_END = vbl_end;
assign nVBL = ~(vbl_flag & vbl_en);


//////////////////////////////////////////////////////////////////////
// H/V timer counter and interrupt

always @(posedge CLK) begin
  if (!HOLD) begin
    cc_d <= CC; // align pixel clock with vbl_end/hbl_end
    hblank_d <= HBLANK;
  end

  if (mon_write_gen0) begin
    if (MON_WS[2])
      hblank_d <= MON_DIN[21];
    if (MON_WS[3])
      cc_d <= MON_DIN[29];
  end
end

assign hbl_start = HBLANK & ~hblank_d;
assign hbl_end = ~HBLANK & hblank_d;

always @(posedge CLK) begin
  if (!HOLD) begin
    if (~nRES) begin
      hcnt <= 9'h1ff;
      vcnt <= 9'h1ff;
    end
    else begin
      if (vbl_end) begin // @(negedge VBLANK), PPU ROW = 0, COL = 0
        hcnt <= 9'd0;
        vcnt <= 9'd0;
      end
      else if (hbl_end) begin // @(negedge HBLANK), PPU COL = 0
        hcnt <= 9'd0;
        vcnt <= vcnt + 1'd1;
      end
      else if (cc_d)
        hcnt <= hcnt + 1'd1;
    end
  end

  if (mon_write_gen1) begin
    if (MON_WS[2])
      hcnt[7:0] <= MON_DIN[23:16];
    if (MON_WS[3])
      hcnt[8] <= MON_DIN[24];
  end
  if (mon_write_gen2) begin
    if (MON_WS[2])
      vcnt[7:0] <= MON_DIN[23:16];
    if (MON_WS[3])
      vcnt[8] <= MON_DIN[24];
  end
end

assign hvt_en = |hvt_mode;

always @* begin
  case (hvt_mode)
    default: hvt_match = 1'b0;
    2'b01: hvt_match = (hcnt == htime);
    2'b10: hvt_match = (vcnt == vtime) & (hcnt == 9'd0);
    2'b11: hvt_match = (vcnt == vtime) & (hcnt == htime);
  endcase
end

always @(posedge CLK) begin
  if (!HOLD) begin
    if (~nRES)
      hvt_set <= 1'b0;
    else begin
      if (hvt_match)
        hvt_set <= 1'b1;
      else
        hvt_set <= 1'b0;
    end
  end

  if (mon_write_gen0) begin
    if (MON_WS[3])
      hvt_set <= MON_DIN[24];
  end
end

// Disabling the timer clears the flag.
always @(posedge CLK) begin
  if (!HOLD) begin
    if (~nRES)
      hvt_clear_flags <= 1'b0;
    else
      hvt_clear_flags <= ~hvt_en;
  end

  if (mon_write_gen0) begin
    if (MON_WS[3])
      hvt_clear_flags <= MON_DIN[25];
  end
end

// To prevent TIMEUP read from hiding a simultaneous hvt_set set, mask
// clear-on-read when the read indicates hvt_buf is clear.
wire hvt_clr = (~nRES) | hvt_clear_flags | (rsel[`REG_TIMEUP] & hvt_buf);

assign hvt_flag = (hvt_flag_d | hvt_set) & ~hvt_clr;

always @(posedge CLK) begin
  if (!HOLD) begin
    hvt_flag_d <= hvt_flag;

    if (~rsel[`REG_TIMEUP]) begin
      hvt_buf <= hvt_flag;
    end
  end

  if (mon_write_gen0) begin
    if (MON_WS[0])
      hvt_buf <= MON_DIN[5];
    if (MON_WS[3])
      hvt_flag_d <= MON_DIN[27];
  end
end

assign HBL_START = hbl_start;
assign nHVT = ~(hvt_flag & hvt_en);


//////////////////////////////////////////////////////////////////////
// Multiply and divide unit

assign wrmpya = wsel[`REG_WRMPYA];
assign wrmpyb = wsel[`REG_WRMPYB];
assign wrdivl = wsel[`REG_WRDIVL];
assign wrdivh = wsel[`REG_WRDIVH];
assign wrdivb = wsel[`REG_WRDIVB];

s_cpu_muldiv muldiv
  (
   .nRES(nRES),
   .CLK(CLK),
   .CP2_NEGEDGE(CP2_NEGEDGE),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_muldiv),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_muldiv),
   .MON_VALID(mon_valid_muldiv),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .D_I(D_I),
   .WRMPYA(wrmpya),
   .WRMPYB(wrmpyb),
   .WRDIVL(wrdivl),
   .WRDIVH(wrdivh),
   .WRDIVB(wrdivb),

   .DIV(div),
   .MPY(mpy)
   );


//////////////////////////////////////////////////////////////////////
// DRAM refresh

reg refresh_en;

always @(posedge CLK) begin
  if (!HOLD) begin
    if (~nRES)
      refresh_en <= 1'b0;
    else begin
      if ((hcnt >= 9'd128) & (hcnt <= 9'd135))
        refresh_en <= 1'b1;
      else if (hcnt[3] & hcnt[1] & (~SLOW_CPU | hcnt[0]))
        refresh_en <= 1'b0;
    end
  end

  if (mon_write_gen0) begin
    if (MON_WS[3])
      refresh_en <= MON_DIN[30];
  end
end

assign REFRESH_EN = refresh_en;


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout, mon_dout_gen;
reg        mon_ack_gen;
reg        mon_valid_gen;
reg        mon_sel_gen0, mon_sel_gen1, mon_sel_gen2, mon_sel_gen3;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  mon_ack_gen = 1'b1;
  mon_sel_gen0 = 1'b0;
  mon_sel_gen1 = 1'b0;
  mon_sel_gen2 = 1'b0;
  mon_sel_gen3 = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL[7:2])         //``SUBREGS CPU
    6'h10: begin              //``REG GEN0
      mon_sel_gen0 = 1'b1;
      mon_dout[3:0] = CPU_VER;
      mon_dout[4] = vbl_buf;
      mon_dout[5] = hvt_buf;
      mon_dout[6] = HBLANK;
      mon_dout[7] = VBLANK;
      mon_dout[8] = AJPR_EN;
      mon_dout[9] = vbl_en;
      mon_dout[11:10] = hvt_mode;
      mon_dout[12] = MEMSEL;
      mon_dout[13] = vblank_d;
      mon_dout[14] = vbl_set;
      mon_dout[15] = vbl_clear_flags;
      mon_dout[16] = vbl_flag;
      mon_dout[17] = vbl_flag_d;
      mon_dout[18] = VBL_START;
      mon_dout[19] = VBL_END;
      mon_dout[20] = nVBL;
      mon_dout[21] = hblank_d;
      mon_dout[22] = hbl_start;
      mon_dout[23] = hbl_end;
      mon_dout[24] = hvt_set;
      mon_dout[25] = hvt_clear_flags;
      mon_dout[26] = hvt_flag;
      mon_dout[27] = hvt_flag_d;
      mon_dout[28] = nHVT;
      mon_dout[29] = cc_d;
      mon_dout[30] = refresh_en;
    end
    6'h11: begin              //``REG GEN1
      mon_sel_gen1 = 1'b1;
      mon_dout[7:0] = DMA_MSET;
      mon_dout[15:8] = DMA_HEN;
      mon_dout[24:16] = hcnt;
    end
    6'h12: begin              //``REG GEN2
      mon_sel_gen2 = 1'b1;
      mon_dout[7:0] = jpi;
      mon_dout[15:8] = jpo;
      mon_dout[24:16] = vcnt;
    end
    6'h13: begin              //``REG GEN3
      mon_sel_gen3 = 1'b1;
      mon_dout[8:0] = htime;
      mon_dout[24:16] = vtime;
    end
    default: begin
      mon_ack_gen = 1'b0;
    end
  endcase
end

assign mon_write = MON_READY & |MON_WS & ~mon_valid_gen;
assign mon_write_gen0 = mon_write & mon_sel_gen0;
assign mon_write_gen1 = mon_write & mon_sel_gen1;
assign mon_write_gen2 = mon_write & mon_sel_gen2;
assign mon_write_gen3 = mon_write & mon_sel_gen3;

always @(posedge CLK) begin
  mon_valid_gen <= 1'b0;
  if (mon_ack_gen & MON_READY) begin
    mon_valid_gen <= 1'b1;
    mon_dout_gen <= mon_dout;
  end
end

always @* begin
  if (mon_ack_muldiv) begin
    MON_ACK = mon_ack_muldiv;
    MON_VALID = mon_valid_muldiv;
    MON_DOUT = mon_dout_muldiv;
  end
  else begin
    MON_ACK = mon_ack_gen;
    MON_VALID = mon_valid_gen;
    MON_DOUT = mon_dout_gen;
  end
end

endmodule
