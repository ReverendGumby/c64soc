// Joypad I/O: $40xx

`timescale 1us / 1ns

module s_cpu_ocio_joy
  (
   input             nRES,
   input             CLK,
   input             SLOW_CP1_POSEDGE,
   input             HOLD,

   input [7:0]       MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input [3:0]       MON_WS,
   input [31:0]      MON_DIN,

   input [8:0]       RA,
   input [7:0]       D_I,
   output reg [7:0]  D_O,
   output reg [7:0]  D_OE,
   input             nRD,
   input             nWR,

   output [2:1]      JPCLK,
   output [2:0]      JPOUT,
   input [1:0]       JP1D, // 4016.D0-1
   input [4:0]       JP2D, // 4017.D0-4

   input             VBLANK,

   input             AJPR_EN,
   output            AJPR_BUSY,
   output [15:0]     JOY1,
   output [15:0]     JOY2,
   output [15:0]     JOY3,
   output [15:0]     JOY4
   );


wire [1:0]  joyser0;
wire [4:0]  joyser1;
reg [2:1]   jpclk_man;
wire [2:1]  jpclk_ajpr;
reg [2:0]   jpout_man;
wire        jpout_ajpr;

wire [31:0] mon_dout_ajpr;
wire        mon_ack_ajpr;
wire        mon_valid_ajpr;
wire        mon_write_jpout_man;


//////////////////////////////////////////////////////////////////////
// Registers

always @(posedge CLK) begin
  if (~nRES) begin
    jpout_man <= 3'b0;
  end
  else if (~nWR) begin
    case (RA)
      9'h16: begin
        jpout_man <= D_I[2:0];
      end
      default: ;
    endcase
  end

  if (mon_write_jpout_man)
    jpout_man <= MON_DIN[2:0];
end

assign joyser0 = ~JP1D;
assign joyser1 = ~JP2D;

always @* begin
  jpclk_man = 2'b11;
  D_O = 8'hxx;
  D_OE = 8'b0;
  if (~nRD) begin
    case (RA)
      9'h16: begin
        jpclk_man[1] = 1'b0;
        D_O[1:0] = joyser0;
        D_OE[1:0] = ~0;
      end
      9'h17: begin
        jpclk_man[2] = 1'b0;
        D_O[4:0] = joyser1;
        D_OE[4:0] = ~0;
      end
      default: ;
    endcase
  end
end


//////////////////////////////////////////////////////////////////////
// Automatic Joy Port Read

s_cpu_ajpr ajpr
  (
   .nRES(nRES),
   .CLK(CLK),
   .SLOW_CP1_POSEDGE(SLOW_CP1_POSEDGE),
   .HOLD(HOLD),

   .MON_SEL(MON_SEL),
   .MON_ACK(mon_ack_ajpr),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_ajpr),
   .MON_VALID(mon_valid_ajpr),
   .MON_WS(MON_WS),
   .MON_DIN(MON_DIN),

   .JPCLK(jpclk_ajpr),
   .JPOUT(jpout_ajpr),
   .JP1D(JP1D),
   .JP2D(JP2D[1:0]),

   .VBLANK(VBLANK),

   .AJPR_EN(AJPR_EN),
   .AJPR_BUSY(AJPR_BUSY),
   .JOY1(JOY1),
   .JOY2(JOY2),
   .JOY3(JOY3),
   .JOY4(JOY4)
   );

assign JPCLK = jpclk_man & jpclk_ajpr;
assign JPOUT = {jpout_man[2:1], jpout_man[0] | jpout_ajpr};


//////////////////////////////////////////////////////////////////////
// Debug monitor interface

reg [31:0] mon_dout, mon_dout_joy;
reg        mon_ack_joy;
reg        mon_valid_joy;
reg        mon_sel_jpout_man;
wire       mon_write;

always @* begin
  mon_dout = 32'h0;
  mon_ack_joy = 1'b1;
  mon_sel_jpout_man = 1'b0;

  //``REGION_REGS EMUBUS_CTRL
  case (MON_SEL[7:2])           //``SUBREGS CPU
    6'h04: begin                //``REG JOY0
      mon_sel_jpout_man = 1'b1;
      mon_dout[2:0] = jpout_man; //``FIELD JOYOUT
      mon_dout[4:3] = joyser0;
      mon_dout[9:5] = joyser1;
      mon_dout[12:10] = JPOUT;
      mon_dout[14:13] = JPCLK;
      mon_dout[16:15] = JP1D;
      mon_dout[21:17] = JP2D;
    end
    default: begin
      mon_ack_joy = 1'b0;
    end
  endcase
end

assign mon_write = MON_READY & |MON_WS & ~mon_valid_joy;
assign mon_write_jpout_man = mon_write & mon_sel_jpout_man;

always @(posedge CLK) begin
  mon_valid_joy <= 1'b0;
  if (mon_ack_joy & MON_READY) begin
    mon_valid_joy <= 1'b1;
    mon_dout_joy <= mon_dout;
  end
end

always @* begin
  if (mon_ack_ajpr) begin
    MON_ACK = mon_ack_ajpr;
    MON_VALID = mon_valid_ajpr;
    MON_DOUT = mon_dout_ajpr;
  end
  else begin
    MON_ACK = mon_ack_joy;
    MON_VALID = mon_valid_joy;
    MON_DOUT = mon_dout_joy;
  end
end

endmodule
