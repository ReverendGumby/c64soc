`timescale 1ps / 1ps

module scaler_tb();

wire [23:0] tp_d;
wire [23:0] out_d;
wire [11:0] tp_x;
wire [11:0] tp_y;
wire        tp_clken;
wire        tp_hs, tp_vs, tp_de;
wire        out_hs, out_vs, out_de;

reg         tp_clk;
reg         rst;
reg         out_clk;

localparam [11:0] TP_WIDTH = 283;
localparam [11:0] TP_HEIGHT = 242;

video_timing video_timing
  (
   .CLK(tp_clk),
   .CLKEN(tp_clken),
   .RST(rst),

   .WIDTH(TP_WIDTH),
   .HEIGHT(TP_HEIGHT),
   .LEADIN_Y(12'd1),

   .X(tp_x),
   .Y(tp_y),

   .HS(tp_hs),
   .VS(tp_vs),
   .DE(tp_de)
   );

video_test_pattern video_test_pattern
  (
   .CLK(tp_clk),
   .CLKEN(tp_clken),

   .WIDTH(TP_WIDTH),
   .HEIGHT(TP_HEIGHT),
   .MODE(3'd4),

   .X(tp_x),
   .Y(tp_y),
   .DE(tp_de),

   .R(tp_d[23:16]),
   .G(tp_d[15:8]),
   .B(tp_d[7:0])
   );

localparam [11:0] OUT_WIDTH = 12'd1280;
localparam [11:0] OUT_HEIGHT = 12'd720;
localparam [11:0] OUT_LEFT = 10;
localparam [11:0] OUT_RIGHT = (OUT_WIDTH - 10);
localparam [11:0] OUT_TOP = 10;
localparam [11:0] OUT_BOTTOM = (OUT_HEIGHT - 10);

wire        fb_ARESETn;
wire        fb_ACLK;

wire [31:0] fb_AWADDR;
wire [2:0]  fb_AWSIZE;
wire [3:0]  fb_AWLEN;
wire [1:0]  fb_AWBURST;
wire [3:0]  fb_AWCACHE;
wire [2:0]  fb_AWPROT;
wire        fb_AWVALID;
wire        fb_AWREADY;
wire [31:0] fb_WDATA;
wire [3:0]  fb_WSTRB;
wire        fb_WLAST;
wire        fb_WVALID;
wire        fb_WREADY;
wire        fb_BVALID;
wire        fb_BREADY;

wire [31:0] fb_ARADDR;
wire [3:0]  fb_ARLEN;
wire [2:0]  fb_ARSIZE;
wire [1:0]  fb_ARBURST;
wire [3:0]  fb_ARCACHE;
wire [2:0]  fb_ARPROT;
wire        fb_ARVALID;
wire        fb_ARREADY;
wire [31:0] fb_RDATA;
wire        fb_RVALID;
wire        fb_RREADY;

assign fb_ARESETn = rst;
assign fb_ACLK = out_clk;

axi_slave_bfm axi_slave_bfm
  (
   .ARESETn(fb_ARESETn),
   .ACLK(fb_ACLK),

   .AWID(4'd0),
   .AWADDR(fb_AWADDR),
   .AWLEN({4'b0, fb_AWLEN}),
   .AWSIZE(fb_AWSIZE),
   .AWBURST(fb_AWBURST),
   .AWVALID(fb_AWVALID),
   .AWREADY(fb_AWREADY),
   .WDATA(fb_WDATA),
   .WSTRB(fb_WSTRB),
   .WLAST(fb_WLAST),
   .WVALID(fb_WVALID),
   .WREADY(fb_WREADY),
   .BID(),
   .BVALID(fb_BVALID),
   .BREADY(fb_BREADY),

   .ARID(4'd0),
   .ARADDR(fb_ARADDR),
   .ARLEN({4'b0, fb_ARLEN}),
   .ARSIZE(fb_ARSIZE),
   .ARBURST(fb_ARBURST),
   .ARVALID(fb_ARVALID),
   .ARREADY(fb_ARREADY),
   .RID(),
   .RDATA(fb_RDATA),
   .RLAST(),
   .RVALID(fb_RVALID),
   .RREADY(fb_RREADY)
   );

wire error;

video_scaler_axi video_scaler
  (
   .RST(rst),

   .IN_PCLK(tp_clk),
   .IN_PCE(tp_clken),
   .IN_HS(tp_hs),
   .IN_VS(tp_vs),
   .IN_DE(tp_de),
   .IN_D(tp_d),

   .IN_WIDTH(TP_WIDTH),
   .IN_HEIGHT(TP_HEIGHT),

   .IN_HS_NEG(1'b1),
   .IN_VS_NEG(1'b1),

   .OUT_PCLK(out_clk),
   .OUT_HS(out_hs),
   .OUT_VS(out_vs),
   .OUT_DE(out_de),
   .OUT_D(out_d),

   .OUT_WIDTH(OUT_WIDTH),
   .OUT_HEIGHT(OUT_HEIGHT),

   .OUT_LEFT(OUT_LEFT),
   .OUT_RIGHT(OUT_RIGHT),
   .OUT_TOP(OUT_TOP),
   .OUT_BOTTOM(OUT_BOTTOM),

   .FB_BASEADDR(32'h3000_0000),

   .FB_AWADDR(fb_AWADDR),
   .FB_AWSIZE(fb_AWSIZE),
   .FB_AWLEN(fb_AWLEN),
   .FB_AWBURST(fb_AWBURST),
   .FB_AWCACHE(fb_AWCACHE),
   .FB_AWPROT(fb_AWPROT),
   .FB_AWVALID(fb_AWVALID),
   .FB_AWREADY(fb_AWREADY),
   .FB_WDATA(fb_WDATA),
   .FB_WSTRB(fb_WSTRB),
   .FB_WLAST(fb_WLAST),
   .FB_WVALID(fb_WVALID),
   .FB_WREADY(fb_WREADY),
   .FB_BRESP(2'b00),
   .FB_BVALID(fb_BVALID),
   .FB_BREADY(fb_BREADY),

   .FB_ARADDR(fb_ARADDR),
   .FB_ARLEN(fb_ARLEN),
   .FB_ARSIZE(fb_ARSIZE),
   .FB_ARBURST(fb_ARBURST),
   .FB_ARCACHE(fb_ARCACHE),
   .FB_ARPROT(fb_ARPROT),
   .FB_ARVALID(fb_ARVALID),
   .FB_ARREADY(fb_ARREADY),
   .FB_RDATA(fb_RDATA),
   .FB_RRESP(2'b00),
   .FB_RVALID(fb_RVALID),
   .FB_RREADY(fb_RREADY),

   .ERROR(error)
   );

initial begin
  $dumpfile("nes_tb.vcd");
  $dumpvars;
  #17_000_000_000 $finish;
end

initial begin
  tp_clk = 1'b1;
  out_clk = 1'b1;
  rst = 1'b0;
  #1_000_000 rst = 1'b1;
end

always #23280.4 begin :tp_clkgen  // 236250/11 KHz
  tp_clk = !tp_clk;
end

integer tp_clk_cnt = 0;
always @(posedge tp_clk)
  tp_clk_cnt <= (tp_clk_cnt + 1) % 4;
assign tp_clken = tp_clk_cnt == 0;

always #6711 begin :out_clkgen
  out_clk = !out_clk;
end

endmodule

// Local Variables:
// compile-command: "iverilog -grelative-include -s scaler_tb -o scaler_tb.vvp -f scaler_tb.files scaler_tb.v && ./scaler_tb.vvp"
// End:
