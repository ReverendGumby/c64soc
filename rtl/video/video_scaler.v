// A trivial video upscaler. 

module video_scaler
  (
   input             RST,

   // Input baseband video. The slowest video timing.
   input             IN_PCLK,
   input             IN_HS,
   input             IN_VS,
   input             IN_DE,
   input [23:0]      IN_D,

   input [11:0]      IN_WIDTH,
   input [11:0]      IN_HEIGHT,

   input             IN_HS_NEG, // set if IN_HS is negative polarity
   input             IN_VS_NEG, // same for IN_VS

   // Output (scaled) baseband video. Faster, bigger, stronger.
   input             OUT_PCLK,
   output reg        OUT_HS,
   output reg        OUT_VS,
   output reg        OUT_DE,
   output reg [23:0] OUT_D,

   input [11:0]      OUT_WIDTH,
   input [11:0]      OUT_HEIGHT,

   input [11:0]      OUT_LEFT,  // edges of input window
   input [11:0]      OUT_RIGHT,
   input [11:0]      OUT_TOP,
   input [11:0]      OUT_BOTTOM,

   input [2:0]       OUT_HSCALE, // horizontal scale factor - 1
   input [2:0]       OUT_VSCALE  // vertical scale factor - 1
   );

// Input video storage
`define STORE_WIDTH     530
`define STORE_HEIGHT    270
reg [23:0] store [0:`STORE_HEIGHT-1] [0:`STORE_WIDTH-1];

`define STORE_X         10 //$bits(`STORE_WIDTH)
`define STORE_Y         9  //$bits(`STORE_HEIGHT)

//////////////////////////////////////////////////////////////////////
// Write input video to storage.

reg [`STORE_X-1:0] in_x;
reg [`STORE_Y-1:0] in_y;

assign in_hs_pos = IN_HS ^ IN_HS_NEG;
assign in_vs_pos = IN_VS ^ IN_VS_NEG;

always @(posedge IN_PCLK) begin
  if (~RST) begin
    in_x <= 0;
    in_y <= 0;
  end
  else begin
    if (!IN_DE) begin
      if (in_x) begin
        in_x <= 0;
        in_y <= in_y + 1;
      end
      else if (in_vs_pos) begin
        in_y <= 0;
      end
    end
    else begin
      store[in_y][in_x] <= IN_D;
      in_x <= in_x + 1;
    end
  end
end

//////////////////////////////////////////////////////////////////////
// Generate output video timing.

wire [11:0] out_x;
wire [11:0] out_y;
wire        out_hsi, out_vsi, out_dei;

video_timing out_timing
  (
   .CLK(OUT_PCLK),
   .RST(RST),

   .WIDTH(OUT_WIDTH),
   .HEIGHT(OUT_HEIGHT),
   .LEADIN_Y(12'd1),

   .X(out_x),
   .Y(out_y),
  
   .HS(out_hsi),
   .VS(out_vsi),
   .DE(out_dei)
   );

//////////////////////////////////////////////////////////////////////
// Generate output video.

reg [`STORE_X-1:0] out_store_x;
reg [`STORE_Y-1:0] out_store_y;
reg                out_vwin;   // out_y is within input window
reg                out_hwin;   // out_x is within input window
reg                out_window; // (out_x,out_y) is within input window
reg [2:0]          out_hcnt, out_vcnt;

// Determine whether we're within the input window.
always @(posedge OUT_PCLK) begin
  if (~RST) begin
    out_hwin <= 1'b0;
    out_vwin <= 1'b0;
  end
  else begin
    if (out_x == OUT_LEFT - 1)
      out_hwin <= 1'b1;
    else if (out_x == OUT_RIGHT)
      out_hwin <= 1'b0;

    if (out_y == OUT_TOP)
      out_vwin <= 1'b1;
    else if (out_y == OUT_BOTTOM + 1)
      out_vwin <= 1'b0;
  end
end

always @* out_window <= out_vwin && out_hwin;

// Given the output coordinates, compute the coordinates of the input
// pixel to display.
always @(posedge OUT_PCLK) begin
  if (~RST) begin
    out_store_x <= 0;
    out_store_y <= 0;
    out_hcnt <= 0;
    out_vcnt <= 0;
  end
  else begin
    if (!out_dei) begin
      if (out_vwin && out_store_x) begin
        out_hcnt <= 0;
        out_store_x <= 0;
        if (out_vcnt == OUT_VSCALE) begin
          out_vcnt <= 0;
          out_store_y <= out_store_y + 1;
        end
        else
          out_vcnt <= out_vcnt + 1;
      end
      else if (out_x == 0) begin // vertical blanking is happening
        out_vcnt <= 0;
        out_store_y <= 0;
      end
    end
    else if (out_window) begin
      if (out_hcnt == OUT_HSCALE) begin
        out_hcnt <= 0;
        out_store_x <= out_store_x + 1;
      end
      else
        out_hcnt <= out_hcnt + 1;
    end
  end
end

// Output dem pixels.
always @(posedge OUT_PCLK) begin
  if (~RST) begin
    OUT_D <= 0;
    OUT_HS <= 0;
    OUT_VS <= 0;
    OUT_DE <= 0;
  end
  else begin
    if (out_dei)
      OUT_D <= store[out_store_y][out_store_x];
    else
      OUT_D <= 0;

    OUT_HS <= out_hsi;
    OUT_VS <= out_vsi;
    OUT_DE <= out_dei;
  end
end

endmodule
