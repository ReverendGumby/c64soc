module video_timing
  (
   input             CLK,
   input             CLKEN,
   input             RST,

   input [11:0]      WIDTH,
   input [11:0]      HEIGHT,
   input [11:0]      LEADIN_Y,

   output reg [11:0] X,
   output reg [11:0] Y,
   
   output reg        HS,
   output reg        VS,
   output reg        DE,

   output reg [11:0] HBLANK_DCNT,
   output reg [11:0] HBLANK_UCNT,
   output reg [11:0] VBLANK_DCNT,
   output reg [11:0] VBLANK_UCNT
   );

// Horizontal timing
//
//       | front  | sync   | back   | visible
//       | porch  |        | porch  |
// CLK   |        |        |        |
//     __|        |        |        |________
// DE    \________|________|________/
//     ___________|        |________|________
// HS             \________/        |
//                                  |
// X                                01234567

// Vertical timing
//
//       | front  | sync   | back   | visible
//       | porch  |        | porch  |
// CLK   |        |        |        |
//       |        |        |        |
// DE  XX\________|________|________/XXXXXXXX
//     ___________|        |________|________
// VS             \________/        |
//                                  |
// Y                                |01234567

reg [11:0] xi, yi;
reg        hsi, vsi;
reg        hactive;
reg        vactive;

// Duration of front porch, sync pulse, and back porch. h_* are in
// CLK, v_* are in horizontal lines. All must be non-zero.
reg [11:0] h_front, h_sync, h_back;
reg [11:0] v_front, v_sync, v_back;

// X and Y values at start of front porch, etc., and maximum counter
// values.
reg [11:0] x_visible, x_front, x_sync, x_back, x_max;
reg [11:0] y_visible, y_front, y_sync, y_back, y_max;

// HS and VS polarity
reg hspol, vspol;
`define SYNC_POSITIVE   0
`define SYNC_NEGATIVE   1

integer i;
always @* begin
  if (WIDTH == 640 && HEIGHT == 480) begin
    h_front = 16;
    h_sync = 96;
    h_back = 48;
    v_front = 10;
    v_sync = 2;
    v_back = 33;
    hspol = `SYNC_NEGATIVE;
    vspol = `SYNC_NEGATIVE;
  end
  else if (WIDTH == 1280 && HEIGHT == 720) begin
    h_front = 72;
    h_sync = 80;
    h_back = 216;
    v_front = 3;
    v_sync = 5;
    v_back = 22;
    hspol = `SYNC_POSITIVE;
    vspol = `SYNC_POSITIVE;
  end
  else if (WIDTH == 418 && HEIGHT == 235) begin
    h_front = 1;
    h_sync = 102;
    h_back = 1;
    v_front = 1;
    v_sync = 28;
    v_back = 1;
    hspol = `SYNC_NEGATIVE;
    vspol = `SYNC_NEGATIVE;
  end
  else if (WIDTH == 283 && HEIGHT == 242) begin
    h_front = 9;
    h_sync = 25;
    h_back = 24;
    v_front = 5;
    v_sync = 2;
    v_back = 15;
    hspol = `SYNC_NEGATIVE;
    vspol = `SYNC_NEGATIVE;
  end
  else begin
    h_front = X;
    h_sync = X;
    h_back = X;
    v_front = X;
    v_sync = X;
    v_back = X;
    hspol = 1'bx;
    vspol = 1'bx;
  end
end

always @* begin
  x_visible = 0;
  x_front = x_visible + WIDTH;
  x_sync = x_front + h_front;
  x_back = x_sync + h_sync;
  x_max = x_back + h_back - 12'd1;

  y_visible = 0;
  y_front = y_visible + HEIGHT;
  y_sync = y_front + v_front;
  y_back = y_sync + v_sync;
  y_max = y_back + v_back - 12'd1;
end

always @(posedge CLK) begin
  if (~RST) begin
    xi <= x_front;
    yi <= y_max - LEADIN_Y;
    vsi <= 1'b0;
    hsi <= 1'b0;
    hactive <= 1'b0;
    vactive <= 1'b0;
  end
  else if (CLKEN) begin
    if (xi == x_max)
      xi <= 10'd0;
    else
      xi <= xi + 10'd1;
    if (xi == x_sync) begin
      if (yi == y_max)
        yi <= 10'd0;
      else
        yi <= yi + 10'd1;
    end
  end

  if (xi == x_front)
    hactive <= 1'b0;
  else if (xi == x_visible)
    hactive <= 1'b1;

  if (xi == x_sync)
    hsi <= 1'b1;
  else if (xi == x_back)
    hsi <= 1'b0;

  if (yi == y_front)
    vactive <= 1'b0;
  else if (yi == y_visible)
    vactive <= 1'b1;

  if (yi == y_sync)
    vsi <= 1'b1;
  else if (yi == y_back)
    vsi <= 1'b0;

  X <= xi;
  Y <= yi;
end

always @* begin
  DE = hactive && vactive;
  HS = hsi ^ hspol;
  VS = vsi ^ vspol;

  HBLANK_DCNT = hactive ? 0 : x_max - X + 1'd1;
  HBLANK_UCNT = hactive ? 0 : X - x_front + 1'd1;

  VBLANK_DCNT = vactive ? 0 : y_max - Y + 1'd1;
  VBLANK_UCNT = vactive ? 0 : Y - y_front + 1'd1;
end

endmodule
