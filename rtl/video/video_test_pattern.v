module video_test_pattern
  (
   input        CLK,
   input        CLKEN,

   input [11:0] WIDTH,
   input [11:0] HEIGHT,
   input [2:0]  MODE,

   input [11:0] X,
   input [11:0] Y,
   input        DE,

   output [7:0] R,
   output [7:0] G,
   output [7:0] B
   );

reg [23:0] rgb;
reg [23:0] clr;
reg        hvl, dot, chk;

wire [11:0] x = X;
wire [11:0] y = Y;

// MODE pattern
// ---- -------
// 0    solid black
// 1    solid red
// 2    solid green
// 3    solid blue
// 4    color bars
// 5    horizontal + vertical lines
// 6    dots
// 7    checkerboard

`define RGB3_TO_RGB24(R, G, B) { {8{R}}, {8{G}}, {8{B}} }

wire xc, yc;

assign xc = x[5:0] == 6'd0 || x == WIDTH - 12'd1;
assign yc = y[5:0] == 6'd0 || y == HEIGHT - 12'd1;

always @(posedge CLK) if (CLKEN) begin
  // color bars w/ gradient
  clr[23:16] <= x[8] ? ~{ x[5:0], x[1:0] } : 8'h00;
  clr[15: 8] <= x[7] ? ~{ x[5:0], x[1:0] } : 8'h00;
  clr[ 7: 0] <= x[6] ? ~{ x[5:0], x[1:0] } : 8'h00;

  hvl <= xc || yc;                         // horiz + vert lines
  dot <= xc && yc;                         // dots
  chk <= x[6] ^ y[6];                      // checkerboard
end

always @* begin
  if (DE)
    case (MODE)
      3'd0: rgb <= `RGB3_TO_RGB24(1'b0, 1'b0, 1'b0);
      3'd1: rgb <= `RGB3_TO_RGB24(1'b1, 1'b0, 1'b0);
      3'd2: rgb <= `RGB3_TO_RGB24(1'b0, 1'b1, 1'b0);
      3'd3: rgb <= `RGB3_TO_RGB24(1'b0, 1'b0, 1'b1);
      3'd4: rgb <= clr;
      3'd5: rgb <= `RGB3_TO_RGB24(hvl,  hvl,  hvl);
      3'd6: rgb <= `RGB3_TO_RGB24(dot,  dot,  dot);
      3'd7: rgb <= `RGB3_TO_RGB24(chk,  chk,  chk);
    endcase
  else
    rgb <= 24'h000000;
end

assign R = rgb[23:16];
assign G = rgb[15:8];
assign B = rgb[7:0];

endmodule
