module linestore_output #(parameter BPP=24,
                          parameter XSIZE=12,
                          parameter YSIZE=12)
  (
   input                CLK,
   input                RST,

   input [XSIZE-1:0]    X,
   input [YSIZE-1:0]    Y,
   input [XSIZE-1:0]    HBLANK_DCNT,
   input [YSIZE-1:0]    VBLANK_DCNT,
   input                HS,
   input                VS,
   input                DE,

   input [XSIZE-1:0]    FB_X,

   output [XSIZE-1:0]   LS_X,
   input [BPP-1:0]      LS_D,

   output reg [XSIZE-1:0] OUT_HBLANK_DCNT,
   output reg [YSIZE-1:0] OUT_VBLANK_DCNT,
   output reg           OUT_HS,
   output reg           OUT_VS,
   output reg           OUT_DE,
   output [BPP-1:0] OUT_D
   );

assign LS_X = FB_X;

assign OUT_D = DE ? LS_D : {BPP{1'b0}};

// Delay from LS_X -> LS_D is 1*CLK. Delay the rest here.
always @(posedge CLK) begin
  if (!RST) begin
    OUT_HS <= 0;
    OUT_VS <= 0;
    OUT_DE <= 0;
    OUT_HBLANK_DCNT <= 0;
    OUT_VBLANK_DCNT <= 0;
  end
  else begin
    OUT_HS <= HS;
    OUT_VS <= VS;
    OUT_DE <= DE;
    OUT_HBLANK_DCNT <= HBLANK_DCNT;
    OUT_VBLANK_DCNT <= VBLANK_DCNT;
  end
end

endmodule
