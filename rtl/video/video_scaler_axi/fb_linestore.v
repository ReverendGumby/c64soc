module fb_linestore #(parameter BPP=24,
                      parameter XSIZE=12,
                      parameter YSIZE=12)
  (
   input              OUT_PCLK,
   input              RST,

   input [XSIZE-1:0]  FB_WIDTH,
   input [31:0]       FB_BASEADDR,
   
   input [YSIZE-1:0]  FB_Y,
   input              VWIN,

   output [31:0]      FB_ARADDR,
   output [3:0]       FB_ARLEN,
   output [2:0]       FB_ARSIZE,
   output [1:0]       FB_ARBURST,
   output [3:0]       FB_ARCACHE,
   output [2:0]       FB_ARPROT,
   output             FB_ARVALID,
   input              FB_ARREADY,
   input [31:0]       FB_RDATA,
   input [1:0]        FB_RRESP,
   input              FB_RVALID,
   output             FB_RREADY,

   output             LS_ENABLE,
   output [XSIZE-1:0] LS_X,
   output [BPP-1:0]   LS_D,
   output             ERROR
   );

wire [31:0] fbaddr;
wire        fetch; 

// Determine which line should be in the linestore.

linestore_addr #(XSIZE,YSIZE) linestore_addr
  (
   .CLK(OUT_PCLK),
   .RST(RST),

   .FB_WIDTH(FB_WIDTH),
   .FB_BASEADDR(FB_BASEADDR),

   .FB_Y(FB_Y),
   .VWIN(VWIN),

   .FBADDR(fbaddr),
   .FETCH(fetch)
   );

// Transfer a line from the framebuffer to the linestore.

fb_reader #(BPP,XSIZE) fb_reader
  (
   .CLK(OUT_PCLK),
   .RST(RST),

   .FB_WIDTH(FB_WIDTH),

   .FBADDR(fbaddr),
   .FETCH(fetch),

   .LS_ENABLE(LS_ENABLE),
   .LS_X(LS_X),
   .LS_D(LS_D),

   .FB_ARADDR(FB_ARADDR),
   .FB_ARLEN(FB_ARLEN),
   .FB_ARSIZE(FB_ARSIZE),
   .FB_ARBURST(FB_ARBURST),
   .FB_ARCACHE(FB_ARCACHE),
   .FB_ARPROT(FB_ARPROT),
   .FB_ARVALID(FB_ARVALID),
   .FB_ARREADY(FB_ARREADY),
   .FB_RDATA(FB_RDATA),
   .FB_RRESP(FB_RRESP),
   .FB_RVALID(FB_RVALID),
   .FB_RREADY(FB_RREADY),

   .ERROR(ERROR)
   );

endmodule
