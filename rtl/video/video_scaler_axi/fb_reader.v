module fb_reader #(parameter BPP=24,
                   parameter XSIZE=12)
  (
   input                  CLK,
   input                  RST,

   input [XSIZE-1:0]      FB_WIDTH,

   input [31:0]           FBADDR,
   input                  FETCH,

   output reg             LS_ENABLE,
   output reg [XSIZE-1:0] LS_X,
   output reg [BPP-1:0]   LS_D,

   output reg [31:0]      FB_ARADDR,
   output [3:0]           FB_ARLEN,
   output [2:0]           FB_ARSIZE,
   output [1:0]           FB_ARBURST,
   output [3:0]           FB_ARCACHE,
   output [2:0]           FB_ARPROT,
   output reg             FB_ARVALID,
   input                  FB_ARREADY,
   input [31:0]           FB_RDATA,
   input [1:0]            FB_RRESP,
   input                  FB_RVALID,
   output reg             FB_RREADY,

   output reg             ERROR
   );

localparam LEN = 8'd8;

reg [XSIZE-1:0] fb_x;
reg             fb_done;
reg [7:0]       rcount;
reg             next_burst;

always @(posedge CLK) begin
  if (!RST) begin
    FB_ARADDR <= 0;
    FB_ARVALID <= 1'b0;
  end
  else begin
    if (FB_ARVALID && FB_ARREADY) begin
      FB_ARVALID <= 1'b0;
    end
    else if (next_burst) begin
      FB_ARADDR <= FB_ARADDR + LEN * 8'd4;
      FB_ARVALID <= 1'b1;
    end
    else if (FETCH) begin
      FB_ARADDR <= FBADDR;
      FB_ARVALID <= 1'b1;
    end
  end
end

always @(posedge CLK) begin
  if (!RST) begin
    FB_RREADY <= 1'b0;
    fb_x <= 0;
    rcount <= 0;
    next_burst <= 1'b0;
  end
  else begin
    if (FETCH) begin
      fb_x <= 0;
    end
    else if (FB_ARVALID && FB_ARREADY) begin
      FB_RREADY <= 1'b1;
      rcount <= LEN - 1'd1;
    end
    else if (FB_RVALID && FB_RREADY) begin
      fb_x <= fb_x + 1'd1;
      if (rcount) begin
        rcount <= rcount - 1'd1;
      end
      else begin
        FB_RREADY <= 1'b0;
        next_burst <= !fb_done;
      end
    end
    else begin
      next_burst <= 1'b0;
    end
  end
end

always @(posedge CLK) begin
  if (!RST) begin
    fb_done <= 1'b0;
  end
  else begin
    if (FETCH) begin
      fb_done <= 1'b0;
    end
    else if (!fb_done && fb_x == FB_WIDTH) begin
      fb_done <= 1'b1;
    end
  end
end

always @(posedge CLK) begin
  if (!RST) begin
    LS_ENABLE <= 0;
  end
  else begin
    if (FB_RVALID && FB_RREADY) begin
      LS_ENABLE <= !fb_done;
      LS_X <= fb_x;
      LS_D <= FB_RDATA[BPP-1:0];
    end
    else begin
      LS_ENABLE <= 1'b0;
    end
  end
end

always @(posedge CLK) begin
  if (!RST)
    ERROR <= 1'b0;
  else if (FETCH)
    if (FB_ARVALID || FB_RVALID)
      ERROR <= 1'b1;
    else
      ERROR <= 1'b0;
end

assign FB_ARLEN = LEN - 1'd1;
assign FB_ARSIZE = 3'b010;      // 4 bytes
assign FB_ARBURST = 2'b01;      // INCR
assign FB_ARCACHE = 4'b0011;    // normal non-cacheable bufferable
assign FB_ARPROT = 3'b000;      // unprivileged secure data

endmodule
