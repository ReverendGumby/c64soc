module fb_output #(parameter BPP=24,
                   parameter XSIZE=12,
                   parameter YSIZE=12)
  (
   input             OUT_PCLK,
   input             RST,

   input [XSIZE-1:0] IN_WIDTH,
   input [YSIZE-1:0] IN_HEIGHT,
   input [XSIZE-1:0] OUT_WIDTH,
   input [YSIZE-1:0] OUT_HEIGHT,
   input [XSIZE-1:0] OUT_LEFT, // edges of input window
   input [XSIZE-1:0] OUT_RIGHT,
   input [YSIZE-1:0] OUT_TOP,
   input [YSIZE-1:0] OUT_BOTTOM,
   input [31:0]      FB_BASEADDR,

   output [31:0]     FB_ARADDR,
   output [3:0]      FB_ARLEN,
   output [2:0]      FB_ARSIZE,
   output [1:0]      FB_ARBURST,
   output [3:0]      FB_ARCACHE,
   output [2:0]      FB_ARPROT,
   output            FB_ARVALID,
   input             FB_ARREADY,
   input [31:0]      FB_RDATA,
   input [1:0]       FB_RRESP,
   input             FB_RVALID,
   output            FB_RREADY,

   output [XSIZE-1:0] OUT_HBLANK_DCNT,
   output [YSIZE-1:0] OUT_VBLANK_DCNT,
   output            OUT_HS,
   output            OUT_VS,
   output            OUT_DE,
   output [BPP-1:0]  OUT_D,
   
   output            ERROR
   );

wire [XSIZE-1:0] x;
wire [XSIZE-1:0] hblank_dcnt;
wire [YSIZE-1:0] y;
wire [YSIZE-1:0] vblank_dcnt;
wire             hs, vs, de;
wire [XSIZE-1:0] fb_x;
wire [YSIZE-1:0] fb_y;
wire             vwin;
wire             ls_in_enable;
wire [XSIZE-1:0] ls_in_x, ls_out_x;
wire [BPP-1:0]   ls_in_d, ls_out_d;

// Generate output video timing.

wire [YSIZE-1:0] leadin_y = 5'd20; // arbitrary

video_timing out_timing
  (
   .CLK(OUT_PCLK),
   .CLKEN(1'b1),
   .RST(RST),

   .WIDTH(OUT_WIDTH),
   .HEIGHT(OUT_HEIGHT),
   .LEADIN_Y(leadin_y),

   .X(x),
   .Y(y),
  
   .HS(hs),
   .VS(vs),
   .DE(de),

   .HBLANK_DCNT(hblank_dcnt),
   .VBLANK_DCNT(vblank_dcnt)
   );

// Translate coordinates from output video to input framebuffer.

coord_out_fb #(XSIZE, YSIZE) coord_out_fb
  (
   .CLK(OUT_PCLK),
   .RST(RST),

   .LEFT(OUT_LEFT),
   .RIGHT(OUT_RIGHT),
   .TOP(OUT_TOP),
   .BOTTOM(OUT_BOTTOM),

   .FB_WIDTH(IN_WIDTH),
   .FB_HEIGHT(IN_HEIGHT),

   .X(x),
   .Y(y),
   .DE(de),

   .FB_X(fb_x),
   .FB_Y(fb_y),

   .HWIN(),
   .VWIN(vwin),
   .WINDOW()
   );

// Transfer lines from the framebuffer to the linestore.

fb_linestore #(BPP, XSIZE, YSIZE) fb_linestore
  (
   .OUT_PCLK(OUT_PCLK),
   .RST(RST),

   .FB_WIDTH(IN_WIDTH),
   .FB_BASEADDR(FB_BASEADDR),
   
   .FB_Y(fb_y),
   .VWIN(vwin),

   .FB_ARADDR(FB_ARADDR),
   .FB_ARLEN(FB_ARLEN),
   .FB_ARSIZE(FB_ARSIZE),
   .FB_ARBURST(FB_ARBURST),
   .FB_ARCACHE(FB_ARCACHE),
   .FB_ARPROT(FB_ARPROT),
   .FB_ARVALID(FB_ARVALID),
   .FB_ARREADY(FB_ARREADY),
   .FB_RDATA(FB_RDATA),
   .FB_RRESP(FB_RRESP),
   .FB_RVALID(FB_RVALID),
   .FB_RREADY(FB_RREADY),

   .LS_ENABLE(ls_in_enable),
   .LS_X(ls_in_x),
   .LS_D(ls_in_d),
   .ERROR(ERROR)
   );

// Dual-port memory for one horizontal line.

linestore #(BPP, XSIZE) linestore
  (
   .CLK(OUT_PCLK),

   .IN_ENABLE(ls_in_enable),
   .IN_X(ls_in_x),
   .IN_D(ls_in_d),

   .OUT_X(ls_out_x),
   .OUT_D(ls_out_d)
   );

// Drive output video from the linestore.

linestore_output #(BPP, XSIZE, YSIZE) linestore_output
  (
   .CLK(OUT_PCLK),
   .RST(RST),

   .X(x),
   .Y(y),
   .HBLANK_DCNT(hblank_dcnt),
   .VBLANK_DCNT(vblank_dcnt),
   .HS(hs),
   .VS(vs),
   .DE(de),

   .FB_X(fb_x),
  
   .LS_X(ls_out_x),
   .LS_D(ls_out_d),

   .OUT_HBLANK_DCNT(OUT_HBLANK_DCNT),
   .OUT_VBLANK_DCNT(OUT_VBLANK_DCNT),
   .OUT_HS(OUT_HS),
   .OUT_VS(OUT_VS),
   .OUT_DE(OUT_DE),
   .OUT_D(OUT_D)
   );

endmodule
