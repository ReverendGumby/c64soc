// A trivial video upscaler with AXI framebuffer interface.

module video_scaler_axi #(parameter BPP=24,
                          parameter XSIZE=12,
                          parameter YSIZE=12)
  (
   input             RST,

   // Input baseband video. The slowest video timing.
   input             IN_PCLK,
   input             IN_PCE,
   input             IN_HS,
   input             IN_VS,
   input             IN_DE,
   input [BPP-1:0]   IN_D,

   input [XSIZE-1:0] IN_WIDTH,
   input [YSIZE-1:0] IN_HEIGHT,

   input             IN_HS_NEG, // set if IN_HS is negative polarity
   input             IN_VS_NEG, // same for IN_VS

   // Output (scaled) baseband video. Faster, bigger, stronger.
   input             OUT_PCLK,
   output [XSIZE-1:0] OUT_HBLANK_DCNT,
   output [YSIZE-1:0] OUT_VBLANK_DCNT,
   output            OUT_HS,
   output            OUT_VS,
   output            OUT_DE,
   output [BPP-1:0]  OUT_D,

   input [XSIZE-1:0] OUT_WIDTH,
   input [YSIZE-1:0] OUT_HEIGHT,

   input [XSIZE-1:0] OUT_LEFT, // edges of input window
   input [XSIZE-1:0] OUT_RIGHT,
   input [YSIZE-1:0] OUT_TOP,
   input [YSIZE-1:0] OUT_BOTTOM,

   // Framebuffer configuration
   input [31:0]      FB_BASEADDR,

   // AXI master write channel: input video -> framebuffer
   output [31:0]     FB_AWADDR,
   output [2:0]      FB_AWSIZE,
   output [3:0]      FB_AWLEN,
   output [1:0]      FB_AWBURST,
   output [3:0]      FB_AWCACHE,
   output [2:0]      FB_AWPROT,
   output            FB_AWVALID,
   input             FB_AWREADY,
   output [31:0]     FB_WDATA,
   output [3:0]      FB_WSTRB,
   output            FB_WLAST,
   output            FB_WVALID,
   input             FB_WREADY,
   input [1:0]       FB_BRESP,
   input             FB_BVALID,
   output            FB_BREADY,

   // AXI master read channel: framebuffer -> output video
   output [31:0]     FB_ARADDR,
   output [3:0]      FB_ARLEN,
   output [2:0]      FB_ARSIZE,
   output [1:0]      FB_ARBURST,
   output [3:0]      FB_ARCACHE,
   output [2:0]      FB_ARPROT,
   output            FB_ARVALID,
   input             FB_ARREADY,
   input [31:0]      FB_RDATA,
   input [1:0]       FB_RRESP,
   input             FB_RVALID,
   output            FB_RREADY,

   // Error flag: if set, something bad happened.
   output            ERROR
   );

wire in_error, out_error;

assign ERROR = in_error | out_error;

//////////////////////////////////////////////////////////////////////
// Write input video to framebuffer.

wire in_hs_pos = IN_HS ^ IN_HS_NEG;
wire in_vs_pos = IN_VS ^ IN_VS_NEG;

input_fb #(BPP) input_fb
  (
   .RST(RST),
   .IN_PCLK(IN_PCLK),
   .IN_PCE(IN_PCE),
   .OUT_PCLK(OUT_PCLK),

   .FB_BASEADDR(FB_BASEADDR),

   .IN_HS(in_hs_pos),
   .IN_VS(in_vs_pos),
   .IN_DE(IN_DE),
   .IN_D(IN_D),

   .FB_AWADDR(FB_AWADDR),
   .FB_AWSIZE(FB_AWSIZE),
   .FB_AWLEN(FB_AWLEN),
   .FB_AWBURST(FB_AWBURST),
   .FB_AWCACHE(FB_AWCACHE),
   .FB_AWPROT(FB_AWPROT),
   .FB_AWVALID(FB_AWVALID),
   .FB_AWREADY(FB_AWREADY),
   .FB_WDATA(FB_WDATA),
   .FB_WSTRB(FB_WSTRB),
   .FB_WLAST(FB_WLAST),
   .FB_WVALID(FB_WVALID),
   .FB_WREADY(FB_WREADY),
   .FB_BRESP(FB_BRESP),
   .FB_BVALID(FB_BVALID),
   .FB_BREADY(FB_BREADY),

   .ERROR(in_error)
   );

//////////////////////////////////////////////////////////////////////
// Generate output video.

fb_output #(BPP,XSIZE,YSIZE) fb_output
  (
   .OUT_PCLK(OUT_PCLK),
   .RST(RST),

   .IN_WIDTH(IN_WIDTH),
   .IN_HEIGHT(IN_HEIGHT),
   .OUT_WIDTH(OUT_WIDTH),
   .OUT_HEIGHT(OUT_HEIGHT),
   .OUT_LEFT(OUT_LEFT),
   .OUT_RIGHT(OUT_RIGHT),
   .OUT_TOP(OUT_TOP),
   .OUT_BOTTOM(OUT_BOTTOM),
   .FB_BASEADDR(FB_BASEADDR),

   .FB_ARADDR(FB_ARADDR),
   .FB_ARLEN(FB_ARLEN),
   .FB_ARSIZE(FB_ARSIZE),
   .FB_ARBURST(FB_ARBURST),
   .FB_ARCACHE(FB_ARCACHE),
   .FB_ARPROT(FB_ARPROT),
   .FB_ARVALID(FB_ARVALID),
   .FB_ARREADY(FB_ARREADY),
   .FB_RDATA(FB_RDATA),
   .FB_RRESP(FB_RRESP),
   .FB_RVALID(FB_RVALID),
   .FB_RREADY(FB_RREADY),

   .OUT_HBLANK_DCNT(OUT_HBLANK_DCNT),
   .OUT_VBLANK_DCNT(OUT_VBLANK_DCNT),
   .OUT_HS(OUT_HS),
   .OUT_VS(OUT_VS),
   .OUT_DE(OUT_DE),
   .OUT_D(OUT_D),

   .ERROR(out_error)
   );

endmodule
