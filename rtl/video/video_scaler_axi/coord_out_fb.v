// Given the output coordinates, compute the coordinates of the
// framebuffer pixel to display.

module coord_out_fb #(parameter XSIZE=12,
                      parameter YSIZE=12)
  (
   input                  CLK,
   input                  RST,

   input [XSIZE-1:0]      LEFT, // edges of input window
   input [XSIZE-1:0]      RIGHT,
   input [YSIZE-1:0]      TOP,
   input [YSIZE-1:0]      BOTTOM,

   input [XSIZE-1:0]      FB_WIDTH, // width of framebuffer
   input [YSIZE-1:0]      FB_HEIGHT, // height of framebuffer

   input [XSIZE-1:0]      X,
   input [YSIZE-1:0]      Y,
   input                  DE,

   output reg [XSIZE-1:0] FB_X,
   output reg [YSIZE-1:0] FB_Y,

   output reg             HWIN, // X is within input window
   output reg             VWIN, // Y is within input window
   output                 WINDOW // (X,Y) is within input window
   );

reg [XSIZE-1:0] hacc, hacc_next;
reg [YSIZE-1:0] vacc, vacc_next;
reg             last_x_hwin, last_y_vwin;

wire [XSIZE-1:0] win_width, hacc_inc;
wire [YSIZE-1:0] win_height, vacc_inc;
wire             hacc_wrap, vacc_wrap;

// Determine whether we're within the input window.
always @(posedge CLK) begin
  if (!RST) begin
    HWIN <= 1'b0;
    VWIN <= 1'b0;
    last_x_hwin <= 1'b0;
  end
  else begin
    last_x_hwin <= X == RIGHT - 1;
    last_y_vwin <= Y == BOTTOM;

    if (X == LEFT - 1)
      HWIN <= 1'b1;
    else if (last_x_hwin)
      HWIN <= 1'b0;

    if (Y == TOP)
      VWIN <= 1'b1;
    else if (Y == BOTTOM + 1)
      VWIN <= 1'b0;
  end
end

assign WINDOW = VWIN && HWIN;

// Ideal scaling math:
//   FB_X = ((X - LEFT) / WIN_WIDTH) * FB_WIDTH
// where WIN_WIDTH = RIGHT - LEFT + 1
//
// To avoid division, multiply both sides by WIN_WIDTH:
//   FB_X * WIN_WIDTH = (X - LEFT) * FB_WIDTH
//
// Assume (1) X increments by 1 every clock; (2) WIN_WIDTH is larger
// than FB_WIDTH. It follows that FB_X increases by 0<x<1 every
// clock. Scaling up all variables by WIN_WIDTH, we can track
// increments of FB_X with an accumulator (hacc). Add FB_WIDTH to hacc
// every clock (for X = X + 1). When hacc >= WIN_WIDTH, then add 1 to
// FB_X and subtract WIN_WIDTH from hacc.

// Pixels before HWIN: FB_X = 0
// Pixels after HWIN: FB_X = last FB_X inside HWIN
// Lines above VWIN: FB_X = 0, FB_Y = 0
// Lines below VWIN: FB_X = 0, FB_Y = last FB_Y inside VWIN

assign win_width = RIGHT - LEFT + 1;
assign win_height = BOTTOM - TOP + 1;

assign hacc_inc = FB_WIDTH;
assign vacc_inc = FB_HEIGHT;

assign hacc_wrap = hacc >= win_width;
assign vacc_wrap = vacc >= win_height;

always @* begin
  hacc_next = hacc + hacc_inc;
  if (hacc_wrap)
    hacc_next = hacc_next - win_width;

  vacc_next = vacc + vacc_inc;
  if (vacc_wrap)
    vacc_next = vacc_next - win_height;
end

always @(posedge CLK) begin
  if (!RST) begin
    FB_X <= 0;
    FB_Y <= 0;
    hacc <= hacc_inc;
    vacc <= vacc_inc;
  end
  else begin
    if (!DE) begin
      if (VWIN && FB_X) begin   // horizontal blanking
        hacc <= hacc_inc;
        FB_X <= 0;
        if (!last_y_vwin) begin
          if (vacc_wrap)
            FB_Y <= FB_Y + 1'd1;
          vacc <= vacc_next;
        end
      end
      else if (X == 0) begin    // vertical blanking
        vacc <= vacc_inc;
        FB_Y <= 0;
      end
    end
    else if (WINDOW && !last_x_hwin) begin
      if (hacc_wrap)
        FB_X <= FB_X + 1'd1;
      hacc <= hacc_next;
    end
  end
end

endmodule
