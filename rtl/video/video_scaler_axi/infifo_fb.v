module infifo_fb #(parameter BPP=24,
                   parameter INFIFO_DSIZE=BPP)
  (
   input                    CLK,
   input                    RST,

   input                    IN_REMPTY,
   input [INFIFO_DSIZE-1:0] IN_RDATA,
   output reg               IN_RINC,

   output [31:0]            FB_AWADDR,
   output [2:0]             FB_AWSIZE,
   output [3:0]             FB_AWLEN,
   output [1:0]             FB_AWBURST,
   output [3:0]             FB_AWCACHE,
   output [2:0]             FB_AWPROT,
   output reg               FB_AWVALID,
   input                    FB_AWREADY,
   output [31:0]            FB_WDATA,
   output [3:0]             FB_WSTRB,
   output                   FB_WLAST,
   output reg               FB_WVALID,
   input                    FB_WREADY,
   input [1:0]              FB_BRESP,
   input                    FB_BVALID,
   output                   FB_BREADY
   );

//////////////////////////////////////////////////////////////////////
// Transfer framebuffer input FIFO to framebuffer memory via AXI.

reg [31:0] in_fbaddr;
reg [BPP-1:0] in_fd;

always @* { in_fbaddr, in_fd } <= IN_RDATA;

assign FB_AWADDR = in_fbaddr;
assign FB_AWSIZE = 3'b010;      // 4 bytes
assign FB_AWLEN = 8'd0;         // 1 burst
assign FB_AWBURST = 2'b01;      // INCR
assign FB_AWCACHE = 4'b0011;    // normal non-cacheable bufferable
assign FB_AWPROT = 3'b000;      // unprivileged secure data

assign FB_WDATA[31:BPP] = 0;
assign FB_WDATA[BPP-1:0] = in_fd;
assign FB_WSTRB = 4'b1111;
assign FB_WLAST = 1'b1;

assign FB_BREADY = 1'b1;

reg fb_awdone, fb_wdone;

always @(posedge CLK) begin
  if (!RST) begin
    FB_AWVALID <= 1'b0;
    FB_WVALID <= 1'b0;
    fb_awdone <= 1'b0;
    fb_wdone <= 1'b0;
    IN_RINC <= 1'b0;
  end
  else begin
    if (fb_awdone && fb_wdone) begin
      IN_RINC <= 1'b1;
      fb_awdone <= 1'b0;
      fb_wdone <= 1'b0;
    end
    else if (IN_RINC) begin
      IN_RINC <= 1'b0;
      // Wait another cycle for IN_REMPTY to update
    end
    else begin
      if (!fb_awdone) begin
        if (!FB_AWVALID && !IN_REMPTY)
          FB_AWVALID <= 1'b1;
        else if (FB_AWVALID && FB_AWREADY) begin
          fb_awdone <= 1'b1;
          FB_AWVALID <= 1'b0;
        end
      end
      if (!fb_wdone) begin
        if (!FB_WVALID && !IN_REMPTY)
          FB_WVALID <= 1'b1;
        else if (FB_WVALID && FB_WREADY) begin
          fb_wdone <= 1'b1;
          FB_WVALID <= 1'b0;
        end
      end
    end
  end
end

endmodule
