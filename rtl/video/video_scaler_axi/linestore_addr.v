module linestore_addr #(parameter XSIZE=12,
                        parameter YSIZE=12)
  (
   input             CLK,
   input             RST,

   input [XSIZE-1:0] FB_WIDTH,
   input [31:0]      FB_BASEADDR,

   input [YSIZE-1:0] FB_Y,
   input             VWIN,

   output reg [31:0] FBADDR,
   output reg        FETCH
   );

// It's assumed that y increments by 1 and resets to 0

wire [31:0] fb_linesize = { FB_WIDTH, 2'b00 }; // width * 4

reg [YSIZE-1:0] y;
reg             y_changed;

always @(posedge CLK) begin
  if (!RST) begin
    y <= {YSIZE{1'b1}};
    y_changed <= 1'b0;
    FBADDR <= 0;
    FETCH <= 1'b0;
  end
  else begin
    if (FB_Y != y) begin
      if (FB_Y)
        FBADDR <= FBADDR + fb_linesize;
      else
        FBADDR <= FB_BASEADDR;
      y <= FB_Y;
      y_changed <= 1'b1;
    end

    // Postpone y_changed -> fetch until VWIN
    if (y_changed) begin
      if (VWIN) begin
        FETCH <= y_changed;
        y_changed <= 1'b0;
      end
    end
    else if (FETCH)
      FETCH <= 1'b0;
  end
end

endmodule
