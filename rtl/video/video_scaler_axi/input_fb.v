module input_fb #(parameter BPP=24)
  (
   input           RST,
   input           IN_PCLK,
   input           IN_PCE,
   input           OUT_PCLK,
   
   input [31:0]    FB_BASEADDR,
   
   input           IN_HS,
   input           IN_VS,
   input           IN_DE,
   input [BPP-1:0] IN_D,

   output [31:0]   FB_AWADDR,
   output [2:0]    FB_AWSIZE,
   output [3:0]    FB_AWLEN,
   output [1:0]    FB_AWBURST,
   output [3:0]    FB_AWCACHE,
   output [2:0]    FB_AWPROT,
   output          FB_AWVALID,
   input           FB_AWREADY,
   output [31:0]   FB_WDATA,
   output [3:0]    FB_WSTRB,
   output          FB_WLAST,
   output          FB_WVALID,
   input           FB_WREADY,
   input [1:0]     FB_BRESP,
   input           FB_BVALID,
   output          FB_BREADY,

   output          ERROR
   );

localparam FBADDR_SIZE = 31-0+1/*$bits(FB_AWADDR)*/;
localparam INFIFO_DSIZE = FBADDR_SIZE + BPP;
localparam INFIFO_ASIZE = 2;

wire [INFIFO_DSIZE-1:0] in_wdata;
wire                    in_winc, in_wfull;

wire [INFIFO_DSIZE-1:0] in_rdata;
wire                    in_rinc, in_rempty;

input_infifo #(BPP, INFIFO_DSIZE) input_infifo
  (
   .RST(RST),
   .IN_PCLK(IN_PCLK),
   .IN_PCE(IN_PCE),

   .FB_BASEADDR(FB_BASEADDR),

   .IN_HS(IN_HS),
   .IN_VS(IN_VS),
   .IN_DE(IN_DE),
   .IN_D(IN_D),

   .IN_WFULL(in_wfull),
   .IN_WDATA(in_wdata),
   .IN_WINC(in_winc),

   .ERROR(ERROR)
   );

fifo1 #(INFIFO_DSIZE, INFIFO_ASIZE) infifo
  (
   .WRST_N(RST),
   .WCLK(IN_PCLK),
   .WINC(in_winc),
   .WFULL(in_wfull),
   .WDATA(in_wdata),

   .RRST_N(RST),
   .RCLK(OUT_PCLK),
   .RINC(in_rinc),
   .REMPTY(in_rempty),
   .RDATA(in_rdata)
   );

infifo_fb #(BPP, INFIFO_DSIZE) infifo_fb
  (
   .CLK(OUT_PCLK),
   .RST(RST),

   .IN_REMPTY(in_rempty),
   .IN_RDATA(in_rdata),
   .IN_RINC(in_rinc),

   .FB_AWADDR(FB_AWADDR),
   .FB_AWSIZE(FB_AWSIZE),
   .FB_AWLEN(FB_AWLEN),
   .FB_AWBURST(FB_AWBURST),
   .FB_AWCACHE(FB_AWCACHE),
   .FB_AWPROT(FB_AWPROT),
   .FB_AWVALID(FB_AWVALID),
   .FB_AWREADY(FB_AWREADY),
   .FB_WDATA(FB_WDATA),
   .FB_WSTRB(FB_WSTRB),
   .FB_WLAST(FB_WLAST),
   .FB_WVALID(FB_WVALID),
   .FB_WREADY(FB_WREADY),
   .FB_BRESP(FB_BRESP),
   .FB_BVALID(FB_BVALID),
   .FB_BREADY(FB_BREADY)
   );

endmodule
