module input_infifo #(parameter BPP=24,
                      parameter INFIFO_DSIZE=BPP
                      )
  (
   input                     RST,
   input                     IN_PCLK,
   input                     IN_PCE,
   
   input [31:0]              FB_BASEADDR,

   input                     IN_HS,
   input                     IN_VS,
   input                     IN_DE,
   input [BPP-1:0]           IN_D,

   input                     IN_WFULL,
   output [INFIFO_DSIZE-1:0] IN_WDATA,
   output                    IN_WINC,

   output reg                ERROR
   );

//////////////////////////////////////////////////////////////////////
// Write input video to framebuffer input FIFO.

reg [31:0] in_addr;

// Generate x,y to accompany data
always @(posedge IN_PCLK) begin
  if (!RST) begin
    in_addr <= FB_BASEADDR;
  end
  else if (IN_PCE) begin
    if (!IN_DE) begin
      if (IN_VS) begin
        in_addr <= FB_BASEADDR;
      end
    end
    else begin
      in_addr <= in_addr + 32'd4;
    end
  end
end

assign IN_WDATA = { in_addr, IN_D };
assign IN_WINC = IN_DE && IN_PCE;

always @(posedge IN_PCLK) begin
  if (!RST)
    ERROR <= 1'b0;
  else if (IN_WFULL)
    ERROR <= 1'b1;
  else if (IN_PCE && !IN_DE && IN_VS)
    ERROR <= 1'b0;
end

endmodule
