module linestore #(parameter BPP=24,
                   parameter XSIZE=12)
  (
   input                CLK,

   input                IN_ENABLE,
   input [XSIZE-1:0]    IN_X,
   input [BPP-1:0]      IN_D,

   input [XSIZE-1:0]    OUT_X,
   output reg [BPP-1:0] OUT_D
   );

localparam STORE_SIZE = 512;

reg [BPP-1:0] store [0:STORE_SIZE-1];

always @(posedge CLK) begin
  if (IN_ENABLE)
    store[IN_X] <= IN_D;
  OUT_D <= store[OUT_X];
end

endmodule
