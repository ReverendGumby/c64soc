// Execute all opcodes.

`timescale 1us / 1ns

module smp_opcodes_tb();

reg        nRES;
reg        clk;
reg        cken_d;
reg [8:0]  ins;

wire [15:0] A;
wire [7:0]  di, do_smp_sim;
wire        cken;
wire        cken_sim;
wire        RW;
wire        nCE, nWE, nOE;
wire        sclken;

initial begin
  $timeformat(-6, 0, " us", 1);

  $dumpfile("smp_opcodes_tb.vcd");
`ifndef VERILATOR
  $dumpvars();
`endif
end

s_smp_sim smp_sim
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken_sim),
   .A(A),
   .DI(di),
   .DO(do_smp_sim),
   .RW(RW)
   );

s_dsp_sim dsp_sim
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken),
   .A(),
   .DI(),
   .DO(),
   .RW(),
   .MA(),
   .MDI(),
   .MDO(),
   .nCE(),
   .nWE(),
   .nOE(),
   .SCLKEN(),
   .PSOUTL(),
   .PSOUTR()
   );

initial begin
  clk = 1;
  nRES = 1;
  ins = 9'h00;
end

initial forever begin :clkgen
  #(500.0/6144.0) clk = ~clk;
end

always @(posedge clk)
  cken_d <= cken;
assign cken_sim = cken & ~cken_d;

wire sync = smp_sim.sync;

//initial #9 nRES = 1;
initial begin
  smp_sim.pc = 16'h1000;
  smp_sim.ab = smp_sim.pc;

  smp_sim.print_ins_enable = 1;

  while (ins < 9'h101) begin
    while (sync !== 1'b1)
      @(negedge cken) ;
    @(negedge cken) ;
    ins = ins + 1;
    if (ins == 9'hEF /*SLEEP*/ || ins == 9'hFF /*STOP*/)
      ins = ins + 1;
  end

  $finish;
end

assign di = ins[7:0];

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -DS_DSP_SIM_NOAUDIO -s smp_opcodes_tb -f s_dsp_sim.files -o smp_opcodes_tb.vvp ../sp_timer.v s_smp_sim.sv ram.sv smp_opcodes_tb.sv && ./smp_opcodes_tb.vvp"
// End:
