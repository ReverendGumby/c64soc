`timescale 1us / 1ns

module s_dsp_sim_echo_mixer
  (
   input signed [15:0]      V0,
   input signed [15:0]      V1,
   input signed [15:0]      V2,
   input signed [15:0]      V3,
   input signed [15:0]      V4,
   input signed [15:0]      V5,
   input signed [15:0]      V6,
   input signed [15:0]      V7,
   input [7:0]              EON,
   input [7:0]              EFB,
   input signed [15:0]      EFBIN,
   output reg signed [15:0] OUT
   );

function signed [15:0] clamp(input signed [16:0] in);
  if (in > 17'sh7fff)
    in = 17'sh7fff;
  else if (in < -17'sh8000)
    in = -17'sh8000;
  clamp = 16'(in);
endfunction

reg signed [15:0] out;

task run_mix;
  out = 0;
  if (EON[0]) out = V0;
  if (EON[1]) out = clamp(out + V1);
  if (EON[2]) out = clamp(out + V2);
  if (EON[3]) out = clamp(out + V3);
  if (EON[4]) out = clamp(out + V4);
  if (EON[5]) out = clamp(out + V5);
  if (EON[6]) out = clamp(out + V6);
  if (EON[7]) out = clamp(out + V7);
  out = clamp(out + 16'((24'(EFBIN) * signed'(EFB)) >>> 7));
  out[0] = 1'b0;
endtask

initial out = 0;

task out_sample;
  run_mix();
  OUT = out[15:0];
endtask

endmodule
