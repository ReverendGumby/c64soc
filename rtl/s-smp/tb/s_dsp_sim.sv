`timescale 1us / 1ns

module s_dsp_sim
  (
   input         nRES, // reset
   input         CLK, // 6.144MHz clock
   output        CKEN, // CPU clock enable (1.024MHz)
   input [15:0]  A, // address bus (from S-SMP)
   input [7:0]   DI, // data bus (from S-SMP)
   output [7:0]  DO, // data bus (to S-SMP)
   input         RW, // R/W (from S-SMP)
   output [15:0] MA, // SRAM address bus
   input [7:0]   MDI, // SRAM data bus (from RAM)
   output [7:0]  MDO, // SRAM data bus (to RAM)
   output        nCE, // SRAM chip enable
   output        nWE, // SRAM write enable
   output        nOE, // SRAM output enable
   output        SCLKEN, // sample clock enable
   output [15:0] PSOUTL, // parallel sample out (L)
   output [15:0] PSOUTR // parallel sample out (R)
   );

reg [7:0] spdb;

wire      sp_sel;
wire      sclken;                    // sample rate clock enable
wire      mad_ssmp;                  // S-SMP owns the SRAM A/D bus
wire      mad_sdsp;                  // S-DSP owns the SRAM A/D bus

// iclken = 3.072MHz = CLK / 2

wire      iclken;
reg       idivcnt;

initial begin
  idivcnt = 0;
end

always @(posedge CLK) begin
  idivcnt <= idivcnt + 1'd1;
end

assign iclken = &idivcnt;

// S-SMP clock enable

reg [1:0] cccnt;
wire      ccken, sglen;
reg       ccken_d;

initial
  cccnt = 2'd2;

always @(posedge CLK) if (iclken) begin
  if (cccnt == 2'd2)
    cccnt <= 0;
  else
    cccnt <= cccnt + 1'd1;
end

always @(posedge CLK)
  ccken_d <= ccken;

assign ccken = cccnt == 2'd0;
assign sglen = cccnt == 2'd0;
assign mad_ssmp = ccken;
assign mad_sdsp = ~mad_ssmp;
assign CKEN = ccken;

// S-SMP address / data bus

reg [7:0] d_o_d;
reg [7:0] d_o;

wire [7:0] d_i = DI;
wire       d_oe = RW;

always @(posedge CLK) if (ccken_d)
  d_o_d <= d_o;           // Hold data for S-SMP while we're accessing RAM

always @* begin
  d_o = d_o_d;

  if (ccken_d) begin
    if (sp_sel)
      d_o = spdb;
    else
      d_o = MDI;
  end
end

assign DO = d_oe ? d_o : 8'hzz;

// S-DSP Access Registers @ $F2-$F3

assign sp_sel = A[15:4] == 'h00f && A[3:1] == 'b001;
wire sp_dspaddr = sp_sel & ~A[0]; // $F2
wire sp_dspdata = sp_sel &  A[0]; // $F3

reg [6:0] spda;
reg [7:0] spdd;
reg       spddrs, spddws, spdro;

wire [7:0] spddo;

initial begin
  spda = 0;
  spdro = 0;
end

always @(posedge CLK) if (iclken) begin
  if (~nRES) begin
  end
  else if (ccken & !RW) begin
    if (sp_dspaddr) begin
      spda <= d_i[6:0];
      spdro <= d_i[7];
    end
  end
end

always @* begin
  spdb = 8'h00;
  if (sp_dspaddr)
    spdb = {1'b0, spda};
  else if (sp_dspdata)
    spdb = spddo;
end

always @* begin
  spdd = 8'hxx;
  spddrs = 0;
  spddws = 0;
  if (ccken_d & sp_dspdata) begin
    spddrs = RW;
    spddws = ~spdro & ~RW;
    spdd = d_i;
  end
end

// DSP Register File

s_dsp_sim_reg_file rf
  (
   .CLK(CLK), .nRES(nRES), .CLKEN(ccken),
   .A(spda), .RS(spddrs), .WS(spddws), .RDI(spdd), .RDO(spddo)
   );

// SRAM address / data and control

s_dsp_sim_sram_if sram
  (
   .CLK(CLK),
   .CLKEN(iclken),
   .MAD_SSMP(mad_ssmp),
   .MAD_SDSP(mad_sdsp),
   .SP_SEL(sp_sel),
   .A(A),
   .DI(DI),
   .RW(RW),
   .MA(MA),
   .MDI(MDI),
   .MDO(MDO),
   .nCE(nCE),
   .nWE(nWE),
   .nOE(nOE)
   );

`ifndef S_DSP_SIM_NOAUDIO

// Envelope event counter

reg [14:0] env_ev_cnt;          // envelope event counter

s_dsp_sim_env_ev_ctr ev_ctr (.CLK(CLK), .nRES(nRES), .SCLKEN(sclken), .CNT(env_ev_cnt));

// Sample generation loop

reg [5:0] sglcnt;
reg [7:0] mvoll, mvolr, evoll, evolr, kon, kof, flg, endx;
reg [7:0] efb, pmon, non, eon, dir, esa, edl;
reg [7:0] fir0, fir1, fir2, fir3, fir4, fir5, fir6, fir7;
reg signed [15:0] v0outl, v1outl, v2outl, v3outl, v4outl, v5outl, v6outl, v7outl;
reg signed [15:0] v0outr, v1outr, v2outr, v3outr, v4outr, v5outr, v6outr, v7outr;
reg               ecen, mute;

initial sglcnt = 0;
always @(posedge CLK) if (iclken & sglen) begin // runs every 3 CLK
  case (sglcnt[4:0])
    5'd0:  begin v0.step5(); v1.step2(); end
    5'd1:  begin v0.step6(); v1.step3(); end
    5'd2:  begin v0.step7(); v1.step4(); v3.step1(); end
    5'd3:  begin v0.step8(); v1.step5(); v2.step2(); end
    5'd4:  begin v0.step9(); v1.step6(); v2.step3(); end
    5'd5:  begin v1.step7(); v2.step4(); v4.step1(); end
    5'd6:  begin v1.step8(); v2.step5(); v3.step2(); end
    5'd7:  begin v1.step9(); v2.step6(); v3.step3(); end
    5'd8:  begin v2.step7(); v3.step4(); v5.step1(); end
    5'd9:  begin v2.step8(); v3.step5(); v4.step2(); end
    5'd10: begin v2.step9(); v3.step6(); v4.step3(); end
    5'd11: begin v3.step7(); v4.step4(); v6.step1(); end
    5'd12: begin v3.step8(); v4.step5(); v5.step2(); end
    5'd13: begin v3.step9(); v4.step6(); v5.step3(); end
    5'd14: begin v4.step7(); v5.step4(); v7.step1(); end
    5'd15: begin v4.step8(); v5.step5(); v6.step2(); end
    5'd16: begin v4.step9(); v5.step6(); v6.step3(); end
    5'd17: begin v5.step7(); v6.step4(); v0.step1(); end
    5'd18: begin v5.step8(); v6.step5(); v7.step2(); end
    5'd19: begin v5.step9(); v6.step6(); v7.step3(); end
    5'd20: begin v6.step7(); v7.step4(); v1.step1(); end
    5'd21: begin v6.step8(); v7.step5(); v0.step2(); end
    5'd22: begin v6.step9(); v7.step6(); v0.step3a(); end
    5'd23: begin v7.step7(); end
    5'd24: begin v7.step8(); end
    5'd25: begin v0.step3b(); v7.step9(); end
    5'd30: begin v0.step3c(); end
    5'd31: begin v0.step4(); v2.step1(); end
    default: ;
  endcase

  case (sglcnt[4:0])
    5'd22: begin
      el.load_buf();
      fir0 = rf.file['h0F];
    end
    5'd23: begin
      er.load_buf();
      fir1 = rf.file['h1F];
      fir2 = rf.file['h2F];
    end
    5'd24: begin
      fir3 = rf.file['h3F];
      fir4 = rf.file['h4F];
      fir5 = rf.file['h5F];
    end
    5'd25: begin
      fir6 = rf.file['h6F];
      fir7 = rf.file['h7F];
    end
    5'd26: begin
      mvoll = rf.file['h0C];
      evoll = rf.file['h2C];
      mute = rf.file['h6C][6];
      el.out_sample();
      mixl.out_sample();
      efb = rf.file['h0D];
    end
    5'd27: begin
      mvolr = rf.file['h1C];
      evolr = rf.file['h3C];
      er.out_sample();
      mixr.out_sample();
      pmon = rf.file['h2D];
    end
    5'd28: begin
      non = rf.file['h3D];
      eon = rf.file['h4D];
      dir = rf.file['h5D];
      ecen = rf.file['h6C][5];
    end
    5'd29: begin
      el.store_buf();
      edl = rf.file['h7D];
      esa = rf.file['h6D];
      ecen = rf.file['h6C][5];
      if (sglcnt[5]) begin
        kon = 0;
      end
    end
    5'd30: begin
      er.store_buf();
      el.inc_offset();
      er.inc_offset();
      flg[4:0] = rf.file['h6C][4:0];
      // TODO: update noise
      if (sglcnt[5]) begin
        rf.get_kon_if_written(kon);
        kof = rf.file['h5C];
      end
    end
    default: ;
  endcase

  sglcnt <= sglcnt + 1'd1;
end

task preload_rf;
  mvoll = rf.file['h0C];
  evoll = rf.file['h2C];
  efb = rf.file['h0D];
  mvolr = rf.file['h1C];
  evolr = rf.file['h3C];
  kon = rf.file['h4c];
  kof = rf.file['h5C];
  {mute, ecen, flg[4:0]} = rf.file['h6C][6:0];
  fir0 = rf.file['h0F];
  fir1 = rf.file['h1F];
  fir2 = rf.file['h2F];
  fir3 = rf.file['h3F];
  fir4 = rf.file['h4F];
  fir5 = rf.file['h5F];
  fir6 = rf.file['h6F];
  fir7 = rf.file['h7F];
  pmon = rf.file['h2D];
  non = rf.file['h3D];
  eon = rf.file['h4D];
  dir = rf.file['h5D];
  esa = rf.file['h6D];
  edl = rf.file['h7D];

  v0.preload_rf();
  v1.preload_rf();
  v2.preload_rf();
  v3.preload_rf();
  v4.preload_rf();
  v5.preload_rf();
  v6.preload_rf();
  v7.preload_rf();
endtask

// 32kHz sample clock

assign sclken = iclken & sglen & (sglcnt[4:0] == 5'd29); // global counter update time
assign SCLKEN = sclken;

// DSP Voices

s_dsp_sim_voice v0
  (
   .nRES(nRES), .ENV_EV_CNT(env_ev_cnt),
   .REG_BASE(8'h00),
   .KON(kon[0]), .KOF(kof[0]),
   .DIR(dir),
   .OUTL(v0outl), .OUTR(v0outr)
   );

s_dsp_sim_voice v1
  (
   .nRES(nRES), .ENV_EV_CNT(env_ev_cnt),
   .REG_BASE(8'h10),
   .KON(kon[1]), .KOF(kof[1]),
   .DIR(dir),
   .OUTL(v1outl), .OUTR(v1outr)
   );

s_dsp_sim_voice v2
  (
   .nRES(nRES), .ENV_EV_CNT(env_ev_cnt),
   .REG_BASE(8'h20),
   .KON(kon[2]), .KOF(kof[2]),
   .DIR(dir),
   .OUTL(v2outl), .OUTR(v2outr)
   );

s_dsp_sim_voice v3
  (
   .nRES(nRES), .ENV_EV_CNT(env_ev_cnt),
   .REG_BASE(8'h30),
   .KON(kon[3]), .KOF(kof[3]),
   .DIR(dir),
   .OUTL(v3outl), .OUTR(v3outr)
   );

s_dsp_sim_voice v4
  (
   .nRES(nRES), .ENV_EV_CNT(env_ev_cnt),
   .REG_BASE(8'h40),
   .KON(kon[4]), .KOF(kof[4]),
   .DIR(dir),
   .OUTL(v4outl), .OUTR(v4outr)
   );

s_dsp_sim_voice v5
  (
   .nRES(nRES), .ENV_EV_CNT(env_ev_cnt),
   .REG_BASE(8'h50),
   .KON(kon[5]), .KOF(kof[5]),
   .DIR(dir),
   .OUTL(v5outl), .OUTR(v5outr)
   );

s_dsp_sim_voice v6
  (
   .nRES(nRES), .ENV_EV_CNT(env_ev_cnt),
   .REG_BASE(8'h60),
   .KON(kon[6]), .KOF(kof[6]),
   .DIR(dir),
   .OUTL(v6outl), .OUTR(v6outr)
   );

s_dsp_sim_voice v7
  (
   .nRES(nRES), .ENV_EV_CNT(env_ev_cnt),
   .REG_BASE(8'h70),
   .KON(kon[7]), .KOF(kof[7]),
   .DIR(dir),
   .OUTL(v7outl), .OUTR(v7outr)
   );

// Echo unit

wire signed [15:0] eoutl, eoutr;

s_dsp_sim_echo_unit #(.SIDE(1'b0)) el
  (
   .V0(v0outl), .V1(v1outl), .V2(v2outl), .V3(v3outl),
   .V4(v4outl), .V5(v5outl), .V6(v6outl), .V7(v7outl),
   .ECEN(ecen), .EON(eon), .EVOL(evoll), .EFB(efb), .ESA(esa), .EDL(edl),
   .FIR0(fir0), .FIR1(fir1), .FIR2(fir2), .FIR3(fir3),
   .FIR4(fir4), .FIR5(fir5), .FIR6(fir6), .FIR7(fir7),
   .OUT(eoutl)
   );

s_dsp_sim_echo_unit #(.SIDE(1'b1))  er
  (
   .V0(v0outr), .V1(v1outr), .V2(v2outr), .V3(v3outr),
   .V4(v4outr), .V5(v5outr), .V6(v6outr), .V7(v7outr),
   .ECEN(ecen), .EON(eon), .EVOL(evolr), .EFB(efb), .ESA(esa), .EDL(edl),
   .FIR0(fir0), .FIR1(fir1), .FIR2(fir2), .FIR3(fir3),
   .FIR4(fir4), .FIR5(fir5), .FIR6(fir6), .FIR7(fir7),
   .OUT(eoutr)
   );

// Final mixer

s_dsp_sim_out_mixer mixl
  (
   .V0(v0outl), .V1(v1outl), .V2(v2outl), .V3(v3outl),
   .V4(v4outl), .V5(v5outl), .V6(v6outl), .V7(v7outl),
   .MVOL(mvoll), .EOUT(eoutl), .MUTE(mute),
   .OUT(PSOUTL)
   );

s_dsp_sim_out_mixer mixr
  (
   .V0(v0outr), .V1(v1outr), .V2(v2outr), .V3(v3outr),
   .V4(v4outr), .V5(v5outr), .V6(v6outr), .V7(v7outr),
   .MVOL(mvolr), .EOUT(eoutr), .MUTE(mute),
   .OUT(PSOUTR)
   );

`else

task preload_rf;
endtask

`endif

endmodule
