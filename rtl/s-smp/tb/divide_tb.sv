`timescale 1us / 1ns

module ram
  (
   input        CLK,
   input        nCE,
   input        nWE,
   input        nOE,
   input [15:0] A,
   input [7:0]  DI,
   output [7:0] DO
   );

reg [7:0] ram [0:16'hffff];

initial begin
reg [15:0] a;
  a = 16'h0000;
  ram[a++] = 'h00; // NOP
  ram[a++] = 'he4; // MOV A, $80
  ram[a++] = 'h80;
  ram[a++] = 'h8d; // MOV Y, $00
  ram[a++] = 'h00;
  ram[a++] = 'h9e; // DIV YA, X
  ram[a++] = 'hab; // INC $80
  ram[a++] = 'h80;
  ram[a++] = 'hD0; // BNE $0001
  ram[a++] = 'hF7;
  ram[a++] = 'hFF; // STOP
  ram['h0080] = 'h00;
end

assign DO = ~(nCE | nOE) ? ram[A] : 8'hxx;

always @(negedge CLK) begin
  if (~(nCE | nWE)) begin
    //$display("ram[%x] <= %x", A, D);
    ram[A] <= DI;
  end
end

endmodule

module divide_tb();

reg        nRES;
reg        clk;

wire [15:0] A;
wire [7:0]  di_ram, do_ram;
wire        RW;
wire        nCE, nWE, nOE;

initial begin
  $dumpfile("divide_tb.vcd");
  $dumpvars;
end

s_smp_sim smp_sim
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(1'b1),
   .A(A),
   .DI(do_ram),
   .DO(di_ram),
   .RW(RW)
   );

initial begin
  clk = 1;
  nRES = 1;
end

initial forever begin :clkgen
  #(500.0/2048.0) clk = ~clk;
end

initial begin
  repeat (150) #(1e6) ; $finish;
end

//initial #9 nRES = 1;
initial begin
  smp_sim.pc = 'h0000;
  smp_sim.ac = 'h00;
  smp_sim.x = 'h0c;
  smp_sim.y = 'h00;
  smp_sim.p = 'h80;
  smp_sim.s = 'hff;
  smp_sim.ab = smp_sim.pc;
end

ram ram(.CLK(clk), .A(A[15:0]), .DI(di_ram), .DO(do_ram), .nCE(1'b0), 
        .nWE(RW), .nOE(~RW));

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s divide_tb -o divide_tb.vvp s_smp_sim.sv divide_tb.sv && ./divide_tb.vvp"
// End:
