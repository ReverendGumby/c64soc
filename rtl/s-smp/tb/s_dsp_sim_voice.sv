`timescale 1us / 1ns

module s_dsp_sim_voice
  (
   input                    nRES,
   input [14:0]             ENV_EV_CNT,
   input [7:0]              REG_BASE,

   input                    KON,
   input                    KOF,
   input [7:0]              DIR,

   output reg signed [15:0] OUTL,
   output reg signed [15:0] OUTR
   );

// Envelope

reg [7:0] adsr1, adsr2, gain;
reg [6:0] envx;
reg       flg_reset;

wire [10:0] envelope;
wire        play, bend;

initial flg_reset = 0;

s_dsp_sim_env env (.nRES(nRES), .ENV_EV_CNT(ENV_EV_CNT),
                   .KON(KON), .KOF(KOF | flg_reset), 
                   .PLAY(play), .BEND(bend),
                   .ADSR1(adsr1), .ADSR2(adsr2), .GAIN(gain),
                   .OUT(envelope));

assign envx = envelope[10:4];

// BRR sample player

reg [7:0] scrn;

wire signed [14:0] brr_sout0, brr_sout1, brr_sout2, brr_sout3;
wire [2:0]         next;

initial scrn = 0;

s_dsp_sim_brr brr
  (
   .nRES(nRES),
   .KON(KON), .DIR(DIR), .SCRN(scrn), .VOICE(REG_BASE[6:4]),
   .NEXT(next), .PLAY(play), .BEND(bend),
   .OUT0(brr_sout0), .OUT1(brr_sout1), .OUT2(brr_sout2), .OUT3(brr_sout3)
   );

// Gaussian interpolator

reg [13:0]    p;

wire signed [14:0] interp_sout;

initial p = 0;

s_dsp_sim_interp interp
  (
   .nRES(nRES),
   .P(p), .PLAY(play), .NEXT(next),
   .IN0(brr_sout0), .IN1(brr_sout1), .IN2(brr_sout2), .IN3(brr_sout3),
   .OUT(interp_sout)
   );

// Final envelope scale and pan

reg [7:0] voll, volr;
reg signed [14:0] out;

initial begin
  voll = 0;
  volr = 0;
  out = 0;
end

task apply_env;
  out = 15'(signed'(26'(interp_sout) * envelope) >>> 11);
endtask

// Voice-related steps of the sample generation loop
task step1;
  scrn = rf.file[REG_BASE + 'h04];
endtask

task step2;
  brr.load_ptr();
  p[7:0] = rf.file[REG_BASE + 'h02];
  adsr1 = rf.file[REG_BASE + 'h05];
endtask

task step3a;
  p[13:8] = rf.file[REG_BASE + 'h03][5:0];
endtask

task step3b;
  brr.load_header_s1();
endtask

task step3c;
  apply_env();
  flg_reset = rf.file['h6C][7];
  if (adsr1[7])
    adsr2 = rf.file[REG_BASE + 'h06];
  else
    gain = rf.file[REG_BASE + 'h07];
  env.update();
endtask

task step3;
  step3a();
  step3b();
  step3c();
endtask

task step4;
  voll = rf.file[REG_BASE + 'h00];
  OUTL = 16'((23'(out) * signed'(voll)) >>> 6);
  brr.load_s2();
endtask

task step5;
  volr = rf.file[REG_BASE + 'h01];
  OUTR = 16'((23'(out) * signed'(volr)) >>> 6);
endtask

task step6;
  rf.file[REG_BASE + 'h09] = out[14:7]; // OUTX
endtask

task step7;
  rf.file[REG_BASE + 'h08] = {1'b0, envx};
endtask

task step8;
  interp.go(); // here because step was empty
endtask

task step9;
endtask

task preload_rf;
  voll = rf.file[REG_BASE + 'h00];
  volr = rf.file[REG_BASE + 'h01];
  p[7:0] = rf.file[REG_BASE + 'h02];
  p[13:8] = rf.file[REG_BASE + 'h03][5:0];
  scrn = rf.file[REG_BASE + 'h04];
  adsr1 = rf.file[REG_BASE + 'h05];
  adsr2 = rf.file[REG_BASE + 'h06];
  gain = rf.file[REG_BASE + 'h07];
  flg_reset = rf.file['h6C][7];
endtask

endmodule
