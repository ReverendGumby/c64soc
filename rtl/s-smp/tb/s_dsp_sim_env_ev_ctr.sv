`timescale 1us / 1ns

module s_dsp_sim_env_ev_ctr
  (
   input             nRES,
   input             CLK,
   input             SCLKEN,
   output reg [14:0] CNT
   );


initial begin
  CNT = 0;
end

// Envelope event counter

always @(posedge CLK) begin
  if (~nRES) begin
    CNT <= 0;
  end
  else if (SCLKEN) begin
    if (CNT == 15'd0)
      CNT <= 15'h77ff;
    else
      CNT <= CNT - 1'd1;
  end
end

endmodule
