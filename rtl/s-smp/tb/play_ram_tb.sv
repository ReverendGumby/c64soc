// Run code in ff5-1-01.spc (Final Fantasy V)
// Disassembly: https://www.ff6hacking.com/ff5wiki/Squall/SPC_disassembly.s

`timescale 1us / 1ns

module play_ram_tb();

reg        nRES;
reg        clk;
reg        hold;
reg        cken_d;

integer    duration_sec, fade_ms;
reg [7:0]  out_g;

wire [15:0] A, MA;
wire [7:0]  do_smp_sim, do_dsp_sim;
wire [7:0]  mdo_dsp_sim, do_ram;
wire signed [15:0] psoutl, psoutr;
wire        cken, cken_hg;
wire        cken_sim;
wire        RW;
wire        nCE, nWE, nOE;
wire        sclken;

initial begin
  $timeformat(-6, 0, " us", 1);

  $dumpfile("play_ram_tb.vcd");
`ifndef VERILATOR
  $dumpvars();
`endif
end

s_smp_sim smp_sim
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken_sim),
   .A(A),
   .DI(do_dsp_sim),
   .DO(do_smp_sim),
   .RW(RW)
   );

s_dsp_sim dsp_sim
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken),
   .A(A),
   .DI(do_smp_sim),
   .DO(do_dsp_sim),
   .RW(RW),
   .MA(MA),
   .MDI(do_ram),
   .MDO(mdo_dsp_sim),
   .nCE(nCE),
   .nWE(nWE),
   .nOE(nOE),
   .SCLKEN(sclken),
   .PSOUTL(psoutl),
   .PSOUTR(psoutr)
   );

initial begin
  clk = 1;
  nRES = 1;
  hold = 1;
end

initial forever begin :clkgen
  #(500.0/6144.0) clk = ~clk;
end

always @(posedge clk)
  cken_d <= cken;
assign cken_hg = cken & ~hold;
assign cken_sim = cken & ~cken_d;

function string note_to_str(input [7:0] vo, input [7:0] note);
string ret;
reg [7:0] ve;
  case (note)
    'd0: ret = "C";
    'd1: ret = "C#";
    'd2: ret = "D";
    'd3: ret = "D#";
    'd4: ret = "E";
    'd5: ret = "F";
    'd6: ret = "F#";
    'd7: ret = "G";
    'd8: ret = "G#";
    'd9: ret = "A";
    'd10: ret = "A#";
    'd11: ret = "B";
    'd12: ret = "x";
    'd13: ret = "-";
    'd14: ret = "y";
  endcase
  if (note != 'd13) begin
    ve = ram.ram['h3d + vo];
    ret = {ret, $sformatf("%1d", ve)};
  end
  return ret;
endfunction

always @(posedge smp_sim.sync) begin
  if (1 && smp_sim.ab == 'h0301) begin
    $display("[ram] %t: 000c/003c: %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x  %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x %02x%02x", $time, ram.ram['h0d], ram.ram['h0c], ram.ram['h0f], ram.ram['h0e], ram.ram['h11], ram.ram['h10], ram.ram['h13], ram.ram['h12], ram.ram['h15], ram.ram['h14], ram.ram['h17], ram.ram['h16], ram.ram['h19], ram.ram['h18], ram.ram['h1b], ram.ram['h1a], ram.ram['h3c], ram.ram['h3d], ram.ram['h3e], ram.ram['h3f], ram.ram['h40], ram.ram['h41], ram.ram['h42], ram.ram['h43], ram.ram['h44], ram.ram['h45], ram.ram['h46], ram.ram['h47], ram.ram['h48], ram.ram['h49], ram.ram['h4a], ram.ram['h4b]);
  end    
end

string notes [0:7];
always @(posedge smp_sim.sync) begin :log_notes
reg [7:0] vo, note;
reg n;
int i;
  if (1 && smp_sim.ab == 'h03f3) begin
    vo = smp_sim.x;
    note = smp_sim.ac;
    notes[vo/2] = note_to_str(vo, note/15);
  end    
  if (1 && smp_sim.ab == 'h0301) begin
    n = 0;
    for (i = 0; i < 8; i++)
      n = n | (notes[i] != "");
    if (n)
      $display("[spc] %12t: %-6s %-6s %-6s %-6s %-6s %-6s %-6s %-6s", $time, notes[0], notes[1], notes[2], notes[3], notes[4], notes[5], notes[6], notes[7]);
    for (i = 0; i < 8; i++)
      notes[i] = "";
  end
end

task read_from_file(input string path);
integer fin, code;
reg [7:0] tmp [0:4];
string    tmps;
  fin = $fopen(path, "r");
  assert(fin != 0) else $finish;

  code = $fseek(fin, 'h25, 0);
  code = $fread(tmp, fin, 0, 2);
  smp_sim.pc = {tmp[1], tmp[0]};
  code = $fread(smp_sim.ac, fin);
  code = $fread(smp_sim.x, fin);
  code = $fread(smp_sim.y, fin);
  code = $fread(smp_sim.p, fin);
  code = $fread(smp_sim.s, fin);
  smp_sim.ab = smp_sim.pc;

  code = $fseek(fin, 'ha9, 0);
  code = $fread(tmp, fin, 0, 3);
  tmps = {tmp[0], tmp[1], tmp[2]};
  duration_sec = tmps.atoi();
  code = $fread(tmp, fin, 0, 5);
  tmps = {tmp[0], tmp[1], tmp[2], tmp[3], tmp[4]};
  fade_ms = tmps.atoi();
  $display("duration: %d", duration_sec + int'(fade_ms / 1000.0));

  code = $fseek(fin, 'h100, 0);
  ram.read_from_file(fin);
  dsp_sim.rf.read_from_file(fin);

  $fclose(fin);
endtask

//initial #9 nRES = 1;
initial begin :load_song
int i;
  out_g = 8'hff;

  read_from_file("song.spc");

  smp_sim.cpui[0] = ram.ram['hf4];
  smp_sim.cpui[1] = ram.ram['hf5];
  smp_sim.cpui[2] = ram.ram['hf6];
  smp_sim.cpui[3] = ram.ram['hf7];
  smp_sim.spt0 = ram.ram['hfa];
  smp_sim.spt1 = ram.ram['hfb];
  smp_sim.spt2 = ram.ram['hfc];
  smp_sim.spcr = ram.ram['hf1] & ~8'h30;

  dsp_sim.preload_rf();

  repeat (3) @(posedge cken) ;
  hold = 0;

  //smp_sim.print_ins_enable = 1;
  dsp_sim.rf.print_reg_enable = 1;

`ifndef VERILATOR
  #200000 $finish;
`endif
  if (duration_sec == 0)
    duration_sec = 1;
  repeat (duration_sec) #(1e6) ;

  for (i = 0; i < fade_ms; i++)
    #(1e3) out_g = 8'(int'((1.0 - i / real'(fade_ms)) * 8'hff));

  $finish;
end

ram ram(.CLK(clk), .A(MA[15:0]), .DI(mdo_dsp_sim), .DO(do_ram), .nCE(nCE), 
        .nWE(nWE), .nOE(nOE));

`ifndef S_DSP_SIM_NOAUDIO

// Write samples to file
// To play the file: 
//   play -b 16 -r 32000 -c 2 -B -e signed-integer play.raw

integer   fout, ftout;
initial begin
  fout = $fopen("play.raw", "w+b");
  ftout = $fopen("play10.raw", "w+b");
end

task static vxout(input signed [14:0] s);
reg signed [15:0] vxout;
  vxout = 16'(s) << 1;
  $fwrite(ftout, "%c%c", vxout[15:8], vxout[7:0]);
endtask

task static exout(input signed [15:0] s);
reg signed [15:0] exout;
  exout = 16'(s) << 0;
  $fwrite(ftout, "%c%c", exout[15:8], exout[7:0]);
endtask

always @(posedge clk) if (sclken) begin :raw_out
reg signed [15:0] l, r;
  l = 16'((24'(psoutl) * out_g) >>> 8);
  r = 16'((24'(psoutr) * out_g) >>> 8);
  $fwrite(fout, "%c%c", l[15:8], l[7:0]);
  $fwrite(fout, "%c%c", r[15:8], r[7:0]);

  vxout(dsp_sim.v0.out);
  vxout(dsp_sim.v1.out);
  vxout(dsp_sim.v2.out);
  vxout(dsp_sim.v3.out);
  vxout(dsp_sim.v4.out);
  vxout(dsp_sim.v5.out);
  vxout(dsp_sim.v6.out);
  vxout(dsp_sim.v7.out);
  exout(dsp_sim.el.OUT);
  exout(dsp_sim.er.OUT);
end

final begin
  $fclose(fout);
  $fclose(ftout);
end

`endif

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s play_ram_tb -f s_dsp_sim.files -o play_ram_tb.vvp ../sp_timer.v s_smp_sim.sv ram.sv play_ram_tb.sv && ./play_ram_tb.vvp"
// End:
