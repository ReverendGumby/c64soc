`timescale 1us / 1ns

module s_dsp_sim_sram_if
  (
   input         CLK,
   input         CLKEN,
   input         MAD_SSMP,
   input         MAD_SDSP,
   input         SP_SEL,
   input [15:0]  A, // address bus (from S-SMP)
   input [7:0]   DI, // data bus (from S-SMP)
   input         RW, // R/W (from S-SMP)
   output [15:0] MA, // SRAM address bus
   input [7:0]   MDI, // SRAM data bus (from RAM)
   output [7:0]  MDO, // SRAM data bus (to RAM)
   output        nCE, // SRAM chip enable
   output        nWE, // SRAM write enable
   output        nOE // SRAM output enable
   );

reg [15:0] dsp_a;
wire [7:0] dsp_di;
reg [7:0]  dsp_do;
reg        dsp_rr, dsp_wr;
reg [15:0] mad_a;
reg        mad_noe, mad_nwe, mad_nce;
reg [7:0]  mad_do;
wire       mad_doe;

assign mad_doe = ~(nWE | nCE);
assign MDO = mad_doe ? mad_do : 8'hxx;

initial begin
  dsp_rr = 0;
  dsp_wr = 0;
end

always @* begin
  if (MAD_SSMP) begin
    mad_a = A;
    mad_do = DI;
    mad_noe = ~RW;
    mad_nwe = RW;
    mad_nce = SP_SEL;
  end
  else /*if (MAD_SDSP)*/ begin
    mad_a = dsp_a;
    mad_do = dsp_do;
    mad_noe = ~dsp_rr;
    mad_nwe = ~dsp_wr;
    mad_nce = ~(dsp_rr | dsp_wr);
  end
end

assign MA = mad_a;
assign nOE = mad_noe;
assign nWE = mad_nwe;
assign nCE = mad_nce;

assign dsp_di = MDI;

task read(input [15:0] addr, output [7:0] data);
  dsp_rr = 1'b1;
  dsp_a = addr;
  do
    @(posedge CLK) ;
  while (~CLKEN);
  assert(MAD_SDSP);
  data = dsp_di;
  dsp_rr = 1'b0;
  dsp_a = 16'bx;
endtask

task write(input [15:0] addr, input [7:0] data);
  dsp_wr = 1'b1;
  dsp_a = addr;
  dsp_do = data;
  do
    @(posedge CLK) ;
  while (~CLKEN);
  assert(MAD_SDSP);
  dsp_wr = 1'b0;
  dsp_a = 16'bx;
endtask

endmodule
