`timescale 1us / 1ns

module s_dsp_sim_out_mixer
  (
   input signed [15:0]      V0,
   input signed [15:0]      V1,
   input signed [15:0]      V2,
   input signed [15:0]      V3,
   input signed [15:0]      V4,
   input signed [15:0]      V5,
   input signed [15:0]      V6,
   input signed [15:0]      V7,
   input [7:0]              MVOL,
   input signed [15:0]      EOUT,
   input                    MUTE,
   output reg signed [15:0] OUT
   );

function signed [15:0] clamp(input signed [16:0] in);
  if (in > 17'sh7fff)
    in = 17'sh7fff;
  else if (in < -17'sh8000)
    in = -17'sh8000;
  clamp = 16'(in);
endfunction

reg signed [15:0] out;

task run_mix;
  out = 16'(V0);
  out = clamp(out + V1);
  out = clamp(out + V2);
  out = clamp(out + V3);
  out = clamp(out + V4);
  out = clamp(out + V5);
  out = clamp(out + V6);
  out = clamp(out + V7);
  out = clamp(17'((24'(out) * signed'(MVOL)) >>> 7));
  out = clamp(out + EOUT);
  if (MUTE)
    out = 0;
endtask

// Mixer-related steps of the sample generation loop
task out_sample;
  run_mix();
  OUT = out[15:0];
endtask

endmodule
