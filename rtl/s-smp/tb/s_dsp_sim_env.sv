`timescale 1us / 1ns

module s_dsp_sim_env
  (
   input         nRES,
   input [14:0]  ENV_EV_CNT,

   input         KON,
   input         KOF,
   input         PLAY,
   input         BEND,
   input [7:0]   ADSR1,
   input [7:0]   ADSR2,
   input [7:0]   GAIN,

   output [10:0] OUT
   );

reg [10:0] envelope;
reg [1:0]  adsr_st;

localparam [1:0] ADSR_ST_RLS = 2'd0;
localparam [1:0] ADSR_ST_ATK = 2'd1;
localparam [1:0] ADSR_ST_DCY = 2'd2;
localparam [1:0] ADSR_ST_STN = 2'd3;

wire [3:0] A = ADSR1[3:0];
wire [2:0] D = ADSR1[6:4];
wire [2:0] S = ADSR2[7:5];
wire [4:0] R = ADSR2[4:0];

wire eam_adsr = ADSR1[7];
wire eam_direct = ~eam_adsr & ~GAIN[7];
wire eam_gain = ~eam_adsr & GAIN[7];

localparam [1:0] GAIN_LIN_DEC = 2'b00;
localparam [1:0] GAIN_EXP_DEC = 2'b01;
localparam [1:0] GAIN_LIN_INC = 2'b10;
localparam [1:0] GAIN_EXP_INC = 2'b11;

wire [1:0] gain_mode = GAIN[6:5];
wire [4:0] gain_rate = GAIN[4:0];

initial begin
  envelope = 0;
  adsr_st = ADSR_ST_RLS;
end

// Period / offset table

reg [14:0] ev_period;
reg [10:0] ev_offset;
reg [4:0] ev_rate;
wire env_ev;

always @* begin
  case (ev_rate)
    5'd0: ev_period = 15'h7fff; // infinite
    5'd1: ev_period = 15'd2048;
    5'd2: ev_period = 15'd1536;
    5'd3: ev_period = 15'd1280;
    5'd4: ev_period = 15'd1024;
    5'd5: ev_period = 15'd768;
    5'd6: ev_period = 15'd640;
    5'd7: ev_period = 15'd512;
    5'd8: ev_period = 15'd384;
    5'd9: ev_period = 15'd320;
    5'd10: ev_period = 15'd256;
    5'd11: ev_period = 15'd192;
    5'd12: ev_period = 15'd160;
    5'd13: ev_period = 15'd128;
    5'd14: ev_period = 15'd96;
    5'd15: ev_period = 15'd80;
    5'd16: ev_period = 15'd64;
    5'd17: ev_period = 15'd48;
    5'd18: ev_period = 15'd40;
    5'd19: ev_period = 15'd32;
    5'd20: ev_period = 15'd24;
    5'd21: ev_period = 15'd20;
    5'd22: ev_period = 15'd16;
    5'd23: ev_period = 15'd12;
    5'd24: ev_period = 15'd10;
    5'd25: ev_period = 15'd8;
    5'd26: ev_period = 15'd6;
    5'd27: ev_period = 15'd5;
    5'd28: ev_period = 15'd4;
    5'd29: ev_period = 15'd3;
    5'd30: ev_period = 15'd2;
    5'd31: ev_period = 15'd1;
    default: ev_period = 15'dx;
  endcase
end

always @* begin
  case (ev_rate % 3)
    5'd0: ev_offset = 11'd536;
    5'd1: ev_offset = 11'd0;
    5'd2: ev_offset = 11'd1040;
    default: ev_offset = 11'dx;
  endcase
end

always @* begin
  ev_rate = 5'd31;
  if (eam_gain && adsr_st != ADSR_ST_RLS)
    ev_rate = gain_rate;
  else if (eam_adsr)
    case (adsr_st)
      ADSR_ST_ATK: ev_rate = A * 2 + 1;
      ADSR_ST_DCY: ev_rate = D * 2 + 16;
      ADSR_ST_STN: ev_rate = R;
      default: ;
    endcase
end

assign env_ev = ((ENV_EV_CNT + 15'(ev_offset)) % ev_period) == 0;

// Envelope output

reg [1:0] ev_gain_mode;

always @* begin
  ev_gain_mode = gain_mode;
  if (eam_adsr) begin
    case (adsr_st)
      ADSR_ST_ATK: ev_gain_mode = GAIN_LIN_INC;
      ADSR_ST_DCY, ADSR_ST_STN: ev_gain_mode = GAIN_EXP_DEC;
      default: ;
    endcase
  end
end

reg [10:0] envelope_next, envelope_stn;

always @* begin :calc_envelope_next
reg signed [12:0] n;
  n = 13'(envelope);

  if (adsr_st == ADSR_ST_RLS) begin
    if (BEND)
      n = 0;
    else
      n = n - 'sd8;
  end
  else begin
    if (eam_direct)
      n = {2'b0, GAIN[6:0], 4'b0};
    else
      case (ev_gain_mode)
        GAIN_LIN_DEC:
          n = n - 'sd32;
        GAIN_EXP_DEC: begin
          n = n - 'sd1;
          n = n - (n >>> 8);
        end
        GAIN_LIN_INC:
          n = n + ((eam_adsr && A == 4'hF) ? 'sd1024 : 'sd32);
        GAIN_EXP_INC:
          n = n + ((n < 'sh600) ? 'sd32 : 'sd8);
        default: n = 13'sdx;
      endcase
  end

  if (n > 13'sd2047)
    n = 13'sd2047;
  else if (n < 13'sd0)
    n = 13'sd0;

  envelope_next = n[10:0];
end

assign envelope_stn = {S, 8'hff};

task update;
  if (KON) begin
    adsr_st = ADSR_ST_ATK;
    envelope = 0;
  end
  else if (KOF | BEND) begin
    adsr_st = ADSR_ST_RLS;
  end

  if (PLAY & env_ev)
    case (adsr_st)
      ADSR_ST_ATK: begin
        if (envelope != envelope_next)
          envelope = envelope_next;
        else if (eam_adsr) begin
          if (envelope_next > envelope_stn)
            adsr_st = ADSR_ST_DCY;
          else
            adsr_st = ADSR_ST_STN;
        end
      end
      ADSR_ST_DCY: begin
        if (envelope_next > envelope_stn)
          envelope = envelope_next;
        else
          adsr_st = ADSR_ST_STN;
      end
      ADSR_ST_STN, ADSR_ST_RLS: begin
        envelope = envelope_next;
      end
      default: ;
    endcase
endtask

assign OUT = envelope;

endmodule
