`timescale 1us / 1ns

module s_dsp_sim_reg_file
  (
   input            nRES, // reset
   input            CLK, // clock
   input            CLKEN,
   input [6:0]      A,
   input            RS,
   input            WS,
   input [7:0]      RDI,
   output reg [7:0] RDO
   );

reg [7:0] file [0:127];
reg       kon_written;

initial begin
  //for (integer i = 0; i < 128; i = i + 1)
  //  file[i] = 0;
  kon_written = 0;
end

task read_from_file(integer fin);
integer code;
  code = $fread(file, fin);
endtask

task write_to_file(integer fout);
int i;
  for (i = 0; i < $size(file); i++)
    $fwrite(fout, "%c", file[i]);
endtask

always @* begin
  RDO = 8'h00;
  RDO = file[A];
  if (RS) begin
    //$display("[dsp] %t: @%02X R %02X", $time, A, RDO);
  end
end

time last_write;
initial last_write = 0;
reg print_reg_enable;
initial print_reg_enable = 0;
task print_reg;
input [6:0] A;
time now;
  begin
    now = $time;
    if (print_reg_enable)
      $display("[dsp] %t (+%9t): @%02X W %02X", now, now - last_write, A, file[A]);
    last_write = now;
  end
endtask

always @(posedge CLK) if (CLKEN) begin
  if (~nRES) begin
  end
  else if (WS) begin
    if (file[A] !== RDI) begin
      file[A] = RDI;
      print_reg(A);
    end
    case (A)
      'h4C: kon_written = 1'b1;
      default: ;
    endcase
  end
end

//assign ENDX = file['h7C];

task get_kon_if_written(inout [7:0] out);
  if (kon_written)
    out = file['h4C];
  kon_written = 0;
endtask

endmodule
