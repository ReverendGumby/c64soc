`timescale 1us / 1ns

module brrdec;

reg CLK, nRES;
reg SCLKEN, clken;

integer fout;
initial begin
  fout = $fopen("brrdec.raw", "w+b");
end
final begin
  $fclose(fout);
end

initial CLK = 1;
initial clken = 0;
always #0.5 CLK <= ~CLK;
always #1 clken <= ~clken;
always begin
  SCLKEN <= 0;
  #15 SCLKEN <= 1;
  #1 ;
end

reg [13:0] P;
reg [7:0]  SCRN;
wire       bend;

// Bridge DMA to RAM

wire [15:0] MA;
wire [7:0]  MDI, MDO;
wire        nCE, nWE, nOE;

ram ram(.CLK(CLK), .A(MA), .DI(MDO), .DO(MDI), 
        .nCE(nCE), .nWE(nWE), .nOE(nOE));

s_dsp_sim_sram_if sram
  (
   .CLK(CLK),
   .CLKEN(clken),
   .MAD_SSMP(1'b0),
   .MAD_SDSP(1'b1),
   .SP_SEL(1'b0),
   .A(16'hzzzz),
   .DI(8'hzz),
   .RW(1'b1),
   .MA(MA),
   .MDI(MDI),
   .MDO(MDO),
   .nCE(nCE),
   .nWE(nWE),
   .nOE(nOE)
   );

initial begin
integer fin;
integer code;
  fin = $fopen("song.spc", "r");
  assert(fin != 0) else $finish;
  code = $fseek(fin, 'h100, 0);
  ram.read_from_file(fin);
  $fclose(fin);
end

// BRR sample player

wire signed [14:0] brr_sout0, brr_sout1, brr_sout2, brr_sout3;

reg                KON;
wire               play;
wire [2:0]         next;

initial KON = 0;

s_dsp_sim_brr brr
  (
   .nRES(nRES),
   .KON(KON), .DIR(8'h1B), .SCRN(SCRN), .VOICE(3'd7),
   .NEXT(next), .PLAY(play), .BEND(bend),
   .OUT0(brr_sout0), .OUT1(brr_sout1), .OUT2(brr_sout2), .OUT3(brr_sout3)
   );

// Gaussian interpolator

wire signed [14:0] interp_sout;

s_dsp_sim_interp interp
  (
   .nRES(nRES),
   .P(P), .PLAY(play), .NEXT(next),
   .IN0(brr_sout0), .IN1(brr_sout1), .IN2(brr_sout2), .IN3(brr_sout3),
   .OUT(interp_sout)
   );

initial begin
  $dumpfile("brrdec.vcd");
  $dumpvars;
end

wire brrfs_max = brr.brrfs[13] ^ brr.brrfs[14];

initial begin
  SCLKEN = 0;
  SCRN = 'h20;
  P = 14'h09F3;
  KON = 1'b1;

  nRES <= 0;
  repeat (10) @(posedge CLK) ;
  nRES <= 1;

  do begin
    @(posedge clken) brr.load_ptr();
    @(posedge clken) brr.load_header_s1();
    @(posedge clken) brr.load_s2();
    @(posedge CLK) interp.go();
    @(posedge SCLKEN) KON <= 1'b0;
  end while (!bend);

  $finish;
end

initial #500000 $finish;

always @(posedge CLK) begin
reg [15:0] out;
  if (SCLKEN) begin
    out = 16'(interp_sout) <<< 1;
    //out = 16'(brr_sout);
    $fwrite(fout, "%c%c", out[15:8], out[7:0]);
  end
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s brrdec -o brrdec.vvp s_dsp_sim_brr.sv s_dsp_sim_interp.sv s_dsp_sim_sram_if.sv ram.sv brrdec.sv && ./brrdec.vvp"
// End:
