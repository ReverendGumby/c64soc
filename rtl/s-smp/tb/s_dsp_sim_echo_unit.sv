`timescale 1us / 1ns

module s_dsp_sim_echo_unit #(parameter reg SIDE = 0)
  (
   input signed [15:0]      V0,
   input signed [15:0]      V1,
   input signed [15:0]      V2,
   input signed [15:0]      V3,
   input signed [15:0]      V4,
   input signed [15:0]      V5,
   input signed [15:0]      V6,
   input signed [15:0]      V7,
   input                    ECEN, // 1=disable echo write
   input [7:0]              EON,
   input [7:0]              EVOL,
   input [7:0]              EFB,
   input [7:0]              ESA,
   input [7:0]              EDL,
   input [7:0]              FIR0,
   input [7:0]              FIR1,
   input [7:0]              FIR2,
   input [7:0]              FIR3,
   input [7:0]              FIR4,
   input [7:0]              FIR5,
   input [7:0]              FIR6,
   input [7:0]              FIR7,

   output reg signed [15:0] OUT
   );

reg signed [15:0] mix_fbin;
wire signed [15:0] mix_out;

initial mix_fbin = 0;

// Mixer

s_dsp_sim_echo_mixer mixer
  (
   .V0(V0), .V1(V1), .V2(V3), .V3(V3), .V4(V4), .V5(V5), .V6(V6), .V7(V7),
   .EON(EON), .EFB(EFB), .EFBIN(mix_fbin),
   .OUT(mix_out)
   );

// FIR filter

reg signed [16:0] fir_sum;
reg signed [14:0] fir_in;
reg signed [15:0] s [0:7];

initial begin :init0
int i;
  fir_sum = 0;
  for (i = 1; i < 8; i++)
    s[i] = 0;
end

function signed [16:0] clamp(input signed [16:0] in);
  if (in > 17'sh7fff)
    in = 17'sh7fff;
  else if (in < -17'sh8000)
    in = -17'sh8000;
  clamp = in;
endfunction

task run_fir;
int i;
  s[0] = 16'(fir_in);

  fir_sum = 0;
  fir_sum = fir_sum + 16'((24'(s[7]) * signed'(FIR0)) >>> 6);
  fir_sum = fir_sum + 16'((24'(s[6]) * signed'(FIR1)) >>> 6);
  fir_sum = fir_sum + 16'((24'(s[5]) * signed'(FIR2)) >>> 6);
  fir_sum = fir_sum + 16'((24'(s[4]) * signed'(FIR3)) >>> 6);
  fir_sum = fir_sum + 16'((24'(s[3]) * signed'(FIR4)) >>> 6);
  fir_sum = fir_sum + 16'((24'(s[2]) * signed'(FIR5)) >>> 6);
  fir_sum = fir_sum + 16'((24'(s[1]) * signed'(FIR6)) >>> 6);
  fir_sum = fir_sum + 16'((24'(s[0]) * signed'(FIR7)) >>> 6);
  fir_sum = clamp(fir_sum);
  fir_sum[0] = 1'b0;

  for (i = 6; i >= 0; i--)
    s[i+1] = s[i];
endtask

// Buffer read/write

reg [12:0] idx, idx_max;
reg [15:0] ebuf_in, ebuf_out;

initial begin
  idx = 0;
  ebuf_in = 0;
  ebuf_out = 0;
end

task adv_idx;
reg [12:0] x;
  if (idx == 0)
    idx_max = 13'(EDL[3:0]) << 9;

  x = idx + 1'd1;
  if (x >= idx_max)
    x = 0;
  idx = x;
endtask

function [15:0] buf_addr(input off);
  buf_addr[15:0] = {ESA, 8'h00} + {idx, SIDE, off};
endfunction

// Echo-related steps of the sample generation loop
task load_buf;
reg [15:0] ebuf_tmp;
  sram.read(buf_addr(1'd0), ebuf_tmp[7:0]);
  sram.read(buf_addr(1'd1), ebuf_tmp[15:8]);
  ebuf_out = ebuf_tmp;
endtask

task store_buf;
  mixer.out_sample();
  ebuf_in = {mix_out[15:1], 1'b0};
  sram.write(buf_addr(1'd0), ebuf_in[7:0]);
  sram.write(buf_addr(1'd1), ebuf_in[15:8]);
endtask

task inc_offset;
  adv_idx();
endtask

task out_sample;
  fir_in = ebuf_out[15:1];
  run_fir();
  mix_fbin = fir_sum[15:0];
  OUT = 16'((24'(fir_sum) * signed'(EVOL)) >>> 7);
endtask

endmodule
