`timescale 1us / 1ns

module s_dsp_sim_interp
  (
   input                nRES,

   input [13:0]         P,
   input                PLAY,
   output [2:0]         NEXT,

   input signed [14:0]  IN0,
   input signed [14:0]  IN1,
   input signed [14:0]  IN2,
   input signed [14:0]  IN3,
   output signed [14:0] OUT
   );

reg [10:0] tbl [0:511];

`include "s_dsp_sim_interp_tbl.svh"

reg signed [16:0] out;
reg [14:0]        pcnt;

wire [15:0] pcnt_next = pcnt + 16'(P);
wire        pcnt_wrap = pcnt_next[14];

initial begin
  out = 0;
  pcnt = 0;
end

event compute;

task go;
  if (PLAY)
    pcnt = pcnt_next[14:0];
  else
    pcnt = 0;

  -> compute;
endtask

assign NEXT = 3'(pcnt_next[15:12] - pcnt[14:12]); // how many new samples needed

wire [7:0] idx = pcnt[11:4];

always @compute begin
  out =       17'(signed'((26'(IN0) * tbl[9'h0ff - idx])) >>> 10);
  out = out + 17'(signed'((26'(IN1) * tbl[9'h1ff - idx])) >>> 10);
  out = out + 17'(signed'((26'(IN2) * tbl[9'h100 + idx])) >>> 10);
  out = out + 17'(signed'((26'(IN3) * tbl[9'h000 + idx])) >>> 10);

  if (out > 17'sh7fff)
    out = 17'sh7fff;
  else if (out < -17'sh8000)
    out = -17'sh8000;
end

assign OUT = out[15:1];

endmodule
