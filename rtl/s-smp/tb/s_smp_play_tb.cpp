// Verilated -*- C++ -*-
// DESCRIPTION: main() calling loop, created with Verilator --main

#include "verilated.h"
#include "Vs_smp_play_tb.h"

//======================

int main(int argc, char** argv, char**) {
    // Setup context, defaults, and parse command line
    Verilated::debug(0);
    const std::unique_ptr<VerilatedContext> contextp{new VerilatedContext};
    contextp->traceEverOn(true);
    contextp->commandArgs(argc, argv);

    // Construct the Verilated model, from Vtop.h generated from Verilating
    const std::unique_ptr<Vs_smp_play_tb> topp{new Vs_smp_play_tb{contextp.get()}};

    // Simulate until $finish
    while (!contextp->gotFinish()) {
        // Evaluate model
        topp->eval();
        // Advance time
        if (!topp->eventsPending()) break;
        contextp->time(topp->nextTimeSlot());
    }

    if (!contextp->gotFinish()) {
        VL_DEBUG_IF(VL_PRINTF("+ Exiting without $finish; no events left\n"););
    }

    // Final model cleanup
    topp->final();
    return 0;
}

// Local Variables:
// compile-command: "verilator +1364-2005ext+v -DS_DSP_SIM_NOAUDIO --cc --exe s_smp_play_tb.cpp --build -j 0 --timing --assert --trace --top-module s_smp_play_tb -F s_dsp_sim_noaudio.files ../sp_timer.v ../s_smp.v s_smp_sim.sv ram.sv s_smp_play_tb.sv"
// End:
