`timescale 1us / 1ns

module s_smp_sim
  (
   input             nRES,                 // reset
   input             CLK,                  // clock
   input             CKEN,
   output reg [15:0] A,                    // address bus
   input [7:0]       DI,                   // data bus (input)
   output [7:0]      DO,                   // data bus (output)
   output reg        RW                    // read / not write
   );

reg nIRQ;                 // interrupt request
reg sync;                 // instruction sync

initial begin
  nIRQ = 'b1;
end

reg cken_d;
initial cken_d = 'b1;
always @(posedge CLK)
  cken_d <= CKEN;
wire cken_posedge = CKEN & ~cken_d;

string    opcode_st [0:255];
reg [7:0] opcode_op [0:255];
reg [4:0] opcode_am [0:255];
reg [4:0] opcode_cy [0:255];
`include "s_smp_sim_opcodes.svh"

reg [15:0]       pc;
reg [7:0]        ir, ir_d;
reg [7:0]        p, s, ac, x, y;
reg [7:0]        dor, m, m2;
reg [8:0]        tmp;
reg [15:0]       ea;
reg              eac, hc;
reg              resp, resg, irqp, intg;
reg [7:0]        op, op_d;
reg [4:0]        am, am_d;
reg [15:0]       ab;
reg              rw_int;
reg              int_op;
reg              branched;
reg [2:0]        abb_bit_sel;
integer          cycles;
reg              hold;

`define p_c p[0]
`define p_z p[1]
`define p_i p[2]
`define p_h p[3]
`define p_b p[4]
`define p_p p[5]                // direct page
`define p_v p[6]
`define p_n p[7]
//always @* `p_b = !(resg || intg);
initial `p_b = 0; // Always 0 in .spc states

initial resg = 0;
initial irqp = 0;
initial resp = 0; //1'b1;
initial intg = 0;
initial rw_int = 1;
initial int_op = 0;
initial hold = 1;

assign RW = rw_int || resg;
assign A = ab;

wire p_sel;

reg  db_oe;
always @* begin
  db_oe = !rw_int;
end
assign DO = db_oe ? dor : 8'hxx;

reg [7:0] ipdb;
wire [7:0] db = p_sel ? ipdb : DI;

task automatic wait_clk(int cnt = 1);
int i;
  for (i = 0; i < cnt; i++) begin
    do begin
      @(posedge CLK) ;
    end while (!cken_posedge);
  end
endtask

initial
  cycles = 0;

always @(posedge CLK) begin
  resp <= !nRES;
  irqp <= !nIRQ;
end

always @(posedge CLK) begin
  if (cken_posedge)
    cycles <= cycles + 1;
end

always begin
  if (hold) begin
    // give tb a chance to set our initial state
    wait_clk(3); // align with s_smp_play_tb.sv hold deassertion
    hold <= 0;
  end

  if (resp) begin
    wait_clk();
    sync <= 0;
    rw_int <= 1'b1;
    int_op <= 0;
    resg <= 1'b1;
  end
  else
    do_ins();
end

event do_ins_dly;

task do_ins();
integer   cy_opstart, cy_want, cy_got;
  cy_opstart = cycles;
  branched = 0;
  sync <= 1;
  ab <= pc;
  rw_int <= 1'b1;
  int_op <= 0;
  if (irqp && !`p_i) begin
    intg = 1'b1;
  end

  wait_clk();
  sync <= 0;
  if (resg || intg)
    ir = `BRK;
  else
    ir = db;
  print_ins();
  if (!intg)
    pc = pc + 1;
  ab <= pc;

  _do_ins();

  cy_want = 32'(opcode_cy[ir]);
  cy_got = cycles - cy_opstart;
  if (branched)
    cy_want = cy_want + 2;
  assert (cy_got == cy_want)
    else
      $fatal(1, "Cycles wanted %1d got %1d", cy_want, cy_got);
endtask

task _do_ins();
  wait_clk();

  op = opcode_op[ir];
  am = opcode_am[ir];
  //$display("sim: ir=%02x op=%02x am=%d", ir, op, am);

  case (am)
    `AM_A: ;
    `AM_IMPL: begin
      case (op)
        `OP_BRK: begin
          if (!intg)
            pc = pc + 1;
          ab[15:8] <= 8'h01;
          ab[7:0] <= s;
          rw_int <= 0;
          dor <= pc[15:8];

          wait_clk();
          ab[7:0] <= s - 1;
          dor <= pc[7:0];

          wait_clk();
          ab[7:0] <= s - 2;
          dor <= p;

          wait_clk();
          rw_int <= 1'b1;
          s <= s - 3;

          wait_clk();
          ab[15:8] <= 8'hff;
          ab[7:0] <= resg ? 8'hfe : 8'hde;

          wait_clk();
          intg = 1'b0;

          ea[7:0] = db;
          ab[0] <= 1'b1;
          if (resg)
            resg = 0;
          `p_i = 1'b1;

          wait_clk();
          ea[15:8] = db;
          pc = ea;
        end
        `OP_CLRC: begin
          `p_c = 0;
        end
        `OP_CLRP: begin
          `p_p = 0;
        end
        `OP_DI: begin
          wait_clk();
          `p_i = 0;
        end
        `OP_DIV: begin :op_div
        logic [16:0] yva;
        logic [16:0] tx;
          yva = {1'b0, y, ac};
          tx = {x, 9'b0};
          wait_clk();

          repeat (9) begin
            yva = {yva[15:0], yva[16]};
            if (yva >= tx)
              yva = yva ^ 1;
            if (yva[0])
              yva = yva - tx;
            {y, `p_v, ac} = yva;
            wait_clk();
          end
        end
        `OP_CLRV: begin
          `p_v = 0;
          `p_h = 0;
        end
        `OP_DAA, `OP_DAS: begin
          wait_clk();
        end
        `OP_DBNZ: begin
          assert (ir == `DBNZ_Y);

          wait_clk();

          ab <= pc;
          wait_clk();

          y = y - 1;
          pc = pc + 1;
          ea[7:0] = db;
          ea[15:8] = {8{db[7]}};
          if (y != 0) begin
            branched = 1;
            wait_clk();
            {eac, pc[7:0]} = ea[7:0] + pc[7:0];

            wait_clk();
            pc[15:8] = pc[15:8] + ea[15:8] + {7'b0, eac};
          end
        end
        `OP_DEC, `OP_INC: begin
          -> do_ins_dly;
        end
        `OP_MUL: begin
          wait_clk(7);
          {y, ac} = y * ac;
          `p_n = y[7];
          `p_z = y == 0;
        end
        `OP_NOP: begin
        end
        `OP_NOTC: begin
          wait_clk(1);
          `p_c = ~`p_c;
        end
        `OP_PCALL: begin
          // Cycle 3 (T2)
          ea[15:8] = 8'hff;
          ea[7:0] = db;
          wait_clk();

          // Cycle 4 (T3)
          ab[15:8] <= 8'h01;
          ab[7:0] <= s;
          dor <= pc[15:8];
          rw_int <= 0;
          wait_clk();

          // Cycle 5 (T4)
          dor <= pc[7:0];
          ab[7:0] <= ab[7:0] - 1;
          wait_clk();

          // Cycle 6 (T5)
          rw_int <= 1;
          wait_clk();

          // Cycle 1 (T0) of the next instruction
          s = s - 2;
          pc = ea;
        end
        `OP_PUSH: begin
          ab[15:8] <= 8'h01;
          ab[7:0] <= s;
          rw_int <= 0;
          case (ir)
            `PUSH_A: dor <= ac;
            `PUSH_X: dor <= x;
            `PUSH_Y: dor <= y;
            `PUSH_PSW: dor <= p;
            default: ;
          endcase

          wait_clk();
          rw_int <= 1'b1;
          s = s - 1;

          wait_clk();
        end
        `OP_POP: begin
          ab[15:8] <= 8'h01;
          ab[7:0] <= s;

          wait_clk();
          ab[7:0] <= s + 1;
          s = s + 1;

          wait_clk();
          case (ir)
            `POP_A: ac = db;
            `POP_X: x = db;
            `POP_Y: y = db;
            `POP_PSW: p = db;
            default: ;
          endcase
        end
        `OP_RET: begin
          // Cycle 3
          pc = pc + 1;
          ab[15:8] <= 8'h01;
          ab[7:0] <= s;
          wait_clk();

          // Cycle 4
          ab[7:0] <= s + 1;
          wait_clk();

          // Cycle 5
          ea[7:0] = db;
          ab[7:0] <= s + 2;
          s = s + 2;
          wait_clk();

          // Cycle 6
          ea[15:8] = db;
          pc = ea;
          ab <= pc;
        end
        `OP_RETI: begin
          // Cycle 3
          pc = pc + 1;
          ab[15:8] <= 8'h01;
          ab[7:0] <= s;
          wait_clk();

          // Cycle 4
          ab[7:0] <= s + 1;
          wait_clk();

          // Cycle 5
          {p[7:6], p[3:0]} = {db[7:6], db[3:0]};
          ab[7:0] <= s + 2;
          wait_clk();

          // Cycle 6
          ea[7:0] = db;
          ab[7:0] <= s + 3;
          s = s + 3;
          wait_clk();

          // Cycle 1 of the next instruction
          ea[15:8] = db;
          pc = ea;
          ab <= pc;
        end
        `OP_SETC: begin
          `p_c = 1'b1;
        end
        `OP_SETP: begin
          `p_p = 1'b1;
        end
        `OP_SLEEP: begin
          $display("SLEEP/STOP");
          $finish;
        end
        `OP_EI: begin
          wait_clk();
          `p_i = 1'b1;
        end
        `OP_TAX: begin
          x = ac;
          `p_z = x == 0;
          `p_n = x[7];
        end
        `OP_TAY: begin
          y = ac;
          `p_z = y == 0;
          `p_n = y[7];
        end
        `OP_TCALL: begin
          // Cycle 3 (T2)
          ab[15:8] <= 8'h01;
          ab[7:0] <= s;
          wait_clk();

          // Cycle 4 (T3)
          dor <= pc[15:8];
          rw_int <= 0;
          wait_clk();

          // Cycle 5 (T4)
          dor <= pc[7:0];
          ab[7:0] <= ab[7:0] - 1;
          wait_clk();

          // Cycle 6 (T5)
          rw_int <= 1;
          wait_clk();

          // Cycle 7 (T6)
          ab <= {8'hff, 3'b110, ~ir[7:4], 1'b0};
          wait_clk();

          // Cycle 8 (T7)
          ea[7:0] = db;
          ab[0] <= 1'b1;
          wait_clk();

          // Cycle 1 (T0) of the next instruction
          ea[15:8] = db;
          s = s - 2;
          pc = ea;
        end
        `OP_TSX: begin
          x = s;
          `p_z = x == 0;
          `p_n = x[7];
        end
        `OP_TXA: begin
          ac = x;
          `p_z = ac == 0;
          `p_n = ac[7];
        end
        `OP_TXS: begin
          s = x;
        end
        `OP_TYA: begin
          ac = y;
          `p_z = ac == 0;
          `p_n = ac[7];
        end
        `OP_XCN: begin
          wait_clk(3);
          ac = {ac[3:0], ac[7:4]};
          `p_z = ac == 0;
          `p_n = ac[7];
        end
        default: $finish;
      endcase
    end
    `AM_IMM: begin
      pc = pc + 1;
    end
    `AM_IMMDP: begin
      m2 = db;
      pc = pc + 1;
      ab <= pc;
      wait_clk();

      ea[15:8] = {7'b0, `p_p};
      ea[7:0] = db;
      pc = pc + 1;
      ab <= ea;

      if (op == `OP_CMP)
        wait_clk();
    end
    `AM_ABB: begin
      ea[7:0] = db;
      pc = pc + 1;
      ab <= pc;
      wait_clk();

      ea[15:8] = {3'b000, db[4:0]};
      abb_bit_sel = db[7:5];
      pc = pc + 1;
      ab <= ea;
    end
    `AM_ABS: begin
      ea[7:0] = db;
      pc = pc + 1;
      ab <= pc;

      wait_clk();
      ea[15:8] = db;
      pc = pc + 1;
      ab <= ea;
    end
    `AM_ABSX, `AM_ABSY: begin
      ea[7:0] = db;
      pc = pc + 1;
      ab <= pc;

      wait_clk();
      ea[15:8] = db;
      pc = pc + 1;

      wait_clk();
      ea = ea + {8'b0, (am == `AM_ABSX ? x : y)};
      ab <= ea;
    end
    `AM_DP, `AM_DPB, `AM_DPBR: begin
      ea[15:8] = {7'b0, `p_p};
      ea[7:0] = db;
      pc = pc + 1;
      ab <= ea;
    end
    `AM_DPDP: begin
      ea[15:8] = {7'b0, `p_p};
      ea[7:0] = db;
      pc = pc + 1;
      ab <= ea;

      wait_clk();
      m2 = db;
      ab <= pc;

      wait_clk();
      ea[15:8] = {7'b0, `p_p};
      ea[7:0] = db;
      pc = pc + 1;
      ab <= ea;
    end
    `AM_DPX, `AM_DPY: begin
      ea[15:8] = {7'b0, `p_p};
      ea[7:0] = db;
      pc = pc + 1;
      wait_clk();

      ea[7:0] = ea[7:0] + (am == `AM_DPX ? x : y);
      ab <= ea;
    end
    `AM_IPIP: begin
      ea[15:8] = {7'b0, `p_p};
      ea[7:0] = y;
      ab <= ea;

      wait_clk();
      m2 = db;
      ea[7:0] = x;
      ab <= ea;
    end
    `AM_XIND, `AM_XINDP: begin
      ea[15:8] = {7'b0, `p_p};
      ea[7:0] = x;
      ab <= ea;
      if (ir == `MOV_XINDP_A)
        int_op <= 1'b1;

      if (am == `AM_XINDP)
        x = x + 1;
    end
    `AM_XIIND: begin
      ea[15:8] = {7'b0, `p_p};
      ea[7:0] = db;
      pc = pc + 1;
      wait_clk();

      ea[7:0] = ea[7:0] + x;
      ab <= ea;
      wait_clk();

      ea[7:0] = db;
      ab[7:0] <= ab[7:0] + 1;
      wait_clk();

      ea[15:8] = db;
      ab <= ea;
    end
    `AM_INDYI: begin
      ab[15:8] <= {7'b0, `p_p};
      ab[7:0] <= db;
      pc = pc + 1;
      wait_clk();

      ea[7:0] = db;
      ab[7:0] <= ab[7:0] + 1;
      wait_clk();

      ea[15:8] = db;
      wait_clk();

      ea = ea + {8'b0, y};
      ab <= ea;
    end
    `AM_REL: begin
      assert (op == `OP_BRA);

      ea[7:0] = db;
      ea[15:8] = {8{db[7]}};
      pc = pc + 1;
      //ab <= pc;

      if (ir === `BRA ||
          ir === `BPL && !`p_n ||
          ir === `BMI &&  `p_n ||
          ir === `BVC && !`p_v ||
          ir === `BVS &&  `p_v ||
          ir === `BCC && !`p_c ||
          ir === `BCS &&  `p_c ||
          ir === `BNE && !`p_z ||
          ir === `BEQ &&  `p_z) begin
        branched = (ir != `BRA);
        wait_clk();
        {eac, pc[7:0]} = ea[7:0] + pc[7:0];

        wait_clk();
        pc[15:8] = pc[15:8] + ea[15:8] + {7'b0, eac};
      end
    end
    `AM_AINDX: begin
      assert (ir == `JMP_AINDX);
      ea[7:0] = db;
      pc = pc + 1;
      ab <= pc;
      wait_clk();

      ea[15:8] = db;
      pc = pc + 1;
      wait_clk();

      ea = ea + {8'b0, x};
      ab <= ea;
      wait_clk();

      ea[7:0] = db;
      ab[7:0] <= ab[7:0] + 1;
      wait_clk();

      ea[15:8] = db;
      pc = ea;
    end
    default: $finish;
  endcase

  case (op)
    `OP_ADC, `OP_AND, `OP_AND1, `OP_ASL, `OP_BBC, `OP_BBS, `OP_CBNE, `OP_CMP,
    `OP_CLR1, `OP_CPX, `OP_CPY, `OP_DBNZ, `OP_DEC, `OP_EOR, `OP_EOR1, `OP_INC,
    `OP_LDA, `OP_LDC, `OP_LDX, `OP_LDY, `OP_LDW, `OP_LSR, `OP_MOV, `OP_NOT1,
    `OP_OR, `OP_OR1, `OP_ROL, `OP_ROR, `OP_SET1, `OP_STC, `OP_STI, `OP_SBC,
    `OP_TCLR1, `OP_TSET1: begin
      if (am == `AM_IMPL)
        ;
      else if (am == `AM_IMM)
        m = db;
      else if (am == `AM_A)
        m = ac;
      else if (am == `AM_DPDP && op == `OP_MOV)
        m = db;
      else begin
        wait_clk();
        m = db;
      end
      dor <= m;
    end
    default: ;
  endcase

  case (op)
    `OP_BRA, `OP_BRK, `OP_CLRC, `OP_CLRP, `OP_CLRV, `OP_DI, `OP_DIV, `OP_EI,
    `OP_MUL, `OP_NOP, `OP_NOTC, `OP_PCALL, `OP_POP, `OP_PUSH, `OP_RET, `OP_RETI,
    `OP_SETC, `OP_SETP, `OP_TAX, `OP_TAY, `OP_TCALL, `OP_TSX, `OP_TXA, `OP_TXS,
    `OP_TYA, `OP_XCN:
      ;
    `OP_ADC, `OP_AND, `OP_CMP, `OP_CPX, `OP_CPY, `OP_EOR, `OP_MOV, `OP_OR,
    `OP_SBC, `OP_LDA, `OP_LDX, `OP_LDY: begin
      if (am == `AM_DPDP || am == `AM_IPIP || am == `AM_IMMDP) begin
        do_alu(op, m, m2, m);
        if (op != `OP_CMP) begin
          dor <= m;
          rw_int <= 0;
          wait_clk();
        end
        else if (ir == `CMP_DPDP || ir == `CMP_IPIP) begin
          int_op <= 1'b1;
          wait_clk();
        end
      end
      else if (ir == `MOV_A_XINDP) begin
        int_op <= 1'b1;
        wait_clk();
      end
      else
        -> do_ins_dly;
    end
    `OP_ADDW: begin
      wait_clk();

      int_op <= 1'b1;
      tmp = ac + db;
      ac = tmp[7:0];
      `p_c = tmp[8];
      wait_clk();

      ab <= ea + 1;
      int_op <= 0;
      wait_clk();

      tmp = y + db + {7'b0, `p_c};
      hc = y[3:0] + db[3:0] + {3'b0, `p_c} > 5'd9;
      `p_n = tmp[7];
      `p_v = y[7] == db[7] && tmp[7] != y[7];
      `p_h = hc;
      y = tmp[7:0];
      `p_z = {y, ac} == 0;
      `p_c = tmp[8];
    end
    `OP_AND1: begin
      if (ir == `AND1N_C_ABB)
        m = ~m;
      `p_c = `p_c & m[abb_bit_sel];
    end
    `OP_CBNE: begin
      int_op <= 1'b1;
      wait_clk();

      ab <= pc;
      int_op <= 0;
      wait_clk();

      pc = pc + 1;
      ea[7:0] = db;
      ea[15:8] = {8{db[7]}};
      if (m != ac) begin
        branched = (ir != `BRA);
        wait_clk();
        {eac, pc[7:0]} = ea[7:0] + pc[7:0];

        wait_clk();
        pc[15:8] = pc[15:8] + ea[15:8] + {7'b0, eac};
      end
    end
    `OP_CMPW: begin :op_cmpw
    reg [7:0] ndb, tac;
      wait_clk();

      ndb = ~db;
      tmp = ac + ndb + 8'b1 /* borrow */;
      tac = tmp[7:0];
      `p_c = tmp[8];
      ab <= ea + 1;
      wait_clk();

      ndb = ~db;
      tmp = y + ndb + {7'b0, `p_c};
      `p_n = tmp[7];
      `p_z = {tmp[7:0], tac} == 0;
      `p_c = tmp[8];
    end
    `OP_DBNZ: begin
      if (ir == `DBNZ_DP) begin
        m = m - 1;
        dor <= m;
        rw_int <= 0;
        wait_clk();

        ab <= pc;
        rw_int <= 1;
        wait_clk();

        pc = pc + 1;
        ea[7:0] = db;
        ea[15:8] = {8{db[7]}};
        if (m != 0) begin
          branched = 1;
          wait_clk();
          {eac, pc[7:0]} = ea[7:0] + pc[7:0];

          wait_clk();
          pc[15:8] = pc[15:8] + ea[15:8] + {7'b0, eac};
        end
      end
    end
    `OP_LDC: begin
      `p_c = m[abb_bit_sel];
    end
    `OP_LSR, `OP_ROR, `OP_ASL, `OP_ROL, `OP_DEC, `OP_INC, `OP_DAA, `OP_DAS: begin
      if (am == `AM_A || am == `AM_IMPL)
        -> do_ins_dly;
      else begin
        do_alu(op, 8'hxx, m, m);
        dor <= m;
        rw_int <= 0;
        wait_clk();
      end
    end
    `OP_EOR1: begin
      wait_clk();
      `p_c = `p_c ^ m[abb_bit_sel];
    end
    `OP_INCW, `OP_DECW: begin
      wait_clk();

      tmp = (op == `OP_INCW) ? 1 : -1;
      tmp = db + tmp;
      rw_int <= 0;
      dor <= tmp[7:0];
      wait_clk();

      ab[7:0] <= ab[7:0] + 1;
      rw_int <= 1;
      wait_clk();

      `p_z = tmp[7:0] == 0;
      tmp = db + tmp[8] * ((op == `OP_INCW) ? 1 : -1);
      dor <= tmp[7:0];
      rw_int <= 0;
      wait_clk();

      `p_n = tmp[7];
      `p_z = `p_z & (tmp[7:0] == 0);
    end
    `OP_BBC, `OP_BBS: begin :op_bbc_bbs
    logic [2:0] bbcs_bit_sel;
    reg bbcs_bra_pred;
      int_op <= 1'b1;
      wait_clk();

      bbcs_bit_sel = ir[7:5];
      bbcs_bra_pred = m[bbcs_bit_sel] == (op == `OP_BBS);
      ab <= pc;
      int_op <= 0;
      wait_clk();

      ea[7:0] = db;
      ea[15:8] = {8{db[7]}};
      pc = pc + 1;

      if (bbcs_bra_pred) begin
        branched = 1;
        wait_clk();
        {eac, pc[7:0]} = ea[7:0] + pc[7:0];

        wait_clk();
        pc[15:8] = pc[15:8] + ea[15:8] + {7'b0, eac};
      end
    end
    `OP_JMP: begin
      pc = ea;
    end
    `OP_CALL: begin
      assert (ir === `CALL_ABS);
      assert (am == `AM_ABS);

      // Cycle 4 (T3)
      ab[15:8] <= 8'h01;
      ab[7:0] <= s;
      wait_clk();

      // Cycle 5 (T4)
      dor <= pc[15:8];
      rw_int <= 0;
      wait_clk();

      // Cycle 6 (T5)
      dor <= pc[7:0];
      ab[7:0] <= ab[7:0] - 1;
      wait_clk();

      // Cycle 7 (T6)
      rw_int <= 1;
      wait_clk();

      // Cycle 8 (T7)
      wait_clk();

      // Cycle 1 (T0) of the next instruction
      s = s - 2;
      pc = ea;
    end

    `OP_LDW: begin
      ac = m;
      int_op <= 1'b1;
      wait_clk();

      ab <= ea + 1;
      int_op <= 0;
      wait_clk();

      y = db;
      `p_n = y[7];
      `p_z = {y, ac} == 0;
    end
    `OP_NOT1: begin
      m[abb_bit_sel] ^= 1;
      dor <= m;
      rw_int <= 0;
      wait_clk();
    end
    `OP_OR1: begin
      wait_clk();
      if (ir == `OR1N_C_ABB)
        m = ~m;
      `p_c = `p_c | m[abb_bit_sel];
    end
    `OP_STA, `OP_STX, `OP_STY: begin
      m = op == `OP_STA ? ac : (op == `OP_STX ? x : y);
      dor <= m;
      int_op <= 1'b0;
      wait_clk();
      rw_int <= 0;
      wait_clk();
    end
    `OP_STC: begin
      wait_clk();
      m[abb_bit_sel] = `p_c;
      dor <= m;
      rw_int <= 0;
      wait_clk();
    end
    `OP_SET1, `OP_CLR1: begin
      m[ir[7:5]] = (op == `OP_SET1);
      dor <= m;
      rw_int <= 0;
      wait_clk();
    end
    `OP_STI: begin
      dor <= m2;
      rw_int <= 0;
      wait_clk();
    end
    `OP_STW: begin
      wait_clk();
      m = ac;
      dor <= m;
      rw_int <= 0;

      wait_clk();
      m = y;
      dor <= m;
      ab <= ea + 1;

      wait_clk();
    end
    `OP_SUBW: begin :op_subw
    reg [7:0] ndb;
      wait_clk();

      int_op <= 1'b1;
      ndb = ~db;
      tmp = ac + ndb + 8'b1 /* borrow */;
      ac = tmp[7:0];
      `p_c = tmp[8];
      wait_clk();

      ab <= ea + 1;
      int_op <= 0;
      wait_clk();

      ndb = ~db;
      tmp = y + ndb + {7'b0, `p_c};
      hc = y[3:0] + ndb[3:0] + {3'b0, `p_c} > 5'd9;
      `p_n = tmp[7];
      `p_v = y[7] == ndb[7] && tmp[7] != y[7];
      `p_h = hc;
      y = tmp[7:0];
      `p_z = {y, ac} == 0;
      `p_c = tmp[8];
    end
    `OP_TCLR1, `OP_TSET1: begin
      wait_clk();
      do_alu(op, ac, m, m);
      dor <= m;
      rw_int <= 0;
      wait_clk();
    end
    default: $finish; // shouldn't get here
  endcase
endtask

// These instrutions execute during the next instruction fetch cycle.
always @do_ins_dly begin
  ir_d = ir;
  op_d = op;
  am_d = am;

  wait_clk();

  case (op_d)
    `OP_ADC, `OP_AND, `OP_EOR, `OP_OR, `OP_SBC: begin
      do_alu(op_d, ac, m, ac);
    end
    `OP_ASL, `OP_LSR, `OP_ROL, `OP_ROR: begin
      do_alu(op_d, 8'hxx, m, ac);
    end
    `OP_CMP, `OP_CBNE: begin
      do_alu(`OP_CMP, ac, m, m);
    end
    `OP_CPX: begin
      do_alu(`OP_CMP, x, m, m);
    end
    `OP_CPY: begin
      do_alu(`OP_CMP, y, m, m);
    end
    `OP_DAA, `OP_DAS: begin
      // m is invalid, because am == `AM_IMPL.
      do_alu(op_d, 8'hxx, ac, ac);
    end
    `OP_DEC: begin
      case (ir_d)
        `DEC_A: begin
          do_alu(op_d, 8'hxx, ac, ac);
        end
        `DEC_X: begin
          do_alu(op_d, 8'hxx, x, x);
        end
        `DEC_Y: begin
          do_alu(op_d, 8'hxx, y, y);
        end
        default:
          $finish; // shouldn't get here
      endcase
    end
    `OP_INC: begin
      case (ir_d)
        `INC_A: begin
          do_alu(op_d, 8'hxx, ac, ac);
        end
        `INC_X: begin
          do_alu(op_d, 8'hxx, x, x);
        end
        `INC_Y: begin
          do_alu(op_d, 8'hxx, y, y);
        end
        default:
          $finish; // shouldn't get here
      endcase
    end
    `OP_LDA: begin
      do_alu(`OP_LDR, 8'hxx, m, ac);
    end
    `OP_LDX: begin
      do_alu(`OP_LDR, 8'hxx, m, x);
    end
    `OP_LDY: begin
      do_alu(`OP_LDR, 8'hxx, m, y);
    end
    default: $finish; // shouldn't get here
  endcase
end

task do_alu
  (
    input [7:0]  op,
    input [7:0]  a,
    input [7:0]  b,
    output [7:0] o
   );
  case (op)
    `OP_ADC: begin
      tmp = a + b + {7'b0, `p_c};
      hc = a[3:0] + b[3:0] + {3'b0, `p_c} > 5'd9;
      `p_z = tmp[7:0] == 0;
      `p_n = tmp[7];
      `p_v = a[7] == b[7] && tmp[7] != a[7];
      `p_c = tmp[8];
      `p_h = hc;
      o = tmp[7:0];
    end
    `OP_AND: begin
      o = a & b;
      `p_z = o == 0;
      `p_n = o[7];
    end
    `OP_ASL: begin
      tmp = {b[7:0], 1'b0};
      `p_n = tmp[7];
      `p_z = tmp[7:0] == 0;
      `p_c = tmp[8];
      o = tmp[7:0];
    end
    `OP_CMP: begin
      tmp = {1'b0, a};
      b = ~b;
      tmp = tmp + b + 9'b1;
      `p_n = tmp[7];
      `p_z = tmp[7:0] == 0;
      `p_c = tmp[8];
    end
    `OP_DAA: begin
      // Ref: https://board.zsnes.com/phpBB3/viewtopic.php?p=65020
      tmp = {1'b0, b};
      if (`p_c || tmp > 9'h99) begin
        tmp = tmp + 9'h60;
        `p_c = 1'b1;
      end
      if (`p_h || tmp[3:0] > 4'h9)
        tmp = tmp + 9'h6;
      `p_z = tmp[7:0] == 0;
      `p_n = tmp[7];
      o = tmp[7:0];
    end
    `OP_DAS: begin
      tmp = {1'b0, b};
      if (~`p_c || tmp > 9'h99) begin
        tmp = tmp - 9'h60;
        `p_c = 1'b0;
      end
      if (~`p_h || tmp[3:0] > 4'h9)
        tmp = tmp - 9'h6;
      `p_z = tmp[7:0] == 0;
      `p_n = tmp[7];
      o = tmp[7:0];
    end
    `OP_DEC: begin
      tmp = b - 1;
      `p_n = tmp[7];
      `p_z = tmp[7:0] == 0;
      o = tmp[7:0];
    end
    `OP_EOR: begin
      o = a ^ b;
      `p_z = o == 0;
      `p_n = o[7];
    end
    `OP_INC: begin
      tmp = b + 1;
      `p_n = tmp[7];
      `p_z = tmp[7:0] == 0;
      o = tmp[7:0];
    end
    `OP_LDR: begin
      o = b;
      `p_n = o[7];
      `p_z = o == 0;
    end
    `OP_LSR: begin
      tmp[7:0] = {1'b0, b[7:1]};
      `p_n = tmp[7];
      `p_z = tmp[7:0] == 0;
      `p_c = b[0];
      o = tmp[7:0];
    end
    `OP_MOV: begin
      o = b;
    end
    `OP_OR: begin
      o = a | b;
      `p_n = o[7];
      `p_z = o == 0;
    end
    `OP_ROL: begin
      tmp = {b[7:0], `p_c};
      `p_n = tmp[7];
      `p_z = tmp[7:0] == 0;
      `p_c = tmp[8];
      o = tmp[7:0];
    end
    `OP_ROR: begin
      tmp = {b[0], `p_c, b[7:1]};
      `p_n = tmp[7];
      `p_z = tmp[7:0] == 0;
      `p_c = tmp[8];
      o = tmp[7:0];
    end
    `OP_SBC: begin
      b = ~b;
      tmp = a + b + {7'b0, `p_c};
      hc = a[3:0] + b[3:0] + {3'b0, `p_c} > 5'd9;
      `p_z = tmp[7:0] == 0;
      `p_n = tmp[7];
      `p_v = a[7] == b[7] && tmp[7] != a[7];
      `p_c = tmp[8];
      `p_h = hc;
      o = tmp[7:0];
    end
    `OP_TCLR1, `OP_TSET1: begin
      // Z and N flags set same as CMP A,!a
      tmp = {1'b0, a + ~b} + 9'b1;
      `p_n = tmp[7];
      `p_z = tmp[7:0] == 0;
      if (op == `OP_TCLR1)
        o = b & ~a;
      else
        o = b | a;
    end
    default: begin
      $display("do_alu: op='h%x", op);
      $finish(); // shouldn't get here
    end
  endcase
endtask

reg print_ins_enable;
initial print_ins_enable = 0;
task print_ins();
  if (print_ins_enable)
    $display("[sim] %t: ($%4X) A=%2X X=%2X Y=%2X S=%2X P=%2X %s", $time, pc, ac, x, y, s, p, opcode_st[ir]);
endtask

// S-SMP Peripheral Registers @ $F0-$FF (except $F2-$F3)

reg [7:0]  spcr;                // $F1: I/O Control
reg [7:0]  cpui [0:3];          // $F4-$F7: 4x system CPU I/O ports
reg [7:0]  spt0, spt1, spt2;    // $FA-$FC: Timer 0-2 Divider
wire [3:0] spc0, spc1, spc2;    // $FD-$FF: Timer 0-2 Output

assign p_sel = ab[15:4] == 'h00f && ab[3:1] != 'b001;

initial begin
  //spcr = 0;
  //spt0 = 0;
  //spt1 = 0;
  spt2 = 0;
end

always @(posedge CLK) begin
  if (resp) begin
    spcr <= 8'hb0;
    spt0 <= 8'hff;
    spt1 <= 8'hff;
    spt2 <= 8'hff;
  end
  else begin
    spcr[5:4] <= 2'b0; // self-clearing

    if (cken_posedge & p_sel & ~rw_int) begin
      case (A[3:0])
        'h1: spcr <= dor;
        'hA: spt0 <= dor;
        'hB: spt1 <= dor;
        'hC: spt2 <= dor;
        default: ;
      endcase
    end
  end
end

always @* begin
  ipdb = 8'h00;
  if (p_sel)
    case (A[3:0])
      'h4, 'h5, 'h6, 'h7: ipdb = cpui[ab[1:0]];
      'hD: ipdb = {4'b0, spc0};
      'hE: ipdb = {4'b0, spc1};
      'hF: ipdb = {4'b0, spc2};
      default: ;
    endcase
end

// Writing $F1 bits 4-5 to 1 clears pairs of input port buffers.
wire cpui01_clr = spcr[4];
wire cpui23_clr = spcr[5];

// For debugging in Icarus Verilog
wire [7:0] cpui0 = cpui[0];
wire [7:0] cpui1 = cpui[1];
wire [7:0] cpui2 = cpui[2];
wire [7:0] cpui3 = cpui[3];

always @(posedge cpui01_clr or posedge cpui23_clr) begin
  if (cpui01_clr) begin
    cpui[0] <= 8'b0;
    cpui[1] <= 8'b0;
  end
  if (cpui23_clr) begin
    cpui[2] <= 8'b0;
    cpui[3] <= 8'b0;
  end
end

// Timers are cleared following non-internal read
reg [2:0] spcc;
always @* begin
  spcc = 3'b0;
  if (p_sel & rw_int & ~int_op) begin
    case (A[3:0])
      'hD: spcc[0] = 'b1;
      'hE: spcc[1] = 'b1;
      'hF: spcc[2] = 'b1;
      default: ;
    endcase
  end
end

// Divide-by-N timer clock enables

wire      clken_64k, clken_8k;
reg [6:0] divcnt;

initial begin
  divcnt = 1; // align timer with DUT
end

always @(posedge CLK) if (cken_posedge & !hold) begin
  if (clken_8k)
    divcnt <= 0;
  else
    divcnt <= divcnt + 1'd1;
end

assign clken_64k = divcnt[3:0] == 4'd15; // (CLK & CKEN) / 16 = 64kHz
assign clken_8k = divcnt == 7'd127;      // (CLK & CKEN) / 128 = 8kHz

s_smp_sp_timer timer0 (.CLK(CLK), .CLKEN(cken_posedge), .TICK(clken_8k), .EN(spcr[0]), .CLR(spcc[0]), .T(spt0), .C(spc0), .MON_WS(0));
s_smp_sp_timer timer1 (.CLK(CLK), .CLKEN(cken_posedge), .TICK(clken_8k), .EN(spcr[1]), .CLR(spcc[1]), .T(spt1), .C(spc1), .MON_WS(0));
s_smp_sp_timer timer2 (.CLK(CLK), .CLKEN(cken_posedge), .TICK(clken_64k), .EN(spcr[2]), .CLR(spcc[2]), .T(spt2), .C(spc2), .MON_WS(0));

endmodule
