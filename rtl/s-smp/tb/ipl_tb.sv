// Reset S-SMP and boot from IPL ROM

`timescale 1us / 1ns

module ipl_tb();

reg        nRES;
reg        clk;

wire [15:0] a, MA;
wire [7:0]  do_smp, do_dsp;
wire [7:0]  mdo_dsp, do_ram;
wire        cken;
wire        rw;
wire        nCE, nWE, nOE;

initial begin
  $timeformat(-6, 0, " us", 1);

`ifndef VERILATOR
  $dumpfile("ipl_tb.vcd");
  $dumpvars();
`else
  $dumpfile("ipl_tb.verilator.vcd");
  //$dumpvars();
`endif
end

s_smp dut
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken),
   .HOLD(1'b0),
   .A(a),
   .DI(do_dsp),
   .DO(do_smp),
   .RW(rw),
   .MON_SEL(),
   .MON_READY(1'b0),
   .MON_WS(1'b0),
   .MON_DOUT(),
   .MON_DIN()
   );

s_dsp_sim dsp_sim
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken),
   .A(a),
   .DI(do_smp),
   .DO(do_dsp),
   .RW(rw),
   .MA(MA),
   .MDI(do_ram),
   .MDO(mdo_dsp),
   .nCE(nCE),
   .nWE(nWE),
   .nOE(nOE),
   .SCLKEN(),
   .PSOUTL(),
   .PSOUTR()
   );

ram ram(.CLK(clk), .A(MA), .DI(mdo_dsp), .DO(do_ram), .nCE(nCE), 
        .nWE(nWE), .nOE(nOE));

initial begin
  clk = 1;
  nRES = 0;
end

initial forever begin :clkgen
  #(500.0/6144.0) clk = ~clk;
end

initial begin
  #10 nRES = 1;

  #2400 ;

  $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -DS_DSP_SIM_NOAUDIO -s ipl_tb -f s_dsp_sim_noaudio.files -o ipl_tb.vvp ../sp_timer.v ../s_smp.v ram.sv ipl_tb.sv && ./ipl_tb.vvp"
// End:
