`timescale 1us / 1ns

module s_dsp_sim_brr
  (
   input                nRES,

   input                KON,
   input [7:0]          DIR,
   input [7:0]          SCRN,
   input [2:0]          VOICE,

   input [2:0]          NEXT,
   output               PLAY,
   output               BEND,
   output signed [14:0] OUT0,
   output signed [14:0] OUT1,
   output signed [14:0] OUT2,
   output signed [14:0] OUT3
   );

reg dec_en, dec_loaded;
wire dec_active;

// BRR block state machine

reg [15:0] scrn0, scrn1;
reg [15:0] scrnc;
reg [7:0]  brrbuf [0:1];        // 2*2 samples
reg [3:0]  bbcnt;

reg [3:0]  left_shift;
reg [1:0]  dec_flt;
reg        loop, bend;

initial begin
  scrn0 = 0;
  scrn1 = 0;
  scrnc = scrn0;
  dec_en = 1'b1;
end

assign PLAY = dec_loaded;
assign BEND = PLAY & bend & ~loop;

// SCRNx (BRR pointer) loader

reg       scrn0_load, scrn1_load;
reg [15:2] ste;
reg [15:0] adir;

initial begin
  scrn0_load = 'b0;
  scrn1_load = 'b0;
end

task load_ptr;
  ste = {DIR, 6'b0} + {6'b0, SCRN};
  adir = {ste, 2'b00};

  if (scrn0_load) begin
    adir[1:0] = 2'd0;
    sram.read(adir, scrn0[7-:8]);
    adir[1:0] = 2'd1;
    sram.read(adir, scrn0[15-:8]);

    scrnc = scrn0;
    bbcnt = 4'd1;

    scrn0_load = 0;
    scrn1_load = 0;
  end
  else if (scrn1_load) begin
    adir[1:0] = 2'd2;
    sram.read(adir, scrn1[7-:8]);
    adir[1:0] = 2'd3;
    sram.read(adir, scrn1[15-:8]);

    scrnc = scrn1;
    bbcnt = 4'd1;

    scrn0_load = 0;
    scrn1_load = 0;
  end
endtask

// BRR buffer loader

initial begin
  bbcnt = 1;
  brrbuf[0] = 8'h00;
  brrbuf[1] = 8'h00;
end

task load_header_s1;
  if (dec_loaded)
    intcnt = (intcnt + NEXT) % 12;

  sram.read(scrnc, {left_shift, dec_flt, loop, bend});

  if (dec_active) begin
    sram.read(scrnc + 16'(bbcnt), brrbuf[0]);
    bbcnt = bbcnt + 1'd1;
  end
endtask

task load_s2;
  if (KON) begin
    scrn0_load = 1'b1;
    decode_reset();
  end
  else if (dec_active) begin
    sram.read(scrnc + 16'(bbcnt), brrbuf[1]);
    bbcnt = bbcnt + 1'd1;

    decode();

    if (bbcnt == 4'd9) begin
      bbcnt = 4'd1;
      if (bend)
        scrn1_load = 1'b1;
      else
        scrnc = scrnc + 16'd9;
    end
  end
endtask

// BRR shifter

reg signed [14:0] brrss;        // shifted sample
reg signed [3:0]  brrs;         // encoded sample
reg [1:0]         sp;           // pointer within BRR buffer

task shift;
  brrs = brrbuf[sp[1]][(7 - 4 * sp[0])-:4];

  if (left_shift <= 12)
    brrss = 15'((signed'(16'(brrs)) << left_shift) >>> 1);
  else
    brrss = 15'(signed'({brrs[3], 11'b0}));
endtask

// BRR filter

reg signed [16:0] brrfs;
reg signed [14:0] brrfsm1, brrfsm2;

function [16:0] samp_muldiv(input signed [14:0] x, input int m, input int d);
  samp_muldiv = 17'((24'(x) * m) / d);
endfunction

task filter;
  brrfs = 17'(brrss);
  brrfsm1 = decbuf[(deccnt + 11) % 12];
  brrfsm2 = decbuf[(deccnt + 10) % 12];
  case (dec_flt)
    2'd0: ;                     // the "non-filter"
    2'd1: brrfs += samp_muldiv(brrfsm1, 15, 16);
    2'd2: brrfs += samp_muldiv(brrfsm1, 61, 32) - samp_muldiv(brrfsm2, 15, 16);
    2'd3: brrfs += samp_muldiv(brrfsm1, 115, 64) - samp_muldiv(brrfsm2, 13, 16);
    default: brrfs = 17'dx;
  endcase

  if (brrfs > 17'sh7fff)
    brrfs = 17'sh7fff;
  else if (brrfs < -17'sh8000)
    brrfs = -17'sh8000;

/* -----\/----- EXCLUDED -----\/-----
  if (VOICE == 3'd7)
    $display("brrs=%d ls=%d brrss=%d brrfs=%d ", brrs, left_shift, brrss, brrfs);
 -----/\----- EXCLUDED -----/\----- */
endtask

// Decoded sample ring buffer

reg [14:0] decbuf [0:11];
reg [3:0]  deccnt;
reg [3:0]  intcnt;
wire       dec_buf_full;

initial begin :init0
int i;
  deccnt = 0;
  for (i = 0; i < 12; i++)
    decbuf[i] = 0;
  decode_reset();
  dec_loaded = 0;
end

task decode_reset;
  deccnt = 0;
  intcnt = 0;
  dec_loaded = 0;
endtask

task decode;
int i;
  for (i = 0; i < 4; i++) begin
    sp = i[1:0];
    shift();
    filter();

    decbuf[deccnt] = brrfs[14:0]; // truncate very -ve / +ve values

    deccnt = (deccnt + 1) % 12;

    if (~dec_loaded && deccnt == intcnt)
      dec_loaded = 1'b1;
  end
endtask

assign dec_buf_full = dec_loaded & (deccnt[3:2] == intcnt[3:2]);
assign dec_active = dec_en & ~dec_buf_full;

// Sample output to interpolator

assign OUT0 = decbuf[(intcnt + 0) % 12];
assign OUT1 = decbuf[(intcnt + 1) % 12];
assign OUT2 = decbuf[(intcnt + 2) % 12];
assign OUT3 = decbuf[(intcnt + 3) % 12];

endmodule
