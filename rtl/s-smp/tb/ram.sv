`timescale 1us / 1ns

module ram
  (
   input        CLK,
   input        nCE,
   input        nWE,
   input        nOE,
   input [15:0] A,
   input [7:0]  DI,
   output [7:0] DO
   );

reg [7:0] ram [0:16'hffff];
reg [7:0] dor;

/* -----\/----- EXCLUDED -----\/-----
initial begin
integer fin;
  fin = $fopen("ram.bin", "r");
  read_from_file(fin);
  $fclose(fin);
end
 -----/\----- EXCLUDED -----/\----- */

task read_from_file(input integer fin);
integer code;
  code = $fread(ram, fin);
endtask

task write_to_file(input integer fout);
int i;
  for (i = 0; i < $size(ram); i++)
    $fwrite(fout, "%c", ram[i]);
endtask

always @(posedge CLK)
  dor <= ~(nCE | nOE) ? ram[A] : 8'hxx;

assign DO = dor;

always @(negedge CLK) begin
  if (~(nCE | nWE)) begin
    //$display("ram[%x] <= %x", A, D);
    ram[A] <= DI;
  end
end

endmodule
