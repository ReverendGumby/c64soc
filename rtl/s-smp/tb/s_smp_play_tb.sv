// Test S-SMP against its own simulation
// Run code in song.spc
// Disassembly: https://www.ff6hacking.com/ff5wiki/Squall/SPC_disassembly.s

`timescale 1us / 1ns

module s_smp_play_tb();

reg        nRES;
reg        clk;
reg        hold;
reg        check = 0;
reg        sync_d;
reg        cken_d;

wire [15:0] a, a_sim, MA;
wire [7:0]  do_smp, do_smp_sim, do_dsp;
wire [7:0]  mdo_dsp, do_ram;
wire        cken, cken_hg;
wire        cken_sim;
wire        rw, rw_sim;
wire        nCE, nWE, nOE;

initial begin
  $timeformat(-6, 0, " us", 1);

`ifndef VERILATOR
  $dumpfile("s_smp_play_tb.vcd");
  $dumpvars();
`else
  $dumpfile("s_smp_play_tb.verilator.vcd");
  //$dumpvars();
`endif
end

/* verilator lint_off PINMISSING */
s_smp dut
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken_hg),
   .HOLD(1'b0),
   .A(a),
   .DI(do_dsp),
   .DO(do_smp),
   .RW(rw),
   .MON_SEL(),
   .MON_READY(1'b0),
   .MON_WS(1'b0),
   .MON_DOUT(),
   .MON_DIN()
   );
/* verilator lint_on PINMISSING */

s_smp_sim smp_sim
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken_sim),
   .A(a_sim),
   .DI(do_dsp),
   .DO(do_smp_sim),
   .RW(rw_sim)
   );

s_dsp_sim dsp_sim
  (
   .nRES(nRES),
   .CLK(clk),
   .CKEN(cken),
   .A(a),
   .DI(do_smp),
   .DO(do_dsp),
   .RW(rw),
   .MA(MA),
   .MDI(do_ram),
   .MDO(mdo_dsp),
   .nCE(nCE),
   .nWE(nWE),
   .nOE(nOE),
   .SCLKEN(),
   .PSOUTL(),
   .PSOUTR()
   );

ram ram(.CLK(clk), .A(MA), .DI(mdo_dsp), .DO(do_ram), .nCE(nCE), 
        .nWE(nWE), .nOE(nOE));

initial begin
  clk = 1;
  nRES = 1;
  hold = 1;
end

initial forever begin :clkgen
  #(500.0/6144.0) clk = ~clk;
end

always @(posedge clk)
  cken_d <= cken;
assign cken_hg = cken & ~hold;
assign cken_sim = cken & ~cken_d;

always @(posedge clk) if (cken_hg)
  sync_d <= smp_sim.sync;

wire sync_negedge = sync_d & ~smp_sim.sync;

task read_from_file(input string path);
integer fin, code;
reg [7:0] tmp [0:1];
  fin = $fopen(path, "r");
  assert(fin != 0) else $finish;

  code = $fseek(fin, 'h25, 0);
  code = $fread(tmp, fin, 0, 2);
  smp_sim.pc = {tmp[1], tmp[0]};
  code = $fread(smp_sim.ac, fin);
  code = $fread(smp_sim.x, fin);
  code = $fread(smp_sim.y, fin);
  code = $fread(smp_sim.p, fin);
  code = $fread(smp_sim.s, fin);
  smp_sim.ab = smp_sim.pc;

  code = $fseek(fin, 'h100, 0);
  ram.read_from_file(fin);
  dsp_sim.rf.read_from_file(fin);

  $fclose(fin);
endtask

task write_to_file(input string path);
integer fout;
reg [15:0] tmp_pc;
reg [7:0]  tmp;
  fout = $fopen(path, "w");
  assert(fout != 0) else $finish;

  tmp = 0;
  while ($ftell(fout) != 'h25)
    $fwrite(fout, "%c", tmp);
  tmp_pc = smp_sim.pc - 1;
  $fwrite(fout, "%c%c%c%c%c%c%c", 
          tmp_pc[7:0] , tmp_pc[15:8],
          smp_sim.ac, smp_sim.x, smp_sim.y, smp_sim.p, smp_sim.s);

  while ($ftell(fout) != 'h100)
    $fwrite(fout, "%c", tmp);
  ram.write_to_file(fout);
  dsp_sim.rf.write_to_file(fout);

  $fclose(fout);
endtask

//initial #9 nRES = 1;
initial begin
  read_from_file("song.spc");
  //read_from_file("temp.spc");

  dut.pc = smp_sim.pc;
  dut.ac = smp_sim.ac;
  dut.x = smp_sim.x;
  dut.y = smp_sim.y;
  dut.p = smp_sim.p;
  dut.s = smp_sim.s;
  dut.aord = smp_sim.ab;

  smp_sim.cpui[0] = ram.ram['hf4];
  smp_sim.cpui[1] = ram.ram['hf5];
  smp_sim.cpui[2] = ram.ram['hf6];
  smp_sim.cpui[3] = ram.ram['hf7];
  smp_sim.spt0 = ram.ram['hfa];
  smp_sim.spt1 = ram.ram['hfb];
  smp_sim.spt2 = ram.ram['hfc];
  smp_sim.spcr = ram.ram['hf1] & ~8'h30;

  dut.cpui[0] = smp_sim.cpui[0];
  dut.cpui[1] = smp_sim.cpui[1];
  dut.cpui[2] = smp_sim.cpui[2];
  dut.cpui[3] = smp_sim.cpui[3];
  dut.spt0 = smp_sim.spt0;
  dut.spt1 = smp_sim.spt1;
  dut.spt2 = smp_sim.spt2;
  dut.spcr = smp_sim.spcr;

  smp_sim.print_ins_enable = 1;
  //dsp_sim.rf.print_reg_enable = 1;

  repeat (3) @(posedge cken) ;
  hold = 0;

  @(posedge clk) check = 1;

`ifndef VERILATOR
  //#1000000 @(negedge smp_sim.sync) write_to_file("temp.spc");
  #1000000;
`else
  repeat (240)
    #1000000 @(negedge smp_sim.sync) write_to_file("temp.spc");
  //repeat (120) #1000000;
`endif

  $finish;
end

event err;

always @(negedge clk) if (check & cken_hg) begin

  assert (smp_sim.sync === dut.sync)
    else begin
      $display("**** assert SYNC @%t: sim %1b != dut %1b", $time, smp_sim.sync, dut.sync);
      -> err;
    end
  assert (rw_sim === rw)
    else begin
      $display("**** assert RW @%t: sim %1b != dut %1b", $time, rw_sim, rw);
      -> err;
    end
  assert (a_sim === a)
    else begin
      $display("**** assert A @%t: sim %04x != dut %04x", $time, a_sim, a);
      -> err;
    end
  assert (rw || do_smp_sim === do_smp)
    else begin
      $display("**** assert DO @%t: sim %02x != dut %02x", $time, do_smp_sim, do_smp);
      -> err;
    end

  if (sync_negedge) begin
    assert (smp_sim.ac === dut.ac)
      else begin
        $display("**** assert ac @%t: sim %8b != dut %8b", $time, smp_sim.ac, dut.ac);
        -> err;
      end
    assert (smp_sim.x === dut.x)
      else begin
        $display("**** assert x @%t: sim %8b != dut %8b", $time, smp_sim.x, dut.x);
        -> err;
      end
    assert (smp_sim.y === dut.y)
      else begin
        $display("**** assert y @%t: sim %8b != dut %8b", $time, smp_sim.y, dut.y);
        -> err;
      end
    assert (smp_sim.s === dut.s)
      else begin
        $display("**** assert s @%t: sim %8b != dut %8b", $time, smp_sim.s, dut.s);
        -> err;
      end
    assert (smp_sim.p === dut.p)
      else begin
        $display("**** assert p @%t: sim %8b != dut %8b", $time, smp_sim.p, dut.p);
        -> err;
      end
  end
end

always @err begin
  check = 0;
  #20 $finish;
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -DS_DSP_SIM_NOAUDIO -s s_smp_play_tb -f s_dsp_sim_noaudio.files -o s_smp_play_tb.vvp ../sp_timer.v ../s_smp.v s_smp_sim.sv ram.sv s_smp_play_tb.sv && ./s_smp_play_tb.vvp"
// End:
