module s_smp_sp_timer
(
 input         CLK,
 input         CLKEN,
 input         TICK,
 input         EN,
 input         CLR,
 input [7:0]   T,
 output [3:0]  C,

 output [12:0] MON_DOUT,
 input         MON_WS,
 input [12:0]  MON_DIN
 );

reg [7:0] ti;
reg [3:0] c;
reg       en_d;

initial begin
  c = 0;
  ti = 0;
end

wire en_negedge = !EN && en_d;

always @(posedge CLK) begin
  if (CLKEN) begin
    en_d <= EN;

    if (en_negedge || CLR)
      c <= 0;
    if (en_negedge)
      ti <= 0;

    if (TICK) begin
      if (EN) begin
        if (ti + 1 == T) begin
          ti <= 0;
          c <= c + 1;
        end
        else
          ti <= ti + 1;
      end
    end
  end

  if (MON_WS) begin
    en_d <= MON_DIN[0];
    ti <= MON_DIN[8:1];
    c <= MON_DIN[12:9];
  end
end

assign C = c;

assign MON_DOUT[0] = en_d;
assign MON_DOUT[8:1] = ti;
assign MON_DOUT[12:9] = c;

endmodule

