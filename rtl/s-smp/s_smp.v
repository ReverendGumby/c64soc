// S-SMP (Sony SPC700), as accurate as needed to make decent sound.

/* verilator lint_off WIDTH */
/* verilator lint_off SIDEEFFECT */

module s_smp
  (
   input             nRES, // reset
   input             CLK, // clock
   input             CKEN, // clock enable (from DSP)
   input             HOLD, // clock stall (emulator control)
   output [15:0]     A, // address bus
   input [7:0]       DI, // data bus, input
   output [7:0]      DO, // data bus, output
   output            RW, // read / not write,

   // Unwrapped I/O interface
   input [7:0]       CPUI0, // CPU -> APU
   input [7:0]       CPUI1,
   input [7:0]       CPUI2,
   input [7:0]       CPUI3,
   input [3:0]       CPUIN,
   output [7:0]      CPUO0, // APU -> CPU
   output [7:0]      CPUO1,
   output [7:0]      CPUO2,
   output [7:0]      CPUO3,
   output [3:0]      CPUON,

   // debug monitor
   input [7:0]       MON_SEL,
   output reg        MON_ACK,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,
   input             MON_WS,
   input [31:0]      MON_DIN
   );

// General notes:
//
// We assume that CKEN has a duty cycle < 100%, and take
// advantage of the time when it's low to do extra work.
//
// Opcodes TODO: PCALL, RETI, BRK, CLRV, SLEEP, STOP

`define p_c p[0]
`define p_z p[1]
`define p_i p[2]
`define p_h p[3]
`define p_b p[4]
`define p_p p[5]                // direct page
`define p_v p[6]
`define p_n p[7]

reg [15:0] pc, npc, aor, aord, a_o, ab;
reg [7:0]  ir, dl, dor, dord, ipdb;
reg [7:0]  ac, x, y, z, p, s;
reg [7:0]  db, rlb, dsb;
reg [7:0]  ai, bi, ibi, co, bsr;
reg [2:0]  bsr_pos, mulscnt;
reg [7:0]  ico;
reg [16:8] icoh;
wire [7:0] dph;
reg        cken_d, cp1, cp1d, cp2;
reg        addc, notbi, pdah, pdal, pdac, cco, cvo, cho;

reg [7:0]  pd;
reg        pd_am_nostart;

reg        sync;
reg        ins_onebyte;
reg        ins_done;

reg [17:0] t;
reg [3:0]  bra_t;
reg        bra_p, bra_p_match;
reg        stall, stalled;
reg [3:0]  ts;
wire       t_reset;
reg        resp;

reg [15:0] am_t;
reg        am_start, am_done, am_done_d;
reg        am_impl, am_a, am_imm, am_abs, am_absx, am_absy, am_dp, am_dpx,
           am_dpy, am_rel, am_xiind, am_indyi, am_xind, am_xindp, am_dpdp,
           am_ipip, am_immdp, am_dpb, am_dpbr, am_abb, am_aindx;

reg [23:0] op_t;
reg        op_ins_done;
reg        op_bra;
reg [2:0]  op_bra_bits;
reg        op_pc_inc, op_store, op_rmw, op_a_out, op_set_nz, op_set16_nz;
reg        op_ac, op_x, op_y;
reg        op_asl, op_rol, op_lsr, op_ror, op_dec, op_inc;
reg        op_or, op_and, op_eor, op_mov, op_movw, op_adc, op_sbc, op_cmp,
           op_push, op_pop, op_call;
reg        op_math;

reg        cl_abl_pcl, cl_abh_pch, cl_pc_inc;
reg        cl_ab_aor, cl_abl_aorl, cl_abh_aorh, cl_zero_aoh765;
reg        cl_dl_db;
reg        cl_rlb_ac, cl_rlb_x, cl_rlb_y, cl_rlb_z, cl_rlb_s;
reg        cl_ir5_i;
reg        cl_rlb_p, cl_rlb7_n, cl_ir6_p, cl_rlbz_z, cl_rlbz_dsbz_z;
reg        cl_cco_c, cl_ir7_c, cl_rlbnz_c;
reg        cl_cho_h;
reg        cl_cvo_v, cl_ir7_v, cl_ico8_v;
reg [3:0]  cl_rlb_rs;
reg [3:0]  cl_dsb_rs;
reg [3:0]  cl_abl_rs, cl_abh_rs;
reg [3:0]  cl_ai_rs, cl_bi_rs;
reg        cl_aorh765_bsr_pos, cl_ir765_bsr_pos, cl_tcall_val_bsr;
reg        cl_sums_cco, cl_carry, cl_one_addc, cl_c_addc, cl_bi_not,
           cl_bi_dah, cl_bi_dal, cl_pdas;
reg        cl_clrs, cl_sums, cl_ors, cl_ands, cl_eors, cl_asls, cl_rols,
           cl_lsrs, cl_rors;
reg        cl_muls, cl_mulss, cl_xcns, cl_divss, cl_divcs;
reg [3:0]  cl_co_lsr;
reg        cl_store_dor, cl_store_dor_d;
reg        cl_int_op, cl_int_op_d;

wire       rw_int, int_op, p_sel, db_oe;
wire       sync_next;
wire       bra_skip;

wire       ipl_sel;
wire [5:0] ipl_a;
wire [7:0] ipl_do;
wire       ipl_en;

wire       mon_write;


//////////////////////////////////////////////////////////////////////
// Clocking

initial begin
  cken_d = 0;
  cp2 = 0;
  t = 'b1000;
  am_t = 'b0;
  stall = 'b0;
  stalled = 'b0;
  resp = 1'b0;
end

always @* cp1 = CKEN & ~cken_d & ~HOLD;
always @* cp2 = ~CKEN & cken_d & ~HOLD;

always @(posedge CLK) begin
  if (~HOLD) begin
    cken_d <= CKEN;
    cp1d <= cp1;
  end

  if (mon_write & (MON_SEL == 8'h10)) begin
    cken_d <= MON_DIN[0];
    cp1d <= MON_DIN[1];
  end
end

always @(posedge CLK) begin
  if (cp1) begin
    resp <= ~nRES;
  end

  if (mon_write & (MON_SEL == 8'h10)) begin
    resp <= MON_DIN[2];
  end
end

// Timing should reset to where stall resumes: just after sync deasserts.
assign t_reset = mon_write & (MON_SEL == 8'h06) & |MON_DIN[7:4];

// Instruction cycle counter
always @(posedge CLK) begin
  if (~stalled & (cp1 | cp2)) begin
    if (resp)
      t <= 'b100;
    else if (ins_done)
      t <= 'b1;
    else
      t <= t << 1;
  end

  if (t_reset)
    t <= 'b100;
  if (mon_write & (MON_SEL == 8'h11))
    t <= MON_DIN[17:0];
end

// Address mode cycle counter (instruction phase 1)
always @(posedge CLK) begin
  if (~stalled & (cp1 | cp2)) begin
    am_done_d <= am_done & ~am_start;

    if (resp | am_start)
      am_t <= 'b1;
    else if (am_done_d)
      am_t <= 'b0;
    else
      am_t <= am_t << 1;
  end

  if (t_reset)
    am_t <= 'b1;
  else if (mon_write & (MON_SEL == 8'h10)) begin
    am_done_d <= MON_DIN[3];
  end
  else if (mon_write & (MON_SEL == 8'h12)) begin
    am_t <= MON_DIN[15:0];
  end
end

// Opcode cycle counter (instruction phase 2)
always @(posedge CLK) begin
  if (~stalled & (cp1 | cp2)) begin
    if (resp)
      op_t <= 'b0;
    else if (am_done | (t[1] & pd_am_nostart))
      op_t <= 'b1;
    else if (t[1])
      op_t <= 'b0;
    else
      op_t <= op_t << 1;
  end

  if (t_reset)
    op_t <= 'b0;
  else if (mon_write & (MON_SEL == 8'h13)) begin
    op_t <= MON_DIN[23:0];
  end
end

// Timing control
always @(posedge CLK) begin
  if (cp1) begin
    if (stall & ~stalled) begin
      if (~|t | sync)
        stalled <= 1'b1;
    end
    else if (~stall & stalled) begin
      stalled <= 1'b0;
    end
  end

  if (mon_write & (MON_SEL == 8'h06))
    stall <= MON_DIN[0];
end

// Encode t for monitor
always @* begin
  case (t)
    18'b0:              ts = 4'd0;
    18'b1 << 0:         ts = 4'd1;
    18'b1 << 1:         ts = 4'd2;
    18'b1 << 2:         ts = 4'd3;
    18'b1 << 3:         ts = 4'd4;
    18'b1 << 4:         ts = 4'd5;
    18'b1 << 5:         ts = 4'd6;
    18'b1 << 6:         ts = 4'd7;
    18'b1 << 7:         ts = 4'd8;
    18'b1 << 8:         ts = 4'd9;
    18'b1 << 9:         ts = 4'd10;
    18'b1 << 10:        ts = 4'd11;
    18'b1 << 11:        ts = 4'd12;
    18'b1 << 12:        ts = 4'd13;
    18'b1 << 13:        ts = 4'd14;
    default:            ts = 4'd15;
  endcase
end


//////////////////////////////////////////////////////////////////////
// Memory interface

assign db_oe = ~rw_int;
always @* begin
  if (p_sel)
    db = ipdb;
  else if (ipl_sel)
    db = ipl_do;
  else
    db = DI;
end

assign A = a_o;
assign RW = rw_int/* || resg*/;
assign DO = db_oe ? dor : 8'hxx;


//////////////////////////////////////////////////////////////////////
// Registers

// pc: program ("P") counter
always @(posedge CLK) begin
  if (~HOLD & resp) begin
    // Shortcut: skip the (IPL ROM) reset vector
    pc <= 16'hFFC0;
  end
  else if (~stalled & (cp1 | cp2))
    pc <= npc + cl_pc_inc;

  if (mon_write & (MON_SEL == 8'h01))
    pc <= MON_DIN[15:0];
end

always @* begin
  npc = pc;
  if (cl_abl_pcl)
    npc[7:0] = ab[7:0];
  if (cl_abh_pch)
    npc[15:8] = ab[15:8];
end

// aor: address bus output register / transparent latch
// latch opens for 1x CLK following CKEN
always @* begin
  aor = aord;
  if (cp1d) begin
    if (cl_abl_aorl)
      aor[7:0] = ab[7:0];
    else if (cl_abh_aorh)
      aor[15:8] = ab[15:8];
    else if (cl_ab_aor)
      aor = ab;
  end
end

always @* begin
  a_o = aor;
  if (cl_zero_aoh765)
    a_o[15:13] = 3'b0;
end

always @(posedge CLK) begin
  if (cp1d) begin
    aord <= aor;
  end

  if (mon_write & (MON_SEL == 8'h04))
    aord <= MON_DIN[23:8];
end

// dor: data output register / transparent latch
// latch opens for 1x CLK following CKEN
always @* begin
  dor = dord;
  if (cp1d) begin
    dor = dsb;
  end
end

always @(posedge CLK) begin
  if (cp1d) begin
    dord <= dor;
  end

  if (mon_write & (MON_SEL == 8'h04))
    dord <= MON_DIN[31:24];
end

// dl: input data latch
always @(posedge CLK) begin
  if (cp2) begin
    if (cl_dl_db)
      dl <= db;
  end

  if (mon_write & (MON_SEL == 8'h12))
    dl <= MON_DIN[23:16];
end

// ac: accumulator
always @(posedge CLK) begin
  if (cp1 | cp2) begin
    if (cl_rlb_ac)
      ac <= rlb;
  end

  if (mon_write & (MON_SEL == 8'h02))
    ac <= MON_DIN[15:8];
end

// x: index register
always @(posedge CLK) begin
  if (cp1 | cp2) begin
    if (cl_rlb_x)
      x <= rlb;
  end
  
  if (mon_write & (MON_SEL == 8'h02))
    x <= MON_DIN[23:16];
end

// y: index register
always @(posedge CLK) begin
  if (cp1 | cp2) begin
    if (cl_rlb_y)
      y <= rlb;
  end

  if (mon_write & (MON_SEL == 8'h02))
    y <= MON_DIN[31:24];
end

// z: internal register
always @(posedge CLK) begin
  if (cp1 | cp2) begin
    if (cl_rlb_z)
      z <= rlb;
  end

  if (mon_write & (MON_SEL == 8'h05))
    z <= MON_DIN[7:0];
end

// s: stack pointer
always @(posedge CLK) begin
  if (cp1 | cp2) begin
    if (cl_rlb_s)
      s <= rlb;
  end

  if (mon_write & (MON_SEL == 8'h02))
    s <= MON_DIN[7:0];
end

// p: processor status flags
always @(posedge CLK) begin
  if (~HOLD & resp) begin
    `p_p <= 1'b0; // IPL assumes this is clear
  end
  else if (cp1 | cp2) begin
    if (cl_rlb_p)
      p <= rlb;

    if (cl_rlb7_n)
      `p_n <= rlb[7];

    if (cl_cvo_v)
      `p_v <= cvo;
    if (cl_ir7_v)
      `p_v <= ir[7];
    if (cl_ico8_v)
      `p_v <= icoh[8];

    if (cl_ir6_p)
      `p_p <= ir[6];

    if (cl_cho_h)
      `p_h <= cho;

    if (cl_ir5_i)
      `p_i <= ir[5];

    if (cl_rlbz_z)
      `p_z <= ~|rlb;
    if (cl_rlbz_dsbz_z)
      `p_z <= ~|{rlb, dsb};

    if (cl_cco_c)
      `p_c <= cco;
    if (cl_ir7_c)
      `p_c <= ir[7];
    if (cl_rlbnz_c)
      `p_c <= |rlb;
  end

  if (mon_write & (MON_SEL == 8'h01))
    p <= MON_DIN[23:16];
end

// dph: direct page, high address
assign dph = {7'b0, `p_p};


//////////////////////////////////////////////////////////////////////
// Register selection

localparam [3:0] rs_0   = 4'd0; // all zeros
localparam [3:0] rs_ac  = 4'd1;
localparam [3:0] rs_x   = 4'd2;
localparam [3:0] rs_y   = 4'd3;
localparam [3:0] rs_p   = 4'd4;
localparam [3:0] rs_s   = 4'd5;
localparam [3:0] rs_pcl = 4'd6;
localparam [3:0] rs_pch = 4'd7;
localparam [3:0] rs_z   = 4'd8;
localparam [3:0] rs_dl  = 4'd9;
localparam [3:0] rs_dor = 4'd10;
localparam [3:0] rs_co  = 4'd11;
localparam [3:0] rs_aol = 4'd12; // aor low
localparam [3:0] rs_aoh = 4'd13; // aor high
localparam [3:0] rs_dph = 4'd14; // direct page
localparam [3:0] rs_bsr = 4'd15;

`define regmux(rs, q) \
\
always @* \
  case (rs) \
    rs_0:   q = 0; \
    rs_ac:  q = ac; \
    rs_x:   q = x; \
    rs_y:   q = y; \
    rs_p:   q = p; \
    rs_s:   q = s; \
    rs_pcl: q = pc[7:0]; \
    rs_pch: q = pc[15:8]; \
    rs_z:   q = z; \
    rs_dl:  q = dl; \
    rs_dor: q = dord; \
    rs_co:  q = co; \
    rs_aol: q = aord[7:0]; \
    rs_aoh: q = aord[15:8]; \
    rs_dph: q = dph; \
    rs_bsr: q = bsr; \
    default:q = 8'hxx; \
  endcase \


//////////////////////////////////////////////////////////////////////
// Internal buses

// rlb: register load bus
`regmux (cl_rlb_rs, rlb);

// dsb: data store bus
`regmux (cl_dsb_rs, dsb);

// ab: address bus
`regmux (cl_abl_rs, ab[7:0]);
`regmux (cl_abh_rs, ab[15:8]);


//////////////////////////////////////////////////////////////////////
// ALU

function [19:0] muls(input reg [16:0] mac,
                     input reg [7:0]  ai, input reg [7:0] bi,
                     input reg [2:0]  mulscnt, input reg start);
reg bs;
reg [16:0] prd;
  begin
    if (start)
      mulscnt = 0;
    bs = |(bi & (1 << (3'd7 - mulscnt)));
    prd = start ? 0 : (mac << 1);
    prd = prd + ai * bs;
    mulscnt = mulscnt + 1;
    muls = {prd, mulscnt};
  end
endfunction

function [16:0] divs(input reg [16:0] yva, input reg [7:0] ai);
reg [16:0] tx;
  begin
    tx = {ai, 9'b0};
    yva = {yva[15:0], yva[16]};
    if (yva >= tx)
      yva = yva ^ 1;
    if (yva[0])
      yva = yva - tx;
    divs = yva;
  end
endfunction

// Inputs
`regmux (cl_ai_rs, ai);
`regmux (cl_bi_rs, bi);
always @* addc = (cl_carry & cco) | cl_one_addc | (cl_c_addc & `p_c);
always @* notbi = cl_bi_not;
always @* pdah = (`p_c ^ cl_pdas) | (ai > 8'h99);
always @* pdal = (`p_h ^ cl_pdas) | (ai[3:0] > 4'h9);
always @* pdac = (~cl_pdas & (`p_c | pdah)) | (cl_pdas & `p_c & ~pdah);

always @* begin
  ibi = bi;
  // DAA/DAS adjust constants
  if (cl_bi_dah & pdah)
    ibi = 8'h60;
  if (cl_bi_dal & pdal)
    ibi = 8'h06;
  ibi = ibi ^ {8{notbi}};
end

// Maths
always @(posedge CLK) begin
  if (cp1 | cp2) begin
    if (cl_sums) begin :sums
    reg [4:0] hsum, lsum;
      lsum = ai[3:0] + ibi[3:0] + {3'b0, addc};
      hsum = ai[7:4] + ibi[7:4] + {3'b0, lsum[4]};
      ico <= {hsum[3:0], lsum[3:0]};
      if (cl_sums_cco)
        cco <= (cl_bi_dah) ? pdac : hsum[4];
      cvo <= (ai[7] == ibi[7]) & (ai[7] != hsum[3]);
      cho <= lsum > 5'd9;
    end
    else if (cl_ors)
      ico <= ai | ibi;
    else if (cl_ands)
      ico <= ai & ibi;
    else if (cl_eors)
      ico <= ai ^ ibi;
    else if (cl_asls | cl_rols)
      {cco, ico} <= {ai[7:0], addc & cl_rols};
    else if (cl_lsrs | cl_rors)
      {ico, cco} <= {addc & cl_rors, ai[7:0]};
    else if (cl_muls)
      {icoh, ico, mulscnt} <= muls({icoh, ico}, ai, bi, mulscnt, cl_mulss);
    else if (cl_xcns)
      ico <= {ai[3:0], ai[7:4]};
    else if (cl_divss)
      {icoh, ico} <= {1'b0, ai, bi};
    else if (cl_divcs)
      {icoh, ico} <= divs({icoh, ico}, ai);
  end

  if (mon_write & (MON_SEL == 8'h10)) begin
    cco <= MON_DIN[4];
    cvo <= MON_DIN[5];
    cho <= MON_DIN[6];
  end
  else if (mon_write & (MON_SEL == 8'h14)) begin
    ico <= MON_DIN[7:0];
    icoh <= MON_DIN[16:8];
    mulscnt <= MON_DIN[19:17];
  end  
end

// Output shifter
always @*
  co = {icoh, ico} >> cl_co_lsr;

// Bit select (and other stuff) register
always @* begin
  bsr_pos = 0;
  if (cl_aorh765_bsr_pos)
    bsr_pos = aord[15:13];
  if (cl_ir765_bsr_pos)
    bsr_pos = ir[7:5];

  bsr = 1 << bsr_pos;

  if (cl_tcall_val_bsr)
    bsr = {3'b110, ~ir[7:4], 1'b0};
end

//////////////////////////////////////////////////////////////////////
// Branch decision

always @* begin
  casez (op_bra_bits)
    3'b000: bra_p = `p_n;       // BPL / BMI
    3'b001: bra_p = `p_v;       // BVC / BVS
    3'b010: bra_p = `p_c;       // BCC / BCS
    3'b011: bra_p = `p_z;       // BNE / BEQ
    3'b10z: bra_p = |co;        // CBNE / DBNZ, BBS / BBC
    3'b111: bra_p = 1'b1;       // BRA
    default: bra_p = 1'bx;
  endcase
end

always @* begin
  case (op_bra_bits[2])
    1'b0: bra_p_match = ir[5];
    1'b1: bra_p_match = op_bra_bits[0];
    default: bra_p_match = 1'bx;
  endcase
end

assign bra_skip = op_bra & (bra_p ^ bra_p_match);


//////////////////////////////////////////////////////////////////////
// Instruction decode and control logic

initial begin
  sync = 0;
  ir = 8'h00; // NOP
  bra_t = 0;
  cl_store_dor_d = 0;
end

always @* ins_done = resp | op_ins_done | bra_skip | bra_t[3];
assign sync_next = ins_done;

always @* pd = db;              // pre-decode opcode

always @* begin
  // pd_am_nostart for empty addressing modes, ie. that are done at
  // t[1]. Needs to be valid when pd holds opcode.
  pd_am_nostart = 0;

  if (sync) begin
    casez (pd)
      // am_impl
      8'bzzz00000, 8'bzzzz0001, 8'b11z11100, 8'bzzz11101, 8'b0zz01101, 8'b11101101,
      8'b10zz1110, 8'b11z01110, 8'b0z001111, 8'b011z1111, 8'b11zz1111,
      // am_a
      8'b0zz11100, 8'b10z11100, 8'b10011111,
      // am_imm
      8'b10001101, 8'b10101101, 8'b11001101, 8'bzzz01000,
      // am_rel
      8'bzzz10000, 8'b00101111:
        pd_am_nostart = 'b1;
      default: ;
    endcase
  end
end

always @(posedge CLK) begin
  if (cp1) begin
    sync <= sync_next;
  end

  if (mon_write & (MON_SEL == 8'h07))
    sync <= MON_DIN[3];
end

always @(posedge CLK) begin
  if (~HOLD & resp) begin
    ir <= 8'h00 /* NOP */;
  end
  else if (cp1) begin
    if (sync)
      ir <= pd;
  end

  if (mon_write & (MON_SEL == 8'h04))
    ir <= MON_DIN[7:0];
end

always @(posedge CLK) begin
  if (cp1 | cp2) begin
    bra_t[0] <= op_bra & ~bra_skip;
    bra_t[3:1] <= bra_t[2:0];
  end

  if (mon_write & (MON_SEL == 8'h11))
    bra_t <= MON_DIN[21:18];
end

always @* begin
  am_impl = 0;                  // implied
  am_a = 0;                     // accumulator = A
  am_imm = 0;                   // immediate = #i
  am_abs = 0;                   // absolute = !a
  am_absx = 0;                  // X-indexed absolute = !a+X
  am_absy = 0;                  // Y-indexed absolute = !a+Y
  am_dp = 0;                    // direct page = d
  am_dpx = 0;                   // X-indexed direct page = d+X
  am_dpy = 0;                   // Y-indexed direct page = d+Y
  am_rel = 0;                   // relative
  am_xiind = 0;                 // X-indexed indirect = [d+X]
  am_indyi = 0;                 // indirect Y-indexed = [d]+Y
  am_xind = 0;                  // indirect = (X)
  am_xindp = 0;                 // indirect auto-increment = (X)+
  am_dpdp = 0;                  // direct page to direct page = dd,ds
  am_ipip = 0;                  // indirect page to indirect page = (X),(Y)
  am_immdp = 0;                 // immediate to direct page = #i,d
  am_dpb = 0;                   // direct page bit = d.b
  am_dpbr = 0;                  // direct page bit relative = d.b, r
  am_abb = 0;                   // absolute boolean bit = m.b
  am_aindx = 0;                 // absolute X-indexed indirect = [!a+X]

  casez (ir)
    8'bzzz10000: am_rel = 'b1;
    8'bzzzz0010: am_dpb = 'b1;
    8'bzzzz0011: am_dpbr = 'b1;
    8'bzzz10100: am_dpx = 'b1;
    8'bzzz00100: am_dp = 'b1;
    8'bzzz00101: am_abs = 'b1;
    8'bzzz10101: am_absx = 'b1;
    8'bzzz00110: am_xind = 'b1;
    8'bzzz10110: am_absy = 'b1;
    8'bzzz00111: am_xiind = 'b1;
    8'bzzz10111: am_indyi = 'b1;
    8'bzzz01000: am_imm = 'b1;
    8'b0zz11000, 8'b10z11000: am_immdp = 'b1;
    8'b11z11000: am_dp = 'b1;
    8'b0zz01001, 8'b10z01001, 8'b11111010: am_dpdp = 'b1;
    8'b0zz11001, 8'b10z11001: am_ipip = 'b1;
    8'b11z01001: am_abs = 'b1;
    8'b11z11001: am_dpy = 'b1;
    8'b0zz11010, 8'b10z11010, 8'b11011010: am_dp = 'b1;
    8'bzzz01010: am_abb = 'b1;
    8'bzzz01011: am_dp = 'b1;
    8'bzzz11011: am_dpx = 'b1; 
    8'bzzz01100: am_abs = 'b1;
    8'b0zz11100, 8'b10z11100: am_a = 'b1;
    8'b10001101, 8'b10101101, 8'b11001101: am_imm = 'b1;
    8'b0z001110, 8'b0z011110: am_abs = 'b1;
    8'b00111110, 8'b01111110: am_dp = 'b1;
    8'b11011110: am_dpx = 'b1;
    8'b0z101110: am_dp = 'b1;
    8'b11111110: am_rel = 'b1;
    8'b00011111: am_aindx = 'b1;
    8'b00101111: am_rel = 'b1;
    8'b00111111, 8'b01011111: am_abs = 'b1;
    8'b10001111: am_immdp = 'b1;
    8'b10011111: am_a = 'b1;
    8'b10101111, 8'b10111111: am_xindp = 'b1;
    default: am_impl = 'b1;
  endcase
end

always @* begin
  // am_start aligns with cp1 of sync, so am_t[0] is on the first cycle
  // after the opcode.
  am_start = ~pd_am_nostart & t[1];

  // am_done aligns with cp2 before A=EA, so op_t[0] is on the first
  // cycle where data is available.
  am_done = (am_abs & t[5]) |
            (am_absx & t[7]) |
            (am_absy & t[7]) |
            (am_dp & t[3]) |
            (am_dpx & t[5]) |
            (am_dpy & t[5]) |
            (am_xiind & t[9]) |
            (am_indyi & t[9]) |
            (am_xind & t[3]) |
            (am_xindp & t[3]) |
            (am_dpdp & t[7]) |
            (am_ipip & t[5]) |
            (am_immdp & t[5]) |
            (am_dpb & t[3]) |
            (am_dpbr & t[7]) |
            (am_abb & t[5]) |
            (am_aindx & t[9]);
end

always @* ins_onebyte = am_impl | am_xind | am_xindp | am_ipip | am_a;

always @* begin
  op_ac = 0;
  op_x = 0;
  op_y = 0;

  casez (ir)
    8'bzzzz01zz, 8'b0zz01000, 8'b1z101000, 8'b10001000, 
    8'b0zz11100, 8'b10101110, 8'b00101101, 8'b10z11100, 
    8'b101z1111:                                        op_ac = 'b1;
    8'b110z1000, 8'b11111000, 8'b11001110,
    8'b00z11110, 8'b11zz1001, 8'b01001101, 8'b11001101, 
    8'b00z11101:                                        op_x = 'b1;
    8'b11zz1100, 8'b11101110, 8'b01z11110, 8'b01101101, 
    8'b10z01101, 8'b11zz1011:                           op_y = 'b1;
    default: ;
  endcase
end

always @* begin
  op_asl = 0;
  op_rol = 0;
  op_lsr = 0;
  op_ror = 0;
  op_dec = 0;
  op_inc = 0;

  casez (ir)
    8'b000z1011, 8'b000z1100:                           op_asl = 'b1;
    8'b001z1011, 8'b001z1100:               			op_rol = 'b1;
    8'b010z1011, 8'b010z1100:               			op_lsr = 'b1;
    8'b011z1011, 8'b011z1100:               			op_ror = 'b1;
    8'b100z1011, 8'b100z1100, 8'h1D, 8'hDC: 			op_dec = 'b1;
    8'b101z1011, 8'b101z1100, 8'h3D, 8'hFC: 			op_inc = 'b1;
    default: ;
  endcase
end

always @* begin
  op_or = 0;
  op_and = 0;
  op_eor = 0;
  op_mov = 0;
  op_movw = 0;
  op_adc = 0;
  op_sbc = 0;
  op_cmp = 0;
  op_push = 0;
  op_pop = 0;
  op_call = 0;

  casez (ir)
    8'b000z01zz, 8'b000z100z, 8'b00z01010:              op_or = 'b1;
    8'b001z01zz, 8'b001z100z, 8'b01z01010:              op_and = 'b1;
    8'b010z01zz, 8'b010z100z, 8'h8A:                    op_eor = 'b1;
    8'b11zz01zz, 8'b11zz10z1, 8'b11z01100, 8'b11z11000,
    8'b11101000, 8'b1z001101, 8'b101z1111:              op_mov = 'b1;
    8'b10111010, 8'b11011010:                           op_movw = 'b1;
    8'b100z01zz, 8'b100z100z:                           op_adc = 'b1;
    8'b101z01zz, 8'b101z100z:                           op_sbc = 'b1;
    8'b011z01zz, 8'b011z100z, 8'hC8, 8'hAD, 8'b0zz11110:op_cmp = 'b1;
    8'b0zz01101:                                        op_push = 'b1;
    8'b1zz01110:                                        op_pop = 'b1;
    8'b00111111:                                        op_call = 'b1;
    default: ;
  endcase
end

always @* op_math = (op_asl | op_rol | op_lsr | op_ror |
                     op_dec | op_inc | op_and | op_or | op_eor |
                     op_adc | op_sbc | op_cmp);

always @* begin
  op_store = 0;
  op_rmw = 0;

  casez (ir)
    8'b110z01zz, 8'b110110z0, 8'b110z10z1, 8'b11001100,
    8'b11011100, 8'b10101111:                           op_store = 'b1;
/* -----\/----- EXCLUDED -----\/-----
    8'bzzzz0010, 8'bz0101000, 8'bz0001000, 8'b01001000,
 -----/\----- EXCLUDED -----/\----- */
    8'bzzzz0010, 8'bz0z1100z, 8'b0101100z, 8'bz0z01001,
    8'b01z01001, 8'b11z01010, 8'b0zzz1011,
    8'b10zz1011, 8'b0zz01100, 8'b10z01100, 8'b0z001110: op_rmw = 'b1;
    default: ;
  endcase
end

always @(posedge CLK) begin
  if (cp1 | cp2) begin
    cl_store_dor_d <= cl_store_dor;
    cl_int_op_d <= cl_int_op;
  end

  if (mon_write & (MON_SEL == 8'h10)) begin
    cl_store_dor_d <= MON_DIN[7];
    cl_int_op_d <= MON_DIN[8];
  end
end

always @* cl_pc_inc = t[0] | (t[2] & ~ins_onebyte) | op_pc_inc;
always @* cl_ab_aor = op_a_out | cl_pc_inc | (t[2] & ins_onebyte);
always @* cl_rlb7_n = op_set_nz | op_set16_nz;
always @* cl_rlbz_z = op_set_nz;
always @* cl_rlbz_dsbz_z = op_set16_nz;

always @* begin
  op_ins_done = 0;
  op_bra = 0;
  op_bra_bits = 0;
  op_pc_inc = 0;
  op_a_out = 0;
  op_set_nz = 0;
  op_set16_nz = 0;
  cl_abl_pcl = 0;
  cl_abh_pch = 0;
  cl_abl_aorl = 0;
  cl_abh_aorh = 0;
  cl_zero_aoh765 = 0;
  cl_dl_db = 1'b1;
  cl_rlb_ac = 0;
  cl_rlb_x = 0;
  cl_rlb_y = 0;
  cl_rlb_z = 0;
  cl_rlb_s = 0;
  cl_ir5_i = 0;
  cl_rlb_p = 0;
  cl_ir6_p = 0;
  cl_cco_c = 0;
  cl_ir7_c = 0;
  cl_rlbnz_c = 0;
  cl_cho_h = 0;
  cl_cvo_v = 0;
  cl_ir7_v = 0;
  cl_ico8_v = 0;
  cl_rlb_rs = rs_dl;
  cl_dsb_rs = 0;
  cl_abl_rs = rs_pcl;
  cl_abh_rs = rs_pch;
  cl_ai_rs = 0;
  cl_bi_rs = 0;
  cl_aorh765_bsr_pos = 0;
  cl_ir765_bsr_pos = 0;
  cl_tcall_val_bsr = 0;
  cl_sums_cco = 1'b1;
  cl_carry = 0;
  cl_one_addc = 0;
  cl_c_addc = 0;
  cl_bi_not = 0;
  cl_bi_dah = 0;
  cl_bi_dal = 0;
  cl_pdas = 0;
  cl_clrs = 0;
  cl_sums = 0;
  cl_ors = 0;
  cl_ands = 0;
  cl_eors = 0;
  cl_asls = 0;
  cl_rols = 0;
  cl_lsrs = 0;
  cl_rors = 0;
  cl_muls = 0;
  cl_mulss = 0;
  cl_xcns = 0;
  cl_divss = 0;
  cl_divcs = 0;
  cl_co_lsr = 0;
  cl_store_dor = 0;
  cl_int_op = 0;

  if (am_impl | am_imm) begin
  end
  else if (am_dp | am_dpdp | am_dpb | am_dpbr) begin
    if (am_t[2]) begin
      cl_abl_rs = rs_dl; cl_abh_rs = rs_dph; op_a_out = ~0;
    end
    if (am_dpdp) begin
      if (am_t[4]) begin
        cl_rlb_rs = rs_dl; cl_rlb_z = ~0;
        op_pc_inc = ~0;
      end
      if (am_t[6]) begin
        cl_abl_rs = rs_dl; cl_abh_rs = rs_dph; op_a_out = ~0;
      end
    end
  end
  else if (am_dpx) begin
    if (am_t[2]) begin
      cl_ai_rs = rs_dl; cl_bi_rs = rs_x; cl_sums = ~0;
    end
    if (am_t[4]) begin
      cl_abl_rs = rs_co; cl_abh_rs = rs_dph; op_a_out = ~0;
    end
  end
  else if (am_immdp) begin
    if (am_t[1]) begin
      cl_rlb_rs = rs_dl; cl_rlb_z = ~0;
    end
    if (am_t[2]) begin
      op_pc_inc = ~0;
    end
    if (am_t[4]) begin
      cl_abl_rs = rs_dl; cl_abh_rs = rs_dph; op_a_out = ~0;
    end
  end
  else if (am_xind) begin
    if (am_t[2]) begin
      cl_abl_rs = rs_x; cl_abh_rs = rs_dph; op_a_out = ~0;
    end
  end
  else if (am_xindp) begin
    if (am_t[2]) begin
      cl_abl_rs = rs_x; cl_abh_rs = rs_dph; op_a_out = ~0;
      cl_ai_rs = rs_x; cl_bi_rs = rs_0; {cl_one_addc, cl_sums} = ~0;
      cl_int_op = op_mov & op_store;
    end
    if (op_t[1]) begin
      cl_rlb_rs = rs_co;
      cl_rlb_x = ~0;
    end
  end
  else if (am_abs & ~op_call) begin
    if (am_t[2]) begin
      cl_ai_rs = rs_dl;
      cl_bi_rs = rs_0;
      {cl_sums} = ~0;
      op_a_out = ~0;
    end
    if (am_t[4]) begin
      cl_abl_rs = rs_co; cl_abh_rs = rs_dl; op_a_out = ~0;
      op_pc_inc = ~0;
    end
  end
  else if (am_absx | am_absy) begin
    if (am_t[2]) begin
      cl_ai_rs = rs_dl;
      cl_bi_rs = am_absx ? rs_x : rs_y;
      {cl_sums} = ~0;
      op_a_out = ~0;
    end
    if (am_t[4]) begin
      cl_rlb_rs = rs_co; cl_rlb_z = ~0;
      op_pc_inc = ~0;
    end
    if (am_t[5]) begin
      cl_ai_rs = rs_dl;
      cl_bi_rs = rs_0;
      {cl_carry, cl_sums} = ~0;
    end
    if (am_t[6]) begin
      cl_abl_rs = rs_z; cl_abh_rs = rs_co; op_a_out = ~0;
    end
  end
  else if (am_xiind) begin
    if (am_t[2]) begin
      cl_ai_rs = rs_dl; cl_bi_rs = rs_x; {cl_sums} = ~0;
    end
    if (am_t[4]) begin
      cl_abl_rs = rs_co; cl_abh_rs = rs_dph; op_a_out = ~0;
    end
    if (am_t[5]) begin
      cl_ai_rs = rs_aol; cl_bi_rs = rs_0; {cl_one_addc, cl_sums} = ~0;
    end
    if (am_t[6]) begin
      cl_rlb_rs = rs_dl; cl_rlb_z = ~0;
      cl_abl_rs = rs_co; cl_abh_rs = rs_dph; op_a_out = ~0;
    end
    if (am_t[8]) begin
      cl_abl_rs = rs_z; cl_abh_rs = rs_dl; op_a_out = ~0;
    end
  end
  else if (am_indyi) begin
    if (am_t[2]) begin
      cl_abl_rs = rs_dl; cl_abh_rs = rs_dph; op_a_out = ~0;
    end
    if (am_t[3]) begin
      cl_rlb_rs = rs_dl; cl_rlb_z = ~0;
      cl_ai_rs = rs_aol; cl_bi_rs = rs_0; {cl_one_addc, cl_sums} = ~0;
    end
    if (am_t[4]) begin
      cl_abl_rs = rs_co; cl_abh_rs = rs_dph; op_a_out = ~0;
    end
    if (am_t[5]) begin
      cl_ai_rs = rs_z; cl_bi_rs = rs_y; {cl_sums} = ~0;
    end
    if (am_t[6]) begin
      cl_rlb_rs = rs_co; cl_rlb_z = ~0;
      cl_ai_rs = rs_dl; cl_bi_rs = rs_0; {cl_carry, cl_sums} = ~0;
      cl_dl_db = 0;
    end
    if (am_t[8]) begin
      cl_abl_rs = rs_z; cl_abh_rs = rs_co; op_a_out = ~0;
    end
  end
  else if (am_abb) begin
    if (am_t[2]) begin
      cl_ai_rs = rs_dl;
      cl_bi_rs = rs_0;
      {cl_sums} = ~0;
      op_a_out = ~0;
    end
    if (am_t[4]) begin
      cl_abl_rs = rs_co; cl_abh_rs = rs_dl; op_a_out = ~0;
      {cl_zero_aoh765, cl_aorh765_bsr_pos} = ~0;
      op_pc_inc = ~0;
    end
  end
  else if (am_ipip) begin
    if (am_t[2]) begin
      cl_abl_rs = rs_y; cl_abh_rs = rs_dph; op_a_out = ~0;
    end
    if (am_t[4]) begin
      cl_rlb_rs = rs_dl; cl_rlb_z = ~0;
      cl_abl_rs = rs_x; cl_abh_rs = rs_dph; op_a_out = ~0;
    end
  end
  else if (am_aindx) begin
    if (am_t[2]) begin
      cl_ai_rs = rs_dl;
      cl_bi_rs = rs_x;
      {cl_sums} = ~0;
      op_a_out = ~0;
    end
    if (am_t[4]) begin
      cl_rlb_rs = rs_co; cl_rlb_z = ~0;
      op_pc_inc = ~0;
    end
    if (am_t[5]) begin
      cl_ai_rs = rs_dl;
      cl_bi_rs = rs_0;
      {cl_carry, cl_sums} = ~0;
    end
    if (am_t[6]) begin
      cl_abl_rs = rs_z; cl_abh_rs = rs_co; op_a_out = ~0;
    end
    if (am_t[7]) begin
      cl_ai_rs = rs_aol; cl_bi_rs = rs_0; {cl_one_addc, cl_sums} = ~0;
    end
    if (am_t[8]) begin
      // Address does not cross page boundary.
      cl_abl_rs = rs_co; cl_abh_rs = rs_aoh; op_a_out = ~0;
    end
  end

  // OR/AND/EOR/ADC/SBC/CMP/MOV A/X/Y, mem
  if (~(am_a | am_impl) & (op_ac | op_x | op_y) & ~op_store & (op_math | op_mov)) begin
    if (op_t[1]) begin
      cl_bi_rs = rs_dl;
      if (op_ac)        cl_ai_rs = rs_ac;
      if (op_x)         cl_ai_rs = rs_x;
      if (op_y)         cl_ai_rs = rs_y;
      if (op_mov)       cl_ai_rs = rs_0;

      if (op_or)        cl_ors = ~0;
      if (op_and)       cl_ands = ~0;
      if (op_eor)       cl_eors = ~0;
      if (op_adc)       {cl_c_addc, cl_sums} = ~0;
      if (op_sbc)       {cl_bi_not, cl_c_addc, cl_sums} = ~0;
      if (op_cmp)       {cl_bi_not, cl_one_addc, cl_sums} = ~0;
      if (op_mov)       cl_ors = ~0;
    end
    if (op_t[2]) begin
      cl_rlb_rs = rs_co;
      if (~op_cmp) begin
        if (op_ac)        cl_rlb_ac = ~0;
        if (op_x)         cl_rlb_x = ~0;
        if (op_y)         cl_rlb_y = ~0;
      end
      else
        cl_int_op = ~0;
      {op_set_nz} = ~0;
      if (op_adc | op_sbc | op_cmp) begin
        cl_cco_c = ~0;
      end;
      if (op_sbc | op_adc) begin
        {cl_cvo_v, cl_cho_h} = ~0;
      end
    end
    op_ins_done = am_xindp ? op_t[3] : op_t[1];
  end

  // OR/AND/EOR/ADC/SBC/CMP d, #i (rmw)
  // OR/AND/EOR/ADC/SBC/CMP (X), (Y) (rmw)
  // ASL/ROL/LSR/ROR/DEC/INC/AND/OR/EOR/CMP mem (rmw)
  if ((op_rmw | (op_cmp & (am_immdp | am_ipip))) & op_math) begin
    if (op_t[1]) begin
      cl_ai_rs = rs_dl;
      cl_bi_rs = (op_and | op_or | op_eor | op_adc | op_sbc | op_cmp) ? rs_z : rs_0;
      if (op_asl)       {cl_asls} = ~0;
      if (op_rol)       {cl_c_addc, cl_rols} = ~0;
      if (op_lsr)       {cl_lsrs} = ~0;
      if (op_ror)       {cl_c_addc, cl_rors} = ~0;
      if (op_dec)       {cl_bi_not, cl_sums} = ~0;
      if (op_inc)       {cl_one_addc, cl_sums} = ~0;
      if (op_and)       {cl_ands} = ~0;
      if (op_or)        {cl_ors} = ~0;
      if (op_eor)       {cl_eors} = ~0;
      if (op_adc)       {cl_c_addc, cl_sums} = ~0;
      if (op_sbc)       {cl_bi_not, cl_c_addc, cl_sums} = ~0;
      if (op_cmp)       {cl_bi_not, cl_one_addc, cl_sums} = ~0;
    end
    if (op_t[2]) begin
      cl_rlb_rs = rs_co;
      {op_set_nz} = ~0;
      if (op_asl | op_rol | op_lsr | op_ror | op_sbc | op_adc | op_cmp) begin
        cl_cco_c = ~0;
      end
      if (op_sbc | op_adc) begin
        {cl_cvo_v, cl_cho_h} = ~0;
      end
      if (~op_cmp) begin
        cl_dsb_rs = rs_co;
        cl_store_dor = ~0;
      end
      else
        cl_int_op = am_ipip;
    end
    op_ins_done = op_t[3];
  end

  // MOV mem, A/X/Y
  if (op_mov & op_store & (op_ac | op_x | op_y)) begin
    if (op_t[2]) begin
      if (op_ac)    cl_dsb_rs = rs_ac;
      if (op_x)     cl_dsb_rs = rs_x;
      if (op_y)     cl_dsb_rs = rs_y;
      cl_store_dor = ~0;
    end
    op_ins_done = op_t[3];
  end

  // MOVW YA, d / MOVW d, YA
  if (op_movw) begin
    if (op_t[0]) begin
      if (op_store)
        cl_dsb_rs = rs_ac;
    end
    if (op_t[2]) begin
      cl_ai_rs = rs_aol; cl_bi_rs = rs_0; {cl_one_addc, cl_sums} = ~0;
      if (op_store) begin
        cl_dsb_rs = rs_ac;
        cl_store_dor = ~0;
      end
      else begin
        cl_rlb_rs = rs_dl; cl_rlb_ac = ~0;
        cl_int_op = ~0;
      end
    end
    if (op_t[4]) begin
      cl_abl_rs = rs_co; cl_abh_rs = rs_dph; op_a_out = ~0;
      if (op_store) begin
        cl_dsb_rs = rs_y;
        cl_store_dor = ~0;
      end
    end
    if (op_t[5] & ~op_store) begin
      cl_rlb_rs = rs_dl; cl_rlb_y = ~0;
    end
    if (op_t[6] & ~op_store) begin
      cl_rlb_rs = rs_y; cl_dsb_rs = rs_ac; op_set16_nz = ~0;
    end
    op_ins_done = op_t[5];
  end

  // ASL/ROL/LSR/ROR/DEC/INC A/X/Y
  if ((am_a | am_impl) & ~op_rmw & (op_math & ~(op_adc | op_sbc))) begin
    if (op_t[1]) begin
      if (op_ac)        cl_ai_rs = rs_ac;
      if (op_x)         cl_ai_rs = rs_x;
      if (op_y)         cl_ai_rs = rs_y;
      cl_bi_rs = rs_0;
      if (op_lsr | op_ror)
        cl_c_addc = ~0;
      if (op_asl)       {cl_asls} = ~0;
      if (op_rol)       {cl_c_addc, cl_rols} = ~0;
      if (op_lsr)       {cl_lsrs} = ~0;
      if (op_ror)       {cl_c_addc, cl_rors} = ~0;
      if (op_dec)       {cl_bi_not, cl_sums} = ~0;
      if (op_inc)       {cl_one_addc, cl_sums} = ~0;
    end
    if (op_t[2]) begin
      cl_rlb_rs = rs_co;
      if (op_ac)        cl_rlb_ac = ~0;
      if (op_x)         cl_rlb_x = ~0;
      if (op_y)         cl_rlb_y = ~0;
      {op_set_nz} = ~0;
      if (op_asl | op_rol | op_lsr | op_ror) begin
        cl_cco_c = ~0;
      end        
    end
    op_ins_done = op_t[1];
  end

  // AND1/OR1/EOR1 C, (/)m.b
  if (am_abb & op_math) begin
    if (|op_t[3:1] & ~|t[1:0])
      {cl_zero_aoh765, cl_aorh765_bsr_pos} = ~0;
    if (op_t[1]) begin
      cl_ai_rs = rs_bsr; cl_bi_rs = rs_dl; cl_ands = ~0;
      cl_bi_not = ir[5];        // /m.b
    end
    if (op_t[2]) begin
      if (op_and) begin
        cl_ai_rs = rs_co; cl_bi_rs = rs_bsr; {cl_bi_not, cl_c_addc, cl_sums} = ~0;
        // co = m.b + 'hFE + p_c
      end
      if (op_or | op_eor) begin
        cl_rlb_rs = rs_co; cl_rlb_z = ~0;
        // Z = m.b
      end
    end
    if (op_t[3]) begin
      if (op_and) begin
        {cl_cco_c} = ~0;
      end
      if (op_or | op_eor) begin
        cl_ai_rs = rs_0; cl_bi_rs = rs_0; {cl_bi_not, cl_c_addc, cl_sums} = ~0;
        // co = 8{~p_c}
      end
    end
    if (op_t[4]) begin
      cl_ai_rs = rs_z; cl_bi_rs = rs_co; {cl_bi_not} = ~0;
      cl_ors = op_or;           // co = Z | 8{p_c}
      cl_sums = op_eor;         // co = Z + 8{p_c}
    end
    if (op_t[5]) begin
      if (op_or | op_eor) begin
        cl_rlb_rs = rs_co; {cl_rlbnz_c} = ~0;
      end
    end
    op_ins_done = (op_t[1] & op_and) | (op_t[3] & (op_or | op_eor));
  end

  // PUSH P/A/X/Y
  if (op_push) begin
    if (op_t[2]) begin
      cl_abl_rs = rs_s; cl_abh_rs = rs_bsr; op_a_out = ~0;
      if (op_ac)        cl_dsb_rs = rs_ac;
      else if (op_x)    cl_dsb_rs = rs_x;
      else if (op_y)    cl_dsb_rs = rs_y;
      else              cl_dsb_rs = rs_p;
      cl_store_dor = ~0;
    end
    if (op_t[4]) begin
      cl_ai_rs = rs_s; cl_bi_rs = rs_0; {cl_bi_not, cl_sums} = ~0;
    end
    if (op_t[5]) begin
      cl_rlb_rs = rs_co; cl_rlb_s = ~0;
    end
    op_ins_done = op_t[5];
  end

  // POP P/A/X/Y
  if (op_pop) begin
    if (op_t[2]) begin
      cl_abl_rs = rs_s; cl_abh_rs = rs_bsr; op_a_out = ~0;
    end
    if (op_t[3]) begin
      cl_ai_rs = rs_s; cl_bi_rs = rs_0; {cl_one_addc, cl_sums} = ~0;
    end
    if (op_t[4]) begin
      cl_abl_rs = rs_co; cl_abh_rs = rs_bsr; op_a_out = ~0;
      cl_rlb_rs = rs_co; cl_rlb_s = ~0;
    end
    if (op_t[6]) begin
      if (op_ac)        cl_rlb_ac = ~0;
      else if (op_x)    cl_rlb_x = ~0;
      else if (op_y)    cl_rlb_y = ~0;
      else              cl_rlb_p = ~0;
    end
    op_ins_done = op_t[5];
  end

  casez (ir)
    8'h00: begin                // NOP
      op_ins_done = t[3];
    end
    8'bzzz10000: begin          // BPL, BMI, ...
      op_bra = t[3];
      op_bra_bits = {1'b0, ir[7:6]};
    end
    8'hz2: begin                // SET1 / CLR1 d.b
      if (t[5]) begin
        cl_ai_rs = rs_dl;
        cl_bi_rs = rs_bsr;
        cl_ir765_bsr_pos = ~0;
        if (ir[4] == 1'b0)
          cl_ors = ~0;
        else
          {cl_bi_not, cl_ands} = ~0;
      end
      if (t[6]) begin
        cl_dsb_rs = rs_co;
        cl_store_dor = ~0;
      end
      op_ins_done = t[7];
    end
    8'hz3: begin                // BBS / BBC d.b
      if (t[6]) begin
        cl_ir765_bsr_pos = ~0;
        cl_ai_rs = rs_dl; cl_bi_rs = rs_bsr; cl_ands = ~0;
        cl_int_op = ~0; // verified
      end
      op_pc_inc = t[8];
      if (t[9]) begin
        op_bra = ~0;
        op_bra_bits = {2'b10, ~ir[4]};
      end
    end
    8'h0E, 8'h4E: begin         // TSET1/TCLR1 !a
      if (op_t[2]) begin
        cl_ai_rs = rs_ac; cl_bi_rs = rs_dl; {cl_bi_not, cl_one_addc, cl_sums} = ~0;
      end
      if (op_t[3]) begin
        cl_rlb_rs = rs_co; {op_set_nz} = ~0;
        cl_ai_rs = rs_dl; cl_bi_rs = rs_ac;
        if (ir == 8'h0E)
          {cl_ors} = ~0;
        else
          {cl_bi_not, cl_ands} = ~0;
      end
      if (op_t[4]) begin
        cl_dsb_rs = rs_co; cl_store_dor = ~0;
      end
      op_ins_done = op_t[5];
    end
    8'h1A, 8'h3A: begin         // INCW/DECW d
      if (op_t[1]) begin
        cl_ai_rs = rs_dl; 
        cl_bi_rs = rs_0;
        if (ir == 8'h1A)    cl_bi_not = ~0;
        else                cl_one_addc = ~0;
        cl_sums = ~0;                             
      end
      if (op_t[2]) begin
        cl_dsb_rs = rs_co;
        cl_store_dor = ~0;
        cl_rlb_rs = rs_co; cl_rlb_z = ~0;
      end
      if (op_t[3]) begin
        cl_ai_rs = rs_aol; cl_bi_rs = rs_0; {cl_one_addc, cl_sums} = ~0;
        cl_sums_cco = 0;
      end
      if (op_t[4]) begin
        cl_abl_rs = rs_co; cl_abh_rs = rs_dph; op_a_out = ~0;
      end
      if (op_t[5]) begin
        cl_ai_rs = rs_dl; 
        cl_bi_rs = rs_0;
        if (ir == 8'h1A)    cl_bi_not = ~0;
        {cl_carry, cl_sums} = ~0;
      end
      if (op_t[6]) begin
        cl_dsb_rs = rs_co;
        cl_store_dor = ~0;
      end
      if (op_t[7]) begin
        cl_rlb_rs = rs_co; cl_dsb_rs = rs_z; op_set16_nz = ~0;
      end
      op_ins_done = op_t[7];
    end
    8'h1F: begin                // JMP [!a+X]
      if (op_t[0]) begin
        cl_rlb_rs = rs_dl; cl_rlb_z = ~0;
        op_a_out = ~0;
      end
      if (op_t[1]) begin
        cl_abh_rs = rs_dl; cl_abh_pch = ~0;
        cl_abl_rs = rs_z; cl_abl_pcl = ~0;
      end
      op_ins_done = op_t[1];
    end
    8'h20, 8'h40: begin         // CLRP, SETP
      if (t[0]) begin
        {cl_ir6_p} = ~0;
      end
      op_ins_done = t[3];
    end
    8'h2E, 8'hDE: begin         // CBNE d(+X), rel
      if (op_t[1]) begin
        cl_ai_rs = rs_ac;
        cl_bi_rs = rs_dl;
        {cl_bi_not, cl_one_addc, cl_sums} = ~0;
      end
      cl_int_op = op_t[2];
      if (op_t[4]) begin
        op_pc_inc = ~0;
      end
      op_bra = op_t[5];
      op_bra_bits = 3'b101;
    end
    8'h2F: begin                // BRA rel
      op_bra = t[3];
      op_bra_bits = 3'b111;
    end
    8'h3F: begin                // CALL !a
      if (t[4]) begin
        cl_rlb_rs = rs_dl; cl_rlb_z = ~0;
        op_a_out = ~0;
      end
      if (t[5]) begin
        op_pc_inc = ~0;
      end
      if (t[6]) begin
        cl_abl_rs = rs_s; cl_abh_rs = rs_bsr; op_a_out = ~0;
      end
      if (t[8]) begin
        cl_dsb_rs = rs_pch;
        cl_store_dor = ~0;
        cl_ai_rs = rs_s; cl_bi_rs = rs_0; {cl_bi_not, cl_sums} = ~0;
      end
      if (t[9]) begin
        cl_rlb_rs = rs_co; cl_rlb_s = ~0;
      end
      if (t[10]) begin
        cl_abl_rs = rs_co; cl_abh_rs = rs_bsr; op_a_out = ~0;
        cl_dsb_rs = rs_pcl;
        cl_store_dor = ~0;
        cl_ai_rs = rs_s; cl_bi_rs = rs_0; {cl_bi_not, cl_sums} = ~0;
      end
      if (t[11]) begin
        cl_rlb_rs = rs_co; cl_rlb_s = ~0;
      end
      if (t[12]) begin
        cl_abh_rs = rs_dl; cl_abh_pch = ~0;
      end
      if (t[14]) begin
        cl_abl_rs = rs_z; cl_abl_pcl = ~0;
      end
      cl_dl_db = cl_dl_db & ~|t[13:6];
      op_ins_done = t[15];
    end
    8'h5A: begin                // CMPW YA, d
      if (op_t[1]) begin
        cl_ai_rs = rs_aol; cl_bi_rs = rs_0; {cl_one_addc, cl_sums} = ~0;
      end
      if (op_t[2]) begin
        cl_abl_rs = rs_co; cl_abh_rs = rs_dph; op_a_out = ~0;
        cl_ai_rs = rs_ac; cl_bi_rs = rs_dl; {cl_sums} = ~0;
        {cl_bi_not, cl_one_addc} = ~0;
      end
      if (op_t[3]) begin
        cl_rlb_rs = rs_co; cl_rlb_z = ~0;
        cl_ai_rs = rs_y; cl_bi_rs = rs_dl; {cl_carry, cl_sums} = ~0;
        {cl_bi_not} = ~0;
      end
      if (t[0]) begin
        cl_rlb_rs = rs_co;
        cl_dsb_rs = rs_z;
        {op_set16_nz, cl_cco_c} = ~0;
      end
      op_ins_done = op_t[3];
    end
    8'h5F: begin                // JMP !a
      if (t[4]) begin
        cl_rlb_rs = rs_dl; cl_rlb_z = ~0;
        op_a_out = ~0;
      end
      if (t[5]) begin
        //op_pc_inc = ~0;
        cl_abh_rs = rs_dl; cl_abh_pch = ~0;
        cl_abl_rs = rs_z; cl_abl_pcl = ~0;
      end
      op_ins_done = t[5];
    end
    8'h60, 8'h80: begin         // CLRC, SETC
      if (t[0]) begin
        cl_ir7_c = ~0;
      end
      op_ins_done = t[3];
    end
    8'h6E: begin                // DBNZ d, rel
      if (op_t[1]) begin
        cl_ai_rs = rs_dl; cl_bi_rs = rs_0; {cl_bi_not, cl_sums} = ~0;
      end
      else if (op_t[2]) begin
        cl_dsb_rs = rs_co;
        cl_store_dor = ~0;
      end
      else if (op_t[4]) begin
        op_pc_inc = ~0;
      end

      op_bra = op_t[5];
      op_bra_bits = 3'b101;
    end
    8'h6F: begin                    // RET
      if (t[4]) begin
        cl_abl_rs = rs_s; cl_abh_rs = rs_bsr; op_a_out = ~0;
      end
      if (t[5]) begin
        cl_ai_rs = rs_s; cl_bi_rs = rs_0; {cl_one_addc, cl_sums} = ~0;
      end
      if (t[6]) begin
        cl_rlb_rs = rs_co; cl_rlb_s = ~0;
        cl_abl_rs = rs_co; cl_abh_rs = rs_bsr; op_a_out = ~0;
      end
      if (t[7]) begin
        cl_abl_rs = rs_dl; cl_abl_pcl = ~0;
        cl_ai_rs = rs_s; cl_bi_rs = rs_0; {cl_one_addc, cl_sums} = ~0;
      end
      if (t[8]) begin
        cl_rlb_rs = rs_co; cl_rlb_s = ~0;
        cl_abl_rs = rs_co; cl_abh_rs = rs_bsr; op_a_out = ~0;
      end
      if (t[9]) begin
        cl_abh_rs = rs_dl; cl_abh_pch = ~0;
      end
      op_ins_done = t[9];
    end
    8'b01z11101, 8'b1zz11101: begin // MOV X/Y/SP, A,X/Y/SP
      if (t[3]) begin
        case (ir[7:4])
          4'h5, 4'hF:   cl_rlb_rs = rs_ac;
          4'h7, 4'hB:   cl_rlb_rs = rs_x;
          4'hD:         cl_rlb_rs = rs_y;
          4'h9:         cl_rlb_rs = rs_s;
          default: ;
        endcase
        case (ir[7:4])
          4'h7, 4'hD:   cl_rlb_ac = ~0;
          4'h5, 4'h9:   cl_rlb_x = ~0;
          4'hF:         cl_rlb_y = ~0;
          4'hB:         cl_rlb_s = ~0;
          default: ;
        endcase
        op_set_nz = ~0;
      end

      op_ins_done = t[3];
    end
    8'h7A, 8'h9A: begin         // ADDW / SUBW YA, d
      if (op_t[2]) begin
        cl_ai_rs = rs_ac; cl_bi_rs = rs_dl; {cl_sums} = ~0;
        if (ir == 8'h9A)
          {cl_bi_not, cl_one_addc} = ~0;
        cl_int_op = ~0;
      end
      if (op_t[3]) begin
        cl_ai_rs = rs_aol; cl_bi_rs = rs_0; {cl_one_addc, cl_sums} = ~0;
        cl_rlb_rs = rs_co; cl_rlb_ac = ~0;
        cl_cco_c = ~0;
      end
      if (op_t[4]) begin
        cl_abl_rs = rs_co; cl_abh_rs = rs_dph; op_a_out = ~0;
      end
      if (op_t[5]) begin
        cl_ai_rs = rs_y; cl_bi_rs = rs_dl; {cl_c_addc, cl_sums} = ~0;
        if (ir == 8'h9A)
          {cl_bi_not} = ~0;
      end
      if (t[0]) begin
        cl_rlb_rs = rs_co; cl_rlb_y = ~0;
        cl_dsb_rs = rs_ac;
        {op_set16_nz, cl_cco_c, cl_cvo_v, cl_cho_h} = ~0;
      end
      op_ins_done = op_t[5];
    end
    8'h8D, 8'hCD: begin         // MOV Y/X, #i
      if (t[0]) begin
        cl_rlb_rs = rs_dl;
        if (op_y)
          cl_rlb_y = ~0;
        else if (op_x)
          cl_rlb_x = ~0;
        op_set_nz = ~0;
      end
      op_ins_done = t[3];
    end
    8'h8F: begin                // MOV d, #i
      if (op_t[2]) begin
        cl_dsb_rs = rs_z; cl_store_dor = ~0;
      end
      op_ins_done = op_t[3];
    end
    8'h9E: begin                // DIV YA, X --> A = YA / X, Y = YA % X
      // yva = {0, Y, A}; tx = x << 9
      if (op_t[0]) begin
        cl_ai_rs = rs_y; cl_bi_rs = rs_ac; cl_divss = ~0;
      end
      // for n = 8 .. 0:
      //   ROL yva
      //   if yva > tx:
      //     yva ^= 1
      //   if (yva & 1)
      //     yva -= tx
      if (|{op_t[18], op_t[16], op_t[14], op_t[12], op_t[10], op_t[8], op_t[6],
            op_t[4], op_t[2]}) begin
        cl_ai_rs = rs_x; cl_divcs = ~0;
      end
      // {Y, V, A} = yva
      if (op_t[20]) begin
        cl_rlb_rs = rs_co;
        cl_rlb_ac = ~0;
      end
      if (op_t[22]) begin
        cl_rlb_rs = rs_co;
        cl_co_lsr = 4'd9;
        {cl_rlb_y, cl_ico8_v} = ~0;
        // TODO: `p_h = ?
      end
      op_ins_done = op_t[21];
    end
    8'h9F: begin                // XCN A
      if (t[7]) begin
        cl_ai_rs = rs_ac; cl_xcns = ~0;
      end
      if (t[8]) begin
        cl_rlb_rs = rs_co; {cl_rlb_ac, op_set_nz} = ~0;
      end
      op_ins_done = t[9];
    end
    8'hA0, 8'hC0: begin         // EI, DI
      if (t[0]) begin
        cl_ir5_i = ~0;
      end
      op_ins_done = t[3];
    end
    8'hAA: begin                // MOV1 C, m.b
      if (op_t[1]) begin
        {cl_zero_aoh765, cl_aorh765_bsr_pos} = ~0;
        cl_ai_rs = rs_bsr; cl_bi_rs = rs_dl; cl_ands = ~0;
      end
      if (t[0]) begin
        cl_rlb_rs = rs_co; {cl_rlbnz_c} = ~0;
      end
      op_ins_done = op_t[1];
    end
    8'hC6,                      // MOV (X), A
    8'hD5, 8'hD6: begin         // MOV !a+x/y, A
      if (op_store & op_t[2]) begin
        cl_dsb_rs = rs_ac;
        cl_store_dor = ~0;
      end
      op_ins_done = op_t[3] & op_store;
    end
    8'hCA: begin                // MOV1 m.b, C
      // TODO: verify
      if (|op_t[5:1])
        {cl_zero_aoh765, cl_aorh765_bsr_pos} = ~0;
      if (op_t[1]) begin
        cl_ai_rs = rs_0; cl_bi_rs = rs_0; {cl_bi_not, cl_c_addc, cl_sums} = ~0;
      end
      if (op_t[2]) begin
        cl_ai_rs = rs_bsr; cl_bi_rs = rs_co; {cl_bi_not, cl_ands} = ~0;
        cl_dl_db = 0;
      end
      if (op_t[3]) begin
        cl_ai_rs = rs_dl; cl_bi_rs = rs_co; cl_ands = ~0;
      end
      if (op_t[4]) begin
        cl_dsb_rs = rs_co; cl_store_dor = ~0;
      end
      op_ins_done = op_t[5];
    end
    8'hCF: begin                // MUL YA --> YA = Y * A
      // for n = 7 .. 0:
      //   z = (z << 1) + (A * Y[n])
      if (|{t[16], t[14], t[12], t[10], t[8], t[6], t[4], t[2]}) begin
        cl_ai_rs = rs_ac;
        cl_bi_rs = rs_y;
        cl_muls = ~0;
        cl_mulss = t[2];
      end

      // YA = z
      if (t[17]) begin
        cl_rlb_rs = rs_co;
        cl_co_lsr = 0;
        cl_rlb_ac = ~0;
      end
      if (t[0]) begin
        cl_rlb_rs = rs_co;
        cl_co_lsr = 4'd8;
        {cl_rlb_y, op_set_nz} = ~0;
      end
      op_ins_done = t[17];
    end
    8'hDF, 8'hBE: begin         // DAA / DAS A
      cl_pdas = (ir == 8'hBE);
      if (t[2]) begin
        cl_ai_rs = rs_ac; {cl_bi_dah, cl_sums} = ~0;
        if (cl_pdas)
          {cl_bi_not, cl_one_addc} = ~0;
      end
      if (t[4]) begin
        cl_cco_c = ~0;
        cl_ai_rs = rs_co; {cl_bi_dal, cl_sums} = ~0;
        if (cl_pdas)
          {cl_bi_not, cl_one_addc} = ~0;
      end
      if (t[0]) begin
        cl_rlb_rs = rs_co; cl_rlb_ac = ~0;
        op_set_nz = ~0;
      end
      op_ins_done = t[5];
    end
    8'hEA: begin                // NOT1 m.b
      if (|op_t[3:1])
        {cl_zero_aoh765, cl_aorh765_bsr_pos} = ~0;
      if (op_t[1]) begin
        cl_ai_rs = rs_dl; cl_bi_rs = rs_bsr; cl_eors = ~0;
      end
      if (op_t[2]) begin
        cl_dsb_rs = rs_co; cl_store_dor = ~0;
      end
      op_ins_done = op_t[3];
    end
    8'hED: begin                // NOTC
      if (op_t[1]) begin
        cl_ai_rs = rs_0; cl_bi_rs = rs_0; {cl_bi_not, cl_c_addc, cl_sums} = ~0;
      end
      if (op_t[3]) begin
        cl_ai_rs = rs_co; {cl_rols} = ~0;
      end
      if (t[0]) begin
        cl_cco_c = ~0;
      end
      op_ins_done = op_t[3];
    end
    8'hFA: begin                // MOV dd, ds
      if (op_t[0]) begin
        cl_abl_rs = rs_dl; cl_abh_rs = rs_dph; op_a_out = ~0;
        cl_dsb_rs = rs_z;
        cl_store_dor = ~0;
      end
      op_ins_done = op_t[1];
    end
    8'hFE: begin                // DBNZ Y, rel
      if (t[4]) begin
        cl_ai_rs = rs_y; cl_bi_rs = rs_0; {cl_bi_not, cl_sums} = ~0;
      end
      else if (t[5]) begin
        cl_rlb_rs = rs_co; {cl_rlb_y} = ~0;
      end

      op_bra = t[7];
      op_bra_bits = 3'b101;
    end
    8'hz1: begin                // TCALL
      if (t[4]) begin
        cl_abl_rs = rs_s; cl_abh_rs = rs_bsr; op_a_out = ~0;
      end
      if (t[6]) begin          // Cycle 4: Push PCH
        cl_dsb_rs = rs_pch;
        cl_store_dor = ~0;
        cl_ai_rs = rs_s; cl_bi_rs = rs_0; {cl_bi_not, cl_sums} = ~0;
      end
      if (t[7]) begin
        cl_rlb_rs = rs_co; cl_rlb_s = ~0;
      end
      if (t[8]) begin          // Cycle 5: Push PCL
        cl_abl_rs = rs_co; cl_abh_rs = rs_bsr; op_a_out = ~0;
        cl_dsb_rs = rs_pcl;
        cl_store_dor = ~0;
        cl_ai_rs = rs_s; cl_bi_rs = rs_0; {cl_bi_not, cl_sums} = ~0;
      end
      if (t[9]) begin
        cl_rlb_rs = rs_co; cl_rlb_s = ~0;
      end
      if (t[10]) begin
        cl_ai_rs = rs_0; cl_bi_rs = rs_0; {cl_bi_not, cl_ors} = ~0;
      end
      if (t[12]) begin          // Cycle 7: Fetch PCL
        cl_tcall_val_bsr = ~0;
        cl_abl_rs = rs_bsr;
        cl_abh_rs = rs_co;
        op_a_out = ~0;
      end
      if (t[13]) begin
        cl_rlb_rs = rs_dl; cl_rlb_z = ~0;
        cl_ai_rs = rs_0; cl_bi_rs = rs_aol; {cl_one_addc, cl_sums} = ~0;
      end
      if (t[14]) begin          // Cycle 8: Fetch PCH
        cl_abl_rs = rs_co; cl_abl_aorl = ~0;
      end
      if (t[15]) begin
        cl_abh_rs = rs_dl; cl_abh_pch = ~0;
        cl_abl_rs = rs_z; cl_abl_pcl = ~0;
      end
      op_ins_done = t[15];
    end
    default: ;
  endcase

  // Branch execution cycles
  if (bra_t[0]) begin
    cl_ai_rs = rs_pcl; cl_bi_rs = rs_dl; {cl_sums} = ~0;
  end
  if (bra_t[1]) begin
    cl_abl_rs = rs_co; cl_abl_pcl = ~0;
    cl_bi_not = dl[7]; // sign-extend rel
    cl_ai_rs = rs_pch; cl_bi_rs = rs_0; {cl_carry, cl_sums} = ~0;
  end
  if (bra_t[2]) begin
    cl_abh_rs = rs_co;
    cl_abh_pch = ~0;
  end
end

assign rw_int = ~(cl_store_dor | cl_store_dor_d);
assign int_op = (cl_int_op | cl_int_op_d);


//////////////////////////////////////////////////////////////////////
// Unwrapped I/O interface
//
// The B-Bus can address 4x input buffers (nRD asserted) and 4x output
// port buffers (nWR asserted).
//
// The bus interface (includingbuffer addressing) lies outside this
// module, because the bus is in another clock domain. Input buffers
// are updated when external inputs change and cleared by $F1.
//
// Monitor may set the input port buffer and get the output port
// buffer.

reg [7:0] cpui [0:3];           // input (I/O -> SMP) buffers
reg [7:0] cpuo [0:3];           // output (SMP -> I/O) buffers
reg [3:0] cpuon;

wire      cpui01_clr, cpui23_clr;

// For debugging in Icarus Verilog
wire [7:0] cpui0 = cpui[0];
wire [7:0] cpui1 = cpui[1];
wire [7:0] cpui2 = cpui[2];
wire [7:0] cpui3 = cpui[3];
wire [7:0] cpuo0 = cpuo[0];
wire [7:0] cpuo1 = cpuo[1];
wire [7:0] cpuo2 = cpuo[2];
wire [7:0] cpuo3 = cpuo[3];

always @(posedge CLK) begin
  if (~HOLD & resp) begin
    cpui[0] <= 8'b0;
    cpui[1] <= 8'b0;
    cpui[2] <= 8'b0;
    cpui[3] <= 8'b0;
  end
  else begin
    if (~HOLD) begin
      if (CPUIN[0])
        cpui[0] <= CPUI0;
      if (CPUIN[1])
        cpui[1] <= CPUI1;
      if (CPUIN[2])
        cpui[2] <= CPUI2;
      if (CPUIN[3])
        cpui[3] <= CPUI3;

      if (cpui01_clr) begin
        cpui[0] <= 8'b0;
        cpui[1] <= 8'b0;
      end
      if (cpui23_clr) begin
        cpui[2] <= 8'b0;
        cpui[3] <= 8'b0;
      end
    end

    if (mon_write & (MON_SEL == 8'h08)) begin
      cpui[0] <= MON_DIN[7:0];
      cpui[1] <= MON_DIN[15:8];
      cpui[2] <= MON_DIN[23:16];
      cpui[3] <= MON_DIN[31:24];
    end
  end
end

assign CPUO0 = cpuo[0];
assign CPUO1 = cpuo[1];
assign CPUO2 = cpuo[2];
assign CPUO3 = cpuo[3];
assign CPUON = cpuon;


//////////////////////////////////////////////////////////////////////
// S-SMP Peripheral Registers @ $F0-$FF (except $F2-$F3)

reg [7:0]  spcr;                // $F1: I/O Control
reg [7:0]  spt0, spt1, spt2;    // $FA-$FC: Timer 0-2 Divider
wire [3:0] spc0, spc1, spc2;    // $FD-$FF: Timer 0-2 Output
reg [2:0]  spcc;

assign p_sel = aor[15:4] == 'h00f && aor[3:1] != 'b001;

// Writing $F1 bits 4-5 to 1 clears pairs of input port buffers.
assign cpui01_clr = spcr[4];
assign cpui23_clr = spcr[5];

// Setting $F1 bit 7 enables the IPL ROM.
assign ipl_en = spcr[7];

// Registers @ $F4-$F7: 4x system CPU I/O ports
//
// Reads return the I/O input port buffer. Writes update the output
// port buffer.

always @(posedge CLK) begin
  cpuon <= 4'b0;
  if (~HOLD & resp) begin
    cpuo[0] <= 8'b0;
    cpuo[1] <= 8'b0;
    cpuo[2] <= 8'b0;
    cpuo[3] <= 8'b0;
  end
  else if (p_sel & cp2 & ~rw_int) begin
    cpuo[aor[1:0]] <= dor;
    cpuon[aor[1:0]] <= 1'b1;
  end

  if (mon_write & (MON_SEL == 8'h09)) begin
    cpuo[0] <= MON_DIN[7:0];
    cpuo[1] <= MON_DIN[15:8];
    cpuo[2] <= MON_DIN[23:16];
    cpuo[3] <= MON_DIN[31:24];
    cpuon <= ~4'b0;
  end
end

// Timers are cleared following non-internal read of $FD-$FF
always @* begin
  spcc = 3'b0;
  if (p_sel & rw_int & ~int_op) begin
    case (A[3:0])
      'hD: spcc[0] = 'b1;
      'hE: spcc[1] = 'b1;
      'hF: spcc[2] = 'b1;
      default: ;
    endcase
  end
end

always @(posedge CLK) begin
  if (~HOLD & resp) begin
    spcr <= 8'hb0;
    spt0 <= 8'hff;
    spt1 <= 8'hff;
    spt2 <= 8'hff;
  end
  else if (cp2) begin
    spcr[5:4] <= 2'b0; // self-clearing

    if (cp2 & p_sel & ~rw_int) begin
      case (aor[3:0])
        'h1: spcr <= dor;
        'hA: spt0 <= dor;
        'hB: spt1 <= dor;
        'hC: spt2 <= dor;
        default: ;
      endcase
    end
  end

  if (mon_write & (MON_SEL == 8'h0A)) begin
    spcr <= MON_DIN[7:0];
    spt0 <= MON_DIN[15:8];
    spt1 <= MON_DIN[23:16];
    spt2 <= MON_DIN[31:24];
  end
end

always @* begin
  ipdb = 8'h00;
  if (p_sel)
    case (aor[3:0])
      'h4, 'h5, 'h6, 'h7: ipdb = cpui[aor[1:0]];
      'hD: ipdb = {4'b0, spc0};
      'hE: ipdb = {4'b0, spc1};
      'hF: ipdb = {4'b0, spc2};
      default: ;
    endcase
end


//////////////////////////////////////////////////////////////////////
// Timers

wire      clken_64k, clken_8k;  // Divide-by-N timer clock enables
reg [6:0] divcnt;

wire [12:0] mon_dout_t0, mon_dout_t1, mon_dout_t2;
wire        mon_write_t0, mon_write_t1, mon_write_t2;

initial begin
  divcnt = 0;
end

always @(posedge CLK) begin
  if (cp1) begin
    if (clken_8k)
      divcnt <= 0;
    else
      divcnt <= divcnt + 1'd1;
  end

  if (mon_write & (MON_SEL == 8'h11))
    divcnt <= MON_DIN[28:22];
end

assign clken_64k = divcnt[3:0] == 4'd15; // (CLK & CKEN) / 16 = 64kHz
assign clken_8k = divcnt == 7'd127;      // (CLK & CKEN) / 128 = 8kHz

s_smp_sp_timer timer0 (.CLK(CLK), .CLKEN(cp1), .TICK(clken_8k), .EN(spcr[0]),
                       .CLR(spcc[0]), .T(spt0), .C(spc0),
                       .MON_DIN(MON_DIN[12:0]), .MON_WS(mon_write_t0),
                       .MON_DOUT(mon_dout_t0));
s_smp_sp_timer timer1 (.CLK(CLK), .CLKEN(cp1), .TICK(clken_8k), .EN(spcr[1]),
                       .CLR(spcc[1]), .T(spt1), .C(spc1),
                       .MON_DIN(MON_DIN[12:0]), .MON_WS(mon_write_t1),
                       .MON_DOUT(mon_dout_t1));
s_smp_sp_timer timer2 (.CLK(CLK), .CLKEN(cp1), .TICK(clken_64k), .EN(spcr[2]),
                       .CLR(spcc[2]), .T(spt2), .C(spc2),
                       .MON_DIN(MON_DIN[12:0]), .MON_WS(mon_write_t2),
                       .MON_DOUT(mon_dout_t2));

assign mon_write_t0 = mon_write & (MON_SEL == 8'h15);
assign mon_write_t1 = mon_write & (MON_SEL == 8'h16);
assign mon_write_t2 = mon_write & (MON_SEL == 8'h17);


//////////////////////////////////////////////////////////////////////
// IPL ROM (boot ROM)

reg [7:0] ipl_rom [0:'h3F];

initial begin
  ipl_rom['h00] = 8'hCD;
  ipl_rom['h01] = 8'hEF;
  ipl_rom['h02] = 8'hBD;
  ipl_rom['h03] = 8'hE8;
  ipl_rom['h04] = 8'h00;
  ipl_rom['h05] = 8'hC6;
  ipl_rom['h06] = 8'h1D;
  ipl_rom['h07] = 8'hD0;
  ipl_rom['h08] = 8'hFC;
  ipl_rom['h09] = 8'h8F;
  ipl_rom['h0A] = 8'hAA;
  ipl_rom['h0B] = 8'hF4;
  ipl_rom['h0C] = 8'h8F;
  ipl_rom['h0D] = 8'hBB;
  ipl_rom['h0E] = 8'hF5;
  ipl_rom['h0F] = 8'h78;
  ipl_rom['h10] = 8'hCC;
  ipl_rom['h11] = 8'hF4;
  ipl_rom['h12] = 8'hD0;
  ipl_rom['h13] = 8'hFB;
  ipl_rom['h14] = 8'h2F;
  ipl_rom['h15] = 8'h19;
  ipl_rom['h16] = 8'hEB;
  ipl_rom['h17] = 8'hF4;
  ipl_rom['h18] = 8'hD0;
  ipl_rom['h19] = 8'hFC;
  ipl_rom['h1A] = 8'h7E;
  ipl_rom['h1B] = 8'hF4;
  ipl_rom['h1C] = 8'hD0;
  ipl_rom['h1D] = 8'h0B;
  ipl_rom['h1E] = 8'hE4;
  ipl_rom['h1F] = 8'hF5;
  ipl_rom['h20] = 8'hCB;
  ipl_rom['h21] = 8'hF4;
  ipl_rom['h22] = 8'hD7;
  ipl_rom['h23] = 8'h00;
  ipl_rom['h24] = 8'hFC;
  ipl_rom['h25] = 8'hD0;
  ipl_rom['h26] = 8'hF3;
  ipl_rom['h27] = 8'hAB;
  ipl_rom['h28] = 8'h01;
  ipl_rom['h29] = 8'h10;
  ipl_rom['h2A] = 8'hEF;
  ipl_rom['h2B] = 8'h7E;
  ipl_rom['h2C] = 8'hF4;
  ipl_rom['h2D] = 8'h10;
  ipl_rom['h2E] = 8'hEB;
  ipl_rom['h2F] = 8'hBA;
  ipl_rom['h30] = 8'hF6;
  ipl_rom['h31] = 8'hDA;
  ipl_rom['h32] = 8'h00;
  ipl_rom['h33] = 8'hBA;
  ipl_rom['h34] = 8'hF4;
  ipl_rom['h35] = 8'hC4;
  ipl_rom['h36] = 8'hF4;
  ipl_rom['h37] = 8'hDD;
  ipl_rom['h38] = 8'h5D;
  ipl_rom['h39] = 8'hD0;
  ipl_rom['h3A] = 8'hDB;
  ipl_rom['h3B] = 8'h1F;
  ipl_rom['h3C] = 8'h00;
  ipl_rom['h3D] = 8'h00;
  ipl_rom['h3E] = 8'hC0;
  ipl_rom['h3F] = 8'hFF;
end

assign ipl_sel = ipl_en & (aor[15:6] == 'h3ff);
assign ipl_a = aor[5:0];
assign ipl_do = ipl_rom[ipl_a];


//////////////////////////////////////////////////////////////////////
// Register monitor interface

reg [31:0] mon_dout;

always @* begin
  //``REGION_REGS EMUBUS_CTRL
  mon_dout = 32'b0;
  MON_ACK = 1'b1;
  case (MON_SEL)                //``SUBREGS APU
    8'h01: begin                //``REG ARC0
      mon_dout[15:0] = pc;
      mon_dout[16] = `p_c;      //``FIELD PF_C
      mon_dout[17] = `p_z;      //``FIELD PF_Z
      mon_dout[18] = `p_i;      //``FIELD PF_I
      mon_dout[19] = `p_h;      //``FIELD PF_H
      mon_dout[20] = `p_b;      //``FIELD PF_B
      mon_dout[21] = `p_p;      //``FIELD PF_P
      mon_dout[22] = `p_v;      //``FIELD PF_V
      mon_dout[23] = `p_n;      //``FIELD PF_N
    end
    8'h02: begin                //``REG ARC1
      mon_dout[7:0] = s;
      mon_dout[15:8] = ac;
      mon_dout[23:16] = x;
      mon_dout[31:24] = y;
    end
    8'h04: begin                //``REG UAR0
      mon_dout[7:0] = ir;
      mon_dout[23:8] = aord;    //``FIELD AOR
      mon_dout[31:24] = dord;   //``FIELD DOR
    end
    8'h05: begin                //``REG UAR1
      mon_dout[7:0] = z;
    end
    8'h06: begin                //``REG STCS // SMP Timing Control / Status
      mon_dout[0] = stall;
      mon_dout[1] = stalled;
      mon_dout[7:4] = ts;       //``FIELD TS // Timing State
    end
    8'h07: begin                //``REG SPS // Pin State
      mon_dout[1] = rw_int;     //``FIELD RW
      mon_dout[3] = sync;
    end
    8'h08: begin                //``REG CPUI
      mon_dout[7:0] = cpui0;    //``FIELD CPUI0
      mon_dout[15:8] = cpui1;   //``FIELD CPUI1
      mon_dout[23:16] = cpui2;  //``FIELD CPUI2
      mon_dout[31:24] = cpui3;  //``FIELD CPUI3
    end
    8'h09: begin                //``REG CPUO
      mon_dout[7:0] = cpuo0;    //``FIELD CPUO0
      mon_dout[15:8] = cpuo1;   //``FIELD CPUO1
      mon_dout[23:16] = cpuo2;  //``FIELD CPUO2
      mon_dout[31:24] = cpuo3;  //``FIELD CPUO3
    end
    8'h0A: begin                //``REG SPR0
      mon_dout[7:0] = spcr; //``FIELD SPCR // Timer, I/O and ROM Control
      mon_dout[15:8] = spt0;    //``FIELD SPT0 // Timer 0 Divider
      mon_dout[23:16] = spt1;   //``FIELD SPT1 // Timer 1 Divider
      mon_dout[31:24] = spt2;   //``FIELD SPT2 // Timer 2 Divider
    end
    8'h0B: begin                //``REG SPR1
      mon_dout[3:0] = spc0;     //``FIELD SPC0 // Timer 0 Output
      mon_dout[11:8] = spc1;    //``FIELD SPC1 // Timer 1 Output
      mon_dout[19:16] = spc2;   //``FIELD SPC2 // Timer 2 Output
    end
    8'h10: begin                //``REG INT0
      mon_dout[0] = cken_d;
      mon_dout[1] = cp1d;
      mon_dout[2] = resp;
      mon_dout[3] = am_done_d;
      mon_dout[4] = cco;
      mon_dout[5] = cvo;
      mon_dout[6] = cho;
      mon_dout[7] = cl_store_dor_d;
      mon_dout[8] = cl_int_op_d;
    end
    8'h11: begin                //``REG INT1
      mon_dout[17:0] = t;
      mon_dout[21:18] = bra_t;
      mon_dout[28:22] = divcnt;
    end
    8'h12: begin                //``REG INT2
      mon_dout[15:0] = am_t;
      mon_dout[23:16] = dl;
    end
    8'h13: begin                //``REG INT3
      mon_dout[23:0] = op_t;
    end
    8'h14: begin                //``REG INT4
      mon_dout[7:0] = ico;
      mon_dout[16:8] = icoh;
      mon_dout[19:17] = mulscnt;
    end
    8'h15: begin                //``REG INT5
      mon_dout[12:0] = mon_dout_t0[12:0]; //``FIELD TIMER0
    end
    8'h16: begin                //``REG INT6
      mon_dout[12:0] = mon_dout_t1[12:0]; //``FIELD TIMER1
    end
    8'h17: begin                //``REG INT7
      mon_dout[12:0] = mon_dout_t2[12:0]; //``FIELD TIMER2
    end
    default: MON_ACK = 1'b0;
  endcase
end

assign mon_write = MON_READY & MON_WS & ~MON_VALID;

always @(posedge CLK) begin
  MON_VALID <= 1'b0;
  if (MON_ACK & MON_READY) begin
    MON_VALID <= 1'b1;
    MON_DOUT <= mon_dout;
  end
end


endmodule
