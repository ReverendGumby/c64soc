// Audio Sample Packet source
//
// Audio samples are buffered on arrival. The buffer holds up to four
// samples. When the buffer has at least one sample, as soon as the
// packet sink allows (i.e., during a blanking interval), a packet is
// sent with all buffered samples.
//
// Shortcuts taken:
//
// - Sample buffer is only one sample deep. 48 kHz audio doesn't need
// more with a 45 kHz raster rate.

module hdmi_audio_sample_src
  #(parameter AUDIO_SAMPLE_RATE = 48000)
  (
   input             PCLK,
   input             nRST,

   // (INPUT) Digital audio
   input             MCLK,      // eg, 128 * 48 kHz = 6.144 MHz
   input             MCLKEN,
   input             FCLK,      // frame clock, PDIN* launched on fall
   input [15:0]      PDINL,     // parallel sample, left channel
   input [15:0]      PDINR,     // parallel sample, right channel

   // (OUTPUT) Audio Sample packets
   output reg        VALID,
   output reg        LAST,
   input             NEXT,
   output reg [35:0] DATA
   );

//////////////////////////////////////////////////////////////////////
// Sample buffer
//
// Timing closure note: setup violations from pdin*_b to *sb0 can be
// ignored. The delay of [ FCLK falling -> tick -> tick_pclk -> go ]
// is guaranteed to meet setup requirements.

reg [15:0] pdinl_b, pdinr_b;    // MCLK
reg        fclk_d;              // MCLK
reg        tick_d;              // MCLK
(* ASYNC_REG = "TRUE" *)
reg [3:0]  tick_pclk;           // MCLK -> PCLK
reg [15:0] lsb0, rsb0;
reg        sbu;                 // *sb0 used

wire       sbc;                    // clear sb*
wire       tick = !FCLK && fclk_d; // FCLK falling edge detector

always @(posedge MCLK) if (MCLKEN) begin
  fclk_d <= FCLK;

  if (tick) begin
    pdinl_b <= PDINL;
    pdinr_b <= PDINR;
  end

  // Safe input to tick_pclk synchronizer chain
  tick_d <= tick;
end

assign go = !tick_pclk[2] && tick_pclk[3]; // tick falling edge detector

always @(posedge PCLK) begin
  // Sync to PCLK
  tick_pclk <= (tick_pclk << 1) | tick_d;

  // Maintain sample buffer
  if (!nRST) begin
    lsb0 <= 0;
    rsb0 <= 0;
    sbu <= 0;
  end
  else begin
    if (sbc) begin
      lsb0 <= 0;
      rsb0 <= 0;
      sbu <= 0;
    end
    if (go) begin
      lsb0 <= pdinl_b;
      rsb0 <= pdinr_b;
      sbu <= 1'b1;
    end
  end
end

//////////////////////////////////////////////////////////////////////
// IEC 60958-3 channel status bitstream generator
//
// A block contains 192 frames.
// A frame contains one channel status bit.
// The first frame in a block has a "B" preamble.

reg [3:0]  cs_sf;               // sampling frequency
reg [35:0] cs;                  // channel status data
reg [7:0]  fidx;                // frame index

initial begin
  if (AUDIO_SAMPLE_RATE == 48000)
    cs_sf = 4'b0010;
  else if (AUDIO_SAMPLE_RATE == 32000)
    cs_sf = 4'b0011;
  else begin
    $display("unsupported AUDIO_SAMPLE_RATE %d", AUDIO_SAMPLE_RATE);
    $finish;
  end
end

always @* begin
  cs = 36'b0;
  cs[0] = 1'b0;                 // consumer use
  cs[1] = 1'b0;                 // liner PCM samples
  cs[7:6] = 2'b00;              // Mode 0
  cs[15:8] = 8'b00000010;       // category code: PCM encoder/decoder
  cs[27:24] = cs_sf;            // sampling frequency
  cs[32] = 1'b0;                // 20 bit max. sample word len
  cs[35:33] = 3'b001;           // 16 bits sample word len
end

always @(posedge PCLK) begin
  if (!nRST) begin
    fidx <= 8'd0;
  end
  else if (sbc) begin
    if (fidx == 8'd191)
      fidx <= 8'd0;
    else
      fidx <= fidx + 1'd1;
  end
end

wire cs_b = fidx == 8'd0;       // frame preamble is "B"
wire as_c = fidx < 8'd36 ? cs[fidx] : 1'b0; // channel status bit

//////////////////////////////////////////////////////////////////////
// Sub-frame generators (one per channel)

wire [31:4] lsf, rsf;

wire       sf_pre = cs_b;       // frame preamble

hdmi_audio_sub_frame_gen lsfgen
  (
   .PCLK(PCLK),
   .nRST(nRST),
   .SB0(lsb0),
   .CS(as_c),
   .GO(go),
   .SF(lsf)
   );

hdmi_audio_sub_frame_gen rsfgen
  (
   .PCLK(PCLK),
   .nRST(nRST),
   .SB0(rsb0),
   .CS(as_c),
   .GO(go),
   .SF(rsf)
   );

//////////////////////////////////////////////////////////////////////
// Packet generator

reg [35:0] pkt [0:7];
reg [2:0]  idx;

initial begin
  // Layout 0: Subpkt 0-3 = frames 0-3
  // Only subpkt 0 is used.
  pkt[0] = 36'h2000000xx;       // HB0[3:0], SB3:0 SB0
  pkt[1] = 36'h0000000xx;       // HB0[7:4], SB3:0 SB1
  pkt[2] = 36'hx000000xx;       // HB1[3:0], SB3:0 SB2
  pkt[3] = 36'h0000000xx;       // HB1[7:4], SB3:0 SB3
  pkt[4] = 36'h0000000xx;       // HB2[3:0], SB3:0 SB4
  pkt[5] = 36'hx000000xx;       // HB2[7:4], SB3:0 SB5
  pkt[6] = 36'hx000000xx;       // BP4[3:0], SB3:0 SB6
  pkt[7] = 36'hxxxxxxxxx;       // BP4[7:4], BP3 , BP2,  BP1,  BP0
end

always @* begin
  // sample_present.sp0-3
  pkt[2][35:32] = { 3'b000, sbu };
  // B.X: 1 if subpacket X contains 1st frame in block
  pkt[5][35:32] = { 3'b000, sf_pre };
  // Sub-frame bits 27:12.
  pkt[0][7:0] = lsf[11:4];
  pkt[1][7:0] = lsf[19:12];
  pkt[2][7:0] = lsf[27:20];
  pkt[3][7:0] = rsf[11:4];
  pkt[4][7:0] = rsf[19:12];
  pkt[5][7:0] = rsf[27:20];
  // Sub-frame bits 31:28.
  pkt[6][3:0] = lsf[31:28];
  pkt[6][7:4] = rsf[31:28];
end

always @(posedge PCLK) begin
  if (!nRST) begin
    VALID <= 1'b0;
  end
  else begin
    if (!VALID && sbu) begin
      idx <= 0;
      VALID <= 1'b1;
    end
    else if (VALID && NEXT) begin
      if (LAST) begin
        VALID <= 1'b0;
      end
      else begin
        idx <= idx + 1'd1;
      end
    end
  end
end

assign sbc = VALID && NEXT && LAST;

always @* begin
  if (!nRST || !VALID) begin
    DATA = 36'dx;
    LAST = 1'bx;
  end
  else begin
    DATA = pkt[idx];
    LAST = idx == 3'd7;
  end
end

endmodule
