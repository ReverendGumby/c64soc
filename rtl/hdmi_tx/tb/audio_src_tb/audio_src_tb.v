`timescale 1ns / 1ps

module audio_src_tb();

`define TP_WIDTH    12'd1280
`define TP_HEIGHT   12'd720

wire [11:0] x, y;
wire [11:0] hblank_dcnt, vblank_dcnt;
wire [35:0] data;

reg         pclk, mclk, fclk, rst;

// Clock / reset generator

// Our MCLK is 512 * Fs. hdmi_audio_src wants 128 * Fs.
integer     mclk_mr = 512;
reg [2:0]   mclk_div = 3'd3;   // MCLK divider: 2^2 = 4

initial pclk = 1'b0;
always #6.25 begin :pclkgen     // 80.000 MHz
  pclk = !pclk;
end

initial mclk = 1'b0;
always #12.5 begin :mclkgen       // 40.000 MHz
  mclk = !mclk;
end

// (Divided) audio / video clock ratio is 1 / 8
// With N = 6144, CTS should be 49152

initial begin
  forever begin
    fclk = 1'b0;
    repeat (mclk_mr / 2) @(posedge mclk) ;
    fclk = 1'b1;
    repeat (mclk_mr / 2) @(posedge mclk) ;
  end
end

initial begin
  rst = 1'b0;
  repeat (10) 
    @(posedge mclk) 
      ;
  rst = 1'b1;
end

// Video signals

video_timing video_timing
  (
   .CLK(pclk),
   .CLKEN(1'b1),
   .RST(rst),

   .WIDTH(`TP_WIDTH),
   .HEIGHT(`TP_HEIGHT),
   .LEADIN_Y(12'd1),

   .X(x),
   .Y(y),

   .HS(hs),
   .VS(vs),
   .DE(de),

   .HBLANK_DCNT(hblank_dcnt),
   .VBLANK_DCNT(vblank_dcnt)
   );

// Audio signals

reg [15:0] pdin;
initial pdin = 0;

always @(negedge fclk) begin
  pdin = pdin + 1;
end

// Output sink
pkt_sink #(36, 3) pkt_sink
  (
   .CLK(pclk),
   .VALID(valid),
   .LAST(last),
   .NEXT(next),
   .DATA(data)
   );

always @(posedge pclk) begin
  if ((vblank_dcnt || hblank_dcnt > 12'd48) && valid)
    pkt_sink.rx();
end

// Module under test

hdmi_audio_src audio_src
  (
   .PCLK(pclk),
   .nRST(rst),

   .MCLK_DIV(mclk_div),
   .MCLK(mclk),
   .FCLK(fclk),
   .PDINL(pdin),
   .PDINR(~pdin),

   .VALID(valid),
   .LAST(last),
   .NEXT(next),
   .DATA(data)
   );

integer frames;

// Simulation lifetime

initial begin
  $dumpfile("audio_src_tb.vcd");
  $dumpvars;

  frames = 0;
end

always @(posedge pclk) begin
  if (x == 0 && y == 0)
    frames = frames + 1;
//  if (vblank_dcnt == 0)
  if (frames == 2)
    $finish();
end

endmodule
