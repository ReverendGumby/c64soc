`timescale 1ns / 1ps

module data_island_gen_tb();

wire [35:0] in_data;
wire [2:2]  da;
wire [3:0]  db, dc;

reg         clk;

// Clock generator

initial clk = 1'b0;
always #20 begin :clkgen
  clk = !clk;
end

// input source
pkt_source #(36, 8, 36'h012345678) pkt_src 
  (
   .CLK(clk),
   .VALID(in_valid),
   .LAST(in_last),
   .NEXT(in_next),
   .DATA(in_data)
   );

initial
  forever begin
    repeat (5)
      @(posedge clk)
        ;
    pkt_src.tx();
    repeat (10)
      @(posedge clk)
        ;
  end

// output sink
pkt_sink #(9, 0) pkt_sink
  (
   .CLK(clk),
   .VALID(out_valid),
   .LAST(out_last),
   .NEXT(out_next),
   .DATA({da, db, dc})
   );

initial
  repeat (3) begin
    repeat (10)
      @(posedge clk)
        ;
    pkt_sink.rx();
  end

// Module under test

hdmi_data_island_gen data_island_gen
  (
   .CLK(clk),

   .IN_VALID(in_valid),
   .IN_LAST(in_last),
   .IN_NEXT(in_next),
   .IN_DATA(in_data),

   .OUT_VALID(out_valid),
   .OUT_LAST(out_last),
   .OUT_NEXT(out_next),
   .OUT_DA(da),
   .OUT_DB(db),
   .OUT_DC(dc)
   );

// Simulation lifetime

initial begin
  $dumpfile("data_island_gen_tb.vcd");
  $dumpvars;

  repeat (160)
    @(posedge clk) ;

  $finish();
end

endmodule
