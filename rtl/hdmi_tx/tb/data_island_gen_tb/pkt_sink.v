module pkt_sink #(parameter DW = 9,
                  parameter DELAY = 0)
(
 input          CLK,
 input          VALID,
 input          LAST,
 output reg     NEXT,
 input [DW-1:0] DATA
 );

initial begin
  NEXT <= 1'b0;
end

task rx;
reg last;
integer cnt;
  begin
    last = 0;
    cnt = 0;
    while (!last) begin
      @(posedge CLK) if (VALID) begin
        last = LAST;
        NEXT <= 1'b1;
        cnt = cnt + 1;
        if (DELAY > 0) begin
          @(posedge CLK) ;
          NEXT <= 1'b0;
          repeat (DELAY - 1) @(posedge CLK) ;
        end
      end
    end
    NEXT <= 1'b0;
    cnt = 'dx;
  end
endtask

endmodule
