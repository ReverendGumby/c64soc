module pkt_source #(parameter DW = 36,
                    parameter NUM_WORDS = 8)
(
 input               CLK,
 output reg          VALID,
 output reg          LAST,
 input               NEXT,
 output reg [DW-1:0] DATA
 );

initial begin
  DATA <= {DW{1'bx}};
  VALID <= 1'b0;
  LAST <= 1'bx;
end

integer seed = 100;

task set_data;
  begin
    DATA[31:0] <= $urandom(seed);
    DATA[DW-1:32] <= $urandom(seed);
  end
endtask

task tx;
integer dcnt;
begin
  set_data(DATA);
  VALID <= 1'b1;
  LAST <= 1'b0;

  dcnt = NUM_WORDS;
  while (dcnt) begin
    @(posedge CLK) if (NEXT) begin
      set_data(DATA);
      dcnt = dcnt - 1;
      LAST <= dcnt == 1;
    end
  end

  DATA <= {DW{1'bx}};
  VALID <= 1'b0;
  LAST <= 1'bx;
end
endtask

endmodule
