// hdmi_tx_tb: Test the full HDMI TX pipeline.

`timescale 1ns / 1ps

module hdmi_tx_tb();

`define TP_WIDTH    12'd1280
`define TP_HEIGHT   12'd720

wire [`ENCODER_MUX_SEL_M:0] sel;
wire [11:0] x, y;
wire [11:0] hblank_dcnt, vblank_dcnt;
wire [7:0]  r, g, b;
wire        hs, vs, de;
wire [29:0] serout;

reg         pclk, mclk, fclk, rst;

// Clock / reset generator

initial pclk = 1'b0;
always #6.734 begin :pclkgen    // 74.250 MHz
  pclk = !pclk;
end

initial mclk = 1'b0;
always #81.380 begin :mclkgen   // 6.144 MHz
  mclk = !mclk;
end

initial begin
  forever begin
    fclk = 1'b0;
    repeat (64) @(posedge mclk) ;
    fclk = 1'b1;
    repeat (64) @(posedge mclk) ;
  end
end

initial begin
  rst = 1'b0;
  repeat (10) 
    @(posedge mclk) 
      ;
  rst = 1'b1;
end

// Video signals

video_timing video_timing
  (
   .CLK(pclk),
   .CLKEN(1'b1),
   .RST(rst),

   .WIDTH(`TP_WIDTH),
   .HEIGHT(`TP_HEIGHT),
   .LEADIN_Y(12'd1),

   .X(x),
   .Y(y),

   .HS(hs),
   .VS(vs),
   .DE(de),

   .HBLANK_DCNT(hblank_dcnt),
   .VBLANK_DCNT(vblank_dcnt)
   );

video_test_pattern video_test_pattern
  (
   .CLK(pclk),
   .CLKEN(1'b1),

   .WIDTH(`TP_WIDTH),
   .HEIGHT(`TP_HEIGHT),
   .MODE(3'd5),

   .X(x),
   .Y(y),
   .DE(de),
   
   .R(r),
   .G(g),
   .B(b)
   );

// Audio signals

reg [15:0] pdin;
initial pdin = 0;

always @(negedge fclk) begin
  pdin = pdin + 1365;           // roughly 1 kHz sawtooth
end

// The HDMI TX pipeline

hdmi_tx tx
  (
   .PCLK(pclk),
   .RESET(~rst),

   .MODE(1'b1),
   .AUD_EN(1'b1),

   .MCLK_DIV(2'b00),
   .MCLK(mclk),
   .FCLK(fclk),
   .PDINL(pdin),
   .PDINR(~pdin),

   .R(r),
   .G(g),
   .B(b),
   .HS(hs),
   .VS(vs),
   .DE(de),
   .HBLANK_DCNT(hblank_dcnt),
   .VBLANK_DCNT(vblank_dcnt),

   .SEROUT(serout)
   );

// Simulation lifetime

initial begin
  $dumpfile("hdmi_tx_tb.vcd");
  $dumpvars;
end

always @(posedge pclk) begin
  if (y == 3)
    $finish();
end

endmodule
