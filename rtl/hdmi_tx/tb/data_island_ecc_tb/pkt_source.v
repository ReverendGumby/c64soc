module pkt_source #(parameter DW = 36,
                    parameter NUM_WORDS = 8)
(
 input               CLK,
 output reg          VALID,
 output reg          LAST,
 input               NEXT,
 output reg [DW-1:0] DATA
 );

initial begin
  DATA <= {DW{1'bx}};
  VALID <= 1'b0;
  LAST <= 1'bx;
end

integer seed = 100;

task set_data(integer cnt);
  begin
    DATA[31:0] <= $urandom(seed);
    DATA[DW-1:32] <= $urandom(seed);
    if (cnt >= NUM_WORDS - 2)
      DATA[DW-1:DW-4] <= 'dx;
    if (cnt >= NUM_WORDS - 1)
      DATA <= 'dx;
  end
endtask

task tx;
integer cnt;
begin
  VALID = 1'b1;

  cnt = 0;
  while (cnt < NUM_WORDS) begin
    set_data(cnt);
    cnt = cnt + 1;
    LAST = cnt == NUM_WORDS;
    @(posedge CLK) ;
    while (!NEXT) @(posedge CLK) ;
  end

  DATA = {DW{1'bx}};
  VALID = 1'b0;
  LAST = 1'bx;
end
endtask

endmodule
