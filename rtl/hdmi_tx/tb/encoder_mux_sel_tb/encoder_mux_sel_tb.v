`timescale 1ns / 1ps

`include "../../encoder_mux.vh"

module encoder_mux_sel_tb();

`define TP_WIDTH    12'd1280
`define TP_HEIGHT   12'd720

wire [`ENCODER_MUX_SEL_M:0] sel;
wire [11:0] x, y;
wire [11:0] hblank_dcnt, hblank_ucnt, vblank_dcnt, vblank_ucnt;
wire [7:0]  r, g, b;
wire        de;
wire        tp_hs, tp_vs, tp_de;
wire        data_next;

reg         clk, rst;
reg         data_valid, data_last;

// Clock / reset generator

initial clk = 1'b0;
always #20 begin :clkgen
  clk = !clk;
end

initial begin
  rst = 1'b0;
  #100 @(posedge clk) rst = 1'b1;
end

// Video signals

video_timing video_timing
  (
   .CLK(clk),
   .CLKEN(1'b1),
   .RST(rst),

   .WIDTH(`TP_WIDTH),
   .HEIGHT(`TP_HEIGHT),
   .LEADIN_Y(12'd1),

   .X(x),
   .Y(y),

   .HS(hs),
   .VS(vs),
   .DE(de),

   .HBLANK_DCNT(hblank_dcnt),
   .HBLANK_UCNT(hblank_ucnt),
   .VBLANK_DCNT(vblank_dcnt),
   .VBLANK_UCNT(vblank_ucnt)
   );

video_test_pattern video_test_pattern
  (
   .CLK(clk),
   .CLKEN(1'b1),

   .WIDTH(`TP_WIDTH),
   .HEIGHT(`TP_HEIGHT),
   .MODE(3'd5),

   .X(x),
   .Y(y),
   .DE(de),
   
   .R(r),
   .G(g),
   .B(b)
   );

// Data pattern generator

always @* data_valid =
  (
   // packets not allowed here
   (vblank_ucnt == 12'd1 && x == 12'd32)
   // standalone packet
   || (vblank_ucnt == 12'd2 && x == 12'd32)
   // packet starting before h.blank begins
   || (y == 12'd2 && (x >= 12'd1270 && x <= 12'd1300))
   // packet starting before h.blank ends
   || (y == 12'd2 && hblank_dcnt == 12'd58)
   // packet starting too late in h.blank
   || (y == 12'd3 && hblank_dcnt == 12'd57)
   // 2 packets back-to-back
   || (vblank_ucnt == 12'd3 && (x >= 12'd2 && x <= 12'd44))
   // 2 packets separated by 0-12 clk
   || (vblank_ucnt == 12'd4  && (x == 12'd2 || (x >= 12'd45 && x <= 12'd44)))
   || (vblank_ucnt == 12'd5  && (x == 12'd2 || (x >= 12'd45 && x <= 12'd45)))
   || (vblank_ucnt == 12'd6  && (x == 12'd2 || (x >= 12'd45 && x <= 12'd46)))
   || (vblank_ucnt == 12'd7  && (x == 12'd2 || (x >= 12'd45 && x <= 12'd47)))
   || (vblank_ucnt == 12'd8  && (x == 12'd2 || (x >= 12'd45 && x <= 12'd48)))
   || (vblank_ucnt == 12'd9  && (x == 12'd2 || (x >= 12'd45 && x <= 12'd49)))
   || (vblank_ucnt == 12'd10 && (x == 12'd2 || (x >= 12'd45 && x <= 12'd50)))
   || (vblank_ucnt == 12'd11 && (x == 12'd2 || (x >= 12'd45 && x <= 12'd51)))
   || (vblank_ucnt == 12'd12 && (x == 12'd2 || (x >= 12'd45 && x <= 12'd52)))
   || (vblank_ucnt == 12'd13 && (x == 12'd2 || (x >= 12'd45 && x <= 12'd53)))
   || (vblank_ucnt == 12'd14 && (x == 12'd2 || (x >= 12'd45 && x <= 12'd54)))
   || (vblank_ucnt == 12'd15 && (x == 12'd2 || (x >= 12'd45 && x <= 12'd55)))
   || (vblank_ucnt == 12'd16 && (x == 12'd2 || (x >= 12'd45 && x <= 12'd56)))
   // 2 packets back-to-back before h.blank ends
   || (y == 12'd4 && (hblank_dcnt <= 12'd90 && hblank_dcnt >= 12'd48))
   // 2 packets back-to-back too late in h.blank
   || (y == 12'd5 && (hblank_dcnt <= 12'd89 && hblank_dcnt >= 12'd47))
   // > 18 back-to-back packets
   || (vblank_ucnt == 12'd20)
   );

// Module under test

hdmi_encoder_mux_sel encoder_mux_sel
  (
   .CLK(clk),

   .HBLANK_DCNT(hblank_dcnt),
   .VBLANK_DCNT(vblank_dcnt),

   .DATA_IN_VALID(data_valid),
   .DATA_OUT_VALID(data_valid),
   .DATA_OUT_LAST(data_last),
   .DATA_OUT_NEXT(data_next),

   .SEL(sel)
   );

integer frames;

// Simulation lifetime

initial begin
  $dumpfile("encoder_mux_sel_tb.vcd");
  $dumpvars;

  frames = 0;
end

always @(posedge clk) begin
  if (x == 0 && y == 0)
    frames = frames + 1;
//  if (vblank_dcnt == 0)
  if (frames == 2)
    $finish();
end

endmodule
