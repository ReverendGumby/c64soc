`timescale 1ns / 1ps

module data_island_pkt_mux_tb();

`define TP_WIDTH    12'd1280
`define TP_HEIGHT   12'd720

wire [11:0] x, y;
wire [11:0] hblank_dcnt, vblank_dcnt;
wire        info_frame_next;
wire        audio_next;
wire        out_valid;
wire [35:0] out_data, info_frame_data, audio_data;

reg [2:0]   out_dcnt;
reg         out_window;
reg         clk, rst;

// Clock / reset generator

initial clk = 1'b0;
always #20 begin :clkgen
  clk = !clk;
end

initial begin
  rst = 1'b0;
  #100 @(posedge clk) rst = 1'b1;
end

// Video signals

video_timing video_timing
  (
   .CLK(clk),
   .CLKEN(1'b1),
   .RST(rst),

   .WIDTH(`TP_WIDTH),
   .HEIGHT(`TP_HEIGHT),
   .LEADIN_Y(12'd1),

   .X(x),
   .Y(y),

   .HS(hs),
   .VS(vs),
   .DE(de),

   .HBLANK_DCNT(hblank_dcnt),
   .VBLANK_DCNT(vblank_dcnt)
   );

// MUX input sources
pkt_source #(36, 8, 36'h012345678) audio_pkt_src 
  (
   .CLK(clk),
   .VALID(audio_valid),
   .LAST(audio_last),
   .NEXT(audio_next),
   .DATA(audio_data)
   );

initial @(posedge rst) begin
  forever begin
    repeat (1547)               // 74.25MHz / 48kHz
      @(posedge clk)
        ;
    audio_pkt_src.tx();
  end
end

hdmi_info_frame_src info_frame_pkt_src
  (
   .CLK(clk),
   .nRST(info_frame_nrst),
   .AUD_EN(1'b1),
   .VALID(info_frame_valid),
   .LAST(info_frame_last),
   .NEXT(info_frame_next),
   .DATA(info_frame_data)
   );

// MUX output sink
initial begin
  out_dcnt <= 2'd0;
  out_window <= 1'b0;
end

always @(posedge clk) begin
  if (vblank_dcnt || (!vblank_dcnt && hblank_dcnt > 12'd48))
    out_window <= 1'b1;
  else if (!out_valid || (out_last && out_next))
    out_window <= 1'b0;

  if (out_dcnt == 2'd0) begin
    if (out_valid && out_window)
      out_dcnt <= 2'd3;
  end
  else
    out_dcnt <= out_dcnt - 1'd1;
end
assign out_next = out_dcnt == 2'd3;

// Module under test

hdmi_data_island_pkt_mux data_island_pkt_mux
  (
   .CLK(clk),
   .nRST(rst),

   .VBLANK_DCNT(vblank_dcnt),

   .INFO_FRAME_nRST(info_frame_nrst),
   .INFO_FRAME_VALID(info_frame_valid),
   .INFO_FRAME_LAST(info_frame_last),
   .INFO_FRAME_NEXT(info_frame_next),
   .INFO_FRAME_DATA(info_frame_data),

   .AUDIO_VALID(audio_valid),
   .AUDIO_LAST(audio_last),
   .AUDIO_NEXT(audio_next),
   .AUDIO_DATA(audio_data),

   .OUT_VALID(out_valid),
   .OUT_LAST(out_last),
   .OUT_NEXT(out_next),
   .OUT_DATA(out_data)
   );

integer frames;

// Simulation lifetime

initial begin
  $dumpfile("data_island_pkt_mux_tb.vcd");
  $dumpvars;

  frames = 0;
end

always @(posedge clk) begin
  if (x == 0 && y == 0)
    frames = frames + 1;
//  if (vblank_dcnt == 0)
  if (frames == 2)
    $finish();
end

endmodule
