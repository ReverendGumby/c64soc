module pkt_source #(parameter DW = 36,
                    parameter NUM_WORDS = 8,
                    parameter [DW-1:0] FIXED_DATA = 0)
(
 input               CLK,
 output reg          VALID,
 output reg          LAST,
 input               NEXT,
 output reg [DW-1:0] DATA
 );

initial begin
  DATA <= {DW{1'bx}};
  VALID <= 1'b0;
  LAST <= 1'bx;
end

task tx;
integer dcnt;
begin
  DATA <= FIXED_DATA;
  VALID <= 1'b1;
  LAST <= 1'b0;

  dcnt = NUM_WORDS;
  while (dcnt) begin
    @(posedge CLK) if (NEXT) begin
      dcnt = dcnt - 1;
      LAST <= dcnt == 1;
    end
  end

  DATA <= {DW{1'bx}};
  VALID <= 1'b0;
  LAST <= 1'bx;
end
endtask

endmodule
