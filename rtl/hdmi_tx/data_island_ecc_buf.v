// Add BCH ECC parity and buffer the output.

module hdmi_data_island_ecc_buf
  (
   input         CLK,

   // (INPUT) Data packet
   input         IN_VALID,
   input         IN_LAST,
   output        IN_NEXT,
   input [35:0]  IN_DATA,
   
   // (OUTPUT) Data packet with ECC parity
   output        OUT_VALID,
   output        OUT_LAST,
   input         OUT_NEXT,
   output [35:0] OUT_DATA
   );

wire [35:0]      data;

hdmi_data_island_ecc ecc
  (
   .CLK(CLK),

   .IN_VALID(IN_VALID),
   .IN_LAST(IN_LAST),
   .IN_NEXT(IN_NEXT),
   .IN_DATA(IN_DATA),

   .OUT_VALID(valid),
   .OUT_LAST(last),
   .OUT_NEXT(next),
   .OUT_DATA(data)
   );

hdmi_pkt_buf bufr
  (
   .CLK(CLK),

   .IN_VALID(valid),
   .IN_LAST(last),
   .IN_NEXT(next),
   .IN_DATA(data),

   .OUT_VALID(OUT_VALID),
   .OUT_LAST(OUT_LAST),
   .OUT_NEXT(OUT_NEXT),
   .OUT_DATA(OUT_DATA)
   );

endmodule
