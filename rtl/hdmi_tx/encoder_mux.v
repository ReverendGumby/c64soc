`include "encoder_mux.vh"

module hdmi_encoder_mux
  (
   input [`ENCODER_MUX_SEL_M:0] SEL, // MUX selector

   // (INPUT) Video data
   input [7:0]                  R,
   input [7:0]                  G,
   input [7:0]                  B,
   input                        VDE, // Video data enable
   input                        HSYNC,
   input                        VSYNC,

   // (INPUT) Packet data
   input [2:2]                  DA,
   input [3:0]                  DB,
   input [3:0]                  DC,

   // (OUTPUT) Channel 0-2 encoder inputs
   output [7:0]                 D0, D1, D2,
   output                       DE,
   output reg [4:0]             C0, C1, C2,
   output reg                   TERC4
   );

reg [3:0] CTL;

always @* begin
  case (SEL)
    // Table 5-2 Preambles for Each Data Period Type
    `ENCODER_MUX_SEL_PRE_DATA:  CTL[3:0] = 4'b0101;
    `ENCODER_MUX_SEL_PRE_VIDEO: CTL[3:0] = 4'b0001;
    `ENCODER_MUX_SEL_CTRL:      CTL[3:0] = 4'b0000;
    default: CTL = 4'dx;
  endcase
end

always @* begin
  case (SEL)
    `ENCODER_MUX_SEL_GB_VIDEO: begin
      // Table 5-5 Video Leading Guard Band Values
      TERC4 = 1'b1;
      C0 = 5'b01000;            // 0b1011001100
      C1 = 5'b11000;            // 0b0100110011
      C2 = 5'b01000;            // 0b1011001100
    end
    `ENCODER_MUX_SEL_GB_DATA,
      `ENCODER_MUX_SEL_GB_CTRL: begin
      // Table 5-6 Data Island Leading and Trailing Guard Band Values
      TERC4 = 1'b1;
      C0 = { 3'b011, VSYNC, HSYNC };
      C1 = 5'b11000;            // 0b0100110011
      C2 = 5'b11000;            // 0b0100110011
    end
    `ENCODER_MUX_SEL_DATA: begin
      TERC4 = 1'b1;
      C0 = { 1'b0, DA, VSYNC, HSYNC };
      C1 = DB;
      C2 = DC;
    end
    `ENCODER_MUX_SEL_PRE_DATA,
      `ENCODER_MUX_SEL_PRE_VIDEO,
      `ENCODER_MUX_SEL_CTRL: begin
      TERC4 = 1'b0;
      C0 = { VSYNC, HSYNC };
      C1 = CTL[1:0];
      C2 = CTL[3:2];
    end
    default: begin
      TERC4 = 1'bx;
      C0 = 5'dx;
      C1 = 5'dx;
      C2 = 5'dx;
    end
  endcase
end

assign D0 = B;
assign D1 = G;
assign D2 = R;

assign DE = VDE;

endmodule
