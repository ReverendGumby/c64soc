// Audio Packet source
//
// Generates two packet types:
// 
// 1. Audio Sample Packet
// 2. Audio Clock Regeneration Packet (N / CTS)
//
// Features:
// - Video and audio clocks may be fully asynchronous
// - Audio formats:
//   - Input: mono, 16-bit LPCM, 48 kHz
//   - Output: "Generic audio" stereo, 16-bit LPCM, 48 kHz

module hdmi_audio_src
  #(parameter AUDIO_SAMPLE_RATE = 48000)
  (
   input         PCLK,
   input         nRST,

   // (INPUT) Digital audio
   input [2:0]   MCLK_DIV,      // MCLK divider: N-1
   input         MCLK,          // 128 * 48 kHz = 6.144 MHz
   input         FCLK,          // frame clock, PDIN* launched on fall
   input [15:0]  PDINL,         // parallel sample, left channel
   input [15:0]  PDINR,         // parallel sample, right channel

   // (OUTPUT) Audio packets
   output        VALID,
   output        LAST,
   input         NEXT,
   output [35:0] DATA
   );

wire [35:0] sample_src_data, sample_ecc_data;
wire [35:0] clk_regen_src_data, clk_regen_ecc_data;

mclk_div mclk_div
  (
   .MCLK_DIV(MCLK_DIV),
   .MCLK(MCLK),
   .FCLK(FCLK),

   .MCLKEN(mclken)
   );

// Audio Sample Packet source
hdmi_audio_sample_src #(AUDIO_SAMPLE_RATE) sample_src
  (
   .PCLK(PCLK),
   .nRST(nRST),

   .MCLK(MCLK),
   .MCLKEN(mclken),
   .FCLK(FCLK),
   .PDINL(PDINL),
   .PDINR(PDINR),

   .VALID(sample_src_valid),
   .LAST(sample_src_last),
   .NEXT(sample_src_next),
   .DATA(sample_src_data)
   );

hdmi_data_island_ecc_buf sample_ecc
  (
   .CLK(PCLK),

   .IN_VALID(sample_src_valid),
   .IN_LAST(sample_src_last),
   .IN_NEXT(sample_src_next),
   .IN_DATA(sample_src_data),

   .OUT_VALID(sample_ecc_valid),
   .OUT_LAST(sample_ecc_last),
   .OUT_NEXT(sample_ecc_next),
   .OUT_DATA(sample_ecc_data)
   );

// Audio Clock Regeneration Packet source
hdmi_audio_clk_regen_src #(AUDIO_SAMPLE_RATE) clk_regen_src
  (
   .PCLK(PCLK),
   .nRST(nRST),

   .MCLK(MCLK),
   .MCLKEN(mclken),

   .VALID(clk_regen_src_valid),
   .LAST(clk_regen_src_last),
   .NEXT(clk_regen_src_next),
   .DATA(clk_regen_src_data)
   );

hdmi_data_island_ecc_buf clk_regen_ecc
  (
   .CLK(PCLK),

   .IN_VALID(clk_regen_src_valid),
   .IN_LAST(clk_regen_src_last),
   .IN_NEXT(clk_regen_src_next),
   .IN_DATA(clk_regen_src_data),

   .OUT_VALID(clk_regen_ecc_valid),
   .OUT_LAST(clk_regen_ecc_last),
   .OUT_NEXT(clk_regen_ecc_next),
   .OUT_DATA(clk_regen_ecc_data)
   );

// Merge both packets sources.
hdmi_audio_pkt_mux mux
  (
   .PCLK(PCLK),
   .nRST(nRST),

   .SAMPLE_VALID(sample_ecc_valid),
   .SAMPLE_LAST(sample_ecc_last),
   .SAMPLE_NEXT(sample_ecc_next),
   .SAMPLE_DATA(sample_ecc_data),

   .CLK_REGEN_VALID(clk_regen_ecc_valid),
   .CLK_REGEN_LAST(clk_regen_ecc_last),
   .CLK_REGEN_NEXT(clk_regen_ecc_next),
   .CLK_REGEN_DATA(clk_regen_ecc_data),

   .OUT_VALID(VALID),
   .OUT_LAST(LAST),
   .OUT_NEXT(NEXT),
   .OUT_DATA(DATA)
   );

endmodule
