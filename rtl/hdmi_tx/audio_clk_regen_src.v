// Audio Clock Regeneration Packet (N / CTS) source
//
// N is chosen to yield a packet frequency of 128 * fs / N = 1 kHz
// (optimal).
//
// CTS (Cycle Time Stamp) is computed by counting the number of video
// clocks between packets. Ideal (instead of actual) packet
// transmission times are used, to prevent introducing jitter.

module hdmi_audio_clk_regen_src
  #(parameter AUDIO_SAMPLE_RATE = 48000)
  (
   input             PCLK,
   input             nRST,

   // (INPUT) Digital audio
   input             MCLK, // eg, 128 * 48 kHz = 6.144 MHz
   input             MCLKEN,

   // (OUTPUT) Audio Clock Regeneration packets
   output reg        VALID,
   output reg        LAST,
   input             NEXT,
   output reg [35:0] DATA
   );

//////////////////////////////////////////////////////////////////////
// Cycle Time Stamp

reg [13:0] cnt;                 // MCLK clock counter
reg [13:0] N;
reg [19:0] cts, cts_b, cts_bd;
(* ASYNC_REG = "TRUE" *)
reg        nrst_s, nrst_mclk;   // MCLK -> PCLK
reg        tick_d;              // MCLK
(* ASYNC_REG = "TRUE" *)
reg [3:0]  tick_pclk;           // MCLK -> PCLK

wire       go;
wire       tick = cnt == N - 1'd1;

// We support these values of N:
//
// - N = 4096 per Table 7-1 (32 kHz audio, 74.25 MHz video)
// - N = 6144 per Table 7-3 (48 kHz audio, 74.25 MHz video)

initial begin
  if (AUDIO_SAMPLE_RATE == 48000)
    N = 14'd6144;
  else if (AUDIO_SAMPLE_RATE == 32000)
    N = 14'd4096;
  else begin
    $display("unsupported AUDIO_SAMPLE_RATE %d", AUDIO_SAMPLE_RATE);
    $finish;
  end
end

always @(posedge MCLK) if (MCLKEN) begin
  // Sync to MCLK
  nrst_s <= nRST;
  nrst_mclk <= nrst_s;

  if (!nrst_mclk) begin
    cnt <= 0;
  end
  else begin
    if (tick)
      cnt <= 0;
    else
      cnt <= cnt + 1'd1;
  end

  // Safe input to tick_pclk synchronizer chain
  tick_d <= tick;
end

assign go = !tick_pclk[2] && tick_pclk[3]; // tick falling edge detector

always @(posedge PCLK) begin
  // Sync to PCLK
  tick_pclk <= (tick_pclk << 1) | tick_d;

  if (!nRST) begin
    cts_bd <= 0;
    cts_b <= 0;
    cts <= 1'd1;
  end
  else begin
    if (go) begin
      cts_bd <= cts_b;
      cts_b <= cts;
      cts <= 1'd1;              // resolves off-by-one error
    end
    else begin
      cts <= cts + 1'd1;
    end
  end
end

//////////////////////////////////////////////////////////////////////
// Packet generator

reg [19:0] cts_q;
reg [11:0] pkt [0:7];
reg [2:0]  idx;
reg        mask;

// Only one subpacket is defined, as all four are identical.
initial begin
  pkt[0] = 12'h100;
  pkt[1] = 12'h00x;             // CTS[19:16]
  pkt[2] = 12'h0xx;             // CTS[15:8]
  pkt[3] = 12'h0xx;             // CTS[7:0]
  pkt[4] = 12'h000;             // N[19:16]
  pkt[5] = 12'h00x;             // N[15:8]
  pkt[6] = 12'hxxx;             // N[7:0]
  pkt[7] = 12'hxxx;
end

always @*
  cts_q = cts_b;

always @* begin
  pkt[1][3:0] = cts_q[19:16];
  pkt[2][7:0] = cts_q[15:8];
  pkt[3][7:0] = cts_q[7:0];
  //pkt[4][7:0] = 8'b0;
  pkt[5][5:0] = N[13:8];
  pkt[6][7:0] = N[7:0];
end

always @(posedge PCLK) begin
  if (!nRST) begin
    VALID <= 1'b0;
    mask <= 1'b0;
  end
  else begin
    if (!VALID && go) begin
      if (!mask) begin
        // First CTS is bogus; drop it.
        mask <= 1'b1;
      end
      else begin
        idx <= 0;
        VALID <= 1'b1;
      end
    end
    else if (VALID && NEXT) begin
      if (LAST) begin
        VALID <= 1'b0;
      end
      else
        idx <= idx + 1'd1;
    end
  end
end

always @* begin
  if (!nRST || !VALID) begin
    DATA = 36'dx;
    LAST = 1'bx;
  end
  else begin
    DATA = { pkt[idx][11:8], {4{pkt[idx][7:0]}} };
    LAST = idx == 3'd7;
  end
end

//////////////////////////////////////////////////////////////////////
// (DEBUG) Output CTS as byte stream

reg dbg_go;
reg [23:0] dbg_cts_sr;

always @(posedge PCLK) begin
  dbg_go <= go;
  if (dbg_go)
    dbg_cts_sr <= cts_q;
  else
    dbg_cts_sr <= {dbg_cts_sr[15:0], 8'b0};
end

wire [7:0] dbg_cts_bs = dbg_cts_sr[23:16];

endmodule
