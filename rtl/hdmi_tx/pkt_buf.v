// Buffer an entire packet, then output it continuously (VALID always 1).

module hdmi_pkt_buf
  (
   input             CLK,

   // (INPUT) Data packet
   input             IN_VALID,
   input             IN_LAST,
   output reg        IN_NEXT,
   input [35:0]      IN_DATA,
   
   // (OUTPUT) Data packet
   output reg        OUT_VALID,
   output reg        OUT_LAST,
   input             OUT_NEXT,
   output reg [35:0] OUT_DATA
   );

reg [35:0] data_buf [0:7];
reg [2:0]  idx;
reg        out;                 // output phase

initial begin
  idx <= 0;
  out <= 0;
end

always @(posedge CLK) begin
  if (!out && IN_VALID && IN_NEXT) begin
    data_buf[idx] <= IN_DATA;
    idx <= idx + 1'd1;
    out <= IN_LAST;
  end
  else if (out && OUT_VALID && OUT_NEXT) begin
    idx <= idx + 1'd1;
    out <= !OUT_LAST;
  end
end

always @*
  IN_NEXT = IN_VALID && !out;

always @* begin
  if (out) begin
    OUT_VALID = 1'b1;
    OUT_LAST = idx == 4'd7;
    OUT_DATA = data_buf[idx];
  end
  else begin
    OUT_VALID = 1'b0;
    OUT_LAST = 1'bx;
    OUT_DATA = 36'dx;
  end
end

endmodule
