// Possible values of hdmi_encoder_mux_sel.SEL

`define ENCODER_MUX_SEL_M       4

// Static state
`define ENCODER_MUX_SEL_CTRL        5'b00001 // control
`define ENCODER_MUX_SEL_DATA        5'b00010 // data island
`define ENCODER_MUX_SEL_VIDEO       5'b00100 // video data

// Temporary preamble state
`define ENCODER_MUX_SEL_PRE_DATA    5'b01010 // data island preamble
`define ENCODER_MUX_SEL_PRE_VIDEO   5'b01100 // video data preamble

// Temporary guard-band state
`define ENCODER_MUX_SEL_GB_DATA     5'b10010 // control -> data island
`define ENCODER_MUX_SEL_GB_CTRL     5'b10001 // data island -> control
`define ENCODER_MUX_SEL_GB_VIDEO    5'b10100 // control -> video

// Selector classification
`define ENCODER_MUX_SEL_IS_PRE(s)   (s[3]) // is preamble
`define ENCODER_MUX_SEL_IS_GB(s)    (s[4]) // is guard-band
