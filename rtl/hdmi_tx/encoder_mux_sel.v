`timescale 1ns / 1ns

`include "encoder_mux.vh"

module hdmi_encoder_mux_sel
  (
   input                             CLK,
   input                             nRST,

   input                             MODE, // 0=DVI, 1=HDMI

   input [11:0]                      HBLANK_DCNT,
   input [11:0]                      VBLANK_DCNT,

   // Signals from hdmi_data_island_gen
   input                             DATA_IN_VALID,
   input                             DATA_OUT_VALID,
   input                             DATA_OUT_LAST,
   output reg                        DATA_OUT_NEXT,

   output reg [`ENCODER_MUX_SEL_M:0] SEL
   );

localparam [11:0] DATA_PKT_N = 12'd32;
localparam [11:0] GB_N = 12'd2;
localparam [3:0] CTRL_MIN_N = 4'd12;
localparam [3:0] PRE_N = 4'd8;

localparam [11:0] FIRST_DATA_MIN_HBLANK_DCNT = PRE_N + GB_N + DATA_PKT_N + 
                                               GB_N + CTRL_MIN_N + GB_N;
localparam [11:0] NEXT_DATA_MIN_HBLANK_DCNT = DATA_PKT_N + 
                                              GB_N + CTRL_MIN_N + GB_N;

wire hblank = HBLANK_DCNT > 12'd0;
wire vblank = VBLANK_DCNT > 12'd0;
wire de = !(hblank || vblank);
// ANDing both _VALID tells us when there's back-to-back packets.
wire data_valid = DATA_IN_VALID && DATA_OUT_VALID;
wire gb;
wire pkt_next;

reg [`ENCODER_MUX_SEL_M:0] last_sel;
reg [3:0]                  ctrl_dcnt;
reg [4:0]                  data_dcnt;
reg [4:0]                  pkt_dcnt;
reg                        gb_dly;
reg                        pkt_next_dly;
reg                        pkt_start, pkt_start_dly;

task assert(input test);
  begin
    if (test !== 1)
      begin
        $display("ASSERTION FAILED in %m @%1t ns", $time);
        $finish;
      end
  end
endtask

always @(posedge CLK) begin
  if (SEL == `ENCODER_MUX_SEL_CTRL) begin
    if (ctrl_dcnt > PRE_N)
      ctrl_dcnt <= ctrl_dcnt - 4'd1;
  end
  else if (`ENCODER_MUX_SEL_IS_PRE(SEL)) begin
    if (ctrl_dcnt > 4'd0)
      ctrl_dcnt <= ctrl_dcnt - 4'd1;
  end
  else
    ctrl_dcnt <= CTRL_MIN_N;
end

// Guard bands are 2 clocks wide.
assign gb = `ENCODER_MUX_SEL_IS_GB(last_sel) && `ENCODER_MUX_SEL_IS_GB(SEL);

always @(posedge CLK)
  gb_dly <= gb;

// data_dcnt counts down clocks within a packet.
always @(posedge CLK) begin
  if (pkt_start)
    data_dcnt <= 5'd31;
  else if (DATA_OUT_NEXT) begin
    if (data_dcnt > 1'd0) begin
      if (DATA_OUT_VALID !== 1'b1)
        begin
          $display("ASSERTION FAILED: DATA_OUT_VALID in %m @%1t ns", $time);
          $finish;
        end
      data_dcnt <= data_dcnt - 1'd1;
    end
    else
      data_dcnt <= 'dX;
  end

  if (DATA_OUT_NEXT)
    if (((data_dcnt > 1'd0 && !DATA_OUT_LAST) ||
         (data_dcnt == 1'd0 && DATA_OUT_LAST)) !== 1) begin
      $display("ASSERTION FAILED: DATA_OUT_LAST in %m @%1t ns", $time);
      $finish;
    end
end

assign pkt_next = data_dcnt == 1'd0 && SEL == `ENCODER_MUX_SEL_DATA;

always @(posedge CLK) begin
  pkt_next_dly <= pkt_next;
  pkt_start_dly <= pkt_start;
end

// pkt_dcnt tracks contiguous data packets. It is used to limit # of
// packets in one data island to 18 max.
always @(posedge CLK) begin
  if (SEL == `ENCODER_MUX_SEL_GB_DATA && gb)
    pkt_dcnt <= 5'd17;
  else if (pkt_next)
    if (pkt_start)
      pkt_dcnt <= pkt_dcnt - 1'd1;
    else
      pkt_dcnt <= 'dX;
end

always @*
  case (SEL)
    `ENCODER_MUX_SEL_GB_DATA:
      pkt_start = gb;
    `ENCODER_MUX_SEL_DATA:
      pkt_start = pkt_next && data_valid && pkt_dcnt > 1'd0 &&
                  (vblank || HBLANK_DCNT > NEXT_DATA_MIN_HBLANK_DCNT);
    default:
      pkt_start = 1'bx;
  endcase

always @(posedge CLK) begin
  if (!nRST)
    last_sel <= `ENCODER_MUX_SEL_CTRL;
  else
    last_sel <= SEL;
end

always @* begin
  SEL = last_sel;
  DATA_OUT_NEXT = 1'b0;

  if (!MODE && !de)
    // DVI uses only CTRL or VIDEO, strictly determined by DE.
    SEL = `ENCODER_MUX_SEL_CTRL;
  else if (MODE && !de) begin
    case (last_sel)
      `ENCODER_MUX_SEL_VIDEO:
        SEL = `ENCODER_MUX_SEL_CTRL;

      `ENCODER_MUX_SEL_CTRL:
        if (!vblank && hblank && HBLANK_DCNT == PRE_N + GB_N)
          SEL = `ENCODER_MUX_SEL_PRE_VIDEO;
        else if (data_valid && ctrl_dcnt == PRE_N
            && (vblank || HBLANK_DCNT >= FIRST_DATA_MIN_HBLANK_DCNT))
          SEL = `ENCODER_MUX_SEL_PRE_DATA;

      `ENCODER_MUX_SEL_PRE_DATA:
        if (ctrl_dcnt == 4'd0)
          SEL = `ENCODER_MUX_SEL_GB_DATA;

      `ENCODER_MUX_SEL_GB_DATA:
        if (pkt_start_dly) begin
          SEL = `ENCODER_MUX_SEL_DATA;
          DATA_OUT_NEXT = 1'b1;
        end

      `ENCODER_MUX_SEL_DATA: begin
        if (!pkt_next_dly)
          DATA_OUT_NEXT = 1'b1;
        else begin
          if (pkt_start_dly) begin
            DATA_OUT_NEXT = 1'b1;
            SEL = `ENCODER_MUX_SEL_DATA;
          end
          else
            SEL = `ENCODER_MUX_SEL_GB_CTRL;
        end
      end

      `ENCODER_MUX_SEL_GB_CTRL:
        if (gb_dly)
          SEL = `ENCODER_MUX_SEL_CTRL;

      `ENCODER_MUX_SEL_PRE_VIDEO:
        if (hblank && HBLANK_DCNT == GB_N)
          SEL = `ENCODER_MUX_SEL_GB_VIDEO;
    endcase
  end
  else if (de)
    SEL = `ENCODER_MUX_SEL_VIDEO;
  else
    SEL = 'dX;
end

endmodule
