// The HDMI TX pipeline

`include "encoder_mux.vh"

module hdmi_tx
  #(parameter AUDIO_SAMPLE_RATE = 48000)
  (
   input        PCLK,
   input        RESET,
   
   // (INPUT) TX Control
   input        MODE, // 0=DVI, 1=HDMI
   input        AUD_EN, // enable audio packets

   // (INPUT) Digital audio
   input [2:0]  MCLK_DIV, // divider: N-1
   input        MCLK, // 128 * 48 kHz = 6.144 MHz
   input        FCLK, // frame clock, PDIN launched on fall
   input [15:0] PDINL, // parallel sample, left channel
   input [15:0] PDINR, // parallel sample, right channel

   // (INPUT) Baseband video
   input [7:0]  R,
   input [7:0]  G,
   input [7:0]  B,
   input        HS,
   input        VS,
   input        DE,
   input [11:0] HBLANK_DCNT,
   input [11:0] VBLANK_DCNT,

   // (OUTPUT) to HDMI TMDS serializeer
   output [29:0] SEROUT
   );

wire [`ENCODER_MUX_SEL_M:0] enc_mux_sel;
wire [35:0] info_frame_data, audio_data, in_pkt_gen_data;
wire [2:2]  da;
wire [3:0]  db, dc;
wire [7:0]  enc0_d, enc1_d, enc2_d;
wire [4:0]  enc0_c, enc1_c, enc2_c;
wire [9:0]  enc0_q, enc1_q, enc2_q;

hdmi_info_frame_src info_frame_pkt_src
  (
   .CLK(PCLK),
   .nRST(info_frame_nrst),
   .AUD_EN(AUD_EN),
   .VALID(info_frame_valid),
   .LAST(info_frame_last),
   .NEXT(info_frame_next),
   .DATA(info_frame_data)
   );

hdmi_data_island_pkt_mux data_island_pkt_mux
  (
   .CLK(PCLK),
   .nRST(~RESET && MODE),

   .VBLANK_DCNT(VBLANK_DCNT),

   .INFO_FRAME_nRST(info_frame_nrst),
   .INFO_FRAME_VALID(info_frame_valid),
   .INFO_FRAME_LAST(info_frame_last),
   .INFO_FRAME_NEXT(info_frame_next),
   .INFO_FRAME_DATA(info_frame_data),

   .AUDIO_VALID(audio_valid),
   .AUDIO_LAST(audio_last),
   .AUDIO_NEXT(audio_next),
   .AUDIO_DATA(audio_data),

   .OUT_VALID(in_pkt_gen_valid),
   .OUT_LAST(in_pkt_gen_last),
   .OUT_NEXT(in_pkt_gen_next),
   .OUT_DATA(in_pkt_gen_data)
   );

hdmi_data_island_gen data_island_gen
  (
   .CLK(PCLK),
   .nRST(~RESET && MODE),

   .IN_VALID(in_pkt_gen_valid),
   .IN_LAST(in_pkt_gen_last),
   .IN_NEXT(in_pkt_gen_next),
   .IN_DATA(in_pkt_gen_data),

   .OUT_VALID(out_pkt_gen_valid),
   .OUT_LAST(out_pkt_gen_last),
   .OUT_NEXT(out_pkt_gen_next),
   .OUT_DA(da),
   .OUT_DB(db),
   .OUT_DC(dc)
   );

hdmi_audio_src #(AUDIO_SAMPLE_RATE) audio_src
  (
   .PCLK(PCLK),
   .nRST(~RESET && MODE && AUD_EN),

   .MCLK_DIV(MCLK_DIV),
   .MCLK(MCLK),
   .FCLK(FCLK),
   .PDINL(PDINL),
   .PDINR(PDINR),

   .VALID(audio_valid),
   .LAST(audio_last),
   .NEXT(audio_next),
   .DATA(audio_data)
   );

hdmi_encoder_mux_sel encoder_mux_sel
  (
   .CLK(PCLK),
   .nRST(~RESET),

   .MODE(MODE),

   .HBLANK_DCNT(HBLANK_DCNT),
   .VBLANK_DCNT(VBLANK_DCNT),

   .DATA_IN_VALID(in_pkt_gen_valid),
   .DATA_OUT_VALID(out_pkt_gen_valid),
   .DATA_OUT_LAST(out_pkt_gen_last),
   .DATA_OUT_NEXT(out_pkt_gen_next),

   .SEL(enc_mux_sel)
   );

hdmi_encoder_mux encoder_mux
  (
   .SEL(enc_mux_sel),

   .R(R),
   .G(G),
   .B(B),
   .VDE(DE),
   .HSYNC(HS),
   .VSYNC(VS),

   .DA(da),
   .DB(db),
   .DC(dc),

   .D0(enc0_d),
   .D1(enc1_d),
   .D2(enc2_d),
   .DE(enc_de),
   .C0(enc0_c),
   .C1(enc1_c),
   .C2(enc2_c),
   .TERC4(enc_terc4)
   );

hdmi_encoder enc0
  (
   .CLK(PCLK),

   .D(enc0_d),
   .C(enc0_c),
   .DE(enc_de),
   .TERC4(enc_terc4),

   .Q_OUT(enc0_q)
   );

hdmi_encoder enc1
  (
   .CLK(PCLK),

   .D(enc1_d),
   .C(enc1_c),
   .DE(enc_de),
   .TERC4(enc_terc4),

   .Q_OUT(enc1_q)
   );

hdmi_encoder enc2
  (
   .CLK(PCLK),

   .D(enc2_d),
   .C(enc2_c),
   .DE(enc_de),
   .TERC4(enc_terc4),

   .Q_OUT(enc2_q)
   );

assign SEROUT = {
                 enc2_q[9], enc1_q[9], enc0_q[9],
                 enc2_q[8], enc1_q[8], enc0_q[8],
                 enc2_q[7], enc1_q[7], enc0_q[7],
                 enc2_q[6], enc1_q[6], enc0_q[6],
                 enc2_q[5], enc1_q[5], enc0_q[5],
                 enc2_q[4], enc1_q[4], enc0_q[4],
                 enc2_q[3], enc1_q[3], enc0_q[3],
                 enc2_q[2], enc1_q[2], enc0_q[2],
                 enc2_q[1], enc1_q[1], enc0_q[1],
                 enc2_q[0], enc1_q[0], enc0_q[0]
                 };

endmodule
