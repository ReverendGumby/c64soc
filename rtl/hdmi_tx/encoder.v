module hdmi_encoder
  (
   input            CLK,

   input [7:0]      D,
   input            DE,
   input [4:0]      C,
   input            TERC4,

   output reg [9:0] Q_OUT
   );

// For Control Period Coding (TERC4=0), C[1:0] carry HSYNC/VSYNC or CTL0-1 or CTL2-3
// For TERC4 Coding (TERC4=1), C[3:0] carry 4 data bits per channel.
// For Guard Band Coding, TERC4=1, and C[4:0] select 10-bit character.

`define DM   ($bits(D) + $bits(DE) + $bits(C))
`define N1M  10
`define QM   9

reg [`DM:0]  d [0:17];
reg [`N1M:0] n1_d [0:8];        // N1(D), one-hot {0, 1, ... `N1M }
reg [`QM:0]  qm [9:18];
reg [`N1M:0] n1_qm [9:17];      // N1(qm)
reg [`N1M:0] cnt;               // average N1(q)

// Indexes into d[]
`define D_DE    ($bits(D))
`define D_C0    (`D_DE + $bits(DE))
`define D_CN    (`D_C0 + $bits(C) - 1)
`define D_TERC4 (`D_C0 + $bits(C))

function [`N1M:0] n1
  (
   input [`N1M:0] n1_old,
   input b
   );
  begin
    if (b)
      n1 = n1_old << 1;
    else
      n1 = n1_old;
  end
endfunction

integer t;
`define NEXT t = t + 1

`define nop(r)          r[t] <= r[t-1]
`define init_n1_d       n1_d[t] <= 1'b1;
`define comp_n1_d(b)    n1_d[t] <= n1(n1_d[t-1], d[t-1][b])
`define init_n1_qm      n1_qm[t] <= 1'b1;
`define comp_n1_qm(N)   n1_qm[t] <= n1(n1_qm[t-1], qm[t-1][N])
`define comp_qm(N)      qm[t] <= `_comp_qm(qm[t-1], d[t-1], N)

task intake;
  begin
    d[t][7:0] <= D;
    d[t][`D_DE] <= DE;
    d[t][`D_CN:`D_C0] <= C;
    d[t][`D_TERC4] <= TERC4;
  end
endtask

task init_qm;
  begin
    qm[t][0] <= d[t-1][0];
    qm[t][7:1] <= 7'bxxxxxxx;   // will be set down the pipe
    // qm[8]: indicates XNOR versus XOR.
    qm[t][8] <= ~(|n1_d[t-1][8:5] || n1_d[t-1][4] && d[t-1][0] == 1'b0);
    // qm[9]: will indicate reversal by DC balancer.
    qm[t][9] <= 1'b0;
  end
endtask

// If qm[8] is 1: qm[N] = qm[N-1] XOR D[N]
// If qm[8] is 0: qm[N] = qm[N-1] XNOR D[N]
// nop other bits
`define _comp_qm(qm, d, N) { qm[`QM:N+1], qm[N-1] ^ d[N] ^ ~qm[8], qm[N-1:0] }

function [`N1M:0] new_cnt
  (
   input [`N1M:0] cnt,
   input [`N1M:0] n1_qm
   );
  begin
    case (n1_qm)
      11'b00000000001: new_cnt = cnt >> 5;
      11'b00000000010: new_cnt = cnt >> 4;
      11'b00000000100: new_cnt = cnt >> 3;
      11'b00000001000: new_cnt = cnt >> 2;
      11'b00000010000: new_cnt = cnt >> 1;
      11'b00000100000: new_cnt = cnt;
      11'b00001000000: new_cnt = cnt << 1;
      11'b00010000000: new_cnt = cnt << 2;
      11'b00100000000: new_cnt = cnt << 3;
      11'b01000000000: new_cnt = cnt << 4;
      11'b10000000000: new_cnt = cnt << 5;
      default: new_cnt = {`N1M+1{1'bx}};
    endcase
  end
endfunction

function [8:0] reverse_8
  (
   input [8:0] v
   );
  begin
    reverse_8 = { v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[8] };
  end
endfunction

function [9:0] coding
  (
   input [4:0] c,
   input terc4
   );
  begin
    coding = 10'bxxxxxxxxxx;
    if (terc4 == 1'b0)
      case (c[1:0])
        2'b00: coding = 10'b1101010100;
        2'b01: coding = 10'b0010101011;
        2'b10: coding = 10'b0101010100;
        2'b11: coding = 10'b1010101011;
        default: ;
      endcase
    else if (terc4 == 1'b1) begin
      case (c[3:0])
        4'b0000: coding = 10'b1010011100;
        4'b0001: coding = 10'b1001100011;
        4'b0010: coding = 10'b1011100100;
        4'b0011: coding = 10'b1011100010;
        4'b0100: coding = 10'b0101110001;
        4'b0101: coding = 10'b0100011110;
        4'b0110: coding = 10'b0110001110;
        4'b0111: coding = 10'b0100111100;
        4'b1000: coding = 10'b1011001100;
        4'b1001: coding = 10'b0100111001;
        4'b1010: coding = 10'b0110011100;
        4'b1011: coding = 10'b1011000110;
        4'b1100: coding = 10'b1010001110;
        4'b1101: coding = 10'b1001110001;
        4'b1110: coding = 10'b0101100011;
        4'b1111: coding = 10'b1011000011;
        default: ;
      endcase
      if (c[4])
        coding = ~coding;
    end
  end
endfunction

task balance;
  begin
    if (!d[t-1][`D_DE]) begin
      qm[t] <= coding(d[t-1][`D_CN:`D_C0], d[t-1][`D_TERC4]);
      cnt <= { 1'b1, {`N1M/2{1'b0}} };
    end
    else if (((cnt[`N1M/2] || n1_qm[t-1][4]) && ~qm[t-1][8]) ||
             (|cnt[`N1M:`N1M/2+1] && |n1_qm[t-1][8:5]) ||
             (|cnt[`N1M/2-1:0] && |n1_qm[t-1][3:0])) begin
      qm[t][9] <= 1'b1;
      qm[t][8] <= qm[t-1][8];
      qm[t][7:0] <= ~qm[t-1][7:0];
      cnt <= new_cnt(cnt, n1(n1(reverse_8(n1_qm[t-1][8:0]), qm[t-1][8]), 1'b1));
    end
    else begin
      qm[t][9] <= 1'b0;
      qm[t][8] <= qm[t-1][8];
      qm[t] <= qm[t-1];
      cnt <= new_cnt(cnt, n1(n1(n1_qm[t-1], qm[t-1][8]), 1'b0));
    end
  end
endtask

always @(posedge CLK) begin
  t = 0;
  intake(); `init_n1_d; `NEXT;                  // t=0

  // Phase 1: Compute N1(D).
  `comp_n1_d(0); `nop(d); `NEXT;                // t=1
  `comp_n1_d(1); `nop(d); `NEXT;                // t=2
  `comp_n1_d(2); `nop(d); `NEXT;                // t=3
  `comp_n1_d(3); `nop(d); `NEXT;                // t=4
  `comp_n1_d(4); `nop(d); `NEXT;                // t=5
  `comp_n1_d(5); `nop(d); `NEXT;                // t=6
  `comp_n1_d(6); `nop(d); `NEXT;                // t=7
  `comp_n1_d(7); `nop(d); `NEXT;                // t=8

  // Phase 2: Minimize transitions.
  // - qm: Serially X(N)OR D[7:0]
  // - Compute N1(qm[7:0])
  init_qm();   `init_n1_qm;    `nop(d); `NEXT;  // t=9
  `comp_qm(1); `comp_n1_qm(0); `nop(d); `NEXT;  // t=10
  `comp_qm(2); `comp_n1_qm(1); `nop(d); `NEXT;  // t=11
  `comp_qm(3); `comp_n1_qm(2); `nop(d); `NEXT;  // t=12
  `comp_qm(4); `comp_n1_qm(3); `nop(d); `NEXT;  // t=13
  `comp_qm(5); `comp_n1_qm(4); `nop(d); `NEXT;  // t=14
  `comp_qm(6); `comp_n1_qm(5); `nop(d); `NEXT;  // t=15
  `comp_qm(7); `comp_n1_qm(6); `nop(d); `NEXT;  // t=16
  `nop(qm);    `comp_n1_qm(7); `nop(d); `NEXT;  // t=17

  // Phase 3: Balance DC offset.
  balance(); `NEXT;                             // t=18

  Q_OUT <= qm[t-1];
end

endmodule
