module hdmi_data_island_gen
  (
   input            CLK,
   input            nRST,

   // (INPUT) Data packet
   input            IN_VALID,
   input            IN_LAST,
   output reg       IN_NEXT,
   input [35:0]     IN_DATA,
   
   // (OUTPUT) Pixel data for each TMDS channel
   output reg       OUT_VALID,
   output reg       OUT_LAST,
   input            OUT_NEXT,
   output reg [2:2] OUT_DA,     // channel 0
   output reg [3:0] OUT_DB,     // channel 1
   output reg [3:0] OUT_DC      // channel 2
   );

// Use shift registers for each channel. Load them when empty and
// source is valid. Clock them when sink receives the LSBs.

reg [3:0] sc;                 // counter: 0s when empty, 1s after load
reg [3:0] sl;                 // last clock
reg [3:0] da;
reg [3:0] db [0:3];
reg [3:0] dc [0:3];

wire nearly_empty = sc == 4'b0001;
wire now_empty = !OUT_VALID;
wire load = IN_VALID && (now_empty || (OUT_NEXT && nearly_empty));

always @(posedge CLK) begin
  if (!nRST) begin
    sc <= 0;
    sl <= 0;
  end
  else begin
    if (load) begin
      // Load shift registers
      sc <= {4{1'b1}};
      sl <= {IN_LAST, 3'b000};
      // Pattern derived from HDMI spec. figure 5-4 and our packet format.
      db[0] <= { IN_DATA[06], IN_DATA[04], IN_DATA[02], IN_DATA[00] };
      dc[0] <= { IN_DATA[07], IN_DATA[05], IN_DATA[03], IN_DATA[01] };
      db[1] <= { IN_DATA[14], IN_DATA[12], IN_DATA[10], IN_DATA[08] };
      dc[1] <= { IN_DATA[15], IN_DATA[13], IN_DATA[11], IN_DATA[09] };
      db[2] <= { IN_DATA[22], IN_DATA[20], IN_DATA[18], IN_DATA[16] };
      dc[2] <= { IN_DATA[23], IN_DATA[21], IN_DATA[19], IN_DATA[17] };
      db[3] <= { IN_DATA[30], IN_DATA[28], IN_DATA[26], IN_DATA[24] };
      dc[3] <= { IN_DATA[31], IN_DATA[29], IN_DATA[27], IN_DATA[25] };
      da <= IN_DATA[35:32];
    end
    else if (OUT_NEXT) begin
      // Clock shift registers
      sc <= sc >> 1;
      sl <= sl >> 1;
      da <= da >> 1;
      db[0] <= db[0] >> 1;
      db[1] <= db[1] >> 1;
      db[2] <= db[2] >> 1;
      db[3] <= db[3] >> 1;
      dc[0] <= dc[0] >> 1;
      dc[1] <= dc[1] >> 1;
      dc[2] <= dc[2] >> 1;
      dc[3] <= dc[3] >> 1;
    end
  end
end

// Drive out LSBs to the sink.

always @* begin
  IN_NEXT = nRST && load;
  OUT_VALID = sc[0];
  OUT_LAST  = sl[0];
  OUT_DA[2] = da[0];
  OUT_DB[0] = db[0][0];
  OUT_DB[1] = db[1][0];
  OUT_DB[2] = db[2][0];
  OUT_DB[3] = db[3][0];
  OUT_DC[0] = dc[0][0];
  OUT_DC[1] = dc[1][0];
  OUT_DC[2] = dc[2][0];
  OUT_DC[3] = dc[3][0];
end

endmodule
