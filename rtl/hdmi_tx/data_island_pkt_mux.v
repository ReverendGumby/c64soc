module hdmi_data_island_pkt_mux
  (
   input             CLK,
   input             nRST,

   input [11:0]      VBLANK_DCNT,

   // (INPUT) InfoFrame packet
   output reg        INFO_FRAME_nRST,
   input             INFO_FRAME_VALID,
   input             INFO_FRAME_LAST,
   output reg        INFO_FRAME_NEXT,
   input [35:0]      INFO_FRAME_DATA,

   // (INPUT) Audio packet
   input             AUDIO_VALID,
   input             AUDIO_LAST,
   output reg        AUDIO_NEXT,
   input [35:0]      AUDIO_DATA,

   // MUX output
   output reg        OUT_VALID,
   output reg        OUT_LAST,
   input             OUT_NEXT,
   output reg [35:0] OUT_DATA
   );

localparam [11:0] INFO_FRAME_VBLANK_DCNT = 12'd5;

reg info_frame_sent;
reg sel_info_frame;

wire pkt_sent;
wire is_info_frame_raster;
wire info_frame_active, audio_active;

// InfoFrames are transmitted every frame on a specific raster.
assign is_info_frame_raster = VBLANK_DCNT == INFO_FRAME_VBLANK_DCNT;

assign info_frame_active = sel_info_frame && INFO_FRAME_VALID;
assign audio_active = !sel_info_frame && AUDIO_VALID;

always @(posedge CLK) begin
  if (!nRST) begin
    sel_info_frame <= 1'b0;
    info_frame_sent <= 1'b0;
  end
  else begin
    // Track whether we've sent InfoFrames.
    if (is_info_frame_raster) begin
      if (!info_frame_sent && info_frame_active)
        info_frame_sent <= 1'b1;
    end
    else begin
      info_frame_sent <= 1'b0;
    end

    // If we're not sending an InfoFrame packet, then send audio packets.
    if (!is_info_frame_raster) begin
      if (sel_info_frame && !info_frame_active)
        sel_info_frame <= 1'b0;
    end
    else if (is_info_frame_raster) begin
      if (!sel_info_frame) begin
        if (!info_frame_sent && !audio_active)
          sel_info_frame <= 1'b1;
      end
      else begin
        if (info_frame_sent && !info_frame_active)
          sel_info_frame <= 1'b0;
      end
    end
  end
end

always @* begin
  if (sel_info_frame) begin
    INFO_FRAME_nRST = 1'b1;
    INFO_FRAME_NEXT = OUT_NEXT;
    AUDIO_NEXT = 1'b0;
    OUT_VALID = INFO_FRAME_VALID;
    OUT_LAST = INFO_FRAME_LAST;
    OUT_DATA = INFO_FRAME_DATA;
  end
  else begin
    INFO_FRAME_nRST = 1'b0;
    INFO_FRAME_NEXT = 1'b0;
    AUDIO_NEXT = OUT_NEXT;
    OUT_VALID = AUDIO_VALID;
    OUT_LAST = AUDIO_LAST;
    OUT_DATA = AUDIO_DATA;
  end
end

endmodule
