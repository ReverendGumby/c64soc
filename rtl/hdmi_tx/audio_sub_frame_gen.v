// Audio sub-frame generator
//
// A sub-frame carries an audio sample word for one channel.

module hdmi_audio_sub_frame_gen
  (
   input         PCLK,
   input         nRST,

   // (INPUT) Sample data and control
   input [15:0]  SB0,
   input         CS,            // channel status bit
   input         GO,

   // (OUTPUT) Sub-frame data
   output [31:4] SF
   );

reg [31:4] sf;
reg [5:0]  sf_sr;
reg        as_p, as_pb;

always @* begin
  sf[11:4] = 8'd0;              // unused sample bits
  sf[27:12] = SB0[15:0];        // 16-bit sample
  sf[28] = 1'b0;                // validity flag: 0 = reliable
  sf[29] = 1'b0;                // user data: unused
  sf[30] = CS;                  // channel status
  sf[31] = as_pb;               // parity
end

// Compute even parity of sub-frame bits 4 - 31. (Parity is bit 31.)
always @(posedge PCLK) begin
  if (!nRST)
    sf_sr <= 0;
  else begin
    if (GO)
      sf_sr[0] <= 1'b1;
    else
      sf_sr <= sf_sr << 1;
  end
end

always @*
  case (sf_sr)
    6'b000001: as_p = 1'b0;     // make even
    6'b000010: as_p = as_pb ^ ^sf[11:4];
    6'b000100: as_p = as_pb ^ ^sf[19:12];
    6'b001000: as_p = as_pb ^ ^sf[27:20];
    6'b010000: as_p = as_pb ^ ^sf[30:28];
    6'b100000: as_p = as_pb;
    default: as_p = 1'bx;
  endcase

always @(posedge PCLK) if (|sf_sr)
  as_pb <= as_p;

assign SF = sf;

endmodule
