// InfoFrame Packet source

module hdmi_info_frame_src
  (
   input             CLK,
   input             nRST,

   input             AUD_EN,    // enable audio packets

   output reg        VALID,
   output reg        LAST,
   input             NEXT,
   output reg [35:0] DATA
   );

reg [35:0] avi_info_frame [0:7];
reg [35:0] audio_info_frame [0:7];
reg [2:0]  idx;
reg        go;
reg        sel;

initial begin
  // AVI InfoFrame fields:
  //   Y=0        # RGB
  //   A=0        # Active Format Information: not present
  //   B=0        # Bar Data not present
  //   S=2        # Scan information: underscanned
  //   C=0        # Colorimetry: No Data
  //   M=2        # 16:9
  //   R=0        # Active Portion AR: unused
  //   SC=0       # Non-Uniform Picture Scaling: none
  //   VIC=0      # Undefined video format
  //   PR=0       # Pixel Repetition Factor: no rep.
  avi_info_frame[0] = 36'h20000004d;
  avi_info_frame[1] = 36'h800000002;
  avi_info_frame[2] = 36'h200000020;
  avi_info_frame[3] = 36'h000000000;
  avi_info_frame[4] = 36'hd00000000;
  avi_info_frame[5] = 36'h000000000;
  avi_info_frame[6] = 36'h400000000;
  avi_info_frame[7] = 36'he0000003c;

  // Audio InfoFrame fields:
  //   CC=1       # Channel Count: 2
  //   CA=0       # Channel Allocation: 1=FL, 2=FR
  //   LSV=0      # Level Shift Value: 0dB
  audio_info_frame[0] = 36'h400000070;
  audio_info_frame[1] = 36'h800000001;
  audio_info_frame[2] = 36'h100000000;
  audio_info_frame[3] = 36'h000000000;
  audio_info_frame[4] = 36'ha00000000;
  audio_info_frame[5] = 36'h000000000;
  audio_info_frame[6] = 36'ha00000000;
  audio_info_frame[7] = 36'h40000007d;
end

always @(posedge CLK) begin
  if (!nRST) begin
    idx <= 0;
    sel <= 0;
    go <= 1'b1;
  end
  else if (NEXT) begin
    if (LAST) begin
      idx <= 0;
      if (AUD_EN) begin
        sel <= ~sel;
        if (sel == 1'b1)
          go <= 1'b0;
      end
      else begin
        // skip sending Audio InfoFrame
        go <= 1'b0;
      end
    end
    else
      idx <= idx + 1'd1;
  end
end

always @* begin
  if (!nRST || !go) begin
    VALID = 1'b0;
    DATA = 36'dx;
    LAST = 1'bx;
  end
  else begin
    if (sel)
      DATA = audio_info_frame[idx];
    else
      DATA = avi_info_frame[idx];
    LAST = idx == 3'd7;
    VALID = 1'b1;
  end
end

endmodule
