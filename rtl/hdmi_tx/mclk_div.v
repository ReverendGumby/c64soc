// MCLK divider. Everything downstream uses MCLK directly, but only if
// MCLKEN. Synchronizes with FCLK, such that MCLKEN is 1 on FCLK
// falling edge.
//
// always @(posedge MCLK) if (MCLKEN) ...

module mclk_div
  (
   input [2:0] MCLK_DIV,        // divider: N-1
   input       MCLK,
   input       FCLK,            // frame clock

   output      MCLKEN           // MCLK enable
   );

reg [7:0] sr;
reg       fclk_d;
reg       tick;

// FCLK falling edge detector
always @(posedge MCLK) fclk_d <= FCLK;
wire fclk_fe = fclk_d && ~FCLK;

wire fclk_sync = fclk_fe && MCLKEN; // FCLK sync detector

initial sr = 0;

always @*
  tick = sr[MCLK_DIV];

// Enable shift register, except if FCLK sync detector fails on FCLK
// falling edge.
wire sren = !fclk_fe || (fclk_fe && fclk_sync);

always @(posedge MCLK) if (sren)
  sr <= (~|sr || tick) ? 1'b1 : sr << 1;

assign MCLKEN = tick;

endmodule
