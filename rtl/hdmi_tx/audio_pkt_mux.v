// Merge Audio packet sources. Prioritize sample packets.

module hdmi_audio_pkt_mux
  (
   input             PCLK,
   input             nRST,

   // (INPUT) Audio Sample packets
   input             SAMPLE_VALID,
   input             SAMPLE_LAST,
   output reg        SAMPLE_NEXT,
   input [35:0]      SAMPLE_DATA,

   // (INPUT) Audio Clock Regeneration packets
   input             CLK_REGEN_VALID,
   input             CLK_REGEN_LAST,
   output reg        CLK_REGEN_NEXT,
   input [35:0]      CLK_REGEN_DATA,

   // (OUTPUT) Audio packets
   output reg        OUT_VALID,
   output reg        OUT_LAST,
   input             OUT_NEXT,
   output reg [35:0] OUT_DATA
   );

wire sample_active, clk_regen_active;

reg sel_sample;

assign sample_active = sel_sample && SAMPLE_VALID &&
                       !(SAMPLE_LAST && SAMPLE_NEXT);
assign clk_regen_active = !sel_sample && CLK_REGEN_VALID &&
                          !(CLK_REGEN_LAST && CLK_REGEN_NEXT);

always @(posedge PCLK) begin
  if (!nRST) begin
    sel_sample <= 1'b1;
  end
  else begin
    // Switch packet sources when idle.
    if (sel_sample && !sample_active && CLK_REGEN_VALID)
      sel_sample <= 1'b0;
    else if (!sel_sample && !clk_regen_active)
      sel_sample <= 1'b1;
  end
end

always @* begin
  if (sel_sample) begin
    SAMPLE_NEXT = OUT_NEXT;
    CLK_REGEN_NEXT = 1'b0;
    OUT_VALID = SAMPLE_VALID;
    OUT_LAST = SAMPLE_LAST;
    OUT_DATA = SAMPLE_DATA;
  end
  else begin
    SAMPLE_NEXT = 1'b0;
    CLK_REGEN_NEXT = OUT_NEXT;
    OUT_VALID = CLK_REGEN_VALID;
    OUT_LAST = CLK_REGEN_LAST;
    OUT_DATA = CLK_REGEN_DATA;
  end
end

endmodule
