module hdmi_data_island_bch #(parameter DW = 8)
  (
   input            CLK,
   input            RST,
   input            VALID,
   output reg       NEXT,
   input [DW-1:0]   DIN,
   output reg [7:0] PARITY
   );

reg [DW-1:0] sc;        // shift counter: 0s when empty, 1s after load
reg [DW-1:0] sr;        // DIN shift register

wire di = sr[0];                // "data input" to parity calculator
wire s = PARITY[0] ^ di;        // syndrome
  
always @(posedge CLK) begin
  if (RST) begin
    PARITY <= 0;
    sc <= 0;
  end
  else begin
    if (VALID && NEXT) begin
      sr <= DIN;
      sc <= {DW{1'b1}};
    end
    else if (sc[0]) begin
      sr <= sr >> 1;
      sc <= sc >> 1;
    end
    if (sc[0])
      // Polynomial: x^8 + x^7 + x^6 + 1 --> 8'h83
      // (lowest order polynomial is MSB)
      PARITY <= (PARITY >> 1) ^ (8'h83 * s);
  end
end

always @* begin
  NEXT <= !RST && !sc[1];
end

endmodule
