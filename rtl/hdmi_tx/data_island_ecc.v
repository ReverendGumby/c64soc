// Add BCH ECC parity to the header and each subpacket. Both input and
// output packets are 8 words long. The input packet reserves space
// for the parity bits; their value is unimportant.

module hdmi_data_island_ecc
  (
   input             CLK,

   // (INPUT) Data packet
   input             IN_VALID,
   input             IN_LAST,
   output reg        IN_NEXT,
   input [35:0]      IN_DATA,
   
   // (OUTPUT) Data packet with ECC parity
   output reg        OUT_VALID,
   output reg        OUT_LAST,
   input             OUT_NEXT,
   output reg [35:0] OUT_DATA
   );

reg [35:0] data_buf;
reg [3:0]  wcnt, next_wcnt;      // Word counter
reg        bch_rst;

wire [7:0] parity [0:4];
wire       sp_next, hb_next;

initial begin
  wcnt <= 4'd8;
  bch_rst <= 1'd1;
end

wire bch_next = sp_next && hb_next;
wire start = IN_VALID && wcnt == 4'd8;
wire load = (start || IN_VALID && OUT_NEXT) && bch_next;
wire done = wcnt == 4'd7 && OUT_NEXT && bch_next;

always @* begin
  if (start && !bch_rst)
    next_wcnt = 4'd0;
  else if (load || done)
    next_wcnt = wcnt + 1'd1;
  else
    next_wcnt = wcnt;
end

always @(posedge CLK) begin
  if (load || done) begin
    data_buf <= IN_DATA;
    OUT_LAST <= IN_LAST;
  end

  if (start && bch_rst)
    bch_rst <= 1'b0;
  else if (done)
    bch_rst <= 1'b1;

  wcnt <= next_wcnt;
end

wire hb_valid = load && next_wcnt < 4'd6;
wire sp_valid = load && next_wcnt < 4'd7;

hdmi_data_island_bch #(8) sp0
  (
   .CLK(CLK),
   .RST(bch_rst),
   .VALID(sp_valid),
   .NEXT(sp_next),
   .DIN(IN_DATA[07:00]),
   .PARITY(parity[0])
   );

hdmi_data_island_bch #(8) sp1
  (
   .CLK(CLK),
   .RST(bch_rst),
   .VALID(sp_valid),
   .NEXT(),
   .DIN(IN_DATA[15:08]),
   .PARITY(parity[1])
   );

hdmi_data_island_bch #(8) sp2
  (
   .CLK(CLK),
   .RST(bch_rst),
   .VALID(sp_valid),
   .NEXT(),
   .DIN(IN_DATA[23:16]),
   .PARITY(parity[2])
   );

hdmi_data_island_bch #(8) sp3
  (
   .CLK(CLK),
   .RST(bch_rst),
   .VALID(sp_valid),
   .NEXT(),
   .DIN(IN_DATA[31:24]),
   .PARITY(parity[3])
   );

hdmi_data_island_bch #(4) hb
  (
   .CLK(CLK),
   .RST(bch_rst),
   .VALID(hb_valid),
   .NEXT(hb_next),
   .DIN(IN_DATA[35:32]),
   .PARITY(parity[4])
   );

always @* begin
  IN_NEXT = load;
  OUT_VALID = !start && bch_next;

  OUT_DATA = data_buf;
  if (wcnt == 4'd6)
    OUT_DATA[35:32] = parity[4][3:0];
  else if (wcnt == 4'd7) begin
    OUT_DATA[07:00] = parity[0][7:0];
    OUT_DATA[15:08] = parity[1][7:0];
    OUT_DATA[23:16] = parity[2][7:0];
    OUT_DATA[31:24] = parity[3][7:0];
    OUT_DATA[35:32] = parity[4][7:4];
  end
end

endmodule
