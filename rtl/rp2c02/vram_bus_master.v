module rp2c02_vram_bus_master
  (
   input             CLK,
   input             CC,
   input             nCC,
   input             RES2,
   input             HOLD,

   // debug monitor
   input [8:2]       MON_SEL,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,

   input [8:0]       ROW,

   input [7:0]       IO_D_I,
   output [7:0]      IO_D_O,
   output            IO_D_OE,
   input             RW,
   input             nCS,

   input [9:0]       REG_WE,
   input [9:0]       REG_RE,
   input             SPR_SIZE,
   input             BKG_PAT,
   input             SPR_PAT,
   input             ADDR_INC,

   input             RENDERING,
   input             HINC,
   input             VINC,
   input             HCOPY,
   input             VCOPY,
   input             FETCH,
   input             FETCH_NT,
   input             FETCH_AT,
   input             FETCH_PTL,
   input             FETCH_PTH,
   input             FETCH_RD,
   input             SPR_FETCH,
   output [14:0]     VAC,

   input [7:0]       SD,

   input [7:0]       AD_I,
   output reg [13:0] AD_O,
   output            AD_OE,
   output reg        ALE,
   output            nR,
   output            nW,
   output            ARW_OE
   );

`include "timings.vh"
`include "reg_idx.vh"

// clock delay games ensure the nCS - ALE timing below.
reg ncc_d;
always @(posedge CLK)
  ncc_d <= nCC;

//////////////////////////////////////////////////////////////////////
// Video address latch, counter

// Accessed via PPUSCROLL and PPUADDR
reg [14:0] val;                 // video address latch (vramaddr_t*)
reg [14:0] vac;                 // video address counter (vramaddr_v*)

// Sub-counters of val/vac
localparam HTS = 0, HTE = 4;    // H tile index
localparam VTS = 5, VTE = 9;    // V tile index
localparam H = 10;              // H name table selection
localparam V = 11;              // V name table selection
localparam FVS = 12, FVE = 14;  // fine V scroll

// For address/data register access, transparent latch is enabled by nCS.
wire ppuctrl_ws = !nCS && REG_WE[REG_PPUCTRL];
wire ppuscroll_h_ws = !nCS && REG_WE[REG_PPUSCROLL_H];
wire ppuscroll_v_ws = !nCS && REG_WE[REG_PPUSCROLL_V];
wire ppuaddr_h_ws = !nCS && REG_WE[REG_PPUADDR_H];
wire ppuaddr_l_ws = !nCS && REG_WE[REG_PPUADDR_L];
wire ppudata_ws = !nCS && REG_WE[REG_PPUDATA];
wire ppudata_rs = !nCS && REG_RE[REG_PPUDATA];

reg [14:0] val_d;
always @(posedge CLK)
  val_d <= val;

always @* begin
  val = val_d;
  if (RES2) begin
    val = 15'h0000;
  end
  else begin
    if (ppuctrl_ws) begin
      val[V] = IO_D_I[1];
      val[H] = IO_D_I[0];
    end
    if (ppuscroll_h_ws) // $2005/1
      val[HTE:HTS] = IO_D_I[7:3];
    if (ppuscroll_v_ws) // $2006/1
      {val[VTE:VTS], val[FVE:FVS]} = IO_D_I;
    if (ppuaddr_h_ws) // $2006/1
      val[14:8] = {1'b0, IO_D_I[5:0]}; // write clears MSB
    if (ppuaddr_l_ws) // $2006/2
      val[7:0] = IO_D_I;
  end
end

// Address register ($2006/2) write completion (nCS rises) is latched
// on CC rising edge.
reg ppuaddr_l_d, ppuaddr_l_done;
wire ppuaddr_l_done_p = ppuaddr_l_d && nCS;
always @(posedge CLK) if (CC) begin
  ppuaddr_l_d <= ppuaddr_l_ws;
  ppuaddr_l_done <= ppuaddr_l_done_p;
end

wire ppuaddr_vbus_rw_done;      // VRAM access completion

reg hinc_d, vinc_d, hcopy_d, vcopy_d;
always @(posedge CLK) if (CC) begin
  hinc_d <= HINC;
  vinc_d <= VINC;
  hcopy_d <= HCOPY;
  vcopy_d <= VCOPY;
end

// Updating counter and latch is a two-step process.

// 1. Do add with carry.
reg [14:0] vac_add;
always @* begin
  if (RENDERING) begin
    // Add 1 (with carry) to both H and V portions separately
    {vac_add[H], vac_add[HTE:HTS]} = {vac[H], vac[HTE:HTS]} + 1'd1;
    {vac_add[VTE:VTS], vac_add[FVE:FVS]} = {vac[VTE:VTS], vac[FVE:FVS]} + 1'd1;
    if (vac_add[FVE:FVS] == 3'd0 && vac_add[VTE:VTS] == 5'd30) // passed last row of NT
      {vac_add[V], vac_add[VTE:VTS]} = {!vac[V], 5'd0};
    else
      vac_add[V] = vac[V];
  end
  else
    // Add 1 or 32 to the whole counter
    vac_add[14:0] = vac + (ADDR_INC ? 15'd32 : 15'd1);
end

// 2. Update portions of counter with latch or adder outputs.
// $2006/2 and $2007 update the full counter.
reg [14:0] vac_next;
always @* begin
  vac_next = vac;

  if (hcopy_d | ppuaddr_l_done)
    {vac_next[H], vac_next[HTE:HTS]} = {val[H], val[HTE:HTS]};
  else if (hinc_d | ppuaddr_vbus_rw_done)
    {vac_next[H], vac_next[HTE:HTS]} = {vac_add[H], vac_add[HTE:HTS]};

  if (vcopy_d | ppuaddr_l_done)
    {vac_next[FVE:V], vac_next[VTE:VTS]} = {val[FVE:V], val[VTE:VTS]};
  else if (vinc_d | ppuaddr_vbus_rw_done)
    {vac_next[FVE:V], vac_next[VTE:VTS]} = {vac_add[FVE:V], vac_add[VTE:VTS]};
end

always @(posedge CLK) if (nCC) begin
  vac <= vac_next;
end

// Palette RAM starts at $3F00.
wire pal_sel = vac[14:8] == 7'h3F;

assign VAC = vac;

//////////////////////////////////////////////////////////////////////
// Picture address register

reg [7:0] par;
reg       fetch_nt_rd_d;

always @(posedge CLK) if (CC) begin
  fetch_nt_rd_d <= FETCH_NT && FETCH_RD;
  if (fetch_nt_rd_d)
    par <= SPR_FETCH ? SD : AD_I;
end

//////////////////////////////////////////////////////////////////////
// Sprite pattern address calculation and latches

reg [3:0] spy0;
reg [3:0] spy;

wire [7:0] spa = SD;
wire       vinv = spa[7];

always @(posedge CLK) if (CC) begin
  if (SPR_FETCH && FETCH_NT && FETCH_RD)
    // SD = Y pos (OAM Byte 0)
    spy0[3:0] <= ROW - SD;

  if (SPR_FETCH && FETCH_AT && FETCH_RD) begin
    // SD = attributes (OAM Byte 2)
    if (vinv)
      spy[3:0] <= 4'd15 - spy0;
    else
      spy <= spy0;
  end
end

//////////////////////////////////////////////////////////////////////
// Pattern table select (A[12]), tile select (A[11:4]), and Y offset
// (A[2:0])

reg [2:0] pat_y;
reg       pat_sel;
reg [7:0] tile_sel;

always @* begin
  tile_sel = par;
  if (SPR_FETCH) begin
    pat_y = spy[2:0];
    if (SPR_SIZE) begin
      pat_sel = par[0];
      tile_sel[0] = spy[3];
    end
    else
      pat_sel = SPR_PAT;
  end
  else begin
    pat_y = vac[FVE:FVS];
    pat_sel = BKG_PAT;
  end
end

//////////////////////////////////////////////////////////////////////
// CPU-directed VRAM / palette read

reg [2:0] read_ppudata_ended;
reg [7:0] inbuf;
reg       read_ppudata, read_ppudata_d;
reg       read_en;

// Data register ($2007) read completion (nCS rises) is latched on CC
// falling edge.
reg ppudata_rs_d;
always @(posedge CLK)
  ppudata_rs_d <= ppudata_rs;

always @* begin
  read_ppudata = read_ppudata_d;
  if (RES2)
    read_ppudata = 1'b0;
  else if (ppudata_rs_d && !ppudata_rs)
    read_ppudata = 1'b1;
  else if (read_ppudata_ended[0])
    read_ppudata = 1'b0;
end

always @(posedge CLK)
  read_ppudata_d <= read_ppudata;

always @(posedge CLK) if (ncc_d) begin
  if (RES2)
    read_ppudata_ended <= 3'b000;
  else
    read_ppudata_ended <= {read_ppudata_ended[1:0], read_ppudata};
end

always @(posedge CLK) if (CC) begin
  if (RES2)
    read_en <= 1'b0;
  else
    read_en <= read_ppudata_ended[2];
end

always @(posedge CLK) if (CC) begin
  if (RES2)
    inbuf <= 8'h00;
  else if (!nR)
    inbuf <= AD_I;
end

//////////////////////////////////////////////////////////////////////
// CPU-directed VRAM / palette write

reg [2:0] write_ppudata_ended;
reg [7:0] io_d_i_buf;
reg       write_ppudata, write_ppudata_d;
reg       write_en;

// Data register ($2007) write completion (nCS rises) is latched on CC
// falling edge.
reg ppudata_ws_d;
always @(posedge CLK)
  ppudata_ws_d <= ppudata_ws;

always @(posedge CLK)
  if (ppudata_ws_d && !ppudata_ws)
    io_d_i_buf <= IO_D_I;

always @* begin
  write_ppudata = write_ppudata_d;
  if (RES2)
    write_ppudata = 1'b0;
  else if (ppudata_ws_d && !ppudata_ws)
    write_ppudata = 1'b1;
  else if (write_ppudata_ended[0])
    write_ppudata = 1'b0;
end

always @(posedge CLK)
  write_ppudata_d <= write_ppudata;

always @(posedge CLK) if (ncc_d) begin
  if (RES2)
    write_ppudata_ended <= 3'b000;
  else
    write_ppudata_ended <= {write_ppudata_ended[1:0], write_ppudata};
end

always @(posedge CLK) if (nCC) begin
  if (RES2)
    write_en <= 1'b0;
  else
    write_en <= write_ppudata_ended[0];
end

assign ppuaddr_vbus_rw_done = read_en | write_en;

//////////////////////////////////////////////////////////////////////
// Video address / data bus output

reg render_rd;
always @(posedge CLK) if (CC)
  render_rd <= FETCH_RD;

// ALE should rise 2x - 5.5x CLK after nCS rises.
always @(posedge CLK) if (CC) begin
  if (RES2)
    ALE <= 1'b0;
  else if (RENDERING)
    ALE <= !FETCH_RD;
  else
    ALE <= write_ppudata_ended[0] | read_ppudata_ended[0];
end

always @(posedge CLK) if (CC) begin
  if (RENDERING) begin
    AD_O[13:0] <= {2'b10, vac[V:HTS]}; // default to FETCH_NT
    if (!SPR_FETCH && FETCH_AT)
      AD_O[13:0] <= {2'b10, vac[V:H], 4'b1111, vac[VTE:VTE-2], vac[HTE:HTE-2]};
    else if (FETCH_PTL || FETCH_PTH)
      AD_O[13:0] <= {1'b0, pat_sel, tile_sel, FETCH_PTH, pat_y};
  end
  else begin
    AD_O[13:0] <= vac[13:0];
    if (write_ppudata_ended[1])
      AD_O[7:0] <= io_d_i_buf;
  end
end

assign AD_OE = ~HOLD & nR;
assign IO_D_O = inbuf;
assign IO_D_OE = ~HOLD & (!nCS && RW && ppudata_rs && !pal_sel);
assign nR = !((read_en && !pal_sel) || render_rd);
assign nW = pal_sel || !write_en;
assign ARW_OE = ~HOLD;

//////////////////////////////////////////////////////////////////////
// Debug monitor interface

always @(posedge CLK) begin
  MON_DOUT <= 32'h0;
  MON_VALID <= 1'b0;

  if (MON_SEL[8:4] == 5'h02 && MON_READY) begin
    //``REGION_REGS NESBUS_CTRL
    case (MON_SEL[8:2])         //``SUBREGS PPU
      8'h08: MON_DOUT <= val;
      8'h09: MON_DOUT <= vac;
    endcase
    MON_VALID <= 1'b1;
  end
end

endmodule
