// RP2C02, the NES PPU
//
// Credit to:
// - Visual6502-style simulator: http://www.qmtpro.com/~nes/chipimages/visual2c02/
// - https://wiki.nesdev.com/w/index.php?title=PPU
// - https://www.nesdev.com/2C02%20technical%20reference.TXT

module rp2c02
  (
   input         CLK_RESET,
   input         CLK,
   output        CC,
   output        nCC,
   input         RES_COLD,
   input         HOLD, // halt and release A/D bus

   // debug monitor
   input [8:2]   MON_SEL,
   input         MON_READY,
   output [31:0] MON_DOUT,
   output        MON_VALID,

   // ICD match interface
   output [8:0]  ICD_COL,
   output [8:0]  ICD_ROW,
   output [31:0] ICD_FRAME,

   input [2:0]   RA,
   input [7:0]   D_I,
   output [7:0]  D_O,
   output        D_OE,
   input         RW,
   input         nCS,

   input [7:0]   AD_I,
   output [13:0] AD_O,
   output        AD_OE,
   output        ALE,
   output        nR,
   output        nW,
   output        ARW_OE,

   input         nSYNC,
   output        nVBL,

   output        PCE, // pixel clock enable
   output        DE, // data enable
   output        HS, // horizontal sync
   output        VS, // vertical sync
   output [5:0]  PIX, // pixel color index
   output [7:0]  R, // red component
   output [7:0]  G, // green component
   output [7:0]  B  // blue component
   );

wire [31:0] mon_dout_vc, mon_dout_rf, mon_dout_oam, mon_dout_bm, mon_dout_pal;
wire [31:0] frame;
wire [14:0] vac;
wire [8:0]  row, col, ren_row, ren_col;
wire [1:0]  ntb;
wire [7:0]  od_i, vd_i;
wire [9:0]  reg_we, reg_re;
wire [5:0]  bm_a_pal;
wire [5:0]  bm_d_pal;
wire [4:0]  pd;
wire [5:0]  pix;
wire [7:5]  io_d_o_reg;
wire [7:0]  io_d_o_pal, io_d_o_bm, io_d_o_oam;
wire [7:0]  oam_a;
wire [7:0]  sd;
wire [2:0]  emph;
wire        cc, ncc, cc_ungated, ncc_ungated;
wire        vbl_end;

// Internal reset
reg res2_d;
wire res = !nSYNC;
wire res2 = (res2_d || res) && !vbl_end;
always @(posedge CLK)
  res2_d <= res2;

// HOLD applies to the following CC.
reg  hold_dly;
always @(posedge CLK) if (cc_ungated)
  hold_dly <= HOLD;

// Internal I/O data bus
reg [7:0] io_d, io_d_d;
wire      d_ie = !nCS && !RW;   // I/O DB pads are driving

initial
  io_d_d = 8'hxx;

always @* begin
  io_d = 8'hxx;
  case ({io_d_oe_reg, io_d_oe_pal, io_d_oe_bm, io_d_oe_oam})
    4'b1000: io_d = {io_d_o_reg[7:5], io_d_d[4:0]};
    4'b0100: io_d = io_d_o_pal;
    4'b0010: io_d = io_d_o_bm;
    4'b0001: io_d = io_d_o_oam;
    4'b0000: io_d = io_d_d;
    default: ;
  endcase
end

always @(posedge CLK)
  io_d_d <= d_ie ? D_I : io_d;
  //io_d_d <= io_d;

rp2c02_clkgen clkgen
  (
   .CLK_RESET(CLK_RESET),
   .CLK(CLK),
   .CC(cc_ungated),
   .nCC(ncc_ungated)
   );

assign cc = cc_ungated & ~hold_dly;
assign ncc = ncc_ungated & ~hold_dly;

assign CC = cc_ungated;
assign nCC = ncc_ungated;

rp2c02_video_counter video_counter
  (
   .CLK(CLK),
   .CC(cc),
   .RES(res),

   .MON_SEL(MON_SEL),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_vc),
   .MON_VALID(mon_valid_vc),

   .RENDER_EN(render_en),

   .COL(col),
   .ROW(row),
   .FRAME(frame),
   .DOT_SKIPPED(dot_skipped),

   .VBL_START(vbl_start),
   .VBL_END(vbl_end)
   );

rp2c02_reg_file reg_file
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),
   .RES_COLD(RES_COLD),
   .RES(res),
   .RES2(res2),
   .HOLD(hold_dly),
   .VBL_START(vbl_start),
   .VBL_END(vbl_end),

   .MON_SEL(MON_SEL),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_rf),
   .MON_VALID(mon_valid_rf),

   .IO_D_I(D_I),
   .IO_D_O(io_d_o_reg),
   .IO_D_OE(io_d_oe_reg),
   .RA(RA),
   .RW(RW),
   .nCS(nCS),

   .REG_WE(reg_we),
   .REG_RE(reg_re),

   .VBL_EN(vbl_en),
   .SLAVE_MODE(slave_mode),
   .SPR_SIZE(spr_size),
   .BKG_PAT(bkg_pat),
   .SPR_PAT(spr_pat),
   .ADDR_INC(addr_inc),
   .EMPH(emph),

   .SPR_EN(spr_en),
   .BKG_EN(bkg_en),
   .SPR_CLIP(spr_clip),
   .BKG_CLIP(bkg_clip),
   .PAL_MONO(pal_mono),
   .VBL_FLAG(vbl_flag),
   .SET_SPR0_HIT(set_spr0_hit),
   .SPR_OVERFLOW(spr_overflow)
   );

rp2c02_renderer renderer
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),
   .RES2(res2),

   .COL(col),
   .ROW(row),
   .SPR_EN(spr_en),
   .BKG_EN(bkg_en),
   .SPR_CLIP(spr_clip),
   .BKG_CLIP(bkg_clip),

   .IO_D_I(D_I),
   .nCS(nCS),
   .REG_WE(reg_we),

   .AD_I(AD_I),
   .SD(sd),
   .SPR_IN_RANGE(spr_in_range),
   .SPR0_IN_RANGE(spr0_in_range),

   .RENDER_EN(render_en),
   .RENDERING(rendering),
   .HINC(hinc),
   .VINC(vinc),
   .HCOPY(hcopy),
   .VCOPY(vcopy),
   .FETCH(fetch),
   .FETCH_NT(fetch_nt),
   .FETCH_AT(fetch_at),
   .FETCH_PTL(fetch_ptl),
   .FETCH_PTH(fetch_pth),
   .FETCH_RD(fetch_rd),
   .SPR_FETCH(spr_fetch),
   .VAC(vac),

   .SET_SPR0_HIT(set_spr0_hit),
   .PD(pd),
   .COL_O(ren_col),
   .ROW_O(ren_row)
   );

rp2c02_oam oam
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),

   .MON_SEL(MON_SEL),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_oam),
   .MON_VALID(mon_valid_oam),

   .A(oam_a),
   .WE(oam_we),
   .RE(oam_re),
   .SEC(oam_sec),
   .CLR(oam_clr),
   .IO_SEL(oam_io_sel),

   .IO_D_I(D_I),
   .IO_D_O(io_d_o_oam),
   .IO_D_OE(io_d_oe_oam),

   .SD(sd)
   );

rp2c02_sprite_eval sprite_eval
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),

   .COL(col),
   .ROW(row),

   .SPR_SIZE(spr_size),
   .SPR_OVERFLOW(spr_overflow),

   .RENDER_EN(render_en),
   .RENDERING(rendering),
   .SPR_IN_RANGE(spr_in_range),
   .SPR0_IN_RANGE(spr0_in_range),

   .OAM_A(oam_a),
   .OAM_WE(oam_we),
   .OAM_RE(oam_re),
   .OAM_SEC(oam_sec),
   .OAM_CLR(oam_clr),
   .OAM_IO_SEL(oam_io_sel),

   .REG_WE(reg_we),
   .REG_RE(reg_re),

   .IO_D_I(D_I),
   .nCS(nCS),

   .SD(sd)
   );

rp2c02_sync_gen sync_gen
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),
   .HOLD(hold_dly),

   .COL(ren_col),
   .ROW(ren_row),
   .DOT_SKIPPED(dot_skipped),

   .PCE(PCE),
   .DE(DE),
   .HSYNC(HS),
   .VSYNC(VS),
   .BLANK(blank)
   );

rp2c02_vram_bus_master bus_master
  (
   .CLK(CLK),
   .CC(cc),
   .nCC(ncc),
   .RES2(res2),
   .HOLD(hold_dly),

   .MON_SEL(MON_SEL),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_bm),
   .MON_VALID(mon_valid_bm),

   .ROW(row),

   .IO_D_I(D_I),
   .IO_D_O(io_d_o_bm),
   .IO_D_OE(io_d_oe_bm),
   .RW(RW),
   .nCS(nCS),

   .REG_WE(reg_we),
   .REG_RE(reg_re),
   .SPR_SIZE(spr_size),
   .BKG_PAT(bkg_pat),
   .SPR_PAT(spr_pat),
   .ADDR_INC(addr_inc),

   .RENDERING(rendering),
   .HINC(hinc),
   .VINC(vinc),
   .HCOPY(hcopy),
   .VCOPY(vcopy),
   .FETCH(fetch),
   .FETCH_NT(fetch_nt),
   .FETCH_AT(fetch_at),
   .FETCH_PTL(fetch_ptl),
   .FETCH_PTH(fetch_pth),
   .FETCH_RD(fetch_rd),
   .SPR_FETCH(spr_fetch),
   .VAC(vac),

   .SD(sd),

   .AD_I(AD_I),
   .AD_O(AD_O),
   .AD_OE(AD_OE),
   .ALE(ALE),
   .nR(nR),
   .nW(nW),
   .ARW_OE(ARW_OE)
   );

rp2c02_palette palette
  (
   .CLK(CLK),
   .CC(cc),

   .MON_SEL(MON_SEL),
   .MON_READY(MON_READY),
   .MON_DOUT(mon_dout_pal),
   .MON_VALID(mon_valid_pal),

   .RENDERING(rendering),
   .VAC(vac),
   .REG_WE(reg_we),
   .REG_RE(reg_re),

   .IO_D_I(D_I),
   .IO_D_O(io_d_o_pal),
   .IO_D_OE(io_d_oe_pal),
   .nCS(nCS),

   .PD_PTR(pd),
   .PD_O(pix)
   );

rp2c02_color_gen color_gen
  (
   .PIX(pix),
   .BLANK(blank),

   .RED(R),
   .GREEN(G),
   .BLUE(B)
   );

// Debug monitor interface
reg [31:0] mon_dout;
reg        mon_valid;

always @* begin
  if (mon_valid_rf) begin
    mon_valid = mon_valid_rf;
    mon_dout = mon_dout_rf;
  end
  else if (mon_valid_vc) begin
    mon_valid = mon_valid_vc;
    mon_dout = mon_dout_vc;
  end
  else if (mon_valid_bm) begin
    mon_valid = mon_valid_bm;
    mon_dout = mon_dout_bm;
  end
  else if (mon_valid_oam) begin
    mon_valid = mon_valid_oam;
    mon_dout = mon_dout_oam;
  end
  else if (mon_valid_pal) begin
    mon_valid = mon_valid_pal;
    mon_dout = mon_dout_pal;
  end
  else begin
    mon_valid = 1'b0;
    mon_dout = 32'h0;
  end
end

assign MON_DOUT = mon_dout;
assign MON_VALID = mon_valid;

assign ICD_COL = col;
assign ICD_ROW = row;
assign ICD_FRAME = frame;

assign D_O = D_OE ? io_d : 8'hxx;
assign D_OE = io_d_oe_reg | io_d_oe_pal | io_d_oe_bm | io_d_oe_oam;

assign nVBL = !(vbl_flag & vbl_en);
assign PIX = pix;

endmodule
