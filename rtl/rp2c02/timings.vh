// We emulate the RP2C02: NTSC, 262 scanlines (242 visible), 341
// pixels / scanline (256 visible).

localparam [8:0] NUM_ROWS = 9'd262;
localparam [8:0] NUM_COLS = 9'd341;

// Reference: https://wiki.nesdev.com/w/index.php?title=NTSC_video

// 240 visible lines rendered
localparam [8:0] FIRST_ROW_RENDER = 9'd0;
localparam [8:0] LAST_ROW_RENDER = 9'd239;
// post-render blanking
localparam [8:0] FIRST_ROW_POST_RENDER = 9'd240;
localparam [8:0] LAST_ROW_POST_RENDER = 9'd241;
// front porch / post-render blanking
localparam [8:0] FIRST_ROW_FRONT = FIRST_ROW_POST_RENDER;
localparam [8:0] LAST_ROW_FRONT = 9'd244;
// VSYNC
localparam [8:0] FIRST_ROW_VSYNC = 9'd245;
localparam [8:0] LAST_ROW_VSYNC = 9'd247;
// back porch / pre-render blanking
localparam [8:0] FIRST_ROW_PRE_RENDER = 9'd247;
localparam [8:0] LAST_ROW_PRE_RENDER = 9'd261; // "the" pre-render scanline

// 256 visible columns rendered
// values are for the start of the 4-cycle pixel render pipeline
localparam [8:0] FIRST_COL_RENDER = 9'd0;
localparam [8:0] LAST_COL_RENDER = 9'd255;
// right border
localparam [8:0] FIRST_COL_RIGHT = 9'd256;
localparam [8:0] LAST_COL_RIGHT = 9'd266;
// front porch
localparam [8:0] FIRST_COL_FRONT = 9'd267;
localparam [8:0] LAST_COL_FRONT = 9'd275;
// HSYNC
localparam [8:0] FIRST_COL_HSYNC = 9'd276;
localparam [8:0] LAST_COL_HSYNC = 9'd300;
// back porch
localparam [8:0] FIRST_COL_BACK = 9'd301;
localparam [8:0] LAST_COL_BACK = 9'd324;
// left border - first pixel is "burst" (grayscale BG)
localparam [8:0] FIRST_COL_LEFT = 9'd325;
localparam [8:0] LAST_COL_LEFT = 9'd340;
