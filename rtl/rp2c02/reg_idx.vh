// Indices for reg_file() REG_WE and REG_RE

localparam REG_PPUCTRL = 0;     // $2000
localparam REG_PPUMASK = 1;     // $2001
localparam REG_PPUSTATUS = 2;   // $2002
localparam REG_OAMADDR = 3;     // $2003
localparam REG_OAMDATA = 4;     // $2004
localparam REG_PPUSCROLL_H = 5;   // $2005/1
localparam REG_PPUSCROLL_V = 6;   // $2005/2
localparam REG_PPUADDR_H = 7;   // $2006/1
localparam REG_PPUADDR_L = 8;   // $2006/2
localparam REG_PPUDATA = 9;     // $2007
