module rp2c02_bkg_renderer
  (
   input            CLK,
   input            CC,
   input            nCC,
   
   input [8:0]      COL,
   input [8:0]      ROW,
   input            BKG_EN,
   input [2:0]      FHS,

   input [7:0]      AD_I,

   input            FETCH,
   input            FETCH_NT,
   input            FETCH_AT,
   input            FETCH_PTL,
   input            FETCH_PTH,
   input            FETCH_RD,
   input [14:0]     VAC,

   output reg [3:0] PD,
   output reg [8:0] COL_O,
   output reg [8:0] ROW_O
   );

`include "swab.vh"

//////////////////////////////////////////////////////////////////////
// Tile pattern data latches and shifters
// These select color bits 1-0.

reg [15:0] tile_l, tile_h;      // shifters
reg [7:0]  tile_lbuf;           // latches
reg        fetch_ptl_rd_d, fetch_pth_rd_d;
reg        shift;

always @(posedge CLK) if (CC) begin
  // Shift the shifters.
  shift <= FETCH;
  if (shift) begin
    tile_l <= {1'b1, tile_l[15:1]};
    tile_h <= {1'b1, tile_h[15:1]};
  end

  // On PTL data fetch, buffer it, so we can load both tile_l and tile_h
  // simultaneously on PTH data fetch.
  fetch_ptl_rd_d <= FETCH_PTL && FETCH_RD;
  if (fetch_ptl_rd_d)
    tile_lbuf <= AD_I;

  // On PTH data fetch, load buffered PTL and fetched PTH.
  // Pattern data is rendered MSB first.
  fetch_pth_rd_d <= FETCH_PTH && FETCH_RD;
  if (fetch_pth_rd_d) begin
    tile_l[15:8] <= swab(tile_lbuf);
    tile_h[15:8] <= swab(AD_I);
  end
end

//////////////////////////////////////////////////////////////////////
// Attribute data latches and shifters
// These select color bits 3-2.

reg [7:0] attrib_l, attrib_h;   // shifters
reg [7:0] attrib_buf;           // latches
reg [1:0] at_xy;
reg       fetch_at_rd_d;
reg       attrib_l_sel, attrib_h_sel;
reg       attrib_l_in, attrib_h_in;

function [3:0] attrib_pick_cbit
  (
   input [7:0] hold,
   input       high,
   input [1:0] xy
   );
reg [3:0]      cbits;
  begin
    if (high)
      cbits = {hold[7], hold[5], hold[3], hold[1]};
    else
      cbits = {hold[6], hold[4], hold[2], hold[0]};
    attrib_pick_cbit = cbits[xy];
  end
endfunction

always @* begin
  // Select the latch bits for the upcoming tile.
  attrib_l_sel = attrib_pick_cbit(attrib_buf, 1'b0, at_xy);
  attrib_h_sel = attrib_pick_cbit(attrib_buf, 1'b1, at_xy);
end

always @(posedge CLK) if (CC) begin
  // Shift the shifters.
  if (shift) begin
    attrib_l <= {attrib_l_in, attrib_l[7:1]};
    attrib_h <= {attrib_h_in, attrib_h[7:1]};
  end

  // Each 2 bits of attribute data covers a 16x16 area. Two VAC bits
  // tell us which area to pick. Register here to use VAC from before
  // the H increment.
  at_xy <= {VAC[6], VAC[1]};

  // On AT data fetch, buffer it.
  fetch_at_rd_d <= FETCH_AT && FETCH_RD;
  if (fetch_at_rd_d)
    attrib_buf <= AD_I;

  // On PTH data fetches, sample the bit to input to shifters.
  if (fetch_pth_rd_d) begin
    attrib_l_in <= attrib_l_sel;
    attrib_h_in <= attrib_h_sel;
  end
end

//////////////////////////////////////////////////////////////////////

reg [1:0] bkg_en_d;

always @(posedge CLK) if (CC) begin
  bkg_en_d <= BKG_EN;
  PD <= bkg_en_d ? {attrib_h[FHS], attrib_l[FHS], tile_h[FHS], tile_l[FHS]} : 4'h0;
  COL_O <= COL;
  ROW_O <= ROW;
end

endmodule
