module rp2c02_sprite_eval
  (
   input        CLK,
   input        CC,
   input        nCC,

   input [8:0]  COL,
   input [8:0]  ROW,

   // Register flags
   input        SPR_SIZE, // sprite size (double width)
   output reg   SPR_OVERFLOW,

   // Renderer signals
   input        RENDER_EN,
   input        RENDERING,
   output reg   SPR_IN_RANGE,
   output reg   SPR0_IN_RANGE,

   // OAM controls
   output [7:0] OAM_A,
   output       OAM_WE,
   output       OAM_RE,
   output       OAM_SEC,
   output       OAM_CLR,
   output       OAM_IO_SEL,

   // register read/write enables
   input [9:0]  REG_WE,
   input [9:0]  REG_RE,

   // internal I/O data bus
   input [7:0]  IO_D_I,
   input        nCS,

   // internal sprite data bus spr_d*
   input [7:0]  SD
   );

`include "reg_idx.vh"
`include "timings.vh"

wire in_range, in_range_m1;

// External read/write selects
wire io_aws = REG_WE[REG_OAMADDR] && !nCS;
wire io_drs = REG_RE[REG_OAMDATA] && !nCS;
wire io_dws = REG_WE[REG_OAMDATA] && !nCS;

wire eval_en = RENDER_EN && ROW <= LAST_ROW_RENDER;

reg  in_range_p;
reg  spr_overflow;

//////////////////////////////////////////////////////////////////////
// OAM access control

reg [5:0] ptr;                  // {spr_ptr[4:0], spr_ptr5}
reg [6:0] ptr_next;
reg       ptr_overflow;
reg [7:0] addr;                 // spr_addr*
reg [8:0] addr_next;
reg       addr_overflow;
reg       io_drws_d;
reg       sec_sel;
reg       sec_we, oam_clr;
reg       addr_inc0_carryout;

wire      io_drws = io_drs | io_dws;

// All signals are one CC early
wire oam_clear = eval_en && (COL >= 9'd0 && COL <= 9'd63);
wire oam_eval = eval_en && (COL >= 9'd64 && COL <= 9'd255);
wire oam_fetch = RENDERING && (COL >= 9'd256 && COL <= 9'd319);
wire oam_spare = RENDERING && COL >= 9'd320;
//wire oam_fetch = eval_en && (COL >= 9'd256 && COL <= 9'd319);
//wire oam_spare = eval_en && COL >= 9'd320;
wire oam_clear0 = RENDERING && COL == 9'd0;
wire oam_eval0 = RENDERING && COL == 9'd64;
wire oam_fetch0 = RENDERING && COL == 9'd256;

always @* begin
  ptr_next = {ptr_overflow, ptr};
  if (oam_clear0 || oam_eval0)
    ptr_next = 7'h00;
  else if (oam_fetch0 || oam_spare)
    ptr_next = 7'h01;
  else if (oam_clear) begin
    ptr_next[5:0] = ptr_next + 1'd1;
  end
  else if (oam_eval) begin
    ptr_next = ptr_next + 1'd1;
    if (addr_overflow || ptr_overflow || !in_range)
      ptr_next[1] = 1'b0;
  end
  else if (oam_fetch) begin
    if (!COL[2])
      ptr_next = ptr_next + 2'd2;
  end
end

// This recreates the sprite overflow bug.
wire spr_overflow_bug = in_range_p && ptr_overflow && !spr_overflow;

wire addr_inc2 = !in_range || addr_inc0_carryout;
wire addr_inc0 = in_range_m1 || spr_overflow_bug;

always @* begin
  addr_next = {addr_overflow, addr};
  if (io_aws)
    addr_next = IO_D_I;
  else if (!io_drws && io_drws_d)
    addr_next = addr_next + 1'd1;
  else if (oam_clear0 || oam_eval0 || oam_fetch0)
    addr_next = 9'h00;
  else if (oam_eval && !COL[0]) begin
    if (addr_inc0)
      addr_next[1:0] = addr_next[1:0] + 1'd1;
    else
      addr_next[1:0] = 2'b00;
    if (addr_inc2)
      addr_next[8:2] = addr_next[8:2] + 1'd1;
  end
end

wire postrender_row = ROW == LAST_ROW_POST_RENDER;

always @(posedge CLK) if (CC) begin
  {addr_overflow, addr} <= addr_next;
end

always @(posedge CLK) if (CC)
  addr_inc0_carryout <= (addr[1:0] + 2'd1) == 3'b100;

always @(posedge CLK) if (CC) begin
  io_drws_d <= io_drws;
  {ptr_overflow, ptr} <= ptr_next;
  sec_sel <= RENDERING;
  sec_we <= (oam_clear || oam_eval) && !(ptr_overflow || addr_overflow);
  oam_clr <= oam_clear;
  if (oam_clear)
    ptr_overflow <= 1'b0;
end

wire oam_sec = sec_sel && ptr[0];
wire oam_we = io_dws || (oam_sec && sec_we);

assign OAM_A = oam_sec ? ptr[5:1] : addr;
assign OAM_WE = oam_we;
assign OAM_RE = !oam_we;
assign OAM_SEC = oam_sec;
assign OAM_CLR = oam_clr;
assign OAM_IO_SEL = io_drws;

//////////////////////////////////////////////////////////////////////
// Sprite Y in-range test
//
// A sprite is in-range if it could appear on the current row. The
// current row, the sprite's Y position, and the global sprite height
// (8 or 16) are all considered.

reg [8:0] first_y, last_y;
reg [3:0] in_range_d;

always @* begin
  // When we perform this test, the OAM data SHOULD BE the sprite's Y
  // position. (After 8 in-range sprites are found, it may not be.)
  first_y = SD;
  last_y[8:0] = first_y + (SPR_SIZE ? 9'd16 : 9'd8) - 9'd1;

  in_range_p = first_y <= ROW && ROW <= last_y;
end

assign in_range_m1 = |(in_range_d >> 1);
assign in_range = |in_range_d;

always @(posedge CLK) if (CC && ptr[0] == 1'b0) begin
  if (!in_range_m1 && in_range_p && !spr_overflow)
    in_range_d <= 4'b1111;
  else
    in_range_d <= in_range_d >> 1;
end

// Dedicated signal for sprite 0 hit detection.
always @(posedge CLK) if (CC) begin
  if (!eval_en)
    SPR0_IN_RANGE <= 1'b0;
  else if (oam_eval && addr == 8'h00 && !oam_sec)
    SPR0_IN_RANGE <= in_range_p;
end

// On OAM secondary readout, the sprite renderers need to know if the
// data they're being loaded with is valid, i.e., from an in-range
// sprite.
always @(posedge CLK) if (CC) begin
  if (oam_fetch && ptr[2:1] == 2'b00)
    SPR_IN_RANGE <= in_range_p;
end

//////////////////////////////////////////////////////////////////////
// Sprite overflow flags

wire set_spr_overflow = ptr_overflow && in_range_m1;

always @(posedge CLK) if (CC) begin
  // This flag gets reset every row.
  if (oam_clear0)
    spr_overflow <= 1'b0;
  else if (set_spr_overflow)
    spr_overflow <= 1'b1;

  // This flag gets reset every frame.
  if (postrender_row && COL == 9'd1)
    SPR_OVERFLOW <= 1'b0;
  else if (set_spr_overflow)
    SPR_OVERFLOW <= 1'b1;
end

endmodule
