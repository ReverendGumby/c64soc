module rp2c02_spr_renderer
  (
   input            CLK,
   input            CC,
   input            nCC,
   
   input [8:0]      COL,
   input [8:0]      ROW,
   input            SPR_EN,

   input [7:0]      AD_I,
   input [7:0]      SD,
   input            SPR_IN_RANGE,

   input            FETCH,
   input            FETCH_AT,
   input            FETCH_PTL,
   input            FETCH_PTH,
   input            FETCH_RD,
   input            VISIBLE_COL,

   output reg [4:0] PD
   );

`include "swab.vh"

//////////////////////////////////////////////////////////////////////
// Sprite attribute data latches
// These select color bits 3-2, priority, and H/V flip.

reg [7:0] attrib;               // latches

always @(posedge CLK) if (CC) begin
  if (FETCH_AT && FETCH_RD)
    attrib <= SPR_IN_RANGE ? SD : 8'h00;
end

wire pri = attrib[5];
wire hinv = attrib[6];

//////////////////////////////////////////////////////////////////////
// Sprite X position counters

reg [7:0] pos;                  // latches
reg       active;

wire active_p = VISIBLE_COL && pos == 8'd0;

always @(posedge CLK) if (CC) begin
  if (FETCH_PTL && FETCH_RD)
    pos <= SD;
  else if (VISIBLE_COL) begin
    if (!active_p)
      pos <= pos - 1'd1;
  end

  active <= active_p;
end

//////////////////////////////////////////////////////////////////////
// Sprite pattern data latches and shifters
// These select color bits 1-0.
// Pattern data is rendered MSB first (hinv=0) or LSB first (hinv=1).

reg [7:0] tile_l, tile_h;      // shifters
reg [7:0] spd;
reg       fetch_ptl_rd_d, fetch_pth_rd_d;
reg       tile_l_out, tile_h_out;

// Swap / zero pattern data if needed.
always @* begin
  if (SPR_IN_RANGE) begin
    if (hinv)
      spd = AD_I;
    else
      spd = swab(AD_I);
  end
  else
    spd = 8'h00;
end  

always @(posedge CLK) if (CC) begin
  // Shift the shifters.
  if (active) begin
    tile_l <= {1'b0, tile_l[7:1]};
    tile_h <= {1'b0, tile_h[7:1]};
  end

  // On PTL/PTH data fetch, load it into the shifter.
  fetch_ptl_rd_d <= FETCH_PTL && FETCH_RD;
  fetch_pth_rd_d <= FETCH_PTH && FETCH_RD;
  if (fetch_ptl_rd_d)
    tile_l <= spd;
  if (fetch_pth_rd_d)
    tile_h <= spd;
end

always @* begin
  tile_l_out = tile_l[0];
  tile_h_out = tile_h[0];
end

//////////////////////////////////////////////////////////////////////

reg spr_en_d;

always @(posedge CLK) if (CC) begin
  spr_en_d <= SPR_EN;
  PD <= spr_en_d && active ? {pri, attrib[1:0], tile_h_out, tile_l_out} : 4'b0;
end

endmodule
