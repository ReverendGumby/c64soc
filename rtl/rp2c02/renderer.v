module rp2c02_renderer
  (
   input            CLK,
   input            CC,
   input            nCC,
   input            RES2,
   
   input [8:0]      COL,
   input [8:0]      ROW,
   input            SPR_EN,
   input            BKG_EN,
   input            SPR_CLIP,
   input            BKG_CLIP,

   input [7:0]      IO_D_I,
   input            nCS,
   input [9:0]      REG_WE,

   input [7:0]      AD_I,
   input [7:0]      SD,
   input            SPR_IN_RANGE,
   input            SPR0_IN_RANGE,

   output           RENDER_EN,
   output           RENDERING,
   output           HINC,
   output           VINC,
   output           HCOPY,
   output           VCOPY,
   output           FETCH,
   output           FETCH_NT,
   output           FETCH_AT,
   output           FETCH_PTL,
   output           FETCH_PTH,
   output           FETCH_RD,
   output           SPR_FETCH,
   input [14:0]     VAC,

   output           SET_SPR0_HIT,
   output reg [4:0] PD,
   output reg [8:0] COL_O,
   output reg [8:0] ROW_O
   );

`include "timings.vh"
`include "reg_idx.vh"

wire visible_col = (COL >= FIRST_COL_RENDER && COL <= LAST_COL_RENDER);
wire render_row = (ROW == LAST_ROW_PRE_RENDER ||
                   (ROW >= FIRST_ROW_RENDER && ROW <= LAST_ROW_RENDER));
wire render_col = (COL >= 9'd320 || visible_col);
wire clip_col = COL < (FIRST_COL_RENDER + 9'd8);

// Chip-global "rendering is happening" and "rendering enabled" signals
assign RENDERING = RENDER_EN && render_row;
assign RENDER_EN = SPR_EN || BKG_EN;

// Render-related counter triggers
// All signals are one CC early
wire prerender_row = ROW == LAST_ROW_PRE_RENDER;
wire bkg_fetch = RENDERING && render_col && !idle;
wire spr_fetch = RENDERING && !render_col;
wire idle = COL == 9'd340;      /* cycle 0 */
wire unused_nt = COL >= 9'd336 && !idle;
//wire bkg_fetch_nt = bkg_fetch && (COL[2:0] == 3'd0 || COL[2:0] == 3'd1 || unused_nt);
//wire spr_fetch_nt = spr_fetch && COL[2] == 1'b0;
assign HINC = FETCH && render_col && COL[2:0] == 3'd7;
assign VINC = RENDERING && COL == FIRST_COL_RIGHT;
assign HCOPY = RENDERING && COL == (FIRST_COL_RIGHT + 1);
assign VCOPY = RENDERING && (COL >= 9'd280 && COL <= 9'd304) && prerender_row;
assign FETCH = RENDERING;
//assign FETCH_NT = bkg_fetch_nt || spr_fetch_nt;
assign FETCH_NT = (COL[2:0] == 3'd0 || COL[2:0] == 3'd1 || unused_nt);
assign FETCH_AT = (COL[2:0] == 3'd2 || COL[2:0] == 3'd3) && !unused_nt;
assign FETCH_PTL = FETCH && (COL[2:0] == 3'd4 || COL[2:0] == 3'd5 || idle);
assign FETCH_PTH = FETCH && (COL[2:0] == 3'd6 || COL[2:0] == 3'd7);
assign FETCH_RD = FETCH && COL[0];
assign SPR_FETCH = spr_fetch;

//////////////////////////////////////////////////////////////////////
// Background renderer

wire [3:0] bkg_pd;
wire [8:0] bkg_col, bkg_row;
wire       bkg_po;              // pixel is opaque

wire ppuscroll_h_ws = !nCS && REG_WE[REG_PPUSCROLL_H];
wire bkg_en_mask = BKG_EN && (BKG_CLIP || !clip_col);

// Fine horizontal scroll
reg [2:0] fhs;
always @(posedge CLK) begin
  if (RES2)
    fhs <= 3'd0;
  else if (ppuscroll_h_ws)
    fhs <= IO_D_I[2:0];
end

rp2c02_bkg_renderer bkg_renderer
  (
   .CLK(CLK),
   .CC(CC),
   .nCC(nCC),

   .COL(COL),
   .ROW(ROW),
   .BKG_EN(bkg_en_mask),
   .FHS(fhs),

   .AD_I(AD_I),

   .FETCH(bkg_fetch && !unused_nt),
   .FETCH_NT(bkg_fetch && FETCH_NT),
   .FETCH_AT(bkg_fetch && FETCH_AT),
   .FETCH_PTL(bkg_fetch && FETCH_PTL),
   .FETCH_PTH(bkg_fetch && FETCH_PTH),
   .FETCH_RD(bkg_fetch && FETCH_RD),
   .VAC(VAC),

   .PD(bkg_pd),
   .COL_O(bkg_col),
   .ROW_O(bkg_row)
   );

assign bkg_po = |bkg_pd[1:0];

//////////////////////////////////////////////////////////////////////
// Sprite renderers

wire [4:0] sprn_pd [0:7];       // pixel data (3:0) and priority (4)
wire [7:0] sprn_po;             // pixel is opaque
wire [7:0] sprn_fetch;

wire spr_en_mask = SPR_EN && (SPR_CLIP || !clip_col);

genvar i;
generate
  for (i = 0; i < 8; i = i + 1) begin :spr_gen
    assign sprn_fetch[i] = COL >= (9'd256 + i * 8) && COL <= (9'd263 + i * 8);

    rp2c02_spr_renderer spr_renderer
      (
       .CLK(CLK),
       .CC(CC),
       .nCC(nCC),

       .COL(COL),
       .ROW(ROW),
       .SPR_EN(spr_en_mask),

       .AD_I(AD_I),
       .SD(SD),
       .SPR_IN_RANGE(SPR_IN_RANGE),

       .FETCH(sprn_fetch[i]),
       .FETCH_AT(sprn_fetch[i] && FETCH_AT),
       .FETCH_PTL(sprn_fetch[i] && FETCH_PTL),
       .FETCH_PTH(sprn_fetch[i] && FETCH_PTH),
       .FETCH_RD(sprn_fetch[i] && FETCH_RD),
       .VISIBLE_COL(visible_col),

       .PD(sprn_pd[i])
       );

    assign sprn_po[i] = |sprn_pd[i][1:0];

  end
endgenerate

//////////////////////////////////////////////////////////////////////
// Sprite 0-7 pixel priority multiplexer
//
// Priority is determined by renderer instance, where 0 is highest,
// and pixel opacity.

reg [4:0] spr_pd;

always @* begin
  spr_pd = 5'hxx;
  casez (sprn_po)
    8'b00000000: spr_pd = 5'h00;
    8'b???????1: spr_pd = sprn_pd[0];
    8'b??????10: spr_pd = sprn_pd[1];
    8'b?????100: spr_pd = sprn_pd[2];
    8'b????1000: spr_pd = sprn_pd[3];
    8'b???10000: spr_pd = sprn_pd[4];
    8'b??100000: spr_pd = sprn_pd[5];
    8'b?1000000: spr_pd = sprn_pd[6];
    8'b10000000: spr_pd = sprn_pd[7];
    default: ;
  endcase
end

wire spr_po = |sprn_po;

//////////////////////////////////////////////////////////////////////
// Sprite / background pixel priority multiplexer
//
// Priority is complicated; sprite priority (from attributes) enters
// the equation here.

wire [3:0] bkg_px = bkg_pd[3:0];
wire [3:0] spr_px = spr_pd[3:0];
wire       spr_pri = spr_pd[4];

reg [4:0]  pdo;
reg        blank;

always @* begin
  pdo = 5'hx;
  casez ({bkg_po, spr_po, spr_pri})
    3'b00?: pdo = 5'h00;
    3'b01?, 3'b110: pdo = {1'b1, spr_px};
    default: pdo = {1'b0, bkg_px};
  endcase
end

always @(posedge CLK) if (CC) begin
  blank <= !(bkg_col >= FIRST_COL_RENDER && bkg_col <= LAST_COL_RENDER);
  PD <= blank ? 5'h0 : pdo;
  COL_O <= bkg_col;
  ROW_O <= bkg_row;
end

//////////////////////////////////////////////////////////////////////
// Sprite 0 hit detection
//
// This is really for sprite 0, which just happens to always use
// sprite renderer 0.

wire spr0_in_range_next;        // sprite 0 in range for next row
reg  spr0_in_range;             // sprite 0 in range for current row

reg visible_col_d, visible_col_dd;

assign spr0_in_range_next = SPR0_IN_RANGE;

always @(posedge CLK) if (CC) begin
  if (RES2) begin
    spr0_in_range <= 1'b0;
  end
  else begin
    if (VINC)                   // just a guess
      spr0_in_range <= spr0_in_range_next;
  end

  visible_col_d <= visible_col;
  visible_col_dd <= visible_col_d;
end

assign SET_SPR0_HIT = spr0_in_range && sprn_po[0] && bkg_po && visible_col_dd;

endmodule
