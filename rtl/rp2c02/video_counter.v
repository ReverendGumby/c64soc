module rp2c02_video_counter
  (
   input             CLK,
   input             CC,
   input             RES,

   input [8:2]       MON_SEL,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,

   input             RENDER_EN,

   output reg [8:0]  COL,
   output reg [8:0]  ROW,
   output [31:0]     FRAME,
   output reg        DOT_SKIPPED,

   output            VBL_START,
   output            VBL_END
   );

reg [31:0] frame_cnt;
reg [8:0]  last_col, last_row;
reg        end_of_row;
reg        res_d;
reg        skip_dot;
reg        odd_frame;

`include "timings.vh"

initial begin
  last_row = NUM_ROWS - 1'd1;
  last_col = NUM_COLS - 1'd1;
end

always @*
  skip_dot = RENDER_EN && odd_frame && ROW == LAST_ROW_PRE_RENDER;
always @*
  end_of_row = (COL == last_col) || (COL == last_col - 1'd1 && skip_dot);

always @(posedge CLK) if (CC) begin
  if (RES) begin
    frame_cnt <= 32'd0;
    COL <= 9'd0;
    ROW <= 9'd0;
    DOT_SKIPPED <= 1'b0;
    odd_frame <= 1'b0;
  end
  else begin
    if (end_of_row) begin
      COL <= 9'd0;
      if (ROW == last_row) begin
        ROW <= 9'd0;
        odd_frame <= ~odd_frame;
      end
      else begin
        ROW <= ROW + 1'd1;
      end
      DOT_SKIPPED <= skip_dot;
    end
    else
      COL <= COL + 1'd1;

    if (ROW == FIRST_ROW_POST_RENDER && COL == FIRST_COL_RENDER)
      frame_cnt[31:0] <= frame_cnt + 1'd1;
  end
end

// Start of vertical blanking interval: set VBL flag.
assign VBL_START = ROW == LAST_ROW_POST_RENDER && COL == 9'd0;

// End of vertical blanking interval: clear VBL flag, RES2, frame,
// spr_overflow, and spr0_hit; and copy all vertical VRAM address bits
// from latch to counter (FV, VT, V).
assign VBL_END = ROW == LAST_ROW_PRE_RENDER;

// Debug monitor interface
always @(posedge CLK) begin
  MON_DOUT <= 32'h0;
  MON_VALID <= 1'b0;

  if (MON_SEL[8:4] == 5'h01 && MON_READY) begin
    //``REGION_REGS NESBUS_CTRL
    case (MON_SEL[8:2])         //``SUBREGS PPU
      8'h04: begin              //``REG ROWCOL
        MON_DOUT[8:0] <= ROW;
        MON_DOUT[24:16] <= COL;
      end
      8'h05: begin              //``REG FRAME
        MON_DOUT <= frame_cnt;
      end
    endcase
    MON_VALID <= 1'b1;
  end
end

assign FRAME = frame_cnt;

endmodule
