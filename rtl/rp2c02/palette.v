// The palette SRAM is 28 x 6-bit words. The 28 words are arranged in
// 4 columns by 7 rows and selected by a 5-bit address. Only 24 words
// can normally be accessed.

module rp2c02_palette
  (
   input             CLK,
   input             CC,
   
   input [8:2]       MON_SEL,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,

   input             RENDERING,
   input [14:0]      VAC,
   input [9:0]       REG_RE,
   input [9:0]       REG_WE,

   input [7:0]       IO_D_I,
   output [7:0]      IO_D_O,
   output            IO_D_OE,
   input             nCS,

   input [4:0]       PD_PTR,
   output reg [5:0]  PD_O
   );

`include "reg_idx.vh"

//            row   col
reg [5:0] ram [0:6] [0:3];

// Palette RAM starts at $3F00.
wire      pal_sel = VAC[14:8] == 7'h3F;
wire      pal_rs = pal_sel && REG_RE[REG_PPUDATA] && !nCS;
wire      pal_ws = pal_sel && REG_WE[REG_PPUDATA] && !nCS;

// Address selector
reg [4:0] ptr;
always @* begin
  if (!RENDERING && (pal_rs || pal_ws))
    ptr = VAC[4:0];
  else
    ptr = PD_PTR;
end

// row/col index
reg [2:0] row;
reg [3:0] col;

task calc_row_col;
input [4:0] ptr;
output [2:0] row;
output [1:0] col;
  begin
  casez ({ptr[4], ptr[1:0]})
    3'bz00: row = 3'd0;         // pal_row0
    3'b101: row = 3'd1;         // pal_row6
    3'b001: row = 3'd2;         // pal_row2
    3'b110: row = 3'd3;         // pal_row5
    3'b010: row = 3'd4;         // pal_row1
    3'b111: row = 3'd5;         // pal_row7
    3'b011: row = 3'd6;         // pal_row3
  endcase

  col = ptr[3:2];
  end
endtask

always @* begin
  calc_row_col(ptr, row, col);
end

always @(posedge CLK) if (CC) begin
  if (pal_ws)
    ram[row][col] <= IO_D_I[5:0];
end

always @* begin
  PD_O = 6'hxx;
  if (!pal_ws)
    PD_O = ram[row][col];
end

assign IO_D_O = {2'b00, PD_O};
assign IO_D_OE = pal_rs;

// Debug monitor interface
reg [2:0] mon_row;
reg [3:0] mon_col;

wire [4:0] mon_ptr = MON_SEL[4:2] * 3'd4;

always @(posedge CLK) begin
  MON_DOUT <= 32'h0;
  MON_VALID <= 1'b0;

  if (MON_READY) begin
    //``REGION_REGS NESBUS_CTRL
    //``SUBREGS PPU_MON_SEL
    if (MON_SEL >= 8'h10 && MON_SEL <= 8'h17) begin //``REG_ARRAY PAL
      calc_row_col(mon_ptr + 5'd0, mon_row, mon_col);
      MON_DOUT[0*8+5:0*8+0] <= ram[mon_row][mon_col];
      calc_row_col(mon_ptr + 5'd1, mon_row, mon_col);
      MON_DOUT[1*8+5:1*8+0] <= ram[mon_row][mon_col];
      calc_row_col(mon_ptr + 5'd2, mon_row, mon_col);
      MON_DOUT[2*8+5:2*8+0] <= ram[mon_row][mon_col];
      calc_row_col(mon_ptr + 5'd3, mon_row, mon_col);
      MON_DOUT[3*8+5:3*8+0] <= ram[mon_row][mon_col];
      MON_VALID <= 1'b1;
    end
  end
end

endmodule
