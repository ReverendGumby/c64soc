module rp2c02_sync_gen
  (
   input       CLK,
   input       CC,
   input       nCC,
   input       HOLD,
   
   input [8:0] COL,
   input [8:0] ROW,
   input       DOT_SKIPPED,

   output reg  PCE,
   output reg  DE,
   output reg  HSYNC,
   output reg  VSYNC,
   output reg  BLANK
   );

`include "timings.vh"

reg       visible;

wire      visible_row = ROW >= FIRST_ROW_RENDER && ROW <= LAST_ROW_RENDER;

always @* begin
  // Enable DE for visible region...
  visible = visible_row && (COL >= FIRST_COL_RENDER && COL <= LAST_COL_RENDER);
  // plus right border...
  if (visible_row)
    visible = visible || COL <= LAST_COL_RIGHT;
  // plus left border, which is on the previous row. Avoid partial
  // borders on pre-render and last render rows.
  if (ROW < LAST_ROW_RENDER || ROW == LAST_ROW_PRE_RENDER)
    visible = visible || COL >= FIRST_COL_LEFT;
  // Add one more pixel if last dot of pre-render row was skipped.
  if (ROW == 0 && COL == LAST_COL_RIGHT + 1)
    visible = visible || DOT_SKIPPED;
end

always @(posedge CLK) if (CC) begin
  DE <= visible;
  HSYNC <= !(COL >= FIRST_COL_HSYNC && COL <= LAST_COL_HSYNC);
  VSYNC <= !(ROW >= FIRST_ROW_VSYNC && ROW <= LAST_ROW_VSYNC);
  BLANK <= !(HSYNC & VSYNC);
end

always @* PCE = CC & ~HOLD;

endmodule
