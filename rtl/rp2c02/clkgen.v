module rp2c02_clkgen
  (
   input  CLK_RESET,
   input  CLK,

   output CC,
   output nCC
   );

reg [1:0] div4_clk;
reg       reset;

// This initial state aligns us with CPU (RP2A03) clocks per
// https://forums.nesdev.org/viewtopic.php?p=110854 alignment 742, the
// most common of the four CPU-PPU alignments.
//
// rp2a03_clkgen CP1_POSEDGE rises 1.5x CLK after rp2c02_clkgen CC.

localparam [1:0] div4_clk_init = 2'b10;

initial begin
  div4_clk = div4_clk_init;
  reset = 1'b1;
end

always @(posedge CLK)
  reset <= CLK_RESET;

always @(posedge CLK)
  if (reset)
    div4_clk <= div4_clk_init;
  else
    div4_clk[1:0] <= div4_clk + 1'b1;

assign CC =  ~reset & div4_clk == 2'b00;
assign nCC = ~reset & div4_clk == 2'b10;

endmodule
