module rp2c02_color_gen
  (
   input [5:0]      PIX,
   input            BLANK,

   output reg [7:0] RED,
   output reg [7:0] GREEN,
   output reg [7:0] BLUE
   );

// Colors from https://wiki.nesdev.com/w/index.php/PPU_palettes#2C02
always @* begin
  if (BLANK)
    { RED, GREEN, BLUE } <= { 8'd000, 8'd000, 8'd000 };
  else
    case (PIX)
      6'h00: { RED, GREEN, BLUE } <= { 8'd84,  8'd84,  8'd84  };
      6'h01: { RED, GREEN, BLUE } <= { 8'd0,   8'd30,  8'd116 };
      6'h02: { RED, GREEN, BLUE } <= { 8'd8,   8'd16,  8'd144 };
      6'h03: { RED, GREEN, BLUE } <= { 8'd48,  8'd0,   8'd136 };
      6'h04: { RED, GREEN, BLUE } <= { 8'd68,  8'd0,   8'd100 };
      6'h05: { RED, GREEN, BLUE } <= { 8'd92,  8'd0,   8'd48  };
      6'h06: { RED, GREEN, BLUE } <= { 8'd84,  8'd4,   8'd0   };
      6'h07: { RED, GREEN, BLUE } <= { 8'd60,  8'd24,  8'd0   };
      6'h08: { RED, GREEN, BLUE } <= { 8'd32,  8'd42,  8'd0   };
      6'h09: { RED, GREEN, BLUE } <= { 8'd8,   8'd58,  8'd0   };
      6'h0A: { RED, GREEN, BLUE } <= { 8'd0,   8'd64,  8'd0   };
      6'h0B: { RED, GREEN, BLUE } <= { 8'd0,   8'd60,  8'd0   };
      6'h0C: { RED, GREEN, BLUE } <= { 8'd0,   8'd50,  8'd60  };
      6'h0D: { RED, GREEN, BLUE } <= { 8'd0,   8'd0,   8'd0   };
      6'h0E: { RED, GREEN, BLUE } <= { 8'd0,   8'd0,   8'd0   };
      6'h0F: { RED, GREEN, BLUE } <= { 8'd0,   8'd0,   8'd0   };
      6'h10: { RED, GREEN, BLUE } <= { 8'd152, 8'd150, 8'd152 };
      6'h11: { RED, GREEN, BLUE } <= { 8'd8,   8'd76,  8'd196 };
      6'h12: { RED, GREEN, BLUE } <= { 8'd48,  8'd50,  8'd236 };
      6'h13: { RED, GREEN, BLUE } <= { 8'd92,  8'd30,  8'd228 };
      6'h14: { RED, GREEN, BLUE } <= { 8'd136, 8'd20,  8'd176 };
      6'h15: { RED, GREEN, BLUE } <= { 8'd160, 8'd20,  8'd100 };
      6'h16: { RED, GREEN, BLUE } <= { 8'd152, 8'd34,  8'd32  };
      6'h17: { RED, GREEN, BLUE } <= { 8'd120, 8'd60,  8'd0   };
      6'h18: { RED, GREEN, BLUE } <= { 8'd84,  8'd90,  8'd0   };
      6'h19: { RED, GREEN, BLUE } <= { 8'd40,  8'd114, 8'd0   };
      6'h1A: { RED, GREEN, BLUE } <= { 8'd8,   8'd124, 8'd0   };
      6'h1B: { RED, GREEN, BLUE } <= { 8'd0,   8'd118, 8'd40  };
      6'h1C: { RED, GREEN, BLUE } <= { 8'd0,   8'd102, 8'd120 };
      6'h1D: { RED, GREEN, BLUE } <= { 8'd0,   8'd0,   8'd0   };
      6'h1E: { RED, GREEN, BLUE } <= { 8'd0,   8'd0,   8'd0   };
      6'h1F: { RED, GREEN, BLUE } <= { 8'd0,   8'd0,   8'd0   };
      6'h20: { RED, GREEN, BLUE } <= { 8'd236, 8'd238, 8'd236 };
      6'h21: { RED, GREEN, BLUE } <= { 8'd76,  8'd154, 8'd236 };
      6'h22: { RED, GREEN, BLUE } <= { 8'd120, 8'd124, 8'd236 };
      6'h23: { RED, GREEN, BLUE } <= { 8'd176, 8'd98,  8'd236 };
      6'h24: { RED, GREEN, BLUE } <= { 8'd228, 8'd84,  8'd236 };
      6'h25: { RED, GREEN, BLUE } <= { 8'd236, 8'd88,  8'd180 };
      6'h26: { RED, GREEN, BLUE } <= { 8'd236, 8'd106, 8'd100 };
      6'h27: { RED, GREEN, BLUE } <= { 8'd212, 8'd136, 8'd32  };
      6'h28: { RED, GREEN, BLUE } <= { 8'd160, 8'd170, 8'd0   };
      6'h29: { RED, GREEN, BLUE } <= { 8'd116, 8'd196, 8'd0   };
      6'h2A: { RED, GREEN, BLUE } <= { 8'd76,  8'd208, 8'd32  };
      6'h2B: { RED, GREEN, BLUE } <= { 8'd56,  8'd204, 8'd108 };
      6'h2C: { RED, GREEN, BLUE } <= { 8'd56,  8'd180, 8'd204 };
      6'h2D: { RED, GREEN, BLUE } <= { 8'd60,  8'd60,  8'd60  };
      6'h2E: { RED, GREEN, BLUE } <= { 8'd0,   8'd0,   8'd0   };
      6'h2F: { RED, GREEN, BLUE } <= { 8'd0,   8'd0,   8'd0   };
      6'h30: { RED, GREEN, BLUE } <= { 8'd236, 8'd238, 8'd236 };
      6'h31: { RED, GREEN, BLUE } <= { 8'd168, 8'd204, 8'd236 };
      6'h32: { RED, GREEN, BLUE } <= { 8'd188, 8'd188, 8'd236 };
      6'h33: { RED, GREEN, BLUE } <= { 8'd212, 8'd178, 8'd236 };
      6'h34: { RED, GREEN, BLUE } <= { 8'd236, 8'd174, 8'd236 };
      6'h35: { RED, GREEN, BLUE } <= { 8'd236, 8'd174, 8'd212 };
      6'h36: { RED, GREEN, BLUE } <= { 8'd236, 8'd180, 8'd176 };
      6'h37: { RED, GREEN, BLUE } <= { 8'd228, 8'd196, 8'd144 };
      6'h38: { RED, GREEN, BLUE } <= { 8'd204, 8'd210, 8'd120 };
      6'h39: { RED, GREEN, BLUE } <= { 8'd180, 8'd222, 8'd120 };
      6'h3A: { RED, GREEN, BLUE } <= { 8'd168, 8'd226, 8'd144 };
      6'h3B: { RED, GREEN, BLUE } <= { 8'd152, 8'd226, 8'd180 };
      6'h3C: { RED, GREEN, BLUE } <= { 8'd160, 8'd214, 8'd228 };
      6'h3D: { RED, GREEN, BLUE } <= { 8'd160, 8'd162, 8'd160 };
      6'h3E: { RED, GREEN, BLUE } <= { 8'd0,   8'd0,   8'd0   };
      6'h3F: { RED, GREEN, BLUE } <= { 8'd0,   8'd0,   8'd0   };
      default: { RED, GREEN, BLUE } <= { 8'dx, 8'dx, 8'dx };
    endcase
end

endmodule
