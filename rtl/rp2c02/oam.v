module rp2c02_oam
  (
   input             CLK,
   input             CC,
   input             nCC,

   // debug monitor
   input [8:2]       MON_SEL,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,

   // access controls
   input [7:0]       A, // address
   input             WE,
   input             RE,
   input             SEC, // select secondary OAM
   input             CLR, // force read data to 8'hff
   input             IO_SEL, // use I/O data bus
   
   // internal I/O data bus
   input [7:0]       IO_D_I,
   output [7:0]      IO_D_O,
   output            IO_D_OE,

   // internal sprite data bus spr_d*
   output reg [7:0]  SD
   );

// Physical DRAM array: 32 rows x 66 columns
//   row select = A[7:3], column select = A[2:0] + 1^
//     6x columns D[4:2] for A[2:0] == 3'bx10 do not exist
//     ^includes 32x8 secondary OAM, column select = spr_ptr5

// For simplicity, just use flat arrays for primary and secondary OAM.
reg [7:0] pri [0:8'hff], sec [0:8'h1f];

//////////////////////////////////////////////////////////////////////
// Array read / write

reg [7:0] wd;

always @* begin
  wd = 8'h00;

  if (IO_SEL)
    wd = IO_D_I;
  else
    wd = SD;

  // Emulate missing columns D[4:2] for A[2:0] == 3'bx10
  if (!SEC && A[1:0] == 2'b10)
    wd[4:2] = 3'b000;
end

always @(posedge CLK) if (WE && nCC) begin
  if (!SEC)
    pri[A] <= wd;
  else
    sec[A[4:0]] <= wd;
end

always @(posedge CLK) if (RE && nCC) begin
  if (CLR)                      // used to clear sec @ col 1-64
    SD <= 8'hff;
  else begin
    if (!SEC)
      SD <= pri[A];
    else
      SD <= sec[A[4:0]];
  end
end

assign IO_D_O = SD;
assign IO_D_OE = IO_SEL && RE;

// Debug monitor interface
wire [7:0] mon_pri_a = (MON_SEL[8:2] - 7'h18) * 8'd4;
wire [5:0] mon_sec_a = (MON_SEL[8:2] - 7'h58) * 3'd4;

always @(posedge CLK) begin
  MON_DOUT <= 32'h0;
  MON_VALID <= 1'b0;

  if (MON_READY) begin
    //``REGION_REGS NESBUS_CTRL
    //``SUBREGS PPU_MON_SEL
    if (MON_SEL >= 7'h18 && MON_SEL <= 7'h57) begin //``REG_ARRAY OAM_PRI
      MON_DOUT[0*8+7:0*8+0] <= pri[mon_pri_a + 8'h0];
      MON_DOUT[1*8+7:1*8+0] <= pri[mon_pri_a + 8'h1];
      MON_DOUT[2*8+7:2*8+0] <= pri[mon_pri_a + 8'h2];
      MON_DOUT[3*8+7:3*8+0] <= pri[mon_pri_a + 8'h3];
      MON_VALID <= 1'b1;
    end
    else if (MON_SEL >= 7'h58 && MON_SEL <= 7'h5f) begin //``REG_ARRAY OAM_SEC
      MON_DOUT[0*8+7:0*8+0] <= sec[mon_sec_a + 5'h0];
      MON_DOUT[1*8+7:1*8+0] <= sec[mon_sec_a + 5'h1];
      MON_DOUT[2*8+7:2*8+0] <= sec[mon_sec_a + 5'h2];
      MON_DOUT[3*8+7:3*8+0] <= sec[mon_sec_a + 5'h3];
      MON_VALID <= 1'b1;
    end
  end
end

endmodule
