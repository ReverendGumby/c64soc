function [7:0] swab;
input [7:0] x;
integer i;
  begin
    for (i = 0; i < 8; i = i + 1)
      swab[7 - i] = x[i];
  end
endfunction
