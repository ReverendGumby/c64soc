module rp2c02_reg_file
  (
   input             CLK,
   input             CC,
   input             nCC,
   input             RES_COLD,
   input             RES, // to implement cold reset
   input             RES2,
   input             HOLD,
   input             VBL_START,
   input             VBL_END,

   // debug monitor
   input [8:2]       MON_SEL,
   input             MON_READY,
   output reg [31:0] MON_DOUT,
   output reg        MON_VALID,

   // internal I/O data bus
   input [7:0]       IO_D_I,
   output reg [7:5]  IO_D_O,
   output reg        IO_D_OE,
   input [2:0]       RA,
   input             RW,
   input             nCS,

   output [9:0]      REG_WE, // Register write enable
   output [9:0]      REG_RE, // Register read enable

   // $2000 PPUCTRL
   output reg        VBL_EN, // enable nVBL
   output reg        SLAVE_MODE, // PPU master/slave
   output reg        SPR_SIZE, // sprite size (double width)
   output reg        BKG_PAT, // background pattern table A[12]
   output reg        SPR_PAT, // sprite pattern table A[12]
   output reg        ADDR_INC, // VRAM address increment (+1/+32)
   // $2001 PPUMASK
   output reg [2:0]  EMPH, // emphasize blue/green/red
   output reg        SPR_EN, // show sprites
   output reg        BKG_EN, // show background
   output reg        SPR_CLIP, // show sprites in leftmost tile
   output reg        BKG_CLIP, // show background in leftmost tile
   output reg        PAL_MONO, // greyscale
   // $2002 PPUSTATUS
   output            VBL_FLAG,
   input             SET_SPR0_HIT,
   input             SPR_OVERFLOW
   );

`include "timings.vh"
`include "reg_idx.vh"

reg [1:0] emph_reg;
reg       spr_size_reg;
reg       bkg_pat_reg;
reg       spr_pat_reg;
reg       addr_inc_reg;
reg       spr_en_reg;
reg       bkg_en_reg;
reg       spr_clip_reg;
reg       bkg_clip_reg;

wire      ppustatus_rs;

//////////////////////////////////////////////////////////////////////
// Video blanking interval flag

reg vbl_flag_d, vbl_set, vbl_clear_flags, vbl_buf;

initial begin
  VBL_EN = 1'b0;
  vbl_flag_d = 1'b0;
  vbl_set = 1'b0;
  vbl_clear_flags = 1'b0;
end

always @(posedge CLK) if (!HOLD) begin
  if (RES && RES_COLD)
    vbl_set <= 1'b0;
  else begin
    if (CC && VBL_START)
      vbl_set <= 1'b1;
    else if (nCC)
      // vbl_set is high only for 1/2 cycle.
      vbl_set <= 1'b0;
  end
end

always @(posedge CLK) if (!HOLD) begin
  if (RES && RES_COLD)
    vbl_clear_flags <= 1'b0;
  else if (CC)
    vbl_clear_flags <= VBL_END;
end

wire vbl_clr = (RES && RES_COLD) || vbl_clear_flags || ppustatus_rs;

assign VBL_FLAG = (vbl_flag_d | vbl_set) & ~vbl_clr;

always @(posedge CLK) begin
  vbl_flag_d <= VBL_FLAG;

  if (!ppustatus_rs)
    vbl_buf <= VBL_FLAG;
end

//////////////////////////////////////////////////////////////////////
// Sprite 0 hit flag

reg spr0_hit_d;

initial
  spr0_hit_d = 1'b0;

wire spr0_clr = vbl_clear_flags;

wire spr0_hit = (spr0_hit_d | SET_SPR0_HIT) & ~spr0_clr;

always @(posedge CLK) begin
  spr0_hit_d <= spr0_hit;
end

//////////////////////////////////////////////////////////////////////
// Register select and data bus logic

reg [7:0] rs;                   // one-hot register select
reg       sc_ws_d, va_ws_d;
reg       hv_sel;
reg       ncs_d;

always @(posedge CLK) if (!HOLD)
  ncs_d <= nCS;
wire ncs_posedge = nCS & ~ncs_d;

always @* begin
  rs = 8'h00;
  rs[RA] = 1'b1;
end

assign REG_RE[REG_PPUSTATUS]    =  RW && rs[2];
assign REG_RE[REG_OAMDATA]      =  RW && rs[4];
assign REG_RE[REG_PPUDATA]      =  RW && rs[7];

assign REG_WE[REG_PPUCTRL]      = !RW && rs[0];
assign REG_WE[REG_PPUMASK]      = !RW && rs[1];
assign REG_WE[REG_OAMADDR]      = !RW && rs[3];
assign REG_WE[REG_OAMDATA]      = !RW && rs[4];
assign REG_WE[REG_PPUSCROLL_H]  = !RW && rs[5] && !hv_sel;
assign REG_WE[REG_PPUSCROLL_V]  = !RW && rs[5] &&  hv_sel;
assign REG_WE[REG_PPUADDR_H]    = !RW && rs[6] && !hv_sel;
assign REG_WE[REG_PPUADDR_L]    = !RW && rs[6] &&  hv_sel;
assign REG_WE[REG_PPUDATA]      = !RW && rs[7];

assign ppustatus_rs = !nCS && REG_RE[REG_PPUSTATUS];

wire ppuctrl_ws = !nCS && REG_WE[REG_PPUCTRL];
wire ppumask_ws = !nCS && REG_WE[REG_PPUMASK];

// Toggle between PPUSCROLL_H/V and PPUADDR_H/L. Reset on PPUSTATUS read.
wire hv_sel_tog = (!nCS &&
                   (((REG_WE[REG_PPUSCROLL_H] || REG_WE[REG_PPUSCROLL_V] ||
                      REG_WE[REG_PPUADDR_H] || REG_WE[REG_PPUADDR_L])) ||
                    (hv_sel && REG_RE[REG_PPUSTATUS])));

reg hv_sel_tog_d;
always @(posedge CLK) begin
  if (RES2)
    hv_sel <= 1'b0;
  else if (ncs_posedge && hv_sel_tog_d)
    hv_sel <= !hv_sel;

  hv_sel_tog_d <= hv_sel_tog;
end

// On PPUCTRL/PPUMASK write, most registers are updated (observed by
// the rest of the chip) after nCS deasserts. A few registers are
// updated immediately when nCS asserts: SLAVE_MODE, VBL_EN, PAL_MONO,
// EMPH[2].
always @(posedge CLK) if (!HOLD) begin
  if (RES2) begin
    {VBL_EN, SLAVE_MODE, spr_size_reg, bkg_pat_reg, spr_pat_reg, addr_inc_reg} <= 6'h00;
    {EMPH[2], emph_reg[1:0], spr_en_reg, bkg_en_reg, spr_clip_reg, bkg_clip_reg, PAL_MONO} <= 8'h00;
  end
  else begin
    if (ppuctrl_ws)
      {VBL_EN, SLAVE_MODE, spr_size_reg, bkg_pat_reg, spr_pat_reg, addr_inc_reg} <= IO_D_I[7:2];
    if (ppumask_ws)
      {EMPH[2], emph_reg[1:0], spr_en_reg, bkg_en_reg, spr_clip_reg, bkg_clip_reg, PAL_MONO} <= IO_D_I;
  end

  if (!ppuctrl_ws)
    {SPR_SIZE, BKG_PAT, SPR_PAT, ADDR_INC} <= {spr_size_reg, bkg_pat_reg, spr_pat_reg, addr_inc_reg};
  if (!ppumask_ws)
    {EMPH[1:0], SPR_EN, BKG_EN, SPR_CLIP, BKG_CLIP} <= {emph_reg[1:0], spr_en_reg, bkg_en_reg, spr_clip_reg, bkg_clip_reg};
end

always @* begin
  IO_D_O[7:5] = 3'hx;
  IO_D_OE = 1'b0;
  if (ppustatus_rs) begin
      IO_D_O[7:5] = {vbl_buf, spr0_hit, SPR_OVERFLOW};
      IO_D_OE = 1'b1;
  end
end

// Debug monitor interface
always @(posedge CLK) begin
  MON_DOUT <= 32'h0;
  MON_VALID <= 1'b0;

  if (MON_SEL[8:4] == 5'h00 && MON_READY) begin
    //``REGION_REGS NESBUS_CTRL
    case (MON_SEL[8:2])         //``SUBREGS PPU
      8'h00: begin              //``REG CMS
        MON_DOUT[2] <= ADDR_INC;
        MON_DOUT[3] <= SPR_PAT;
        MON_DOUT[4] <= BKG_PAT;
        MON_DOUT[5] <= SPR_SIZE;
        MON_DOUT[6] <= SLAVE_MODE;
        MON_DOUT[7] <= VBL_EN;
        MON_DOUT[8] <= PAL_MONO;
        MON_DOUT[9] <= BKG_CLIP;
        MON_DOUT[10] <= SPR_CLIP;
        MON_DOUT[11] <= BKG_EN;
        MON_DOUT[12] <= SPR_EN;
        MON_DOUT[15:13] <= EMPH;
        MON_DOUT[21] <= SPR_OVERFLOW;
        MON_DOUT[22] <= spr0_hit;
        MON_DOUT[23] <= VBL_FLAG;
      end
    endcase
    MON_VALID <= 1'b1;
  end
end
  
endmodule
