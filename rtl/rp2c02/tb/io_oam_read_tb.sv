`timescale 1ns / 1ps

module io_oam_read_tb();

`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("io_oam_read.vcd");
  $dumpvars;
end

task init_oam;
  // Set known values in OAM.
  for (integer i = 0; i < 256; i++) begin
    ppu.oam.pri[i] = i;
    if (i[1:0] == 2'b10)
      ppu.oam.pri[i][4:2] = 3'b000;
  end
endtask

initial begin
  #8 @(posedge clk) res = 0;

  // Skip to vbl_end and res2 set.
  #4 ppu.video_counter.ROW = 9'h104;
  ppu.video_counter.COL = 9'h153;
  #4 @(posedge clk) ;

  init_oam();

  set_reg(3'h0, 8'h00);
  get_reg(3'h2, 8'h00, 8'h80);

  set_reg(3'h3, 8'h00);

  #4 ;
  for (integer i = 0; i < 256; i++) begin
    get_reg(3'h4, ppu.oam.pri[i]);
    #5 ;
  end

  $finish();
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s io_oam_read_tb -o io_oam_read_tb.vvp -f clk_ppu_vram.files io_oam_read_tb.sv && ./io_oam_read_tb.vvp"
// End:
