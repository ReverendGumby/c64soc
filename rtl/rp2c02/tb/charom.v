module charom
  (
   input        CLK,
   input        nCE,
   input        nOE,
   input [12:0] A,
   output [7:0] DB_O,
   output       DB_OE
   );

reg [31:0]      mem [0:11'h7ff];
reg [7:0]       d;

wire [10:0]     mem_a = A[12:2];

always @* begin
  d = 8'hxx;
  case (A[1:0])
    2'b00: d = mem[mem_a][7:0];
    2'b01: d = mem[mem_a][15:8];
    2'b10: d = mem[mem_a][23:16];
    2'b11: d = mem[mem_a][31:24];
  endcase
end

assign DB_OE = !(nCE || nOE);
assign DB_O = d;

endmodule
