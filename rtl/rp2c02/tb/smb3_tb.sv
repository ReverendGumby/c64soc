`timescale 1ns / 1ps

module smb3_tb();

`define DUMP_PIC_HEX "smb3.hex"

`include "../timings.vh"
`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("smb3.vcd");
  $dumpvars;

  $readmemh("vram-smb3.hex", vram.mem);
  $readmemh("charom-smb3.hex", charom.mem);
  $readmemh("oam-smb3.hex", ppu.oam.pri);
  //$readmemh("oamsec-smb3.hex", ppu.oam.sec);
  $readmemh("pal-smb3.hex", init_pal);
end

initial begin
  #8 @(posedge clk) res <= 0;

  // Skip to vbl_end and res2 set.
  #8 ;
  ppu.video_counter.ROW = 9'h104;
  ppu.video_counter.COL = 9'h153;
  #4 @(posedge clk) ;

  // Skip to vbl_start, enable rendering, and load palette.
  #8 ;
  ppu.video_counter.ROW = 9'd241;
  ppu.video_counter.COL = 9'h0;
  #4 @(posedge clk) ;
  set_reg(3'h0, 8'hA8);
  set_reg(3'h1, 8'h1E);

  load_palette();

  set_reg(3'h6, 8'h00);
  set_reg(3'h6, 8'h00);

  // Run the first render row.
  while (!(ppu.video_counter.ROW == 9'd1 && ppu.video_counter.COL == 9'd0))
    @(posedge clk) ;

  // Skip to the last render row minus 1.
  ppu.video_counter.ROW = 9'd238;
  ppu.bus_master.vac = 15'h63A2;

  // Run the last render row, through VBL, to the first render row.
  #((263-238)*NUM_COLS*4) $finish();
//  #(300*NUM_COLS*4) $finish();
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s smb3_tb -o smb3_tb.vvp -f clk_ppu_vram.files smb3_tb.sv && ./smb3_tb.vvp"
// End:
