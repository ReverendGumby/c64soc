`timescale 1ns / 1ps

module io_oam_write_tb();

`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("io_oam_write.vcd");
  $dumpvars;
end

initial begin
  #8 @(posedge clk) res = 0;

  // Skip to vbl_end and res2 set.
  #4 ppu.video_counter.ROW = 9'h104;
  ppu.video_counter.COL = 9'h153;
  #4 @(posedge clk) ;

  set_reg(3'h0, 8'h00);
  get_reg(3'h2, 8'h00, 8'h80);

  set_reg(3'h3, 8'h00);

  #4 ;
  for (integer i = 0; i < 256; i++) begin
    set_reg(3'h4, i);
    assert(ppu.oam.IO_D_I == i);
    #5 check_oam_write(i);
  end

  dump_oam();
  $finish();
end

task check_oam_write(input integer i);
reg [7:0] want;
  want = i;
  // Byte 2 bits 4:2 are always 0.
  if (i[1:0] == 2'b10)
    want[4:2] = 3'b000;

  assert(ppu.oam.pri[i] == want);
endtask

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s io_oam_write_tb -o io_oam_write_tb.vvp -f clk_ppu_vram.files io_oam_write_tb.sv && ./io_oam_write_tb.vvp"
// End:
