`timescale 1ns / 1ps

module io_vram_write_tb();

`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("io_vram_write.vcd");
  $dumpvars;
end

initial begin
  #8 @(posedge clk) res = 0;

  // Skip to vbl_end and res2 set.
  #4 ppu.video_counter.ROW = 9'h104;
  ppu.video_counter.COL = 9'h153;
  #4 @(posedge clk) ;

  set_reg(3'h0, 8'h00);
  get_reg(3'h2, 8'h00, 8'h80);

  set_reg(3'h6, 8'h20);
  set_reg(3'h6, 8'h80);

  #4 ;
  for (integer i = 0; i < 10; i++) begin
    set_reg(3'h7, i);
    fork
      check_vram_write(i);
      #17 ;
    join
  end

  $finish();
end

task check_vram_write(input integer i);
integer start, now, delta;
  start = $time;
  @(posedge ale) now = $time;
  delta = now - start;
  $display("check_vram_write: %1t-%1t = %1t", now, start, delta);
  assert(delta >= 2 && delta < 6);
  assert(clk && ppu.clkgen.div4_clk == 2'b01);
  #3 assert(ale && !nBWRITE);
  assert(ba_o[13:8] == 6'h20 && bd_o_ppu == 8'h80 + i);
  #2 assert(!ale && !nBWRITE);
  assert(bd_o == i);
  #2 assert(!ale && nBWRITE);
endtask

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s io_vram_write_tb -o io_vram_write_tb.vvp -f clk_ppu_vram.files io_vram_write_tb.sv && ./io_vram_write_tb.vvp"
// End:
