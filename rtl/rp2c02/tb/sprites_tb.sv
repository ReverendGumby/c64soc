`timescale 1ns / 1ps

module sprites_tb();

`define DUMP_PIC_HEX "sprites.hex"

`include "../timings.vh"
`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("sprites.vcd");
  $dumpvars;

  $readmemh("vram-sprites.hex", vram.mem);
  $readmemh("charom-test.hex", charom.mem);
  $readmemh("oam-sprites.hex", ppu.oam.pri);
  $readmemh("pal-sprites.hex", init_pal);
end

initial begin
integer fscr;
integer c;
  fscr = $fopen("sprites.scr", "r");
  if (fscr != 0) begin

    for (integer y = 0; y < 30; y++) begin
      for (integer x = 0; x < 32; x++) begin
        c = $fgetc(fscr);
        vram.mem[y * 32 + x] = c;
      end
      while ($fgetc(fscr) != "\n")
        ;
    end
    $fclose(fscr);
  end
end

initial begin
  #8 @(posedge clk) res <= 0;

  // Skip to vbl_end and res2 set.
  #8 ;
  ppu.video_counter.ROW = 9'h104;
  ppu.video_counter.COL = 9'h153;
  #4 @(posedge clk) ;

  // Skip to vbl_start, enable rendering, and load palette.
  #8 ;
  ppu.video_counter.ROW = 9'd241;
  ppu.video_counter.COL = 9'h0;
  #4 @(posedge clk) ;
  set_reg(3'h0, 8'h90);
  set_reg(3'h1, 8'h1E);

  load_palette();

  set_reg(3'h6, 8'h00);
  set_reg(3'h6, 8'h00);

  // Skip to just before pre-render row.
  ppu.video_counter.ROW = 9'h104;
  ppu.video_counter.COL = 9'h150;
  #4 @(posedge clk) ;

  //#((64)*NUM_COLS*4) $finish();
  #((NUM_ROWS+10)*NUM_COLS*4) $finish();
end

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s sprites_tb -o sprites_tb.vvp -f clk_ppu_vram.files sprites_tb.sv && ./sprites_tb.vvp"
// End:
