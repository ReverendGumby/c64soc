`timescale 1ns / 1ps

module io_pal_read_tb();

`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("io_pal_read.vcd");
  $dumpvars;
end

task get_expected_color(input [5:0] idx, output [5:0] ce);
  begin
  end  
endtask

reg [5:0] c;

initial begin
  // Set known values in palette RAM.
  ppu.palette.ram[0][0] = 6'h10;
  ppu.palette.ram[0][1] = 6'h14;
  ppu.palette.ram[0][2] = 6'h18;
  ppu.palette.ram[0][3] = 6'h1c;
  ppu.palette.ram[1][0] = 6'h11;
  ppu.palette.ram[1][1] = 6'h15;
  ppu.palette.ram[1][2] = 6'h19;
  ppu.palette.ram[1][3] = 6'h1d;
  ppu.palette.ram[2][0] = 6'h01;
  ppu.palette.ram[2][1] = 6'h05;
  ppu.palette.ram[2][2] = 6'h09;
  ppu.palette.ram[2][3] = 6'h0d;
  ppu.palette.ram[3][0] = 6'h12;
  ppu.palette.ram[3][1] = 6'h16;
  ppu.palette.ram[3][2] = 6'h1a;
  ppu.palette.ram[3][3] = 6'h1e;
  ppu.palette.ram[4][0] = 6'h02;
  ppu.palette.ram[4][1] = 6'h06;
  ppu.palette.ram[4][2] = 6'h0a;
  ppu.palette.ram[4][3] = 6'h0e;
  ppu.palette.ram[5][0] = 6'h13;
  ppu.palette.ram[5][1] = 6'h17;
  ppu.palette.ram[5][2] = 6'h1b;
  ppu.palette.ram[5][3] = 6'h1f;
  ppu.palette.ram[6][0] = 6'h03;
  ppu.palette.ram[6][1] = 6'h07;
  ppu.palette.ram[6][2] = 6'h0b;
  ppu.palette.ram[6][3] = 6'h0f;

  #8 @(posedge clk) res = 0;

  // Skip to vbl_end and res2 set.
  #4 ppu.video_counter.ROW = 9'h104;
  ppu.video_counter.COL = 9'h153;
  #4 @(posedge clk) ;

  set_reg(3'h0, 8'h00);
  get_reg(3'h2, 8'h00, 8'h80);

  set_reg(3'h6, 8'h3F);
  set_reg(3'h6, 8'h00);

  #4 ;
  for (integer i = 0; i < 32; i++) begin
    c = i;
    if ({c[4], c[1:0]} == 3'b000) // row 0
      c = c + 6'h10;
    get_reg(3'h7, c);
    fork
      check_pal_read(c);
      #21 ;
    join
  end

  $finish();
end

task check_pal_read(input [5:0] ce);
integer start, now, delta;
  start = $time;
  @(posedge ale) now = $time;
  delta = now - start;
  $display("check_pal_read: %1t-%1t = %1t", now, start, delta);
  assert(delta >= 2 && delta < 6);
  assert(clk && ppu.clkgen.div4_clk == 2'b01);
  #3 assert(ale && nBREAD);
  #4 assert(!ale && nBREAD);
  #4 assert(nBREAD);
  assert(!ppu.palette.IO_D_OE);
  #2 ;
endtask

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s io_pal_read_tb -o io_pal_read_tb.vvp -f clk_ppu_vram.files io_pal_read_tb.sv && ./io_pal_read_tb.vvp"
// End:
