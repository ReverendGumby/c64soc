// https://github.com/christopherpow/nes-test-roms/tree/master/ppu_vbl_nmi
// 02-vbl_set_time
`timescale 1ns / 1ps

module vbl_set_time_tb();

`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("vbl_set_time.vcd");
  $dumpvars;
end

initial begin
  #8 @(posedge clk) res = 0;

  // Skip to vbl_end and res2 set.
  #4 ppu.video_counter.ROW = 9'h104;
  ppu.video_counter.COL = 9'h153;
  #4 @(posedge clk) ;

  set_reg(3'h0, 8'h00);
  get_reg(3'h2, 8'h00, 8'h80);

  set_reg(3'h6, 8'h20);
  set_reg(3'h6, 8'h80);
  #4 @(posedge clk) ;

/* -----\/----- EXCLUDED -----\/-----
  #4 ;
  for (integer i = 0; i < 10; i++) begin
    if (i == 0)
      get_reg(3'h7, 0, 0);
    else
      get_reg(3'h7, i-1);
    fork
      check_vram_read(i);
      #21 ;
    join
  end
 -----/\----- EXCLUDED -----/\----- */

  for (integer i = 0; i < 9; i++) begin
  reg [7:0] status;
  reg e1, e2, g1, g2;
    if (i < 4)
      {e1, e2} = 2'b01;
    else if (i > 4)
      {e1, e2} = 2'b10;
    else
      {e1, e2} = 2'b00;

    // Skip to vbl_end.
    @(negedge cc);
    #4 ppu.video_counter.ROW = 9'd260;
    ppu.video_counter.COL = 9'd339;
    #4 @(posedge clk) ;

    // Skip to before VBL starts (R241,C1).
    #4 ppu.video_counter.ROW = 9'd240;
    ppu.video_counter.COL = 9'd325;
    #4 @(negedge cc) ;

    // Delay i pixels
    #(i * 4) ;

    // Setup CPU-PPU alignment
    #0 ;  // CP1+ is CC+ - 3 PCLK : PASS
    //#1 ;  // CP1+ is CC+ - 2 PCLK : PASS
    //#2 ;  // CP1+ is CC+ - 1 PCLK : FAIL
    //#3 ;  // CP1+ is CC+ - 0 PCLK : FAIL

    // Read PPUSTATUS twice, as if the CPU was running:
    //   LDX $2002   ; 4 CPU cycles
    //   LDY $2002   ; 4 CPU cycles
    #(12 * 3);
    read_reg(3'h2, status);
    g1 = status[7];
    assert(g1 == e1);
    #(12 * 3);
    read_reg(3'h2, status);
    g2 = status[7];
    assert(g2 == e2);

    $display("vbl_set_time: T+%1d: expected %b %b, got %b %b", i, e1, e2, g1, g2);
  end

  $finish();
end

/* -----\/----- EXCLUDED -----\/-----
task check_vram_read(input integer i);
integer start, now, delta;
  start = $time;
  @(posedge ale) now = $time;
  delta = now - start;
  $display("check_vram_read: %1t-%1t = %1t", now, start, delta);
  assert(delta >= 2 && delta < 6);
  assert(clk && ppu.clkgen.div4_clk == 2'b01);
  #3 assert(ale && nBREAD);
  #4 assert(!ale && nBREAD);
  #4 assert(!nBREAD);
  assert(bd_o == i);
  #2 assert(nBREAD);
endtask
 -----/\----- EXCLUDED -----/\----- */

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s vbl_set_time_tb -o vbl_set_time_tb.vvp -f clk_ppu_vram.files vbl_set_time_tb.sv && ./vbl_set_time_tb.vvp"
// End:
