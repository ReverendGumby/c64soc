// https://github.com/christopherpow/nes-test-roms/tree/master/ppu_vbl_nmi
// 02-vbl_clear_time
`timescale 1ns / 1ps

module vbl_clear_time_tb();

`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("vbl_clear_time.vcd");
  $dumpvars;
end

initial begin
  #8 @(posedge clk) res = 0;

  // Skip to vbl_end and res2 set.
  #4 ppu.video_counter.ROW = 9'h104;
  ppu.video_counter.COL = 9'h153;
  #4 @(posedge clk) ;

  set_reg(3'h0, 8'h00);
  get_reg(3'h2, 8'h00, 8'h80);

  set_reg(3'h6, 8'h20);
  set_reg(3'h6, 8'h80);
  #4 @(posedge clk) ;

/* -----\/----- EXCLUDED -----\/-----
  #4 ;
  for (integer i = 0; i < 10; i++) begin
    if (i == 0)
      get_reg(3'h7, 0, 0);
    else
      get_reg(3'h7, i-1);
    fork
      check_vram_read(i);
      #21 ;
    join
  end
 -----/\----- EXCLUDED -----/\----- */

  for (integer i = 0; i < 9; i++) begin
  reg [7:0] status;
  reg e, g;
    if (i < 6)
      e = 1'b1;
    else
      e = 1'b0;

    // Skip to vbl_start.
    @(negedge cc);
    #4 ppu.video_counter.ROW = 9'd240;
    ppu.video_counter.COL = 9'd340;
    #4 @(posedge clk) ;

    // Skip to before VBL ends (R261,C1).
    #4 ppu.video_counter.ROW = 9'd260;
    ppu.video_counter.COL = 9'd324;
    #4 @(negedge cc) ;

    // Delay 2273 CPU clocks
    //#(2273 * 3 * 4) ;

    // Delay i pixels
    #(i * 4) ;

    // Setup CPU-PPU alignment
    #0 ;  // CP1+ is CC+ - 3 PCLK : PASS
    //#1 ;  // CP1+ is CC+ - 2 PCLK : PASS
    //#2 ;  // CP1+ is CC+ - 1 PCLK : FAIL
    //#3 ;  // CP1+ is CC+ - 0 PCLK : FAIL

    // Read PPUSTATUS, as if the CPU was running:
    //   LDA $2002   ; 4 CPU cycles
    #(12 * 3);
    read_reg(3'h2, status);
    g = status[7];
    assert(g == e);

    $display("vbl_clear_time: T+%1d: expected %b, got %b", i, e, g);
  end

  $finish();
end

/* -----\/----- EXCLUDED -----\/-----
task check_vram_read(input integer i);
integer start, now, delta;
  start = $time;
  @(posedge ale) now = $time;
  delta = now - start;
  $display("check_vram_read: %1t-%1t = %1t", now, start, delta);
  assert(delta >= 2 && delta < 6);
  assert(clk && ppu.clkgen.div4_clk == 2'b01);
  #3 assert(ale && nBREAD);
  #4 assert(!ale && nBREAD);
  #4 assert(!nBREAD);
  assert(bd_o == i);
  #2 assert(nBREAD);
endtask
 -----/\----- EXCLUDED -----/\----- */

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s vbl_clear_time_tb -o vbl_clear_time_tb.vvp -f clk_ppu_vram.files vbl_clear_time_tb.sv && ./vbl_clear_time_tb.vvp"
// End:
