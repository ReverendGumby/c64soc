`timescale 1ns / 1ps

module io_vram_read_tb();

`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("io_vram_read.vcd");
  $dumpvars;
end

initial begin
  // Set known values in VRAM.
  vram.mem['h80] = 8'h00;
  vram.mem['h81] = 8'h01;
  vram.mem['h82] = 8'h02;
  vram.mem['h83] = 8'h03;
  vram.mem['h84] = 8'h04;
  vram.mem['h85] = 8'h05;
  vram.mem['h86] = 8'h06;
  vram.mem['h87] = 8'h07;
  vram.mem['h88] = 8'h08;
  vram.mem['h89] = 8'h09;

  #8 @(posedge clk) res = 0;

  // Skip to vbl_end and res2 set.
  #4 ppu.video_counter.ROW = 9'h104;
  ppu.video_counter.COL = 9'h153;
  #4 @(posedge clk) ;

  set_reg(3'h0, 8'h00);
  get_reg(3'h2, 8'h00, 8'h80);

  set_reg(3'h6, 8'h20);
  set_reg(3'h6, 8'h80);

  #4 ;
  for (integer i = 0; i < 10; i++) begin
    if (i == 0)
      get_reg(3'h7, 0, 0);
    else
      get_reg(3'h7, i-1);
    fork
      check_vram_read(i);
      #21 ;
    join
  end

  $finish();
end

task check_vram_read(input integer i);
integer start, now, delta;
  start = $time;
  @(posedge ale) now = $time;
  delta = now - start;
  $display("check_vram_read: %1t-%1t = %1t", now, start, delta);
  assert(delta >= 2 && delta < 6);
  assert(clk && ppu.clkgen.div4_clk == 2'b01);
  #3 assert(ale && nBREAD);
  #4 assert(!ale && nBREAD);
  #4 assert(!nBREAD);
  assert(bd_o == i);
  #2 assert(nBREAD);
endtask

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s io_vram_read_tb -o io_vram_read_tb.vvp -f clk_ppu_vram.files io_vram_read_tb.sv && ./io_vram_read_tb.vvp"
// End:
