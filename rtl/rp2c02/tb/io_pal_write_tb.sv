`timescale 1ns / 1ps

module io_pal_write_tb();

`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("io_pal_write.vcd");
  $dumpvars;
end

initial begin
  #8 @(posedge clk) res = 0;

  // Skip to vbl_end and res2 set.
  #4 ppu.video_counter.ROW = 9'h104;
  ppu.video_counter.COL = 9'h153;
  #4 @(posedge clk) ;

  set_reg(3'h0, 8'h00);
  get_reg(3'h2, 8'h00, 8'h80);

  set_reg(3'h6, 8'h3F);
  set_reg(3'h6, 8'h00);

  #4 ;
  for (integer i = 0; i < 32; i++) begin
    set_reg(3'h7, i);
    assert(ppu.palette.IO_D_I == i);
    //$display("check_pal_write: 'h%2x = 6'h%02x", bd_o_ppu[7:0], i[5:0]);
    fork
      check_pal_write(i);
      #17 ;
    join
  end

  dump_palette();
  $finish();
end

task check_pal_write(input integer i);
integer start, now, delta;
  start = $time;
  @(posedge ale) now = $time;
  delta = now - start;
  assert(delta >= 2 && delta < 6);
  assert(clk && ppu.clkgen.div4_clk == 2'b01);
  #3 assert(ale && nBWRITE);
  assert(ba_o[13:8] == 6'h3F);
  #2 assert(!ale && nBWRITE);
  assert(bd_o == i);
  #2 assert(!ale && nBWRITE);
endtask

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s io_pal_write_tb -o io_pal_write_tb.vvp -f clk_ppu_vram.files io_pal_write_tb.sv && ./io_pal_write_tb.vvp"
// End:
