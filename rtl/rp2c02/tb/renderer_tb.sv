`timescale 1ns / 1ps

module renderer_tb();

`include "../timings.vh"
`include "clk_ppu_vram.svh"

initial begin
  $dumpfile("renderer_tb.vcd");
  $dumpvars;

  $readmemh("vram-smbtitle.hex", vram.mem);
  $readmemh("charom-smb.hex", charom.mem);
  $readmemh("pal-smbtitle.hex", init_pal);
end

initial begin
  #8 @(posedge clk) res <= 0;

  // Skip to vbl_end and res2 set.
  #8 ;
  ppu.video_counter.ROW = 9'h104;
  ppu.video_counter.COL = 9'h153;
  #4 @(posedge clk) ;

  // Skip to vbl_start, enable rendering, and load palette.
  #8 ;
  ppu.video_counter.ROW = 9'd241;
  ppu.video_counter.COL = 9'h0;
  #4 @(posedge clk) ;
  set_reg(3'h0, 8'h90);
  set_reg(3'h1, 8'h0E);

  load_palette();

  set_reg(3'h6, 8'h00);
  set_reg(3'h6, 8'h00);

  // Skip to just before pre-render row.
  ppu.video_counter.ROW = 9'h104;
  ppu.video_counter.COL = 9'h150;
  #4 @(posedge clk) ;

  //#((64)*NUM_COLS*4) $finish();
  #((NUM_ROWS+3)*NUM_COLS*4) $finish();
end

integer fpic, pice;
initial begin
  fpic = $fopen("render.hex", "w");
  pice = 0;
end
always @(posedge clk) begin
  if (pce && de) begin
    if (pice) begin
      pice = 0;
      $fwrite(fpic, "\n");
    end
    $fwrite(fpic, "%2x", pix);
  end
  if (!hs)
    pice = 1;
end
final
  $fclose(fpic);

endmodule

// Local Variables:
// compile-command: "iverilog -g2012 -grelative-include -s renderer_tb -o renderer_tb.vvp -f clk_ppu_vram.files renderer_tb.sv && ./renderer_tb.vvp"
// End:
