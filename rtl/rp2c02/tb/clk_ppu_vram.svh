reg [2:0]   a;
reg [7:0]   d_i;
wire [7:0]  d_o;
reg         res, clk;
reg         rw, ncs;

initial begin
  clk = 1;
  res = 1;
  rw = 1;
  ncs = 1;
end

always #0.5 begin :clkgen
  clk = !clk;
end

//////////////////////////////////////////////////////////////////////

reg [7:0] bd_o;
reg [8:2] mon_sel;
reg       mon_ready;

wire [13:8] ba_o;
wire [7:0]  bd, bd_o_charom, bd_o_ppu, bd_o_vram;
wire        bd_oe_charom, bd_oe_ppu, bd_oe_vram;
wire        cc;
wire        pce, de, hs, vs;
wire [5:0]  pix;
wire [7:0]  ppu_r, ppu_g, ppu_b;
wire [31:0] mon_dout;
wire        mon_valid;

rp2c02 ppu
  (
   .CLK_RESET(1'b0),
   .CLK(clk),
   .CC(cc),
   .RES_COLD(1'b1),
   .HOLD(1'b0),

   .MON_SEL(mon_sel),
   .MON_READY(mon_ready),
   .MON_DOUT(mon_dout),
   .MON_VALID(mon_valid),

   .RA(a),
   .D_I(d_i),
   .D_O(d_o),
   .D_OE(),
   .RW(rw),
   .nCS(ncs),

   .AD_I(bd),
   .AD_O({ba_o[13:8], bd_o_ppu}),
   .AD_OE(bd_oe_ppu),
   .ALE(ale),
   .nR(nBREAD),
   .nW(nBWRITE),

   .nSYNC(!res),
   .nVBL(),

   .PCE(pce),
   .DE(de),
   .HS(hs),
   .VS(vs),
   .PIX(pix),
   .R(ppu_r),
   .G(ppu_b),
   .B(ppu_g)
  );

// The video data bus is driven by at most one driver.
always @* begin
  case ({bd_oe_charom, bd_oe_ppu, bd_oe_vram})
    3'b100: bd_o = bd_o_charom;
    3'b010: bd_o = bd_o_ppu;
    3'b001: bd_o = bd_o_vram;
    default: bd_o = 8'hxx;
  endcase
end
assign BD_OE = bd_oe_ppu | bd_oe_vram | bd_oe_charom;
assign bd = BD_OE ? bd_o : 8'hzz;

// 74LS373 (U2)
reg [7:0] u2_d;
always @(posedge clk) if (ale)
    u2_d <= bd;

wire [7:0] ba = ale ? bd : u2_d;

sram_16k vram // U4
  (
   .CLK(clk),
   .CLKEN(1'b1),
   .nCE(~ba_o[13]),
   .nOE(nBREAD),
   .nWE(nBWRITE),
   .A({ba_o[10:8], ba}),
   .D_I(bd),
   .D_O(bd_o_vram),
   .D_OE(bd_oe_vram)
   );

// CHAROM
charom charom
  (
   .CLK(clk),
   .nCE(ba_o[13]),
   .nOE(nBREAD),
   .A({ba_o[12:8], ba}),
   .DB_O(bd_o_charom),
   .DB_OE(bd_oe_charom)
   );

initial begin
  mon_sel = 7'h00;
  mon_ready = 1'b0;
end

//////////////////////////////////////////////////////////////////////

task set_reg(input [2:0] r, input [7:0] v); 
  begin
    rw <= 1'b0;
    a <= r;
    d_i <= v;
    #5 ncs <= 1'b0;
    #7 ncs <= 1'b1;
  end
endtask

task get_reg(input [2:0] r, input [7:0] v, input [7:0] m = 8'hff);
  begin
    rw <= 1'b1;
    a <= r;
    d_i <= 8'hzz;
    #5 ncs <= 1'b0;
    #7 ncs <= 1'b1;
    assert ((d_o & m) == v);
  end
endtask

task read_reg(input [2:0] r, output [7:0] o);
  begin
    rw <= 1'b1;
    a <= r;
    d_i <= 8'hzz;
    #5 ncs <= 1'b0;
    #7 ncs <= 1'b1;
    o = d_o;
  end
endtask

//////////////////////////////////////////////////////////////////////

reg [5:0] init_pal [0:31];

task load_palette;
  begin
    get_reg(3'h2, 8'h00, 8'h00);
    set_reg(3'h6, 8'h3F);
    set_reg(3'h6, 8'h00);
    for (integer i = 0; i < 32; i ++) begin
    reg [5:0] p;
      p = init_pal[i];
      if (i >= 16 && (i % 4) == 0)
        p = init_pal[i - 16]; // clone from BG entries
      set_reg(3'h7, {2'b00, p});
      #16;
    end
  end
endtask

task dump_palette;
  begin
    for (integer r = 0; r < 7; r++)
      for (integer c = 0; c < 4; c++)
        $display("dump_palette: (%1d,%1d) = 5'h%02x", r, c, ppu.palette.ram[r][c]);
  end
endtask

task dump_oam;
  for (integer p = 0; p < 256; p++)
    $display("dump_oam: (8'h%02x) = 8'h%02x", p[7:0], ppu.oam.pri[p]);
endtask

task dump_oam_sec;  
  for (integer p = 0; p < 32; p++)
    $display("dump_oam_sec: (8'h%02x) = 8'h%02x", p[7:0], ppu.oam.sec[p]);
endtask

//////////////////////////////////////////////////////////////////////

`ifdef DUMP_PIC_HEX
integer fpic, pice;
initial begin
  fpic = $fopen(`DUMP_PIC_HEX, "w");
  pice = 0;
end
always @(posedge clk) begin
  if (pce && de) begin
    if (pice) begin
      pice = 0;
      $fwrite(fpic, "\n");
    end
    $fwrite(fpic, "%2x", pix);
  end
  if (!hs)
    pice = 1;
end
final
  $fclose(fpic);
`endif

