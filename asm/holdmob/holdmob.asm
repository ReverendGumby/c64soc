;;; holdmob
;;; Helping to find a MOB render bug on c64bus HOLD

	;; Kernal vectors
init_vic = $ff81
init_sid = $ff84
init_ram = $ff87
init_io = $ff8a
nmi_ret = $febc
irq_vec = $0314
brk_vec = $0316

	;; MOB pointers
mp0 = $07f8
	;; MOB data
mob0 = $0800
	;; VIC registers
vic_m0x = $d000
vic_m0y = $d001	
vic_mxx8 = $d010
vic_mxe = $d015
vic_mxye = $d017
vic_mxxe = $d01d
vic_ec = $d020
vic_b0c = $d021
vic_m0c = $d027
	;; Bounds
first_y = 50

	processor 6502

	seg.u bss
	org $10
x	ds 2
y   ds 1

	seg code
	org $8000

	;; Cartridge header
	dc.w res_entry
	dc.w nmi_entry
	dc.b $c3, $c2, $cd, $38, $30 ; 'CBM80'

	;; Initialize machine: RAM test, VIC, etc.
res_entry
	jsr init_sid
	jsr init_ram
	jsr init_io
	jsr init_vic

	;; Program MOB pointers
	lda #$20					; MP0 = $0800
	sta mp0

	;; Program MOB 0 data
	ldx #$0
cpy_mob0
	lda mob_pat,x
	sta mob0,x
	inx
	cpx #64
	bne cpy_mob0

	;; Program MOB 0 control
	lda #$01
	sta vic_mxe
	sta vic_mxxe
	sta vic_mxye
	lda #$01
	sta vic_m0c
	lda #0
	sta x
	sta x+1
	lda #first_y
	sta y

main_loop
	;; Compute new position
	clc
	lda x
	adc	#1
	sta x
	lda x+1
	adc #0
	cmp #2
	bne no_wrap_x
	clc
	lda y
	adc #1
	bcc no_wrap_y
	lda #first_y
no_wrap_y
	sta y
	lda #0
	sta x
no_wrap_x
	sta x+1

	;; Move MOB 0 to new position
	lda x
	sta vic_m0x
	lda x+1
	sta vic_mxx8
	lda y
	sta vic_m0y

	;; Update MOB 0 expansion
	lda y						; y[0] -> y expansion
	and #$01
	sta vic_mxye
	lda y						; y[1] -> x expansion
	and #$02
	lsr
	sta vic_mxxe

	;; Sleep
	ldx #12
sleep2
	ldy #0
sleep1
	dey
	bne sleep1
	dex
	bne sleep2

	jmp main_loop

nmi_entry
	jmp nmi_ret

	;; MOB data pattern
mob_pat
	dc.b $55, $55, $55
	dc.b $aa, $aa, $aa
	dc.b $80, $10, $01
	dc.b $00, $20, $02
	dc.b $80, $40, $05
	dc.b $00, $80, $08
	dc.b $81, $00, $11
	dc.b $02, $00, $20
	dc.b $84, $00, $41
	dc.b $08, $00, $80
	dc.b $90, $01, $01
	dc.b $20, $02, $00
	dc.b $c0, $04, $01
	dc.b $80, $08, $00
	dc.b $80, $10, $01
	dc.b $00, $20, $02
	dc.b $80, $40, $05
	dc.b $00, $80, $08
	dc.b $81, $00, $11
	dc.b $aa, $aa, $aa
	dc.b $55, $55, $55

	org $9fff, $ff				; fill to end
	dc.b $ff
;;; EOF
