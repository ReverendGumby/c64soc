set NAME=todclk
cd %~p0
call ..\common\env.bat
del rom.bin
%DASM% %NAME%.asm -f3 -orom.bin -l%NAME%.lst
if %errorlevel% neq 0 exit /b %errorlevel%
copy /b rom.hdr+rom.bin %NAME%.crt
