;;; todclk
;;; Display the CIA chip's Time Of Day clock

	;; Kernal vectors
nmi_ret = $febc
init_vic = $ff81
init_sid = $ff84
init_ram = $ff87
init_io = $ff8a
chrout = $ffd2	
plot = $fff0

	;; CIA#1 registers
tod_10ths = $dc08
tod_sec = $dc09
tod_min = $dc0a
tod_hr = $dc0b
cia_icr = $dc0d
cia_crb = $dc0f

icr_alarm = $04
crb_alarm = $80

	;; VIC registers
vic_bc = $d020
LT_BLUE = 14

	processor 6502

	seg.u bss
	org $10
bits	ds 1
sh_10ths    ds 1

	seg code
	org $8000

	;; Cartridge header
	dc.w res_entry
	dc.w nmi_entry
	dc.b $c3, $c2, $cd, $38, $30 ; 'CBM80'

	;; Initialize machine: RAM test, VIC, etc.
res_entry
	jsr init_sid
	jsr init_ram
	jsr init_io
	jsr init_vic

	;; Set alarm
    lda cia_crb
    ora #crb_alarm
    sta cia_crb                 ;Write alarm
    lda #$01
    sta tod_hr
    lda #$00
    sta tod_min
	sta tod_10ths
    lda #$10
    sta tod_sec
	lda cia_icr                 ;Clear alarm int.

	;; Start clock
    lda cia_crb
    and #~crb_alarm
    sta cia_crb                 ;Write TOD
    lda #$00
    sta tod_10ths

main_loop
	;; Signal alarm interrupt
	lda cia_icr
    and #icr_alarm
	clc
    adc #LT_BLUE
    sta vic_bc

	;; Draw clock
    ldx #10
    ldy #11
	clc
    jsr plot                    ; move cursor

	lda tod_hr
    and #$1F
    jsr print_byte
    lda #':
    jsr print_sep

	lda tod_min
    jsr print_byte
    lda #':
    jsr print_sep
	
	lda tod_sec
    jsr print_byte
    lda #'.
    jsr print_sep
	
    lda tod_10ths
    lda tod_10ths
    sta sh_10ths
    jsr print_hex

    ;; Wait for TOD to advance
wait
    lda tod_10ths
    cmp sh_10ths
    beq wait

go_main_loop
	jmp main_loop
	
print_sep
	tax
    lda #' 
    jsr chrout
    txa
    jsr chrout
    lda #' 
    jmp chrout

print_byte
	pha
	lsr
	lsr
	lsr
	lsr
	jsr print_hex
	pla
	jmp print_hex

print_hex
	and #$0F
	ora #$30
	cmp #$3A
	bmi ph1
	adc #($40-$3A)
ph1	jmp chrout

print_bits
	sta bits
	txa
	pha
	ldx #8
pb1	asl bits
	lda #'0
	adc #0
	jsr chrout
	lda #' 
	jsr chrout
	dex
	bne pb1
	pla
	tax
	rts

nmi_entry
	jmp nmi_ret

;;; End of program
	org $9fff, $ff				; fill to end
	dc.b $ff
;;; EOF
