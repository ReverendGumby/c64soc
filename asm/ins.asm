;; ins.asm

	processor 6502

reset	org $fe00, $ea

	nop			;actually test nop
	
	;; All addressing modes for store
	lda #$99
	ldx #$11
	ldy #$22
	sta $12
	sta $12,x
	stx $24
	sta $12,y
	sta $1234
	sta $1234,x
	sta $12f0,x
	sta $1234,y
	sta $12f0,y
	sta ($12,x)
	sta ($23),y
	ldy #$ee
	sta ($23),y

	;; Some fun stx/sty stuff
	ldx #$55
	ldy #$77
	stx $36
	stx $0163
	stx $64,y
	sty $37
	sty $0165
	sty $67,x

	;; Register-to-register transfers, index +/-
	ldx #$ff
	txs
	lda #$00
	tay
	dey
	tya
	iny
	inx
	tsx
	dex
	tax
	inx
	txa

	;; Register rotates
	lda #$55
	clc
	rol
	rol
	sec
	ror
	ror
	sec
	asl
	asl
	clc
	ror
	ror
	sec
	lsr
	lsr

	;; Interrupt fun
	cli
	sei

	;; Arithmetic add
	lda #$00
	clc
	cld
	adc #$20				;A=$20
	adc #$60				;A=$80,V=1
	adc #$80				;A=$00,C=1,V=1
    
	;; Arithmetic subtract
	lda #$70
	sec
	cld
	sbc #$FF				;A=$71,C=0
	sec
	sbc #$41				;A=$30
	sbc #$31				;A=$FF,C=0
	sbc #$80				;A=$7E,C=1,V=1

	;; Logic AND, OR, EOR
	lda #$AA
	and #$F0				;A=$A0,Z=0,N=1
	lda #$55
	and #$AA				;A=$00,Z=1,N=0
	ora #$84				;A=$84,Z=0,N=1
	ldx #$A6
	stx $66
	eor $C0,x				;($66)=$A6,A=$22,Z=0,N=0

	;; That rare decimal math!
	lda #$99
	clc
	sed
	adc #$11
	sta $12
	sec
	sbc #$51
	sta $12

	;; Rotate memory
	ldx #$c8
	lda #$55
	sta $2243,x
	clc
	rol $2243,x
	rol $2243,x
	sec
	ror $2243,x
	ror $2243,x
	sec
	asl $2243,x
	asl $2243,x
	clc
	ror $2243,x
	ror $2243,x
	sec
	lsr $2243,x
	lsr $2243,x

	;; Increment / decrement memory
	sec
	lda #$00
	ldx #$12
	sta $12
	sta $0100,x
	dec $0112
	inc $12
	inc $0100,x
	dec $00,x
	
	;; Test and compare
	lda #$5a
	cmp #$5a		;$5A==$5A: Z=1,N=0,C=1
	bne fail
	bcc fail
	bmi fail
	sta $12
	ldx #$5b		;$5B>$5A: Z=0,N=0,C=1
	cpx $12
	beq fail
	bmi fail
	bcc fail
	ldy #$59
	cpy $12			;$59<$5A: Z=0,N=1,C=0
	beq fail
	bpl fail
	bcs fail
	bit $12			;$5A^$5A=$5A: Z=0,N=0,V=1
	beq fail
	bmi fail
	bvc fail

	jmp branch

fail	jmp $0000

	

	org $fefc, $ea
back	beq fwd3		;$FEFC -> $FEFE
fwd3	beq fwd			;$FEFE -> $FF02
back2	beq fwd2		;$FF00 -> $FF04
fwd	beq back2		;$FF02 -> $FF00
fwd2	jmp page2
branch  lda #$00
	beq back		;$FF09 -> $FEFC
	
	

page2	org $ff0c, $ea
	
	;; Test and compare cont'd
	lda #$a5
	sta $12
	eor #$ff
	bit $12			;$5A^$A5=$00: Z=1,N=1,V=0
	bne fail
	bpl fail
	bvs fail	
	lda #$00
	bne fail
	jsr subr

	;; JMP ($01FF): should read $01FF,$0100
	ldx #<next
	ldy #>next
	stx $01ff
	sty $0100
	jmp ($01ff)
	jmp fail

next	php
	lda #$00
	pha
	plp
	lda #$ff
	php
	pla
	plp

	;; Peripheral I/O Port
pio	lda #$00
	sta $00			;all inputs
	sta $01			;clear por

	ldx $01			;some pattern
	stx $12
	ldx #$55		;even ports outputs
	stx $00
	ldx #$ff
	stx $01			;por = $FF
	ldx $01			;odd = some pattern, even = 1s
	stx $12
	ldx $00
	stx $13

	asl $00			;odd ports outputs
	ldx $01			;even = some pattern, odds = 1s
	stx $12
	ldx $00
	stx $13

	sta $00			;all inputs
	dec $00			;all outputs
	dec $01			;output all 1s

	jmp end
	
end	org $ff80, $ea
	brk
	nop
	jmp reset
	
subr	lda #$55
	rts

nmivec	rti
irqvec	rti

	org $fffa
	dc.w nmivec
	dc.w reset
	dc.w irqvec

;;; Local Variables:
;;; mode: asm
;;; tab-width: 8
;;; End:
