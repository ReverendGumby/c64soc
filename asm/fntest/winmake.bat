cd %~p0
call ..\common\env.bat
del test.bin
%DASM% 6502_functional_test.asm -f3 -otest.bin -ltest.lst
if %errorlevel% neq 0 exit /b %errorlevel%
c:\python27\python.exe -u gen_hex_sv.py <test.bin >ram_hex.sv
if %errorlevel% neq 0 exit /b %errorlevel%
c:\python27\python.exe -u gen_coe.py <test.bin >test.coe
