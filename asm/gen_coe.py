import sys

sys.stdout.write("memory_initialization_radix=16;\n")
sys.stdout.write("memory_initialization_vector=\n")

pos = 0

#for i in range(65536 - 512):
#    sys.stdout.write("00,\n");
#    pos = i

while True:
    b = sys.stdin.read(1)
    if b == '':
        break
    if pos:
        sys.stdout.write(",\n");
    sys.stdout.write("{0:02X}".format(ord(b)))
    pos = pos + 1

sys.stdout.write(";\n");
