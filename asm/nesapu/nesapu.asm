;;; nesapu
;;; Hacking on the APU as it gets developed

        ;; APU registers
SND_REGISTER          = $4000
SND_SQUARE1_REG       = $4000
SND_SQUARE2_REG       = $4004
SND_TRIANGLE_REG      = $4008
SND_NOISE_REG         = $400c
SND_DELTA_REG         = $4010
SND_MASTERCTRL_REG    = $4015
SND_FRAME_CNT_REG     = $4017

    processor 6502

    seg.u bss
    org $10

    seg code
    org $8000

        ;; Initialize machine
entry
    sei
    clc
    cld
    ldx #$ff                ;reset stack pointer
    txs

    lda #$FF
    sta SND_FRAME_CNT_REG   ;reset frame counter

    lda #%00001111
    sta SND_MASTERCTRL_REG  ;enable all sound channels except dmc

    ;; Start square1
    lda #$82
    sta SND_SQUARE1_REG + 0
    lda #$A1
    sta SND_SQUARE1_REG + 1
    lda #$80
    sta SND_SQUARE1_REG + 2
    lda #$08
    sta SND_SQUARE1_REG + 3

lp  lda #$FF
    sta SND_FRAME_CNT_REG   ;reset frame counter, emit ticks
    jmp lp

exit	jmp exit

;;; End of program
        org $fffc, $ff          ; fill to end
        dc.w entry
        dc.w $fff0              ;unused
;;; EOF
