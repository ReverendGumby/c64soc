import sys

name = sys.argv[1]
width = sys.argv[2]
offset = int(sys.argv[3], 16)
count = int(sys.argv[4], 16)

data = []
while True:
    b = sys.stdin.read(1)
    if offset:
        offset = offset - 1
        continue
    if b == '':
        break
    data = data + [ord(b)]
    count = count - 1
    if count == 0:
        break

idx_width = int.bit_length(len(data) - 1)
idx_chars = (idx_width + 3) / 4

print ("reg [{0}:0] ").format(int(width)-1) + name + " [" + ("{1}'h{0:0" + str(idx_chars) + "x}").format(len(data) - 1, idx_width) + ":0];"
print "initial begin"

i = 0
for b in data:
    print ("  {0}[{3}'h{1:0" + str(idx_chars) + "x}] = " + width + "'h{2:0" + str(int(width)/4) + "x};").format(name, i, b, idx_width)
    i = i + 1

print "end"
