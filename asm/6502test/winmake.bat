cd %~p0
call ..\common\env.bat
del test.bin load.bin rom.bin
%DASM% 6502test.asm -f3 -otest.bin -ltest.lst
if %errorlevel% neq 0 exit /b %errorlevel%
%DASM% load_test.asm -f3 -oload.bin -lload.lst
if %errorlevel% neq 0 exit /b %errorlevel%
c:\python27\python.exe -u combine_load_test.py >rom.bin
if %errorlevel% neq 0 exit /b %errorlevel%
copy /b rom.hdr+rom.bin 6502test.crt
c:\python27\python.exe -u ..\common\bin_to_v.py roml 8 0000 2000 <rom.bin >roml.vh
c:\python27\python.exe -u ..\common\bin_to_v.py romh 8 2000 2000 <rom.bin >romh.vh
copy /b rom*.vh ..\..\xilinx\main\main.srcs\sources_1\new
