import sys

load = open('load.bin', 'rb')
load_org = 0x8000

test = open('test.bin', 'rb')
test_org = 0x0000
test_offset = 0x8000
test_start = 0x9000
test_end = 0xbfff

for pos in range(0x8000, 0xc000):
    load_b = ''
    test_b = ''
    if pos >= load_org and load:
        load_b = load.read(1)
    if pos - test_offset >= test_org and test:
        b = test.read(1)
        if pos >= test_start and pos < test_end:
            test_b = b

    out = chr(0xFF)
    if load_b:
        out = load_b
    elif test_b:
        out = test_b
    sys.stdout.write(out)
