; 6502_functional_test ROM loader
; Copies test image from ROM into RAM

catch_trap = 1
irq_cnt_val = 2
last_cnt_val = 10

code_seg = $8000
zp_seg = $02
data_seg = $200

test_src = $9000
test_dst = $1000
test_pages = $30
test_entry = test_dst

; Kernal vectors
init_vic = $ff81
init_sid = $ff84
init_ram = $ff87
init_io = $ff8a
nmi_ret = $febc
irq_vec = $0314
brk_vec = $0316

; VIC registers
vic_ec = $d020
vic_b0c = $d021

	processor 6502

	seg.u bss
	org zp_seg
test_cnt ds 2
sptr	ds 2
dptr	ds 2
irqv	ds 2
irq_cnt ds 1
retv    ds 2
    if catch_trap = 1
last_retv ds 2
last_cnt ds 1	
    endif

	seg code
	org code_seg

	;; Cartridge header
	dc.w res_entry
	dc.w nmi_entry
	dc.b $c3, $c2, $cd, $38, $30 ; 'CBM80'

	;; Initialize machine: RAM test, VIC, etc.
res_entry
	jsr init_sid
	jsr init_ram
	jsr init_io
	jsr init_vic
	lda #0
	sta test_cnt
	sta test_cnt+1
	lda #1
	sta irq_cnt
	lda #last_cnt_val
	sta last_cnt
	
	;; Print welcome banner
	ldy #0
msg_loop
	lda banner,y
	beq msg_done
	jsr putc
	jmp msg_loop
banner	dc.b "6502 FUNCTIONAL TEST", 0
msg_done

	;; Hook IRQ for our evil purposes
	sei			; disable interrupts
    if 1
	lda irq_vec
	sta irqv
	lda irq_vec+1
	sta irqv+1
	lda #(<irq_trap)
	sta $0314
	lda #(>irq_trap)
	sta $0315
    endif
	;; Redirect BRK
	lda #(<brk_trap)
	sta brk_vec
	lda #(>brk_trap)
	sta brk_vec+1

	;; Copy test code
	lda #(<test_src)
	sta sptr
	lda #(>test_src)
	sta sptr+1
	lda #(<test_dst)
	sta dptr
	lda #(>test_dst)
	sta dptr+1
	ldx #test_pages
	ldy #0
cloop	lda (sptr),y
	sta (dptr),y
	iny
	bne cloop
	inc sptr+1
	inc dptr+1
	dex
	bne cloop

	;; Run test
	cli			; enable interrupts
	jmp test_entry

nmi_entry
	cld
	;; Stack @ $0101+S: Y, X, A, P, PCL, PCH
	tsx
	lda $0106,x		; PCH
	sta retv+1
	lda $0105,x		; PCL
	sta retv
	;; Dump state now.
	jsr dump
	jmp nmi_ret

	;; Put character to screen
	;; A = character in ASCII
	;; Y = screen position (0 = upper-left)
	;; On return: A destroyed, Y incremented
putc
	cmp #$40		; Letter?
	bmi putc1
	sbc #$40
putc1	sta $0400,y
	iny
	rts

print_hex
	pha
	lsr
	lsr
	lsr
	lsr
	jsr print_nibble
	pla
	and #$0F
	jmp print_nibble

print_nibble
	clc
	adc #$30		;'0'
	cmp #$3a		;'0'+10
	bmi digit
	adc #$06		;'A'-'0'-C
digit	jmp putc

irq_trap
	cld
	;; Dump only now and then
	dec irq_cnt
	bne irq_done
	lda #irq_cnt_val
	sta irq_cnt
	;; Save original IRQ return address
	;; Stack @ $0101+X: Y, X, A, P, dummy PCL, dummy PCH
	ldy $0106,x		; PCH
	sty retv+1
	lda $0105,x		; PCL
	sta retv
    if catch_trap = 1
	;; Has the test trapped (stopped)?
	cmp last_retv
	bne notrap
	cpy last_retv+1
	bne notrap
	dec last_cnt
	bne notrap2
	;; Trapped! Flip border color forever
trap	inc vic_b0c
trap1	inc vic_ec
	jmp trap1
notrap  ldx #last_cnt_val
	stx last_cnt
notrap2	sta last_retv
	sty last_retv+1
    endif
	jsr dump
irq_done
	jmp (irqv)

brk_trap
	inc test_cnt
	bne bt1
	inc test_cnt+1
bt1	ldx #$ff
	txs
	cli
	jmp test_entry
	
dump
	ldy #40			; screen position
	lda retv+1
	jsr print_hex		; print PCH
	lda retv
	jsr print_hex		; print PCL

	lda #$3a		; ':'
	jsr putc

	ldx #$5c
dzp	lda $0,x
	jsr print_hex
	lda #$20		; ' '
	jsr putc
	inx
	cpx #$63
	bne dzp

	ldy #36
	lda test_cnt+1
	jsr print_hex
	lda test_cnt
	jsr print_hex
	
	rts
