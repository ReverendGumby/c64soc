;;; sidhack
;;; Hacking on the SID chip as it gets developed

	;; Kernal vectors
nmi_ret = $febc
init_vic = $ff81
init_sid = $ff84
init_ram = $ff87
init_io = $ff8a
open = $ffc0
chrout = $ffd2	
getin = $ffe4
plot = $fff0

irq_vec = $0314
brk_vec = $0316

	;; SID registers
sid_regs = $d400
; $00 - $18 are write-only, $19 - $1C are read-only
sid_last_wor = $18
sid_first_ror = $19

	;; Last register
lst_reg_invalid = $FF

	;; Register ranges per page
min_reg_pg1 = 0
max_reg_pg1 = 13
min_reg_pg2 = 14
max_reg_pg2 = 28

    ;; Screen layout
line_first_reg = 4

	processor 6502

	seg.u bss
	org $10
bits	ds 1
lst_reg ds 1
cur_reg ds 1
min_reg ds 1
max_reg ds 1
sid_shdw ds sid_last_wor+1		; shadow for write-only
str     ds 2
	
	seg code
	org $8000

	;; Cartridge header
	dc.w res_entry
	dc.w nmi_entry
	dc.b $c3, $c2, $cd, $38, $30 ; 'CBM80'

	;; Initialize machine: RAM test, VIC, etc.
res_entry
	jsr init_sid
	jsr init_ram
	jsr init_io
	jsr init_vic

	;; Initialize shadow register set
	ldx #sid_last_wor
	lda #$00
init_shdw
	sta sid_shdw,X
	dex
	bpl init_shdw

	;; Set volume to full
	lda #$0F
	sta sid_shdw+$18
	jsr load_sid

	ldx #min_reg_pg1
	ldy #max_reg_pg1
	jmp set_page

	;; Write shadow registers to SID
load_sid
	ldx #sid_last_wor
load_sid_next
	lda sid_shdw,X
	sta sid_regs,X
	dex
	bpl load_sid_next
	rts

	;; Select page (displayed register subset)
set_page
	stx cur_reg
	stx min_reg
	sty max_reg
    lda #lst_reg_invalid
    sta lst_reg

main_loop
    ;; Optimize redraws: If a last register selection is known, just
    ;; redraw that one and the currently selected register. Otherwise,
    ;; clear and redraw the entire screen.
    ldx lst_reg
    cpx #lst_reg_invalid
    beq redraw_screen
    jmp redraw_lst_reg

redraw_screen
	lda #147					; {clear}
	jsr chrout

	;; Dump all registers
    lda #<header
    sta str
    lda #>header
    sta str+1
    jsr printstr
	ldx min_reg
reg_next
	sec
	jsr print_reg
	inx
	cpx max_reg
	beq reg_next				; X == max_reg
	bmi reg_next				; X < max_reg
    jmp redraw_done

redraw_lst_reg
	ldx lst_reg
    jsr move_and_print_reg
    ldx cur_reg
    jsr move_and_print_reg
    jmp redraw_done

	;; Move cursor to and print one register
move_and_print_reg
    txa
    pha
    sec
    sbc min_reg
    clc
    adc #line_first_reg
	tax
    ldy #0
    jsr plot                    ; move cursor
    pla
    tax
    clc
	jmp print_reg

redraw_done
    ldx cur_reg                 ; remember last register drawn
    stx lst_reg

	;; Wait for input
wait_input
	jsr getin
	cmp #0
	beq wait_input
	cmp #17						; {down}
	beq key_down
	cmp #145					; {up}
	beq key_up
	cmp #'  					; {space}
	beq key_space
	cmp #133					; F1
	beq key_f1
	ldx #$80
	cmp #'A						; bit 7
	beq toggle_bit
	ldx #$40
	cmp #'S						; bit 6
	beq toggle_bit
	ldx #$20
	cmp #'D						; bit 5
	beq toggle_bit
	ldx #$10
	cmp #'F						; bit 4
	beq toggle_bit
	ldx #$08
	cmp #'G						; bit 3
	beq toggle_bit
	ldx #$04
	cmp #'H						; bit 2
	beq toggle_bit
	ldx #$02
	cmp #'J						; bit 1
	beq toggle_bit
	ldx #$01
	cmp #'K						; bit 0
	beq toggle_bit
    jmp keys2

	;; Cursor down: increment index
key_down
	ldx cur_reg
	cpx max_reg
	beq no_key_down
	inc cur_reg
no_key_down
	jmp go_main_loop

	;; Cursor up: decrement index
key_up
	ldx cur_reg
	cpx min_reg
	beq no_key_up
	dec cur_reg
no_key_up
	jmp go_main_loop

	;; Space: toggle page
key_space
	ldx min_reg
	cpx #min_reg_pg2
	beq pg2
	ldx #min_reg_pg2
	ldy #max_reg_pg2
	jmp set_page
pg2
	ldx #min_reg_pg1
	ldy #max_reg_pg1
	jmp set_page

	;; F1: Load preset #1
key_f1
	jsr init_sid
	lda #130
	sta sid_shdw+1
	lda #9
	sta sid_shdw+5
	lda #30
	sta sid_shdw+15
	lda #15
	sta sid_shdw+24
	lda #20
	sta sid_shdw+4
	jsr load_sid
	jmp main_loop

	;; Toggle bit in SID register
toggle_bit
	txa
	ldx cur_reg
    cpx #sid_first_ror          ; C = read-only
    bcs go_main_loop
	eor sid_shdw,X
	sta sid_shdw,X
	sta sid_regs,X
	jmp go_main_loop

go_main_loop
	jmp main_loop

keys2
	;; lines 0 - 14
    ldx #$00
    cmp #'0                     ; line 0
    beq goto_line
    inx
    cmp #'1                     ; line 1
    beq goto_line
    inx
    cmp #'2                     ; line 2
    beq goto_line
    inx
    cmp #'3                     ; line 3
    beq goto_line
    inx
    cmp #'4                     ; line 4
    beq goto_line
    inx
    cmp #'5                     ; line 5
    beq goto_line
    inx
    cmp #'6                     ; line 6
    beq goto_line
    inx
    cmp #'7                     ; line 7
    beq goto_line
    inx
    cmp #'8                     ; line 8
    beq goto_line
    inx
    cmp #'9                     ; line 9
    beq goto_line
    inx
    cmp #'Q                     ; line 10
    beq goto_line
    inx
    cmp #'W                     ; line 11
    beq goto_line
    inx
    cmp #'E                     ; line 12
    beq goto_line
    inx
    cmp #'R                     ; line 13
    beq goto_line
	inx
    cmp #'T                     ; line 14
    beq goto_line

	jmp wait_input              ; unknown key

	;; Go to line
goto_line
	clc
    txa
	adc min_reg
    cmp max_reg
    bcc goto_line_safe          ; reg < max_reg
    beq goto_line_safe          ; reg == max_reg
	jmp go_main_loop
goto_line_safe
    sta cur_reg
	jmp go_main_loop

printstr
	ldy #0
printstr_next
	lda (str),Y
	beq printstr_done
	jsr chrout
	iny
	jmp printstr_next
printstr_done
    rts
	
	;; Print register line
    ;; X = register number
    ;; C = 1: print register name (redraw optimization)
print_reg
	txa
    php
    sec
	sbc min_reg
    tay
	lda linekeys,Y
    jsr chrout
	lda #' 
	jsr chrout
	cpx cur_reg
	bne pr1
	lda #18                     ; {reverse}
	jsr chrout					; highlight current register
pr1	txa
	jsr print_byte				; register index
	lda #146                    ; {normal}
	jsr chrout
	lda #' 
	jsr chrout
	cpx #sid_first_ror          ; C = read-only
	bcs pr2
	lda sid_shdw,X
	jmp pr3
pr2	lda sid_regs,X
pr3 jsr print_bits				; register value in bits
	plp                         ; restore C from entry
    bcc pr4
	lda #'                      ; print register name
	jsr chrout
    txa
    asl
    tay
    lda regnames,Y
    sta str
    lda regnames+1,Y
    sta str+1
    jsr printstr
pr4
	lda #13
	jmp chrout
	
print_byte
	pha
	lsr
	lsr
	lsr
	lsr
	jsr print_hex
	pla
	jmp print_hex

print_hex
	and #$0F
	ora #$30
	cmp #$3A
	bmi ph1
	adc #($40-$3A)
ph1	jmp chrout

print_bits
	sta bits
	txa
	pha
	ldx #8
pb1	asl bits
	lda #'0
	adc #0
	jsr chrout
	lda #' 
	jsr chrout
	dex
	bne pb1
	pla
	tax
	rts

nmi_entry
	jmp nmi_ret

;;; Screen header
header
	dc.b 19						; {home}
	dc.b "CRSR UP/DN SELECTS, SPACE PAGES", 13
	dc.b "KEY  A S D F G H J K", 13
	dc.b "BIT  7 6 5 4 3 2 1 0", 13
	dc.b 13
	dc.b 0						; end of string

;;; Line keys
linekeys
    dc.b "0123456789QWERT"

;;; Register names
regnames
    dc.w regname_0
    dc.w regname_1
    dc.w regname_2
    dc.w regname_3
    dc.w regname_4
    dc.w regname_5
    dc.w regname_6
    dc.w regname_7
    dc.w regname_8
    dc.w regname_9
    dc.w regname_10
    dc.w regname_11
    dc.w regname_12
    dc.w regname_13
    dc.w regname_14
    dc.w regname_15
    dc.w regname_16
    dc.w regname_17
    dc.w regname_18
    dc.w regname_19
    dc.w regname_20
    dc.w regname_21
    dc.w regname_22
    dc.w regname_23
    dc.w regname_24
    dc.w regname_25
    dc.w regname_26
    dc.w regname_27
    dc.w regname_28
regname_0
    dc.b "F1[7:0]", 0
regname_1
    dc.b "F1[15:8]", 0
regname_2
    dc.b "PW1[7:0]", 0
regname_3
    dc.b "PW1[11:8]", 0
regname_4
    dc.b "CR1: NPSTTRSG", 0
regname_5
    dc.b "AD1: ATK,DCY", 0
regname_6
    dc.b "SR1: STN,RLS", 0
regname_7
    dc.b "F2[7:0]", 0
regname_8
    dc.b "F2[15:8]", 0
regname_9
    dc.b "PW2[7:0]", 0
regname_10
    dc.b "PW2[11:8]", 0
regname_11
    dc.b "CR2: NPSTTRSG", 0
regname_12
    dc.b "AD2: ATK,DCY", 0
regname_13
    dc.b "SR2: STN,RLS", 0
regname_14
    dc.b "F3[7:0]", 0
regname_15
    dc.b "F3[15:8]", 0
regname_16
    dc.b "PW3[7:0]", 0
regname_17
    dc.b "PW3[11:8]", 0
regname_18
    dc.b "CR3: NPSTTRSG", 0
regname_19
    dc.b "AD3: ATK,DCY", 0
regname_20
    dc.b "SR3: STN,RLS", 0
regname_21
    dc.b "FC[2:0]", 0
regname_22
    dc.b "FC[10:3]", 0
regname_23
    dc.b "FCR1:RES4,EX,F321", 0
regname_24
    dc.b "FCR2:C3,HBLP,VOL4", 0
regname_25
    dc.b "POTX", 0
regname_26
    dc.b "POTY", 0
regname_27
    dc.b "OSC3", 0
regname_28
    dc.b "ENV3", 0

;;; End of program
	org $9fff, $ff				; fill to end
	dc.b $ff
;;; EOF
