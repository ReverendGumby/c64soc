import sys

with open(sys.argv[1], 'rb') as fin:
    with open(sys.argv[2], 'w') as fout:
        while True:
            b = fin.read(1)
            if b == b'':
                break
            fout.write("{0:02X}\n".format(ord(b)))
