import sys

pos = 0
while True:
    b = sys.stdin.read(1)
    if b == '':
        break
    if pos:
        sys.stdout.write(", ");
        if pos % 16 == 0:
            sys.stdout.write("\n");
    sys.stdout.write("8'h{0:02X}".format(ord(b)))
    pos = pos + 1
