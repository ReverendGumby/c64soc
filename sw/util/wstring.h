#pragma once

#include <string>
#include <locale>

using codecvt_type = std::codecvt<wchar_t, char, std::mbstate_t>;

const codecvt_type& codecvt();

bool wstring_to_string(const std::wstring& in, std::string& out,
                       const codecvt_type& cvt = codecvt());
bool string_to_wstring(const std::string& in, std::wstring& out,
                       const codecvt_type& cvt = codecvt());
