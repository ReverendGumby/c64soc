#ifndef OSTREAM_FMT_SAVE_
#define OSTREAM_FMT_SAVE_

#include <ostream>
#include <iomanip>

class ostream_fmt_save
{
public:
    explicit ostream_fmt_save(std::ostream& os)
        : _os(os)
    {
        _flags = _os.flags();
        _precision = _os.precision();
        _fill = _os.fill();
    }
    ~ostream_fmt_save()
    {
        _os.flags(_flags);
        _os.precision(_precision);
        _os.fill(_fill);
    }
private:
    std::ostream& _os;
    std::ios::fmtflags _flags;
    std::streamsize _precision;
    char _fill;
};

#endif /* OSTREAM_FMT_SAVE_ */
