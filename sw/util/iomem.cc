#include "iomem.h"

void iowrite32_fill(volatile void* io, uint32_t v, size_t cnt)
{
    auto* dp = reinterpret_cast<volatile uint32_t*>(io);
    while (cnt--)
        *dp++ = v;
}

void iowrite32_copy(volatile void* to, const void* from, size_t cnt)
{
    auto* dp = reinterpret_cast<volatile uint32_t*>(to);
    auto* sp = reinterpret_cast<const uint32_t*>(from);
    while (cnt--)
        *dp++ = *sp++;
}

void ioread32_copy(void* to, const volatile void* from, size_t cnt)
{
    auto* dp = reinterpret_cast<uint32_t*>(to);
    auto* sp = reinterpret_cast<const volatile uint32_t*>(from);
    while (cnt--)
        *dp++ = *sp++;
}
