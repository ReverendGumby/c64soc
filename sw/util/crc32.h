#pragma once

#include <stddef.h>
#include <stdint.h>

// Compute CRC-32 like the BSD cksum utility.
//
//     The default CRC used is based on the polynomial used for CRC error checking in the networking standard
//     ISO 8802-3: 1989.  The CRC checksum encoding is defined by the generating polynomial:
//
//           G(x) = x^32 + x^26 + x^23 + x^22 + x^16 + x^12 +
//                x^11 + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x + 1

uint32_t calc_crc32(const uint8_t* buf, size_t len);
