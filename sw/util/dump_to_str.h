#ifndef DUMP_TO_STR__H
#define DUMP_TO_STR__H

#include <limits.h>
#include <vector>
#include <string>

std::vector<std::string> dump_to_str(const void *data, unsigned data_len,
                                     unsigned index);
inline std::vector<std::string> dump_mem_to_str(const void *data, unsigned data_len)
{
    auto lp = reinterpret_cast<unsigned long>(data);
	return dump_to_str(data, data_len, (unsigned)(lp & UINT_MAX));
}

inline std::vector<std::string> dump_buf_to_str(const void *data, unsigned data_len)
{
	return dump_to_str(data, data_len, 0);
}

#endif /* DUMP_TO_STR__H */
