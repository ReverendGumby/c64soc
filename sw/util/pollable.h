#ifndef POLLABLE__H
#define POLLABLE__H

#include "util/listener.h"

class pollable
{
public:
    virtual ~pollable();
    virtual void poll() = 0;
    virtual const char* get_poll_name() const = 0;
};

class poll_manager : private talker<pollable>
{
public:
    void poll();

    void add_pollable(pollable* l);
    void remove_pollable(pollable* l);
    bool is_added(pollable* l);
};

// Get the manager.
poll_manager& poll_manager_get();

// (Un)register a pollable object.
inline void pollable_add(pollable* p)
{
    poll_manager_get().add_pollable(p);
}

inline void pollable_remove(pollable* p)
{
    poll_manager_get().remove_pollable(p);
}

// Test whether an object was unregistered.
inline bool pollable_was_removed(pollable* p)
{
    return !poll_manager_get().is_added(p);
}

#endif /* POLLABLE__H */

