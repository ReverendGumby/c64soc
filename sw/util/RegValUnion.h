#pragma once

// Helper class to convert between a bitfield struct and its native type.
// Example:
//   struct thingy_t { uint32_t a : 16; uint32_t b : 16 };
//   RegValUnion<thingy_t, uint32_t> u { 0x12345678 };
//   thingy_t(u).b => 0x5678

template <typename RegStruct, typename Val>
union RegValUnion
{
    static_assert(sizeof(RegStruct) == sizeof(Val), "type size mismatch");

    RegStruct r;
    Val v;

    RegValUnion() = default;
    explicit RegValUnion(RegStruct r) : r(r) {}
    RegValUnion(Val v) : v(v) {}

    operator RegStruct() const { return r; }
    operator Val() const { return v; }
    operator RegStruct() const volatile { return const_cast<RegStruct&>(r); }
    operator Val() const volatile { return v; }

    // Copy from non-volatile union to volatile RegValUnion
    void operator=(const RegValUnion& x) volatile { v = x.v; }
    // Copy from non-volatile struct to volatile RegValUnion
    void operator=(const RegStruct& r) volatile { *this = RegValUnion(r); }
};

