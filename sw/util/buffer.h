/*
 * A generic buffer.
 */

#ifndef UTIL__BUFFER_H
#define UTIL__BUFFER_H

#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

struct buffer {
    void* ptr = nullptr;
    size_t len = 0;

    buffer() {}
    virtual ~buffer() {}

    // Wrap a preallocated buffer.
    // "const void*" covers both const and non-const pointers.
    buffer(const void* p, size_t len)
        : ptr(const_cast<void*>(p)), len(len) {}

    // Wrap a variable of any type.
    template <typename Type>
    explicit buffer(const Type& obj)
        : buffer(&obj, sizeof(obj)) {}

    uint8_t* get_ptr(size_t off = 0) const
    {
        return reinterpret_cast<uint8_t*>(ptr) + off;
    }

    void copy_from(const buffer& that)
    {
        assert(len == that.len);
        memcpy(ptr, that.ptr, len);
    }
};

#endif
