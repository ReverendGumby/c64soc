#include "wstring.h"

static std::locale* locale;

const codecvt_type& codecvt()
{
    if (!locale)
    {
        locale = new std::locale("");
    }        
    return std::use_facet<std::codecvt<wchar_t, char, std::mbstate_t> >(*locale);
}

bool wstring_to_string(const std::wstring& in, std::string& out,
                       const codecvt_type& cvt)
{
    out.resize(in.size() * cvt.max_length());
	auto mb = std::mbstate_t();
    const wchar_t* from_next;
    char* to_next;
    auto result = cvt.out(mb, &in[0], &in[in.size()], from_next,
                          &out[0], &out[out.size()], to_next);
    if (result != codecvt_type::ok) {
        printf("wstring_to_string: error %d\n", (int)result);
        return false;
    }
    out.resize(to_next - &out[0]);
    return true;
}

bool string_to_wstring(const std::string& in, std::wstring& out,
                       const codecvt_type& cvt)
{
    out.assign(in.size(), L'\0');
	auto mb = std::mbstate_t();
    const char* from_next;
    wchar_t* to_next;
    auto result = cvt.in(mb, &in[0], &in[in.size()], from_next,
                          &out[0], &out[out.size()], to_next);
    if (result != codecvt_type::ok)
        return false;
    out.resize(to_next - &out[0]);
    return true;
}
