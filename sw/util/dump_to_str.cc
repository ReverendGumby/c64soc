#include <string.h>
#include <stdio.h>
#include "dump_to_str.h"

std::vector<std::string> dump_to_str(const void *data, unsigned data_len,
                                     unsigned index)
{
    std::vector<std::string> lines;
    char line_buf[48+16+10+1];
    char* hex_buf = &line_buf[10];
    char* str_buf = hex_buf + 48;
    unsigned j, count;
    unsigned char *p = (unsigned char *)data;

    for (; data_len; p += 16, index += 16, data_len -= count) {
        count = 16;
        if (count > data_len)
            count = data_len;
        memset(str_buf, ' ', 16);
        sprintf(line_buf, "%08X: ", (unsigned)index);
        str_buf[16] = '\0';
        hex_buf[0] = '\0';
        for (j = 0; j < count; j++) {
            unsigned char b = p[j];
            sprintf(hex_buf + strlen(hex_buf), "%02X ", b);
            str_buf[j] = (b >= 0x20 && b <= 0x7f) ? b : '.';
        }
        for (; j < 16; j++) {
            strcpy(hex_buf + strlen(hex_buf), "   ");
        }
        str_buf[-1] = ' ';

        lines.push_back(std::string{line_buf});
    }
    return lines;
}
