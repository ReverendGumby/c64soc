#pragma once

class iovec;

class iop
{
public:
    iop(iovec& vec) : _vec(&vec) {}
    iop(void* buf) : _buf(buf) {}
    iop(const void* buf) : _buf(const_cast<void*>(buf)) {}

    bool is_vector() { return _vec; }

    void*& get_buf() { return _buf; }
    iovec& get_iovec() { return *_vec; }

private:
    iovec* _vec = nullptr;
    void* _buf = nullptr;
};
