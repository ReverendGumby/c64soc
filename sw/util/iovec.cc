#include "iovec.h"

struct subbuffer : buffer
{
    subbuffer(iovec::iob_t&& base, unsigned off, unsigned count)
        : _base(base)
    {
        ptr = _base->get_ptr(off);
        len = count;
    }

    virtual ~subbuffer()
    {
        // _base will be deleted shortly.
    }

    iovec::iob_t _base;
};

void iovec::push_back_sub(iob_t&& full_buffer, unsigned off, unsigned count)
{
    // Trivial optimization: use the entire buffer.
    if (off == 0 && count == full_buffer->len)
        return push_back(std::move(full_buffer));
    
    auto* sbb = new subbuffer(std::move(full_buffer), off, count);
    return push_back(iob_t{sbb});
}

buffer iovec::merge_consecutive(const buffer& first, iterator& it)
{
    buffer ret{first};

    for (; it != end(); it++) {
        auto& iop = *(it + 1);
        auto& iob = *iop;
        if (ret.get_ptr(ret.len) != iob.ptr)
            break;
        ret.len += iob.len;
    }

    return ret;
}
