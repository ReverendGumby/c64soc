#ifndef UTIL__TIMER__H
#define UTIL__TIMER__H

#include "os/cpu/cpu.h"

class timer
{
public:
    using ticks = decltype(os::cpu::cpu.timer)::count_t;

    static constexpr ticks sec = os::cpu::cpu.timer.hz();
    static constexpr ticks ms = sec / 1000;
    static constexpr ticks us = ms / 1000;
    static constexpr ticks min = sec * 60;
    static constexpr ticks hr = min * 60;

    static ticks now() { return os::cpu::cpu.timer.get_counter(); }

    void set_duration(ticks duration) { _dur = duration; }

    void start() { _end = now() + _dur; }
    void start(ticks d) { set_duration(d); start(); }
    void restart() { _end += _dur; }
    void restart(ticks d) { set_duration(d); restart(); }
    ticks remain() const
    {
        auto n = now();
        return (_end > n) ? _end - n : 0;
    }
    ticks elapsed() const { return _dur - remain(); }
    bool expired() const { return remain() == 0; }

    static void sleep(ticks duration)
    {
        timer t;
        t.start(duration);
        while (!t.expired())
            ;
    }

private:
    ticks _dur, _end;
};

#endif /* UTIL__TIMER__H */
