#ifndef DEBUG__H
#define DEBUG__H

#ifndef NDEBUG

#ifndef FSBL
#define DBG_CAN_BUFFER_PRINTF 1
#endif

#if DBG_CAN_BUFFER_PRINTF
void debug_printf(const char* fmt, ...)
    __attribute__ ((format (printf, 1, 2)));
#else
#include <stdio.h>
#define debug_printf printf
#endif

// #includer must define these:
//
// #define DBG_LVL 5
// #define DBG_TAG "SDHC"
//
// For DBG_PRINTF_T, class must define these:
//
// virtual const char* DBG_CLS() { return "class"; }

#define DBG_TEST_LEVEL(lvl) ((lvl) <= DBG_LVL)

#define DBG_DO(lvl, x) do { if (DBG_TEST_LEVEL(lvl)) x; } while (0)

#define DBG_PRINTF_FUNC(lvl, fn, fmt, ...)                              \
    do {                                                                \
        if (DBG_TEST_LEVEL(lvl))                                        \
            debug_printf("[%s][%s] " fmt, DBG_TAG, fn, ## __VA_ARGS__); \
    } while (0)

#define DBG_PRINTF(lvl, fmt, ...) \
    DBG_PRINTF_FUNC(lvl, __FUNCTION__, fmt, ## __VA_ARGS__)

#define DBG_PRINTF_CONT(lvl, fmt, ...)          \
    do {                                        \
        if (DBG_TEST_LEVEL(lvl))                \
            debug_printf(fmt, ## __VA_ARGS__);  \
    } while (0)

#define DBG_PRINTF_NOFUNC(lvl, fmt, ...)                        \
    do {                                                        \
        if (DBG_TEST_LEVEL(lvl))                                \
            debug_printf("[%s] " fmt, DBG_TAG, ## __VA_ARGS__); \
    } while (0)

#define DBG_PRINTF_T(lvl, fmt, ...)                                 \
    do {                                                            \
        if (DBG_TEST_LEVEL(lvl))                                    \
            debug_printf("[%s][%s::%s] " fmt, DBG_TAG, DBG_CLS(),   \
                         __FUNCTION__, ## __VA_ARGS__);             \
    } while (0)

#define DBG__PRINTF(fmt, ...) debug_printf(fmt, ## __VA_ARGS__)

#else // NDEBUG

#define DBG_TEST_LEVEL(lvl) 0
#define DBG_DO(lvl, x)
#define DBG_PRINTF_FUNC(lvl, fn, fmt, ...)
#define DBG_PRINTF(lvl, fmt, ...)
#define DBG_PRINTF_CONT(lvl, fmt, ...)
#define DBG_PRINTF_NOFUNC(lvl, fmt, ...)
#define DBG_PRINTF_T(lvl, fmt, ...)
#define DBG__PRINTF(fmt, ...)

#endif

#define DBG_HERE() DBG__PRINTF("{%s:%u}\n", __FILE__, __LINE__)

// Debug prints are buffered between critical sections.
void debug_critical_enter();
void debug_critical_exit();

#endif
