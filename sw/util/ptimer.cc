/* ptimer: A "pollable timer" */

#include "ptimer.h"

ptimer::ptimer()
{
}

ptimer::ptimer(timer::ticks d)
{
    duration = d;
}

ptimer::ptimer(timer::ticks d, action_fobj f)
{
    duration = d;
    action = f;
}

ptimer::~ptimer()
{
    remove();
}

void ptimer::start()
{
    t.start(duration);
    add();
}

void ptimer::restart()
{
    t.restart(duration);
    add();
}

void ptimer::add()
{
    if (!added) {
        pollable_add(this);
        added = true;
    }
}

void ptimer::remove()
{
    if (added) {
        pollable_remove(this);
        added = false;
    }
}

void ptimer::poll()
{
    if (!t.expired())
        return;

    if (repeat) {
        t.restart();
    } else {
        cancel();
    }

    action();
}

const char* ptimer::get_poll_name() const
{
    return "ptimer";
}
