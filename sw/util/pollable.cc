#include <assert.h>
#include "util/debug.h"
#include "pollable.h"

#define DBG_LVL 1
#define DBG_TAG "POLL"

static poll_manager the_mgr;

pollable::~pollable()
{
    assert(pollable_was_removed(this));
}

poll_manager& poll_manager_get()
{
    return the_mgr;
}

void poll_manager::add_pollable(pollable* l)
{
    DBG_PRINTF(2, "%p %s\n", l, l->get_poll_name());
    add_listener(l);
}

void poll_manager::remove_pollable(pollable* l)
{
    DBG_PRINTF(2, "%p %s\n", l, l->get_poll_name());
    remove_listener(l);
}

bool poll_manager::is_added(pollable* p)
{
    for (auto* l : get_listener_list())
        if (p == l)
            return true;
    return false;
}

void poll_manager::poll()
{
    listener_list_purge();

    for (auto* l : get_listener_list()) {
        DBG_PRINTF(3, "%p %s\n", l, l->get_poll_name());
        l->poll();
    }
}
