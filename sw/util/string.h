#ifndef UTIL_STRING_H__
#define UTIL_STRING_H__

#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#ifdef TEST
// Xcode 10.0 has vasprintf() but not vasnprintf()
#define HAVE_VASNPRINTF 0
#else
// newlib has vasnprintf() but not vasprintf()
#define HAVE_VASNPRINTF 1
#endif

inline std::string string_vprintf(const char* fmt, va_list ap)
{
    char* cstr = 0;
#if HAVE_VASNPRINTF
    size_t len;
#endif
#if HAVE_VASNPRINTF
    cstr = vasnprintf(cstr, &len, fmt, ap);
#else
    vasprintf(&cstr, fmt, ap);
#endif
    std::string str{cstr};
    free(cstr);
    return str;
}

inline std::string string_printf(const char* fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    std::string str{string_vprintf(fmt, ap)};
    va_end(ap);
    return str;
}

#endif /* UTIL_STRING_H__ */
