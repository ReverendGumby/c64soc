#ifndef UTIL__PTIMER__H
#define UTIL__PTIMER__H

/* ptimer: A "pollable timer"
 *
 * Create timers that, on expiration, get called back via the pollable
 * subsystem.
 */

#include <functional>
#include "timer.h"
#include "pollable.h"

class ptimer : private pollable
{
public:
    using action_fobj = std::function<void(void)>;

    ptimer();
    ptimer(timer::ticks duration);
    ptimer(timer::ticks duration, action_fobj);
    virtual ~ptimer();

    void set_duration(timer::ticks d) { duration = d; }

    void start();
    void start(timer::ticks d) { set_duration(d); start(); };
    void restart();
    void restart(timer::ticks d) { set_duration(d); restart(); };
    void cancel() { remove(); }

    bool is_added() const { return added; }

    timer::ticks duration;
    action_fobj action;
    bool repeat = false;

private:
    void add();
    void remove();

    // Interface for pollable
    virtual void poll();
    virtual const char* get_poll_name() const;

    timer t;
    bool added = false;
};

#endif
