#pragma once

#include <util/string.h>

#include <string>
#include <ostream>

template <typename T>
struct reg_val
{
    T val;
    // Default implementation assumes T is an integer type.
    std::string to_str() const
    {
        return string_printf("%*0X", sizeof(val) * 2, val);
    }
};

template <typename T>
reg_val<T> reg_val_of(const T val)
{
    return reg_val<T>{val};
}

template <typename T>
std::ostream& operator<<(std::ostream& os, struct reg_val<T> rv)
{
    os << rv.to_str();
    return os;
}

