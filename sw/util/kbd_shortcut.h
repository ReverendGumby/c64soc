#ifndef UTIL__KBD_SHORTCUT__H
#define UTIL__KBD_SHORTCUT__H

#include <functional>
#include "ui/kbd.h"

class kbd_shortcut : public ui::kbd::listener
{
public:
    using action_fobj = std::function<void(void)>;
    kbd_shortcut(ui::kbd::kbcode code, action_fobj action)
        : code(code), action(action) {}

private:
    virtual void event(const ui::kbd::keypress& kp)
    {
        if (kp.code == code)
            action();
    }
    virtual void event(const ui::kbd::report&) {}

    ui::kbd::kbcode code;
    action_fobj action;
};

#endif /* UTIL__KBD_SHORTCUT__H */
