// A dynamically-allocated buffer that has std::unique_ptr semantics.

#pragma once

#include "buffer.h"
#include <stddef.h>
#include <stdint.h>
#include <memory>

struct unique_buf : public buffer
{
    std::unique_ptr<uint8_t[]> mem;

    // Construct a unique_buf that owns no buffer.
    unique_buf();

    // Construct a 'len' byte buffer stored on the heap.
    unique_buf(size_t len);

    // Construct a copy of an existing buffer.
    explicit unique_buf(const buffer& src);

    unique_buf(unique_buf&& that);

    virtual ~unique_buf();

    unique_buf& operator=(unique_buf&& that);
};
