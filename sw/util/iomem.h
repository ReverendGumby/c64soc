#pragma once

#include <stddef.h>
#include <stdint.h>

void iowrite32_fill(volatile void* io, uint32_t v, size_t cnt);

// Use these instead of memcpy() when using our PL peripherals. memcpy() might
// do 2-beat 32-bit writes using VSTM.64, or reads using VLDM.64. The AXI
// interconnect won't split them into single beats as required by AXI4Lite, but
// will simply reject them.

void iowrite32_copy(volatile void* to, const void* from, size_t cnt);
void ioread32_copy(void* to, const volatile void* from, size_t cnt);
