#include "unique_buf.h"
#include <string.h>

unique_buf::unique_buf()
    : unique_buf(0)
{
}

unique_buf::unique_buf(size_t len)
    : buffer(nullptr, len)
{
    if (len) {
        mem = std::unique_ptr<uint8_t[]> {new uint8_t[len]};
        ptr = mem.get();
    }
}

unique_buf::unique_buf(const buffer& src)
    : unique_buf(src.len)
{
    if (len)
        memcpy(ptr, src.ptr, len);
}

unique_buf::unique_buf(unique_buf&& that)
{
    *this = std::move(that);
}

unique_buf::~unique_buf()
{
}

unique_buf& unique_buf::operator=(unique_buf&& that)
{
    mem = std::move(that.mem);
    len = that.len;
    ptr = that.ptr;
    return *this;
}
