/* All things error handling */

#ifndef UTIL__ERROR__H
#define UTIL__ERROR__H

#include <errno.h>                              // E*
#include <string.h>                             // strerror()

/* For anything that can return -E* for error: */

#define EC(exp)                                                     \
    do {                                                            \
        int ret;                                                    \
        if ((ret = exp) < 0) {                                      \
            printf(__FILE__ ":%d: %s\n", __LINE__, strerror(-ret)); \
            return ret;                                             \
        }                                                           \
    } while (0)

#endif
