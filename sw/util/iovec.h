#pragma once

#include <vector>
#include <memory>
#include "util/buffer.h"

class iovec : public std::vector<std::shared_ptr<buffer> >
{
public:
    using iob_t = std::shared_ptr<buffer>;
    using vec_t = std::vector<std::shared_ptr<buffer> >;

    void push_back_sub(iob_t&& full_buffer, unsigned off, unsigned count);

    buffer merge_consecutive(const buffer& first, iterator& it);
};
