#include "debug.h"
#include "string.h"
#include <stdio.h>
#include <vector>
#include <stdarg.h>

#if DBG_CAN_BUFFER_PRINTF

static int debug_critical_count = 0;
static std::vector<std::string> debug_critical_buf;

void debug_printf(const char* fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    if (debug_critical_count)
        debug_critical_buf.push_back(string_vprintf(fmt, ap));
    else
        vprintf(fmt, ap);
    va_end(ap);
}

#endif

void debug_critical_enter()
{
#if DBG_CAN_BUFFER_PRINTF
    debug_critical_count ++;
#endif
}

void debug_critical_exit()
{
#if DBG_CAN_BUFFER_PRINTF
    if (-- debug_critical_count == 0) {
        for (const auto& s : debug_critical_buf)
            printf("%s", s.c_str());
        debug_critical_buf.clear();
    }
#endif
}
