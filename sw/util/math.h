#pragma once

static constexpr unsigned ceil_log2(unsigned x)
{
    return (x > 1) ? (sizeof(x) * 8) - __builtin_clz(x - 1) : 0;
}
