#pragma once

#include "buffer.h"
#include "unique_buf.h"
#include <string.h>
#include <assert.h>

template <typename Memptr>
class membuf
{
public:
    membuf() = delete;
    explicit membuf(const membuf& that)
        : start(that.start), size(that.size)
    {
        alloc();
        memcpy(buf.ptr, that.buf.ptr, sizeof(uint8_t) * size);
    }

    explicit membuf(membuf&& that) noexcept
        : start(that.start), size(that.size)
    {
        buf = std::move(that.buf);
        that.buf = unique_buf();
    }

    explicit membuf(const Memptr& start, int size)
        : start(start), size(size)
    {
        alloc();
    }

    explicit membuf(const Memptr& start, const Memptr& end)
        : membuf(start, end.addr() - start.addr() + 1)
    {
    }

    explicit membuf(const Memptr& start, const Memptr& end,
                    const unique_buf& that_buf)
        : membuf(start, end)
    {
        assert(size == static_cast<int>(that_buf.len));
        memcpy(buf.ptr, that_buf.ptr, sizeof(uint8_t) * size);
    }

    virtual ~membuf()
    {
    }

    void fetch()
    {
        Memptr p = start;
        auto buf_p = buf.get_ptr();
        for (int i = 0; i < size; i++, ++p)
            buf_p[i] = *p;
    }

    void store()
    {
        Memptr p = start;
        auto buf_p = buf.get_ptr();
        for (int i = 0; i < size; i++, ++p)
            *p = buf_p[i];
    }

    // Remove the buffer and return it. After calling, this object is unusable.
    unique_buf take_buffer()
    {
        auto ret = std::move(buf);
        buf = unique_buf();
        return ret;
    }

private:
    void alloc()
    {
        buf = unique_buf(size);
    }

    Memptr start;
    int size;
    unique_buf buf;
};
