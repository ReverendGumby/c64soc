#ifndef LISTENER__H
#define LISTENER__H

#include <assert.h>
#include <list>
#include <iterator>
#include <algorithm>
#include <functional>
#include "debug.h"

template <typename Listener>
class listener_list
{
    struct node
    {
        bool active;
        Listener* l;
    };

    // Using std::list, so this list can be modified while it's being iterated.
    // However, beware: any iterators that point to the remove()'d element are
    // invalidated!
    std::list<node> v;
    // And here's how we handle that edge case above. remove() sets this when it
    // marks a listener as not active. purge() resets this after it does the
    // actual removal.
    bool purgeable = false;

    size_t count = 0;

public:
    void add(Listener* l)
    {
        if (std::find(begin(), end(), l) == end()) {
            struct node node;
            node.active = true;
            node.l = l;

        	v.push_back(node);
            count ++;
        }
    }

    void remove(Listener* l)
    {
    	for (auto& node : v) {
			if (node.l == l && node.active) {
				node.active = false;
				purgeable = true;
                count --;
				break;
			}
    	}
    }

    // This should ONLY be called when there are no iterators pointing to us.
    void purge()
    {
        if (!purgeable)
            return;
        v.erase(std::remove_if(v.begin(), v.end(),
                               [](const node& x){ return !x.active; }),
                v.end());
        purgeable = false;
    }

    void dump()
    {
        DBG__PRINTF("listener_list::dump:");
        for (auto* l : *this)
            DBG__PRINTF(" %p", l);
        DBG__PRINTF("\n");
    }

    bool intact()
    {
        size_t num = 0;
        for (auto* l __attribute__((unused)) : *this)
            num ++;
        return num == count;
    }

    // iterators
    class iterator;
    friend iterator;

    iterator begin();
    iterator end();
};

/***********************************************************************/

template <typename Listener>
class listener_list<Listener>::iterator
    : public std::iterator<std::input_iterator_tag, Listener*>
{
    using ll = listener_list<Listener>;
    friend ll;

    using a = std::list<listener_list<Listener>::node>;
    using lit = typename a::iterator;

    explicit iterator(lit i, lit e);
    void increment();

    lit _i;                                    // underlying list iterator
    lit _e;                                    // end of list

public:
    iterator() = default;
    iterator(const iterator&) = default;
    iterator(iterator&&) = default;
    iterator& operator=(const iterator&) = default;
    Listener*& operator*() { return _i->l; }
    Listener** operator->() const { return &_i->l; }

    iterator& operator++() { increment(); return *this; }

    friend bool operator!=(const iterator& lhs, const iterator& rhs)
    {
        return lhs._i != rhs._i;
    }

    friend bool operator==(const iterator& lhs, const iterator& rhs)
    {
        return lhs._i == rhs._i;
    }
};

template <typename Listener>
typename listener_list<Listener>::iterator listener_list<Listener>::begin()
{
    // Skip to the first active listener.
    auto i = v.begin();
    auto e = v.end();
    while (i != e && !i->active)
        i++;
    return iterator(i, e);
}

template <typename Listener>
typename listener_list<Listener>::iterator listener_list<Listener>::end()
{
    return iterator(v.end(), v.end());
}

template <typename Listener>
listener_list<Listener>::iterator::iterator(listener_list<Listener>::iterator::lit i,
                                  listener_list<Listener>::iterator::lit e)
    : _i(i), _e(e)
{
}

template <typename Listener>
void listener_list<Listener>::iterator::increment()
{
    // Advance to the next active listener.
    do
        _i ++;
    while (_i != _e && !_i->active);
}

/**********************************************************************
 * Talker: Template class for a source for a listener.
 */

template <class Listener>
class talker
{
private:
    listener_list<Listener> _ll;

protected:
    template<typename... Args>
    void speak(void (Listener::* fn)(const Args&...), const Args&... args)
    {
        for (auto* l : _ll) {
            (l->*fn)(args...);
        }
    }

    template<typename... Args>
    void speak(void (Listener::* fn)(Args...), Args... args)
    {
        for (auto* l : _ll) {
            (l->*fn)(args...);
        }
    }

    listener_list<Listener>& get_listener_list() { return _ll; }

    void listener_list_purge()
    {
        assert(_ll.intact());
        _ll.purge();
        assert(_ll.intact());
    }

public:
    void add_listener(Listener* l)
    {
        _ll.add(l);
        assert(_ll.intact());
    }
    void remove_listener(Listener* l)
    {
        _ll.remove(l);
        assert(_ll.intact());
    }
};

#endif /* LISTENER__H */

