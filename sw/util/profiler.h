#pragma once

#include <util/timer.h>
#include <array>
#include <stdio.h>

/*
Example usage:

profiler<3> pr;

for (;;) {
    pr.start();
    ... do thing 1...
    pr.split(0);
    ... do thing 2...
    pr.split(1);
    ... do thing 3...
    pr.split(2);
}

pr.dump();


*/

template <int NumSplits>
struct profiler
{
    using ticks = timer::ticks;

    int laps;
    std::array<ticks, NumSplits> tcum;
    ticks tstart;

    profiler()
    {
        reset();
    }

    void reset()
    {
        laps = 0;
        tcum.fill(0);
    }

    void start()
    {
        laps ++;
        tstart = timer::now();
    }

    void split(int s)
    {
        auto now = timer::now();
        tcum.at(s) += now - tstart;
        tstart = now;
    }

    void dump(const char* name = "profiler", ticks res = 1)
    {
        int w = 10;
        printf("<%s>    %*s %*s / %d laps\n", name, w, "cum", w, "avg", laps);
        for (int i = 0; i < NumSplits; i++) {
            auto cum = tcum[i] / res;
            auto avg = cum / laps;
            printf("<%s>[%d] %*llu %*llu\n", name, i, w, (long long unsigned)cum, w, (long long unsigned)avg);
        }
    }
};
