#include <string.h>
#include <ctime>
#include <stdexcept>
#include <os/init.h>
#include "manager.h"
#include "sysctl.h"
#include <util/string.h>
#include "audio.h"
#include <os/thread/scheduler.h>

namespace plat {

manager::manager()
{
}

manager::manager(const config_t& cfg)
{
    audio_set_config(cfg.audio);
}

void manager::early_init()
{
    os::io_use_serial(true);
    os::init();
    os::thread::scheduler.start();
}

void manager::late_init()
{
    sysctl_init();
    audio_init();
}

static void read_rtl_string(volatile uint32_t* rp, std::string& ret)
{
    // The RTL string is null-terminated, but in reverse byte order.
    const char* sp = (const char*)rp;
    int sl = strlen(sp);
    ret.resize(sl);
    for (int i = 0; i < sl; i++)
        ret[i] = sp[sl - 1 - i];
}

manager::version manager::get_rtl_version()
{
    version v;

    std::string s;
    read_rtl_string(&SYSCTL_BS_GIT_BRANCH, v.git_branch);
    read_rtl_string(&SYSCTL_BS_GIT_REV, v.git_rev);

    // See Xilinx doc. XAPP1232 for timestamp format.
    auto ts = SYSCTL_BS_TIMESTAMP;
    struct tm tstm = {
        .tm_sec = int((ts >> 0) & ((1<<6)-1)),
        .tm_min = int((ts >> 6) & ((1<<6)-1)),
        .tm_hour = int((ts >> 12) & ((1<<5)-1)),
        .tm_mday = int((ts >> 27) & ((1<<5)-1)),
        .tm_mon = int((ts >> 23) & ((1<<4)-1)) - 1, // 0 = Jan
        .tm_year = int((ts >> 17) & ((1<<6)-1)) + 2000 - 1900, // 0 = 1900
    };
    v.timestamp.resize(40);
    auto* ts_fmt = "%b %e %Y %H:%M:%S"; // like sw_build_timestamp
    strftime(&v.timestamp[0], v.timestamp.size(), ts_fmt, &tstm);
    v.timestamp.resize(strlen(v.timestamp.c_str()));

    return v;
}

void manager::reset_video(bool assert)
{
    if (assert)
        SYSCTL_VID |= SYSCTL_VID_RESET;
    else
        SYSCTL_VID &= ~SYSCTL_VID_RESET;
}

void manager::set_video_hdmi_mode(bool assert)
{
    if (assert)
        SYSCTL_VID |= SYSCTL_VID_HDMI_MODE;
    else
        SYSCTL_VID &= ~SYSCTL_VID_HDMI_MODE;
}

void manager::audio_up()
{
    SYSCTL_AUD |= SYSCTL_AUD_SDOUT_EN;
    SYSCTL_AUD &= ~SYSCTL_AUD_MUTE;
}

bool manager::get_sw(int i)
{
    return (((SYSCTL_HMI_IN & SYSCTL_HMI_IN_SW) >> SYSCTL_HMI_IN_SW_S)
            & (1 << i)) != 0;
}

void manager::set_led(int i, bool on)
{
    uint32_t v = (1 << (SYSCTL_HMI_OUT_LED_S + i)) & SYSCTL_HMI_OUT_LED;
    if (on)
        SYSCTL_HMI_OUT |= v;
    else
        SYSCTL_HMI_OUT &= ~v;
}

void manager::sysctl_init()
{
	uint32_t val;

	val = SYSCTL_ID;
	if (val != SYSCTL_ID_DATA) {
        auto why = string_printf("SYSCTL ID mismatch: got %08lx, wanted %08lx", val, SYSCTL_ID_DATA);
		throw std::runtime_error(why);
	}
}

} // namespace plat
