#ifndef AUDIO__H
#define AUDIO__H

namespace plat {

struct audio_config_t {
    unsigned sample_rate;
};

int audio_set_config(const audio_config_t&);
int audio_init();

} // namespace plat

#endif /* AUDIO__H */
