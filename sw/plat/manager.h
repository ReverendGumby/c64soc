#ifndef PLAT__MANAGER_H
#define PLAT__MANAGER_H

#include <string>

namespace plat {

class manager
{
public:
    struct version {
        std::string git_branch;
        std::string git_rev;
        std::string timestamp;
    };

    struct config_t {
        const struct audio_config_t& audio;
    };

    manager();
    manager(const config_t&);

    void early_init();
    void late_init();

    version get_rtl_version();

    void reset_video(bool assert);
    void set_video_hdmi_mode(bool assert);

    void audio_up();

    bool get_sw(int);
    void set_led(int i, bool on);

private:
    void sysctl_init();
};

} // namespace plat

#endif
