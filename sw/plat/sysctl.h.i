#ifndef PLAT__SYSCTL_H
#define PLAT__SYSCTL_H

#include <stdint.h>
#include "xparameters.h"

namespace plat {

//``PERIPHERAL SYSCTL

//``BASE 32

//``REGS s/_(\w+)_\1/_\1/ s/_(\w+)_(\w+)_\1/_\1_\2/

} // namespace plat

#endif

// Local Variables:
// compile-command: "cd ../.. && python3 tools/gen_regs_h.py sw/plat/sysctl.h.i sw/plat/sysctl.h rtl/sys/sysctl.v"
// End:
