#include <stdint.h>
#include <unistd.h>
#include "xparameters.h"
#include "os/soc/soc.h"
#include "os/soc/i2c.h"
#include "util/debug.h"
#include "util/timer.h"

#include "audio.h"

#define DBG_LVL 1
#define DBG_TAG "AUD"

using namespace os::soc;
i2c* ai2c;

namespace plat {

// CODEC: Analog Devices SSM2603

static int audio_init_i2c()
{
    ai2c = static_cast<i2c*>(soc.get_module(module::id_t::i2c0));

    i2c::config_t cfg {
        .scl_hz = 200000,
    };
    ai2c->set_config(cfg);

    return 0;
}

constexpr uint8_t slave_addr = 0x1A;

constexpr int NUM_REGS = 0x13;

// Registers are 9 bits wide! Bit 8 goes in the LSB of the first byte
// transmitted after the slave address; the 8-bit index is shifted up one
// to make room.

static uint16_t audio_read(uint8_t idx)
{
    uint8_t reg_addr = idx << 1;
    uint8_t buf[2];
    i2c::xfer_t xfer {
        .slave_addr = slave_addr,
        .msgs {
            i2c::msg_t::write(buffer{reg_addr}),
            i2c::msg_t::read(buffer{buf})
        }
    };

    uint16_t val;
    try {
        ai2c->transfer(xfer);
        val = (uint16_t(buf[1] & 1) << 8) | buf[0];
    } catch (i2c::error&) {
        val = 0x1CC;
    }

    return val;
}

static int audio_is_present()
{
    // Make sure slave responds.
    return ai2c->probe(slave_addr);
}

static void audio_write(uint8_t idx, uint16_t val)
{
    uint8_t reg_addr = idx << 1;
    uint8_t buf[2];

    buf[0] = reg_addr;
    buf[0] |= val >> 8;
    buf[1] = val & 0xff;

    i2c::xfer_t xfer {
        .slave_addr = slave_addr,
        .msgs {
            i2c::msg_t::write(buffer{buf})
        }
    };

    ai2c->transfer(xfer);
}

constexpr uint8_t SSM_AAP = 0x04;
constexpr uint16_t SSM_AAP_DACSEL = 1<<4;
constexpr uint16_t SSM_AAP_MUTEMIC = 1<<1;

constexpr uint8_t SSM_DAP = 0x05;
constexpr uint16_t SSM_DAP_DACMU = 1<<3;
constexpr uint16_t SSM_DAP_DEEMPH_32K = 1<<1;
constexpr uint16_t SSM_DAP_DEEMPH_48K = 3<<1;

constexpr uint8_t SSM_PM = 0x06;
constexpr uint8_t SSM_PM_CLEAR(uint16_t x) { return 0x007f & ~x; }
constexpr uint16_t SSM_PM_PWROFF = 1<<7;
constexpr uint16_t SSM_PM_OUT = 1<<4;
constexpr uint16_t SSM_PM_DAC = 1<<3;

constexpr uint8_t SSM_DAIF = 0x07;
constexpr uint16_t SSM_DAIF_WL_16 = 0<<2;
constexpr uint16_t SSM_DAIF_FMT_I2S = 2<<0;

constexpr uint8_t SSM_SR = 0x08;
constexpr uint16_t SSM_SR_BOSR = 1<<1;
constexpr uint16_t SSM_SR_SR = 0xf<<2;
constexpr uint16_t SSM_SR_48K = 0<<2;
constexpr uint16_t SSM_SR_32K = 6<<2;
constexpr uint16_t SSM_SR_96K = 7<<2;
constexpr uint16_t SSM_SR_44K1 = 8<<2;
constexpr uint16_t SSM_SR_CLKDIV2 = 1<<6;

constexpr uint8_t SSM_A = 0x09;
constexpr uint16_t SSM_A_ACTIVE = 1<<0;

constexpr uint8_t SSM_SWR = 0x0f;
constexpr uint16_t SSM_SWR_VAL = 0x00;

static bool configured = false;
static uint16_t sr, dap;

static void audio_dump_regs()
{
    if (DBG_TEST_LEVEL(5)) {
        for (uint8_t idx = 0x00; idx <= 0x12; idx++)
        {
            uint16_t val = audio_read(idx);
            DBG_PRINTF(5, "[%02X] %03X\n", idx, val);
        }
    }
}

int audio_set_config(const audio_config_t& cfg)
{
    switch (cfg.sample_rate) {
    case 32000:
        sr = SSM_SR_32K /* | SSM_SR_BOSR*/;
        dap = SSM_DAP_DEEMPH_32K;
        break;
    case 48000:
        sr = SSM_SR_48K;
        dap = SSM_DAP_DEEMPH_48K;
        break;
    default:
        DBG_PRINTF(0, "unsupported sample rate\n");
        return -1;
    }

    configured = true;
    return 0;
}

int audio_init()
{
    int ret;

    if (!configured)
        return 0;
    if ((ret = audio_init_i2c()) < 0)
        return ret;
    if (!audio_is_present())
    {
        DBG_PRINTF(0, "could not find SSM2603\n");
        return -1;
    }

    audio_write(SSM_SWR, SSM_SWR_VAL);
    audio_write(SSM_PM, SSM_PM_CLEAR(SSM_PM_PWROFF | SSM_PM_DAC));
    audio_write(SSM_AAP, SSM_AAP_DACSEL | SSM_AAP_MUTEMIC);
    audio_write(SSM_DAP, dap);
    audio_write(SSM_DAIF, SSM_DAIF_WL_16 | SSM_DAIF_FMT_I2S);
    audio_write(SSM_SR, sr | SSM_SR_CLKDIV2);
    timer::sleep(100 * timer::ms);
    audio_write(SSM_A, SSM_A_ACTIVE);
    audio_write(SSM_PM, SSM_PM_CLEAR(SSM_PM_PWROFF | SSM_PM_OUT | SSM_PM_DAC));

    audio_dump_regs();

    return 0;
}

} // namespace plat
