// Auto-generated: Do not edit.
// /usr/local/opt/python@3.9/bin/python3.9 tools/gen_regs_h.py sw/plat/sysctl.h.i sw/plat/sysctl.h rtl/sys/sysctl.v

#ifndef PLAT__SYSCTL_H
#define PLAT__SYSCTL_H

#include <stdint.h>
#include "xparameters.h"

namespace plat {

#define SYSCTL_BASE             XPAR_SYSCTL_BASEADDR

#define SYSCTL_REG(off)         (*((volatile uint32_t *)(SYSCTL_BASE + (off))))

#define SYSCTL_ID                   SYSCTL_REG(0x000)
#define SYSCTL_ID_DATA                  0x05450545UL

#define SYSCTL_BS_GIT_BRANCH        SYSCTL_REG(0x040)

#define SYSCTL_BS_GIT_REV           SYSCTL_REG(0x060)

#define SYSCTL_BS_TIMESTAMP         SYSCTL_REG(0x070)

#define SYSCTL_HMI_IN               SYSCTL_REG(0x080)
#define SYSCTL_HMI_IN_SW                (15<<0)
#define SYSCTL_HMI_IN_SW_S              0
#define SYSCTL_HMI_IN_BTN               (15<<8)
#define SYSCTL_HMI_IN_BTN_S             8

#define SYSCTL_HMI_OUT              SYSCTL_REG(0x084)
#define SYSCTL_HMI_OUT_LED              (15<<0)
#define SYSCTL_HMI_OUT_LED_S            0

#define SYSCTL_VID                  SYSCTL_REG(0x0C0)
#define SYSCTL_VID_RESET                (1<<0)
#define SYSCTL_VID_HDMI_MODE            (1<<1)

#define SYSCTL_AUD                  SYSCTL_REG(0x0C4)
#define SYSCTL_AUD_MUTE                 (1<<0)
#define SYSCTL_AUD_SDOUT_EN             (1<<1)

} // namespace plat

#endif

// Local Variables:
// compile-command: "cd ../.. && python3 tools/gen_regs_h.py sw/plat/sysctl.h.i sw/plat/sysctl.h rtl/sys/sysctl.v"
// End:
