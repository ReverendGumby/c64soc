#include "util/listener.h"

class speaker_listener
{
public:
    virtual void doit(int x) = 0;
};

class speaker : private talker<class speaker_listener>
{
public:
    using listener = speaker_listener;

    void reg(listener& l)
    {
        add_listener(&l);
    }

    void go(int x)
    {
        speak(&listener::doit, x);
    }
};

class mic : public speaker::listener
{
public:
    const char* name;
    mic(const char* name) : name(name) {}
    virtual void doit(int x) {
        printf("mic::do: %s %d\n", name, x);
    }
};

int main()
{
    speaker spk{};
    mic m1{"mic1"}, m2{"mic2"};

    spk.reg(m1);
    spk.reg(m2);
    spk.go(3);

    return 0;
}
