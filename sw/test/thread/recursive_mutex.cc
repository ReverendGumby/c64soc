#include <iostream>
#include <mutex>
#include <assert.h>

using namespace std;

struct recursive_mutex_data_t {
    recursive_mutex mtx;

    void task1() {
        lock_guard<recursive_mutex> lck{mtx};
        cout << "thread " << this_thread::get_id() << " in task1\n";
    }

    void task2() {
        lock_guard<recursive_mutex> lck{mtx};
        cout << "thread " << this_thread::get_id() << " in task2\n";
        task1();
        cout << "thread " << this_thread::get_id() << " back in task2\n";
    }
};

void test_recursive_mutex()
{
    cout << "\nTest: recursive mutex\n";

    using data_t = recursive_mutex_data_t;
    data_t data;
    thread t1{&data_t::task1, &data};
    thread t2{&data_t::task2, &data};

    t1.join();
    t2.join();
}
