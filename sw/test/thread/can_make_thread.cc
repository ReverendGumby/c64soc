#include <thread>
#include <iostream>
#include <assert.h>

static bool done = false;

static void thread_task()
{
    std::cout << "current thread ID: " << std::this_thread::get_id() << "\n";
    done = true;
}

void test_can_make_thread()
{
    std::cout << "\nTest: can make thread?\n";
    std::cout << "current thread ID: " << std::this_thread::get_id() << "\n";

    std::thread t {thread_task};
    t.join();

    std::cout << "done: " << done << "\n";
    assert(done == true);
}
