#include "common.h"

void watch::start()
{
    _running = true;
    _start = timer::now();
}

void watch::stop()
{
    _end = timer::now();
    _running = false;
}

int watch::elapsed_us()
{
    auto end = _running ? timer::now() : _end;
    return int((end - _start) / timer::us);
}

std::ostream& operator<<(std::ostream& os, watch w)
{
    os << '[';
    os.width(10);
    os << w.elapsed_us() << ']';
    return os;
}
