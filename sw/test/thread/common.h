#include <ostream>
#include <util/timer.h>

class watch {
public:
    void start();
    void stop();
    int elapsed_us();

private:
    timer::ticks _start, _end;
    bool _running = false;
};

std::ostream& operator<<(std::ostream& os, watch w);
