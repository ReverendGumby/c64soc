/*
 * thread/main.cc: Thread class test
*/

#include "can_make_thread.h"
#include "mutex.h"
#include "recursive_mutex.h"
#include "condition_variable.h"
#include "sleeping.h"

#include <iostream>
#include "plat/manager.h"

int main()
{
    plat::manager plm;
    plm.early_init();

    std::cout << "Thread test suite\n";

    test_can_make_thread();
    test_sleeping();
    test_mutex();
    test_recursive_mutex();
    test_condition_variable();

    std::cout << "\nAll tests done\n";

    return 0;
}
