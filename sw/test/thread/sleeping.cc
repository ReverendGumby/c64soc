#include "common.h"
#include <thread>
#include <iostream>
#include <assert.h>
#include <util/timer.h>
#include <os/thread/scheduler.h>

static constexpr int goal_us = 0.5e6;
static watch watch;

static void sleep_loop(int loops)
{
    int loop_us = goal_us / loops;

    for (int i = 0; i < loops; i++) {
        std::cout << watch << " thread " << std::this_thread::get_id()
                  << ", loop " << i << "\n";
        std::this_thread::sleep_for(std::chrono::microseconds(loop_us));
    }
    std::cout << watch << " thread " << std::this_thread::get_id() << " done\n";
}

static void thread_one_task()
{
    sleep_loop(7);
}

static void thread_two_task()
{
    sleep_loop(3);
}

void test_sleeping()
{
    std::cout << "\nTest: sleeping\n";

    auto st0 = os::thread::scheduler.get_stats();
    watch.start();

    std::thread t1 {thread_one_task};
    std::thread t2 {thread_two_task};
    t1.join();
    t2.join();

    watch.stop();
    auto st1 = os::thread::scheduler.get_stats();

    std::cout << watch << " test done\n";
    auto elapsed = watch.elapsed_us();
    assert(elapsed > goal_us * 0.9 && elapsed < goal_us * 1.1);

    auto context_switches = st1.context_switches - st0.context_switches;
    auto idle_loops = st1.idle_loops - st0.idle_loops;
    auto wfi_entries = st1.wfi_entries - st0.wfi_entries;
    std::cout << "Scheduler stats: \n"
              << "  Context switches: " << context_switches << "\n"
              << "  Idle loops: " << idle_loops << "\n"
              << "  WFI entries: " << wfi_entries << "\n";
}
