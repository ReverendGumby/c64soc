#include <array>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <iostream>
#include <assert.h>
#include <util/timer.h>

struct task_t {
    std::thread t;
    std::mutex mtx;
    bool done = false;

    task_t();
};

static std::condition_variable cv;

static void thread_task(task_t& task)
{
    std::unique_lock<std::mutex> lck{task.mtx};
    std::cout << "thread " << std::this_thread::get_id() << " waiting...\n";
    cv.wait(lck);
    std::cout << "thread " << std::this_thread::get_id() << " awoke\n";
    task.done = true;
}

task_t::task_t() :
    t{thread_task, std::ref(*this)}
{
}

template<size_t num_tasks>
static size_t count_done(std::array<task_t, num_tasks>& tasks)
{
    size_t num = 0;
    for (auto& task : tasks) {
        std::unique_lock<std::mutex> lck{task.mtx};
        num += task.done;
    }
    return num;
}

void test_condition_variable()
{
    std::cout << "\nTest: condition_variable\n";

    static constexpr size_t num_tasks = 3;
    std::array<task_t, num_tasks> tasks;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    assert(count_done(tasks) == 0);

    std::cout << "notify_one:\n";
    cv.notify_one();
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    assert(count_done(tasks) == 1);

    std::cout << "notify_all\n";
    cv.notify_all();
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    assert(count_done(tasks) == 3);

    for (auto& task : tasks)
        task.t.join();
}
