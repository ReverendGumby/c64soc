#include <iostream>
#include <mutex>
#include <assert.h>

using namespace std;

struct mutex_data_t {
    mutex mtx;
    int flags = 0;
    condition_variable changed;
};

struct mutex_task_t {
    thread t;
    int flag;
    mutex_data_t& data;

    mutex_task_t(mutex_data_t&, int flag);
};

static void thread_task(mutex_task_t& task)
{
    auto& data = task.data;
    cout << "thread " << this_thread::get_id() << " waiting...\n";
    {
        unique_lock<mutex> lck{data.mtx};
        cout << "thread " << this_thread::get_id() << " acquired lock\n";
        data.flags |= task.flag;
        data.changed.notify_one();
        cout << "thread " << this_thread::get_id() << " releasing lock\n";
    }
}

mutex_task_t::mutex_task_t(mutex_data_t& data, int flag) :
    flag(flag), data(data)
{
    t = thread{thread_task, ref(*this)};
}

void test_mutex()
{
    cout << "\nTest: mutex\n";

    mutex_data_t data {};
    unique_lock<mutex> lck{data.mtx};
    static constexpr size_t num_tasks = 3;
    array<mutex_task_t, num_tasks> tasks = {
        mutex_task_t{data, 1},
        mutex_task_t{data, 2},
        mutex_task_t{data, 4},
    };

    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    assert(data.flags == 0);

    for (;;) {
        cout << "data.flags: " << data.flags << "\n";
        if (data.flags == 7)
            break;
        data.changed.wait(lck);
    }

    for (auto& task : tasks)
        task.t.join();
}
