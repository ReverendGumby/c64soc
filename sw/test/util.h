#ifndef TEST__UTIL_H
#define TEST__UTIL_H

#include <iostream>

extern void dump(std::ostream& os, uint8_t* buf, unsigned count, unsigned off=0);

#endif
