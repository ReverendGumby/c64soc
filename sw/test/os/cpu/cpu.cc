#include <sys/time.h>
#include "util/timer.h"
#include "cpu.h"

namespace os {
namespace cpu {

struct cpu cpu;

cpu::timer_t::count_t cpu::timer_t::get_counter()
{
    static time_t sec_start;
    struct timeval tv;
    gettimeofday(&tv, NULL);
    if (sec_start == 0)
        sec_start = tv.tv_sec;
    return ((count_t)(tv.tv_sec - sec_start) * 1e6 + (tv.tv_usec)) * timer::us;
}

}
}
