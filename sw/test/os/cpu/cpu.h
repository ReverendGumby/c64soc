#pragma once

namespace os {
namespace cpu {

struct cpu
{
    struct timer_t
    {
        using count_t = unsigned long long;
        constexpr count_t hz() const { return 1; }
        count_t get_counter();
    } timer;

    struct
    {
        using addr_t = uintptr_t;
        static addr_t to_addr_t(void* p)
        {
            return reinterpret_cast<addr_t>(p);
        }
        void invalidate(addr_t, size_t) {}
        void invalidate(void* p, size_t s) { return invalidate(to_addr_t(p), s); }
        void clean(addr_t, size_t) {}
        void clean(void* p, size_t s) { return clean(to_addr_t(p), s); }
    } cache;
};

extern struct cpu cpu;

}
}
