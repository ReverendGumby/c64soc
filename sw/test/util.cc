#include <iostream>
#include <iomanip>
#include <string.h>

#include "util/ostream_fmt_save.h"

using namespace std;

void dump(ostream& os, uint8_t* buf, unsigned count, unsigned off=0)
{
    ostream_fmt_save ofs{os};
    for (unsigned idx = 0; idx < count; idx += 16)
    {
        os << hex << noshowbase << setfill('0')
           << setw(4) << (idx + off) << " : ";
        string pp(16, ' ');
        for (unsigned i2 = 0; i2 < 16; i2++)
        {
            if (idx + i2 < count)
            {
                uint8_t c = buf[idx + i2];
                os << setw(2) << (unsigned)c << ' ';
                pp[i2] = (c >= 0x20 && c <= 0x7e) ? c : '.';
            }
            else
                os << "   ";
        }
        os << " " << pp << '\n';
    }
}
