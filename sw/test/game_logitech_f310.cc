// Wireshark capture from Logitech Gamepad F310 game controller

namespace game_logitech_f310 {

/* Frame (40 bytes) */
static const unsigned char pkt1[40] = {
0x00, 0x01, 0x20, 0x00, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xa8, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x00, 0x01, 0x00, 0x00, 0x08, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt2[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xa8, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x12, 0x01, 0x00, 0x02, 0x00, 0x00, 0x00, 0x08  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt3[40] = {
0x00, 0x01, 0x20, 0x00, 0x12, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xaa, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x00, 0x01, 0x00, 0x00, 0x12, 0x00  /* ........ */
};

/* Frame (50 bytes) */
static const unsigned char pkt4[50] = {
0x00, 0x01, 0x20, 0x01, 0x12, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xaa, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x12, 0x01, 0x00, 0x02, 0x00, 0x00, 0x00, 0x08, /* ........ */
0x6d, 0x04, 0x16, 0xc2, 0x14, 0x04, 0x01, 0x09, /* m....... */
0x03, 0x01                                      /* .. */
};

/* Frame (40 bytes) */
static const unsigned char pkt5[40] = {
0x00, 0x01, 0x20, 0x00, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xac, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x09, 0x03, 0x09, 0x04, 0x02, 0x00  /* ........ */
};

/* Frame (34 bytes) */
static const unsigned char pkt6[34] = {
0x00, 0x01, 0x20, 0x01, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xac, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x2a, 0x03                                      /* *. */
};

/* Frame (40 bytes) */
static const unsigned char pkt7[40] = {
0x00, 0x01, 0x20, 0x00, 0x2a, 0x00, 0x00, 0x00, /* .. .*... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xae, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x09, 0x03, 0x09, 0x04, 0x2a, 0x00  /* ......*. */
};

/* Frame (74 bytes) */
static const unsigned char pkt8[74] = {
0x00, 0x01, 0x20, 0x01, 0x2a, 0x00, 0x00, 0x00, /* .. .*... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xae, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x2a, 0x03, 0x4c, 0x00, 0x6f, 0x00, 0x67, 0x00, /* *.L.o.g. */
0x69, 0x00, 0x74, 0x00, 0x65, 0x00, 0x63, 0x00, /* i.t.e.c. */
0x68, 0x00, 0x20, 0x00, 0x44, 0x00, 0x75, 0x00, /* h. .D.u. */
0x61, 0x00, 0x6c, 0x00, 0x20, 0x00, 0x41, 0x00, /* a.l. .A. */
0x63, 0x00, 0x74, 0x00, 0x69, 0x00, 0x6f, 0x00, /* c.t.i.o. */
0x6e, 0x00                                      /* n. */
};

/* Frame (40 bytes) */
static const unsigned char pkt9[40] = {
0x00, 0x01, 0x20, 0x00, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xb0, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x01, 0x03, 0x09, 0x04, 0x02, 0x00  /* ........ */
};

/* Frame (34 bytes) */
static const unsigned char pkt10[34] = {
0x00, 0x01, 0x20, 0x01, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xb0, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x12, 0x03                                      /* .. */
};

/* Frame (40 bytes) */
static const unsigned char pkt11[40] = {
0x00, 0x01, 0x20, 0x00, 0x12, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xb2, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x01, 0x03, 0x09, 0x04, 0x12, 0x00  /* ........ */
};

/* Frame (50 bytes) */
static const unsigned char pkt12[50] = {
0x00, 0x01, 0x20, 0x01, 0x12, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xb2, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x12, 0x03, 0x4c, 0x00, 0x6f, 0x00, 0x67, 0x00, /* ..L.o.g. */
0x69, 0x00, 0x74, 0x00, 0x65, 0x00, 0x63, 0x00, /* i.t.e.c. */
0x68, 0x00                                      /* h. */
};

/* Frame (40 bytes) */
static const unsigned char pkt13[40] = {
0x00, 0x01, 0x20, 0x00, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xb4, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x03, 0x03, 0x09, 0x04, 0x02, 0x00  /* ........ */
};

/* Frame (34 bytes) */
static const unsigned char pkt14[34] = {
0x00, 0x01, 0x20, 0x01, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xb4, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x12, 0x03                                      /* .. */
};

/* Frame (40 bytes) */
static const unsigned char pkt15[40] = {
0x00, 0x01, 0x20, 0x00, 0x12, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xb6, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x03, 0x03, 0x09, 0x04, 0x12, 0x00  /* ........ */
};

/* Frame (50 bytes) */
static const unsigned char pkt16[50] = {
0x00, 0x01, 0x20, 0x01, 0x12, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xb6, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x12, 0x03, 0x38, 0x00, 0x37, 0x00, 0x39, 0x00, /* ..8.7.9. */
0x30, 0x00, 0x42, 0x00, 0x45, 0x00, 0x42, 0x00, /* 0.B.E.B. */
0x39, 0x00                                      /* 9. */
};

/* Frame (40 bytes) */
static const unsigned char pkt17[40] = {
0x00, 0x01, 0x20, 0x00, 0x09, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xb8, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x00, 0x02, 0x00, 0x00, 0x09, 0x00  /* ........ */
};

/* Frame (41 bytes) */
static const unsigned char pkt18[41] = {
0x00, 0x01, 0x20, 0x01, 0x09, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xb8, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x09, 0x02, 0x29, 0x00, 0x01, 0x01, 0x00, 0x80, /* ..)..... */
0x32                                            /* 2 */
};

/* Frame (40 bytes) */
static const unsigned char pkt19[40] = {
0x00, 0x01, 0x20, 0x00, 0x29, 0x00, 0x00, 0x00, /* .. .)... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xba, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x00, 0x02, 0x00, 0x00, 0x29, 0x00  /* ......). */
};

/* Frame (73 bytes) */
static const unsigned char pkt20[73] = {
0x00, 0x01, 0x20, 0x01, 0x29, 0x00, 0x00, 0x00, /* .. .)... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xba, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x09, 0x02, 0x29, 0x00, 0x01, 0x01, 0x00, 0x80, /* ..)..... */
0x32, 0x09, 0x04, 0x00, 0x00, 0x02, 0x03, 0x00, /* 2....... */
0x00, 0x00, 0x09, 0x21, 0x11, 0x01, 0x00, 0x01, /* ...!.... */
0x22, 0x63, 0x00, 0x07, 0x05, 0x81, 0x03, 0x20, /* "c.....  */
0x00, 0x04, 0x07, 0x05, 0x02, 0x03, 0x20, 0x00, /* ...... . */
0x08                                            /* . */
};

/* Frame (40 bytes) */
static const unsigned char pkt21[40] = {
0x00, 0x01, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xbc, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x00, 0x00, /* ........ */
0x00, 0x09, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt22[32] = {
0x00, 0x01, 0x20, 0x01, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xbc, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x00, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt23[40] = {
0x00, 0x01, 0x20, 0x00, 0x63, 0x00, 0x00, 0x00, /* .. .c... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xbe, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x81, 0x06, 0x00, 0x22, 0x00, 0x00, 0x63, 0x00  /* ..."..c. */
};

/* Frame (131 bytes) */
static const unsigned char pkt24[131] = {
0x00, 0x01, 0x20, 0x01, 0x63, 0x00, 0x00, 0x00, /* .. .c... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xbe, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x80, 0x00, /* ........ */
0x05, 0x01, 0x09, 0x04, 0xa1, 0x01, 0xa1, 0x02, /* ........ */
0x75, 0x08, 0x95, 0x04, 0x15, 0x00, 0x26, 0xff, /* u.....&. */
0x00, 0x35, 0x00, 0x46, 0xff, 0x00, 0x09, 0x30, /* .5.F...0 */
0x09, 0x31, 0x09, 0x32, 0x09, 0x35, 0x81, 0x02, /* .1.2.5.. */
0x75, 0x04, 0x95, 0x01, 0x25, 0x07, 0x46, 0x3b, /* u...%.F; */
0x01, 0x65, 0x14, 0x09, 0x39, 0x81, 0x42, 0x65, /* .e..9.Be */
0x00, 0x75, 0x01, 0x95, 0x0c, 0x25, 0x01, 0x45, /* .u...%.E */
0x01, 0x05, 0x09, 0x19, 0x01, 0x29, 0x0c, 0x81, /* .....).. */
0x02, 0x06, 0x00, 0xff, 0x75, 0x01, 0x95, 0x10, /* ....u... */
0x25, 0x01, 0x45, 0x01, 0x09, 0x01, 0x81, 0x02, /* %.E..... */
0xc0, 0xa1, 0x02, 0x75, 0x08, 0x95, 0x07, 0x46, /* ...u...F */
0xff, 0x00, 0x26, 0xff, 0x00, 0x09, 0x02, 0x91, /* ..&..... */
0x02, 0xc0, 0xc0                                /* ... */
};

/* Frame (32 bytes) */
static const unsigned char pkt25[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xc0, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt26[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xc0, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x6e, 0x93, 0x74, 0x8f, 0x08, 0x00, 0x00, 0xff  /* n.t..... */
};

/* Frame (32 bytes) */
static const unsigned char pkt27[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xc2, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt28[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xc2, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x6e, 0x90, 0x76, 0x8f, 0x08, 0x00, 0x00, 0xff  /* n.v..... */
};

/* Frame (32 bytes) */
static const unsigned char pkt29[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xc4, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt30[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xc4, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x71, 0x90, 0x76, 0x8d, 0x08, 0x00, 0x00, 0xff  /* q.v..... */
};

/* Frame (32 bytes) */
static const unsigned char pkt31[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xc6, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt32[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xc6, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x71, 0x8d, 0x77, 0x8d, 0x08, 0x00, 0x00, 0xff  /* q.w..... */
};

/* Frame (32 bytes) */
static const unsigned char pkt33[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xc8, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt34[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xc8, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x74, 0x8d, 0x77, 0x8a, 0x08, 0x00, 0x00, 0xff  /* t.w..... */
};

/* Frame (32 bytes) */
static const unsigned char pkt35[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xca, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt36[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xca, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x74, 0x8b, 0x79, 0x8a, 0x08, 0x00, 0x00, 0xff  /* t.y..... */
};

/* Frame (32 bytes) */
static const unsigned char pkt37[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xcc, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt38[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xcc, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x77, 0x8b, 0x79, 0x88, 0x08, 0x00, 0x00, 0xff  /* w.y..... */
};

/* Frame (32 bytes) */
static const unsigned char pkt39[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xce, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt40[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xce, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x77, 0x88, 0x7b, 0x88, 0x08, 0x00, 0x00, 0xff  /* w.{..... */
};

/* Frame (32 bytes) */
static const unsigned char pkt41[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd0, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt42[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd0, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x7a, 0x88, 0x7b, 0x86, 0x08, 0x00, 0x00, 0xff  /* z.{..... */
};

/* Frame (32 bytes) */
static const unsigned char pkt43[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd2, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt44[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd2, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x7a, 0x85, 0x7c, 0x84, 0x08, 0x00, 0x00, 0xff  /* z.|..... */
};

/* Frame (32 bytes) */
static const unsigned char pkt45[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd4, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt46[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd4, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x7d, 0x85, 0x7c, 0x84, 0x08, 0x00, 0x00, 0xff  /* }.|..... */
};

/* Frame (32 bytes) */
static const unsigned char pkt47[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd6, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt48[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd6, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x7d, 0x82, 0x7e, 0x82, 0x08, 0x00, 0x00, 0xff  /* }.~..... */
};

/* Frame (32 bytes) */
static const unsigned char pkt49[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd8, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt50[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd8, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x82, 0x7e, 0x82, 0x08, 0x00, 0x00, 0xff  /* ..~..... */
};

/* Frame (32 bytes) */
static const unsigned char pkt51[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xda, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt52[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xda, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt53[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xdc, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt54[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xdc, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x00, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt55[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xde, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt56[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xde, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt57[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xe0, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt58[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xe0, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x04, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt59[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xe2, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt60[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xe2, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt61[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xe4, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt62[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xe4, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x06, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt63[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xe6, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt64[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xe6, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt65[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xe8, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt66[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xe8, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x02, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt67[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xea, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt68[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xea, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt69[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xec, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt70[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xec, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x28, 0x00, 0x00, 0xff  /* ....(... */
};

/* Frame (32 bytes) */
static const unsigned char pkt71[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xee, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt72[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xee, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt73[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xf0, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt74[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xf0, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x48, 0x00, 0x00, 0xff  /* ....H... */
};

/* Frame (32 bytes) */
static const unsigned char pkt75[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xf2, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt76[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xf2, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt77[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xf4, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt78[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xf4, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x18, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt79[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xf6, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt80[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xf6, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt81[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xf8, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt82[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xf8, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x88, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt83[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xfa, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt84[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xfa, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt85[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xfc, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt86[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xfc, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x08, 0x10, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt87[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xfe, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt88[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xfe, 0x0b, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt89[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x00, 0x0c, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt90[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x00, 0x0c, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x08, 0x20, 0x00, 0xff  /* ..... .. */
};

/* Frame (32 bytes) */
static const unsigned char pkt91[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x02, 0x0c, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt92[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x02, 0x0c, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x80, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x00, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt93[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x1e, 0x0c, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt94[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x1e, 0x0c, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x7f, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x08, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt95[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd2, 0x0c, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt96[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd2, 0x0c, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x7f, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x08, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt97[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd4, 0x0c, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt98[40] = {
0x00, 0x01, 0x20, 0x01, 0x08, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd4, 0x0c, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03, /* ........ */
0x7f, 0x7f, 0x80, 0x7f, 0x08, 0x00, 0x08, 0xff  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt99[32] = {
0x00, 0x01, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, /* .. . ... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0xd6, 0x0c, 0x56, 0x03, 0x00, 0x00, 0x00, 0x00, /* ..V..... */
0x00, 0x00, 0x10, 0x14, 0x01, 0x0d, 0x81, 0x03  /* ........ */
};

static test_urbs_t test_urbs[] = {
    PKT(pkt1),
    PKT(pkt2),
    PKT(pkt3),
    PKT(pkt4),
    PKT(pkt5),
    PKT(pkt6),
    PKT(pkt7),
    PKT(pkt8),
    PKT(pkt9),
    PKT(pkt10),
    PKT(pkt11),
    PKT(pkt12),
    PKT(pkt13),
    PKT(pkt14),
    PKT(pkt15),
    PKT(pkt16),
    PKT(pkt17),
    PKT(pkt18),
    PKT(pkt19),
    PKT(pkt20),
    PKT(pkt21),
    PKT(pkt22),
    PKT(pkt23),
    PKT(pkt24),
    PKT(pkt25),
    PKT(pkt26),
    PKT(pkt27),
    PKT(pkt28),
    PKT(pkt29),
    PKT(pkt30),
    PKT(pkt31),
    PKT(pkt32),
    PKT(pkt33),
    PKT(pkt34),
    PKT(pkt35),
    PKT(pkt36),
    PKT(pkt37),
    PKT(pkt38),
    PKT(pkt39),
    PKT(pkt40),
    PKT(pkt41),
    PKT(pkt42),
    PKT(pkt43),
    PKT(pkt44),
    PKT(pkt45),
    PKT(pkt46),
    PKT(pkt47),
    PKT(pkt48),
    PKT(pkt49),
    PKT(pkt50),
    PKT(pkt51),
    PKT(pkt52),
    PKT(pkt53),
    PKT(pkt54),
    PKT(pkt55),
    PKT(pkt56),
    PKT(pkt57),
    PKT(pkt58),
    PKT(pkt59),
    PKT(pkt60),
    PKT(pkt61),
    PKT(pkt62),
    PKT(pkt63),
    PKT(pkt64),
    PKT(pkt65),
    PKT(pkt66),
    PKT(pkt67),
    PKT(pkt68),
    PKT(pkt69),
    PKT(pkt70),
    PKT(pkt71),
    PKT(pkt72),
    PKT(pkt73),
    PKT(pkt74),
    PKT(pkt75),
    PKT(pkt76),
    PKT(pkt77),
    PKT(pkt78),
    PKT(pkt79),
    PKT(pkt80),
    PKT(pkt81),
    PKT(pkt82),
    PKT(pkt83),
    PKT(pkt84),
    PKT(pkt85),
    PKT(pkt86),
    PKT(pkt87),
    PKT(pkt88),
    PKT(pkt89),
    PKT(pkt90),
    PKT(pkt91),
    PKT(pkt92),
    PKT(pkt93),
    PKT(pkt94),
    PKT(pkt95),
    PKT(pkt96),
    PKT(pkt97),
    PKT(pkt98),
    PKT(pkt99),
};

} // namespace game_logitech_f310
