// Wireshark capture from Sabrent USB 4-port hub

namespace hub_sabrent {

/* Frame (40 bytes) */
static const unsigned char pkt1[40] = {
0x00, 0x01, 0x20, 0x00, 0x12, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x2a, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* *....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x00, 0x01, 0x00, 0x00, 0x12, 0x00  /* ........ */
};

/* Frame (50 bytes) */
static const unsigned char pkt2[50] = {
0x00, 0x01, 0x20, 0x01, 0x12, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x2a, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* *....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x12, 0x01, 0x00, 0x02, 0x09, 0x00, 0x01, 0x40, /* .......@ */
0x40, 0x1a, 0x01, 0x01, 0x11, 0x01, 0x00, 0x01, /* @....... */
0x00, 0x01                                      /* .. */
};

/* Frame (40 bytes) */
static const unsigned char pkt3[40] = {
0x00, 0x01, 0x20, 0x00, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x2c, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* ,....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x01, 0x03, 0x09, 0x04, 0x02, 0x00  /* ........ */
};

/* Frame (34 bytes) */
static const unsigned char pkt4[34] = {
0x00, 0x01, 0x20, 0x01, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x2c, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* ,....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x18, 0x03                                      /* .. */
};

/* Frame (40 bytes) */
static const unsigned char pkt5[40] = {
0x00, 0x01, 0x20, 0x00, 0x18, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x2e, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x01, 0x03, 0x09, 0x04, 0x18, 0x00  /* ........ */
};

/* Frame (56 bytes) */
static const unsigned char pkt6[56] = {
0x00, 0x01, 0x20, 0x01, 0x18, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x2e, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x18, 0x03, 0x55, 0x00, 0x53, 0x00, 0x42, 0x00, /* ..U.S.B. */
0x20, 0x00, 0x32, 0x00, 0x2e, 0x00, 0x30, 0x00, /*  .2...0. */
0x20, 0x00, 0x48, 0x00, 0x75, 0x00, 0x62, 0x00  /*  .H.u.b. */
};

/* Frame (40 bytes) */
static const unsigned char pkt7[40] = {
0x00, 0x01, 0x20, 0x00, 0x09, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x30, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* 0....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x00, 0x02, 0x00, 0x00, 0x09, 0x00  /* ........ */
};

/* Frame (41 bytes) */
static const unsigned char pkt8[41] = {
0x00, 0x01, 0x20, 0x01, 0x09, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x30, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* 0....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x09, 0x02, 0x19, 0x00, 0x01, 0x01, 0x00, 0xe0, /* ........ */
0x32                                            /* 2 */
};

/* Frame (40 bytes) */
static const unsigned char pkt9[40] = {
0x00, 0x01, 0x20, 0x00, 0x19, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x32, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* 2....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x80, 0x06, 0x00, 0x02, 0x00, 0x00, 0x19, 0x00  /* ........ */
};

/* Frame (57 bytes) */
static const unsigned char pkt10[57] = {
0x00, 0x01, 0x20, 0x01, 0x19, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x32, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* 2....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x09, 0x02, 0x19, 0x00, 0x01, 0x01, 0x00, 0xe0, /* ........ */
0x32, 0x09, 0x04, 0x00, 0x00, 0x01, 0x09, 0x00, /* 2....... */
0x00, 0x00, 0x07, 0x05, 0x81, 0x03, 0x01, 0x00, /* ........ */
0x0c                                            /* . */
};

/* Frame (40 bytes) */
static const unsigned char pkt11[40] = {
0x00, 0x01, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x34, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* 4....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x00, 0x00, /* ........ */
0x00, 0x09, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt12[32] = {
0x00, 0x01, 0x20, 0x01, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x34, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* 4....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x00, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt13[40] = {
0x00, 0x01, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x36, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* 6....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x00, 0x00, /* ........ */
0x00, 0x03, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt14[32] = {
0x00, 0x01, 0x20, 0x01, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x36, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* 6....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x00, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt15[40] = {
0x00, 0x01, 0x20, 0x00, 0x15, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x38, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* 8....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa0, 0x06, 0x00, 0x29, 0x00, 0x00, 0x15, 0x00  /* ...).... */
};

/* Frame (41 bytes) */
static const unsigned char pkt16[41] = {
0x00, 0x01, 0x20, 0x01, 0x09, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x38, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* 8....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x09, 0x29, 0x04, 0x00, 0x00, 0x32, 0x64, 0x00, /* .)...2d. */
0xff                                            /* . */
};

/* Frame (40 bytes) */
static const unsigned char pkt17[40] = {
0x00, 0x01, 0x20, 0x00, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x3a, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* :....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00  /* ........ */
};

/* Frame (34 bytes) */
static const unsigned char pkt18[34] = {
0x00, 0x01, 0x20, 0x01, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x3a, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* :....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x03, 0x00                                      /* .. */
};

/* Frame (40 bytes) */
static const unsigned char pkt19[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x3c, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* <....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (36 bytes) */
static const unsigned char pkt20[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x3c, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* <....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x00, 0x00, 0x00                          /* .... */
};

/* Frame (32 bytes) */
static const unsigned char pkt21[32] = {
0x00, 0x01, 0x20, 0x00, 0x01, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x3e, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* >....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x81, 0x03  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt22[40] = {
0x00, 0x01, 0x20, 0x00, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x40, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* @....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00  /* ........ */
};

/* Frame (34 bytes) */
static const unsigned char pkt23[34] = {
0x00, 0x01, 0x20, 0x01, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x40, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* @....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x03, 0x00                                      /* .. */
};

/* Frame (40 bytes) */
static const unsigned char pkt24[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x42, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* B....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (36 bytes) */
static const unsigned char pkt25[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x42, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* B....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x00, 0x00, 0x00                          /* .... */
};

/* Frame (40 bytes) */
static const unsigned char pkt26[40] = {
0x00, 0x01, 0x20, 0x00, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x44, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* D....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00  /* ........ */
};

/* Frame (34 bytes) */
static const unsigned char pkt27[34] = {
0x00, 0x01, 0x20, 0x01, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x44, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* D....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x03, 0x00                                      /* .. */
};

/* Frame (40 bytes) */
static const unsigned char pkt28[40] = {
0x00, 0x01, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x46, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* F....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x00, 0x00, /* ........ */
0x23, 0x03, 0x08, 0x00, 0x01, 0x00, 0x00, 0x00  /* #....... */
};

/* Frame (32 bytes) */
static const unsigned char pkt29[32] = {
0x00, 0x01, 0x20, 0x01, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x46, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* F....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x00, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt30[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x48, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* H....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (36 bytes) */
static const unsigned char pkt31[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x48, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* H....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x00, 0x00, 0x00                          /* .... */
};

/* Frame (40 bytes) */
static const unsigned char pkt32[40] = {
0x00, 0x01, 0x20, 0x00, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x4a, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* J....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt33[40] = {
0x00, 0x01, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x4c, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* L....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x00, 0x00, /* ........ */
0x23, 0x03, 0x08, 0x00, 0x02, 0x00, 0x00, 0x00  /* #....... */
};

/* Frame (34 bytes) */
static const unsigned char pkt34[34] = {
0x00, 0x01, 0x20, 0x01, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x4a, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* J....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x03, 0x00                                      /* .. */
};

/* Frame (40 bytes) */
static const unsigned char pkt35[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x4e, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* N....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (32 bytes) */
static const unsigned char pkt36[32] = {
0x00, 0x01, 0x20, 0x01, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x4c, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* L....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x00, 0x00  /* ........ */
};

/* Frame (36 bytes) */
static const unsigned char pkt37[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x4e, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* N....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x00, 0x00, 0x00                          /* .... */
};

/* Frame (40 bytes) */
static const unsigned char pkt38[40] = {
0x00, 0x01, 0x20, 0x00, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x50, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* P....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt39[40] = {
0x00, 0x01, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x52, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* R....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x00, 0x00, /* ........ */
0x23, 0x03, 0x08, 0x00, 0x03, 0x00, 0x00, 0x00  /* #....... */
};

/* Frame (34 bytes) */
static const unsigned char pkt40[34] = {
0x00, 0x01, 0x20, 0x01, 0x02, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x50, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* P....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x03, 0x00                                      /* .. */
};

/* Frame (32 bytes) */
static const unsigned char pkt41[32] = {
0x00, 0x01, 0x20, 0x01, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x52, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* R....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x00, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt42[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x54, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* T....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (36 bytes) */
static const unsigned char pkt43[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x54, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* T....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x00, 0x00, 0x00                          /* .... */
};

/* Frame (40 bytes) */
static const unsigned char pkt44[40] = {
0x00, 0x01, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x56, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* V....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x00, 0x00, /* ........ */
0x23, 0x03, 0x08, 0x00, 0x04, 0x00, 0x00, 0x00  /* #....... */
};

/* Frame (32 bytes) */
static const unsigned char pkt45[32] = {
0x00, 0x01, 0x20, 0x01, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x56, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* V....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x00, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt46[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x58, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* X....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa3, 0x00, 0x00, 0x00, 0x01, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (36 bytes) */
static const unsigned char pkt47[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x58, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* X....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x01, 0x00, 0x00                          /* .... */
};

/* Frame (40 bytes) */
static const unsigned char pkt48[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x5a, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* Z....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa3, 0x00, 0x00, 0x00, 0x02, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt49[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x5c, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* \....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa3, 0x00, 0x00, 0x00, 0x03, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt50[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x5e, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* ^....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa3, 0x00, 0x00, 0x00, 0x04, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (36 bytes) */
static const unsigned char pkt51[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x5a, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* Z....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x01, 0x00, 0x00                          /* .... */
};

/* Frame (36 bytes) */
static const unsigned char pkt52[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x5c, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* \....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x01, 0x00, 0x00                          /* .... */
};

/* Frame (36 bytes) */
static const unsigned char pkt53[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x5e, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* ^....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x01, 0x00, 0x00                          /* .... */
};

/* Frame (40 bytes) */
static const unsigned char pkt54[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x60, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* `....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa3, 0x00, 0x00, 0x00, 0x01, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (36 bytes) */
static const unsigned char pkt55[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x60, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* `....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x01, 0x00, 0x00                          /* .... */
};

/* Frame (40 bytes) */
static const unsigned char pkt56[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x62, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* b....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa3, 0x00, 0x00, 0x00, 0x02, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt57[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x64, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* d....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa3, 0x00, 0x00, 0x00, 0x01, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (36 bytes) */
static const unsigned char pkt58[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x62, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* b....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x01, 0x00, 0x00                          /* .... */
};

/* Frame (36 bytes) */
static const unsigned char pkt59[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x64, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* d....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x01, 0x00, 0x00                          /* .... */
};

/* Frame (40 bytes) */
static const unsigned char pkt60[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x66, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* f....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa3, 0x00, 0x00, 0x00, 0x03, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt61[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x68, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* h....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa3, 0x00, 0x00, 0x00, 0x04, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (36 bytes) */
static const unsigned char pkt62[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x66, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* f....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x01, 0x00, 0x00                          /* .... */
};

/* Frame (36 bytes) */
static const unsigned char pkt63[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x68, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* h....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x01, 0x00, 0x00                          /* .... */
};

/* Frame (40 bytes) */
static const unsigned char pkt64[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x6a, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* j....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa3, 0x00, 0x00, 0x00, 0x02, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (36 bytes) */
static const unsigned char pkt65[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x6a, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* j....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x01, 0x00, 0x00                          /* .... */
};

/* Frame (40 bytes) */
static const unsigned char pkt66[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x6c, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* l....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa3, 0x00, 0x00, 0x00, 0x04, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (40 bytes) */
static const unsigned char pkt67[40] = {
0x00, 0x01, 0x20, 0x00, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x6e, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* n....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0xa3, 0x00, 0x00, 0x00, 0x03, 0x00, 0x04, 0x00  /* ........ */
};

/* Frame (36 bytes) */
static const unsigned char pkt68[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x6c, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* l....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x01, 0x00, 0x00                          /* .... */
};

/* Frame (36 bytes) */
static const unsigned char pkt69[36] = {
0x00, 0x01, 0x20, 0x01, 0x04, 0x00, 0x00, 0x00, /* .. ..... */
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x6e, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* n....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x80, 0x00, /* ........ */
0x00, 0x01, 0x00, 0x00                          /* .... */
};

/* Frame (32 bytes) */
static const unsigned char pkt278[32] = {
0x00, 0x01, 0x20, 0x01, 0x00, 0x00, 0x00, 0x00, /* .. ..... */
0xeb, 0x02, 0x00, 0xe0, 0x00, 0x00, 0x00, 0x00, /* ........ */
0x3e, 0x1f, 0xa3, 0x00, 0x00, 0x00, 0x00, 0x00, /* >....... */
0x00, 0x00, 0x10, 0x14, 0x02, 0x16, 0x81, 0x03  /* ........ */
};

static test_urbs_t test_urbs[] = {
    PKT(pkt1),
    PKT(pkt2),
    PKT(pkt3),
    PKT(pkt4),
    PKT(pkt5),
    PKT(pkt6),
    PKT(pkt7),
    PKT(pkt8),
    PKT(pkt9),
    PKT(pkt10),
    PKT(pkt11),
    PKT(pkt12),
    PKT(pkt13),
    PKT(pkt14),
    PKT(pkt15),
    PKT(pkt16),
    PKT(pkt17),
    PKT(pkt18),
    PKT(pkt19),
    PKT(pkt20),
    PKT(pkt21),
    PKT(pkt22),
    PKT(pkt23),
    PKT(pkt24),
    PKT(pkt25),
    PKT(pkt26),
    PKT(pkt27),
    PKT(pkt28),
    PKT(pkt29),
    PKT(pkt30),
    PKT(pkt31),
    PKT(pkt32),
    PKT(pkt33),
    PKT(pkt34),
    PKT(pkt35),
    PKT(pkt36),
    PKT(pkt37),
    PKT(pkt38),
    PKT(pkt39),
    PKT(pkt40),
    PKT(pkt41),
    PKT(pkt42),
    PKT(pkt43),
    PKT(pkt44),
    PKT(pkt45),
    PKT(pkt46),
    PKT(pkt47),
    PKT(pkt48),
    PKT(pkt49),
    PKT(pkt50),
    PKT(pkt51),
    PKT(pkt52),
    PKT(pkt53),
    PKT(pkt54),
    PKT(pkt55),
    PKT(pkt56),
    PKT(pkt57),
    PKT(pkt58),
    PKT(pkt59),
    PKT(pkt60),
    PKT(pkt61),
    PKT(pkt62),
    PKT(pkt63),
    PKT(pkt64),
    PKT(pkt65),
    PKT(pkt66),
    PKT(pkt67),
    PKT(pkt68),
    PKT(pkt69),
};

} // namespace hub_sabrent
