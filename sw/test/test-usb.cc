#include <vector>
#include <algorithm>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "usb/urb.h"
#include "usb/usb.h"
#include "usb/usb_manager.h"
#include "util.h"

#define count_of(array) (sizeof(array) / sizeof(array[0]))

using namespace std;

enum iostatus_t : uint32_t {
    kIOReturnSuccess = 0,
    kUSBHostReturnPipeStalled = 0xe0005000,
    kUSBHostReturnAborted = 0xe00002eb,
};

struct darwin_hdr_t {
    enum class req_type_t : uint8_t {SUBMIT=0, COMPLETE=1};

    uint16_t bcdVersion;                        // 0x0100
    uint8_t hdr_length;                         // 32
    req_type_t req_type;
    uint32_t io_len;
    iostatus_t status;
    uint32_t iso_num_frames;
    uint64_t id;                             // identifies SUBMIT/COMPLETE pairs
    uint32_t location_id;
    uint8_t speed;
    uint8_t dev_idx;
    uint8_t ep_addr;                            // UDRRT_DIRECTION | ep number
    uint8_t ep_xfer;                            // UEDA_*

    bool is_out() const { return (ep_addr & UDRRT_DIRECTION_OUT) != 0; }
} __attribute__((packed));

struct test_urbs_t {
    const uint8_t *buf;
    size_t len;
    const char* name;

    const uint8_t* data() const { return buf + 0x20; }
    uint16_t data_len() const { return len - 0x20; }
    const darwin_hdr_t& hdr() const
    {
        return *reinterpret_cast<const darwin_hdr_t*>(buf);
    }

    bool is_submit() const
    {
        return hdr().req_type == darwin_hdr_t::req_type_t::SUBMIT;
    }
    bool is_complete() const
    {
        return hdr().req_type == darwin_hdr_t::req_type_t::COMPLETE;
    }
    bool is_out() const { return hdr().is_out(); }
    bool match_dev_req(const usb::usb_ep* ep,
                       const usb::usb_dev_request* req) const
    {
        auto& h = hdr();
        // printf("  %s: xfer=%u<>%u ep_addr=%u<>%u is_submit=%d\n",
        //        name, h.ep_xfer, UEDA_CONTROL, h.ep_addr,
        //        ep->bEnd, is_submit());
        return (h.ep_xfer == UEDA_CONTROL && h.ep_addr == ep->bEndpointAddress
                && is_submit() && memcmp(req, data(), 6) == 0);
    }
};

test_urbs_t* test_urbs = nullptr;
size_t num_test_urbs;

struct ep_tracker {
    uint8_t ep_addr;
    bool out;
    test_urbs_t* submit;
    test_urbs_t* complete;
    const usb::hci_urb* urb;

    ep_tracker(const usb::hci_urb* urb)
        : urb(urb)
    {
        ep_addr = urb->ep->bEndpointAddress;
        out = urb->ep->out;
        submit = nullptr;
        complete = nullptr;
    }
    bool match(const usb::hci_urb* that)
    {
        return that == urb;
    }
    void set_submit()
    {
        // Find the first matching test URB.
        test_urbs_t* pt = complete ? complete + 1 : test_urbs;
        for (; pt < test_urbs + num_test_urbs; pt++) {
            auto& t = *pt;
            if (t.is_submit() && t.hdr().ep_addr == ep_addr) {
                // printf("%s: Matched submit %s\n", __FUNCTION__, t.name);
                submit = pt;
                return;
            }
        }
        printf("%s: No submit URB found\n", __FUNCTION__);
    }
    test_urbs_t* get_complete()
    {
        // Find the complete test URB that matches the submit test URB.
        if (submit) {
            test_urbs_t* pt = submit + 1;
            for (; pt < test_urbs + num_test_urbs; pt++) {
                auto& t = *pt;
                if (t.is_complete() && t.is_out() == out
                    && t.hdr().ep_addr == ep_addr) {
                    // printf("%s: Matched\n", __FUNCTION__);
                    complete = pt;
                    submit = nullptr;
                    return pt;
                }
            }
        }
        printf("%s: No complete URB found\n", __FUNCTION__);
        return nullptr;
    }
};

class Ep_Trackers {
public:
    ep_tracker& get(const usb::hci_urb* urb)
    {
        for (auto& t : v) {
            if (t.match(urb))
                return t;
        }
        throw std::domain_error("Ep_Trackers::get(): no such URB\n");
    }
    ep_tracker& get_or_create(const usb::hci_urb* urb)
    {
        try {
            return get(urb);
        }
        catch (std::domain_error) {
            v.push_back(ep_tracker {urb});
            return v.back();
        }
    }
private:
    std::vector<ep_tracker> v;
} ep_trackers;

#define PKT(p) test_urbs_t { p, sizeof(p), #p }

#include "kbd_ms_natural.cc"
#include "game_xbox360.cc"
#include "game_logitech_f310.cc"
#include "hub_sabrent.cc"

#define TEST_URBS(name) { #name, name::test_urbs, count_of(name::test_urbs) }
static struct {
    const char* name;
    test_urbs_t* urbs;
    size_t num_urbs;
} tests[] = {
    TEST_URBS(kbd_ms_natural),
    TEST_URBS(game_xbox360),
    TEST_URBS(game_logitech_f310),
    TEST_URBS(hub_sabrent),
};

bool poll = true;

/***********************************************************************
 * USB HCI simulation
 */

namespace usb {

struct hci_status st;

int hci_init()
{
    return 0;
}

void hci_set_port_power(bool e)
{
    printf("hci_set_port_power\n");
    assert(e);
    // On power set, simulate connection.
    st.PP = true;
    st.CCS = true;
    st.CSC = true;
}

static void disconnect_port()
{
    st.CCS = false;
    st.CSC = true;
}

hci_status hci_get_status(void)
{
    //printf("hci_get_status\n");
    return st;
}

void hci_clear_status(hci_status s)
{
    printf("hci_clear_status:");
    if (s.CSC) {
        st.CSC = false;
        printf(" CSC");
    }
    if (s.PEC) {
        st.PEC = false;
        printf(" PEC");
    }
    if (s.OCC) {
        st.OCC = false;
        printf(" OCC");
    }
    if (s.PRC) {
        st.PRC = false;
        printf(" PRC");
    }
    printf("\n");
}

void hci_reset_port(void)
{
    // Reset completes immediately, and port is enabled.
    st.PR = false;
    st.PE = true;
    st.PEC = true;
    st.PRC = true;
}

void *hci_malloc_read_buffer(unsigned size)
{
    return malloc(size);
}

void hci_free_read_buffer(void *dma_buf)
{
    free(dma_buf);
}

int hci_create_endpoint(usb_ep *)
{
    return 0;
}

void hci_free_endpoint(usb_ep *)
{
}

int hci_enable_endpoint(usb_ep *)
{
    return 0;
}

int hci_disable_endpoint(usb_ep *)
{
    return 0;
}

int hci_ep_control_read(const usb_ep *ep, const usb_dev_request *req,
                        void *data)
{
    /* Find the test URB that matches the request. Search backwards to find
       largest responses first. */
    for (unsigned i = 0; i < num_test_urbs; i++) {
        unsigned idx = (num_test_urbs - 1) - i;
        test_urbs_t& t = test_urbs[idx];
        if (t.match_dev_req(ep, req))
        {
            test_urbs_t& resp = test_urbs[idx + 1];
            uint16_t resp_len = std::min(resp.data_len(), req->wLength);
            // printf("hci_ep_control_read: Matched %s, bytes %u asked, %u sent\n",
            //        t.name, req->wLength, resp_len);
            assert(resp.is_complete());
            memcpy(data, resp.data(), resp_len);
            return resp_len;
        }
    }
    printf("hci_ep_control_read: no response found\n");
    return -EPIPE;                              // stall
}

int hci_ep_control_write(const usb_ep *, const usb_dev_request *,
                         const void *)
{
    return 0;
}

int hci_submit_urb(hci_urb* urb)
{
    if (!urb->ep->out) {
        auto& trk = ep_trackers.get_or_create(urb);
        trk.set_submit();
    }
    return 0;
}

int hci_complete_urb(hci_urb *urb)
{
    if (urb->ep->out) {
        // Eat output URBs.
        std::stringstream ss;
        dump(ss, (uint8_t*)urb->buf, urb->buf_len);
        printf("hci_complete_urb: output\n%s", ss.str().c_str());
        return 0;
    }
    try {
        auto& trk = ep_trackers.get(urb);
        auto* resp = trk.get_complete();
        if (resp) {
            auto& status = resp->hdr().status;
            switch (status) {
            case kIOReturnSuccess:
                break;
            case kUSBHostReturnPipeStalled:
                printf("hci_complete_urb: Stall\n");
                return -EPIPE;
            case kUSBHostReturnAborted:
                printf("hci_complete_urb: Abort\n");
                return -EIO;
            default:
                printf("hci_complete_urb: Unknown status\n");
                return -EIO;
            }
            uint16_t resp_len = std::min(resp->data_len(), urb->buf_len);
            // printf("hci_complete_urb: Matched %s, bytes %u asked, %u sent\n",
            //        resp->name, urb->buf_len, resp_len);
            memcpy(urb->buf, resp->data(), resp_len);
            return resp_len;
        }
        printf("hci_complete_urb: URB not found; no such complete\n");
        poll = false;
        return -EINPROGRESS;
    }
    catch (std::domain_error) {
        printf("hci_complete_urb: URB not found; no such EP\n");
        poll = false;
        return -EPIPE;
    }
}

void hci_abort_urb(hci_urb *)
{
    printf("hci_abort_urb\n");
}

} // namespace usb

void find_test(const char* name)
{
    for (size_t i = 0; i < count_of(tests); i++) {
        auto* test = &tests[i];
        if (strcmp(name, test->name) == 0) {
            test_urbs = test->urbs;
            num_test_urbs = test->num_urbs;
            break;
        }
    }
    if (!test_urbs) {
        printf("%s: No such USB device\n", name);
        exit(1);
    }
}

int main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("Need a name of the USB device\n");
        return 1;
    }

    find_test(argv[1]);

    usb::usb_manager um;
    um.init();

    while (poll) {
        poll_manager_get().poll();

        if (um.is_root_error())
            poll = false;
    }

    printf("...Disconnecting...\n");
    usb::disconnect_port();
    while (um.has_devices())
        poll_manager_get().poll();

    return 0;
}
