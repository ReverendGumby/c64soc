#include <string>
#include <iostream>
#include <assert.h>
#include "fat/path.h"

using namespace std;
using namespace fat;

void assert_replace_extension(const wchar_t* in, const wchar_t* replacement, const wchar_t* sexp)
{
    path p1{in};
    path p2{replacement};
    path exp{sexp};
    cout << "path(" << p1 << ").replace_extension(path(" << p2 << ")) = ";
    path get = p1.replace_extension(p2);
    cout << get << '\n';
    assert(get.string() == exp.string());
}

void assert_append(const wchar_t* slhs, const wchar_t* srhs, const wchar_t* sexp)
{
    path lhs{slhs};
    path rhs{srhs};
    path exp{sexp};
    path get = lhs;
    get.append(rhs);
    cout << "path(" << lhs << ") / " << rhs << " = " << get << '\n';
    assert(get.string() == exp.string());
}

void assert_lexically_normal(const wchar_t* in, const wchar_t* sexp)
{
    path p{in};
    path exp{sexp};
    path lnp = p.lexically_normal();
    cout << p << ".lexically_normal() == " << lnp << '\n';
    assert(lnp.string() == exp.string());
}

int main(int, char *[])
{
    typedef struct {
        string p;
        string root_path;
        string relative_path;
        string filename;
    } inpaths_t;

    inpaths_t inpaths[] = {
        {
            .p = "",
            .root_path = "",
            .relative_path = "",
            .filename = "",
        },
        {
            .p = ".",
            .root_path = "",
            .relative_path = "",
            .filename = ".",
        },
        {
            .p = "..",
            .root_path = "",
            .relative_path = "",
            .filename = "..",
        },
        {
            .p = "foo",
            .root_path = "",
            .relative_path = "",
            .filename = "foo",
        },
        {
            .p = "\\",
            .root_path = "\\",
            .relative_path = "",
            .filename = "",
        },
        {
            .p = "\\foo",
            .root_path = "\\",
            .relative_path = "",
            .filename = "foo",
        },
        {
            .p = "foo\\",
            .root_path = "",
            .relative_path = "foo",
            .filename = "",
        },
        {
            .p = "\\foo\\",
            .root_path = "\\",
            .relative_path = "foo",
            .filename = "",
        },
        {
            .p = "foo\\bar",
            .root_path = "",
            .relative_path = "foo",
            .filename = "bar",
        },
        {
            .p = "\\foo\\bar",
            .root_path = "\\",
            .relative_path = "foo",
            .filename = "bar",
        },
        {
            .p = "\\.",
            .root_path = "\\",
            .relative_path = "",
            .filename = ".",
        },
        {
            .p = ".\\",
            .root_path = "",
            .relative_path = ".",
            .filename = "",
        },
        {
            .p = "\\..",
            .root_path = "\\",
            .relative_path = "",
            .filename = "..",
        },
        {
            .p = "..\\",
            .root_path = "",
            .relative_path = "..",
            .filename = "",
        },
        {
            .p = "foo\\.",
            .root_path = "",
            .relative_path = "foo",
            .filename = ".",
        },
        {
            .p = "foo\\..",
            .root_path = "",
            .relative_path = "foo",
            .filename = "..",
        },
        {
            .p = "foo\\.\\",
            .root_path = "",
            .relative_path = "foo\\.",
            .filename = "",
        },
        {
            .p = "foo\\.\\bar",
            .root_path = "",
            .relative_path = "foo\\.",
            .filename = "bar",
        },
        {
            .p = "foo\\..\\",
            .root_path = "",
            .relative_path = "foo\\..",
            .filename = "",
        },
        {
            .p = "foo\\..\\bar",
            .root_path = "",
            .relative_path = "foo\\..",
            .filename = "bar",
        },
    };

    for (auto inpath : inpaths)
    {
        path p{inpath.p};
        path root_path{inpath.root_path};
        path relative_path{inpath.relative_path};
        path filename{inpath.filename};
        cout << inpath.p << "\n";

        cout << "  iteration: ";
        string iter_str;
        for (auto i : p)
        {
            if (!iter_str.empty())
                iter_str += ",";
            iter_str += '"' + i.string() + '"';
        }
        cout << '(' << iter_str << ")\n";

        cout << "  root_name(): "      << p.root_name() << "\n";
        assert(p.root_name() == path());
        assert(p.has_root_name() == (p.root_name().size() != 0));
        cout << "  root_directory(): " << p.root_directory() << "\n";
        assert(p.has_root_directory() == (p.root_directory().size() != 0));
        assert(p.root_directory() == root_path);
        cout << "  root_path(): "      << p.root_path() << "\n";
        assert(p.has_root_path() == (p.root_path().size() != 0));
        assert(p.root_directory() == p.root_path());
        assert(p.root_path() == root_path);
        cout << "  relative_path(): "  << p.relative_path() << "\n";
        assert(p.relative_path() == relative_path);
        assert(p.has_relative_path() == (p.relative_path().size() != 0));
        cout << "  parent_path(): "    << p.parent_path() << "\n";
        assert(p.has_parent_path() == (p.parent_path().size() != 0));
        assert(p.parent_path() == p.root_directory() / p.relative_path());
        cout << "  filename(): "       << p.filename() << "\n";
        assert(p.filename() == filename);
        assert(p.has_filename() == (p.filename().size() != 0));
        assert(p.has_stem() == (p.stem().size() != 0));
        assert(p.has_extension() == (p.extension().size() != 0));

        path p2{p};
        p2.remove_filename();
        cout << "  remove_filename(): " << p2 << "\n";
        assert(!p2.has_filename());

        cout << "\n";
    }

    {
        cout << "path(foo) / path(bar) = " << path(L"foo") / path(L"bar") << "\n";
    }

    {
        path p{L"\\foo\\bar.txt"};
        cout << "path(" << p << ").stem() = " << p.stem() << "\n";
    }
    {
        path p{L"foo.bar.baz.tar"};
        for (; !p.extension().empty(); p = p.stem())
            cout << p.extension() << '\n';
    }
    
    {
        assert_replace_extension(L"\\test.1\\foo.bar.baz", L".tar.taz", L"\\test.1\\foo.bar.tar.taz");
        assert_replace_extension(L"\\", L".sav", L"\\.sav");
        assert_replace_extension(L"foo", L".sav", L"foo.sav");
        assert_replace_extension(L"\\foo\\.", L"png", L"\\foo\\..png");
        assert_replace_extension(L"\\foo\\.", L".png", L"\\foo\\..png");
        assert_replace_extension(L"\\foo\\", L"png", L"\\foo\\.png");
        assert_replace_extension(L"\\foo\\", L".png", L"\\foo\\.png");
        assert_replace_extension(L".", L"png", L"..png");
        assert_replace_extension(L"", L"png", L".png");
        cout << "\n";
    }

    {
        path p{L"\\"};
        cout << p << ".filename() = " << p.filename() << "\n";
        cout << p << ".has_filename() = " << p.has_filename() << "\n";
        cout << p << ".has_root_directory() = " << p.has_root_directory() << "\n";
        cout << p << ".is_absolute() = " << p.is_absolute() << "\n";
        cout << "\n";
    }

    {
        assert_append(L"\\", L"foo", L"\\foo");
        assert_append(L"foo", L"", L"foo\\");
        assert_append(L"foo", L"\\bar", L"\\bar");
        assert_append(L"foo", L"bar", L"foo\\bar");
        assert_append(L"\\foo", L"bar", L"\\foo\\bar");
        cout << "\n";
    }

    {
        assert_lexically_normal(L"\\.", L"\\");
        assert_lexically_normal(L"\\\\.", L"\\");
        assert_lexically_normal(L"\\.\\\\", L"\\");
        assert_lexically_normal(L".\\foo", L"foo");
        assert_lexically_normal(L"\\..", L"\\");
        assert_lexically_normal(L"\\..\\", L"\\");
        assert_lexically_normal(L"foo\\..", L"");
        assert_lexically_normal(L"foo\\..\\", L"");
        assert_lexically_normal(L"foo\\..", L"");
        assert_lexically_normal(L"foo\\bar\\..", L"foo\\");
        assert_lexically_normal(L"foo\\.\\bar\\..", L"foo\\");
        assert_lexically_normal(L"foo\\.\\\\\\bar\\..\\", L"foo\\");

        cout << "\n";
    }

    return 0;
}
