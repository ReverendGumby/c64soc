#pragma once

#include "ui/screen.h"

class screen : public ui::screen
{
public:
    static constexpr int WIDTH = 32;
    static constexpr int HEIGHT = 28; // 224 rows

    uint8_t ascii_to_screen(char);

    screen();
    virtual ~screen();

    int get_width() const { return WIDTH; }
    int get_height() const { return HEIGHT; }

    void set_bg(color_t c);
    void set_border(color_t c);

    void init();
    void clear(color_t c);

    void write(int x, int y, uint8_t b, color_t c);

    void invert_rect(int x, int y, int w, int h);
    void invert_row(int y) { invert_rect(0, y, WIDTH, 1); }
};
