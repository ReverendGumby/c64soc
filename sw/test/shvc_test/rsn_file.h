#pragma once

#include "fat/path.h"
#include <string.h>
#include <unistd.h>
#include <vector>
#include "unrar/unrar/rar.hpp"
#include "unrar/fstream.h"

struct rsn_file
{
    struct file_t {
        fat::path path;
        size_t size;
        std::unique_ptr<filebuf> buf;
    };

    std::vector<file_t> get_files(const wchar* ArcName);
    fstream extract_file(const file_t& file);
};
