#include <istream>
#include <string.h>
#include <iostream>
#include "filebuf.h"

//#define _DEBUG

filebuf::filebuf(unsigned buf_size)
    : std::basic_streambuf<char>(), buf_size(buf_size)
{
    _buf = new char[buf_size];
    _used = 0;
    _off = 0;
}

filebuf::filebuf(filebuf&& rhs)
    : std::basic_streambuf<char>(rhs)
{
    _buf = rhs._buf;
    _used = rhs._used;
    _off = rhs._off;

    rhs._buf = nullptr;
}

filebuf::~filebuf()
{
    try {
        delete [] _buf;
        _buf = nullptr;
    } catch (...) {
    }
}

void filebuf::write(const char* p, size_t l)
{
    memcpy(_buf + _used, p, l);
    _used += l;
}

bool filebuf::is_full() const
{
    return _used == buf_size;
}

void filebuf::reset()
{
    setg(_buf, _buf, _buf + _used);
}

std::streamsize filebuf::showmanyc()
{
    //return std::streamsize(_f->size() - _f->tell());
    return std::streamsize(_used);
}

filebuf::int_type filebuf::underflow()
{
    // EOF follows buffer exhaustion.
    _used = 0;
#ifdef _DEBUG
    std::cout << "filebuf::underflow: _off=" << _off
                          << ", _used=" << _used
                          << ", *_buf=" << _buf[0]
                          << ", eback()=&_buf[" << eback() - _buf << "]"
                          << ", gptr()=&_buf[" << gptr() - _buf << "]"
                          << ", egptr()=&_buf[" << egptr() - _buf << "]\n";
#endif
    if (_used == 0)
        return traits_type::eof();
    return _buf[0];
}

filebuf::int_type filebuf::pbackfail(int_type c)
{
#ifdef _DEBUG
    std::cout << "filebuf::pbackfail(c=" << c << ")\n";
#endif
#if 0
    if (!is_open() || _off == 0)
        return traits_type::eof();

    if (c == traits_type::eof() || gptr() > eback()) {
        seekpos(_off - pos_type(1));
        _used = _f->read(_buf, buf_size);
        setg(_buf, _buf + 1, _buf + _used);
    }
#endif
#ifdef _DEBUG
    std::cout << "filebuf::pbackfail: _off=" << _off
                          << ", _used=" << _used
                          << ", *_buf=" << _buf[0]
                          << ", eback()=&_buf[" << eback() - _buf << "]"
                          << ", gptr()=&_buf[" << gptr() - _buf << "]"
                          << ", egptr()=&_buf[" << egptr() - _buf << "]\n";
#endif
#if 0
    if (_used == 0)
        return traits_type::eof();

    gbump(-1);
    if (c == traits_type::eof())
        return traits_type::not_eof(c);
    if (!traits_type::eq(c, *gptr()))
        *gptr() = c;
#endif
    return c;
}

filebuf::pos_type filebuf::seekoff(off_type off,
                                   std::ios_base::seekdir dir,
                                   std::ios_base::openmode)
{
#ifdef _DEBUG
    std::cout << "filebuf::seekoff(off=" << off << ", dir="
                          << dir << ")\n";
#endif
    switch (dir) {
    case std::ios::beg:
        _off = off;
        break;
    case std::ios::end:
        _off = _used + off;
        break;
    case std::ios::cur:
        _off = _off + off_type(gptr() - _buf) + off;
        break;
    default:
        return pos_type(off_type(-1));
    }

    setg(_buf, _buf + _off, _buf + _used);
    return pos_type(_off);
}

filebuf::pos_type filebuf::seekpos(pos_type sp,
                                   std::ios_base::openmode)
{
    return seekoff(off_type(sp), std::ios::beg);
}
