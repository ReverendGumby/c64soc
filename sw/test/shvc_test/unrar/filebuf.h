#pragma once

#include <streambuf>

class filebuf : public std::basic_streambuf<char>
{
public:
    filebuf(unsigned buf_size);
    filebuf(const filebuf&) = delete;
    filebuf(filebuf&&);
    ~filebuf() override;

    void write(const char* p, size_t l);
    bool is_full() const;
    void reset();

protected:
    std::streamsize showmanyc() override;
    int_type underflow() override;
    int_type pbackfail(int_type c = traits_type::eof()) override;
    pos_type seekoff(off_type off,
                     std::ios_base::seekdir dir,
                     std::ios_base::openmode which
                     = std::ios_base::in | std::ios_base::out) override;
    pos_type seekpos(pos_type sp,
                     std::ios_base::openmode which
                     = std::ios_base::in | std::ios_base::out) override;

private:
    unsigned buf_size;

    char* _buf;                                 // read buffer
    pos_type _used;                             // read buffer occupancy
    pos_type _off;                              // file position of _buf[0]
};
