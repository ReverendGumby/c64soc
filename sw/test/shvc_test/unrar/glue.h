#pragma once

#include <functional>

class filebuf;

extern std::function<filebuf*(void)> _unrar_filebuf_on_open;
extern std::function<void(void)> _unrar_filebuf_on_close;
