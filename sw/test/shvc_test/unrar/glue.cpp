#include "fat/file.h"
#include "fat/filesystem.h"
#include "fat/path.h"
#include "filebuf.h"
#include "util/debug.h"

#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <sys/file.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <sys/times.h>
#include <reent.h>

#define DBG_LVL 0
#define DBG_TAG "RARG"

// TODO: Move these into libgloss

static constexpr int _unrar_fd_open_min = 3;
static constexpr int _unrar_fd_open_num = 3;
static constexpr int _unrar_fd_open_max = _unrar_fd_open_min + _unrar_fd_open_num - 1;
static constexpr int _unrar_fd_dup = 6;
static fat::file* _unrar_f[_unrar_fd_open_num];

filebuf* _unrar_filebuf;
std::function<filebuf*(void)> _unrar_filebuf_on_open;
std::function<void(void)> _unrar_filebuf_on_close;

extern "C"
int
open(const char *pathname, int, ...)
{
    int ret = -1;
    int i;
    for (i = 0; i < _unrar_fd_open_num; i++)
        if (_unrar_f[i] == nullptr)
            break;
    if (i == _unrar_fd_open_num) {
        errno = ENFILE;
    } else {
        try {
            fat::path path(pathname);
            _unrar_f[i] = new fat::file(fat::open(path));
            ret = _unrar_fd_open_min + i;
        } catch (fat::filesystem_error& e) {
            errno = ENOENT;
        }
    }
    return ret;
}

extern "C"
int
close(int fd)
{
    DBG_PRINTF(1, "%d\n", fd);
    if (fd >= _unrar_fd_open_min && fd <= _unrar_fd_open_max) {
        int i = fd - _unrar_fd_open_min;
        if (_unrar_f[i]) {
            delete _unrar_f[i];
            _unrar_f[i] = nullptr;
        }
    }
    else if (fd == _unrar_fd_dup) {
        _unrar_filebuf_on_close();
        _unrar_filebuf = nullptr;
    }
    return 0;
}

extern "C"
int
isatty(int fd)
{
    if (fd >= _unrar_fd_open_min && fd <= _unrar_fd_open_max)
        return 0;
    return 1;
}

extern size_t (*readbytes)(char* buf, size_t nbyte);

extern "C"
int
_read(int fd, void* buf, size_t nbyte)
{
    int ret = -1;
    if (fd >= _unrar_fd_open_min && fd <= _unrar_fd_open_max) {
        int i = fd - _unrar_fd_open_min;
        if (!_unrar_f[i]) {
            errno = EBADF;
        }
        ret = _unrar_f[i]->read(buf, nbyte);
    } else
        ret = readbytes((char*)buf, nbyte);
    return ret;
}

extern void (*outbyte)(char b);

extern "C"
int _write(int fd, const void* buf, size_t nbyte)
{
    if (fd == _unrar_fd_dup) {
        DBG_PRINTF(2, "{%d}", nbyte);
        _unrar_filebuf->write((const char*)buf, nbyte);
    } else {
        for (size_t i = 0; i < nbyte; i++) {
            const char c = ((const char*)buf)[i];
            if (c == '\n')
                outbyte('\r');
            outbyte(c);
        }
    }
    return nbyte;
}

extern "C"
off_t
lseek(int fd, off_t offset, int whence)
{
    int ret = -1;
    if (fd >= _unrar_fd_open_min && fd <= _unrar_fd_open_max) {
        int i = fd - _unrar_fd_open_min;
        if (!_unrar_f[i]) {
            errno = EBADF;
        }
        auto sf = fat::seekfrom::start;
        if (whence == SEEK_CUR)
            sf = fat::seekfrom::cur;
        else if (whence == SEEK_END)
            sf = fat::seekfrom::end;
        try {
            _unrar_f[i]->seek(offset, sf);
            ret = _unrar_f[i]->tell();
        } catch (fat::filesystem_error& e) {
            errno = EINVAL;
        }
    } else {
        errno = EPIPE;
    }
    return ret;
}

mode_t
umask (mode_t mask)
{
    return mask;
}

char *
getpass(const char *prompt)
{
    return 0;
}

int
flock(int fd, int operation)
{
    return 0;
}

int
ftruncate (int file, off_t length)
{
    return 0;
}

int
dup (int oldfd)
{
    if (oldfd == 1) {
        DBG_PRINTF(1, "\n");
        _unrar_filebuf = _unrar_filebuf_on_open();
        return _unrar_fd_dup;
    }
    return -1;
}

int
fsync(int fd)
{
    return 0;
}

int
mkdir(const char *pathname, mode_t mode)
{
    return 0;
}

int
chmod (const char *_path, mode_t _mode)
{
    return 0;
}

int
rmdir(const char *pathname)
{
    return 0;
}

char *
getcwd(char *buf, size_t size)
{
    buf[0] = '\\';
    buf[1] = 0;
    return buf;
}

int
symlink(const char *target, const char *linkpath)
{
    return 0;
}

struct passwd *
getpwnam (const char *name)
{
    return NULL;
}

struct group *
getgrnam (const char *name)
{
    return NULL;
}

int
lchown (const char *__path, uid_t __owner, gid_t __group)
{
    return 0;
}

_CLOCK_T_
_times_r (struct _reent *, struct tms *)
{
    return 0;
}

int
_unlink_r (struct _reent *, const char *)
{
    return 0;
}

int
_link_r (struct _reent *ptr, const char *old, const char *_new)
{
    return 0;
}

int
_stat_r (struct _reent *ptr, const char *file, struct stat *pstat)
{
    return 0;
}
