#include "fstream.h"

fstream::fstream(filebuf* buf)
    : std::basic_iostream<char>{buf}
{
}

fstream::~fstream()
{
}

void fstream::write_buf(const char* p, size_t l)
{
    dynamic_cast<filebuf*>(rdbuf())->write(p, l);
}

bool fstream::is_buf_full()
{
    return dynamic_cast<filebuf*>(rdbuf())->is_full();
}
