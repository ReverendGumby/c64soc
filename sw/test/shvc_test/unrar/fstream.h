#pragma once

#include "filebuf.h"
#include <istream>

class fstream : public std::basic_iostream<char>
{
public:
    fstream(filebuf*);
    fstream(const fstream&) = delete;
    ~fstream() override;

    void write_buf(const char* p, size_t l);
    bool is_buf_full();
};
