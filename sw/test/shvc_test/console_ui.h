#pragma once

#include "fat/path.h"
#include "util/pollable.h"
#include "os/dev/char_device.h"
#include "rsn_file.h"
#include <vector>

class console_ui : private pollable
{
public:
    console_ui();

private:
    void load_songlist(const fat::path& path);
    void show_song(int dir);
    void read_and_load_rsn();
    void show_rsn(int dir);
    void check_console();
    void load_rsnlist();

    // interface for pollable
    const char* get_poll_name() const { return "console_ui"; }
    void poll();

private:
    os::dev::char_device* con;
    bool ready;
    std::vector<rsn_file::file_t> songlist;
    decltype(songlist)::iterator cur_song;
    std::vector<fat::path> rsnlist;
    decltype(rsnlist)::iterator cur_rsn;
};
