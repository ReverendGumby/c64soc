#pragma once

#include "fat/path.h"
#include "fat/directory_entry.h"
#include "ui/fullscreen_view.h"
#include "ui/list_view.h"
#include "ui/label.h"

namespace main_menu {

class file_view : public ui::fullscreen_view,
                  private ui::list_view::model
{
public:
    struct model
    {
        struct file_item
        {
            fat::path fname;
            fat::filesize size;
        };

        using file_list = std::vector<file_item>;
        using dir_list = std::vector<fat::path>;

        virtual const fat::path& get_curdir() = 0;
        virtual const dir_list& get_dirs() = 0;
        virtual const file_list& get_files() = 0;
        virtual std::string get_last_err() = 0;

        virtual void open_dir(const fat::path&) = 0;
        virtual void open_file(const fat::path&) = 0;
        virtual void clear_last_err() = 0;
    };

    file_view(model&);

    static constexpr int update_flag_files = 1<<0;
    static constexpr int update_flag_last_err = 1<<1;

    void update(int flags);

protected:
    // interface for ui::view
    virtual const char* get_class_name() const { return "file_view"; }

    // interface for ui::list_view::model
    virtual std::string get_item(int idx);
    virtual void on_selected_changed(int idx);
    virtual void on_selected(int idx);

private:
    void update_files();
    void update_last_err();

    void draw_directory(const drawing_context*);

    // interface for ui::fullscreen_view
    //virtual bool on_key_down(const keypress_t& k, int repeat);
    virtual void on_hide();

    model& model;
    int dir_count;                              // number of directories
    int count;                                  // total number of list items
    bool no_dir;

    ui::label lbl_menu, lbl_dir;
    ui::list_view list;
    ui::label lbl_err, lbl_open, lbl_exit;
};

} // namespace main_menu
