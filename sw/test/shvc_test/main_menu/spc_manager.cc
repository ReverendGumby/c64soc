#include "fat/path.h"
#include "fat/directory_iterator.h"
#include "fat/filesystem.h"
#include "../song.h"
#include "spc_manager.h"
#include <fat/fstream.h>

namespace main_menu {

spc_manager::spc_manager(sd::sd_manager& sd_mgr)
    : _file_view(*this)
{
    sd_mgr.add_listener(this);
}

void spc_manager::read_dir()
{
    dirs = dir_list{};
    files = file_list{};
    last_err = "";

    try
    {
        for (const auto& de : fat::directory_iterator{curdir})
        {
            if (de.is_hidden)
                continue;
            if (de.is_dir) {
                if (de.name == fat::path{L"."})
                    continue;
                dirs.push_back(de.name);
            } else {
                const auto& ext = de.name.extension();
                if (ext != fat::path{L".spc"} && ext != fat::path{L".SPC"})
                    continue;
                file_item item;
                item.fname = de.name;
                item.size = de.size;
                files.push_back(item);
            }
        }
    }
    catch (std::runtime_error& e)
    {
        last_err = e.what();
    }

    _file_view.update(file_view::update_flag_files | file_view::update_flag_last_err);
}

void spc_manager::open_dir(const fat::path& p)
{
    curdir = (curdir / p).lexically_normal();
    read_dir();
}

void spc_manager::read_spc(const fat::path& p)
{
    auto fn = p.string();
    printf("Reading song %s\n", fn.c_str());

    song_t* song;
    {
        auto f = fat::fstream(p);
        song = read_song(f);
    }

    load_song(*song);
    delete song;
}

void spc_manager::open_file(const fat::path& p)
{
    fat::path ap = p;
    if (ap.is_relative())
        ap = curdir / p;

    last_err = "";
    try
    {
        read_spc(ap);
    }
    catch (std::runtime_error& e)
    {
        last_err = e.what();
        //_file_view.restore_and_show_all();
    }

    _file_view.update(file_view::update_flag_last_err);
}

void spc_manager::mounted()
{
    curdir = fat::path{L"\\"};
    read_dir();
}

void spc_manager::unmounted()
{
    dirs = dir_list{};
    files = file_list{};
    curdir.clear();
    last_err = "";

    _file_view.update(file_view::update_flag_files | file_view::update_flag_last_err);
}

void spc_manager::clear_last_err()
{
    last_err = "";
    _file_view.update(file_view::update_flag_last_err);
}

} // namespace main_menu
