#pragma once

#include <vector>
#include <functional>
#include "ui/fullscreen_view.h"
#include "ui/label.h"
#include "ui/list_view.h"

namespace main_menu {

class menu_base : public ui::fullscreen_view,
                  private ui::list_view::model
{
public:
    // header: reserve rows at top, above lbl_menu and list
    // footer; reserve rows between list and lbl_exit
    menu_base(const char* menu_txt, int header = 0, int footer = 0);

protected:
    using menu_item_t = std::pair<const std::string, std::function<void(void)>>;
    std::vector<menu_item_t> menu_items;

    void add_menu_item(const std::string&&, ui::view* = nullptr);
    void add_menu_item(const std::string&&, std::function<void(void)>);
    virtual void update();

    // interface for ui::list_view::model
    virtual std::string get_item(int idx);
    virtual void on_selected_changed(int idx);
    virtual void on_selected(int idx);

    ui::label lbl_menu, lbl_exit;
    ui::list_view list;
};

} // namespace main_menu
