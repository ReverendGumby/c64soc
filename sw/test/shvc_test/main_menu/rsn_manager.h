#pragma once

#include "../rsn_file.h"
#include "fat/path.h"
#include "sd/sd_manager.h"
#include "file_view.h"
#include "song_view.h"
#include <vector>

namespace main_menu {

class rsn_manager : private sd::sd_manager::listener,
                    private file_view::model,
                    private song_view::model
{
public:
    rsn_manager(sd::sd_manager&);

    ui::view* get_view() { return &_file_view; }

private:
    friend file_view;

    void read_dir();
    void read_rsn(const fat::path&);

    // interface for file_view::model
    using file_list = file_view::model::file_list;
    using dir_list = file_view::model::dir_list;
    const fat::path& get_curdir() override { return curdir; }
    const dir_list& get_dirs() override { return dirs; }
    virtual const file_list& get_files() { return files; }
    virtual std::string get_last_err() { return last_err; }
    void open_dir(const fat::path&) override;
    virtual void open_file(const fat::path&);
    virtual void clear_last_err();

    // interface for song_view_model
    using song_list = song_view::model::song_list;
    const std::string& get_cur_rsn() override { return currsn; }
    bool get_is_loading() override { return songs_loading; }
    const song_list& get_songs() override { return songs; }
    void open_song(const song_view::model::song_item&);

    // interface for sd::sd_manager::listener
    virtual void mounted();
    virtual void unmounted();

    fat::path curdir;
    dir_list dirs;
    file_list files;
    song_list songs;
    std::string currsn;
    bool songs_loading;
    std::string last_err;
    file_view _file_view;
    song_view _song_view;
};

} // namespace main_menu
