#include <utility>
#include "util/string.h"
#include "plat/manager.h"
#include "app/version.h"
#include "rsn_manager.h"
#include "spc_manager.h"

#include "top_menu.h"

namespace main_menu {

top_menu::top_menu(plat::manager& plat_mgr, rsn_manager& rsn_mgr,
                   spc_manager& spc_mgr)
    : menu_base("Top Menu", 7, 1)
{
    add_menu_item("Select archive", rsn_mgr.get_view());
    add_menu_item("Select SPC file", spc_mgr.get_view());
    update();

    set_color_border(color_t::CYAN);
    set_color_bg(color_t::BLUE);

    int y = 0;
    lbl_hdr.set_color_fg(color_t::LT_GREEN);
    lbl_hdr.add_to_view(this, "  SNES Music Player on an FPGA", 0, y);

    y += 2;
    int h = 2;
    std::string branch = std::string{sw_build_branch}.substr(0, 20); // truncate
    std::string s = string_printf(
        "SW version %s (@%s)\n"
        "   build %s\n",
        branch.c_str(),
        sw_build_rev,
        sw_build_timestamp);
    lbl_sw_ver.set_color_fg(color_t::GRAY2);
    lbl_sw_ver.set_text(s);
    lbl_sw_ver.set_pos_size(0, y, max_width(), h);
    lbl_sw_ver.show();
    add_child_view(&lbl_sw_ver);

    y += 2;
    h = 2;
    auto hw_ver = plat_mgr.get_rtl_version();
    branch = hw_ver.git_branch.substr(0, 20); // truncate
    s = string_printf(
        "HW version %s (@%s)\n"
        "   build %s\n",
        branch.c_str(),
        hw_ver.git_rev.c_str(),
        hw_ver.timestamp.c_str());
    lbl_hw_ver.set_color_fg(color_t::GRAY2);
    lbl_hw_ver.set_text(s);
    lbl_hw_ver.set_pos_size(0, y, max_width(), h);
    lbl_hw_ver.show();
    add_child_view(&lbl_hw_ver);

    // y = MAX_HEIGHT - 2;
    // lbl_exit.add_to_view(this, "Press [ESC] to exit and start emulator", 0, y++);
    // lbl_enter.add_to_view(this, "Press [F12] at any time for Top Menu", 0, y++);
    lbl_exit.hide();
}

void top_menu::hide()
{
    // There is no escape.
}

} // namespace main_menu
