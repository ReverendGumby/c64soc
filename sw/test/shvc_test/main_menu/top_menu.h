#pragma once

#include "menu_base.h"
#include "plat/manager.h"
#include "ui/label.h"

namespace main_menu {

class rsn_manager;
class spc_manager;

class top_menu : public menu_base
{
public:
    top_menu(plat::manager&, rsn_manager&, spc_manager&);

protected:
    // interface for ui::view
    void hide() override;
    virtual const char* get_class_name() const { return "top_menu"; }

private:
    // UI
    ui::label lbl_hdr, lbl_sw_ver, lbl_hw_ver;
};

} // namespace main_menu
