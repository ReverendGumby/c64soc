#include "fat/path.h"
#include "fat/directory_iterator.h"
#include "fat/filesystem.h"
#include "../song.h"
#include "rsn_manager.h"

namespace main_menu {

rsn_manager::rsn_manager(sd::sd_manager& sd_mgr)
    : _file_view(*this), _song_view(*this)
{
    songs_loading = false;
    sd_mgr.add_listener(this);
}

void rsn_manager::read_dir()
{
    dirs = dir_list{};
    files = file_list{};
    last_err = "";

    try
    {
        for (const auto& de : fat::directory_iterator{curdir})
        {
            if (de.is_hidden)
                continue;
            if (de.is_dir) {
                if (de.name == fat::path{L"."})
                    continue;
                dirs.push_back(de.name);
            } else {
                const auto& ext = de.name.extension();
                if (ext != fat::path{L".rsn"} && ext != fat::path{L".RSN"})
                    continue;
                file_item item;
                item.fname = de.name;
                item.size = de.size;
                files.push_back(item);
            }
        }
    }
    catch (std::runtime_error& e)
    {
        last_err = e.what();
    }

    _file_view.update(file_view::update_flag_files | file_view::update_flag_last_err);
}

void rsn_manager::open_dir(const fat::path& p)
{
    curdir = (curdir / p).lexically_normal();
    read_dir();
}

void rsn_manager::read_rsn(const fat::path& p)
{
    rsn_file rsn;
    songs = rsn.get_files(p.native().c_str());
}

void rsn_manager::open_file(const fat::path& p)
{
    currsn = p.string();
    songs.clear();
    songs_loading = true;
    _song_view.update(song_view::update_flag_songs);
    _song_view.show();

    fat::path ap = p;
    if (ap.is_relative())
        ap = curdir / p;

    last_err = "";
    try
    {
        read_rsn(ap);
        songs_loading = false;
        _song_view.update(song_view::update_flag_songs);
    }
    catch (std::runtime_error& e)
    {
        last_err = e.what();
        //_file_view.restore_and_show_all();
        _song_view.hide();
    }

    _file_view.update(file_view::update_flag_last_err);
}

void rsn_manager::open_song(const song_view::model::song_item& item)
{
    const auto& path = item.path;
    auto fn = path.string();
    printf("Reading song %s\n", fn.c_str());

    song_t* song;
    {
        rsn_file rsn;
        auto f = rsn.extract_file(item);
        song = read_song(f);
    }

    load_song(*song);
    delete song;
}

void rsn_manager::mounted()
{
    curdir = fat::path{L"\\"};
    read_dir();
}

void rsn_manager::unmounted()
{
    dirs = dir_list{};
    files = file_list{};
    curdir.clear();
    last_err = "";

    _file_view.update(file_view::update_flag_files | file_view::update_flag_last_err);
}

void rsn_manager::clear_last_err()
{
    last_err = "";
    _file_view.update(file_view::update_flag_last_err);
}

} // namespace main_menu
