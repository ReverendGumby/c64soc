#include <sstream>

#include "file_view.h"

namespace main_menu {

file_view::file_view(struct model& model)
    : model(model), list(*this)
{
    int y = 0;
    int y_footer = max_height() - 4;

    lbl_menu.add_to_view(this, "Select Archive", 0, y);

    y += 2;
    lbl_dir.add_to_view(this, "", 0, y, max_width());

    y += 2;
    int h = y_footer - 1 - y;
    list.set_pos_size(1, y, max_width() - 2, h);
    list.show();
    add_child_view(&list);

    y = y_footer;
    lbl_err.add_to_view(this, "", 0, y, max_width());

    y += 2;
    lbl_open.add_to_view(this, "[RET] Open Archive", 0, y++);
    lbl_exit.add_to_view(this, "[ESC] Return to previous menu", 0, y++);

    set_color_bg(color_t::PURPLE);

    update(update_flag_files | update_flag_last_err);
}

void file_view::update(int flags)
{
    if (flags & update_flag_files)
        update_files();
    if (flags & update_flag_last_err)
        update_last_err();
}

std::string file_view::get_item(int idx)
{
    if (count == 0 && idx == 0)
        return "(empty)";

    std::stringstream ss;
    if (idx < dir_count) {
        const auto& dir = model.get_dirs().at(idx);
        ss << '[' << dir.string() << ']';
    } else {
        idx -= dir_count;
        const auto& it = model.get_files().at(idx);
        constexpr int fnw = 28;
        ss << std::left << std::setfill(' ') << std::setw(fnw)
           << it.fname.string().substr(0, fnw);
    }
    return ss.str();
}

void file_view::on_selected_changed(int)
{
}

void file_view::on_selected(int idx)
{
    if (idx == -1)
        return;

    if (idx < dir_count) {
        const auto& dirs = model.get_dirs();
        const auto& dir = dirs[idx];
        model.open_dir(dir);
    } else {
        idx -= dir_count;
        const auto& files = model.get_files();
        const auto& item = files[idx];
        fat::path p = item.fname;
        model.open_file(p);
    }
}

void file_view::update_files()
{
    const auto& curdir = model.get_curdir();
    const auto& dirs = model.get_dirs();
    const auto& files = model.get_files();
    no_dir = curdir.empty();

    dir_count = dirs.size();
    auto file_count = files.size();
    count = dir_count + file_count;

    if (no_dir) {
        lbl_dir.set_text("(no SD card inserted)");
    } else {
        std::stringstream ss;
        ss << "--- Directory " << curdir << " ---";
        lbl_dir.set_text(ss.str());
    }
    lbl_dir.draw();

    list.set_count(count);
    if (no_dir) {
        list.hide();
    } else {
        list.show();
        if (count) {
            // Select first file, else first dir
            list.select(file_count ? dir_count : 0);
            list.grab_focus();
        } else
            list.select(-1);
    }

    lbl_open.set_shown(!no_dir);
}

void file_view::update_last_err()
{
    auto last_err = model.get_last_err();
    std::stringstream ss;
    if (!last_err.empty()) {
        ss << "Error: " << last_err << std::endl;
    }
    lbl_err.set_text(ss.str());
}

// bool file_view::on_key_down(const keypress_t& kp, int repeat)
// {
//     if (kp.code == kbcode::BAK)
//         model.remove_file();
//     else
//         return fullscreen_view::on_key_down(kp, repeat);
//     return true;
// }

void file_view::on_hide()
{
    // Clear error on view dismissal
    model.clear_last_err();

    fullscreen_view::on_hide();
}

} // namespace main_menu
