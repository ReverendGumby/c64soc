#pragma once

#include "ui/fullscreen_view.h"
#include "ui/list_view.h"
#include "ui/label.h"
#include "../rsn_file.h"

namespace main_menu {

class song_view : public ui::fullscreen_view,
                  private ui::list_view::model
{
public:
    struct model
    {
        using song_item = rsn_file::file_t;
        using song_list = std::vector<song_item>;

        virtual bool get_is_loading() = 0;
        virtual const std::string& get_cur_rsn() = 0;
        virtual const song_list& get_songs() = 0;
        virtual std::string get_last_err() = 0;

        virtual void open_song(const song_item&) = 0;
        virtual void clear_last_err() = 0;
    };

    song_view(model&);

    static constexpr int update_flag_songs = 1<<0;
    static constexpr int update_flag_last_err = 1<<1;

    void update(int flags);

protected:
    // interface for ui::view
    virtual const char* get_class_name() const { return "song_view"; }

    // interface for ui::list_view::model
    virtual std::string get_item(int idx);
    virtual void on_selected_changed(int idx);
    virtual void on_selected(int idx);

private:
    void update_songs();
    void update_last_err();

    void draw_directory(const drawing_context*);

    // interface for ui::fullscreen_view
    //virtual bool on_key_down(const keypress_t& k, int repeat);
    virtual void on_hide();

    model& model;
    int count;                                  // total number of list items

    ui::label lbl_menu, lbl_rsn, lbl_loading;
    ui::list_view list;
    ui::label lbl_err, lbl_open, lbl_exit;
};

} // namespace main_menu
