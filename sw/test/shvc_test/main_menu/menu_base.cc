#include "menu_base.h"

namespace main_menu {

menu_base::menu_base(const char* menu_txt, int header, int footer)
    : list(*this)
{
    int y = header;
    int y_lbl_exit = max_height() - 1;

    lbl_menu.add_to_view(this, menu_txt, 0, y);

    y += 2;
    int h = y_lbl_exit - 1 - footer - y;
    list.set_pos_size(1, y, max_width() - 2, h);
    list.show();
    add_child_view(&list);

    y = y_lbl_exit;
    lbl_exit.add_to_view(this, "[ESC] Return to previous menu", 0, y++);

    list.grab_focus();
}

void menu_base::add_menu_item(const std::string&& s, ui::view* v)
{
    menu_items.push_back({ s, [v](){ if (v) v->show(); } });
}

void menu_base::add_menu_item(const std::string&& s,
                              std::function<void(void)> fn)
{
    menu_items.push_back({ s, fn });
}

void menu_base::update()
{
    int count = menu_items.size();
    list.set_count(count);

    // Keep selection within bounds
    if (list.get_selected() >= count)
        list.select(count - 1);
    else if (count > 0 && list.get_selected() == -1)
        list.select(0);

    list.draw();
}

std::string menu_base::get_item(int idx)
{
    return menu_items.at(idx).first;
}

void menu_base::on_selected_changed(int)
{
}

void menu_base::on_selected(int idx)
{
    auto f = menu_items.at(idx).second;
    if (f)
        f();
}

} // namespace main_menu
