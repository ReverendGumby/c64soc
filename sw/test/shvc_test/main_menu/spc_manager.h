#pragma once

#include "fat/path.h"
#include "sd/sd_manager.h"
#include "file_view.h"
#include <vector>

namespace main_menu {

class spc_manager : private sd::sd_manager::listener,
                    private file_view::model
{
public:
    spc_manager(sd::sd_manager&);

    ui::view* get_view() { return &_file_view; }

private:
    friend file_view;

    void read_dir();
    void read_spc(const fat::path&);

    // interface for file_view::model
    using file_list = file_view::model::file_list;
    using dir_list = file_view::model::dir_list;
    const fat::path& get_curdir() override { return curdir; }
    const dir_list& get_dirs() override { return dirs; }
    virtual const file_list& get_files() { return files; }
    virtual std::string get_last_err() { return last_err; }
    void open_dir(const fat::path&) override;
    virtual void open_file(const fat::path&);
    virtual void clear_last_err();

    // interface for sd::sd_manager::listener
    virtual void mounted();
    virtual void unmounted();

    fat::path curdir;
    dir_list dirs;
    file_list files;
    std::string last_err;
    file_view _file_view;
};

} // namespace main_menu
