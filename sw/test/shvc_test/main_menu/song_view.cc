#include <sstream>

#include "song_view.h"

namespace main_menu {

song_view::song_view(struct model& model)
    : model(model), list(*this)
{
    int y = 0;
    int y_footer = max_height() - 4;

    lbl_menu.add_to_view(this, "Select Song", 0, y);

    y += 2;
    lbl_rsn.add_to_view(this, "", 0, y, max_width());

    y += 2;
    lbl_loading.add_to_view(this, "Loading...", 0, y, max_width());

    int h = y_footer - 1 - y;
    list.set_pos_size(1, y, max_width() - 2, h);
    list.show();
    add_child_view(&list);

    y = y_footer;
    lbl_err.add_to_view(this, "", 0, y, max_width());

    y += 2;
    lbl_open.add_to_view(this, "[RET] Play Song", 0, y++);
    lbl_exit.add_to_view(this, "[ESC] Return to previous menu", 0, y++);

    set_color_bg(color_t::BROWN);

    update(update_flag_songs | update_flag_last_err);
}

void song_view::update(int flags)
{
    if (flags & update_flag_songs)
        update_songs();
    if (flags & update_flag_last_err)
        update_last_err();
}

std::string song_view::get_item(int idx)
{
    if (count == 0 && idx == 0)
        return "(empty)";

    std::stringstream ss;
    const auto& it = model.get_songs().at(idx);
    constexpr int fnw = 28;
    ss << std::left << std::setfill(' ') << std::setw(fnw)
       << it.path.string().substr(0, fnw);
    return ss.str();
}

void song_view::on_selected_changed(int)
{
}

void song_view::on_selected(int idx)
{
    if (idx == -1)
        return;

    const auto& songs = model.get_songs();
    const auto& item = songs[idx];
    model.open_song(item);
}

void song_view::update_songs()
{
    bool is_loading = model.get_is_loading();
    const auto& songs = model.get_songs();

    auto song_count = songs.size();
    count = song_count;

    std::stringstream ss;
    auto curdir = model.get_cur_rsn();
    ss << "--- Archive " << curdir << " ---";
    lbl_rsn.set_text(ss.str());
    lbl_rsn.draw();

    list.set_count(std::max(count, 1));
    if (count) {
        list.select(0);
        list.grab_focus();
    } else {
        list.select(-1);
        list.release_focus();
    }

    if (is_loading) {
        list.hide();
        lbl_loading.show();
    } else {
        lbl_loading.hide();
        list.show();
    }
}

void song_view::update_last_err()
{
    auto last_err = model.get_last_err();
    std::stringstream ss;
    if (!last_err.empty()) {
        ss << "Error: " << last_err << std::endl;
    }
    lbl_err.set_text(ss.str());
}

void song_view::on_hide()
{
    // Clear error on view dismissal
    model.clear_last_err();

    fullscreen_view::on_hide();
}

} // namespace main_menu
