#pragma once

#include <stdint.h>
#include <istream>

struct song_t {
    int duration_sec;
    int fade_ms;

    struct {
        uint16_t pc;
        uint8_t ac, x, y, p, s;
    } smp_regs;
    uint8_t ram[0x10000];
    uint8_t dsp_regs[0x80];
};

uint32_t apu_mon_read(uint8_t a);
void apu_mon_write(uint8_t a, uint32_t v);
void apu_mon_read_until(uint8_t a, uint32_t v, uint32_t m);
void load_dsp_regs(const uint8_t* regs);
void load_ram(const uint8_t* ram);
void stall(bool e);
void load_song(const song_t& song);
song_t* read_song(std::basic_iostream<char>& f);
void load_song(song_t*);
void toggle_pause();
