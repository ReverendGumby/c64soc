#include "snes/emubus.h"
#include "cli/command.h"
#include "util/string.h"
#include "util/dump_to_str.h"
#include <iostream>
#include <memory>
#include "apu.h"

uint32_t apu_mon_read(uint8_t a)
{
    EMUBUS_APU_MON_SEL = a;
    return EMUBUS_APU_MON_DATA;
}

void apu_mon_write(uint8_t a, uint32_t v)
{
    EMUBUS_APU_MON_SEL = a;
    EMUBUS_APU_MON_DATA = v;
}

void apu_mon_read_until(uint8_t a, uint32_t v, uint32_t m)
{
    EMUBUS_APU_MON_SEL = a;
    while ((EMUBUS_APU_MON_DATA & m) != v)
        ;
}

int apu_mon_reg_dump(const cli::command::args_t& args)
{
#define dump(x, lbl)                                            \
    do {                                                        \
        auto v = apu_mon_read(EMUBUS_APU_MON_SEL_##x);          \
        std::cout << lbl " = " <<                               \
            string_printf("0x%08x", v) << "  ";                 \
    } while(0)

    dump(ARC0, "PF,PC");
    dump(ARC1, "Y,X,AC,S");
    std::cout << '\n';
    dump(UAR0, "DOR,AOR,IR");
    dump(STCS, "STCS");
    dump(SPS, "SPS");
    std::cout << '\n';
    dump(CPUI, "CPUI3-0");
    dump(CPUO, "CPUO3-0");
    std::cout << '\n';
    dump(SPR0, "SPT2-0,SPCR");
    std::cout << '\n';
    dump(DTCS, "DTCS");
    dump(DRS, "DRS");
    dump(DRD, "DRD");
    std::cout << '\n';
    dump(SMP1, "SPDRS");
    std::cout << '\n';

#undef dump

    return 0;
}

int apu_dsp_dump(const cli::command::args_t& args)
{
    uint8_t regs[0x80];

    //apu_stall(true);

    for (int i = 0; i < 0x80; i++) {
        apu_mon_write(EMUBUS_APU_MON_SEL_DRS, i);
        regs[i] = apu_mon_read(EMUBUS_APU_MON_SEL_DRD);
    }

    std::cout << "DSP regs:\n";
    for (auto& line : dump_to_str(regs, sizeof(regs), 0))
        std::cout << line << std::endl;

    //apu_stall(false);

    return 0;
}

int apu_mem_display(const cli::command::args_t& args)
{
    auto it = args.begin();
    if (args.end() - it < 1)
        throw std::range_error("missing argument");
    unsigned addr = strtoul(it->c_str(), NULL, 0);
    if (addr >= 0x10000)
        throw std::range_error("addr too large");

    unsigned long len = 1;
    it++;
    if (it < args.end())
        len = strtoul(it->c_str(), NULL, 0);
    if (addr + len - 1 >= 0x10000)
        throw std::range_error("length too long");

    auto data = std::unique_ptr<uint8_t[]>(new uint8_t[len]);

    //apu_stall(true);
    for (unsigned long i = 0; i < len; i++)
        data[i] = EMUBUS_APU_MEM[addr + i];
    //apu_stall(false);

    std::cout << "SPC RAM:\n";
    for (auto& line : dump_to_str(&data[0], len, addr))
        std::cout << line << std::endl;


    return 0;
}

