#pragma once

#include <stdint.h>

struct ppu_pin_status_t {
    bool hblank;
    bool vblank;
};

void ppu_init();
ppu_pin_status_t ppu_get_pin_status();
void ppu_reg_set(uint8_t addr, uint8_t val);
uint16_t ppu_vram_get(uint16_t addr);
void ppu_vram_set(uint16_t addr, uint16_t val);
void ppu_vram_get_ch_clr(int x, int y, int& ch, int& clr);
void ppu_vram_set_ch_clr(int x, int y, int ch, int clr);
void ppu_palette_load_clr(int idx, int clr);
void ppu_vram_dump();
void ppu_cgram_dump();
