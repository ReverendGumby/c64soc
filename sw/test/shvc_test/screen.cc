#include "ppu.h"
#include "screen.h"

// Translate from ASCII to screen code.
uint8_t screen::ascii_to_screen(char c)
{
    constexpr uint8_t unk = 32;

    // Use space (' ') for untranslated ASCII chars. Coincidentally, the
    // screen code and ASCII values for space are identical.
    constexpr uint8_t screen[128] = {
        unk, unk, unk, unk, unk, unk, unk, unk, //  0 -  7
        unk, unk, unk, unk, unk, unk, unk, unk, //  8 - 15
        unk, unk, unk, unk, unk, unk, unk, unk, // 16 - 23
        unk, unk, unk, unk, unk, unk, unk, unk, // 24 - 31
        32,  33,  34,  35,  36,  37,  38,  39,  //  !"#$%&'
        40,  41,  42,  43,  44,  45,  46,  47,  // ()*+,-./
        48,  49,  50,  51,  52,  53,  54,  55,  // 01234567
        56,  57,  58,  59,  60,  61,  62,  63,  // 89:;<=>?
        64,  65,  66,  67,  68,  69,  70,  71,  // @ABCDEFG
        72,  73,  74,  75,  76,  77,  78,  79,  // HIJKLMNO
        80,  81,  82,  83,  84,  85,  86,  87,  // PQRSTUVW
        88,  89,  90,  91,  unk, 93,  94,  95,  // XYZ[\]^_
        96,  97,  98,  99,  100, 101, 102, 103, // `abcdefg
        104, 105, 106, 107, 108, 109, 110, 111, // hijklmno
        112, 113, 114, 115, 116, 117, 118, 119, // pqrstuvw
        120, 121, 122, 123, 124, 125, 126, 127, // xyz{|}~
    };

    if (c >= 0 && c < (0 + sizeof(screen)))
        return screen[c - 0];
    return unk;
}

screen::screen()
{
    init();
}

screen::~screen()
{
}

void screen::set_bg(screen::color_t c)
{
    for (int i = 0; i < 256; i += 16)
        ppu_palette_load_clr(i, static_cast<uint8_t>(c));
}

void screen::set_border(screen::color_t c)
{
}

void screen::init()
{
}

void screen::clear(screen::color_t c)
{
    for (int y = 0; y < HEIGHT; y++)
        for (int x = 0; x < WIDTH; x++)
            write(x, y, 0, c);
}

void screen::write(int x, int y, uint8_t b, screen::color_t c)
{
    ppu_vram_set_ch_clr(x, y, b, static_cast<uint8_t>(c));
}

void screen::invert_rect(int x0, int y0, int w, int h)
{
    for (int y = y0; y < y0 + h; y++)
        for (int x = x0; x < x0 + w; x++) {
            int ch, clr;
            ppu_vram_get_ch_clr(x, y, ch, clr);
            ch ^= (1<<7);
            ppu_vram_set_ch_clr(x, y, ch, clr);
        }
}

