#pragma once

#include <stdint.h>

// Indices for reg_file() REG_WE and REG_RE

// Write-only registers
static constexpr uint8_t PPU_REG_INIDISP  = 0x00;
static constexpr uint8_t PPU_REG_OBSEL    = 0x01;
static constexpr uint8_t PPU_REG_OAMADDL  = 0x02;
static constexpr uint8_t PPU_REG_OAMADDH  = 0x03;
static constexpr uint8_t PPU_REG_OAMDATA  = 0x04;
static constexpr uint8_t PPU_REG_BGMODE   = 0x05;
static constexpr uint8_t PPU_REG_MOSAIC   = 0x06;
static constexpr uint8_t PPU_REG_BG1SC    = 0x07;
static constexpr uint8_t PPU_REG_BG2SC    = 0x08;
static constexpr uint8_t PPU_REG_BG3SC    = 0x09;
static constexpr uint8_t PPU_REG_BG4SC    = 0x0a;
static constexpr uint8_t PPU_REG_BG12NBA  = 0x0b;
static constexpr uint8_t PPU_REG_BG34NBA  = 0x0c;
static constexpr uint8_t PPU_REG_BG1HOFS  = 0x0d;
static constexpr uint8_t PPU_REG_BG1VOFS  = 0x0e;
static constexpr uint8_t PPU_REG_BG2HOFS  = 0x0f;
static constexpr uint8_t PPU_REG_BG2VOFS  = 0x10;
static constexpr uint8_t PPU_REG_BG3HOFS  = 0x11;
static constexpr uint8_t PPU_REG_BG3VOFS  = 0x12;
static constexpr uint8_t PPU_REG_BG4HOFS  = 0x13;
static constexpr uint8_t PPU_REG_BG4VOFS  = 0x14;
static constexpr uint8_t PPU_REG_VMAIN    = 0x15;
static constexpr uint8_t PPU_REG_VMADDL   = 0x16;
static constexpr uint8_t PPU_REG_VMADDH   = 0x17;
static constexpr uint8_t PPU_REG_VMDATAL  = 0x18;
static constexpr uint8_t PPU_REG_VMDATAH  = 0x19;
static constexpr uint8_t PPU_REG_M7SEL    = 0x1a;
static constexpr uint8_t PPU_REG_M7A      = 0x1b;
static constexpr uint8_t PPU_REG_M7B      = 0x1c;
static constexpr uint8_t PPU_REG_M7C      = 0x1d;
static constexpr uint8_t PPU_REG_M7D      = 0x1e;
static constexpr uint8_t PPU_REG_M7X      = 0x1f;
static constexpr uint8_t PPU_REG_M7Y      = 0x20;
static constexpr uint8_t PPU_REG_CGADD    = 0x21;
static constexpr uint8_t PPU_REG_CGDATA   = 0x22;
static constexpr uint8_t PPU_REG_W12SEL   = 0x23;
static constexpr uint8_t PPU_REG_W34SEL   = 0x24;
static constexpr uint8_t PPU_REG_WOBJSEL  = 0x25;
static constexpr uint8_t PPU_REG_WH0      = 0x26;
static constexpr uint8_t PPU_REG_WH1      = 0x27;
static constexpr uint8_t PPU_REG_WH2      = 0x28;
static constexpr uint8_t PPU_REG_WH3      = 0x29;
static constexpr uint8_t PPU_REG_WBGLOG   = 0x2a;
static constexpr uint8_t PPU_REG_WOBJLOG  = 0x2b;
static constexpr uint8_t PPU_REG_TM       = 0x2c;
static constexpr uint8_t PPU_REG_TS       = 0x2d;
static constexpr uint8_t PPU_REG_TMW      = 0x2e;
static constexpr uint8_t PPU_REG_TSW      = 0x2f;
static constexpr uint8_t PPU_REG_CGWSEL   = 0x30;
static constexpr uint8_t PPU_REG_CGADSUB  = 0x31;
static constexpr uint8_t PPU_REG_COLDATA  = 0x32;
static constexpr uint8_t PPU_REG_SETINI   = 0x33;
// Read-only registers
static constexpr uint8_t PPU_REG_MPYL     = 0x34;
static constexpr uint8_t PPU_REG_MPYM     = 0x35;
static constexpr uint8_t PPU_REG_MPYH     = 0x36;
static constexpr uint8_t PPU_REG_SLHV     = 0x37;
static constexpr uint8_t PPU_REG_RDOAM    = 0x38;
static constexpr uint8_t PPU_REG_RDVRAML  = 0x39;
static constexpr uint8_t PPU_REG_RDVRAMH  = 0x3a;
static constexpr uint8_t PPU_REG_RDCGRAM  = 0x3b;
static constexpr uint8_t PPU_REG_OPHCT    = 0x3c;
static constexpr uint8_t PPU_REG_OPVCT    = 0x3d;
static constexpr uint8_t PPU_REG_STAT77   = 0x3e;
static constexpr uint8_t PPU_REG_STAT78   = 0x3f;
