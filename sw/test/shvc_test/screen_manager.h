#pragma once

#include "ui/screen_manager.h"
#include "screen.h"

class screen_manager : public ui::screen_manager
{
public:
    screen_manager(screen&);

private:
    ui::screen* grab_screen();
    void release_screen();

    screen& _screen;
};
