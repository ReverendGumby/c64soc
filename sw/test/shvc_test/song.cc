#include "snes/emubus.h"
#include "apu.h"
#include "song.h"
#include <stdio.h>
#include <thread>

bool paused;

void load_dsp_regs(const uint8_t* regs)
{
    printf("Loading DSP regs\n");

    for (int i = 0; i < 0x80; i++) {
        apu_mon_write(EMUBUS_APU_MON_SEL_DRS, i);
        apu_mon_write(EMUBUS_APU_MON_SEL_DRD, regs[i]);
    }

    for (int i = 0; i < 0x80; i++) {
        apu_mon_write(EMUBUS_APU_MON_SEL_DRS, i);
        auto wanted = regs[i];
        auto got = static_cast<uint8_t>(apu_mon_read(EMUBUS_APU_MON_SEL_DRD));
        if (wanted != got)
            printf("At 0x02%x, wanted 0x02%x, got 0x02%x\n", i, wanted, got);
    }
}

void load_ram(const uint8_t* ram)
{
    printf("Loading RAM\n");

    for (int i = 0; i < 0x10000; i++)
        EMUBUS_APU_MEM[i] = ram[i];

    for (int i = 0; i < 0x10000; i++) {
        auto wanted = ram[i];
        auto got = EMUBUS_APU_MEM[i];
        if (wanted != got)
            printf("At 0x04%x, wanted 0x02%x, got 0x02%x\n", i, wanted, got);
    }
}

void stall(bool e)
{
    if (e) {
        // Stall SMP and DSP
        apu_mon_write(EMUBUS_APU_MON_SEL_STCS, EMUBUS_APU_MON_STCS_STALL);
        apu_mon_write(EMUBUS_APU_MON_SEL_DTCS, EMUBUS_APU_MON_DTCS_STALL);
        apu_mon_read_until(EMUBUS_APU_MON_SEL_STCS,
                           EMUBUS_APU_MON_STCS_STALLED,
                           EMUBUS_APU_MON_STCS_STALLED);
        apu_mon_read_until(EMUBUS_APU_MON_SEL_DTCS,
                           EMUBUS_APU_MON_DTCS_STALLED,
                           EMUBUS_APU_MON_DTCS_STALLED);
    } else {
        // Release DSP stall
        apu_mon_write(EMUBUS_APU_MON_SEL_DTCS, 0);

        // Reset timing state, release SMP stall
        apu_mon_write(EMUBUS_APU_MON_SEL_STCS, EMUBUS_APU_MON_STCS_TS);
    }
}

void load_song(const song_t& song)
{
    EMUBUS_RESETS &= ~EMUBUS_RESETS_EMU;
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    EMUBUS_RESETS |= EMUBUS_RESETS_EMU;
    std::this_thread::sleep_for(std::chrono::milliseconds(1));

    printf("Stalling SMP and DSP\n");
    stall(true);

    // Load DSP registers
    load_dsp_regs(song.dsp_regs);

    // Load RAM
    load_ram(song.ram);

    // Load SMP state
    printf("Loading SMP state\n");
    auto& sr = song.smp_regs;
    apu_mon_write(EMUBUS_APU_MON_SEL_ARC0,
                  (sr.pc << EMUBUS_APU_MON_ARC0_PC_S) |
                  (sr.p * EMUBUS_APU_MON_ARC0_PF_C));
    apu_mon_write(EMUBUS_APU_MON_SEL_ARC1,
                  (sr.s << EMUBUS_APU_MON_ARC1_S_S) |
                  (sr.ac << EMUBUS_APU_MON_ARC1_AC_S) |
                  (sr.x << EMUBUS_APU_MON_ARC1_X_S) |
                  (sr.y << EMUBUS_APU_MON_ARC1_Y_S));
    apu_mon_write(EMUBUS_APU_MON_SEL_UAR0,
                  (0x00 << EMUBUS_APU_MON_UAR0_IR_S));

    // Load SMP peripheral registers
    apu_mon_write(EMUBUS_APU_MON_SEL_CPUI,
                  (song.ram[0xF4] << EMUBUS_APU_MON_CPUI_CPUI0_S) |
                  (song.ram[0xF5] << EMUBUS_APU_MON_CPUI_CPUI1_S) |
                  (song.ram[0xF6] << EMUBUS_APU_MON_CPUI_CPUI2_S) |
                  (song.ram[0xF7] << EMUBUS_APU_MON_CPUI_CPUI3_S));
    apu_mon_write(EMUBUS_APU_MON_SEL_SPR0,
                  (song.ram[0xFA] << EMUBUS_APU_MON_SPR0_SPT0_S) |
                  (song.ram[0xFB] << EMUBUS_APU_MON_SPR0_SPT1_S) |
                  (song.ram[0xFC] << EMUBUS_APU_MON_SPR0_SPT2_S));
    auto spr0 = apu_mon_read(EMUBUS_APU_MON_SEL_SPR0);
    auto ram_spcr = song.ram[0xF1] & ~0x30; // mask out CPUI resets
    spr0 = (spr0 & ~EMUBUS_APU_MON_SPR0_SPCR) |
        (ram_spcr << EMUBUS_APU_MON_SPR0_SPCR_S);
    apu_mon_write(EMUBUS_APU_MON_SEL_SPR0, spr0);

    printf("Releasing SMP and DSP\n");
    stall(false);
    paused = false;
}

song_t* read_song(std::basic_iostream<char>& f)
{
    song_t* psong = new song_t(); // too big to fit on stack
    uint8_t tmp[8];

    auto& sr = psong->smp_regs;
    f.seekg(0x25);
    f.read(reinterpret_cast<char*>(tmp), 2);
    sr.pc = (tmp[1] << 8) | tmp[0];
    f.read(reinterpret_cast<char*>(&sr.ac), 1);
    f.read(reinterpret_cast<char*>(&sr.x), 1);
    f.read(reinterpret_cast<char*>(&sr.y), 1);
    f.read(reinterpret_cast<char*>(&sr.p), 1);
    f.read(reinterpret_cast<char*>(&sr.s), 1);

    f.seekg(0xa9);
    f.read(reinterpret_cast<char*>(tmp), 3);
    tmp[3] = 0;
    psong->duration_sec = atoi((const char*)tmp);
    f.read(reinterpret_cast<char*>(tmp), 5);
    tmp[5] = 0;
    psong->fade_ms = atoi((const char*)tmp);

    f.seekg(0x100);
    f.read(reinterpret_cast<char*>(psong->ram), 0x10000);

    f.read(reinterpret_cast<char*>(psong->dsp_regs), 0x80);

    return psong;
}

void toggle_pause()
{
    paused = !paused;
    stall(paused);
}
