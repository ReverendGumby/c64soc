#include "unrar/unrar/rar.hpp"
#include "unrar/glue.h"
#include "rsn_file.h"
#include <assert.h>

ErrorHandler ErrHandler; // for unrar

std::vector<rsn_file::file_t> rsn_file::get_files(const wchar* ArcName)
{
    std::vector<file_t> ret;

    CommandData Cmd {};
    Cmd.Command[0] = 'P';
    Cmd.MsgStream=MSG_ERRONLY;
    SetConsoleMsgStream(MSG_ERRONLY);
    Cmd.AddArcName(ArcName);
    Cmd.FileArgs.AddString(L"*.spc");

    std::unique_ptr<Archive> pArc {new Archive(&Cmd)};
    Archive& Arc = *pArc;
    assert (Arc.Open(ArcName,FMF_OPENSHARED));
    assert (Arc.IsArchive(true));

    auto& fh = Arc.FileHead;
    std::unique_ptr<filebuf> fb;

    _unrar_filebuf_on_open = [&]() {
        fb.reset(new filebuf(fh.UnpSize));
        return fb.get();
    };
    _unrar_filebuf_on_close = [&]() {
        file_t file;
        file.path = fat::path(fh.FileName);
        file.size = fh.UnpSize;
        file.buf = std::move(fb);
        ret.push_back(std::move(file));
    };

    CmdExtract Extract(&Cmd);
    Extract.ExtractArchiveInit(Arc);
    while (1) {
        size_t Size = Arc.ReadHeader();
        bool Repeat = false;
        if (!Extract.ExtractCurrentFile(Arc, Size, Repeat))
            break;
    }

    Arc.Close();

    return ret;
}

fstream rsn_file::extract_file(const file_t& file)
{
    auto* fb = file.buf.get();
    assert(fb);
    assert(fb->is_full());
    fb->reset();
    return fstream(fb);
}

