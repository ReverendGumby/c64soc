#pragma once

#include <stdint.h>
#include "cli/command.h"

uint32_t apu_mon_read(uint8_t a);
void apu_mon_write(uint8_t a, uint32_t v);
void apu_mon_read_until(uint8_t a, uint32_t v, uint32_t m);

int apu_mon_reg_dump(const cli::command::args_t& args);
int apu_dsp_dump(const cli::command::args_t& args);
int apu_mem_display(const cli::command::args_t& args);
