#include "ppu.h"
#include "ppu_reg.h"
#include "snes/emubus.h"
#include <stdio.h>

static bool render_en = false;

ppu_pin_status_t ppu_get_pin_status()
{
    auto v = EMUBUS_PPU_STATUS;
    return ppu_pin_status_t {
        .hblank = (v & EMUBUS_PPU_STATUS_HBLANK) != 0,
        .vblank = (v & EMUBUS_PPU_STATUS_VBLANK) != 0,
    };
}

uint8_t ppu_reg_get(uint8_t addr)
{
    EMUBUS_IO_ADDR = addr;
    return EMUBUS_IO_DATA;
}

void ppu_reg_set(uint8_t addr, uint8_t val)
{
    EMUBUS_IO_ADDR = addr;
    EMUBUS_IO_DATA = val;
}

void ppu_render_set(bool e)
{
    if (render_en != e) {
        render_en = e;
        ppu_reg_set(PPU_REG_INIDISP, e ? 0x0F : 0x80);
    }
}

#if 0
// Set force blanking to access VRAM
struct render_pause
{
    render_pause()
    {
        old_render_en = render_en;
        ppu_render_set(false);
    }
    ~render_pause()
    {
        ppu_render_set(old_render_en);
    }
    bool old_render_en;
};
#else
// Wait for V-Blank to access VRAM
struct render_pause
{
    render_pause()
    {
        if (render_en) {
            for (;;) {
                auto s = ppu_get_pin_status();
                if (s.vblank && !s.hblank)
                    break;
            }
            // Even if we're on the last row of V-Blank, we still have at least
            // one H-Blank to get the job done. Go fast. Think about disabling
            // interrupts if needed.
        }
    }
};
#endif

uint16_t ppu_vram_get(uint16_t addr)
{
    ppu_reg_set(PPU_REG_VMAIN, 0x80);
    ppu_reg_set(PPU_REG_VMADDL, (addr & 0xff));
    ppu_reg_set(PPU_REG_VMADDH, ((addr >> 8) & 0xff));
    auto h = ppu_reg_get(PPU_REG_RDVRAMH);
    auto l = ppu_reg_get(PPU_REG_RDVRAML);
    return (uint16_t(h) << 8) | l;
}

void ppu_vram_set(uint16_t addr, uint16_t val)
{
    ppu_reg_set(PPU_REG_VMAIN, 0x80);
    ppu_reg_set(PPU_REG_VMADDL, (addr & 0xff));
    ppu_reg_set(PPU_REG_VMADDH, ((addr >> 8) & 0xff));
    ppu_reg_set(PPU_REG_VMDATAL, (val & 0xff));
    ppu_reg_set(PPU_REG_VMDATAH, ((val >> 8) & 0xff));
}

void ppu_vram_get_ch_clr(int x, int y, int& ch, int& clr)
{
    unsigned a = 0x6000 + y * 32 + x;
    render_pause rp;
    uint16_t v = ppu_vram_get(a);
    ch = v & 0xff;
    clr = ((v >> 8) & 1) | (((v >> 10) & 7) << 1);
}

void ppu_vram_set_ch_clr(int x, int y, int ch, int clr)
{
    unsigned a = 0x6000 + y * 32 + x;
    uint16_t v = ch | ((clr & 1) << 8) | ((clr >> 1) << 10);
    //printf("ppu_vram_set_ch_clr(%x, %x)\n", a, v);
    render_pause rp;
    ppu_vram_set(a, v);
}

void ppu_palette_load_clr(int idx, int clr)
{
    static const struct {
        uint8_t r, g, b;
    } c64_colors[16] = {
        {   0,   0,   0 }, // black
        { 255, 255, 255 }, // white
        { 136,   0,   0 }, // red
        { 170, 255, 238 }, // cyan
        { 204,  68, 204 }, // violet
        {   0, 204,  85 }, // green
        {   0,   0, 170 }, // blue
        { 238, 238, 119 }, // yellow
        { 221, 136,  85 }, // orange
        { 102,  68,   0 }, // brown
        { 255, 119, 119 }, // light red
        {  51,  51,  51 }, // gray 1
        { 119, 119, 119 }, // gray 2
        { 170, 255, 102 }, // light green
        {   0, 136, 255 }, // light blue
        { 187, 187, 187 }, // gray 3
    };

    const auto& c = c64_colors[clr];
    uint16_t cv =
        (uint16_t(c.r >> 3) << 0) |
        (uint16_t(c.g >> 3) << 5) |
        (uint16_t(c.b >> 3) << 10);

    render_pause rp;
    ppu_reg_set(PPU_REG_CGADD, idx);
    ppu_reg_set(PPU_REG_CGDATA, cv >> 0);
    ppu_reg_set(PPU_REG_CGDATA, cv >> 8);
}

#if 0
static void init_vram()
{
    for (int i = 0; i < 0x8000; i++)
        ppu_vram_set(i, 0);
}
#endif

// Tile / palette allocation:
//
// Screen tile:
//   bits 6:0   = character 0 - 127
//   bit  7     = reverse video
//   bit  8     = color bit 0
//   bits 12:10 = color bits 3:1
//
// Palette index:
//   (n * 16) + 0 = BG color
//   (n * 16) + 1 = color (n * 2) + 0
//   (n * 16) + 2 = color (n * 2) + 1

static void load_charset()
{
    static const uint8_t charset[] = {
#include "charset.h"
    };

    for (int n = 0; n < 4; n++) {
        int rev = n & 1;
        bool c0 = n & 2;
        for (int c = 0; c < 128; c++) {
            for (int y = 0; y < 8; y++) {
                unsigned src = c * 0x10 + y * 2;
                uint8_t a = charset[src];
                if (rev)
                    a = ~a;
                // Convert 2bpp to 4bpp, in 1 of 2 colors
                uint16_t b = a;
                if (c0)
                    b <<= 8;
                unsigned dst = 0x0 + (n * 128 + c) * 0x10 + y;
                ppu_vram_set(dst + 0x0, b);
                ppu_vram_set(dst + 0x8, 0);
            }
        }
    }
}

static void load_palette()
{
    constexpr int initial_bg = 0;

    for (int i = 0; i < 8; i++) {
        for (int x = 0; x < 3; x++) {
            int clr = (i << 1) | (x == 2);
            int dst = i * 16 + x;
            clr = (x == 0) ? initial_bg : clr;
            ppu_palette_load_clr(dst, clr);
        }
    }

    if (0) {
        for (int i = 0; i < 256; i += 8) {
            printf("%02x:", i);
            for (int j = i; j < i + 8; j++) {
                ppu_reg_set(PPU_REG_CGADD, j);
                auto c1 = ppu_reg_get(PPU_REG_RDCGRAM);
                auto c2 = ppu_reg_get(PPU_REG_RDCGRAM) & 0x7F;
                printf(" %02x%02x", c2, c1);
            }
            printf("\n");
        }
    }
}

static void init_screen()
{
    int ch = 0x20;
    int clr = 1;
    bool rev = false;
    for (int y = 0; y < 32; y++) {
        for (int x = 0; x < 32; x++) {
            int rch = ch | (rev << 7);
            ppu_vram_set_ch_clr(x, y, rch, clr);

            if (++clr == 16)
                clr = 1;
            if (++ch == 0x80) {
                ch = 0x20;
                rev = !rev;
            }
        }
    }
}

void ppu_init()
{
    //init_vram();
    load_charset();
    load_palette();
    init_screen();

    ppu_reg_set(PPU_REG_BGMODE, 0x01);
    ppu_reg_set(PPU_REG_BG1SC, 0x60);
    ppu_reg_set(PPU_REG_BG12NBA, 0x00);
    ppu_reg_set(PPU_REG_BG1VOFS, 0xFF);
    ppu_reg_set(PPU_REG_BG1VOFS, 0x03);
    ppu_reg_set(PPU_REG_TM, 0x01);

    // Enable rendering
    ppu_render_set(true);
}

void ppu_vram_dump()
{
    for (int a = 0x6000; a < 0x7000; a += 16) {
        printf("%04x:", a);
        for (int b = a; b < a + 16; b++) {
            render_pause rp;
            uint16_t v = ppu_vram_get(b);
            printf(" %04x", v);
        }
        printf("\n");
    }
}

void ppu_cgram_dump()
{
    for (int i = 0; i < 256; i += 16) {
        printf("%02x:", i);
        for (int j = i; j < i + 16; j++) {
            render_pause rp;
            ppu_reg_set(PPU_REG_CGADD, j);
            auto c1 = ppu_reg_get(PPU_REG_RDCGRAM);
            auto c2 = ppu_reg_get(PPU_REG_RDCGRAM) & 0x7F;
            printf(" %02x%02x", c2, c1);
        }
        printf("\n");
    }
}
