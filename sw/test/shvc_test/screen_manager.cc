#include "screen_manager.h"
#include "ui/fullscreen_view.h"

screen_manager::screen_manager(screen& scr)
    : _screen(scr)
{
    ui::fullscreen_view::set_size(screen::WIDTH, screen::HEIGHT);
}

ui::screen* screen_manager::grab_screen()
{
    return &_screen;
}

void screen_manager::release_screen()
{
}
