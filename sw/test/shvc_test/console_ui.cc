#include "fat/directory_iterator.h"
#include "fat/path.h"
#include "fat/filesystem.h"
#include "console_ui.h"
#include "rsn_file.h"
#include "song.h"

console_ui::console_ui()
{
    con = static_cast<decltype(con)>(os::dev::device::find("console"));
    ready = false;

    pollable_add(this);
}

void console_ui::load_songlist(const fat::path& path)
{
    rsn_file rsn;
    songlist.clear();
    songlist = rsn.get_files(path.native().c_str());

    cur_song = songlist.begin();
}

void console_ui::show_song(int dir)
{
    if (dir < 0) {
        if (cur_song != songlist.begin())
            --cur_song;
    } else {
        if ((cur_song + 1) != songlist.end())
            ++cur_song;
    }
    const auto& path = cur_song->path;
    auto fn = path.string();
    printf("Next song: %s\n", fn.c_str());
}

static void read_and_load_song(const rsn_file::file_t& item)
{
    const auto& path = item.path;
    auto fn = path.string();
    printf("Reading song %s\n", fn.c_str());

    song_t* song;
    {
        rsn_file rsn;
        auto f = rsn.extract_file(item);
        song = read_song(f);
    }

    load_song(*song);
    delete song;
}

void console_ui::read_and_load_rsn()
{
    const auto& path = *cur_rsn;
    auto fn = path.string();
    printf("Reading archive %s\n", fn.c_str());

    load_songlist(path);

    read_and_load_song(*cur_song);
}

void console_ui::show_rsn(int dir)
{
    if (dir < 0) {
        if (cur_rsn != rsnlist.begin())
            --cur_rsn;
    } else {
        if ((cur_rsn + 1) != rsnlist.end())
            ++cur_rsn;
    }
    const auto& path = *cur_rsn;
    auto fn = path.string();
    printf("Next rsn: %s\n", fn.c_str());
}

void console_ui::check_console()
{
    if (con->is_rx_empty())
        return;
    char c = con->rx();
    switch (c) {
    case 'P':
        show_rsn(-1);
        break;
    case 'N':
        show_rsn(1);
        break;
    case '\r':
        read_and_load_rsn();
        break;
    case 'p':
        show_song(-1);
        break;
    case 'n':
        show_song(1);
        break;
    case ' ':
        read_and_load_song(*cur_song);
        break;
    case 'c':
        toggle_pause();
    default:
        break;
    }
}

void console_ui::load_rsnlist()
{
    const char* pn = "\\soundtracks";
    auto dir = fat::path(pn);

    for (const auto& de : fat::directory_iterator{dir}) {
        const auto& name = de.name;
        if (!de.is_hidden && name.extension() == L".rsn")
            rsnlist.push_back(dir / name);
    }

    cur_rsn = rsnlist.begin();
}

void console_ui::poll()
{
    if (fat::is_mounted() && !ready) {
        ready = true;
        load_rsnlist();
        read_and_load_rsn();
    }

    if (ready) {
        check_console();
    }
}
