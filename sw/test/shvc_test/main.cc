/*
 * shvc_test/main.cc: SHVC test
*/

#include <chrono>
#include <thread>
#include "plat/audio.h"
#include "plat/manager.h"
#include "app/version.h"
#include "sd/sd_manager.h"
#include "usb/usb_manager.h"
#include "ui/hid_manager.h"
#include "cli/manager.h"
#include "cli/command.h"
#include "ui/view_manager.h"
#include "apu.h"
#include "console_ui.h"
#include "ppu.h"
#include "screen.h"
#include "screen_manager.h"
#include "main_menu/top_menu.h"
#include "main_menu/rsn_manager.h"
#include "main_menu/spc_manager.h"

//#define CONSOLE_UI

static const plat::audio_config_t audio_cfg = {
    .sample_rate = 32000,
};
static const plat::manager::config_t plat_cfg = {
    .audio = audio_cfg,
};

int main()
{
    plat::manager plm(plat_cfg);
    plm.early_init();

    printf("SHVC test\n"
           "SW version %s (@%s) build %s\n",
           sw_build_branch, sw_build_rev, sw_build_timestamp);

    plm.late_init();

    auto hw_ver = plm.get_rtl_version();
    printf("HW version %s (@%s) build %s\n",
           hw_ver.git_branch.c_str(), hw_ver.git_rev.c_str(), hw_ver.timestamp.c_str());

#ifndef CONSOLE_UI
    usb::usb_manager um;
    um.init();
#endif

    sd::sd_manager sm;

    plm.reset_video(false);
    plm.audio_up();

    ui::hid_manager hid_mgr{};
#ifndef CONSOLE_UI
    hid_mgr.listen(um);

    cli::manager clim;
    if (sm.is_card_inserted())
        clim.try_autoexec();
    clim.start();

    cli::command::define("pvd", [](const cli::command::args_t& args) {
        ppu_vram_dump(); return 0; });
    cli::command::define("pcd", [](const cli::command::args_t& args) {
        ppu_cgram_dump(); return 0; });

    cli::command::define("amrd", [](const cli::command::args_t& args) {
        return apu_mon_reg_dump(args); });
    cli::command::define("add", [](const cli::command::args_t& args) {
        return apu_dsp_dump(args); });
    cli::command::define("amd", [](const cli::command::args_t& args) {
        return apu_mem_display(args); });
#endif

#ifdef CONSOLE_UI
    console_ui cui;
#else
    ppu_init();
    screen scr;
    screen_manager screen_mgr(scr);
    ui::view_manager view_mgr(hid_mgr, screen_mgr);
    main_menu::rsn_manager rsn_mgr(sm);
    main_menu::spc_manager spc_mgr(sm);
    main_menu::top_menu _top_menu(plm, rsn_mgr, spc_mgr);
    _top_menu.show();
#endif

    for (;;) {
        poll_manager_get().poll();

        plm.set_video_hdmi_mode(plm.get_sw(0)); // XXX
        pthread_yield();
    }

    //for (;;)
    //    std::this_thread::sleep_for(std::chrono::seconds(10));

    return 0;
}
