#include <fstream>
#include <iostream>
#include <iomanip>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/time.h>
#include "util/iovec.h"
#include "util/ostream_fmt_save.h"
#include "util/string.h"
#include "fat/filesystem.h"
#include "fat/fat32.h"
#include "fat/directory_iterator.h"
#include "fat/lru_block_cache.h"
#include "fat/block_device.h"
#include "fat/partitioner.h"
#include "fat/partition.h"
#include "fat/fstream.h"
#include "util.h"

#define tassert(cond)                                                   \
    do {                                                                \
        if (!(cond)) {                                                  \
            cout << "assertion \"" << #cond << "\" failed: line "       \
                 << __LINE__ << "\n";                                   \
            fs.write_on_unmount();                                      \
            fs.unmount();                                               \
            exit(1);                                                    \
        }                                                               \
    } while (0)

#define PROFILE_START()                         \
    long __ps = get_time_us()

#define PROFILE_END()                                                   \
    long __pd = get_time_us() - __ps;                                   \
    cout << "% PROFILE: line " << __LINE__ << ": " << __pd << " us\n"

using namespace std;
using namespace fat;

struct {
    int debug_start = -1;
    int debug_end = -1;
    const char* run_this_test = nullptr;
    bool run_all_tests = false;
    bool writethru = false;
} flags;

long get_time_us()
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return (long)(tp.tv_sec * 1e6) + tp.tv_usec;
}

class test_block_device : public block_device
{
public:
    static void load_fsbin() {
        if (orig_bin)
            return;

        std::fstream fsbin { "test-fs.bin", std::fstream::in | std::fstream::out };
        if (fsbin.fail()) {
            cout << "test block device file not found\n";
            exit(1);
        }
        fsbin.seekg(0, fsbin.end);
        orig_bin_size = fsbin.tellg();

        orig_bin = (char *)malloc(orig_bin_size);
        fsbin.seekg(0);
        fsbin.read(orig_bin, orig_bin_size);
    }

    test_block_device()
    {
        //PROFILE_START();
        load_fsbin();
        _bin = (char*)malloc(orig_bin_size);
        memcpy(_bin, orig_bin, orig_bin_size);
        _num_sectors = orig_bin_size / fat::sector_size;
        //PROFILE_END();
    }
    virtual ~test_block_device()
    {
        cout << "~test_block_device()\n";
    }

    virtual sector get_capacity() const override { return _num_sectors; }
    virtual unsigned get_sectors_per_block() const override { return _sectors_per_block; }
    virtual block_buffer_t read_block(sector s) override
    {
        ostream_fmt_save ofs{fat32::debug_stream()};
        auto* buf = new test_buffer{s};
        size_t p = (size_t)s * fat::sector_size;
        memcpy((char*)(buf->ptr), &_bin[p], buf->len);
        //dump(cout, buf->ptr, buf->len);
        return block_buffer_t{buf};
    }
    virtual void write_block(sector s, const block_buffer_t& bb) override
    {
        fat32::debug_stream() << string_printf("test_block_device::write_block(sector=0x%x):\n", s);
        ostream_fmt_save ofs{fat32::debug_stream()};
        dump(fat32::debug_stream(), (uint8_t*)bb->ptr, bb->len);
        size_t p = (size_t)s * fat::sector_size;
        memcpy(&_bin[p], (char*)(bb->ptr), bb->len);
    }
    virtual void flush_writes() override
    {
        fat32::debug_stream() << "test_block_device::flush_writes()\n";
    }

    void write_bin()
    {
        std::fstream fsbin { "test-fs.new.bin", std::fstream::out };
        if (fsbin.fail()) {
            cout << "unable to open test block device file for write\n";
        } else {
            fsbin.write(_bin, orig_bin_size);
        }
    }

private:
    struct test_buffer : public buffer
    {
        sector _sector;
        test_buffer(sector s)
            : buffer(), _sector(s)
        {
            ostream_fmt_save ofs {fat32::debug_stream()};
            fat32::debug_stream()
                << hex << showbase
                << "test_buffer(" << _sector
                << ")\n";
            len = _block_size;
            ptr = malloc(len);
        }

        virtual ~test_buffer()
        {
            ostream_fmt_save ofs {fat32::debug_stream()};
            fat32::debug_stream()
                << hex << showbase
                << "~test_buffer(" << _sector
                << ")\n";
            ::free(ptr);
        }
    };

    static char* orig_bin;
    static size_t orig_bin_size;
    char* _bin;
    fat::sector _num_sectors;
    static constexpr unsigned _sectors_per_block = 2;
    static constexpr unsigned _block_size = _sectors_per_block * fat::sector_size;

    void copy_file(const char* fn_src, const char* fn_dst)
    {
        int src = open(fn_src, O_RDONLY, 0);
        int dst = open(fn_dst, O_WRONLY, O_CREAT | O_TRUNC);
        assert(src != -1 && dst != -1);

        char buf[1 << 15];
        for (;;) {
            ssize_t cnt = read(src, buf, sizeof(buf));
            assert(cnt >= 0);
            if (cnt == 0)
                break;
            write(dst, buf, cnt);
        }

        close(dst);
        close(src);
    }
};

char* test_block_device::orig_bin = nullptr;
size_t test_block_device::orig_bin_size;

class test_filesystem {
public:
    virtual ~test_filesystem()
    {
        unmount();
    }

    void mount()
    {
        if (_fs)
            unmount();

        _tb = std::shared_ptr<test_block_device> { new test_block_device() };
        _bc = std::shared_ptr<fat::lru_block_cache> { new fat::lru_block_cache{_tb, 4} };
        _bc->set_writethru(flags.writethru);

        partitioner pr {};
        pr.load(_bc);
        const std::vector<partitioner::part_dev>& parts = pr.partitions();
        assert(parts.size() == 1);
        const partitioner::part_dev& pd{parts[0]};
        const auto& part = pd.part;
        assert(part._type == partition::type::fat32);
        std::shared_ptr<fat::block_device> fs_dev = pd.dev;
        // sector part_sectors = part._last - part._first + 1;
        // unsigned long part_bytes = part_sectors * 512;
        // cout << "fat32 partition starts at sector " << part._first
        //      << ", spans " << part_sectors << " sectors ("
        //      << part_bytes / (1024 * 1024) << " Mbytes)\n";

        _fs = fat32::mount(fs_dev);
        cout << "mounted #" << _mount_cnt << "\n";

        if (flags.debug_start == _mount_cnt)
            fat32::set_debug_stream(&cout);
    }

    void unmount()
    {
        if (_fs)
        {
            bool debug_end = flags.debug_end == _mount_cnt;
            if (debug_end)
                fat32::set_debug_stream(nullptr);

            _fs->unmount();
            cout << "unmounted\n";
            _fs.reset();

            _bc.reset();

            if (debug_end || _write_on_unmount)
                _tb->write_bin();
            _tb.reset();

            _mount_cnt ++;
        }
    }

    void write_on_unmount()
    {
        _write_on_unmount = true;
    }

private:
    std::shared_ptr<test_block_device> _tb;
    std::shared_ptr<fat::lru_block_cache> _bc;
    std::shared_ptr<fat::fat32> _fs;
    int _mount_cnt = 0;
    bool _write_on_unmount = false;
};

test_filesystem fs;

void read_dir(const path& p)
{
    for (const auto& de : directory_iterator{p})
    {
        cout << '[' << de.idx << "]: "
             << (de.is_dir ? 'D' : 'F')
             << (de.is_hidden ? 'H' : ' ') << ' '
             << de.name
             << " size=" << de.size
             << '\n';
    }
}

bool test_exists(const path& p)
{
    bool found = exists(p);
    cout << "exists(" << p << "): " << found << "\n";
    return found;
}

void read_file(const path& p)
{
    tassert(test_exists(p));
    file f = open(p, omode::r);
    unsigned total_read = 0;
    while (!f.eof())
    {
        uint8_t buf[0x400];
        fileoff where = f.tell();
        unsigned read = f.read(buf, sizeof(buf));
        tassert(read != 0);
        total_read += read;
        dump(cout, buf, read, (unsigned)where);
    }
    uint8_t buf;
    tassert(f.read(&buf, 1) == 0);
    tassert(total_read == 79);
}

void read_iovec_file(const path& p)
{
    tassert(test_exists(p));
    file f{p};
    iovec iov;
    unsigned total_want = f.size();
    cout << "read_iovec_file: reading " << total_want << "...\n";
    unsigned total = 0;
    while (!f.eof())
    {
        unsigned read = f.read(iov, 0x777);
        tassert(read != 0);
        fat32::debug_stream() << "read_iovec_file: { ";
        for (auto& p : iov) {
            auto& iob = *p;
            fat32::debug_stream() << typeid(iob).name() << "(" << iob.ptr << ", " << iob.len << ") ";
        }
        fat32::debug_stream() << "}\n";
        total += read;
    }
    cout << "read_iovec_file: total read: " << total << "\n";
    tassert(total_want == total);
    uint8_t buf;
    tassert(f.read(&buf, 1) == 0);
}

void stream_file_in(const path& p)
{
    cout << "stream_file_in: getline()...\n```\n";
    fat::fstream in{p};
    for (std::string line; getline(in, line); )
        cout << line << '\n';
    cout << "```\n";
    tassert(in.eof());
    (void)in.tellg();
    tassert(in.fail());
    in.clear();
    tassert(!in.fail());

    char c[3] = { '?', '?', '?' };
    in.seekg(0);
    tassert(!in.eof());
    in.get(c[0]);
    tassert(!in.fail());
    cout << "first char: '" << c[0] << "'\n";

    in.seekg(0, std::ios::cur);
    tassert(!in.eof());
    in.get(c[0]).get(c[1]).get(c[2]);
    tassert(!in.fail());
    cout << "second - fourth chars: \"" << c[0] << c[1] << c[2] << "\"\n";

    in.seekg(-1, std::ios::end);
    tassert(!in.eof());
    in.get(c[0]);
    tassert(!in.fail());
    cout << "last char: \'" << c[0] << "\'\n";
    in.unget();
    in.unget();
    tassert(!in.eof());
    in.get(c[0]);
    tassert(!in.fail());
    cout << "last-1 char: \'" << c[0] << "\'\n";
    in.get(c[0]);
    tassert(!in.eof());
    in.get(c[0]);
    tassert(in.eof());
    tassert(in.fail());
}

void read_stuff()
{
    tassert(exists(path{L"\\foo.crt"}) == false);
    path root{L"\\"};
    read_dir(root);
    path cfg{root / path{L"Wizard of Wor.crt.cfg"}};
    read_file(cfg);
    stream_file_in(cfg);
    path bootbin{root / path{L"BOOT.BIN"}};
    read_iovec_file(bootbin);
}

void create_remove_files()
{
    for (const wchar_t* sp : {
            L"\\TEMP.TXT",
            L"\\temp.txt",
            L"\\TEMP.txt",
            L"\\te mp.txt",
            L"\\Temp.Txt",
            L"\\.temp.txt",
            L"\\temporary.txtfile",
        }) {
        path p { sp };
        directory pd { p.parent_path() };
        auto cnt = pd.size();
        tassert(exists(p) == false);
        {
            file f = open(p, omode::w, oflags::create);
        }
        tassert(test_exists(p) == true);
        tassert(pd.size() == cnt + 1);
        tassert(remove(p) == true);
        tassert(exists(p) == false);
        tassert(pd.size() == cnt);
    }
}

void test_shortname_dup()
{
    // Attempt to create a shortname conflict
    path p1 { L"\\ghostbusters.crt" };
    path p2 { L"\\ghostbusting.crt" };

    auto de1 = resolve_path(p1);
    tassert(exists(p2) == false);
    {
        file f = open(p2, omode::w, oflags::create);
    }
    auto de2 = resolve_path(p2);
    tassert(de1.shortname != de2.shortname);
}

void write_new_file()
{
    path p { L"\\BLISSEY.TXT" };
    const char data[] = "Let me tell you about the story of EGGS.\n";
    const size_t len = sizeof(data);

    {
        file f = open(p, omode::w, oflags::create);
        f.write(data, len);
    }
    {
        file f { p };
        tassert(f.size() == len);
        char buf[len];
        auto actual = f.read(buf, len);
        cout << "Read " << actual << " bytes\n";
        tassert(actual == len);
        tassert(memcmp(buf, data, len) == 0);
    }
}

void stream_file_out()
{
    path p { L"\\Arcade Pit Easter 2024 Round 1.txt" };
    const char data[] = "Blue Team\n"
        "In loving memory of Chunky Kong\n"
        "TieTuesday & Tinahacks\n";
    const size_t len = sizeof(data) - 1; // skip NUL-terminator

    {
        // Write file backwards, one byte at a time.
        fat::fstream out{p, ios::out | ios::trunc};
        char tmp[len] = {};
        out.write(tmp, len);
        tassert(out.tellp() == len);

        for (size_t i = len; i; i--) {
            size_t j = i - 1;
            out.seekp(j);
            tassert(size_t(out.tellp()) == j);
            out.put(data[j]);
        }
        out.seekp(len);
        tassert(size_t(out.tellp()) == len);
    }
    {
        // Read and verify
        file f { p };
        tassert(f.size() == len);
        char buf[len];
        auto actual = f.read(buf, len);
        cout << "Read " << actual << " bytes\n";
        tassert(actual == len);
        tassert(memcmp(buf, data, len) == 0);
    }
    {
        // Write file forwards, one byte at a time.
        fat::fstream out{p, ios::out | ios::trunc};

        for (size_t i = 0; i < len; i++) {
            tassert(size_t(out.tellp()) == i);
            out << data[i];
        }
        tassert(size_t(out.tellp()) == len);
    }
    {
        // Read and verify
        file f { p };
        tassert(f.size() == len);
        char buf[len];
        auto actual = f.read(buf, len);
        cout << "Read " << actual << " bytes\n";
        tassert(actual == len);
        tassert(memcmp(buf, data, len) == 0);
    }
    {
        // Write file many times, forcing at least one filebuf overflow.
        fat::fstream out{p, ios::out | ios::trunc};

        for (size_t j = 0; j < 50; j++) {
            out << data;
            tassert(size_t(out.tellp()) == j * len + len);
        }
    }
    {
        // Read and verify
        file f { p };
        tassert(f.size() == 50 * len);
        for (size_t j = 0; j < 50; j++) {
            char buf[len];
            auto actual = f.read(buf, len);
            tassert(actual == len);
            tassert(memcmp(buf, data, len) == 0);
        }
    }
}

void append_file()
{
    path p { L"\\BLISSEY is a Pokemon.TXT" };
    const char data[] = "Let me tell you about the story of EGGS.\n";
    const size_t len = sizeof(data);

    {
        // Create a new file
        file f = open(p, omode::w, oflags::create);
        f.write(data, len);
    }
    {
        // Append to that file
        file f = open(p, omode::w);
        f.seek(0, seekfrom::end);
        f.write(data, len);
    }
    {
        // Read and verify
        file f { p };
        tassert(f.size() == len * 2);
        for (int i = 0; i < 2; i++) {
            char buf[len];
            auto actual = f.read(buf, len);
            cout << "Read " << actual << " bytes\n";
            tassert(actual == len);
            tassert(memcmp(buf, data, len) == 0);
        }
    }
}

void truncate_file()
{
    path p { L"\\BLISSEY is a rockstar of a Pokemon.TXT" };

    {
        // Create a new file
        file f = open(p, omode::w, oflags::create);
        const char data[] = "Let me tell you about the story of EGGS.\n";
        const size_t len = sizeof(data);
        f.write(data, len);
    }
    const char data[] = "Chat was so attached.\n";
    const size_t len = sizeof(data);
    {
        // Truncate that file and write shorter data to it
        file f = open(p, omode::w, oflags::truncate);
        f.write(data, len);
    }
    {
        // Read and verify
        file f { p };
        tassert(f.size() == len);
        char buf[len];
        auto actual = f.read(buf, len);
        cout << "Read " << actual << " bytes\n";
        tassert(actual == len);
        tassert(memcmp(buf, data, len) == 0);
    }
}

void grow_directory()
{
    // Enough files to cause the root directory to overflow one cluster (given
    // how much is already in the directory), without exceeding our 3-digit
    // naming convention.
    const int num_files = 1000;

    // Create a bunch of new files
    cout << "Creating " << num_files << " files...\n";
    for (int fnum = 0; fnum < num_files; fnum++) {
        path p { string_printf("\\GROWTEST.%03d", fnum) };
        open(p, omode::w, oflags::create);
    }
    // Verify they all exist
    cout << "Verifying " << num_files << " files...\n";
    for (int fnum = 0; fnum < num_files; fnum++) {
        path p { string_printf("\\GROWTEST.%03d", fnum) };
        tassert(exists(p));
    }
}

void grow_truncate_files()
{
    // Create a bunch of files. Truncate and make them larger in turn. Repeat.
    const int num_files = 10;
    const int block_len = 11111;
    const int grow_loops = 50;

    std::unique_ptr<char[]> pblock { new char[block_len] };
    std::unique_ptr<char[]> prbuf { new char[block_len] };
    char* block = pblock.get();
    char* rbuf = prbuf.get();

    for (int m = 1; m <= grow_loops; m ++)
    {
        size_t len = m * block_len;
        cout << "Writing " << num_files << " files of " << len << " bytes...\n";
        for (int fnum = 0; fnum < num_files; fnum++) {
            path p { string_printf("\\GROWTEST.%03d", fnum) };
            fat::fstream out(p, ios::out | ios::trunc);
            for (int j = 0; j < m; j++) {
                memset(block, char(j), block_len);
                out.write(block, block_len);
            }
        }
    }
    // Verify they all have the correct contents.
    cout << "Verifying " << num_files << " files...\n";
    for (int fnum = 0; fnum < num_files; fnum++) {
        path p { string_printf("\\GROWTEST.%03d", fnum) };
        fat::fstream in(p, ios::in);
        for (int j = 0; j < grow_loops; j++) {
            memset(block, char(j), block_len);
            in.read(rbuf, block_len);
            tassert(memcmp(rbuf, block, block_len) == 0);
        }
    }
}

void clear_root_files()
{
    // Clear all files out of the root directory
    vector<path> files;
    auto root = directory::root();
    for (auto de : directory_iterator(root))
        if (!de.is_dir)
            files.push_back(de.name);
    for (auto f : files) {
        root.remove(f);
        tassert(!exists(path{L"\\"} / f));
    }
    for (auto de : directory_iterator(root))
        cout << de.name << '\n';
}

#define TEST_FN_NAME(fn) fn, #fn

const struct testdef_t {
    void (*fn)();
    const char* name;
    bool disable;
} tests[] = {
    { TEST_FN_NAME(read_stuff), false },
    { TEST_FN_NAME(create_remove_files), false },
    { TEST_FN_NAME(test_shortname_dup), false },
    { TEST_FN_NAME(write_new_file), false },
    { TEST_FN_NAME(stream_file_out), false },
    { TEST_FN_NAME(append_file), false },
    { TEST_FN_NAME(truncate_file), false },
    { TEST_FN_NAME(grow_directory), true },
    { TEST_FN_NAME(grow_truncate_files), true },
    { TEST_FN_NAME(clear_root_files), false },
};

void run_tests()
{
    for (unsigned i = 0; i < sizeof(tests) / sizeof(tests[0]); i++) {
        const auto& t = tests[i];

        bool skip = t.disable;
        if (flags.run_all_tests)
            skip = false;
        if (flags.run_this_test)
            skip = strcmp(t.name, flags.run_this_test) != 0;
        if (skip)
            continue;

        cout << "--- " << t.name << " ---\n";
        fs.mount();
        t.fn();
        fs.unmount();
    }
}

void parse_options(int argc, char* argv[])
{
    int ch;

    while ((ch = getopt(argc, argv, "dD:t:aw")) != -1) {
        switch (ch) {
        case 'd':
            flags.debug_start = 0;
            break;
        case 'D':
            flags.debug_start = flags.debug_end = strtol(optarg, NULL, 0);
            break;
        case 't':
            flags.run_this_test = optarg;
            break;
        case 'a':
            flags.run_all_tests = true;
            break;
        case 'w':
            flags.writethru = true;
            break;
        case '?':
        default:
            exit(1);
        }
    }
}

int main(int argc, char *argv[])
{
    parse_options(argc, argv);

    try
    {
        run_tests();
    }
    catch (filesystem_error& e)
    {
        cout << "filesystem_error: " << e.what() << '\n';
        tassert(false);
    }

    fs.unmount();

    return 0;
}
