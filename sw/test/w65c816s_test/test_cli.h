#pragma once

#include <cli/command.h>

class test_cli
{
public:

    test_cli();

private:
    int reset(const ::cli::command::args_t& args);

    int cpumem_store_display(const ::cli::command::args_t& args);

    int cpumem_dump_stats(const ::cli::command::args_t& args);
};
