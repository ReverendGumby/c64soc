#pragma once

#include <stdint.h>

void cpumem_init(void);

const uint8_t* cpumem_get_wram();
const uint8_t* cpumem_get_rom();
