/*
 * w65c816s_test/main.cc: CPU test
*/

#include <chrono>
#include <thread>
#include "util/pollable.h"
#include "plat/manager.h"
#include "app/version.h"
#include "cli/manager.h"
#include "test_cli.h"
#include "snes/icd.h"
#include "snes/monitor/monitor.h"
#include "cpumem.h"

int main()
{
    plat::manager plm;
    plm.early_init();

    printf("W65C816S test\n"
           "SW version %s (@%s) build %s\n",
           sw_build_branch, sw_build_rev, sw_build_timestamp);

    plm.late_init();

    auto hw_ver = plm.get_rtl_version();
    printf("HW version %s (@%s) build %s\n",
           hw_ver.git_branch.c_str(), hw_ver.git_rev.c_str(), hw_ver.timestamp.c_str());

    class snes::icd icd;
    icd.init();
    snes::monitor::monitor mon(icd);

    cpumem_init();

    cli::manager clim;
    clim.start();
    test_cli test_cli;

    for (;;) {
        poll_manager_get().poll();

        pthread_yield();
    }

    //for (;;)
    //    std::this_thread::sleep_for(std::chrono::seconds(10));

    return 0;
}
