#include <stdint.h>
#include <string.h>
#include <memory>
#include "../../snes/emubus.h"

static std::unique_ptr<uint8_t[]> wram, rom;

#if 0
constexpr size_t wram_to_cpu_addr(size_t idx)
{
    return 0x7E0000 | (idx & 0x1FFFF);
}

constexpr size_t rom_to_cpu_addr(size_t idx)
{
    return ((idx >> 15) << 16) | (idx & 0x7FFF) | 0x8000;
}

uint8_t cpumem_read(size_t addr)
{
    EMUBUS_CPUMEM_BA = addr >> 16;
    return EMUBUS_CPU_MEM_REG(addr & 0xFFFF);
}

void cpumem_write(size_t addr, uint8_t val)
{
    EMUBUS_CPUMEM_BA = addr >> 16;
    EMUBUS_CPU_MEM_REG(addr & 0xFFFF) = val;
}
#endif

#include "cputest-basic.h"

void cpumem_init(void)
{
    const size_t wram_len = 128 * 1024;
    const size_t rom_len = 256 * 1024;

    wram = std::unique_ptr<uint8_t[]> {new uint8_t[wram_len]};
    rom = std::unique_ptr<uint8_t[]> {new uint8_t[rom_len]};

    memcpy(&rom[0], cputest_basic_rom, rom_len);

    EMUBUS_CPUMEM_BASE_WRAM = reinterpret_cast<uint32_t>(&wram[0]);
    EMUBUS_CPUMEM_BASE_ROM = reinterpret_cast<uint32_t>(&rom[0]);
}

const uint8_t* cpumem_get_wram()
{
    return wram.get();
}

const uint8_t* cpumem_get_rom()
{
    return rom.get();
}
