#include "snes/emubus.h"
#include "snes/memptr.h"
#include "cpumem.h"

#include <cli/command.h>
#include <cli/manager.h>
#include <util/dump_to_str.h>
#include <util/reg_val.h>
#include <os/cpu/cpu.h>

#include <stdint.h>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>

#include "test_cli.h"

using namespace ::cli;
using namespace snes;

test_cli::test_cli()
{
    command::define("reset", [this](const command::args_t& args) {
        return reset(args); });

    command::define("cmd", [this](const command::args_t& args) {
        return cpumem_store_display(args); });

    command::define("cds", [this](const command::args_t& args) {
        return cpumem_dump_stats(args); });
}

int test_cli::reset(const command::args_t& args)
{
    bool assert = false, deassert = false;

    if (args.size() >= 1) {
        assert = (args[0] == "assert");
        deassert = (args[0] == "deassert");
    } else {
        assert = deassert = true;
    }

    if (assert) {
        EMUBUS_RESETS &= ~EMUBUS_RESETS_EMU;
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    if (deassert) {
        EMUBUS_RESETS |= EMUBUS_RESETS_EMU;
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    return 0;
}

int test_cli::cpumem_store_display(const command::args_t& args)
{
    auto it = args.begin();
    if (args.end() - it < 1)
        throw std::range_error("missing argument");
    const uint8_t* mem = nullptr;
    if (*it == "wram")
        mem = cpumem_get_wram();
    else if (*it == "rom")
        mem = cpumem_get_rom();
    else
        throw std::range_error("unknown memory type");

    it++;
    if (args.end() - it < 1)
        throw std::range_error("missing argument");
    auto addr = memaddr(strtoul(it->c_str(), NULL, 0));

    unsigned long len = 1;
    it++;
    if (it < args.end())
        len = strtoul(it->c_str(), NULL, 0);

    if (!mem)
        throw std::runtime_error("memory not allocated");

    uint8_t data[len];
    for (unsigned long i = 0; i < len; i++)
        data[i] = mem[addr + i];

    for (auto& line : dump_to_str(data, len, addr))
        std::cout << line << std::endl;

    return 0;
}

int test_cli::cpumem_dump_stats(const ::cli::command::args_t& args)
{
#define dump(x)                                             \
    do {                                                    \
        EMUBUS_CPUMEM_MON_SEL = EMUBUS_CPUMEM_MON_SEL_##x;  \
        auto v = EMUBUS_CPUMEM_MON_DATA;                    \
        std::cout << #x " = " << v << '\n';                 \
    } while (0)

    dump(ATM_RDONE_MAX);
    dump(ATM_RDONE_MIN);
    dump(ATM_WDONE_MAX);
    dump(ATM_WDONE_MIN);
    dump(ATM_NOT_READY_CNT);

#undef dump

    // Clear stats
    EMUBUS_CPUMEM_MON_SEL = EMUBUS_CPUMEM_MON_SEL_ATM_RDONE_MAX;
    EMUBUS_CPUMEM_MON_DATA = 0;
    EMUBUS_CPUMEM_MON_SEL = EMUBUS_CPUMEM_MON_SEL_ATM_NOT_READY_CNT;
    EMUBUS_CPUMEM_MON_DATA = 0;

    return 0;
}
