/*
 * cli/main.cc: CLI test
*/

#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include "plat/manager.h"
#include "cli/command.h"
#include "cli/manager.h"

int do_ping(const cli::command::args_t&)
{
    std::cout << "pong\n";
    return 0;
}

int main()
{
    plat::manager plm;
    plm.early_init();

    std::cout << "CLI test\n\n";

    cli::manager clim;
    clim.start();

    cli::command::define("ping", do_ping);

    for (;;)
        std::this_thread::sleep_for(std::chrono::seconds(10));

    return 0;
}
