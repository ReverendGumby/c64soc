#pragma once

#include "block_device.h"

namespace fat
{

class block_cache : public block_device
{
public:
    block_cache(std::shared_ptr<block_device> dev, unsigned num_blocks);
    virtual ~block_cache();

    // implement (most of) block_device
    unsigned get_sectors_per_block() const override;
    sector get_capacity() const override;
    // read_block: subclass to override
    void write_block(sector s, const block_buffer_t&) override;
    void flush_writes() override;

protected:
    unsigned _num_blocks;
    std::shared_ptr<block_device> _dev;
};

} // namespace fat
