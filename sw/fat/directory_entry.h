#ifndef DIRECTORY_ENTRY_H_
#define DIRECTORY_ENTRY_H_

#include "fat32.h"
#include "path.h"

namespace fat
{

struct directory_entry
{
    unsigned idx;
    path name;
#ifdef FAT_HAVE_LONGNAME
    path shortname;
#endif
    bool is_dir;
    bool is_hidden;
    cluster first_cluster;
    filesize size;

    // A pointer back to the directory from whence this originated
    cluster dir_first_cluster;
    fileoff dir_offset;                         // offset of DIR in directory

    static directory_entry dir();               // initialize for directory
    static directory_entry root();              // for THE root directory
};

directory_entry resolve_path(const path &path);

} // namespace fat

#endif /* DIRECTORY_ENTRY_H_ */
