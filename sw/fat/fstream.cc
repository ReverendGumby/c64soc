#include "fstream.h"

namespace fat {

fstream::fstream()
    : std::basic_iostream<char>{new filebuf()}
{
}

fstream::fstream(const path& filename, std::ios_base::openmode mode)
    : fstream{}
{
    filebuf* fbuf = dynamic_cast<filebuf*>(rdbuf());
    if (!fbuf->open(filename, mode))
        setstate(failbit);
}

fstream::~fstream()
{
    delete rdbuf();
}

void fstream::open(const path& filename, std::ios_base::openmode mode)
{
    filebuf* fbuf = dynamic_cast<filebuf*>(rdbuf());
    if (!fbuf->open(filename, mode))
        setstate(failbit);
    else
        clear();
}

} // namespace fat
