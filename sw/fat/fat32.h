#ifndef FAT32_H_
#define FAT32_H_

#include <memory>
#include <stdint.h>
#include <ostream>
#include <utility>
#include <mutex>
#include "util/iop.h"
#include "fat32_priv.h"
#include "block_device.h"

namespace fat
{

using cluster = uint32_t;
using filesize = int64_t;
using fileoff = filesize;

class fat32
{
public:
    static constexpr cluster cluster_free = 0x00000000;
    static constexpr cluster cluster_eof = 0x0FFFFFFF;

    // filesystem factory
    static std::shared_ptr<fat32> mount(std::shared_ptr<block_device> dev);

    // singleton
    static std::shared_ptr<const fat32> get_filesystem();
    static std::pair<std::shared_ptr<fat32>, std::unique_lock<std::recursive_mutex> > get_filesystem_with_lock();

    static bool is_mounted();

#ifdef FAT_DEBUG
    static void set_debug_stream(std::ostream* in_ds);
    static std::ostream& debug_stream() { return *ds; }
#endif

    void unmount();

    unsigned sector_size() const { return _sector_size; }
    sector sectors_per_cluster() const { return _sectors_per_cluster; }
    unsigned cluster_size() const { return sector_size() * sectors_per_cluster(); }
    cluster root_cluster() const { return _root_cluster; }

    cluster read_fat(cluster);
    cluster find_free_fat();
    void write_fat(cluster cl, cluster next);

    unsigned read_cluster(cluster cl, unsigned off, iop&& iop, unsigned count);
    void write_cluster(cluster cl, unsigned off, iop&& iop, unsigned count);

    void sync();

    void add_writer(int c);

private:
    fat32(std::shared_ptr<block_device> dev);

    void _mount();
    sector cluster_to_sector(cluster cl) const;
    void sector_offset_to_block_offset(sector& s, unsigned& off) const;
    void read_sector(sector s, unsigned off, iop&& iop, unsigned count);
    void write_sector(sector s, unsigned off, iop&& iop, unsigned count);

    static std::shared_ptr<fat32> filesystem;
#ifdef FAT_DEBUG
    static std::ostream* ds;
#endif

    std::recursive_mutex _mtx; // protects full filesystem
    std::shared_ptr<block_device> _dev;
    unsigned _spb; // size of _dev block in bytes
    bool _mounted;
    BPB _bpb;

    unsigned _sector_size;
    sector _sectors_per_cluster;
    sector _fat_sz;
    sector _resvd_sec_cnt;
    cluster _root_cluster;
    sector _first_data_sector;
    unsigned _num_fats;
    cluster _count_of_clusters;
    cluster _max_valid_cluster;

    int _writer_count = 0;
};

} // namespace fat

#endif /* FAT32_H_ */
