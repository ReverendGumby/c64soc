#include "block_cache.h"

namespace fat
{

block_cache::block_cache(std::shared_ptr<block_device> dev, unsigned num_blocks)
    : _num_blocks(num_blocks), _dev(dev)
{
}

block_cache::~block_cache()
{
}

sector block_cache::get_capacity() const
{
    return _dev->get_capacity();
}

unsigned block_cache::get_sectors_per_block() const
{
    return _dev->get_sectors_per_block();
}

void block_cache::write_block(sector, const block_buffer_t&)
{
}

void block_cache::flush_writes()
{
}

} // namespace fat
