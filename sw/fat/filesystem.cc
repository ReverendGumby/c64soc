#include "fat32.h"
#include "filesystem.h"
#include "filesystem_error.h"
#include "directory.h"
#include "directory_entry.h"

namespace fat {

bool is_mounted()
{
    return fat32::is_mounted();
}

bool exists(const path& p)
{
    try {
        auto de = resolve_path(p);
        return true;
    } catch (filesystem_error& e) {
        return false;
    }
}

file open(const path& p, omode mode, int flags)
{
    if (mode == omode::r) {
        return file{p};
    }
    file f;
    bool found = exists(p);
    if (!found && (flags & oflags::create) != 0) {
        // Create a new file
        directory_entry de { };
        de.name = p.filename();
        de.is_dir = false;

        directory pd { p.parent_path() };
        pd.add(de);
    }
    f = file{p};
    if (found && (flags & oflags::truncate) != 0) {
        f.truncate();
    }
    return f;
}

bool remove(const path& p)
{
    directory pd { p.parent_path() };
    return pd.remove(p.filename());
}

} // namespace fat
