#ifndef FAT32_PRIV_H_
#define FAT32_PRIV_H_

#include <stdint.h>

namespace fat
{

// Common to MBR, BPB, FSInfo
constexpr uint16_t TrailSig_value = 0xAA55;

struct __attribute__((packed)) CHS
{
    uint8_t data[3];
    // uint16_t c() const { return ((uint16_t)(data[1] & 0xc0) << 2) | data[2]; }
    // uint8_t h() const { return data[0]; }
    // uint8_t s() const { return data[1] & 0x3f; }
};

struct __attribute__((packed)) MBR_PE
{
    uint8_t status;
    CHS first_chs;
    uint8_t type;
    CHS last_chs;
    uint32_t first_lba;
    uint32_t num_sectors;
};

constexpr uint8_t MBR_TYPE_FREE = 0x00;
constexpr uint8_t MBR_TYPE_FAT32_CHS = 0x0B;
constexpr uint8_t MBR_TYPE_FAT32_LBA = 0x0C;

struct __attribute__((packed)) MBR
{
    uint8_t MBR_Reserved0[0x1BE];
    MBR_PE MBR_entry1;
    MBR_PE MBR_entry2;
    MBR_PE MBR_entry3;
    MBR_PE MBR_entry4;
    uint16_t MBR_TrailSig;                      // offset 510: 0xAA55
};

constexpr unsigned SHORTNAME_LEN = 11;
constexpr unsigned SHORTNAME_BASE_OFF = 0;
constexpr unsigned SHORTNAME_BASE_LEN = 8;
constexpr unsigned SHORTNAME_EXT_OFF = 8;
constexpr unsigned SHORTNAME_EXT_LEN = 3;

struct __attribute__((packed)) BPB
{
    uint8_t BS_jmpBoot[3];
    char BS_OEMName[8];
    uint16_t BPB_BytsPerSec;
    uint8_t BPB_SecPerClus;
    uint16_t BPB_ResvdSecCnt;
    uint8_t BPB_NumFATs;
    uint16_t BPB_RootEntCnt;
    uint16_t BPB_TotSec16;
    uint8_t BPB_Media;
    uint16_t BPB_FATSz16;
    uint16_t BPB_SecPerTrk;
    uint16_t BPB_NumHeads;
    uint32_t BPB_HiddSec;
    uint32_t BPB_TotSec32;
    // Starting here, FAT12/FAT16 and FAT32 diverge.
    uint32_t BPB_FATSz32;                       // offset 36
    uint16_t BPB_ExtFlags;
    uint16_t BPB_FSVer;
    uint32_t BPB_RootClus;
    uint16_t BPB_FSInfo;
    uint16_t BPB_BkBootSec;
    uint8_t BPB_Reserved[12];
    uint8_t BS_DrvNum;
    uint8_t BS_Reserved1;
    uint8_t BS_BootSig;
    uint32_t BS_VolID;
    char BS_VolLab[SHORTNAME_LEN];
    char BS_FilSysType[8];                      // offset 82
    uint8_t _BS_Reserved2[420];                 // offset 90
    uint16_t _BS_TrailSig;                      // offset 510: 0xAA55
};

constexpr uint16_t BPB_EXTFLAGS_MIRRORING_DISABLED = 1 << 7;

struct __attribute__((packed)) FSInfo
{
    uint32_t FSI_LeadSig;                       // offset 0: "AaRR"
    uint8_t FSI_Reserved1[480];
    uint32_t FSI_StrucSig;                      // offset 484: "aArr"
    uint32_t FSI_Free_Count;
    uint32_t FSI_Nxt_Free;
    uint8_t FSI_Reserved2[14];
    uint16_t FSI_TrailSig;                      // offset 510: 0xAA55
};

struct __attribute__((packed)) DIR
{
    char DIR_Name[SHORTNAME_LEN];
    uint8_t DIR_Attr;
    uint8_t DIR_NTRes;
    uint8_t DIR_CrtTimeTenth;
    uint16_t DIR_CrtTime;
    uint16_t DIR_CrtDate;
    uint16_t DIR_LstAccDate;
    uint16_t DIR_FstClusHI;
    uint16_t DIR_WrtTime;
    uint16_t DIR_WrtDate;
    uint16_t DIR_FstClusLO;
    uint32_t DIR_FileSize;
};

constexpr char DIR_NAME_FREE = 0xe5;
constexpr char DIR_NAME_EOD  = 0x00;

constexpr uint8_t DIR_ATTR_MASK      = 0x3f;

constexpr uint8_t DIR_ATTR_READ_ONLY = 0x01;
constexpr uint8_t DIR_ATTR_HIDDEN    = 0x02;
constexpr uint8_t DIR_ATTR_SYSTEM    = 0x04;
constexpr uint8_t DIR_ATTR_VOLUME_ID = 0x08;
constexpr uint8_t DIR_ATTR_DIRECTORY = 0x10;
constexpr uint8_t DIR_ATTR_ARCHIVE   = 0x20;

constexpr uint8_t DIR_ATTR_LONG_NAME = 0x0f;

constexpr uint8_t DIR_NTRES_LCASE_MAIN  = 0x08;
constexpr uint8_t DIR_NTRES_LCASE_EXT   = 0x10;

struct __attribute__((packed)) LDIR
{
    uint8_t LDIR_Ord;
    uint16_t LDIR_Name1[5];
    uint8_t LDIR_Attr;
    uint8_t LDIR_Type;
    uint8_t LDIR_Chksum;
    uint16_t LDIR_Name2[6];
    uint16_t LDIR_FstClusLO;
    uint16_t LDIR_Name3[2];
};

constexpr uint8_t LDIR_ORD_LAST_LONG_ENTRY = 0x40;

inline uint16_t fstoh16(const void* px)
{
    auto p = reinterpret_cast<const uint8_t*>(px);
    return ((uint16_t)p[0] << 8*0)
        | ((uint16_t)p[1] << 8*1);
}

inline uint32_t fstoh32(const void* px)
{
    auto p = reinterpret_cast<const uint8_t*>(px);
    return ((uint32_t)p[0] << 8*0)
        | ((uint32_t)p[1] << 8*1)
        | ((uint32_t)p[2] << 8*2)
        | ((uint32_t)p[3] << 8*3);
}

inline void fswrite16(void* px, uint16_t v)
{
    auto p = reinterpret_cast<uint8_t*>(px);
    p[0] = v >> 8*0;
    p[1] = v >> 8*1;
}

inline void fswrite32(void* px, uint32_t v)
{
    auto p = reinterpret_cast<uint8_t*>(px);
    p[0] = v >> 8*0;
    p[1] = v >> 8*1;
    p[2] = v >> 8*2;
    p[3] = v >> 8*3;
}

} // namespace fat

#endif /* FAT32_PRIV_H_ */
