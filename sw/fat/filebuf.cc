#include "filesystem.h"
#include "filebuf.h"

namespace fat {

filebuf::filebuf()
    : std::basic_streambuf<char>()
{
    _f = nullptr;
    _buf = nullptr;
    _used = 0;
    _off = 0;
    _reading = false;
    _writing = false;
}

filebuf::filebuf(filebuf&& rhs)
    : std::basic_streambuf<char>(rhs)
{
    _f = rhs._f;
    _buf = rhs._buf;
    _used = rhs._used;
    _off = rhs._off;
    _reading = rhs._reading;
    _writing = rhs._writing;

    rhs._f = nullptr;
    rhs._buf = nullptr;
}

filebuf::~filebuf()
{
    try {
        close();
    } catch (...) {
    }
}

bool filebuf::is_open() const
{
    return _f;
}

filebuf* filebuf::open(const path& filename, std::ios_base::openmode mode)
{
    mode &= ~std::ios_base::binary; // who cares?

    if (is_open())
        return nullptr;

    fat::omode omode;
    bool mode_in = mode & std::ios_base::in;
    bool mode_out = mode & std::ios_base::out;
    if (mode_in && mode_out)
        omode = fat::omode::rw;
    else if (mode_in)
        omode = fat::omode::r;
    else if (mode_out)
        omode = fat::omode::w;
    else
        throw std::domain_error("filebuf::open: invalid mode");
    int oflags = 0;
    if (!mode_in || (mode & ~(std::ios_base::in | std::ios_base::out))) {
        oflags |= fat::oflags::create;
        if (mode_out && !(mode & std::ios_base::app))
            oflags |= fat::oflags::truncate;
    }

    try {
        _f = new file(fat::open(filename, omode, oflags));
    } catch (filesystem_error& e) {
        close();
        return nullptr;
    }
    _buf = new char[buf_size];
    setg(_buf, _buf, _buf);
    setp(_buf, _buf + buf_size);
    return this;
}

filebuf* filebuf::close()
{
    if (!is_open())
        return nullptr;

    if (pptr() > pbase())
        sync();

    delete _f;
    _f = nullptr;
    delete [] _buf;
    _buf = nullptr;
    return this;
}

int filebuf::sync()
{
#ifdef FAT_DEBUG
    fat32::debug_stream() << "filebuf::sync\n";
#endif

    return -(overflow() == traits_type::eof());
}

std::streamsize filebuf::showmanyc()
{
    if (!is_open())
        return -1;
    return std::streamsize(_f->size() - _f->tell());
}

filebuf::int_type filebuf::underflow()
{
    if (!is_open())
        return traits_type::eof();

    if (_writing) {
        if (overflow() == traits_type::eof())
            return traits_type::eof();
        _writing = false;
    }

    if (!_f->eof()) {
        _off += _used;
        _used = _f->read(_buf, buf_size);
        setg(_buf, _buf, _buf + _used);
        setp(_buf, _buf + buf_size);
        _reading = true;
    } else {
        _used = 0;
        _reading = true;
    }
#ifdef FAT_DEBUG
    fat32::debug_stream() << "filebuf::underflow: _off=" << _off
                          << ", _used=" << _used
                          << ", *_buf=" << _buf[0]
                          << ", eback()=&_buf[" << eback() - _buf << "]"
                          << ", gptr()=&_buf[" << gptr() - _buf << "]"
                          << ", egptr()=&_buf[" << egptr() - _buf << "]\n";
#endif

    if (_used == 0)
        return traits_type::eof();
    return _buf[0];
}

filebuf::int_type filebuf::overflow(int_type c)
{
    if (!is_open())
        return traits_type::eof();

#ifdef FAT_DEBUG
    fat32::debug_stream() << "filebuf::overflow: _off=" << _off
                          << ", _used=" << _used
                          << ", *_buf=" << _buf[0]
                          << ", pbase()=&_buf[" << pbase() - _buf << "]"
                          << ", pptr()=&_buf[" << pptr() - _buf << "]"
                          << ", epptr()=&_buf[" << epptr() - _buf << "]\n";
#endif

    if (_reading) {
        _f->seek(fileoff(_off), seekfrom::start);
        _reading = false;
    }

    auto len = pptr() - pbase();
    if (len) {
        _f->write(pbase(), len);
        _off += len;
    }
    _used = 0;
    setg(_buf, _buf, _buf + _used);
    setp(_buf, _buf + buf_size);

    if (c != traits_type::eof()) {
        *pptr() = c;
        pbump(1);
        _writing = true;
        //gbump(1);
    }
    // If _write() returned an error, we'd do this:
    //   if (wrote < len)
    //     return traits_type::eof();
    return 0;
}

std::streamsize filebuf::xsputn(const char_type* s, std::streamsize count)
{
    if (!is_open())
        return traits_type::eof();

    if (count <= std::streamsize(buf_size))
        return std::basic_streambuf<char>::xsputn(s, count);

    if (overflow() == traits_type::eof())
        return traits_type::eof();
    _f->write(s, count);
    _off += count;
    return count;
}

filebuf::int_type filebuf::pbackfail(int_type c)
{
#ifdef FAT_DEBUG
    fat32::debug_stream() << "filebuf::pbackfail(c=" << c << ")\n";
#endif
    if (!is_open() || _off == 0)
        return traits_type::eof();

    if (_writing) {
        if (overflow() == traits_type::eof())
            return traits_type::eof();
        _writing = false;
    }

    if (c == traits_type::eof() || gptr() > eback()) {
        seekpos(_off - pos_type(1));
        _used = _f->read(_buf, buf_size);
        setg(_buf, _buf + 1, _buf + _used);
        setp(_buf, _buf + buf_size);
    }
#ifdef FAT_DEBUG
    fat32::debug_stream() << "filebuf::pbackfail: _off=" << _off
                          << ", _used=" << _used
                          << ", *_buf=" << _buf[0]
                          << ", eback()=&_buf[" << eback() - _buf << "]"
                          << ", gptr()=&_buf[" << gptr() - _buf << "]"
                          << ", egptr()=&_buf[" << egptr() - _buf << "]\n";
#endif
    if (_used == 0)
        return traits_type::eof();

    gbump(-1);
    if (c == traits_type::eof())
        return traits_type::not_eof(c);
    if (!traits_type::eq(c, *gptr())) {
        *gptr() = c;
        _reading = true;
    }
    return c;
}

filebuf::pos_type filebuf::seekoff(off_type off,
                                   std::ios_base::seekdir dir,
                                   std::ios_base::openmode)
{
#ifdef FAT_DEBUG
    fat32::debug_stream() << "filebuf::seekoff(off=" << off << ", dir="
                          << dir << ")\n";
#endif
    if (!is_open())
        return pos_type(off_type(-1));

    bool moving = !(off == 0 && dir == std::ios::cur);
    if (moving) {
        // Flush any buffered writes before computing new _off.
        if (pptr() > pbase()) {
            if (overflow() == traits_type::eof())
                return traits_type::eof();
        }
    }

    pos_type ret;
    switch (dir) {
    case std::ios::beg:
        _off = off;
        ret = _off;
        break;
    case std::ios::end:
        _off = _f->size() + off;
        ret = _off;
        break;
    case std::ios::cur:
        _off = _off + off;
        ret = _off + off_type(gptr() - _buf) + off_type(pptr() - pbase());
        break;
    default:
        return pos_type(off_type(-1));
    }

    if (moving) {
        _f->seek(fileoff(ret), seekfrom::start);
        _used = 0;
        setg(_buf, _buf, _buf + _used);
        setp(_buf, _buf + buf_size);
        _reading = false;
        _writing = false;
    }
#ifdef FAT_DEBUG
    fat32::debug_stream() << "filebuf::seekoff: _off=" << _off
                          << ", return(" << ret << ")\n";
#endif
    return ret;
}

filebuf::pos_type filebuf::seekpos(pos_type sp,
                                   std::ios_base::openmode)
{
    return seekoff(off_type(sp), std::ios::beg);
}

} // namespace fat
