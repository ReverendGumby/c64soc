#ifndef DIRECTORY_H_
#define DIRECTORY_H_

#include "file.h"
#include "path.h"
#include "directory_entry.h"
#include "fat32.h"

namespace fat
{

class directory : private file
{
public:
	directory();
    explicit directory(const path &path) : directory(resolve_path(path)) {}
    explicit directory(const directory_entry &dirent);

    static directory root();

    bool is_eod() const { return _eod; }
    void reset_read();
    directory_entry read();

    size_t size();

    // Update the given entry in the directory from whence it originated
    static void update(directory_entry& de);

    void add(directory_entry& de);
    bool remove(const path& filename);

private:
    struct sldirs_t;

    bool is_root() const;
    bool read_next(sldirs_t* psld = nullptr);
    bool has_shortname(const char* name);
    void make_dir_for_long_name(const path& fnpath, struct DIR& d);
    sldirs_t build_sldirs(const path& filename);
    int add_scan(sldirs_t& sld);

    bool _eod = false;
    directory_entry _next;
    unsigned _entry_index = 0;
};

} // namespace fat

#endif /* DIRECTORY_H_ */
