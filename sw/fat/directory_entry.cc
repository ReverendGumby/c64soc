#include "path.h"
#include "fat32.h"
#include "filesystem_error.h"
#include "directory_iterator.h"
#include "directory_entry.h"

namespace fat
{

static const path root_dir{'\\'};
static const path self_dir{'.'};

directory_entry resolve_path(const path &in_path)
{
#ifdef FAT_DEBUG
    fat32::debug_stream()
        << "resolve_path(" << in_path << ")\n";
#endif

    // We've no current directory, thus all paths must be absolute.
    if (!in_path.has_root_directory())
        throw filesystem_error("relative path");

    directory_entry last_de;

    for (const auto& p : in_path)
    {
#ifdef FAT_DEBUG
        fat32::debug_stream()
            << "  resolve_path: p = " << p << '\n';
#endif
        if (p == self_dir)
            continue;                           // trivial optimization
        if (p.has_root_directory())
        {
            last_de = directory_entry::root();
            continue;
        }
        bool found = false;
        for (const auto& de : directory_iterator{last_de})
            if (p == de.name)
            {
                last_de = de;
                found = true;
                break;
            }
        if (!found)
            throw filesystem_error("path not found");
    }
#ifdef FAT_DEBUG
    fat32::debug_stream()
        << "resolve_path: return(de.name=" << last_de.name << ")\n";
#endif
    return last_de;
}

directory_entry directory_entry::dir()
{
    directory_entry ret {};
    ret.is_dir = true;
    ret.size = -1;
    return ret;
}

directory_entry directory_entry::root()
{
    directory_entry ret = dir();
    ret.idx = 0;
    ret.name = root_dir;
#ifdef FAT_HAVE_LONGNAME
    ret.shortname = ret.name;
#endif
    ret.is_hidden = false;
    ret.first_cluster = fat32::get_filesystem()->root_cluster();
    return ret;
}

} // namespace fat
