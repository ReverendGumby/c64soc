#include "path.h"
#include "directory_iterator.h"

#include <util/string.h>
#include <cli/command.h>

#include <iostream>
#include <iomanip>
#include <regex>

namespace fat {
namespace cli {

using namespace ::cli;

static std::wregex
glob_to_regex(const std::wstring& glob)
{
    static const std::wstring escapes = L"^$\\.*+?()[]{}!";
    std::wstring re;

    for (auto c : glob) {
        if (c == L'*')
            re += L'.';
        else if (escapes.find(c) != escapes.npos)
            re += L'\\';
        re += c;
    }

    return std::wregex(re, std::wregex::icase);
}

static int ls(const command::args_t& args)
{
    fat::path dir {L"\\"};
    if (args.size() >= 1) {
        dir = args[0];
    }

    auto re = std::wregex(L".*");
    auto dir_fname = dir.filename().native();
    if (dir_fname.find(L'*') != dir_fname.npos) {
        auto glob = dir.filename().native();
        dir = dir.parent_path();
        re = glob_to_regex(glob);
    }

    for (const auto& de : fat::directory_iterator{dir})
    {
        std::string f1, f2;
        const auto& name = de.name;
        if (de.is_hidden)
            continue;
        if (!std::regex_match(name.native(), re))
            continue;
        if (de.is_dir) {
            if (name == fat::path{L"."})
                continue;
            f1 = "<DIR>";
        } else {
            f1 = string_printf("%llu", (unsigned long long)de.size);
        }
        f2 = name.string();

        std::cout << std::left << std::setw(12) << f1
                  << f2 << "\n";
    }

    return 0;
}

void init()
{
    command::define("ls", [](const command::args_t& args) {
        return ls(args); });
    command::define("dir", [](const command::args_t& args) {
        return ls(args); });
}

} // namespace cli
} // namespace fat
