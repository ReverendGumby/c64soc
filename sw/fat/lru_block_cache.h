#pragma once

// Block cache with an LRU replacement policy.

#include <map>
#include "block_cache.h"

namespace fat
{

class lru_block_cache : public block_cache
{
public:
    lru_block_cache(std::shared_ptr<block_device> dev, unsigned num_blocks);
    virtual ~lru_block_cache();

    void set_writethru(bool e);

    // implement block_device
    block_buffer_t read_block(sector s) override;
    void write_block(sector s, const block_buffer_t&) override;
    void flush_writes() override;

private:
    struct cache_entry_t {
        block_buffer_t bb;
        int age;
        bool dirty;
    };
    using cache_t = std::map<sector, cache_entry_t>;

    cache_entry_t& find_block(sector, const block_buffer_t* pbb);
    void make_room();
    void cache_maintenance();

    cache_t _cache;
    bool _writethru;
};

} // namespace fat
