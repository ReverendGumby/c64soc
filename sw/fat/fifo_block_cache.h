#pragma once

// Block cache with a FIFO replacement policy

// At the moment, FSBL requires this policy, because (a) it allocates a 2-buffer
// cache, and (b) we don't track which buffers are in use by iovec's.

#include <map>
#include "block_cache.h"

namespace fat
{

class fifo_block_cache : public block_cache
{
public:
    fifo_block_cache(std::shared_ptr<block_device> dev, unsigned num_blocks);
    virtual ~fifo_block_cache();

    // implement block_device
    block_buffer_t read_block(sector s) override;

private:
    std::map<sector, block_buffer_t> _cache;
};

} // namespace fat
