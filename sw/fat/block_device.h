#ifndef BLOCK_DEVICE_H_
#define BLOCK_DEVICE_H_

#include <memory>
#include "util/buffer.h"

namespace fat
{

// A sector is defined as 512 bytes.
using sector = uint32_t;
static constexpr unsigned sector_size = 512;

class block_device
{
public:
    using block_buffer_t = std::shared_ptr<buffer>;

    // A block is N sectors, where N is a power of two.
    virtual unsigned get_sectors_per_block() const = 0;

    unsigned get_block_size() const
    {
        return get_sectors_per_block() * sector_size;
    }

    virtual sector get_capacity() const = 0;

    // Blocks are addressed by their first sector.
    virtual block_buffer_t read_block(sector s) = 0;
    virtual void write_block(sector s, const block_buffer_t&) = 0;
    virtual void flush_writes() = 0;
};

} // namespace fat

#endif /* BLOCK_DEVICE_H_ */
