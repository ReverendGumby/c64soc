#ifndef PARTITION_H_
#define PARTITION_H_

#include <stdint.h>
#include "fat32.h"
#include "fat32_priv.h"

namespace fat
{

struct partition
{
public:
    enum class type { free, unknown, fat32 };

    partition() {}
    partition(const MBR_PE& pe);

    bool is_free() const { return _type == type::free; }

    type _type = type::free;
    uint8_t _raw_type = 0;
    sector _first = 0, _last = 0;
};

} // namespace fat

#endif /* PARTITION_H_ */
