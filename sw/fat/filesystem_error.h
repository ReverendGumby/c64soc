#ifndef FILESYSTEM_ERROR_H_
#define FILESYSTEM_ERROR_H_

#include <stdexcept>

namespace fat
{

struct filesystem_error : public std::runtime_error
{
    filesystem_error(const char *what_arg) : std::runtime_error(what_arg) {}
};

} // namespace fat

#endif /* FILESYSTEM_ERROR_H_ */
