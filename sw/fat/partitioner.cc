#include "filesystem_error.h"
#include "partitioner.h"
#include "os/cpu/cpu.h"
#include "fat32_priv.h"

namespace fat {

partitioner::partitioner()
{
}

partitioner::~partitioner()
{
}

void partitioner::load(std::shared_ptr<block_device> dev)
{
    auto bb = dev->read_block(0);
    MBR& mbr = *reinterpret_cast<MBR*>(bb->ptr);
    os::cpu::cpu.cache.invalidate(&mbr, sizeof(mbr));
    if (fstoh16(&mbr.MBR_TrailSig) != TrailSig_value)
        throw filesystem_error("bad MBR signature");

    _part_devs.clear();
    add(dev, mbr.MBR_entry1);
    add(dev, mbr.MBR_entry2);
    add(dev, mbr.MBR_entry3);
    add(dev, mbr.MBR_entry4);
}

void partitioner::add(std::shared_ptr<block_device> dev, const MBR_PE& pe)
{
    partition part{pe};
    if (!part.is_free())
    {
        std::shared_ptr<block_device> tbd{new translated_block_device{dev, part}};
        part_dev pd{part, tbd};
        _part_devs.push_back(pd);
    }
}

partitioner::translated_block_device
::translated_block_device(std::shared_ptr<block_device> dev,
                          const partition& part)
    : _dev(dev), _part(part)
{
}

partitioner::translated_block_device
::~translated_block_device()
{
}

partitioner::translated_block_device::block_buffer_t
partitioner::translated_block_device
::read_block(sector s)
{
    return _dev->read_block(translate(s));
}

void partitioner::translated_block_device
::write_block(sector s, const block_buffer_t& bb)
{
    _dev->write_block(translate(s), bb);
}

void partitioner::translated_block_device::flush_writes()
{
    _dev->flush_writes();
}

sector partitioner::translated_block_device
::get_capacity() const
{
    return _part._last - _part._first + 1;
}

unsigned partitioner::translated_block_device
::get_sectors_per_block() const
{
    return _dev->get_sectors_per_block();
}

sector partitioner::translated_block_device
::translate(sector s) const
{
    s += _part._first;
    if (s > _part._last)
        throw filesystem_error("translated_block_device: sector out of range");
    return s;
}

} // namespace fat
