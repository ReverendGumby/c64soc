#ifndef FAT__FILESYSTEM_H_
#define FAT__FILESYSTEM_H_

#include "path.h"
#include "file.h"

namespace fat {

// File open mode
enum class omode { r, w, rw };

// File open flags
struct oflags {
    enum {
        create = 1 << 0,
        truncate = 1 << 1,
    };
};

bool is_mounted();

bool exists(const path& p);
file open(const path& path, omode mode = omode::r, int flags = 0);
bool remove(const path& p);

} // namespace fat

#endif // FAT__FILESYSTEM_H_
