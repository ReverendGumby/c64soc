#include <algorithm>
#include <fstream>
#include <cstring>
#include <stdexcept>
#include <assert.h>
#include "util/iovec.h"
#include "util/ostream_fmt_save.h"
#include "os/cpu/cpu.h"
#include "filesystem_error.h"
#include "block_device.h"
#include "fat32.h"

namespace fat
{

const sector sector_BPB = 0;
const cluster fat16_max_clusters = 65525 - 1;
const uint8_t jmpBoot_0 = 0xEB;
const uint8_t jmpBoot_1 = 0xE9;

std::shared_ptr<fat32> fat32::filesystem;
#ifdef FAT_DEBUG
static std::ofstream nullstream;                // effectively /dev/null
std::ostream* fat32::ds = &nullstream;
#endif

fat32::fat32(std::shared_ptr<block_device> dev)
    : _dev(dev), _mounted(false)
{
    _spb = _dev->get_sectors_per_block();
}

std::shared_ptr<fat32> fat32::mount(std::shared_ptr<block_device> dev)
{
    std::shared_ptr<fat32> fs{new fat32(dev)};

    fs->_mount();
    filesystem = fs;
    return fs;
}

std::shared_ptr<const fat32> fat32::get_filesystem()
{
    if (!is_mounted())
        throw filesystem_error("FS unmounted");
    return filesystem;
}

std::pair<std::shared_ptr<fat32>, std::unique_lock<std::recursive_mutex> > fat32::get_filesystem_with_lock()
{
    if (!is_mounted())
        throw filesystem_error("FS unmounted");
    return { filesystem, std::unique_lock<std::recursive_mutex> { filesystem->_mtx } };
}

#ifdef FAT_DEBUG
void fat32::set_debug_stream(std::ostream* in_ds)
{
    ds = in_ds ? in_ds : &nullstream;
}
#endif

bool fat32::is_mounted()
{
	auto fs = filesystem;
    return fs && fs->_mounted;
}

void fat32::_mount()
{
    // Read the BPB and extract the fields we'll use.
    read_sector(sector_BPB, 0, &_bpb, sizeof(_bpb));
    if (!(_bpb.BS_jmpBoot[0] == jmpBoot_0
          || _bpb.BS_jmpBoot[0] == jmpBoot_1)
        || fstoh16(&_bpb._BS_TrailSig) != TrailSig_value)
        throw filesystem_error("bad BPB signature");
    _sector_size = fstoh16(&_bpb.BPB_BytsPerSec);
    _sectors_per_cluster = _bpb.BPB_SecPerClus;
    _resvd_sec_cnt = fstoh16(&_bpb.BPB_ResvdSecCnt);
    _root_cluster = fstoh32(&_bpb.BPB_RootClus);
    _num_fats = _bpb.BPB_NumFATs;
    uint16_t ext_flags = fstoh16(&_bpb.BPB_ExtFlags);

    if (ext_flags & BPB_EXTFLAGS_MIRRORING_DISABLED)
        throw filesystem_error("FAT mirroring disabled; not supported");

    // Make sure the volume is indeed FAT32 by counting clusters.
    auto root_ent_cnt = fstoh16(&_bpb.BPB_RootEntCnt);
    auto fat_sz_16 = fstoh16(&_bpb.BPB_FATSz16);
    auto fat_sz_32 = fstoh32(&_bpb.BPB_FATSz32);
    auto tot_sec_16 = fstoh16(&_bpb.BPB_TotSec16);
    auto tot_sec_32 = fstoh32(&_bpb.BPB_TotSec32);

    auto root_dir_sectors = ((root_ent_cnt * 32) + (_sector_size - 1)) / _sector_size;
    _fat_sz = fat_sz_16 != 0 ? fat_sz_16 : fat_sz_32;
    _first_data_sector = _resvd_sec_cnt + (_num_fats * _fat_sz) + root_dir_sectors;
    auto tot_sec = tot_sec_16 != 0 ? tot_sec_16 : tot_sec_32;
    auto data_sec = tot_sec - _first_data_sector;
    _count_of_clusters = data_sec / _sectors_per_cluster;
    _max_valid_cluster = _count_of_clusters + 1;

    if (_count_of_clusters <= fat16_max_clusters)
        throw filesystem_error("Volume has too few clusters to be FAT32");

    // The FS is now mounted.
    _mounted = true;
}

void fat32::unmount()
{
    if (_mounted)
    {
        sync();
        _mounted = false;
        filesystem = nullptr;
    }
}

cluster fat32::read_fat(cluster c)
{
    uint32_t entry;
    unsigned long offset = (unsigned long)c * sizeof(entry);
    sector s = _resvd_sec_cnt + (offset / _sector_size);
    unsigned ent_offset = offset % _sector_size;
    read_sector(s, ent_offset, &entry, sizeof(entry));
    cluster next = fstoh32(&entry) & 0x0FFFFFFF;
	return next;
}

cluster fat32::find_free_fat()
{
    // Find the first free cluster.
    cluster ret = cluster_free;
    for (cluster c = 2; c <= _max_valid_cluster; c++) {
        auto e = read_fat(c);
        if (e == cluster_free) {
            ret = c;
            break;
        }
    }
    if (ret == cluster_free)
        throw filesystem_error("out of space");

    return ret;
}

void fat32::write_fat(cluster c, cluster next)
{
    // Read-modify-write, preserving the upper 4 bits
    uint32_t entry;
    unsigned long offset = (unsigned long)c * sizeof(entry);
    unsigned ent_offset = offset % _sector_size;

    for (unsigned fat = 0; fat < _num_fats; fat++) {
        sector s = _resvd_sec_cnt + fat * _fat_sz + (offset / _sector_size);
        read_sector(s, ent_offset, &entry, sizeof(entry));
        next = (fstoh32(&entry) & 0xF0000000) | (next & 0x0FFFFFFF);
#ifdef FAT_DEBUG
        {
            ostream_fmt_save ofs{fat32::debug_stream()};
            fat32::debug_stream()
                << "fat32::write_fat: FAT" << fat
                << std::hex << std::showbase
                << '[' << c << "] <= " << next << "\n";
        }
#endif
        fswrite32(&entry, next);
        write_sector(s, ent_offset, &entry, sizeof(entry));
    }

    // TODO: Clone to FAT mirrors.
}

unsigned fat32::read_cluster(cluster cl, unsigned off, iop&& iop, unsigned count)
{
#ifdef FAT_DEBUG
    {
        ostream_fmt_save ofs{fat32::debug_stream()};
        fat32::debug_stream()
            << std::hex << std::showbase
            << "fat32::read_cluster(cluster=" << cl
            << ", off=" << off
            << ", count=" << count
            << ")\n";
    }
#endif
	sector s = cluster_to_sector(cl) + off / _sector_size;
    unsigned end = off + count;
    unsigned i = off;
    try {
        while (i < end)
        {
            unsigned sec_off = i % _sector_size;
            unsigned sec_count = std::min(_sector_size - sec_off, end - i);
            read_sector(s, sec_off, std::move(iop), sec_count);
            s ++;
            i += sec_count;
        }
    }
    catch (std::overflow_error &) {
        // out of buffers
    }
    return i - off;
}

void fat32::write_cluster(cluster cl, unsigned off, iop&& iop, unsigned count)
{
#ifdef FAT_DEBUG
    {
        ostream_fmt_save ofs{fat32::debug_stream()};
        fat32::debug_stream()
            << std::hex << std::showbase
            << "fat32::write_cluster(cluster=" << cl
            << ", off=" << off
            << ", count=" << count
            << ")\n";
    }
#endif
	sector s = cluster_to_sector(cl) + off / _sector_size;
    unsigned end = off + count;
    unsigned i = off;
    while (i < end)
    {
        unsigned sec_off = i % _sector_size;
        unsigned sec_count = std::min(_sector_size - sec_off, end - i);
        write_sector(s, sec_off, std::move(iop), sec_count);
        s ++;
        i += sec_count;
    }
}

void fat32::sync()
{
#ifdef FAT_DEBUG
    {
        fat32::debug_stream()
            << "fat32::sync()\n";
    }
#endif

    _dev->flush_writes();
}

void fat32::add_writer(int c)
{
    assert(c == -1 || c == 1);
    _writer_count += c;

    if (_writer_count == 0)
        sync();
}

sector fat32::cluster_to_sector(cluster cl) const
{
    sector s = ((cl - 2) * _sectors_per_cluster) + _first_data_sector;
    return s;
}

void fat32::sector_offset_to_block_offset(sector& s, unsigned& off) const
{
    // Translate (s, off) to (first sector in block, offset in block)
    auto so = s & (_spb - 1);
    off += so * fat::sector_size;
    s -= so;
}

void fat32::read_sector(sector s, unsigned off, iop&& iop, unsigned count)
{
    assert(off + count <= fat::sector_size);
    sector_offset_to_block_offset(s, off);
    auto bb = _dev->read_block(s);
    if (iop.is_vector())
        iop.get_iovec().push_back_sub(std::move(bb), off, count);
    else {
        auto* src = reinterpret_cast<uint8_t*>(bb->ptr) + off;
        auto*& dst = reinterpret_cast<uint8_t*&>(iop.get_buf());
        os::cpu::cpu.cache.invalidate(src, count);
        memcpy(dst, src, count);
        dst += count;
    }
}

void fat32::write_sector(sector s, unsigned off, iop&& iop, unsigned count)
{
    assert(off + count <= fat::sector_size);
    sector_offset_to_block_offset(s, off);
    auto bb = _dev->read_block(s);
    auto*& src = reinterpret_cast<uint8_t*&>(iop.get_buf());
    auto* dst = reinterpret_cast<uint8_t*>(bb->ptr) + off;
    memcpy(dst, src, count);
    // TODO [optimization]: delay cache clean to writing to actual device
    os::cpu::cpu.cache.clean(dst, count);
    _dev->write_block(s, bb);
    src += count;
}

} // namespace fat
