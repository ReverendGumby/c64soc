#include <string>
#include <vector>
#include <algorithm>
#include <string.h>
#include "directory.h"
#include "filesystem_error.h"
#include "fat32_priv.h"

namespace fat
{

directory::directory()
{
    // Empty directory
    _eod = true;
}

directory::directory(const directory_entry &dirent)
    : file(dirent)
{
    if (!dirent.is_dir)
        throw filesystem_error("not a directory");

    _eod = false;
}

void directory::reset_read()
{
    seek(0);
    _eod = false;
    _entry_index = 0;
}

directory directory::root()
{
    return directory(directory_entry::root());
}

bool directory::is_root() const
{
    return get_first_cluster() == fat32::get_filesystem()->root_cluster();
}

directory_entry directory::read()
{
    read_next();
    return _next;
}

template<typename Char>
void rtrim(std::basic_string<Char>& s, Char c = static_cast<Char>(' '))
{
    while (!s.empty() && s.back() == c)
        s.resize(s.size() - 1);
}

#ifdef FAT_HAVE_LONGNAME
static std::wstring build_long_name(const DIR& d, const std::vector<LDIR>& ldirs)
{
    try
    {
        if (ldirs.empty())                      // no long dir entries
            throw false;

        // Compute short name checksum.
        uint8_t chksum = 0;
        for (auto c : d.DIR_Name)
            chksum = ((chksum & 1) ? (1<<7) : 0) + (chksum >> 1) + c;

        // Process long dir entries in ordinal order.
        bool last = false;
        std::wstring longname;
        for (uint8_t ord = 1; !last && ord <= ldirs.size(); ord++)
        {
            const auto& ld = ldirs[ldirs.size() - ord];
            last = ld.LDIR_Ord & LDIR_ORD_LAST_LONG_ENTRY;
            uint8_t this_ord = ld.LDIR_Ord & ~LDIR_ORD_LAST_LONG_ENTRY;
            if (this_ord != ord || ld.LDIR_Chksum != chksum)
                throw false;
            for (auto x : ld.LDIR_Name1)
                longname.push_back(fstoh16(&x));
            for (auto x : ld.LDIR_Name2)
                longname.push_back(fstoh16(&x));
            for (auto x : ld.LDIR_Name3)
                longname.push_back(fstoh16(&x));
        }
        if (!last)                              // didn't find the last entry
            throw false;

        // Remove null terminator and any padding that follows.
        auto zt = longname.find_first_of(L'\0');
        if (zt != longname.npos)
            longname.resize(zt);
        return longname;
    }
    catch (bool)
    {
        return std::wstring();
    }
}
#endif

static std::string build_short_name(const DIR& d)
{
    auto main = std::string(&d.DIR_Name[SHORTNAME_BASE_OFF], SHORTNAME_BASE_LEN);
    auto ext = std::string(&d.DIR_Name[SHORTNAME_EXT_OFF], SHORTNAME_EXT_LEN);
    rtrim(main);
    rtrim(ext);

#ifdef FAT_HAVE_LONGNAME
    // From doc/fat32/fatplus.txt: DIR_NTRes[4:3] can alter case.
    if ((d.DIR_NTRes & DIR_NTRES_LCASE_MAIN) != 0)
        for (char& c : main)
            c = tolower(c);
    if ((d.DIR_NTRes & DIR_NTRES_LCASE_EXT) != 0)
        for (char& c : ext)
            c = tolower(c);
#endif

    auto name = main;
    if (!ext.empty())
        name += '.' + ext;
    return name;
}

struct directory::sldirs_t {
    fileoff off;
    DIR d;
#ifdef FAT_HAVE_LONGNAME
    std::vector<LDIR> ldirs;
#endif

#ifdef FAT_HAVE_LONGNAME
    size_t dir_count() const
    {
        return 1 + ldirs.size();
    }
#else
    constexpr size_t dir_count() const
    {
        return 1;
    }
#endif
};

bool directory::read_next(sldirs_t* psld)
{
#ifdef FAT_DEBUG
    fat32::debug_stream() << "directory::read_next()\n";
#endif

    if (_eod)
        return false;

    sldirs_t sld;
    sld.off = tell();
    DIR& d = sld.d;
#ifdef FAT_HAVE_LONGNAME
    std::vector<LDIR>& ldirs = sld.ldirs;
#endif
    for (;;)
    {
        if (file::read(&d, sizeof(d)) == 0
            || d.DIR_Name[0] == DIR_NAME_EOD)
        {
            _eod = true;
            return false;
        }
        if (d.DIR_Name[0] == DIR_NAME_FREE)
        {
#ifdef FAT_HAVE_LONGNAME
            ldirs.clear();
#endif
            sld.off = tell();
            continue;
        }
        if ((d.DIR_Attr & DIR_ATTR_MASK) != DIR_ATTR_LONG_NAME)
            break;

#ifdef FAT_HAVE_LONGNAME
        auto ld = reinterpret_cast<const LDIR&>(d);
        ldirs.push_back(ld);
#endif
    }
#ifdef FAT_HAVE_LONGNAME
    std::wstring longname = build_long_name(d, ldirs);
    _next.shortname = build_short_name(d);

    if (longname.size())
        _next.name = longname;
    else
        _next.name = _next.shortname;
#else
    _next.name = build_short_name(d);
#endif
    _next.idx = _entry_index ++;
    _next.is_dir = d.DIR_Attr & DIR_ATTR_DIRECTORY;
    _next.is_hidden = d.DIR_Attr & DIR_ATTR_HIDDEN;
    _next.first_cluster = (fstoh16(&d.DIR_FstClusHI) << 2*8)
        | (fstoh16(&d.DIR_FstClusLO) << 0*8);
    _next.size = _next.is_dir ? -1 : fstoh32(&d.DIR_FileSize);
    _next.dir_first_cluster = get_first_cluster();
    _next.dir_offset = sld.off + (sld.dir_count() - 1) * sizeof(DIR);

    if (psld)
        *psld = sld;
    return true;
}

size_t directory::size()
{
    reset_read();
    size_t ret;
    for (ret = 0; !is_eod(); ret ++)
        read();
    return ret;
}

bool directory::has_shortname(const char* name)
{
    sldirs_t sld;
    while (read_next(&sld)) {
        if (memcmp(name, sld.d.DIR_Name, sizeof(sld.d.DIR_Name)) == 0)
            return true;
    }
    return false;
}

enum {
    lower_case = 1,
    upper_case = 2,
    mixed_case = lower_case | upper_case
};

template <class String>
int get_string_case(const String& s)
{
    int ret = 0;

    for (auto ch : s) {
        if (ch >= 'A' && ch <= 'Z')
            ret |= upper_case;
        else if (ch >= 'a' && ch <= 'z')
            ret |= lower_case;
    }

    return ret;
}

bool is_filename_valid(const path& fnpath, bool must_be_short)
{
#ifndef FAT_HAVE_LONGNAME
    must_be_short = true;
#endif
    const path::string_type& fn = fnpath.native();

    if (fn.empty())
        return false;

    if (must_be_short) {
        // Names cannot start with a space.
        if (fn[0] == ' ')
            return false;
        // Short file names are limited to 8 characters followed by an optional
        // period (“.”) and extension of up to 3 characters.
        if (count(fn.begin(), fn.end(), '.') > 1)
            return false;
        const auto stem = fnpath.stem().native();
        auto ext = fnpath.extension().native();
        if (!ext.empty())                       // extension() includes '.'
            ext.erase(ext.begin());
        if (stem.size() > 8 || ext.size() > 3)
            return false;
        for (auto ch : fn) {
            // Of the ASCII symbols, only these are allowed:
            // ! # $ % & ' ( ) - . @ ^ _ ` { } ~
            const std::string symbols { "\"*+,/:;<=>?[\\]|" };
            if (ch < 32 || ch == 127 || ch > 255 ||
                (ch < 127 && symbols.find_first_of(char(ch)) != symbols.npos))
                return false;
        }
        // Both stem and ext must not have mixed case.
        if (get_string_case(stem) == mixed_case ||
            get_string_case(ext) == mixed_case)
            return false;
    } else {
        if (fn.size() > 255)
            return false;
        for (auto ch : fn) {
            // Of the ASCII symbols, only these are allowed:
            // ! # $ % & ' ( ) + , - . ; = @ [ ] ^ _ ` { } ~
            const std::string symbols { "\"*/:<>?\\|" };
            if (ch < 32 || ch == 127 ||
                (ch < 127 && symbols.find_first_of(char(ch)) != symbols.npos))
                return false;
        }
    }
    return true;
}

void make_dir_for_short_name(const path& fnpath, DIR& d)
{
    memset(d.DIR_Name, ' ', sizeof(d.DIR_Name));

    auto stem = fnpath.stem().string();
    if (get_string_case(stem) == lower_case) {
        for (char& c : stem)
            c = toupper(static_cast<unsigned char>(c));
        d.DIR_NTRes |= DIR_NTRES_LCASE_MAIN;
    }
    stem.copy(&d.DIR_Name[0], 8);

    auto ext = fnpath.extension().string();
    if (!ext.empty())                       // extension() includes '.'
        ext.erase(ext.begin());
    if (get_string_case(ext) == lower_case) {
        for (char& c : ext)
            c = toupper(static_cast<unsigned char>(c));
        d.DIR_NTRes |= DIR_NTRES_LCASE_EXT;
    }
    ext.copy(&d.DIR_Name[8], 3);
}

// Reference: https://en.wikipedia.org/wiki/8.3_filename#VFAT_and_computer-generated_8.3_filenames
void directory::make_dir_for_long_name(const path& fnpath, DIR& d)
{
    bool uniquify = false;

    auto name = fnpath.native();
    size_t name_len = name.size();
    size_t dot_pos = fnpath.stem().size();

    memset(d.DIR_Name, ' ', sizeof(d.DIR_Name));
    char* pdn = d.DIR_Name;

    for (size_t i = 0; i < name_len; i++) {
        auto ch = name[i];
        if (i == dot_pos) {
            // Reached period between stem and extension
            pdn = &d.DIR_Name[8];
        }
        else if (i < dot_pos && pdn == &d.DIR_Name[8]) {
            // Stem is too long
            uniquify = true;
            i = dot_pos;
        }
        else if (i > dot_pos && pdn == &d.DIR_Name[11]) {
            // Extension is too long
            uniquify = true;
            break;
        }
        else if (ch == ' ' || ch == '.') {
            // Strip spaces, periods
            uniquify = true;
        }
        else {
            // Replace long name-only symbols with underscore
            const std::string symbols { "+,;=[]" };
            if ((ch < 127 && symbols.find_first_of(char(ch)) != symbols.npos) ||
                ch >= 128) {
                uniquify = true;
                ch = '_';
            }
            *(pdn++) = toupper(static_cast<unsigned char>(ch));
        }
    }

    // If required, append the tilde-number
    if (uniquify) {
        // Find the end of the stem
        char* pdn = d.DIR_Name;
        size_t i;
        for (i = 6; i > 0; i--) {
            if (pdn[i - 1] != ' ')
                break;
        }
        pdn[i++] = '~';
        pdn[i] = '1';

        while (has_shortname(d.DIR_Name)) {
            // Increment tilde-number to ensure uniqueness
            if (pdn[i] == '9')
                throw filesystem_error("unable to uniquify shortname");
            pdn[i] ++;
        }
    }
}

#ifdef FAT_HAVE_LONGNAME
void make_ldirs_for_long_name(const path& fnpath, DIR& d, std::vector<LDIR>& ldirs)
{
    // Compute short name checksum.
    uint8_t chksum = 0;
    for (auto c : d.DIR_Name)
        chksum = ((chksum & 1) ? (1<<7) : 0) + (chksum >> 1) + c;

    // Append null terminator and padding
    auto ln = fnpath.native();
    ln.push_back(L'\0');
    while (ln.size() % 13)
        ln.push_back(L'\uFFFF');

    auto ln_len = ln.size();
    auto* pln = &ln[0];
    int last_ord = ln_len / 13;
    for (int ord = 1; ord <= last_ord ; ord++) {
        LDIR ld {};
        ld.LDIR_Attr = DIR_ATTR_LONG_NAME;
        ld.LDIR_Ord = ord;
        if (ord == last_ord)
            ld.LDIR_Ord |= LDIR_ORD_LAST_LONG_ENTRY;
        ld.LDIR_Chksum = chksum;

        for (size_t i = 0; i < 5; i++)
            fswrite16(&ld.LDIR_Name1[i], *(pln++));
        for (size_t i = 0; i < 6; i++)
            fswrite16(&ld.LDIR_Name2[i], *(pln++));
        for (size_t i = 0; i < 2; i++)
            fswrite16(&ld.LDIR_Name3[i], *(pln++));

        ldirs.insert(ldirs.begin(), ld);
    }
}
#endif

directory::sldirs_t directory::build_sldirs(const path& fnpath)
{
    sldirs_t ret {};
    DIR& d = ret.d;

    if (!is_filename_valid(fnpath, false))
        throw filesystem_error("not a valid filename");

#ifdef FAT_HAVE_LONGNAME
    if (is_filename_valid(fnpath, true)) {
        make_dir_for_short_name(fnpath, d);
    } else {
        make_dir_for_long_name(fnpath, d);
        make_ldirs_for_long_name(fnpath, d, ret.ldirs);
    }
#else
    make_dir_for_short_name(fnpath, d);
#endif

    return ret;
}

// Scan the entire directory for a long enough run of free space to store all
// entries.
int directory::add_scan(directory::sldirs_t& sld)
{
    DIR d;
    int free_start;
    int free_cnt = 0;
    int need = sld.dir_count();

    reset_read();

    for (int i = 0; ; i++) {
        if (file::read(&d, sizeof(d)) == 0
            || d.DIR_Name[0] == DIR_NAME_EOD) {
            // We've reached the end
            _eod = true;
            free_start = i;
            break;
        }

        if (d.DIR_Name[0] == DIR_NAME_FREE) {
            if (free_cnt == 0)
                free_start = i;
            free_cnt ++;

            if (free_cnt == need)
                break;
        } else {
            free_cnt = 0;
        }
    }

    return free_start;
}

void directory::update(directory_entry& de)
{
#ifdef FAT_DEBUG
    fat32::debug_stream() << "directory::update()\n";
#endif

    // Open the directory that contains this entry.
    directory_entry dir_de = directory_entry::dir();
    dir_de.first_cluster = de.dir_first_cluster;
    directory dir { dir_de };

    // Read the current entry
    DIR d;
    dir.seek(de.dir_offset);
    if (file(dir).read(&d, sizeof(d)) != sizeof(d))
        throw filesystem_error("read error on dir update");

    // Apply changes
    fswrite16(&d.DIR_FstClusHI, de.first_cluster >> 2*8);
    fswrite16(&d.DIR_FstClusLO, de.first_cluster >> 0*8);
    fswrite32(&d.DIR_FileSize, de.size);

    // Write the new entry
    dir.seek(de.dir_offset);
    file(dir).write(&d, sizeof(d));
}

void directory::add(directory_entry& de)
{
#ifdef FAT_DEBUG
    fat32::debug_stream() << "directory::add()\n";
#endif

    // Build all entries (DIR + LDIRs) needed to hold the filename
    sldirs_t sld = build_sldirs(de.name);

    // Find where to add them
    int i = add_scan(sld);
    bool reached_eod = _eod;

    // Write the entries
    file::seek(i * sizeof(DIR));
#ifdef FAT_HAVE_LONGNAME
    for (const auto& ldir : sld.ldirs)
        write(&ldir, sizeof(ldir));
#endif
    write(&sld.d, sizeof(sld.d));

    // Move the EOD marker if needed
    if (reached_eod && !eof()) {
        char eod_marker = DIR_NAME_EOD;
        write(&eod_marker, sizeof(eod_marker));
    }
}

bool directory::remove(const path& filename)
{
#ifdef FAT_DEBUG
    fat32::debug_stream() << "directory::remove()\n";
#endif

    reset_read();

    if (!is_root()) {
        // Skip dot and dotdot entries
        read_next();
        read_next();
    }

    bool found = false;
    sldirs_t sld;
    while (read_next(&sld)) {
        if (_next.name == filename) {
            found = true;
            break;
        }
    }

    if (found) {
        if (_next.is_dir)
            throw filesystem_error("directory may not be removed");

        // Set the first byte of each directory entry to the free marker
        int cnt = sld.dir_count();
        for (int i = 0; i < cnt; i++) {
            seek(sld.off + i * sizeof(DIR));
            char free = DIR_NAME_FREE;
            write(&free, 1);
        }
    }

    return found;
}

} // namespace fat
