#ifndef PARTITIONER_H_
#define PARTITIONER_H_

#include <stdint.h>
#include <vector>
#include "fat32.h"
#include "partition.h"
#include "block_device.h"

namespace fat
{

class partitioner
{
public:
    struct part_dev
    {
        partition part;
        std::shared_ptr<block_device> dev;
    };

    partitioner();
    virtual ~partitioner();

    void load(std::shared_ptr<block_device> dev);

    const std::vector<part_dev>& partitions() { return _part_devs; }

private:
    class translated_block_device : public block_device
    {
    public:
        translated_block_device();
        translated_block_device(std::shared_ptr<block_device> dev,
                                const partition& part);
        virtual ~translated_block_device();

        virtual unsigned get_sectors_per_block() const;
        virtual sector get_capacity() const;
        virtual block_buffer_t read_block(sector s);
        virtual void write_block(sector s, const block_buffer_t&);
        virtual void flush_writes();
    private:
        sector translate(sector s) const;

        std::shared_ptr<block_device> _dev;
        partition _part;
    };

    void add(std::shared_ptr<block_device> dev, const MBR_PE& pe);

    std::vector<part_dev> _part_devs;
};

} // namespace fat

#endif /* PARTITIONER_H_ */
