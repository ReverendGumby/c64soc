#include "fifo_block_cache.h"

namespace fat
{

fifo_block_cache::fifo_block_cache(std::shared_ptr<block_device> dev, unsigned num_blocks)
    : block_cache(dev, num_blocks)
{
}

fifo_block_cache::~fifo_block_cache()
{
}

fifo_block_cache::block_buffer_t fifo_block_cache::read_block(sector s)
{
    auto i = _cache.find(s);
    if (i == _cache.end()) {
        while (_cache.size() >= _num_blocks)
            _cache.erase(_cache.cbegin());

        auto p = _cache.emplace(std::make_pair(s, _dev->read_block(s)));
        i = p.first;
    }
    return i->second;
}

} // namespace fat
