#ifndef PATH_H_
#define PATH_H_

#ifndef FSBL
#define FAT_HAVE_LONGNAME
#endif

#include <initializer_list>
#include <iterator>
#ifdef FAT_HAVE_LONGNAME
#include <locale>
#include "util/wstring.h"
#endif
#include <string>
#include <ostream>
#include "filesystem_error.h"

namespace fat
{

class path
{
public:
#ifdef FAT_HAVE_LONGNAME
    using value_type = wchar_t;
    using string_type = std::wstring;
#else
    using value_type = char;
    using string_type = std::string;
#endif

    static constexpr value_type preferred_separator = '\\';

    // ctors and dtors
    path() {}
    path(const path& p) = default;
    explicit path(path&& p) noexcept;
    explicit path(const value_type* s) : _pathname{s} {}
    explicit path(const string_type& s) : _pathname{s} {}
    explicit path(std::initializer_list<value_type> l) : _pathname{l} {}
    //explicit path(const char* x) : path(std::string{x}) {}
#ifdef FAT_HAVE_LONGNAME
    //template <class Source>
    //explicit path(Source const& source, const codecvt_type& cvt = codecvt());
    explicit path(std::string const& source, const codecvt_type& cvt = codecvt());
    // template <class InputIterator>
    // path(InputIterator begin, InputIterator end,
    //      const codecvt_type& cvt = codecvt());
#endif

    virtual ~path() {}

    // assignments
    path& operator=(const path& p) = default;
    path& operator=(path&& p) noexcept;
    path& operator=(const string_type& s) { _pathname = s; return *this; }
    template <class Source>
    path& operator=(Source const& source);
#ifdef FAT_HAVE_LONGNAME
    template <class Source>
    path& assign(Source const& source, const codecvt_type& cvt);
    // template <class InputIterator>
    // path& assign(InputIterator begin, InputIterator end,
    //              const codecvt_type& cvt = codecvt());
#endif

    // appends
    path& operator/=(const string_type& x) { return append(path(x)); }
    path& operator/=(const value_type* x) { return append(path(x)); }
    path& operator/=(const path& p) { return append(p); }
    path& append(const string_type& x) { return append(path(x)); }
    path& append(const value_type* x) { return append(path(x)); }
    path& append(const path& p);
    template <class InputIterator>
    path& append(InputIterator begin, InputIterator end);
#ifdef FAT_HAVE_LONGNAME
    template <class Source>
    path& append(Source const& source, const codecvt_type& cvt);
    template <class InputIterator>
    path& append(InputIterator begin, InputIterator end,
                 const codecvt_type& cvt);
#endif

    // concatenation
    path& operator+=(const path& x);
    path& operator+=(const string_type& x);
    path& operator+=(const value_type* x);
    path& operator+=(value_type x);
    template <class Source>
    path& operator+=(Source const& x);
    template <class CharT>
    path& operator+=(CharT x);
#ifdef FAT_HAVE_LONGNAME
    template <class Source>
    path& concat(Source const& x, const codecvt_type& cvt);
    // template <class InputIterator>
    // path& concat(InputIterator begin, InputIterator end);
    // template <class InputIterator>
    // path& concat(InputIterator begin, InputIterator end,
    //              const codecvt_type& cvt);
#endif

    // modifiers
    void clear();
    path& make_preferred();
    path& remove_filename();
    path& replace_extension(const path& new_extension = path());
    void swap(path& rhs);

    // lexical operations
    path lexically_normal() const;
    // path lexically_relative(const path& base) const;

    // native format observers
    const string_type& native() const noexcept { return _pathname; }
    const value_type* c_str() const noexcept { return _pathname.c_str(); }
    string_type::size_type size() const noexcept { return _pathname.size(); }

#ifdef FAT_HAVE_LONGNAME
    std::string string(const codecvt_type& cvt=codecvt()) const;
#else
    std::string string() const { return native(); }
#endif

    // generic format observers
    // template <class String>
    // String generic_string() const;
#ifdef FAT_HAVE_LONGNAME
    std::string generic_string(const codecvt_type& cvt=codecvt()) const;
#endif

    // compare
    int compare(const path& p) const noexcept;
    int compare(const path::string_type& s) const { return compare(path{s}); }
#ifdef FAT_HAVE_LONGNAME
    int compare(const std::string& s) const { return compare(path{s}); }
#endif
    int compare(const value_type* s) const { return compare(path{s}); }

    // decomposition
    path root_name() const;
    path root_directory() const;
    path root_path() const;
    path relative_path() const;
    path parent_path() const;
    path filename() const;
    path stem() const;
    path extension() const;

    // query
    bool empty() const;
    bool has_root_name() const;
    bool has_root_directory() const;
    bool has_root_path() const;
    bool has_relative_path() const;
    bool has_parent_path() const;
    bool has_filename() const;
    bool has_stem() const;
    bool has_extension() const;
    bool is_absolute() const;
    bool is_relative() const { return !is_absolute(); }

    // iterators
    class iterator;    
    using const_iterator = iterator;
    class reverse_iterator;
    using const_reverse_iterator = reverse_iterator;

    iterator begin() const;
    iterator end() const;
    reverse_iterator rbegin() const;
    reverse_iterator rend() const;

    // template <class Char, class Traits>
    // friend std::basic_ostream<Char, Traits>&
    // operator<<(std::basic_ostream<Char, Traits>& os, const path& p);

    // imbued locale
    //static std::locale imbue(const std::locale& loc);

private:
    string_type _pathname;
};

class path::iterator
    : public std::iterator<std::input_iterator_tag, const path>
{
public:
    iterator() = default;
    iterator(const iterator&) = default;
    iterator(iterator&&) = default;
    iterator& operator=(const iterator&) = default;
    reference operator*() const { return _e; }
    pointer operator->() const { return &_e; }

    iterator& operator++() { increment(); return *this; }
    iterator operator++(int) { auto ret = *this; increment(); return ret; }

    friend bool operator==(const iterator& lhs, const iterator& rhs)
    {
        return lhs._path == rhs._path && lhs._pos == rhs._pos;
    }
    friend bool operator!=(const iterator& lhs, const iterator& rhs)
    {
        return !(lhs == rhs);
    }

private:
    friend class path;

    explicit iterator(const path* p)
        : _path(p) {}
    void increment();

    const path* _path;                       // path being iterator'd
    path _e;                                 // current element
    string_type::size_type _pos;             // pos of _e in _path._pathname
};

template <class Source>
path& path::operator=(Source const& source)
{
    path p{source};
    _pathname = p._pathname;
    return *this;
}

#ifdef FAT_HAVE_LONGNAME
template <class Source>
path& path::assign(Source const& source, const codecvt_type& cvt)
{
    path p{source, cvt};
    _pathname = p._pathname;
    return *this;
}

template <class Source>
path& path::append(Source const& source, const codecvt_type& cvt)
{
    path p{source, cvt};
    return append(p);
}
#endif

template <class InputIterator>
path& path::append(InputIterator begin, InputIterator end)
{
    for (auto i = begin; i != end; i++)
    {
        const auto ii = *i;
        append(ii);
    }
    return *this;
}

#ifdef FAT_HAVE_LONGNAME
template <class InputIterator>
path& path::append(InputIterator begin, InputIterator end,
                   const codecvt_type& cvt)
{
    for (auto i = begin; i != end; i++)
    {
        const auto ii = *i;
        append(ii, cvt);
    }
    return *this;
}
#endif

template <class Source>
path& path::operator+=(Source const& x)
{
    path p{x};
    _pathname.append(p._pathname);
    return *this;
}

template <class CharT>
path& path::operator+=(CharT x)
{
    std::basic_string<CharT> s{x};
    path p{s};
    _pathname.append(p._pathname);
    return *this;
}

#ifdef FAT_HAVE_LONGNAME
template <class Source>
path& path::concat(Source const& x, const codecvt_type& cvt)
{
    path p{x, cvt};
    _pathname.append(p._pathname);
    return *this;
}
#endif

inline bool operator==(const path& lhs, const path& rhs) { return lhs.compare(rhs) == 0; }
inline bool operator==(const path& lhs, const path::string_type& rhs) { return lhs.compare(rhs) == 0; }
inline bool operator==(const path::string_type& lhs, const path& rhs) { return rhs.compare(lhs) == 0; }
inline bool operator==(const path& lhs, const path::value_type* rhs) { return lhs.compare(rhs) == 0; }
inline bool operator==(const path::value_type* lhs, const path& rhs) { return rhs.compare(lhs) == 0; }

inline bool operator!=(const path& lhs, const path& rhs) { return lhs.compare(rhs) != 0; }
inline bool operator!=(const path& lhs, const path::string_type& rhs) { return lhs.compare(rhs) != 0; }
inline bool operator!=(const path::string_type& lhs, const path& rhs) { return rhs.compare(lhs) != 0; }
inline bool operator!=(const path& lhs, const path::value_type* rhs) { return lhs.compare(rhs) != 0; }
inline bool operator!=(const path::value_type* lhs, const path& rhs) { return rhs.compare(lhs) != 0; }

inline bool operator< (const path& lhs, const path& rhs) { return lhs.compare(rhs) < 0; }
inline bool operator<=(const path& lhs, const path& rhs) { return !(rhs < lhs); }
inline bool operator> (const path& lhs, const path& rhs) { return rhs < lhs; }
inline bool operator>=(const path& lhs, const path& rhs) { return !(lhs < rhs); }

inline path operator/(const path& lhs, const path& rhs) { return path(lhs) /= rhs; }

#ifndef FSBL
template <class Traits>
inline std::basic_ostream<path::value_type, Traits>&
operator<<(std::basic_ostream<path::value_type, Traits>& os, const path& p)
{
    constexpr auto q = L'"';
    return os << q << p.native() << q;
}

inline std::ostream& operator<<(std::ostream& os, const path& p)
{
    constexpr char q = '"';
    return os << q << p.string() << q;
}
#endif

} // namespace fat

#endif /* PATH_H_ */
