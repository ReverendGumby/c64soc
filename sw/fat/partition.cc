#include "partition.h"
#include "fat32_priv.h"

namespace fat {

partition::partition(const MBR_PE& pe)
{
    _raw_type = pe.type;
    switch (_raw_type)
    {
    case MBR_TYPE_FREE:         _type = type::free;     break;
    case MBR_TYPE_FAT32_CHS:
    case MBR_TYPE_FAT32_LBA:    _type = type::fat32;    break;
    default:                    _type = type::unknown;  break;
    }
    sector count = fstoh32(&pe.num_sectors);
    _first = fstoh32(&pe.first_lba);
    _last = _first + count - 1;
}

} // namespace fat
