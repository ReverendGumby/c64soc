#include <algorithm>
#include <assert.h>
#ifdef FAT_DEBUG
#include "util/ostream_fmt_save.h"
#endif
#include "util/iovec.h"
#include "file.h"
#include "fat32.h"
#include "filesystem_error.h"
#include "directory_iterator.h"
#include "directory_entry.h"

namespace fat
{

file::file()
{
}

file::file(const file& that)
    : file()
{
    _de = that._de;
    _first_cluster = that._first_cluster;
    _cluster = that._cluster;
    _prev_cluster = that._prev_cluster;
    _size = that._size;
    _pos = that._pos;
}

file::file(file&& that)
    : file()
{
    *this = std::move(that);
}

file::file(const path& path)
    : file(resolve_path(path))
{
    if (_de.is_dir)
        throw filesystem_error("not a file");
}

file::file(const directory_entry &dirent)
    : file()
{
    _de = dirent;

    _first_cluster = dirent.first_cluster;
    if (_first_cluster == fat32::cluster_free)
        _first_cluster = fat32::cluster_eof;
    _cluster = _first_cluster;
    _prev_cluster = _cluster;

    fileoff size{dirent.size};
    _size = size;
}

file::~file()
{
    if (_modified) {
        try {
            auto fslck = fat32::get_filesystem_with_lock();
            auto& fs = *fslck.first;
            fs.add_writer(-1);
        } catch (filesystem_error& e) {
            // Oh, well; we tried.
        }
    }
}

file& file::operator=(file&& that)
{
    _de = that._de;
    _first_cluster = that._first_cluster;
    _cluster = that._cluster;
    _prev_cluster = that._prev_cluster;
    _size = that._size;
    _pos = that._pos;
    return *this;
}

unsigned file::read(iop&& iop, unsigned len)
{
    auto fslck = fat32::get_filesystem_with_lock();
    auto& fs = *fslck.first;

#ifdef FAT_DEBUG
    {
        ostream_fmt_save ofs{fat32::debug_stream()};
        fat32::debug_stream()
            << std::hex << std::showbase
            << "file::read(len=" << len << "): _pos=" << _pos << '\n';
    }
#endif

    if (iop.is_vector())
        iop.get_iovec().clear();

    unsigned ret, read_req = 0, read = 0;
    unsigned cluster_size = fs.cluster_size();
    unsigned cluster_pos = _pos % cluster_size;

    for (ret = 0; ret < len && read == read_req; ret += read)
    {
        read = std::min(len - ret, cluster_size - cluster_pos);
        if (read == 0)
            break;
        if (_size >= 0)
        {
            fileoff file_remain = _size - _pos;
            if (read > file_remain)
                read = file_remain;
            if (read == 0)
                break;
        }
        else
        {
            // File size is unknown (i.e., we're a directory). Read until an
            // end-of-file cluster is found.
            if (_cluster == fat32::cluster_eof)
                break;
        }
        read_req = read;
        read = fs.read_cluster(_cluster, cluster_pos, std::move(iop), read_req);
        cluster_pos += read;
        if (cluster_pos == cluster_size)
        {
            next_cluster(fs);
            cluster_pos = 0;
        }
        _pos += read;
    }
    return ret;
}

void file::seek(fileoff off, seekfrom from)
{
    auto fslck = fat32::get_filesystem_with_lock();
    auto& fs = *fslck.first;

    seek(fs, off, from);
}

void file::seek(fat32& fs, fileoff off, seekfrom from)
{
    fileoff new_pos;
    switch (from)
    {
    case seekfrom::start:   new_pos = off;          break;
    case seekfrom::cur:     new_pos = _pos + off;   break;
    case seekfrom::end:     new_pos = _size - off;  break;
    default:
        throw filesystem_error("bad from");
    }
#ifdef FAT_DEBUG
    {
        ostream_fmt_save ofs{fat32::debug_stream()};
        fat32::debug_stream()
            << std::hex << std::showbase
            << "file::seek(off=" << off << ", from="
            << ((from == seekfrom::start) ? "start" :
                ((from == seekfrom::cur) ? "cur" : "end"))
            << "): _pos=" << _pos << ", new_pos=" << new_pos << '\n';
    }
#endif
    if (new_pos < 0 || (_size >= 0 && new_pos > _size))
        throw filesystem_error("bad seek off/from");

    if (new_pos < _pos)
    {
        // Clusters link forward. Rewind to the beginning.
        _pos = 0;
        _cluster = _first_cluster;
        _prev_cluster = _cluster;
    }
    if (new_pos > _pos)
    {
        unsigned cluster_size = fs.cluster_size();
        cluster pos_clusters = _pos / cluster_size;
        cluster new_pos_clusters = new_pos / cluster_size;
        while (pos_clusters < new_pos_clusters)
        {
            next_cluster(fs);
            pos_clusters ++;
        }
        _pos = new_pos;
    }
}

bool file::eof() const
{
    return (_size >= 0) ? _pos >= _size : _cluster == fat32::cluster_eof;
}

void file::set_modified()
{
    if (_modified)
        return;
    _modified = true;

    auto fslck = fat32::get_filesystem_with_lock();
    auto& fs = *fslck.first;
    fs.add_writer(1);
}

void file::write(iop&& iop, unsigned len)
{
    auto fslck = fat32::get_filesystem_with_lock();
    auto& fs = *fslck.first;

#ifdef FAT_DEBUG
    {
        ostream_fmt_save ofs{fat32::debug_stream()};
        fat32::debug_stream()
            << std::hex << std::showbase
            << "file::write(len=" << len << "): _pos=" << _pos << '\n';
    }
#endif

    unsigned write = 0;
    const unsigned cluster_size = fs.cluster_size();
    unsigned cluster_pos = _pos % cluster_size;

    for (; len; len -= write)
    {
        write = std::min(len, cluster_size - cluster_pos);
        if (_pos + write > _size)
            grow(fs, write);                    // writing past the end
        fs.write_cluster(_cluster, cluster_pos, std::move(iop), write);
        set_modified();
        cluster_pos += write;
        if (cluster_pos == cluster_size)
        {
            next_cluster(fs);
            cluster_pos = 0;
        }
        _pos += write;
    }
}

void file::next_cluster(fat32& fs)
{
    _prev_cluster = _cluster;
    _cluster = fs.read_fat(_cluster);
}

void file::grow(fat32& fs, unsigned inc)
{
    if (_cluster == fat32::cluster_eof) {
        // Allocate a new cluster from the FAT.
        assert(_cluster == fat32::cluster_eof);
        _cluster = fs.find_free_fat();
#ifdef FAT_DEBUG
        fat32::debug_stream() << "file::grow: add cluster " << _cluster << "\n";
#endif

        // Mark it as the new EOF.
        fs.write_fat(_cluster, fat32::cluster_eof);
        set_modified();

        if (_first_cluster == fat32::cluster_eof) {
            // File was empty; point directory to our first cluster.
            _de.first_cluster = _first_cluster = _cluster;
        } else {
            // Link the previous cluster to the new one.
            fs.write_fat(_prev_cluster, _cluster);
        }
    }

    if (_size >= 0) {
        _size = _pos + inc;
        _de.size = filesize(_size);
        directory::update(_de);
    }
}

void file::truncate()
{
    auto fslck = fat32::get_filesystem_with_lock();
    auto& fs = *fslck.first;

#ifdef FAT_DEBUG
    fat32::debug_stream() << "file::truncate\n";
#endif
    assert(_size >= 0); // no directories allowed

    // Seek to the beginning.
    seek(fs, 0);

    // Free all clusters allocated on the FAT.
    while (_cluster != fat32::cluster_eof) {
        next_cluster(fs);
        fs.write_fat(_prev_cluster, fat32::cluster_free);
    }
    set_modified();

    // Zero file size and first cluster.
    _size = 0;
    _first_cluster = fat32::cluster_eof;
    _de.size = filesize(_size);
    _de.first_cluster = fat32::cluster_free;

    directory::update(_de);
}

} // namespace fat
