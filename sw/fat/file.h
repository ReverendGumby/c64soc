#ifndef FILE_H_
#define FILE_H_

#include <memory>
#include <stdint.h>
#include "util/iop.h"
#include "fat32.h"
#include "fat32_priv.h"
#include "directory_entry.h"

namespace fat
{

enum class seekfrom { start, cur, end };

class file
{
public:
	file();
    file(const file&);
    file(file&&);
    explicit file(const path &path);
	virtual ~file();

    file& operator=(file&&);

    fileoff size() { return _size; }
    unsigned read(iop&& iop, unsigned len);
    void seek(fileoff off, seekfrom from = seekfrom::start);
    fileoff tell() { return _pos; }
    bool eof() const;
    void write(iop&& iop, unsigned len);
    void truncate();

protected:
    explicit file(const directory_entry &dirent);

    cluster get_first_cluster() const { return _first_cluster; }
    const directory_entry& get_directory_entry() const { return _de; }

private:
    void set_modified();
    void seek(fat32& fs, fileoff off, seekfrom from = seekfrom::start);
    void next_cluster(fat32&);
    void grow(fat32& fs, unsigned inc);

    std::shared_ptr<fat32> _fs;

    // The directory entry that points to us
    directory_entry _de;

    cluster _first_cluster = fat32::cluster_eof;
    cluster _cluster = fat32::cluster_eof;
    cluster _prev_cluster;

    fileoff _size {-1};
    fileoff _pos {0};

    bool _modified = false;
};

} // namespace fat

#endif /* FILE_H_ */
