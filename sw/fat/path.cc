#include <utility>
#include "filesystem_error.h"
#include "path.h"

namespace fat
{

constexpr path::value_type preferred_separator_string[] {'\\', 0};
constexpr path::value_type dot{'.'};
static const path dot_path{'.'};
static const path dot_dot_path{'.', '.'};

path::path(path&& p) noexcept
{
    _pathname = std::move(p._pathname);
}

#ifdef FAT_HAVE_LONGNAME
path::path(std::string const& source, const codecvt_type& cvt)
{
    if (!string_to_wstring(source, _pathname, cvt))
		throw filesystem_error("unable to convert path");
}
#endif

path& path::operator=(path&& p) noexcept
{
    _pathname = std::move(p._pathname);
    return *this;
}

path& path::append(const path& p)
{
    if (p.is_absolute() ||
        (p.has_root_name() && p.root_name() != root_name())) {
        _pathname = p._pathname;
    } else {
        if (has_filename() || (!has_root_directory() && is_absolute()))
            _pathname += preferred_separator;
        _pathname += p._pathname;
    }
    return *this;
}

path& path::operator+=(const path& x)
{
    _pathname.append(x._pathname);
    return *this;
}

path& path::operator+=(const path::string_type& x)
{
    _pathname.append(x);
    return *this;
}

path& path::operator+=(const path::value_type* x)
{
    _pathname.append(x);
    return *this;
}

path& path::operator+=(path::value_type x)
{
    _pathname.push_back(x);
    return *this;
}

void path::clear()
{
    _pathname.clear();
}

path& path::make_preferred()
{
    // Nah...
    return *this;
}

path& path::remove_filename()
{
    if (!empty()) {
        auto i = _pathname.find_last_of(preferred_separator);
        if (i == _pathname.npos)
            i = 0;
        else
            i = i + 1;
        _pathname.resize(i);
    }
    return *this;
}

path& path::replace_extension(const path& new_extension)
{
    if (!empty())
    {
		// Find leaf
		string_type::size_type leaf;
		for (leaf = size(); leaf != 0; leaf--)
			if (_pathname[leaf - 1] == preferred_separator)
				break;

		// Find last dot in leaf
		string_type::size_type idot;
		for (idot = size() - 1; idot > leaf; idot--)
			if (_pathname[idot] == dot)
			{
				// Truncate from dot on
				_pathname.resize(idot);
				break;
			}
    }

    // Maybe add new dot
    if (!new_extension.empty() &&
        new_extension._pathname.front() != dot)
        _pathname += dot;

    // Append new extension
    _pathname += new_extension._pathname;
    return *this;
}

void path::swap(path& rhs)
{
    _pathname.swap(rhs._pathname);
}

path path::lexically_normal() const
{
    path ret;
    for (path sp : *this) {
        if (sp == dot_dot_path)
            ret.remove_filename();
        else if (sp == dot_path)
            ;                                   // NOP
        else
            ret.append(sp);
    }
    return ret;
}

#ifdef FAT_HAVE_LONGNAME
std::string path::string(const codecvt_type& cvt) const
{
    std::string ret;
    if (!wstring_to_string(_pathname, ret, cvt))
		throw filesystem_error("unable to convert path");
    return ret;
}
#endif

int path::compare(const path& p) const noexcept
{
    iterator i1 = begin(), i2 = p.begin();
    const iterator i1_end = end(), i2_end = p.end();
    for (; i1 != i1_end && i2 != i2_end; i1++, i2++)
    {
        if (i1->native() < i2->native())
            return -1;
        if (i1->native() > i2->native())
            return 1;
    }
    if (i1 == i1_end && i2 == i2_end)
        return 0;
    return i1 == i1_end ? -1 : 1;
}

path path::root_name() const
{
    return path();
}

path path::root_directory() const
{
    if (has_root_directory())
        return path(preferred_separator_string);
    else
        return path();
}

path path::root_path() const
{
    return root_directory();
}

path path::relative_path() const
{
    size_t i0 = has_root_directory();
    auto i1 = _pathname.find_last_of(preferred_separator);
    if (i1 != _pathname.npos && i0 < i1)
        return path(_pathname.substr(i0, i1 - i0));
    return path();
}

path path::parent_path() const
{
    // parent path is everything up to filename non-inclusive.
    auto i = _pathname.find_last_of(preferred_separator);
    if (i == 0)
        return path(preferred_separator_string);
    if (i != _pathname.npos)
        return path(_pathname.substr(0, i));
    return path();
}

path path::filename() const
{
    if (empty())
        return path();
    auto i = _pathname.find_last_of(preferred_separator);
    if (i == _pathname.npos)
        return path(_pathname);
    return path(_pathname.substr(i + 1));
}

path path::stem() const
{
    path p{filename()};
    auto i = p._pathname.find_last_of(dot);
    if (i != _pathname.npos &&
        p._pathname != dot_path._pathname &&
        p._pathname != dot_dot_path._pathname)
        return path(p._pathname.substr(0, i));
    return p;
}

path path::extension() const
{
    path p{filename()};
    auto i = p._pathname.find_last_of(dot);
    if (i != _pathname.npos &&
        p._pathname != dot_path._pathname &&
        p._pathname != dot_dot_path._pathname)
        return path(p._pathname.substr(i));
    return path();
}

bool path::empty() const
{
    return _pathname.empty();
}

bool path::has_root_name() const
{
    return false;
}

bool path::has_root_directory() const
{
    return !empty() && _pathname.front() == preferred_separator;
}

bool path::has_root_path() const
{
    return has_root_directory();
}

bool path::has_relative_path() const
{
    size_t i0 = has_root_directory();
    auto i1 = _pathname.find_last_of(preferred_separator);
    return i1 != _pathname.npos && i1 > i0;
}

bool path::has_parent_path() const
{
    // parent path is everything up to filename non-inclusive.
    auto i = _pathname.find_last_of(preferred_separator);
    return i == 0 || (i != _pathname.npos && i < _pathname.size());
}

bool path::has_filename() const
{
    if (empty())
        return false;
    auto i = _pathname.find_last_of(preferred_separator);
    return i == _pathname.npos || i < size() - 1;
}

bool path::has_stem() const
{
    return !stem().empty();
}

bool path::has_extension() const
{
    path p = extension();
    return p._pathname.find_first_of(dot) != _pathname.npos &&
        p._pathname != dot_path._pathname &&
        p._pathname != dot_dot_path._pathname;
}

bool path::is_absolute() const
{
    return has_root_path();
}

path::iterator path::begin() const
{
    const path& p = *this;
    iterator it;
    it._path = &p;
    it._pos = 0;

    auto i = p._pathname.find_first_of(preferred_separator);
    if (i == string_type::npos)
        i = p.size();
    else if (i == 0)                            // root directory
        i = 1;
    it._e._pathname = p._pathname.substr(0, i);
    return it;
}

path::iterator path::end() const
{
    const path& p = *this;
    iterator it;
    it._path = &p;
    it._pos = p.size();
    return it;
}

void path::iterator::increment()
{
    const path& p = *_path;

    _pos += _e.size();
    if (_pos == p.size())
    {
        // end of path reached
        _e.clear();
        return;
    }

    if (p._pathname[_pos] == preferred_separator)
    {
        // skip separator(s) and advance to next filename
        while (_pos != p.size() &&
               p._pathname[_pos] == preferred_separator)
            _pos++;
        // found trailing non-root separator: emit dot
        if (_pos == p.size())
        {
            _e = dot_path;
            // set _pos so next increment() will be the last
            _pos--;
            return;
        }
    }
    // emit filename
    auto i = p._pathname.find_first_of(preferred_separator, _pos);
    if (i == string_type::npos)
        i = p.size();
    _e._pathname = p._pathname.substr(_pos, i - _pos);
}

} // namespace fat
