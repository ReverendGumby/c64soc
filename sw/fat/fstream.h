#pragma once

#include <istream>
#include "path.h"
#include "filebuf.h"

namespace fat {

class fstream : public std::basic_iostream<char>
{
public:
    fstream();
    explicit fstream(const path& filename,
                     std::ios_base::openmode mode = std::ios_base::in);
    //fstream(fstream&&);
    fstream(const fstream&) = delete;
    ~fstream() override;

    void open(const path& filename,
              std::ios_base::openmode mode = std::ios_base::in);
};

} // namespace fat
