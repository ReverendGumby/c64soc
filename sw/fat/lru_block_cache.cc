#include "lru_block_cache.h"
#include <assert.h>

namespace fat
{

lru_block_cache::lru_block_cache(std::shared_ptr<block_device> dev, unsigned num_blocks)
    : block_cache(dev, num_blocks)
{
    _writethru = true;
}

lru_block_cache::~lru_block_cache()
{
}

void lru_block_cache::set_writethru(bool e)
{
    _writethru = e;
    if (_writethru)
        flush_writes();
}

lru_block_cache::cache_entry_t& lru_block_cache::find_block(sector s, const block_buffer_t* pbb)
{
    cache_maintenance();

    auto i = _cache.find(s);
    if (i == _cache.end()) {
        block_buffer_t bb;
        if (pbb)
            bb = *pbb;
        else
            bb = _dev->read_block(s);
        auto e = cache_entry_t { .bb = bb, .age = 0, .dirty = false };
        auto p = _cache.emplace(std::make_pair(s, e));
        i = p.first;
    } else
        i->second.age = 0;
    return i->second;
}

void lru_block_cache::cache_maintenance()
{
    // Make room
    while (_cache.size() >= _num_blocks) {
        // Find oldest entry
        auto oldest = _cache.begin();
        for (auto i = oldest; i != _cache.end(); i++)
            if (i != oldest && oldest->second.age < i->second.age)
                oldest = i;

        // Drop it
        auto s = oldest->first;
        auto& e = oldest->second;
        if (e.dirty)
            _dev->write_block(s, e.bb);
        _cache.erase(oldest);
    }

    // Bump ages
    for (auto i = _cache.begin(); i != _cache.end(); i++)
        i->second.age++;
}

lru_block_cache::block_buffer_t lru_block_cache::read_block(sector s)
{
    auto& e = find_block(s, nullptr);
    return e.bb;
}

void lru_block_cache::write_block(sector s, const block_buffer_t& bb)
{
    auto& e = find_block(s, &bb);
    assert(e.bb == bb);
    if (_writethru) {
        _dev->write_block(s, e.bb);
    } else
        e.dirty = true;
}

void lru_block_cache::flush_writes()
{
    for (auto i = _cache.begin(); i != _cache.end(); i++) {
        auto s = i->first;
        auto& e = i->second;
        if (e.dirty) {
            e.dirty = false;
            _dev->write_block(s, e.bb);
        }
    }
    _dev->flush_writes();
}

} // namespace fat
