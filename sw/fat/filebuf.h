#ifndef FILEBUF_H_
#define FILEBUF_H_

#include <streambuf>
#include "path.h"
#include "file.h"

namespace fat {

class filebuf : public std::basic_streambuf<char>
{
public:
    filebuf();
    filebuf(const filebuf&) = delete;
    filebuf(filebuf&&);
    ~filebuf() override;

    bool is_open() const;
    filebuf* open(const path& filename, std::ios_base::openmode mode);
    filebuf* close();

protected:
    int sync() override;
    std::streamsize showmanyc() override;
    int_type underflow() override;
    int_type overflow(int_type c = traits_type::eof()) override;
    std::streamsize xsputn(const char_type* s, std::streamsize count) override;
    int_type pbackfail(int_type c = traits_type::eof()) override;
    pos_type seekoff(off_type off,
                     std::ios_base::seekdir dir,
                     std::ios_base::openmode which
                     = std::ios_base::in | std::ios_base::out) override;
    pos_type seekpos(pos_type sp,
                     std::ios_base::openmode which
                     = std::ios_base::in | std::ios_base::out) override;

private:
    static constexpr unsigned buf_size = 1024;

    file* _f;
    char* _buf;                                 // read/write buffer
    pos_type _used;                             // read/write buffer occupancy
    pos_type _off;                              // file position of _buf[0]
    bool _reading;                              // reading confirmed
    bool _writing;                              // writing confirmed
};

} // namespace fat

#endif /* FILEBUF_H_ */
