#ifndef DIRECTORY_ITERATOR_H_
#define DIRECTORY_ITERATOR_H_

#include <iterator>
#include "path.h"
#include "directory_entry.h"
#include "directory.h"
#include "fat32.h"

namespace fat
{

class directory_iterator
    : public std::iterator<std::input_iterator_tag, directory_entry>
{
public:
    directory_iterator() noexcept : _dir() {}
    directory_iterator(const directory_iterator&) = default;
    directory_iterator(directory_iterator&&) = default;
    explicit directory_iterator(const path& p)
        : directory_iterator(resolve_path(p)) {}
    explicit directory_iterator(const directory_entry &de)
        : _dir(de) { reset(); }
    explicit directory_iterator(const directory &dir)
        : _dir(dir) { reset(); }

    reference operator*() { return _e; }
    pointer operator->() { return &_e; }

    directory_iterator& operator++()
    {
        increment();
        return *this;
    }

    directory_iterator operator++(int)
    {
        directory_iterator ret{*this};
        increment();
        return ret;
    }

/*
    friend void swap(directory_iterator& lhs, directory_iterator& rhs)
    {
        lhs._dir.swap(rhs._dir);
        lhs._entry.swap(rhs._e);
    }
*/

    friend bool operator==(const directory_iterator& lhs,
                           const directory_iterator& rhs)
    {
        return lhs._dir.is_eod() == rhs._dir.is_eod();
    }

    friend bool operator!=(const directory_iterator& lhs,
                           const directory_iterator& rhs)
    {
        return !(lhs == rhs);
    }

private:
    void reset()
    {
        _dir.reset_read();
        _e = _dir.read();
    }

    void increment()
    {
        if (!_dir.is_eod())
            _e = _dir.read();
    }

    directory _dir;
    value_type _e;
};

inline const directory_iterator& begin(const directory_iterator& i)
{
    return i;
}

inline directory_iterator end(const directory_iterator&)
{
    return directory_iterator();
}

} // namespace fat

#endif /* DIRECTORY_ITERATOR_H_ */
