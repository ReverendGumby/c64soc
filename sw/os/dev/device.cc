#include "device.h"

#include <stdexcept>

namespace os {
namespace dev {

std::unordered_map<device::name_t, device*> device::devmap;

void device::register_name(name_t name, device* dev)
{
    devmap.emplace(name, dev);
}

device* device::find(name_t name)
{
    try {
        return devmap.at(name);
    } catch (std::out_of_range&) {
        return nullptr;
    }
}

device::device()
{
}

const device::name_t& device::get_name() const
{
    return name;
}


void device::set_name(name_t new_name)
{
    name = new_name;
}

} // namespace dev
} // namespace os
