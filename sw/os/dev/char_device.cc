#include "char_device.h"

namespace os {
namespace dev {

char char_device::read()
{
    while (is_rx_empty())
        ;
    return rx();
}

void char_device::write(char c)
{
    while (is_tx_full())
        ;
    tx(c);
}

} // namespace dev
} // namespace os
