#include "console.h"

namespace os {
namespace dev {

console::console(char_device* base)
    : base(base)
{
    std::string s {"console/"};
    s.append(base->get_name());

    base->set_event_handler([this](event_t ev){ on_event(ev); });

    has_on_rx_ready = base->has_event(event_t::on_rx_ready);
    if (has_on_rx_ready) {
        base->set_event_enable(event_t::on_rx_ready, true);
    }
    set_name(s);
}

bool console::is_rx_empty()
{
    return base->is_rx_empty();
}

bool console::is_tx_full()
{
    return base->is_tx_full();
}

char console::rx()
{
    return base->rx();
}

void console::tx(char b)
{
    base->tx(b);
}

bool console::has_event(event_t)
{
    return false;
}

void console::set_event_handler(event_handler_t)
{
}

void console::set_event_enable(event_t, bool)
{
}

void console::on_event(event_t ev)
{
    if (ev == event_t::on_rx_ready)
        rx_ready.notify_one();
}

char console::read()
{
    std::unique_lock<std::mutex> lck{mtx};
    while (is_rx_empty()) {
        if (base->has_event(event_t::on_rx_ready))
            rx_ready.wait(lck);
    }
    return rx();
}

void console::write(char b)
{
    std::unique_lock<std::mutex> lck{mtx};
    char_device::write(b);
}

} // namespace dev
} // namespace os
