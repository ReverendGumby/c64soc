#pragma once

#include <string>
#include <unordered_map>

namespace os {
namespace dev {

class device
{
public:
    using name_t = std::string;

    static void register_name(name_t name, device*);
    static void unregister_name(name_t name, device*);

    static device* find(name_t name);

    device();

    const name_t& get_name() const;

protected:
    void set_name(name_t);

private:
    static std::unordered_map<name_t, device*> devmap;

    name_t name;
};

} // namespace dev
} // namespace os
