#pragma once

#include "device.h"
#include <functional>

namespace os {
namespace dev {

class char_device : public device
{
public:
    enum class event_t {
        on_rx_ready,
    };
    using event_handler_t = std::function<void(event_t)>;

    // Status tests
    virtual bool is_rx_empty() = 0;
    virtual bool is_tx_full() = 0;

    // Non-blocking I/O: results undefined if RX/TX is empty/full
    // Use events (if available) to wait for RX/TX
    // Use status tests to confirm RX/TX is ready
    virtual char rx() = 0;
    virtual void tx(char) = 0;

    // Events
    // Test whether device can raise event
    virtual bool has_event(event_t) = 0;
    // Set an event handler
    virtual void set_event_handler(event_handler_t) = 0;
    // Enable or disable an event
    virtual void set_event_enable(event_t, bool) = 0;

    // Blocking I/O: waits until RX/TX is not empty/full
    virtual char read();
    virtual void write(char);
};

} // namespace dev
} // namespace os
