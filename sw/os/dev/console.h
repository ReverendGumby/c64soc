#pragma once

#include "char_device.h"
#include <mutex>

namespace os {
namespace dev {

// Provide thread-safe read/write().

class console : public char_device
{
public:
    console(char_device* base);

    // Interface for char_device
    bool is_rx_empty() override;
    bool is_tx_full() override;
    char rx() override;
    void tx(char) override;
    bool has_event(event_t) override;
    void set_event_handler(event_handler_t) override;
    void set_event_enable(event_t, bool) override;
    char read() override;
    void write(char b) override;

private:
    void on_event(event_t);

    char_device* base;
    std::mutex mtx;
    bool has_on_rx_ready;
    std::condition_variable rx_ready;
};

} // namespace dev
} // namespace os
