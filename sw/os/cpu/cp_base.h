#pragma once

#include <arm_acle.h>
#include <type_traits>
#include <stdint.h>
#include "util/RegValUnion.h"

namespace os {
namespace cpu {

class cp_base
{
public:
    // Coprocessor register accessors

    // Default template handles uint32_t and equivalents
    template <typename RegType,
              unsigned int cp, unsigned int opc1,
              unsigned int Cn, unsigned int Cm, unsigned int opc2,
              class Enable = void>
    class cpr
    {
    protected:
        static uint32_t mrc() { return __arm_mrc(cp, opc1, Cn, Cm, opc2); }
        static void mcr(uint32_t r) { __arm_mcr(cp, opc1, r, Cn, Cm, opc2); }

    public:
        using reg_t = RegType;
        static reg_t get() { return reg_t(mrc()); }
        static void set(reg_t r = 0) { mcr(uint32_t(r)); }
    };

    // Partial specialized template automagically uses RegValUnion for bitfield
    // structs that don't readily convert to uint32_t
    template<typename RegStruct,
             unsigned int cp, unsigned int opc1,
             unsigned int Cn, unsigned int Cm, unsigned int opc2>
    class cpr<RegStruct, cp, opc1, Cn, Cm, opc2,
              typename std::enable_if<
                  (std::is_class<RegStruct>::value &&
                   !std::is_convertible<RegStruct, uint32_t>::value)>::type>
        : protected cpr<uint32_t, cp, opc1, Cn, Cm, opc2>
    {
    public:
        using reg_t = RegStruct;
        static reg_t get() { return RegValUnion<reg_t, uint32_t>{cpr::mrc()}; }
        static void set(reg_t r) { cpr::mcr(RegValUnion<reg_t, uint32_t>{r}); }
    };
};

} // namespace cpu
} // namespace os
