#include "interrupts.h"

#include "armv7_exceptions.h"
#include "arm_sys.h"
#include "os/soc/interrupt.h"

namespace os {
namespace cpu {

static constexpr interrupts::id_t max_id = os::soc::interrupt.max_id;

interrupt::interrupt()
{
}

interrupt::interrupt(id_t id, interrupts* parent)
    : id(id), parent(parent)
{
}

void interrupt::enable()
{
    parent->gic.enable(id);
}

void interrupt::disable()
{
    parent->gic.disable(id);
}

void interrupt::set_pending()
{
    parent->gic.set_pending(id);
}

void interrupt::clear_pending()
{
    parent->gic.clear_pending(id);
}

bool interrupt::is_pending()
{
    return parent->gic.is_pending(id);
}

static const uint32_t gic_icdicfr[] = {
    // Derived from Zync 7000 TRM (UG585), Section 7.2.3 Shared Periperhal
    // Interrupts, Table 7-4: PS and PL Shared Peripheral Interrupts (SPI)
    // 0b01 = high-level active, 0b11 = rising-edge active
    // For PL, choosing high-level
    0b01010101010111010101010001011111,
    0b01010101010101011101010101010101,
    0b01110101010101010101010101010101,
    0b00000011010101010101010101010101,
};

static const arm_gic::config_t gic_config = {
    .max_id = max_id,
    .icdicfr = gic_icdicfr,
};

armv7_exceptions::context_t* interrupts::irq_ctx;

interrupts::interrupts(armv7_exceptions& exc)
    : gic(gic_config),
      exc(exc)
{
    ints = new interrupt[max_id + 1];
    for (id_t id = 0; id < max_id; id++)
        ints[id] = interrupt{id, this};
}

void interrupts::init()
{
    gic.init();
    exc.set_handler(exc.irq_int, irq_handler,
                    reinterpret_cast<uintptr_t>(this));

    enable();
}

interrupt* interrupts::get(id_t id)
{
    return &ints[id];
}

void interrupts::irq_handler(uintptr_t arg, armv7_exceptions::context_t* ctx)
{
    auto& self = *reinterpret_cast<interrupts*>(arg);
    auto ack = self.gic.acknowledge();
    auto id = self.gic.get_id(ack);
    if (id <= max_id) {
        irq_ctx = ctx;
        self.ints[id].handler();
        self.gic.complete(ack);
    }
}

} // namespace cpu
} // namespace os
