#pragma once

#include "cache.h"

#include "arm_core_cache.h"
#include "cp15.h"

namespace os {
namespace cpu {

//////////////////////////////////////////////////////////////////////
// ARM Cortex-A9 L1 instruction cache

class arm_l1_icache : public arm_core_cache
{
public:
    void init() override;

    using cache::invalidate; // bring in overloads

    void invalidate(addr_t) override;
    void invalidate_all() override;

    void sync() override;
};

} // namespace cpu
} // namespace os
