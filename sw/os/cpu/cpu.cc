#include "cpu.h"

#include "arm_sys.h"
#include "xparameters.h"
#include "armv7_neon.h"

#include <arm_acle.h>

namespace os {
namespace cpu {

class cpu cpu;

cpu::cpu()
    : cache(l1_dcache, l2_cache)
#ifndef FSBL
    , interrupts(exceptions)
#endif
{
}

void cpu::pre_main_init()
{
    // When we compile with hard floating point (-mfpu=vfpv3), static
    // initialization -- called before main() -- can use VFP instructions. Such
    // instructions are disabled at CPU reset and will cause an undefined
    // instruction exception, unless we do this:
    enable_vfp();
}

void cpu::init()
{
    init_scu();
    init_caches();
    init_mmu();
    enable_mmu();
    enable_scu();
    enable_caches();
    enable_smp();

#ifndef FSBL
    exceptions.init();
    interrupts.init();
#endif

    timer.init();
}

void cpu::deinit()
{
    from_uncached_stack(&cpu::disable_caches_mmu);
    disable_vfp();
}

void cpu::init_scu()
{
    scu.init();
}

void cpu::init_caches()
{
    // L1 caches
    mmu.init();
    l1_icache.init();
    cp15::cmo::bpiall::set(); // invalidate branch predictor array
    l1_dcache.init();

    // L2 cache
    l2_cache.init();
}

void cpu::init_mmu()
{
    arm_mmu::range_map_t map {
#ifdef FSBL
        { 0x00000000, 0x000FFFFF, arm_mmu::range_t::normal }, // OCM
#endif
        { XPAR_PS7_DDR_0_S_AXI_BASEADDR, XPAR_PS7_DDR_0_S_AXI_HIGHADDR, arm_mmu::range_t::normal },
        { 0x40000000, 0x7FFFFFFF, arm_mmu::range_t::device }, // M_AXI_GP0
        { 0xE0000000, 0xE7FFFFFF, arm_mmu::range_t::device }, // I/O, SMC
        { 0xF8000000, 0xFDFFFFFF, arm_mmu::range_t::device }, // CPU, QSPI
        { 0xFFF00000, 0xFFFFFFFF, arm_mmu::range_t::device }, // OCM3
    };
    mmu.init_tables(map);
}

void cpu::enable_scu()
{
    scu.enable();
}

void cpu::enable_mmu()
{
    auto sctlr = cp15::vmcr::sctlr::get();
    sctlr.m = true;
    cp15::vmcr::sctlr::set(sctlr);
}

void cpu::enable_caches()
{
    // L2
    l2_cache.set_enable(true);

    // L1
    auto sctlr = cp15::vmcr::sctlr::get();
    sctlr.z = true;
    sctlr.c = true;
    sctlr.i = true;
    cp15::vmcr::sctlr::set(sctlr);
    __arm_dsb();
    __arm_isb();
}

void cpu::enable_smp()
{
    auto actlr = cp15::oscr::actlr::get();
    // [https://community.arm.com/support-forums/f/architectures-and-processors-forum/7155/cortex-a9-single-core/24698]
    // "Even for a single core, you should really have the SCU enabled and
    // ACTLR.SMP=1 and ACTLR.FW=1"
    actlr.fw = 1;
    actlr.smp = 1;
    cp15::oscr::actlr::set(actlr);
}

void cpu::enable_vfp()
{
    // First, enable access to CP10 and CP11.
    auto cpacr = cp15::oscr::cpacr::get();
    cpacr.cp10 = cpacr.cp_access_full;
    cpacr.cp11 = cpacr.cp_access_full;
    cp15::oscr::cpacr::set(cpacr);
    __arm_isb();

    // Now we can enable floating-point functionality.
    // (If not, an undefined instruction exception will happen here.)
    auto r = armv7_neon::oss::fpexc::get();
    r.en = 1;
    armv7_neon::oss::fpexc::set(r);
}

void cpu::disable_vfp()
{
    // Disable floating-point functionality.
    auto r = armv7_neon::oss::fpexc::get();
    r.en = 0;
    armv7_neon::oss::fpexc::set(r);

    // Disable access to CP10 and CP11.
    auto cpacr = cp15::oscr::cpacr::get();
    cpacr.cp10 = cpacr.cp_access_denied;
    cpacr.cp11 = cpacr.cp_access_denied;
    cp15::oscr::cpacr::set(cpacr);
}

void cpu::disable_caches_mmu()
{
    // Inspired by https://community.arm.com/developer/ip-products/processors/f/cortex-r-forum/7245/mmu-deactivation-and-i-cache-branch-predictor

    // Because this function and functions it calls use stack, and memory must
    // not be modified between D-cache flush and disable, this function must be
    // called from an uncached stack.

    // Disable and flush L1 I-cache and BP
    __arm_dsb();
    auto sctlr = cp15::vmcr::sctlr::get();
    sctlr.z = false;
    sctlr.i = false;
    cp15::vmcr::sctlr::set(sctlr);
    __arm_dsb();
    __arm_isb();
    l1_icache.invalidate_all();

    // Flush D-caches
    l1_dcache.clean_all();
    l1_dcache.sync();
    l2_cache.clean_invalidate_all();
    l2_cache.sync();
    l1_dcache.invalidate_all();
    l1_dcache.sync();

    // Disable MMU and L1 D-cache simultaneously
    sctlr = cp15::vmcr::sctlr::get();
    sctlr.c = false;
    sctlr.m = false;
    cp15::vmcr::sctlr::set(sctlr);
    __arm_dsb();

    // Disable L2
    l2_cache.set_enable(false);
}

void cpu::from_uncached_stack_thunk(cpu* pthis, void (cpu::*fn)())
{
    (pthis->*fn)();
}

void cpu::wfi()
{
    __arm_dsb();
    asm volatile ("wfi");
}

} // namespace cpu
} // namespace os
