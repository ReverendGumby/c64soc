#pragma once

#include "cache.h"

namespace os {
namespace cpu {

class cpu_cache : public cache
{
public:
    cpu_cache(cache& l1, cache& l2);

    size_t linelen() const override;
    set_t sets() const override;
    way_t ways() const override;

    using cache::invalidate; // bring in overloads
    using cache::clean;
    using cache::clean_invalidate;

    void invalidate(addr_t) override;
    void clean(addr_t) override;
    void clean_invalidate(addr_t) override;

    void invalidate(addr_t, size_t) override;
    void clean(addr_t, size_t) override;
    void clean_invalidate(addr_t, size_t) override;

    void sync() override;

private:
    bool is_in_l2(addr_t);

    cache& l1;
    cache& l2;
};

} // namespace cpu
} // namespace os
