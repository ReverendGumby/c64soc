    .text
    .arm

// static void os::cpu::cpu::from_uncached_stack_thunk(cpu* pthis, void (cpu::*fn)());
    .extern _ZN2os3cpu3cpu25from_uncached_stack_thunkEPS1_MS1_FvvE
    
// void os::cpu::cpu::from_uncached_stack(void (cpu::*fn)())
    .global _ZN2os3cpu3cpu19from_uncached_stackEMS1_FvvE
	.type _ZN2os3cpu3cpu19from_uncached_stackEMS1_FvvE,function
_ZN2os3cpu3cpu19from_uncached_stackEMS1_FvvE:
	push {r4, lr}
    add r4, sp, #4

    // OCM3 (upper 64K of OCM) is mapped to 0xFFFF0000 (both PA and VA) and is
    // configured in MMU as uncacheable.
    ldr r3, =0xFFFFFFE0
    mov sp, r3

	// Arguments are already in r0-r2
    bl _ZN2os3cpu3cpu25from_uncached_stack_thunkEPS1_MS1_FvvE

    sub sp, r4, #4
    pop {r4, pc}
