#include "arm_core_cache.h"

#include "cp15.h"

namespace os {
namespace cpu {

void arm_core_cache::init()
{
    cp15::ir::csselr_t csselr { .in_d = is_inst, .level = level };
    cp15::ir::csselr::set(csselr);
    ccsidr = cp15::ir::ccsidr::get();
}

size_t arm_core_cache::linelen() const
{
    return (4 << ccsidr.linesize) * sizeof(uint32_t);
}

arm_core_cache::set_t arm_core_cache::sets() const
{
    return ccsidr.num_sets + 1;
}

arm_core_cache::way_t arm_core_cache::ways() const
{
    return ccsidr.associativity + 1;
}

cp15::cmo::set_way_t arm_core_cache::set_way(set_way_t sw) const
{
    return cp15::cmo::set_way_t {
        .level = level,
        .set = sw.set,
        .way = sw.way,
        .associativity = ways(),
        .line_len = linelen(),
        .n_sets = sets(),
    };
}

} // namespace cpu
} // namespace os
