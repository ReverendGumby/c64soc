#include "arm_scu.h"

#include "arm_mpcore.h"
#include "cp15.h"

namespace os {
namespace cpu {

static volatile struct scu_regs {
    struct control_t {
        uint32_t scu_en : 1;                 // SCU enable
        uint32_t afe : 1;                    // Address filtering enable
        uint32_t srpe : 1;                   // SCU RAMs parity enable
        uint32_t ssle : 1;                   // SCU Speculative linefills enable
        uint32_t fadtp0e : 1;                // Force all Device to port0 enable
        uint32_t ssen : 1;                   // SCU standby enable
        uint32_t isen : 1;                   // IC standby enable
        uint32_t _res7 : 25;
    };

    struct config_t {
        uint32_t cpu_num : 2;                   // CPU number
        uint32_t _res2 : 2;
        uint32_t smp0 : 1;                      // CPU0 is in SMP
        uint32_t smp1 : 1;                      // CPU1 is in SMP
        uint32_t smp2 : 1;                      // CPU2 is in SMP
        uint32_t smp3 : 1;                      // CPU3 is in SMP
        uint32_t trsz_cpu0 : 2;                 // CPU0 tag RAM size
        uint32_t trsz_cpu1 : 2;                 // CPU0 tag RAM size
        uint32_t trsz_cpu2 : 2;                 // CPU0 tag RAM size
        uint32_t trsz_cpu3 : 2;                 // CPU0 tag RAM size
        uint32_t _res16 : 16;
    };

    // struct power_status_t ..
    struct iariss_t {
        uint32_t cpu0_ways : 4;
        uint32_t cpu1_ways : 4;
        uint32_t cpu2_ways : 4;
        uint32_t cpu3_ways : 4;
        uint32_t _res16 : 16;
    };

    RegValUnion<control_t, uint32_t> control; // SCU Control Register, +0x00
    RegValUnion<config_t, uint32_t> config; // SCU Configuration Register, +0x04
    uint32_t cpu_pwr_stat; // SCU Power Status Register, +0x08
    RegValUnion<iariss_t, uint32_t> iariss; // Invalidate All Registers in Secure State, +0x0C
    uint32_t _res10[(0x40 - 0x10) / 4];
    uint32_t filt_start_addr; // Filtering Start Address Register, +0x40
    uint32_t filt_end_addr; // Filtering End Address Register, +0x44
    uint32_t _res48[(0x50 - 0x48) / 4];
    uint32_t sac; // SCU Access Control (SAC) Register, +0x50
    uint32_t snsac; // SCU Non-Secure Access Control (SNSAC) Register, +0x54

} *regs = (volatile struct scu_regs *)arm_pmr.scu;

void arm_scu::init()
{
    // Invalidate SCU duplicate tags for all cores
    regs->iariss = scu_regs::iariss_t {
        .cpu0_ways = 0xF,
        .cpu1_ways = 0xF,
        .cpu2_ways = 0xF,
        .cpu3_ways = 0xF,
    };
}

void arm_scu::enable()
{
    auto control = regs->control;
    control.r.scu_en = 1;
    regs->control = control;
}

} // namespace cpu
} // namespace os
