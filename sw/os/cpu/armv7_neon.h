#pragma once

#include <stdint.h>

// Section references from ARM DEN 0018A (NEON, Version 1.0, Programmer's Guide)

namespace os {
namespace cpu {

//////////////////////////////////////////////////////////////////////
// ARMv7 NEON and VFP support

// Define a NEON system register accessor with type conversion
#define DEFINE_NSRA(StructName, Reg)                        \
    struct StructName                                       \
    {                                                       \
        static uint32_t vmrs()                              \
        {                                                   \
            uint32_t v;                                     \
            __asm__ ("vmrs %0, " Reg : "=r" (v));        \
            return v;                                       \
        }                                                   \
                                                            \
        static void vmsr(uint32_t v)                        \
        {                                                   \
            __asm__ ("vmsr " Reg ", %0" : : "r" (v)); \
        }                                                   \
    };

class armv7_neon
{
private:
    // NEON system register accessor with type conversion
    template <typename RegStruct, class RegAccessor>
    class nsr
    {
    public:
        using reg_t = RegStruct;
        static reg_t get() { return RegValUnion<reg_t, uint32_t>{RegAccessor::vmrs()}; }
        static void set(reg_t r) { RegAccessor::vmsr(RegValUnion<reg_t, uint32_t>{r}); }
    };

    DEFINE_NSRA(fpexc_nsra, "fpexc");

public:
    // Appendix B, Operating System Support
    struct oss {
        struct fpexc_t {
            uint32_t _res0 : 30;
            uint32_t en : 1; // enable NEON and VFP
            uint32_t ex : 1; // implementation-specific state
        };

        using fpexc = nsr<fpexc_t, fpexc_nsra>;
    };
};

} // namespace cpu
} // namespace os
