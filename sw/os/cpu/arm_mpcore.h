#include <stdint.h>

#include "os/soc/module_base.h"

namespace os {
namespace cpu {

// From Cortex-A9 MPCore TRM (ARM DDI 0407G), section 1.5 Private Memory Region

static constexpr struct {
    // PMR starts at PERIPHBASE[31:13]
    uint32_t base = os::soc::module_base.mpcore;

    // MPCore peripherals
    uint32_t scu = base + 0x0000;             // SCU registers
    uint32_t int_ctrl = base + 0x0100;        // Interrupt controller interfaces
    uint32_t glb_tmr = base + 0x0200;         // Global timer
    uint32_t priv_tmr_wdt = base + 0x0600;    // Private timer and watchdogs
    uint32_t int_dist = base + 0x1000;        // Interrupt distributor
} arm_pmr;

} // namespace cpu
} // namespace os
