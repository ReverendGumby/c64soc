#pragma once

#include <stdint.h>
#include "cache.h"

namespace os {
namespace cpu {

//////////////////////////////////////////////////////////////////////
// ARM PL310 / L2C-310 CoreLink Level 2 Cache Controller

class arm_pl310 : public cache
{
public:
    struct ev_ctr_t {
        uint32_t ctr0;
        uint32_t ctr1;
    };

    void init() override;
    void set_enable(bool e);

    void init_counters();
    ev_ctr_t read_counters();

    size_t linelen() const override;
    set_t sets() const override;
    way_t ways() const override;

    using cache::invalidate; // bring in overloads
    using cache::clean;
    using cache::clean_invalidate;

    void invalidate(addr_t) override;
    void clean(addr_t) override;
    void clean_invalidate(addr_t) override;

    void invalidate_all() override;
    void clean_all() override;
    void clean_invalidate_all() override;

    void sync() override;

private:
};

} // namespace cpu
} // namespace os
