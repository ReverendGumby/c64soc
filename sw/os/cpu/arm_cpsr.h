#include "util/RegValUnion.h"

namespace os {
namespace cpu {

struct cpsr {
    struct cpsr_t {
        uint32_t m : 5;                         // Mode field
        uint32_t t : 1;                         // Thumb execution state
        uint32_t f : 1;                         // FIQ mask
        uint32_t i : 1;                         // IRQ mask
        uint32_t a : 1;                         // Asynchronous abort mask
        uint32_t e : 1;                         // Endianness execution state
        uint32_t it2 : 6;                       // If-Then execution state
        uint32_t ge : 4;                        // Greater Than or Equal flags
        uint32_t _res20 : 4;
        uint32_t j : 1;                         // Jazelle bit
        uint32_t it0 : 2;                       // If-Then execution state
        uint32_t q : 1;                         // Cumulative saturation
        uint32_t v : 1;                         // Overflow condition flag
        uint32_t c : 1;                         // Carry condition flag
        uint32_t z : 1;                         // Zero condition flag
        uint32_t n : 1;                         // Negative condition flag
    };

    enum {
        mode_usr = 0b10000,                     // User
        mode_fiq = 0b10001,                     // FIQ
        mode_irq = 0b10010,                     // IRQ
        mode_svc = 0b10011,                     // Supervisor
        mode_mon = 0b10110,                     // Monitor
        mode_abt = 0b10111,                     // Abort
        mode_hyp = 0b11010,                     // Hypervisor
        mode_und = 0b11011,                     // Undefined
        mode_sys = 0b11111,                     // System
    };

    static inline cpsr_t get()
    {
        RegValUnion<cpsr_t, uint32_t> u;
        asm volatile ("mrs %[ro], cpsr" : [ro]"=r"(u.v));
        return u.r;
    }

    static inline uint32_t to_val(cpsr_t r)
    {
        RegValUnion<cpsr_t, uint32_t> u {r};
        return u.v;
    }
};

} // namespace cpu
} // namespace os
