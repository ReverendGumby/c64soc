#pragma once

#include "cache.h"
#include "cp15.h"

namespace os {
namespace cpu {

//////////////////////////////////////////////////////////////////////
// ARM Cortex-A9 core cache

using mva_t = void *; // MVA (Modified Virtual Address)

class arm_core_cache : public cache
{
public:
    void init() override;

    size_t linelen() const override;
    set_t sets() const override;
    way_t ways() const override;

protected:
    bool is_inst; // I$ or D$?
    cp15::ir::ccsidr_t ccsidr;
    unsigned level;

    cp15::cmo::set_way_t set_way(set_way_t) const;
};

} // namespace cpu
} // namespace os
