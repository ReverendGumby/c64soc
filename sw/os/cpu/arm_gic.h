#pragma once

#include <stdint.h>

namespace os {
namespace cpu {

class arm_gic
{
public:
    // Interrupt IDs
    using id_t = uint8_t;
    enum {
        // Software Generated Interrupts (SGI)
        sgi0 = 0,
        sgi15 = 15,
        // Private Peripheral Interrupts (PPI)
        gt = 27,                                // Global timer, PPI(0)
        nfiq = 28,                              // nFIQ pin, PPI(1)
        pt = 29,                                // Private timer, PPI(2)
        wdt = 30,                               // Watchdog timers, PPI(3)
        nirq = 31,                              // nIRQ pin, PPI(4)
        // Shared Peripheral Interrupts (SPI)
        spi = 32,
    };

    static constexpr bool is_sgi(id_t id) { return id >= sgi0 && id <= sgi15; }

    // Interrupt acknowledge value
    using ack_t = uint32_t;

    // Peripheral configuration
    struct config_t {
        id_t max_id;
        const uint32_t* icdicfr;
    };

    arm_gic(const config_t& config);

    void init();

    void enable(id_t);
    void disable(id_t);

    void set_pending(id_t);
    void clear_pending(id_t);

    bool is_pending(id_t);
    bool is_active(id_t);

    // Acknowledge the interrupt that caused the interrupt exception. Sets
    // interrupt state to active. Returns an acknowledge value.
    ack_t acknowledge();

    // Extract the interrupt's ID from the acknowledge value.
    static id_t get_id(ack_t);

    // Signal interrupt handling completion. Pass the value returned by
    // acknowledge().
    void complete(ack_t);

private:
    const config_t& config;
};

} // namespace cpu
} // namespace os
