#pragma once

#include <stdint.h>
#include <vector>

namespace os {
namespace cpu {

//////////////////////////////////////////////////////////////////////
// ARM Cortex-A9 MMU

class arm_mmu
{
public:
    struct range_t {
        uint32_t pa_start, pa_end;
        // PA is mapped 1:1 to VA.
        enum { normal, device } attr;
    };

    using range_map_t = std::vector<range_t>;

    arm_mmu();

    void init();
    void init_tables(const range_map_t&);

private:
    void set_config();
    const range_t* find_addr_in_map(const range_map_t&, uint32_t addr);
};

} // namespace cpu
} // namespace os
