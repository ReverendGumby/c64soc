#pragma once

#include <stdint.h>
#include "cp_base.h"

// Section references from ARM DDI 0406C (ARM Architecture Reference Manual, ARMv7-A and ARMv7-R Edition)

namespace os {
namespace cpu {

class cp14 : private cp_base
{
private:
    // C6.4.1 Using CP14 to access debug registers
    // opc1 = 0, CRn = reg[9:7], opc2 = reg[6:4], CRm = reg[3:0]
    template <typename RegType, unsigned int dbgreg>
    using dcpr = cpr<RegType, 14, 0, (dbgreg >> 7) & 7, (dbgreg >> 0) & 15,
        (dbgreg >> 4) & 7>;

public:
    // C11.4 Control and status registers
    struct csr {
        struct dbgdscr_t {
            uint32_t halted : 1;
            uint32_t restarted : 1;
            uint32_t mode : 4;
            uint32_t sdabort_l : 1;
            uint32_t adabort_l : 1;
            uint32_t und_l : 1;
            uint32_t fs : 1;
            uint32_t dbgack : 1;
            uint32_t intdis : 1;
            uint32_t udccdis : 1;
            uint32_t itren : 1;
            uint32_t hdbgen : 1;
            uint32_t mdbgen : 1;
            uint32_t spiddis : 1;
            uint32_t spnidis : 1;
            uint32_t ns : 1;
            uint32_t adadiscard : 1;
            uint32_t extdccmode : 2;
            uint32_t _res22 : 2;
            uint32_t instrcompl_l : 1;
            uint32_t pipeadv : 1;
            uint32_t txfull_l : 1;
            uint32_t rxfull_l : 1;
            uint32_t _res28 : 1;
            uint32_t txfull : 1;                // DBGDTRTX register full
            uint32_t rxfull : 1;                // DBGDTRRX register full
            uint32_t _res31 : 1;
        };

        using dbgdscr = dcpr<dbgdscr_t, 1>; // RO
    };

    // C11.5 Instruction and data transfer registers
    struct idtr {
        using dbgdtrrx = dcpr<uint32_t, 5>; // RO
        using dbgdtrtx = dcpr<uint32_t, 5>; // WO
    };
};

} // namespace cpu
} // namespace os
