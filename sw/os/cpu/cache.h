#pragma once

#include <stddef.h>
#include <stdint.h>

namespace os {
namespace cpu {

//////////////////////////////////////////////////////////////////////
// Generic cache

class cache
{
public:
    using addr_t = uintptr_t;
    using set_t = size_t;
    using way_t = size_t;

    static constexpr addr_t to_addr_t(void* p)
    {
        return reinterpret_cast<addr_t>(p);
    }

    struct set_way_t {
        set_t set;
        way_t way;
    };

    // Initialization
    virtual void init();

    // Geometry
    virtual size_t linelen() const = 0;
    virtual set_t sets() const = 0;
    virtual way_t ways() const = 0;

    void round_to_line(addr_t&, size_t&);

    // Maintenance operations by address
    virtual void invalidate(addr_t);
    virtual void clean(addr_t);
    virtual void clean_invalidate(addr_t);

    void invalidate(void* p) { return invalidate(to_addr_t(p)); }
    void clean(void* p) { return clean(to_addr_t(p)); }
    void clean_invalidate(void* p) { return clean_invalidate(to_addr_t(p)); }

    // Maintenance operations by address range
    virtual void invalidate(addr_t, size_t);
    virtual void clean(addr_t, size_t);
    virtual void clean_invalidate(addr_t, size_t);

    void invalidate(void* p, size_t s) { return invalidate(to_addr_t(p), s); }
    void clean(void* p, size_t s) { return clean(to_addr_t(p), s); }
    void clean_invalidate(void* p, size_t s) { return clean_invalidate(to_addr_t(p), s); }

    // Maintenance operations on all entries
    virtual void invalidate_all();
    virtual void clean_all();
    virtual void clean_invalidate_all();

    // Maintenance operation synchronization point
    virtual void sync() = 0;

protected:
    // Maintenance operations by set/way
    virtual void invalidate(set_way_t);
    virtual void clean(set_way_t);
    virtual void clean_invalidate(set_way_t);
};

} // namespace cpu
} // namespace os
