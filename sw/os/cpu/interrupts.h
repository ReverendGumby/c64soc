#pragma once

#include <functional>
#include <stdint.h>
#include "arm_gic.h"
#include "arm_cpsr.h"
#include "armv7_exceptions.h"

namespace os {
namespace cpu {

class interrupts;

class interrupt
{
public:
    using id_t = arm_gic::id_t;
    using handler_t = std::function<void()>;

    interrupt();
    interrupt(id_t, interrupts*);

    void enable();
    void disable();

    void set_pending();
    void clear_pending();
    bool is_pending();

    id_t id;
    handler_t handler;

private:
    interrupts* parent;
};

class interrupts
{
public:
    using id_t = interrupt::id_t;

    interrupts(armv7_exceptions&);

    static bool is_in_irq();

    static void enable();
    static void disable();
    static bool are_enabled();

    static bool save();
    static void restore(bool);

    void init();

    interrupt* get(id_t);

    static armv7_exceptions::context_t* irq_ctx;

private:
    friend interrupt;

    static void irq_handler(uintptr_t arg, armv7_exceptions::context_t* ctx);

    arm_gic gic;
    armv7_exceptions& exc;
    interrupt *ints;
};

inline bool interrupts::is_in_irq()
{
    auto m = cpsr::get().m;
    return m == cpsr::mode_irq;
}

inline void interrupts::enable()
{
    asm volatile ("cpsie i" ::: "memory", "cc");
}

inline void interrupts::disable()
{
    asm volatile ("cpsid i" ::: "memory", "cc");
}

inline bool interrupts::are_enabled()
{
    bool masked = cpsr::get().i;
    return !masked;
}

inline bool interrupts::save()
{
    bool masked = cpsr::get().i;
    disable();
    return masked;
}

inline void interrupts::restore(bool masked)
{
    if (!masked)
        enable();
}

} // namespace cpu
} // namespace os
