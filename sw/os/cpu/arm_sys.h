#pragma once

#define __arm_dmb() __asm__ __volatile__ ("dmb sy")
#define __arm_dsb() __asm__ __volatile__ ("dsb sy")
#define __arm_isb() __asm__ __volatile__ ("isb sy")

#define __arm_wfi() __asm__ __volatile__ ("wfi")
