set (sources
  arm_core_cache.cc
  arm_gic.cc
  arm_l1_dcache.cc
  arm_l1_icache.cc
  arm_mmu.cc
  arm_pl310.cc
  arm_scu.cc
  arm_timer.cc
  cache.cc
  cp15.cc
  cpu.cc
  cpu_cache.cc
  cpu_stack.S
  interrupts.cc
  )

target_sources(fsbl PRIVATE ${sources})

target_sources(emu PRIVATE
  ${sources}
  armv7_exceptions.cc
  armv7_vtab.S
  )
