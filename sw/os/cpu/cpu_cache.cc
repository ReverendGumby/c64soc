#include "cpu.h"

namespace os {
namespace cpu {

cpu_cache::cpu_cache(cache& l1, cache& l2)
    : l1(l1), l2(l2)
{
}

// Report geometry of first-level cache

size_t cpu_cache::linelen() const
{
    return l1.linelen();
}

cpu_cache::set_t cpu_cache::sets() const
{
    return l1.sets();
}

cpu_cache::way_t cpu_cache::ways() const
{
    return l1.ways();
}

void cpu_cache::invalidate(addr_t addr)
{
    l2.invalidate(addr);
    l2.sync();
    l1.invalidate(addr);
    l1.sync();
}

void cpu_cache::clean(addr_t addr)
{
    l1.clean(addr);
    l1.sync();
    l2.clean(addr);
    l2.sync();
}

void cpu_cache::clean_invalidate(addr_t addr)
{
    l1.clean(addr);
    l1.sync();
    l2.clean_invalidate(addr);
    l2.sync();
    l1.clean_invalidate(addr);
    l1.sync();
}

void cpu_cache::invalidate(addr_t addr, size_t len)
{
    round_to_line(addr, len);
    if (is_in_l2(addr)) {
        for (size_t i = 0, line = l2.linelen(); i < len; i += line)
            l2.invalidate(addr + i);
        l2.sync();
    }
    for (size_t i = 0, line = l1.linelen(); i < len; i += line)
        l1.invalidate(addr + i);
    l1.sync();
}

void cpu_cache::clean(addr_t addr, size_t len)
{
    round_to_line(addr, len);
    for (size_t i = 0, line = l1.linelen(); i < len; i += line)
        l1.clean(addr + i);
    l1.sync();
    if (is_in_l2(addr)) {
        for (size_t i = 0, line = l2.linelen(); i < len; i += line)
            l2.clean(addr + i);
        l2.sync();
    }
}

void cpu_cache::clean_invalidate(addr_t addr, size_t len)
{
    round_to_line(addr, len);
    if (is_in_l2(addr)) {
        for (size_t i = 0, line = l1.linelen(); i < len; i += line)
            l1.clean(addr + i);
        l1.sync();
        for (size_t i = 0, line = l2.linelen(); i < len; i += line)
            l2.clean_invalidate(addr + i);
        l2.sync();
    }
    for (size_t i = 0, line = l1.linelen(); i < len; i += line)
        l1.clean_invalidate(addr + i);
    l1.sync();
}

void cpu_cache::sync()
{
    l1.sync();
    l2.sync();
}

bool cpu_cache::is_in_l2(addr_t addr)
{
    // Values from TRM Section 29.2.5 Address Mapping, Table 29-2
    constexpr uint32_t mpcore_Filtering_Start_Address_Register = 0x00100000;
    constexpr uint32_t mpcore_Filtering_End_Address_Register = 0xFFE00000;

    // The L2 cache does not see addresses outside this range.
    return addr >= mpcore_Filtering_Start_Address_Register
        && addr < mpcore_Filtering_End_Address_Register;
}

} // namespace cpu
} // namespace os
