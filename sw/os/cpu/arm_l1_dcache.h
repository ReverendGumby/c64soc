#pragma once

#include "cache.h"

#include "arm_core_cache.h"

namespace os {
namespace cpu {

//////////////////////////////////////////////////////////////////////
// ARM Cortex-A9 L1 data cache

class arm_l1_dcache : public arm_core_cache
{
public:
    void init() override;

    using arm_core_cache::invalidate; // bring in overloads
    using arm_core_cache::clean;
    using arm_core_cache::clean_invalidate;

    void invalidate(addr_t) override;
    void clean(addr_t) override;
    void clean_invalidate(addr_t) override;

    void sync() override;

protected:
    void invalidate(set_way_t) override;
    void clean(set_way_t) override;
    void clean_invalidate(set_way_t) override;
};

} // namespace cpu
} // namespace os
