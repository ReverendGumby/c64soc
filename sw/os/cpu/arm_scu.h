#pragma once

namespace os {
namespace cpu {

//////////////////////////////////////////////////////////////////////
// ARM Cortex-A9 MPCore Snoop Control Unit

class arm_scu
{
public:
    void init();

    void enable();
};

} // namespace cpu
} // namespace os
