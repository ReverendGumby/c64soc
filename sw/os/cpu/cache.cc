#include "cache.h"

#include <system_error>

namespace os {
namespace cpu {

void cache::init()
{
}

void cache::round_to_line(addr_t& addr, size_t& len)
{
    size_t line = linelen();
    size_t prepend = addr & (line - 1);
    size_t append = (line - (addr + len)) & (line - 1);
    addr -= prepend;
    len += prepend + append;
}

void cache::invalidate(addr_t)
{
    throw std::system_error(std::make_error_code(std::errc::function_not_supported));
}

void cache::clean(addr_t)
{
    throw std::system_error(std::make_error_code(std::errc::function_not_supported));
}

void cache::clean_invalidate(addr_t)
{
    throw std::system_error(std::make_error_code(std::errc::function_not_supported));
}

void cache::invalidate(set_way_t)
{
    throw std::system_error(std::make_error_code(std::errc::function_not_supported));
}

void cache::clean(set_way_t)
{
    throw std::system_error(std::make_error_code(std::errc::function_not_supported));
}

void cache::clean_invalidate(set_way_t)
{
    throw std::system_error(std::make_error_code(std::errc::function_not_supported));
}

void cache::invalidate(addr_t addr, size_t len)
{
    round_to_line(addr, len);
    for (size_t i = 0, line = linelen(); i < len; i += line)
        invalidate(addr + i);
}

void cache::clean(addr_t addr, size_t len)
{
    round_to_line(addr, len);
    for (size_t i = 0, line = linelen(); i < len; i += line)
        clean(addr + i);
}

void cache::clean_invalidate(addr_t addr, size_t len)
{
    round_to_line(addr, len);
    for (size_t i = 0, line = linelen(); i < len; i += line)
        clean_invalidate(addr + i);
}

void cache::invalidate_all()
{
    for (set_t s = 0, ns = sets(); s < ns; s++)
        for (way_t w = 0, nw = ways(); w < nw; w++)
            invalidate(set_way_t{s, w});
}

void cache::clean_all()
{
    for (set_t s = 0, ns = sets(); s < ns; s++)
        for (way_t w = 0, nw = ways(); w < nw; w++)
            clean(set_way_t{s, w});
}

void cache::clean_invalidate_all()
{
    for (set_t s = 0, ns = sets(); s < ns; s++)
        for (way_t w = 0, nw = ways(); w < nw; w++)
            clean_invalidate(set_way_t{s, w});
}

} // namespace cpu
} // namespace os
