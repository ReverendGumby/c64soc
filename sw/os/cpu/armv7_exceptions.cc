#include <unistd.h>

#include "armv7_exceptions.h"

#include "cp15.h"
#include "arm_cpsr.h"

extern "C" void armv7_vtab(void);

namespace os {
namespace cpu {

static void default_handler(uintptr_t arg, armv7_exceptions::context_t* ctx)
{
    // auto mode = cpsr::get().m;
    _exit(-arg);
}

static void undef_inst_handler(uintptr_t arg, armv7_exceptions::context_t* ctx)
{
    _exit(-arg);
}

static void prefetch_abort_handler(uintptr_t arg, armv7_exceptions::context_t* ctx)
{
    _exit(-arg);
}

static void data_abort_handler(uintptr_t arg, armv7_exceptions::context_t* ctx)
{
    _exit(-arg);
}

armv7_exceptions::handler_arg_t armv7_exceptions::handlers[] = {
    [reset] = { default_handler, reset },
    [undef_inst] = { undef_inst_handler, undef_inst },
    [super_call] = { default_handler, super_call },
    [prefetch_abort] = { prefetch_abort_handler, prefetch_abort },
    [data_abort] = { data_abort_handler, data_abort },
    [hyp_trap] = { default_handler, hyp_trap },
    [irq_int] = { default_handler, irq_int },
    [fiq_int] = { default_handler, fiq_int },
};

void armv7_exceptions::init()
{
    cp15::se::vbar::set(reinterpret_cast<void*>(armv7_vtab));
}

void armv7_exceptions::set_handler(vector_t v, handler_t h, uintptr_t a)
{
    handlers[v] = { h, a };
}

} // namespace cpu
} // namespace os
