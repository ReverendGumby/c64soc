#pragma once

#include "arm_scu.h"
#include "arm_l1_dcache.h"
#include "arm_l1_icache.h"
#include "arm_mmu.h"
#include "arm_pl310.h"
#include "armv7_exceptions.h"
#include "arm_timer.h"
#include "cpu_cache.h"
#include "interrupts.h"

namespace os {
namespace cpu {

class cpu
{
public:
    cpu();

    static void pre_main_init();

    void init();
    void deinit();

    void wfi();

    arm_scu scu;
    arm_l1_dcache l1_dcache;
    arm_l1_icache l1_icache;
    arm_mmu mmu;
    arm_pl310 l2_cache;
    cpu_cache cache; // combines L1D$ + L2$
#ifndef FSBL
    armv7_exceptions exceptions;
    class interrupts interrupts;
#endif
    arm_timer timer;

private:
    void init_scu();
    void init_caches();
    void init_mmu();
    void enable_scu();
    void enable_mmu();
    void enable_caches();
    void enable_smp();
    static void enable_vfp();

    void disable_vfp();
    void disable_caches_mmu();

    void from_uncached_stack(void (cpu::*fn)());
    static void from_uncached_stack_thunk(cpu* pthis, void (cpu::*fn)());
};

extern class cpu cpu;

} // namespace cpu
} // namespace os
