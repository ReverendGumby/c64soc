#include "arm_l1_icache.h"

#include "arm_sys.h"
#include "cp15.h"

namespace os {
namespace cpu {

void arm_l1_icache::init()
{
    level = 0;
    is_inst = true;
    arm_core_cache::init();

    cp15::cmo::iciallu::set();
}

void arm_l1_icache::invalidate(addr_t a)
{
    cp15::cmo::icimvau::set(a);
}

void arm_l1_icache::invalidate_all()
{
    cp15::cmo::icialluis::set();
}

void arm_l1_icache::sync()
{
    __arm_isb();
}

} // namespace cpu
} // namespace os
