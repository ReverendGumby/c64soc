#pragma once

#include "ps7_init.h" // for APU_FREQ

namespace os {
namespace cpu {

class arm_timer
{
public:
    using count_t = unsigned long long;
    using delta_t = signed long long;

    constexpr count_t hz();

    void init();

    count_t get_counter();

    void set_comparator(count_t);
    void disable_comparator();
    // event set when counter >= comparator
    bool is_event_pending();
    void enable_event_interrupt(bool en);

private:
    void clear_counter();
};

constexpr arm_timer::count_t arm_timer::hz()
{
    // The GTC is always clocked at 1/2 of the CPU frequency (CPU_3x2x).
    return APU_FREQ / 2;
}

} // namespace cpu
} // namespace os
