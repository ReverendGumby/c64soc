#include "arm_mmu.h"

#include <assert.h>
#include <algorithm>

#include "cp15.h"

namespace os {
namespace cpu {

// Short-descriptor translation table first-level descriptor
union tt1d_t {
    struct {
        uint32_t type : 2; // ttd_type_fault
    } fault;
    struct {
        uint32_t type : 2; // ttd_type_page_table
        uint32_t pxn : 1; // SBZ
        uint32_t ns : 1; // non-secure
        uint32_t sbz4 : 1;
        uint32_t domain : 4;
        uint32_t id9 : 1; // IMPLEMENTATION DEFINED
        uint32_t base : 22; // page table base address A[31:10]
    } page_table;
    struct {
        uint32_t type : 2; // ttd_type_section
        uint32_t b : 1; // Bufferable
        uint32_t c : 1; // Cacheable
        uint32_t xn : 1; // eXecute-Never
        uint32_t domain : 4;
        uint32_t id9 : 1;
        uint32_t ap : 2; // Access Permission AP[1:0]
        uint32_t tex : 3; // Type EXtension
        uint32_t ap2 : 1; // AP[2]
        uint32_t s : 1; // Shareable
        uint32_t ng : 1; // not Global
        uint32_t sbz18 : 1; // 0 = section
        uint32_t ns : 1; // Non-Secure
        uint32_t base : 12; // section base address A[31:20]

        void set_base(void *p) { base = uint32_t(p) >> 20; }
    } section;
};

// Translation table descriptor type (bits [1:0])
enum {
    ttd_type_fault = 0b00,
    ttd_type_page_table = 0b01,
    ttd_type_section = 0b10,
};

// First-level translation table
//
// Configuration for TTBCR.N = 0:
// - Table indexed by VA[31:20]
// - Descriptor count: 0x1000 / 4K
// - Table size / alignment: 0x4000 / 16KB
//
// Sections translate 1 MB pages

static constexpr int tt1_cnt = 0x1000;

__attribute__ ((section (".mmu_tbl"), aligned (tt1_cnt * sizeof(tt1d_t))))
static tt1d_t tt1[tt1_cnt];

arm_mmu::arm_mmu()
{
}

void arm_mmu::init()
{
    cp15::tmo::tlbiall::set(); // invalidate TLBs
}

void arm_mmu::init_tables(const range_map_t& map)
{
	// Validate map
    for (const auto& m : map) {
    	constexpr uint32_t mb = 1 << 20;
    	assert((m.pa_start & (mb - 1)) == 0 && ((m.pa_end + 1) & (mb - 1)) == 0);
    }

    for (int i = 0; i < tt1_cnt; i++) {
        tt1d_t d = {};
        uint32_t addr = (1ULL << 32) / tt1_cnt * i;
        const auto* pr = find_addr_in_map(map, addr);
        if (!pr) {
            d.fault.type = ttd_type_fault;
        } else {
            auto& s = d.section;
            s.type = ttd_type_section;
            s.xn = 0b0;
            s.domain = 0;
            s.ng = 0b0;
            //s.s = 0b0; // Non-shareable should use L1 cache
            s.s = 0b1; // Shareable

            switch (pr->attr) {
            case range_t::normal:
                // From Cortex-A9 MPCore TRM: "To be kept coherent, the memory
                // must be marked as Write-Back, Shareable, Normal memory."

                // Normal, Write-Back, Write-Allocate
                s.tex = 0b001, s.c = 0b1, s.b = 0b1;
                break;
            case range_t::device:
                // Shareable Device
                s.tex = 0b000, s.c = 0b0, s.b = 0b1;
                break;
            }

            s.set_base((void*)addr);
        }
        tt1[i] = d;
    }

    set_config();
}

void arm_mmu::set_config()
{
    cp15::vmcr::dacr_t dacr = {
        .d0 = cp15::vmcr::dacr_t::d__manager,
    };
    cp15::vmcr::dacr::set(dacr);

    unsigned rgn = 0b00; // Normal memory, (Inner / Outer) Non-cacheable
    cp15::vmcr::ttbr0_t ttbr0 {
        .irgn1 = rgn >> 1,
        .s = 1,
        .rgn = rgn,
        .irgn0 = rgn & 1,
    };
    ttbr0.set_base(tt1);
    cp15::vmcr::ttbr0::set(ttbr0);

    cp15::vmcr::ttbcr_t ttbcr {
        .n = 0,
        .pd0 = 0,
        .pd1 = 1,
    };
    cp15::vmcr::ttbcr::set(ttbcr);
}

const arm_mmu::range_t* arm_mmu::find_addr_in_map(const range_map_t& map, uint32_t addr)
{
    auto p = find_if(map.begin(), map.end(),
                  [=](const range_t& r) {
                      return addr >= r.pa_start && addr <= r.pa_end;
                  });
    return (p == map.end()) ? nullptr : &*p;
}

} // namespace cpu
} // namespace os
