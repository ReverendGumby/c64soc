#include "arm_gic.h"

#include "util/RegValUnion.h"
#include "arm_mpcore.h"

namespace os {
namespace cpu {

static volatile struct icc_regs {
    struct iccicr_t {
        uint32_t enable_s : 1; // Global enable, Secure interrupts
        uint32_t enable_ns : 1; // Global enable, Non-secure interrupts
        uint32_t ack_ctl : 1;
        uint32_t fiq_en : 1;
        uint32_t sbpr : 1;
    };

    struct icciar_t {
        uint32_t ackintid : 10;
        uint32_t cpuid : 3;
    };

    RegValUnion<iccicr_t, uint32_t> iccicr; // CPU Interface Control Register, +0x00
    uint32_t iccpmr; // Interrupt Priority Mask Register, +0x04
    uint32_t iccbpr; // Binary Point Register, +0x08
    RegValUnion<icciar_t, uint32_t> icciar; // Interrupt Acknowledge Register, +0x0c
    RegValUnion<icciar_t, uint32_t> icceoir; // End Of Interrupt Register, +0x10
    uint32_t iccrpr; // Running Priority Register, +0x14
    uint32_t icchpir; // Highest Pending Interrupt Register, +0x18
    uint32_t iccabpr; // Aliased Binary Point Register, +0x1c
} *icc = (volatile struct icc_regs *)arm_pmr.int_ctrl;

static volatile struct icd_regs {
    struct icdsgir_t {
        uint32_t sgiintid : 4;
        uint32_t _res4 : 11;
        uint32_t satt : 1;
        uint32_t cpu_target_list : 8;
        uint32_t target_list_filter : 3;

        enum {
            tlf_only_me = 0b10,
        };
    };

    uint32_t icddcr; // Distributor Control Register, +0x000
    uint32_t icdictr; // Interrupt Controller Type Register, +0x004
    uint32_t icdiidr; // Distributor Implementer Identification Register, +0x008
    uint32_t _res0c[(0x80 - 0x0c) / 4];
    uint32_t icdisr[32]; // Interrupt Security Registers, +0x080
    uint32_t icdiser[32]; // Interrupt Set-enable Registers, +0x100
    uint32_t icdicer[32]; // Interrupt Clear-enable Registers, +0x180
    uint32_t icdispr[32]; // Interrupt Set-pending Registers, +0x200
    uint32_t icdicpr[32]; // Interrupt Clear-pending Registers, +0x280
    uint32_t icdabr[32]; // Interrupt Active Bit Registers, +0x300
    uint32_t _res380[32];
    uint8_t icdipr[256 * 4]; // Interrupt Priority Registers, +0x400
    uint8_t icdiptr[256 * 4]; // Interrupt Processor Target Registers, +0x800
    uint32_t icdicfr[64]; // Interrupt Configuration Registers, +0xC00
    uint32_t ppi_status; // PPI Status Register, +0xD00
    uint32_t spi_status[2]; // SPI Status Registers, +0xD04
    uint32_t _resd0c[(0xf00 - 0xd0c) / 4];
    RegValUnion<icdsgir_t, uint32_t> icdsgir; // Software Generated Interrupt Register, +0xF00
} *icd = (volatile struct icd_regs *)arm_pmr.int_dist;

arm_gic::arm_gic(const config_t& config)
    : config(config)
{
}

void arm_gic::init()
{
    // Prepare for configuration
    icd->icddcr = 0;
    icc->iccicr = 0;

    // Target all SPI to this CPU.
    auto this_cpu = icd->icdiptr[sgi0];         // sgi* read this CPU
    for (int i = spi; i <= config.max_id; i++)
        icd->icdiptr[i] = this_cpu;

    // Configure SPIs with sensitivity / model
    for (int i = spi; i <= config.max_id; i += 16)
        icd->icdicfr[i / 16] = config.icdicfr[(i - spi) / 16];

    // Disable priority masking
    icc->iccpmr = 255;

    // Enable interrupt signalling to this CPU's interface.
    auto iccicr = icc->iccicr;
    iccicr.r.enable_s = 1;
    icc->iccicr = iccicr;

    // Enable the interrupt distributor.
    icd->icddcr = 1;
}

void arm_gic::enable(id_t id)
{
    auto i = id / 32;
    uint32_t bit = 1 << (id % 32);
    icd->icdiser[i] = bit;
}

void arm_gic::disable(id_t id)
{
    auto i = id / 32;
    uint32_t bit = 1 << (id % 32);
    icd->icdicer[i] = bit;
}

void arm_gic::set_pending(id_t id)
{
    if (is_sgi(id)) {
        icd->icdsgir = icd_regs::icdsgir_t {
            .sgiintid = id,
            .target_list_filter = icd_regs::icdsgir_t::tlf_only_me,
        };
    } else {
        auto i = id / 32;
        uint32_t bit = 1 << (id % 32);
        icd->icdispr[i] = bit;
    }
}

void arm_gic::clear_pending(id_t id)
{
    if (is_sgi(id))
        return;
    auto i = id / 32;
    uint32_t bit = 1 << (id % 32);
    icd->icdicpr[i] = bit;
}

bool arm_gic::is_pending(id_t id)
{
    auto i = id / 32;
    uint32_t bit = 1 << (id % 32);
    return icd->icdispr[i] & bit;
}

bool arm_gic::is_active(id_t id)
{
    auto i = id / 32;
    uint32_t bit = 1 << (id % 32);
    return icd->icdabr[i] & bit;
}

arm_gic::ack_t arm_gic::acknowledge()
{
    return icc->icciar;
}

arm_gic::id_t arm_gic::get_id(ack_t ack)
{
    return decltype(icc->icciar){ack}.r.ackintid;
}

void arm_gic::complete(ack_t ack)
{
    icc->icceoir = ack;
}

} // namespace cpu
} // namespace os
