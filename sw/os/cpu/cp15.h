#pragma once

#include <stdint.h>
#include "cp_base.h"

// Section references from ARM DDI 0406C (ARM Architecture Reference Manual, ARMv7-A and ARMv7-R Edition)

namespace os {
namespace cpu {

class cp15 : private cp_base
{
public:
    // B3.18.1 Identification registers
    struct ir {
        struct csselr_t {
            uint32_t in_d : 1; // 1 = I$, 0 = D$
            uint32_t level : 3; // 0 = L1
        };

        struct ccsidr_t {
            uint32_t linesize : 3;
            uint32_t associativity : 10;
            uint32_t num_sets : 15;
            uint32_t wa : 1; // write-allocation
            uint32_t ra : 1; // read-allocation
            uint32_t wb : 1; // write-back
            uint32_t wt : 1; // write-through
        };

        using csselr = cpr<csselr_t, 15, 2, 0, 0, 0>;
        using ccsidr = cpr<ccsidr_t, 15, 1, 0, 0, 0>;
    };

    // B3.18.2 Virtual memory control registers
    struct vmcr {
        struct dacr_t {
            uint32_t d0 : 2;

            static constexpr uint32_t d__manager = 0b11; // accesses not checked
        };

        struct sctlr_t {
            uint32_t m : 1; // MMU enable
            uint32_t a : 1; // Alignment check enable
            uint32_t c : 1; // Data Cache enable
            uint32_t rao3 : 2;
            uint32_t cp15ben : 1; // CP15 barrier enable
            uint32_t rao6 : 1;
            uint32_t b : 1; // Endianness
            uint32_t raz8 : 2;
            uint32_t sw : 1; // SWP and SWBP enable
            uint32_t z : 1; // Branch prediction enable
            uint32_t i : 1; // Instruction cache enable
            uint32_t v : 1; // Vectors bit
            uint32_t rr : 1; // Round Robin select
            uint32_t raz15 : 1;
            uint32_t rao16 : 1;
            uint32_t ha : 1; // Hardware Access flag enable
            uint32_t rao18 : 1;
            uint32_t raz19 : 2;
            uint32_t fi : 1; // Fast interrupts configuration enable
            uint32_t u : 1; // Alignment model
            uint32_t rao23 : 1;
            uint32_t ve : 1; // Interrupt Vectors enable
            uint32_t ee : 1; // Exception Endinness
            uint32_t raz26 : 1;
            uint32_t nmfi : 1; // Non-Maskable FIQ support
            uint32_t tre : 1; // TEX remap enable
            uint32_t afe : 1; // Access flag enable
            uint32_t te : 1; // Thumb Exception enable
        };

        struct ttbcr_t {
            uint32_t n : 3;
            uint32_t sbz3 : 1;
            uint32_t pd0 : 1; // Translation table walk disable for TTBR0
            uint32_t pd1 : 1; // Translation table walk disable for TTBR1
        };

        struct ttbr0_t {
            uint32_t irgn1 : 1; // IRGN[1]
            uint32_t s : 1; // Shareable
            uint32_t imp : 1;
            uint32_t rgn : 2;
            uint32_t nos : 1;
            uint32_t irgn0 : 1;
            // bits [31:x] are Translation table base 0 address, where x = 14 - TTBCR.N.
            uint32_t base : 25;

            void set_base(void *a) { base = uint32_t(a) >> 7; }
        };

        using dacr = cpr<dacr_t, 15, 0, 3, 0, 0>;
        using sctlr = cpr<sctlr_t, 15, 0, 1, 0, 0>;
        using ttbcr = cpr<ttbcr_t, 15, 0, 2, 0, 2>;
        using ttbr0 = cpr<ttbr0_t, 15, 0, 2, 0, 0>;
    };

    // B3.18.4 Other system control registers
    struct oscr {
        struct actlr_t {
            // From ARM 100511_0401_10_en (ARM Cortex-A9 Technical Reference
            // Manual, Revision r4p1)
            uint32_t fw : 1;              // Cache and TLB maintenance broadcast
            uint32_t _res1 : 1;
            uint32_t l1pe : 1;                  // L1 prefetch enable
            uint32_t wflo0m : 1;                // Write full line of zeros mode
            uint32_t _res4 : 2;
            uint32_t smp : 1;
            uint32_t excl : 1;
            uint32_t ai1w : 1;                  // Alloc in one way
            uint32_t pon : 1;                   // Parity on
        };

        struct cpacr_t {
            uint32_t cp0 : 2;
            uint32_t cp1 : 2;
            uint32_t cp2 : 2;
            uint32_t cp3 : 2;
            uint32_t cp4 : 2;
            uint32_t cp5 : 2;
            uint32_t cp6 : 2;
            uint32_t cp7 : 2;
            uint32_t cp8 : 2;
            uint32_t cp9 : 2;
            uint32_t cp10 : 2;
            uint32_t cp11 : 2;
            uint32_t cp12 : 2;
            uint32_t cp13 : 2;
            uint32_t trcdis : 1; // Disable CP14 access to trace registers
            uint32_t _res29 : 1;
            uint32_t d32dis : 1; // Disable use of floating-point regs D16-D31
            uint32_t asedis : 1; // Disable Advanced SIMD functionality

            // Values for cp0-cp13
            enum {
                cp_access_denied = 0b00,
                cp_access_pl1_only = 0b01,
                cp_access_full = 0b11,
            };
        };

        using actlr = cpr<actlr_t, 15, 0, 1, 0, 1>;
        using cpacr = cpr<cpacr_t, 15, 0, 1, 0, 2>;
    };

    // B3.18.6 Cache maintenance operations
    struct cmo {
        struct set_way_t {
            uint32_t level, set, way;
            uint32_t associativity, line_len, n_sets;
            operator uint32_t() const;
        };

        using bpiall = cpr<uint32_t, 15, 0, 7, 5, 6>;
        using bpiallis = cpr<uint32_t, 15, 0, 7, 1, 6>;

        using dccimvac = cpr<uint32_t, 15, 0, 7, 14, 1>;
        using dccisw = cpr<set_way_t, 15, 0, 7, 14, 2>;
        using dccmvac = cpr<uint32_t, 15, 0, 7, 10, 1>;
        using dccsw = cpr<set_way_t, 15, 0, 7, 10, 2>;
        using dcimvac = cpr<uint32_t, 15, 0, 7, 6, 1>;
        using dcisw = cpr<set_way_t, 15, 0, 7, 6, 2>;

        using iciallu = cpr<uint32_t, 15, 0, 7, 5, 0>;
        using icialluis = cpr<uint32_t, 15, 0, 7, 1, 0>;
        using icimvau = cpr<uint32_t, 15, 0, 7, 5, 1>;
    };

    // B3.18.7 TLB maintenance operations
    struct tmo {
        using tlbiall = cpr<uint32_t, 15, 0, 8, 7, 0>;
    };

    // B3.18.9 Miscellaneous operations
    struct mo {
        using tpidrurw = cpr<void*, 15, 0, 13, 0, 2>;
    };

    // B3.18.11 Security Extension registers
    struct se {
        using vbar = cpr<void*, 15, 0, 12, 0, 0>;
    };
};

} // namespace cpu
} // namespace os
