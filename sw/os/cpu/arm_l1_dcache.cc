#include "arm_l1_dcache.h"

#include "arm_sys.h"
#include "cp15.h"

namespace os {
namespace cpu {

void arm_l1_dcache::init()
{
    level = 0;
    is_inst = false;
    arm_core_cache::init();

    invalidate_all();
}

void arm_l1_dcache::invalidate(addr_t a)
{
    cp15::cmo::dcimvac::set(a);
}

void arm_l1_dcache::clean(addr_t a)
{
    cp15::cmo::dccmvac::set(a);
}

void arm_l1_dcache::clean_invalidate(addr_t a)
{
    cp15::cmo::dccimvac::set(a);
}

void arm_l1_dcache::sync()
{
    __arm_dsb();
}

void arm_l1_dcache::invalidate(set_way_t sw)
{
    cp15::cmo::dcisw::set(set_way(sw));
}

void arm_l1_dcache::clean(set_way_t sw)
{
    cp15::cmo::dccsw::set(set_way(sw));
}

void arm_l1_dcache::clean_invalidate(set_way_t sw)
{
    cp15::cmo::dccisw::set(set_way(sw));
}

} // namespace cpu
} // namespace os
