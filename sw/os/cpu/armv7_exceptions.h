#pragma once

#include <stdint.h>

namespace os {
namespace cpu {

//////////////////////////////////////////////////////////////////////
// ARMv7 exception handling
// Implementation includes Security Extension

class armv7_exceptions
{
public:
    typedef enum {
        reset = 0,                              // Not used
        undef_inst = 1,                         // Undefined Instruction
        super_call = 2,                         // Supervisor Call
        prefetch_abort = 3,                     // Prefetch Abort
        data_abort = 4,                         // Data Abort
        hyp_trap = 5,                           // Not used
        irq_int = 6,                            // IRQ interrupt
        fiq_int = 7,                            // FIQ interrupt
        num_vectors
    } vector_t;

    // Exception stack layout
    struct context_t {
        uint64_t d[8];                          // D0 - D7
        uint32_t r[13];                         // R0 - R12
        uint32_t lr;                            // R14 (PC + N)
    };

    using handler_t = void(*)(uintptr_t arg, context_t* ctx);
    struct handler_arg_t {
        handler_t handler;
        uintptr_t arg;
    };

    void init();

    void set_handler(vector_t, handler_t, uintptr_t);

private:
    static handler_arg_t handlers[num_vectors];
};

} // namespace cpu
} // namespace os
