#include "arm_timer.h"

#include "util/RegValUnion.h"
#include "arm_mpcore.h"

#include <stdint.h>

namespace os {
namespace cpu {

static volatile struct timer_regs {
    struct control_t {
        uint32_t tmr_en : 1; // Timer Enable
        uint32_t comp_en : 1; // Comp Enable
        uint32_t irq_en : 1; // IRQ Enable
        uint32_t auto_inc : 1; // Auto-increment
        uint32_t _res4 : 4;
        uint32_t prescalar : 8; // Prescalar
        uint32_t _res16 : 16;
    };

    struct int_status_t {
        uint32_t event : 1; // (w1c) Event flag
    };

    uint32_t counter_lo; // Global Timer Counter Registers, +0x00
    uint32_t counter_hi; // Global Timer Counter Registers, +0x04
    RegValUnion<control_t, uint32_t> control; // Global Timer Control Register, +0x08
    RegValUnion<int_status_t, uint32_t> int_status; // Global Timer Interrupt Status Register, +0x0C
    uint32_t comp_val_lo; // Comparator Value Registers, +0x10
    uint32_t comp_val_hi; // Comparator Value Registers, +0x14
    uint32_t auto_inc; // Auto-increment Register, +0x18

} *regs = (volatile struct timer_regs *)arm_pmr.glb_tmr;

void arm_timer::init()
{
    // Reset all control registers
    regs->control = 0;

    // Reset and start global counter
    clear_counter();
}

void arm_timer::clear_counter()
{
    auto control = regs->control;
    control.r.tmr_en = 0;
    regs->control = control;

    regs->counter_lo = 0;
    regs->counter_hi = 0;

    control.r.tmr_en = 1;
    regs->control = control;
}

arm_timer::count_t arm_timer::get_counter()
{
    uint32_t hi, hi_p, lo;

    hi = regs->counter_hi;
    do {
        lo = regs->counter_lo;
        hi_p = hi;
        hi = regs->counter_hi;
    } while (hi != hi_p);

    return (count_t(hi) << 32) | lo;
}

void arm_timer::set_comparator(count_t comp)
{
    auto control = regs->control;
    control.r.comp_en = 0;
    regs->control = control;

    regs->comp_val_lo = uint32_t(comp);
    regs->comp_val_hi = uint32_t(comp >> 32);

    regs->int_status = decltype(regs->int_status.r) { .event = 1 };

    control.r.comp_en = 1;
    regs->control = control;
}

void arm_timer::disable_comparator()
{
    auto control = regs->control;
    control.r.comp_en = 0;
    regs->control = control;
}

bool arm_timer::is_event_pending()
{
    return regs->int_status.r.event;
}

void arm_timer::enable_event_interrupt(bool en)
{
    auto control = regs->control;
    control.r.irq_en = en;
    regs->control = control;
}

} // namespace cpu
} // namespace os
