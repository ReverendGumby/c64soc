#include "cp15.h"

#include <util/math.h>

#include <stdint.h>

namespace os {
namespace cpu {

cp15::cmo::set_way_t::operator uint32_t() const
{
    unsigned s = ceil_log2(n_sets);
    unsigned l = ceil_log2(line_len);
    unsigned b = l + s;
    unsigned a = ceil_log2(associativity);

    uint32_t r = 0;
    r |= level << 1;
    r |= (set << l) & ((1 << b) - 1);
    r |= way << (32 - a);

    return r;
}

} // namespace cpu
} // namespace os
