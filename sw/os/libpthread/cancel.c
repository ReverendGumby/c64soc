#include <pthread.h>

static int curstate;

// TEMPORARY: required by newlib _fclose_r() etc.
int pthread_setcancelstate(int state, int *oldstate)
{
    *oldstate = curstate;
    curstate = state;
    return 0;
}

// __gthread_active_p() tests this function's existence, and some gthread
// wrappers (eg all mutexes) turn into NOPs if missing.
int pthread_cancel(pthread_t pthread)
{
    return 0;
}
