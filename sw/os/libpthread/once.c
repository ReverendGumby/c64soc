#include <pthread.h>

#include <assert.h>

int pthread_once(pthread_once_t* once_control, void (*init_routine)(void))
{
    assert(once_control->is_initialized);
    if (!once_control->init_executed) {
        init_routine();
        once_control->init_executed = 1;
    }
    return 0;
}
