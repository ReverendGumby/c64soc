#include <os/thread/scheduler.h>
#include <os/thread/thread.h>

#include <pthread.h>
#include <errno.h>

using namespace os::thread;

int pthread_create(pthread_t *pthread, const pthread_attr_t *attr,
                   void *(*start_routine)(void *), void *arg)
{
    auto nt = new thread {start_routine, arg};
    *pthread = pthread_t(nt->get_id());

    nt->run();

    return 0;
}

pthread_t pthread_self(void)
{
    auto ct = scheduler::get_current_thread();
    return pthread_t(ct->get_id());
}

int pthread_join(pthread_t pthread, void **value_ptr)
{
    auto t = thread::find(int(pthread));
    if (t == nullptr)
        return EINVAL;
    auto ct = scheduler::get_current_thread();
    ct->wait_for_exit(t);
    // pthread_exit value is not implemented.
    if (value_ptr)
        *value_ptr = 0;
    delete t;
    return 0;
}

void pthread_yield (void)
{
    scheduler.schedule();
}

int pthread_detach(pthread_t pthread)
{
    // TODO: For thread::detach
    return ENOSYS;
}
