#include <os/thread/condvar.h>
#include <os/thread/scheduler.h>

#include "mutex.h"

#include <pthread.h>
#include <errno.h>

using namespace os::thread;

static condvar* get_or_create_condvar(pthread_cond_t* cond)
{
    condvar* cv;
    if (*cond == PTHREAD_COND_INITIALIZER) {
        cv = new condvar {};
        *cond = reinterpret_cast<pthread_cond_t>(cv);
    } else
        cv = reinterpret_cast<condvar*>(*cond);
    return cv;
}

int pthread_cond_init(pthread_cond_t *cond, const pthread_condattr_t *attr)
{
    get_or_create_condvar(cond);
    return 0;
}

int pthread_cond_destroy(pthread_cond_t *cond)
{
    auto cv = get_or_create_condvar(cond);
    if (cv->has_waiters())
        return EBUSY;
    *cond = 0;
    delete cv;
    return 0;
}

int pthread_cond_signal(pthread_cond_t *cond)
{
    auto cv = get_or_create_condvar(cond);
    cv->signal();
    return 0;
}

int pthread_cond_broadcast(pthread_cond_t *cond)
{
    auto cv = get_or_create_condvar(cond);
    cv->broadcast();
    return 0;
}

int pthread_cond_wait(pthread_cond_t *cond, pthread_mutex_t *mutex)
{
    auto cv = get_or_create_condvar(cond);
    auto mtx = get_or_create_mutex(mutex);
    auto ct = scheduler::get_current_thread();
    cv->wait(ct, mtx);
    return 0;
}
