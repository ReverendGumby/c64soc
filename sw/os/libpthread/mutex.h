#pragma once

#include <pthread.h>
#include <os/thread/mutex.h>

os::thread::mutex* get_or_create_mutex(pthread_mutex_t* ptmutex);
