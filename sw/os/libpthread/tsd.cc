// TSD: Thread-Specific Data

#include <os/thread/scheduler.h>
#include <os/thread/thread.h>

#include <pthread.h>
#include <errno.h>

using namespace os::thread;

int pthread_key_create (pthread_key_t* key, void (*destructor)(void*))
{
    int id = thread::add_local(destructor);
    *key = id;
    return 0;
}

int pthread_setspecific (pthread_key_t key, const void* value)
{
    auto ct = scheduler::get_current_thread();
    ct->set_local(key, const_cast<void*>(value));
    return 0;
}

void* pthread_getspecific (pthread_key_t key)
{
    auto ct = scheduler::get_current_thread();
    return ct->get_local(key);
}

int pthread_key_delete (pthread_key_t key)
{
    return ENOSYS;
}
