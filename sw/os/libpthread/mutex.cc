#include "mutex.h"

#include <os/thread/mutex.h>

#include <pthread.h>
#include <errno.h>

using namespace os::thread;

mutex* get_or_create_mutex(pthread_mutex_t* ptmutex)
{
    mutex* mtx;
    if (*ptmutex == PTHREAD_MUTEX_INITIALIZER) {
        mtx = new mutex {};
        *ptmutex = reinterpret_cast<pthread_mutex_t>(mtx);
    } else
        mtx = reinterpret_cast<mutex*>(*ptmutex);
    return mtx;
}

int pthread_mutexattr_init(pthread_mutexattr_t* attr)
{
    attr->is_initialized = 1;
    attr->type = PTHREAD_MUTEX_NORMAL;
    attr->recursive = 0;
    return 0;
}

int pthread_mutexattr_destroy(pthread_mutexattr_t* attr)
{
    attr->is_initialized = 0;
    return 0;
}

int pthread_mutexattr_gettype(pthread_mutexattr_t* attr, int* kind)
{
    *kind = attr->type;
    return 0;
}

int pthread_mutexattr_settype(pthread_mutexattr_t* attr, int kind)
{
    if (kind == PTHREAD_MUTEX_NORMAL || kind == PTHREAD_MUTEX_RECURSIVE) {
        attr->type = kind;
        attr->recursive = kind == PTHREAD_MUTEX_RECURSIVE;
    } else {
        return EINVAL;
    }
    return 0;
}

int pthread_mutex_init(pthread_mutex_t* ptmutex, const pthread_mutexattr_t* attr)
{
    bool recursive = false;
    if (attr) {
        recursive = attr->recursive;
    }
    auto mtx = new mutex {recursive};
    *ptmutex = reinterpret_cast<pthread_mutex_t>(mtx);
    return 0;
}

int pthread_mutex_destroy(pthread_mutex_t *ptmutex)
{
    auto mtx = get_or_create_mutex(ptmutex);
    if (mtx->has_waiters())
        return EBUSY;
    *ptmutex = 0;
    delete mtx;
    return 0;
}

int pthread_mutex_lock(pthread_mutex_t *ptmutex)
{
    auto mtx = get_or_create_mutex(ptmutex);
    mtx->lock();
    return 0;
}

int pthread_mutex_trylock(pthread_mutex_t *ptmutex)
{
    auto mtx = get_or_create_mutex(ptmutex);
    return mtx->trylock() ? 0 : EBUSY;
}

int pthread_mutex_unlock(pthread_mutex_t *ptmutex)
{
    auto mtx = get_or_create_mutex(ptmutex);
    mtx->unlock();
    return 0;
}
