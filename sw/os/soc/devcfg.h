#pragma once

#include <stddef.h>
#include <stdint.h>
#include "module.h"
#include "util/iovec.h"

namespace os {
namespace soc {

class devcfg : public module
{
public:
    struct bitstream_t {
        // All sizes are in 32-bit words.

        size_t total_wlen;                      // total bitstream length

        // Load portion of bitstream into vectored buffer.
        virtual void load(iovec& iov, size_t buf_wlen, size_t woff) = 0;
    };

    struct status_t {
        uint32_t int_sts;                       // Interrupt Status, +0x00C
        uint32_t status;                        // Status Register, +0x014
    };

    devcfg(id_t);

    // Configure (program) the PL with a bitstream
    void configure_pl(bitstream_t&);

    status_t get_status();

private:
    void set_enable(bool);
    void clear_int();
    void init_pl();
    void start_config();
    void dma_xfer(iovec& iov, bool last);
    void dma_xfer(const buffer& buf, bool last);
    void end_config();

    struct devcfg_regs;
    volatile struct devcfg_regs* regs;
};

} // namespace soc
} // namespace os
