#include "module.h"
#include "soc.h"
#include "module_base.h"
#include "interrupt.h"

#include <unordered_map>

namespace os {
namespace soc {

module::module(id_t id)
    : id(id)
{
    set_name(get_info(id).name);
    register_name(get_name(), this);
}

int module::instance() const
{
    int i;
    switch (id) {
    case id_t::usb1:
    case id_t::gem1:
    case id_t::sdio1:
    case id_t::spi1:
    case id_t::can1:
    case id_t::i2c1:
    case id_t::uart1:
        i = 1;
        break;
    default:
        i = 0;
        break;
    }
    return i;
}

void module::init()
{
    hw_reset();
}

void module::deinit()
{
    hw_reset();
}

void module::hw_reset()
{
}

void module::sw_reset()
{
}

uint32_t module::get_base_addr()
{
    return get_info(id).base;
}

uint8_t module::get_base_irq()
{
    return get_info(id).irq;
}

const module::info_t& module::get_info(id_t id)
{
    static const std::unordered_map<id_t, info_t> infos {
        { id_t::dmac,
          {
              .name = "soc/dmac",
              .base = module_base.dmac0_s,
              .irq = interrupt.dmac0,
          }
        },
        { id_t::usb0,
          {
              .name = "soc/usb0",
              .base = module_base.usb0,
              .irq = interrupt.usb0,
          }
        },
        { id_t::usb1,
          {
              .name = "soc/usb1",
              .base = module_base.usb1,
              .irq = interrupt.usb1,
          }
        },
        { id_t::gem0,
          {
              .name = "soc/gem0",
              .base = module_base.gem0,
              .irq = interrupt.gem0,
          }
        },
        { id_t::gem1,
          {
              .name = "soc/gem1",
              .base = module_base.gem1,
              .irq = interrupt.gem1,
          }
        },
        { id_t::sdio0,
          {
              .name = "soc/sdio0",
              .base = module_base.sd0,
              .irq = interrupt.sdio0,
          }
        },
        { id_t::sdio1,
          {
              .name = "soc/sdio1",
              .base = module_base.sd1,
              .irq = interrupt.sdio1,
          }
        },
        { id_t::spi0,
          {
              .name = "soc/spi0",
              .base = module_base.spi0,
              .irq = interrupt.spi0,
          }
        },
        { id_t::spi1,
          {
              .name = "soc/spi1",
              .base = module_base.spi1,
              .irq = interrupt.spi1,
          }
        },
        { id_t::can0,
          {
              .name = "soc/can0",
              .base = module_base.can0,
              .irq = interrupt.can0,
          }
        },
        { id_t::can1,
          {
              .name = "soc/can1",
              .base = module_base.can1,
              .irq = interrupt.can1,
          }
        },
        { id_t::i2c0,
          {
              .name = "soc/i2c0",
              .base = module_base.i2c0,
              .irq = interrupt.i2c0,
          }
        },
        { id_t::i2c1,
          {
              .name = "soc/i2c1",
              .base = module_base.i2c1,
              .irq = interrupt.i2c1,
          }
        },
        { id_t::uart0,
          {
              .name = "soc/uart0",
              .base = module_base.uart0,
              .irq = interrupt.uart0,
          }
        },
        { id_t::uart1,
          {
              .name = "soc/uart1",
              .base = module_base.uart1,
              .irq = interrupt.uart1,
          }
        },
        { id_t::gpio,
          {
              .name = "soc/gpio",
              .base = module_base.gpio,
              .irq = interrupt.gpio,
          }
        },
        { id_t::qspi,
          {
              .name = "soc/qspi",
              .base = module_base.qspi,
              .irq = interrupt.qspi,
          }
        },
        { id_t::smc,
          {
              .name = "soc/smc",
              .base = module_base.smcc,
              .irq = interrupt.smc,
          }
        },
        { id_t::devcfg,
          {
              .name = "soc/devcfg",
              .base = module_base.devcfg,
              .irq = interrupt.devc,
          }
        },
        { id_t::ocm,
          {
              .name = "soc/ocm",
              .base = module_base.ocm,
              .irq = interrupt.apu_ocm,
          }
        },
    };

    return infos.at(id);
}

} // namespace soc
} // namespace os
