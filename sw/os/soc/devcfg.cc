#include "util/RegValUnion.h"
#include "util/timer.h"
#include "util/debug.h"
#include "util/profiler.h"
#include "os/cpu/cpu.h"
#include <assert.h>
#include <stdint.h>
#include <stdexcept>
#include <algorithm>

#include "devcfg.h"

#define DBG_LVL 1
#define DBG_TAG "DEVCFG"

namespace os {
namespace soc {

struct devcfg::devcfg_regs
{
    struct ctrl_t {
        uint32_t dap_en : 3;
        uint32_t dbgen : 1;
        uint32_t niden : 1;
        uint32_t spiden : 1;
        uint32_t spniden : 1;
        uint32_t sec_en : 1;
        uint32_t seu_en : 1;
        uint32_t pcfg_aes_en : 3;
        uint32_t pcfg_aes_fuse : 1;
        uint32_t _res13 : 1;                    // Always write with 1
        uint32_t _res14 : 1;                    // Always write with 1
        uint32_t _res15 : 1;                    // Do not modify
        uint32_t _res16 : 7;
        uint32_t jtag_chain_dis : 1;
        uint32_t multiboot_en : 1;
        uint32_t quarter_pcap_rate_en : 1;
        uint32_t pcap_mode : 1;
        uint32_t pcap_pr : 1;
        uint32_t _res28 : 1;
        uint32_t pcfg_por_cnt_4k : 1;
        uint32_t pcfg_prog_b : 1;
        uint32_t force_rst : 1;
    };

    // All bits are write 1 to clear.
    struct int_sts_t {
        uint32_t pcfg_init_ne_int : 1;    // negative edge of the PL INIT signal
        uint32_t pcfg_init_pe_int : 1;    // positive edge of the PL INIT signal
        uint32_t pcfg_done_int : 1;       // DONE signal from PL
        uint32_t pcfg_cfg_rst_int : 1; // PL configuration controller is under reset
        uint32_t pcfg_por_b_int : 1; // PL loses power, PL POR_B signal goes low
        uint32_t pcfg_seu_err_int : 1;        // SEU error received from the PL
        uint32_t pcfg_hmac_err_int : 1;       // HMAC error received from the PL
        uint32_t _res7 : 4;
        uint32_t p2d_len_err_int : 1; // Inconsistent PCAP to DMA transfer length error
        uint32_t d_p_done_int : 1;    // Both DMA and PCAP transfers are done
        uint32_t dma_done_int : 1;    // DMA command is done
        uint32_t dma_q_ov_int : 1;    // DMA command queue overflows
        uint32_t dma_cmd_err_int : 1; // Illegal DMA command
        uint32_t rd_fifo_lvl_int : 1; // Rx FIFO level >= threshold, see reg 0x008
        uint32_t wr_fifo_lvl_int : 1; // Tx FIFO level < threshold, see reg 0x008
        uint32_t rx_fifo_ov_int : 1;  // RX FIFO overflow
        uint32_t _res19 : 1;          // 
        uint32_t axi_rerr_int : 1;    // AXI read response error
        uint32_t axi_rto_int : 1;     // AXI read address or response time out
        uint32_t axi_werr_int : 1;    // AXI write response error
        uint32_t axi_wto_int : 1; // AXI write address, data or response time out
        uint32_t _res24 : 3;
        uint32_t pss_cfg_reset_b_int : 1;  // PL configuration reset, both edges
        uint32_t pss_gts_cfg_b_int : 1; // Tri-state PL IO during configuration, both edges
        uint32_t pss_gpwrdwn_b_int : 1; // Global power down, both edges
        uint32_t pss_fst_cfg_b_int : 1; // First configuration done, both edges
        uint32_t pss_gts_usr_b_int : 1; // Tri-state PL IO during HIZ, both edges
    };

    struct status_t {
        uint32_t _res0 : 1;
        uint32_t efuse_jtag_dis : 1;
        uint32_t efuse_sec_en : 1;
        uint32_t efuse_bbram_key_disable : 1;
        uint32_t pcfg_init : 1;            // PL INIT signal
        uint32_t pss_cfg_reset_b : 1;      // PL configuration reset, active low
        uint32_t illegal_apb_access : 1;
        uint32_t secure_rst : 1;
        uint32_t pss_gts_cfg_b : 1;   // Tri-state IO during config, active low
        uint32_t pss_gpwrdwn_b : 1;   // Global power down, active low
        uint32_t pss_fst_cfg_b : 1;   // First PL configuration done, active low
        uint32_t pss_gts_usr_b : 1;   // Tri-state IO during HIZ, active low
        uint32_t tx_fifo_lvl : 7;
        uint32_t _res19 : 1;
        uint32_t rx_fifo_lvl : 5;
        uint32_t _res25 : 3;
        uint32_t dma_done_cnt : 2; // Number of completed DMA transfers that have not been acknowledged by SW
        uint32_t dma_cmd_q_e : 1;  // DMA command queue empty
        uint32_t dma_cmd_q_f : 1;  // DMA command queue full
    };

    struct mctrl_t {
        uint32_t ps_version : 4;                // Version ID for silicon
        uint32_t _res9 : 19;
        uint32_t pcfg_por_b : 1;                // PL POR_B signal
        uint32_t _res5 : 3;
        uint32_t int_pcap_lpbk : 1;             // Internal PCAP loopback
        uint32_t _res0 : 4;
    };

    RegValUnion<ctrl_t, uint32_t> ctrl;         // Control Register, +0x000
    uint32_t lock;                              // lock changes, +0x004
    uint32_t cfg;                               // Configuration Register, +0x008
    RegValUnion<int_sts_t, uint32_t> int_sts;   // Interrupt Status, +0x00C
    uint32_t int_mask;                          // Interrupt Mask, +0x010
    RegValUnion<status_t, uint32_t> status;     // Status Register, +0x014
    uint32_t dma_src_addr;                      // DMA Source address, +0x018
    uint32_t dma_dst_addr;            // DMA Destination address, +0x01C
    uint32_t dma_src_len;             // DMA Source transfer Length, +0x020
    uint32_t dma_dst_len;             // DMA Destination transfer Length, +0x024
    uint32_t _res28;
    uint32_t multiboot_addr;
    uint32_t _res30;
    uint32_t unlock;
    uint32_t _res38[(0x80 - 0x38) / 4];
    RegValUnion<mctrl_t, uint32_t> mctrl;       // Miscellaneous control, +0x080
};

devcfg::devcfg(id_t id)
    : module(id)
{
    regs = reinterpret_cast<volatile struct devcfg_regs*>(get_base_addr());
}

// Programming steps from Zync 7000 TRM (UG585), Section 6.4.2 Boot Sequence Examples

void devcfg::configure_pl(bitstream_t& bs)
{
    set_enable(true);
    init_pl();
    start_config();

    constexpr size_t load_wlen = 0x200;
    size_t xfer_wlen;
    size_t woff;
    unsigned last_pct = 0;
    // profiler<3> xfer;
    for (woff = 0; woff < bs.total_wlen; woff += xfer_wlen) {
        // xfer.start();
    	unsigned pct = woff * 100 / bs.total_wlen;
    	if (pct - last_pct >= 10) {
    		last_pct = pct;
            DBG_PRINTF(10, "%08x (%u%%)\n", woff, pct);
    	}
        // xfer.split(0);
        xfer_wlen = std::min(bs.total_wlen - woff, load_wlen);
        iovec iov;
        bs.load(iov, xfer_wlen, woff);
        // xfer.split(1);
        bool last = bs.total_wlen == woff;
        dma_xfer(iov, last);
        // xfer.split(2);
    }
    DBG_PRINTF(10, "%08x (100%%)\n", woff);
    // xfer.dump("xfer", timer::us);

    end_config();
    set_enable(false);
}

void devcfg::set_enable(bool e)
{
    // Enable the PCAP bridge and select PCAP for reconfiguration
    auto ctrl = regs->ctrl;
    ctrl.r.pcap_mode = 1;
    ctrl.r.pcap_pr = 1;
    regs->ctrl = ctrl;
}

void devcfg::clear_int()
{
    // Clear the Interrupts
    regs->int_sts = regs->int_sts;
}

void devcfg::init_pl()
{
    // Initialize the PL (clears previous configuration)
    auto ctrl = regs->ctrl;
    ctrl.r.pcfg_prog_b = 1;
    regs->ctrl = ctrl;
    ctrl.r.pcfg_prog_b = 0;
    regs->ctrl = ctrl;

    while (regs->status.r.pcfg_init != 0)
        ;

    ctrl = regs->ctrl;
    ctrl.r.pcfg_prog_b = 1;
    regs->ctrl = ctrl;

    while (regs->status.r.pcfg_init != 1)
        ;
}

void devcfg::start_config()
{
	// Do this after init_pl(), in case that sets PCFG_DONE_INT
    clear_int();

    // Disable the PCAP loopback
    auto mctrl = regs->mctrl;
    mctrl.r.int_pcap_lpbk = 0;
    regs->mctrl = mctrl;

    // Program the PCAP_2x clock divider
    auto ctrl = regs->ctrl;
    ctrl.r.quarter_pcap_rate_en = 0; // non-secure mode
    regs->ctrl = ctrl;
}

void devcfg::dma_xfer(iovec& iov, bool last)
{
    for (auto it = iov.begin(); it != iov.end(); it++)
    {
        auto& iob = *it;
        auto buf = iov.merge_consecutive(*iob, it);
        dma_xfer(buf, last);
    }
}

void devcfg::dma_xfer(const buffer& buf, bool last)
{
    size_t wlen = buf.len / sizeof(uint32_t);
    assert(wlen * sizeof(uint32_t) == buf.len);

    // Queue-up a DMA transfer using the devcfg DMA registers
    regs->dma_src_addr = reinterpret_cast<uint32_t>(buf.ptr) | last;
    regs->dma_dst_addr = 0xFFFFFFFF;
    regs->dma_src_len = wlen;
    regs->dma_dst_len = wlen;

    // Wait for the DMA transfer to be done
    decltype(regs->int_sts) int_sts;
    do {
        int_sts = regs->int_sts;
    } while (int_sts.r.dma_done_int == 0);
    regs->int_sts = devcfg_regs::int_sts_t{.dma_done_int = 1};

    // Check for errors
    int_sts = regs->int_sts;
    bool errors = int_sts.r.axi_werr_int
        || int_sts.r.axi_rto_int
        || int_sts.r.axi_rerr_int
        || int_sts.r.rx_fifo_ov_int
        || int_sts.r.dma_cmd_err_int
        || int_sts.r.dma_q_ov_int
        || int_sts.r.p2d_len_err_int
        || int_sts.r.pcfg_hmac_err_int;
    if (errors) {
        DBG_PRINTF(0, "int_sts=0x%08lx, status=0x%08lx\n", regs->int_sts.v, regs->status.v);
        throw std::runtime_error("devcfg::configure_pl: dma_xfer error");
    }
}

void devcfg::end_config()
{
    // Make sure the PL configuration is done
    timer t;
    t.start(10 * timer::ms); // total guess
    while (!regs->int_sts.r.pcfg_done_int) {
        if (t.expired()) {
            DBG_PRINTF(0, "int_sts=0x%08lx, status=0x%08lx\n", regs->int_sts.v, regs->status.v);
            throw std::runtime_error("devcfg::configure_pl: end_config timeout");
        }
    }
}

devcfg::status_t devcfg::get_status()
{
    return status_t {
        .int_sts = regs->int_sts.v,
        .status = regs->status.v,
    };
}

} // namespace soc
} // namespace os
