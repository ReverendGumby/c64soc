#pragma once

#include "module.h"
#include <vector>
#include <stdexcept>
#include "util/buffer.h"

namespace os {
namespace soc {

class i2c : public module
{
public:
    struct config_t {
        unsigned scl_hz;
    };

    struct msg_t {
        buffer data;
        bool rnw;                               // true=Read, false=Write

        static msg_t read(const buffer& data)
        {
            return msg_t{data, true};
        }
        static msg_t write(const buffer& data)
        {
            return msg_t{data, false};
        }
    };

    struct xfer_t {
        unsigned slave_addr; // 7-bit
        std::vector<msg_t> msgs;
    };

    class error : public std::runtime_error
	{
    public:
        error(const char *what_arg) : std::runtime_error(what_arg) {}
	};

    i2c(id_t);

    void hw_reset() override;

    void set_config(const config_t&);

    bool probe(unsigned slave_addr); // 7-bit
    void transfer(xfer_t&);

private:
    struct i2c_regs;
    volatile struct i2c_regs* regs;

    struct clk_divs
    {
        unsigned a, b;
    };
    struct transfer_state;

    clk_divs find_clk_divs(unsigned hz);
    void transfer_read(transfer_state&);
    void transfer_write(transfer_state&);
    void check_error();
};

} // namespace soc
} // namespace os
