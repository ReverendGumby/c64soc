#pragma once

#include "module.h"
#include <os/dev/char_device.h>
#include <os/cpu/interrupts.h>

namespace os {
namespace soc {

class uart : public module, public os::dev::char_device
{
public:
    struct config_t {
        int baud;
    };

    uart(id_t);

    void hw_reset() override;
    void sw_reset() override;

    void set_config(const config_t&);
    void start();

    // Interface for char_device
    bool is_rx_empty() override;
    bool is_tx_full() override;
    char rx() override;
    void tx(char b) override;
    bool has_event(event_t) override;
    void set_event_handler(event_handler_t) override;
    void set_event_enable(event_t, bool) override;

    bool check_error();

private:
    struct uart_regs;
    volatile struct uart_regs* regs;
    os::cpu::interrupt* irq;
    event_handler_t event_fn;

    struct baud_clk_divs {
        int clks; // Clock source select
        int cd;
        int bdiv;
    };

    void set_enable(bool);

    // Compute the baud clock dividers that produce the lowest error
    baud_clk_divs find_baud_clk_divs(int baud);

    void irq_handler();
};

} // namespace soc
} // namespace os
