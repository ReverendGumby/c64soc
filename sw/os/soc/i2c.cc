#include "i2c.h"

#include "soc.h"
#include "ps7_init.h" // for I2C_FREQ
#include "util/debug.h"
#include "util/RegValUnion.h"

#include <stdint.h>
#include <cmath>

#define DBG_LVL 0
#define DBG_TAG "I2C"

namespace os {
namespace soc {

struct i2c::i2c_regs
{
    struct control_reg_t {
        uint32_t rw : 1;                        // direction of transfer
        uint32_t ms : 1;                        // overall interface mode
        uint32_t nea : 1;                       // addressing mode
        uint32_t ack_en : 1;                    // set to 1
        uint32_t hold : 1;                      // hold bus
        uint32_t slvmon : 1;                    // slave monitor mode
        uint32_t clr_fifo : 1;                  // init FIFO to all 0s
        uint32_t _res7 : 1;
        uint32_t divisor_b : 6;             // divisor for stage B clock divider
        uint32_t divisor_a : 2;             // divisor for stage A clock divider
    };
    struct status_reg_t {
        uint32_t _res0 : 3;
        uint32_t rxrw : 1;                      // RX read_write
        uint32_t _res4 : 1;
        uint32_t rxdv : 1;                      // Receiver Data Valid
        uint32_t txdv : 1;                      // Transmit Data Valid
        uint32_t rxovf : 1;                     // Receiver Overflow
        uint32_t ba : 1;                        // Bus Active
    };
    struct int_reg_t {
        uint32_t comp : 1;                      // Transfer complete
        uint32_t data : 1;                      // More data
        uint32_t nack : 1;                      // Transfer not acknowledged
        uint32_t to : 1;                        // Transfer time out
        uint32_t slv_rdy : 1;                   // Monitored slave ready
        uint32_t rx_ovf : 1;                    // Receive overflow
        uint32_t tx_ovf : 1;                    // FIFO transmit overflow
        uint32_t rx_unf : 1;                    // FIFO receive overflow
        uint32_t _res8 : 1;
        uint32_t arb_lost : 1;                  // arbitration lost
    };

    RegValUnion<control_reg_t, uint32_t> control_reg0; // Control Register, +0x00
    RegValUnion<status_reg_t, uint32_t> status_reg0; // Status register, +0x04
    uint32_t i2c_address_reg0; // IIC Address register, +0x08
    uint32_t i2c_data_reg0; // IIC data register, +0x0C
    RegValUnion<int_reg_t, uint32_t> interrupt_status_reg0; // IIC interrupt status register, +0x10
    uint32_t transfer_size_reg0; // Transfer Size Register, +0x14
    uint32_t slave_mon_pause_reg0; // Slave Monitor Pause Register, +0x18
    uint32_t time_out_reg0; // Time out register, +0x1C
    RegValUnion<int_reg_t, uint32_t> intrpt_mask_reg0; // Interrupt mask register, +0x20
    RegValUnion<int_reg_t, uint32_t> intrpt_enable_reg0; // Interrupt Enable Register, +0x24
    RegValUnion<int_reg_t, uint32_t> intrpt_disable_reg0; // Interrupt Disable Register, +0x28
};

static constexpr unsigned fifo_depth = 16;

i2c::i2c(id_t id)
    : module(id)
{
    regs = reinterpret_cast<volatile struct i2c_regs*>(get_base_addr());
}

void i2c::hw_reset()
{
    constexpr uint32_t I2C0_CPU1X_RST = 1<<0;

    auto& slcr = soc.slcr;
    const uint32_t rst = (I2C0_CPU1X_RST) << instance();
    slcr.pulse_rst_ctrl(slcr.regs->i2c_rst_ctrl, rst);
}

i2c::clk_divs i2c::find_clk_divs(unsigned hz)
{
    int best_a = -1, best_b = -1;
    unsigned best_actual = 0;
    for (int a = 3; a >= 0; a--) {
        // From TRM, Section 20.2.5, I2C Speed
        // I2C SCL Clock = CPU_1X_Clock / (22 * (divisor_a + 1) * (divisor_b + 1))
        int b = I2C_FREQ / (22 * (a + 1) * hz);
        if (b >= 0 && b <= 63) {
            unsigned actual = I2C_FREQ / (22 * (a + 1) * (b + 1));
            if (actual > best_actual && actual < hz) {
                best_actual = actual;
                best_a = a;
                best_b = b;
            }
        }
    }

    if (best_a == -1)
        throw error("find_clk_divs: unsupported hz");

    return clk_divs{unsigned(best_a), unsigned(best_b)};
}

void i2c::set_config(const config_t& cfg)
{
    auto clks = find_clk_divs(cfg.scl_hz);

    auto control = regs->control_reg0;
    control.r.ack_en = 1; // set to 1
    control.r.nea = 1; // normal (7-bit) address
    control.r.ms = 1; // master mode
    control.r.divisor_a = clks.a;
    control.r.divisor_b = clks.b;
    regs->control_reg0 = control;

    regs->time_out_reg0 = 255; // maximum timeout
}

bool i2c::probe(unsigned slave_addr)
{
    xfer_t xfer {
        .slave_addr = slave_addr,
        .msgs {
            msg_t::write(buffer{})
        }
    };
    try {
        transfer(xfer);
        return true;
    } catch (error&) {
        return false;
    }
}

#define DBG_DUMP(lvl)                                               \
    DBG_PRINTF(lvl, "ctrl=%04lX stat=%04lX trsz=%04lX isr=%04lX\n", \
               regs->control_reg0.v,                                \
               regs->status_reg0.v,                                 \
               regs->transfer_size_reg0,                            \
               regs->interrupt_status_reg0.v)

void i2c::check_error()
{
    const char* why = nullptr;

    auto isr = regs->interrupt_status_reg0;
    if (isr.r.nack)
        why = "NACK";
    else if (isr.r.arb_lost)
        why = "arbitration lost";
    else if (isr.r.rx_ovf)
        why = "RX overflow";
    else if (isr.r.rx_unf)
        why = "RX underflow";
    else if (isr.r.tx_ovf)
        why = "TX overflow";
    else if (isr.r.to) {
        if (regs->transfer_size_reg0 == 0xFF)
            // This signals that invalid reads have happened.
            // http://www.xilinx.com/support/answers/61664.htm
            why = "timeout with invalid read";
    }

    if (!why)
        return;

    DBG_DUMP(0);
    auto control = regs->control_reg0;
    control.r.hold = 0;
    control.r.clr_fifo = 1;
    regs->control_reg0 = control;

    while (regs->status_reg0.r.ba)
        ;

    throw error(why);
}

struct i2c::transfer_state {
    xfer_t& xfer;
    uint8_t* pdata;
    size_t len;
    bool last_msg;
    bool started;
    struct i2c_regs::control_reg_t control;
};

void i2c::transfer_read(struct transfer_state& xs)
{
    if (!xs.started) {
        regs->transfer_size_reg0 = xs.len;
        xs.control.hold = !xs.last_msg || xs.len > fifo_depth;
        regs->control_reg0 = xs.control;
        regs->i2c_address_reg0 = xs.xfer.slave_addr;
        xs.started = true;
    }
    DBG_DUMP(10);
    for (; regs->status_reg0.r.rxdv; xs.len--) {
        if (xs.last_msg && xs.control.hold && regs->transfer_size_reg0 <= 1) {
            xs.control.hold = 0;
            regs->control_reg0 = xs.control;
        }
        auto b = uint8_t(regs->i2c_data_reg0);
        DBG_PRINTF(15, "    rx: %02X\n", b);
        *(xs.pdata++) = b;
        DBG_DUMP(10);
        check_error();
    }
}

void i2c::transfer_write(struct transfer_state& xs)
{
    if (xs.len && !regs->status_reg0.r.txdv) {
        regs->interrupt_status_reg0 = i2c_regs::int_reg_t{.comp = 1};
        for (unsigned avail = fifo_depth - regs->transfer_size_reg0;
             avail && xs.len; xs.len--, avail--) {
            auto b = *(xs.pdata++);
            DBG_PRINTF(15, "    tx: %02X\n", b);
            regs->i2c_data_reg0 = b;
        }
    }
    if (!xs.started) {
        xs.control.hold = !xs.last_msg || xs.len > 0;
        regs->control_reg0 = xs.control;
        regs->i2c_address_reg0 = xs.xfer.slave_addr;
        xs.started = true;
    }
    DBG_DUMP(10);
}

void i2c::transfer(xfer_t& xfer)
{
    DBG_PRINTF(5, "addr=%02x, %u msgs\n", xfer.slave_addr, xfer.msgs.size());
    auto msgs_end = xfer.msgs.end();
    for (auto i = xfer.msgs.begin(); i != msgs_end; i++) {
        auto& msg = *i;
        bool rnw = msg.rnw;
        transfer_state xs {
            .xfer = xfer,
            .pdata = msg.data.get_ptr(),
            .len = msg.data.len,
            .last_msg = i == (msgs_end - 1),
            .started = false,
        };
        DBG_PRINTF(8, "  msg: %s %d @%p %s\n",
                   rnw ? "R" : "w", (int)xs.len, xs.pdata,
                   xs.last_msg ? "last" : "");

        xs.control = regs->control_reg0;
        xs.control.rw = rnw;
        xs.control.clr_fifo = 1;
        regs->control_reg0 = xs.control;
        xs.control.clr_fifo = 0;
        regs->interrupt_status_reg0 = ~0; // clear all interrupts

        debug_critical_enter();
        bool done = false;
        while (!(xs.started && done)) {
            if (rnw)
                transfer_read(xs);
            else
                transfer_write(xs);
            check_error();
            // Errata says that back-to-back reads won't set COMP. Avoid them.
            // https://www.xilinx.com/support/answers/61665.html
            done = done || (xs.len == 0 && regs->interrupt_status_reg0.r.comp);
        }
        debug_critical_exit();
    }
}

} // namespace soc
} // namespace os
