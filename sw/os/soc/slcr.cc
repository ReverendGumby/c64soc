#include "slcr.h"
#include "module_base.h"

namespace os {
namespace soc {

slcr::slcr()
{
    regs = reinterpret_cast<volatile struct slcr_regs*>(module_base.slcr);
}

void slcr::lock()
{
    regs->slcr_lock = 0x767B;
}

void slcr::unlock()
{
    regs->slcr_unlock = 0xDF0D;
}

void slcr::pulse_rst_ctrl(volatile uint32_t& rst_ctrl, uint32_t rst_val)
{
    unlock();
    auto cur_val = rst_ctrl;
    rst_ctrl = cur_val | rst_val;
    rst_ctrl = cur_val;
    lock();
}


} // namespace soc
} // namespace os
