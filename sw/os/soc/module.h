#pragma once

#include <stdint.h>
#include <os/dev/device.h>

namespace os {
namespace soc {

class module : public os::dev::device
{
public:
    enum class id_t {
        dmac,
        usb0,
        usb1,
        gem0,
        gem1,
        sdio0,
        sdio1,
        spi0,
        spi1,
        can0,
        can1,
        i2c0,
        i2c1,
        uart0,
        uart1,
        gpio,
        qspi,
        smc,
        devcfg,
        ocm,
    };

    module(id_t id);

    int instance() const;

    virtual void init(void);
    virtual void deinit();

    virtual void hw_reset();
    virtual void sw_reset();

protected:
    id_t id;

    uint32_t get_base_addr();
    uint8_t get_base_irq();

private:
    struct info_t {
        const char* name;
        uint32_t base;
        uint8_t irq;
    };

    static const info_t& get_info(id_t);
};

} // namespace soc
} // namespace os
