#include <stdint.h>

namespace os {
namespace soc {

// From Zync 7000 TRM (UG585), Section 7.2.2 CPU Private Peripheral Interrupts (PPI)

static constexpr struct {
    // To be used as os::cpu::arm_gic::id_t
    uint8_t apu_cpu0 = 32;
    uint8_t apu_cpu1 = 33;
    uint8_t apu_l2c = 34;
    uint8_t apu_ocm = 35;
    uint8_t pmu0 = 37;
    uint8_t pmu1 = 38;
    uint8_t xadc = 39;
    uint8_t devc = 40;
    uint8_t swdt = 41;
    uint8_t ttc0 = 42; // 44:42 = TTC_0 Timer/Clock 2:0
    uint8_t dmac_abort = 45;
    uint8_t dmac0 = 46; // 49:46 = DMAC[3:0]
    uint8_t smc = 50;
    uint8_t qspi = 51;
    uint8_t gpio = 52;
    uint8_t usb0 = 43;
    uint8_t gem0 = 54;
    uint8_t gem0_wake = 55;
    uint8_t sdio0 = 56;
    uint8_t i2c0 = 57;
    uint8_t spi0 = 58;
    uint8_t uart0 = 59;
    uint8_t can0 = 60;
    uint8_t pl0 = 61; // 68:61 = PL[7:0]
    uint8_t ttc1 = 69; // 71:69 = TTC_1 Timer/Clock 2:0
    uint8_t dmac4 = 72; // 75:72 = DMAC[7:4]
    uint8_t usb1 = 76;
    uint8_t gem1 = 77;
    uint8_t gem1_wake = 78;
    uint8_t sdio1 = 79;
    uint8_t i2c1 = 80;
    uint8_t spi1 = 81;
    uint8_t uart1 = 82;
    uint8_t can1 = 83;
    uint8_t pl8 = 84; // 91:84 = PL[15:8]
    uint8_t scu = 92; // Parity

    uint8_t max_id = 92;
} interrupt;

} // namespace soc
} // namespace os
