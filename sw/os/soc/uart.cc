#include "uart.h"

#include "soc.h"
#include "ps7_init.h" // for UART_FREQ
#include "util/RegValUnion.h"
#include <os/cpu/cpu.h>

#include <stdint.h>
#include <cmath>

namespace os {
namespace soc {

struct uart::uart_regs
{
    struct control_reg_t {
        uint32_t rxres : 1; // Software reset for Rx data path
        uint32_t txres : 1; // Software reset for Tx data path
        uint32_t rxen : 1; // Receive enable
        uint32_t rxdis : 1; // Receive disable
        uint32_t txen : 1; // Transmit enable
        uint32_t txdis : 1; // Transmit disable
        uint32_t rstto : 1; // Restart receiver timeout counter
        uint32_t sttbrk : 1; // Start transmitter break
        uint32_t stpbrk : 1; // Stop transmitter break
        uint32_t _res9 : 23;
    };
    struct mode_reg_t {
        uint32_t clks : 1; // Clock source select: 0=/1, 1=/8
        uint32_t chrl : 2; // Character length select
        uint32_t par : 3; // Parity type select
        uint32_t nbstop : 2; // Number of stop bits
        uint32_t chmode : 2; // Channel mode
        uint32_t _res10 : 22;

        enum {
            clks_uart_ref_clk = 0b0,
            clks_uart_ref_clk_div_8 = 0b1,

            chrl_8b = 0b00,
            chrl_7b = 0b10,
            chrl_6b = 0b11,

            par_even = 0b000,
            par_odd = 0b001,
            par_space = 0b010,
            par_mark = 0b011,
            par_none = 0b100,

            nbstop_1b = 0b00,
            nbstop_1b5 = 0b01,
            nbstop_2b = 0b10,

            chmode_normal = 0b00,
            chmode_automatic_echo = 0b01,
            chmode_local_loopback = 0b10,
            chmode_remote_loopback = 0b11,
        };
    };
    struct int_reg_t {
        uint32_t rtrig : 1; // Receiver FIFO Trigger interrupt
        uint32_t rempty : 1; // Receiver FIFO Empty interrupt
        uint32_t rful : 1; // Receiver FIFO Full interrupt
        uint32_t tempty : 1; // Transmitter FIFO Empty interrupt
        uint32_t tful : 1; // Transmitter FIFO Full interrupt
        uint32_t rovr : 1; // Receiver Overflow Error interrupt
        uint32_t frame : 1; // Receiver Framing Error interrupt
        uint32_t pare : 1; // Receiver Parity Error interrupt
        uint32_t timeout : 1; // Receiver Timeout Error interrupt
        uint32_t dmsi : 1; // Delta Modem Status Indicator interrupt
        uint32_t ttrig : 1; // Transmitter FIFO Trigger interrupt
        uint32_t tnful : 1; // Transmitter FIFO Nearly Full interrupt
        uint32_t tovr : 1; // Transmitter FIFO Overflow interrupt
        uint32_t _res13 : 19;
    };
    struct chnl_sts_reg_t {
        uint32_t rtrig : 1; // Receiver FIFO Trigger continuous status
        uint32_t rempty : 1; // Receiver FIFO Empty continuous status
        uint32_t rful : 1; // Receiver FIFO Full continuous status
        uint32_t tempty : 1; // Transmitter FIFO Empty continuous status
        uint32_t tful : 1; // Transmitter FIFO Full continuous status
        uint32_t _res5 : 5;
        uint32_t ractive : 1; // Receiver state machine active status
        uint32_t tactive : 1; // Receiver flow delay trigger continuous status
        uint32_t fdelt : 1; // Receiver flow delay trigger continuous status
        uint32_t ttrig : 1; // Transmitter FIFO Trigger continuous status
        uint32_t tnful : 1; // Transmitter FIFO Nearly Full continuous status
        uint32_t _res15 : 17;
    };

    RegValUnion<control_reg_t, uint32_t> control_reg0; // UART Control Register, +0x00
    RegValUnion<mode_reg_t, uint32_t> mode_reg0; // UART Mode Register, +0x04
    RegValUnion<int_reg_t, uint32_t> intrpt_en_reg0; // Interrupt Enable Register, +0x08
    RegValUnion<int_reg_t, uint32_t> intrpt_dis_reg0; // Interrupt Disable Register, +0x0C
    RegValUnion<int_reg_t, uint32_t> intrpt_mask_reg0; // Interrupt Mask Register, +0x10
    RegValUnion<int_reg_t, uint32_t> chnl_int_sts_reg0; // Channel Interrupt Status Register, +0x14
    uint32_t baud_rate_gen_reg0; // Baud Rate Generator Register., +0x18
    uint32_t rcvr_timeout_reg0; // Receiver Timeout Register, +0x1C
    uint32_t rcvr_fifo_trigger_level0; // Receiver FIFO Trigger Level Register, +0x20
    uint32_t modem_ctrl_reg0; // Modem Control Register, +0x24
    uint32_t modem_sts_reg0; // Modem Status Register, +0x28
    RegValUnion<chnl_sts_reg_t, uint32_t> chnl_sts_reg0; // Channel Status Register, +0x2C
    uint32_t tx_rx_fifo0; // Transmit and Receive FIFO, +0x30
    uint32_t baud_rate_divider_reg; // Baud Rate Divider Register, +0x34
    uint32_t flow_delay_reg0; // Flow Control Delay Register, +0x38
    uint32_t tx_fifo_trigger_level_reg0; // Transmitter FIFO Trigger Level Register, +0x44
};

uart::uart(id_t id)
    : module(id)
{
    os::dev::char_device::set_name(module::get_name());
    regs = reinterpret_cast<volatile struct uart_regs*>(get_base_addr());

#ifndef FSBL
    auto irq_id = get_base_irq();
    irq = os::cpu::cpu.interrupts.get(irq_id);
    irq->handler = [this](){ irq_handler(); };
#endif
}

void uart::hw_reset()
{
    constexpr uint32_t UART0_CPU1X_RST = 1<<0;
    constexpr uint32_t UART0_REF_RST = 1<<2;

    auto& slcr = soc.slcr;
    const uint32_t rst = (UART0_CPU1X_RST | UART0_REF_RST) << instance();
    slcr.pulse_rst_ctrl(slcr.regs->uart_rst_ctrl, rst);
}

void uart::sw_reset()
{
    // Software reset RX and TX
    auto cr = regs->control_reg0;
    cr.r.rxres = cr.r.txres = 1;
    regs->control_reg0 = cr;
    do
        cr = regs->control_reg0;
    while (cr.r.rxres || cr.r.txres);
}

void uart::set_enable(bool b)
{
    // Enable / disable RX and TX
    auto cr = regs->control_reg0;
    if (b) {
        cr.r.rxdis = cr.r.txdis = 0;
        cr.r.rxen = cr.r.txen = 1;
    } else
        cr.r.rxdis = cr.r.txdis = 1;
    regs->control_reg0 = cr;

#ifndef FSBL
    // Enable / disable interrupts
    if (b) {
        // Clear all interrupts
        regs->chnl_int_sts_reg0 = ~0;
        irq->enable();
    } else {
        irq->disable();
    }
#endif
}

uart::baud_clk_divs uart::find_baud_clk_divs(int baud)
{
    baud_clk_divs bcds, best_bcds {};
    double best_actual = 0;

    for (bcds.clks = 0; bcds.clks <= 1; bcds.clks ++) {
        double sel_clk = UART_FREQ;
        if (bcds.clks)
            sel_clk /= 8;
        for (bcds.bdiv = 4; bcds.bdiv < 256; bcds.bdiv ++) {
            bcds.cd = sel_clk / (bcds.bdiv + 1) / baud + 0.5;
            if (bcds.cd > 0 && bcds.cd <= 65535) {
                double actual = sel_clk / (bcds.cd * (bcds.bdiv + 1));
                if (abs(best_actual - baud) > abs(actual - baud)) {
                    best_bcds = bcds;
                    best_actual = actual;
                }
            }
        }
    }

    return best_bcds;
}

void uart::set_config(const config_t& config)
{
    set_enable(false);

    // Set defaults
    auto mode = regs->mode_reg0;
    mode.r.chrl = mode.r.chrl_8b;
    mode.r.par = mode.r.par_none;
    mode.r.nbstop = mode.r.nbstop_1b;
    mode.r.chmode = mode.r.chmode_normal;

    // Set baud clock dividers
    auto bcds = find_baud_clk_divs(config.baud);
    mode.r.clks = bcds.clks;                    // sel_clk
    regs->mode_reg0 = mode;
    regs->baud_rate_gen_reg0 = bcds.cd;         // baud sample
    regs->baud_rate_divider_reg = bcds.bdiv;    // baud_rate

    // Set FIFO levels
    regs->rcvr_fifo_trigger_level0 = 1;         // int. on first byte
}

void uart::start()
{
    sw_reset();

    set_enable(true);
}

bool uart::is_rx_empty()
{
    auto sts = regs->chnl_sts_reg0;
    return sts.r.rempty;
}

bool uart::is_tx_full()
{
    auto sts = regs->chnl_sts_reg0;
    return sts.r.tful;
}

char uart::rx()
{
    auto b = regs->tx_rx_fifo0;
    return char(b);
}

void uart::tx(char b)
{
    regs->tx_rx_fifo0 = b;
}

bool uart::has_event(event_t ev)
{
    switch (ev) {
    case event_t::on_rx_ready:
        return true;
    default:
        return false;
    }
}

void uart::set_event_handler(event_handler_t fn)
{
    event_fn = fn;
}

void uart::set_event_enable(event_t ev, bool en)
{
    auto mask = regs->intrpt_mask_reg0;

    switch (ev) {
    case event_t::on_rx_ready:
        mask.r.rtrig = 1;
        break;
    default:
        return;
    }

    regs->intrpt_dis_reg0 = ~mask;
    regs->intrpt_en_reg0 = mask;
}

void uart::irq_handler()
{
    auto sts = regs->chnl_int_sts_reg0;
    if (sts.r.rtrig) {
        regs->chnl_int_sts_reg0 = decltype(sts){.rtrig = 1};
        event_fn(event_t::on_rx_ready);
    }
}

} // namespace soc
} // namespace os
