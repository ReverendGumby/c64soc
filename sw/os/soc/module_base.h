#include <stdint.h>

namespace os {
namespace soc {

// From Zync 7000 TRM (UG585), Section B.3 Module Summary

static constexpr struct {
    uint32_t axi_hp0 = 0xF8008000; // AXI_HP Interface (AFI), Interface 0
    uint32_t axi_hp1 = 0xF8009000; // AXI_HP Interface (AFI), Interface 1
    uint32_t axi_hp2 = 0xF800A000; // AXI_HP Interface (AFI), Interface 2
    uint32_t axi_hp3 = 0xF800B000; // AXI_HP Interface (AFI), Interface 3
    uint32_t can0 = 0xE0008000; // Controller Area Network
    uint32_t can1 = 0xE0009000; // Controller Area Network
    uint32_t ddrc = 0xF8006000; // DDR memory controller
    uint32_t debug_cpu_cti0 = 0xF8898000; // Cross Trigger Interface, CPU 0
    uint32_t debug_cpu_cti1 = 0xF8899000; // Cross Trigger Interface, CPU 1
    uint32_t debug_cpu_pmu0 =  0xF8891000; // Cortex A9 Performance Monitoring Unit, CPU 0
    uint32_t debug_cpu_pmu1 =  0xF8893000; // Cortex A9 Performance Monitoring Unit, CPU 1
    uint32_t debug_cpu_ptm0 = 0xF889C000; // CoreSight PTM-A9, CPU 0
    uint32_t debug_cpu_ptm1 = 0xF889D000; // CoreSight PTM-A9, CPU 1
    uint32_t debug_cti_etb_tpiu = 0xF8802000; // Cross Trigger Interface, ETB and TPIU
    uint32_t debug_cti_ftm = 0xF8809000; // Cross Trigger Interface, FTM
    uint32_t debug_dap_rom = 0xF8800000; // Debug Access Port ROM Table
    uint32_t debug_etb = 0xF8801000; // Embedded Trace Buffer
    uint32_t debug_ftm = 0xF880B000; // Fabric Trace Macrocell
    uint32_t debug_funnel = 0xF8804000; // CoreSight Trace Funnel
    uint32_t debug_itm = 0xF8805000; // Instrumentation Trace Macrocell
    uint32_t debug_tpiu = 0xF8803000; // Trace Port Interface Unit
    uint32_t devcfg = 0xF8007000; // Device configuraion Interface
    uint32_t dmac0_ns = 0xF8004000; // Direct Memory Access Controller, PL330, Non-Secure Mode
    uint32_t dmac0_s = 0xF8003000; // Direct Memory Access Controller, PL330, Secure Mode
    uint32_t gem0 = 0xE000B000; // GEM = Gigabit Ethernet Controller
    uint32_t gem1 = 0xE000C000; // Gigabit Ethernet Controller
    uint32_t gpio = 0xE000A000; // General Purpose Input / Output
    uint32_t gpv_qos301_cpu = 0xF8946000; // AMBA Network Interconnect Advanced Quality of Service (QoS-301), CPU-to-DDR
    uint32_t gpv_qos301_dmac = 0xF8947000; // AMBA Network Interconnect Advanced Quality of Service (QoS-301), DMAC
    uint32_t gpv_qos301_iou = 0xF8948000; // AMBA Network Interconnect Advanced Quality of Service (QoS-301), IOU
    uint32_t gpv_trustzone = 0xF8900000; // AMBA NIC301 TrustZone.
    uint32_t i2c0 = 0xE0004000; // Inter Integrated Circuit (I2C)
    uint32_t i2c1 = 0xE0005000; // Inter Integrated Circuit (I2C)
    uint32_t l2cache = 0xF8F02000; // L2 cache PL310
    uint32_t mpcore = 0xF8F00000; // Mpcore - SCU, Interrupt controller, Counters and Timers - PERIPHBASE[31:13]
    uint32_t ocm = 0xF800C000; // On-Chip Memory Registers
    uint32_t qspi = 0xE000D000; // LQSPI module Registers
    uint32_t sd0 = 0xE0100000; // SD2.0 / SDIO2.0 / MMC3.31 AHB Host Controller Registers
    uint32_t sd1 = 0xE0101000; // SD2.0 / SDIO2.0 / MMC3.31 AHB Host Controller Registers
    uint32_t slcr = 0xF8000000; // System Level Control Registers
    uint32_t smcc = 0xE000E000; // Shared memory controller
    uint32_t spi0 = 0xE0006000; // Serial Peripheral Interface
    uint32_t spi1 = 0xE0007000; // Serial Peripheral Interface
    uint32_t swdt = 0xF8005000; // System Watchdog Timer Registers
    uint32_t ttc0 = 0xF8001000; // Triple Timer Counter
    uint32_t ttc1 = 0xF8002000; // Triple Timer Counter
    uint32_t uart0 = 0xE0000000; // Universal Asynchronous Receiver Transmitter
    uint32_t uart1 = 0xE0001000; // Universal Asynchronous Receiver Transmitter
    uint32_t usb0 = 0xE0002000; // USB controller registers
    uint32_t usb1 = 0xE0003000; // USB controller registers
} module_base;

} // namespace soc
} // namespace os
