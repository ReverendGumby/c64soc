#include "soc.h"

#include "i2c.h"
#include "uart.h"
#include "devcfg.h"

#include "os/cpu/cpu.h"
#include <stdexcept>

namespace os {
namespace soc {

class soc soc;

void soc::init()
{
    os::cpu::cpu.init();
}

void soc::deinit()
{
    deinit_modules();

    os::cpu::cpu.deinit();
}

module* soc::get_module(module::id_t id)
{
    auto i = modules.find(id);
    if (i == modules.end()) {
        module* m;
        switch (id) {
        case module::id_t::i2c0:
        case module::id_t::i2c1:
            m = new i2c(id);
            break;
        case module::id_t::uart0:
        case module::id_t::uart1:
            m = new uart(id);
            break;
        case module::id_t::devcfg:
            m = new devcfg(id);
            break;
        default:
            throw std::runtime_error("unimplemented module");
        }
        auto p = modules.emplace(std::make_pair(id, m));
        i = p.first;
    }
    return i->second;
}

void soc::deinit_modules()
{
    for (auto v : modules) {
        v.second->deinit();
    }
}

} // namespace soc
} // namespace os
