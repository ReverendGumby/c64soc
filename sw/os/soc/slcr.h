#pragma once

#include <stdint.h>
#include "module.h"

namespace os {
namespace soc {

class slcr
{
public:
    struct slcr_regs
    {
        uint32_t scl;              // Secure Configuration Lock, +0x000
        uint32_t slcr_lock;        // SLCR Write Protection Lock, +0x004
        uint32_t slcr_unlock;      // SLCR Write Protection Unlock, +0x008
        uint32_t slcr_locksta;     // SLCR Write Protection Status, +0x00C
        uint8_t _res010[0x200 - 0x010];

        uint32_t pss_rst_ctrl;     // PS Software Reset Control, +0x200
        uint32_t ddr_rst_ctrl;     // DDR Software Reset Control, +0x204
        uint32_t topsw_rst_ctrl;   // Central Interconnect Reset Control, +0x208
        uint32_t dmac_rst_ctrl;    // DMAC Software Reset Control, +0x20C
        uint32_t usb_rst_ctrl;     // USB Software Reset Control, +0x210
        uint32_t gem_rst_ctrl;     // Gigabit Ethernet SW Reset Control, +0x214
        uint32_t sdio_rst_ctrl;    // SDIO Software Reset Control, +0x218
        uint32_t spi_rst_ctrl;     // SPI Software Reset Control, +0x21C
        uint32_t can_rst_ctrl;     // CAN Software Reset Control, +0x220
        uint32_t i2c_rst_ctrl;     // I2C Software Reset Control, +0x224
        uint32_t uart_rst_ctrl;    // UART Software Reset Control, +0x228
        uint32_t gpio_rst_ctrl;    // GPIO Software Reset Control, +0x22C
        uint32_t lqspi_rst_ctrl;   // Quad SPI Software Reset Control, +0x230
        uint32_t smc_rst_ctrl;     // SMC Software Reset Control, +0x234
        uint32_t ocm_rst_ctrl;     // OCM Software Reset Control, +0x238
        uint32_t fpga_rst_ctrl;    // FPGA Software Reset Control, +0x240
        uint32_t a9_cpu_rst_ctrl;  // CPU Reset and Clock control, +0x244
    };
    volatile struct slcr_regs *regs;

    slcr();

    void lock();
    void unlock();

    void pulse_rst_ctrl(volatile uint32_t& rst_ctrl, uint32_t rst_val);
};

} // namespace soc
} // namespace os
