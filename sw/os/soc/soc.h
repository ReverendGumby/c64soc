#pragma once

#include "module.h"
#include "slcr.h"
#include <unordered_map>

namespace os {
namespace soc {

class soc
{
public:
    void init();
    void deinit();

    class slcr slcr;

    module* get_module(module::id_t);

private:
    void deinit_modules();

    std::unordered_map<module::id_t, module*> modules;
};

extern class soc soc;

} // namespace soc
} // namespace os
