#include <unistd.h>

#ifdef _REENTRANT

#include <os/thread/ticks.h>
#include <os/thread/thread.h>
#include <os/thread/scheduler.h>

int usleep(useconds_t useconds)
{
    using namespace os::thread;
    constexpr ticks_t us = 1e6;
    static_assert(hz % us == 0, "non-integer number of ticks per microsecond");
    ticks_t ticks = useconds * (hz / us);

    auto ct = scheduler::get_current_thread();
    ct->sleep(ticks);
    return 0;
}

#else // _REENTRANT

#include <util/timer.h>

int usleep(useconds_t useconds)
{
    timer::sleep(useconds * timer::us);
    return 0;
}

#endif

unsigned int sleep(unsigned int seconds)
{
    while (seconds--)
        usleep(1e6);
    return 0;
}
