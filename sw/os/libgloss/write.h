#pragma once

// Set outbyte to a function to output characters from _write().
extern void (*outbyte)(char b);
