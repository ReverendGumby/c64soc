// Copied from newlib-3.0.0/libgloss/arm/_kill.c

#include <_ansi.h>

_BEGIN_STD_C

int _kill (int, int);

_END_STD_C

int
_kill (int pid, int sig)
{
  (void) pid; (void) sig;
  return -1;
}
