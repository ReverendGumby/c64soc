#include <unistd.h>

static void default_outbyte(char b)
{
}

void (*outbyte)(char b) = default_outbyte;

extern "C"
__attribute__((weak))
int _write(int fd, const void* buf, size_t nbyte)
{
    for (size_t i = 0; i < nbyte; i++) {
        const char c = ((const char*)buf)[i];
        if (c == '\n')
            outbyte('\r');
        outbyte(c);
    }
    return nbyte;
}
