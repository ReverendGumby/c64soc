#include <unistd.h>

long sysconf(int name)
{
    switch (name) {
    case _SC_NPROCESSORS_ONLN:
        return 1; // uniprocessor for now...
    default:
        return -1;
    }
}
