// Copied from newlib-3.0.0/libgloss/arm/syscalls.c

#include <_ansi.h>

_BEGIN_STD_C

int _getpid (int);

_END_STD_C

int
_getpid (int n __attribute__ ((unused)))
{
  return 1;
}
