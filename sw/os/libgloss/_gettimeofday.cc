#include <sys/time.h>

#include <util/timer.h>

extern "C"
int _gettimeofday (struct timeval *ptimeval, void *ptimezone)
{
    if (ptimeval) {
        auto now = timer::now();

        // Some day, we'll know seconds since the epoch.
        ptimeval->tv_sec = now / timer::sec;
        ptimeval->tv_usec = (now % timer::sec) / timer::us;
    }
    return 0;
}

