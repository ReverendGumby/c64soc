#pragma once

#include <unistd.h>

// Set readbytes to a function to input characters to _read().
extern size_t (*readbytes)(char* buf, size_t nbyte);
