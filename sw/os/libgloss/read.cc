#include "read.h"

static size_t default_readbytes(char* buf, size_t nbyte)
{
    // Nada.
    return 0;
}

size_t (*readbytes)(char* buf, size_t nbyte) = default_readbytes;

extern "C"
__attribute__((weak))
int _read(int fd, void* buf, size_t nbyte)
{
    return readbytes((char*)buf, nbyte);
}
