#pragma once

#include <util/buffer.h>
#include <stddef.h>
#include <stdint.h>

namespace os {
namespace mem {

class mempool_buffer;

class mempool
{
public:
    mempool(void* mem_start, size_t mem_size, size_t count);
    virtual ~mempool();

    unsigned get_mem_size() { return _mem_size; }

    buffer* get_buffer();

private:
    friend mempool_buffer;

    size_t acquire();
    void release(size_t idx);

    uint8_t* _mem_start;
    size_t _mem_size;
    size_t _count;
    bool* _used;
};

} // namespace mem
} // namespace os
