#include "mempool_buffer.h"

#include "mempool.h"

namespace os {
namespace mem {

mempool_buffer::mempool_buffer(mempool& mp, size_t idx)
    : _pool(mp), _idx(idx)
{
    len = _pool._mem_size;
    ptr = _pool._mem_start + _pool._mem_size * _idx;
}

mempool_buffer::~mempool_buffer()
{
    _pool.release(_idx);
}

} // namespace mem
} // namespace os
