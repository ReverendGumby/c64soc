#include "mempool.h"

#include "mempool_buffer.h"
#include <stdexcept>

namespace os {
namespace mem {

mempool::mempool(void* mem_start, size_t mem_size, size_t count)
    : _mem_start(reinterpret_cast<uint8_t*>(mem_start)),
      _mem_size(mem_size), _count(count)
{
    _used = new bool[_count];
    for (size_t i = 0; i < _count; i++)
        _used[i] = false;
}

mempool::~mempool()
{
    delete[] _used;
}

size_t mempool::acquire()
{
    size_t i;
    for (i = 0; i < _count; i++) {
        if (!_used[i]) {
            _used[i] = true;
            break;
        }
    }

    if (i == _count)
        throw std::overflow_error("mempool::acquire(): pool is full");
    return i;
}

void mempool::release(size_t idx)
{
    _used[idx] = false;
}

buffer* mempool::get_buffer()
{
    auto idx = acquire();
    return new mempool_buffer{*this, idx};
}

} // namespace mem
} // namespace os
