#pragma once

#include <util/buffer.h>

namespace os {
namespace mem {

class mempool;

class mempool_buffer : public buffer
{
public:
    mempool_buffer(mempool&, size_t);
    virtual ~mempool_buffer();

private:
    mempool& _pool;
    size_t _idx;
};

} // namespace mem
} // namespace os
