#include "condvar.h"
#include "thread.h"
#include "mutex.h"
#include "scheduler.h"

#include <algorithm>

namespace os {
namespace thread {

condvar::condvar()
{
}

condvar::~condvar()
{
}

void condvar::add_waiter(thread* t)
{
    auto it = std::find(wakeq.begin(), wakeq.end(), t);
    if (it == wakeq.end())
        wakeq.push_back(t);
}

void condvar::remove_waiter(thread* t)
{
    auto it = std::find(wakeq.begin(), wakeq.end(), t);
    if (it != wakeq.end())
        wakeq.erase(it);
}

void condvar::signal()
{
    splck.lock_irq();
    if (wakeq.size()) {
        auto t = wakeq.front();
        wakeq.erase(wakeq.begin());
        t->run();
        scheduler.schedule();
    }
    splck.unlock_irq();
}

void condvar::broadcast()
{
    splck.lock_irq();
    if (wakeq.size()) {
        for (auto t : wakeq) {
            t->run();
        }
        wakeq.clear();
        scheduler.schedule();
    }
    splck.unlock_irq();
}

void condvar::wait(thread* ct, mutex* mtx)
{
    splck.lock_irq();
    mtx->unlock();
    add_waiter(ct);
    ct->wait();
    scheduler.schedule();
    splck.unlock_irq();
    // Scheduling happens now

    splck.lock_irq();
    remove_waiter(ct);
    splck.unlock_irq();
    mtx->lock();
}

} // namespace thread
} // namespace os
