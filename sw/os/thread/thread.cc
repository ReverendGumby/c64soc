#include "thread.h"
#include "ticks.h"
#include "scheduler.h"

#include <os/cpu/interrupts.h>

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <array>
#include <algorithm>

extern uint32_t _user_stack_end;

namespace os {
namespace thread {

// 0 is reserved for non-existant thread
static constexpr int initial_thread_id = 1;
static constexpr int max_num_threads = 64;

static std::array<thread*, max_num_threads> thread_list;

int thread::locals_used = 0;
void (*thread::local_destructor_cbs[num_local])(void*);
spinlock thread::local_splck;

static int get_thread_list_idx(int id)
{
    return id - initial_thread_id;
}

static inline thread*& thread_list_at(int id)
{
    return thread_list[get_thread_list_idx(id)];
}

thread::thread()
    : start(nullptr), start_arg(nullptr)
{
    // This is the first thread.
    allocate_id();
    stack_bottom = reinterpret_cast<stack_el_t*>(&_user_stack_end);
}

thread::thread(void* (*start)(void*), void* arg)
    : start(start), start_arg(arg)
{
    allocate_id();
    allocate_stack();
}

thread::~thread()
{
    free(stack_bottom);
    thread_list_at(id) = nullptr;
}

void thread::allocate_id()
{
    static int next_id = initial_thread_id;

    do {
        id = next_id;
        next_id ++;
        if (get_thread_list_idx(next_id) >= max_num_threads)
            next_id = initial_thread_id;
    } while (thread_list_at(id));

    thread_list_at(id) = this;
}

thread* thread::find(int id)
{
    auto idx = get_thread_list_idx(id);
    if (idx < 0 || idx >= max_num_threads)
        return nullptr;
    return thread_list[idx];
}

void thread::entry(thread* self)
{
    self->start(self->start_arg);
    self->exit();
}

void thread::run()
{
    splck.lock_irq();
    set_state(state_t::run);
    splck.unlock_irq();
}

void thread::wait()
{
    splck.lock_irq();
    set_state(state_t::wait);
    splck.unlock_irq();
}

void thread::exit()
{
    mtx_exit.lock();
    splck.lock_irq();
    exiting = true;
    destroy_locals();
    cv_exit.broadcast();
    mtx_exit.unlock();

    set_state(state_t::exit);
    scheduler.schedule();
    splck.unlock_irq();
    // and we should never get here.
    assert(os::cpu::interrupts::are_enabled());
    assert(false);
}

void thread::allocate_stack()
{
    // TODO: Stop arbitrarily growing stack_size to avoid stack overflows! Find
    // and enable some sort of proactive stack bounds checking.
    static constexpr size_t stack_size = 0x20000;
    static constexpr size_t stack_cnt = stack_size / sizeof(stack_el_t);

    stack_bottom = (stack_el_t*)aligned_alloc(8, stack_size);

    // Fill stack top with zeros to hint backtracing to stop
    auto stack_ptr = stack_bottom + stack_cnt;
    for (int i = 0; i < 8; i++)
        *(--stack_ptr) = 0;

    // Initialize context so that switch_context() will end up returning to
    // thread::entry(this).
    memset(&ctx, 0, sizeof(ctx));
    ctx.r[0] = reinterpret_cast<uint32_t>(this);
    ctx.lr = reinterpret_cast<uint32_t>(nullptr);
    ctx.sp = reinterpret_cast<uint32_t>(stack_ptr);
    ctx.pc = reinterpret_cast<uint32_t>(entry);
    auto cpsr = cpu::cpsr::get();
    cpsr.t = ctx.pc & 1;                        // Thumb bit
    ctx.cpsr = cpu::cpsr::to_val(cpsr);
}

void thread::set_state(state_t s)
{
    assert (state != state_t::exit);

    bool was_runnable = state == state_t::run;
    state = s;
    bool is_runnable = state == state_t::run;
    if (was_runnable != is_runnable)
        scheduler.update(this, is_runnable);
}

void thread::switch_context(thread* nt)
{
    assert (nt->state != state_t::exit);

    auto* irq_ctx = cpu::interrupts::irq_ctx;

    for (int i = 0; i < 13; i++)
        ctx.r[i] = irq_ctx->r[i];
    for (int i = 0; i < 8; i++)
        ctx.d[i] = irq_ctx->d[i];
    ctx.pc = irq_ctx->lr - 4;
    __asm__ ("vstm %1, {d8-d15}\n"
             "vstm %1, {d16-d31}\n"
             "vmrs %0, fpscr"
             : "=r" (ctx.fpscr)
             : "r" (&ctx.d[8]));

    __asm__ ("cps %[mode_sys]\n"
             "mov %[old_sp], sp\n"
             "mov %[old_lr], lr\n"
             "mov sp, %[new_sp]\n"
             "mov lr, %[new_lr]\n"
             "cps %[mode_irq]\n"
             "mrs %[old_sr], spsr\n"
             "msr spsr_fsxc, %[new_sr]\n"
             : [old_sp]"=&l" (ctx.sp),         // use 'l' constraint to avoid lr
               [old_lr]"=&l" (ctx.lr),
               [old_sr]"=&l" (ctx.cpsr)
             : [new_sp]"l" (nt->ctx.sp),
               [new_lr]"l" (nt->ctx.lr),
               [new_sr]"l" (nt->ctx.cpsr),
               [mode_sys]"i" (cpu::cpsr::mode_sys),
               [mode_irq]"i" (cpu::cpsr::mode_irq));

    __asm__ ("vldm %1, {d8-d15}\n"
             "vldm %1, {d16-d31}\n"
             "vmsr fpscr, %0"
             : : "r" (nt->ctx.fpscr), "r" (&nt->ctx.d[8]));
    irq_ctx->lr = nt->ctx.pc + 4;
    for (int i = 0; i < 8; i++)
        irq_ctx->d[i] = nt->ctx.d[i];
    for (int i = 0; i < 13; i++)
        irq_ctx->r[i] = nt->ctx.r[i];

    assert (ctx.sp >= uint32_t(stack_bottom));
    assert (nt->ctx.sp >= uint32_t(nt->stack_bottom));
}

void thread::sleep(ticks_t ticks)
{
    auto start = now();
    auto when = start + ticks;
    do {
        scheduler.set_alarm(this, when);
        wait();
        scheduler.schedule();
    } while (sticks_t(now() - start) < sticks_t(ticks));
}

void thread::wait_for_exit(thread* et)
{
    auto& cv = et->cv_exit;
    auto& mtx = et->mtx_exit;
    mtx.lock();
    while (!et->exiting) {
        cv.wait(this, &mtx);
    }
    mtx.unlock();
}

int thread::add_local(void (*destructor_cb)(void*))
{
    int id = 0;
    local_splck.lock_irq();
    id = locals_used ++;
    assert(id < num_local);
    local_destructor_cbs[id] = destructor_cb;
    local_splck.unlock_irq();
    return id;
}

void thread::set_local(int id, void* data)
{
    assert(id < num_local);
    splck.lock_irq();
    locals[id] = data;
    splck.unlock_irq();
}

void* thread::get_local(int id)
{
    void* data;
    splck.lock_irq();
    data = locals[id];
    splck.unlock_irq();
    return data;
}

void thread::destroy_locals()
{
    // splck is locked by caller.
    for (int id = 0; id < num_local; id++) {
        auto* destructor = local_destructor_cbs[id];
        auto* value = locals[id];
        if (value && destructor)
            destructor(value);
    }
}

} // namespace thread
} // namespace os
