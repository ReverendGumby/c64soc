#include <os/cpu/cpu.h>
#include <os/cpu/cp15.h>
#include "thread.h"
#include <algorithm>
#include <assert.h>
#include <util/timer.h>
#include <util/debug.h>

#include "scheduler.h"

namespace os {
namespace thread {

static constexpr cpu::interrupt::id_t sched_irq = cpu::arm_gic::sgi0;

class scheduler scheduler;

scheduler::scheduler()
    : stats{}
{
    // We are called by the first thread.
    auto first_thread = new thread {};
    set_current_thread(first_thread);
    first_thread->run();
}

void scheduler::start()
{
    // SW uses an SGI to request scheduling.
    irq = cpu::cpu.interrupts.get(sched_irq);
    irq->handler = [this](){ schedule(); };
    irq->enable();

    // Create the idle thread.
    auto idle_thread = new thread(call_idle_task, this);
    idle_thread->set_prio(-1000);
    idle_thread->run();

    alarm_timer.init();

    started = true;
}

thread* scheduler::get_current_thread()
{
    auto tpid = cpu::cp15::mo::tpidrurw::get();
    return static_cast<thread *>(tpid);
}

void scheduler::set_current_thread(thread* t)
{
    cpu::cp15::mo::tpidrurw::set(t);
}

scheduler::stats_t scheduler::get_stats() const
{
    return stats;
}

void scheduler::set_alarm(thread* t, ticks_t when)
{
    splck.lock_irq();
    clear_alarm(t);
    
    // alarmq is fully sorted by 'when', accounting for counter wrap.
    alarm_t a { t, when };
    if (alarmq.size() == 0) {
        alarmq.push_back(a);
    } else {
        auto oldest = alarmq.front().when;
        auto comp = [&](const alarm_t& a, const alarm_t& b) {
            return sticks_remain(a.when, oldest) <
                sticks_remain(b.when, oldest);
        };
        auto it = std::upper_bound(alarmq.begin(), alarmq.end(), a, comp);
        alarmq.insert(it, a);
    }
    splck.unlock_irq();
}

void scheduler::clear_alarm(thread* t)
{
    auto old_pred = [t](const alarm_t& a) { return a.t == t; };
    auto old_it = std::find_if(alarmq.begin(), alarmq.end(), old_pred);
    if (old_it != alarmq.end())
        alarmq.erase(old_it);
}

void scheduler::update(thread* t, bool runnable)
{
    if (!is_scheduling)
        splck.lock_irq();
    auto it = std::find(runq.begin(), runq.end(), t);
    bool on_runq = it != runq.end();
    if (runnable != on_runq) {
        if (on_runq)
            runq.erase(it);
        else
            runq.push_front(t);
    }
    if (!is_scheduling)
        splck.unlock_irq();
}

void scheduler::schedule()
{
    if (!started)
        return;

    if (!cpu::interrupts::is_in_irq()) {
        // Trigger the SGI interrupt
        auto save_ints = cpu::interrupts::save();
        irq->set_pending();
        while (!irq->is_pending())
            ;
        cpu::interrupts::restore(save_ints);
        return;
    }
    splck.lock_irq();
    is_scheduling = true;
    check_alarms();

    auto nti = runq.end();
    thread* nt = nullptr; // next thread
    auto ntp = 0;
    for (auto it = runq.begin(); it != runq.end(); it++) {
        auto t = *it;
        auto tp = t->get_prio();
        if (!nt || tp > ntp) {
            nti = it;
            nt = t;
            ntp = tp;
        }
    }

    assert(nt);
    runq.erase(nti);
    runq.push_back(nt);
    switch_to(nt);

    is_scheduling = false;
    splck.unlock_irq();
}

void scheduler::switch_to(thread* nt)
{
    auto ct = get_current_thread();
    if (ct != nt) {
        stats.context_switches ++;
        set_current_thread(nt);
        ct->switch_context(nt);
    }
}

void scheduler::check_alarms()
{
    while (alarmq.size()) {
        auto tnow = now();
        const auto& a = alarmq.front();
        auto remain = sticks_remain(a.when, tnow);
        if (remain > 0)
            break;
        a.t->run();
        alarmq.erase(alarmq.begin());
    }
}

bool scheduler::next_alarm(ticks_t& when)
{
    bool ret = alarmq.size();
    if (ret)
        when = alarmq.front().when;
    return ret;
}

void* scheduler::call_idle_task(void* arg)
{
    scheduler* self = static_cast<scheduler*>(arg);
    self->idle_task();
    return nullptr;
}

void scheduler::idle_task()
{
    constexpr sticks_t min_timer_sleep = 100e-6 * hz;

    for (;;) {
        stats.idle_loops ++;
        splck.lock_irq();
        ticks_t when;
        if (next_alarm(when)) {
            auto ticks = sticks_remain(when, now());
            if (ticks >= min_timer_sleep) {
                alarm_timer.set(when);
                idle_wait();
                if (!alarm_timer.is_done())
                    alarm_timer.reset();
            } else {
                timer::sleep(ticks);
            }
        } else
            idle_wait();
        schedule();
        splck.unlock_irq();
    }
}

void scheduler::idle_wait()
{
    os::cpu::cpu.wfi();
    stats.wfi_entries ++;
}

} // namespace thread
} // namespace os
