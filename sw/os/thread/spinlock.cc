#include "spinlock.h"

#include <assert.h>

namespace os {
namespace thread {

void spinlock::lock_debug()
{
    cnt++;
    assert(cnt == 1);
}

void spinlock::unlock_debug()
{
    cnt--;
    assert(cnt == 0);
}

} // namespace thread
} // namespace os
