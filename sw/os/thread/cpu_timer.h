#pragma once

#include <os/cpu/cpu.h>
#include "ticks.h"

namespace os {
namespace thread {

// Use the CPU global timer and its interrupt for power-efficient sleeps. After
// setting the timer comparator, the caller uses WFI to enter standby. When the
// specified time has been reached (comparator match event), the timer interrupt
// fires and exits standby. The caller can then confirm that the timer has gone
// off, or else loop until it does.

class cpu_timer
{
public:
    void init();

    // Set the timer to go off at an absolute time, and enable its interrupt.
    // The interrupt will be disabled when the timer goes off.
    void set(ticks_t when);

    // Stop the timer and disable its interrupt.
    void reset();

    // Confirm that the timer has gone off.
    bool is_done();

private:
    void irq_handler();

    os::cpu::interrupt* event;
};

} // namespace thread
} // namespace os
