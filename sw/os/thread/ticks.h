#pragma once

#include <os/cpu/cpu.h>

namespace os {
namespace thread {

using ticks_t = decltype(os::cpu::cpu.timer)::count_t;
using sticks_t = decltype(os::cpu::cpu.timer)::delta_t;

static constexpr ticks_t hz = os::cpu::cpu.timer.hz();

inline static ticks_t now()
{
    return os::cpu::cpu.timer.get_counter();
}

inline static sticks_t sticks_remain(ticks_t when, ticks_t now)
{
    return sticks_t(when - now);
}

} // namespace thread
} // namespace os
