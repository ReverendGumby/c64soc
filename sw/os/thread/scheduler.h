#pragma once

#include "thread.h"
#include "ticks.h"
#include "cpu_timer.h"
#include "spinlock.h"
#include <os/cpu/interrupts.h>
#include <list>
#include <vector>

namespace os {
namespace thread {

class scheduler
{
public:
    struct stats_t {
        unsigned long context_switches;
        unsigned long idle_loops;
        unsigned long wfi_entries;
    };

    scheduler();

    static thread* get_current_thread();

    stats_t get_stats() const;

    void start();
    void update(thread*, bool runnable);
    void schedule();

    void set_alarm(thread*, ticks_t when);
    void clear_alarm(thread*);

private:
    struct alarm_t {
        thread* t;
        ticks_t when;
    };

    void switch_to(thread* t);
    void check_alarms();
    bool next_alarm(ticks_t& when);

    static void set_current_thread(thread*);
    static void* call_idle_task(void*);
    void idle_task();
    void idle_wait();

    cpu::interrupt* irq;
    spinlock splck;

    bool started = false;
    bool is_scheduling = false;
    stats_t stats;
    std::list<thread*> runq;

    std::vector<alarm_t> alarmq;
    cpu_timer alarm_timer;
};

extern class scheduler scheduler;

} // namespace thread
} // namespace os
