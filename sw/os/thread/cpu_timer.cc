#include <os/cpu/cpu.h>
#include <os/cpu/cp15.h>
#include "thread.h"
#include <algorithm>
#include <assert.h>
#include <util/timer.h>

#include "scheduler.h"

namespace os {
namespace thread {

static constexpr auto irq = os::cpu::arm_gic::gt;

void cpu_timer::init()
{
    os::cpu::cpu.timer.disable_comparator();
    os::cpu::cpu.timer.enable_event_interrupt(true);

    event = os::cpu::cpu.interrupts.get(irq);
    event->handler = [this](){ irq_handler(); };
    event->clear_pending();
    event->enable();
}

void cpu_timer::set(ticks_t when)
{
    os::cpu::cpu.timer.set_comparator(when);
}

void cpu_timer::reset()
{
    os::cpu::cpu.timer.disable_comparator();
}

bool cpu_timer::is_done()
{
    return os::cpu::cpu.timer.is_event_pending();
}

void cpu_timer::irq_handler()
{
    reset();
    // The timer interrupt may have gone pending again.
    event->clear_pending();
}

} // namespace thread
} // namespace os
