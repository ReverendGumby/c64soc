#include "mutex.h"
#include "scheduler.h"
#include "thread.h"

#include <algorithm>

namespace os {
namespace thread {

mutex::mutex(bool recursive)
    : recursive(recursive), locker(nullptr), depth(0)
{
}

mutex::~mutex()
{
}

void mutex::add_waiter(thread* t)
{
    auto it = std::find(wakeq.begin(), wakeq.end(), t);
    if (it == wakeq.end())
        wakeq.push_back(t);
}

void mutex::remove_waiter(thread* t)
{
    auto it = std::find(wakeq.begin(), wakeq.end(), t);
    if (it != wakeq.end())
        wakeq.erase(it);
}

void mutex::lock()
{
    splck.lock_irq();
    if (!int_trylock())
        slow_lock();
    splck.unlock_irq();
}

bool mutex::trylock()
{
    splck.lock_irq();
    bool ret = int_trylock();
    splck.unlock_irq();
    return ret;
}

void mutex::unlock()
{
    splck.lock_irq();
    if (depth)
        depth --;
    else if (!has_waiters())
        locker = nullptr;
    else
        slow_unlock();
    splck.unlock_irq();
}

bool mutex::int_trylock()
{
    auto ct = scheduler::get_current_thread();
    if (!locker)
        locker = ct;
    else if (recursive && locker == ct)
        depth ++;
    else
        return false;
    return true;
}

void mutex::slow_lock()
{
    auto ct = scheduler::get_current_thread();
    add_waiter(ct);
    do {
        ct->wait();
        scheduler.schedule();
        splck.unlock_irq();
        splck.lock_irq();
    } while (locker != ct);
}

void mutex::slow_unlock()
{
    auto wt = wakeq.front();
    wakeq.erase(wakeq.begin());
    locker = wt;
    wt->run();
    scheduler.schedule();
}

} // namespace thread
} // namespace os
