#pragma once

#include "ticks.h"
#include "mutex.h"
#include "condvar.h"
#include "spinlock.h"

#include <stdint.h>
#include <memory>

namespace os {
namespace thread {

class thread
{
public:
    static thread* find(int id);

    thread();
    thread(void* (*start)(void*), void* arg);
    virtual ~thread();

    int get_id() const { return id; }
    int get_prio() const { return prio; }
    void set_prio(int p) { prio = p; }

    void switch_context(thread* nt);

    void run();
    void wait();
    void exit();

    void sleep(ticks_t ticks);
    // Make 'this' thread wait for 'et' thread to exit.
    void wait_for_exit(thread* et);

    static constexpr int num_local = 4;

    static int add_local(void (*destructor_cb)(void*));
    void set_local(int id, void* data);
    void* get_local(int id);

private:
    enum class state_t {
        init,
        run,
        wait,
        exit,
    };

    struct context_t {
        uint32_t fpscr;
        uint64_t d[32];                         // D0 - D31
        uint32_t cpsr;
        uint32_t pc;                            // R15
        uint32_t r[13];                         // R0 - R12
        uint32_t sp;                            // R13
        uint32_t lr;                            // R14
    } __attribute__((packed));

    using stack_el_t = uintptr_t;

    void allocate_id();
    void allocate_stack();

    void set_state(state_t);

    void destroy_locals();

    static void entry(thread*);

    // Fields read by openocd at fixed offsets. Do not rearrange.
    // <<<
    int id;                                     // system-wide unique ID
    state_t state = state_t::init;
    context_t ctx;
    // >>>

    int prio = 0;                               // smaller is lower
    void* (*start)(void*);
    void* start_arg;
    mutex mtx_exit;
    bool exiting = false;
    condvar cv_exit;

    stack_el_t* stack_bottom = nullptr;
    spinlock splck;

    static int locals_used;
    static spinlock local_splck;
    static void (*local_destructor_cbs[num_local])(void*);
    void* locals[num_local] = {};
};

using thread_ptr = std::shared_ptr<thread>;

} // namespace thread
} // namespace os
