#pragma once

#include <vector>
#include "spinlock.h"

namespace os {
namespace thread {

// Condition variable

class thread;

class mutex;

class condvar
{
public:
    condvar();

    virtual ~condvar();

    bool has_waiters() { return wakeq.size(); }

    void signal();
    void broadcast();
    void wait(thread* ct, mutex* mtx);

private:
    void add_waiter(thread*);
    void remove_waiter(thread*);

    spinlock splck;
    std::vector<thread*> wakeq;
};

} // namespace thread
} // namespace os
