#pragma once

#include <os/cpu/interrupts.h>

namespace os {
namespace thread {

class spinlock
{
public:
    void lock_irq()
    {
        // UP-only version
        save_ints = cpu::interrupts::save();
        lock_debug();
    }

    void unlock_irq()
    {
        // UP-only version
        unlock_debug();
        cpu::interrupts::restore(save_ints);
    }

private:
    int cnt = 0;
    decltype(cpu::interrupts::save()) save_ints;

    void lock_debug();
    void unlock_debug();
};

} // namespace thread
} // namespace os
