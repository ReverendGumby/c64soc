#pragma once

#include "spinlock.h"

#include <vector>

namespace os {
namespace thread {

// Mutex

class thread;

class mutex
{
public:
    mutex(bool recursive = false);

    virtual ~mutex();

    void add_waiter(thread*);
    void remove_waiter(thread*);
    bool has_waiters() { return wakeq.size(); }

    void lock();
    bool trylock();
    void unlock();

private:
    bool int_trylock();
    void slow_lock();
    void slow_unlock();

    bool recursive;
    spinlock splck;
    thread* locker; // nullptr if unlocked
    int depth; // recursive lock counter
    std::vector<thread*> wakeq;
};

} // namespace thread
} // namespace os
