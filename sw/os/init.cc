#include "init.h"

#include "libgloss/write.h"
#include "cpu/cpu.h"
#include "cpu/cp14.h"
#include "soc/soc.h"
#include "soc/uart.h"
#include "dev/console.h"

extern "C"
void software_init_hook()
{
    // crt0 provides this hook and calls it before static initialization.
    os::cpu::cpu::pre_main_init();
}

namespace os {

static bool use_serial = false;
static bool serial_initialized = false;

static soc::uart* uart1;
static os::dev::console* console;

static void dbg_outbyte(char b)
{
	using namespace os::cpu;
    while (cp14::csr::dbgdscr::get().txfull)
        ;
    cp14::idtr::dbgdtrtx::set((unsigned char)b);
}

static bool debugger_attached()
{
    // Xilinx debugger changes extdccmode from its reset value of 0.
	using namespace os::cpu;
    return cp14::csr::dbgdscr::get().extdccmode != 0;
}

static void init_dbg()
{
    // Tell libgloss to route output to SWD.
    outbyte = dbg_outbyte;
}

static void uart1_outbyte(char b)
{
    uart1->write(b);
}

static void init_serial()
{
    // Setup UART1
    soc::uart::config_t cfg {
        .baud = 115200,
    };
    uart1 = static_cast<soc::uart*>(soc::soc.get_module(soc::module::id_t::uart1));
    uart1->set_config(cfg);
    uart1->start();

    // Tell libgloss to route output to UART1
    outbyte = uart1_outbyte;

    serial_initialized = true;
}

static void init_console()
{
    // Create console device for CLI
    console = new os::dev::console(uart1);
    console->register_name("console", console);
}

static void deinit_serial()
{
    if (!serial_initialized)
        return;

    // TODO uart1->stop();

    serial_initialized = false;
}

void io_use_serial(bool e)
{
    use_serial = e;
}

void init()
{
    soc::soc.init();

    if (use_serial) {
        init_serial();
#ifndef FSBL
        init_console();
#endif
    }
    else if (debugger_attached())
        init_dbg();
}

void deinit()
{
    deinit_serial();

    soc::soc.deinit();
}

} // namespace os
