#ifndef MAIN_MENU__USB_MENU__H
#define MAIN_MENU__USB_MENU__H

#include "menu_base.h"

namespace main_menu {

class usb_menu : public menu_base
{
public:
    usb_menu(main_manager&);

protected:
    // interface for ui::view
    virtual const char* get_class_name() const { return "usb_menu"; }
};

} // namespace main_menu

#endif
