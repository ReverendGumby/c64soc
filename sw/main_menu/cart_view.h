#ifndef MAIN_MENU__CART_VIEW__H
#define MAIN_MENU__CART_VIEW__H

#include "fat/path.h"
#include "fat/directory_entry.h"
#include "ui/fullscreen_view.h"
#include "ui/list_view.h"
#include "ui/label.h"

namespace main_menu {

class cart_view : public ui::fullscreen_view,
                  private ui::list_view::model
{
public:
    struct model
    {
        struct cart_item
        {
            fat::path fname;
            fat::filesize size;
        };

        using cart_list = std::vector<cart_item>;
        using dir_list = std::vector<fat::path>;

        virtual const fat::path& get_curdir() = 0;
        virtual const dir_list& get_dirs() = 0;
        virtual const cart_list& get_carts() = 0;
        virtual std::string get_last_err() = 0;
        virtual bool is_cart_inserted() = 0;

        virtual void open_dir(const fat::path&) = 0;
        virtual void insert_cart(const fat::path&) = 0;
        virtual void remove_cart() = 0;
        virtual void clear_last_err() = 0;
    };

    cart_view(model&);

    static constexpr int update_flag_carts = 1<<0;
    static constexpr int update_flag_last_err = 1<<1;
    static constexpr int update_inserted = 1<<2;

    void update(int flags);

protected:
    // interface for ui::view
    virtual const char* get_class_name() const { return "cart_view"; }

    // interface for ui::list_view::model
    virtual std::string get_item(int idx);
    virtual void on_selected_changed(int idx);
    virtual void on_selected(int idx);

private:
    void update_carts();
    void update_last_err();

    void draw_directory(const drawing_context*);

    // interface for ui::fullscreen_view
    virtual bool on_key_down(const keypress_t& k, int repeat);
    virtual void on_hide();

    model& model;
    int dir_count;                              // number of directories
    int count;                                  // total number of list items
    bool no_dir;

    ui::label lbl_menu, lbl_dir;
    ui::list_view list;
    ui::label lbl_err, lbl_insert, lbl_eject, lbl_exit;
};

} // namespace main_menu

#endif /* MAIN_MENU__CART_VIEW__H */
