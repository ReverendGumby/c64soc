#ifndef MAIN_MENU__USB_DEVICES__H
#define MAIN_MENU__USB_DEVICES__H

#include "usb/usb_manager.h"
#include "usb_device_view.h"
#include "menu_base.h"

namespace main_menu {

class usb_devices : public menu_base,
                    private usb::usb_manager::listener,
                    private usb_device_view::model
{
public:
    usb_devices(main_manager&, usb::usb_manager&);
    virtual ~usb_devices();

protected:
    virtual void update();
    virtual void on_selected_changed(int idx);

    // interface for ui::view
    virtual const char* get_class_name() const { return "usb_devices"; }

    // interface for usb::usb_manager
    virtual void attached(usb::device*);
    virtual void detached(usb::device*);

    // interface for usb_device_view
    virtual usb_device_view::model::info_t get_info();

    usb::usb_manager& usb_mgr;
    ui::label lbl_view_hdr;
    usb_device_view dev_view;

private:
    usb_device_view::model::info_t info;
};

} // namespace main_menu

#endif
