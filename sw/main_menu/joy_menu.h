#ifndef MAIN_MENU__JOY_MENU__H
#define MAIN_MENU__JOY_MENU__H

//#include "usb/usb_manager.h"
#include "c64/joy.h"
#include "menu_base.h"

namespace main_menu {

class joy_menu : public menu_base,
                 private c64::joy::config::listener
{
public:
    using config = c64::joy::config;

    joy_menu(main_manager&, config&);
    virtual ~joy_menu();

private:
    c64::joy_port_t get_joy_port() const
    {
    	return c64::joy_port_t(c64::JOYA + list.get_selected());
    }
    int joy_port_to_idx(c64::joy_port_t port) const { return port - c64::JOYA; }

    virtual void update();
    void update_mode_list();
    virtual void on_selected_changed(int idx);
    virtual void on_selected(int idx);

    // interface for ui::view
    virtual const char* get_class_name() const { return "joy_menu"; }
    virtual bool on_key_down(const keypress_t&, int);

    // interface for c64::joy::config::listener
    virtual void joy_mode_changed(c64::joy_port_t);
    virtual void joy_mode_desc_changed(c64::joy_mode_t);

    class mode_model : public ui::list_view::model {
    public:
    	mode_model(joy_menu&);

        // interface for ui::list_view
        virtual std::string get_item(int idx);
        virtual void on_selected(int idx);

    private:
        joy_menu& menu;
    } mode_model;
    friend class mode_model;

    config& cfg;
    ui::label lbl_mode;
    ui::list_view mode_list;
};

} // namespace main_menu

#endif
