#ifndef MAIN_MENU__CART_MANAGER__H
#define MAIN_MENU__CART_MANAGER__H

#include <vector>
#include "c64/sys.h"
#include "c64/config/option_set.h"
#include "fat/path.h"
#include "sd/sd_manager.h"
#include "cart_view.h"

namespace main_menu {

class main_manager;

class cart_manager : private sd::sd_manager::listener,
                     private cart_view::model
{
public:
    cart_manager(main_manager&, sd::sd_manager&);

    ui::view* get_view() { return &view; }

private:
    friend cart_view;

    void read_dir();
    c64::cart::PCrt read_cart(const fat::path&);
    c64::config::option_set read_config(const fat::path&);
    void insert_cart(c64::cart::PCrt&);

    // interface for cart_view::model
    using cart_list = cart_view::model::cart_list;
    using dir_list = cart_view::model::dir_list;
    const fat::path& get_curdir() override { return curdir; }
    const dir_list& get_dirs() override { return dirs; }
    virtual const cart_list& get_carts() { return carts; }
    virtual std::string get_last_err() { return last_err; }
    virtual bool is_cart_inserted() { return cart_inserted; }
    void open_dir(const fat::path&) override;
    virtual void insert_cart(const fat::path&);
    virtual void remove_cart();

    // interface for sd::sd_manager::listener
	virtual void mounted();
	virtual void unmounted();
    virtual void clear_last_err();

    class c64::sys& sys;
    bool cart_inserted;
    fat::path curdir;
    dir_list dirs;
    cart_list carts;
    std::string last_err;
    cart_view view;
};

} // namespace main_menu

#endif /* MAIN_MENU__CART_MANAGER__H */
