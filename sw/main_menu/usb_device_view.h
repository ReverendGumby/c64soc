#ifndef MAIN_MENU__USB_DEVICE_VIEW__H
#define MAIN_MENU__USB_DEVICE_VIEW__H

#include "ui/subscreen_view.h"

namespace main_menu {

class usb_device_view : public ui::subscreen_view
{
public:
    struct model
    {
        struct info_t
        {
            bool valid;
            unsigned address;
            std::string port_name;
            unsigned idVendor, idProduct;
            std::string iManufacturer, iProduct;
            std::string speed;
        };

        virtual info_t get_info() = 0;
    };

    usb_device_view(model&);

    void update();

protected:
    // interface for ui::view
    virtual const char* get_class_name() const { return "usb_device_view"; }

    // interface for ui::subscreen_view
    virtual void on_draw(const drawing_context*);

private:
    model& model;
};

} // namespace main_menu

#endif
