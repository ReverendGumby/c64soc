#include "main_manager.h"
#include "c64/sys.h"
#include "c64/screen.h"
#include "c64/crt.h"
#include "c64/cart.h"
#include "fat/path.h"
#include "fat/directory_iterator.h"
#include "fat/filesystem.h"

#include "cart_manager.h"

namespace main_menu {

cart_manager::cart_manager(main_manager& main_manager, sd::sd_manager& sd_mgr)
    : sys(main_manager._sys_manager), cart_inserted(false), view(*this)
{
    sd_mgr.add_listener(this);
}

void cart_manager::read_dir()
{
    dirs = dir_list{};
    carts = cart_list{};
    last_err = "";

    try
    {
        for (const auto& de : fat::directory_iterator{curdir})
        {
            if (de.is_hidden)
                continue;
            if (de.is_dir) {
                if (de.name == fat::path{L"."})
                    continue;
                dirs.push_back(de.name);
            } else {
                const auto& ext = de.name.extension();
                if (ext != fat::path{L".crt"} && ext != fat::path{L".CRT"})
                    continue;
                cart_item item;
                item.fname = de.name;
                item.size = de.size;
                carts.push_back(item);
            }
        }
    }
    catch (std::runtime_error& e)
    {
        last_err = e.what();
    }

    view.update(cart_view::update_flag_carts | cart_view::update_flag_last_err);
}

void cart_manager::open_dir(const fat::path& p)
{
    curdir = (curdir / p).lexically_normal();
    read_dir();
}

c64::cart::PCrt cart_manager::read_cart(const fat::path& p)
{
    fat::file f{p};
    auto s = f.size();
    c64::Crt::Buf buf{new uint8_t[s]};
    f.read(&buf[0], s);

    c64::cart::PCrt crt{new c64::Crt{std::move(buf), (size_t)s}};
    return crt;
}

c64::config::option_set cart_manager::read_config(const fat::path& cart_p)
{
    auto config_p = cart_p;
    config_p += fat::path{L".cfg"};
    c64::config::option_set os;
    if (!fat::exists(config_p)) {
        // No config file was found.
        return os;
    }
    sys.cfg.load(os, config_p);
    return os;
}

void cart_manager::insert_cart(const fat::path& p)
{
	fat::path ap = p;
    if (ap.is_relative())
        ap = curdir / p;

    last_err = "";
    try
    {
        auto crt = read_cart(ap);
        auto cs = read_config(ap);

        view.save_and_hide_all();

        insert_cart(crt);
        sys.cfg.apply(cs);
    }
    catch (std::runtime_error& e)
    {
        last_err = e.what();
        sys.reset_c64(false);
        view.restore_and_show_all();
    }

    view.update(cart_view::update_flag_last_err | cart_view::update_inserted);
}

void cart_manager::insert_cart(c64::cart::PCrt& crt)
{
    cart_inserted = false;

    sys.reset_c64(true);
    sys.cart.init();
    sys.cart.load(crt);
    sys.reset_c64(false);

    cart_inserted = true;
}

void cart_manager::remove_cart()
{
    if (cart_inserted)
    {
        view.save_and_hide_all();

        cart_inserted = false;

        sys.reset_c64(true);
        sys.cart.init();
        sys.reset_c64(false);

        view.update(cart_view::update_inserted);
    }
}

void cart_manager::mounted()
{
    curdir = fat::path{L"\\"};
    read_dir();
}

void cart_manager::unmounted()
{
    dirs = dir_list{};
    carts = cart_list{};
    curdir.clear();
    last_err = "";

    view.update(cart_view::update_flag_carts | cart_view::update_flag_last_err);
}

void cart_manager::clear_last_err()
{
    last_err = "";
    view.update(cart_view::update_flag_last_err);
}

} // namespace main_menu
