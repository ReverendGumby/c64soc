#include "util/debug.h"
#include "main_manager.h"

#include "joy_menu.h"

#define DBG_LVL 1
#define DBG_TAG "MMJOY"

namespace main_menu {

joy_menu::joy_menu(main_manager& mm, config& cfg)
    : menu_base(mm, "USB > Setup Joysticks", 0, 8),
      mode_model(*this),
      cfg(cfg),
      mode_list(mode_model)
{
    set_color_border(color_t::PURPLE);
    set_color_bg(color_t::BLUE);

    int y = list.get_bottom() + 1;
    lbl_mode.add_to_view(this, "Input source:", 0, y);

    y += 2;
    int h = lbl_exit.get_top() - 1 - y;
    mode_list.set_pos_size(1, y, max_width() - 2, h);
    mode_list.show();
    add_child_view(&mode_list);

    add_menu_item("Control Port 1 / JOYA");
    add_menu_item("Control Port 2 / JOYB (common)");
    update();
    list.select(joy_port_to_idx(c64::JOYB));

    cfg.add_listener(this);
}

joy_menu::~joy_menu()
{
    cfg.remove_listener(this);
}

void joy_menu::joy_mode_changed(c64::joy_port_t port)
{
    if (get_joy_port() == port)
        update_mode_list();
}

void joy_menu::joy_mode_desc_changed(c64::joy_mode_t)
{
    // TODO: Opportunity to optimize: redraw only changed mode
    update_mode_list();
}

joy_menu::mode_model::mode_model(joy_menu& m)
    : menu(m)
{
}

std::string joy_menu::mode_model::get_item(int idx)
{
    return menu.cfg.get_joy_mode_desc(c64::joy_mode_t(idx));
}

void joy_menu::mode_model::on_selected(int idx)
{
    try {
        auto port = menu.get_joy_port();
        menu.cfg.set_joy_mode(port, c64::joy_mode_t(idx));

        menu.list.grab_focus();
    }
    catch (std::runtime_error&) {
    }
}

void joy_menu::update()
{
    menu_base::update();

    update_mode_list();
}

void joy_menu::update_mode_list()
{
    auto port = get_joy_port();

    mode_list.set_count(int(c64::joy_mode_t::joy_modes));
    mode_list.select(int(cfg.get_joy_mode(port)));
    mode_list.draw();
}

void joy_menu::on_selected_changed(int idx)
{
    update_mode_list();
}

void joy_menu::on_selected(int idx)
{
    mode_list.grab_focus();
}

bool joy_menu::on_key_down(const keypress_t& kp, int repeat)
{
    if (kp.code == kbcode::ESC) {
        if (mode_list.has_focus()) {
            list.grab_focus();
            update_mode_list();
            return true;
        }
    }
    return menu_base::on_key_down(kp, repeat);
}

} // namespace main_menu
