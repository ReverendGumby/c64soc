#include <sstream>

#include "cart_view.h"

namespace main_menu {

cart_view::cart_view(struct model& model)
    : model(model), list(*this)
{
    int y = 0;
    int y_footer = max_height() - 5;

    lbl_menu.add_to_view(this, "Select Cartridge", 0, y);

    y += 2;
    lbl_dir.add_to_view(this, "", 0, y, max_width());

    y += 2;
    int h = y_footer - 1 - y;
    list.set_pos_size(1, y, max_width() - 2, h);
    list.show();
    add_child_view(&list);

    y = y_footer;
    lbl_err.add_to_view(this, "", 0, y, max_width());

    y += 2;
    lbl_insert.add_to_view(this, "[RET] Insert selected cartridge", 0, y++);
    lbl_eject.add_to_view(this, "[BAK] Eject cartridge", 0, y++);
    lbl_exit.add_to_view(this, "[ESC] Return to previous menu", 0, y++);

    set_color_bg(color_t::BLACK);
    set_color_border(color_t::BLACK);

    update(update_flag_carts | update_flag_last_err | update_inserted);
}

void cart_view::update(int flags)
{
    if (flags & update_flag_carts)
        update_carts();
    if (flags & update_flag_last_err)
        update_last_err();
    if (flags & update_inserted)
        lbl_eject.set_shown(model.is_cart_inserted());
}

std::string cart_view::get_item(int idx)
{
    if (count == 0 && idx == 0)
        return "(empty)";

    std::stringstream ss;
    if (idx < dir_count) {
        const auto& dir = model.get_dirs().at(idx);
        ss << '[' << dir.string() << ']';
    } else {
        idx -= dir_count;
        const auto& it = model.get_carts().at(idx);
        constexpr int fnw = 32;
        auto kb = it.size / 1024;
        ss << std::left << std::setfill(' ') << std::setw(fnw)
           << it.fname.string().substr(0, fnw)
           << kb << "K";
    }
    return ss.str();
}

void cart_view::on_selected_changed(int)
{
}

void cart_view::on_selected(int idx)
{
    if (idx == -1)
        return;

    if (idx < dir_count) {
        const auto& dirs = model.get_dirs();
        const auto& dir = dirs[idx];
        model.open_dir(dir);
    } else {
        idx -= dir_count;
        const auto& carts = model.get_carts();
        const auto& item = carts[idx];
        fat::path p = item.fname;
        model.insert_cart(p);
    }
}

void cart_view::update_carts()
{
    const auto& curdir = model.get_curdir();
    const auto& dirs = model.get_dirs();
    const auto& carts = model.get_carts();
    no_dir = curdir.empty();

    dir_count = dirs.size();
    auto cart_count = carts.size();
    count = dir_count + cart_count;

    if (no_dir) {
        lbl_dir.set_text("(no SD card inserted)");
    } else {
        std::stringstream ss;
        ss << "--- Directory " << curdir << " ---";
        lbl_dir.set_text(ss.str());
    }
    lbl_dir.draw();

    list.set_count(count);
    if (no_dir) {
        list.hide();
    } else {
        list.show();
        if (count) {
            // Select first cart, else first dir
            list.select(cart_count ? dir_count : 0);
            list.grab_focus();
        } else
            list.select(-1);
    }

    lbl_insert.set_shown(!no_dir);
}

void cart_view::update_last_err()
{
    auto last_err = model.get_last_err();
    std::stringstream ss;
    if (!last_err.empty()) {
        ss << "Error: " << last_err << std::endl;
    }
    lbl_err.set_text(ss.str());
}

bool cart_view::on_key_down(const keypress_t& kp, int repeat)
{
    if (kp.code == kbcode::BAK)
        model.remove_cart();
    else
        return fullscreen_view::on_key_down(kp, repeat);
    return true;
}

void cart_view::on_hide()
{
    // Clear error on view dismissal
    model.clear_last_err();

    fullscreen_view::on_hide();
}

} // namespace main_menu
