#include "main_manager.h"

namespace main_menu {

main_manager::main_manager(c64::sys& sys, sd::sd_manager& sd_mgr,
                           usb::usb_manager& um)
    : _sys_manager(sys),
      _cart_manager(*this, sd_mgr),
      _top_menu(*this),
      _usb_menu(*this),
      _usb_devices(*this, um),
      _joy_menu(*this, sys.joy)
{
    _top_menu.show();
}

} // namespace main_menu
