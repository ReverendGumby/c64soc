#include "util/debug.h"
#include "util/string.h"
#include "main_manager.h"
#include "usb/device.h"
#include "usb/hub/port.h"

#include "usb_devices.h"

#define DBG_LVL 1
#define DBG_TAG "MMUD"

namespace main_menu {

usb_devices::usb_devices(main_manager& mm, usb::usb_manager& um)
    : menu_base(mm, "USB > Show Devices", 0, 10),
      usb_mgr(um),
      dev_view(*this)
{
	usb_mgr.add_listener(this);

    set_color_border(color_t::PURPLE);
    set_color_bg(color_t::BLUE);

    int y = list.get_bottom() + 1;
    lbl_view_hdr.add_to_view(this, "Device details:", 0, y);

    y += 2;
    int h = lbl_exit.get_top() - 1 - y;
    dev_view.set_pos_size(1, y, max_width() - 1, h);
    dev_view.show();
    add_child_view(&dev_view);
}

usb_devices::~usb_devices()
{
	usb_mgr.remove_listener(this);
}

void usb_devices::attached(usb::device*)
{
    update();
}

void usb_devices::detached(usb::device*)
{
    update();
}

void usb_devices::update()
{
    menu_items.clear();
    auto& devs = usb_mgr.get_devices();
    for (auto& d : devs) {
        auto name = d->get_name();
        menu_items.push_back({ name, nullptr });
    }

    menu_base::update();
    dev_view.update();
}

static const char* eps_to_speed(uint8_t eps)
{
    switch (eps) {
    case usb::EPS_FS: return "Full Speed";
    case usb::EPS_LS: return "Low Speed";
    case usb::EPS_HS: return "High Speed";
    default: break;
    }
    return "?";
}

void usb_devices::on_selected_changed(int idx)
{
    auto& devs = usb_mgr.get_devices();
    if (idx < 0 || idx >= (int)devs.size())
        info.valid = false;
    else {
        auto d = devs.at(idx);
        auto dev_desc = d->get_dev_desc();
        auto p = d->get_port();

        info.valid = true;
        info.address = d->get_dev_addr();
        info.port_name = p->get_name();
        info.idVendor = dev_desc.idVendor;
        info.idProduct = dev_desc.idProduct;
        info.iManufacturer = d->get_manufacturer();
        info.iProduct = d->get_product();
        info.speed = eps_to_speed(p->get_eps());
    }

    dev_view.update();
}

usb_device_view::model::info_t usb_devices::get_info()
{
    return info;
}

} // namespace main_menu
