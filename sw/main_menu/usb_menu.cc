#include "main_manager.h"

#include "usb_menu.h"

namespace main_menu {

usb_menu::usb_menu(main_manager& mm)
    : menu_base(mm, "USB Menu")
{
    add_menu_item("Show Devices", &mm._usb_devices);
    add_menu_item("Setup Joysticks", &mm._joy_menu);
    update();

    set_color_border(color_t::PURPLE);
    set_color_bg(color_t::BLUE);
}

} // namespace main_menu
