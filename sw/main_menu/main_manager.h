#ifndef MAIN_MENU__MAIN_MANAGER__H
#define MAIN_MENU__MAIN_MANAGER__H

namespace sd {
class sd_manager;
}

namespace usb {
class usb_manager;
}

#include "cart_manager.h"
#include "top_menu.h"
#include "usb_menu.h"
#include "usb_devices.h"
#include "joy_menu.h"

namespace main_menu {

class main_manager
{
public:
    main_manager(c64::sys&, sd::sd_manager&, usb::usb_manager&);

    c64::sys& _sys_manager;
    cart_manager _cart_manager;
    top_menu _top_menu;
    usb_menu _usb_menu;
    usb_devices _usb_devices;
    joy_menu _joy_menu;
};

} // namespace main_menu

#endif /* MAIN_MENU__MAIN_MANAGER__H */
