#ifndef MAIN_MENU__TOP_MENU__H
#define MAIN_MENU__TOP_MENU__H

#include "menu_base.h"
#include "ui/label.h"

namespace main_menu {

class top_menu : public menu_base
{
public:
    top_menu(main_manager&);

protected:
    // interface for ui::view
    virtual const char* get_class_name() const { return "top_menu"; }

private:
    // UI
    ui::label lbl_hdr, lbl_sw_ver, lbl_hw_ver, lbl_enter;
};

} // namespace main_menu

#endif
