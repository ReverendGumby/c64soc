#include "util/string.h"

#include "usb_device_view.h"

namespace main_menu {

usb_device_view::usb_device_view(struct model& model)
    : model(model)
{
}

void usb_device_view::update()
{
    draw();
}

void usb_device_view::on_draw(const drawing_context* dc)
{
    auto& ss = *(dc->ss);

    auto i = model.get_info();
    if (!i.valid)
        return;

    ss << string_printf("VID:PID %04X:%04X", i.idVendor, i.idProduct)
       << "   " << i.speed << std::endl
       << "Mfg: " << i.iManufacturer << std::endl
       << "Prod: " << i.iProduct << std::endl;
}

} // namespace main_menu
