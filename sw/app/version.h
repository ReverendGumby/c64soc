#ifndef VERSION__H
#define VERSION__H

extern const char* sw_build_timestamp;
extern const char* sw_build_branch;
extern const char* sw_build_rev;

#endif /* VERSION__H */
