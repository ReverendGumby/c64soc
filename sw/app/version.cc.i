// GIT_BRANCH: git branch name of HEAD.
// GIT_REV: abbreviated SHA-1 name of HEAD.

#define GIT_BRANCH "@GEN_VERSION:git rev-parse --abbrev-ref HEAD@"
#define GIT_REV "@GEN_VERSION:git rev-parse --short HEAD@"

const char* sw_build_timestamp = __DATE__ " " __TIME__;
const char* sw_build_branch = GIT_BRANCH;
const char* sw_build_rev = GIT_REV;
