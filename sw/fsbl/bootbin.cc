#include <assert.h>
#include "util/debug.h"
#include "partition.h"
#include "partition_iterator.h"
#include "image.h"
#include "bootbin.h"

#define DBG_LVL 10
#define DBG_TAG "BOOTBIN"

bootbin::bootbin(input& in)
    : in(in)
{
    in.read(&hdr, sizeof(hdr), 0);
    DBG_PRINTF(2, "image_identification: %08lx\n", hdr.image_identification);
    assert(hdr.image_identification == hdr.image_identification_value);
}

void bootbin::read_partition(int idx, partition& part)
{
    DBG_PRINTF(5, "idx=%d\n", idx);
    auto woff = hdr.image_header_table.partition_header_woff
        + idx * (sizeof(part.hdr) / sizeof(uint32_t));
    read_bytes(&part.hdr, sizeof(part.hdr), woff);
}

partition_iterator bootbin::partitions()
{
    return partition_iterator{*this};
}

void bootbin::read_bytes(iop&& iop, uint32_t blen, uint32_t woff)
{
    in.read(std::move(iop), blen, woff * sizeof(uint32_t));
}

void bootbin::read_words(iop&& iop, uint32_t wlen, uint32_t woff)
{
    read_bytes(std::move(iop), wlen * sizeof(uint32_t), woff);
}
