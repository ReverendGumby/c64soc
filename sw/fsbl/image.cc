#include "partition.h"
#include "bootbin.h"
#include "image.h"

image::image(bootbin& bin, partition& partition)
    : bin(bin), part(partition)
{
    bin.read_bytes(&hdr, sizeof(hdr), part.hdr.image_header_woff);
}

std::string image::get_name()
{
    uint32_t name_len;
    for (name_len = 0; hdr.image_name[name_len]; name_len++)
        ;

    const int ws = sizeof(uint32_t);
    std::string name;
    name.reserve(name_len * ws + 1);
    for (uint32_t i = 0; i < name_len; i++)
        for (uint32_t w = 0; w < ws; w++)
            name[i * ws + w] = char(hdr.image_name[i] >> ((ws - 1 - w) * 8));
    name[name_len * ws] = '\0';

    return name;
}
