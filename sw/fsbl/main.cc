#include <stdio.h>
#include <os/init.h>
#include "ps7_init_gpl.h"
#include "sd_reader.h"
#include "bootbin.h"
#include "partition.h"
#include "partition_iterator.h"

void call_entry_point(uint32_t entry_point)
{
    reinterpret_cast<void (*)(void)>(entry_point)();
}

int main()
{
    os::init();

    uint32_t entry_point = 0;

	ps7_init();

    {
        sd_reader sd;
        bootbin boot(sd);

        for (auto& part : boot.partitions()) {
            part.load();
            part.get_ps_entry_point(entry_point);
        }
    }

    ps7_post_config();

    os::deinit();

    if (entry_point) {
    	call_entry_point(entry_point);
    }

	return 0;
}
