#pragma once

#include "util/iop.h"
#include "sd/boot.h"
#include "bootbin.h"
#include "fat/file.h"

class sd_reader : public bootbin::input
{
public:
    sd_reader();

private:
    // Interface for bootbin::input
    void read(iop&& iop, size_t bytes, size_t offset);

    sd::boot sb;
    fat::file fboot;
};

