#pragma once

#include "util/iop.h"
#include "boothdr.h"
#include <stddef.h>
#include <stdint.h>

class partition;
class partition_iterator;
class image;

class bootbin
{
public:
    struct input
    {
        virtual void read(iop&& iop, size_t bytes, size_t offset) = 0;
    };

    bootrom_header_t hdr;

    bootbin(input& in);

    void read_partition(int, partition&);
    partition_iterator partitions();

private:
    // Offsets are in 32-bit words.
    void read_bytes(iop&& iop, uint32_t blen, uint32_t woff);
    void read_words(iop&& iop, uint32_t wlen, uint32_t woff);

    friend partition_iterator;
    friend partition;
    friend image;

    input& in;
};
