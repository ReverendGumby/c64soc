#include <assert.h>
#include "os/soc/soc.h"
#include "os/soc/devcfg.h"
#include "util/debug.h"
#include "util/timer.h"
#include "image.h"
#include "partition.h"
#include "bootbin.h"

#define DBG_LVL 10
#define DBG_TAG "BBPART"

partition::partition(const partition& that)
    : bin(that.bin)
{
    hdr = that.hdr;
}

bool partition::is_null() const
{
    return hdr.total_wlen == 0
        && ~hdr.checksum == 0;
}

void partition::read_header(int idx)
{
    bin.read_partition(idx, *this);
}

void partition::read_words(iop&& iop, uint32_t wlen, uint32_t woff)
{
    bin.read_words(std::move(iop), wlen, hdr.data_woff + woff);
}

void partition::load()
{
    if (DBG_TEST_LEVEL(2)) {
        image img {bin, *this};
        auto name {img.get_name()};
        DBG_PRINTF(2, "%s : 0x%lx @ 0x%lx\n", name.c_str(), hdr.total_wlen * sizeof(uint32_t), hdr.data_woff * sizeof(uint32_t));
    }

    if (hdr.attributes.partition_owner == hdr.PO_FSBL) {
        switch (hdr.attributes.destination_device) {
        case hdr.DD_PS:
            load_ps();
            break;
        case hdr.DD_PL:
            load_pl();
            break;
        default:
            break;
        }
    }
}

void partition::get_ps_entry_point(uint32_t& ep)
{
    if (ps_entry_point)
        ep = ps_entry_point;
}

void partition::load_ps()
{
    if (hdr.destination_load_addr == 0)
        // The FSBL loads to 0: this is us.
        return;

    auto wlen = hdr.extracted_data_wlen;
    auto dest = reinterpret_cast<void*>(hdr.destination_load_addr);

    auto start = timer::now();
    read_words(dest, wlen, 0);
    auto end = timer::now();
    auto dur_ms = (end - start) / timer::ms;

    ps_entry_point = hdr.destination_exe_addr;

    DBG_PRINTF(0, "PS loaded in %llu ms\n", (unsigned long long)dur_ms);
}

void partition::load_pl()
{
    using namespace os::soc;
    struct bitstream_partition_t : devcfg::bitstream_t
    {
        partition* part;
        void load(iovec& iov, size_t buf_wlen, size_t woff) override
        {
            part->read_words(iov, buf_wlen, woff);
        }
    } bs;
    bs.part = this;
    bs.total_wlen = hdr.extracted_data_wlen;

    auto* dc = static_cast<devcfg*>(soc.get_module(module::id_t::devcfg));

    auto start = timer::now();
    dc->configure_pl(bs);
    auto end = timer::now();
    auto dur_ms = (end - start) / timer::ms;
    DBG_PRINTF(0, "PL configured in %llu ms\n", (unsigned long long)dur_ms);
}
