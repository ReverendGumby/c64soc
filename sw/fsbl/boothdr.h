#pragma once

#include <stdint.h>

// From Zync 7000 AP SWDG (UG821), Appendix A, Using Bootgen
// *_woff and *_wlen are word (4-byte) offset and length
struct partition_header_t
{
    enum { DD_PS = 1, DD_PL = 2, DD_INT = 3 };  // destination_device
    enum { PO_FSBL = 0, PO_UBOOT = 1 };         // partition_owner

    uint32_t partition_data_wlen;               // +0x00
    uint32_t extracted_data_wlen;               // +0x04
    uint32_t total_wlen;                        // +0x08
    uint32_t destination_load_addr;             // +0x0c
    uint32_t destination_exe_addr;              // +0x10
    uint32_t data_woff;                         // +0x14
    struct {
        uint32_t tail_alignment : 2;
        uint32_t head_alignment : 2;
        uint32_t destination_device : 4;
        uint32_t destination_instance : 4;
        uint32_t checksum_type : 3;
        uint32_t rsa_signature_present : 1;
        uint32_t partition_owner : 2;
    } attributes;                               // +0x18
    uint32_t section_count;                     // +0x1c
    uint32_t checksum_woff;                     // +0x20
    uint32_t image_header_woff;                 // +0x24
    uint32_t auth_cert_woff;                    // +0x28
    uint32_t _res2c[4];                         // +0x2c
    uint32_t checksum;                          // +0x3c
};

struct image_header_t
{
    uint32_t next_woff;                         // +0x00
    uint32_t first_partition_header_woff;       // +0x04
    uint32_t _res8;                             // +0x08
    uint32_t partition_count;                   // +0x0c
    uint32_t image_name[12];                    // +0x10
};

struct image_header_table_t
{
    uint32_t version;                           // +0x00
    uint32_t image_header_count;                // +0x04
    uint32_t partition_header_woff;             // +0x08
    uint32_t first_image_header_woff;           // +0x0c
    uint32_t auth_header_woff;                  // +0x10
    uint32_t pad[11];                           // +0x14
};

// From Zync 7000 TRM (UG585), Section 6.3.2 BootROM Header
struct bootrom_header_t
{
    uint32_t interrupt_table[8];                // +0x000
    uint32_t width_detection;                   // +0x020
    uint32_t image_identification;              // +0x024
    uint32_t encryption_status;                 // +0x028
    uint32_t user10;                            // +0x02c
    uint32_t source_offset;                     // +0x030
    uint32_t image_length;                      // +0x034
    uint32_t _res14;                            // +0x038
    uint32_t start_of_execution;                // +0x03c
    uint32_t total_image_length;                // +0x040
    uint32_t _res17;                            // +0x044
    uint32_t checksum;                          // +0x048
    uint32_t user19[21];                        // +0x04c
    uint32_t register_initialization[512];      // +0x0a0
    uint32_t user552[8];                        // +0x8a0
    image_header_table_t image_header_table;    // +0x8c0

    static constexpr uint32_t image_identification_value = 0x584C4E58; // 'XLNX'
};

