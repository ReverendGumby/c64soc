#include <assert.h>
#include "util/debug.h"
#include "sd_reader.h"

#define DBG_LVL 1
#define DBG_TAG "FSBL-SD"

sd_reader::sd_reader()
{
    sb.mount_card();
    DBG_PRINTF(1, "card mounted\n");

    fboot = fat::file{fat::path{"\\BOOT.BIN"}};
    DBG_PRINTF(1, "BOOT.BIN size: %lld\n", (long long)fboot.size());
}

void sd_reader::read(iop&& iop, size_t bytes, size_t offset)
{
    DBG_PRINTF(10, "0x%08x - 0x%08x\n", offset, offset + bytes - 1);
    fboot.seek(offset);
    auto was_read = fboot.read(std::move(iop), bytes);
    assert(bytes == was_read);
}
