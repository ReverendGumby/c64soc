#pragma once

#include <iterator>
#include "partition.h"

class partition_iterator
    : public std::iterator<std::input_iterator_tag, partition>
{
public:
    partition_iterator(bootbin& bin) : _bin(bin), _e(bin) {}

    reference operator*() { return _e; }
    pointer operator->() { return &_e; }

    partition_iterator begin() { return partition_iterator{*this, 0}; }
    partition_iterator end() { return partition_iterator{_bin}; }

    partition_iterator& operator++()
    {
        increment();
        return *this;
    }

    friend bool operator!=(const partition_iterator& lhs,
                           const partition_iterator& rhs)
    {
        return !(lhs.is_end() == rhs.is_end());
    }

private:
    partition_iterator(const partition_iterator& i, int idx)
        : _bin(i._bin), _e(i._bin), _idx(idx)
    {
        read();
    }

    bool is_end() const { return _idx < 0 || _e.is_null(); }
    void read() { _e.read_header(_idx); }

    void increment()
    {
        if (!_e.is_null()) {
            _idx ++;
            read();
        }
    }

    bootbin& _bin;
    value_type _e;
    int _idx = -1;
};
