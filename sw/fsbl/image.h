#pragma once

#include "boothdr.h"
#include <string>

class bootbin;
class partition;

class image
{
public:
    image_header_t hdr;

    image(bootbin&, partition&);
    std::string get_name();

private:
    bootbin& bin;
    partition& part;
    uint32_t data_woff;
};
