#pragma once

#include "util/iop.h"
#include "boothdr.h"

class bootbin;
class image;

class partition
{
public:
    partition_header_t hdr;

    partition(bootbin& b) : bin(b) {}
    partition(const partition&);

    bool is_null() const;

    void read_header(int idx);
    void read_words(iop&& iop, uint32_t wlen, uint32_t woff);
    void load();

    void get_ps_entry_point(uint32_t&);

private:
    void load_ps();
    void load_pl();

    bootbin& bin;
    uint32_t ps_entry_point = 0;
};
