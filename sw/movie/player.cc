#include "fm2.h"
#include "nes/sys.h"

#include <mutex>

#include "player.h"

namespace movie {

player::player(nes::sys& sys)
    : _sys(sys)
{
    _thread = std::thread{&player::thread_task, this};
}

player::~player()
{
    // TODO: Terminate and join _thread
}

player::status_t player::get_status()
{
    player::status_t ret {
        .loaded = false,
        .frame = -1,
    };

    auto at = [=, &ret]() {
        ret.playing = _playing;
        if (_mov) {
            ret.loaded = true;
            auto ps = _mov->get_play_state();
            ret.frame = ps.frame;
        }
    };
    perform(action_task_t(at));

    return ret;
}

void player::set_movie(std::shared_ptr<fm2> new_mov)
{
    stop();

    auto at = [=]() {
        _mov = new_mov;
    };
    perform(action_task_t(at));
}

void player::play()
{
    auto at = [=]() {
        if (_mov) {
            _mov->attach_sys(&_sys);
            _mov->seek(0);
            if (!_mov->eom()) {
                set_playing(true);
            }
        }
    };
    perform(action_task_t(at));
}

void player::pause()
{
    // TODO
}

void player::resume()
{
    // TODO
}

void player::stop()
{
    auto at = [=]() {
        set_playing(false);
        if (_mov)
            _mov->attach_sys(nullptr);
    };
    perform(action_task_t(at));
}

void player::set_playing(bool new_playing)
{
    if (_playing == new_playing)
        return;
    _playing = new_playing;

    if (_playing)
        speak(&listener::play_started);
    else if (!_playing)
        speak(&listener::play_ended);
}

void player::perform(action_task_t&& at)
{
    auto f = at.get_future();
    {
        std::lock_guard<std::mutex> lck{_atq_mtx};
        _atq.push_back(std::move(at));
        _atq_pending.notify_one();
    }
    f.wait();
    f.get();
}

bool player::is_action_pending()
{
    std::lock_guard<std::mutex> lck{_atq_mtx};
    return !_atq.empty();
}

player::action_task_t player::get_next_action()
{
    std::lock_guard<std::mutex> lck{_atq_mtx};
    auto at = std::move(_atq.front());
    _atq.pop_front();
    return at;
}

void player::wait_for_action_pending()
{
    std::unique_lock<std::mutex> lck{_atq_mtx};
    _atq_pending.wait(lck, [this]{ return !_atq.empty(); });
}

static bool is_ppu_in_vbl(nes::sys& sys)
{
    auto regs = sys.ppu.read_regs(nes::ppu::regs_video_counter);
    return regs.video_counter.row >= 241;
}

void player::task_playing()
{
    auto old_vbl = _new_vbl;
    _new_vbl = is_ppu_in_vbl(_sys);

    auto next_frame = !old_vbl && _new_vbl;
    if (next_frame) {
        _mov->advance();
        if (_mov->eom())
            set_playing(false);
    }
}

void player::thread_task()
{
    for (;;) {
        if (_playing)
            task_playing();

        while (is_action_pending()) {
            auto at = get_next_action();
            at();
        }

        if (_playing)
            pthread_yield();
        else
            wait_for_action_pending();
    }
}

} // namespace movie
