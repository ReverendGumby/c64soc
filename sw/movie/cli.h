#pragma once

#include "player.h"

#include <cli/command.h>

namespace movie {

class fm2;

class cli : private player::listener
{
public:
    cli(player&);
    ~cli();

private:
    int movie(const ::cli::command::args_t& args);
    int movie_status();
    int play_fm2(const ::cli::command::args_t& args);

    // interface for player::listener
    void play_started() override;
    void play_ended() override;

    player& _player;
};

} // namespace movie
