#pragma once

#include "cli.h"
#include "player.h"

namespace nes {
class sys;
}

namespace movie {

class manager
{
public:
    manager(class nes::sys& sys);

private:
    // void fm2_player(bool step, int play_to_frame);

    player _player;
    cli _cli;
};

} // namespace movie
