#pragma once

#include "util/listener.h"

namespace movie {

namespace _input_source {

struct report
{
    struct joy {
        int a : 1;
        int b : 1;
        int sl : 1;                             // select
        int st : 1;                             // start
        int up : 1;
        int dn : 1;
        int lt : 1;
        int rt : 1;
    };

    joy joy1, joy2;
};

struct listener
{
    virtual void event(const report&) = 0;
};

} // namespace _input_source

struct input_source : talker<_input_source::listener>
{
public:
    using report = _input_source::report;
    using listener = _input_source::listener;
};

} // namespace movie
