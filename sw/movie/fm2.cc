#include "nes/sys.h"

#include <fat/path.h>
#include <fat/fstream.h>

#include <stdexcept>
#include <assert.h>

#include "fm2.h"

namespace movie {

std::shared_ptr<fm2> fm2::load(const char* path)
{
    return std::shared_ptr<fm2>(new fm2{path});
}

fm2::fm2(const char* path)
{
    _ps = play_state_t{};
    _sys = nullptr;

    parse_file(path);
    num_frames = get_num_frames_total();
}

static constexpr bool is_button_press(char c)
{
    return c != '.' && c != ' ';
}

static constexpr input_source::report::joy gamepad_to_joy(uint8_t pad)
{
    return input_source::report::joy {
        .a =  (pad & (1<<0)) != 0,
        .b =  (pad & (1<<1)) != 0,
        .sl = (pad & (1<<2)) != 0,
        .st = (pad & (1<<3)) != 0,
        .up = (pad & (1<<4)) != 0,
        .dn = (pad & (1<<5)) != 0,
        .lt = (pad & (1<<6)) != 0,
        .rt = (pad & (1<<7)) != 0,
    };
}

void fm2::parse_file(const char* path)
{
    const fat::path fpath{path};
    fat::fstream in(fpath);

    for (std::string line; getline(in, line); ) {
        if (line[0] != '|')
            continue;

        input_t inp {};
        for (size_t f = 0, p = 1, q = line.find_first_of('|', p);
             q != line.npos;
             f++, p = q + 1, q = line.find_first_of('|', p)) {
            auto field = line.substr(p, q - p);
            if (f == 0) {
                inp.cmds = std::stoi(field);
            } else if (f == 1 || f == 2) {
                uint8_t pad = 0;
                int bits = field.size();
                for (int i = 0; i < bits; i++)
                    pad |= (1 << (bits - 1 - i)) * is_button_press(field[i]);
                auto joy = gamepad_to_joy(pad);
                if (f == 1)
                    inp.rpt.joy1 = joy;
                else
                    inp.rpt.joy2 = joy;
            }
        }
        // Playback always starts with a poewr-on reset.
        if (inp.cmds == 1 && inputs.size() == 0)
            continue;
        inputs.push_back(inp);
    }
}

int fm2::get_num_frames_total() const
{
    return inputs.size();
}

int fm2::get_num_frames() const
{
    return num_frames;
}

void fm2::set_num_frames(int in)
{
    num_frames = in;
}

void fm2::attach_sys(nes::sys* sys)
{
    if (sys && !_sys) {
        _sys = sys;

        _sys->cart.set_battery_disabled(true);

        auto& joy = _sys->joy;
        joy.stop();
        add_listener(&joy);
    } else if (!sys && _sys) {
        _sys->cart.set_battery_disabled(false);

        auto& joy = _sys->joy;
        remove_listener(&joy);
        joy.start();

        _sys = nullptr;
    }
}

fm2::play_state_t fm2::get_play_state() const
{
    return _ps;
}

void fm2::seek(int frame)
{
    _ps = play_state_t{.frame = frame - 1};
    advance();
}

bool fm2::eom() const
{
    return _ps.frame + 1 >= num_frames;
}

void fm2::advance()
{
    if (eom()) {
        input_t null {};
        speak(&listener::event, null.rpt);
        return;
    }

    if (_ps.frame == -1)
        _sys->power_cycle_nes();

    _ps.frame ++;
    const auto& inp = inputs[_ps.frame];

    speak(&listener::event, inp.rpt);
}

} // namespace movie
