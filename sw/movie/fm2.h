#pragma once

#include "input_source.h"

#include <functional>
#include <vector>
#include <memory>
#include <stdint.h>

namespace nes {
class sys;
}

namespace movie {

class fm2 : private input_source
{
public:
    struct input_t {
        uint8_t cmds;
        report rpt;
    };
    struct play_state_t {
        int frame;
    };

    static std::shared_ptr<fm2> load(const char* path);

    fm2(const char* path);

    int get_num_frames_total() const;
    int get_num_frames() const;
    void set_num_frames(int);

    void attach_sys(nes::sys* sys);

    play_state_t get_play_state() const;

    void seek(int frame);
    bool eom() const;
    void advance();

private:
    void parse_file(const char* path);

    std::vector<input_t> inputs;
    int num_frames;
    play_state_t _ps;
    nes::sys* _sys;
};

} // namespace movie
