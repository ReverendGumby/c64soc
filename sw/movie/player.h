#pragma once

#include "util/listener.h"

#include <memory>
#include <thread>
#include <mutex>
#include <future>
#include <list>

namespace nes {
class sys;
};

namespace movie {

class fm2;

namespace _player {

struct listener
{
    virtual void play_started() = 0;
    virtual void play_ended() = 0;
};

} // namespace _player

class player : public talker<_player::listener>
{
public:
    using listener = _player::listener;

    struct status_t {
        bool loaded;
        bool playing;
        int frame;
    };

    player(nes::sys& sys);
    ~player();

    status_t get_status();

    void set_movie(std::shared_ptr<fm2>);
    void play();
    void pause();
    void resume();
    void stop();

private:
    using action_task_t = std::packaged_task<void(void)>;

    void set_playing(bool);
    void perform(action_task_t&&);
    bool is_action_pending();
    action_task_t get_next_action();
    void wait_for_action_pending();
    void task_playing();
    void thread_task();

    nes::sys& _sys;

    std::thread _thread;
    std::mutex _atq_mtx;
    std::condition_variable _atq_pending;
    std::list<action_task_t> _atq;

    std::shared_ptr<fm2> _mov;
    bool _playing = false;
    bool _new_vbl;
};

} // namespace movie
