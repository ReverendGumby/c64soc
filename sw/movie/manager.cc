#include "cli/manager.h"
#include "nes/sys.h"
#include "fm2.h"

#include <iostream>
#include <stdexcept>

#include "manager.h"

namespace movie {

manager::manager(class nes::sys& sys)
    : _player(sys), _cli(_player)
{
}

#if 0 // TODO: Integrate all of these features
void manager::fm2_player(bool step, int play_to_frame)
{
    auto& movie = *mov;
    try {
        auto num_frames = movie.get_num_frames();
        bool play = play_to_frame != -1;
        if (step) {
            std::cout << "Press any key to step to the next frame.\n";
            movie.set_on_frame([this, &play, &play_to_frame]
                               (const fm2::play_state_t& ps) {
                if (play_to_frame == ps.frame)
                    play = false;
                using cli_manager = ::cli::manager;
                if (!play || cli_manager::is_getch_ready()) {
                    cli_manager::run("pds");
                    cli_manager::run("ctlr");
                    auto c = cli_manager::getch();
                    if (c == 'q')
                        return -1;
                    if (c == 'p')
                        play = !play;
                }
                return 0;
            });
        }

        std::cout << "Playing " << num_frames << " frame movie...\n";
        movie.play(_sys, num_frames);
        std::cout << "... done.\n";
    }
    catch (std::runtime_error& e) {
        std::cout << e.what() << std::endl;
    }
}
#endif

} // namespace movie
