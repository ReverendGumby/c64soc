#include "fm2.h"

#include <cli/command.h>

#include <stdexcept>
#include <iostream>

#include "cli.h"

namespace movie {

using namespace ::cli;

cli::cli(class player& player)
    : _player(player)
{
    command::define("movie", [this](const command::args_t& args) {
        return movie(args); });
    command::define("fm2", [this](const command::args_t& args) {
        return play_fm2(args); });

    _player.add_listener(this);
}

cli::~cli()
{
    _player.remove_listener(this);
}

int cli::movie(const command::args_t& args)
{
    if (args.size() < 1)
        return movie_status();
    auto it = args.begin();
    auto sub = *it;
    command::args_t sub_args(++it, args.end());
    if (sub == "status")
        return movie_status();
    else if (sub == "play")
        _player.play();
    else if (sub == "stop")
        _player.stop();
    else if (sub == "pause")
        _player.pause();
    else if (sub == "resume")
        _player.resume();
    else
        throw std::runtime_error("unrecognized subcommand");

    return 0;
}

int cli::movie_status()
{
    auto s = _player.get_status();

    if (!s.loaded) {
        std::cout << "No movie is loaded\n";
    } else {
        std::cout << "Movie is " << (s.playing ? "" : "NOT ") << "playing\n";
        std::cout << "  Current frame: " << s.frame << '\n';
    }

    return 0;
}

int cli::play_fm2(const command::args_t& args)
{
    auto it = args.begin();
    if (args.end() - it < 1)
        throw std::range_error("missing filename");
    const char* fname = it->c_str();
    it++;
    int user_num_frames = -1;
    if (it < args.end()) {
        user_num_frames = strtoul(it->c_str(), NULL, 0);
        it++;
    }

    auto mov = fm2::load(fname);
    if (user_num_frames >= 0)
        mov->set_num_frames(user_num_frames - 1);
    std::cout << "Loaded " << mov->get_num_frames() << " frame movie\n";

    _player.set_movie(mov);
    _player.play();

    return 0;
}

void cli::play_started()
{
    std::cout << "Movie playback started.\n";
}

void cli::play_ended()
{
    std::cout << "Movie playback ended.\n";
}

} // namespace movie
