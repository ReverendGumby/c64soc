#pragma once

// Reference: https://en.wikipedia.org/wiki/ANSI_escape_code

#include <ui/kbd.h>

#include <vector>

namespace cli {

struct esc_seq
{
   // Fe Escape sequence bytes
    enum {
        CSI = '[',
    };

    bool empty;
    bool done;
    char fe_esc;                                // Fe Escape sequence
    std::vector<char> param_bytes;              // parameter bytes, 0x30-0x3F
    std::vector<char> int_bytes;                // intermediate bytes, 0x20-0x2F
    char final_byte;                            // final byte, 0x40-0x7E

    esc_seq();

    void clear();
    void start();
    void add(char c);

    bool is_empty() const { return empty; }
    bool is_done() const { return done; }

    using keypress_t = ui::kbd::keypress;
    using kbcode_t = decltype(keypress_t::code);
    keypress_t to_keypress() const;
};

} // namespace cli
