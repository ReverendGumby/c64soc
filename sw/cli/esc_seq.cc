#include "esc_seq.h"

#include <assert.h>

namespace cli {

esc_seq::esc_seq()
{
    clear();
}

void esc_seq::clear()
{
    empty = true;
    done = true;
    fe_esc = 0;
    param_bytes.clear();
    int_bytes.clear();
    final_byte = 0;
}

void esc_seq::start()
{
    clear();
    empty = false;
    done = false;
}

void esc_seq::add(char c)
{
    if (!done) {
        if (!fe_esc)
            fe_esc = c;
        else if (fe_esc == CSI) {
            if (c >= 0x30 && c <= 0x3F)
                param_bytes.push_back(c);
            else if (c >= 0x20 && c <= 0x2F)
                int_bytes.push_back(c);
            else {
                final_byte = c;
                done = true;
            }
        }
    }
}

esc_seq::keypress_t esc_seq::to_keypress() const
{
    keypress_t kp {};

    if (done) {
        kp.down = true;
        if (fe_esc == CSI) {
            switch (final_byte) {
            case 'A': kp.code = kbcode_t::UAR; break;
            case 'B': kp.code = kbcode_t::DAR; break;
            case 'C': kp.code = kbcode_t::RAR; break;
            case 'D': kp.code = kbcode_t::LAR; break;
            }
        }
    }

    return kp;
}

} // namespace cli
