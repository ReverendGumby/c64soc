#pragma once

#include "termout.h"

#include <os/dev/char_device.h>

#include <string>
#include <vector>

#include "esc_seq.h"

namespace cli {

class terminal
{
public:
    terminal(os::dev::char_device*);

    char getchar();

private:
    static constexpr int len = 128;

    char handle_char(char);
    void handle_es();

    void reset();
    void readline();

    void delete_char();
    void insert(char c);
    void move_cursor(int dist);

    void add_history();
    void recall_history(int dir);

    os::dev::char_device* dev;
    termout out;

    char buf[len];
    int used, writep, readp;
    esc_seq es;

    static constexpr int history_max = 100;
    std::vector<std::string> history;
    decltype(history)::const_iterator history_it;
};

} // namespace cli
