#include "manager.h"

#include "builtins.h"
#include "readbytes.h"
#include "script.h"

#include <os/dev/device.h>
#include <fat/filesystem.h>
#include <util/string.h>

#include <iostream>
#include <algorithm>
#include <pthread.h>

namespace cli {

std::vector<command> manager::cmdlist;
os::dev::char_device* manager::dev = nullptr;

manager::run_error::run_error(const char* who, const char* what)
    : std::runtime_error{string_printf("%s: %s", who, what)}
{
}

void manager::add_command(command&& cmd)
{
    cmdlist.emplace(cmdlist.end(), cmd);
}

command* manager::find_command(const std::string& cmd)
{
    auto icmd = std::find_if(cmdlist.begin(), cmdlist.end(),
                             [&cmd](const command& cc)
                             { return cmd == cc.name; });
    if (icmd == cmdlist.end())
        throw std::runtime_error("command not found");
    return &*icmd;
}

bool manager::is_getch_ready()
{
    if (!dev)
        return false;
    return !dev->is_rx_empty();
}

char manager::getch()
{
    if (!dev)
        return 0;
    return dev->read();
}

manager::manager()
    : term(nullptr)
{
    define_builtin_commands();
}

void manager::try_autoexec()
{
    wait_for_filesystem = true;
}

void manager::start()
{
    dev = static_cast<decltype(dev)>(os::dev::device::find("console"));
    term = new terminal(dev);
    cli_set_readbytes(term);

    cli_thread = std::thread{&manager::cli_task, this};
}

parser::parsed_command manager::parse(const char* cmdline)
{
    const char* who = "(manager)";

    try {
        return parser::parse(cmdline);
    }
    catch (std::runtime_error& e) {
        throw run_error(who, e.what());
    }
}

void manager::run(const parser::parsed_command& pc)
{
    auto* pcmd = pc._command;
    const auto& args = pc._args;

    if (!pcmd)                                  // empty command
        return;

    const char* who = pcmd->name;

    try {
        (void)pcmd->action(args);
    }
    catch (std::runtime_error& e) {
        throw run_error(who, e.what());
    }
}

void manager::run(const char* cmdline)
{
    auto pc = parse(cmdline);
    run(pc);
}

const std::vector<command>& manager::get_cmdlist()
{
    return cmdlist;
}

void manager::cli_task()
{
    if (wait_for_filesystem)
        run_autoexec();

    std::string last_line;
    for (;;) {
        std::cout << "$ ";
        std::cout.flush();

        std::string line;
        std::getline(std::cin, line);

        if (line.empty() && is_last_command_repeatable)
            line = last_line;

        try {
            run_cli(line.c_str());
        } catch (run_error& e) {
            std::cout << e.what() << "\n";
        }

        last_line = line;
    }
}

void manager::run_cli(const char* cmdline)
{
    auto pc = parse(cmdline);

    auto* pcmd = pc._command;
    if (pcmd) {
        is_last_command_repeatable = pcmd->flags & command::repeatable;
    }

    run(pc);
}

void manager::run_autoexec()
{
    while (!fat::is_mounted())
        pthread_yield();

    cli::script scr{"\\autoexec.sh"};
    if (scr.exists())
        scr.run();
}

} // namespace cli
