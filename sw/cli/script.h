#pragma once

namespace cli {

class script
{
public:
    script(const char* path);

    bool exists();
    void run();

private:
    const char* path;
};

} // namespace cli
