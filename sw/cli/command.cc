#include "command.h"

#include "manager.h"

namespace cli {

void command::define(const char* name, action_t action, flags_t flags)
{
    manager::add_command(command{name, action, flags});
}

} // namespace cli
