#pragma once

#include <os/dev/char_device.h>

#include <string>

namespace cli {

class termout
{
public:
    termout(os::dev::char_device*);

    void write(char c) { dev->write(c); }
    void write(const char*);
    void write(const char*, size_t);
    void write(const std::string& s) { write(s.c_str()); }

    void delete_char();
    void move_cursor(int dist);

private:
    os::dev::char_device* dev;
};

} // namespace cli
