#include "command.h"

#include "manager.h"

#include <iostream>

namespace cli {

static int help(const command::args_t& args)
{
    std::cout << "Available commands:\n";
    for (const auto& cmd : manager::get_cmdlist())
        std::cout << "  " << cmd.name << "\n";
    return 0;
}

void define_builtin_commands()
{
    command::define("help", &help);
}

} // namespace cli
