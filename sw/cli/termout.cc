#include "termout.h"

#include <util/string.h>

namespace cli {

termout::termout(os::dev::char_device* dev)
    : dev(dev)
{
}

void termout::write(const char* s)
{
    for (; *s; s++)
        write(*s);
}

void termout::write(const char* s, size_t len)
{
    for (size_t i = 0; i < len; i++)
        write(s[i]);
}

void termout::delete_char()
{
    write("\b \b");
}

void termout::move_cursor(int dist)
{
    if (dist == -1) {
        write('\b');
        return;
    } else if (dist != 0) {
        char final_byte = 'C';
        if (dist < 0) {
            final_byte = 'D';
            dist = -dist;
        }
        write(string_printf("\x1b[%d%c", dist, final_byte));
    }
}

} // namespace cli
