#include "terminal.h"

#include <util/string.h>

#include <string.h>

namespace cli {

enum {
    SOH = 1, // ^A
    STX = 2, // ^B
    ETX = 3, // ^C
    ENQ = 5, // ^E
    ACK = 6, // ^F
    BS = 8,
    FF = 12, // ^L
    ESC = 27,
    DEL = 127,
};

terminal::terminal(os::dev::char_device* dev)
    : dev(dev), out(dev)
{
    reset();
    es.clear();
    history_it = history.cend();
}

char terminal::getchar()
{
    if (readp == used + 1) {
        readline();
        readp = 0;
    }

    if (readp == used) {
        readp ++;
        return '\n';
    }
    return buf[readp ++];
}

char terminal::handle_char(char c)
{
    switch (c) {
    case ETX:
        out.write("^C");
        reset();
        goto crlf;
    case BS:
        delete_char();
        break;
    case ESC:
        es.start();
        break;
    case STX:
        move_cursor(-1);
        break;
    case ACK:
        move_cursor(1);
        break;
    case SOH:
        move_cursor(-writep);
        break;
    case ENQ:
        move_cursor(used - writep);
        break;
    case '\r':
    case '\n':
    crlf:
        // CR or LF -> CRLF
        out.write('\r');
        out.write(c = '\n');
        break;
    default:
        if (c < ' ')
            break;
        insert(c);
        break;
    }

    return c;
}

void terminal::handle_es()
{
    auto kp = es.to_keypress();
    auto code = kp.code;
    switch (code) {
    case decltype(code)::LAR:
        move_cursor(-1);
        break;
    case decltype(code)::RAR:
        move_cursor(1);
        break;
    case decltype(code)::UAR:
        recall_history(-1);
        break;
    case decltype(code)::DAR:
        recall_history(1);
        break;
    default:
        break;
    }
}

void terminal::reset()
{
    writep = used = 0;
    readp = used + 1;
}

void terminal::readline()
{
    writep = used = 0;

    char c;
    do {
        c = dev->read();
        if (!es.is_empty()) {
            es.add(c);
            if (es.is_done()) {
                handle_es();
                es.clear();
            }
            continue;
        } else
            c = handle_char(c);
    } while (c != '\n');

    add_history();
}

void terminal::delete_char()
{
    if (writep == 0)
        return;

    writep--;
    memmove(&buf[writep], &buf[writep + 1], len - writep - 1);
    used--;

    out.move_cursor(-1);
    out.write(&buf[writep], used - writep);
    out.write(' ');
    out.move_cursor(writep - used - 1);
}

void terminal::insert(char c)
{
    if (used >= len)
        return;

    memmove(&buf[writep + 1], &buf[writep], used - writep);
    used++;
    buf[writep++] = c;

    out.write(c);
    if (writep < used) {
        out.write(&buf[writep], used - writep);
        out.move_cursor(writep - used);
    }
}

void terminal::move_cursor(int dist)
{
    if ((dist < 0 && writep + dist >= 0)
        || (dist > 0 && writep + dist <= used)) {
        writep += dist;
        out.move_cursor(dist);
    }
}

void terminal::add_history()
{
    if (used) {
        if (history.size() == history_max)
            history.erase(history.begin());
        history.push_back(std::string(buf, used));
        history_it = history.cend();
    }
}

void terminal::recall_history(int dir)
{
    // Don't go past the start or (end + 1)
    if ((dir < 0 && history_it == history.cbegin())
        || (dir > 0 && history_it == history.cend()))
        return;

    if (writep < used)
        out.move_cursor(used - writep);
    while (used--)
        out.delete_char();

    history_it += dir;
    if (history_it != history.cend()) {
        auto line = *history_it;
        used = line.size();
        memcpy(buf, line.c_str(), used);
        out.write(buf, used);
    } else
        used = 0;
    writep = used;
}

} // namespace cli
