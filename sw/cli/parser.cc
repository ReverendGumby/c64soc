#include "parser.h"

#include "manager.h"

#include <regex>

namespace cli {

parser::parse_error::parse_error(const char* what)
    : std::runtime_error{what}
{
}

static parser::args_t get_cmd_args(const std::string& line)
{
    // Split into space-separated words. Double quotes preserve spaces.
    parser::args_t args;
    static const std::regex re{R"(\s*(?:\"(.*)\"|(\S+)))"};
    for (std::sregex_iterator ri(
             line.begin(), line.end(), re,
             std::regex_constants::match_continuous);
         ri != std::sregex_iterator{};
         ++ri) {
            const auto& mr = *ri;
            if (mr[1].matched)
                args.push_back(mr[1].str());
            else if (mr[2].matched)
                args.push_back(mr[2].str());
    }
    return args;
}

parser::parsed_command parser::parse(const char* cmdline)
{
    parsed_command ret;

    ret._command = nullptr;
    auto& args = ret._args;

    args = get_cmd_args(cmdline);
    if (args.size() != 0) {
        auto& cmd = args.front();
        ret._command = manager::find_command(cmd);
        args.erase(args.begin());
    }
    return ret;
}

} // namespace cli
