#pragma once

namespace cli {

class terminal;

void cli_set_readbytes(terminal*);

} // namespace cli
