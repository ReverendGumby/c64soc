#pragma once

#include <string>
#include <vector>
#include <functional>

namespace cli {

struct command
{
    using args_t = std::vector<std::string>;
    using action_t = std::function<int(const args_t&)>;

    enum flags_t {
        no_flags = 0,
        repeatable = 1<<0,
    };

    const char* name;
    action_t action;
    flags_t flags = no_flags;

    static void define(const char* name, action_t action,
                       flags_t flags = no_flags);
};

} // namespace cli
