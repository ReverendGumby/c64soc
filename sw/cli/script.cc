#include "script.h"

#include "manager.h"

#include <fat/path.h>
#include <fat/filesystem.h>
#include <fat/fstream.h>

#include <stdexcept>
#include <iostream>
#include <algorithm>

namespace cli {

script::script(const char* path)
    : path(path)
{
}

bool script::exists()
{
    return fat::exists(fat::path{path});
}

void script::run()
{
    int linenum = -1;
    try {
        fat::path fpath{path};
        fat::fstream in(fpath);
        std::cout << "Executing " << path << "..." << std::endl;

        linenum = 0;
        for (std::string line; getline(in, line); linenum++) {
            // Remove comments
            auto p = line.find('#');
            if (p != line.npos)
                line.erase(p);
            // Remove trailing whitespace
            line.erase(line.begin(),
                       std::find_if(line.begin(), line.end(),
                                    [](char c) { return !std::isspace(c); }));
            if (line.size() == 0)
                continue;

            // Execute command
            manager::run(line.c_str());
        }
    } catch (std::runtime_error& e) {
        std::cout << path;
        if (linenum >= 0)
            std::cout << ':' << linenum;
        std::cout << ": " << e.what() << std::endl;
    }
}

} // namespace cli
