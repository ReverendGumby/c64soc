#include "terminal.h"
#include <os/libgloss/read.h>

namespace cli {

static terminal* cli_term;

static size_t cli_readbytes(char *buf, size_t nbyte)
{
    size_t i;

    for (i = 0; i < nbyte; i++) {
        char c = cli_term->getchar();
        ((char*)buf)[i] = c;

        // We're line-buffered.
        if (c == '\n') {
            i++;
            break;
        }
    }
    return i;
}

void cli_set_readbytes(terminal* term)
{
    cli_term = term;
    ::readbytes = cli_readbytes;
}

} // namespace cli
