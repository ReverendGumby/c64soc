#pragma once

#include "command.h"

#include <stdexcept>

namespace cli {

struct parser
{
    using command_t = command;
    using args_t = command::args_t;

    struct parse_error : public std::runtime_error
    {
        parse_error(const char* what);
    };

    struct parsed_command {
        command_t* _command;
        args_t _args;
    };

    static parsed_command parse(const char* cmdline);
};

} // namespace cli
