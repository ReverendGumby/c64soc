#pragma once

#include <os/dev/char_device.h>
#include "terminal.h"
#include "command.h"
#include "parser.h"

#include <stdexcept>
#include <thread>

namespace cli {

class manager
{
public:
    struct run_error : public std::runtime_error
    {
        run_error(const char* who, const char* what);
    };

    manager();

    static void add_command(command&&);
    static command* find_command(const std::string&);

    static bool is_getch_ready();
    static char getch();

    void try_autoexec();
    void start();

    static void run(const char* cmdline);

    static const std::vector<command>& get_cmdlist();

private:
    void cli_task();
    void run_cli(const char* cmdline);
    void run_autoexec();

    static parser::parsed_command parse(const char* cmdline);
    static void run(const parser::parsed_command&);

    static std::vector<command> cmdlist;
    static os::dev::char_device* dev;

    terminal* term;
    std::thread cli_thread;
    bool wait_for_filesystem = false;
    bool is_last_command_repeatable = false;
};

} // namespace cli
