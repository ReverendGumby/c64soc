#ifndef C64_HID__H
#define C64_HID__H

#include "ui/hid_manager.h"

namespace c64 {

class hid
{
public:
    hid(ui::hid_manager&);

    virtual void start();
    virtual void stop();

protected:
    void start_on_idle();
    void on_event();

    virtual bool is_idle() = 0;
    virtual void reset_c64bus_matrix() = 0;

    ui::hid_manager& hid_mgr;
    bool started;
    bool resume;
};

} // namespace c64

#endif /* C64_HID__H */
