#ifndef C64_JOY__H
#define C64_JOY__H

#include <string>
#include "config/manager.h"
#include "config/option.h"
#include "ui/hid_manager.h"
#include "ui/kbd.h"
#include "ui/joy.h"
#include "hid.h"

namespace c64 {

enum class joy_mode_t {
    none = 0,
    keymap_a,
    keymap_b,
    hid_1,
    hid_2,
    joy_modes
};

// Joystick ports
// (Values can be used to index _mode[])
enum joy_port_t { JOY_PORT_INVALID = -1, JOYA = 0, JOYB = 1, NUM_JOY_PORTS };

constexpr bool joy_mode_is_hid(joy_mode_t m) { return m == joy_mode_t::hid_1 || m == joy_mode_t::hid_2; }
constexpr bool joy_mode_is_keymap(joy_mode_t m) { return m == joy_mode_t::keymap_a || m == joy_mode_t::keymap_b; }

class joy;

namespace _joy {

class listener
{
public:
    virtual void joy_mode_changed(joy_port_t) = 0;
    virtual void joy_mode_desc_changed(joy_mode_t) = 0;
};

class config : public talker<class listener>
{
public:
    using listener = class listener;

    virtual joy_mode_t get_joy_mode(joy_port_t) = 0;
    virtual std::string get_joy_mode_desc(joy_mode_t) = 0;

    virtual void set_joy_mode(joy_port_t, joy_mode_t) = 0;
};

class option : public c64::config::option
{
public:
    option(class joy&, joy_port_t);

    const std::string& name() override;
    std::string get() override;
    void set(const std::string&) override;

private:
    class joy& _joy;
    joy_port_t _port;
    std::string _name;
};

} // namespace _joy

class joy : public hid,
            private ui::joy::listener,
            private ui::kbd::listener,
            private ui::hid_manager::listener,
            public _joy::config
{
public:
    using config = _joy::config;

    joy(ui::hid_manager&, c64::config::manager&);
    virtual ~joy();

private:
    struct joy_out {
        uint8_t joy_a;
        uint8_t joy_b;
    };

    friend _joy::option;

    joy_mode_t& mode(joy_port_t port) { return _mode[port - JOYA]; }

    ui::joy::report* jrpt(joy_mode_t);

    joy_out reports_to_joy_out();
    void set_c64bus_matrix();

    std::string get_joy_mode_desc_hid(int);
    void change_port_mode(joy_port_t, joy_mode_t);

    // interface for hid
    bool is_idle() override;
    void reset_c64bus_matrix() override;

    // interface for ui::joy::listener
    virtual void event(const ui::joy::report&);

    // interface for ui::kbd::listener
    virtual void event(const ui::kbd::report&);
    virtual void event(const ui::kbd::keypress&);

    // interface for ui::hid_manager::listener
    virtual void joy_dev_changed(int id);

    // interface for config
    virtual joy_mode_t get_joy_mode(joy_port_t);
    virtual std::string get_joy_mode_desc(joy_mode_t);
    virtual void set_joy_mode(joy_port_t, joy_mode_t);

    ui::joy* joy_dev = nullptr;
    ui::kbd* kbd_dev = nullptr;

    static constexpr int num_joys = NUM_JOY_PORTS;
    joy_mode_t _mode[num_joys];

    ui::joy::report jrpt1;
    ui::joy::report jrpt2;
    ui::kbd::report krpt;
};

} // namespace c64

#endif /* C64_JOY__H */
