#ifndef C64_SCREEN__H
#define C64_SCREEN__H

#include "vid.h"
#include "ui/screen.h"

namespace c64 {

class screen : public ui::screen
{
public:
    static constexpr int WIDTH = vid::WIDTH;
    static constexpr int HEIGHT = vid::HEIGHT;

    uint8_t ascii_to_screen(char);

    screen(vid&);
    virtual ~screen();

    int get_width() const { return WIDTH; }
    int get_height() const { return HEIGHT; }

    vid::saved_state get_saved_state() const { return ss; }
    
    void set_bg(color_t c);
    void set_border(color_t c);

    void init();
    void clear(color_t c);

    void write(int x, int y, uint8_t b, color_t c);

    void invert_rect(int x, int y, int w, int h);
    void invert_row(int y) { invert_rect(0, y, WIDTH, 1); }

private:
    vid& v;
    class vid::saved_state ss;
    vid::addrs& addrs;
};

} // namespace c64

#endif /* C64_SCREEN__H */
