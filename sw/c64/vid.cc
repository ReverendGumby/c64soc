#include "c64bus.h"
#include "memptr.h"
#include "screen.h"
#include "util/debug.h"
#include "hwregs.h"
#include "vid.h"

#define DBG_LVL 2
#define DBG_TAG "vid"

namespace c64 {

constexpr uint8_t nVA15_14 = 3<<0;
constexpr uint8_t nVA15_14_S = 0;

vid::addrs vid::get_addrs()
{
    uint8_t pa = CIA2[CIA_PA];     // bits [1:0] are nVA[15:14] = ~VA[15:14]
    uint8_t mp = VIC[VIC_MP];
    uint16_t va_15_14 = ((~pa) & nVA15_14) >> nVA15_14_S;
    uint16_t vm_13_10 = (mp & MP_VM) >> MP_VM_S;
    memaddr gfxmem_addr = (va_15_14 << 14) | (vm_13_10 << 10);
    memptr gfxmem{gfxmem_addr, memtype::ram};
    DBG_PRINTF(2, "CIA2.PA=%02X VIC.MP=%02X gfxmem=%s\n", pa, mp, gfxmem.to_string().c_str());

    vid::addrs a;
    a.gfxmem_base = gfxmem;

    return a;
}

vid::saved_state vid::save_state()
{
    saved_state ss;

    for (int i = 0; i < REGS_SIZE; i++)
    {
        ss.regs[i] = VIC[i];
        DBG_PRINTF(3, "VIC[%02X]=%02X\n", i, ss.regs[i]);
    }

    // Reset the VIC to bank 0 ($0000-$3FFF) by floating nVA14/15. This lets
    // the VIC see the character ROM.
    ss.ddra = CIA2[CIA_DDRA];
    CIA2[CIA_DDRA] = ss.ddra & ~nVA15_14;

    // Set the screen memory and character ROM bases.
    VIC[VIC_MP] = MP_VM_DEFAULT | MP_CB_UPPER;

    ss.a = get_addrs();
    auto gfxmem = ss.a.gfxmem_base;

    for (int i = 0; i < CLRMEM_SIZE; i++)
        ss.clrmem[i] = CLRMEM[i];
    for (int i = 0; i < GFXMEM_SIZE; i++)
        ss.gfxmem[i] = gfxmem[i];

    return ss;
}

void vid::restore_state(const vid::saved_state& ss)
{
    auto gfxmem = ss.a.gfxmem_base;

    for (int i = 0; i < GFXMEM_SIZE; i++)
        gfxmem[i] = ss.gfxmem[i];
    for (int i = 0; i < CLRMEM_SIZE; i++)
        CLRMEM[i] = ss.clrmem[i];
    for (int i = 0; i < REGS_SIZE; i++)
        VIC[i] = ss.regs[i];

    CIA2[CIA_DDRA] = ss.ddra;
}

screen* vid::get_screen()
{
    return new screen(*this);
}

} // namespace c64
