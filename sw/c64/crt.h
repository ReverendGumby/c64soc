#ifndef __CRT_H__
#define __CRT_H__

#include <stdint.h>
#include <stddef.h>
#include <string>
#include <vector>
#include <memory>
#include <stdexcept>

namespace c64 {

struct Crt_format_error : public std::runtime_error
{
    Crt_format_error(const char *what_arg)
        : std::runtime_error {what_arg} {}
};

class Chip {
public:
    Chip(const uint8_t *bin, size_t len);
    Chip(const Chip &that) = delete;
    Chip(Chip &&that) = default;
    virtual ~Chip() = default;

    uint16_t bank() const { return _bank; }
    uint16_t start() const { return _start; }
    const uint8_t *data() const { return _data; }
    size_t len() const { return _len; }

private:
    uint16_t _bank;
    uint16_t _start;
    const uint8_t* _data;
    size_t _len;
};

class Crt {
public:
    using Buf = std::unique_ptr<uint8_t[]>;
	using ChipList = std::vector<Chip>;

    Crt(Buf&& bin, size_t len);
    virtual ~Crt();

    enum hw_type_t {
        NORMAL = 0,
        FINAL_CARTRIDGE_III = 3,
        OCEAN_1 = 5,
        MAGIC_DESK = 19,
        EASYFLASH = 32,
    };

    const std::string &name() const { return _name; }
    hw_type_t hw_type() const { return _hw_type; }
    bool exrom_asserted() const { return _exrom; }
    bool game_asserted() const { return _game; }

    const ChipList &chips() const { return _chips; }

private:
    Buf _bin;
    hw_type_t _hw_type;
    std::string _name;
    bool _exrom, _game;
    ChipList _chips;
};

} // namespace c64

#endif /* __CRT_H__ */
