#ifndef C64_KBD__H
#define C64_KBD__H

#include "ui/kbd.h"
#include "hid.h"

namespace c64 {

class kbd : public hid,
            private ui::kbd::listener
{
public:
    kbd(ui::hid_manager&);

private:
    using kbcode = ui::kbd::kbcode;

    struct kbd_out {
        int shift;
        int ctrl;
        int alt;
        int win;
        uint8_t matrix[8];
        uint8_t misc;
    };

    void translate_code_pos(kbcode c, kbd_out& k);
    void translate_code_sym(kbcode c, kbd_out& k);
    kbd_out kbd_report_to_kbd_out(const ui::kbd::report& rpt);
    void set_c64bus_matrix(const kbd_out& k);

    // interface for hid
    bool is_idle() override;
    void reset_c64bus_matrix() override;

    // interface for usb::kbd::listener
    virtual void event(const ui::kbd::report&);
    virtual void event(const ui::kbd::keypress&);

    ui::kbd* dev = nullptr;
};

} // namespace c64

#endif /* C64_KBD__H */
