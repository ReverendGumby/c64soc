#ifndef C64_MEMPTR__H
#define C64_MEMPTR__H

#include <stdint.h>
#include <string>
#include "c64bus.h"

namespace c64 {

using memaddr = uint16_t;

constexpr memaddr memaddr_min = 0x0000;
constexpr memaddr memaddr_max = 0xFFFF;

enum class memtype
{
    undef = 0,                                  // no specific target
    ram,                                        // internal 64K RAM
    introm,                                     // KERNAL, BASIC, CHARGEN
    extrom,                                     // ROMH, ROML
    io,                                         // VIC, SID, etc.
};

const char* name(memtype);

class memval;

class memptr
{
public:
    memptr() {}
    constexpr memptr(memaddr a, memtype t) : _a(a), _t(t) {}
    constexpr memptr(memaddr a) : memptr(a, memtype::undef) {}
    constexpr memptr(const memptr&) = default;

    memtype type() const { return _t; }
    memaddr addr() const { return _a; }
    unsigned region() const { return (_a >> 12) & 0xF; }

    memptr& operator+=(int i) { _a += i; return *this; }
    memptr& operator++() { *this += 1; return *this; }
    memptr operator++(int) { auto c = *this; *this += 1; return c; }

    memval operator*() const;
    memval operator[](int) const;

    bool same_target(const memptr&) const;

    std::string to_string() const;

private:
    memaddr _a;
    memtype _t;
};

inline memptr operator+(memptr p, int i)
{
    return p += i;
}

class memcso
{
public:
    using value_type = uint8_t;

    // Chip select override bits
    static constexpr value_type charen = C64BUS_CSO_CHAREN;
    static constexpr value_type hiram  = C64BUS_CSO_HIRAM;
    static constexpr value_type loram  = C64BUS_CSO_LORAM;
    static constexpr value_type game   = C64BUS_CSO_GAME;
    static constexpr value_type exrom  = C64BUS_CSO_EXROM;

    static constexpr value_type none = 0;
    static constexpr value_type all = charen | hiram | loram | game | exrom;

    memcso() : _def(all), _val(none) {} // default mapping
    memcso(value_type def, value_type val) : _def(def), _val(val) {}
    memcso(value_type val) : _def(all), _val(val) {}

    bool is_def(value_type v) const { return (_def & v) == v; }
    bool is_undef(value_type v) const { return (_def & v) == 0; }
    bool is_set(value_type v) const { return is_def(v) && (_val & v) == v; }
    bool is_clr(value_type v) const { return is_def(v) && (_val & v) == 0; }

    memcso& set(value_type v) { _def |= v; _val |= v; return *this; }
    memcso& clr(value_type v) { _def |= v; _val &= ~v; return *this; }
    memcso& undef(value_type v) { _def &= ~v; return *this; }

    bool operator==(const memcso& that) const;
    bool operator!=(const memcso& that) const { return !(*this == that); }

    // Read actual state of chip select controls
    static memcso read_mon();

    void apply() const;

    std::string to_string() const;

private:
    // bits set in _def are defined in _val, clr are don't care (X)
    // defined bits in _val: set asserts, clr deasserts
    value_type _def;
    value_type _val;
};

memcso to_memcso(const memptr&);
memtype to_memtype(const memaddr&, const memcso&);

class memval
{
public:
    using value_type = uint8_t;

    explicit memval(const memptr& p) : _p(p) {}
    operator uint8_t();
    memval& operator=(uint8_t);
    memval& operator^=(uint8_t);

private:
    memptr _p;
    static memptr last_p;
    static memcso last_cso;

    void set_cso();
};

} // namespace c64

#endif /* C64_MEMPTR__H */
