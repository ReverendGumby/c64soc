#include "internal.h"
#include "cart_loader.h"

namespace c64 {
namespace cart_loader {

void cart_loader::init()
{
    reset();
}

void cart_loader::reset()
{
    C64BUS_RESETS &= ~C64BUS_RESETS_CART;
    C64BUS_RESETS |= C64BUS_RESETS_CART;
}

cart_loader::cart_loader(PCrt& crt)
    : _crt(crt)
{
    DBG_PRINTF(2, "\n");
}

cart_loader::~cart_loader()
{
    DBG_PRINTF(2, "\n");
}

void cart_loader::load()
{
}

void cart_loader::enable()
{
    C64BUS_CCSG0 |= C64BUS_CCSG0_ENABLE;
}    

} // namespace cart_loader
} // namespace c64
