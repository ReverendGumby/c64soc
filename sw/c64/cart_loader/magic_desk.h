#ifndef __CART_LOADER__MAGIC_DESK_H__
#define __CART_LOADER__MAGIC_DESK_H__

/* Loader for a "Magic Desk" cartridge (type 19)
 *
 * This cartridge type has 4 x 8K banks @ $8000-$9FFF and an I/O register.
 *
 *   $DE00[1:0] selects the bank
 *   $DE00[7] control GAME/EXROM: 1 = disable, 0 = enable
 */

#include <stdexcept>
#include <stdint.h>
#include "util/pollable.h"
#include "cart_loader.h"

namespace c64 {
namespace cart_loader {

class magic_desk : public cart_loader,
                   private pollable
{
public:
    magic_desk(PCrt&);
    virtual ~magic_desk();

    virtual void load();

private:
    using bus_state_t = uint32_t[6];
    bus_state_t last_bus_state;

    // Interface for pollable
    virtual const char* get_poll_name() const { return "c64::cart_loader::magic_desk"; }
    virtual void poll();
};

} // namespace cart_loader
} // namespace c64

#endif
