#include <stdint.h>
#include "internal.h"
#include "normal.h"

namespace c64 {
namespace cart_loader {

normal::normal(PCrt& p)
    : cart_loader(p)
{
}

void normal::load()
{
    DBG_PRINTF(1, "entry\n");
    auto& crt = *_crt;

    DBG_PRINTF(2, "nEXROM=%d nGAME=%d\n",
               !crt.exrom_asserted(), !crt.game_asserted());
    uint32_t ccsg1 = C64BUS_CCSG1;
    if (crt.exrom_asserted())
        ccsg1 |= C64BUS_CCSG1_NEXROM_STATIC;
    else
        ccsg1 &= ~C64BUS_CCSG1_NEXROM_STATIC;
    if (crt.game_asserted())
        ccsg1 |= C64BUS_CCSG1_NGAME_STATIC;
    else
        ccsg1 &= ~C64BUS_CCSG1_NGAME_STATIC;
    C64BUS_CCSG1 = ccsg1;

    bool bank_used[C64BUS_CART_BS_BANKS] = {};
    auto &chips = crt.chips();
    for (auto &c : chips)
    {
        unsigned bank = 0;
        unsigned num_banks = 0;
        if (c.len() == C64BUS_CART_BS_BANK_SIZE * 2) {
            // Special case: 16K chip spans two banks
            if (c.start() == 0x8000) {
                bank = 0;
                num_banks = 2;
            }
        } else {
            // 8K chip
            if (c.start() == ROML_ADDR && c.bank() < C64BUS_CART_BS_BANKS) {
                bank = c.bank();
                num_banks = 1;
            } else if (c.start() == ROMH_ADDR && c.bank() == 0) {
                bank = 1;
                num_banks = 1;
            }
        }
        if (num_banks == 0) {
            throw cart_loader_exception("unable to map chip to bank");
        }
        for (unsigned i = 0; i < num_banks; i++) {
            if (bank_used[bank + i]) {
                throw cart_loader_exception("bank already mapped");
            }
            bank_used[bank + i] = true;
        }
        auto bs = C64BUS_CART_BS_BANK(bank);
        DBG_PRINTF(2, "CHIP $%04X-$%04X bank %d => BS bank %d\n",
                   c.start(), c.start() + c.len() - 1, c.bank(), bank);

        // memcpy() might do 2-beat 32-bit writes using VSTM.64. The AXI
        // interconnect won't split them into single beats as required
        // by AXI4Lite, but will simply reject them.
        volatile uint32_t *dp = bs;
        const uint8_t *sp = c.data();
        size_t clen = c.len();
        for (size_t i = 0; i < clen / sizeof(*dp); i++) {
            *dp++ = uint32_t(sp[0]) << 0*8
                | uint32_t(sp[1]) << 1*8
                | uint32_t(sp[2]) << 2*8
                | uint32_t(sp[3]) << 3*8;
            sp += 4;
        }
    }

    enable();
}

} // namespace cart_loader
} // namespace c64
