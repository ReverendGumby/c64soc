#include <stdint.h>
#include <string.h>
#include "internal.h"
#include "magic_desk.h"

namespace c64 {
namespace cart_loader {

magic_desk::magic_desk(PCrt& p)
    : cart_loader(p)
{
}

magic_desk::~magic_desk()
{
    DBG_DO(3, pollable_remove(this));
}

void magic_desk::load()
{
    DBG_PRINTF(1, "entry\n");
    auto& crt = *_crt;

    DBG_PRINTF(2, "nEXROM=%d nGAME=%d\n",
               !crt.exrom_asserted(), !crt.game_asserted());
    uint32_t ccsg1 = C64BUS_CCSG1;
    ccsg1 |= C64BUS_CCSG1_DYNAMIC;
    if (crt.game_asserted())
        ccsg1 |= C64BUS_CCSG1_NGAME_STATIC;
    else
        ccsg1 &= ~C64BUS_CCSG1_NGAME_STATIC;
    C64BUS_CCSG1 = ccsg1;
    C64BUS_BSAG0 = C64BUS_BSAG0_DYNAMIC;
    C64BUS_VIOD0 = C64BUS_VIOD0_BS;
    C64BUS_VIOD1 = C64BUS_VIOD1_NEXROM_D7;

    bool bank_used[C64BUS_CART_BS_BANKS] = {};
    auto &chips = crt.chips();
    for (auto &c : chips)
    {
        unsigned bank = c.bank();
        if (bank_used[bank]) {
            throw cart_loader_exception("bank already mapped");
        }
        bank_used[bank] = true;
        if (bank >= C64BUS_CART_BS_BANKS)
            throw cart_loader_exception("out of banks");
        auto bs = C64BUS_CART_BS_BANK(bank);
        DBG_PRINTF(2, "CHIP $%04X-$%04X bank %d => BS bank %d\n",
                   c.start(), c.start() + c.len() - 1, c.bank(), bank);

        // memcpy() might do 2-beat 32-bit writes using VSTM.64. The AXI
        // interconnect won't split them into single beats as required
        // by AXI4Lite, but will simply reject them.
        volatile uint32_t *dp = bs;
        const uint8_t *sp = c.data();
        size_t clen = c.len();
        for (size_t i = 0; i < clen / sizeof(*dp); i++) {
            *dp++ = uint32_t(sp[0]) << 0*8
                | uint32_t(sp[1]) << 1*8
                | uint32_t(sp[2]) << 2*8
                | uint32_t(sp[3]) << 3*8;
            sp += 4;
        }
        bank++;
    }

    enable();

    DBG_DO(3, pollable_add(this));
}

void magic_desk::poll()
{
    bus_state_t new_bus_state = {
        C64BUS_CCSG0,
        C64BUS_CCSG1,
        C64BUS_BSAG0,
        C64BUS_BSAG1,
        C64BUS_VIOD0,
        C64BUS_VIOD1,
    };

    if (memcmp(new_bus_state, last_bus_state, sizeof(bus_state_t)) != 0) {
        DBG_PRINTF(3, "CCSG0=%08x CCSG1=%08x BSAG0=%08x BSAG1=%08x\n"
                   "VIOD0=%08x VIOD1=%08x\n",
                   (unsigned int)new_bus_state[0],
                   (unsigned int)new_bus_state[1],
                   (unsigned int)new_bus_state[2],
                   (unsigned int)new_bus_state[3],
                   (unsigned int)new_bus_state[4],
                   (unsigned int)new_bus_state[5]);
        memcpy(last_bus_state, new_bus_state, sizeof(bus_state_t));
    }
}

} // namespace cart_loader
} // namespace c64
