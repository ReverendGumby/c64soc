#ifndef __CART_LOADER__CART_LOADER_H__
#define __CART_LOADER__CART_LOADER_H__

#include <stdexcept>
#include <memory>
#include "../crt.h"

namespace c64 {
namespace cart_loader {

struct cart_loader_exception : public std::runtime_error
{
    cart_loader_exception(const char* what_arg) : std::runtime_error(what_arg) {}
};

class cart_loader
{
public:
    using PCrt = std::shared_ptr<Crt>;

    static void init();

    cart_loader(PCrt&);
    virtual ~cart_loader();

    virtual void load();

protected:
    static void reset();

    virtual void enable();

    PCrt _crt;
};

} // namespace cart_loader
} // namespace c64

#endif
