#ifndef __CART_LOADER__NORMAL_H__
#define __CART_LOADER__NORMAL_H__

/* Loader for a normal cartridge */

#include <stdexcept>
#include <stdint.h>
#include "cart_loader.h"

namespace c64 {
namespace cart_loader {

class normal : public cart_loader
{
public:
    normal(PCrt&);

    virtual void load();
};

} // namespace cart_loader
} // namespace c64

#endif
