#ifndef __CART_LOADER__INTERNAL_H__
#define __CART_LOADER__INTERNAL_H__

#include "util/debug.h"
#include "../c64bus.h"

#define ROML_ADDR       0x8000
#define ROMH_ADDR       0xA000

#define DBG_LVL 5
#define DBG_TAG "CLDR"

namespace c64 {
namespace cart_loader {


} // namespace cart_loader
} // namespace c64

#endif
