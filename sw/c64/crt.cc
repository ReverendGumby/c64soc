#include <string.h>
#include <stdio.h>
#include "crt.h"

namespace c64 {

Chip::Chip(const uint8_t *bin, size_t len)
{
	if (len < 0x10)
		throw Crt_format_error("invalid chip header size");

	_len = bin[0x0e] << 8 | bin[0x0f] << 0;
	if (_len != 0x2000 && _len != 0x4000)
		throw Crt_format_error("unsupported chip size");

	_bank = bin[0x0a] << 8 | bin[0x0b] << 0;
	_start = bin[0x0c] << 8 | bin[0x0d] << 0;
	_data = &bin[0x10];
}

Crt::Crt(Buf&& buf, size_t bin_len)
	: _bin(std::move(buf))
{
    static const uint8_t sig[] = "C64 CARTRIDGE   ";
    auto* bin = &_bin[0];
    const uint8_t *end = bin + bin_len;

    if (bin + 0x40 >= end)
        throw Crt_format_error("header too small");
    if (memcmp(sig, &bin[0x00], 0x10) != 0)
        throw Crt_format_error("bad sig");
    if (bin[0x10] != 0x00 ||
        bin[0x11] != 0x00 ||
        bin[0x12] != 0x00 ||
        bin[0x13] != 0x40 ||
        bin[0x14] != 0x01 ||
        bin[0x15] != 0x00)
        throw Crt_format_error("bad header");
    uint16_t hdr_hw_type = ((uint16_t)bin[0x16] << 8) | bin[0x17];

    _name = std::string((const char *)&bin[0x20], 0x20);
    auto i = _name.find_first_of('\0');
    if (i != std::string::npos)
        _name.resize(i);

    _hw_type = (hw_type_t)hdr_hw_type;
    _exrom = bin[0x18] != 0;
    _game = bin[0x19] != 0;

    uint32_t len = 0;
    for (bin += 0x40; bin + 0x08 < end; bin += len)
    {
        len =
            ((uint32_t)bin[0x04] << 8*3) |
            ((uint32_t)bin[0x05] << 8*2) |
            ((uint32_t)bin[0x06] << 8*1) |
            ((uint32_t)bin[0x07] << 8*0);
        std::string tag((const char *)&bin[0x00], 0x04);
        if (tag == "CHIP")
        {
            Chip c {bin, len};
            _chips.push_back(std::move(c));
        }
    }
}

Crt::~Crt()
{
}

} // namespace c64
