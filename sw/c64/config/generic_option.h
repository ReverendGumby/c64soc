#ifndef C64__CONFIG__GENERIC_OPTION__H
#define C64__CONFIG__GENERIC_OPTION__H

#include "option.h"

namespace c64 {
namespace config {

// generic_option: A configuration option that can represent any name and
// value. Chiefly used to hold an option for transport during
// (de)serialization.

class generic_option : public option
{
public:
    generic_option(const std::string& name, const std::string& value);

    const std::string& name() override;
    std::string get() override;
    void set(const std::string&) override;

private:
    const std::string _name;
    std::string _value;
};

} // namespace config
} // namespace c64

#endif /* C64__CONFIG__GENERIC_OPTION__H */
