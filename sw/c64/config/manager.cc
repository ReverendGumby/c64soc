#include <regex>
#include "util/debug.h"
#include "fat/fstream.h"
#include "manager.h"
#include "generic_option.h"

#define DBG_LVL 2
#define DBG_TAG "cfg_mgr"

namespace c64 {
namespace config {

manager::manager()
    : options{}
{
}

void manager::add(option_ptr&& o)
{
    options.push_back(std::move(o));
}

void manager::load(option_set& os, const fat::path& p)
{
    DBG_PRINTF(1, "\n");

    // Read options from the file.
    fat::fstream in{p};

    std::regex pat {R"((\S+) = (.*)$)"};

    for (std::string line; getline(in, line); ) {
        // Remove comments
        auto p = line.find('#');
        if (p != line.npos)
            line.erase(p);
        // Remove trailing whitespace
        line.erase(line.begin(),
                   std::find_if(line.begin(), line.end(),
                                [](char c) { return !std::isspace(c); }));

        // Find and add options.
        std::smatch m;
        if (std::regex_match(line, m, pat)) {
            const std::string& name = m[1];
            const std::string& value = m[2];
            DBG_PRINTF(2, "%s = %s\n", name.c_str(), value.c_str());
            os.push_back(option_ptr{new generic_option{name, value}});
        }
    }
}

void manager::apply(option_set& os)
{
    for (auto& new_o : os)
        for (auto& our_o : options) {
            const auto& name = our_o->name();
            if (name == new_o->name()) {
                auto val = new_o->get();
                DBG_PRINTF(2, "%s <= %s\n", name.c_str(), val.c_str());
                try {
                    our_o->set(val);
                }
                catch (c64::config::option_error& e) {
                    DBG_PRINTF(0, "Error setting %s <= %s: caught %s\n",
                               name.c_str(), val.c_str(), e.what());
                }
            }
        }
}

} // namespace config
} // namespace c64
