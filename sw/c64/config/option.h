#ifndef C64__CONFIG__OPTION__H
#define C64__CONFIG__OPTION__H

#include <string>
#include <stdexcept>

namespace c64 {
namespace config {

// option: A configuration option.
//
// All options have a unique name. That name will be stored to (serialized) and
// read from file (deserialized).
//
// An option has a value. Derivative classes will encapsulate their value in a
// type-safe manner.

class option
{
public:
    // Return the option's unique name.
    virtual const std::string& name() = 0;

    // Get this option's value.
    virtual std::string get() = 0;

    // Set this option's value. May throw option_error.
    virtual void set(const std::string&) = 0;
};

class option_error : public std::runtime_error
{
public:
    option_error(const char* what_arg) : std::runtime_error(what_arg) {}
};

} // namespace config
} // namespace c64

#endif /* C64__CONFIG__OPTION__H */
