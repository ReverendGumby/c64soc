#ifndef C64__CONFIG__MANAGER__H
#define C64__CONFIG__MANAGER__H

#include <vector>
#include "fat/path.h"
#include "option.h"
#include "option_set.h"

namespace c64 {
namespace config {

// manager: Tracks configuration options and the objects that implement them.

class manager
{
public:
    manager();

    void add(option_ptr&&);

    void load(option_set&, const fat::path&);

    void apply(option_set&);

private:
    option_set options;
};

} // namespace config
} // namespace c64

#endif /* C64__CONFIG__MANAGER__H */
