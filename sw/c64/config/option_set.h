#ifndef C64__CONFIG__OPTION_SET__H
#define C64__CONFIG__OPTION_SET__H

#include <memory>
#include <vector>

namespace c64 {
namespace config {

// set: A collection of configuration options.

class option;

using option_ptr = std::unique_ptr<option>;
using option_set = std::vector<option_ptr>;

} // namespace config
} // namespace c64

#endif /* C64__CONFIG__OPTION_SET__H */
