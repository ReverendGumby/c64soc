#include "generic_option.h"

namespace c64 {
namespace config {

generic_option::generic_option(const std::string& name, const std::string& value)
    : _name(name), _value(value)
{
}

const std::string& generic_option::name()
{
    return _name;
}

std::string generic_option::get()
{
    return _value;
}

void generic_option::set(const std::string& new_value)
{
    _value = new_value;
}

} // namespace config
} // namespace c64
