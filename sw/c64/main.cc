/*
 * main.cc: main application for C64
*/

#include <memory>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <unistd.h>
#include "plat/audio.h"
#include "plat/manager.h"
#include "c64/sys.h"
#include "c64/monitor/monitor.h"
#include "sd/sd_manager.h"
#include "usb/usb_manager.h"
#include "ui/kbd.h"
#include "c64/screen_manager.h"
#include "ui/view_manager.h"
#include "util/pollable.h"
#include "util/kbd_shortcut.h"
#include "cli/manager.h"
#include "main_menu/main_manager.h"
#include "app/version.h"

static const plat::audio_config_t audio_cfg = {
    .sample_rate = 48000,
};
static const plat::manager::config_t plat_cfg = {
    .audio = audio_cfg,
};

int main()
{
    plat::manager plm(plat_cfg);
    plm.early_init();

    printf("c64soc\n"
           "SW version %s (@%s) build %s\n",
           sw_build_branch, sw_build_rev, sw_build_timestamp);

    plm.late_init();

    auto hw_ver = plm.get_rtl_version();
    printf("HW version %s (@%s) build %s\n",
           hw_ver.git_branch.c_str(), hw_ver.git_rev.c_str(), hw_ver.timestamp.c_str());

    usb::usb_manager um;
    um.init();

    sd::sd_manager sm;

    ui::hid_manager hid_mgr{};
    hid_mgr.listen(um);

    c64::sys sys(plm, hid_mgr);
    sys.init();

    c64::screen_manager screen_mgr(sys);
    ui::view_manager view_mgr(hid_mgr, screen_mgr);

    cli::manager clim;
    clim.start();

    main_menu::main_manager mm(sys, sm, um);
    kbd_shortcut mm_sc(ui::kbd::kbcode::F12, [&mm]() { mm._top_menu.show(); } );
    hid_mgr.add_kbd_listener(&mm_sc);

    c64::monitor::monitor mon(sys);
    kbd_shortcut mon_sc(ui::kbd::kbcode::PSR, [&mon]() { mon.halt(); } );
    hid_mgr.add_kbd_listener(&mon_sc);

    plm.reset_video(false);
    plm.audio_up();
    sys.reset_all(false);

    printf("Polling...\n");
    //unsigned loops = 0;
    for (;;) {
    	//printf("Polling %u\n\r", loops++);
        poll_manager_get().poll();
        //(uint8_t)*c64::memptr{0x0000, c64::memtype::undef}; // XXX testing VIC corruption by HOLD
        plm.set_video_hdmi_mode(plm.get_sw(0)); // XXX
        pthread_yield();
    }

    return 0;
}
