#include "screen_manager.h"
#include "ui/fullscreen_view.h"

namespace c64 {

screen_manager::screen_manager(c64::sys& sys)
    : sys(sys)
{
    ui::fullscreen_view::set_size(screen::WIDTH, screen::HEIGHT);
}

ui::screen* screen_manager::grab_screen()
{
    sys.kbd.stop();
    sys.joy.stop();
    sys.pause();

    return sys.vid.get_screen();
}

void screen_manager::release_screen()
{
    sys.resume();
    sys.kbd.start();
    sys.joy.start();
}

} // namespace ui
