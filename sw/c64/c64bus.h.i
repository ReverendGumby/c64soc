#ifndef __C64BUS_H
#define __C64BUS_H

#include <stdint.h>
#include "xparameters.h"

namespace c64 {

//``PERIPHERAL C64BUS

//////////////////////////////////////////////////////////////////////
// Bus control interface

//``BASE 32 CTRL C64BUS_CTRL

//``REGS s/_(\w+)_\1/_\1/ s/ICD_CTRL/ICD/

// Bits for C64BUS_CPU_REG_DOUT

//``SUBREG_FIELDS CPU_REG_SEL s/SEL_(\w+)_/\1_/

// Bits for C64BUS_ICD_MATCH_DIN, .._DEN, and .._DOUT
//``FIELDS ICD_MATCH_D

//////////////////////////////////////////////////////////////////////
// C64 native memory bus interface

//``BASE 8 C64_MEM

#define C64BUS_C64_MEM          (& C64BUS_C64_MEM_REG(0))

//////////////////////////////////////////////////////////////////////
// ROM cartridge control interface

//``BASE 32 CART_CTRL
//``REGS s/(\w+)(.)_\1/\1\2/ s/_VIOD0_BSA/_VIOD0_BS_BA/

//////////////////////////////////////////////////////////////////////
// ROM cartridge backing store interface

//``BASE 32 CART_BS

#define C64BUS_CART_BS_BANK_SIZE    0x2000
#define C64BUS_CART_BS_BANK(n)      (& C64BUS_CART_BS_REG(C64BUS_CART_BS_BANK_SIZE * (n)))
#define C64BUS_CART_BS_BANKS        4

//////////////////////////////////////////////////////////////////////

} // namespace c64

#endif
