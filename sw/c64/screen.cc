#include "c64bus.h"
#include "vid.h"
#include "memptr.h"
#include "util/debug.h"
#include "hwregs.h"
#include "screen.h"

#define DBG_LVL 1
#define DBG_TAG "vid"

namespace c64 {

// Translate from ASCII to screen code.
uint8_t screen::ascii_to_screen(char c)
{
    constexpr uint8_t unk = 32;

    // Use space (' ') for untranslated ASCII chars. Coincidentally, the
    // screen code and ASCII values for space are identical.
    constexpr uint8_t screen[128] = {
        unk, unk, unk, unk, unk, unk, unk, unk, //  0 -  7
        unk, unk, unk, unk, unk, unk, unk, unk, //  8 - 15
        unk, unk, unk, unk, unk, unk, unk, unk, // 16 - 23
        unk, unk, unk, unk, unk, unk, unk, unk, // 24 - 31
        32,  33,  34,  35,  36,  37,  38,  39,  //  !"#$%&'
        40,  41,  42,  43,  44,  45,  46,  47,  // ()*+,-./
        48,  49,  50,  51,  52,  53,  54,  55,  // 01234567
        56,  57,  58,  59,  60,  61,  62,  63,  // 89:;<=>?
        0,   65,  66,  67,  68,  69,  70,  71,  // @ABCDEFG
        72,  73,  74,  75,  76,  77,  78,  79,  // HIJKLMNO
        80,  81,  82,  83,  84,  85,  86,  87,  // PQRSTUVW
        88,  89,  90,  27,  127, 29,  30,  100, // XYZ[\]^_
        39,  1,   2,   3,   4,   5,   6,   7,   // `abcdefg
        8,   9,   10,  11,  12,  13,  14,  15,  // hijklmno
        16,  17,  18,  19,  20,  21,  22,  23,  // pqrstuvw
        24,  25,  26,  27,  93,  29,  unk, unk, // xyz{|}~
    };

    if (c >= 0 && c < (0 + sizeof(screen)))
        return screen[c - 0];
    return unk;
}

screen::screen(class vid& vid)
    : v(vid), addrs(ss.a)
{
    ss = v.save_state();

    init();
}

screen::~screen()
{
    v.restore_state(ss);
}

void screen::set_bg(screen::color_t c)
{
    VIC[VIC_B0C] = static_cast<uint8_t>(c);
}

void screen::set_border(screen::color_t c)
{
    VIC[VIC_EC] = static_cast<uint8_t>(c);
}

void screen::init()
{
    uint8_t cr1 = VIC[VIC_CR1];
    uint8_t cr2 = VIC[VIC_CR2];
    uint8_t mp = VIC[VIC_MP];

    // Standard text mode, 40x25, uppercase chars
    cr1 &= ~(CR1_ECM | CR1_BMM | CR1_YSCROLL);
    cr1 |= CR1_DEN | CR1_RSEL | CR1_YSCROLL_25;
    cr2 &= ~(CR2_RES | CR2_MCM | CR2_XSCROLL);
    cr2 |= CR2_CSEL | CR2_XSCROLL_40;
    mp &= ~MP_CB;
    mp |= MP_CB_LOWER;

    VIC[VIC_CR1] = cr1;
    VIC[VIC_CR2] = cr2;
    VIC[VIC_MP] = mp;
    VIC[VIC_MXE] = 0;                           // disable sprites
}

void screen::clear(screen::color_t c)
{
    for (int y = 0; y < HEIGHT; y++)
        for (int x = 0; x < WIDTH; x++)
            write(x, y, 0, c);
}

void screen::write(int x, int y, uint8_t b, screen::color_t c)
{
    auto gfxmem = addrs.gfxmem_base;
    gfxmem[y * WIDTH + x] = b;
    CLRMEM[y * WIDTH + x] = static_cast<uint8_t>(c);
}

void screen::invert_rect(int x0, int y0, int w, int h)
{
    auto gfxmem = addrs.gfxmem_base;

    for (int y = y0; y < y0 + h; y++)
        for (int x = x0; x < x0 + w; x++)
            gfxmem[y * WIDTH + x] ^= (1<<7);
}

} // namespace c64
