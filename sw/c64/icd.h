#ifndef C64__ICD__H
#define C64__ICD__H

#include <stdint.h>
#include "icd_match.h"
#include "monitor/mos6502/cpu_regs.h"

namespace c64 {

class icd
{
public:
    using cpu_regs = ::monitor::mos6502::cpu_regs;

    void init();
    void reset();

    cpu_regs read_cpu_regs();

    void force_halt();
    void resume();

    icd_match match;

private:
    uint8_t read_cpu_reg(uint8_t reg);
};

} // namespace c64

#endif /* C64__ICD__H */
