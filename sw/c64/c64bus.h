// Auto-generated: Do not edit.
// /Applications/Xcode.app/Contents/Developer/usr/bin/python3 tools/gen_regs_h.py sw/c64/c64bus.h.i sw/c64/c64bus.h rtl/c64/c64bus.v rtl/c64/cart.v rtl/c64/icd.v rtl/mos6510/mos6510.v

#ifndef __C64BUS_H
#define __C64BUS_H

#include <stdint.h>
#include "xparameters.h"

namespace c64 {

#define C64BUS_BASE             XPAR_C64BUS_BASEADDR

//////////////////////////////////////////////////////////////////////
// Bus control interface

#define C64BUS_CTRL_BASE        (C64BUS_BASE + 0x00000)
#define C64BUS_CTRL_REG(off)    (*((volatile uint32_t *)(C64BUS_CTRL_BASE + (off))))

#define C64BUS_ID                   C64BUS_CTRL_REG(0x000)
#define C64BUS_ID_DATA                  0x0C640C64UL

#define C64BUS_KBD_MATRIX_0         C64BUS_CTRL_REG(0x040)

#define C64BUS_KBD_MATRIX_1         C64BUS_CTRL_REG(0x044)

#define C64BUS_JOY_MATRIX           C64BUS_CTRL_REG(0x048)

#define C64BUS_MISC_KEYS            C64BUS_CTRL_REG(0x04C)
#define C64BUS_MISC_KEYS_RESTORE        (1<<0)

#define C64BUS_RESETS               C64BUS_CTRL_REG(0x050)
#define C64BUS_RESETS_C64               (1<<0)
#define C64BUS_RESETS_CART              (1<<2)

#define C64BUS_CSO                  C64BUS_CTRL_REG(0x058)
#define C64BUS_CSO_CHAREN               (1<<0)
#define C64BUS_CSO_HIRAM                (1<<1)
#define C64BUS_CSO_LORAM                (1<<2)
#define C64BUS_CSO_GAME                 (1<<3)
#define C64BUS_CSO_EXROM                (1<<4)
#define C64BUS_CSO_ENABLE               (1<<15)
#define C64BUS_CSO_MON_CHAREN           (1<<16)
#define C64BUS_CSO_MON_HIRAM            (1<<17)
#define C64BUS_CSO_MON_LORAM            (1<<18)
#define C64BUS_CSO_MON_GAME             (1<<19)
#define C64BUS_CSO_MON_EXROM            (1<<20)

#define C64BUS_CPU_REG_SEL          C64BUS_CTRL_REG(0x060)
#define C64BUS_CPU_REG_SEL_PCL          0x1
#define C64BUS_CPU_REG_SEL_PCH          0x2
#define C64BUS_CPU_REG_SEL_PF           0x3     // Processor Flags
#define C64BUS_CPU_REG_SEL_S            0x4
#define C64BUS_CPU_REG_SEL_AC           0x5
#define C64BUS_CPU_REG_SEL_X            0x6
#define C64BUS_CPU_REG_SEL_Y            0x7
#define C64BUS_CPU_REG_SEL_IR           0x8
#define C64BUS_CPU_REG_SEL_ABL          0x9
#define C64BUS_CPU_REG_SEL_ABH          0xA
#define C64BUS_CPU_REG_SEL_DOR          0xB
#define C64BUS_CPU_REG_SEL_TS           0xC     // Timing State
#define C64BUS_CPU_REG_SEL_IS           0xD     // Interrupt State
#define C64BUS_CPU_REG_SEL_PS           0xE     // Pin State

#define C64BUS_CPU_REG_DOUT         C64BUS_CTRL_REG(0x064)

#define C64BUS_ICD_CTRL             C64BUS_CTRL_REG(0x070)
#define C64BUS_ICD_CPU_ENABLE           (1<<0)
#define C64BUS_ICD_CPU_FORCE_HALT       (1<<1)
#define C64BUS_ICD_CPU_RESUME           (1<<2)
#define C64BUS_ICD_CPU_CLR_RDY          (1<<8)
#define C64BUS_ICD_MATCH_ENABLE         (1<<16)
#define C64BUS_ICD_MATCH_TRIGGER        (1<<24)

#define C64BUS_ICD_MATCH_DIN        C64BUS_CTRL_REG(0x074)

#define C64BUS_ICD_MATCH_DEN        C64BUS_CTRL_REG(0x078)

#define C64BUS_ICD_MATCH_DOUT       C64BUS_CTRL_REG(0x07C)

#define C64BUS_GPO                  C64BUS_CTRL_REG(0x080)

// Bits for C64BUS_CPU_REG_DOUT

// Processor Flags
#define C64BUS_CPU_REG_PF_C             (1<<0)
#define C64BUS_CPU_REG_PF_Z             (1<<1)
#define C64BUS_CPU_REG_PF_I             (1<<2)
#define C64BUS_CPU_REG_PF_D             (1<<3)
#define C64BUS_CPU_REG_PF_B             (1<<4)
#define C64BUS_CPU_REG_PF_V             (1<<6)
#define C64BUS_CPU_REG_PF_N             (1<<7)

// Timing State
#define C64BUS_CPU_REG_TS_CLOCK1        (1<<0)
#define C64BUS_CPU_REG_TS_CLOCK2        (1<<1)
#define C64BUS_CPU_REG_TS_T2            (1<<2)
#define C64BUS_CPU_REG_TS_T3            (1<<3)
#define C64BUS_CPU_REG_TS_T4            (1<<4)
#define C64BUS_CPU_REG_TS_T5            (1<<5)

// Interrupt State
#define C64BUS_CPU_REG_IS_RESP          (1<<0)
#define C64BUS_CPU_REG_IS_RESG          (1<<1)
#define C64BUS_CPU_REG_IS_NMIP          (1<<2)
#define C64BUS_CPU_REG_IS_NMIG          (1<<3)
#define C64BUS_CPU_REG_IS_IRQP          (1<<4)

// Pin State
#define C64BUS_CPU_REG_PS_RDY           (1<<0)
#define C64BUS_CPU_REG_PS_RW            (1<<1)
#define C64BUS_CPU_REG_PS_AEC           (1<<2)
#define C64BUS_CPU_REG_PS_SYNC          (1<<3)
#define C64BUS_CPU_REG_PS_SO            (1<<4)

// Bits for C64BUS_ICD_MATCH_DIN, .._DEN, and .._DOUT
#define C64BUS_ICD_MATCH_D_A            (65535<<0)
#define C64BUS_ICD_MATCH_D_A_S          0
#define C64BUS_ICD_MATCH_D_DB           (255<<16)
#define C64BUS_ICD_MATCH_D_DB_S         16
#define C64BUS_ICD_MATCH_D_RW           (1<<24)
#define C64BUS_ICD_MATCH_D_SYNC         (1<<25)

//////////////////////////////////////////////////////////////////////
// C64 native memory bus interface

#define C64BUS_C64_MEM_BASE     (C64BUS_BASE + 0x10000)
#define C64BUS_C64_MEM_REG(off) (*((volatile uint8_t *)(C64BUS_C64_MEM_BASE + (off))))

#define C64BUS_C64_MEM          (& C64BUS_C64_MEM_REG(0))

//////////////////////////////////////////////////////////////////////
// ROM cartridge control interface

#define C64BUS_CART_CTRL_BASE   (C64BUS_BASE + 0x01000)
#define C64BUS_CART_CTRL_REG(off) (*((volatile uint32_t *)(C64BUS_CART_CTRL_BASE + (off))))
#define C64BUS_CCSG0                C64BUS_CART_CTRL_REG(0x000)
#define C64BUS_CCSG0_ENABLE             (1<<0)

#define C64BUS_CCSG1                C64BUS_CART_CTRL_REG(0x004)
#define C64BUS_CCSG1_NGAME_STATIC       (1<<0)
#define C64BUS_CCSG1_NEXROM_STATIC      (1<<1)
#define C64BUS_CCSG1_DYNAMIC            (1<<4)
#define C64BUS_CCSG1_NGAME              (1<<16)
#define C64BUS_CCSG1_NEXROM             (1<<17)

#define C64BUS_BSAG0                C64BUS_CART_CTRL_REG(0x008)
#define C64BUS_BSAG0_DYNAMIC            (1<<0)

#define C64BUS_BSAG1                C64BUS_CART_CTRL_REG(0x00C)
#define C64BUS_BSAG1_BA_ROML            (15<<0)
#define C64BUS_BSAG1_BA_ROML_S          0
#define C64BUS_BSAG1_BA_ROMH            (15<<4)
#define C64BUS_BSAG1_BA_ROMH_S          4

#define C64BUS_VIOD0                C64BUS_CART_CTRL_REG(0x010)
#define C64BUS_VIOD0_BS                 (1<<0)
#define C64BUS_VIOD0_BS_BAL             (15<<16)
#define C64BUS_VIOD0_BS_BAL_S           16
#define C64BUS_VIOD0_BS_BAH             (15<<20)
#define C64BUS_VIOD0_BS_BAH_S           20

#define C64BUS_VIOD1                C64BUS_CART_CTRL_REG(0x014)
#define C64BUS_VIOD1_NEXROM_D7          (1<<0)
#define C64BUS_VIOD1_NEXROM             (1<<3)

//////////////////////////////////////////////////////////////////////
// ROM cartridge backing store interface

#define C64BUS_CART_BS_BASE     (C64BUS_BASE + 0x20000)
#define C64BUS_CART_BS_REG(off) (*((volatile uint32_t *)(C64BUS_CART_BS_BASE + (off))))

#define C64BUS_CART_BS_BANK_SIZE    0x2000
#define C64BUS_CART_BS_BANK(n)      (& C64BUS_CART_BS_REG(C64BUS_CART_BS_BANK_SIZE * (n)))
#define C64BUS_CART_BS_BANKS        4

//////////////////////////////////////////////////////////////////////

} // namespace c64

#endif
