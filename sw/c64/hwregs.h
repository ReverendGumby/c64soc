// C64 hardware register definitions

#ifndef C64_HWREGS__H
#define C64_HWREGS__H

#include <stdint.h>
#include "memptr.h"

namespace c64 {

//////////////////////////////////////////////////////////////////////
// $D000-$D3FF: VIC (Video Controller)
constexpr memptr VIC{0xD000, memtype::io};

constexpr int VIC_M0X  = 0x00;                  // X coordinate sprite 0
constexpr int VIC_M0Y  = 0x01;                  // Y coordinate sprite 0
constexpr int VIC_M1X  = 0x02;                  // X coordinate sprite 1
constexpr int VIC_M1Y  = 0x03;                  // Y coordinate sprite 1
constexpr int VIC_M2X  = 0x04;                  // X coordinate sprite 2
constexpr int VIC_M2Y  = 0x05;                  // Y coordinate sprite 2
constexpr int VIC_M3X  = 0x06;                  // X coordinate sprite 3
constexpr int VIC_M3Y  = 0x07;                  // Y coordinate sprite 3
constexpr int VIC_M4X  = 0x08;                  // X coordinate sprite 4
constexpr int VIC_M4Y  = 0x09;                  // Y coordinate sprite 4
constexpr int VIC_M5X  = 0x0A;                  // X coordinate sprite 5
constexpr int VIC_M5Y  = 0x0B;                  // Y coordinate sprite 5
constexpr int VIC_M6X  = 0x0C;                  // X coordinate sprite 6
constexpr int VIC_M6Y  = 0x0D;                  // Y coordinate sprite 6
constexpr int VIC_M7X  = 0x0E;                  // X coordinate sprite 7
constexpr int VIC_M7Y  = 0x0F;                  // Y coordinate sprite 7
constexpr int VIC_MXX8 = 0x10;                  // MSBs of X coordinates
constexpr int VIC_CR1  = 0x11;                  // Control register 1
constexpr int VIC_RC   = 0x12;                  // Raster counter
constexpr int VIC_LPX  = 0x13;                  // Light pen X
constexpr int VIC_LPY  = 0x14;                  // Light pen Y
constexpr int VIC_MXE  = 0x15;                  // Sprite enabled
constexpr int VIC_CR2  = 0x16;                  // Control register 2
constexpr int VIC_MXYE = 0x17;                  // Sprite Y expansion
constexpr int VIC_MP   = 0x18;                  // Memory Control register
constexpr int VIC_IR   = 0x19;                  // Interrupt register
constexpr int VIC_IE   = 0x1a;                  // Interrupt enabled
constexpr int VIC_MXDP = 0x1b;                  // Sprite data priority
constexpr int VIC_MXMC = 0x1c;                  // Sprite multicolor
constexpr int VIC_MXXE = 0x1d;                  // Sprite X expansion
constexpr int VIC_MXM  = 0x1e;                  // Sprite-sprite collision
constexpr int VIC_MXD  = 0x1f;                  // Sprite-data collision
constexpr int VIC_EC   = 0x20;                  // Border color
constexpr int VIC_B0C  = 0x21;                  // Background color 0 
constexpr int VIC_B1C  = 0x22;                  // Background color 1
constexpr int VIC_B2C  = 0x23;                  // Background color 2
constexpr int VIC_B3C  = 0x24;                  // Background color 3
constexpr int VIC_MM0  = 0x25;                  // Sprite multicolor 0
constexpr int VIC_MM1  = 0x26;                  // Sprite multicolor 1
constexpr int VIC_M0C  = 0x27;                  // Color sprite 0
constexpr int VIC_M1C  = 0x28;                  // Color sprite 1
constexpr int VIC_M2C  = 0x29;                  // Color sprite 2
constexpr int VIC_M3C  = 0x2A;                  // Color sprite 3
constexpr int VIC_M4C  = 0x2B;                  // Color sprite 4
constexpr int VIC_M5C  = 0x2C;                  // Color sprite 5
constexpr int VIC_M6C  = 0x2D;                  // Color sprite 6
constexpr int VIC_M7C  = 0x2E;                  // Color sprite 7

constexpr int VIC_MXX(int mob) { return VIC_M0X + mob * 2; }
constexpr int VIC_MXY(int mob) { return VIC_M0Y + mob * 2; }
constexpr int VIC_MXC(int mob) { return VIC_M0C + mob; }

// Control Register 1
constexpr uint8_t CR1_RC8 = 1<<7;               // Raster Compare: (Bit 8)
constexpr uint8_t CR1_ECM = 1<<6;               // Extended Color Text Mode
constexpr uint8_t CR1_BMM = 1<<5;               // Bit Map Mode
constexpr uint8_t CR1_DEN = 1<<4;               // Data Enable
constexpr uint8_t CR1_RSEL = 1<<3;              // Row Select: 0=24, 1=25
constexpr uint8_t CR1_YSCROLL = 7<<0;           // Smooth Scroll to Y pos
constexpr uint8_t CR1_YSCROLL_25 = 3<<0;
constexpr uint8_t CR1_YSCROLL_S = 0;

// Control Register 2
constexpr uint8_t CR2_RES = 1<<5;               // Reset: 0=normal
constexpr uint8_t CR2_MCM = 1<<4;               // Multi-Color Mode
constexpr uint8_t CR2_CSEL = 1<<3;              // Column Select: 0=38, 1=40
constexpr uint8_t CR2_XSCROLL = 7<<0;           // Smooth Scroll to X pos
constexpr uint8_t CR2_XSCROLL_40 = 0<<0;
constexpr uint8_t CR2_XSCROLL_S = 0;

// Memory Control register
constexpr uint8_t MP_VM = 15<<4;                // Video Mem. Base VM[13:10]
constexpr uint8_t MP_VM_DEFAULT = 1<<4;         // $0400
constexpr uint8_t MP_VM_S = 4;
constexpr uint8_t MP_CB = 7<<1;                 // Char. ROM Base CB[13:11]
constexpr uint8_t MP_CB_UPPER = 2<<1;           // uppercase chars
constexpr uint8_t MP_CB_LOWER = 3<<1;           // lowercase chars
constexpr uint8_t MP_CB_S = 1;

// Interrupt Register / Interrupt Enable
constexpr uint8_t IR_IRQ = 1<<7;                // nIRQ asserted (IR only)
constexpr uint8_t IR_LP = 1<<3;                 // light pen
constexpr uint8_t IR_MMC = 1<<2;                // MOB/MOB collision
constexpr uint8_t IR_MBC = 1<<1;                // MOB/BG collision
constexpr uint8_t IR_RST = 1<<0;                // raster counter match

//////////////////////////////////////////////////////////////////////
// $D400-$D7FF: SID (Sound Synthesizer)
constexpr memptr SID{0xD400, memtype::io};

// Voice 1
constexpr uint8_t SID_F1L = 0x00;               // Frequency Control - Low
constexpr uint8_t SID_F1H = 0x01;               // Frequency Control - High
constexpr uint8_t SID_PW1L = 0x02;              // Pulse Waveform Width - Low
constexpr uint8_t SID_PW1H = 0x03;              // Pulse Waveform Width - High
constexpr uint8_t SID_CR1 = 0x04;               // Control Register
constexpr uint8_t SID_AD1 = 0x05;               // Env. Gen.: Attack/Decay
constexpr uint8_t SID_SR1 = 0x06;               // Env. Gen.: Sustain/Release
// Voice 2
constexpr uint8_t SID_F2L = 0x07;
constexpr uint8_t SID_F2H = 0x08;
constexpr uint8_t SID_PW2L = 0x09;
constexpr uint8_t SID_PW2H = 0x0A;
constexpr uint8_t SID_CR2 = 0x0B;
constexpr uint8_t SID_AD2 = 0x0C;
constexpr uint8_t SID_SR2 = 0x0D;
// Voice 3
constexpr uint8_t SID_F3L = 0x0E;
constexpr uint8_t SID_F3H = 0x0F;
constexpr uint8_t SID_PW3L = 0x10;
constexpr uint8_t SID_PW3H = 0x11;
constexpr uint8_t SID_CR3 = 0x12;
constexpr uint8_t SID_AD3 = 0x13;
constexpr uint8_t SID_SR3 = 0x14;
// Filter
constexpr uint8_t SID_FCL = 0x15;               // Filter Cutoff Frequency - Low
constexpr uint8_t SID_FCH = 0x16;               // Filter Cutoff Frequency - High
constexpr uint8_t SID_FCR1 = 0x17;              // Filter Reson. / Voice Input
constexpr uint8_t SID_FCR2 = 0x18;              // Filter Mode, Volume
// Misc
constexpr uint8_t SID_POTX = 0x19;              // ADC: Game Paddle 1
constexpr uint8_t SID_POTY = 0x1A;              // ADC: Game Paddle 2
constexpr uint8_t SID_OSC3 = 0x1B;              // Oscillator 3 Random Number Gen.
constexpr uint8_t SID_ENV3 = 0x1C;              // Env. Gen. 3 Output

// Pulse Waveform Width
constexpr uint8_t SID_PWL = 0xFF;
constexpr uint8_t SID_PWH = 0xFF;

// Control Register
constexpr uint8_t SID_CR_WNOI = 1<<7;           // Random Noise Waveform
constexpr uint8_t SID_CR_WPUL = 1<<6;           // Pulse Waveform
constexpr uint8_t SID_CR_WSAW = 1<<5;           // Sawtooth Waveform
constexpr uint8_t SID_CR_WTRI = 1<<4;           // Triangle Waveform
constexpr uint8_t SID_CR_TEST = 1<<3;           // Test Bit: 1 = Disable Osc.
constexpr uint8_t SID_CR_RING = 1<<2;           // Ring Module Osc.N w/ N-1 Out
constexpr uint8_t SID_CR_SYNC = 1<<1;           // Synchronize Osc.N w/ N-1 Freq.
constexpr uint8_t SID_CR_GATE = 1<<0;           // Gate Bit

// Envelope Generator
constexpr uint8_t SID_AD_ATK = 0xF0;            // Select Attack Cycle Duration
constexpr uint8_t SID_AD_ATK_S = 4;
constexpr uint8_t SID_AD_DCY = 0x0F;            // Select Decay Cycle Duration
constexpr uint8_t SID_AD_DCY_S = 0;
constexpr uint8_t SID_SR_STN = 0xF0;            // Select Sustain Cycle Duration
constexpr uint8_t SID_SR_STN_S = 4;
constexpr uint8_t SID_SR_RLS = 0x0F;            // Select Release Cycle Duration
constexpr uint8_t SID_SR_RLS_S = 0;

// Filter
constexpr uint8_t SID_FCR1_RES = 0xF0;          // Select Filter Resonance
constexpr uint8_t SID_FCR1_RES_S = 4;
constexpr uint8_t SID_FCR1_EX = 1<<3;           // Filter External Input
constexpr uint8_t SID_FCR1_F3 = 1<<2;           // Filter Voice 3 Output
constexpr uint8_t SID_FCR1_F2 = 1<<1;           // Filter Voice 2 Output
constexpr uint8_t SID_FCR1_F1 = 1<<0;           // Filter Voice 1 Output
constexpr uint8_t SID_FCR2_C3OFF = 1<<7;        // Cut-Off Voice 3 Output: 0=On
constexpr uint8_t SID_FCR2_HP = 1<<6;           // Select Filter High-Pass Mode
constexpr uint8_t SID_FCR2_BP = 1<<5;           // Select Filter Band-Pass Mode
constexpr uint8_t SID_FCR2_LP = 1<<4;           // Select Filter Low-Pass Mode
constexpr uint8_t SID_FCR2_VOL = 0x0F;          // Select Output Volume
constexpr uint8_t SID_FCR2_VOL_S = 0;

//////////////////////////////////////////////////////////////////////
// $D800-$D8FF: Color RAM
constexpr memptr CLRMEM{0xD800, memtype::io};

//////////////////////////////////////////////////////////////////////
// $DD00-$DDFF: CIA1 (Keyboard)
constexpr memptr CIA1{0xDC00, memtype::io};

constexpr int CIA_PA    = 0x00;                 // Port A input(R) / output(W)
constexpr int CIA_PB    = 0x01;                 // Port B input(R) / output(W)
constexpr int CIA_DDRA  = 0x02;                 // Port A direction (1 = output)
constexpr int CIA_DDRB  = 0x03;                 // Port B direction

//////////////////////////////////////////////////////////////////////
// $DD00-$DDFF: CIA2 (Serial Bus, User Port/RS-232))
constexpr memptr CIA2{0xDD00, memtype::io};

} // namespace c64

#endif /* C64_HWREGS__H */
