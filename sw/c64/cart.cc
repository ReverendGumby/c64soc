#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "util/error.h"
#include "util/debug.h"
#include "cart.h"

#include "cart_loader/normal.h"
#include "cart_loader/magic_desk.h"

#define DBG_LVL 1
#define DBG_TAG "CART"

namespace c64 {

void cart::init()
{
    DBG_PRINTF(1, "entry\n");

    _cldr = nullptr;
    Cldr::init();
}

cart::Cldr* cart::new_loader()
{
    switch (_crt->hw_type()) {
    case Crt::NORMAL:
        return new cart_loader::normal{_crt};
    case Crt::MAGIC_DESK:
        return new cart_loader::magic_desk{_crt};
        //case Crt::EASYFLASH:
        //return new cart_loader::easyflash{pcrt};
        //break;
    default:
        break;
    }
    throw cart_exception("unhandled crt hw_type");    
}

void cart::load(PCrt pcrt)
{
    DBG_PRINTF(1, "entry\n");
    _crt = pcrt;

    _cldr = decltype(_cldr) { new_loader() };

    _cldr->init();
    _cldr->load();
}

} // namespace c64
