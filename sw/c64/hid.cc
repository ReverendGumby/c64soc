#include "hid.h"

namespace c64 {

hid::hid(ui::hid_manager& hm)
    : hid_mgr(hm)
{
    started = false;
    resume = false;
}

void hid::start()
{
    resume = true;
    start_on_idle();
}

void hid::stop()
{
    started = false;
    resume = false;
    reset_c64bus_matrix();
}

void hid::start_on_idle()
{
    if (is_idle()) {
        started = true;
        resume = false;
    }
}

void hid::on_event()
{
    if (resume)
        start_on_idle();
}

} // namespace c64
