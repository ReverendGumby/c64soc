#ifndef C64__SCREEN_MANAGER__H
#define C64__SCREEN_MANAGER__H

#include "c64/sys.h"
#include "c64/screen.h"
#include "ui/screen_manager.h"

namespace c64 {

class screen_manager : public ui::screen_manager
{
public:
    screen_manager(c64::sys&);

private:
    ui::screen* grab_screen();
    void release_screen();

    c64::sys& sys;
};

} // namespace c64

#endif /* C64__SCREEN_MANAGER__H */
