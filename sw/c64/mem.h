#ifndef C64_MEM__H
#define C64_MEM__H

#include "memptr.h"
#include "util/membuf.h"

namespace c64 {

class mem
{
public:
    mem();

    void init();

    // Reset RAM to initial state, usually a random pattern.
    void reset();

private:

    membuf<memptr> initial;
};

} // namespace c64

#endif /* C64_MEM__H */
