#ifndef __CART_H__
#define __CART_H__

#include <stdexcept>
#include <memory>
#include <stdint.h>
#include "cart_loader/cart_loader.h"
#include "crt.h"

namespace c64 {

struct cart_exception : public std::runtime_error
{
    cart_exception(const char* what_arg) : std::runtime_error(what_arg) {}
};

class cart
{
public:
    using PCrt = std::shared_ptr<Crt>;
    void init();
    void load(PCrt);

private:
    using Cldr = cart_loader::cart_loader;

    Cldr* new_loader();

    PCrt _crt;
    std::unique_ptr<Cldr> _cldr;
};

} // namespace c64

#endif /* __CART_H__ */
