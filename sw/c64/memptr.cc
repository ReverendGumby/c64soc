#include <stdexcept>
#include "util/string.h"
#include "util/debug.h"
#include "c64bus.h"
#include "memptr.h"

#define DBG_LVL 1
#define DBG_TAG "MEMP"

namespace c64 {

memptr memval::last_p = memptr{0x0000, memtype::undef};
memcso memval::last_cso = memcso().undef(memcso::all);

const char* name(memtype e)
{
    switch (e)
    {
    case memtype::undef: return "undef";
    case memtype::ram: return "ram";
    case memtype::introm: return "introm";
    case memtype::extrom: return "extrom";
    case memtype::io: return "io";
    }
    return "???";
}

std::string memptr::to_string() const
{
    return string_printf("(%04X,%s)", (unsigned)addr(), name(type()));
}

memval memptr::operator*() const
{
    return memval{*this};
}

memval memptr::operator[](int i) const
{
    auto temp = *this + i;
    return *temp;
}

bool memptr::same_target(const memptr& that) const
{
    auto r1 = region();
    auto r2 = that.region();
    auto t1 = type();
    auto t2 = that.type();
    return r1 == r2 && t1 == t2;
}

bool memcso::operator==(const memcso& that) const
{
    return _def == that._def && _val == that._val;
}

memcso memcso::read_mon()
{
    const int mon_s = 16;                       // TODO: derive this
    return memcso((C64BUS_CSO >> mon_s) & all);
}

void memcso::apply() const
{
    DBG_PRINTF(9, "memcso %s\n", to_string().c_str());
    if (_def == none)
        C64BUS_CSO &= ~C64BUS_CSO_ENABLE;
    else
    {
        auto r = C64BUS_CSO;
        r &= ~(_def & ~_val);
        r |= _def & _val;
        r |= C64BUS_CSO_ENABLE;
        C64BUS_CSO = r;
    }
}

std::string memcso::to_string() const
{
    std::string s;
    value_type v;
    v = exrom;  s += is_undef(v) ? 'X' : (is_set(v) ? '1' : '0');
    v = game;   s += is_undef(v) ? 'X' : (is_set(v) ? '1' : '0');
    v = loram;  s += is_undef(v) ? 'X' : (is_set(v) ? '1' : '0');
    v = hiram;  s += is_undef(v) ? 'X' : (is_set(v) ? '1' : '0');
    v = charen; s += is_undef(v) ? 'X' : (is_set(v) ? '1' : '0');
    return s;
}

memcso to_memcso(const memptr& p)
{
    auto t = p.type();
    auto r = p.region();

    if (t == memtype::undef)
        return memcso().undef(memcso::all);
    if (r < 0x8)                                // 0000 - 7FFF
        return memcso();
    else if (r < 0xA)                           // 8000 - 9FFF
    {
        if (t == memtype::ram)
            return memcso();
        else if (t == memtype::extrom)
            return memcso().set(memcso::exrom);
    }
    else if (r < 0xC)                           // A000 - BFFF
    {
        if (t == memtype::introm)
            return memcso();
        else if (t == memtype::ram)
            return memcso().set(memcso::hiram);
        else if (t == memtype::extrom)
            return memcso().set(memcso::exrom | memcso::game);
    }
    else if (r < 0xD)                           // C000 - CFFF
    {
        if (t == memtype::ram)
            return memcso();
    }
    else if (r < 0xE)                           // D000 - DFFF
    {
        if (t == memtype::io)
            return memcso();
        else if (t == memtype::introm)
            return memcso().set(memcso::charen);
        else if (t == memtype::ram)
            return memcso().set(memcso::loram | memcso::hiram);
    }
    else                                        // E000 - FFFF
    {
        if (t == memtype::introm)
            return memcso();
        else if (t == memtype::ram)
            return memcso().set(memcso::hiram);
        else if (t == memtype::extrom)
            return memcso().set(memcso::game);
    }
    auto what = string_printf("unable to map memptr(%s) to memcso", p.to_string().c_str());
    throw std::runtime_error(what.c_str());
}

memtype to_memtype(const memaddr& a, const memcso& c)
{
    const auto C = memcso::charen;
    const auto L = memcso::loram;
    const auto H = memcso::hiram;
    const auto G = memcso::game;
    const auto E = memcso::exrom;

    auto m1 = c.is_clr(L|H|G|E);
    auto m2 = c.is_clr(L|G) && c.is_set(H);
    auto m3 = c.is_clr(L) && c.is_set(H|G|E);
    auto m4 = c.is_clr(H|G) && c.is_set(L);
    auto m5 = c.is_clr(G) && c.is_set(L|H);
    auto m6 = c.is_clr(L|H|G) && c.is_set(E);
    auto m7 = c.is_clr(H) && c.is_set(L|G|E);
    auto m8 = c.is_clr(L|H) && c.is_set(G|E);
    auto m9 = c.is_clr(E) && c.is_set(G);

    if (!(m1 || m2 || m3 || m4 || m5 || m6 || m7 || m8 || m9))
        return memtype::undef;

    auto r = memptr{a}.region();
    switch (r)
    {
    case 0x0:
        return memtype::ram;
    case 0x1:
    case 0x2:
    case 0x3:
    case 0x4:
    case 0x5:
    case 0x6:
    case 0x7:
    case 0xC:
        if (!m9)
            return memtype::ram;
        return memtype::undef;
    case 0x8:
    case 0x9:
        if (m6 || m8 || m9)
            return memtype::extrom;
        return memtype::ram;
    case 0xA:
    case 0xB:
        if (m1 || m6)
            return memtype::introm;
        if (m2 || m3 || m4 || m5)
            return memtype::ram;
        if (m7 || m8)
            return memtype::extrom;
        return memtype::undef;
    case 0xD:
        if (m5 || (m3 && c.is_set(C)))
            return memtype::ram;
        if (m9)
            return memtype::io;
        if (c.is_set(C))
            return memtype::introm;
        return memtype::io;
    case 0xE:
    case 0xF:
        if (m9)
            return memtype::extrom;
        if (m2 || m3 || m5)
            return memtype::ram;
        return memtype::introm;
    default:
        return memtype::undef;
    }
}

memval::operator uint8_t()
{
    set_cso();
    auto v = C64BUS_C64_MEM[_p.addr()];
    DBG_PRINTF_NOFUNC(10, "R [%s] = %02X\n", _p.to_string().c_str(), (unsigned)v);
    return v;
}

memval& memval::operator=(uint8_t v)
{
    set_cso();
    DBG_PRINTF_NOFUNC(10, "W [%s] = %02X\n", _p.to_string().c_str(), (unsigned)v);
    C64BUS_C64_MEM[_p.addr()] = v;
    return *this;
}

memval& memval::operator^=(uint8_t w)
{
    uint8_t v = *this;
    *this = v ^ w;
    return *this;
}

void memval::set_cso()
{
    if (!_p.same_target(last_p))
    {
        auto cso = to_memcso(_p);
        if (last_cso != cso)
        {
            cso.apply();
            last_cso = cso;
        }
    }
    last_p = _p;
}

} // namespace c64
