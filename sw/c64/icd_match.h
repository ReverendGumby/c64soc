#ifndef C64__ICD_MATCH__H
#define C64__ICD_MATCH__H

#include <stdint.h>

namespace c64 {

class icd_match
{
public:
    struct data
    {
        uint16_t a;
        uint8_t db;
        bool rw;
        bool sync;
    };

    class trigger
    {
    public:
        // "set" means the field(s) value must match to trigger.
        bool is_a_set();
        bool is_db_set();
        bool is_rw_set();
        bool is_sync_set();

        uint16_t get_a();
        uint8_t get_db();
        bool get_rw();
        bool get_sync();

        void set_a(uint16_t v);
        void set_db(uint8_t v);
        void set_rw(bool v);
        void set_sync(bool v);

        void clear_a();
        void clear_db();
        void clear_rw();
        void clear_sync();

    private:
        friend class icd_match;

        bool is_set(uint32_t mask);
        uint32_t get(uint32_t mask);
        void set(uint32_t bits, uint32_t mask);
        void clear(uint32_t mask);

        uint32_t din = 0;
        uint32_t den = 0;
    };

    bool is_enabled();
    bool is_triggered();
    data get_data();

    void set_trigger(const trigger& t);
    void disable();
    void enable();
};

} // namespace c64

#endif /* C64__ICD_MATCH__H */
