#include <stdexcept>
#include <stdio.h>
#include "util/string.h"
#include "ui/hid_manager.h"
#include "c64bus.h"
#include "sys.h"

namespace c64 {

sys::sys(plat::manager& pm, ui::hid_manager& hm)
    : plat_mgr(pm),
      kbd(hm),
      joy(hm, cfg)
{
    pause_count = 0;
    reset_ram_pending = false;
}

void sys::init()
{
	uint32_t val;

	val = C64BUS_ID;
	if (val != C64BUS_ID_DATA) {
        auto why = string_printf("C64BUS ID mismatch: got %08lx, wanted %08lx", val, C64BUS_ID_DATA);
		throw std::runtime_error(why);
	}
    auto why = string_printf("C64BUS ID is %08lx", val);
    printf("%s\n", why.c_str());

    reset_all(true);

    mem.init();
    cart.init();
    icd.init();
    kbd.start();
    joy.start();
}

void sys::reset_all(bool assert)
{
    if (assert)
        C64BUS_RESETS = 0;
    else
        C64BUS_RESETS = (uint32_t)-1;
}

void sys::reset_c64(bool assert)
{
    if (assert) {
        C64BUS_RESETS &= ~C64BUS_RESETS_C64;
        reset_ram();
    } else
        C64BUS_RESETS |= C64BUS_RESETS_C64;
}

bool sys::is_paused() const
{
    return pause_count;
}

void sys::pause()
{
    if (pause_count == 0)
    {
        icd.force_halt();
    }
    pause_count ++;
}

void sys::resume()
{
    pause_count --;
    if (pause_count == 0)
    {
        reset_ram_if_pending();
        icd.resume();
    }
}

void sys::reset_ram()
{
    if (is_paused())
        reset_ram_pending = true;
    else
        reset_ram_now();
}

void sys::reset_ram_if_pending()
{
    if (reset_ram_pending) {
        reset_ram_pending = false;
        reset_ram_now();
    }
}

void sys::reset_ram_now()
{
    mem.reset();
}

void sys::set_gpo(uint32_t o)
{
    C64BUS_GPO = o;
}

} // namespace c64
