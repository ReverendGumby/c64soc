#ifndef C64_VID__H
#define C64_VID__H

#include <stdint.h>
#include "memptr.h"

namespace c64 {

class screen;

// Used to manipulate the video display.
class vid
{
public:
    static constexpr int WIDTH = 40;
    static constexpr int HEIGHT = 25;
    static constexpr int GFXMEM_SIZE = WIDTH * HEIGHT;
    static constexpr int CLRMEM_SIZE = WIDTH * HEIGHT; // only 4 bits used
    static constexpr int REGS_SIZE = 0x2f;

    enum class color
    {
        BLACK = 0,
        WHITE = 1,
        RED = 2,
        CYAN = 3,
        PURPLE = 4,
        GREEN = 5,
        BLUE = 6,
        YELLOW = 7,
        ORANGE = 8,
        BROWN = 9,
        LT_RED = 10,
        GRAY1 = 11,
        GRAY2 = 12,
        LT_GREEN = 13,
        LT_BLUE = 14,
        GRAY3 = 15,
        NUM_COLORS
    };           

    struct addrs
    {
        memptr gfxmem_base;
    };

    struct saved_state
    {
        uint8_t ddra;
        addrs a;
        uint8_t gfxmem[GFXMEM_SIZE];
        uint8_t clrmem[CLRMEM_SIZE];
        uint8_t regs[REGS_SIZE];
    };

    addrs get_addrs();

    saved_state save_state();
    void restore_state(const saved_state&);

    screen* get_screen();
};

} // namespace c64

#endif /* C64_VID__H */
