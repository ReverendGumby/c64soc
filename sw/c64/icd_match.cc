#include "c64bus.h"

#include "icd_match.h"

namespace c64 {

bool icd_match::trigger::is_a_set()
{
    return is_set(C64BUS_ICD_MATCH_D_A);
}

bool icd_match::trigger::is_db_set()
{
    return is_set(C64BUS_ICD_MATCH_D_DB);
}

bool icd_match::trigger::is_rw_set()
{
    return is_set(C64BUS_ICD_MATCH_D_RW);
}

bool icd_match::trigger::is_sync_set()
{
    return is_set(C64BUS_ICD_MATCH_D_SYNC);
}

uint16_t icd_match::trigger::get_a()
{
    return uint16_t(get(C64BUS_ICD_MATCH_D_A) >> C64BUS_ICD_MATCH_D_A_S);
}

uint8_t icd_match::trigger::get_db()
{
    return uint8_t(get(C64BUS_ICD_MATCH_D_DB) >> C64BUS_ICD_MATCH_D_DB_S);
}

bool icd_match::trigger::get_rw()
{
    return get(C64BUS_ICD_MATCH_D_RW);
}

bool icd_match::trigger::get_sync()
{
    return get(C64BUS_ICD_MATCH_D_SYNC);
}

void icd_match::trigger::set_a(uint16_t v)
{
    uint32_t bits = uint32_t(v) << C64BUS_ICD_MATCH_D_A_S;
    uint32_t mask = C64BUS_ICD_MATCH_D_A;
    set(bits, mask);
}

void icd_match::trigger::set_db(uint8_t v)
{
    uint32_t bits = uint32_t(v) << C64BUS_ICD_MATCH_D_DB_S;
    uint32_t mask = C64BUS_ICD_MATCH_D_DB;
    set(bits, mask);
}

void icd_match::trigger::set_rw(bool v)
{
    uint32_t mask = C64BUS_ICD_MATCH_D_RW;
    uint32_t bits = v * mask;
    set(bits, mask);
}

void icd_match::trigger::set_sync(bool v)
{
    uint32_t mask = C64BUS_ICD_MATCH_D_SYNC;
    uint32_t bits = v * mask;
    set(bits, mask);
}

void icd_match::trigger::clear_a()
{
    clear(C64BUS_ICD_MATCH_D_A);
}

void icd_match::trigger::clear_db()
{
    clear(C64BUS_ICD_MATCH_D_DB);
}

void icd_match::trigger::clear_rw()
{
    clear(C64BUS_ICD_MATCH_D_RW);
}

void icd_match::trigger::clear_sync()
{
    clear(C64BUS_ICD_MATCH_D_SYNC);
}

bool icd_match::trigger::is_set(uint32_t mask)
{
    return (den & mask) == mask;
}

uint32_t icd_match::trigger::get(uint32_t mask)
{
    return din & mask;
}

void icd_match::trigger::set(uint32_t bits, uint32_t mask)
{
    din = (din & ~mask) | (bits & mask);
    den |= mask;
}

void icd_match::trigger::clear(uint32_t mask)
{
    din &= ~mask;
    den &= ~mask;
}

//////////////////////////////////////////////////////////////////////

bool icd_match::is_enabled()
{
    return C64BUS_ICD_CTRL & C64BUS_ICD_MATCH_ENABLE;
}

bool icd_match::is_triggered()
{
    return C64BUS_ICD_CTRL & C64BUS_ICD_MATCH_TRIGGER;
}

icd_match::data icd_match::get_data()
{
    uint32_t dout = C64BUS_ICD_MATCH_DOUT;

    data d;
    d.a = (dout & C64BUS_ICD_MATCH_D_A) >> C64BUS_ICD_MATCH_D_A_S;
    d.db = (dout & C64BUS_ICD_MATCH_D_DB) >> C64BUS_ICD_MATCH_D_DB_S;
    d.rw = dout & C64BUS_ICD_MATCH_D_RW;
    d.sync = dout & C64BUS_ICD_MATCH_D_SYNC;

    return d;
}

void icd_match::set_trigger(const icd_match::trigger& t)
{
    C64BUS_ICD_MATCH_DIN = t.din;
    C64BUS_ICD_MATCH_DEN = t.den;
}

void icd_match::disable()
{
    C64BUS_ICD_CTRL &= ~C64BUS_ICD_MATCH_ENABLE;
}

void icd_match::enable()
{
    C64BUS_ICD_CTRL |= C64BUS_ICD_MATCH_ENABLE;
}

} // namespace c64
