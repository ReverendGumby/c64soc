#include <stdexcept>
#include <string.h>
#include "util/debug.h"
#include <util/string.h>
#include "c64/c64bus.h"
#include "joy.h"

#define DBG_LVL 2
#define DBG_TAG "joy"

namespace c64 {

inline joy_mode_t id_to_joy_mode(int id)
{
    if (id == 0)
    	return joy_mode_t::hid_1;
    if (id == 1)
    	return joy_mode_t::hid_2;
    return joy_mode_t::none;
}

inline const char* joy_mode_to_str(joy_mode_t m)
{
    switch (m) {
    case joy_mode_t::none:      return "none";
    case joy_mode_t::keymap_a:  return "keymap_a";
    case joy_mode_t::keymap_b:  return "keymap_b";
    case joy_mode_t::hid_1:     return "hid_1";
    case joy_mode_t::hid_2:     return "hid_2";
    default:                    break;
    }
    return "?";
}

inline joy_mode_t str_to_joy_mode(const char* s)
{
    for (int i = 0; i < static_cast<int> (joy_mode_t::joy_modes); i++) {
        auto mode = static_cast<joy_mode_t>(i);
        if (strcmp(s, joy_mode_to_str(mode)) == 0)
            return mode;
    }
    throw std::domain_error("str_to_joy_mode:: unknown mode");
}

_joy::option::option(class joy& joy, joy_port_t port)
    : _joy(joy), _port(port)
{
    _name = std::string{"joy.joy"} + char('a' + char(_port));
}

const std::string& _joy::option::name()
{
	return _name;
}

std::string _joy::option::get()
{
    auto mode = _joy.mode(_port);
    return joy_mode_to_str(mode);
}

void _joy::option::set(const std::string& value)
{
    try {
        const auto mode = str_to_joy_mode(value.c_str());
        DBG_PRINTF(2, "%s <= %s\n", _name.c_str(), joy_mode_to_str(mode));
        _joy.change_port_mode(_port, mode);
    } catch (std::domain_error& e) {
        throw c64::config::option_error(e.what());
    }
}

joy::joy(ui::hid_manager& hm, c64::config::manager& cfg)
    : hid(hm), jrpt1{}, jrpt2{}, krpt{}
{
    joy_dev = &hid_mgr;
    kbd_dev = &hid_mgr;

    // Initial options
    static const joy_mode_t initial_modes[] = {
        [JOYA] = joy_mode_t::hid_2,
        [JOYB] = joy_mode_t::hid_1,
    };

    for (int i = 0; i < num_joys; i++) {
        _mode[i] = initial_modes[i];
        cfg.add(c64::config::option_ptr{new _joy::option{*this, joy_port_t(i)}});
    }

    hid_mgr.talker<ui::hid_manager::listener>::add_listener(this);

    joy_dev->add_listener(this);
    kbd_dev->add_listener(this);
}

joy::~joy()
{
    joy_dev->remove_listener(this);
    kbd_dev->remove_listener(this);

    hid_mgr.talker<ui::hid_manager::listener>::remove_listener(this);
}

ui::joy::report* joy::jrpt(joy_mode_t mode)
{
    if (mode == joy_mode_t::hid_1)
        return &jrpt1;
    if (mode == joy_mode_t::hid_2)
        return &jrpt2;
    return nullptr;
}

//      Bit  0       1       2       3       4
//  JOY A/B  Up      Down    Left    Right   Fire

static uint8_t get_jbits(const ui::joy::report& jrpt)
{
    const int jbits[] = {
        jrpt.up,
        jrpt.dn,
        jrpt.lt,
        jrpt.rt,
        jrpt.f1 || jrpt.f2 || jrpt.f3 || jrpt.f4,
    };
    uint8_t out = 0;
    for (unsigned b = 0; b < sizeof(jbits)/sizeof(jbits[0]); b++) {
        out |= ((!!jbits[b]) << b);
    }
    return out;
}

joy::joy_out joy::reports_to_joy_out()
{
	using kbcode = ui::kbd::kbcode;

    static const kbcode keymap_a[] = {
        kbcode::K8,                             /* Up */
        kbcode::K2,                             /* Down */
        kbcode::K4,                             /* Left */
        kbcode::K6,                             /* Right */
        kbcode::K0                              /* Fire */
    };
    static const kbcode keymap_b[] = {
        kbcode::W,                              /* Up */
        kbcode::S,                              /* Down */
        kbcode::A,                              /* Left */
        kbcode::D,                              /* Right */
        kbcode::SPC                             /* Fire */
    };

    joy_out j = {};

    uint8_t joy_1 = get_jbits(*jrpt(joy_mode_t::hid_1));
    uint8_t joy_2 = get_jbits(*jrpt(joy_mode_t::hid_2));

    uint8_t key_a = 0;
    uint8_t key_b = 0;

    for (int i = 0; i < krpt.num_codes; i++) {
        auto c = krpt.codes[i];
        unsigned col;

        for (col = 0; col < sizeof(keymap_a)/sizeof(keymap_a[0]); col++)
            if (c == keymap_a[col])
                key_a |= (1 << col);
        for (col = 0; col < sizeof(keymap_b)/sizeof(keymap_b[0]); col++)
            if (c == keymap_b[col])
                key_b |= (1 << col);
    }

    j.joy_a |= (mode(JOYA) == joy_mode_t::hid_1)    * joy_1;
    j.joy_a |= (mode(JOYA) == joy_mode_t::hid_2)    * joy_2;
    j.joy_a |= (mode(JOYA) == joy_mode_t::keymap_a) * key_a;
    j.joy_a |= (mode(JOYA) == joy_mode_t::keymap_b) * key_b;

    j.joy_b |= (mode(JOYB) == joy_mode_t::hid_1)    * joy_1;
    j.joy_b |= (mode(JOYB) == joy_mode_t::hid_2)    * joy_2;
    j.joy_b |= (mode(JOYB) == joy_mode_t::keymap_a) * key_a;
    j.joy_b |= (mode(JOYB) == joy_mode_t::keymap_b) * key_b;

    return j;
}

bool joy::is_idle()
{
    // joy_dev and kbd_dev both point to the same object.
    return joy_dev->is_idle();
}

void joy::set_c64bus_matrix()
{
    if (!started)
        return;

    auto m = reports_to_joy_out();
    C64BUS_JOY_MATRIX =
        m.joy_a << 0 |
        m.joy_b << 8;
}

void joy::reset_c64bus_matrix()
{
    C64BUS_JOY_MATRIX = 0;
}

void joy::change_port_mode(joy_port_t port, joy_mode_t new_mode)
{
    auto old_mode = mode(port);
    if (old_mode == new_mode)
        return;
    mode(port) = new_mode;

    talker<config::listener>::speak(&config::listener::joy_mode_changed, port);
}

void joy::event(const ui::kbd::keypress& c)
{
}

void joy::event(const ui::kbd::report& rpt)
{
    krpt = rpt;
    set_c64bus_matrix();
    on_event();
}

void joy::event(const ui::joy::report& rpt)
{
    auto m = id_to_joy_mode(rpt.id);
    auto* jr = jrpt(m);
    if (jr) {
        *jr = rpt;
        set_c64bus_matrix();
    }
    on_event();
}

std::string joy::get_joy_mode_desc_hid(int id)
{
    auto* dev = hid_mgr.get_joy_dev(id);
    if (!dev)
        return string_printf("USB joystick %d", id + 1);
    return dev->name;
}

void joy::joy_dev_changed(int id)
{
    auto new_mode = id_to_joy_mode(id);

    talker<config::listener>::speak(&config::listener::joy_mode_desc_changed, new_mode);
}

joy_mode_t joy::get_joy_mode(joy_port_t port)
{
    return mode(port);
}

std::string joy::get_joy_mode_desc(joy_mode_t m)
{
    switch (m) {
    case c64::joy_mode_t::none: return "None";
    case c64::joy_mode_t::keymap_a: return "Keymap A (Numpad)";
    case c64::joy_mode_t::keymap_b: return "Keymap B (ASDW space)";
    case c64::joy_mode_t::hid_1: return get_joy_mode_desc_hid(0);
    case c64::joy_mode_t::hid_2: return get_joy_mode_desc_hid(1);
    default: break;
    }
    return "";
}

void joy::set_joy_mode(joy_port_t port, joy_mode_t new_mode)
{
    // TODO: Check for another port with this same mode.
    change_port_mode(port, new_mode);
}

} // namespace c64
