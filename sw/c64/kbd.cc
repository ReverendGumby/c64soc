#include "c64/c64bus.h"
#include "kbd.h"

namespace c64 {

using kbcode = ui::kbd::kbcode;

kbd::kbd(ui::hid_manager& hm)
    : hid(hm)
{
    dev = &hid_mgr;
    dev->add_listener(this);
}

//      COL  0       1       2       3       4       5       6       7
//  ROW
//  0        DEL/INS 3/#     5/%     7/'     9/)     +       £       1/!
//  1        RETURN  W       R       Y       I       P       *       <--
//  2        CRSR RL A       D       G       J       L       ;/]     CTRL
//  3        F7/8    4/$     6       8/(     0       -       HOME    2/"
//  4        F1/2    Z       C       B       M       ./>     RSHIFT  SPACE
//  5        F3/4    S       F       H       K       :/[     =       C=
//  6        F5/6    E       T       U       O       @       ^       Q
//  7        CRSR DU LSHIFT  X       V       N       ,/<     //?     STOP/RUN

void kbd::translate_code_pos(kbcode c, kbd_out& k)
{
#define KC kbcode::
    static const kbcode kbd_codes[8][8] = {
        { KC BAK, KC N3 , KC N5 , KC N7 , KC N9 , KC MIN, KC END, KC N1  },
        { KC RET, KC W  , KC R  , KC Y  , KC I  , KC P  , KC RBR, KC GRA },
        { KC RAR, KC A  , KC D  , KC G  , KC J  , KC L  , KC QUO, KC RSV },
        { KC F7 , KC N4 , KC N6 , KC N8 , KC N0 , KC EQU, KC HOM, KC N2  },
        { KC F1 , KC Z  , KC C  , KC B  , KC M  , KC PER, KC RSV, KC SPC },
        { KC F3 , KC S  , KC F  , KC H  , KC K  , KC COL, KC DEL, KC RSV },
        { KC F5 , KC E  , KC T  , KC U  , KC O  , KC LBR, KC BSL, KC Q   },
        { KC DAR, KC RSV, KC X  , KC V  , KC N  , KC COM, KC SLA, KC ESC }
    };
#undef KC
    int row, col;

    for (row = 0; row < 8; row++)
        for (col = 0; col < 8; col++)
            if (c == kbd_codes[row][col]) {
                k.matrix[row] |= (1 << col);
                break;
            }
}

void kbd::translate_code_sym(kbcode c, kbd_out& k)
{
    enum s_t {
        S_XX, /* USB shift 0/1 -> C64 shift 0/1 */
        S_00, /* USB shift 0   -> C64 shift 0   */
        S_01, /* USB shift 0   -> C64 shift 1   */
        S_10, /* USB shift 1   -> C64 shift 0   */
        S_11, /* USB shift 1   -> C64 shift 1   */
    };
    static const struct { kbcode code; int row; int col; enum s_t shift; } map[] = {
        { kbcode::A  , 2, 1, S_XX },
        { kbcode::B  , 4, 3, S_XX },
        { kbcode::C  , 4, 2, S_XX },
        { kbcode::D  , 2, 2, S_XX },
        { kbcode::E  , 6, 1, S_XX },
        { kbcode::F  , 5, 2, S_XX },
        { kbcode::G  , 2, 3, S_XX },
        { kbcode::H  , 5, 3, S_XX },
        { kbcode::I  , 1, 4, S_XX },
        { kbcode::J  , 2, 4, S_XX },
        { kbcode::K  , 5, 4, S_XX },
        { kbcode::L  , 2, 5, S_XX },
        { kbcode::M  , 4, 4, S_XX },
        { kbcode::N  , 7, 4, S_XX },
        { kbcode::O  , 6, 4, S_XX },
        { kbcode::P  , 1, 5, S_XX },
        { kbcode::Q  , 6, 7, S_XX },
        { kbcode::R  , 1, 2, S_XX },
        { kbcode::S  , 5, 1, S_XX },
        { kbcode::T  , 6, 2, S_XX },
        { kbcode::U  , 6, 3, S_XX },
        { kbcode::V  , 7, 3, S_XX },
        { kbcode::W  , 1, 1, S_XX },
        { kbcode::X  , 7, 2, S_XX },
        { kbcode::Y  , 1, 3, S_XX },
        { kbcode::Z  , 4, 1, S_XX },
        { kbcode::N1 , 0, 7, S_XX },
        { kbcode::N2 , 3, 7, S_00 },
        { kbcode::N2 , 6, 5, S_10 },
        { kbcode::N3 , 0, 1, S_XX },
        { kbcode::N4 , 3, 1, S_XX },
        { kbcode::N5 , 0, 2, S_XX },
        { kbcode::N6 , 3, 2, S_00 },
        { kbcode::N6 , 6, 6, S_10 },
        { kbcode::N7 , 0, 3, S_00 },
        { kbcode::N7 , 0, 6, S_10 },
        { kbcode::N8 , 3, 3, S_00 },
        { kbcode::N8 , 1, 6, S_10 },
        { kbcode::N9 , 0, 4, S_00 },
        { kbcode::N9 , 3, 3, S_11 },
        { kbcode::N0 , 3, 4, S_00 },
        { kbcode::N0 , 0, 4, S_11 },
        { kbcode::RET, 1, 0, S_XX },
        { kbcode::ESC, 7, 7, S_XX },
        { kbcode::BAK, 0, 0, S_XX }, /* Backspace -> DEL/INST */
        { kbcode::DEL, 0, 0, S_XX }, /* Delete -> DEL/INST */
        { kbcode::SPC, 4, 7, S_XX },
        { kbcode::MIN, 3, 5, S_00 }, /* - */
        { kbcode::MIN, 0, 6, S_10 }, /* _ -> £ */
        { kbcode::EQU, 5, 6, S_00 }, /* = */
        { kbcode::EQU, 0, 5, S_10 }, /* + */
        { kbcode::LBR, 5, 5, S_01 }, /* [ */
        { kbcode::RBR, 2, 6, S_01 }, /* ] */
        { kbcode::BSL, 6, 6, S_00 }, /* \ -> ^ */
        { kbcode::COL, 2, 6, S_00 }, /* ; */
        { kbcode::COL, 5, 5, S_10 }, /* : */
        { kbcode::QUO, 0, 3, S_01 }, /* ' */
        { kbcode::QUO, 3, 7, S_11 }, /* " */
        { kbcode::GRA, 1, 7, S_00 }, /* ` -> <-- */
        { kbcode::COM, 7, 5, S_XX },
        { kbcode::PER, 4, 5, S_XX },
        { kbcode::SLA, 7, 6, S_XX },
        { kbcode::F1,  4, 0, S_00 },
        { kbcode::F2,  4, 0, S_01 },
        { kbcode::F3,  5, 0, S_00 },
        { kbcode::F4,  5, 0, S_01 },
        { kbcode::F5,  6, 0, S_00 },
        { kbcode::F6,  6, 0, S_01 },
        { kbcode::F7,  3, 0, S_00 },
        { kbcode::F8,  3, 0, S_01 },
        { kbcode::INS, 0, 0, S_01 },
        { kbcode::HOM, 3, 6, S_00 },
        { kbcode::RAR, 2, 0, S_00 },
        { kbcode::LAR, 2, 0, S_01 },
        { kbcode::DAR, 7, 0, S_00 },
        { kbcode::UAR, 7, 0, S_01 },
    };

    for (unsigned i = 0; i < sizeof(map)/sizeof(map[0]); i++) {
        if (map[i].code == c) {
            int match = 0;
            enum s_t ms = map[i].shift;
            switch (ms) {
            case S_XX:
                match = 1; break;
            case S_00:
            case S_01:
                match = !k.shift; break;
            case S_10:
            case S_11:
                match = k.shift; break;
            }
            if (match) {
                int row = map[i].row;
                int col = map[i].col;
                k.matrix[row] |= (1 << col);
                if (ms == S_00 || ms == S_10)
                    k.shift = 0;
                else if (ms == S_01 || ms == S_11)
                    k.shift = ui::kbd::K_LEFT;
            }
        }
    }
}

kbd::kbd_out kbd::kbd_report_to_kbd_out(const ui::kbd::report& rpt)
{
    static const kbcode misc_codes[] = {
        kbcode::PAU, /* C64BUS_MISC_KEYS_RESTORE */
    };
    bool kbd_symbolic = !rpt.mods.scroll_lock;

    kbd_out k = {};
    k.ctrl = rpt.mods.ctrl;
    k.shift = rpt.mods.shift;
    k.alt = rpt.mods.alt;
    k.win = rpt.mods.win;

    for (int i = 0; i < rpt.num_codes; i++) {
        auto c = rpt.codes[i];
        unsigned col;

        if (c == kbcode::RSV)
            continue;
        for (col = 0; col < sizeof(misc_codes)/sizeof(misc_codes[0]); col++)
            if (c == misc_codes[col])
            	k.misc |= (1 << col);
        if (kbd_symbolic)
        	translate_code_sym(c, k);
        else
        	translate_code_pos(c, k);
    }

    if (k.shift & ui::kbd::K_LEFT)  k.matrix[7] |= (1 << 1);
    if (k.shift & ui::kbd::K_RIGHT) k.matrix[4] |= (1 << 6);
    if (k.ctrl)                     k.matrix[2] |= (1 << 7);
    if (k.win)                      k.matrix[5] |= (1 << 7);

    return k;
}

bool kbd::is_idle()
{
    return dev->is_idle();
}

void kbd::set_c64bus_matrix(const kbd::kbd_out& k)
{
    if (!started)
        return;

    C64BUS_KBD_MATRIX_0 =
    	k.matrix[0] << 0 |
    	k.matrix[1] << 8 |
    	k.matrix[2] << 16 |
    	k.matrix[3] << 24;
    C64BUS_KBD_MATRIX_1 =
    	k.matrix[4] << 0 |
    	k.matrix[5] << 8 |
    	k.matrix[6] << 16 |
    	k.matrix[7] << 24;
    C64BUS_MISC_KEYS = k.misc;
}

void kbd::reset_c64bus_matrix()
{
    C64BUS_KBD_MATRIX_0 = 0;
    C64BUS_KBD_MATRIX_1 = 0;
    C64BUS_MISC_KEYS = 0;
}

void kbd::event(const ui::kbd::keypress& c)
{
}

void kbd::event(const ui::kbd::report& rpt)
{
    auto k = kbd_report_to_kbd_out(rpt);
    set_c64bus_matrix(k);

    on_event();
}

} // namespace c64
