#include <sstream>
#include <stdexcept>
#include "monitor/reg_printer.h"

#include "mem_view.h"

namespace c64 {
namespace monitor {

using namespace ::monitor;

constexpr int y_first_row = 0;
constexpr int n_rows = 22;
constexpr int y_addr = 23;

mem_view::mem_view(struct model& model)
    : model(model)
{
    set_color_border(color_t::BLUE);
    set_color_bg(color_t::GREEN);

    ui_address_lbl.add_to_view(this, "Address:", 0, y_addr);

    ui_address.set_pos_size(ui_address_lbl.get_right() + 2, y_addr, 5, 1);
    ui_address.set_change_action([this]() { this->on_address_changed(); });
    ui_address.show();
    add_child_view(&ui_address);

    set_start_address(0);
    set_address_type(memtype::undef);
}

void mem_view::update()
{
    // Model data is fetched during drawing.
    draw();
}

void mem_view::set_start_address(const memaddr& ma)
{
    start_ptr = memptr{ma, start_ptr.type()};
    ui_address.set_text(reg_printer::to_str(ma));
}

void mem_view::set_address_type(const memtype& mt)
{
    start_ptr = memptr{start_ptr.addr(), mt};
}

void mem_view::print_row(const drawing_context* dc, int row)
{
    constexpr int bytes_per_row = 8;

    int y = y_first_row + row;
    auto mp = start_ptr + bytes_per_row * row;

    uint8_t bytes[bytes_per_row];
    bool valid[bytes_per_row];

    for (int i = 0; i < bytes_per_row; i++)
        try
        {
            bytes[i] = model.get_byte(mp + i);
            valid[i] = true;
        }
        catch (std::runtime_error&)
        {
            valid[i] = false;
        }

    auto* ss = dc->ss;

    *ss << ui::pos(0, y) << reg_printer::to_str(mp.addr()) << ": ";
    for (int i = 0; i < bytes_per_row; i++)
    {
        if (valid[i])
            *ss << reg_printer::to_str(bytes[i]);
        else
            *ss << "??";
        *ss << ' ';
        if (i % 4 == 3)
            *ss << ' ';
    }
    for (int i = 0; i < bytes_per_row; i++)
    {
        if (valid[i])
            *ss << char(bytes[i]);
        else
            *ss << '?';
    }
    *ss << std::endl;
}

void mem_view::on_address_changed()
{
    const auto& s = ui_address.get_text();
    std::stringstream ss{s};
    unsigned int v;
    ss >> std::hex >> v;
    if (ss)
    {
        set_start_address(v);
        draw();
    }
}

void mem_view::on_draw(const drawing_context* dc)
{
    for (int i = 0; i < n_rows; i++)
        print_row(dc, i);
}

bool mem_view::on_key_down(const keypress_t& kp, int repeat)
{
    if (kp.code == kbcode::A)
        ui_address.grab_focus();
    else
        return fullscreen_view::on_key_down(kp, repeat);
    return true;
}

} // namespace monitor
} // namespace c64

/*
   0         1         2         3        3
   0123456789012345678901234567890123456789
00 0000: 41 42 43 44  45 46 47 48  ABCDEFGH
 1 0008: 49 4A 4B 4C  4D 4E 4F 50  IJKLMNOP
 2 0010:
 3 0018:
 4 0020:
 5 0028:
 6 0030:
 7 0038:
 8 0040:
 9 0048:
10 0050:
 1 0058:
 2 0060:
 3 0068:
 4 0070:
 5 0078:
 6 0080:
 7 0088:
 8 0090:
 9 0098:
20 00A0:
 1 00A8: 30 31 32 33  34 35 36 37  01234567
 2 
 3 Address: 0000
24 Mem type: Undef Ram iNt-rom Ext-rom Io
 */
