#ifndef MONITOR__SID_VIEW__H
#define MONITOR__SID_VIEW__H

#include "ui/fullscreen_view.h"
#include "c64/hwregs.h"

namespace c64 {
namespace monitor {

class sid_view : public ui::fullscreen_view
{
public:
    struct model
    {
    };

    sid_view(model& model);

    void update();

protected:
    virtual const char* get_class_name() const { return "sid_view"; }

private:
    // interface for ui::fullscreen_view
    virtual void on_draw(const drawing_context*);

    model& model;
    static constexpr unsigned num_regs =
        unsigned(SID_ENV3 - SID_F1L) + 1;
    uint8_t regs[num_regs];
};

} // namespace monitor
} // namespace c64

#endif /* MONITOR__SID_VIEW__H */
