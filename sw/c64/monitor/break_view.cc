#include <sstream>
//#include <stdexcept>
#include "util/string.h"

#include "break_view.h"

namespace c64 {
namespace monitor {

using namespace c64;

constexpr int y_first_row = 0;
constexpr int n_rows = 22;
constexpr int y_addr = 23;

break_view::break_view(struct model& model)
    : model(model)
{
    set_color_border(color_t::RED);
    set_color_bg(color_t::GREEN);

    ui_brk_lbl.add_to_view(this, "Breakpoint:", 0, 0);
    ui_brk_en.set_pos_size(ui_brk_lbl.get_right() + 2, ui_brk_lbl.get_top(), 10, 1);
    ui_brk_en.show();
    add_child_view(&ui_brk_en);

    ui_trigger_lbl.add_to_view(this, "Trigger:", 0, 2);

    ui_addr_lbl.add_to_view(this, "Addr:", 2, 3);
    ui_addr.set_pos_size(ui_addr_lbl.get_right() + 2, ui_addr_lbl.get_top(), 5, 1);
    ui_addr.set_change_action([this]() { on_addr_changed(); });
    ui_addr.show();
    add_child_view(&ui_addr);

    ui_data_lbl.add_to_view(this, "Data:", 2, 4);
    ui_data.set_pos_size(ui_data_lbl.get_right() + 2, ui_data_lbl.get_top(), 3, 1);
    ui_data.set_change_action([this]() { on_data_changed(); });
    ui_data.show();
    add_child_view(&ui_data);

    ui_rw_lbl.add_to_view(this, "RW:", 2, 5);
    ui_rw.set_pos_size(ui_rw_lbl.get_right() + 2, ui_rw_lbl.get_top(), 1, 1);
    ui_rw.show();
    add_child_view(&ui_rw);

    ui_sync_lbl.add_to_view(this, "SYNC:", 2, 6);
    ui_sync.set_pos_size(ui_sync_lbl.get_right() + 2, ui_sync_lbl.get_top(), 1, 1);
    ui_sync.show();
    add_child_view(&ui_sync);
}

void break_view::update(int flags)
{
    if (flags & update_flag_en) {
        if (enabled) {
            ui_brk_en.set_text("Enabled");
        } else {
            ui_brk_en.set_text("Disabled");
        }
        ui_brk_en.draw();
    }

    if (flags & update_flag_addr) {
        auto s = trig.is_a_set() ? string_printf("%04X", trig.get_a()) : "XXXX";
        ui_addr.set_text(s);
        ui_addr.draw();
    }
    if (flags & update_flag_data) {
        auto s = trig.is_db_set() ? string_printf("%02X", trig.get_db()) : "XX";
        ui_data.set_text(s);
        ui_data.draw();
    }
    if (flags & update_flag_rw) {
        auto s = trig.is_rw_set() ? (trig.get_rw() ? "R" : "W") : "X";
        ui_rw.set_text(s);
        ui_rw.draw();
    }
    if (flags & update_flag_sync) {
        auto s = trig.is_sync_set() ? (trig.get_sync() ? "H" : "L") : "X";
        ui_sync.set_text(s);
        ui_sync.draw();
    }
}

void break_view::toggle_en()
{
    enabled = !enabled;
    update(update_flag_en);
}

void break_view::toggle_addr()
{
    if (trig.is_a_set()) {
        trig.clear_a();
        update(update_flag_addr);
    } else {
        ui_addr.grab_focus();
    }
}

void break_view::toggle_data()
{
    if (trig.is_db_set()) {
        trig.clear_db();
        update(update_flag_data);
    } else {
        ui_data.grab_focus();
    }
}

void break_view::toggle_rw()
{
    if (!trig.is_rw_set()) {
        trig.set_rw(true);                      // Read
    } else if (trig.is_rw_set() && trig.get_rw()) {
        trig.set_rw(false);                     // Write
    } else if (trig.is_rw_set() && !trig.get_rw()) {
        trig.clear_rw();                        // Don't care
    }
    update(update_flag_rw);
}

void break_view::toggle_sync()
{
    if (!trig.is_sync_set()) {
        trig.set_sync(false);                   // Low
    } else if (trig.is_sync_set() && !trig.get_sync()) {
        trig.set_sync(true);                    // High
    } else if (trig.is_sync_set() && trig.get_sync()) {
        trig.clear_sync();                      // Don't care
    }
    update(update_flag_sync);
}

void break_view::on_addr_changed()
{
    const auto& s = ui_addr.get_text();
    std::stringstream ss{s};
    unsigned int v;
    ss >> std::hex >> v;
    if (ss)
    {
        trig.set_a(uint16_t(v));
    }
    update(update_flag_addr);
}

void break_view::on_data_changed()
{
    const auto& s = ui_data.get_text();
    std::stringstream ss{s};
    unsigned int v;
    ss >> std::hex >> v;
    if (ss)
    {
        trig.set_db(uint8_t(v));
    }
    update(update_flag_data);
}

bool break_view::on_key_down(const keypress_t& kp, int repeat)
{
    if (kp.ch == 'b')
        toggle_en();
    else if (kp.ch == 'a')
        toggle_addr();
    else if (kp.ch == 'd')
        toggle_data();
    else if (kp.ch == 'r')
        toggle_rw();
    else if (kp.ch == 's')
        toggle_sync();
    else
        return fullscreen_view::on_key_down(kp, repeat);
    return true;
}

void break_view::on_show()
{
    enabled = model.break_get_enable();
    trig = model.break_get_trigger();
    update();

    ui::fullscreen_view::on_show();
}

void break_view::on_hide()
{
    ui::fullscreen_view::on_hide();

    model.break_set_enable(enabled);
    model.break_set_trigger(trig);
}

} // namespace monitor
} // namespace c64

/*
   0         1         2         3        3
   0123456789012345678901234567890123456789
00 Breakpoint: Enabled
 1
 2 Trigger:
 3   Addr: 0000
 4   Data: 00
 5   RW: R
 6   SYNC: H
 7
 8
 9
10
 1
 2
 3
 4
 5
 6
 7
 8
 9
20
 1
 2
 3
24
 */
