#ifndef MONITOR__BREAK_VIEW__H
#define MONITOR__BREAK_VIEW__H

#include "c64/memptr.h"
#include "c64/icd_match.h"
#include "ui/fullscreen_view.h"
#include "ui/label.h"
#include "ui/text_editor.h"

namespace c64 {
namespace monitor {

class break_view : public ui::fullscreen_view
{
public:
    struct model
    {
        virtual icd_match::trigger break_get_trigger() = 0;
        virtual void break_set_trigger(const icd_match::trigger&) = 0;

        virtual bool break_get_enable() = 0;
        virtual void break_set_enable(bool) = 0;
    };

    break_view(model& model);

protected:
    virtual const char* get_class_name() const { return "break_view"; }

private:
    static constexpr int update_flag_en = 1<<0;
    static constexpr int update_flag_addr = 1<<1;
    static constexpr int update_flag_data = 1<<2;
    static constexpr int update_flag_rw = 1<<3;
    static constexpr int update_flag_sync = 1<<3;

    void update(int flags = -1);

    void toggle_en();
    void toggle_addr();
    void toggle_data();
    void toggle_rw();
    void toggle_sync();

    void on_addr_changed();
    void on_data_changed();

    // interface for ui::fullscreen_view
    virtual bool on_key_down(const keypress_t& k, int repeat);
    virtual void on_show();
    virtual void on_hide();

    bool enabled;
    model& model;
    icd_match::trigger trig;

    ui::label ui_brk_lbl;
    ui::label ui_brk_en;
    ui::label ui_trigger_lbl;
    ui::label ui_addr_lbl;
    ui::label ui_data_lbl;
    ui::label ui_rw_lbl;
    ui::label ui_rw;
    ui::label ui_sync_lbl;
    ui::label ui_sync;
    ui::text_editor ui_addr, ui_data;
};

} // namespace monitor
} // namespace c64

#endif /* MONITOR__BREAK_VIEW__H */
