#include <sstream>
#include "util/reg_val.h"
#include "monitor/reg_printer.h"
#include "c64/hwregs.h"

#include "sid_view.h"

constexpr int y_reg = 1;
constexpr int x_lbl_1 = 0;
constexpr int x_lbl_2 = 19;
constexpr int w_lbl = 5;

using namespace c64;

struct cr_t { uint8_t val; };
template <>
std::string reg_val<cr_t>::to_str() const
{
    auto v = val.val;
    std::stringstream ss;
    ss << reg_val_of(v) << ' ';
    ss << ((v & SID_CR_WNOI) ? 'N' : '_')
       << ((v & SID_CR_WPUL) ? 'P' : '_')
       << ((v & SID_CR_WSAW) ? 'S' : '_')
       << ((v & SID_CR_WTRI) ? 'T' : '_')
       << ((v & SID_CR_TEST) ? 't' : '_')
       << ((v & SID_CR_RING) ? 'r' : '_')
       << ((v & SID_CR_SYNC) ? 's' : '_')
       << ((v & SID_CR_GATE) ? 'G' : '_');
    return ss.str();
}

struct ad_t { uint8_t val; };
template <>
std::string reg_val<ad_t>::to_str() const
{
    auto v = val.val;
    std::stringstream ss;
    ss << reg_val_of(v) << ' ';
    ss <<  "A" << std::setw(2) << unsigned((v & SID_AD_ATK) >> SID_AD_ATK_S)
       << ",D" << std::setw(2) << unsigned((v & SID_AD_DCY) >> SID_AD_DCY_S);
    return ss.str();
}

struct sr_t { uint8_t val; };
template <>
std::string reg_val<sr_t>::to_str() const
{
    auto v = val.val;
    std::stringstream ss;
    ss << reg_val_of(v) << ' ';
    ss <<  "S" << std::setw(2) << unsigned((v & SID_SR_STN) >> SID_SR_STN_S)
       << ",R" << std::setw(2) << unsigned((v & SID_SR_RLS) >> SID_SR_RLS_S);
    return ss.str();
}

struct fcr1_t { uint8_t val; };
template <>
std::string reg_val<fcr1_t>::to_str() const
{
    auto v = val.val;
    std::stringstream ss;
    ss << reg_val_of(v) << ' ';
    ss << "RES" << unsigned((v & SID_FCR1_RES) >> SID_FCR1_RES_S)
       << ','
       << ((v & SID_FCR1_EX) ? 'E' : '_')
       << ((v & SID_FCR1_F3) ? '3' : '_')
       << ((v & SID_FCR1_F2) ? '2' : '_')
       << ((v & SID_FCR1_F1) ? '1' : '_');
    return ss.str();
}

struct fcr2_t { uint8_t val; };
template <>
std::string reg_val<fcr2_t>::to_str() const
{
    auto v = val.val;
    std::stringstream ss;
    ss << reg_val_of(v) << ' ';
    ss << ((v & SID_FCR2_C3OFF) ? '3' : '_')
       << ((v & SID_FCR2_HP) ? 'H' : '_')
       << ((v & SID_FCR2_BP) ? 'B' : '_')
       << ((v & SID_FCR2_LP) ? 'L' : '_')
       << ",V" << unsigned((v & SID_FCR2_VOL) >> SID_FCR2_VOL_S);
    return ss.str();
}

namespace c64 {
namespace monitor {

using namespace ::monitor;

sid_view::sid_view(struct model& model)
    : model(model)
{
    set_color_bg(color_t::GREEN);
    set_color_border(color_t::ORANGE);
}

void sid_view::update()
{
    for (unsigned i = 0; i < num_regs; i++)
        regs[i] = SID[SID_F1L + uint8_t(i)];
}

void sid_view::on_draw(const drawing_context* dc)
{
    auto* ss = dc->ss;
    const auto& r = regs;
    reg_printer rp(ss, x_lbl_1, w_lbl);

    rp.y = y_reg;
    rp.print("F1L",  r[SID_F1L]);
    rp.print("F1H",  r[SID_F1H]);
    rp.print("PW1L", r[SID_PW1L]);
    rp.print("PW1H", r[SID_PW1H]);
    rp.print("CR1",  cr_t{r[SID_CR1]});
    rp.print("AD1",  ad_t{r[SID_AD1]});
    rp.print("SR1",  sr_t{r[SID_SR1]});
    rp.y ++;
    rp.print("F2L",  r[SID_F2L]);
    rp.print("F2H",  r[SID_F2H]);
    rp.print("PW2L", r[SID_PW2L]);
    rp.print("PW2H", r[SID_PW2H]);
    rp.print("CR2",  cr_t{r[SID_CR2]});
    rp.print("AD2",  ad_t{r[SID_AD2]});
    rp.print("SR2",  sr_t{r[SID_SR2]});
    rp.y ++;
    rp.print("F3L",  r[SID_F3L]);
    rp.print("F3H",  r[SID_F3H]);
    rp.print("PW3L", r[SID_PW3L]);
    rp.print("PW3H", r[SID_PW3H]);
    rp.print("CR3",  cr_t{r[SID_CR3]});
    rp.print("AD3",  ad_t{r[SID_AD3]});
    rp.print("SR3",  sr_t{r[SID_SR3]});

    rp.y = y_reg;
    rp.x_lbl = x_lbl_2;
    rp.print("FCL",  r[SID_FCL]);
    rp.print("FCH",  r[SID_FCH]);
    rp.print("FCR1", fcr1_t{r[SID_FCR1]});
    rp.print("FCR2", fcr2_t{r[SID_FCR2]});
    rp.print("POTX", r[SID_POTX]);
    rp.print("POTY", r[SID_POTY]);
    rp.print("OSC3", r[SID_OSC3]);
    rp.print("ENV3", r[SID_ENV3]);
}

} // namespace monitor
} // namespace c64
