#ifndef MONITOR__MEM_VIEW__H
#define MONITOR__MEM_VIEW__H

#include "c64/memptr.h"
#include "ui/fullscreen_view.h"
#include "ui/label.h"
#include "ui/text_editor.h"

namespace c64 {
namespace monitor {

class mem_view : public ui::fullscreen_view
{
public:
    struct model
    {
        virtual uint8_t get_byte(const memptr& mp) = 0;
    };

    mem_view(model& model);

    void update();

protected:
    virtual const char* get_class_name() const { return "mem_view"; }

private:
    void set_start_address(const memaddr& ma);
    void set_address_type(const memtype& mt);

    void print_row(const drawing_context*, int row);

    void on_address_changed();

    // interface for ui::fullscreen_view
    virtual void on_draw(const drawing_context*);
    virtual bool on_key_down(const keypress_t& k, int repeat);

    model& model;
    memptr start_ptr;
    ui::label ui_address_lbl;
    ui::text_editor ui_address;
};

} // namespace monitor
} // namespace c64

#endif /* MONITOR__MEM_VIEW__H */
