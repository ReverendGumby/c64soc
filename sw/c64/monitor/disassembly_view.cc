#include <sstream>
#include <iomanip>
#include "monitor/reg_printer.h"
#include "monitor/mos6502/asm.h"
#include "monitor/mos6502/cpu_reg_val.h"
#include "c64/screen.h"

#include "disassembly_view.h"

using namespace c64;
using namespace ::monitor::mos6502;

namespace c64 {
namespace monitor {

using namespace ::monitor;

constexpr int x_inst = 0;
constexpr int x_inst_ctr = 12;
constexpr int w_inst = 23;
constexpr int w_lbl = 10;
constexpr int x_lbl = 31 - w_lbl + 1;

disassembly_view::disassembly_view(struct model& model)
    : model(model),
      iq_model(*this),
      iq_view(iq_model)
{
    h_inst = max_height();

    set_color_border(color_t::GREEN);
    set_color_bg(color_t::GREEN);

    iq_view.set_page_mode(false);
    iq_view.set_pos_size(0, 0, w_inst, h_inst);
    iq_view.set_count(iq_view.get_height());
    iq_view.show();
    add_child_view(&iq_view);

    iq_view.grab_focus();
}

disassembly_view::iq_model::iq_model(disassembly_view& p)
    : parent(p)
{
}

void disassembly_view::get_insts(address_t a)
{
    iq = model.get_insts(a, h_inst, x_inst_ctr);
}

void disassembly_view::update()
{
    model.get_regs(regs, last_regs);
    get_insts(regs.pc);

    // Set list selection to the instruction at PC.
    iq_view.select(iq_model.addr_to_idx(regs.pc));

    draw();
}

void disassembly_view::get_vic_state(vid::saved_state& ss) const
{
    auto* dc = get_dc();
    if (dc) {
        auto* scr = dynamic_cast<screen*>(dc->scr);
        if (scr)
            ss = scr->get_saved_state();
    }
}

void disassembly_view::toggle_break()
{
    auto i = iq_view.get_selected();
    auto addr = iq_model.idx_to_addr(i);
    model.toggle_break(addr);
    draw();
}

void disassembly_view::print_help()
{
    auto* ss = get_dc()->ss;
    *ss << ui::window(26, 18, 14, 7);
    *ss << "[C]ontinue" << std::endl;
    *ss << "[S]tep" << std::endl;
    *ss << "[T]oggle break" << std::endl;
    *ss << "View [V]IC" << std::endl;
    *ss << "View SI[D]" << std::endl;
    *ss << "View [M]emory" << std::endl;
    *ss << "[B]reakpoints" << std::endl;
    *ss << ui::window();
}

void disassembly_view::print_regs()
{
    auto* ss = get_dc()->ss;
    reg_printer rp(ss, x_lbl, w_lbl);
    rp.y = 1;
    rp.print("PC", regs.pc, last_regs.pc);
    rp.print("A", regs.a, last_regs.a);
    rp.print("X", regs.x, last_regs.x);
    rp.print("Y", regs.y, last_regs.y);
    rp.print("S", regs.s, last_regs.s);
    rp.print("PF", pf_t{regs}, pf_t{last_regs});
    rp.y ++;
    rp.print("ABR", regs.abr, last_regs.abr);
    rp.print("DOR", regs.dor, last_regs.dor);
    rp.print("IR", regs.ir, last_regs.ir);
    rp.print("Tstate", tstate_t{regs}, tstate_t{last_regs});
    rp.print("Pin", pstate_t{regs}, pstate_t{last_regs});
}

int disassembly_view::iq_model::addr_to_idx(address_t a) const
{
    for (size_t i = 0; i < parent.iq.size(); i++)
        if (idx_to_addr(i) == a)
            return int(i);
    return -1;
}

address_t disassembly_view::iq_model::idx_to_addr(int i) const
{
    return parent.iq[i].addr();
}

std::string disassembly_view::iq_model::get_item(int idx)
{
    std::stringstream ss;
    auto& i = parent.iq[idx];
    auto ip = i.addr();
    auto brk = parent.model.is_break_set(ip);
    ss << std::hex << std::uppercase << std::setfill('0') << std::right
       << std::setw(4) << unsigned(ip) << (brk ? '*' : ' ');
    for (auto b : i.bytes())
        ss << std::setw(2) << (unsigned)b;
    ss << std::setfill(' ') << std::setw(1 + 2 * (3 - i.size())) << ""
       << i.str() << std::endl;
    return ss.str();
}

void disassembly_view::on_draw(const drawing_context* dc)
{
    print_help();
    print_regs();
}

bool disassembly_view::on_key_down(const keypress_t& kp, int repeat)
{
    if (kp.ch == 'c' || kp.code == kbcode::ESC)
        model.exit();
    else if (kp.ch == 's')
        model.step();
    else if (kp.ch == 't')
        toggle_break();
    else if (kp.ch == 'v')
        model.show_vic();
    else if (kp.ch == 'd')
        model.show_sid();
    else if (kp.ch == 'm')
        model.show_mem();
    else if (kp.ch == 'b')
        model.show_break();
    else
        return fullscreen_view::on_key_down(kp, repeat);
    return true;
}

void disassembly_view::iq_model::on_selected(int)
{
}

int disassembly_view::iq_model::get_scroll_distance(int want)
{
    // Scroll at most one-half of the view.
    // TODO: Prevent wrap-around scrolling
    auto h_inst = parent.h_inst;
    return std::max(-h_inst / 2, std::min(h_inst / 2, want));
}

void disassembly_view::iq_model::on_scrolled(int distance)
{
    // Locate the instruction that will be centered in the view.
    auto& i = parent.iq[x_inst_ctr + distance];
    // Fetch instructions around the new center.
    parent.get_insts(i.addr());
}

} // namespace monitor
} // namespace c64

/*
   0         1         2         3        3
   0123456789012345678901234567890123456789
00 .... ...... ... .......
 1 .... ...... ... .......       PC 1234
 2 .... ...... ... .......        A 00
 3 .... ...... ... .......        X 00
 4 .... ...... ... .......        Y 00
 5 .... ...... ... .......        S 00
 6 .... ...... ... .......       PF CZIDBVN
 7 .... ...... ... .......
 8 .... ...... ... .......      ABR 0000
 9 .... ...... ... .......      DOR 00
10 .... ...... ... .......       IR 00
 1 1231 BD5544 LDA $4455,X   Tstate 111010
 2 1234 A901   LDA #$01         Pin RsI
 3 1236 A110   LDA ($10,X)
 4 .... ...... ... .......
 5 .... ...... ... .......
 6 .... ...... ... .......
 7 .... ...... ... .......
 8 .... ...... ... .......
 9 .... ...... ... .......   [C]ontinue
20 .... ...... ... .......   [S]tep
 1 .... ...... ... .......   View [V]IC
 2 .... ...... ... .......   View SI[D]
 3 .... ...... ... .......   View [M]emory
24 .... ...... ... .......   [B]reakpoints
 */
