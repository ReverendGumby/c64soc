#ifndef MONITOR__DISASSEMBLY_VIEW__H
#define MONITOR__DISASSEMBLY_VIEW__H

#include <vector>
#include "monitor/mos6502/asm.h"
#include "c64/sys.h"
#include "c64/vid.h"
#include "ui/fullscreen_view.h"
#include "ui/scrollable_list_view.h"

namespace c64 {
namespace monitor {

using namespace ::monitor;
using namespace mos6502;

class disassembly_view : public ui::fullscreen_view
{
public:
    struct model
    {
        using insts = std::vector<inst>;

        virtual void get_regs(cpu_regs& regs,
                              cpu_regs& last_regs) = 0;
        virtual insts get_insts(address_t pc, size_t max_insts,
                                size_t num_prev_inst) = 0;
        virtual bool is_break_set(address_t pc) = 0;

        virtual void step() = 0;
        virtual void exit() = 0;
        virtual void toggle_break(address_t pc) = 0;
        virtual void show_vic() = 0;
        virtual void show_sid() = 0;
        virtual void show_mem() = 0;
        virtual void show_break() = 0;
    };

    disassembly_view(model& model);

    void update();

    void get_vic_state(vid::saved_state&) const;

protected:
    virtual const char* get_class_name() const { return "disassembly_view"; }

private:
    // major actions
    void get_insts(address_t a);
    void scroll(int distance);
    void toggle_break();

    // drawing code
    void print_help();
    void print_regs();

    // interface for ui::fullscreen_view
    virtual void on_draw(const drawing_context*);
    virtual bool on_key_down(const keypress_t& k, int repeat);

    int h_inst;
    model& model;
    cpu_regs regs, last_regs;

    class iq_model : public ui::scrollable_list_view::model {
    public:
    	iq_model(disassembly_view&);

        int addr_to_idx(address_t) const;
        address_t idx_to_addr(int) const;

        // interface for ui::scrollable_list_view::model
        virtual std::string get_item(int idx);
        virtual void on_selected(int idx);
        virtual int get_scroll_distance(int want);
        virtual void on_scrolled(int distance);

    private:
        disassembly_view& parent;
    } iq_model;
    friend class iq_model;

    model::insts iq;
    ui::scrollable_list_view iq_view;
};

} // namespace monitor
} // namespace c64

#endif /* MONITOR__DISASSEMBLY_VIEW__H */
