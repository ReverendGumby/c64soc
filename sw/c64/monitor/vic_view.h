#ifndef MONITOR__VIC_VIEW__H
#define MONITOR__VIC_VIEW__H

#include "c64/vid.h"
#include "ui/fullscreen_view.h"

namespace c64 {
namespace monitor {

class vic_view : public ui::fullscreen_view
{
public:
    struct model
    {
        using state = vid::saved_state;
        virtual void get_state(state& state) = 0;
    };

    vic_view(model& model);

    void update();

protected:
    virtual const char* get_class_name() const { return "vic_view"; }

private:
    // interface for ui::fullscreen_view
    virtual void on_draw(const drawing_context*);

    model& model;
    model::state state;
};

} // namespace monitor
} // namespace c64

#endif /* MONITOR__VIC_VIEW__H */
