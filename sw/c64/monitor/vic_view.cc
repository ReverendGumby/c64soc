#include <sstream>
#include "util/reg_val.h"
#include "monitor/reg_printer.h"
#include "c64/vid.h"
#include "c64/hwregs.h"

#include "vic_view.h"

constexpr int x_lbl_1 = 0;
constexpr int x_lbl_2 = 20;
constexpr int w_lbl = 5;

using namespace c64;

struct cr1_t { uint8_t val; };
template <>
std::string reg_val<cr1_t>::to_str() const
{
    auto v = val.val;
    std::stringstream ss;
    ss << reg_val_of(v) << ' ';
    ss << ((v & CR1_RC8) ? 'R' : 'r')
       << ((v & CR1_ECM) ? 'E' : 'e')
       << ((v & CR1_BMM) ? 'B' : 'b')
       << ((v & CR1_DEN) ? 'D' : 'd')
       << ",R" << ((v & CR1_RSEL) ? "25" : "24")
       << ",Y" << unsigned((v & CR1_YSCROLL) >> CR1_YSCROLL_S);
    return ss.str();
}

struct cr2_t { uint8_t val; };
template <>
std::string reg_val<cr2_t>::to_str() const
{
    auto v = val.val;
    std::stringstream ss;
    ss << reg_val_of(v) << ' ';
    ss << ((v & CR2_RES) ? 'R' : 'r')
       << ((v & CR2_MCM) ? 'M' : 'm')
       << ",C" << ((v & CR2_CSEL) ? "40" : "38")
       << ",X" << unsigned((v & CR2_XSCROLL) >> CR2_XSCROLL_S);
    return ss.str();
}

struct ir_t { uint8_t val; };
template <>
std::string reg_val<ir_t>::to_str() const
{
    auto v = val.val;
    std::stringstream ss;
    ss << reg_val_of(v) << ' ';
    ss << ((v & IR_IRQ) ? 'I' : 'i')
       << "___"
       << ((v & IR_LP) ? 'L' : 'l')
       << ((v & IR_MMC) ? 'M' : 'm')
       << ((v & IR_MBC) ? 'B' : 'b')
       << ((v & IR_RST) ? 'R' : 'r');
    return ss.str();
}

struct ie_t { uint8_t val; };
template <>
std::string reg_val<ie_t>::to_str() const
{
    auto v = val.val;
    std::stringstream ss;
    ss << reg_val_of(v) << ' ';
    ss << "____"
       << ((v & IR_LP) ? 'L' : 'l')
       << ((v & IR_MMC) ? 'M' : 'm')
       << ((v & IR_MBC) ? 'B' : 'b')
       << ((v & IR_RST) ? 'R' : 'r');
    return ss.str();
}

struct clr_t { uint8_t val; };
template <>
std::string reg_val<clr_t>::to_str() const
{
    auto v = val.val;
    std::stringstream ss;
    ss << reg_val_of(v) << ' ';
    switch (static_cast<vid::color>(v & 0x0F))
    {
    case vid::color::BLACK:   ss << "BLACK";      break;
    case vid::color::WHITE:   ss << "WHITE";      break;
    case vid::color::RED:     ss << "RED";        break;
    case vid::color::CYAN:    ss << "CYAN";       break;
    case vid::color::PURPLE:  ss << "PURPLE";     break;
    case vid::color::GREEN:   ss << "GREEN";      break;
    case vid::color::BLUE:    ss << "BLUE";       break;
    case vid::color::YELLOW:  ss << "YELLOW";     break;
    case vid::color::ORANGE:  ss << "ORANGE";     break;
    case vid::color::BROWN:   ss << "BROWN";      break;
    case vid::color::LT_RED:  ss << "LT_RED";     break;
    case vid::color::GRAY1:   ss << "GRAY1";      break;
    case vid::color::GRAY2:   ss << "GRAY2";      break;
    case vid::color::LT_GREEN:ss << "LT_GREEN";   break;
    case vid::color::LT_BLUE: ss << "LT_BLUE";    break;
    case vid::color::GRAY3:   ss << "GRAY3";      break;
    default: break;
    }
    return ss.str();
}

namespace c64 {
namespace monitor {

using namespace ::monitor;

vic_view::vic_view(struct model& model)
    : model(model)
{
    set_color_bg(color_t::GREEN);
    set_color_border(color_t::LT_GREEN);
}

void vic_view::update()
{
    model.get_state(state);
}

void vic_view::on_draw(const drawing_context* dc)
{
    auto* ss = dc->ss;
    const auto& r = state.regs;
    reg_printer rp(ss, x_lbl_1, w_lbl);

    rp.y = 0;
    rp.print("M0X",  r[VIC_M0X]);
    rp.print("M0Y",  r[VIC_M0Y]);
    rp.print("M1X",  r[VIC_M1X]);
    rp.print("M1Y",  r[VIC_M1Y]);
    rp.print("M2X",  r[VIC_M2X]);
    rp.print("M2Y",  r[VIC_M2Y]);
    rp.print("M3X",  r[VIC_M3X]);
    rp.print("M3Y",  r[VIC_M3Y]);
    rp.print("M4X",  r[VIC_M4X]);
    rp.print("M4Y",  r[VIC_M4Y]);
    rp.print("M5X",  r[VIC_M5X]);
    rp.print("M5Y",  r[VIC_M5Y]);
    rp.print("M6X",  r[VIC_M6X]);
    rp.print("M6Y",  r[VIC_M6Y]);
    rp.print("M7X",  r[VIC_M7X]);
    rp.print("M7Y",  r[VIC_M7Y]);
    rp.print("MXX8", r[VIC_MXX8]);
    rp.print("CR1",  cr1_t{r[VIC_CR1]});
    rp.print("RC",   r[VIC_RC]);
    rp.print("LPX",  r[VIC_LPX]);
    rp.print("LPY",  r[VIC_LPY]);
    rp.print("MXE",  r[VIC_MXE]);
    rp.print("CR2",  cr2_t{r[VIC_CR2]});
    rp.print("MXYE", r[VIC_MXYE]);

    rp.y = 0;
    rp.x_lbl = x_lbl_2;
    rp.print("MP",   r[VIC_MP]);
    rp.print("IR",   ir_t{r[VIC_IR]});
    rp.print("IE",   ie_t{r[VIC_IE]});
    rp.print("MXDP", r[VIC_MXDP]);
    rp.print("MXMC", r[VIC_MXMC]);
    rp.print("MXXE", r[VIC_MXXE]);
    rp.print("MXM",  r[VIC_MXM]);
    rp.print("MXD",  r[VIC_MXD]);
    rp.print("EC",   clr_t{r[VIC_EC]});
    rp.print("B0C",  clr_t{r[VIC_B0C]});
    rp.print("B1C",  clr_t{r[VIC_B1C]});
    rp.print("B2C",  clr_t{r[VIC_B2C]});
    rp.print("B3C",  clr_t{r[VIC_B3C]});
    rp.print("MM0",  clr_t{r[VIC_MM0]});
    rp.print("MM1",  clr_t{r[VIC_MM1]});
    rp.print("M0C",  clr_t{r[VIC_M0C]});
    rp.print("M1C",  clr_t{r[VIC_M1C]});
    rp.print("M2C",  clr_t{r[VIC_M2C]});
    rp.print("M3C",  clr_t{r[VIC_M3C]});
    rp.print("M4C",  clr_t{r[VIC_M4C]});
    rp.print("M5C",  clr_t{r[VIC_M5C]});
    rp.print("M6C",  clr_t{r[VIC_M6C]});
    rp.print("M7C",  clr_t{r[VIC_M7C]});
}

} // namespace monitor
} // namespace c64
