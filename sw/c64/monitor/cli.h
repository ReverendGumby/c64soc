#pragma once

#include <c64/memptr.h>
#include <cli/command.h>

namespace c64 {
namespace monitor {

class cli
{
public:
    struct model
    {
        virtual uint8_t get_byte(const memptr& mp) = 0;
    };

    cli(model& model);

private:
    int mem_display(const ::cli::command::args_t& args);

    model& model;
};

} // namespace monitor
} // namespace c64
