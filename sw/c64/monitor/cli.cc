#include <cli/command.h>
#include <util/dump_to_str.h>

#include <stdint.h>
#include <stdexcept>
#include <iostream>

#include "cli.h"

namespace c64 {
namespace monitor {

using namespace ::cli;

cli::cli(struct model& model)
    : model(model)
{
    command::define("md", [this](const command::args_t& args) {
        return mem_display(args); });
}

int cli::mem_display(const command::args_t& args)
{
    if (args.size() < 1)
        throw std::range_error("missing argument");
    auto addr = memaddr(strtoul(args[0].c_str(), NULL, 0));
    memptr mp{addr};

    unsigned long len = 1;
    if (args.size() >= 2)
        len = strtoul(args[1].c_str(), NULL, 0);

    uint8_t data[len];
    for (unsigned long i = 0; i < len; i++)
        data[i] = model.get_byte(mp + i);

    for (auto& line : dump_to_str(data, len, addr))
        std::cout << line << std::endl;

    return 0;
}

} // namespace monitor
} // namespace c64
