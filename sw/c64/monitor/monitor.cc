#include <cli/command.h>
#include <monitor/mos6502/find_pc.h>

#include <string>
#include <ios>
#include <array>
#include <algorithm>

#include "monitor.h"

namespace c64 {
namespace monitor {

monitor::monitor(class sys& sys)
    : sys(sys), dis_view(*this), vic_view(*this), sid_view(*this),
      mem_view(*this), break_view(*this), cli(*this)
{
    got_regs = false;
    bp_en = false;

    pollable_add(this);
}

void monitor::halt()
{
    if (!stopped)
    {
        got_regs = false;
        halt_cpu_on_sync();
    }
}

void monitor::enter()
{
    update_regs();
    dis_view.show();
    // show() called sys.pause() for us
    stopped = true;
    // Multiple entities will want the VIC saved state.
    dis_view.get_vic_state(vic_ss);

    vic_view.update();                          // VIC state is now saved
    sid_view.update();
    mem_view.update();
}

void monitor::exit()
{
    match_break();
    stopped = false;
    dis_view.hide();
    // hide() called sys.resume() for us
}

void monitor::toggle_break(address_t a)
{
    if (is_break_set(a)) {
        bp_en = false;
    } else {
        icd_match::trigger t {};
        t.set_a(a);
        t.set_sync(true);
        bp_trig = t;
        bp_en = true;
    }
}

void monitor::show_vic()
{
    vic_view.show();
}

void monitor::show_sid()
{
    sid_view.show();
}

void monitor::show_mem()
{
    mem_view.show();
}

void monitor::show_break()
{
    break_view.show();
}

void monitor::poll()
{
    // If ICD halted the CPU because the match unit triggered, then enter the monitor.
    if (match && match->is_triggered())
    {
        auto d = match->get_data();
        pc_known = false;
        if (d.sync) {
            icd_pc = d.a;
            pc_known = true;
        }

        match->disable();
        release_match();

        enter();
    }
}

void monitor::update_regs()
{
    last_regs = regs;

    regs = sys.icd.read_cpu_regs();
    if (pc_known)
        regs.pc = icd_pc;
    else
        regs.pc = find_pc<memptr>(regs);

    if (!got_regs)
    {
        got_regs = true;
        last_regs = regs;
    }

    dis_view.update();
}

void monitor::get_regs(cpu_regs& out_regs,
                       cpu_regs& out_last_regs)
{
    out_regs = regs;
    out_last_regs = last_regs;
}

monitor::insts monitor::get_insts(address_t pc, size_t max_insts,
                                  size_t num_prev_inst)
{
    disassembler_ctx ctx;
    ctx.ip = pc;
    return monitor::disassembler::get_insts(ctx, max_insts, num_prev_inst);
}

bool monitor::is_break_set(address_t a)
{
    // Is a breakpoint set on this CPU instruction?
    return bp_en &&
        !bp_trig.is_db_set() &&
        !bp_trig.is_rw_set() &&
        bp_trig.is_sync_set() && bp_trig.get_sync() &&
        bp_trig.is_a_set() && bp_trig.get_a() == a;
}

void monitor::get_state(vic_view::model::state& state)
{
    state = vic_ss;
}

uint8_t monitor::get_byte(const memptr& mp)
{
    auto& gbase = vic_ss.a.gfxmem_base;
    if (mp.addr() >= gbase.addr() &&
        mp.addr() <= gbase.addr() + (vid::GFXMEM_SIZE - 1)) {
        return vic_ss.gfxmem[mp.addr() - gbase.addr()];
    }
    return *mp;
}

icd_match::trigger monitor::break_get_trigger()
{
    return bp_trig;
}

void monitor::break_set_trigger(const icd_match::trigger& t)
{
    bp_trig = t;

    dis_view.update();
}

bool monitor::break_get_enable()
{
    return bp_en;
}

void monitor::break_set_enable(bool en)
{
    bp_en = en;

    dis_view.update();
}

bool monitor::grab_match()
{
    if (match)
        return false;
    match = &sys.icd.match;
    return true;
}

void monitor::release_match()
{
    match = nullptr;
}

void monitor::halt_cpu_on_sync()
{
    icd_match::trigger t;
    t.set_sync(true);

    (void)grab_match();
    match->disable();
    match->set_trigger(t);
    match->enable();
}

void monitor::match_break()
{
    if (bp_en && grab_match()) {
        match->disable();
        match->set_trigger(bp_trig);
        match->enable();
    }
}

void monitor::step()
{
    halt_cpu_on_sync();
    exit();
}

} // namespace monitor
} // namespace c64
