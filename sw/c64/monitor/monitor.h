#pragma once

#include <string>
#include "c64/sys.h"
#include "c64/icd_match.h"
#include "c64/memptr.h"
#include "disassembly_view.h"
#include "vic_view.h"
#include "sid_view.h"
#include "mem_view.h"
#include "break_view.h"
#include "cli.h"
#include "monitor/mos6502/asm.h"
#include "monitor/disassembler.h"
#include "util/pollable.h"

namespace c64 {
namespace monitor {

using namespace ::monitor;
using namespace mos6502;

class monitor :
        private pollable,
        private disassembler<disassembler_ctx, inst, memptr>,
        private disassembly_view::model,
        private vic_view::model,
        private sid_view::model,
        private mem_view::model,
        private break_view::model,
        private cli::model
{
public:
    monitor(class sys& sys);

    void halt();

private:
    using insts = disassembly_view::model::insts;

    // interface for pollable
    virtual const char* get_poll_name() const { return "monitor"; }
    virtual void poll();

    // interface for disassembly_view::model
    virtual void get_regs(cpu_regs& regs,
                          cpu_regs& last_regs);
    virtual insts get_insts(address_t pc, size_t max_insts,
                            size_t num_prev_inst);
    virtual bool is_break_set(address_t a);
    virtual void step();
    virtual void exit();
    virtual void toggle_break(address_t a);
    virtual void show_vic();
    virtual void show_sid();
    virtual void show_mem();
    virtual void show_break();

    // interface for vic_view::model
    virtual void get_state(vic_view::model::state& state);

    // interface for mem_view::model, cli::model
    virtual uint8_t get_byte(const memptr& mp);

    // interface for break_view::model
    virtual icd_match::trigger break_get_trigger();
    virtual void break_set_trigger(const icd_match::trigger&);
    virtual bool break_get_enable();
    virtual void break_set_enable(bool);

    void update_regs();

    void enter();

    bool grab_match();
    void release_match();

    void halt_cpu_on_sync();
    void match_break();

    class sys& sys;
    disassembly_view dis_view;
    class vic_view vic_view;
    class sid_view sid_view;
    class mem_view mem_view;
    class break_view break_view;
    class cli cli;
    icd_match* match = nullptr;
    bool pc_known;
    address_t icd_pc;
    bool stopped = false;
    cpu_regs regs, last_regs;
    bool got_regs;
    bool bp_en;
    icd_match::trigger bp_trig;
    vic_view::model::state vic_ss;
};

} // namespace monitor
} // namespace c64
