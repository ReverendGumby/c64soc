#include <unistd.h>
#include "c64bus.h"
#include "util/timer.h"

#include "icd.h"

namespace c64 {

void icd::init()
{
    reset();
}

void icd::reset()
{
    C64BUS_ICD_CTRL = 0;
    C64BUS_ICD_CTRL = C64BUS_ICD_CPU_ENABLE;
}

uint8_t icd::read_cpu_reg(uint8_t reg)
{
    C64BUS_CPU_REG_SEL = reg;
    uint8_t val = C64BUS_CPU_REG_DOUT;
    return val;
}

icd::cpu_regs icd::read_cpu_regs()
{
    auto pcl = read_cpu_reg(C64BUS_CPU_REG_SEL_PCL);
    auto pch = read_cpu_reg(C64BUS_CPU_REG_SEL_PCH);
    auto pf  = read_cpu_reg(C64BUS_CPU_REG_SEL_PF);
    auto s   = read_cpu_reg(C64BUS_CPU_REG_SEL_S);
    auto ac  = read_cpu_reg(C64BUS_CPU_REG_SEL_AC);
    auto x   = read_cpu_reg(C64BUS_CPU_REG_SEL_X);
    auto y   = read_cpu_reg(C64BUS_CPU_REG_SEL_Y);
    auto ir  = read_cpu_reg(C64BUS_CPU_REG_SEL_IR);
    auto abl = read_cpu_reg(C64BUS_CPU_REG_SEL_ABL);
    auto abh = read_cpu_reg(C64BUS_CPU_REG_SEL_ABH);
    auto dor = read_cpu_reg(C64BUS_CPU_REG_SEL_DOR);
    auto ts  = read_cpu_reg(C64BUS_CPU_REG_SEL_TS);
    auto is  = read_cpu_reg(C64BUS_CPU_REG_SEL_IS);
    auto ps  = read_cpu_reg(C64BUS_CPU_REG_SEL_PS);

    cpu_regs ret;
    ret.pc = (uint16_t(pch) << 8) | pcl;
    ret.pf.c = pf & C64BUS_CPU_REG_PF_C;
    ret.pf.z = pf & C64BUS_CPU_REG_PF_Z;
    ret.pf.i = pf & C64BUS_CPU_REG_PF_I;
    ret.pf.d = pf & C64BUS_CPU_REG_PF_D;
    ret.pf.b = pf & C64BUS_CPU_REG_PF_B;
    ret.pf.v = pf & C64BUS_CPU_REG_PF_V;
    ret.pf.n = pf & C64BUS_CPU_REG_PF_N;
    ret.s = s;
    ret.a = ac;
    ret.x = x;
    ret.y = y;
    ret.ir = ir;
    ret.abr = (uint16_t(abh) << 8) | abl;
    ret.dor = dor;
    ret.tstate.clock1 = ts & C64BUS_CPU_REG_TS_CLOCK1;
    ret.tstate.clock2 = ts & C64BUS_CPU_REG_TS_CLOCK2;
    ret.tstate.t2     = ts & C64BUS_CPU_REG_TS_T2;
    ret.tstate.t3     = ts & C64BUS_CPU_REG_TS_T3;
    ret.tstate.t4     = ts & C64BUS_CPU_REG_TS_T4;
    ret.tstate.t5     = ts & C64BUS_CPU_REG_TS_T5;
    ret.istate.resp = is & C64BUS_CPU_REG_IS_RESP;
    ret.istate.resg = is & C64BUS_CPU_REG_IS_RESG;
    ret.istate.nmip = is & C64BUS_CPU_REG_IS_NMIP;
    ret.istate.nmig = is & C64BUS_CPU_REG_IS_NMIG;
    ret.istate.irqp = is & C64BUS_CPU_REG_IS_IRQP;
    ret.pstate.rdy  = ps & C64BUS_CPU_REG_PS_RDY;
    ret.pstate.rw   = ps & C64BUS_CPU_REG_PS_RW;
    ret.pstate.aec  = ps & C64BUS_CPU_REG_PS_AEC;
    ret.pstate.sync = ps & C64BUS_CPU_REG_PS_SYNC;
    ret.pstate.so   = ps & C64BUS_CPU_REG_PS_SO;
    return ret;
}

void icd::force_halt()
{
    // Halt CPU by deasserting RDY. This can take up to 3 cycles to take
    // effect.
    C64BUS_ICD_CTRL |= C64BUS_ICD_CPU_FORCE_HALT;
    timer::sleep(3 * timer::us);
    C64BUS_ICD_CTRL &= ~C64BUS_ICD_CPU_FORCE_HALT;
}

void icd::resume()
{
    C64BUS_ICD_CTRL |= C64BUS_ICD_CPU_RESUME;
    while (C64BUS_ICD_CTRL & C64BUS_ICD_CPU_CLR_RDY)
        ;
    C64BUS_ICD_CTRL &= ~C64BUS_ICD_CPU_RESUME;
}

} // namespace c64
