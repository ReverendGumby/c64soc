#ifndef SYS_H__
#define SYS_H__

#include <stdint.h>
#include "config/manager.h"
#include "mem.h"
#include "cart.h"
#include "vid.h"
#include "kbd.h"
#include "joy.h"
#include "icd.h"

namespace plat {
class manager;
}

namespace ui {
class hid_manager;
}

namespace c64 {

class sys
{
public:
    sys(plat::manager&, ui::hid_manager&);
    void init();

    void reset_all(bool assert);
    void reset_c64(bool assert);
    void reset_ram();

    void pause();
    void resume();
    bool is_paused() const;

    void set_gpo(uint32_t o);

    plat::manager& plat_mgr;

    class config::manager cfg;
    class mem mem;
    class cart cart;
    class vid vid;
    class kbd kbd;
    class joy joy;
    class icd icd;

private:
    int pause_count;
    bool reset_ram_pending;

    uint8_t read_cpu_reg(uint8_t reg);
    void reset_ram_if_pending();
    void reset_ram_now();
};

} // namespace c64

#endif /* SYS_H__ */
