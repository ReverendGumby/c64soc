#include <stdexcept>
#include "util/string.h"
#include "util/debug.h"
#include "memptr.h"
#include "mem.h"

#define DBG_LVL 1
#define DBG_TAG "MEM"

namespace c64 {

mem::mem()
    : initial(memptr(memaddr_min, memtype::ram),
              memptr(memaddr_max, memtype::ram))
{
}

void mem::init()
{
    initial.fetch();
}

void mem::reset()
{
    DBG_PRINTF(1, "\n");
    initial.store();
}

} // namespace c64
