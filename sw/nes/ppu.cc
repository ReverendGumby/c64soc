#include "ppu.h"

#include "nesbus.h"

#include <unistd.h>

namespace nes {

void ppu::init()
{
}

uint32_t ppu::read_reg(uint32_t sel)
{
    NESBUS_PPU_MON_SEL = sel;
    uint32_t val = NESBUS_PPU_MON_DOUT;
    return val;
}

template<size_t Len>
std::array<uint32_t, Len> ppu::read_array(uint32_t sel)
{
    std::array<uint32_t, Len> ret;
    for (size_t i = 0; i < Len; i++)
        ret[i] = read_reg(sel + i * 4);
    return ret;
}

ppu_regs ppu::read_regs(int which)
{
    ppu_regs ret;

    if (which & regs_cms) {
        auto cms = read_reg(NESBUS_PPU_MON_SEL_CMS);

        ret.ctrl.addr_inc = (cms & NESBUS_PPU_MON_CMS_ADDR_INC) != 0;
        ret.ctrl.spr_pat = (cms & NESBUS_PPU_MON_CMS_SPR_PAT) != 0;
        ret.ctrl.bkg_pat = (cms & NESBUS_PPU_MON_CMS_BKG_PAT) != 0;
        ret.ctrl.spr_size = (cms & NESBUS_PPU_MON_CMS_SPR_SIZE) != 0;
        ret.ctrl.slave_mode = (cms & NESBUS_PPU_MON_CMS_SLAVE_MODE) != 0;
        ret.ctrl.vbl_en = (cms & NESBUS_PPU_MON_CMS_VBL_EN) != 0;
        ret.mask.pal_mono = (cms & NESBUS_PPU_MON_CMS_PAL_MONO) != 0;
        ret.mask.bkg_clip = (cms & NESBUS_PPU_MON_CMS_BKG_CLIP) != 0;
        ret.mask.spr_clip = (cms & NESBUS_PPU_MON_CMS_SPR_CLIP) != 0;
        ret.mask.bkg_en = (cms & NESBUS_PPU_MON_CMS_BKG_EN) != 0;
        ret.mask.spr_en = (cms & NESBUS_PPU_MON_CMS_SPR_EN) != 0;
        ret.mask.emph = (cms & NESBUS_PPU_MON_CMS_EMPH) >> NESBUS_PPU_MON_CMS_EMPH_S;
        ret.stat.spr_overflow = (cms & NESBUS_PPU_MON_CMS_SPR_OVERFLOW) != 0;
        ret.stat.spr0_hit = (cms & NESBUS_PPU_MON_CMS_SPR0_HIT) != 0;
        ret.stat.vbl_flag = (cms & NESBUS_PPU_MON_CMS_VBL_FLAG) != 0;
    }
    
    if (which & regs_video_counter) {
        auto rowcol = read_reg(NESBUS_PPU_MON_SEL_ROWCOL);
        auto frame = read_reg(NESBUS_PPU_MON_SEL_FRAME);

        ret.video_counter.row = (rowcol & NESBUS_PPU_MON_ROWCOL_ROW) >> NESBUS_PPU_MON_ROWCOL_ROW_S;
        ret.video_counter.col = (rowcol & NESBUS_PPU_MON_ROWCOL_COL) >> NESBUS_PPU_MON_ROWCOL_COL_S;
        ret.video_counter.frame = frame;
    }

    if (which & regs_bus_master) {
        auto val = read_reg(NESBUS_PPU_MON_SEL_VAL);
        auto vac = read_reg(NESBUS_PPU_MON_SEL_VAC);

        ret.bus_master.val = val;
        ret.bus_master.vac.v = vac;
    }

    if (which & regs_palette) {
        auto pal = read_array<NESBUS_PPU_MON_SEL_PAL_LEN / 4>(NESBUS_PPU_MON_SEL_PAL);

        for (size_t i = 0; i < pal.size(); i++)
            for (size_t j = 0; j < 4; j++)
                ret.palette.pal[i * 4 + j] = pal[i] >> 8 * j;
    }

    if (which & regs_oam) {
        auto oam_pri = read_array<NESBUS_PPU_MON_SEL_OAM_PRI_LEN / 4>(NESBUS_PPU_MON_SEL_OAM_PRI);
        auto oam_sec = read_array<NESBUS_PPU_MON_SEL_OAM_SEC_LEN / 4>(NESBUS_PPU_MON_SEL_OAM_SEC);

        for (size_t i = 0; i < oam_pri.size(); i++)
            for (size_t j = 0; j < 4; j++)
                ret.oam.pri[i * 4 + j] = oam_pri[i] >> 8 * j;
        for (size_t i = 0; i < oam_sec.size(); i++)
            for (size_t j = 0; j < 4; j++)
                ret.oam.sec[i * 4 + j] = oam_sec[i] >> 8 * j;
    }

    return ret;
}

} // namespace nes
