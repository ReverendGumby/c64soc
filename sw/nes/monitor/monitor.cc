#include <cli/command.h>
#include <monitor/mos6502/find_pc.h>

#include <string>
#include <ios>
#include <array>
#include <algorithm>

#include "monitor.h"

namespace nes {
namespace monitor {

monitor::monitor(class sys& sys)
    : sys(sys), cli(*this)
{
    matches[match_idx_cpu_sync] = &sys.icd.get_match(0);
    matches[match_idx_break] = &sys.icd.get_match(1);
    got_regs = false;
    bp_en = false;

    // Listen for someone else (eg sys::reset_nes) calling sys.icd.resume().
    sys.icd.add_listener(this);
}

void monitor::halt()
{
    if (!halted())
    {
        got_regs = false;
        halt_cpu_on_sync();
    }
}

void monitor::resume()
{
    if (halted()) {
        sys.icd.resume();
        // icd will call resumed()
    }
}

bool monitor::halted()
{
    poll();
    return stopped;
}

void monitor::enter()
{
    // clean up after halt_cpu_on_sync()
    disable_all_matches();
    match_break();

    update_regs();
    stopped = true;
}

void monitor::exit()
{
    stopped = false;
}

void monitor::toggle_break(address_t a)
{
    if (is_break_set(a)) {
        bp_en = false;
    } else {
        icd_match::trigger t {};
        t.set_a(a);
        t.set_sync(true);
        bp_trig = t;
        bp_en = true;
    }

    match_break();
}

void monitor::poll()
{
    if (!stopped && sys.icd.is_halted())
    {
        // Find the first triggered match unit.
        icd_match* first_match = nullptr;
        for (int i = 0; i < match_num; i++) {
            auto* match = matches[i];
            if (match->is_triggered()) {
                first_match = match;
                break;
            }
        }

        // Figure out where we've stopped.
        pc_known = false;
        ppu_pos_known = false;
        if (first_match) {
            auto d = first_match->get_data();
            if (d.sync) {
                icd_pc = d.a;
                pc_known = true;
            }
            ppu_pos.col = d.col;
            ppu_pos.row = d.row;
            ppu_pos.frame = d.frame;
            ppu_pos_known = true;
            cpu_cyc = d.cpu_cyc;
        }

        // Reset all triggered match units.
        for (int i = 0; i < match_num; i++) {
            auto* match = matches[i];
            if (match->is_triggered()) {
                match->reset_triggered();
            }
        }

        enter();
    }
    else if (stopped && !sys.icd.is_halted()) {
        exit();
    }
}

void monitor::update_regs()
{
    last_regs = regs;

    regs = sys.icd.read_cpu_regs();
    if (pc_known)
        regs.pc = icd_pc;
    else
        regs.pc = find_pc<memptr>(regs);

    if (!got_regs)
    {
        got_regs = true;
        last_regs = regs;
    }
}

void monitor::get_regs(cpu_regs& out_regs,
                       cpu_regs& out_last_regs)
{
    out_regs = regs;
    out_last_regs = last_regs;
}

monitor::insts monitor::get_insts(address_t pc, size_t max_insts,
                                  size_t num_prev_inst)
{
    disassembler_ctx ctx;
    ctx.ip = pc;
    return monitor::disassembler::get_insts(ctx, max_insts, num_prev_inst);
}

void monitor::get_ppu_pos(ppu_pos_t& pos, bool& known)
{
    pos = ppu_pos;
    known = ppu_pos_known;
}

uint32_t monitor::get_cpu_cyc()
{
    return cpu_cyc;
}

bool monitor::is_break_set(address_t a)
{
    // Is a breakpoint set on this CPU instruction?
    return bp_en &&
        !bp_trig.is_db_set() &&
        !bp_trig.is_rw_set() &&
        bp_trig.is_sync_set() && bp_trig.get_sync() &&
        bp_trig.is_a_set() && bp_trig.get_a() == a;
}

uint8_t monitor::get_byte(const memptr& mp)
{
    return *mp;
}

icd_match::trigger monitor::break_get_trigger()
{
    return bp_trig;
}

void monitor::break_set_trigger(const icd_match::trigger& t)
{
    bp_trig = t;
}

bool monitor::break_get_enable()
{
    return bp_en;
}

void monitor::break_set_enable(bool en)
{
    bp_en = en;
    match_break();
}

void monitor::resumed()
{
    exit();
}

void monitor::disable_all_matches()
{
    for (int i = 0; i < match_num; i++)
        matches[i]->disable();
}

void monitor::halt_cpu_on_sync()
{
    disable_all_matches();

    icd_match::trigger t;
    t.set_sync(true);

    auto* match = matches[match_idx_cpu_sync];
    match->set_trigger(t);
    match->enable();
}

void monitor::match_break()
{
    auto* match = matches[match_idx_break];
    match->disable();
    if (bp_en) {
        match->set_trigger(bp_trig);
        match->enable();
    }
}

void monitor::step()
{
    halt_cpu_on_sync();
    resume();
}

} // namespace monitor
} // namespace nes
