#include <cli/command.h>
#include <cli/manager.h>
#include <util/dump_to_str.h>
#include <util/reg_val.h>
#include <util/ostream_fmt_save.h>
#include <monitor/mos6502/asm.h>
#include <monitor/mos6502/cpu_reg_val.h>

#include <stdint.h>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <sstream>

#include "cli.h"

using namespace monitor::mos6502;

namespace nes {
namespace monitor {

using namespace ::cli;

cli::cli(struct model& model)
    : model(model)
{
    command::define("md", [this](const command::args_t& args) {
        return mem_display(args); });
    command::define("mw", [this](const command::args_t& args) {
        return mem_write(args); });
    command::define("dis", [this](const command::args_t& args) {
        return inst_display(args); });

    command::define("halt", [this](const command::args_t&) {
        return halt(); });
    command::define("step", [this](const command::args_t& args) {
        return step(args); },
        command::repeatable);
    command::define("cont", [this](const command::args_t&) {
        return cont(); },
        command::repeatable);
    command::define("where", [this](const command::args_t&) {
        return where(); });
    command::define("bp", [this](const command::args_t& args) {
        return breakpoints(args); });
    command::define("fstep", [this](const command::args_t&) {
        return fstep(); });
}

std::string cli::disassemble_to_str(const inst& i)
{
    std::stringstream ss;
    auto ip = i.addr();
    ss << std::hex << std::uppercase << std::setfill('0')
       << std::right << std::setw(4) << unsigned(ip) << ": ";
    for (auto b : i.bytes())
        ss << std::setw(2) << (unsigned)b;
    ss << std::setfill(' ') << std::setw(1 + 2 * (3 - i.size())) << ""
       << i.str();
    return ss.str();
}

void cli::disassemble(address_t pc, int num)
{
    auto insts = model.get_insts(pc, num, 0);
    for (auto& i : insts)
        std::cout << disassemble_to_str(i) << std::endl;
}

int cli::mem_display(const command::args_t& args)
{
    auto it = args.begin();
    if (args.end() - it < 1)
        throw std::range_error("missing argument");
    auto addr = memaddr(strtoul(it->c_str(), NULL, 0));
    memptr mp{addr};

    unsigned long len = 1;
    it++;
    if (it < args.end())
        len = strtoul(it->c_str(), NULL, 0);
    if (len >= 0x10000)
        throw std::range_error("length too long");

    auto data = std::unique_ptr<uint8_t[]>(new uint8_t[len]);
    for (unsigned long i = 0; i < len; i++)
        data[i] = model.get_byte(mp + i);

    for (auto& line : dump_to_str(&data[0], len, addr))
        std::cout << line << std::endl;

    return 0;
}

int cli::mem_write(const command::args_t& args)
{
    auto it = args.begin();
    if (args.end() - it < 2)
        throw std::range_error("missing argument");
    auto addr = memaddr(strtoul(it->c_str(), NULL, 0));
    it++;
    memptr mp{addr};

    for (; it != args.end(); it++) {
        uint8_t data = strtoul(it->c_str(), NULL, 0);
        *(mp++) = data;
    }

    return 0;
}

int cli::inst_display(const command::args_t& args)
{
    if (args.size() < 1)
        throw std::range_error("missing argument");
    auto addr = memaddr(strtoul(args[0].c_str(), NULL, 0));
    int num = 1;
    if (args.size() >= 2)
        num = strtoul(args[1].c_str(), NULL, 0);

    disassemble(addr, num);
    return 0;
}

int cli::halt()
{
    if (!model.halted()) {
        model.halt();
        while (!model.halted())
            pthread_yield();
    }
    return where();
}

int cli::step(const command::args_t& args)
{
    if (!model.halted())
        throw std::runtime_error("machine not stopped");

    bool until = false;
    uint16_t until_pc;

    auto it = args.begin();
    while (it != args.end()) {
        if (*it == "--until") {
            until = true;
            it++;
            if (it == args.end())
                throw std::range_error("missing argument");
            until_pc = strtoul(it->c_str(), NULL, 0);
            it++;
        }
    }

    for (;;) {
        model.step();
        while (!model.halted())
            pthread_yield();
        where();

        if (!until)
            break;
        cpu_regs regs, last_regs;
        model.get_regs(regs, last_regs);
        if (regs.pc == until_pc)
            break;
    }
    return 0;
}

int cli::cont()
{
    if (!model.halted())
        throw std::runtime_error("machine not stopped");

    model.resume();
    return 0;
}

static std::string ppu_pos_to_str(const cli::model::ppu_pos_t& pos)
{
    std::stringstream ss;
    ss << std::setfill('0') << std::right
       << 'C' << std::setw(3) << pos.col
       << ",R" << std::setw(3) << pos.row
       << ",F" << std::setw(5) << pos.frame;
    return ss.str();
}

int cli::where()
{
    if (!model.halted())
        throw std::runtime_error("machine not stopped");

    cpu_regs regs, last_regs;
    model.get_regs(regs, last_regs);
    auto insts = model.get_insts(regs.pc, 1, 0);
    auto inst_str = disassemble_to_str(insts[0]);
    model::ppu_pos_t ppu_pos;
    bool ppu_pos_known;
    model.get_ppu_pos(ppu_pos, ppu_pos_known);
    std::cout << std::left << std::setw(30) << inst_str << std::right
              << " A=" << reg_val_of(regs.a)
              << " X=" << reg_val_of(regs.x)
              << " Y=" << reg_val_of(regs.y)
              << " PF=" << reg_val<pf_t>{regs};
    if (ppu_pos_known)
        std::cout << "  PPU=" << ppu_pos_to_str(ppu_pos)
                  << "   CCy=" << model.get_cpu_cyc();
    std::cout << "\n";

    return 0;
}

int cli::fstep()
{
    if (!model.halted())
        halt();

    auto brk_en = model.break_get_enable();
    auto brk_trig = model.break_get_trigger();

    std::cout << "Press any key to step to the next frame.\n";
    for (;;) {
        where();
        ::cli::manager::run("ctlr");

        auto c = ::cli::manager::getch();
        if (c == 'q')
            break;

        model::ppu_pos_t ppu_pos;
        bool ppu_pos_known;
        model.get_ppu_pos(ppu_pos, ppu_pos_known);
        if (!ppu_pos_known)
            throw std::runtime_error("PPU position unknown");

        icd_match::trigger trig {};
        trig.set_sync(1);
        trig.set_frame(ppu_pos.frame + 1);
        trig.set_row(241);
        model.break_set_trigger(trig);
        model.break_set_enable(true);
        model.resume();

        while (!model.halted())
            pthread_yield();
    }

    model.break_set_trigger(brk_trig);
    model.break_set_enable(brk_en);

    return 0;
}

int cli::breakpoints(const command::args_t& args)
{
    if (args.size() < 1)
        return breakpoints_list();
    auto it = args.begin();
    auto sub = *it;
    command::args_t sub_args(++it, args.end());
    if (sub == "set")
        return breakpoints_set(sub_args);
    else if (sub == "clear")
        return breakpoints_clear();
    else if (sub == "toggle")
        return breakpoints_toggle(sub_args);
    else if (sub == "list")
        return breakpoints_list();
    throw std::runtime_error("unrecognized subcommand");
}

int cli::breakpoints_list()
{
    auto en = model.break_get_enable();
    auto trig = model.break_get_trigger();
    if (en)
        std::cout << "Break on " << trig.to_string() << '\n';
    else
        std::cout << "No breakpoints\n";
    return 0;
}

int cli::breakpoints_set(const command::args_t& args)
{
    icd_match::trigger trig {};
    if (args.size() < 1)
        throw std::range_error("missing arguments");
    auto it = args.begin();
    while (it != args.end()) {
        if (*it == "--pc") {
            it++;
            if (it == args.end())
                throw std::range_error("missing argument");
            auto v = strtoul(it->c_str(), NULL, 0);
            it++;
            trig.set_a(v);
            trig.set_sync(1);
        }
        else if (*it == "--a") {
            it++;
            if (it == args.end())
                throw std::range_error("missing argument");
            auto v = strtoul(it->c_str(), NULL, 0);
            it++;
            trig.set_a(v);
        }
        else if (*it == "--db") {
            it++;
            if (it == args.end())
                throw std::range_error("missing argument");
            auto v = strtoul(it->c_str(), NULL, 0);
            it++;
            trig.set_db(v);
        }
        else if (*it == "--rw") {
            it++;
            if (it == args.end())
                throw std::range_error("missing argument");
            auto v = strtoul(it->c_str(), NULL, 0);
            it++;
            trig.set_rw(v);
        }
        else if (*it == "--col") {
            it++;
            if (it == args.end())
                throw std::range_error("missing argument");
            auto v = strtoul(it->c_str(), NULL, 0);
            it++;
            trig.set_col(v);
        }
        else if (*it == "--row") {
            it++;
            if (it == args.end())
                throw std::range_error("missing argument");
            auto v = strtoul(it->c_str(), NULL, 0);
            it++;
            trig.set_row(v);
        }
        else if (*it == "--frame") {
            it++;
            if (it == args.end())
                throw std::range_error("missing argument");
            auto v = strtoul(it->c_str(), NULL, 0);
            it++;
            trig.set_frame(v);
        }
        else {
            throw std::runtime_error("unrecognized argument");
        }
    }
    model.break_set_trigger(trig);
    model.break_set_enable(true);
    return breakpoints_list();
}

int cli::breakpoints_clear()
{
    model.break_set_enable(false);
    return breakpoints_list();
}

int cli::breakpoints_toggle(const command::args_t& args)
{
    if (args.size() < 1)
        throw std::range_error("missing argument");
    auto addr = memaddr(strtoul(args[0].c_str(), NULL, 0));
    model.toggle_break(addr);
    return breakpoints_list();
}

} // namespace monitor
} // namespace nes
