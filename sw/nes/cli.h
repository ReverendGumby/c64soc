#pragma once

#include <cli/command.h>

#include "cart.h"

namespace nes {

class sys;

class cli
{
public:

    cli(class sys&);

private:
    int reset(const ::cli::command::args_t& args);

    int ppu_mem_display(const ::cli::command::args_t& args);
    int ppu_mem_write(const ::cli::command::args_t& args);
    int ppu_dump_state(const ::cli::command::args_t& args);

    int ctlr(const ::cli::command::args_t& args);

    int mmu(const ::cli::command::args_t& args);

    class sys& sys;
};

} // namespace nes
