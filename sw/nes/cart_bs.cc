#include "cart_bs.h"

#include "crt.h"
#include "cart.h"
#include "nesbus.h"

#include <util/iomem.h>
#include <util/debug.h>

#define DBG_LVL 4
#define DBG_TAG "CART_BS"

namespace nes {

cart_bs::cart_bs(cart& cart)
    : _cart(cart)
{
}

void cart_bs::init()
{
}

unsigned cart_bs::bs_to_bank(const volatile uint32_t* bs) const
{
    const volatile uint32_t* const bs_start = &NESBUS_CART_BS_REG(0);
    unsigned bank = (bs - bs_start) * sizeof(uint32_t) / _cart.geom.bank_size;
    return bank;
}

volatile uint32_t* cart_bs::bank_to_bs(unsigned bank) const
{
    volatile uint32_t* const bs_start = &NESBUS_CART_BS_REG(0);
    auto bs = bs_start + (bank * _cart.geom.bank_size) / sizeof(uint32_t);
    return bs;
}

void cart_bs::zero(volatile uint32_t* bs, size_t len)
{
    auto len_words = len / sizeof(uint32_t);
    DBG_PRINTF(5, "iowrite32_fill(%p, 0, 0x%x)\n", bs, len_words);
    iowrite32_fill(bs, 0, len_words);
}

void cart_bs::copy_in(volatile uint32_t* bs, const void* data, size_t len)
{
    auto len_words = len / sizeof(uint32_t);
    DBG_PRINTF(5, "iowrite32_copy(%p, %p, 0x%x)\n", bs, data, len_words);
    iowrite32_copy(bs, data, len_words);
}

void cart_bs::copy_out(void* data, const volatile uint32_t* bs, size_t len)
{
    auto len_words = len / sizeof(uint32_t);
    ioread32_copy(data, bs, len_words);
}

} // namespace nes
