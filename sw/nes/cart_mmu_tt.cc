#include "cart_mmu_tt.h"

#include "crt.h"
#include "cart.h"
#include "nesbus.h"

#include <util/debug.h>

#define DBG_LVL 4
#define DBG_TAG "CART_MT"

namespace nes {

// From cart_bop.vh
enum {
    BID_PRG = 1,
    BID_CHR = 2,
};

enum {
    CID_PRGROM = 1,
    CID_PRGRAM = 2,
    CID_CHR = 3,
};

cart_mmu_tt::cart_mmu_tt(cart& cart)
    : _cart(cart)
{
}

void cart_mmu_tt::init()
{
    _bank_size = _cart.geom.bank_size;

    for (unsigned i = 0; i < _cart.geom.num_banks; i++)
        _tt.emplace_back(tt_el_t {nullptr, 0});
}

void cart_mmu_tt::reset()
{
    clear(0, _cart.geom.num_banks);
}

std::vector<std::pair<unsigned, cart_mmu_tt::tt_el_t>>
cart_mmu_tt::get_all()
{
    decltype(get_all()) ret;
    ret.reserve(_tt.size());
    for (const auto& x : _tt) {
        unsigned bank = &x - &_tt[0];
        ret.emplace_back(bank, x);
    }
    return ret;
}

const cart_mmu_tt::tt_el_t& cart_mmu_tt::get(unsigned bank)
{
    return _tt[bank];
}

void cart_mmu_tt::set(const Chip& chip, unsigned base,
                      unsigned first_bank, size_t num_banks)
{
    const struct {
        uint8_t bid, cid;
    } ids[] = {
        [Chip::PRGROM] = { BID_PRG, CID_PRGROM },
        [Chip::PRGRAM] = { BID_PRG, CID_PRGRAM },
        [Chip::CHAROM] = { BID_CHR, CID_CHR },
        [Chip::CHARAM] = { BID_CHR, CID_CHR },
    };

    auto bank = first_bank;
    auto type = chip.type();
    auto& id = ids[type];

    while (num_banks--) {
        auto& el = _tt[bank];
        el.chip = &chip;
        el.base = base;

        uint32_t dat = (id.cid << 20) | (id.bid << 16) | base;
        DBG_PRINTF(5, "NESBUS_CART_MMU_TT[0x%x] = 0x%lx\n", bank, dat);
        NESBUS_MMU_TT_IDX = bank;
        NESBUS_MMU_TT_DAT = dat;

        base ++;
        bank ++;
    }
}

void cart_mmu_tt::clear(unsigned first_bank, size_t num_banks)
{
    auto bank = first_bank;

    while (num_banks--) {
        auto& el = _tt[bank];
        el.chip = nullptr;
        el.base = 0;

        uint32_t dat = 0;
        DBG_PRINTF(5, "NESBUS_CART_MMU_TT[0x%x] = 0x%lx\n", bank, dat);
        NESBUS_MMU_TT_IDX = bank;
        NESBUS_MMU_TT_DAT = dat;

        bank ++;
    }
}

} // namespace nes
