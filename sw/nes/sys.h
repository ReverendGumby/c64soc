#ifndef SYS_H__
#define SYS_H__

#include <stdint.h>
#include "mem.h"
#include "cart.h"
#include "ctlr.h"
#include "ppu.h"
#include "joy.h"
#include "icd.h"
#include "cli.h"

namespace plat {
class manager;
}

namespace ui {
class hid_manager;
}

namespace nes {

class sys
{
public:
    sys(plat::manager&, ui::hid_manager&);
    void init();

    void reset_all(bool assert);
    void reset_nes(bool assert);

    void power_cycle_nes();

    void pause();
    void resume();
    bool is_paused() const;

    plat::manager& plat_mgr;

    class mem mem;
    class cart cart;
    class ctlr ctlr;
    class ppu ppu;
    class joy joy;
    class icd icd;

private:
    class cli cli;

    int pause_count;
    bool reset_ram_pending;
    bool cold_reset;

    uint8_t read_cpu_reg(uint8_t reg);
    void reset_ram();
    void reset_ram_if_pending();
    void reset_ram_now();
};

} // namespace nes

#endif /* SYS_H__ */
