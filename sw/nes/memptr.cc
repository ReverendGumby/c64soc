#include <stdexcept>
#include "util/string.h"
#include "util/debug.h"
#include "nesbus.h"
#include "memptr.h"

#define DBG_LVL 1
#define DBG_TAG "MEMP"

namespace nes {

const char* name(memtype e)
{
    switch (e)
    {
    case memtype::cpu: return "cpu";
    case memtype::ppu: return "ppu";
    }
    return "???";
}

std::string memptr::to_string() const
{
    return string_printf("(%04X,%s)", (unsigned)addr(), name(type()));
}

memval memptr::operator*() const
{
    return memval{*this};
}

memval memptr::operator[](int i) const
{
    auto temp = *this + i;
    return *temp;
}

volatile uint8_t* memval::get_base() const
{
    return (_p.type() == memtype::cpu) ? NESBUS_CPU_MEM : NESBUS_PPU_MEM;
}

memval::operator uint8_t()
{
    auto v = get_base()[_p.addr()];
    DBG_PRINTF_NOFUNC(10, "R [%s] = %02X\n", _p.to_string().c_str(), (unsigned)v);
    return v;
}

memval& memval::operator=(uint8_t v)
{
    DBG_PRINTF_NOFUNC(10, "W [%s] = %02X\n", _p.to_string().c_str(), (unsigned)v);
    get_base()[_p.addr()] = v;
    return *this;
}

memval& memval::operator^=(uint8_t w)
{
    uint8_t v = *this;
    *this = v ^ w;
    return *this;
}

} // namespace nes
