#ifndef __CART_H__
#define __CART_H__

#include <stdexcept>
#include <memory>
#include <mutex>
#include <stdint.h>
#include "cart_bs.h"
#include "cart_mmu_tt.h"
#include "cart_loader/cart_loader.h"
#include "crt.h"
#include "util/pollable.h"
#include "util/timer.h"
#include "util/listener.h"
#include <ui/hmi_cart_listener.h>

namespace nes {

namespace _cart {

class listener
{
public:
    virtual void cart_modified(bool) = 0;
};

} // namespace _cart

struct cart_exception : public std::runtime_error
{
    cart_exception(const char* what_arg) : std::runtime_error(what_arg) {}
};

class cart : public talker<_cart::listener>,
             public talker<ui::hmi_cart_listener>,
             private pollable
{
public:
    using PCrt = std::shared_ptr<Crt>;
    using listener = _cart::listener;

    cart();

    void init();
    void reset();
    void load(PCrt);
    PCrt save();
    void reset_ram();

    bool is_modified() const { return _modified; }
    bool can_save() const;
    void set_battery_disabled(bool);

    struct {
        size_t num_banks;
        size_t bank_size;
    } geom;

    cart_bs bs;
    cart_mmu_tt mmu_tt;

private:
    using Cldr = cart_loader::cart_loader;

    Cldr* new_loader();

    // interface for pollable
    virtual const char* get_poll_name() const { return "cart"; }
    virtual void poll();

    void set_modified(bool);

    std::mutex _mtx;
    PCrt _crt;
    std::unique_ptr<Cldr> _cldr;
    timer _write_tmr;
    bool _modified = false;
    bool _battery_disabled = false;
};

} // namespace nes

#endif /* __CART_H__ */
