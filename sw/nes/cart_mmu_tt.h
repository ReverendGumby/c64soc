#pragma once

#include <vector>
#include <utility>
#include <stdint.h>

namespace nes {

class cart;
class Chip;

class cart_mmu_tt {
public:
    struct tt_el_t {
        const Chip* chip;
        unsigned base;
    };

    cart_mmu_tt(cart&);

    void init();
    void reset();

    std::vector<std::pair<unsigned, tt_el_t>> get_all();
    const tt_el_t& get(unsigned bank);

    void set(const Chip& chip, unsigned base, unsigned first_bank,
             size_t num_banks);
    void clear(unsigned first_bank, size_t num_banks);

private:
    cart& _cart;
    size_t _bank_size;
    std::vector<tt_el_t> _tt;
};

} // namespace nes
