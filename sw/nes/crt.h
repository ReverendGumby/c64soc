#ifndef __CRT_H__
#define __CRT_H__

#include <stdint.h>
#include <stddef.h>
#include <string>
#include <vector>
#include <memory>
#include <stdexcept>

namespace nes {

struct Crt_format_error : public std::runtime_error
{
    Crt_format_error(const char *what_arg)
        : std::runtime_error {what_arg} {}
};

class Chip {
public:
    typedef enum {
        PRGROM,
        PRGRAM,
        CHAROM,
        CHARAM,
    } type_t;

    Chip(type_t type, uint8_t *bin, size_t len);
    Chip(const Chip &that) = default;
    Chip(Chip &&that) = default;
    virtual ~Chip() = default;
    Chip& operator=(const Chip &that) = default;

    type_t type() const { return _type; }
    bool is_ram() const;
    const char* name() const;
    uint8_t *data() const { return _data; }
    size_t len() const { return _len; }

private:
    type_t _type;
    uint8_t* _data;
    size_t _len;
};

class Crt {
public:
    using Buf = std::unique_ptr<uint8_t[]>;
	using ChipList = std::vector<Chip>;

    Crt(Buf&& bin, size_t len);

    const std::string& pcb_class() const { return _pcb_class; }
    const ChipList &chips() const { return _chips; }
    uint8_t mapper() const { return _mapper; }
    bool has_battery() const { return _battery; }
    bool is_mirroring_vertical() const { return _vertical_mirroring; }
    int chip_size_kb(Chip::type_t) const;

    void load_prgram(Buf&& bin, size_t len);
    void save_prgram(Buf& bin, size_t& len);

private:
    void patch_problems();
    ChipList::iterator find_prgram();

    Buf _bin;
    size_t _bin_len;
    std::string _pcb_class;
    ChipList _chips;
    uint8_t _mapper;
    bool _vertical_mirroring;
    bool _battery;
    Buf _prgram_bin;
};

} // namespace nes

#endif /* __CRT_H__ */
