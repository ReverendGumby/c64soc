#include <sstream>
#include <iomanip>
#include <mutex>
#include "nesbus.h"

#include "icd_match.h"

namespace nes {

std::mutex icd_mtx;

bool icd_match::trigger::is_a_set() const
{
    return is_set(NESBUS_ICD_MATCH_REG_DL_CPU_A);
}

bool icd_match::trigger::is_db_set() const
{
    return is_set(NESBUS_ICD_MATCH_REG_DL_CPU_DB);
}

bool icd_match::trigger::is_rw_set() const
{
    return is_set(NESBUS_ICD_MATCH_REG_DL_CPU_RW);
}

bool icd_match::trigger::is_sync_set() const
{
    return is_set(NESBUS_ICD_MATCH_REG_DL_CPU_SYNC);
}

bool icd_match::trigger::is_col_set() const
{
    return is_set(NESBUS_ICD_MATCH_REG_DH_PPU_COL, 1);
}

bool icd_match::trigger::is_row_set() const
{
    return is_set(NESBUS_ICD_MATCH_REG_DH_PPU_ROW, 1);
}

bool icd_match::trigger::is_frame_set() const
{
    return is_set(NESBUS_ICD_MATCH_REG_DH_PPU_FRAME, 1) && is_set(NESBUS_ICD_MATCH_REG_DL_PPU_FRAME, 0);
}

uint16_t icd_match::trigger::get_a() const
{
    return uint16_t(get(NESBUS_ICD_MATCH_REG_DL_CPU_A) >> NESBUS_ICD_MATCH_REG_DL_CPU_A_S);
}

uint8_t icd_match::trigger::get_db() const
{
    return uint8_t(get(NESBUS_ICD_MATCH_REG_DL_CPU_DB) >> NESBUS_ICD_MATCH_REG_DL_CPU_DB_S);
}

bool icd_match::trigger::get_rw() const
{
    return get(NESBUS_ICD_MATCH_REG_DL_CPU_RW);
}

bool icd_match::trigger::get_sync() const
{
    return get(NESBUS_ICD_MATCH_REG_DL_CPU_SYNC);
}

uint16_t icd_match::trigger::get_col() const
{
    return get(NESBUS_ICD_MATCH_REG_DH_PPU_COL, 1) >> NESBUS_ICD_MATCH_REG_DH_PPU_COL_S;
}

uint16_t icd_match::trigger::get_row() const
{
    return get(NESBUS_ICD_MATCH_REG_DH_PPU_ROW, 1) >> NESBUS_ICD_MATCH_REG_DH_PPU_ROW_S;
}

uint32_t icd_match::trigger::get_frame() const
{
    return get(NESBUS_ICD_MATCH_REG_DH_PPU_FRAME, 1) >> NESBUS_ICD_MATCH_REG_DH_PPU_FRAME_S
        | (uint32_t(get(NESBUS_ICD_MATCH_REG_DL_PPU_FRAME, 0) >> NESBUS_ICD_MATCH_REG_DL_PPU_FRAME_S) << 14);
}

void icd_match::trigger::set_a(uint16_t v)
{
    uint32_t bits = uint32_t(v) << NESBUS_ICD_MATCH_REG_DL_CPU_A_S;
    uint32_t mask = NESBUS_ICD_MATCH_REG_DL_CPU_A;
    set(bits, mask);
}

void icd_match::trigger::set_db(uint8_t v)
{
    uint32_t bits = uint32_t(v) << NESBUS_ICD_MATCH_REG_DL_CPU_DB_S;
    uint32_t mask = NESBUS_ICD_MATCH_REG_DL_CPU_DB;
    set(bits, mask);
}

void icd_match::trigger::set_rw(bool v)
{
    uint32_t mask = NESBUS_ICD_MATCH_REG_DL_CPU_RW;
    uint32_t bits = v * mask;
    set(bits, mask);
}

void icd_match::trigger::set_sync(bool v)
{
    uint32_t mask = NESBUS_ICD_MATCH_REG_DL_CPU_SYNC;
    uint32_t bits = v * mask;
    set(bits, mask);
}

void icd_match::trigger::set_col(uint16_t v)
{
    uint32_t bits = uint32_t(v) << NESBUS_ICD_MATCH_REG_DH_PPU_COL_S;
    uint32_t mask = NESBUS_ICD_MATCH_REG_DH_PPU_COL;
    set(bits, mask, 1);
}

void icd_match::trigger::set_row(uint16_t v)
{
    uint32_t bits = uint32_t(v) << NESBUS_ICD_MATCH_REG_DH_PPU_ROW_S;
    uint32_t mask = NESBUS_ICD_MATCH_REG_DH_PPU_ROW;
    set(bits, mask, 1);
}

void icd_match::trigger::set_frame(uint32_t v)
{
    uint32_t bits = v << NESBUS_ICD_MATCH_REG_DH_PPU_FRAME_S;
    uint32_t mask = NESBUS_ICD_MATCH_REG_DH_PPU_FRAME;
    set(bits, mask, 1);
    bits = (v >> 14) << NESBUS_ICD_MATCH_REG_DL_PPU_FRAME_S;
    mask = NESBUS_ICD_MATCH_REG_DL_PPU_FRAME;
    set(bits, mask, 0);
}

void icd_match::trigger::clear_a()
{
    clear(NESBUS_ICD_MATCH_REG_DL_CPU_A);
}

void icd_match::trigger::clear_db()
{
    clear(NESBUS_ICD_MATCH_REG_DL_CPU_DB);
}

void icd_match::trigger::clear_rw()
{
    clear(NESBUS_ICD_MATCH_REG_DL_CPU_RW);
}

void icd_match::trigger::clear_sync()
{
    clear(NESBUS_ICD_MATCH_REG_DL_CPU_SYNC);
}

void icd_match::trigger::clear_col()
{
    clear(NESBUS_ICD_MATCH_REG_DH_PPU_COL, 1);
}

void icd_match::trigger::clear_row()
{
    clear(NESBUS_ICD_MATCH_REG_DH_PPU_ROW, 1);
}

void icd_match::trigger::clear_frame()
{
    clear(NESBUS_ICD_MATCH_REG_DH_PPU_FRAME, 1);
    clear(NESBUS_ICD_MATCH_REG_DL_PPU_FRAME, 0);
}

std::string icd_match::trigger::to_string() const
{
    std::stringstream ss;
    ss << std::setfill('0');
    if (is_a_set())
        ss << "A=" << std::setw(4) << std::hex << get_a();
    if (is_db_set()) {
        if (ss.tellp())
            ss << ", ";
        ss << "DB=" << std::setw(2) << std::hex << get_db();
    }
    if (is_rw_set()) {
        if (ss.tellp())
            ss << ", ";
        ss << "RW=" << (get_rw() ? 'R' : 'W');
    }
    if (is_sync_set()) {
        if (ss.tellp())
            ss << ", ";
        ss << "SYNC=" << std::dec << get_sync();
    }
    if (is_col_set()) {
        if (ss.tellp())
            ss << ", ";
        ss << "COL=" << std::dec << get_col();
    }
    if (is_row_set()) {
        if (ss.tellp())
            ss << ", ";
        ss << "ROW=" << std::dec << get_row();
    }
    if (is_frame_set()) {
        if (ss.tellp())
            ss << ", ";
        ss << "FRAME=" << std::dec << get_frame();
    }
    return ss.str();
}

bool icd_match::trigger::is_set(uint32_t mask, int idx) const
{
    return (den[idx] & mask) == mask;
}

uint32_t icd_match::trigger::get(uint32_t mask, int idx) const
{
    return din[idx] & mask;
}

void icd_match::trigger::set(uint32_t bits, uint32_t mask, int idx)
{
    din[idx] = (din[idx] & ~mask) | (bits & mask);
    den[idx] |= mask;
}

void icd_match::trigger::clear(uint32_t mask, int idx)
{
    din[idx] &= ~mask;
    den[idx] &= ~mask;
}

//////////////////////////////////////////////////////////////////////

icd_match::icd_match(int idx)
    : _idx(idx)
{
}

icd_match::status icd_match::get_status()
{
    std::unique_lock<std::mutex> lck{icd_mtx};
    NESBUS_ICD_MATCH_SEL = _idx;

    NESBUS_ICD_MATCH_REG_SEL = NESBUS_ICD_MATCH_REG_SEL_STATUS;
    uint32_t reg = NESBUS_ICD_MATCH_REG_DOUT;

    status s;
    s.match_sel = (reg & NESBUS_ICD_MATCH_REG_STATUS_MATCH_SEL) >> NESBUS_ICD_MATCH_REG_STATUS_MATCH_SEL_S;
    s.num_match = (reg & NESBUS_ICD_MATCH_REG_STATUS_NUM_MATCH) >> NESBUS_ICD_MATCH_REG_STATUS_NUM_MATCH_S;

    return s;
}

bool icd_match::is_enabled()
{
    return NESBUS_ICD_CTRL & ((NESBUS_ICD_MATCH_ENABLE / 255) << _idx);
}

bool icd_match::is_triggered()
{
    return NESBUS_ICD_CTRL & ((NESBUS_ICD_MATCH_TRIGGER / 255) << _idx);
}

icd_match::data icd_match::get_data()
{
    std::unique_lock<std::mutex> lck{icd_mtx};
    NESBUS_ICD_MATCH_SEL = _idx;

    NESBUS_ICD_MATCH_REG_SEL = NESBUS_ICD_MATCH_REG_SEL_DLOUT;
    uint32_t dlout = NESBUS_ICD_MATCH_REG_DOUT;
    NESBUS_ICD_MATCH_REG_SEL = NESBUS_ICD_MATCH_REG_SEL_DHOUT;
    uint32_t dhout = NESBUS_ICD_MATCH_REG_DOUT;
    NESBUS_ICD_MATCH_REG_SEL = NESBUS_ICD_MATCH_REG_SEL_D2OUT;
    uint32_t d2out = NESBUS_ICD_MATCH_REG_DOUT;

    data d;
    d.a = (dlout & NESBUS_ICD_MATCH_REG_DL_CPU_A) >> NESBUS_ICD_MATCH_REG_DL_CPU_A_S;
    d.db = (dlout & NESBUS_ICD_MATCH_REG_DL_CPU_DB) >> NESBUS_ICD_MATCH_REG_DL_CPU_DB_S;
    d.rw = dlout & NESBUS_ICD_MATCH_REG_DL_CPU_RW;
    d.sync = dlout & NESBUS_ICD_MATCH_REG_DL_CPU_SYNC;
    d.frame = ((dlout & NESBUS_ICD_MATCH_REG_DL_PPU_FRAME) >> NESBUS_ICD_MATCH_REG_DL_PPU_FRAME_S) << 14;
    d.col = (dhout & NESBUS_ICD_MATCH_REG_DH_PPU_COL) >> NESBUS_ICD_MATCH_REG_DH_PPU_COL_S;
    d.row = (dhout & NESBUS_ICD_MATCH_REG_DH_PPU_ROW) >> NESBUS_ICD_MATCH_REG_DH_PPU_ROW_S;
    d.frame |= (dhout & NESBUS_ICD_MATCH_REG_DH_PPU_FRAME) >> NESBUS_ICD_MATCH_REG_DH_PPU_FRAME_S;
    d.cpu_cyc = d2out;

    return d;
}

void icd_match::set_trigger(const icd_match::trigger& t)
{
    std::unique_lock<std::mutex> lck{icd_mtx};
    NESBUS_ICD_MATCH_SEL = _idx;

    NESBUS_ICD_MATCH_REG_SEL = NESBUS_ICD_MATCH_REG_SEL_DLIN;
    NESBUS_ICD_MATCH_REG_DOUT = t.din[0];
    NESBUS_ICD_MATCH_REG_SEL = NESBUS_ICD_MATCH_REG_SEL_DHIN;
    NESBUS_ICD_MATCH_REG_DOUT = t.din[1];
    NESBUS_ICD_MATCH_REG_SEL = NESBUS_ICD_MATCH_REG_SEL_DLEN;
    NESBUS_ICD_MATCH_REG_DOUT = t.den[0];
    NESBUS_ICD_MATCH_REG_SEL = NESBUS_ICD_MATCH_REG_SEL_DHEN;
    NESBUS_ICD_MATCH_REG_DOUT = t.den[1];
}

void icd_match::disable()
{
    std::unique_lock<std::mutex> lck{icd_mtx};
    NESBUS_ICD_CTRL &= ~((NESBUS_ICD_MATCH_ENABLE / 255) << _idx);
}

void icd_match::enable()
{
    std::unique_lock<std::mutex> lck{icd_mtx};
    NESBUS_ICD_CTRL |= ((NESBUS_ICD_MATCH_ENABLE / 255) << _idx);
}

void icd_match::reset_triggered()
{
    std::unique_lock<std::mutex> lck{icd_mtx};
    auto ctrl = NESBUS_ICD_CTRL;
    auto bit = (NESBUS_ICD_MATCH_ENABLE / 255) << _idx;
    if (ctrl & bit) {
        // Clearing enable resets trigger.
        NESBUS_ICD_CTRL = ctrl & ~bit;
        NESBUS_ICD_CTRL = ctrl;
    }
}

} // namespace nes
