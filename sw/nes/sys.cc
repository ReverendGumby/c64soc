#include <stdexcept>
#include <stdio.h>
#include "util/string.h"
#include "ui/hid_manager.h"
#include "nesbus.h"
#include "sys.h"

namespace nes {

sys::sys(plat::manager& pm, ui::hid_manager& hm)
    : plat_mgr(pm),
      joy(hm, ctlr),
      cli(*this)
{
    pause_count = 0;
    reset_ram_pending = false;
    cold_reset = false;
}

void sys::init()
{
	uint32_t val;

	val = NESBUS_ID;
	if (val != NESBUS_ID_DATA) {
        auto why = string_printf("NESBUS ID mismatch: got %08lx, wanted %08lx", val, NESBUS_ID_DATA);
		throw std::runtime_error(why);
	}
    auto why = string_printf("NESBUS ID is %08lx", val);
    printf("%s\n", why.c_str());

    reset_all(true);

    mem.init();
    cart.init();
    ctlr.init();
    ppu.init();
    icd.init();
    joy.start();
}

void sys::reset_all(bool assert)
{
    if (assert)
        NESBUS_RESETS = 0;
    else
        NESBUS_RESETS = (uint32_t)-1;
}

void sys::reset_nes(bool assert)
{
    if (assert) {
        NESBUS_RESETS &= ~NESBUS_RESETS_NES;
        icd.resume(); // in case the ICD was asserting SYS_HOLD
        reset_ram();
    } else
        NESBUS_RESETS |= NESBUS_RESETS_NES;
}

void sys::power_cycle_nes()
{
    cold_reset = true;
    reset_nes(true);
    reset_nes(false);
    cold_reset = false;
}

bool sys::is_paused() const
{
    return pause_count;
}

void sys::pause()
{
    if (pause_count == 0)
    {
        icd.force_halt();
    }
    pause_count ++;
}

void sys::resume()
{
    pause_count --;
    if (pause_count == 0)
    {
        reset_ram_if_pending();
        icd.resume();
    }
}

void sys::reset_ram()
{
    if (is_paused())
        reset_ram_pending = true;
    else
        reset_ram_now();
}

void sys::reset_ram_if_pending()
{
    if (reset_ram_pending) {
        reset_ram_pending = false;
        reset_ram_now();
    }
}

void sys::reset_ram_now()
{
    if (cold_reset) {
        mem.reset();
        cart.reset_ram();
    }
}

} // namespace nes
