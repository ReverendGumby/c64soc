#ifndef NES_MEMPTR__H
#define NES_MEMPTR__H

#include <stdint.h>
#include <string>
#include "nesbus.h"

namespace nes {

using memaddr = uint16_t;

constexpr memaddr memaddr_min = 0x0000;
constexpr memaddr memaddr_max = 0xFFFF;

enum class memtype
{
    cpu,                                        // WRAM, PRGROM
    ppu,                                        // VRAM, CHAROM
};

const char* name(memtype);

class memval;

class memptr
{
public:
    memptr() {}
    constexpr memptr(memaddr a, memtype t) : _a(a), _t(t) {}
    constexpr memptr(memaddr a) : memptr(a, memtype::cpu) {}
    constexpr memptr(const memptr&) = default;

    memtype type() const { return _t; }
    memaddr addr() const { return _a; }

    memptr& operator+=(int i) { _a += i; return *this; }
    memptr& operator++() { *this += 1; return *this; }
    memptr operator++(int) { auto c = *this; *this += 1; return c; }

    memval operator*() const;
    memval operator[](int) const;

    std::string to_string() const;

private:
    memaddr _a;
    memtype _t;
};

inline memptr operator+(memptr p, int i)
{
    return p += i;
}

class memval
{
public:
    using value_type = uint8_t;

    explicit memval(const memptr& p) : _p(p) {}
    operator uint8_t();
    memval& operator=(uint8_t);
    memval& operator^=(uint8_t);

private:
    volatile uint8_t* get_base() const;

    memptr _p;
};

} // namespace nes

#endif /* NES_MEMPTR__H */
