#include "sys.h"
#include "memptr.h"

#include <cli/command.h>
#include <cli/manager.h>
#include <util/dump_to_str.h>
#include <util/reg_val.h>

#include <stdint.h>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>

#include "cli.h"

struct ppuctrl_t { nes::ppu_regs regs; };
template <>
std::string reg_val<ppuctrl_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << (r.ctrl.vbl_en ? 'V' : '_')
       << (r.ctrl.slave_mode ? 'P' : '_')
       << (r.ctrl.spr_size ? 'H' : '_')
       << (r.ctrl.bkg_pat ? 'B' : '_')
       << (r.ctrl.spr_pat ? 'S' : '_')
       << (r.ctrl.addr_inc ? 'I' : '_')
       << (r.bus_master.val.r.v ? 'Y' : '_')
       << (r.bus_master.val.r.h ? 'X' : '_');
    return ss.str();
}

struct ppumask_t { nes::ppu_regs regs; };
template <>
std::string reg_val<ppumask_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << (r.mask.emph & (1<<2) ? 'B' : '_')
       << (r.mask.emph & (1<<1) ? 'G' : '_')
       << (r.mask.emph & (1<<0) ? 'R' : '_')
       << (r.mask.spr_en ? 's' : '_')
       << (r.mask.bkg_en ? 'b' : '_')
       << (r.mask.spr_clip ? 'M' : '_')
       << (r.mask.bkg_clip ? 'm' : '_')
       << (r.mask.pal_mono ? 'G' : '_');
    return ss.str();
}

struct ppustat_t { nes::ppu_regs regs; };
template <>
std::string reg_val<ppustat_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << (r.stat.vbl_flag ? 'V' : '_')
       << (r.stat.spr0_hit ? 'S' : '_')
       << (r.stat.spr_overflow ? 'O' : '_')
       << ".....";
    return ss.str();
}

struct ctlr_t { uint8_t reg; };
template <>
std::string reg_val<ctlr_t>::to_str() const
{
    bool a = val.reg & (1<<0),
        b = val.reg & (1<<1),
        sl = val.reg & (1<<2),
        st = val.reg & (1<<3),
        u = val.reg & (1<<4),
        d = val.reg & (1<<5),
        l = val.reg & (1<<6),
        r = val.reg & (1<<7);

    std::stringstream ss;
    ss << (a ? 'A' : '_')
       << (b ? 'B' : '_')
       << (sl ? 's' : '_')
       << (st ? 'S' : '_')
       << (u ? 'U' : '_')
       << (d ? 'D' : '_')
       << (l ? 'L' : '_')
       << (r ? 'R' : '_');
    return ss.str();
}

namespace nes {

using namespace ::cli;

cli::cli(class sys& sys)
    : sys(sys)
{
    command::define("reset", [this](const command::args_t& args) {
        return reset(args); });

    command::define("pmd", [this](const command::args_t& args) {
        return ppu_mem_display(args); });
    command::define("pmw", [this](const command::args_t& args) {
        return ppu_mem_write(args); });
    command::define("pds", [this](const command::args_t& args) {
        return ppu_dump_state(args); });
    command::define("ctlr", [this](const command::args_t& args) {
        return ctlr(args); });

    command::define("mmu", [this](const command::args_t& args) {
        return mmu(args); });
}

int cli::reset(const command::args_t&)
{
    sys.reset_nes(true);
    sys.reset_nes(false);

    return 0;
}

int cli::ppu_mem_display(const command::args_t& args)
{
    auto it = args.begin();
    if (args.end() - it < 1)
        throw std::range_error("missing argument");
    auto addr = memaddr(strtoul(it->c_str(), NULL, 0));
    memptr mp{addr, memtype::ppu};

    unsigned long len = 1;
    it++;
    if (it < args.end())
        len = strtoul(it->c_str(), NULL, 0);

    uint8_t data[len];
    for (unsigned long i = 0; i < len; i++)
        data[i] = *(mp++);

    for (auto& line : dump_to_str(data, len, addr))
        std::cout << line << std::endl;

    return 0;
}

int cli::ppu_mem_write(const command::args_t& args)
{
    auto it = args.begin();
    if (args.end() - it < 2)
        throw std::range_error("missing argument");
    auto addr = memaddr(strtoul(it->c_str(), NULL, 0));
    it++;
    memptr mp{addr, memtype::ppu};

    for (; it != args.end(); it++) {
        uint8_t data = strtoul(it->c_str(), NULL, 0);
        *(mp++) = data;
    }

    return 0;
}

int cli::ppu_dump_state(const command::args_t& args)
{
    struct {
        bool def;
        bool all;
        bool reg;
        bool cnt;
        bool bm;
        bool pal;
        bool oam;
    } opts = {};

    if (!args.size())
        opts.def = true;
    else
        for (auto& arg : args) {
            if (arg == "--all")
                opts.all = true;
            if (arg == "--reg")
                opts.reg = true;
            if (arg == "--cnt")
                opts.cnt = true;
            if (arg == "--bm")
                opts.bm = true;
            if (arg == "--pal")
                opts.pal = true;
            if (arg == "--oam")
                opts.oam = true;
        }

    auto regs = sys.ppu.read_regs();

    if (opts.all || opts.reg || opts.def) {
        std::cout << "Registers: "
            << "  PPUCTRL=" << reg_val<ppuctrl_t>{regs}
            << "  PPUMASK=" << reg_val<ppumask_t>{regs}
            << "  PPUSTAT=" << reg_val<ppustat_t>{regs}
            << "\n";
    }
    if (opts.all || opts.cnt || opts.def) {
        std::cout << "Video counter: " << std::dec
                  << "  ROW=" << regs.video_counter.row
                  << "/0x" << reg_val_of(regs.video_counter.row)
                  << "  COL=" << regs.video_counter.col
                  << "/0x" << reg_val_of(regs.video_counter.col)
                  << "  FRAME=" << regs.video_counter.frame
                  << "\n";
    }
    if (opts.all || opts.bm || opts.def) {
        std::cout << "Bus master: "
                  << "  VAL=0x" << reg_val_of(regs.bus_master.val)
                  << "  VAC=0x" << reg_val_of(regs.bus_master.vac)
                  << "\n";
    }
    if (opts.all || opts.pal) {
        std::cout << "Palette:\n";
        for (auto& line : dump_to_str(regs.palette.pal, sizeof(regs.palette.pal), 0))
            std::cout << line << std::endl;
        std::cout << "\n";
    }
    if (opts.all || opts.oam) {
        std::cout << "OAM:\n";
        for (auto& line : dump_to_str(regs.oam.pri, sizeof(regs.oam.pri), 0))
            std::cout << line << std::endl;
        for (auto& line : dump_to_str(regs.oam.sec, sizeof(regs.oam.sec), 0 + NESBUS_PPU_MON_SEL_OAM_PRI_LEN))
            std::cout << line << std::endl;
        std::cout << "\n";
    }

    return 0;
}

int cli::ctlr(const command::args_t& args)
{
    auto cs = sys.ctlr.get();
    std::cout << "CTLR1=" << reg_val<ctlr_t>{cs.c1}
        << "  CTLR2=" << reg_val<ctlr_t>{cs.c2}
        << "\n";

    return 0;
}

int cli::mmu(const command::args_t& args)
{
    const auto& geom = sys.cart.geom;
    auto tt = sys.cart.mmu_tt.get_all();
    // Filter out clear entries
    tt.erase(
        std::remove_if(tt.begin(), tt.end(), [](const auto& x) {
            return !x.second.chip;
        }), tt.end());
    // Sort entries by chip type and base
    std::sort(tt.begin(), tt.end(), [](const auto& a, const auto& b) {
        auto ea = a.second, eb = b.second;
        auto ta = ea.chip->type(), tb = eb.chip->type();
        if (ta != tb)
            return ta < tb;
        return ea.base < eb.base;
    });
    for (const auto& i_el : tt) {
        auto i = i_el.first;
        const auto& el = i_el.second;
        if (!el.chip)
            continue;
        std::cout << "Bank " << std::setw(2) << i << ": "
                  << el.chip->name()
                  << ".(0x" << std::hex << el.base * geom.bank_size
                  << ",+0x" << geom.bank_size << std::dec << ")\n";
    }
    return 0;
}

} // namespace cli
