#pragma once

#include <stdint.h>
#include "xparameters.h"

namespace nes {

//``PERIPHERAL NESBUS

//////////////////////////////////////////////////////////////////////
// Bus control interface

//``BASE 32 CTRL NESBUS_CTRL

//``REGS s/_(\w+)_\1/_\1/ s/ICD_CTRL/ICD/

// Bits for NESBUS_CPU_REG_DOUT

//``SUBREG_FIELDS CPU_REG_SEL s/SEL_(\w+)_/\1_/

// Bits for NESBUS_ICD_MATCH register STATUS
//``FIELDS ICD_MATCH_REG_STATUS

// Bits for NESBUS_ICD_MATCH registers D[LH]IN, D[LH]EN, and D[LH]OUT
//``FIELDS ICD_MATCH_REG_DL
//``FIELDS ICD_MATCH_REG_DH

// Bits for NESBUS_PPU_MON_DOUT

//``SUBREG_FIELDS PPU_MON_SEL s/SEL_(\w+)_/\1_/

// Bits for NESBUS_APU_MON_DOUT

//``SUBREG_FIELDS APU_MON_SEL s/SEL_(\w+)_/\1_/

//////////////////////////////////////////////////////////////////////
// NES native memory bus interfaces

//``BASE 8 CPU_MEM
//``BASE 8 PPU_MEM

#define NESBUS_CPU_MEM          (& NESBUS_CPU_MEM_REG(0))
#define NESBUS_PPU_MEM          (& NESBUS_PPU_MEM_REG(0))

//////////////////////////////////////////////////////////////////////
// ROM cartridge control interface

//``BASE 32 CART_CTRL
//``REGS s/(\w{2,})_\1/\1/

//////////////////////////////////////////////////////////////////////
// ROM cartridge backing store interface

//``BASE 32 CART_BS

//////////////////////////////////////////////////////////////////////

} // namespace nes

// Local Variables:
// compile-command: "cd ../.. && python3 tools/gen_regs_h.py sw/nes/nesbus.h.i sw/nes/nesbus.h rtl/nes/nesbus.v rtl/nes/cart.v rtl/nes/icd.v rtl/mos6502/mos6502_core.vh rtl/rp2a03/*.v rtl/rp2c02/*.v"
// End:
