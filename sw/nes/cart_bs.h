#pragma once

#include <stddef.h>
#include <stdint.h>

namespace nes {

class cart;

class cart_bs {
public:
    cart_bs(cart&);

    void init();

    unsigned bs_to_bank(const volatile uint32_t* bs) const;
    volatile uint32_t* bank_to_bs(unsigned bank) const;

    void zero(volatile uint32_t* bs, size_t len);
    void copy_in(volatile uint32_t* bs, const void* data, size_t len);
    void copy_out(void* data, const volatile uint32_t* bs, size_t len);

private:
    cart& _cart;
};

} // namespace nes
