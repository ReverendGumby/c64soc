#ifndef __CART_LOADER__NORMAL_H__
#define __CART_LOADER__NORMAL_H__

/* Loader for a normal cartridge */

#include <stdexcept>
#include <stdint.h>
#include "cart_loader.h"

namespace nes {
namespace cart_loader {

class normal : public cart_loader
{
public:
    normal(PCrt&, cart&);

    void load() override;
    void save() override;
    void reset_ram(bool force_zero) override;

    void service_swap() override;

private:
    struct map_el_t {
        // BS / MMU_TT bank span
        struct {
            unsigned first = 0;
            size_t count = 0;
        } banks;
        // Chip data span
        const Chip* chip = nullptr;
        unsigned base;
        size_t len;
        // Attributes
        bool fixed = false;
    };

    void set_mirroring();
    void set_mapper();

    bool map_is_chip_mapped(const map_el_t&) const;
    map_el_t& map_alloc(const map_el_t&);
    size_t map_find_largest_free();
    void map_in(const map_el_t&);
    void map_out(size_t len);
    void swap_bank(size_t sbba, const Chip* chip, size_t len);

    size_t _sbba_len;
    size_t _num_banks;
    size_t _bank_size;
    std::vector<map_el_t> _map;
    bool _loaded;
    bool _swap_en;
    size_t _prg_sb_len;
    size_t _chr_sb_len;
    const Chip* _chip_prgrom;
    const Chip* _chip_charom;
    decltype(_map)::iterator _last_alloc;
};

} // namespace cart_loader
} // namespace nes

#endif
