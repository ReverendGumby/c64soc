#include "cart_loader.h"
#include "internal.h"

namespace nes {
namespace cart_loader {

void cart_loader::init()
{
    reset();
}

void cart_loader::reset()
{
    NESBUS_RESETS &= ~NESBUS_RESETS_CART;
    NESBUS_RESETS |= NESBUS_RESETS_CART;
}

cart_loader::cart_loader(PCrt& crt, cart& cart)
    : _crt(crt), _cart(cart)
{
    DBG_PRINTF(2, "\n");
}

cart_loader::~cart_loader()
{
    DBG_PRINTF(2, "\n");
}

void cart_loader::load()
{
}

void cart_loader::save()
{
}

void cart_loader::reset_ram(bool force_zero)
{
}

void cart_loader::service_swap()
{
}

} // namespace cart_loader
} // namespace nes
