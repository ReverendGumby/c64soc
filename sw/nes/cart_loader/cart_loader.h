#ifndef __CART_LOADER__CART_LOADER_H__
#define __CART_LOADER__CART_LOADER_H__

#include <stdexcept>
#include <memory>
#include "../crt.h"

namespace nes {

class cart;

namespace cart_loader {

struct cart_loader_exception : public std::runtime_error
{
    cart_loader_exception(const char* what_arg) : std::runtime_error(what_arg) {}
};

class cart_loader
{
public:
    using PCrt = std::shared_ptr<Crt>;

    static void init();

    cart_loader(PCrt&, cart&);
    virtual ~cart_loader();

    virtual void load();
    virtual void save();
    virtual void reset_ram(bool force_zero = false);

    virtual void service_swap();

protected:
    static void reset();

    PCrt _crt;
    cart& _cart;
};

} // namespace cart_loader
} // namespace nes

#endif
