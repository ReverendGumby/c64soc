#include "normal.h"

#include "internal.h"

#include <algorithm>
#include <stdint.h>
#include <util/iomem.h>
#include <util/math.h>

namespace nes {
namespace cart_loader {

normal::normal(PCrt& p, cart& c)
    : cart_loader(p, c)
{
    auto sbb_al = (NESBUS_MMU_CFG & NESBUS_MMU_CFG_SBB_AL) >> NESBUS_MMU_CFG_SBB_AL_S;
    _sbba_len = 1 << sbb_al;

    _num_banks = _cart.geom.num_banks;
    _bank_size = _cart.geom.bank_size;
    _loaded = false;
    _swap_en = false;
    _chip_prgrom = nullptr;
    _chip_charom = nullptr;
}

static void set_chip_config(Chip::type_t type, size_t len)
{
    auto addr_max = ceil_log2(len) - 1;

    auto chip_cfg = NESBUS_CHIP_CFG;

    switch (type) {
    case Chip::PRGROM: {
        chip_cfg &= ~NESBUS_CHIP_CFG_PRG_ROM_ADDR_MAX;
        chip_cfg |= addr_max << NESBUS_CHIP_CFG_PRG_ROM_ADDR_MAX_S;
        break;
    }
    case Chip::PRGRAM:
        chip_cfg &= ~NESBUS_CHIP_CFG_PRG_RAM_ADDR_MAX;
        chip_cfg |= addr_max << NESBUS_CHIP_CFG_PRG_RAM_ADDR_MAX_S;
        break;
    case Chip::CHAROM:
    case Chip::CHARAM:
        chip_cfg &= ~(NESBUS_CHIP_CFG_CHR_ADDR_MAX | NESBUS_CHIP_CFG_CHR_READ_ONLY);
        chip_cfg |= addr_max << NESBUS_CHIP_CFG_CHR_ADDR_MAX_S;
        if (type == Chip::CHAROM)
            chip_cfg |= NESBUS_CHIP_CFG_CHR_READ_ONLY;
        break;
        chip_cfg &= ~NESBUS_CHIP_CFG_CHR_ADDR_MAX;
        chip_cfg |= addr_max << NESBUS_CHIP_CFG_CHR_ADDR_MAX_S;
        break;
    default:
        break;
    }

    NESBUS_CHIP_CFG = chip_cfg;
    DBG_PRINTF(1, "NESBUS_CHIP_CFG = 0x%08lx\n", NESBUS_CHIP_CFG);
}

void normal::load()
{
    DBG_PRINTF(1, "entry\n");
    auto& crt = *_crt;

    set_mirroring();
    set_mapper();

    const size_t bs_size = _bank_size * _num_banks;

    size_t total_len = 0;

    auto &chips = crt.chips();
    for (auto &c : chips)
    {
        auto len = c.len();
        auto type = c.type();

        set_chip_config(type, len);

        map_el_t me;
        me.chip = &c;
        me.fixed = true;

        switch (type) {
        case Chip::PRGROM:
        case Chip::CHAROM: {
            if (_swap_en) {
                size_t sb_len;
                if (type == Chip::PRGROM) {
                    _chip_prgrom = &c;
                    sb_len = std::max(_bank_size, _prg_sb_len);
                    // Map in the last swap bank only.
                    me.len = sb_len;
                    if ((type == Chip::PRGROM
                         && (crt.mapper() == 4 || crt.mapper() == 206))
                        || (crt.pcb_class() == "SEROM")) {
                        // Map in the second-last bank, too.
                        me.len *= 2;
                    }
                    me.base = len - me.len;
                }
                else if (type == Chip::CHAROM) {
                    _chip_charom = &c;
                    sb_len = std::max(_bank_size, _chr_sb_len);
                    me.len = 0;
                }

                // Ensure that there will be room for at least one more.
                total_len += sb_len;
                break;
            } else
                [[fallthrough]];
        }
        case Chip::PRGRAM:
        case Chip::CHARAM:
            // Map in the entire chip.
            me.base = 0;
            me.len = len;
            break;
        default:
            throw cart_loader_exception("unsupported chip type");
        }

        total_len += me.len;
        if (total_len > bs_size)
            throw cart_loader_exception("unsupported total size");

        if (me.len)
            map_in(me);
    }

    _loaded = true;
}

void normal::set_mirroring()
{
    NESBUS_MIRRORING = _crt->is_mirroring_vertical() ?
        NESBUS_MIRRORING_VERT : NESBUS_MIRRORING_HORIZ;
    DBG_PRINTF(1, "NESBUS_MIRRORING = 0x%08lx\n", NESBUS_MIRRORING);
}

void normal::set_mapper()
{
    int sel = 0;

    auto& crt = *_crt;
    switch (crt.mapper()) {
    case 0: sel = NESBUS_MAPPER_SEL_BYPASS; break;
    case 2: sel = NESBUS_MAPPER_SEL_UXROM; break;
    case 1: {
        sel = NESBUS_MAPPER_SEL_MMC1;
        _swap_en = true;
        _prg_sb_len = 1 << 15;                  // max. of 16KB or 32KB bank
        _chr_sb_len = 1 << 13;                  // max. of 4KB or 8KB bank

        uint32_t mmc1_cfg = 0;
        auto pcb_class = crt.pcb_class();
        if (pcb_class == "SNROM")
            mmc1_cfg |= NESBUS_MMC1_CFG_SNROM;
        else if (pcb_class == "SEROM")
            mmc1_cfg |= NESBUS_MMC1_CFG_SEROM;
        NESBUS_MMC1_CFG = mmc1_cfg;
        DBG_PRINTF(1, "NESBUS_MMC1_CFG = 0x%08lx\n", NESBUS_MMC1_CFG);
        break;
    }
    case 4:
    case 206: {
        sel = NESBUS_MAPPER_SEL_MMC3_6;
        _swap_en = true;
        _prg_sb_len = 1 << 13;
        _chr_sb_len = 1 << 10;

        uint32_t mmc3_6_cfg = 0;
        if (crt.mapper() == 206)
            mmc3_6_cfg |= NESBUS_MMC3_6_CFG_NAMCOT;
        NESBUS_MMC3_6_CFG = mmc3_6_cfg;
        DBG_PRINTF(1, "NESBUS_MMC3_6_CFG = 0x%08lx\n", NESBUS_MMC3_6_CFG);
        break;
    }
    default:
        throw cart_loader_exception("unsupported mapper");
    }

    NESBUS_MAPPER_SEL = sel;
    DBG_PRINTF(1, "NESBUS_MAPPER_SEL = 0x%08lx\n", NESBUS_MAPPER_SEL);
}

void normal::save()
{
    DBG_PRINTF(1, "entry\n");

    for (size_t bank = 0; bank < _num_banks; bank++)
    {
        const auto &el = _cart.mmu_tt.get(bank);
        if (el.chip) {
            const Chip& c = *el.chip;
            auto data = c.data();
            if (c.is_ram() && data != nullptr) {
                data += el.base * _bank_size;
                auto* bs = _cart.bs.bank_to_bs(bank);
                _cart.bs.copy_out(data, bs, _bank_size);
            }
        }
    }
}

void normal::reset_ram(bool force_zero)
{
    DBG_PRINTF(1, "entry\n");

    for (size_t bank = 0; bank < _num_banks; bank++)
    {
        const auto &el = _cart.mmu_tt.get(bank);
        if (el.chip) {
            const Chip& c = *el.chip;
            if (c.is_ram()) {
                auto* bs = _cart.bs.bank_to_bs(bank);
                auto data = c.data();
                if (force_zero || data == nullptr) {
                    _cart.bs.zero(bs, _bank_size);
                } else {
                    data += el.base * _bank_size;
                    _cart.bs.copy_in(bs, data, _bank_size);
                }
            }
        }
    }
}

bool normal::map_is_chip_mapped(const map_el_t& in) const
{
    for (const auto& el : _map) {
        if (el.chip == in.chip && el.base == in.base)
            return true;
    }
    return false;
}

normal::map_el_t& normal::map_alloc(const map_el_t& in)
{
    unsigned first = 0;
    auto count = in.len / _bank_size;

    if (map_is_chip_mapped(in))
        throw cart_loader_exception("chip already mapped");

    auto i = _map.cbegin(), end = _map.cend();
    for (; i != end; i++) {
        if (first + count <= i->banks.first)
            break;
        first = i->banks.first + i->banks.count;
    }
    if (first + count > _num_banks)
        throw cart_loader_exception("out of backing store");

    _last_alloc = _map.insert(i, in);
    auto& me = *_last_alloc;
    me.banks.first = first;
    me.banks.count = count;
    DBG_PRINTF(10, "_map[%d] = {banks=(%u,%u), chip=%s.(0x%x,+0x%x), fixed=%d}\n",
               _last_alloc - _map.begin(), me.banks.first,
               me.banks.first + me.banks.count - 1,
               me.chip->name(), me.base, me.len, me.fixed);

    return me;
}

size_t normal::map_find_largest_free()
{
    size_t ret = 0;

    unsigned last = 0;
    for (const auto& me : _map) {
        ret = std::max(ret, me.banks.first - last);
        last = me.banks.first + me.banks.count;
    }
    ret = std::max(ret, _num_banks - last);

    return ret * _bank_size;
}

void normal::map_in(const map_el_t& me_in)
{
    auto& me = map_alloc(me_in);

    auto data = me.chip->data();
    if (data != nullptr) {
        auto bs = _cart.bs.bank_to_bs(me.banks.first);
        data += me.base;
        _cart.bs.copy_in(bs, data, me.len);
    }

    auto tt_base = me.base / _bank_size;
    _cart.mmu_tt.set(*me.chip, tt_base, me.banks.first, me.banks.count);
}

void normal::map_out(size_t len)
{
    // Is there enough space already?
    if (map_find_largest_free() >= len)
        return;

    // Find and free the next non-fixed map entry of equal size.
    // (Equal size prevents fragmentation.)
    auto i = _last_alloc + 1;
    do {
        if (i == _map.end())
            i = _map.begin();
        auto& me = *i;
        if (me.fixed)
            continue;
        if (me.banks.count * _bank_size == len) {
            _cart.mmu_tt.clear(me.banks.first, me.banks.count);
            _map.erase(i);
            DBG_PRINTF(10, "freed _map[%d]\n", i - _map.begin());
            return;
        }
    } while (++i != _last_alloc + 1);
}

void normal::swap_bank(size_t sbba, const Chip* chip, size_t len)
{
    auto base = (sbba * _sbba_len) % chip->len();
    len = std::max(_bank_size, len);
    base &= ~(len - 1); // lower bits of sbba may be 1s

    map_el_t me;
    me.chip = chip;
    me.base = base;
    me.len = len;

    if (!map_is_chip_mapped(me)) {
        map_out(len);
        map_in(me);
    }
}

void normal::service_swap()
{
    if (!(_loaded && _swap_en))
        return;

    auto prg_ready = (NESBUS_SWAP0 & NESBUS_SWAP0_PRG_READY) >> NESBUS_SWAP0_PRG_READY_S;
    auto chr_ready = (NESBUS_SWAP0 & NESBUS_SWAP0_CHR_READY) >> NESBUS_SWAP0_CHR_READY_S;

#define IF_READY_SBBA_SWAP(ready, ready_bit, swapx, swapx_mask, name, chip, len) \
    do {                                                                \
        if (((ready) & (1 << (ready_bit))) == 0) {                      \
            size_t sbba = ((swapx) & (swapx_mask)) >> (swapx_mask##_S); \
            DBG_PRINTF(4, name " = 0x%x\n", sbba);                      \
            swap_bank(sbba, chip, len);                                 \
        }                                                               \
    } while (0)

    IF_READY_SBBA_SWAP(prg_ready, 0, NESBUS_SWAP1, NESBUS_SWAP1_PRG_SBBA0, "prg0", _chip_prgrom, _prg_sb_len);
    IF_READY_SBBA_SWAP(prg_ready, 1, NESBUS_SWAP1, NESBUS_SWAP1_PRG_SBBA1, "prg1", _chip_prgrom, _prg_sb_len);

    IF_READY_SBBA_SWAP(chr_ready, 0, NESBUS_SWAP2, NESBUS_SWAP2_CHR_SBBA0, "chr0", _chip_charom, _chr_sb_len);
    IF_READY_SBBA_SWAP(chr_ready, 1, NESBUS_SWAP2, NESBUS_SWAP2_CHR_SBBA1, "chr1", _chip_charom, _chr_sb_len);
    IF_READY_SBBA_SWAP(chr_ready, 2, NESBUS_SWAP2, NESBUS_SWAP2_CHR_SBBA2, "chr2", _chip_charom, _chr_sb_len);
    IF_READY_SBBA_SWAP(chr_ready, 3, NESBUS_SWAP2, NESBUS_SWAP2_CHR_SBBA3, "chr3", _chip_charom, _chr_sb_len);
    IF_READY_SBBA_SWAP(chr_ready, 4, NESBUS_SWAP3, NESBUS_SWAP3_CHR_SBBA4, "chr4", _chip_charom, _chr_sb_len);
    IF_READY_SBBA_SWAP(chr_ready, 5, NESBUS_SWAP3, NESBUS_SWAP3_CHR_SBBA5, "chr5", _chip_charom, _chr_sb_len);

#undef IF_READY_SBBA_SWAP
}

} // namespace cart_loader
} // namespace nes
