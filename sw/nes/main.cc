/*
 * main.cc: main application for NES
*/

#include <memory>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <unistd.h>
#include "plat/audio.h"
#include "plat/manager.h"
#include "nes/sys.h"
#include "nes/cart_manager.h"
#include "nes/monitor/monitor.h"
#include "sd/sd_manager.h"
#include "fat/cli.h"
#include "ui/hid_manager.h"
#include "usb/usb_manager.h"
#include "ui/hmi_manager.h"
#include "util/pollable.h"
#include "util/kbd_shortcut.h"
#include "cli/manager.h"
#include "movie/manager.h"
#include "app/version.h"

static const plat::audio_config_t audio_cfg = {
    .sample_rate = 48000,
};
static const plat::manager::config_t plat_cfg = {
    .audio = audio_cfg,
};

int main()
{
    plat::manager plm(plat_cfg);
    plm.early_init();

    printf("c64soc\n"
           "SW version %s (@%s) build %s\n",
           sw_build_branch, sw_build_rev, sw_build_timestamp);

    plm.late_init();

    auto hw_ver = plm.get_rtl_version();
    printf("HW version %s (@%s) build %s\n",
           hw_ver.git_branch.c_str(), hw_ver.git_rev.c_str(), hw_ver.timestamp.c_str());

    usb::usb_manager um;
    um.init();

    sd::sd_manager sm;
    fat::cli::init();

    ui::hid_manager hid_mgr{};
    hid_mgr.listen(um);

    nes::sys sys(plm, hid_mgr);
    sys.init();

    nes::cart_manager cm(sys);

    ui::hmi_manager hmi_mgr(plm);
    hmi_mgr.listen(sys.cart);

    cli::manager clim;
    if (sm.is_card_inserted())
        clim.try_autoexec();
    clim.start();

    nes::monitor::monitor mon(sys);

    movie::manager mov_mgr{sys};

    plm.reset_video(false);
    plm.audio_up();
    sys.reset_all(false);

    printf("Polling...\n");
    //unsigned loops = 0;
    for (;;) {
        poll_manager_get().poll();
        plm.set_video_hdmi_mode(plm.get_sw(0)); // XXX
        pthread_yield();
    }

    return 0;
}
