#include "nesbus.h"

#include "ctlr.h"

namespace nes {

ctlr::ctlr()
{
}

void ctlr::init()
{
    reset();
}

void ctlr::reset()
{
    NESBUS_CTLR_MATRIX = 0;
}

void ctlr::set(const state_t& s)
{
    NESBUS_CTLR_MATRIX = s.c1 << 0 | s.c2 << 8;
}

ctlr::state_t ctlr::get()
{
    auto reg = NESBUS_CTLR_MATRIX;
    state_t s = {
        .c1 = uint8_t(reg >> 0),
        .c2 = uint8_t(reg >> 8),
    };
    return s;
}

} // namespace nes
