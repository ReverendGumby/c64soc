// Auto-generated: Do not edit.
// /Applications/Xcode.app/Contents/Developer/usr/bin/python3 tools/gen_regs_h.py sw/nes/nesbus.h.i sw/nes/nesbus.h rtl/nes/nesbus.v rtl/nes/cart.v rtl/nes/icd.v rtl/mos6502/mos6502_core.vh rtl/rp2a03/apu.v rtl/rp2a03/clkgen.v rtl/rp2a03/ctrl.v rtl/rp2a03/digi_mod_chan.v rtl/rp2a03/dmcdma.v rtl/rp2a03/filter.v rtl/rp2a03/filter_dec.v rtl/rp2a03/filter_resample_output.v rtl/rp2a03/noise_chan.v rtl/rp2a03/oamdma.v rtl/rp2a03/rp2a03.v rtl/rp2a03/square_chan.v rtl/rp2a03/triangle_chan.v rtl/rp2c02/bkg_renderer.v rtl/rp2c02/clkgen.v rtl/rp2c02/color_gen.v rtl/rp2c02/oam.v rtl/rp2c02/palette.v rtl/rp2c02/reg_file.v rtl/rp2c02/renderer.v rtl/rp2c02/rp2c02.v rtl/rp2c02/spr_renderer.v rtl/rp2c02/sprite_eval.v rtl/rp2c02/sync_gen.v rtl/rp2c02/video_counter.v rtl/rp2c02/vram_bus_master.v

#pragma once

#include <stdint.h>
#include "xparameters.h"

namespace nes {

#define NESBUS_BASE             XPAR_NESBUS_BASEADDR

//////////////////////////////////////////////////////////////////////
// Bus control interface

#define NESBUS_CTRL_BASE        (NESBUS_BASE + 0x00000)
#define NESBUS_CTRL_REG(off)    (*((volatile uint32_t *)(NESBUS_CTRL_BASE + (off))))

#define NESBUS_ID                   NESBUS_CTRL_REG(0x000)
#define NESBUS_ID_DATA                  0x4D455331UL

#define NESBUS_CTLR_MATRIX          NESBUS_CTRL_REG(0x040)

#define NESBUS_RESETS               NESBUS_CTRL_REG(0x050)
#define NESBUS_RESETS_NES               (1<<0)
#define NESBUS_RESETS_CART              (1<<2)

#define NESBUS_RESET_CTRL           NESBUS_CTRL_REG(0x054)
#define NESBUS_RESET_CTRL_COLD          (1<<0)

#define NESBUS_CPU_REG_SEL          NESBUS_CTRL_REG(0x060)
#define NESBUS_CPU_REG_SEL_PCL          0x1
#define NESBUS_CPU_REG_SEL_PCH          0x2
#define NESBUS_CPU_REG_SEL_PF           0x3     // Processor Flags
#define NESBUS_CPU_REG_SEL_S            0x4
#define NESBUS_CPU_REG_SEL_AC           0x5
#define NESBUS_CPU_REG_SEL_X            0x6
#define NESBUS_CPU_REG_SEL_Y            0x7
#define NESBUS_CPU_REG_SEL_IR           0x8
#define NESBUS_CPU_REG_SEL_ABL          0x9
#define NESBUS_CPU_REG_SEL_ABH          0xA
#define NESBUS_CPU_REG_SEL_DOR          0xB
#define NESBUS_CPU_REG_SEL_TS           0xC     // Timing State
#define NESBUS_CPU_REG_SEL_IS           0xD     // Interrupt State
#define NESBUS_CPU_REG_SEL_PS           0xE     // Pin State

#define NESBUS_CPU_REG_DOUT         NESBUS_CTRL_REG(0x064)

#define NESBUS_PPU_MON_SEL          NESBUS_CTRL_REG(0x068)
#define NESBUS_PPU_MON_SEL_CMS          0x0
#define NESBUS_PPU_MON_SEL_ROWCOL       0x10
#define NESBUS_PPU_MON_SEL_FRAME        0x14
#define NESBUS_PPU_MON_SEL_VAL          0x20
#define NESBUS_PPU_MON_SEL_VAC          0x24
#define NESBUS_PPU_MON_SEL_PAL          0x40
#define NESBUS_PPU_MON_SEL_PAL_LEN      0x20
#define NESBUS_PPU_MON_SEL_OAM_PRI      0x60
#define NESBUS_PPU_MON_SEL_OAM_PRI_LEN  0x100
#define NESBUS_PPU_MON_SEL_OAM_SEC      0x160
#define NESBUS_PPU_MON_SEL_OAM_SEC_LEN  0x20

#define NESBUS_PPU_MON_DOUT         NESBUS_CTRL_REG(0x06C)

#define NESBUS_ICD_CTRL             NESBUS_CTRL_REG(0x070)
#define NESBUS_ICD_SYS_ENABLE           (1<<0)
#define NESBUS_ICD_SYS_FORCE_HALT       (1<<1)
#define NESBUS_ICD_SYS_RESUME           (1<<2)
#define NESBUS_ICD_SYS_HOLD             (1<<8)
#define NESBUS_ICD_MATCH_ENABLE         (255<<16)
#define NESBUS_ICD_MATCH_ENABLE_S       16
#define NESBUS_ICD_MATCH_TRIGGER        (255U<<24)
#define NESBUS_ICD_MATCH_TRIGGER_S      24

#define NESBUS_ICD_MATCH_SEL        NESBUS_CTRL_REG(0x074)

#define NESBUS_ICD_MATCH_REG_SEL    NESBUS_CTRL_REG(0x078)
#define NESBUS_ICD_MATCH_REG_SEL_STATUS 0x0
#define NESBUS_ICD_MATCH_REG_SEL_DLIN   0x2
#define NESBUS_ICD_MATCH_REG_SEL_DHIN   0x3
#define NESBUS_ICD_MATCH_REG_SEL_DLEN   0x4
#define NESBUS_ICD_MATCH_REG_SEL_DHEN   0x5
#define NESBUS_ICD_MATCH_REG_SEL_DLOUT  0x6
#define NESBUS_ICD_MATCH_REG_SEL_DHOUT  0x7
#define NESBUS_ICD_MATCH_REG_SEL_D2OUT  0x8

#define NESBUS_ICD_MATCH_REG_DOUT   NESBUS_CTRL_REG(0x07C)

#define NESBUS_APU_MON_SEL          NESBUS_CTRL_REG(0x090)
#define NESBUS_APU_MON_SEL_SND_CHN      0x15

#define NESBUS_APU_MON_DOUT         NESBUS_CTRL_REG(0x094)

// Bits for NESBUS_CPU_REG_DOUT

// Processor Flags
#define NESBUS_CPU_REG_PF_C             (1<<0)
#define NESBUS_CPU_REG_PF_Z             (1<<1)
#define NESBUS_CPU_REG_PF_I             (1<<2)
#define NESBUS_CPU_REG_PF_D             (1<<3)
#define NESBUS_CPU_REG_PF_B             (1<<4)
#define NESBUS_CPU_REG_PF_V             (1<<6)
#define NESBUS_CPU_REG_PF_N             (1<<7)

// Timing State
#define NESBUS_CPU_REG_TS_CLOCK1        (1<<0)
#define NESBUS_CPU_REG_TS_CLOCK2        (1<<1)
#define NESBUS_CPU_REG_TS_T2            (1<<2)
#define NESBUS_CPU_REG_TS_T3            (1<<3)
#define NESBUS_CPU_REG_TS_T4            (1<<4)
#define NESBUS_CPU_REG_TS_T5            (1<<5)

// Interrupt State
#define NESBUS_CPU_REG_IS_RESP          (1<<0)
#define NESBUS_CPU_REG_IS_RESG          (1<<1)
#define NESBUS_CPU_REG_IS_NMIP          (1<<2)
#define NESBUS_CPU_REG_IS_NMIG          (1<<3)
#define NESBUS_CPU_REG_IS_IRQP          (1<<4)

// Pin State
#define NESBUS_CPU_REG_PS_RDY           (1<<0)
#define NESBUS_CPU_REG_PS_RW            (1<<1)
#define NESBUS_CPU_REG_PS_AEC           (1<<2)
#define NESBUS_CPU_REG_PS_SYNC          (1<<3)
#define NESBUS_CPU_REG_PS_SO            (1<<4)

// Bits for NESBUS_ICD_MATCH register STATUS
#define NESBUS_ICD_MATCH_REG_STATUS_MATCH_SEL (15<<0)
#define NESBUS_ICD_MATCH_REG_STATUS_MATCH_SEL_S 0
#define NESBUS_ICD_MATCH_REG_STATUS_NUM_MATCH (15<<4)
#define NESBUS_ICD_MATCH_REG_STATUS_NUM_MATCH_S 4

// Bits for NESBUS_ICD_MATCH registers D[LH]IN, D[LH]EN, and D[LH]OUT
#define NESBUS_ICD_MATCH_REG_DL_CPU_A   (65535<<0)
#define NESBUS_ICD_MATCH_REG_DL_CPU_A_S 0
#define NESBUS_ICD_MATCH_REG_DL_CPU_DB  (255<<16)
#define NESBUS_ICD_MATCH_REG_DL_CPU_DB_S 16
#define NESBUS_ICD_MATCH_REG_DL_CPU_RW  (1<<24)
#define NESBUS_ICD_MATCH_REG_DL_CPU_SYNC (1<<25)
#define NESBUS_ICD_MATCH_REG_DL_PPU_FRAME (63U<<26)
#define NESBUS_ICD_MATCH_REG_DL_PPU_FRAME_S 26
#define NESBUS_ICD_MATCH_REG_DH_PPU_COL (511<<0)
#define NESBUS_ICD_MATCH_REG_DH_PPU_COL_S 0
#define NESBUS_ICD_MATCH_REG_DH_PPU_ROW (511<<9)
#define NESBUS_ICD_MATCH_REG_DH_PPU_ROW_S 9
#define NESBUS_ICD_MATCH_REG_DH_PPU_FRAME (16383U<<18)
#define NESBUS_ICD_MATCH_REG_DH_PPU_FRAME_S 18

// Bits for NESBUS_PPU_MON_DOUT

#define NESBUS_PPU_MON_CMS_ADDR_INC     (1<<2)
#define NESBUS_PPU_MON_CMS_SPR_PAT      (1<<3)
#define NESBUS_PPU_MON_CMS_BKG_PAT      (1<<4)
#define NESBUS_PPU_MON_CMS_SPR_SIZE     (1<<5)
#define NESBUS_PPU_MON_CMS_SLAVE_MODE   (1<<6)
#define NESBUS_PPU_MON_CMS_VBL_EN       (1<<7)
#define NESBUS_PPU_MON_CMS_PAL_MONO     (1<<8)
#define NESBUS_PPU_MON_CMS_BKG_CLIP     (1<<9)
#define NESBUS_PPU_MON_CMS_SPR_CLIP     (1<<10)
#define NESBUS_PPU_MON_CMS_BKG_EN       (1<<11)
#define NESBUS_PPU_MON_CMS_SPR_EN       (1<<12)
#define NESBUS_PPU_MON_CMS_EMPH         (7<<13)
#define NESBUS_PPU_MON_CMS_EMPH_S       13
#define NESBUS_PPU_MON_CMS_SPR_OVERFLOW (1<<21)
#define NESBUS_PPU_MON_CMS_SPR0_HIT     (1<<22)
#define NESBUS_PPU_MON_CMS_VBL_FLAG     (1<<23)

#define NESBUS_PPU_MON_ROWCOL_ROW       (511<<0)
#define NESBUS_PPU_MON_ROWCOL_ROW_S     0
#define NESBUS_PPU_MON_ROWCOL_COL       (511<<16)
#define NESBUS_PPU_MON_ROWCOL_COL_S     16


// Bits for NESBUS_APU_MON_DOUT

#define NESBUS_APU_MON_SND_CHN_DMC_LEN  (1<<4)
#define NESBUS_APU_MON_SND_CHN_NOI_LEN  (1<<3)
#define NESBUS_APU_MON_SND_CHN_TRI_LEN  (1<<2)
#define NESBUS_APU_MON_SND_CHN_SQ2_LEN  (1<<1)
#define NESBUS_APU_MON_SND_CHN_SQ1_LEN  (1<<0)

//////////////////////////////////////////////////////////////////////
// NES native memory bus interfaces

#define NESBUS_CPU_MEM_BASE     (NESBUS_BASE + 0x10000)
#define NESBUS_CPU_MEM_REG(off) (*((volatile uint8_t *)(NESBUS_CPU_MEM_BASE + (off))))
#define NESBUS_PPU_MEM_BASE     (NESBUS_BASE + 0x20000)
#define NESBUS_PPU_MEM_REG(off) (*((volatile uint8_t *)(NESBUS_PPU_MEM_BASE + (off))))

#define NESBUS_CPU_MEM          (& NESBUS_CPU_MEM_REG(0))
#define NESBUS_PPU_MEM          (& NESBUS_PPU_MEM_REG(0))

//////////////////////////////////////////////////////////////////////
// ROM cartridge control interface

#define NESBUS_CART_CTRL_BASE   (NESBUS_BASE + 0x01000)
#define NESBUS_CART_CTRL_REG(off) (*((volatile uint32_t *)(NESBUS_CART_CTRL_BASE + (off))))
#define NESBUS_CART_BS              NESBUS_CART_CTRL_REG(0x000)
#define NESBUS_CART_BS_BANKS            (255<<0)
#define NESBUS_CART_BS_BANKS_S          0
#define NESBUS_CART_BS_BANK_AM          (255<<8)
#define NESBUS_CART_BS_BANK_AM_S        8

#define NESBUS_MMU_CFG              NESBUS_CART_CTRL_REG(0x004)
#define NESBUS_MMU_CFG_SBB_AL           (255<<0)
#define NESBUS_MMU_CFG_SBB_AL_S         0

#define NESBUS_CHIP_CFG             NESBUS_CART_CTRL_REG(0x040)
#define NESBUS_CHIP_CFG_PRG_ROM_ADDR_MAX (31<<0)
#define NESBUS_CHIP_CFG_PRG_ROM_ADDR_MAX_S 0
#define NESBUS_CHIP_CFG_CHR_ADDR_MAX    (31<<8)
#define NESBUS_CHIP_CFG_CHR_ADDR_MAX_S  8
#define NESBUS_CHIP_CFG_CHR_READ_ONLY   (1<<15)
#define NESBUS_CHIP_CFG_PRG_RAM_ADDR_MAX (31<<16)
#define NESBUS_CHIP_CFG_PRG_RAM_ADDR_MAX_S 16

#define NESBUS_PRG_RAM_WRITE_CNT    NESBUS_CART_CTRL_REG(0x050)

#define NESBUS_SWAP0                NESBUS_CART_CTRL_REG(0x060)
#define NESBUS_SWAP0_SYS_HOLD_REQ       (1<<0)
#define NESBUS_SWAP0_PRG_READY          (255<<8)
#define NESBUS_SWAP0_PRG_READY_S        8
#define NESBUS_SWAP0_CHR_READY          (255<<16)
#define NESBUS_SWAP0_CHR_READY_S        16

#define NESBUS_SWAP1                NESBUS_CART_CTRL_REG(0x064)
#define NESBUS_SWAP1_PRG_SBBA0          (255<<0)
#define NESBUS_SWAP1_PRG_SBBA0_S        0
#define NESBUS_SWAP1_PRG_SBBA1          (255<<8)
#define NESBUS_SWAP1_PRG_SBBA1_S        8

#define NESBUS_SWAP2                NESBUS_CART_CTRL_REG(0x068)
#define NESBUS_SWAP2_CHR_SBBA0          (255<<0)
#define NESBUS_SWAP2_CHR_SBBA0_S        0
#define NESBUS_SWAP2_CHR_SBBA1          (255<<8)
#define NESBUS_SWAP2_CHR_SBBA1_S        8
#define NESBUS_SWAP2_CHR_SBBA2          (255<<16)
#define NESBUS_SWAP2_CHR_SBBA2_S        16
#define NESBUS_SWAP2_CHR_SBBA3          (255U<<24)
#define NESBUS_SWAP2_CHR_SBBA3_S        24

#define NESBUS_SWAP3                NESBUS_CART_CTRL_REG(0x06C)
#define NESBUS_SWAP3_CHR_SBBA4          (255<<0)
#define NESBUS_SWAP3_CHR_SBBA4_S        0
#define NESBUS_SWAP3_CHR_SBBA5          (255<<8)
#define NESBUS_SWAP3_CHR_SBBA5_S        8

#define NESBUS_MIRRORING            NESBUS_CART_CTRL_REG(0x080)
#define NESBUS_MIRRORING_LOWER          0
#define NESBUS_MIRRORING_UPPER          1
#define NESBUS_MIRRORING_VERT           2
#define NESBUS_MIRRORING_HORIZ          3

#define NESBUS_MAPPER_SEL           NESBUS_CART_CTRL_REG(0x0A0)
#define NESBUS_MAPPER_SEL_BYPASS        0
#define NESBUS_MAPPER_SEL_MMC1          1
#define NESBUS_MAPPER_SEL_UXROM         2
#define NESBUS_MAPPER_SEL_MMC3_6        4

#define NESBUS_MMC1_CFG             NESBUS_CART_CTRL_REG(0x0A4)
#define NESBUS_MMC1_CFG_SNROM           (1<<0)
#define NESBUS_MMC1_CFG_SEROM           (1<<1)

#define NESBUS_MMC3_6_CFG           NESBUS_CART_CTRL_REG(0x0A8)
#define NESBUS_MMC3_6_CFG_NAMCOT        (1<<0)

#define NESBUS_MMU_TT_IDX           NESBUS_CART_CTRL_REG(0x0C0)

#define NESBUS_MMU_TT_DAT           NESBUS_CART_CTRL_REG(0x0C4)

//////////////////////////////////////////////////////////////////////
// ROM cartridge backing store interface

#define NESBUS_CART_BS_BASE     (NESBUS_BASE + 0x40000)
#define NESBUS_CART_BS_REG(off) (*((volatile uint32_t *)(NESBUS_CART_BS_BASE + (off))))

//////////////////////////////////////////////////////////////////////

} // namespace nes

// Local Variables:
// compile-command: "cd ../.. && python3 tools/gen_regs_h.py sw/nes/nesbus.h.i sw/nes/nesbus.h rtl/nes/nesbus.v rtl/nes/cart.v rtl/nes/icd.v rtl/mos6502/mos6502_core.vh rtl/rp2a03/*.v rtl/rp2c02/*.v"
// End:
