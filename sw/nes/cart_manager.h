#pragma once

#include <cli/command.h>
#include <fat/path.h>
#include <util/ptimer.h>

#include <mutex>

#include "cart.h"

namespace nes {

class sys;

class cart_manager : private cart::listener
{
public:
    cart_manager(sys& sys);

    void load_cart(const fat::path& p);
    size_t save_cart();

private:
    void insert_cart(cart::PCrt& crt);
    void remove_cart();

    int cli_cart(const ::cli::command::args_t& args);
    int cli_save(const ::cli::command::args_t& args);

    // interface for cart::listener
    virtual void cart_modified(bool);

    sys& _sys;
    std::recursive_mutex _mtx;
    fat::path _cart_path;
    fat::path _cart_sav_path;
    bool _cart_inserted;
    ptimer _save_tmr;
};

} // namespace nes
