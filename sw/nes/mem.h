#pragma once

#include "memptr.h"
#include "util/membuf.h"

namespace nes {

class mem
{
public:
    mem();

    void init();

    // Reset RAM to initial state, usually a random pattern.
    void reset();

private:

    membuf<memptr> initial;
};

} // namespace nes
