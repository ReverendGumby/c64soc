#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "util/error.h"
#include "util/debug.h"
#include "cart.h"
#include "nesbus.h"

#include "cart_loader/normal.h"

#define DBG_LVL 0
#define DBG_TAG "CART"

namespace nes {

cart::cart()
    : bs(*this), mmu_tt(*this)
{
}

void cart::init()
{
    DBG_PRINTF(1, "entry\n");

    const auto cart_bs = NESBUS_CART_BS;
    auto bs_bank_am = (cart_bs & NESBUS_CART_BS_BANK_AM) >> NESBUS_CART_BS_BANK_AM_S;
    geom.num_banks = (cart_bs & NESBUS_CART_BS_BANKS) >> NESBUS_CART_BS_BANKS_S;
    geom.bank_size = 1 << (bs_bank_am + 1);

    bs.init();
    mmu_tt.init();

    _cldr = nullptr;
    Cldr::init();

    _write_tmr.set_duration(0.25 * timer::sec);
    _write_tmr.start();
    pollable_add(this);
}

void cart::reset()
{
    std::unique_lock<std::mutex> lck { _mtx };

    _cldr = nullptr;

    mmu_tt.reset();
}

cart::Cldr* cart::new_loader()
{
    return new cart_loader::normal{_crt, *this};
}

void cart::load(PCrt pcrt)
{
    DBG_PRINTF(1, "entry\n");
    std::unique_lock<std::mutex> lck { _mtx };

    _crt = pcrt;

    _cldr = decltype(_cldr) { new_loader() };

    _cldr->init();
    _cldr->load();
    set_modified(false);
}

cart::PCrt cart::save()
{
    DBG_PRINTF(1, "entry\n");
    std::unique_lock<std::mutex> lck { _mtx };

    if (_cldr) {
        _cldr->save();
        set_modified(false);
    }

    return _crt;
}

void cart::reset_ram()
{
    DBG_PRINTF(1, "entry\n");
    bool force_zero = _battery_disabled;
    if (_cldr)
        _cldr->reset_ram(force_zero);
}

bool cart::can_save() const
{
    return !_battery_disabled && _crt && _crt->has_battery();
}

void cart::set_battery_disabled(bool v)
{
    _battery_disabled = v;
    if (_battery_disabled)
        set_modified(false);
}

void cart::poll()
{
    std::unique_lock<std::mutex> lck { _mtx };

    if (_cldr)
        _cldr->service_swap();

    if (_write_tmr.expired()) {
        _write_tmr.restart();
        int cnt = NESBUS_PRG_RAM_WRITE_CNT;
        if (cnt)
        {
            NESBUS_PRG_RAM_WRITE_CNT = 0;
            DBG_PRINTF(5, "PRGRAM writes: %d\n", cnt);
            if (can_save()) {
                set_modified(true);
            }
        }
    }
}

void cart::set_modified(bool b)
{
    // Pass all updates to listeners, even if _modified hasn't changed, so they
    // can learn of repeated modifications.
    _modified = b;
    talker<listener>::speak(&listener::cart_modified, _modified);
    talker<ui::hmi_cart_listener>::speak(
        &ui::hmi_cart_listener::cart_modified, _modified);
}

} // namespace nes
