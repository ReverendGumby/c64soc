#include "sys.h"
#include "cart.h"

#include <cli/command.h>
#include <fat/path.h>
#include <fat/filesystem.h>

#include <iostream>

#include "cart_manager.h"

// NB: 'ptimer _save_tmr' isn't thread-safe, and CLI and timers are in different
// threads. Be careful not to race loading with saving.

namespace nes {

using namespace ::cli;

static nes::cart::PCrt read_cart(const fat::path& p)
{
    fat::file f{p};
    auto s = f.size();
    nes::Crt::Buf buf{new uint8_t[s]};
    f.read(&buf[0], s);

    nes::cart::PCrt crt{new nes::Crt{std::move(buf), (size_t)s}};
    return crt;
}

static fat::path sav_path(const fat::path& cart_path)
{
    fat::path p = cart_path;
    p.replace_extension(fat::path { L".sav" });
    return p;
}

static std::pair<nes::Crt::Buf, size_t> read_sav(const fat::path& p)
{
    std::pair<nes::Crt::Buf, size_t> ret;
    if (fat::exists(p)) {
        fat::file f { p };
        auto s = f.size();
        std::cout << "Loading " << s << " bytes from " << p << std::endl;
        auto buf = nes::Crt::Buf { new uint8_t[s] };
        f.read(&buf[0], s);
        ret.first = std::move(buf);
        ret.second = s;
    }
    return ret;
}

cart_manager::cart_manager(sys& sys)
    : _sys(sys), _cart_inserted(false)
{
    _save_tmr.duration = 2 * timer::sec;
    _save_tmr.action = [this]() { save_cart(); };

    command::define("cart", [this](const command::args_t& args) {
        return cli_cart(args); });
    command::define("save", [this](const command::args_t& args) {
        return cli_save(args); });

    _sys.cart.talker<cart::listener>::add_listener(this);
}

void cart_manager::load_cart(const fat::path& p)
{
    auto sav_p = sav_path(p);

    std::unique_lock<std::recursive_mutex> lck { _mtx };

    auto crt = read_cart(p);
    auto sav = read_sav(sav_p);
    if (sav.first)
        crt->load_prgram(std::move(sav.first), sav.second);
    insert_cart(crt);

    _cart_path = p;
    _cart_sav_path = sav_p;
}

size_t cart_manager::save_cart()
{
    std::unique_lock<std::recursive_mutex> lck { _mtx };

    size_t len = 0;
    const auto& sav_p = _cart_sav_path;
    if (_sys.cart.can_save() && !sav_p.empty()) {
        auto crt = _sys.cart.save();
        nes::Crt::Buf bin;
        crt->save_prgram(bin, len);
        if (bin) {
            auto f { fat::open(sav_p, fat::omode::w, fat::oflags::create) };
            f.write(&bin[0], len);
        }
    }
    return len;
}

void cart_manager::insert_cart(nes::cart::PCrt& crt)
{
    remove_cart();

    _sys.reset_nes(true);
    _sys.cart.load(crt);
    _sys.reset_nes(false);

    _cart_inserted = true;
}

void cart_manager::remove_cart()
{
    if (_cart_inserted)
    {
        _cart_inserted = false;
        _cart_path.clear();
        _cart_sav_path.clear();
        _save_tmr.cancel();

        _sys.reset_nes(true);
        _sys.cart.reset();
    }
}

void cart_manager::cart_modified(bool b)
{
    std::unique_lock<std::recursive_mutex> lck { _mtx };

    if (b)
        _save_tmr.start();
}

int cart_manager::cli_cart(const command::args_t& args)
{
    if (args.size() < 1)
        throw std::range_error("missing filename");

    const char* fname = args[0].c_str();
    
    try {
        fat::path p {fname};
        load_cart(p);
    }
    catch (std::runtime_error& e) {
        std::cout << e.what() << std::endl;
    }

    return 0;
}

int cart_manager::cli_save(const command::args_t&)
{
    auto len = save_cart();

    if (len)
        std::cout << "Saved " << len << " bytes to " << _cart_sav_path << std::endl;

    return 0;
}

} // namespace nes
