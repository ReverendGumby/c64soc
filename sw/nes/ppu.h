#pragma once

#include "nesbus.h"

#include <util/RegValUnion.h>

#include <stdint.h>
#include <unistd.h>
#include <array>

namespace nes {

struct ppu_regs
{
    struct val_t {
        uint16_t ht : 5;                        // H tile index
        uint16_t vt : 5;                        // V tile index
        uint16_t h : 1;                         // H name table selection
        uint16_t v : 1;                         // V name table selection
        uint16_t fv : 3;                        // fine V scroll
    };

    struct {
        uint8_t _res0 : 2;
        uint8_t addr_inc : 2;
        uint8_t spr_pat : 1;
        uint8_t bkg_pat : 1;
        uint8_t spr_size : 1;
        uint8_t slave_mode : 1;
        uint8_t vbl_en : 1;
    } ctrl;                                     // PPUCTRL $2000

    struct {
        uint8_t pal_mono : 1;
        uint8_t bkg_clip : 1;
        uint8_t spr_clip : 1;
        uint8_t bkg_en : 1;
        uint8_t spr_en : 1;
        uint8_t emph : 3;
    } mask;                                     // PPUMASK $2001

    struct {
        uint8_t _res0 : 5;
        uint8_t spr_overflow : 1;
        uint8_t spr0_hit : 1;
        uint8_t vbl_flag : 1;
    } stat;                                     // PPUSTAT $2002

    struct {
        uint16_t row;
        uint16_t col;
        uint32_t frame;
    } video_counter;

    struct {
        RegValUnion<val_t, uint16_t> val;
        RegValUnion<val_t, uint16_t> vac;
    } bus_master;

    struct {
        uint8_t pal[NESBUS_PPU_MON_SEL_PAL_LEN];
    } palette;

    struct {
        uint8_t pri[NESBUS_PPU_MON_SEL_OAM_PRI_LEN];
        uint8_t sec[NESBUS_PPU_MON_SEL_OAM_SEC_LEN];
    } oam;
};

class ppu
{
public:
    void init();

    enum {
        regs_cms = 1<<0,
        regs_video_counter = 1<<1,
        regs_bus_master = 1<<2,
        regs_palette = 1<<3,
        regs_oam = 1<<4,
        regs_all = -1,
    };
    ppu_regs read_regs(int which = regs_all);

private:
    uint32_t read_reg(uint32_t sel);
    template<size_t Len>
    std::array<uint32_t, Len> read_array(uint32_t sel);
};

} // namespace nes
