#ifndef NES__ICD__H
#define NES__ICD__H

#include <util/listener.h>
#include <vector>
#include <stdint.h>
#include "icd_match.h"
#include "monitor/mos6502/cpu_regs.h"

namespace nes {

namespace _icd
{

struct listener
{
    virtual void resumed() = 0;
};

} // namespace _icd

class icd : public talker<_icd::listener>
{
public:
    using listener = _icd::listener;
    using cpu_regs = ::monitor::mos6502::cpu_regs;

    void init();
    void reset();

    cpu_regs read_cpu_regs();

    bool is_halted();
    void force_halt();
    void resume();

    icd_match& get_match(int);
    bool is_any_match_triggered();

private:
    std::vector<icd_match> match_units;

    uint8_t read_cpu_reg(uint8_t reg);
};

} // namespace nes

#endif /* NES__ICD__H */
