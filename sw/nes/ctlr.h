#pragma once

#include <stdint.h>

namespace nes {

class ctlr
{
public:
    struct state_t {
        uint8_t c1, c2;
    };

    ctlr();

    void init();

    void reset();
    void set(const state_t&);

    state_t get();
};

} // namespace nes
