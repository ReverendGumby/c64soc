#include <unistd.h>
#include "nesbus.h"
#include "util/timer.h"

#include "icd.h"

namespace nes {

void icd::init()
{
    for (int i = 0, num = 1; i < num; i++) {
        match_units.emplace_back(i);
        auto& match = match_units.back();
        num = match.get_status().num_match;
    }

    reset();
}

void icd::reset()
{
    NESBUS_ICD_CTRL = 0;
    NESBUS_ICD_CTRL = NESBUS_ICD_SYS_ENABLE;
}

uint8_t icd::read_cpu_reg(uint8_t reg)
{
    NESBUS_CPU_REG_SEL = reg;
    uint8_t val = NESBUS_CPU_REG_DOUT;
    return val;
}

icd::cpu_regs icd::read_cpu_regs()
{
    auto pcl = read_cpu_reg(NESBUS_CPU_REG_SEL_PCL);
    auto pch = read_cpu_reg(NESBUS_CPU_REG_SEL_PCH);
    auto pf  = read_cpu_reg(NESBUS_CPU_REG_SEL_PF);
    auto s   = read_cpu_reg(NESBUS_CPU_REG_SEL_S);
    auto ac  = read_cpu_reg(NESBUS_CPU_REG_SEL_AC);
    auto x   = read_cpu_reg(NESBUS_CPU_REG_SEL_X);
    auto y   = read_cpu_reg(NESBUS_CPU_REG_SEL_Y);
    auto ir  = read_cpu_reg(NESBUS_CPU_REG_SEL_IR);
    auto abl = read_cpu_reg(NESBUS_CPU_REG_SEL_ABL);
    auto abh = read_cpu_reg(NESBUS_CPU_REG_SEL_ABH);
    auto dor = read_cpu_reg(NESBUS_CPU_REG_SEL_DOR);
    auto ts  = read_cpu_reg(NESBUS_CPU_REG_SEL_TS);
    auto is  = read_cpu_reg(NESBUS_CPU_REG_SEL_IS);
    auto ps  = read_cpu_reg(NESBUS_CPU_REG_SEL_PS);

    cpu_regs ret;
    ret.pc = (uint16_t(pch) << 8) | pcl;
    ret.pf.c = pf & NESBUS_CPU_REG_PF_C;
    ret.pf.z = pf & NESBUS_CPU_REG_PF_Z;
    ret.pf.i = pf & NESBUS_CPU_REG_PF_I;
    ret.pf.d = pf & NESBUS_CPU_REG_PF_D;
    ret.pf.b = pf & NESBUS_CPU_REG_PF_B;
    ret.pf.v = pf & NESBUS_CPU_REG_PF_V;
    ret.pf.n = pf & NESBUS_CPU_REG_PF_N;
    ret.s = s;
    ret.a = ac;
    ret.x = x;
    ret.y = y;
    ret.ir = ir;
    ret.abr = (uint16_t(abh) << 8) | abl;
    ret.dor = dor;
    ret.tstate.clock1 = ts & NESBUS_CPU_REG_TS_CLOCK1;
    ret.tstate.clock2 = ts & NESBUS_CPU_REG_TS_CLOCK2;
    ret.tstate.t2     = ts & NESBUS_CPU_REG_TS_T2;
    ret.tstate.t3     = ts & NESBUS_CPU_REG_TS_T3;
    ret.tstate.t4     = ts & NESBUS_CPU_REG_TS_T4;
    ret.tstate.t5     = ts & NESBUS_CPU_REG_TS_T5;
    ret.istate.resp = is & NESBUS_CPU_REG_IS_RESP;
    ret.istate.resg = is & NESBUS_CPU_REG_IS_RESG;
    ret.istate.nmip = is & NESBUS_CPU_REG_IS_NMIP;
    ret.istate.nmig = is & NESBUS_CPU_REG_IS_NMIG;
    ret.istate.irqp = is & NESBUS_CPU_REG_IS_IRQP;
    ret.pstate.rdy  = ps & NESBUS_CPU_REG_PS_RDY;
    ret.pstate.rw   = ps & NESBUS_CPU_REG_PS_RW;
    ret.pstate.aec  = ps & NESBUS_CPU_REG_PS_AEC;
    ret.pstate.sync = ps & NESBUS_CPU_REG_PS_SYNC;
    ret.pstate.so   = ps & NESBUS_CPU_REG_PS_SO;
    return ret;
}

bool icd::is_halted()
{
    return NESBUS_ICD_CTRL & NESBUS_ICD_SYS_HOLD;
}

void icd::force_halt()
{
    NESBUS_ICD_CTRL |= NESBUS_ICD_SYS_FORCE_HALT;
    NESBUS_ICD_CTRL &= ~NESBUS_ICD_SYS_FORCE_HALT;
}

void icd::resume()
{
    NESBUS_ICD_CTRL |= NESBUS_ICD_SYS_RESUME;
    NESBUS_ICD_CTRL &= ~NESBUS_ICD_SYS_RESUME;
    speak(&listener::resumed);
}

icd_match& icd::get_match(int idx)
{
    return match_units.at(idx);
}

bool icd::is_any_match_triggered()
{
    return NESBUS_ICD_CTRL & NESBUS_ICD_MATCH_TRIGGER;
}

} // namespace nes
