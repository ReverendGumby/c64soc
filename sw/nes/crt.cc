#include <string.h>
#include <stdio.h>
#include <algorithm>
#include <util/crc32.h>
#include "crt.h"

// Reference: https://wiki.nesdev.org/w/index.php/INES

namespace nes {

Chip::Chip(type_t type, uint8_t *bin, size_t len)
{
    _type = type;
	_len = len;
	_data = &bin[0];
}

bool Chip::is_ram() const
{
    return _type == Chip::PRGRAM || _type == Chip::CHARAM;
}

const char* Chip::name() const
{
    switch (type()) {
    case PRGROM: return "PRGROM";
    case PRGRAM: return "PRGRAM";
    case CHAROM: return "CHAROM";
    case CHARAM: return "CHARAM";
    default: break;
    }
    return "?";
}

Crt::Crt(Buf&& buf, size_t bin_len)
	: _bin(std::move(buf)), _bin_len(bin_len)
{
    static const uint8_t sig[] = "NES\x1A";
    auto* hdr = &_bin[0];
    uint8_t *end = hdr + bin_len;

    if (hdr + 0x10 >= end)
        throw Crt_format_error("header too small");
    if (memcmp(sig, &hdr[0x00], 0x4) != 0)
        throw Crt_format_error("bad sig");

    patch_problems();

    auto* bin = &hdr[0x10];
    size_t len = hdr[4] * 16384;
    _chips.push_back(Chip{Chip::PRGROM, bin, len});

    if (hdr[5] == 0) {                          // cartridge uses CHR RAM
        len = 1 * 8192;
        _chips.push_back(Chip{Chip::CHARAM, nullptr, len});
    } else {
        bin += len;
        len = hdr[5] * 8192;
        _chips.push_back(Chip{Chip::CHAROM, bin, len});
    }
    _vertical_mirroring = (hdr[6] & (1<<0)) != 0;
    _battery = (hdr[6] & (1<<1)) != 0;
    if (_battery) {
        size_t len = 0x2000;
        _prgram_bin = Buf { new uint8_t[len] };
        memset(&_prgram_bin[0], 0, len); // TODO: Randomize
        _chips.push_back(Chip{Chip::PRGRAM, &_prgram_bin[0], len});
    } else {
        // Value 0 infers 8 KB for compatibility
        _chips.push_back(Chip{Chip::PRGRAM, nullptr, 0x2000});
    }

    _mapper = ((hdr[6] >> 4) & 0x0f) | (hdr[7] & 0xf0);

    _pcb_class = "?";
    if (_mapper == 1) {
        if (chip_size_kb(Chip::PRGROM) <= 256 &&
            chip_size_kb(Chip::CHARAM) == 8 &&
            chip_size_kb(Chip::PRGRAM) == 8)
            _pcb_class = "SNROM";
        else if (chip_size_kb(Chip::PRGROM) == 32)
            _pcb_class = "SEROM";
    }
    if (_mapper == 206) {
        _pcb_class = "DxROM";
    }
}

template <class Container, class Value>
static constexpr bool can_find_in(const Container& c, Value v)
{
    return std::find(begin(c), end(c), v) != end(c);
}

void Crt::patch_problems()
{
    auto* hdr = &_bin[0];

    // Fix up the header, based on CRC32 matching.
    uint32_t crc32 = calc_crc32(&_bin[0], _bin_len);

    static const uint8_t name0[] = "DiskDude!";
    if (memcmp(name0, &hdr[7], 9) == 0) {
        // Seems someone scribbled their name here...
        memset(&hdr[7], 0, 9);
    }

    // These carts have batteries.
    static constexpr std::array has_battery_sums {
        1321743800, // Startropics (U).nes
    };
    if (can_find_in(has_battery_sums, crc32)) {
        hdr[6] |= (1<<1);
    }

    // These carts use mapper 206 (Namcot 108 family).
    static constexpr std::array mapper_206 {
        178861397, // Karnov (U).nes
        272566101, // Karnov (USA).nes
    };
    if (can_find_in(mapper_206, crc32)) {
        hdr[6] = (hdr[6] & ~0xF0) | ((206 << 4) & 0xF0);
        hdr[7] = (hdr[7] & ~0xF0) | (206 & 0xF0);
    }
}

int Crt::chip_size_kb(Chip::type_t type) const
{
    int kb = 0;
    for (const auto& c : _chips)
        if (c.type() == type)
            kb += c.len() / 1024;
    return kb;
}

void Crt::load_prgram(Buf&& bin, size_t len)
{
    if (_prgram_bin == nullptr)
        return;
    auto i = find_prgram();
    if (i != _chips.end()) {
        auto& c = *i;
        memcpy(&c.data()[0], &bin[0], std::min(len, c.len()));
    }
}

void Crt::save_prgram(Buf& bin, size_t& len)
{
    len = 0;
    if (_prgram_bin == nullptr)
        return;
    auto i = find_prgram();
    if (i == _chips.end())
        return;
    len = i->len();
    bin = Buf { new uint8_t[len] };
    memcpy(&bin[0], &_prgram_bin[0], len);
}

Crt::ChipList::iterator Crt::find_prgram()
{
    return std::find_if(_chips.begin(), _chips.end(), 
                        [](const Chip& c)
                        { return Chip::PRGRAM == c.type(); });
}

} // namespace nes
