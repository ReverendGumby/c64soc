#ifndef NES_JOY__H
#define NES_JOY__H

#include <string>
#include "ui/hid_manager.h"
#include "ui/joy.h"
#include "hid.h"
#include "movie/input_source.h"

namespace nes {

class ctlr;

enum class joy_mode_t {
    none = 0,
    hid_1,
    hid_2,
    joy_modes
};

// Joystick ports
// (Values can be used to index _mode[])
enum joy_port_t { JOY_PORT_INVALID = -1, JOY1 = 0, JOY2 = 1, NUM_JOY_PORTS };

class joy : public hid,
            private ui::joy::listener,
            private ui::hid_manager::listener,
            public movie::input_source::listener
{
public:
    joy(ui::hid_manager&, class ctlr&);
    virtual ~joy();

private:
    struct joy_out {
        uint8_t ctlr1;
        uint8_t ctlr2;
    };

    joy_mode_t& mode(joy_port_t port) { return _mode[port - JOY1]; }

    ui::joy::report* jrpt(joy_mode_t);

    joy_out ui_reports_to_joy_out();
    joy_out movie_inputs_to_joy_out(const movie::input_source::report&);
    void set_ctlr();

    std::string get_joy_mode_desc_hid(int);
    void change_port_mode(joy_port_t, joy_mode_t);

    // interface for hid
    bool is_idle() override;
    void reset_ctlr() override;

    // interface for ui::joy::listener
    virtual void event(const ui::joy::report&);

    // interface for ui::hid_manager::listener
    virtual void joy_dev_changed(int id);

    // interface for movie::input_source::listener
    virtual void event(const movie::input_source::report&);

    class ctlr ctlr;
    ui::joy* joy_dev = nullptr;

    static constexpr int num_joys = NUM_JOY_PORTS;
    joy_mode_t _mode[num_joys];

    ui::joy::report jrpt1;
    ui::joy::report jrpt2;
};

} // namespace nes

#endif /* NES_JOY__H */
