#ifndef MONITOR__REG_PRINTER__H
#define MONITOR__REG_PRINTER__H

#include <string>
#include <ostream>
#include "ui/screenstream.h"
#include "util/reg_val.h"
#include "util/string.h"

namespace monitor {

struct reg_printer
{
    reg_printer(ui::screenstream* ss, int x_lbl, int w_lbl);

    void print_label(const char* label);

    template <typename T>
    static std::string to_str(const T val)
    {
        return reg_val<T>{val}.to_str();
    }

    template <typename T>
    void print(const char* label, const T val)
    {
        auto str = to_str(val);
        print(label, str, str);
    }
    template <typename T>
    void print(const char* label, const T new_val, const T old_val)
    {
        auto new_str = to_str(new_val);
        auto old_str = to_str(old_val);
        print(label, new_str, old_str);
    }

    void print(const char* label, const std::string& str);
    void print(const char* label, const std::string& new_str, const std::string& old_str, bool per_char_diff = true);

    ui::screenstream* ss;
    int y = 0;
    int x_lbl, w_lbl;
};

} // namespace monitor

#endif /* MONITOR__REG_PRINTER__H */
