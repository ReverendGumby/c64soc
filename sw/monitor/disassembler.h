#pragma once

#include <stdint.h>
#include <vector>
#include <algorithm>

namespace monitor {

template <typename Ctx, typename Inst, typename Memptr>
struct disassembler
{
    using bytes = std::vector<uint8_t>;
    using insts = std::vector<Inst>;

    void disassemble(bytes::const_iterator& it,
                     bytes::const_iterator end,
                     Ctx& ctx,
                     size_t max_insts,
                     bool force_unknown,
                     insts& out)
    {
        size_t is;
        for (; it != end && out.size() < max_insts; it += is)
        {
            if (!force_unknown)
            {
                auto x = Inst{ctx, it, end};
                if ((is = x.size()) == 0)
                    break;
                out.push_back(x);
            }
            else
            {
                is = 1;
                out.push_back(Inst::make_unknown(ctx, *it));
            }
        }
    }

    bytes get_inst_bytes(Memptr mp, size_t num)
    {
        auto bq = bytes(num);
        for (size_t i = 0; i < num; i++)
            bq[i] = mp[i];
        return bq;
    }

    insts get_insts(Ctx ctx, size_t max_insts,
                    size_t num_prev_inst)
    {
        size_t num_bytes = Inst::max_size() * max_insts;
        size_t max_prev_bytes = Inst::max_size() * num_prev_inst;
        size_t min_prev_bytes = 1 * num_prev_inst;

        // Disassemble the 'num_prev_inst' instructions prior to PC. Hunt for
        // starting points that yield the right number of instructions with the
        // right ending address. If none are found, force the byte(s) before PC
        // to be interpreted as unknown instructions, just in case they're
        // really not.
        auto pc = ctx.ip;
        auto sip = pc;                          // force type to match pc's
        sip -= max_prev_bytes;
        auto mp = Memptr{sip};
        auto pbq = get_inst_bytes(mp, max_prev_bytes);
        auto bq_end = pbq.cend();

        struct start {
            size_t num_unknowns;
            size_t num_bytes;
        };
        std::vector<start> starts;
        size_t num_force_unknowns = 0;
        if (num_prev_inst) {
            for (;
                 num_force_unknowns <= num_prev_inst;
                 num_force_unknowns ++)
            {
                starts.clear();
                for (size_t num_prev_bytes = max_prev_bytes;
                     num_prev_bytes >= min_prev_bytes;
                     num_prev_bytes--)
                {
                    size_t offset = max_prev_bytes - num_prev_bytes;
                    auto bqit = pbq.cbegin() + offset;
                    auto num_insts = num_prev_inst - num_force_unknowns;
                    Ctx pctx{ctx};
                    pctx.ip = sip + offset;
                    insts dis;
                    disassemble(bqit, bq_end, pctx, num_insts, false, dis);
                    disassemble(bqit, bq_end, pctx, num_prev_inst, true, dis);
                    if (dis.size() == num_prev_inst && pctx.ip == pc)
                    {
                        start s;
                        s.num_unknowns = std::count_if(
                            dis.begin(), dis.end(),
                            [](Inst& x) { return x.is_unknown(); });
                        s.num_bytes = num_prev_bytes;
                        starts.push_back(s);
                    }
                }
                if (!starts.empty())
                    break;
            }
        }

        // Multiple starting points can be found. Choose the one with the fewest
        // unknown instructions. If there are multiple of those, choose the one
        // with the fewest total bytes (i.e., closest to the end of the list).
        size_t piq_min_unknowns = num_prev_inst;
        for (const auto& s : starts)
            piq_min_unknowns = std::min(piq_min_unknowns, s.num_unknowns);
        auto has_min_unknowns = [piq_min_unknowns](const struct start& x) {
            return x.num_unknowns == piq_min_unknowns;
        };
        const auto& start = *std::find_if(starts.crbegin(), starts.crend(),
                                          has_min_unknowns);

        // Disassemble the previous instructions, the one at PC, and the next
        // 'num_next_inst' instructions.
        auto nbq = get_inst_bytes(pc, num_bytes - max_prev_bytes);
        pbq.insert(pbq.end(), nbq.begin(), nbq.end());
        size_t offset = max_prev_bytes - start.num_bytes;
        auto bqit = pbq.cbegin() + offset;
        bq_end = pbq.cend();
        ctx.ip = sip + offset;
        auto num_insts = num_prev_inst - num_force_unknowns;
        insts iq;
        disassemble(bqit, bq_end, ctx, num_insts, false, iq);
        disassemble(bqit, bq_end, ctx, num_prev_inst, true, iq);
        disassemble(bqit, bq_end, ctx, max_insts, false, iq);

        return iq;
    }
};

} // namespace monitor
