#pragma once

#include "asm.h"
#include "cpu_regs.h"

namespace monitor {
namespace w65c816s {

template <typename Memptr>
address_t find_pc(const cpu_regs& regs)
{
    // Finding the address of the current instruction is tricky. The PC
    // register continually increments and can be up to 4 bytes ahead of the
    // opcode. If SYNC is low, IR will contain the opcode, helping us find
    // it. If SYNC is high (opcode fetch in progress), IR will contain the
    // opcode of the previous instruction, but AOR will point to the next
    // opcode.
    address_t pc;
    if (regs.pstate.sync)
        pc = regs.aor;
    else
    {
        int i;
        for (i = 1; i <= 4; i++)
        {
            pc = (uint32_t(regs.pbr) << 16) | (regs.pc - i);
            if (*Memptr{pc} == regs.ir)
                break;
        }
        // Special case: IR was cleared due to pending interrupt, so of
        // course we didn't actually find it in memory.
        if (regs.ir == 0x00 && i == 5)
            // Interrupts clear PBR.
            pc = regs.pc - 2;
    }
    return pc;
}

} // namespace w65c816s
} // namespace monitor
