#pragma once

#include "cpu_regs.h"
#include "util/reg_val.h"
#include <sstream>

// reg_val is in the global namespace.

struct pf_t { monitor::w65c816s::cpu_regs regs; };
template <>
std::string reg_val<pf_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << (r.pf.c ? 'C' : 'c')
       << (r.pf.z ? 'Z' : 'z')
       << (r.pf.i ? 'I' : 'i')
       << (r.pf.d ? 'D' : 'd')
       << (r.pf.x ? 'X' : 'x')
       << (r.pf.m ? 'M' : 'm')
       << (r.pf.v ? 'V' : 'v')
       << (r.pf.n ? 'N' : 'n');
    return ss.str();
}

struct tstate_t { monitor::w65c816s::cpu_regs regs; };
template <>
std::string reg_val<tstate_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << r.tstate.ts;
    return ss.str();
}

struct pstate_t { monitor::w65c816s::cpu_regs regs; };
template <>
std::string reg_val<pstate_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << (r.pstate.rw   ? 'R' : 'W')
       << (r.pstate.sync ? 'S' : 's');
    return ss.str();
}
