#pragma once

#include <stdint.h>

namespace monitor {
namespace w65c816s {

struct cpu_regs
{
    uint16_t pc;
    struct pf_t {
        bool c;                                 // pf[0]
        bool z;                                 // pf[1]
        bool i;                                 // pf[2]
        bool d;                                 // pf[3]
        bool x;                                 // pf[4]
        bool m;                                 // pf[5]
        bool v;                                 // pf[6]
        bool n;                                 // pf[7]
    } pf;                                       // processor flags
    bool e;
    uint16_t s;
    uint16_t a;
    uint16_t d;
    uint16_t x;
    uint16_t y;
    uint8_t dbr;
    uint8_t pbr;
    uint8_t ir;                                 // instruction register
    uint32_t aor;                               // address output register
    uint8_t dor;                                // data output register
    uint16_t z;
    struct {
        uint8_t ts;
    } tstate;                                   // Timing State
    struct {
        bool rw;
        bool sync;
    } pstate;                                   // Pin State

    struct internal_t {
        // CPU_CORE_MON_INT0_*
        uint32_t cp2 : 1;
        uint32_t t_reset_pend : 1;
        uint32_t am_nostart : 1;
        uint32_t am_done_d : 1;
        uint32_t am_done_d2 : 1;
        uint32_t resp : 1;
        uint32_t nmip : 1;
        uint32_t irqp : 1;
        uint32_t resg : 1;
        uint32_t nmig : 1;
        uint32_t respd : 1;
        uint32_t nmil : 1;
        uint32_t intg_pre2 : 1;
        uint32_t intg : 1;
        uint32_t vec1 : 1;
        uint32_t intr_id : 3;
        uint32_t rwb_ext : 1;
        uint32_t mlb_ext : 1;
        uint32_t vpb_ext : 1;
        uint32_t vda_ext : 1;
        uint32_t vpa_ext : 1;
        uint32_t ac_z : 1;
        uint32_t dl_z : 1;
        uint32_t addc : 1;
        uint32_t add16 : 1;
        uint32_t accoh : 1;
        uint32_t daa : 1;
        uint32_t das : 1;

        // CPU_CORE_MON_INT1_*
        uint32_t ors : 1;
        uint32_t ands : 1;
        uint32_t eors : 1;
        uint32_t asls : 1;
        uint32_t rols : 1;
        uint32_t lsrs : 1;
        uint32_t rors : 1;
        uint32_t ccod : 1;
        uint32_t cvod : 1;
        uint32_t op_am_page_wrap_d : 1;
        uint32_t rdy1 : 1;
        uint32_t waitn0 : 1;

        // CPU_CORE_MON_INT4_*
        uint32_t t : 8;
        uint32_t am_t : 7;
        uint32_t op_tp : 7;
        uint32_t aab : 8;

        // CPU_CORE_MON_INT5_*
        uint16_t upc;
        uint16_t npc;

        // CPU_CORE_MON_INT6_*
        uint16_t idld;
        uint16_t aao;

        // CPU_CORE_MON_INT7_*
        uint16_t ai;
        uint16_t bi;

        // CPU_CORE_MON_INT8_*
        uint32_t cod;
    } internal;

    constexpr uint32_t get_ip() const
    {
        return (uint32_t(pbr) << 16) | pc;
    }
};

} // namespace w65c816s
} // namespace monitor
