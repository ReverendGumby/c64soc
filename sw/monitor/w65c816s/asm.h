#pragma once

#include <stddef.h>
#include <stdint.h>
#include <iterator>
#include <string>
#include <vector>

namespace monitor {
namespace w65c816s {

using byte_t = uint8_t;
using opcode_t = byte_t;                        // 8 bit operation code
using operand_t = uint16_t;                     // 8-16 bit operand
using address_t = uint32_t;                     // 24-bit address
enum class opname_t                             // operation mnemonic
{
    UNK,                                        // unknown
    ADC, AND, ASL, BCC, BCS, BEQ, BIT, BMI,
    BNE, BPL, BRA, BRK, BRL, BVC, BVS, CLC,
    CLD, CLI, CLV, CMP, COP, CPX, CPY, DEC,
    DEX, DEY, EOR, INC, INX, INY, JML, JMP,
    JSL, JSR, LDA, LDX, LDY, LSR, MVN, MVP,
    NOP, ORA, PEA, PEI, PER, PHA, PHB, PHD,
    PHK, PHP, PHX, PHY, PLA, PLB, PLD, PLP,
    PLX, PLY, REP, ROL, ROR, RTI, RTL, RTS,
    SBC, SEP, SEC, SED, SEI, STA, STP, STX,
    STY, STZ, TAX, TAY, TCD, TCS, TDC, TRB,
    TSB, TSC, TSX, TXA, TXS, TXY, TYA, TYX,
    WAI, WDM, XBA, XCE
};
enum class admode_t                     // addressing mode
{
    UNK,                                // unknown
    ABS,                                // absolute
    A,                                  // accumulator
    ABSX,                               // absolute X indexed
    ABSY,                               // absolute Y indexed
    LABS,                               // absolute long
    LABSX,                              // absolute long X indexed
    AIND,                               // absolute indirect
    AINDX,                              // absolute indirect X indexed
    DP,                                 // direct
    SR,                                 // stack relative
    DPX,                                // direct X indexed
    DPY,                                // direct Y indexed
    IND,                                // direct indirect
    LIND,                               // direct indirect long
    SRINDY,                             // stack relative indirect indexed
    XIIND,                              // direct indexed indirect (#,X)
    INDYI,                              // direct indirect indexed (#),Y
    LINDYI,                             // direct indirect long indexed
    IMPL,                               // implied
    REL,                                // PC relative
    LREL,                               // PC relative long
    BLK,                                // block move
    IMM,                                // immediate
};

const char* name(opname_t);

struct disassembler_ctx
{
    address_t ip;                               // instruction pointer
    bool x = true;                              // processor flag bit 4
    bool m = true;                              // processor flag bit 5
};

opname_t opcode_opname(opcode_t);
admode_t opcode_admode(opcode_t);
bool opcode_is_16bit(opcode_t, disassembler_ctx&);
size_t opcode_size(opcode_t, disassembler_ctx&);
size_t admode_size(admode_t);

class inst
{
public:
    static constexpr size_t max_size() { return 4; }

    static inst make_unknown(disassembler_ctx&, opcode_t);
    
    inst() {}
    inst(const inst& i) = default;
    explicit inst(inst&& i) = default;
    template <class InputIterator>
    inst(disassembler_ctx& ctx, InputIterator begin, InputIterator end);

    address_t addr() const { return _addr; }
    size_t size() const { return _size; }
    opname_t opname() const { return _opname; }
    admode_t admode() const { return _admode; }
    operand_t operand() const { return _operand; }
    size_t operand_size() const { return _size - 1; }
    bool is_unknown() const { return _opname == opname_t::UNK; }

    std::string str() const;
    std::vector<byte_t> bytes() const;

private:
    template <class InputIterator>
    bool disassemble(disassembler_ctx& ctx, InputIterator begin,
                     InputIterator end);

    address_t _addr = 0;
    size_t _size = 0;                           // byte length
    opcode_t _opcode = 0;
    opname_t _opname = opname_t::UNK;
    admode_t _admode = admode_t::UNK;
    operand_t _operand = 0;
};

template <class InputIterator>
inst::inst(disassembler_ctx& ctx, InputIterator begin, InputIterator end)
    : _addr(ctx.ip)
{
    disassemble(ctx, begin, end);
    ctx.ip += _size;
}

template <class InputIterator>
bool inst::disassemble(disassembler_ctx& ctx, InputIterator begin,
                       InputIterator end)
{
    auto i = begin;
    if (i == end)
        return false;
    opcode_t opcode = *i++;
    auto size = opcode_size(opcode, ctx);
    operand_t operand = 0;
    for (size_t c = 1; c < size; c++)
    {
        if (i == end)
            return false;
        operand_t b = *i++;
        operand |= b << ((c-1)*8);
    }

    _size = size;
    _opcode = opcode;
    _opname = opcode_opname(opcode);
    _admode = opcode_admode(opcode);
    _operand = operand;
    return true;
}

} // namespace w65c816s
} // namespace monitor
