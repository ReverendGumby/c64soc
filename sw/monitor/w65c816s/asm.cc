#include <util/string.h>

#include "asm.h"

namespace monitor {
namespace w65c816s {

opname_t opcode_opname(opcode_t opcode)
{
    const opname_t table[] = {
        opname_t::BRK, opname_t::ORA, opname_t::COP, opname_t::ORA,
        opname_t::TSB, opname_t::ORA, opname_t::ASL, opname_t::ORA,
        opname_t::PHP, opname_t::ORA, opname_t::ASL, opname_t::PHD,
        opname_t::TSB, opname_t::ORA, opname_t::ASL, opname_t::ORA,
        opname_t::BPL, opname_t::ORA, opname_t::ORA, opname_t::ORA,
        opname_t::TRB, opname_t::ORA, opname_t::ASL, opname_t::ORA,
        opname_t::CLC, opname_t::ORA, opname_t::INC, opname_t::TCS,
        opname_t::TRB, opname_t::ORA, opname_t::ASL, opname_t::ORA,
        opname_t::JSR, opname_t::AND, opname_t::JSL, opname_t::AND,
        opname_t::BIT, opname_t::AND, opname_t::ROL, opname_t::AND,
        opname_t::PLP, opname_t::AND, opname_t::ROL, opname_t::PLD,
        opname_t::BIT, opname_t::AND, opname_t::ROL, opname_t::AND,
        opname_t::BMI, opname_t::AND, opname_t::AND, opname_t::AND,
        opname_t::BIT, opname_t::AND, opname_t::ROL, opname_t::AND,
        opname_t::SEC, opname_t::AND, opname_t::DEC, opname_t::TSC,
        opname_t::BIT, opname_t::AND, opname_t::ROL, opname_t::AND,
        opname_t::RTI, opname_t::EOR, opname_t::WDM, opname_t::EOR,
        opname_t::MVP, opname_t::EOR, opname_t::LSR, opname_t::EOR,
        opname_t::PHA, opname_t::EOR, opname_t::LSR, opname_t::PHK,
        opname_t::JMP, opname_t::EOR, opname_t::LSR, opname_t::EOR,
        opname_t::BVC, opname_t::EOR, opname_t::EOR, opname_t::EOR,
        opname_t::MVN, opname_t::EOR, opname_t::LSR, opname_t::EOR,
        opname_t::CLI, opname_t::EOR, opname_t::PHY, opname_t::TCD,
        opname_t::JMP, opname_t::EOR, opname_t::LSR, opname_t::EOR,
        opname_t::RTS, opname_t::ADC, opname_t::PER, opname_t::ADC,
        opname_t::STZ, opname_t::ADC, opname_t::ROR, opname_t::ADC,
        opname_t::PLA, opname_t::ADC, opname_t::ROR, opname_t::RTL,
        opname_t::JMP, opname_t::ADC, opname_t::ROR, opname_t::ADC,
        opname_t::BVS, opname_t::ADC, opname_t::ADC, opname_t::ADC,
        opname_t::STZ, opname_t::ADC, opname_t::ROR, opname_t::ADC,
        opname_t::SEI, opname_t::ADC, opname_t::PLY, opname_t::TDC,
        opname_t::JMP, opname_t::ADC, opname_t::ROR, opname_t::ADC,
        opname_t::BRA, opname_t::STA, opname_t::BRL, opname_t::STA,
        opname_t::STY, opname_t::STA, opname_t::STX, opname_t::STA,
        opname_t::DEY, opname_t::BIT, opname_t::TXA, opname_t::PHB,
        opname_t::STY, opname_t::STA, opname_t::STX, opname_t::STA,
        opname_t::BCC, opname_t::STA, opname_t::STA, opname_t::STA,
        opname_t::STY, opname_t::STA, opname_t::STX, opname_t::STA,
        opname_t::TYA, opname_t::STA, opname_t::TXS, opname_t::TXY,
        opname_t::STZ, opname_t::STA, opname_t::STZ, opname_t::STA,
        opname_t::LDY, opname_t::LDA, opname_t::LDX, opname_t::LDA,
        opname_t::LDY, opname_t::LDA, opname_t::LDX, opname_t::LDA,
        opname_t::TAY, opname_t::LDA, opname_t::TAX, opname_t::PLB,
        opname_t::LDY, opname_t::LDA, opname_t::LDX, opname_t::LDA,
        opname_t::BCS, opname_t::LDA, opname_t::LDA, opname_t::LDA,
        opname_t::LDY, opname_t::LDA, opname_t::LDX, opname_t::LDA,
        opname_t::CLV, opname_t::LDA, opname_t::TSX, opname_t::TYX,
        opname_t::LDY, opname_t::LDA, opname_t::LDX, opname_t::LDA,
        opname_t::CPY, opname_t::CMP, opname_t::REP, opname_t::CMP,
        opname_t::CPY, opname_t::CMP, opname_t::DEC, opname_t::CMP,
        opname_t::INY, opname_t::CMP, opname_t::DEX, opname_t::WAI,
        opname_t::CPY, opname_t::CMP, opname_t::DEC, opname_t::CMP,
        opname_t::BNE, opname_t::CMP, opname_t::CMP, opname_t::CMP,
        opname_t::PEI, opname_t::CMP, opname_t::DEC, opname_t::CMP,
        opname_t::CLD, opname_t::CMP, opname_t::PHX, opname_t::STP,
        opname_t::JML, opname_t::CMP, opname_t::DEC, opname_t::CMP,
        opname_t::CPX, opname_t::SBC, opname_t::SEP, opname_t::SBC,
        opname_t::CPX, opname_t::SBC, opname_t::INC, opname_t::SBC,
        opname_t::INX, opname_t::SBC, opname_t::NOP, opname_t::XBA,
        opname_t::CPX, opname_t::SBC, opname_t::INC, opname_t::SBC,
        opname_t::BEQ, opname_t::SBC, opname_t::SBC, opname_t::SBC,
        opname_t::PEA, opname_t::SBC, opname_t::INC, opname_t::SBC,
        opname_t::SED, opname_t::SBC, opname_t::PLX, opname_t::XCE,
        opname_t::JSR, opname_t::SBC, opname_t::INC, opname_t::SBC,
    };
    return table[opcode];
}

admode_t opcode_admode(opcode_t opcode)
{
    const admode_t table[] = {
        admode_t::IMPL,   admode_t::XIIND,  admode_t::IMPL,   admode_t::SR,
        admode_t::DP,     admode_t::DP,     admode_t::DP,     admode_t::LIND,
        admode_t::IMPL,   admode_t::IMM,    admode_t::A,      admode_t::IMPL,
        admode_t::ABS,    admode_t::ABS,    admode_t::ABS,    admode_t::LABS,
        admode_t::REL,    admode_t::INDYI,  admode_t::IND,    admode_t::SRINDY,
        admode_t::DP,     admode_t::DPX,    admode_t::DPX,    admode_t::LINDYI,
        admode_t::IMPL,   admode_t::ABSY,   admode_t::A,      admode_t::IMPL,
        admode_t::ABS,    admode_t::ABSX,   admode_t::ABSX,   admode_t::LABSX,
        admode_t::ABS,    admode_t::XIIND,  admode_t::LABS,   admode_t::SR,
        admode_t::DP,     admode_t::DP,     admode_t::DP,     admode_t::LIND,
        admode_t::IMPL,   admode_t::IMM,    admode_t::A,      admode_t::IMPL,
        admode_t::ABS,    admode_t::ABS,    admode_t::ABS,    admode_t::LABS,
        admode_t::REL,    admode_t::INDYI,  admode_t::IND,    admode_t::SRINDY,
        admode_t::DPX,    admode_t::DPX,    admode_t::DPX,    admode_t::LINDYI,
        admode_t::IMPL,   admode_t::ABSY,   admode_t::A,      admode_t::IMPL,
        admode_t::ABSX,   admode_t::ABSX,   admode_t::ABSX,   admode_t::LABSX,
        admode_t::IMPL,   admode_t::XIIND,  admode_t::IMPL,   admode_t::SR,
        admode_t::BLK,    admode_t::DP,     admode_t::DP,     admode_t::LIND,
        admode_t::IMPL,   admode_t::IMM,    admode_t::A,      admode_t::IMPL,
        admode_t::ABS,    admode_t::ABS,    admode_t::ABS,    admode_t::LABS,
        admode_t::REL,    admode_t::INDYI,  admode_t::IND,    admode_t::SRINDY,
        admode_t::BLK,    admode_t::DPX,    admode_t::DPX,    admode_t::LINDYI,
        admode_t::IMPL,   admode_t::ABSY,   admode_t::IMPL,   admode_t::IMPL,
        admode_t::LABS,   admode_t::ABSX,   admode_t::ABSX,   admode_t::LABSX,
        admode_t::IMPL,   admode_t::XIIND,  admode_t::LREL,   admode_t::SR,
        admode_t::DP,     admode_t::DP,     admode_t::DP,     admode_t::LIND,
        admode_t::IMPL,   admode_t::IMM,    admode_t::A,      admode_t::IMPL,
        admode_t::AIND,   admode_t::ABS,    admode_t::ABS,    admode_t::LABS,
        admode_t::REL,    admode_t::INDYI,  admode_t::IND,    admode_t::SRINDY,
        admode_t::DPX,    admode_t::DPX,    admode_t::DPX,    admode_t::LINDYI,
        admode_t::IMPL,   admode_t::ABSY,   admode_t::IMPL,   admode_t::IMPL,
        admode_t::AINDX,  admode_t::ABSX,   admode_t::ABSX,   admode_t::LABSX,
        admode_t::REL,    admode_t::XIIND,  admode_t::LREL,   admode_t::SR,
        admode_t::DP,     admode_t::DP,     admode_t::DP,     admode_t::LIND,
        admode_t::IMPL,   admode_t::IMM,    admode_t::IMPL,   admode_t::IMPL,
        admode_t::ABS,    admode_t::ABS,    admode_t::ABS,    admode_t::LABS,
        admode_t::REL,    admode_t::INDYI,  admode_t::IND,    admode_t::SRINDY,
        admode_t::DPX,    admode_t::DPX,    admode_t::DPY,    admode_t::LINDYI,
        admode_t::IMPL,   admode_t::ABSY,   admode_t::IMPL,   admode_t::IMPL,
        admode_t::ABS,    admode_t::ABSX,   admode_t::ABSX,   admode_t::LABSX,
        admode_t::IMM,    admode_t::XIIND,  admode_t::IMM,    admode_t::SR,
        admode_t::DP,     admode_t::DP,     admode_t::DP,     admode_t::LIND,
        admode_t::IMPL,   admode_t::IMM,    admode_t::IMPL,   admode_t::IMPL,
        admode_t::ABS,    admode_t::ABS,    admode_t::ABS,    admode_t::LABS,
        admode_t::REL,    admode_t::INDYI,  admode_t::IND,    admode_t::SRINDY,
        admode_t::DPX,    admode_t::DPX,    admode_t::DPY,    admode_t::LINDYI,
        admode_t::IMPL,   admode_t::ABSY,   admode_t::IMPL,   admode_t::IMPL,
        admode_t::ABSX,   admode_t::ABSX,   admode_t::ABSY,   admode_t::LABSX,
        admode_t::IMM,    admode_t::XIIND,  admode_t::IMM,    admode_t::SR,
        admode_t::DP,     admode_t::DP,     admode_t::DP,     admode_t::LIND,
        admode_t::IMPL,   admode_t::IMM,    admode_t::IMPL,   admode_t::IMPL,
        admode_t::ABS,    admode_t::ABS,    admode_t::ABS,    admode_t::LABS,
        admode_t::REL,    admode_t::INDYI,  admode_t::IND,    admode_t::SRINDY,
        admode_t::DP,     admode_t::DPX,    admode_t::DPX,    admode_t::LINDYI,
        admode_t::IMPL,   admode_t::ABSY,   admode_t::IMPL,   admode_t::IMPL,
        admode_t::AIND,   admode_t::ABSX,   admode_t::ABSX,   admode_t::LABSX,
        admode_t::IMM,    admode_t::XIIND,  admode_t::IMM,    admode_t::SR,
        admode_t::DP,     admode_t::DP,     admode_t::DP,     admode_t::LIND,
        admode_t::IMPL,   admode_t::IMM,    admode_t::IMPL,   admode_t::IMPL,
        admode_t::ABS,    admode_t::ABS,    admode_t::ABS,    admode_t::LABS,
        admode_t::REL,    admode_t::INDYI,  admode_t::IND,    admode_t::SRINDY,
        admode_t::ABS,    admode_t::DPX,    admode_t::DPX,    admode_t::LINDYI,
        admode_t::IMPL,   admode_t::ABSY,   admode_t::IMPL,   admode_t::IMPL,
        admode_t::AINDX,  admode_t::ABSX,   admode_t::ABSX,   admode_t::LABSX,
    };
    return table[opcode];
}

const char* name(opname_t opname)
{
    switch (opname)
    {
    case opname_t::UNK: return "DCB";           // nearly "DC.B"
    case opname_t::ADC: return "ADC";
    case opname_t::AND: return "AND";
    case opname_t::ASL: return "ASL";
    case opname_t::BCC: return "BCC";
    case opname_t::BCS: return "BCS";
    case opname_t::BEQ: return "BEQ";
    case opname_t::BIT: return "BIT";
    case opname_t::BMI: return "BMI";
    case opname_t::BNE: return "BNE";
    case opname_t::BPL: return "BPL";
    case opname_t::BRA: return "BRA";
    case opname_t::BRK: return "BRK";
    case opname_t::BRL: return "BRL";
    case opname_t::BVC: return "BVC";
    case opname_t::BVS: return "BVS";
    case opname_t::CLC: return "CLC";
    case opname_t::CLD: return "CLD";
    case opname_t::CLI: return "CLI";
    case opname_t::CLV: return "CLV";
    case opname_t::CMP: return "CMP";
    case opname_t::COP: return "COP";
    case opname_t::CPX: return "CPX";
    case opname_t::CPY: return "CPY";
    case opname_t::DEC: return "DEC";
    case opname_t::DEX: return "DEX";
    case opname_t::DEY: return "DEY";
    case opname_t::EOR: return "EOR";
    case opname_t::INC: return "INC";
    case opname_t::INX: return "INX";
    case opname_t::INY: return "INY";
    case opname_t::JML: return "JML";
    case opname_t::JMP: return "JMP";
    case opname_t::JSL: return "JSL";
    case opname_t::JSR: return "JSR";
    case opname_t::LDA: return "LDA";
    case opname_t::LDX: return "LDX";
    case opname_t::LDY: return "LDY";
    case opname_t::LSR: return "LSR";
    case opname_t::MVN: return "MVN";
    case opname_t::MVP: return "MVP";
    case opname_t::NOP: return "NOP";
    case opname_t::ORA: return "ORA";
    case opname_t::PEA: return "PEA";
    case opname_t::PEI: return "PEI";
    case opname_t::PER: return "PER";
    case opname_t::PHA: return "PHA";
    case opname_t::PHB: return "PHB";
    case opname_t::PHD: return "PHD";
    case opname_t::PHK: return "PHK";
    case opname_t::PHP: return "PHP";
    case opname_t::PHX: return "PHX";
    case opname_t::PHY: return "PHY";
    case opname_t::PLA: return "PLA";
    case opname_t::PLB: return "PLB";
    case opname_t::PLD: return "PLD";
    case opname_t::PLP: return "PLP";
    case opname_t::PLX: return "PLX";
    case opname_t::PLY: return "PLY";
    case opname_t::REP: return "REP";
    case opname_t::ROL: return "ROL";
    case opname_t::ROR: return "ROR";
    case opname_t::RTI: return "RTI";
    case opname_t::RTL: return "RTL";
    case opname_t::RTS: return "RTS";
    case opname_t::SBC: return "SBC";
    case opname_t::SEP: return "SEP";
    case opname_t::SEC: return "SEC";
    case opname_t::SED: return "SED";
    case opname_t::SEI: return "SEI";
    case opname_t::STA: return "STA";
    case opname_t::STP: return "STP";
    case opname_t::STX: return "STX";
    case opname_t::STY: return "STY";
    case opname_t::STZ: return "STZ";
    case opname_t::TAX: return "TAX";
    case opname_t::TAY: return "TAY";
    case opname_t::TCD: return "TCD";
    case opname_t::TCS: return "TCS";
    case opname_t::TDC: return "TDC";
    case opname_t::TRB: return "TRB";
    case opname_t::TSB: return "TSB";
    case opname_t::TSC: return "TSC";
    case opname_t::TSX: return "TSX";
    case opname_t::TXA: return "TXA";
    case opname_t::TXS: return "TXS";
    case opname_t::TXY: return "TXY";
    case opname_t::TYA: return "TYA";
    case opname_t::TYX: return "TYX";
    case opname_t::WAI: return "WAI";
    case opname_t::WDM: return "WDM";
    case opname_t::XBA: return "XBA";
    case opname_t::XCE: return "XCE";
    default: return "???";
    }
}

bool opcode_is_16bit(opcode_t opcode, disassembler_ctx& ctx)
{
    auto opname = opcode_opname(opcode);
    switch (opname) {
    case opname_t::CPX:
    case opname_t::CPY:
    case opname_t::DEX:
    case opname_t::DEY:
    case opname_t::INX:
    case opname_t::INY:
    case opname_t::LDX:
    case opname_t::LDY:
    case opname_t::PHX:
    case opname_t::PHY:
    case opname_t::PLX:
    case opname_t::PLY:
    case opname_t::STX:
    case opname_t::STY:
        return !ctx.x;
    case opname_t::PHB:
    case opname_t::PHK:
    case opname_t::PHP:
    case opname_t::PLB:
    case opname_t::PLP:
    case opname_t::REP:
    case opname_t::SEP:
        return false;
    case opname_t::PHD:
    case opname_t::PEA:
    case opname_t::PEI:
    case opname_t::PER:
    case opname_t::PLD:
        return true;
    default:
        return !ctx.m;
    }
}

size_t opcode_size(opcode_t opcode, disassembler_ctx& ctx)
{
    auto admode = opcode_admode(opcode);
    auto ret = admode_size(admode);
    if (admode == admode_t::IMM && opcode_is_16bit(opcode, ctx))
        ret ++;
    return ret;
}

size_t admode_size(admode_t admode)
{
    switch (admode)
    {
    case admode_t::UNK:
    case admode_t::IMPL:
    case admode_t::A:
        return 1;
    case admode_t::IMM:
    case admode_t::DP:
    case admode_t::DPX:
    case admode_t::DPY:
    case admode_t::SR:
    case admode_t::SRINDY:
    case admode_t::REL:
    case admode_t::IND:
    case admode_t::XIIND:
    case admode_t::INDYI:
    case admode_t::LINDYI:
        return 2;
    case admode_t::ABS:
    case admode_t::ABSX:
    case admode_t::ABSY:
    case admode_t::AIND:
    case admode_t::AINDX:
    case admode_t::LREL:
    case admode_t::LIND:
    case admode_t::BLK:
        return 3;
    case admode_t::LABS:
    case admode_t::LABSX:
        return 4;
    default:
        return 0;
    }
}

inst inst::make_unknown(disassembler_ctx& ctx, opcode_t opcode)
{
    inst i;
    i._addr = ctx.ip;
    i._size = 1;
    i._opcode = opcode;
    ctx.ip += i._size;
    return i;
}

std::string inst::str() const
{
    std::string ret{name(_opname)};

    switch (_admode)
    {
    case admode_t::UNK:
        ret += string_printf(" $%02X", _opcode);
        break;
    case admode_t::ABS:
        ret += string_printf(" $%04X", _operand);
        break;
    case admode_t::A:
        ret += " A";
        break;
    case admode_t::ABSX:
        ret += string_printf(" $%04X,X", _operand);
        break;
    case admode_t::ABSY:
        ret += string_printf(" $%04X,Y", _operand);
        break;
    case admode_t::LABS:
        ret += string_printf(" $%06X", _operand);
        break;
    case admode_t::LABSX:
        ret += string_printf(" $%06X,X", _operand);
        break;
    case admode_t::AIND:
        ret += string_printf(" ($%04X)", _operand);
        break;
    case admode_t::AINDX:
        ret += string_printf(" ($%04X,X)", _operand);
        break;
    case admode_t::DP:
        ret += string_printf(" $%02X", _operand);
        break;
    case admode_t::SR:
        ret += string_printf(" ($%02X,S)", _operand);
        break;
    case admode_t::DPX:
        ret += string_printf(" $%02X,X", _operand);
        break;
    case admode_t::DPY:
        ret += string_printf(" $%02X,Y", _operand);
        break;
    case admode_t::IND:
        ret += string_printf(" ($%02X)", _operand);
        break;
    case admode_t::LIND:
        ret += string_printf(" [$%02X]", _operand);
        break;
    case admode_t::SRINDY:
        ret += string_printf(" ($%02X,S),Y", _operand);
        break;
    case admode_t::XIIND:
        ret += string_printf(" ($%02X,X)", _operand);
        break;
    case admode_t::INDYI:
        ret += string_printf(" ($%02X),Y", _operand);
        break;
    case admode_t::LINDYI:
        ret += string_printf(" [$%02X],Y", _operand);
        break;
    case admode_t::IMPL:
        break;
    case admode_t::REL:
        {
            int offset = int8_t(_operand) + 2;
            if (_addr)
            {
                address_t dest = _addr + offset;
                ret += string_printf(" $%04X", dest);
            }
            else
            {
                if (offset >= 0)
                    ret += string_printf(" .+$%02X", offset);
                else
                    ret += string_printf(" .-$%02X", -offset);
            }
        }
        break;
    case admode_t::LREL:
        {
            int offset = int16_t(_operand) + 3;
            if (_addr)
            {
                address_t dest = _addr + offset;
                ret += string_printf(" $%06X", dest);
            }
            else
            {
                if (offset >= 0)
                    ret += string_printf(" .+$%04X", offset);
                else
                    ret += string_printf(" .-$%04X", -offset);
            }
        }
        break;
    case admode_t::BLK:
        {
            uint8_t dba = uint8_t(_operand);
            uint8_t sba = uint8_t(_operand >> 8);
            ret += string_printf(" #$%02X,#$%02X", dba, sba);
        }
        break;
    case admode_t::IMM:
        {
            const char* fmt = (_size == 3) ? " #%04X" : " #%02X";
            ret += string_printf(fmt, _operand);
        }
        break;
    }
    return ret;
}

std::vector<byte_t> inst::bytes() const
{
    std::vector<byte_t> ret { _opcode };
    for (size_t c = 1; c < _size; c++)
    {
        byte_t b = _operand >> ((c-1)*8);
        ret.push_back(b);
    }
    return ret;
}

} // namespace w65c816s
} // namespace monitor
