#include <sstream>
#include <iomanip>

#include "reg_printer.h"

namespace monitor {

reg_printer::reg_printer(ui::screenstream* ss, int x_lbl, int w_lbl)
    : ss(ss), x_lbl(x_lbl), w_lbl(w_lbl)
{
}

void reg_printer::print_label(const char* label)
{
    *ss << ui::pos(x_lbl, y) << std::right << std::setfill(' ')
        << std::setw(w_lbl) << label
        << std::left << ' ';
}

void reg_printer::print(const char* label, const std::string& str)
{
    print(label, str, str);
}

void reg_printer::print(const char* label, const std::string& new_str, const std::string& old_str, bool per_char_diff)
{
    print_label(label);

    if (per_char_diff)
    {
        for (size_t i = 0; i < new_str.size(); i++)
        {
            bool diff = i < old_str.size() && new_str[i] != old_str[i];
            *ss << ui::rev(diff) << new_str[i];
        }
    }
    else
    {
        bool diff = &new_str != &old_str && new_str != old_str;
        *ss << ui::rev(diff) << new_str;
        *ss << ui::revoff << std::endl;
    }
    *ss << ui::revoff << std::endl;
    y ++;
}

} // namespace monitor
