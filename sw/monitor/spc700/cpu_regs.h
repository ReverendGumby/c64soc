#pragma once

#include <stdint.h>

namespace monitor {
namespace spc700 {

struct cpu_regs
{
    uint16_t pc;
    struct pf_t {
        bool c;                                 // pf[0]
        bool z;                                 // pf[1]
        bool i;                                 // pf[2]
        bool h;                                 // pf[3]
        bool b;                                 // pf[4]
        bool p;                                 // pf[5]
        bool v;                                 // pf[6]
        bool n;                                 // pf[7]
    } pf;                                       // processor flags
    uint8_t s;
    uint8_t ac;
    uint8_t x;
    uint8_t y;
    uint8_t ir;                                 // instruction register
    uint16_t aor;                               // address output register
    uint8_t dor;                                // data output register
    uint8_t z;
    struct {
        uint8_t ts;
    } tstate;                                   // Timing State
    struct {
        bool rw;
        bool sync;
    } pstate;                                   // Pin State

    // Internal peripheral registers
    struct periph_t {
        uint8_t cpui[4];
        uint8_t cpuo[4];
        uint8_t spcr;
        uint8_t spt[3];
        uint8_t spc[3];                         // (read-only)
    } periph;

    struct internal_t {
        // APU_CORE_MON_INT0_*
        uint32_t cken_d : 1;
        uint32_t cp1d : 1;
        uint32_t resp : 1;
        uint32_t am_done_d : 1;
        uint32_t cco : 1;
        uint32_t cvo : 1;
        uint32_t cho : 1;
        uint32_t cl_store_dor_d : 1;
        uint32_t cl_int_op_d : 1;

        // APU_CORE_MON_INT1_*
        uint32_t t : 18;
        uint32_t bra_t : 4;
        uint32_t divcnt : 7;

        // APU_CORE_MON_INT2_*
        uint32_t am_t : 16;
        uint32_t dl : 8;

        // APU_CORE_MON_INT3_*
        uint32_t op_t : 24;

        // APU_CORE_MON_INT4_*
        uint32_t ico : 8;
        uint32_t icoh : 9;
        uint32_t mulscnt : 3;

        // APU_CORE_MON_INT5_*
        uint32_t timer0 : 13;

        // APU_CORE_MON_INT6_*
        uint32_t timer1 : 13;

        // APU_CORE_MON_INT7_*
        uint32_t timer2 : 13;
    } internal;
};

} // namespace spc700
} // namespace monitor
