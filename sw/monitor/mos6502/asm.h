#pragma once

#include <stddef.h>
#include <stdint.h>
#include <iterator>
#include <string>
#include <vector>

namespace monitor {
namespace mos6502 {

using byte_t = uint8_t;
using opcode_t = byte_t;                        // 8 bit operation code
using operand_t = uint16_t;                     // 8-16 bit operand
using address_t = uint16_t;
enum class opname_t                             // operation mnemonic
{
    UNK,                                        // unknown
    ADC, AND, ASL, BCC, BCS, BEQ, BIT, BMI,
    BNE, BPL, BRK, BVC, BVS, CLC, CLD, CLI,
    CLV, CMP, CPX, CPY, DEC, DEX, DEY, EOR,
    INC, INX, INY, JMP, JSR, LDA, LDX, LDY,
    LSR, NOP, ORA, PHA, PHP, PLA, PLP, ROL,
    ROR, RTI, RTS, SBC, SEC, SED, SEI, STA,
    STX, STY, TAX, TAY, TSX, TXA, TXS, TYA
};
enum class admode_t                             // addressing mode
{
    UNK,                                        // unknown
    IMPL,                                       // implied
    A,                                          // accumulator
    IMM,                                        // immediate
    ABS,                                        // absolute
    ABSX,                                       // absolute X indexed
    ABSY,                                       // absolute Y indexed
    ZP,                                         // zero page
    ZPX,                                        // zero page X indexed
    ZPY,                                        // zero page Y indexed
    REL,                                        // relative branch
    INDX,                                       // indirect indexed (#),Y
    INDY,                                       // indexed indirect (#,X)
    IND2,                                       // indirect JMP
};

const char* name(opname_t);

opname_t opcode_opname(opcode_t);
admode_t opcode_admode(opcode_t);
size_t opcode_size(opcode_t);
size_t admode_size(admode_t);

struct disassembler_ctx
{
    address_t ip;                               // instruction pointer
};

constexpr opcode_t unknown_opcode = 0xFF;

class inst
{
public:
    static constexpr size_t max_size() { return 3; }

    static inst make_unknown(disassembler_ctx&, opcode_t);
    
    inst() {}
    inst(const inst& i) = default;
    explicit inst(inst&& i) = default;
    template <class InputIterator>
    inst(disassembler_ctx& ctx, InputIterator begin, InputIterator end);

    address_t addr() const { return _addr; }
    size_t size() const { return _size; }
    opname_t opname() const { return _opname; }
    admode_t admode() const { return _admode; }
    operand_t operand() const { return _operand; }
    size_t operand_size() const { return _size - 1; }
    bool is_unknown() const { return _opname == opname_t::UNK; }

    std::string str() const;
    std::vector<byte_t> bytes() const;

private:
    template <class InputIterator>
    bool disassemble(disassembler_ctx& ctx, InputIterator begin,
                     InputIterator end);

    address_t _addr = 0;
    size_t _size = 0;                           // byte length
    opcode_t _opcode = unknown_opcode;
    opname_t _opname = opname_t::UNK;
    admode_t _admode = admode_t::UNK;
    operand_t _operand = 0;
};

template <class InputIterator>
inst::inst(disassembler_ctx& ctx, InputIterator begin, InputIterator end)
    : _addr(ctx.ip)
{
    disassemble(ctx, begin, end);
    ctx.ip += _size;
}

template <class InputIterator>
bool inst::disassemble(disassembler_ctx& ctx, InputIterator begin,
                       InputIterator end)
{
    auto i = begin;
    if (i == end)
        return false;
    opcode_t opcode = *i++;
    auto size = opcode_size(opcode);
    operand_t operand = 0;
    for (size_t c = 1; c < size; c++)
    {
        if (i == end)
            return false;
        operand_t b = *i++;
        operand |= b << ((c-1)*8);
    }

    _size = size;
    _opcode = opcode;
    _opname = opcode_opname(opcode);
    _admode = opcode_admode(opcode);
    _operand = operand;
    return true;
}

} // namespace mos6502
} // namespace monitor
