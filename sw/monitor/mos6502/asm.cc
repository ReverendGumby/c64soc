#include <util/string.h>

#include "asm.h"

namespace monitor {
namespace mos6502 {

opname_t opcode_opname(opcode_t opcode)
{
    const opname_t table[] = {
        opname_t::BRK, opname_t::ORA, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::ORA, opname_t::ASL, opname_t::UNK,
        opname_t::PHP, opname_t::ORA, opname_t::ASL, opname_t::UNK,
        opname_t::UNK, opname_t::ORA, opname_t::ASL, opname_t::UNK,
        opname_t::BPL, opname_t::ORA, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::ORA, opname_t::ASL, opname_t::UNK,
        opname_t::CLC, opname_t::ORA, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::ORA, opname_t::ASL, opname_t::UNK,
        opname_t::JSR, opname_t::AND, opname_t::UNK, opname_t::UNK,
        opname_t::BIT, opname_t::AND, opname_t::ROL, opname_t::UNK,
        opname_t::PLP, opname_t::AND, opname_t::ROL, opname_t::UNK,
        opname_t::BIT, opname_t::AND, opname_t::ROL, opname_t::UNK,
        opname_t::BMI, opname_t::AND, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::AND, opname_t::ROL, opname_t::UNK,
        opname_t::SEC, opname_t::AND, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::AND, opname_t::ROL, opname_t::UNK,
        opname_t::RTI, opname_t::EOR, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::EOR, opname_t::LSR, opname_t::UNK,
        opname_t::PHA, opname_t::EOR, opname_t::LSR, opname_t::UNK,
        opname_t::JMP, opname_t::EOR, opname_t::LSR, opname_t::UNK,
        opname_t::BVC, opname_t::EOR, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::EOR, opname_t::LSR, opname_t::UNK,
        opname_t::CLI, opname_t::EOR, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::EOR, opname_t::LSR, opname_t::UNK,
        opname_t::RTS, opname_t::ADC, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::ADC, opname_t::ROR, opname_t::UNK,
        opname_t::PLA, opname_t::ADC, opname_t::ROR, opname_t::UNK,
        opname_t::JMP, opname_t::ADC, opname_t::ROR, opname_t::UNK,
        opname_t::BVS, opname_t::ADC, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::ADC, opname_t::ROR, opname_t::UNK,
        opname_t::SEI, opname_t::ADC, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::ADC, opname_t::ROR, opname_t::UNK,
        opname_t::UNK, opname_t::STA, opname_t::UNK, opname_t::UNK,
        opname_t::STY, opname_t::STA, opname_t::STX, opname_t::UNK,
        opname_t::DEY, opname_t::UNK, opname_t::TXA, opname_t::UNK,
        opname_t::STY, opname_t::STA, opname_t::STX, opname_t::UNK,
        opname_t::BCC, opname_t::STA, opname_t::UNK, opname_t::UNK,
        opname_t::STY, opname_t::STA, opname_t::STX, opname_t::UNK,
        opname_t::TYA, opname_t::STA, opname_t::TXS, opname_t::UNK,
        opname_t::UNK, opname_t::STA, opname_t::UNK, opname_t::UNK,
        opname_t::LDY, opname_t::LDA, opname_t::LDX, opname_t::UNK,
        opname_t::LDY, opname_t::LDA, opname_t::LDX, opname_t::UNK,
        opname_t::TAY, opname_t::LDA, opname_t::TAX, opname_t::UNK,
        opname_t::LDY, opname_t::LDA, opname_t::LDX, opname_t::UNK,
        opname_t::BCS, opname_t::LDA, opname_t::UNK, opname_t::UNK,
        opname_t::LDY, opname_t::LDA, opname_t::LDX, opname_t::UNK,
        opname_t::CLV, opname_t::LDA, opname_t::TSX, opname_t::UNK,
        opname_t::LDY, opname_t::LDA, opname_t::LDX, opname_t::UNK,
        opname_t::CPY, opname_t::CMP, opname_t::UNK, opname_t::UNK,
        opname_t::CPY, opname_t::CMP, opname_t::DEC, opname_t::UNK,
        opname_t::INY, opname_t::CMP, opname_t::DEX, opname_t::UNK,
        opname_t::CPY, opname_t::CMP, opname_t::DEC, opname_t::UNK,
        opname_t::BNE, opname_t::CMP, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::CMP, opname_t::DEC, opname_t::UNK,
        opname_t::CLD, opname_t::CMP, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::CMP, opname_t::DEC, opname_t::UNK,
        opname_t::CPX, opname_t::SBC, opname_t::UNK, opname_t::UNK,
        opname_t::CPX, opname_t::SBC, opname_t::INC, opname_t::UNK,
        opname_t::INX, opname_t::SBC, opname_t::NOP, opname_t::UNK,
        opname_t::CPX, opname_t::SBC, opname_t::INC, opname_t::UNK,
        opname_t::BEQ, opname_t::SBC, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::SBC, opname_t::INC, opname_t::UNK,
        opname_t::SED, opname_t::SBC, opname_t::UNK, opname_t::UNK,
        opname_t::UNK, opname_t::SBC, opname_t::INC, opname_t::UNK,
    };
    return table[opcode];
}

admode_t opcode_admode(opcode_t opcode)
{
    const admode_t table[] = {
        admode_t::IMPL, admode_t::INDX, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ZP,   admode_t::ZP,   admode_t::UNK, 
        admode_t::IMPL, admode_t::IMM,  admode_t::A,    admode_t::UNK, 
        admode_t::UNK,  admode_t::ABS,  admode_t::ABS,  admode_t::UNK, 
        admode_t::REL,  admode_t::INDY, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ZPX,  admode_t::ZPX,  admode_t::UNK, 
        admode_t::IMPL, admode_t::ABSY, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ABSX, admode_t::ABSX, admode_t::UNK, 
        admode_t::ABS,  admode_t::INDX, admode_t::UNK,  admode_t::UNK, 
        admode_t::ZP,   admode_t::ZP,   admode_t::ZP,   admode_t::UNK, 
        admode_t::IMPL, admode_t::IMM,  admode_t::A,    admode_t::UNK, 
        admode_t::ABS,  admode_t::ABS,  admode_t::ABS,  admode_t::UNK, 
        admode_t::REL,  admode_t::INDY, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ZPX,  admode_t::ZPX,  admode_t::UNK, 
        admode_t::IMPL, admode_t::ABSY, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ABSX, admode_t::ABSX, admode_t::UNK, 
        admode_t::IMPL, admode_t::INDX, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ZP,   admode_t::ZP,   admode_t::UNK, 
        admode_t::IMPL, admode_t::IMM,  admode_t::A,    admode_t::UNK, 
        admode_t::ABS,  admode_t::ABS,  admode_t::ABS,  admode_t::UNK, 
        admode_t::REL,  admode_t::INDY, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ZPX,  admode_t::ZPX,  admode_t::UNK, 
        admode_t::IMPL, admode_t::ABSY, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ABSX, admode_t::ABSX, admode_t::UNK, 
        admode_t::IMPL, admode_t::INDX, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ZP,   admode_t::ZP,   admode_t::UNK, 
        admode_t::IMPL, admode_t::IMM,  admode_t::A,    admode_t::UNK, 
        admode_t::IND2, admode_t::ABS,  admode_t::ABS,  admode_t::UNK, 
        admode_t::REL,  admode_t::INDY, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ZPX,  admode_t::ZPX,  admode_t::UNK, 
        admode_t::IMPL, admode_t::ABSY, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ABSX, admode_t::ABSX, admode_t::UNK, 
        admode_t::UNK,  admode_t::INDX, admode_t::UNK,  admode_t::UNK, 
        admode_t::ZP,   admode_t::ZP,   admode_t::ZP,   admode_t::UNK, 
        admode_t::IMPL, admode_t::UNK,  admode_t::IMPL, admode_t::UNK, 
        admode_t::ABS,  admode_t::ABS,  admode_t::ABS,  admode_t::UNK, 
        admode_t::REL,  admode_t::INDY, admode_t::UNK,  admode_t::UNK, 
        admode_t::ZPX,  admode_t::ZPX,  admode_t::ZPY,  admode_t::UNK, 
        admode_t::IMPL, admode_t::ABSY, admode_t::IMPL, admode_t::UNK, 
        admode_t::UNK,  admode_t::ABSX, admode_t::UNK,  admode_t::UNK, 
        admode_t::IMM,  admode_t::INDX, admode_t::IMM,  admode_t::UNK, 
        admode_t::ZP,   admode_t::ZP,   admode_t::ZP,   admode_t::UNK, 
        admode_t::IMPL, admode_t::IMM,  admode_t::IMPL, admode_t::UNK, 
        admode_t::ABS,  admode_t::ABS,  admode_t::ABS,  admode_t::UNK, 
        admode_t::REL,  admode_t::INDY, admode_t::UNK,  admode_t::UNK, 
        admode_t::ZPX,  admode_t::ZPX,  admode_t::ZPY,  admode_t::UNK, 
        admode_t::IMPL, admode_t::ABSY, admode_t::IMPL, admode_t::UNK, 
        admode_t::ABSX, admode_t::ABSX, admode_t::ABSY, admode_t::UNK, 
        admode_t::IMM,  admode_t::INDX, admode_t::UNK,  admode_t::UNK, 
        admode_t::ZP,   admode_t::ZP,   admode_t::ZP,   admode_t::UNK, 
        admode_t::IMPL, admode_t::IMM,  admode_t::IMPL, admode_t::UNK, 
        admode_t::ABS,  admode_t::ABS,  admode_t::ABS,  admode_t::UNK, 
        admode_t::REL,  admode_t::INDY, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ZPX,  admode_t::ZPX,  admode_t::UNK, 
        admode_t::IMPL, admode_t::ABSY, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ABSX, admode_t::ABSX, admode_t::UNK, 
        admode_t::IMM,  admode_t::INDX, admode_t::UNK,  admode_t::UNK, 
        admode_t::ZP,   admode_t::ZP,   admode_t::ZP,   admode_t::UNK, 
        admode_t::IMPL, admode_t::IMM,  admode_t::IMPL, admode_t::UNK, 
        admode_t::ABS,  admode_t::ABS,  admode_t::ABS,  admode_t::UNK, 
        admode_t::REL,  admode_t::INDY, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ZPX,  admode_t::ZPX,  admode_t::UNK, 
        admode_t::IMPL, admode_t::ABSY, admode_t::UNK,  admode_t::UNK, 
        admode_t::UNK,  admode_t::ABSX, admode_t::ABSX, admode_t::UNK, 
    };
    return table[opcode];
}

const char* name(opname_t opname)
{
    switch (opname)
    {
    case opname_t::UNK: return "DCB";           // nearly "DC.B"
    case opname_t::ADC: return "ADC";
    case opname_t::AND: return "AND";
    case opname_t::ASL: return "ASL";
    case opname_t::BCC: return "BCC";
    case opname_t::BCS: return "BCS";
    case opname_t::BEQ: return "BEQ";
    case opname_t::BIT: return "BIT";
    case opname_t::BMI: return "BMI";
    case opname_t::BNE: return "BNE";
    case opname_t::BPL: return "BPL";
    case opname_t::BRK: return "BRK";
    case opname_t::BVC: return "BVC";
    case opname_t::BVS: return "BVS";
    case opname_t::CLC: return "CLC";
    case opname_t::CLD: return "CLD";
    case opname_t::CLI: return "CLI";
    case opname_t::CLV: return "CLV";
    case opname_t::CMP: return "CMP";
    case opname_t::CPX: return "CPX";
    case opname_t::CPY: return "CPY";
    case opname_t::DEC: return "DEC";
    case opname_t::DEX: return "DEX";
    case opname_t::DEY: return "DEY";
    case opname_t::EOR: return "EOR";
    case opname_t::INC: return "INC";
    case opname_t::INX: return "INX";
    case opname_t::INY: return "INY";
    case opname_t::JMP: return "JMP";
    case opname_t::JSR: return "JSR";
    case opname_t::LDA: return "LDA";
    case opname_t::LDX: return "LDX";
    case opname_t::LDY: return "LDY";
    case opname_t::LSR: return "LSR";
    case opname_t::NOP: return "NOP";
    case opname_t::ORA: return "ORA";
    case opname_t::PHA: return "PHA";
    case opname_t::PHP: return "PHP";
    case opname_t::PLA: return "PLA";
    case opname_t::PLP: return "PLP";
    case opname_t::ROL: return "ROL";
    case opname_t::ROR: return "ROR";
    case opname_t::RTI: return "RTI";
    case opname_t::RTS: return "RTS";
    case opname_t::SBC: return "SBC";
    case opname_t::SEC: return "SEC";
    case opname_t::SED: return "SED";
    case opname_t::SEI: return "SEI";
    case opname_t::STA: return "STA";
    case opname_t::STX: return "STX";
    case opname_t::STY: return "STY";
    case opname_t::TAX: return "TAX";
    case opname_t::TAY: return "TAY";
    case opname_t::TSX: return "TSX";
    case opname_t::TXA: return "TXA";
    case opname_t::TXS: return "TXS";
    case opname_t::TYA: return "TYA";
    default: return "???";
    }
}

size_t opcode_size(opcode_t opcode)
{
    return admode_size(opcode_admode(opcode));
}

size_t admode_size(admode_t admode)
{
    switch (admode)
    {
    case admode_t::UNK:
    case admode_t::IMPL:
    case admode_t::A:
        return 1;
    case admode_t::IMM:
    case admode_t::ZP:
    case admode_t::ZPX:
    case admode_t::ZPY:
    case admode_t::REL:
    case admode_t::INDX:
    case admode_t::INDY:
        return 2;
    case admode_t::ABS:
    case admode_t::ABSX:
    case admode_t::ABSY:
    case admode_t::IND2:
        return 3;
    default:
        return 0;
    }
}

inst inst::make_unknown(disassembler_ctx& ctx, opcode_t opcode)
{
    inst i;
    i._addr = ctx.ip;
    i._size = 1;
    i._opcode = opcode;
    ctx.ip += i._size;
    return i;
}

std::string inst::str() const
{
    std::string ret{name(_opname)};

    switch (_admode)
    {
    case admode_t::UNK:
        ret += string_printf(" $%02X", _opcode);
        break;
    case admode_t::IMPL:
        break;
    case admode_t::A:
        ret += " A";
        break;
    case admode_t::IMM:
        ret += string_printf(" #$%02X", _operand);
        break;
    case admode_t::ABS:
        ret += string_printf(" $%04X", _operand);
        break;
    case admode_t::ABSX:
        ret += string_printf(" $%04X,X", _operand);
        break;
    case admode_t::ABSY:
        ret += string_printf(" $%04X,Y", _operand);
        break;
    case admode_t::ZP:
        ret += string_printf(" $%02X", _operand);
        break;
    case admode_t::ZPX:
        ret += string_printf(" $%02X,X", _operand);
        break;
    case admode_t::ZPY:
        ret += string_printf(" $%02X,Y", _operand);
        break;
    case admode_t::REL:
        {
            int offset = int8_t(_operand) + 2;
            if (_addr)
            {
                address_t dest = _addr + offset;
                ret += string_printf(" $%04X", dest);
            }
            else
            {
                if (offset >= 0)
                    ret += string_printf(" .+$%02X", offset);
                else
                    ret += string_printf(" .-$%02X", -offset);
            }
        }
        break;
    case admode_t::INDY:
        ret += string_printf(" ($%02X),Y", _operand);
        break;
    case admode_t::INDX:
        ret += string_printf(" ($%02X,X)", _operand);
        break;
    case admode_t::IND2:
        ret += string_printf(" ($%04X)", _operand);
        break;
    }
    return ret;
}

std::vector<byte_t> inst::bytes() const
{
    std::vector<byte_t> ret { _opcode };
    for (size_t c = 1; c < _size; c++)
    {
        byte_t b = _operand >> ((c-1)*8);
        ret.push_back(b);
    }
    return ret;
}

} // namespace mos6502
} // namespace monitor
