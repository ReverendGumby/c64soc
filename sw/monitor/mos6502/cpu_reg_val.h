#pragma once

#include "cpu_regs.h"
#include "util/reg_val.h"
#include <sstream>

// reg_val is in the global namespace.

struct pf_t { monitor::mos6502::cpu_regs regs; };
template <>
std::string reg_val<pf_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << (r.pf.c ? 'C' : 'c')
       << (r.pf.z ? 'Z' : 'z')
       << (r.pf.i ? 'I' : 'i')
       << (r.pf.d ? 'D' : 'd')
       << (r.pf.b ? 'B' : 'b')
       << (r.pf.v ? 'V' : 'v')
       << (r.pf.n ? 'N' : 'n');
    return ss.str();
}

struct tstate_t { monitor::mos6502::cpu_regs regs; };
template <>
std::string reg_val<tstate_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << r.tstate.t5
       << r.tstate.t4
       << r.tstate.t3
       << r.tstate.t2
       << r.tstate.clock2
       << r.tstate.clock1;
    return ss.str();
}

struct pstate_t { monitor::mos6502::cpu_regs regs; };
template <>
std::string reg_val<pstate_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << (r.pstate.rw   ? 'R' : 'W')
       << (r.pstate.sync ? 'S' : 's')
       << (r.istate.irqp ? 'I' : 'i');
    return ss.str();
}
