#pragma once

#include <stdint.h>

namespace monitor {
namespace mos6502 {

struct cpu_regs
{
    uint16_t pc;
    struct {
        bool c;                                 // pf[0]
        bool z;                                 // pf[1]
        bool i;                                 // pf[2]
        bool d;                                 // pf[3]
        bool b;                                 // pf[4]
        bool v;                                 // pf[6]
        bool n;                                 // pf[7]
    } pf;                                       // processor flags
    uint8_t s;
    uint8_t a;
    uint8_t x;
    uint8_t y;
    uint8_t ir;                                 // instruction register
    uint16_t abr;                               // address bus register
    uint8_t dor;                                // data output register
    struct {
        bool clock1;
        bool clock2;
        bool t2;
        bool t3;
        bool t4;
        bool t5;
    } tstate;                                   // Timing State
    struct {
        bool resp;
        bool resg;
        bool nmip;
        bool nmig;
        bool irqp;
    } istate;                                   // Interrupt State
    struct {
        bool rdy;
        bool rw;
        bool aec;
        bool sync;
        bool so;
    } pstate;                                   // Pin State
};

} // namespace mos6502
} // namespace monitor
