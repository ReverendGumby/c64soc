#pragma once

#include "c64/vid.h"

namespace ui {

class screen
{
public:
    using color_t = c64::vid::color;

    virtual ~screen();

    virtual uint8_t ascii_to_screen(char) = 0;

    virtual int get_width() const = 0;
    virtual int get_height() const = 0;

    virtual void set_bg(color_t c) = 0;
    virtual void set_border(color_t c) = 0;

    virtual void init() = 0;
    virtual void clear(color_t c) = 0;

    virtual void write(int x, int y, uint8_t b, color_t c) = 0;

    virtual void invert_rect(int x, int y, int w, int h) = 0;
    void invert_row(int y) { invert_rect(0, y, get_width(), 1); }
};

} // namespace ui
