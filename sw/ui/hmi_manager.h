#pragma once

#include <util/listener.h>
#include "hmi_cart_listener.h"

namespace plat {
class manager;
}

namespace ui {

class hmi_manager : public hmi_cart_listener
{
public:
    hmi_manager(plat::manager&);

    void listen(talker<hmi_cart_listener>&);

private:
    // interface for hmi_cart_listener
    virtual void cart_modified(bool);

    plat::manager& _plat_mgr;
};

} // namespace ui
