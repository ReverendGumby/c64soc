#ifndef UI__SCROLLABLE_LIST_VIEW__H
#define UI__SCROLLABLE_LIST_VIEW__H

#include <string>
#include "subscreen_view.h"

namespace ui {

// scrollable_list_view: A view that shows a subset (viewport) of a larger
// underlying item list. The user may scroll the view to change the viewport.
//
// The list selection ('selected') is always relative to the first item in the
// viewport. As the viewport scrolls, the index of the selected item (if any) is
// recomputed. 'count' is the number of items that fit in the viewport. The
// model must track the viewport's position.
//
// Scrolling distances and directions are negative to scroll up (viewport moves
// to reveal earlier items), positive to scroll down.
//
// Cursor keys scroll by a single item, when the selection is at a viewport
// edge. Page keys scroll by 'page_distance', which defaults to the list view
// count.

class scrollable_list_view : public subscreen_view
{
public:
    struct model
    {
        virtual std::string get_item(int idx) = 0;

        virtual void on_selected_changed(int idx) {}
        virtual void on_selected(int idx) = 0;

        // User has asked to scroll a distance of 'want'. Return the actual
        // permitted scroll distance. This can be used to prevent scrolling
        // beyond the bounds of the underlying list. Or, if the list wraps
        // indefinitely, just return 'want'.
        virtual int get_scroll_distance(int want) = 0;

        // Scroll the list by 'distance'.
        virtual void on_scrolled(int distance) = 0;
    };

    scrollable_list_view(model&);
    virtual ~scrollable_list_view();

    void set_count(int);
    int get_selected() const { return selected; }
    void select(int new_selection);

    void set_page_distance(int);

    // When true, page keys move selection by page distance, and viewport
    // scrolling follows. When false, page keys scroll viewport, and selection
    // tries to remain (relatively) static.
    void set_page_mode(bool);

    void scroll_distance(int distance);
    void scroll_page(int direction);

    // interface for view
    virtual bool is_focusable() const { return true; }
    virtual void on_focus_gained();
    virtual void on_focus_lost();

protected:
    virtual const char* get_class_name() const { return "scrollable_list_view"; }

private:
    void move_selection(int dir);
    void draw_item(const drawing_context*, int);

    void invert(int idx);

    // interface for screen_view
    virtual void on_draw(const drawing_context*);
    virtual bool on_key_down(const keypress_t&, int);

    model& model;
    int count;                                  // number of items
    int selected;                               // -1 = none, 0..count-1 = item
    int page_distance;
    bool page_mode;                             // page keys move sel. by page
};

} // namespace ui

#endif /* UI__SCROLLABLE_LIST_VIEW__H */
