#ifndef UI__VIEW_MANAGER__H
#define UI__VIEW_MANAGER__H

#include <list>
#include "util/ptimer.h"
#include "hid_manager.h"

namespace ui {

class screen_manager;
class fullscreen_view;

class view_manager : private kbd::listener,
                     private joy::listener
{
public:
    using view_t = fullscreen_view;
    using view_list_t = std::list<view_t*>;

    view_manager(hid_manager&, screen_manager&);

    void show(view_t*);
    void hide(view_t*);

    void save_and_hide_all();
    void restore_and_show_all();

    screen_manager& screen_mgr;

private:
    void dbc_joy_event();
    void send_keypress(const kbd::keypress&);
    void key_repeat_event();

    // interface for kbd::listener
    virtual void event(const kbd::report&);
    virtual void event(const kbd::keypress&);

    // interface for usb::hid::joy::listener
    virtual void event(const joy::report&);

    view_list_t shown, save_shown;
    joy::report last_jrpt, new_jrpt;
    ptimer tjrpt;
    kbd::keypress last_kp, last_jp;
    int kp_repeats;
    ptimer tkrpt;
};

} // namespace ui

#endif /* UI__VIEW_MANAGER__H */
