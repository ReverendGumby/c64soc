#include "plat/manager.h"

#include "hmi_manager.h"

namespace ui {

struct leds {
    enum {
        cart_modified = 0,
        spare1,
        spare2,
        spare3,
    };
};

hmi_manager::hmi_manager(plat::manager& plat_mgr)
    : _plat_mgr(plat_mgr)
{
}

void hmi_manager::listen(talker<hmi_cart_listener>& t)
{
    t.add_listener(this);
}

void hmi_manager::cart_modified(bool v)
{
    _plat_mgr.set_led(leds::cart_modified, v);
}

} // namespace ui
