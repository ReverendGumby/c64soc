#ifndef UI__VIEW__H
#define UI__VIEW__H

#include <string>
#include <vector>
#include "ui/kbd.h"

namespace ui {

class view
{
public:
    using keypress_t = ui::kbd::keypress;
    using kbcode = ui::kbd::kbcode;

    const char* get_full_name();
    static const char* safe_get_full_name(view* v);

    view* get_parent_view() const { return parent; }
    view* get_topmost_parent_view();
    const std::vector<view*>& get_child_views() const { return children; }

    void add_child_view(view*);

    view* get_focus_child() const { return focus_child; }
    void set_focus_child(view*);
    view* get_focus_leaf();

    bool is_visible() const { return visible; }
    bool is_revealed() const { return revealed; }
    virtual bool is_focusable() const { return false; }

    virtual void show();
    virtual void hide();
    virtual void set_shown(bool);
    virtual void draw();
    virtual void grab_focus();
    virtual void release_focus();
    virtual bool has_focus() const;

    virtual void on_show();          // make visible
    virtual void on_hide();          // make invisible
    virtual void on_revealed();      // visible, and we became top-most view
    virtual void on_obscured();      // visible, but other view covers us
    virtual void on_draw();
    virtual bool on_key_down(const keypress_t& kp, int repeat) { return false; }
    virtual bool on_key_up(const keypress_t& kp) { return false; }
    virtual void on_focus_gained();
    virtual void on_focus_lost();

protected:
    virtual const char* get_class_name() const { return "view"; }
    virtual std::string get_name() const { return get_class_name(); }

private:
    void set_full_name();

    std::string name;
    bool visible = false;
    bool revealed = false;
    std::vector<view*> children;
    view* parent = nullptr;
    view* focus_child = nullptr;
};

} // namespace ui

#endif /* UI__VIEW__H */
