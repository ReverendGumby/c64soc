#include "text_editor.h"

namespace ui {

constexpr timer::ticks cursor_blink_rate = timer::ms * 500;

text_editor::text_editor()
{
    editing = false;
    cursor_enabled = false;
    cursor_seen = false;
}

text_editor::~text_editor()
{
}

void text_editor::set_text(std::string new_text)
{
    stop_editing(abort);
    text = new_text;
}

void text_editor::start_editing()
{
    if (editing)
        return;
    orig_text = text;
    editing = true;
    cursor_pos = 0;
    enable_cursor(true);
}

void text_editor::stop_editing(stop_editing_mode_t mode)
{
    if (!editing)
        return;
    enable_cursor(false);
    editing = false;

    if (mode == abort)
    {
        text = orig_text;
        draw();
    }
    else
        change_action();

    release_focus();
}

void text_editor::draw_text(const drawing_context* dc)
{
    auto* ss = dc->ss;
    *ss << text << std::endl;
}

void text_editor::enable_cursor(bool en)
{
    if (en == cursor_enabled)
        return;
    if (en)
    {
        set_cursor_seen(true);
        pollable_add(this);
        cursor_timer.start(cursor_blink_rate);
    }
    else
    {
        pollable_remove(this);
        set_cursor_seen(false);
    }
    cursor_enabled = en;
}

void text_editor::set_cursor_seen(bool seen)
{
    if (seen == cursor_seen)
        return;

    // Invert cursor position.
    auto* dc = get_dc();
    if (dc)
        invert_cursor(dc);

    cursor_seen = seen;
}

void text_editor::draw_cursor(const drawing_context* dc)
{
    if (cursor_seen)
        invert_cursor(dc);
}

void text_editor::invert_cursor(const drawing_context* dc)
{
    dc->scr->invert_rect(left + cursor_pos, top, 1, 1);
}

bool text_editor::move_cursor(int dir)
{
    int new_pos = cursor_pos + dir;
    if (new_pos < 0 || new_pos > (int)text.size())
        return false;

    bool was_cursor_enabled = cursor_enabled;
    if (was_cursor_enabled)
        enable_cursor(false);
    cursor_pos = new_pos;
    if (was_cursor_enabled)
        enable_cursor(true);
    return true;
}

bool text_editor::insert_at_cursor(char ch)
{
    if (cursor_pos + 1 >= get_width())
        return false;
    text.insert(text.begin() + cursor_pos, ch);
    move_cursor(1);
    return true;
}

bool text_editor::delete_at_cursor()
{
    if (cursor_pos >= (int)text.size())
        return false;
    text.erase(text.begin() + cursor_pos);
    return true;
}

void text_editor::set_change_action(change_action_fobj action)
{
    change_action = action;
}

void text_editor::poll()
{
    if (!cursor_enabled || !cursor_timer.expired())
        return;
    set_cursor_seen(!cursor_seen);
    cursor_timer.restart(cursor_blink_rate);
}

bool text_editor::on_key_down(const keypress_t& k, int repeat)
{
    switch (k.code)
    {
    case kbcode::RET:
        stop_editing(commit);
        break;
    case kbcode::ESC:
        stop_editing(abort);
        break;
    case kbcode::LAR:
        if (move_cursor(-1))
            draw();
        break;
    case kbcode::RAR:
        if (move_cursor(1))
            draw();
        break;
    case kbcode::BAK:
        if (move_cursor(-1) && delete_at_cursor())
            draw();
        break;
    case kbcode::DEL:
        if (delete_at_cursor())
            draw();
        break;
    default:
        if (k.ch)
        {
            delete_at_cursor();
            insert_at_cursor(k.ch);
            draw();
            break;
        }
        else
            return subscreen_view::on_key_down(k, repeat);
    }
    return true;
}

void text_editor::on_focus_gained()
{
    subscreen_view::on_focus_gained();
    start_editing();
}

void text_editor::on_focus_lost()
{
    stop_editing(abort);
    subscreen_view::on_focus_lost();
}

void text_editor::on_draw(const drawing_context* dc)
{
    draw_text(dc);
    draw_cursor(dc);
}

} // namespace ui
