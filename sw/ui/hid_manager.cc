#include <stdexcept>
#include "usb/hid/kbd.h"
#include "hid_manager.h"

namespace ui {

hid_manager::hid_manager()
{
    kbd = nullptr;
    for (int i = 0; i < max_joys; i++)
        joy_id_used[i] = nullptr;
}

void hid_manager::listen(usb::usb_manager& mgr)
{
    mgr.add_listener(this);
}

void hid_manager::add_kbd_listener(kbd::listener* l)
{
    talker<kbd::listener>::add_listener(l);
}

void hid_manager::add_joy_listener(joy::listener* l)
{
    talker<joy::listener>::add_listener(l);
}

joy* hid_manager::get_joy_dev(int id) const
{
    if (id < 0 || id >= max_joys)
        return nullptr;
    return joy_id_used[id];
}

int hid_manager::allocate_joy_id(joy* dev)
{
    for (int i = 0; i < max_joys; i++)
        if (!joy_id_used[i]) {
        	joy_id_used[i] = dev;
            return i;
        }
    throw std::runtime_error("hid_manager::allocate_joy_id(): out of ids");
}

void hid_manager::free_joy_id(int id)
{
    joy_id_used[id] = nullptr;
}

template <typename T>
static T* to_device(usb::device* d)
{
    auto* hd = dynamic_cast<usb::hid::device*>(d);
    if (hd) {
        auto* kd = hd->get_sub_device<T>();
        if (kd)
            return kd;
    }
    return nullptr;
}

void hid_manager::attached(usb::device* d)
{
    if (auto kd = to_device<usb::hid::kbd>(d)) {
        kd->add_listener(this);
        kbd = kd;
    }
    if (auto jd = to_device<usb::hid::joy>(d)) {
        jd->add_listener(this);
        auto id = allocate_joy_id(jd);
        jd->id = id;
        jd->name = jd->get_name();
        jd->set_port(joy::port_first + id);
        talker<listener>::speak(&listener::joy_dev_changed, id);
    }
}

void hid_manager::detached(usb::device* d)
{
    if (auto kd = to_device<usb::hid::kbd>(d))
        kd->remove_listener(this);
    if (auto jd = to_device<usb::hid::joy>(d)) {
        auto id = jd->id;
        free_joy_id(id);
        talker<listener>::speak(&listener::joy_dev_changed, id);
        jd->remove_listener(this);
    }
}

void hid_manager::event(const kbd::keypress& kp)
{
	talker<kbd::listener>::speak(&kbd::listener::event, kp);
}

void hid_manager::event(const kbd::report& r)
{
	talker<kbd::listener>::speak(&kbd::listener::event, r);
}

void hid_manager::event(const joy::report& r)
{
	talker<joy::listener>::speak(&joy::listener::event, r);
}

bool hid_manager::is_idle()
{
    // Return AND of idle of kbd and all used joys.
    for (int i = 0; i < max_joys; i++)
        if (joy_id_used[i] && !joy_id_used[i]->is_idle())
            return false;
    if (kbd && !kbd->is_idle())
        return false;
    return true;
}

} // namespace ui
