#include <algorithm>
#include "util/debug.h"

#include "view.h"

#define DBG_LVL 1
#define DBG_TAG "view"

namespace ui {

const char* view::get_full_name()
{
    if (name.empty())
        set_full_name();
    return name.c_str();
}

const char* view::safe_get_full_name(view* v)
{
    if (v == nullptr)
        return "(nullptr)";
    return v->get_full_name();
}

view* view::get_topmost_parent_view()
{
    if (parent)
        return parent->get_topmost_parent_view();
    return this;                                // I'm the eldest!
}

void view::set_full_name()
{
    if (parent)
    {
        name = parent->get_full_name();
        name += '.';
    }
    name += get_name();
}

void view::add_child_view(view* child)
{
    child->parent = this;
    child->name.clear();                         // name is invalidated

    auto i = std::find(children.cbegin(), children.cend(), child);
    if (i == children.cend())
        children.push_back(child);
}

void view::set_focus_child(view* child)
{
    DBG_PRINTF(4, "this=%s child=%s\n", get_full_name(), safe_get_full_name(child));

    if (child == focus_child)
        return;
    if (child != nullptr)
    {
        auto i = std::find(children.cbegin(), children.cend(), child);
        if (i == children.cend())                   // no such child
            return;
    }

    bool focused = has_focus();
    auto old_focus_child = focus_child;
    focus_child = child;

    if (focused && old_focus_child && old_focus_child->is_focusable())
        old_focus_child->on_focus_lost();

    if (focused && focus_child && focus_child->is_focusable())
        focus_child->on_focus_gained();
}

view* view::get_focus_leaf()
{
    if (focus_child)
        return focus_child->get_focus_leaf();
    else
        return this;
}

void view::show()
{
    on_show();
}

void view::hide()
{
    release_focus();

    on_hide();
}

void view::set_shown(bool b)
{
    if (b)
        show();
    else
        hide();
}

void view::draw()
{
    if (is_visible() && is_revealed())
        on_draw();
}

void view::grab_focus()
{
    DBG_PRINTF(2, "this=%s\n", get_full_name());

    if (has_focus())
        return;
    if (parent)
    {
        parent->grab_focus();
        parent->set_focus_child(this);
    }
}

void view::release_focus()
{
    DBG_PRINTF(2, "this=%s\n", get_full_name());

    if (parent)
    {
        bool had_focus = has_focus();

        if (parent->focus_child == this)
            parent->set_focus_child(nullptr);

        if (!had_focus)
            return;

        // Pass upwards to un-focusable ancestors.
        if (!parent->is_focusable())
            parent->release_focus();
    }
}

// Return true if all ancestors have focus.
bool view::has_focus() const
{
    if (parent)
        return parent->get_focus_child() == this && parent->has_focus();
    else
        return false;
}

void view::on_show()
{
    DBG_PRINTF(3, "this=%s\n", get_full_name());

    visible = true;

    draw();
}

void view::on_hide()
{
    DBG_PRINTF(3, "this=%s\n", get_full_name());

    visible = false;

    if (parent)
        parent->draw();
}

void view::on_revealed()
{
    DBG_PRINTF(3, "this=%s\n", get_full_name());

    revealed = true;
    for (auto* c : children)
        c->on_revealed();
}

void view::on_obscured()
{
    DBG_PRINTF(3, "this=%s\n", get_full_name());

    for (auto* c : children)
        c->on_obscured();
    revealed = false;
}

void view::on_draw()
{
    DBG_PRINTF(5, "this=%s\n", get_full_name());

    for (auto* c : children)
        c->draw();
}

void view::on_focus_gained()
{
    DBG_PRINTF(3, "this=%s\n", get_full_name());

    if (focus_child)
        focus_child->on_focus_gained();
}

void view::on_focus_lost()
{
    DBG_PRINTF(3, "this=%s\n", get_full_name());

    if (focus_child)
        focus_child->on_focus_lost();
}

} // namespace ui
