#include "label.h"

namespace ui {

label::label()
{
}

label::~label()
{
}

void label::set_text(const std::string& new_text)
{
    text = new_text;
}

void label::add_to_view(view* v, const std::string& s, int x, int y, int w)
{
    if (w == -1)
        w = s.size();

    set_text(s);
    set_pos_size(x, y, w, 1);
    show();
    v->add_child_view(this);
}

void label::on_draw(const drawing_context* dc)
{
    auto* ss = dc->ss;
    *ss << text << std::endl;
}

} // namespace ui
