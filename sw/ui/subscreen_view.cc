#include <typeinfo>
#include "util/string.h"
#include "screen_view.h"

#include "subscreen_view.h"

namespace ui {

subscreen_view::subscreen_view()
    : left(0), top(0), width(0), height(0)
{
}

subscreen_view::subscreen_view(int left, int top, int width, int height)
    : subscreen_view()
{
    set_pos_size(left, top, width, height);
}

subscreen_view::~subscreen_view()
{
}

std::string subscreen_view::get_name() const
{
    return string_printf("%s[%d,%d]", get_class_name(), left, top);
}

void subscreen_view::set_pos(int new_left, int new_top)
{
    left = new_left;
    top = new_top;
}

void subscreen_view::set_size(int new_width, int new_height)
{
    width = new_width;
    height = new_height;
}

void subscreen_view::set_pos_size(int new_left, int new_top, int new_width, int new_height)
{
    set_pos(new_left, new_top);
    set_size(new_width, new_height);
}

void subscreen_view::on_draw()
{
    if (width == 0 || height == 0)
        return;
    screen_view::on_draw();
}

void subscreen_view::create_dc(drawing_context& dc)
{
    auto* pdc = get_parent_dc();
    dc.scr = pdc->scr;
    dc.ss = new screenstream(dc.scr, left, top, width, height);
}

void subscreen_view::destroy_dc(drawing_context& dc)
{
    delete dc.ss;
    dc.ss = nullptr;
    dc.scr = nullptr;
}

} // namespace ui
