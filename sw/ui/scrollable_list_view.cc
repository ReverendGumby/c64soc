#include "scrollable_list_view.h"

namespace ui {

scrollable_list_view::scrollable_list_view(struct model& model)
    : model(model)
{
    count = 0;
    selected = -1;
    page_distance = 0;
    page_mode = true;
}

scrollable_list_view::~scrollable_list_view()
{
}

void scrollable_list_view::set_count(int new_count)
{
    if (count == new_count)
        return;
    count = new_count;

    // Ensure selection remains within bounds.
    if (selected > count)
        select(count - 1);
}

void scrollable_list_view::select(int new_selection)
{
    if (new_selection < (int)count && new_selection != selected)
    {
        invert(selected);
        selected = new_selection;
        invert(selected);
        model.on_selected_changed(new_selection);
    }
}

void scrollable_list_view::set_page_distance(int p)
{
    page_distance = p;
}

void scrollable_list_view::set_page_mode(bool b)
{
    page_mode = b;
}

void scrollable_list_view::scroll_distance(int distance)
{
    distance = model.get_scroll_distance(distance);
    if (distance) {
        model.on_scrolled(distance);

        if (selected != -1) {
            selected -= distance;
            if (selected < 0)
                selected = 0;
            else if (selected >= (int)count)
                selected = count - 1;
        }

        draw();
    }
}

void scrollable_list_view::scroll_page(int direction)
{
    int distance = page_distance ? : count;
    scroll_distance(direction * distance);
}

void scrollable_list_view::on_focus_gained()
{
    draw();
    subscreen_view::on_focus_gained();
}

void scrollable_list_view::on_focus_lost()
{
    draw();
    subscreen_view::on_focus_lost();
}

void scrollable_list_view::move_selection(int dir)
{
    auto& cs = selected;
    if (dir == 0 || cs < 0)
        return;

    int ns = cs + dir;
    if (ns >= (int)count) {
        dir = ns - (count - 1);
        scroll_distance(dir);
        ns = count - 1;
    } else if (ns < 0) {
        dir = ns;
        scroll_distance(dir);
        ns = 0;
    }

    if (ns >= 0)
        select(ns);
}

bool scrollable_list_view::on_key_down(const keypress_t& kp, int repeat)
{
    // TODO: Item letter shortcuts
    if (kp.ch >= '1' && kp.ch <= '9' && !repeat)
        model.on_selected(kp.ch - '1');
    else if (kp.ch == '\n' && !repeat) {
        if (selected != -1)
            model.on_selected(selected);
    } else if (kp.code == kbcode::UAR)
        move_selection(-1);
    else if (kp.code == kbcode::DAR)
        move_selection(1);
    else if (kp.code == kbcode::PGU) {
        if (page_mode)
            move_selection(-page_distance);
        else
            scroll_page(-1);
    } else if (kp.code == kbcode::PGD) {
        if (page_mode)
            move_selection(page_distance);
        else
            scroll_page(1);
    } else
        return subscreen_view::on_key_down(kp, repeat);
    return true;
}

void scrollable_list_view::draw_item(const drawing_context* dc, int idx)
{
    // Clip to item bounds.
    auto* ss = dc->ss;
    ss->setwindow(0, idx, width, 1);

    auto s = model.get_item(idx);
    *ss << s << std::endl;

    if (selected == idx)
        ss->invert();

    ss->setwindow();
}

void scrollable_list_view::invert(int idx)
{
    auto dc = get_dc();
    if (!dc)
        return;
    if (selected != -1) {
        auto* ss = dc->ss;
        ss->setwindow(0, idx, width, 1);
        ss->invert();
        ss->setwindow();
    }
}

void scrollable_list_view::on_draw(const drawing_context* dc)
{
    for (int i = 0; i < (int)count; i++)
        draw_item(dc, i);
}

} // namespace ui
