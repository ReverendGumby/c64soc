#ifndef UI__FULLSCREEN_VIEW__H
#define UI__FULLSCREEN_VIEW__H

#include "screen_view.h"

namespace ui {

class view_manager;

class fullscreen_view : public screen_view
{
public:
    static void set_manager(view_manager*);
    static void set_size(int width, int height);
    static int max_width();
    static int max_height();

    fullscreen_view();
    virtual ~fullscreen_view();

    void set_color_border(color_t);
    void set_color_bg(color_t);

    void save_and_hide_all();
    void restore_and_show_all();

    // interface for view
    virtual bool is_focusable() const { return true; }
    virtual void show();
    virtual void hide();
    virtual bool has_focus() const;
    //virtual void draw();
    virtual void on_show();
    virtual void on_hide();
    virtual void on_revealed();
    virtual void on_obscured();
    virtual bool on_key_down(const keypress_t& k, int repeat);

private:
    static view_manager* mgr;
    static int width, height;

    void grab_screen();
    void release_screen();

    virtual void create_dc(drawing_context&);
    virtual void destroy_dc(drawing_context&);

    color_t color_border;
    color_t color_bg;
    screen* scr;
};

} // namespace ui

#endif /* UI__FULLSCREEN_VIEW__H */
