#include <assert.h>
#include "fullscreen_view.h"
#include "screen_manager.h"

#include "view_manager.h"

// view_manager enforces the following rules:
// 1. Only one view can use the screen at a time
// 2. Most recent view to be shown obscures the others until it's hidden

namespace ui {

// Key repeat times
constexpr timer::ticks krpt_initial = 400 * timer::ms;
constexpr timer::ticks krpt_remain  =  80 * timer::ms;

view_manager::view_manager(hid_manager& hid_mgr, screen_manager& scr_mgr)
    : screen_mgr(scr_mgr), last_jrpt{}, new_jrpt{}, last_kp{}, last_jp{}
{
    fullscreen_view::set_manager(this);
    hid_mgr.add_kbd_listener(this);
    hid_mgr.add_joy_listener(this);

    // Setup joystick debounce timer
    tjrpt.duration = 20 * timer::ms;
    tjrpt.action = [this](){ dbc_joy_event(); };

    // Setup key repeats
    kp_repeats = 0;
    tkrpt.action = [this](){ key_repeat_event(); };
}

void view_manager::event(const kbd::report&)
{
}

void view_manager::event(const kbd::keypress& k)
{
    // Implement key repeat.
    if (k.down) {
        // A new key was pressed.
        last_kp = k;
        kp_repeats = 0;
        tkrpt.start(krpt_initial);
    } else if (last_kp.down && last_kp.code == k.code) {
        // Repeating key was released.
        last_kp = k;
        tkrpt.cancel();
    }

    send_keypress(k);
}

void view_manager::send_keypress(const kbd::keypress& k)
{
    if (shown.size())
    {
        auto* top = shown.front();
        for (auto* v = top->get_focus_leaf(); v != nullptr; v = v->get_parent_view())
            if (k.down ? v->on_key_down(k, kp_repeats) : v->on_key_up(k))
                break;
    }
}

void view_manager::key_repeat_event()
{
    if (last_kp.down) {
        kp_repeats ++;
        tkrpt.restart(krpt_remain);
        send_keypress(last_kp);
    }
}

bool btn_select(const joy::report& r)
{
    return r.f1 || r.f3 || r.st;
}

bool btn_cancel(const joy::report& r)
{
    return r.f2 || r.f4 || r.sl;
}

void view_manager::event(const joy::report& r)
{
    // Save the event, then wait for the debounce timer to expire.
    new_jrpt = r;
    tjrpt.start();
}

void view_manager::dbc_joy_event()
{
    // Translate joystick button press to equivalent keypress.
    kbd::keypress jp = {};
    auto& r = new_jrpt;
    auto& lr = last_jrpt;
    auto select = btn_select(r);
    auto lselect = btn_select(lr);
    auto cancel = btn_cancel(r);
    auto lcancel = btn_cancel(lr);
    jp.down = true;
    if (r.up && !lr.up)
        jp.code = kbd::kbcode::UAR;
    else if (r.dn && !lr.dn)
        jp.code = kbd::kbcode::DAR;
    else if (r.lt && !lr.lt)
        jp.code = kbd::kbcode::LAR;
    else if (r.rt && !lr.rt)
        jp.code = kbd::kbcode::RAR;
    else if (r.bl && !lr.bl)
        jp.code = kbd::kbcode::PGU;
    else if (r.br && !lr.br)
        jp.code = kbd::kbcode::PGD;
    else if (cancel && !lcancel)
        jp.code = kbd::kbcode::ESC;
    else if (select && !lselect) {
        jp.code = kbd::kbcode::RET;
        jp.ch = '\n';
    } else
        jp.down = false;

    if (jp.code != last_jp.code ||
        jp.down != last_jp.down) {
        if (last_jp.down) {
            // Release last joy down-press.
            last_jp.down = false;
            event(last_jp);
        }
        event(jp);
        last_jp = jp;
    }

    last_jrpt = new_jrpt;
}

void view_manager::show(view_t* v)
{
    auto i = find(shown.begin(), shown.end(), v);
    if (i != shown.end())                       // already in the stack
        return;
    if (shown.size())
    {
        // Obscure the visible view.
        auto* top = shown.front();
        top->on_obscured();
    }
    // Add the view to the stack.
    shown.push_front(v);

    // Show the view.
    v->on_show();
    v->on_revealed();
}

void view_manager::hide(view_t* v)
{
    auto i = find(shown.begin(), shown.end(), v);
    if (i == shown.end())                       // not in the stack
        return;
    bool visible = i == shown.begin();

    // Remove the view from the stack.
    shown.erase(i);

    if (visible)
    {
        // The view was visible. Obscure it.
        v->on_obscured();

        if (shown.size())
        {
            // Show the next view in the stack.
            auto* top = shown.front();
            top->on_revealed();
        }
    }

    // Notify the view.
    v->on_hide();
}

void view_manager::save_and_hide_all()
{
    if (!shown.empty())
    {
        // Obscure the topmost view.
        auto* top = shown.front();
        top->on_obscured();

        // Hide all views from the top down.
        for (auto* v : shown)
            v->on_hide();
    }

    // Save and clear the stack.
    save_shown = shown;
    shown.clear();
}

void view_manager::restore_and_show_all()
{
    // Restore the stack.
    shown = save_shown;
    save_shown.clear();

    if (!shown.empty())
    {
        // Show all views from the bottom up.
        for (auto i = shown.rbegin(); i != shown.rend(); i++)
            (*i)->on_show();

        // Reveal the topmost view.
        shown.front()->on_revealed();
    }
}

} // namespace ui
