#include "screenstream.h"

namespace ui {

screenstreambuf::screenstreambuf(screen* scr, int left, int top,
                                 int right, int bottom)
    : scr(scr)
{
    fg = screen::color_t::WHITE;
    set_window(left, top, right, bottom);
    rev = false;
}

void screenstreambuf::set_fg(screen::color_t c)
{
    fg = c;
}

void screenstreambuf::set_pos(int nx, int ny)
{
    x = std::min(left + nx, right);
    y = std::min(top + ny, bottom);
    line_wrapped = false;
}

void screenstreambuf::set_rev(bool en)
{
    rev = en;
}

void screenstreambuf::set_window(int l, int t, int r, int b)
{
    left = l;
    top = t;
    right = r;
    bottom = b;
    x = left;
    y = top;
    line_wrapped = false;
}

// Write an ASCII character.
void screenstreambuf::write(char c)
{
    if (c == '\n')
    {
        x = left;
        if (!line_wrapped && y < bottom)
            y ++;
        line_wrapped = false;
    }
    else if (x < right && y < bottom)
    {
        uint8_t b = scr->ascii_to_screen(c);
        if (rev)
            b |= (1<<7);
        scr->write(x, y, b, fg);

        x ++;
        if (x >= right)
        {
            x = left;
            y ++;
            line_wrapped = true;
        }
    }
    if (y >= bottom)
        y = bottom;
}

void screenstreambuf::clear()
{
    uint8_t space = scr->ascii_to_screen(' ');
    for (int y = top; y < bottom; y++)
        for (int x = left; x < right; x++)
            scr->write(x, y, space, fg);
    set_pos(0, 0);
}

void screenstreambuf::invert()
{
    int w = right - left;
    int h = bottom - top;
    scr->invert_rect(left, top, w, h);
}

screenstreambuf::int_type screenstreambuf::overflow(screenstreambuf::int_type c)
{
    write((char)c);
    return c;
}

//////////////////////////////////////////////////////////////////////

screenstream& clear(screenstream& os)
{
    os.clear();
    return os;
}

screenstream& invert(screenstream& os)
{
    os.invert();
    return os;
}

screenstream& revon(screenstream& os)
{
    os.setrev(true);
    return os;
}

screenstream& revoff(screenstream& os)
{
    os.setrev(false);
    return os;
}

screenstream::screenstream(screen* scr,
                           int left, int top, int width, int height)
    : std::ostream(),
      left(left), top(top),
      right(left + width),
      bottom(top + height),
      ssb(scr, left, top, right, bottom)
{
    this->init(&ssb);
}

screenstream::screenstream(screen* scr)
    : screenstream(scr, 0, 0, scr->get_width(), scr->get_height())
{
}

screenstream::~screenstream()
{
}

void screenstream::clear()
{
    flush();
    ssb.clear();
}

void screenstream::invert()
{
    flush();
    ssb.invert();
}

void screenstream::setrev(bool en)
{
    ssb.set_rev(en);
}

void screenstream::setpos(int x, int y)
{
    flush();
    ssb.set_pos(x, y);
}

void screenstream::setfg(screen::color_t c)
{
    flush();
    ssb.set_fg(c);
}

void screenstream::setwindow(int offx, int offy, int width, int height)
{
    int l = left + offx;
    int t = top + offy;
    int r = l + width;
    int b = t + height;

    // Clip to initial region.
    l = std::min(l, right);
    t = std::min(t, bottom);
    r = std::min(r, right);
    b = std::min(b, bottom);

    flush();
    ssb.set_window(l, t, r, b);
}

void screenstream::setwindow()
{
    flush();
    ssb.set_window(left, top, right, bottom);
}

} // namespace ui
