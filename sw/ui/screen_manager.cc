#include "screen_manager.h"

namespace ui {

screen_manager::screen_manager()
{
    _screen = nullptr;
    count = 0;
}

screen* screen_manager::grab()
{
    if (count == 0)
    {
        _screen = grab_screen();
    }
    count++;
    return _screen;
}

void screen_manager::release()
{
    count--;
    if (count == 0)
    {
        delete _screen;
        _screen = nullptr;

        release_screen();
    }
}

} // namespace ui
