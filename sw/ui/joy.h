#ifndef UI__JOY__H
#define UI__JOY__H

#include <string>
#include "util/listener.h"

// Joystick definitions

namespace ui {

struct joy;

namespace _joy {

struct report
{
    int id;                                     // joystick identifier

    int up;                                 // up
    int dn;                                 // down
    int lt;                                 // left
    int rt;                                 // right
    int f1;                                 // fire 1
    int f2;                                 // fire 2
    int f3;                                 // fire 3
    int f4;                                 // fire 4
    int sl;                                 // select
    int st;                                 // start
    int bl;                                 // bumper left
    int br;                                 // bumper right

    inline bool is_idle() const
    {
        return !(up || dn || lt || rt || f1 || f2 || f3 || f4 || sl || st ||
                 bl || br);
    }
};

inline bool operator==(const report& lhs, const report& rhs)
{
    return (lhs.up == rhs.up &&
            lhs.dn == rhs.dn &&
            lhs.lt == rhs.lt &&
            lhs.rt == rhs.rt &&
            lhs.f1 == rhs.f1 &&
            lhs.f2 == rhs.f2 &&
            lhs.f3 == rhs.f3 &&
            lhs.f4 == rhs.f4 &&
            lhs.sl == rhs.sl &&
            lhs.st == rhs.st &&
            lhs.bl == rhs.bl &&
            lhs.br == rhs.br);
}

inline bool operator!=(const report& lhs, const report& rhs)
{
    return !operator==(lhs, rhs);
}

class listener
{
public:
    virtual void event(const report&) = 0;
};

} // namespace _joy

struct joy : talker<_joy::listener> {
    using report = _joy::report;
    using listener = _joy::listener;

    static constexpr int reserved_id = -1;
    int id = reserved_id;                       // joystick identifier
    std::string name;                           // UI-friendly name

    // Constants for set_port()
    static constexpr int port_none = 0;
    static constexpr int port_first = 1;

    virtual void set_port(int) {}

    virtual bool is_idle() = 0;
};

} // namespace ui

#endif /* UI__JOY__H */
