#pragma once

#include "screen.h"

namespace ui {

class screen_manager
{
public:
    screen_manager();

    screen* grab();                     // repeated calls return same screen
    void release();

protected:
    virtual screen* grab_screen() = 0;
    virtual void release_screen() = 0;

private:
    screen* _screen;
    int count;
};

} // namespace ui
