#ifndef UI__LABEL__H
#define UI__LABEL__H

#include <string>
#include "subscreen_view.h"

namespace ui {

class label : public subscreen_view
{
public:
    label();
    virtual ~label();

    const std::string& get_text() { return text; }
    void set_text(const std::string& s);

    // Helper to setup and add a one-line label to a view.
    void add_to_view(view* v, const std::string& s, int x, int y, int w = -1);

    // interface for screen_view
    virtual void on_draw(const drawing_context*);

protected:
    virtual const char* get_class_name() const { return "label"; }

private:
    std::string text;
};

} // namespace ui

#endif /* UI__LABEL__H */
