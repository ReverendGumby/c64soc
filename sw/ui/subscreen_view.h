#ifndef UI__SUBSCREEN_VIEW__H
#define UI__SUBSCREEN_VIEW__H

#include "screen_view.h"

namespace ui {

class subscreen_view : public screen_view
{
public:
    subscreen_view();
    subscreen_view(int left, int top, int width, int height);
    virtual ~subscreen_view();

    virtual void set_pos(int left, int top);
    virtual void set_size(int width, int height);
    void set_pos_size(int left, int top, int width, int height);

    int get_left() const { return left; }
    int get_top() const { return top; }
    int get_right() const { return left + width - 1; }
    int get_bottom() const { return top + height - 1; }
    int get_width() const { return width; }
    int get_height() const { return height; }

    // interface for screen_view
    virtual void on_draw();

protected:
    int left, top, width, height;

    virtual std::string get_name() const;

private:
    virtual void create_dc(drawing_context&);
    virtual void destroy_dc(drawing_context&);
};

} // namespace ui

#endif /* UI__SUBSCREEN_VIEW__H */
