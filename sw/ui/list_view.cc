#include <assert.h>
#include "list_view.h"

namespace ui {

list_view::list_view(struct model& model)
    : scrollable_list_view(
        *static_cast<struct scrollable_list_view::model*>(this)),
      model(model)
{
    count = 0;
    offset = 0;
}

list_view::~list_view()
{
}

void list_view::set_size(int new_width, int new_height)
{
    subscreen_view::set_size(new_width, new_height);
    update_view_count();
}

void list_view::set_count(int new_count)
{
    if (count == new_count)
        return;
    count = new_count;
    update_view_count();
}

int list_view::get_selected() const
{
    return scrollable_list_view::get_selected() + offset;
}

void list_view::select(int new_selection)
{
    int ns = new_selection;
    if (new_selection > -1) {
        ns = ns - offset;
        int h = get_height();
        if (ns < 0) {
            scroll_distance(ns);
            ns = 0;
        } else if (ns >= h) {
            int d = ns - h;
            scroll_distance(d);
            ns -= d;
        }
    }
    scrollable_list_view::select(ns);
}

// Viewport height or item count have changed.
void list_view::update_view_count()
{
    // Ensure the last item is not higher than the top of the view.
    if (count && (count - 1) - offset < 0)
        offset = (count - 1);

    auto h = get_height();
    int slv_count = std::min(h, count);
    scrollable_list_view::set_count(slv_count);
    scrollable_list_view::set_page_distance(slv_count);
}

std::string list_view::get_item(int idx)
{
    idx += offset;
    assert(idx >= 0 && idx < count);
    return model.get_item(idx);
}

void list_view::on_selected_changed(int idx)
{
    if (idx != -1)
        idx += offset;
    assert(idx >= -1 && idx < count);
    model.on_selected_changed(idx);
}

void list_view::on_selected(int idx)
{
    idx += offset;
    assert(idx >= 0 && idx < count);
    model.on_selected(idx);
}

int list_view::get_scroll_distance(int want)
{
    int h = get_height();
    if (count <= h)
        return 0;

    if (want > 0)
        want = std::min(want, count - h - offset);
    else
        want = std::max(want, -offset);
    return want;
}

void list_view::on_scrolled(int distance)
{
    offset += distance;
}

} // namespace ui
