#include <assert.h>
#include "view_manager.h"
#include "screen_manager.h"

#include "fullscreen_view.h"

namespace ui {

view_manager* fullscreen_view::mgr;
int fullscreen_view::width;
int fullscreen_view::height;

void fullscreen_view::set_manager(view_manager* mgr)
{
    fullscreen_view::mgr = mgr;
}

void fullscreen_view::set_size(int w, int h)
{
    width = w;
    height = h;
}

int fullscreen_view::max_width()
{
    assert(width);
    return width;
}

int fullscreen_view::max_height()
{
    assert(height);
    return height;
}

fullscreen_view::fullscreen_view()
{
    color_border = color_bg = color_t::BLACK;
    scr = nullptr;
}

fullscreen_view::~fullscreen_view()
{
}

void fullscreen_view::set_color_border(color_t c)
{
    color_border = c;
}

void fullscreen_view::set_color_bg(color_t c)
{
    color_bg = c;
}

void fullscreen_view::show()
{
    mgr->show(this);
}

void fullscreen_view::hide()
{
    mgr->hide(this);
}

bool fullscreen_view::has_focus() const
{
    return is_revealed();
}

void fullscreen_view::on_show()
{
    screen_view::on_show();
    grab_screen();
}

void fullscreen_view::on_hide()
{
    release_screen();
    screen_view::on_hide();
}

void fullscreen_view::on_revealed()
{
    on_focus_gained();
    screen_view::on_revealed();
    on_draw();
}

void fullscreen_view::on_obscured()
{
    on_focus_lost();
    screen_view::on_obscured();
}

bool fullscreen_view::on_key_down(const keypress_t& kp, int repeat)
{
    if (kp.code == kbcode::ESC && !repeat)
        hide();
    else
        return screen_view::on_key_down(kp, repeat);
    return true;
}

void fullscreen_view::save_and_hide_all()
{
    mgr->save_and_hide_all();
}

void fullscreen_view::restore_and_show_all()
{
    mgr->restore_and_show_all();
}

void fullscreen_view::grab_screen()
{
    if (scr == nullptr)
    {
        scr = mgr->screen_mgr.grab();
    }
}

void fullscreen_view::release_screen()
{
    if (scr != nullptr)
    {
        mgr->screen_mgr.release();
        scr = nullptr;
    }
}

void fullscreen_view::create_dc(drawing_context& dc)
{
    dc.scr = scr;
    dc.ss = new screenstream(dc.scr);

    dc.scr->set_border(color_border);
    dc.scr->set_bg(color_bg);
}

void fullscreen_view::destroy_dc(drawing_context& dc)
{
    delete dc.ss;
    dc.ss = nullptr;
    dc.scr = scr;
}

} // namespace ui

