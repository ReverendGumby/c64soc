#ifndef UI__SCREENSTREAM__H
#define UI__SCREENSTREAM__H

#include <streambuf>
#include <ostream>
#include <iomanip>
#include <limits>
#include "screen.h"

namespace ui {

// About window coordinates: right and bottom are one past the last usable
// character positions. Window width is right - left. A zero-width window has
// left == right. This avoids the +/- 1 that would have to happen otherwise.

class screenstreambuf : public std::streambuf
{
public:
    screenstreambuf(screen* scr, int left, int top, int right, int bottom);
    virtual ~screenstreambuf() {}

    screen* get_screen() { return scr; }

    void set_fg(screen::color_t c);
    void set_pos(int x, int y);
    void set_rev(bool en);
    void set_window(int l, int t, int w, int h);

    void write(char);
    void clear();
    void invert();

protected:
    virtual int_type overflow(int_type c);

private:
    screen* scr;
    int left, top, right, bottom;
    int x, y;
    bool line_wrapped;
    screen::color_t fg;
    bool rev;
};

class screenstream : public std::ostream
{
public:
    explicit screenstream(screen* scr, int left, int top, int width, int height);
    explicit screenstream(screen* scr);
    virtual ~screenstream();

    void clear();
    void invert();

    void setrev(bool en);
    void setpos(int x, int y);
    void setfg(screen::color_t c);

    // Select the active region within the initial region. Current position is
    // reset.
    void setwindow(int offx, int offy, int width, int height);

    // Reset the active region to the full initial region. Current position is
    // reset.
    void setwindow();

private:
    int left, top, right, bottom;               // initial region
    screenstreambuf ssb;
};

// TODO: These screenstream-specific manipulators can't follow std::ostream
// manipulators, because a downcast is needed from std::ostream to
// screenstream. Use dynamic_cast?

screenstream& clear(screenstream& os);
screenstream& invert(screenstream& os);
screenstream& revon(screenstream& os);
screenstream& revoff(screenstream& os);

inline screenstream& operator<<(screenstream& os, screenstream& (*fp)(screenstream&))
{
    return (*fp)(os);
}

struct _Setrev { bool en; };
inline _Setrev rev(bool en) { return {en}; }

inline screenstream& operator<<(screenstream& os, const _Setrev& f)
{
    os.setrev(f.en);
    return os;
}

struct _Setpos { int x, y; };
inline _Setpos pos(int x, int y) { return {x, y}; }

inline screenstream& operator<<(screenstream& os, const _Setpos& f)
{
    os.setpos(f.x, f.y);
    return os;
}

struct _Setfg { screen::color_t c; };
inline _Setfg fg(screen::color_t c) { return {c}; }

inline screenstream& operator<<(screenstream& os, const _Setfg& f)
{
    os.setfg(f.c);
    return os;
}

struct _Setwindow { int x, y, w, h; };
inline _Setwindow window(int x, int y, int w, int h)
{
    return {x, y, w, h};
}

inline screenstream& operator<<(screenstream& os, const _Setwindow& f)
{
    os.setwindow(f.x, f.y, f.w, f.h);
    return os;
}

struct _Resetwindow {};
inline _Resetwindow window()
{
    return {};
}

inline screenstream& operator<<(screenstream& os, const _Resetwindow& f)
{
    os.setwindow();
    return os;
}

} // namespace ui

#endif /* UI__SCREENSTREAM__H */
