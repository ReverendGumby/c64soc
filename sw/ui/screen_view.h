#ifndef UI__SCREEN_VIEW__H
#define UI__SCREEN_VIEW__H

#include "view.h"
#include "screenstream.h"
#include "screen.h"

namespace ui {

class view_manager;

class screen_view : public view
{
public:
    using color_t = screen::color_t;

    // static constexpr int MAX_WIDTH = screenstream::MAX_WIDTH;
    // static constexpr int MAX_HEIGHT = screenstream::MAX_HEIGHT;

    screen_view();
    virtual ~screen_view();

    void set_color_fg(color_t, bool focus=true);

    // interface for view
    //virtual void draw();
    virtual void on_revealed();
    virtual void on_obscured();
    virtual void on_draw();

protected:
    static constexpr color_t COLOR_UNDEF = color_t::NUM_COLORS;

    struct drawing_context
    {
        screen* scr;
        screenstream* ss;
    };

    const drawing_context* get_dc() const;
    const drawing_context* get_parent_dc() const;

    color_t get_color_fg(int idx);

    virtual void on_draw(const drawing_context*);
    
private:
    virtual void create_dc(drawing_context&) = 0;
    virtual void destroy_dc(drawing_context&) = 0;

    int color_idx(bool focus) const { return is_focusable() && !focus; }

    static const color_t default_color_fg[2];

    drawing_context dc;
    color_t color_fg[2] = { COLOR_UNDEF, COLOR_UNDEF };
};

} // namespace ui

#endif /* UI__SCREEN_VIEW__H */
