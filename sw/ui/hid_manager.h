#ifndef UI__HID_MANAGER__H
#define UI__HID_MANAGER__H

#include "util/listener.h"
#include "usb/usb_manager.h"
#include "usb/hid/kbd.h"
#include "usb/hid/joy.h"
#include "kbd.h"

namespace ui {

namespace _hid_manager {

class listener
{
public:
    virtual void joy_dev_changed(int id) = 0;
};

} // namespace _hid_manager

class hid_manager : private usb::usb_manager::listener,
                    private usb::hid::kbd::listener,
                    private usb::hid::joy::listener,
                    public talker<class _hid_manager::listener>,
                    public kbd,
                    public joy
{
public:
    using listener = _hid_manager::listener;

    static constexpr int max_joys = 2;

    hid_manager();

    void listen(usb::usb_manager&);

    void add_kbd_listener(kbd::listener*);
    void add_joy_listener(joy::listener*);

    joy* get_joy_dev(int id) const;

private:
    int allocate_joy_id(joy* dev);
    void free_joy_id(int);

    // interface for usb::usb_manager
    virtual void attached(usb::device*);
    virtual void detached(usb::device*);

    // interface for usb::hid::kbd::listener
    virtual void event(const kbd::report&);
    virtual void event(const kbd::keypress&);

    // interface for usb::hid::joy::listener
    virtual void event(const joy::report&);

    // interface for joy and kbd
    bool is_idle() override;

    kbd* kbd;
    joy* joy_id_used[max_joys];
};

} // namespace ui

#endif /* UI__HID_MANAGER__H */
