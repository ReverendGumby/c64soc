#pragma once

namespace ui {

class hmi_cart_listener
{
public:
    virtual void cart_modified(bool) = 0;
};

} // namespace ui
