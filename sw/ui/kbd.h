#ifndef UI__KBD__H
#define UI__KBD__H

#include "util/listener.h"

// Keyboard definitions

namespace ui {

namespace _kbd {

enum class kbcode : uint8_t {
    RSV = 0,
    A = 4,
    B,
    C,
    D,
    E,
    F,
    G = 10,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q = 20,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    N1 = 30,   /* 1 and ! */
    N2,        /* 2 and @ */
    N3,        /* 3 and # */
    N4,        /* 4 and $ */
    N5,        /* 5 and % */
    N6,        /* 6 and ^ */
    N7,        /* 7 and & */
    N8,        /* 8 and * */
    N9,        /* 9 and ( */
    N0,        /* 0 and ) */
    RET = 40,  /* Return (ENTER) */
    ESC,       /* ESCAPE */
    BAK,       /* DELETE (Backspace) */
    TAB,       /* Tab */
    SPC,       /* Spacebar */
    MIN,       /* - and _ */
    EQU,       /* = and + */
    LBR,       /* [ and { */
    RBR,       /* ] and } */
    BSL = 49,  /* \ and | */
    COL = 51,  /* ; and : */
    QUO,       /* ' and " */
    GRA,       /* ` and ~ */
    COM,       /* , and < */
    PER,       /* . and > */
    SLA,       /* / and ? */
    CAP,       /* Caps Lock */
    F1,
    F2,
    F3 = 60,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    PSR = 70,  /* PrintScreen */
    SCL,       /* Scroll Lock */
    PAU,       /* Pause */
    INS,       /* Insert */
    HOM,       /* Home */
    PGU,       /* PageUp */
    DEL,       /* Delete Forward (Delete) */
    END,       /* End */
    PGD,       /* PageDown */
    RAR,       /* RightArrow */
    LAR = 80,  /* LeftArrow */
    DAR,       /* DownArrow */
    UAR,       /* UpArrow */
    NUM,       /* Num Lock and Clear */
    NSL,       /* Keypad / */
    KAS,       /* Keypad * */
    KMI,       /* Keypad - */
    KPL,       /* Keypad + */
    KEN,       /* Keypad ENTER */
    K1,        /* Keypad 1 and End */
    K2 = 90,   /* Keypad 2 and Down Arrow */
    K3,        /* Keypad 3 and PageDn */
    K4,        /* Keypad 4 and Left Arrow */
    K5,        /* Keypad 5 */
    K6,        /* Keypad 6 and Right Arrow */
    K7,        /* Keypad 7 and Home */
    K8,        /* Keypad 8 and Up Arrow */
    K9,        /* Keypad 9 and PageUp */
    K0,        /* Keypad 0 and Insert */
    KPE = 99,  /* Keypad . and Delete */
    KEQ = 103, /* Keypad = */
};

constexpr int operator-(kbcode lhs, kbcode rhs)
{
    return static_cast<uint8_t>(lhs) - static_cast<uint8_t>(rhs);
}

struct mods
{
    int shift;                                  // K_LEFT | K_RIGHT
    int ctrl;                                   // K_LEFT | K_RIGHT
    int alt;                                    // K_LEFT | K_RIGHT
    int win;                                    // K_LEFT | K_RIGHT
    int caps_lock;
    int num_lock;
    int scroll_lock;
};

struct report
{
    static constexpr int max_codes = 10;

    struct mods mods;
    kbcode codes[max_codes];
    int num_codes;

    inline bool has_code(kbcode c) const
    {
        for (int i = 0; i < num_codes; i++)
            if (c == codes[i])
                return true;
        return false;
    }

    inline bool is_idle() const
    {
        return num_codes == 0;
    }
};

struct keypress
{
    struct mods mods;
    kbcode code;
    char ch;
    bool down;                                  // press (down) or release (up)
};

class listener
{
public:
    virtual void event(const report&) = 0;
    virtual void event(const keypress&) = 0;
};

} // namespace _kbd

struct kbd : talker<_kbd::listener> {
    using kbcode = _kbd::kbcode;
    using listener = _kbd::listener;
    using mods = _kbd::mods;
    using report = _kbd::report;
    using keypress = _kbd::keypress;

    static constexpr int K_LEFT = (1<<0);
    static constexpr int K_RIGHT = (1<<1);

    virtual bool is_idle() = 0;
};

} // namespace ui

#endif /* UI__KBD__H */
