#include <assert.h>

#include "screen_view.h"

namespace ui {

const screen_view::color_t screen_view::default_color_fg[2] =
	{ color_t::WHITE, color_t::GRAY2 };

screen_view::screen_view()
{
    dc.scr = nullptr;
    dc.ss = nullptr;
}

screen_view::~screen_view()
{
    assert(!is_revealed());
}

void screen_view::set_color_fg(color_t c, bool focus)
{
    auto idx = color_idx(focus);
    color_fg[idx] = c;
}

screen_view::color_t screen_view::get_color_fg(int idx)
{
    if (color_fg[idx] != COLOR_UNDEF)
        return color_fg[idx];
    auto* pv = dynamic_cast<screen_view*>(get_parent_view());
    if (pv)
        return pv->get_color_fg(idx);
    return default_color_fg[idx];
}

const screen_view::drawing_context* screen_view::get_dc() const
{
    if (!is_revealed())
        return nullptr;
    if (dc.scr == nullptr || dc.ss == nullptr)
        return nullptr;
    return &dc;
}

const screen_view::drawing_context* screen_view::get_parent_dc() const
{
    auto* pv = dynamic_cast<screen_view*>(get_parent_view());
    if (!pv)
        return nullptr;
    auto* pdc = pv->get_dc();
    return pdc;
}

void screen_view::on_revealed()
{
    create_dc(dc);

    view::on_revealed();
}

void screen_view::on_obscured()
{
    view::on_obscured();

    destroy_dc(dc);
}

void screen_view::on_draw()
{
    auto* dc = get_dc();
    if (dc)
    {
        auto c = get_color_fg(color_idx(has_focus()));
        *dc->ss << fg(c);
        dc->ss->clear();
        on_draw(dc);
    }

    view::on_draw();
}

void screen_view::on_draw(const drawing_context*)
{
}

} // namespace ui
