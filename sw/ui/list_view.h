#ifndef UI__LIST_VIEW__H
#define UI__LIST_VIEW__H

#include "scrollable_list_view.h"

namespace ui {

// list_view: A view that shows a scrollable list of items. This class
// guarantees that the index of the first item is always 0.
//
// 'count' is the total number of items in the list. This is independent of the
// view height.

class list_view : public scrollable_list_view,
                  private scrollable_list_view::model
{
public:
    struct model
    {
        virtual std::string get_item(int idx) = 0;

        virtual void on_selected_changed(int idx) {}
        virtual void on_selected(int idx) = 0;
    };

    list_view(model&);
    virtual ~list_view();

    void set_size(int new_width, int new_height) override;

    void set_count(int);
    int get_selected() const;
    void select(int new_selection);

protected:
    virtual const char* get_class_name() const { return "list_view"; }

private:
    void update_view_count();

    // interface for scrollable_list_view::model
    std::string get_item(int idx) override;
    void on_selected_changed(int idx) override;
    void on_selected(int idx) override;
    int get_scroll_distance(int want) override;
    void on_scrolled(int distance) override;

    model& model;
    int count;                                  // number of items
    int offset;                                 // scroll viewport offset
};

} // namespace ui

#endif /* UI__LIST_VIEW__H */
