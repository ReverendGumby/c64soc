#ifndef UI__TEXT_EDITOR__H
#define UI__TEXT_EDITOR__H

#include <string>
#include <functional>
#include "subscreen_view.h"
#include "util/pollable.h"
#include "util/timer.h"

namespace ui {

class text_editor : public subscreen_view, private pollable
{
public:
    text_editor();
    virtual ~text_editor();

    const std::string& get_text() { return text; }
    void set_text(std::string s);

    bool is_editing() const { return editing; }

    using change_action_fobj = std::function<void(void)>;
    void set_change_action(change_action_fobj action);

    // interface for pollable
    virtual const char* get_poll_name() const { return "text_editor"; }
    virtual void poll();

    // interface for view
    virtual bool is_focusable() const { return true; }
    virtual bool on_key_down(const keypress_t& k, int repeat);
    virtual void on_focus_gained();
    virtual void on_focus_lost();

protected:
    virtual const char* get_class_name() const { return "text_editor"; }

private:
    void start_editing();
    enum stop_editing_mode_t { commit, abort };
    void stop_editing(stop_editing_mode_t mode = commit);

    void draw_text(const drawing_context*);

    void enable_cursor(bool);
    void set_cursor_seen(bool);
    void draw_cursor(const drawing_context*);
    void invert_cursor(const drawing_context*);
    bool move_cursor(int dir);

    bool insert_at_cursor(char);
    bool delete_at_cursor();

    // interface for screen_view
    virtual void on_draw(const drawing_context*);

    std::string text;
    std::string orig_text;
    bool editing;
    bool cursor_enabled;
    bool cursor_seen;
    int cursor_pos;
    timer cursor_timer;
    change_action_fobj change_action;
};

} // namespace ui

#endif /* UI__TEXT_EDITOR__H */
