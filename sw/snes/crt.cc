#include <string.h>
#include <stdio.h>
#include <algorithm>
#include <util/crc32.h>
#include <util/string.h>
#include "crt.h"

namespace snes {

Chip::Chip(type_t type, uint8_t *bin, size_t len)
{
    _type = type;
	_len = len;
	_data = &bin[0];
}

bool Chip::is_ram() const
{
    return _type == Chip::SRAM;
}

const char* Chip::name() const
{
    switch (type()) {
    case ROM: return "ROM";
    case SRAM: return "SRAM";
    default: break;
    }
    return "?";
}

Crt::Crt(Buf&& buf, size_t bin_len)
	: _bin(std::move(buf)), _bin_len(bin_len)
{
    auto* bin = &_bin[0];
    size_t len = _bin_len;
    _chips.push_back(Chip{Chip::ROM, bin, len});
    
    // In case of no data spec, pick reasonable defaults
    uint8_t rds_map_mode = 0x20;
    uint8_t rds_cart_type = 0x00;
    uint8_t rds_ram_size = 0x00;

    _rom_data_spec = find_rom_data_spec();
    if (_rom_data_spec) {
        rds_map_mode = _rom_data_spec[0x25];
        rds_cart_type = _rom_data_spec[0x26];
        //rds_rom_size = _rom_data_spec[0x27];
        rds_ram_size = _rom_data_spec[0x28];
    }

    switch (rds_map_mode & 0xF) {
    case 0x0: _map_mode = LOROM;    break;
    case 0x1: _map_mode = HIROM;    break;
    case 0x5: _map_mode = EXHIROM;  break;
    default:
        auto what = string_printf("unsupported map mode 0x%02X", rds_map_mode);
        throw Crt_format_error(what);
    }

    if (rds_cart_type > 0x02) {
        auto what = string_printf("unsupported cart type 0x%02X", rds_cart_type);
        throw Crt_format_error(what);
    }

    _battery = false;
    switch (rds_cart_type & 0xF) {
    case 0x2:
    case 0x5:
    case 0x6:
        _battery = true;
        break;
    default:
        break;
    }

    size_t sram_len;
    switch (rds_ram_size) {
    case 0x0: sram_len = 0;       break;
    case 0x1: sram_len = 16;      break;
    case 0x3: sram_len = 64;      break;
    case 0x5: sram_len = 256;     break;
    case 0x6: sram_len = 512;     break;
    case 0x7: sram_len = 1024;    break;
    default: throw Crt_format_error("unsupported RAM size");
    }
    sram_len *= 1024 / 8;                       // Kbit -> bytes
    if (sram_len) {
        if (_battery) {
            _sram_bin = Buf { new uint8_t[len] };
            memset(&_sram_bin[0], 0, len); // TODO: Randomize
        }
        _chips.push_back(Chip{Chip::SRAM, &_sram_bin[0], sram_len});
    }
}

int Crt::chip_size_kb(Chip::type_t type) const
{
    int kb = 0;
    for (const auto& c : _chips)
        if (c.type() == type)
            kb += c.len() / 1024;
    return kb;
}

void Crt::load_sram(Buf&& bin, size_t len)
{
    if (_sram_bin == nullptr)
        return;
    auto i = find_sram();
    if (i != _chips.end()) {
        auto& c = *i;
        memcpy(&c.data()[0], &bin[0], std::min(len, c.len()));
    }
}

void Crt::save_sram(Buf& bin, size_t& len)
{
    len = 0;
    if (_sram_bin == nullptr)
        return;
    auto i = find_sram();
    if (i == _chips.end())
        return;
    len = i->len();
    bin = Buf { new uint8_t[len] };
    memcpy(&bin[0], &_sram_bin[0], len);
}

Crt::ChipList::iterator Crt::find_sram()
{
    return std::find_if(_chips.begin(), _chips.end(), 
                        [](const Chip& c)
                        { return Chip::SRAM == c.type(); });
}

uint8_t* Crt::find_rom_data_spec()
{
    size_t len = _bin_len;

    for (size_t start : { 0x7FB0, 0xFFB0, 0x40FFB0 }) {
        if (start + 0x50 > len)
            continue;
        auto* p = &_bin[start];
        if (p[0x2c] == uint8_t(~p[0x2e]) &&
            p[0x2d] == uint8_t(~p[0x2f])) {
            return p;
        }
    }

    return nullptr;
}

const char* name(Crt::map_mode_t e)
{
    switch (e) {
    case Crt::LOROM: return "LOROM";
    case Crt::HIROM: return "HIROM";
    case Crt::EXHIROM: return "EXHIROM";
    default: break;
    }
    return "?";
}

} // namespace snes
