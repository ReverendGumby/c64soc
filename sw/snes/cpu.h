#pragma once

#include "icd.h"
#include <stdint.h>

namespace snes {

struct cpu_chip_state
{
    // On-chip I/O registers
    struct io_regs_t {
        // Write-only registers
        uint8_t joyout;                         // $4016 JOYOUT

        // Read-only registers
        uint8_t joyser0 : 2;                    // $4016 JOYSER0
        uint8_t joyser1 : 5;                    // $4017 JOYSER1

        // Write-only registers
        struct {                                // $4200 NMITIMEN
            uint8_t ajpr_en : 1;
            uint8_t hvt_mode : 2;
            uint8_t vbl_en : 1;
        } nmitimen;
        uint8_t wrio;                           // $4201 WRIO
        uint8_t wrmpya;                         // $4202 WRMPYA
        uint16_t wrdiva;                        // $4204/5 WRDIVL/H
        uint16_t htime;                         // $4207/8 HTIMEL/H
        uint16_t vtime;                         // $4209/A VTIMEL/H
        uint8_t mdmaen;                         // $420B MDMAEN
        uint8_t hdmaen;                         // $420C HDMAEN
        struct {                                // $420D MEMSEL
            uint8_t memsel;
        } memsel;

        // Read-only registers
        struct {                                // $4210 RDNMI
            uint8_t cpu_ver : 4;
            uint8_t flag : 1;
        } rdnmi;
        struct {                                // $4211 TIMEUP
            uint8_t flag : 1;
        } timeup;
        struct {                                // $4212 HVBJOY
            uint8_t ajpr_busy : 1;
            uint8_t hblank : 1;
            uint8_t vblank : 1;
        } hvbjoy;
        uint8_t rdio;                           // $4213 RDIO
        uint16_t rddiv;                         // $4214/5 RDDIVL/H
        uint16_t rdmpy;                         // $4216/7 RDMPYL/H
        uint16_t joy1;                          // $4218/9 JOY1L/H
        uint16_t joy2;                          // $421A/B JOY2L/H
        uint16_t joy3;                          // $421C/D JOY3L/H
        uint16_t joy4;                          // $421E/F JOY4L/H
    } io_regs;

    // DMA registers
    struct dma_regs_t {
        struct ch_t {
            uint8_t dmapx;                      // $43x0 DMAPx
            uint8_t bbadx;                      // $43x1 BBADx
            uint8_t a1txl;                      // $43x2 A1TxL
            uint8_t a1txh;                      // $43x3 A1TxH
            uint8_t a1xb;                       // $43x4 A1xB
            uint8_t dasxl;                      // $43x5 DASxL
            uint8_t dasxh;                      // $43x6 DASxH
            uint8_t dasbx;                      // $43x7 DASBx
            uint8_t a2axl;                      // $43x8 A2AxL
            uint8_t a2axh;                      // $43x9 A2AxH
            uint8_t ntrlx;                      // $43xA NTRLx
            uint8_t unusedx;                    // $43xB UNUSEDx
        } ch[8];
    } dma_regs;

    // DMA high-level state
    struct dma_state_t {
        uint32_t active : 1;
        uint32_t gpdma : 1;
        uint32_t hdma : 1;
        uint32_t xfer : 1;
        uint32_t ch_active : 1;
        uint32_t done : 1;

        uint32_t hcdis;
        uint32_t hxfer;

        uint32_t a_o : 24;
        uint32_t pa_o : 8;
        uint32_t a_oe : 1;
        uint32_t pa_oe : 1;
        uint32_t nwr_o : 1;
        uint32_t nrd_o : 1;
        uint32_t npawr_o : 1;
        uint32_t npard_o : 1;
    } dma_state;

    // External state
    struct pins_t {
        uint32_t jpout : 3;
        uint32_t jpclk : 2;
        uint32_t jp1d : 2;
        uint32_t jp2d : 5;
    } pins;

    // Internal state
    struct internal_t {
        struct clkgen_t {
            // CPU_MON_CLKGEN_*
            uint32_t cycle_ccnt : 4;
            uint32_t cp2_clkcnt : 4;
            uint32_t slow_ccnt : 4;
            uint32_t cpu_gate_dly : 1;
            uint32_t cpu_act : 1;
            uint32_t slowck_act : 1;
            uint32_t cp2 : 1;
            uint32_t cpu_refresh_en : 1;
            uint32_t slow_gate_dly : 1;
            uint32_t slow_refresh_en : 1;
        } clkgen;

        struct ajpr_t {
            // CPU_MON_AJPR0_*
            uint32_t ccnt : 4;
            uint32_t cycle : 6;
            uint32_t vblank_d : 1;
            uint32_t jpclk : 2;
            uint32_t jpout : 1;
        } ajpr;

        struct gen_t {
            // CPU_MON_GEN0_*
            uint32_t vblank_d : 1;
            uint32_t vbl_set : 1;
            uint32_t vbl_clear_flags : 1;
            uint32_t vbl_flag : 1;
            uint32_t vbl_flag_d : 1;
            uint32_t vbl_start : 1;
            uint32_t vbl_end : 1;
            uint32_t hblank_d : 1;
            uint32_t hbl_start : 1;
            uint32_t hbl_end : 1;
            uint32_t hvt_set : 1;
            uint32_t hvt_clear_flags : 1;
            uint32_t hvt_flag : 1;
            uint32_t hvt_flag_d : 1;
            uint32_t cc_d : 1;
            uint32_t refresh_en : 1;

            // CPU_MON_GEN1_*
            uint32_t dma_mset : 8;
            uint32_t hcnt : 9;

            // CPU_MON_GEN2_*
            uint32_t vcnt : 9;
        } gen;

        struct muldiv_t {
            // CPU_MON_MULDIV1_*
            uint32_t shifter : 23;

            // CPU_MON_MULDIV3_*
            uint32_t st : 5;
            uint32_t muldivnot : 1;
        } muldiv;

        struct dmac_t {
            // CPU_MON_DMAC_INT0_*
            uint32_t ntrlz : 1;
            uint32_t post_vbl_end : 1;
            uint32_t hstartl : 1;
            uint32_t hstart : 1;
            uint32_t gprestart_d : 1;
            uint32_t aca : 3;
            uint32_t dl : 8;
            uint32_t men : 8;
            uint32_t hen_d : 8;

            // CPU_MON_DMAC_INT1_*
            uint32_t cen : 8;
            uint32_t gptpos : 2;
            uint32_t hst : 3;
            uint32_t hbc : 3;
        } dmac;
    } internal;
};

class cpu
{
public:
    void init();

    enum {
        state_io_regs = 1<<0,
        state_dma_regs = 1<<1,
        state_pins = 1<<2,
        state_dma_state = 1<<3,
        state_internal = 1<<4,
        state_all = -1,
    };
    cpu_chip_state read_chip_state(int which = state_all);
    void write_chip_state(const cpu_chip_state&, int which = state_all);

    struct state_t {
        cpu_chip_state chip;
        icd::state_t core;
    };
    void read_state(state_t&);
    void write_state(const state_t&);

    class icd icd;

private:
    uint32_t read_reg(uint32_t sel);
    void write_reg(uint32_t sel, uint32_t val);
};

} // namespace snes
