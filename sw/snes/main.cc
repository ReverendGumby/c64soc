/*
 * snes/main.cc: main application for SNES
*/

#include <chrono>
#include <thread>
#include "plat/audio.h"
#include "plat/manager.h"
#include "app/version.h"
#include "sd/sd_manager.h"
#include "fat/cli.h"
#include "usb/usb_manager.h"
#include "ui/hmi_manager.h"
#include "ui/hid_manager.h"
#include "util/pollable.h"
#include "cli/manager.h"
#include "snes/sys.h"
#include "snes/cart_manager.h"
#include "monitor/monitor.h"
#include "cpumem.h"

static const plat::audio_config_t audio_cfg = {
    .sample_rate = 32000,
};
static const plat::manager::config_t plat_cfg = {
    .audio = audio_cfg,
};

int main()
{
    plat::manager plm(plat_cfg);
    plm.early_init();

    printf("snes\n"
           "SW version %s (@%s) build %s\n",
           sw_build_branch, sw_build_rev, sw_build_timestamp);

    plm.late_init();

    auto hw_ver = plm.get_rtl_version();
    printf("HW version %s (@%s) build %s\n",
           hw_ver.git_branch.c_str(), hw_ver.git_rev.c_str(), hw_ver.timestamp.c_str());

    usb::usb_manager um;
    um.init();

    sd::sd_manager sm;
    fat::cli::init();

    ui::hid_manager hid_mgr{};
    hid_mgr.listen(um);

    snes::sys sys(plm, hid_mgr);
    sys.init();

    snes::cart_manager cm(sys);

    ui::hmi_manager hmi_mgr(plm);
    hmi_mgr.listen(sys.cart);

    snes::monitor::monitor mon(sys.cpu.icd);

    cli::manager clim;
    if (sm.is_card_inserted())
        clim.try_autoexec();
    clim.start();

    plm.reset_video(false);
    plm.audio_up();

    for (;;) {
        poll_manager_get().poll();

        plm.set_video_hdmi_mode(plm.get_sw(0)); // XXX
        pthread_yield();
    }

    //for (;;)
    //    std::this_thread::sleep_for(std::chrono::seconds(10));

    return 0;
}
