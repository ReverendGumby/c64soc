#pragma once

#include <util/listener.h>
#include <vector>
#include <stdint.h>
#include "icd_match.h"
#include "monitor/w65c816s/cpu_regs.h"

namespace snes {

namespace _icd
{

struct listener
{
    virtual void resumed() = 0;
    virtual void on_state_loaded() = 0;
};

} // namespace _icd

class icd : public talker<_icd::listener>
{
public:
    using listener = _icd::listener;
    using cpu_regs = ::monitor::w65c816s::cpu_regs;

    void init();
    void reset();

    cpu_regs read_cpu_regs();
    void write_cpu_regs(const cpu_regs&);

    bool is_halted();
    void force_halt();
    void resume();

    icd_match& get_match(int);
    bool is_any_match_triggered();

    struct state_t {
        cpu_regs regs;
    };
    void read_state(state_t&);
    void write_state(const state_t&);

private:
    std::vector<icd_match> match_units;

    uint32_t read_cpu_reg(uint8_t reg);
    void write_cpu_reg(uint8_t reg, uint32_t val);
};

} // namespace snes
