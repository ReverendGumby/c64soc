#pragma once

#include <stdint.h>

namespace snes {

class ctlr
{
public:
    struct state_t {
        uint16_t c1, c2;
    };

    ctlr();

    void init();

    void reset();
    void set(const state_t&);

    state_t get();
};

} // namespace snes
