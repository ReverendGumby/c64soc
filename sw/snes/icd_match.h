#pragma once

#include <string>
#include <stdint.h>

namespace snes {

class icd_match
{
public:
    struct status
    {
        int match_sel;
        int num_match;
    };

    struct data
    {
        uint32_t a;
        uint8_t db;
        bool rw;
        bool sync;
        uint16_t col;
        uint16_t row;
        uint32_t frame;
        uint32_t cpu_cyc;
    };

    class trigger
    {
    public:
        // "set" means the field(s) value must match to trigger.
        bool is_a_set() const;
        bool is_db_set() const;
        bool is_rw_set() const;
        bool is_sync_set() const;
        bool is_col_set() const;
        bool is_row_set() const;
        bool is_frame_set() const;

        uint32_t get_a() const;
        uint8_t get_db() const;
        bool get_rw() const;
        bool get_sync() const;
        uint16_t get_col() const;
        uint16_t get_row() const;
        uint32_t get_frame() const;

        void set_a(uint32_t v);
        void set_db(uint8_t v);
        void set_rw(bool v);
        void set_sync(bool v);
        void set_col(uint16_t v);
        void set_row(uint16_t v);
        void set_frame(uint32_t v);

        void clear_a();
        void clear_db();
        void clear_rw();
        void clear_sync();
        void clear_col();
        void clear_row();
        void clear_frame();

        std::string to_string() const;

    private:
        friend class icd_match;

        bool is_set(uint32_t mask, int idx = 0) const;
        uint32_t get(uint32_t mask, int idx = 0) const;
        void set(uint32_t bits, uint32_t mask, int idx = 0);
        void clear(uint32_t mask, int idx = 0);

        uint32_t din[3] = {};
        uint32_t den[3] = {};
    };

    icd_match(int idx);

    status get_status();

    bool is_enabled();
    bool is_triggered();
    data get_data();

    void set_trigger(const trigger& t);
    void disable();
    void enable();
    void reset_triggered();

private:
    int _idx;
};

} // namespace snes
