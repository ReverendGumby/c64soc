#pragma once

#include "memptr.h"
#include "util/unique_buf.h"
#include <stdint.h>
#include <memory>
#include <vector>

namespace snes {

class cart;

class cpumem
{
public:
    enum class source_t {
        unknown,
        wram,
        cart_rom,
        cart_sram,
    };

    struct mem_desc_t {
        source_t source;
        uint8_t* base;
        size_t len;
    };

    static constexpr size_t wram_len = 128 * 1024;

    cpumem(class cart& cart);

    void init();

    void add_mem(const mem_desc_t&);
    const mem_desc_t& get_mem(source_t);

    uint8_t* get_wram();

    uint8_t* addr_to_ptr(memaddr a);

    struct state_t {
        unique_buf wram;
        uint32_t wmadd;
        struct last_read_t {
            source_t source;
            uint32_t addr_off;
            uint8_t data;
        } last_read;
    };
    void read_state(state_t&);
    void pre_write_state();
    void post_write_state();
    void write_state(const state_t&);

private:
    uint32_t read_reg(uint8_t sel);
    void write_reg(uint8_t sel, uint32_t val);

    void find_last_reader(uint32_t araddr, source_t& source, uint32_t& addr_off);

    static std::unique_ptr<uint8_t[]> wram;

    class cart& _cart;
    std::vector<mem_desc_t> _mems;
    state_t::last_read_t _write_state_last_read;
};

const char* name(cpumem::source_t);
cpumem::source_t from_name(const char*);

} // namespace snes
