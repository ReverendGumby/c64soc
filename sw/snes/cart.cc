#include <memory>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "util/debug.h"
#include "cpumem.h"
#include "cart.h"
#include "emubus.h"

#include "cart_loader/normal.h"

#define DBG_LVL 0
#define DBG_TAG "CART"

namespace snes {

std::unique_ptr<uint8_t[]> cart::sram, cart::rom;

cart::cart(class cpumem& cpumem)
    : _cpumem(cpumem)
{
}

void cart::init()
{
    DBG_PRINTF(1, "entry\n");

    sram = std::unique_ptr<uint8_t[]> {new uint8_t[sram_len]};
    rom = std::unique_ptr<uint8_t[]> {new uint8_t[rom_len]};

    _cpumem.add_mem(cpumem::mem_desc_t{
            cpumem::source_t::cart_rom, get_rom(), rom_len});
    _cpumem.add_mem(cpumem::mem_desc_t{
            cpumem::source_t::cart_sram, get_sram(), sram_len});

    _cldr = nullptr;
    //Cldr::init();

    _write_tmr.set_duration(0.25 * timer::sec);
    _write_tmr.start();
    pollable_add(this);
}

uint8_t* cart::get_sram()
{
    return sram.get();
}

uint8_t* cart::get_rom()
{
    return rom.get();
}

uint8_t* cart::addr_to_ptr(memaddr a)
{
    uint16_t a16 = a & 0xFFFF;
    uint8_t ba = (a >> 16) & 0xFF;
    bool romsel = ((ba & 0x7F) >= 0x40) || (a16 >= 0x8000);
    uint32_t off;
    bool sram = false;

    switch (_map_cfg.mem_map) {
    case Crt::LOROM:
        if (a >= 0x8000) {
            off = ((ba & 0x7F) * 0x8000) + (a16 & 0x7FFF);
            break;
        }
        else if (romsel && (ba & 0x70) == 0x70) {
            sram = true;
            off = a16;
            break;
        }
        else
            return nullptr;
    case Crt::HIROM:
        if (romsel) {
            off = a & 0x3FFFFF;
            break;
        }
        else if ((ba & 0x70) == 0x30) {
            sram = true;
            off = ((ba & 0x0F) * 0x2000) + (a16 - 0x6000);
            break;
        }
        else
            return nullptr;
    case Crt::EXHIROM:
        if (romsel) {
            off = ((ba & 0x80) * 0x40000) + (a & 0x3FFFFF);
            break;
        }
        else if (ba & 0x80) {
            sram = true;
            off = ((ba & 0x3F) * 0x2000) + (a16 - 0x6000);
            break;
        }
        else
            return nullptr;
    default:
        return nullptr;
    }

    if (sram) {
        return get_sram() + (off & _map_cfg.sram_mask);
    } else {
        return get_rom() + (off & _map_cfg.rom_mask);
    }
}

void cart::read_map_cfg()
{
    switch (EMUBUS_MEM_MAP) {
    case EMUBUS_MEM_MAP_LOROM:      _map_cfg.mem_map = Crt::LOROM;      break;
    case EMUBUS_MEM_MAP_HIROM:      _map_cfg.mem_map = Crt::HIROM;      break;
    case EMUBUS_MEM_MAP_EXHIROM:    _map_cfg.mem_map = Crt::EXHIROM;    break;
    }
    _map_cfg.rom_mask = EMUBUS_ROM_ADDR_MASK;
    _map_cfg.sram_mask = EMUBUS_RAM_ADDR_MASK;
    _map_cfg.sram_enable = (EMUBUS_CHIP_CFG & EMUBUS_CHIP_CFG_RAM_ENABLE) != 0;
}

void cart::set_map_cfg(const map_cfg_t& cfg)
{
    DBG_PRINTF(1, "\n");
    uint32_t mem_map = 0;
    uint32_t chip_cfg = 0;

    switch (cfg.mem_map) {
    case Crt::LOROM:    mem_map = EMUBUS_MEM_MAP_LOROM;      break;
    case Crt::HIROM:    mem_map = EMUBUS_MEM_MAP_HIROM;      break;
    case Crt::EXHIROM:  mem_map = EMUBUS_MEM_MAP_EXHIROM;    break;
    }

    if (cfg.sram_enable)
        chip_cfg |= EMUBUS_CHIP_CFG_RAM_ENABLE;

    EMUBUS_CHIP_CFG = chip_cfg;
    EMUBUS_MEM_MAP = mem_map;
    EMUBUS_ROM_ADDR_MASK = cfg.rom_mask;
    EMUBUS_RAM_ADDR_MASK = cfg.sram_mask;

    read_map_cfg();
}

void cart::reset()
{
    DBG_PRINTF(1, "\n");

    EMUBUS_RESETS &= ~EMUBUS_RESETS_CART;
    EMUBUS_RESETS |= EMUBUS_RESETS_CART;

    EMUBUS_BASE_ROM = reinterpret_cast<uint32_t>(get_rom());
    EMUBUS_BASE_RAM = reinterpret_cast<uint32_t>(get_sram());

    read_map_cfg();
}

cart::Cldr* cart::new_loader()
{
    return new cart_loader::normal{_crt, _cpumem, *this};
}

void cart::unload()
{
    DBG_PRINTF(1, "\n");

    std::unique_lock<std::mutex> lck { _mtx };

    _cldr = nullptr;
}

void cart::load(PCrt pcrt)
{
    DBG_PRINTF(1, "entry\n");
    std::unique_lock<std::mutex> lck { _mtx };

    _crt = pcrt;

    _cldr = decltype(_cldr) { new_loader() };

    _cldr->init();
    _cldr->load();
    set_modified(false);
}

cart::PCrt cart::save()
{
    DBG_PRINTF(1, "entry\n");
    std::unique_lock<std::mutex> lck { _mtx };

    if (_cldr) {
        _cldr->save();
        set_modified(false);
    }

    return _crt;
}

void cart::reset_ram()
{
    DBG_PRINTF(1, "entry\n");
    bool force_zero = _battery_disabled;
    if (_cldr)
        _cldr->reset_ram(force_zero);
}

bool cart::can_save() const
{
    return !_battery_disabled && _crt && _crt->has_battery();
}

void cart::set_battery_disabled(bool v)
{
    _battery_disabled = v;
    if (_battery_disabled)
        set_modified(false);
}

void cart::poll()
{
    std::unique_lock<std::mutex> lck { _mtx };

    if (_write_tmr.expired()) {
        _write_tmr.restart();
        int cnt = EMUBUS_RAM_WRITE_CNT;
        if (cnt)
        {
            EMUBUS_RAM_WRITE_CNT = 0;
            DBG_PRINTF(5, "RAM writes: %d\n", cnt);
            if (can_save()) {
                set_modified(true);
            }
        }
    }
}

void cart::set_modified(bool b)
{
    // Pass all updates to listeners, even if _modified hasn't changed, so they
    // can learn of repeated modifications.
    _modified = b;
    talker<listener>::speak(&listener::cart_modified, _modified);
    talker<ui::hmi_cart_listener>::speak(
        &ui::hmi_cart_listener::cart_modified, _modified);
}

void cart::read_state(state_t& st)
{
    st.loaded = _cldr != nullptr;
    if (st.loaded) {
        if (_map_cfg.sram_enable) {
            size_t used_sram_len = _map_cfg.sram_mask + 1;
            st.sram = unique_buf(buffer(get_sram(), used_sram_len));
        }
    }
}

} // namespace snes
