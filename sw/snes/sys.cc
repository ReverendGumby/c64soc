#include <stdexcept>
#include <stdio.h>
#include "util/string.h"
#include "ui/hid_manager.h"
#include "emubus.h"
#include "memptr.h"
#include "sys.h"

namespace snes {

sys::sys(plat::manager& pm, ui::hid_manager& hm)
    : plat_mgr(pm),
      cpumem(cart),
      cart(cpumem),
      joy(hm, ctlr),
      cli(*this)
{
    pause_count = 0;
    reset_ram_pending = false;
    cold_reset = false;
}

void sys::init()
{
	uint32_t val;

	val = EMUBUS_ID;
	if (val != EMUBUS_ID_DATA) {
        auto why = string_printf("EMUBUS ID mismatch: got %08lx, wanted %08lx", val, EMUBUS_ID_DATA);
		throw std::runtime_error(why);
	}
    auto why = string_printf("EMUBUS ID is %08lx", val);
    printf("%s\n", why.c_str());

    // HACK: testing how this sounds...
    EMUBUS_HOLD |= EMUBUS_HOLD_LOCK_APU_TO_CPU_HOLD;

    reset_all(true);

    cpumem.init();
    memptr::attach_sys(*this);
    mem.init();
    cart.init();
    ctlr.init();
    cpu.init();
    ppu.init();
    apu.init();
    joy.start();
}

void sys::reset_all(bool assert)
{
    if (assert)
        EMUBUS_RESETS = 0;
    else
        EMUBUS_RESETS = (uint32_t)-1;
}

void sys::reset_snes(bool assert)
{
    if (assert) {
        EMUBUS_RESETS &= ~EMUBUS_RESETS_EMU;
        cpu.icd.resume(); // in case the ICD was asserting SYS_HOLD
        reset_ram();
    } else
        EMUBUS_RESETS |= EMUBUS_RESETS_EMU;
}

void sys::power_cycle_snes()
{
    cold_reset = true;
    reset_snes(true);
    reset_snes(false);
    cold_reset = false;
}

bool sys::is_paused() const
{
    return pause_count;
}

void sys::pause()
{
    if (pause_count == 0)
    {
        set_sys_hold_req(true);
    }
    pause_count ++;
}

void sys::resume()
{
    pause_count --;
    if (pause_count == 0)
    {
        set_sys_hold_req(false);
    }
}

void sys::reset_ram()
{
    if (is_paused())
        reset_ram_pending = true;
    else
        reset_ram_now();
}

void sys::reset_ram_if_pending()
{
    if (reset_ram_pending) {
        reset_ram_pending = false;
        reset_ram_now();
    }
}

void sys::reset_ram_now()
{
    if (cold_reset) {
        mem.reset();
        cart.reset_ram();
    }
}

void sys::set_sys_hold_req(bool assert)
{
    auto hold = EMUBUS_HOLD;
    if (assert)
        hold |= EMUBUS_HOLD_SYS_HOLD_REQ;
    else
        hold &= ~EMUBUS_HOLD_SYS_HOLD_REQ;
    EMUBUS_HOLD = hold;
}

bool sys::get_sys_hold_ack()
{
    return (EMUBUS_HOLD & EMUBUS_HOLD_SYS_HOLD_ACK) != 0;
}

void sys::read_state(state_t& st)
{
    assert(get_sys_hold_ack());

    cpu.read_state(st._cpu);
    ppu.read_state(st._ppu);
    cpumem.read_state(st._cpumem);
    apu.read_state(st._apu);
    cart.read_state(st._cart);
}

void sys::write_state(const state_t& st)
{
    assert(get_sys_hold_ack());

    cpumem.pre_write_state();

    cpu.write_state(st._cpu);
    ppu.write_state(st._ppu);
    cpumem.write_state(st._cpumem);
    apu.write_state(st._apu);
    // cart.write_state(st._cart);

    cpumem.post_write_state();
}

} // namespace snes
