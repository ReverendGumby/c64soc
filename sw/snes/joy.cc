#include <stdexcept>
#include <string.h>
#include "util/debug.h"
#include <util/string.h>
#include "ctlr.h"
#include "joy.h"

#define DBG_LVL 2
#define DBG_TAG "joy"

namespace snes {

inline joy_mode_t id_to_joy_mode(int id)
{
    if (id == 0)
    	return joy_mode_t::hid_1;
    if (id == 1)
    	return joy_mode_t::hid_2;
    return joy_mode_t::none;
}

inline const char* joy_mode_to_str(joy_mode_t m)
{
    switch (m) {
    case joy_mode_t::none:      return "none";
    case joy_mode_t::hid_1:     return "hid_1";
    case joy_mode_t::hid_2:     return "hid_2";
    default:                    break;
    }
    return "?";
}

inline joy_mode_t str_to_joy_mode(const char* s)
{
    for (int i = 0; i < static_cast<int> (joy_mode_t::joy_modes); i++) {
        auto mode = static_cast<joy_mode_t>(i);
        if (strcmp(s, joy_mode_to_str(mode)) == 0)
            return mode;
    }
    throw std::domain_error("str_to_joy_mode:: unknown mode");
}

joy::joy(ui::hid_manager& hm, class ctlr& ctlr)
    : hid(hm), ctlr(ctlr), jrpt1{}, jrpt2{}
{
    joy_dev = &hid_mgr;

    // Initial options
    static const joy_mode_t initial_modes[] = {
        [JOY1] = joy_mode_t::hid_1,
        [JOY2] = joy_mode_t::hid_2,
    };

    for (int i = 0; i < num_joys; i++) {
        _mode[i] = initial_modes[i];
    }

    hid_mgr.talker<ui::hid_manager::listener>::add_listener(this);

    joy_dev->add_listener(this);
}

joy::~joy()
{
    joy_dev->remove_listener(this);

    hid_mgr.talker<ui::hid_manager::listener>::remove_listener(this);
}

ui::joy::report* joy::jrpt(joy_mode_t mode)
{
    if (mode == joy_mode_t::hid_1)
        return &jrpt1;
    if (mode == joy_mode_t::hid_2)
        return &jrpt2;
    return nullptr;
}

//  Bit     0   1   2       3       4   5   6   7   8   9   10   11   12-15
// JOYx     B   Y   Select  Start   Up  Dn  Lt  Rt  A   X   LSh  RSh  0000

static uint16_t get_jbits(const ui::joy::report& jrpt)
{
    const int jbits[] = {
        jrpt.f1,
        jrpt.f3,
        jrpt.sl,
        jrpt.st,
        jrpt.up,
        jrpt.dn,
        jrpt.lt,
        jrpt.rt,
        jrpt.f2,
        jrpt.f4,
        jrpt.bl,
        jrpt.br,
    };
    uint16_t out = 0;
    for (unsigned b = 0; b < sizeof(jbits)/sizeof(jbits[0]); b++) {
        out |= ((!!jbits[b]) << b);
    }
    return out;
}

#if 0
static uint16_t get_jbits(const movie::input_source::report::joy& joy)
{
    const int jbits[] = {
        joy.a,
        joy.b,
        joy.sl,
        joy.st,
        joy.up,
        joy.dn,
        joy.lt,
        joy.rt,
    };
    uint16_t out = 0;
    for (unsigned b = 0; b < sizeof(jbits)/sizeof(jbits[0]); b++) {
        out |= ((!!jbits[b]) << b);
    }
    return out;
}
#endif

joy::joy_out joy::ui_reports_to_joy_out()
{
    joy_out j = {};

    uint16_t joy_1 = get_jbits(*jrpt(joy_mode_t::hid_1));
    uint16_t joy_2 = get_jbits(*jrpt(joy_mode_t::hid_2));

    j.ctlr1 |= (mode(JOY1) == joy_mode_t::hid_1)    * joy_1;
    j.ctlr1 |= (mode(JOY1) == joy_mode_t::hid_2)    * joy_2;

    j.ctlr2 |= (mode(JOY2) == joy_mode_t::hid_1)    * joy_1;
    j.ctlr2 |= (mode(JOY2) == joy_mode_t::hid_2)    * joy_2;

    return j;
}

#if 0
joy::joy_out joy::movie_inputs_to_joy_out(const movie::input_source::report& rpt)
{
    joy_out j = {};

    uint16_t joy1 = get_jbits(rpt.joy1);
    uint16_t joy2 = get_jbits(rpt.joy2);

    j.ctlr1 = joy1;
    j.ctlr2 = joy2;

    return j;
}
#endif

bool joy::is_idle()
{
    return joy_dev->is_idle();
}

void joy::set_ctlr()
{
    if (!started)
        return;

    auto m = ui_reports_to_joy_out();
    ctlr::state_t s = {
        .c1 = m.ctlr1,
        .c2 = m.ctlr2,
    };
    ctlr.set(s);
}

void joy::reset_ctlr()
{
    ctlr.reset();
}

void joy::change_port_mode(joy_port_t port, joy_mode_t new_mode)
{
    auto old_mode = mode(port);
    if (old_mode == new_mode)
        return;
    mode(port) = new_mode;
}

void joy::event(const ui::joy::report& rpt)
{
    auto m = id_to_joy_mode(rpt.id);
    auto* jr = jrpt(m);
    if (jr) {
        *jr = rpt;
        set_ctlr();
    }
    on_event();
}

std::string joy::get_joy_mode_desc_hid(int id)
{
    auto* dev = hid_mgr.get_joy_dev(id);
    if (!dev)
        return string_printf("USB joystick %d", id + 1);
    return dev->name;
}

void joy::joy_dev_changed(int id)
{
}

#if 0
void joy::event(const movie::input_source::report& rpt)
{
    auto m = movie_inputs_to_joy_out(rpt);
    ctlr::state_t s = {
        .c1 = m.ctlr1,
        .c2 = m.ctlr2,
    };
    ctlr.set(s);
}
#endif

} // namespace snes
