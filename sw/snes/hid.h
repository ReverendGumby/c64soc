#pragma once

#include "ui/hid_manager.h"

namespace snes {

class hid
{
public:
    hid(ui::hid_manager&);

    virtual void start();
    virtual void stop();

protected:
    void start_on_idle();
    void on_event();

    virtual bool is_idle() = 0;
    virtual void reset_ctlr() = 0;

    ui::hid_manager& hid_mgr;
    bool started;
    bool resume;
};

} // namespace snes
