#include "hid.h"

namespace snes {

hid::hid(ui::hid_manager& hm)
    : hid_mgr(hm)
{
    started = false;
    resume = false;
}

void hid::start()
{
    resume = true;
    reset_ctlr();
    start_on_idle();
}

void hid::stop()
{
    started = false;
    resume = false;
    reset_ctlr();
}

void hid::start_on_idle()
{
    if (is_idle()) {
        started = true;
        resume = false;
    }
}

void hid::on_event()
{
    if (resume)
        start_on_idle();
}

} // namespace snes
