#pragma once

#include <cli/command.h>

namespace snes {

class sys;

class cli
{
public:

    cli(class sys&);

private:
    int reset(const ::cli::command::args_t& args);

    int cpumem_store_display(const ::cli::command::args_t& args);

    int cpumem_dump_stats(const ::cli::command::args_t& args);

    int ppu_dump_state(const ::cli::command::args_t& args);
    int ppu_mem_display(const ::cli::command::args_t& args);

    int apu_mon_reg_dump(const ::cli::command::args_t& args);
    int apu_dsp_dump(const ::cli::command::args_t& args);
    int apu_mem_display(const ::cli::command::args_t& args);

    int print_state(const ::cli::command::args_t& args);
    int read_state(const ::cli::command::args_t& args);
    int write_state(const ::cli::command::args_t& args);
    int save_state(const ::cli::command::args_t& args);
    int load_resave_state(const ::cli::command::args_t& args);
    int load_state(const ::cli::command::args_t& args);
    int read_save_write_read_save_state(const ::cli::command::args_t& args);

    class sys& sys;
};

} // namespace snes
