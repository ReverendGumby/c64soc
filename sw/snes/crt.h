#pragma once

#include <stdint.h>
#include <stddef.h>
#include <string>
#include <vector>
#include <memory>
#include <stdexcept>

namespace snes {

struct Crt_format_error : public std::runtime_error
{
    Crt_format_error(const char *what_arg)
        : std::runtime_error {what_arg} {}
    Crt_format_error(const std::string& what_arg)
        : std::runtime_error {what_arg} {}
};

class Chip {
public:
    typedef enum {
        ROM,
        SRAM,
    } type_t;

    Chip(type_t type, uint8_t *bin, size_t len);
    Chip(const Chip &that) = default;
    Chip(Chip &&that) = default;
    virtual ~Chip() = default;
    Chip& operator=(const Chip &that) = default;

    type_t type() const { return _type; }
    bool is_ram() const;
    const char* name() const;
    uint8_t *data() const { return _data; }
    size_t len() const { return _len; }

private:
    type_t _type;
    uint8_t* _data;
    size_t _len;
};

class Crt {
public:
    using Buf = std::unique_ptr<uint8_t[]>;
	using ChipList = std::vector<Chip>;

    typedef enum {
        LOROM,
        HIROM,
        EXHIROM,
    } map_mode_t;

    Crt(Buf&& bin, size_t len);

    const ChipList &chips() const { return _chips; }
    int chip_size_kb(Chip::type_t) const;
    bool found_rom_data_spec() const { return _rom_data_spec; }
    map_mode_t map_mode() const { return _map_mode; }
    bool has_battery() const { return _battery; }

    void load_sram(Buf&& bin, size_t len);
    void save_sram(Buf& bin, size_t& len);

private:
    ChipList::iterator find_sram();

    uint8_t* find_rom_data_spec();

    Buf _bin;
    size_t _bin_len;
    ChipList _chips;
    uint8_t* _rom_data_spec;
    map_mode_t _map_mode;
    bool _battery;
    Buf _sram_bin;
};

const char* name(Crt::map_mode_t);

} // namespace snes
