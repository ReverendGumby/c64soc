#pragma once

#include <stdint.h>
#include "cpumem.h"
#include "mem.h"
#include "cart.h"
#include "ctlr.h"
#include "cpu.h"
#include "ppu.h"
#include "apu.h"
#include "joy.h"
#include "cli.h"

namespace plat {
class manager;
}

namespace ui {
class hid_manager;
}

namespace snes {

class sys
{
public:
    sys(plat::manager&, ui::hid_manager&);
    void init();

    void reset_all(bool assert);
    void reset_snes(bool assert);

    void power_cycle_snes();

    void pause();
    void resume();
    bool is_paused() const;

    struct state_t {
        cpu::state_t _cpu;
        ppu::state_t _ppu;
        cpumem::state_t _cpumem;
        apu::state_t _apu;
        cart::state_t _cart;
    };
    void read_state(state_t&);
    void write_state(const state_t&);

    plat::manager& plat_mgr;

    class cpumem cpumem;
    class mem mem;
    class cart cart;
    class ctlr ctlr;
    class cpu cpu;
    class ppu ppu;
    class apu apu;
    class joy joy;

private:
    class cli cli;

    int pause_count;
    bool reset_ram_pending;
    bool cold_reset;

    void reset_ram();
    void reset_ram_if_pending();
    void reset_ram_now();

    void set_sys_hold_req(bool);
    bool get_sys_hold_ack();
};

} // namespace snes
