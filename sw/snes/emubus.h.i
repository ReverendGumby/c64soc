#pragma once

#include <stdint.h>
#include "xparameters.h"

namespace snes {

//``PERIPHERAL EMUBUS

//////////////////////////////////////////////////////////////////////
// Bus control interface

//``BASE 32 CTRL EMUBUS_CTRL

//``REGS s/_(\w+)_\1/_\1/

// Bits for EMUBUS_CPU_MON_DATA

//``SUBREG_FIELDS CPU_MON_SEL s/SEL_(\w+)_/\1_/

// Bits for EMUBUS_CPU_CORE_MON_DATA

//``SUBREG_FIELDS CPU_CORE_MON_SEL s/SEL_(\w+)_/\1_/

// Bits for EMUBUS_ICD_MATCH register STATUS
//``FIELDS ICD_MATCH_REG_STATUS

// Bits for EMUBUS_ICD_MATCH registers D*IN, D*EN, and D*OUT
//``FIELDS ICD_MATCH_REG_D0
//``FIELDS ICD_MATCH_REG_D1
//``FIELDS ICD_MATCH_REG_D2

// Bits for EMUBUS_CPUMEM_MON_DATA

//``SUBREG_FIELDS CPUMEM_MON_SEL s/SEL_(\w+)_/\1_/

// Bits for EMUBUS_PPU_MON_DATA

//``SUBREG_FIELDS PPU_MON_SEL s/SEL_(\w+)_/\1_/

// Address span of PPU_MON_SEL_BGN* registers (eg BG2 - BG1)
#define EMUBUS_PPU_MON_SEL_BGN_SPAN EMUBUS_PPU_MON_SEL_BGNSC_NBA

// Bits for EMUBUS_APU_MON_DATA

//``SUBREG_FIELDS APU_MON_SEL s/SEL_(\w+)_/\1_/

//////////////////////////////////////////////////////////////////////
// ROM cartridge control interface

//``BASE 32 CART_CTRL
//``REGS s/(\w{2,})_\1/\1/

//////////////////////////////////////////////////////////////////////
// Direct memory interfaces

//``BASE 8 APU_MEM

#define EMUBUS_APU_MEM          (& EMUBUS_APU_MEM_REG(0))

//``BASE 8 PPU_VRAM

#define EMUBUS_PPU_VRAM         (& EMUBUS_PPU_VRAM_REG(0))

//////////////////////////////////////////////////////////////////////

} // namespace snes

// Local Variables:
// compile-command: "cd ../.. && python3 tools/gen_regs_h.py sw/snes/emubus.h.i sw/snes/emubus.h rtl/snes/emubus.v rtl/snes/icd.v rtl/snes/cart.v rtl/s-cpu/*.v rtl/w65c816s/w65c816s_core.vh rtl/snes/cpumem.v rtl/s-ppu/*.v rtl/s-smp/s_smp.v rtl/s-dsp/*.v"
// End:
