#include "emubus.h"

#include "ctlr.h"

namespace snes {

ctlr::ctlr()
{
}

void ctlr::init()
{
    reset();
}

void ctlr::reset()
{
    EMUBUS_CTLR1_MATRIX = 0;
    EMUBUS_CTLR2_MATRIX = 0;
}

void ctlr::set(const state_t& s)
{
    uint32_t hwid = 0xFF0 << 12; // normal joypad

    EMUBUS_CTLR1_MATRIX = hwid | s.c1;
    EMUBUS_CTLR2_MATRIX = hwid | s.c2;
}

ctlr::state_t ctlr::get()
{
    auto reg1 = EMUBUS_CTLR1_MATRIX;
    auto reg2 = EMUBUS_CTLR2_MATRIX;
    state_t s = {
        .c1 = uint16_t(reg1),
        .c2 = uint16_t(reg2),
    };
    return s;
}

} // namespace snes
