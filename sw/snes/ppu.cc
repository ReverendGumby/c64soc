#include "ppu.h"

#include "emubus.h"
#include "memptr.h"

#include <util/membuf.h>

#include <unistd.h>
#include <string.h>

namespace snes {

void ppu::init()
{
}

uint8_t ppu::read_reg8(uint32_t sel)
{
    EMUBUS_PPU_MON_SEL = sel;
    uint8_t val = *reinterpret_cast<volatile uint8_t*>(&EMUBUS_PPU_MON_DATA);
    return val;
}

uint16_t ppu::read_reg16(uint32_t sel)
{
    EMUBUS_PPU_MON_SEL = sel;
    uint16_t val = *reinterpret_cast<volatile uint16_t*>(&EMUBUS_PPU_MON_DATA);
    return val;
}

uint32_t ppu::read_reg(uint32_t sel)
{
    EMUBUS_PPU_MON_SEL = sel;
    uint32_t val = EMUBUS_PPU_MON_DATA;
    return val;
}

template<size_t Len>
std::array<uint8_t, Len> ppu::read_array8(uint32_t sel)
{
    std::array<uint8_t, Len> ret;
    for (size_t i = 0; i < Len; i++)
        ret[i] = read_reg(sel + i) >> ((sel & 3) * 8);
    return ret;
}

template<size_t Len>
std::array<uint16_t, Len> ppu::read_array16(uint32_t sel)
{
    std::array<uint16_t, Len> ret;
    for (size_t i = 0; i < Len; i++)
        ret[i] = read_reg(sel + (i * 2)) >> ((sel & 1) * 16);
    return ret;
}

ppu_regs ppu::read_regs(int which)
{
    ppu_regs ret;

    if (which & regs_io) {
        uint32_t reg;
        
        reg = read_reg(EMUBUS_PPU_MON_SEL_REN0);
        ret.inidisp.bright = (reg & EMUBUS_PPU_MON_REN0_INIDISP_BRIGHT) >> EMUBUS_PPU_MON_REN0_INIDISP_BRIGHT_S;
        ret.inidisp.forced_blank = (reg & EMUBUS_PPU_MON_REN0_INIDISP_FORCED_BLANK) != 0;
        ret.bgmode.mode = (reg & EMUBUS_PPU_MON_REN0_BGMODE) >> EMUBUS_PPU_MON_REN0_BGMODE_S;
        ret.bgmode.bg3_pri = (reg & EMUBUS_PPU_MON_REN0_BGMODE_BG3_PRI) != 0;
        ret.tm.bg1 = (((reg & EMUBUS_PPU_MON_REN0_TM) >> EMUBUS_PPU_MON_REN0_TM_S) & (1<<0)) != 0;
        ret.tm.bg2 = (((reg & EMUBUS_PPU_MON_REN0_TM) >> EMUBUS_PPU_MON_REN0_TM_S) & (1<<1)) != 0;
        ret.tm.bg3 = (((reg & EMUBUS_PPU_MON_REN0_TM) >> EMUBUS_PPU_MON_REN0_TM_S) & (1<<2)) != 0;
        ret.tm.bg4 = (((reg & EMUBUS_PPU_MON_REN0_TM) >> EMUBUS_PPU_MON_REN0_TM_S) & (1<<3)) != 0;
        ret.tm.obj = (((reg & EMUBUS_PPU_MON_REN0_TM) >> EMUBUS_PPU_MON_REN0_TM_S) & (1<<4)) != 0;
        ret.tmw.bg1 = (((reg & EMUBUS_PPU_MON_REN0_TMW) >> EMUBUS_PPU_MON_REN0_TMW_S) & (1<<0)) != 0;
        ret.tmw.bg2 = (((reg & EMUBUS_PPU_MON_REN0_TMW) >> EMUBUS_PPU_MON_REN0_TMW_S) & (1<<1)) != 0;
        ret.tmw.bg3 = (((reg & EMUBUS_PPU_MON_REN0_TMW) >> EMUBUS_PPU_MON_REN0_TMW_S) & (1<<2)) != 0;
        ret.tmw.bg4 = (((reg & EMUBUS_PPU_MON_REN0_TMW) >> EMUBUS_PPU_MON_REN0_TMW_S) & (1<<3)) != 0;
        ret.tmw.obj = (((reg & EMUBUS_PPU_MON_REN0_TMW) >> EMUBUS_PPU_MON_REN0_TMW_S) & (1<<4)) != 0;
        ret.ts.bg1 = (((reg & EMUBUS_PPU_MON_REN0_TS) >> EMUBUS_PPU_MON_REN0_TS_S) & (1<<0)) != 0;
        ret.ts.bg2 = (((reg & EMUBUS_PPU_MON_REN0_TS) >> EMUBUS_PPU_MON_REN0_TS_S) & (1<<1)) != 0;
        ret.ts.bg3 = (((reg & EMUBUS_PPU_MON_REN0_TS) >> EMUBUS_PPU_MON_REN0_TS_S) & (1<<2)) != 0;
        ret.ts.bg4 = (((reg & EMUBUS_PPU_MON_REN0_TS) >> EMUBUS_PPU_MON_REN0_TS_S) & (1<<3)) != 0;
        ret.ts.obj = (((reg & EMUBUS_PPU_MON_REN0_TS) >> EMUBUS_PPU_MON_REN0_TS_S) & (1<<4)) != 0;
        ret.tsw.bg1 = (((reg & EMUBUS_PPU_MON_REN0_TSW) >> EMUBUS_PPU_MON_REN0_TSW_S) & (1<<0)) != 0;
        ret.tsw.bg2 = (((reg & EMUBUS_PPU_MON_REN0_TSW) >> EMUBUS_PPU_MON_REN0_TSW_S) & (1<<1)) != 0;
        ret.tsw.bg3 = (((reg & EMUBUS_PPU_MON_REN0_TSW) >> EMUBUS_PPU_MON_REN0_TSW_S) & (1<<2)) != 0;
        ret.tsw.bg4 = (((reg & EMUBUS_PPU_MON_REN0_TSW) >> EMUBUS_PPU_MON_REN0_TSW_S) & (1<<3)) != 0;
        ret.tsw.obj = (((reg & EMUBUS_PPU_MON_REN0_TSW) >> EMUBUS_PPU_MON_REN0_TSW_S) & (1<<4)) != 0;

        reg = read_reg(EMUBUS_PPU_MON_SEL_OPHVC);
        ret.ophct = (reg & EMUBUS_PPU_MON_OPHVC_OPHCT) >> EMUBUS_PPU_MON_OPHVC_OPHCT_S;
        ret.opvct = (reg & EMUBUS_PPU_MON_OPHVC_OPVCT) >> EMUBUS_PPU_MON_OPHVC_OPVCT_S;

        reg = read_reg(EMUBUS_PPU_MON_SEL_STAT7X);
        ret.stat77.range_over = (reg & EMUBUS_PPU_MON_STAT7X_RANGE_OVER) != 0;
        ret.stat77.time_over = (reg & EMUBUS_PPU_MON_STAT7X_TIME_OVER) != 0;
        ret.stat78.opl = (reg & EMUBUS_PPU_MON_STAT7X_OPL) != 0;
        ret.stat78.field = (reg & EMUBUS_PPU_MON_STAT7X_FIELD) != 0;

        for (int bg = 1; bg <= 4; bg++) {
            uint32_t roff = EMUBUS_PPU_MON_SEL_BGN_SPAN * (bg - 1);
            reg = read_reg(EMUBUS_PPU_MON_SEL_BGNSC_NBA + roff);
            auto scs = (reg & EMUBUS_PPU_MON_BGNSC_NBA_SCS) >> EMUBUS_PPU_MON_BGNSC_NBA_SCS_S;
            auto scba = (reg & EMUBUS_PPU_MON_BGNSC_NBA_SCBA) >> EMUBUS_PPU_MON_BGNSC_NBA_SCBA_S;
            auto nba = (reg & EMUBUS_PPU_MON_BGNSC_NBA_NBA) >> EMUBUS_PPU_MON_BGNSC_NBA_NBA_S;
            auto hofs = read_reg(EMUBUS_PPU_MON_SEL_BGNHOFS + roff);
            auto vofs = read_reg(EMUBUS_PPU_MON_SEL_BGNVOFS + roff);
            switch (bg) {
            case 1:
                ret.bg1sc.size = scs;
                ret.bg1sc.base = scba;
                ret.bg12nba.bg1nba = nba;
                ret.bg1hofs = hofs;
                ret.bg1vofs = vofs;
                break;
            case 2:
                ret.bg2sc.size = scs;
                ret.bg2sc.base = scba;
                ret.bg12nba.bg2nba = nba;
                ret.bg2hofs = hofs;
                ret.bg2vofs = vofs;
                break;
            case 3:
                ret.bg3sc.size = scs;
                ret.bg3sc.base = scba;
                ret.bg34nba.bg3nba = nba;
                ret.bg3hofs = hofs;
                ret.bg3vofs = vofs;
                break;
            case 4:
                ret.bg4sc.size = scs;
                ret.bg4sc.base = scba;
                ret.bg34nba.bg4nba = nba;
                ret.bg4hofs = hofs;
                ret.bg4vofs = vofs;
                break;
            }
        }

        ret.m7hofs = read_reg(EMUBUS_PPU_MON_SEL_M7HOFS);
        ret.m7vofs = read_reg(EMUBUS_PPU_MON_SEL_M7VOFS);
        reg = read_reg(EMUBUS_PPU_MON_SEL_M7SEL);
        ret.m7sel.hflip = (reg & EMUBUS_PPU_MON_M7SEL_HFLIP) != 0;
        ret.m7sel.vflip = (reg & EMUBUS_PPU_MON_M7SEL_VFLIP) != 0;
        ret.m7sel.scover = (reg & EMUBUS_PPU_MON_M7SEL_SCOVER) >> EMUBUS_PPU_MON_M7SEL_SCOVER_S;
        ret.m7a = read_reg(EMUBUS_PPU_MON_SEL_M7A);
        ret.m7b = read_reg(EMUBUS_PPU_MON_SEL_M7B);
        ret.m7c = read_reg(EMUBUS_PPU_MON_SEL_M7C);
        ret.m7d = read_reg(EMUBUS_PPU_MON_SEL_M7D);
        ret.m7x = read_reg(EMUBUS_PPU_MON_SEL_M7X);
        ret.m7y = read_reg(EMUBUS_PPU_MON_SEL_M7Y);
        reg = read_reg(EMUBUS_PPU_MON_SEL_M7MPYC);
        ret.mpylmh = reg >> 3; // MPYL/M/H = mpyc[26:3]

        uint8_t wxsel;
        reg = read_reg(EMUBUS_PPU_MON_SEL_WIN0);
        wxsel = (reg & EMUBUS_PPU_MON_WIN0_W1SEL_INV) >> EMUBUS_PPU_MON_WIN0_W1SEL_INV_S;
        ret.w12sel.w1_inv_bg1 = (wxsel & (1 << 0)) != 0;
        ret.w12sel.w1_inv_bg2 = (wxsel & (1 << 1)) != 0;
        ret.w34sel.w1_inv_bg3 = (wxsel & (1 << 2)) != 0;
        ret.w34sel.w1_inv_bg4 = (wxsel & (1 << 3)) != 0;
        ret.wobjsel.w1_inv_obj = (wxsel & (1 << 4)) != 0;
        ret.wobjsel.w1_inv_clr = (wxsel & (1 << 5)) != 0;
        wxsel = (reg & EMUBUS_PPU_MON_WIN0_W1SEL_ENA) >> EMUBUS_PPU_MON_WIN0_W1SEL_ENA_S;
        ret.w12sel.w1_ena_bg1 = (wxsel & (1 << 0)) != 0;
        ret.w12sel.w1_ena_bg2 = (wxsel & (1 << 1)) != 0;
        ret.w34sel.w1_ena_bg3 = (wxsel & (1 << 2)) != 0;
        ret.w34sel.w1_ena_bg4 = (wxsel & (1 << 3)) != 0;
        ret.wobjsel.w1_ena_obj = (wxsel & (1 << 4)) != 0;
        ret.wobjsel.w1_ena_clr = (wxsel & (1 << 5)) != 0;
        wxsel = (reg & EMUBUS_PPU_MON_WIN0_W2SEL_INV) >> EMUBUS_PPU_MON_WIN0_W2SEL_INV_S;
        ret.w12sel.w2_inv_bg1 = (wxsel & (1 << 0)) != 0;
        ret.w12sel.w2_inv_bg2 = (wxsel & (1 << 1)) != 0;
        ret.w34sel.w2_inv_bg3 = (wxsel & (1 << 2)) != 0;
        ret.w34sel.w2_inv_bg4 = (wxsel & (1 << 3)) != 0;
        ret.wobjsel.w2_inv_obj = (wxsel & (1 << 4)) != 0;
        ret.wobjsel.w2_inv_clr = (wxsel & (1 << 5)) != 0;
        wxsel = (reg & EMUBUS_PPU_MON_WIN0_W2SEL_ENA) >> EMUBUS_PPU_MON_WIN0_W2SEL_ENA_S;
        ret.w12sel.w2_ena_bg1 = (wxsel & (1 << 0)) != 0;
        ret.w12sel.w2_ena_bg2 = (wxsel & (1 << 1)) != 0;
        ret.w34sel.w2_ena_bg3 = (wxsel & (1 << 2)) != 0;
        ret.w34sel.w2_ena_bg4 = (wxsel & (1 << 3)) != 0;
        ret.wobjsel.w2_ena_obj = (wxsel & (1 << 4)) != 0;
        ret.wobjsel.w2_ena_clr = (wxsel & (1 << 5)) != 0;
        ret.wh0 = (reg & EMUBUS_PPU_MON_WIN0_W1LPOS) >> EMUBUS_PPU_MON_WIN0_W1LPOS_S;
        reg = read_reg(EMUBUS_PPU_MON_SEL_WIN1);
        ret.wh1 = (reg & EMUBUS_PPU_MON_WIN1_W1RPOS) >> EMUBUS_PPU_MON_WIN1_W1RPOS_S;
        ret.wh2 = (reg & EMUBUS_PPU_MON_WIN1_W2LPOS) >> EMUBUS_PPU_MON_WIN1_W2LPOS_S;
        ret.wh3 = (reg & EMUBUS_PPU_MON_WIN1_W2RPOS) >> EMUBUS_PPU_MON_WIN1_W2RPOS_S;
        ret.wbglog.bg1 = static_cast<ppu_regs::wmlog_t>((reg & EMUBUS_PPU_MON_WIN1_WMLOG0) >> EMUBUS_PPU_MON_WIN1_WMLOG0_S);
        ret.wbglog.bg2 = static_cast<ppu_regs::wmlog_t>((reg & EMUBUS_PPU_MON_WIN1_WMLOG1) >> EMUBUS_PPU_MON_WIN1_WMLOG1_S);
        ret.wbglog.bg3 = static_cast<ppu_regs::wmlog_t>((reg & EMUBUS_PPU_MON_WIN1_WMLOG2) >> EMUBUS_PPU_MON_WIN1_WMLOG2_S);
        ret.wbglog.bg4 = static_cast<ppu_regs::wmlog_t>((reg & EMUBUS_PPU_MON_WIN1_WMLOG3) >> EMUBUS_PPU_MON_WIN1_WMLOG3_S);
        reg = read_reg(EMUBUS_PPU_MON_SEL_WIN2);
        ret.wobjlog.obj = static_cast<ppu_regs::wmlog_t>((reg & EMUBUS_PPU_MON_WIN2_WMLOG4) >> EMUBUS_PPU_MON_WIN2_WMLOG4_S);
        ret.wobjlog.clr = static_cast<ppu_regs::wmlog_t>((reg & EMUBUS_PPU_MON_WIN2_WMLOG5) >> EMUBUS_PPU_MON_WIN2_WMLOG5_S);

        reg = read_reg(EMUBUS_PPU_MON_SEL_SPR_REN0);
        reg = (reg & EMUBUS_PPU_MON_SPR_REN0_OBJSEL) >> EMUBUS_PPU_MON_SPR_REN0_OBJSEL_S;
        ret.objsel.ba = (reg >> 0) & 7;
        ret.objsel.n = (reg >> 3) & 3;
        ret.objsel.ss = (reg >> 5) & 7;

        reg = read_reg(EMUBUS_PPU_MON_SEL_REN1);
        ret.cgwsel.direct = (reg & EMUBUS_PPU_MON_REN1_CLR_DIRECT) != 0;
        ret.cgwsel.addend = (reg & EMUBUS_PPU_MON_REN1_CLR_ADD_SUB) != 0;
        ret.cgwsel.sub_win_rgn = (reg & EMUBUS_PPU_MON_REN1_CLR_SUB_WIN_RGN) >> EMUBUS_PPU_MON_REN1_CLR_SUB_WIN_RGN_S;
        ret.cgwsel.main_win_rgn = (reg & EMUBUS_PPU_MON_REN1_CLR_MAIN_WIN_RGN) >> EMUBUS_PPU_MON_REN1_CLR_MAIN_WIN_RGN_S;
        ret.cgadsub.bg1 = (((reg & EMUBUS_PPU_MON_REN1_CLR_MATH_EN) >> EMUBUS_PPU_MON_REN1_CLR_MATH_EN_S) & (1 << 0)) != 0;
        ret.cgadsub.bg2 = (((reg & EMUBUS_PPU_MON_REN1_CLR_MATH_EN) >> EMUBUS_PPU_MON_REN1_CLR_MATH_EN_S) & (1 << 1)) != 0;
        ret.cgadsub.bg3 = (((reg & EMUBUS_PPU_MON_REN1_CLR_MATH_EN) >> EMUBUS_PPU_MON_REN1_CLR_MATH_EN_S) & (1 << 2)) != 0;
        ret.cgadsub.bg4 = (((reg & EMUBUS_PPU_MON_REN1_CLR_MATH_EN) >> EMUBUS_PPU_MON_REN1_CLR_MATH_EN_S) & (1 << 3)) != 0;
        ret.cgadsub.obj = (((reg & EMUBUS_PPU_MON_REN1_CLR_MATH_EN) >> EMUBUS_PPU_MON_REN1_CLR_MATH_EN_S) & (1 << 4)) != 0;
        ret.cgadsub.back = (((reg & EMUBUS_PPU_MON_REN1_CLR_MATH_EN) >> EMUBUS_PPU_MON_REN1_CLR_MATH_EN_S) & (1 << 5)) != 0;
        ret.cgadsub.half = (reg & EMUBUS_PPU_MON_REN1_CLR_HALF) != 0;
        ret.cgadsub.sub = (reg & EMUBUS_PPU_MON_REN1_CLR_OP_SUB) != 0;
        reg = (reg & EMUBUS_PPU_MON_REN1_CLR_FIXED) >> EMUBUS_PPU_MON_REN1_CLR_FIXED_S;
        ret.coldata.r = (reg >> 0) & 31;
        ret.coldata.g = (reg >> 5) & 31;
        ret.coldata.b = (reg >> 10) & 31;

        reg = read_reg(EMUBUS_PPU_MON_SEL_REN2);
        ret.mosaic.sz = ((reg & EMUBUS_PPU_MON_REN2_MOSAIC_SZ) >> EMUBUS_PPU_MON_REN2_MOSAIC_SZ_S);
        ret.mosaic.bg1 = (((reg & EMUBUS_PPU_MON_REN2_MOSAIC_BKG_EN) >> EMUBUS_PPU_MON_REN2_MOSAIC_BKG_EN_S) & (1 << 0)) != 0;
        ret.mosaic.bg2 = (((reg & EMUBUS_PPU_MON_REN2_MOSAIC_BKG_EN) >> EMUBUS_PPU_MON_REN2_MOSAIC_BKG_EN_S) & (1 << 1)) != 0;
        ret.mosaic.bg3 = (((reg & EMUBUS_PPU_MON_REN2_MOSAIC_BKG_EN) >> EMUBUS_PPU_MON_REN2_MOSAIC_BKG_EN_S) & (1 << 2)) != 0;
        ret.mosaic.bg4 = (((reg & EMUBUS_PPU_MON_REN2_MOSAIC_BKG_EN) >> EMUBUS_PPU_MON_REN2_MOSAIC_BKG_EN_S) & (1 << 3)) != 0;
    }

    if (which & regs_video_counter) {
        auto rowcol = read_reg(EMUBUS_PPU_MON_SEL_ROWCOL);
        auto frame = read_reg(EMUBUS_PPU_MON_SEL_FRAME);

        ret.video_counter.row = (rowcol & EMUBUS_PPU_MON_ROWCOL_ROW) >> EMUBUS_PPU_MON_ROWCOL_ROW_S;
        ret.video_counter.col = (rowcol & EMUBUS_PPU_MON_ROWCOL_COL) >> EMUBUS_PPU_MON_ROWCOL_COL_S;
        ret.video_counter.frame = frame;
    }

    if (which & regs_bus_master) {
        for (int bg = 1; bg <= 4; bg++) {
            uint32_t roff = EMUBUS_PPU_MON_SEL_BGN_SPAN * (bg - 1);
            auto vab = read_reg(EMUBUS_PPU_MON_SEL_VAB + roff);
            auto vac = read_reg(EMUBUS_PPU_MON_SEL_VAC + roff);
            auto vas = read_reg(EMUBUS_PPU_MON_SEL_VAS + roff);

            ret.bus_master.bg[bg - 1].vab = vab;
            ret.bus_master.bg[bg - 1].vac = vac;
            ret.bus_master.bg[bg - 1].vas = vas;
        }

        ret.bus_master.m7.mpya = read_reg(EMUBUS_PPU_MON_SEL_M7MPYA);
        ret.bus_master.m7.mpyb = read_reg(EMUBUS_PPU_MON_SEL_M7MPYB);
        ret.bus_master.m7.mpyc = read_reg(EMUBUS_PPU_MON_SEL_M7MPYC);
        ret.bus_master.m7.vas = read_reg(EMUBUS_PPU_MON_SEL_M7VAS);
    }

    if (which & regs_palette) {
        auto pal = read_array16<EMUBUS_PPU_MON_SEL_CGRAM_LEN / 2>(EMUBUS_PPU_MON_SEL_CGRAM);
        memcpy(ret.cgram, pal.data(), sizeof(ret.cgram));
    }

    if (which & regs_oam) {
        auto oaml = read_array16<EMUBUS_PPU_MON_SEL_OAML_LEN / 2>(EMUBUS_PPU_MON_SEL_OAML);
        auto oamh = read_array8<EMUBUS_PPU_MON_SEL_OAMH_LEN>(EMUBUS_PPU_MON_SEL_OAMH);

        memcpy(ret.oaml, oaml.data(), sizeof(ret.oaml));
        memcpy(ret.oamh, oamh.data(), sizeof(ret.oamh));
    }

    return ret;
}

void ppu::read_state(state_t& st)
{
    st.regs = read_regs();

    membuf<memptr> vram_buf(memptr(0x0000, memtype::ppu),
                            memptr(0xFFFF, memtype::ppu));
    vram_buf.fetch();
    st.vram = unique_buf(vram_buf.take_buffer());
}

void ppu::write_reg8(uint32_t sel, uint8_t val)
{
    EMUBUS_PPU_MON_SEL = sel;
    *reinterpret_cast<volatile uint8_t*>(&EMUBUS_PPU_MON_DATA) = val;
}

void ppu::write_reg16(uint32_t sel, uint16_t val)
{
    EMUBUS_PPU_MON_SEL = sel;
    *reinterpret_cast<volatile uint16_t*>(&EMUBUS_PPU_MON_DATA) = val;
}

void ppu::write_reg(uint32_t sel, uint32_t val)
{
    EMUBUS_PPU_MON_SEL = sel;
    EMUBUS_PPU_MON_DATA = val;
}

template<size_t Len>
void ppu::write_array8(uint32_t sel, const uint8_t array[Len])
{
    for (size_t i = 0; i < Len; i++)
        write_reg8(sel + i, static_cast<uint32_t>(array[i] << ((sel & 3) * 8)));
}

template<size_t Len>
void ppu::write_array16(uint32_t sel, const uint16_t array[Len])
{
    for (size_t i = 0; i < Len; i++)
        write_reg16(sel + (i * 2),
                  static_cast<uint32_t>(array[i] << ((sel & 1) * 16)));
}

void ppu::write_state(const state_t& st)
{
    write_regs(st.regs);

    membuf<memptr> vram_buf(memptr(0x0000, memtype::ppu),
                            memptr(0xFFFF, memtype::ppu),
                            st.vram);
    vram_buf.store();
}

#define INSERT1(reg, name, field)   \
    reg = (reg & ~name) | ((field) ? name : 0)
#define INSERTN(reg, name, field)   \
    reg = (reg & ~name) | ((static_cast<decltype(reg)>(field) << name##_S) & name)

void ppu::write_regs(const ppu_regs& regs, int which)
{
    uint32_t reg;

    if (which & regs_io) {
        reg = read_reg(EMUBUS_PPU_MON_SEL_REN0);
        INSERTN(reg, EMUBUS_PPU_MON_REN0_INIDISP_BRIGHT, regs.inidisp.bright);
        INSERT1(reg, EMUBUS_PPU_MON_REN0_INIDISP_FORCED_BLANK, regs.inidisp.forced_blank);
        INSERTN(reg, EMUBUS_PPU_MON_REN0_BGMODE, regs.bgmode.mode);
        INSERT1(reg, EMUBUS_PPU_MON_REN0_BGMODE_BG3_PRI, regs.bgmode.bg3_pri);
        INSERTN(reg, EMUBUS_PPU_MON_REN0_TM,
                ((regs.tm.bg1 << 0) |
                 (regs.tm.bg2 << 1) |
                 (regs.tm.bg3 << 2) |
                 (regs.tm.bg4 << 3) |
                 (regs.tm.obj << 4)));
        INSERTN(reg, EMUBUS_PPU_MON_REN0_TMW,
                ((regs.tmw.bg1 << 0) |
                 (regs.tmw.bg2 << 1) |
                 (regs.tmw.bg3 << 2) |
                 (regs.tmw.bg4 << 3) |
                 (regs.tmw.obj << 4)));
        INSERTN(reg, EMUBUS_PPU_MON_REN0_TS,
                ((regs.ts.bg1 << 0) |
                 (regs.ts.bg2 << 1) |
                 (regs.ts.bg3 << 2) |
                 (regs.ts.bg4 << 3) |
                 (regs.ts.obj << 4)));
        INSERTN(reg, EMUBUS_PPU_MON_REN0_TSW,
                ((regs.tsw.bg1 << 0) |
                 (regs.tsw.bg2 << 1) |
                 (regs.tsw.bg3 << 2) |
                 (regs.tsw.bg4 << 3) |
                 (regs.tsw.obj << 4)));
        write_reg(EMUBUS_PPU_MON_SEL_REN0, reg);

        reg = read_reg(EMUBUS_PPU_MON_SEL_OPHVC);
        INSERTN(reg, EMUBUS_PPU_MON_OPHVC_OPHCT, regs.ophct);
        INSERTN(reg, EMUBUS_PPU_MON_OPHVC_OPVCT, regs.opvct);
        write_reg(EMUBUS_PPU_MON_SEL_OPHVC, reg);

        reg = read_reg(EMUBUS_PPU_MON_SEL_STAT7X);
        INSERT1(reg, EMUBUS_PPU_MON_STAT7X_RANGE_OVER, regs.stat77.range_over);
        INSERT1(reg, EMUBUS_PPU_MON_STAT7X_TIME_OVER, regs.stat77.time_over);
        INSERT1(reg, EMUBUS_PPU_MON_STAT7X_OPL, regs.stat78.opl);
        INSERT1(reg, EMUBUS_PPU_MON_STAT7X_FIELD, regs.stat78.field);
        write_reg(EMUBUS_PPU_MON_SEL_STAT7X, reg);

        for (int bg = 1; bg <= 4; bg++) {
            uint32_t roff = EMUBUS_PPU_MON_SEL_BGN_SPAN * (bg - 1);
            uint32_t scs, scba, nba, hofs, vofs;
            switch (bg) {
            case 1:
                scs = regs.bg1sc.size;
                scba = regs.bg1sc.base;
                nba = regs.bg12nba.bg1nba;
                hofs = regs.bg1hofs;
                vofs = regs.bg1vofs;
                break;
            case 2:
                scs = regs.bg2sc.size;
                scba = regs.bg2sc.base;
                nba = regs.bg12nba.bg2nba;
                hofs = regs.bg2hofs;
                vofs = regs.bg2vofs;
                break;
            case 3:
                scs = regs.bg3sc.size;
                scba = regs.bg3sc.base;
                nba = regs.bg34nba.bg3nba;
                hofs = regs.bg3hofs;
                vofs = regs.bg3vofs;
                break;
            case 4:
                scs = regs.bg4sc.size;
                scba = regs.bg4sc.base;
                nba = regs.bg34nba.bg4nba;
                hofs = regs.bg4hofs;
                vofs = regs.bg4vofs;
                break;
            }
            reg = read_reg(EMUBUS_PPU_MON_SEL_BGNSC_NBA + roff);
            INSERTN(reg, EMUBUS_PPU_MON_BGNSC_NBA_SCS, scs);
            INSERTN(reg, EMUBUS_PPU_MON_BGNSC_NBA_SCBA, scba);
            INSERTN(reg, EMUBUS_PPU_MON_BGNSC_NBA_NBA, nba);
            write_reg(EMUBUS_PPU_MON_SEL_BGNSC_NBA + roff, reg);
            write_reg(EMUBUS_PPU_MON_SEL_BGNHOFS + roff, hofs);
            write_reg(EMUBUS_PPU_MON_SEL_BGNVOFS + roff, vofs);
        }

        write_reg(EMUBUS_PPU_MON_SEL_M7HOFS, regs.m7hofs);
        write_reg(EMUBUS_PPU_MON_SEL_M7VOFS, regs.m7vofs);
        reg = read_reg(EMUBUS_PPU_MON_SEL_M7SEL);
        INSERT1(reg, EMUBUS_PPU_MON_M7SEL_HFLIP, regs.m7sel.hflip);
        INSERT1(reg, EMUBUS_PPU_MON_M7SEL_VFLIP, regs.m7sel.vflip);
        INSERTN(reg, EMUBUS_PPU_MON_M7SEL_SCOVER, regs.m7sel.scover);
        write_reg(EMUBUS_PPU_MON_SEL_M7SEL, reg);
        write_reg(EMUBUS_PPU_MON_SEL_M7A, regs.m7a);
        write_reg(EMUBUS_PPU_MON_SEL_M7B, regs.m7b);
        write_reg(EMUBUS_PPU_MON_SEL_M7C, regs.m7c);
        write_reg(EMUBUS_PPU_MON_SEL_M7D, regs.m7d);
        write_reg(EMUBUS_PPU_MON_SEL_M7X, regs.m7x);
        write_reg(EMUBUS_PPU_MON_SEL_M7Y, regs.m7y);
        //write_reg(EMUBUS_PPU_MON_SEL_M7MPYC, regs.mpylmh); // Handled in regs_bus_master

        uint8_t wxsel;
        reg = read_reg(EMUBUS_PPU_MON_SEL_WIN0);
        wxsel =
            (regs.w12sel.w1_inv_bg1 << 0) |
            (regs.w12sel.w1_inv_bg2 << 1) |
            (regs.w34sel.w1_inv_bg3 << 2) |
            (regs.w34sel.w1_inv_bg4 << 3) |
            (regs.wobjsel.w1_inv_obj << 4) |
            (regs.wobjsel.w1_inv_clr << 5);
        INSERTN(reg, EMUBUS_PPU_MON_WIN0_W1SEL_INV, wxsel);
        wxsel =
            (regs.w12sel.w1_ena_bg1 << 0) |
            (regs.w12sel.w1_ena_bg2 << 1) |
            (regs.w34sel.w1_ena_bg3 << 2) |
            (regs.w34sel.w1_ena_bg4 << 3) |
            (regs.wobjsel.w1_ena_obj << 4) |
            (regs.wobjsel.w1_ena_clr << 5);
        INSERTN(reg, EMUBUS_PPU_MON_WIN0_W1SEL_ENA, wxsel);
        wxsel =
            (regs.w12sel.w2_inv_bg1 << 0) |
            (regs.w12sel.w2_inv_bg2 << 1) |
            (regs.w34sel.w2_inv_bg3 << 2) |
            (regs.w34sel.w2_inv_bg4 << 3) |
            (regs.wobjsel.w2_inv_obj << 4) |
            (regs.wobjsel.w2_inv_clr << 5);
        INSERTN(reg, EMUBUS_PPU_MON_WIN0_W2SEL_INV, wxsel);
        wxsel =
            (regs.w12sel.w2_ena_bg1 << 0) |
            (regs.w12sel.w2_ena_bg2 << 1) |
            (regs.w34sel.w2_ena_bg3 << 2) |
            (regs.w34sel.w2_ena_bg4 << 3) |
            (regs.wobjsel.w2_ena_obj << 4) |
            (regs.wobjsel.w2_ena_clr << 5);
        INSERTN(reg, EMUBUS_PPU_MON_WIN0_W2SEL_ENA, wxsel);
        INSERTN(reg, EMUBUS_PPU_MON_WIN0_W1LPOS, regs.wh0);
        write_reg(EMUBUS_PPU_MON_SEL_WIN0, reg);

        reg = read_reg(EMUBUS_PPU_MON_SEL_WIN1);
        INSERTN(reg, EMUBUS_PPU_MON_WIN1_W1RPOS, regs.wh1);
        INSERTN(reg, EMUBUS_PPU_MON_WIN1_W2LPOS, regs.wh2);
        INSERTN(reg, EMUBUS_PPU_MON_WIN1_W2RPOS, regs.wh3);
        INSERTN(reg, EMUBUS_PPU_MON_WIN1_WMLOG0, regs.wbglog.bg1);
        INSERTN(reg, EMUBUS_PPU_MON_WIN1_WMLOG1, regs.wbglog.bg2);
        INSERTN(reg, EMUBUS_PPU_MON_WIN1_WMLOG2, regs.wbglog.bg3);
        INSERTN(reg, EMUBUS_PPU_MON_WIN1_WMLOG3, regs.wbglog.bg4);
        write_reg(EMUBUS_PPU_MON_SEL_WIN1, reg);

        reg = read_reg(EMUBUS_PPU_MON_SEL_WIN2);
        INSERTN(reg, EMUBUS_PPU_MON_WIN2_WMLOG4, regs.wobjlog.obj);
        INSERTN(reg, EMUBUS_PPU_MON_WIN2_WMLOG5, regs.wobjlog.clr);
        write_reg(EMUBUS_PPU_MON_SEL_WIN2, reg);

        reg = read_reg(EMUBUS_PPU_MON_SEL_SPR_REN0);
        INSERTN(reg, EMUBUS_PPU_MON_SPR_REN0_OBJSEL,
                ((regs.objsel.ba) << 0) |
                ((regs.objsel.n) << 3) |
                ((regs.objsel.ss) << 5));
        write_reg(EMUBUS_PPU_MON_SEL_SPR_REN0, reg);

        reg = read_reg(EMUBUS_PPU_MON_SEL_REN1);
        INSERT1(reg, EMUBUS_PPU_MON_REN1_CLR_DIRECT, regs.cgwsel.direct);
        INSERT1(reg, EMUBUS_PPU_MON_REN1_CLR_ADD_SUB, regs.cgwsel.addend);
        INSERTN(reg, EMUBUS_PPU_MON_REN1_CLR_SUB_WIN_RGN, regs.cgwsel.sub_win_rgn);
        INSERTN(reg, EMUBUS_PPU_MON_REN1_CLR_MAIN_WIN_RGN, regs.cgwsel.main_win_rgn);
        INSERTN(reg, EMUBUS_PPU_MON_REN1_CLR_MATH_EN,
                (regs.cgadsub.bg1 << 0) |
                (regs.cgadsub.bg2 << 1) |
                (regs.cgadsub.bg3 << 2) |
                (regs.cgadsub.bg4 << 3) |
                (regs.cgadsub.obj << 4) |
                (regs.cgadsub.back << 5));
        INSERT1(reg, EMUBUS_PPU_MON_REN1_CLR_HALF, regs.cgadsub.half);
        INSERT1(reg, EMUBUS_PPU_MON_REN1_CLR_OP_SUB, regs.cgadsub.sub);
        INSERTN(reg, EMUBUS_PPU_MON_REN1_CLR_FIXED,
                (regs.coldata.r << 0) |
                (regs.coldata.g << 5) |
                (regs.coldata.b << 10));
        write_reg(EMUBUS_PPU_MON_SEL_REN1, reg);

        reg = read_reg(EMUBUS_PPU_MON_SEL_REN2);
        INSERTN(reg, EMUBUS_PPU_MON_REN2_MOSAIC_SZ, regs.mosaic.sz);
        INSERT1(reg, EMUBUS_PPU_MON_REN2_MOSAIC_BKG_EN, regs.mosaic.bg1);
        INSERT1(reg, EMUBUS_PPU_MON_REN2_MOSAIC_BKG_EN, regs.mosaic.bg2);
        INSERT1(reg, EMUBUS_PPU_MON_REN2_MOSAIC_BKG_EN, regs.mosaic.bg3);
        INSERT1(reg, EMUBUS_PPU_MON_REN2_MOSAIC_BKG_EN, regs.mosaic.bg4);
        write_reg(EMUBUS_PPU_MON_SEL_REN2, reg);
    }

    if (which & regs_video_counter) {
        reg = read_reg(EMUBUS_PPU_MON_SEL_ROWCOL);
        INSERTN(reg, EMUBUS_PPU_MON_ROWCOL_ROW, regs.video_counter.row);
        INSERTN(reg, EMUBUS_PPU_MON_ROWCOL_COL, regs.video_counter.col);
        write_reg(EMUBUS_PPU_MON_SEL_ROWCOL, reg);

        write_reg(EMUBUS_PPU_MON_SEL_FRAME, regs.video_counter.frame);
    }

    if (which & regs_bus_master) {
        write_reg(EMUBUS_PPU_MON_SEL_M7MPYA, regs.bus_master.m7.mpya);
        write_reg(EMUBUS_PPU_MON_SEL_M7MPYB, regs.bus_master.m7.mpyb);
        write_reg(EMUBUS_PPU_MON_SEL_M7MPYC, regs.bus_master.m7.mpyc);
    }

    if (which & regs_palette) {
        write_array16<EMUBUS_PPU_MON_SEL_CGRAM_LEN / 2>(EMUBUS_PPU_MON_SEL_CGRAM, regs.cgram);
    }

    if (which & regs_oam) {
        write_array16<EMUBUS_PPU_MON_SEL_OAML_LEN / 2>(EMUBUS_PPU_MON_SEL_OAML, regs.oaml);
        write_array8<EMUBUS_PPU_MON_SEL_OAMH_LEN>(EMUBUS_PPU_MON_SEL_OAMH, regs.oamh);
    }
}

#undef INSERT1
#undef INSERTN

} // namespace snes
