#pragma once

#include <stdint.h>
#include <string>
#include <stdexcept>
#include "emubus.h"

namespace snes {

using memaddr = uint32_t;

constexpr memaddr memaddr_min = 0x000000;
constexpr memaddr memaddr_max = 0xFFFFFF;

enum class memtype
{
    cpu,                                        // WRAM, cart. (ROM, SRAM)
    ppu,                                        // VRAM
    apu,                                        // SMP/DSP RAM
};

const char* name(memtype);

class sys;
class memval;

struct memptr_exception : public std::runtime_error
{
    memptr_exception(const char* what_arg) : std::runtime_error(what_arg) {}
};

class memptr
{
public:
    static void attach_sys(sys&);

    memptr() {}
    constexpr memptr(memaddr a, memtype t) : _a(a), _t(t) {}
    constexpr memptr(memaddr a) : memptr(a, memtype::cpu) {}
    constexpr memptr(const memptr&) = default;

    memtype type() const { return _t; }
    memaddr addr() const { return _a; }

    memptr& operator+=(int i) { _a += i; return *this; }
    memptr& operator++() { *this += 1; return *this; }
    memptr operator++(int) { auto c = *this; *this += 1; return c; }

    memval operator*() const;
    memval operator[](int) const;

    std::string to_string() const;

private:
    memaddr _a;
    memtype _t;
};

inline memptr operator+(memptr p, int i)
{
    return p += i;
}

class memval
{
public:
    using value_type = uint8_t;

    explicit memval(const memptr& p) : _p(p) {}
    operator uint8_t();
    memval& operator=(uint8_t);
    memval& operator^=(uint8_t);

private:
    volatile uint8_t* get_reg() const;

    memptr _p;
};

} // namespace snes
