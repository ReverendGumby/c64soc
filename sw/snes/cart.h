#pragma once

#include <stdexcept>
#include <memory>
#include <mutex>
#include <stdint.h>
#include "cart_loader/cart_loader.h"
#include "crt.h"
#include "memptr.h"
#include "util/pollable.h"
#include "util/timer.h"
#include "util/listener.h"
#include "util/unique_buf.h"
#include <ui/hmi_cart_listener.h>

namespace snes {

namespace _cart {

class listener
{
public:
    virtual void cart_modified(bool) = 0;
};

} // namespace _cart

struct cart_exception : public std::runtime_error
{
    cart_exception(const char* what_arg) : std::runtime_error(what_arg) {}
};

class cpumem;

class cart : public talker<_cart::listener>,
             public talker<ui::hmi_cart_listener>,
             private pollable
{
public:
    using PCrt = std::shared_ptr<Crt>;
    using listener = _cart::listener;

    static constexpr size_t sram_len = 256 * 1024;
    static constexpr size_t rom_len = 4 * 1024 * 1024;

    struct map_cfg_t {
        Crt::map_mode_t mem_map;
        uint32_t rom_mask, sram_mask;
        bool sram_enable;
    };

    cart(cpumem&);

    void init();

    uint8_t* get_sram();
    uint8_t* get_rom();

    uint8_t* addr_to_ptr(memaddr a);

    void set_map_cfg(const map_cfg_t& cfg);

    void reset();

    void unload();
    void load(PCrt);
    PCrt save();
    void reset_ram();

    bool is_modified() const { return _modified; }
    bool can_save() const;
    void set_battery_disabled(bool);

    struct state_t {
        bool loaded;
        unique_buf sram;
    };
    void read_state(state_t&);

private:
    using Cldr = cart_loader::cart_loader;

    static std::unique_ptr<uint8_t[]> sram, rom;

    void read_map_cfg();

    Cldr* new_loader();

    // interface for pollable
    virtual const char* get_poll_name() const { return "cart"; }
    virtual void poll();

    void set_modified(bool);

    cpumem& _cpumem;
    map_cfg_t _map_cfg;
    std::mutex _mtx;
    PCrt _crt;
    std::unique_ptr<Cldr> _cldr;
    timer _write_tmr;
    bool _modified = false;
    bool _battery_disabled = false;
};

} // namespace snes
