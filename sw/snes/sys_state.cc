#include "sys_state.h"
#include <plat/manager.h>
#include <app/version.h>
#include <util/string.h>
#include <util/buffer.h>
#include <stdexcept>
#include <iostream>

// This code takes a _long_ time to compile. Isolate it here.

namespace snes {

using namespace YAML;

// XXX: Making it easier to print to terminal
bool printable = false;

//////////////////////////////////////////////////////////////////////
// Common types

static Emitter& operator<<(Emitter& es, const buffer& v)
{
    if (printable) {
        auto s = std::to_string(v.len) + " bytes";
        es << s;
    } else {
        es << Binary(v.get_ptr(), v.len);
    }
    return es;
}

static void deyamlify(unique_buf& v, const Node& node)
{
    auto bin = node.as<Binary>();
    v = unique_buf(bin.size());
    memcpy(v.ptr, bin.data(), v.len);
}

static void deyamlify(buffer v, const Node& node)
{
    auto bin = node.as<Binary>();
    if (v.len != bin.size()) {
        std::cout << "v.len=" << v.len << " bin.size()=" << bin.size() << '\n';
        throw std::runtime_error("deyamlify: buffer size mismatch");
    }
    memcpy(v.ptr, bin.data(), v.len);
}

//////////////////////////////////////////////////////////////////////
// CPU

static Emitter& operator<<(Emitter& es, const icd::cpu_regs::internal_t& v)
{
    es << BeginMap;

    es << Key << "cp2" << Value << v.cp2;
    es << Key << "t_reset_pend" << Value << v.t_reset_pend;
    es << Key << "am_nostart" << Value << v.am_nostart;
    es << Key << "am_done_d" << Value << v.am_done_d;
    es << Key << "am_done_d2" << Value << v.am_done_d2;
    es << Key << "resp" << Value << v.resp;
    es << Key << "nmip" << Value << v.nmip;
    es << Key << "irqp" << Value << v.irqp;
    es << Key << "resg" << Value << v.resg;
    es << Key << "nmig" << Value << v.nmig;
    es << Key << "respd" << Value << v.respd;
    es << Key << "nmil" << Value << v.nmil;
    es << Key << "intg_pre2" << Value << v.intg_pre2;
    es << Key << "intg" << Value << v.intg;
    es << Key << "vec1" << Value << v.vec1;
    es << Key << "intr_id" << Value << v.intr_id;
    es << Key << "rwb_ext" << Value << v.rwb_ext;
    es << Key << "mlb_ext" << Value << v.mlb_ext;
    es << Key << "vpb_ext" << Value << v.vpb_ext;
    es << Key << "vda_ext" << Value << v.vda_ext;
    es << Key << "vpa_ext" << Value << v.vpa_ext;
    es << Key << "ac_z" << Value << v.ac_z;
    es << Key << "dl_z" << Value << v.dl_z;
    es << Key << "addc" << Value << v.addc;
    es << Key << "add16" << Value << v.add16;
    es << Key << "accoh" << Value << v.accoh;
    es << Key << "daa" << Value << v.daa;
    es << Key << "das" << Value << v.das;
    es << Key << "ors" << Value << v.ors;
    es << Key << "ands" << Value << v.ands;
    es << Key << "eors" << Value << v.eors;
    es << Key << "asls" << Value << v.asls;
    es << Key << "rols" << Value << v.rols;
    es << Key << "lsrs" << Value << v.lsrs;
    es << Key << "rors" << Value << v.rors;
    es << Key << "ccod" << Value << v.ccod;
    es << Key << "cvod" << Value << v.cvod;
    es << Key << "op_am_page_wrap_d" << Value << v.op_am_page_wrap_d;
    es << Key << "rdy1" << Value << v.rdy1;
    es << Key << "waitn0" << Value << v.waitn0;
    es << Key << "t" << Value << Hex << v.t;
    es << Key << "am_t" << Value << Hex << v.am_t;
    es << Key << "op_tp" << Value << Hex << v.op_tp;
    es << Key << "aab" << Value << Hex << v.aab;
    es << Key << "upc" << Value << Hex << v.upc;
    es << Key << "idld" << Value << Hex << v.idld;
    es << Key << "aao" << Value << Hex << v.aao;
    es << Key << "ai" << Value << Hex << v.ai;
    es << Key << "bi" << Value << Hex << v.bi;
    es << Key << "cod" << Value << Hex << v.cod;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const icd::cpu_regs& v)
{
    es << BeginMap;

    es << Key << "pc" << Value << Hex << v.pc;
    es << Key << "pf" << Value;
    {
        const auto& pf = v.pf;
        es << BeginMap
           << Key << "c" << Value << pf.c
           << Key << "z" << Value << pf.z
           << Key << "i" << Value << pf.i
           << Key << "d" << Value << pf.d
           << Key << "x" << Value << pf.x
           << Key << "m" << Value << pf.m
           << Key << "v" << Value << pf.v
           << Key << "n" << Value << pf.n
           << EndMap;
    }
    es << Key << "e" << Value << v.e;
    es << Key << "s" << Value << Hex << v.s;
    es << Key << "a" << Value << Hex << v.a;
    es << Key << "d" << Value << Hex << v.d;
    es << Key << "x" << Value << Hex << v.x;
    es << Key << "y" << Value << Hex << v.y;
    es << Key << "dbr" << Value << Hex << unsigned(v.dbr);
    es << Key << "pbr" << Value << Hex << unsigned(v.pbr);
    es << Key << "ir" << Value << Hex << unsigned(v.ir);
    es << Key << "aor" << Value << Hex << v.aor;
    es << Key << "dor" << Value << Hex << unsigned(v.dor);
    es << Key << "z" << Value << Hex << v.z;
    es << Key << "tstate" << Value;
    {
        const auto& tstate = v.tstate;
        es << BeginMap
           << Key << "ts" << Value << unsigned(tstate.ts)
           << EndMap;
    }
    es << Key << "pstate" << Value;
    {
        const auto& pstate = v.pstate;
        es << BeginMap
           << Key << "rw" << Value << unsigned(pstate.rw)
           << Key << "sync" << Value << unsigned(pstate.sync)
           << EndMap;
    }
    es << Key << "internal" << Value << v.internal;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const icd::state_t& v)
{
    es << BeginMap;

    es << Key << "regs" << Value << v.regs;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const cpu_chip_state::io_regs_t& v)
{
    es << Hex;
    es << BeginMap;

    es << Key << "joyout" << Value << unsigned(v.joyout);
    es << Key << "joyser0" << Value << unsigned(v.joyser0);
    es << Key << "joyser1" << Value << unsigned(v.joyser1);
    es << Key << "nmitimen.ajpr_en" << Value << unsigned(v.nmitimen.ajpr_en);
    es << Key << "nmitimen.hvt_mode" << Value << unsigned(v.nmitimen.hvt_mode);
    es << Key << "nmitimen.vbl_en" << Value << unsigned(v.nmitimen.vbl_en);
    es << Key << "wrio" << Value << unsigned(v.wrio);
    es << Key << "wrmpya" << Value << unsigned(v.wrmpya);
    es << Key << "wrdiva" << Value << unsigned(v.wrdiva);
    es << Key << "htime" << Value << unsigned(v.htime);
    es << Key << "vtime" << Value << unsigned(v.vtime);
    es << Key << "mdmaen" << Value << unsigned(v.mdmaen);
    es << Key << "hdmaen" << Value << unsigned(v.hdmaen);
    es << Key << "memsel" << Value << unsigned(v.memsel.memsel);
    es << Key << "rdnmi.cpu_ver" << Value << unsigned(v.rdnmi.cpu_ver);
    es << Key << "rdnmi.flag" << Value << unsigned(v.rdnmi.flag);
    es << Key << "timeup.flag" << Value << unsigned(v.timeup.flag);
    es << Key << "hvbjoy.flag" << Value << unsigned(v.hvbjoy.ajpr_busy);
    es << Key << "hvbjoy.hblank" << Value << unsigned(v.hvbjoy.hblank);
    es << Key << "hvbjoy.vblank" << Value << unsigned(v.hvbjoy.vblank);
    es << Key << "rdio" << Value << unsigned(v.rdio);
    es << Key << "rddiv" << Value << unsigned(v.rddiv);
    es << Key << "rdmpy" << Value << unsigned(v.rdmpy);
    es << Key << "joy1" << Value << unsigned(v.joy1);
    es << Key << "joy2" << Value << unsigned(v.joy2);
    es << Key << "joy3" << Value << unsigned(v.joy3);
    es << Key << "joy4" << Value << unsigned(v.joy4);

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const cpu_chip_state::dma_regs_t& v)
{
    es << BeginSeq;

    for (size_t i = 0; i < 8; i++) {
        auto& ch = v.ch[i];
        es << Hex;
        es << BeginMap;
        es << Key << "dmapx" << Value << unsigned(ch.dmapx);
        es << Key << "bbadx" << Value << unsigned(ch.bbadx);
        es << Key << "a1txl" << Value << unsigned(ch.a1txl);
        es << Key << "a1txh" << Value << unsigned(ch.a1txh);
        es << Key << "a1xb" << Value << unsigned(ch.a1xb);
        es << Key << "dasxl" << Value << unsigned(ch.dasxl);
        es << Key << "dasxh" << Value << unsigned(ch.dasxh);
        es << Key << "dasbx" << Value << unsigned(ch.dasbx);
        es << Key << "a2axl" << Value << unsigned(ch.a2axl);
        es << Key << "a2axh" << Value << unsigned(ch.a2axh);
        es << Key << "ntrlx" << Value << unsigned(ch.ntrlx);
        es << Key << "unusedx" << Value << unsigned(ch.unusedx);
        es << EndMap;
    }

    es << EndSeq;
    return es;
}

static Emitter& operator<<(Emitter& es, const cpu_chip_state::dma_state_t& v)
{
    es << BeginMap;

    es << Key << "active" << Value << v.active;
    es << Key << "gpdma" << Value << v.gpdma;
    es << Key << "hdma" << Value << v.hdma;
    es << Key << "xfer" << Value << v.xfer;
    es << Key << "ch_active" << Value << v.ch_active;
    es << Key << "done" << Value << v.done;

    es << Key << "hcdis" << Value << v.hcdis;
    es << Key << "hxfer" << Value << v.hxfer;

    es << Key << "a_o" << Value << Hex << v.a_o;
    es << Key << "pa_o" << Value << Hex << v.pa_o;
    es << Key << "a_oe" << Value << v.a_oe;
    es << Key << "pa_oe" << Value << v.pa_oe;
    es << Key << "nwr_o" << Value << v.nwr_o;
    es << Key << "nrd_o" << Value << v.nrd_o;
    es << Key << "npawr_o" << Value << v.npawr_o;
    es << Key << "npard_o" << Value << v.npard_o;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const cpu_chip_state::pins_t& v)
{
    es << BeginMap;

    es << Key << "jpout" << Value << v.jpout;
    es << Key << "jpclk" << Value << v.jpclk;
    es << Key << "jp1d" << Value << v.jp1d;
    es << Key << "jp2d" << Value << v.jp2d;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es,
                           const cpu_chip_state::internal_t::clkgen_t& v)
{
    es << BeginMap;

    es << Key << "cycle_ccnt" << Value << v.cycle_ccnt;
    es << Key << "cp2_clkcnt" << Value << v.cp2_clkcnt;
    es << Key << "slow_ccnt" << Value << v.slow_ccnt;
    es << Key << "cpu_gate_dly" << Value << v.cpu_gate_dly;
    es << Key << "cpu_act" << Value << v.cpu_act;
    es << Key << "slowck_act" << Value << v.slowck_act;
    es << Key << "cp2" << Value << v.cp2;
    es << Key << "cpu_refresh_en" << Value << v.cpu_refresh_en;
    es << Key << "slow_gate_dly" << Value << v.slow_gate_dly;
    es << Key << "slow_refresh_en" << Value << v.slow_refresh_en;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es,
                           const cpu_chip_state::internal_t::ajpr_t& v)
{
    es << BeginMap;

    es << Key << "ccnt" << Value << v.ccnt;
    es << Key << "cycle" << Value << v.cycle;
    es << Key << "vblank_d" << Value << v.vblank_d;
    es << Key << "jpclk" << Value << v.jpclk;
    es << Key << "jpout" << Value << v.jpout;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es,
                           const cpu_chip_state::internal_t::gen_t& v)
{
    es << BeginMap;

    es << Key << "vblank_d" << Value << v.vblank_d;
    es << Key << "vbl_set" << Value << v.vbl_set;
    es << Key << "vbl_clear_flags" << Value << v.vbl_clear_flags;
    es << Key << "vbl_flag" << Value << v.vbl_flag;
    es << Key << "vbl_flag_d" << Value << v.vbl_flag_d;
    es << Key << "vbl_start" << Value << v.vbl_start;
    es << Key << "vbl_end" << Value << v.vbl_end;
    es << Key << "hblank_d" << Value << v.hblank_d;
    es << Key << "hbl_start" << Value << v.hbl_start;
    es << Key << "hbl_end" << Value << v.hbl_end;
    es << Key << "hvt_set" << Value << v.hvt_set;
    es << Key << "hvt_clear_flags" << Value << v.hvt_clear_flags;
    es << Key << "hvt_flag" << Value << v.hvt_flag;
    es << Key << "hvt_flag_d" << Value << v.hvt_flag_d;
    es << Key << "cc_d" << Value << v.cc_d;
    es << Key << "refresh_en" << Value << v.refresh_en;
    es << Key << "dma_mset" << Value << Hex << v.dma_mset;
    es << Key << "hcnt" << Value << Hex << v.hcnt;
    es << Key << "vcnt" << Value << Hex << v.vcnt;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es,
                           const cpu_chip_state::internal_t::muldiv_t& v)
{
    es << BeginMap;

    es << Key << "shifter" << Value << v.shifter;
    es << Key << "st" << Value << v.st;
    es << Key << "muldivnot" << Value << v.muldivnot;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es,
                           const cpu_chip_state::internal_t::dmac_t& v)
{
    es << BeginMap;

    es << Key << "ntrlz" << Value << v.ntrlz;
    es << Key << "post_vbl_end" << Value << v.post_vbl_end;
    es << Key << "hstartl" << Value << v.hstartl;
    es << Key << "hstart" << Value << v.hstart;
    es << Key << "gprestart_d" << Value << v.gprestart_d;
    es << Key << "aca" << Value << Hex << v.aca;
    es << Key << "dl" << Value << Hex << v.dl;
    es << Key << "men" << Value << Hex << v.men;
    es << Key << "hen_d" << Value << Hex << v.hen_d;
    es << Key << "cen" << Value << Hex << v.cen;
    es << Key << "gptpos" << Value << Hex << v.gptpos;
    es << Key << "hst" << Value << Hex << v.hst;
    es << Key << "hbc" << Value << Hex << v.hbc;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const cpu_chip_state::internal_t& v)
{
    es << BeginMap;

    es << Key << "clkgen" << Value << v.clkgen;
    es << Key << "ajpr" << Value << v.ajpr;
    es << Key << "gen" << Value << v.gen;
    es << Key << "muldiv" << Value << v.muldiv;
    es << Key << "dmac" << Value << v.dmac;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const cpu_chip_state& v)
{
    es << BeginMap;

    es << Key << "io_regs" << Value << v.io_regs;
    es << Key << "dma_regs" << Value << v.dma_regs;
    es << Key << "dma_state" << Value << v.dma_state;
    es << Key << "pins" << Value << v.pins;
    es << Key << "internal" << Value << v.internal;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const cpu::state_t& v)
{
    es << BeginMap;

    es << Key << "core" << Value << v.core;
    es << Key << "chip" << Value << v.chip;

    es << EndMap;
    return es;
}

static void deyamlify(icd::cpu_regs::internal_t& v, const Node& node)
{
    v.cp2 = node["cp2"].as<unsigned>();
    v.t_reset_pend = node["t_reset_pend"].as<unsigned>();
    v.am_nostart = node["am_nostart"].as<unsigned>();
    v.am_done_d = node["am_done_d"].as<unsigned>();
    v.am_done_d2 = node["am_done_d2"].as<unsigned>();
    v.resp = node["resp"].as<unsigned>();
    v.nmip = node["nmip"].as<unsigned>();
    v.irqp = node["irqp"].as<unsigned>();
    v.resg = node["resg"].as<unsigned>();
    v.nmig = node["nmig"].as<unsigned>();
    v.respd = node["respd"].as<unsigned>();
    v.nmil = node["nmil"].as<unsigned>();
    v.intg_pre2 = node["intg_pre2"].as<unsigned>();
    v.intg = node["intg"].as<unsigned>();
    v.vec1 = node["vec1"].as<unsigned>();
    v.intr_id = node["intr_id"].as<unsigned>();
    v.rwb_ext = node["rwb_ext"].as<unsigned>();
    v.mlb_ext = node["mlb_ext"].as<unsigned>();
    v.vpb_ext = node["vpb_ext"].as<unsigned>();
    v.vda_ext = node["vda_ext"].as<unsigned>();
    v.vpa_ext = node["vpa_ext"].as<unsigned>();
    v.ac_z = node["ac_z"].as<unsigned>();
    v.dl_z = node["dl_z"].as<unsigned>();
    v.addc = node["addc"].as<unsigned>();
    v.add16 = node["add16"].as<unsigned>();
    v.accoh = node["accoh"].as<unsigned>();
    v.daa = node["daa"].as<unsigned>();
    v.das = node["das"].as<unsigned>();
    v.ors = node["ors"].as<unsigned>();
    v.ands = node["ands"].as<unsigned>();
    v.eors = node["eors"].as<unsigned>();
    v.asls = node["asls"].as<unsigned>();
    v.rols = node["rols"].as<unsigned>();
    v.lsrs = node["lsrs"].as<unsigned>();
    v.rors = node["rors"].as<unsigned>();
    v.ccod = node["ccod"].as<unsigned>();
    v.cvod = node["cvod"].as<unsigned>();
    v.op_am_page_wrap_d = node["op_am_page_wrap_d"].as<unsigned>();
    v.rdy1 = node["rdy1"].as<unsigned>();
    v.waitn0 = node["waitn0"].as<unsigned>();
    v.t = node["t"].as<unsigned>();
    v.am_t = node["am_t"].as<unsigned>();
    v.op_tp = node["op_tp"].as<unsigned>();
    v.aab = node["aab"].as<unsigned>();
    v.upc = node["upc"].as<unsigned>();
    v.idld = node["idld"].as<unsigned>();
    v.aao = node["aao"].as<unsigned>();
    v.ai = node["ai"].as<unsigned>();
    v.bi = node["bi"].as<unsigned>();
    v.cod = node["cod"].as<unsigned>();
}

static void deyamlify(icd::cpu_regs& v, const Node& node)
{
    v.pc = node["pc"].as<unsigned>();
    {
        auto& pf = v.pf;
        const Node& ns = node["pf"];

        pf.c = ns["c"].as<bool>();
        pf.z = ns["z"].as<bool>();
        pf.i = ns["i"].as<bool>();
        pf.d = ns["d"].as<bool>();
        pf.x = ns["x"].as<bool>();
        pf.m = ns["m"].as<bool>();
        pf.v = ns["v"].as<bool>();
        pf.n = ns["n"].as<bool>();
    }
    v.e = node["e"].as<bool>();
    v.s = node["s"].as<unsigned>();
    v.a = node["a"].as<unsigned>();
    v.d = node["d"].as<unsigned>();
    v.x = node["x"].as<unsigned>();
    v.y = node["y"].as<unsigned>();
    v.dbr = node["dbr"].as<unsigned>();
    v.pbr = node["pbr"].as<unsigned>();
    v.ir = node["ir"].as<unsigned>();
    v.aor = node["aor"].as<unsigned>();
    v.dor = node["dor"].as<unsigned>();
    v.z = node["z"].as<unsigned>();
    {
        auto& tstate = v.tstate;
        const Node& ns = node["tstate"];
        tstate.ts = ns["ts"].as<unsigned>();
    }
    {
        auto& pstate = v.pstate;
        const Node& ns = node["pstate"];
        pstate.rw = ns["rw"].as<unsigned>();
        pstate.sync = ns["sync"].as<unsigned>();
    }
    deyamlify(v.internal, node["internal"]);
}

static void deyamlify(icd::state_t& v, const Node& node)
{
    deyamlify(v.regs, node["regs"]);
}

static void deyamlify(cpu_chip_state::io_regs_t& v, const Node& node)
{
    v.joyout = node["joyout"].as<unsigned>();
    v.joyser0 = node["joyser0"].as<unsigned>();
    v.joyser1 = node["joyser1"].as<unsigned>();
    v.nmitimen.ajpr_en = node["nmitimen.ajpr_en"].as<unsigned>();
    v.nmitimen.hvt_mode = node["nmitimen.hvt_mode"].as<unsigned>();
    v.nmitimen.vbl_en = node["nmitimen.vbl_en"].as<unsigned>();
    v.wrio = node["wrio"].as<unsigned>();
    v.wrmpya = node["wrmpya"].as<unsigned>();
    v.wrdiva = node["wrdiva"].as<unsigned>();
    v.htime = node["htime"].as<unsigned>();
    v.vtime = node["vtime"].as<unsigned>();
    v.mdmaen = node["mdmaen"].as<unsigned>();
    v.hdmaen = node["hdmaen"].as<unsigned>();
    v.memsel.memsel = node["memsel"].as<unsigned>();
    v.rdnmi.cpu_ver = node["rdnmi.cpu_ver"].as<unsigned>();
    v.rdnmi.flag = node["rdnmi.flag"].as<unsigned>();
    v.timeup.flag = node["timeup.flag"].as<unsigned>();
    v.hvbjoy.ajpr_busy = node["hvbjoy.flag"].as<unsigned>();
    v.hvbjoy.hblank = node["hvbjoy.hblank"].as<unsigned>();
    v.hvbjoy.vblank = node["hvbjoy.vblank"].as<unsigned>();
    v.rdio = node["rdio"].as<unsigned>();
    v.rddiv = node["rddiv"].as<unsigned>();
    v.rdmpy = node["rdmpy"].as<unsigned>();
    v.joy1 = node["joy1"].as<unsigned>();
    v.joy2 = node["joy2"].as<unsigned>();
    v.joy3 = node["joy3"].as<unsigned>();
    v.joy4 = node["joy4"].as<unsigned>();
}

static void deyamlify(cpu_chip_state::dma_regs_t& v, const Node& node)
{
    for (size_t i = 0; i < 8; i++) {
        auto& ch = v.ch[i];
        const auto& node_ch = node[i];

        ch.dmapx = node_ch["dmapx"].as<unsigned>();
        ch.bbadx = node_ch["bbadx"].as<unsigned>();
        ch.a1txl = node_ch["a1txl"].as<unsigned>();
        ch.a1txh = node_ch["a1txh"].as<unsigned>();
        ch.a1xb = node_ch["a1xb"].as<unsigned>();
        ch.dasxl = node_ch["dasxl"].as<unsigned>();
        ch.dasxh = node_ch["dasxh"].as<unsigned>();
        ch.dasbx = node_ch["dasbx"].as<unsigned>();
        ch.a2axl = node_ch["a2axl"].as<unsigned>();
        ch.a2axh = node_ch["a2axh"].as<unsigned>();
        ch.ntrlx = node_ch["ntrlx"].as<unsigned>();
        ch.unusedx = node_ch["unusedx"].as<unsigned>();
    }
}

static void deyamlify(cpu_chip_state::dma_state_t& v, const Node& node)
{
    v.active = node["active"].as<unsigned>();
    v.gpdma = node["gpdma"].as<unsigned>();
    v.hdma = node["hdma"].as<unsigned>();
    v.xfer = node["xfer"].as<unsigned>();
    v.ch_active = node["ch_active"].as<unsigned>();
    v.done = node["done"].as<unsigned>();

    v.hcdis = node["hcdis"].as<unsigned>();
    v.hxfer = node["hxfer"].as<unsigned>();

    v.a_o = node["a_o"].as<unsigned>();
    v.pa_o = node["pa_o"].as<unsigned>();
    v.a_oe = node["a_oe"].as<unsigned>();
    v.pa_oe = node["pa_oe"].as<unsigned>();
    v.nwr_o = node["nwr_o"].as<unsigned>();
    v.nrd_o = node["nrd_o"].as<unsigned>();
    v.npawr_o = node["npawr_o"].as<unsigned>();
    v.npard_o = node["npard_o"].as<unsigned>();
}

static void deyamlify(cpu_chip_state::pins_t& v, const Node& node)
{
    v.jpout = node["jpout"].as<unsigned>();
    v.jpclk = node["jpclk"].as<unsigned>();
    v.jp1d = node["jp1d"].as<unsigned>();
    v.jp2d = node["jp2d"].as<unsigned>();
}

static void deyamlify(cpu_chip_state::internal_t::clkgen_t& v, const Node& node)
{
    v.cycle_ccnt = node["cycle_ccnt"].as<unsigned>();
    v.cp2_clkcnt = node["cp2_clkcnt"].as<unsigned>();
    v.slow_ccnt = node["slow_ccnt"].as<unsigned>();
    v.cpu_gate_dly = node["cpu_gate_dly"].as<unsigned>();
    v.cpu_act = node["cpu_act"].as<unsigned>();
    v.slowck_act = node["slowck_act"].as<unsigned>();
    v.cp2 = node["cp2"].as<unsigned>();
    v.cpu_refresh_en = node["cpu_refresh_en"].as<unsigned>();
    v.slow_gate_dly = node["slow_gate_dly"].as<unsigned>();
    v.slow_refresh_en = node["slow_refresh_en"].as<unsigned>();
}

static void deyamlify(cpu_chip_state::internal_t::ajpr_t& v, const Node& node)
{
    v.ccnt = node["ccnt"].as<unsigned>();
    v.cycle = node["cycle"].as<unsigned>();
    v.vblank_d = node["vblank_d"].as<unsigned>();
    v.jpclk = node["jpclk"].as<unsigned>();
    v.jpout = node["jpout"].as<unsigned>();
}

static void deyamlify(cpu_chip_state::internal_t::gen_t& v, const Node& node)
{
    v.vblank_d = node["vblank_d"].as<unsigned>();
    v.vbl_set = node["vbl_set"].as<unsigned>();
    v.vbl_clear_flags = node["vbl_clear_flags"].as<unsigned>();
    v.vbl_flag = node["vbl_flag"].as<unsigned>();
    v.vbl_flag_d = node["vbl_flag_d"].as<unsigned>();
    v.vbl_start = node["vbl_start"].as<unsigned>();
    v.vbl_end = node["vbl_end"].as<unsigned>();
    v.hblank_d = node["hblank_d"].as<unsigned>();
    v.hbl_start = node["hbl_start"].as<unsigned>();
    v.hbl_end = node["hbl_end"].as<unsigned>();
    v.hvt_set = node["hvt_set"].as<unsigned>();
    v.hvt_clear_flags = node["hvt_clear_flags"].as<unsigned>();
    v.hvt_flag = node["hvt_flag"].as<unsigned>();
    v.hvt_flag_d = node["hvt_flag_d"].as<unsigned>();
    v.cc_d = node["cc_d"].as<unsigned>();
    v.refresh_en = node["refresh_en"].as<unsigned>();
    v.dma_mset = node["dma_mset"].as<unsigned>();
    v.hcnt = node["hcnt"].as<unsigned>();
    v.vcnt = node["vcnt"].as<unsigned>();
}

static void deyamlify(cpu_chip_state::internal_t::muldiv_t& v, const Node& node)
{
    v.shifter = node["shifter"].as<unsigned>();
    v.st = node["st"].as<unsigned>();
    v.muldivnot = node["muldivnot"].as<unsigned>();
}

static void deyamlify(cpu_chip_state::internal_t::dmac_t& v, const Node& node)
{
    v.ntrlz = node["ntrlz"].as<unsigned>();
    v.post_vbl_end = node["post_vbl_end"].as<unsigned>();
    v.hstartl = node["hstartl"].as<unsigned>();
    v.hstart = node["hstart"].as<unsigned>();
    v.gprestart_d = node["gprestart_d"].as<unsigned>();
    v.aca = node["aca"].as<unsigned>();
    v.dl = node["dl"].as<unsigned>();
    v.men = node["men"].as<unsigned>();
    v.hen_d = node["hen_d"].as<unsigned>();
    v.cen = node["cen"].as<unsigned>();
    v.gptpos = node["gptpos"].as<unsigned>();
    v.hst = node["hst"].as<unsigned>();
    v.hbc = node["hbc"].as<unsigned>();
}

static void deyamlify(cpu_chip_state::internal_t& v, const Node& node)
{
    deyamlify(v.clkgen, node["clkgen"]);
    deyamlify(v.ajpr, node["ajpr"]);
    deyamlify(v.gen, node["gen"]);
    deyamlify(v.muldiv, node["muldiv"]);
    deyamlify(v.dmac, node["dmac"]);
}

static void deyamlify(cpu_chip_state& v, const Node& node)
{
    deyamlify(v.io_regs, node["io_regs"]);
    deyamlify(v.dma_regs, node["dma_regs"]);
    deyamlify(v.dma_state, node["dma_state"]);
    deyamlify(v.pins, node["pins"]);
    deyamlify(v.internal, node["internal"]);
}

static void deyamlify(cpu::state_t& v, const Node& node)
{
    deyamlify(v.core, node["core"]);
    deyamlify(v.chip, node["chip"]);
}

//////////////////////////////////////////////////////////////////////
// PPU

static Emitter& operator<<(Emitter& es, const ppu_regs::bus_master_t::bg_t& v)
{
    es << BeginMap
       << Key << "vab" << Value << v.vab
       << Key << "vac" << Value << v.vac
       << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const ppu_regs::bus_master_t::m7_t& v)
{
    es << BeginMap
       << Key << "mpya" << Value << v.mpya
       << Key << "mpyb" << Value << v.mpyb
       << Key << "mpyc" << Value << v.mpyc
       << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const ppu_regs& v)
{
    es << Hex;
    es << BeginMap;

    es << Key << "inidisp.bright" << Value <<  unsigned(v.inidisp.bright);
    es << Key << "inidisp.forced_blank" << Value << bool(v.inidisp.forced_blank);
    es << Key << "objsel.ba" << Value <<  unsigned(v.objsel.ba);
    es << Key << "objsel.n" << Value <<  unsigned(v.objsel.n);
    es << Key << "objsel.ss" << Value <<  unsigned(v.objsel.ss);
    es << Key << "bgmode.mode" << Value << unsigned(v.bgmode.mode);
    es << Key << "bgmode.bg3_pri" << Value << bool(v.bgmode.bg3_pri);
    es << Key << "mosaic.sz" << Value << unsigned(v.mosaic.sz);
    es << Key << "mosaic.bg1-4" << Value << Flow << BeginSeq
       << bool(v.mosaic.bg1) << bool(v.mosaic.bg2)
       << bool(v.mosaic.bg3) << bool(v.mosaic.bg4) << EndSeq;
    es << Key << "bg1sc.size" << Value << unsigned(v.bg1sc.size);
    es << Key << "bg1sc.base" << Value << unsigned(v.bg1sc.base);
    es << Key << "bg2sc.size" << Value << unsigned(v.bg2sc.size);
    es << Key << "bg2sc.base" << Value << unsigned(v.bg2sc.base);
    es << Key << "bg3sc.size" << Value << unsigned(v.bg3sc.size);
    es << Key << "bg3sc.base" << Value << unsigned(v.bg3sc.base);
    es << Key << "bg4sc.size" << Value << unsigned(v.bg4sc.size);
    es << Key << "bg4sc.base" << Value << unsigned(v.bg4sc.base);
    es << Key << "bg1nba" << Value << unsigned(v.bg12nba.bg1nba);
    es << Key << "bg2nba" << Value << unsigned(v.bg12nba.bg2nba);
    es << Key << "bg3nba" << Value << unsigned(v.bg34nba.bg3nba);
    es << Key << "bg4nba" << Value << unsigned(v.bg34nba.bg4nba);
    es << Key << "bg1hofs" << Value << v.bg1hofs;
    es << Key << "m7hofs" << Value << v.m7hofs;
    es << Key << "bg1vofs" << Value << v.bg1vofs;
    es << Key << "m7vofs" << Value << v.m7vofs;
    es << Key << "bg2hofs" << Value << v.bg2hofs;
    es << Key << "bg2vofs" << Value << v.bg2vofs;
    es << Key << "bg3hofs" << Value << v.bg3hofs;
    es << Key << "bg3vofs" << Value << v.bg3vofs;
    es << Key << "bg4hofs" << Value << v.bg4hofs;
    es << Key << "bg4vofs" << Value << v.bg4vofs;
    es << Key << "m7sel.hflip" << Value << bool(v.m7sel.hflip);
    es << Key << "m7sel.vflip" << Value << bool(v.m7sel.vflip);
    es << Key << "m7sel.scover" << Value << unsigned(v.m7sel.scover);
    es << Key << "m7a" << Value << v.m7a;
    es << Key << "m7b" << Value << v.m7b;
    es << Key << "m7c" << Value << v.m7c;
    es << Key << "m7d" << Value << v.m7d;
    es << Key << "m7x" << Value << v.m7x;
    es << Key << "m7y" << Value << v.m7y;
    es << Key << "w1_inv_bg1" << Value << bool(v.w12sel.w1_inv_bg1);
    es << Key << "w1_ena_bg1" << Value << bool(v.w12sel.w1_ena_bg1);
    es << Key << "w2_inv_bg1" << Value << bool(v.w12sel.w2_inv_bg1);
    es << Key << "w2_ena_bg1" << Value << bool(v.w12sel.w2_ena_bg1);
    es << Key << "w1_inv_bg2" << Value << bool(v.w12sel.w1_inv_bg2);
    es << Key << "w1_ena_bg2" << Value << bool(v.w12sel.w1_ena_bg2);
    es << Key << "w2_inv_bg2" << Value << bool(v.w12sel.w2_inv_bg2);
    es << Key << "w2_ena_bg2" << Value << bool(v.w12sel.w2_ena_bg2);
    es << Key << "w1_inv_bg3" << Value << bool(v.w34sel.w1_inv_bg3);
    es << Key << "w1_ena_bg3" << Value << bool(v.w34sel.w1_ena_bg3);
    es << Key << "w2_inv_bg3" << Value << bool(v.w34sel.w2_inv_bg3);
    es << Key << "w2_ena_bg3" << Value << bool(v.w34sel.w2_ena_bg3);
    es << Key << "w1_inv_bg4" << Value << bool(v.w34sel.w1_inv_bg4);
    es << Key << "w1_ena_bg4" << Value << bool(v.w34sel.w1_ena_bg4);
    es << Key << "w2_inv_bg4" << Value << bool(v.w34sel.w2_inv_bg4);
    es << Key << "w2_ena_bg4" << Value << bool(v.w34sel.w2_ena_bg4);
    es << Key << "w1_inv_obj" << Value << bool(v.wobjsel.w1_inv_obj);
    es << Key << "w1_ena_obj" << Value << bool(v.wobjsel.w1_ena_obj);
    es << Key << "w2_inv_obj" << Value << bool(v.wobjsel.w2_inv_obj);
    es << Key << "w2_ena_obj" << Value << bool(v.wobjsel.w2_ena_obj);
    es << Key << "w1_inv_clr" << Value << bool(v.wobjsel.w1_inv_clr);
    es << Key << "w1_ena_clr" << Value << bool(v.wobjsel.w1_ena_clr);
    es << Key << "w2_inv_clr" << Value << bool(v.wobjsel.w2_inv_clr);
    es << Key << "w2_ena_clr" << Value << bool(v.wobjsel.w2_ena_clr);
    es << Key << "wh0" << Value << unsigned(v.wh0);
    es << Key << "wh1" << Value << unsigned(v.wh1);
    es << Key << "wh2" << Value << unsigned(v.wh2);
    es << Key << "wh3" << Value << unsigned(v.wh3);
    es << Key << "wbglog.bg1" << Value << unsigned(v.wbglog.bg1);
    es << Key << "wbglog.bg2" << Value << unsigned(v.wbglog.bg2);
    es << Key << "wbglog.bg3" << Value << unsigned(v.wbglog.bg3);
    es << Key << "wbglog.bg4" << Value << unsigned(v.wbglog.bg4);
    es << Key << "wobjlog.obj" << Value << unsigned(v.wobjlog.obj);
    es << Key << "wobjlog.clr" << Value << unsigned(v.wobjlog.clr);
    es << Key << "tm.bg1-4,obj" << Value << Flow << BeginSeq
       << bool(v.tm.bg1) << bool(v.tm.bg2) << bool(v.tm.bg3) << bool(v.tm.bg4)
       << bool(v.tm.obj) << EndSeq;
    es << Key << "ts.bg1-4,obj" << Value << Flow << BeginSeq
       << bool(v.ts.bg1) << bool(v.ts.bg2) << bool(v.ts.bg3) << bool(v.ts.bg4)
       << bool(v.ts.obj) << EndSeq;
    es << Key << "tmw.bg1-4,obj" << Value << Flow << BeginSeq
       << bool(v.tmw.bg1) << bool(v.tmw.bg2) << bool(v.tmw.bg3) << bool(v.tmw.bg4)
       << bool(v.tmw.obj) << EndSeq;
    es << Key << "tsw.bg1-4,obj" << Value << Flow << BeginSeq
       << bool(v.tsw.bg1) << bool(v.tsw.bg2) << bool(v.tsw.bg3) << bool(v.tsw.bg4)
       << bool(v.tsw.obj) << EndSeq;
    es << Key << "cgwsel.direct" << Value << bool(v.cgwsel.direct);
    es << Key << "cgwsel.addend" << Value << bool(v.cgwsel.addend);
    es << Key << "cgwsel.sub_win_rgn" << Value << unsigned(v.cgwsel.sub_win_rgn);
    es << Key << "cgwsel.main_win_rgn" << Value << unsigned(v.cgwsel.main_win_rgn);
    es << Key << "cgadsub.bg1-4,obj,back" << Value << Flow << BeginSeq
       << bool(v.cgadsub.bg1) << bool(v.cgadsub.bg2) << bool(v.cgadsub.bg3)
       << bool(v.cgadsub.bg4) << bool(v.cgadsub.obj) << bool(v.cgadsub.back)
       << EndSeq;
    es << Key << "cgadsub.half" << Value << bool(v.cgadsub.half);
    es << Key << "cgadsub.sub" << Value << bool(v.cgadsub.sub);
    es << Key << "coldata.rgb" << Value << Flow << BeginSeq
       << unsigned(v.coldata.r) << unsigned(v.coldata.g)
       << unsigned(v.coldata.b) << EndSeq;
    es << Key << "setini.interlacing" << Value << bool(v.setini.interlacing);
    es << Key << "setini.obj_vdir_disp" << Value << bool(v.setini.obj_vdir_disp);
    es << Key << "setini.bg_vdir_disp" << Value << bool(v.setini.bg_vdir_disp);
    es << Key << "setini.h512" << Value << bool(v.setini.h512);
    es << Key << "setini.extbg" << Value << bool(v.setini.extbg);
    es << Key << "setini.extsync" << Value << bool(v.setini.extsync);
    es << Key << "mpylmh" << Value << v.mpylmh;
    es << Key << "ophct" << Value << v.ophct;
    es << Key << "opvct" << Value << v.opvct;
    es << Key << "stat77.range_over" << Value << bool(v.stat77.range_over);
    es << Key << "stat77.time_over" << Value << bool(v.stat77.time_over);
    es << Key << "stat78.opl" << Value << bool(v.stat78.opl);
    es << Key << "stat78.field" << Value << bool(v.stat78.field);
    es << Key << "video_counter" << Value;
    {
        es << BeginMap
           << Key << "row" << Value << v.video_counter.row
           << Key << "col" << Value << v.video_counter.col
           << Key << "frame" << Value << v.video_counter.frame
           << EndMap;
    }
    es << Key << "bus_master" << Value;
    {
        es << BeginMap
           << Key << "bg1" << Value << v.bus_master.bg[1]
           << Key << "bg2" << Value << v.bus_master.bg[2]
           << Key << "bg3" << Value << v.bus_master.bg[3]
           << Key << "bg4" << Value << v.bus_master.bg[4]
           << Key << "m7" << Value << v.bus_master.m7
           << EndMap;
    }

    es << Key << "cgram" << Value << buffer(v.cgram);
    es << Key << "oaml" << Value << buffer(v.oaml);
    es << Key << "oamh" << Value << buffer(v.oamh);

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const ppu::state_t& v)
{
    es << BeginMap;

    es << Key << "regs" << Value << v.regs;
    es << Key << "vram" << Value << v.vram;

    es << EndMap;
    return es;
}

static void deyamlify(ppu_regs::bus_master_t::bg_t& v, const Node& node)
{
    v.vab = node["vab"].as<unsigned>();
    v.vac = node["vac"].as<unsigned>();
}

static void deyamlify(ppu_regs::bus_master_t::m7_t& v, const Node& node)
{
    v.mpya = node["mpya"].as<unsigned>();
    v.mpyb = node["mpyb"].as<unsigned>();
    v.mpyc = node["mpyc"].as<unsigned>();
}

static void deyamlify(ppu_regs& v, const Node& node)
{
    v.inidisp.bright = node["inidisp.bright"].as<int>();
    v.inidisp.forced_blank = node["inidisp.forced_blank"].as<bool>();
    v.objsel.ba = node["objsel.ba"].as<unsigned>();
    v.objsel.n = node["objsel.n"].as<unsigned>();
    v.objsel.ss = node["objsel.ss"].as<unsigned>();
    v.bgmode.mode = node["bgmode.mode"].as<unsigned>();
    v.bgmode.bg3_pri = node["bgmode.bg3_pri"].as<bool>();
    v.mosaic.sz = node["mosaic.sz"].as<unsigned>();
    {
        const Node& ns = node["mosaic.bg1-4"];
        v.mosaic.bg1 = ns[0].as<bool>();
        v.mosaic.bg2 = ns[1].as<bool>();
        v.mosaic.bg3 = ns[2].as<bool>();
        v.mosaic.bg4 = ns[3].as<bool>();
    }
    v.bg1sc.size = node["bg1sc.size"].as<unsigned>();
    v.bg1sc.base = node["bg1sc.base"].as<unsigned>();
    v.bg2sc.size = node["bg2sc.size"].as<unsigned>();
    v.bg2sc.base = node["bg2sc.base"].as<unsigned>();
    v.bg3sc.size = node["bg3sc.size"].as<unsigned>();
    v.bg3sc.base = node["bg3sc.base"].as<unsigned>();
    v.bg4sc.size = node["bg4sc.size"].as<unsigned>();
    v.bg4sc.base = node["bg4sc.base"].as<unsigned>();
    v.bg12nba.bg1nba = node["bg1nba"].as<unsigned>();
    v.bg12nba.bg2nba = node["bg2nba"].as<unsigned>();
    v.bg34nba.bg3nba = node["bg3nba"].as<unsigned>();
    v.bg34nba.bg4nba = node["bg4nba"].as<unsigned>();
    v.bg1hofs = node["bg1hofs"].as<unsigned>();
    v.m7hofs = node["m7hofs"].as<unsigned>();
    v.bg1vofs = node["bg1vofs"].as<unsigned>();
    v.m7vofs = node["m7vofs"].as<unsigned>();
    v.bg2hofs = node["bg2hofs"].as<unsigned>();
    v.bg2vofs = node["bg2vofs"].as<unsigned>();
    v.bg3hofs = node["bg3hofs"].as<unsigned>();
    v.bg3vofs = node["bg3vofs"].as<unsigned>();
    v.bg4hofs = node["bg4hofs"].as<unsigned>();
    v.bg4vofs = node["bg4vofs"].as<unsigned>();
    v.m7sel.hflip = node["m7sel.hflip"].as<bool>();
    v.m7sel.vflip = node["m7sel.vflip"].as<bool>();
    v.m7sel.scover = node["m7sel.scover"].as<unsigned>();
    v.m7a = node["m7a"].as<unsigned>();
    v.m7b = node["m7b"].as<unsigned>();
    v.m7c = node["m7c"].as<unsigned>();
    v.m7d = node["m7d"].as<unsigned>();
    v.m7x = node["m7x"].as<unsigned>();
    v.m7y = node["m7y"].as<unsigned>();
    v.w12sel.w1_inv_bg1 = node["w1_inv_bg1"].as<bool>();
    v.w12sel.w1_ena_bg1 = node["w1_ena_bg1"].as<bool>();
    v.w12sel.w2_inv_bg1 = node["w2_inv_bg1"].as<bool>();
    v.w12sel.w2_ena_bg1 = node["w2_ena_bg1"].as<bool>();
    v.w12sel.w1_inv_bg2 = node["w1_inv_bg2"].as<bool>();
    v.w12sel.w1_ena_bg2 = node["w1_ena_bg2"].as<bool>();
    v.w12sel.w2_inv_bg2 = node["w2_inv_bg2"].as<bool>();
    v.w12sel.w2_ena_bg2 = node["w2_ena_bg2"].as<bool>();
    v.w34sel.w1_inv_bg3 = node["w1_inv_bg3"].as<bool>();
    v.w34sel.w1_ena_bg3 = node["w1_ena_bg3"].as<bool>();
    v.w34sel.w2_inv_bg3 = node["w2_inv_bg3"].as<bool>();
    v.w34sel.w2_ena_bg3 = node["w2_ena_bg3"].as<bool>();
    v.w34sel.w1_inv_bg4 = node["w1_inv_bg4"].as<bool>();
    v.w34sel.w1_ena_bg4 = node["w1_ena_bg4"].as<bool>();
    v.w34sel.w2_inv_bg4 = node["w2_inv_bg4"].as<bool>();
    v.w34sel.w2_ena_bg4 = node["w2_ena_bg4"].as<bool>();
    v.wobjsel.w1_inv_obj = node["w1_inv_obj"].as<bool>();
    v.wobjsel.w1_ena_obj = node["w1_ena_obj"].as<bool>();
    v.wobjsel.w2_inv_obj = node["w2_inv_obj"].as<bool>();
    v.wobjsel.w2_ena_obj = node["w2_ena_obj"].as<bool>();
    v.wobjsel.w1_inv_clr = node["w1_inv_clr"].as<bool>();
    v.wobjsel.w1_ena_clr = node["w1_ena_clr"].as<bool>();
    v.wobjsel.w2_inv_clr = node["w2_inv_clr"].as<bool>();
    v.wobjsel.w2_ena_clr = node["w2_ena_clr"].as<bool>();
    v.wh0 = node["wh0"].as<unsigned>();
    v.wh1 = node["wh1"].as<unsigned>();
    v.wh2 = node["wh2"].as<unsigned>();
    v.wh3 = node["wh3"].as<unsigned>();
    v.wbglog.bg1 = ppu_regs::wmlog_t(node["wbglog.bg1"].as<unsigned>());
    v.wbglog.bg2 = ppu_regs::wmlog_t(node["wbglog.bg2"].as<unsigned>());
    v.wbglog.bg3 = ppu_regs::wmlog_t(node["wbglog.bg3"].as<unsigned>());
    v.wbglog.bg4 = ppu_regs::wmlog_t(node["wbglog.bg4"].as<unsigned>());
    v.wobjlog.obj = ppu_regs::wmlog_t(node["wobjlog.obj"].as<unsigned>());
    v.wobjlog.clr = ppu_regs::wmlog_t(node["wobjlog.clr"].as<unsigned>());
    {
        const Node& ns = node["tm.bg1-4,obj"];
        v.tm.bg1 = ns[0].as<bool>();
        v.tm.bg2 = ns[1].as<bool>();
        v.tm.bg3 = ns[2].as<bool>();
        v.tm.bg4 = ns[3].as<bool>();
        v.tm.obj = ns[4].as<bool>();
    }
    {
        const Node& ns = node["ts.bg1-4,obj"];
        v.ts.bg1 = ns[0].as<bool>();
        v.ts.bg2 = ns[1].as<bool>();
        v.ts.bg3 = ns[2].as<bool>();
        v.ts.bg4 = ns[3].as<bool>();
        v.ts.obj = ns[4].as<bool>();
    }
    {
        const Node& ns = node["tmw.bg1-4,obj"];
        v.tmw.bg1 = ns[0].as<bool>();
        v.tmw.bg2 = ns[1].as<bool>();
        v.tmw.bg3 = ns[2].as<bool>();
        v.tmw.bg4 = ns[3].as<bool>();
        v.tmw.obj = ns[4].as<bool>();
    }
    {
        const Node& ns = node["tsw.bg1-4,obj"];
        v.tsw.bg1 = ns[0].as<bool>();
        v.tsw.bg2 = ns[1].as<bool>();
        v.tsw.bg3 = ns[2].as<bool>();
        v.tsw.bg4 = ns[3].as<bool>();
        v.tsw.obj = ns[4].as<bool>();
    }
    v.cgwsel.direct = node["cgwsel.direct"].as<bool>();
    v.cgwsel.addend = node["cgwsel.addend"].as<bool>();
    v.cgwsel.sub_win_rgn = node["cgwsel.sub_win_rgn"].as<unsigned>();
    v.cgwsel.main_win_rgn = node["cgwsel.main_win_rgn"].as<unsigned>();
    {
        const Node& ns = node["cgadsub.bg1-4,obj,back"];
        v.cgadsub.bg1 = ns[0].as<bool>();
        v.cgadsub.bg2 = ns[1].as<bool>();
        v.cgadsub.bg3 = ns[2].as<bool>();
        v.cgadsub.bg4 = ns[3].as<bool>();
        v.cgadsub.obj = ns[4].as<bool>();
        v.cgadsub.back = ns[5].as<bool>();
    }
    v.cgadsub.half = node["cgadsub.half"].as<bool>();
    v.cgadsub.sub = node["cgadsub.sub"].as<bool>();
    {
        const Node& ns = node["coldata.rgb"];
        v.coldata.r = ns[0].as<unsigned>();
        v.coldata.g = ns[1].as<unsigned>();
        v.coldata.b = ns[2].as<unsigned>();
    }
    v.setini.interlacing = node["setini.interlacing"].as<bool>();
    v.setini.obj_vdir_disp = node["setini.obj_vdir_disp"].as<bool>();
    v.setini.bg_vdir_disp = node["setini.bg_vdir_disp"].as<bool>();
    v.setini.h512 = node["setini.h512"].as<bool>();
    v.setini.extbg = node["setini.extbg"].as<bool>();
    v.setini.extsync = node["setini.extsync"].as<bool>();
    v.mpylmh = node["mpylmh"].as<unsigned>();
    v.ophct = node["ophct"].as<unsigned>();
    v.opvct = node["opvct"].as<unsigned>();
    v.stat77.range_over = node["stat77.range_over"].as<bool>();
    v.stat77.time_over = node["stat77.time_over"].as<bool>();
    v.stat78.opl = node["stat78.opl"].as<bool>();
    v.stat78.field = node["stat78.field"].as<bool>();
    {
        const Node& ns = node["video_counter"];
        v.video_counter.row = ns["row"].as<unsigned>();
        v.video_counter.col = ns["col"].as<unsigned>();
        v.video_counter.frame = ns["frame"].as<unsigned>();
    }
    {
        const Node& ns = node["bus_master"];
        deyamlify(v.bus_master.bg[1], ns["bg1"]);
        deyamlify(v.bus_master.bg[2], ns["bg2"]);
        deyamlify(v.bus_master.bg[3], ns["bg3"]);
        deyamlify(v.bus_master.bg[4], ns["bg4"]);
        deyamlify(v.bus_master.m7, ns["m7"]);
    }
    deyamlify(buffer(v.cgram), node["cgram"]);
    deyamlify(buffer(v.oaml), node["oaml"]);
    deyamlify(buffer(v.oamh), node["oamh"]);
}

static void deyamlify(ppu::state_t& v, const Node& node)
{
    deyamlify(v.regs, node["regs"]);
    deyamlify(v.vram, node["vram"]);
}

//////////////////////////////////////////////////////////////////////
// cpumem

static Emitter& operator<<(Emitter& es, const cpumem::state_t::last_read_t& v)
{
    es << BeginMap;

    es << Key << "source" << Value << name(v.source);
    es << Key << "addr_off" << Value << Hex << v.addr_off;
    es << Key << "data" << Value << Hex << unsigned(v.data);

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const cpumem::state_t& v)
{
    es << BeginMap;

    es << Key << "wram" << Value << v.wram;
    es << Key << "wmadd" << Value << Hex << v.wmadd;
    es << Key << "last_read" << Value << v.last_read;

    es << EndMap;
    return es;
}

static void deyamlify(cpumem::state_t::last_read_t& v, const Node& node)
{
    v.source = from_name(node["source"].as<std::string>().c_str());
    v.addr_off = node["addr_off"].as<unsigned>();
    v.data = node["data"].as<unsigned>();
}

static void deyamlify(cpumem::state_t& v, const Node& node)
{
    deyamlify(v.wram, node["wram"]);
    v.wmadd = node["wmadd"].as<unsigned>();
    deyamlify(v.last_read, node["last_read"]);
}

//////////////////////////////////////////////////////////////////////
// apu

static Emitter& operator<<(Emitter& es, const apu_smp_state::periph_t& v)
{
    es << BeginMap;

    es << Key << "cpui" << Value << Flow << Hex << BeginSeq
       << unsigned(v.cpui[0]) << unsigned(v.cpui[1])
       << unsigned(v.cpui[2]) << unsigned(v.cpui[3]) << EndSeq;
    es << Key << "cpuo" << Value << Flow << Hex << BeginSeq
       << unsigned(v.cpuo[0]) << unsigned(v.cpuo[1])
       << unsigned(v.cpuo[2]) << unsigned(v.cpuo[3]) << EndSeq;
    es << Key << "spcr" << Value << Hex << unsigned(v.spcr);
    es << Key << "spt" << Value << Hex << Flow << BeginSeq
       << unsigned(v.spt[0]) << unsigned(v.spt[1]) << unsigned(v.spt[2])
       << EndSeq;
    es << Key << "spc" << Value << Hex << Flow << BeginSeq
       << unsigned(v.spc[0]) << unsigned(v.spc[1]) << unsigned(v.spc[2])
       << EndSeq;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_smp_state::internal_t& v)
{
    es << BeginMap;

    es << Key << "cken_d" << Value << v.cken_d;
    es << Key << "cp1d" << Value << v.cp1d;
    es << Key << "resp" << Value << v.resp;
    es << Key << "am_done_d" << Value << v.am_done_d;
    es << Key << "cco" << Value << v.cco;
    es << Key << "cvo" << Value << v.cvo;
    es << Key << "cho" << Value << v.cho;
    es << Key << "cl_store_dor_d" << Value << v.cl_store_dor_d;
    es << Key << "cl_int_op_d" << Value << v.cl_int_op_d;
    es << Key << "t" << Value << Hex << v.t;
    es << Key << "bra_t" << Value << Hex << v.bra_t;
    es << Key << "divcnt" << Value << Hex << v.divcnt;
    es << Key << "am_t" << Value << Hex << v.am_t;
    es << Key << "dl" << Value << Hex << v.dl;
    es << Key << "op_t" << Value << Hex << v.op_t;
    es << Key << "ico" << Value << Hex << v.ico;
    es << Key << "icoh" << Value << Hex << v.icoh;
    es << Key << "mulscnt" << Value << Hex << v.mulscnt;
    es << Key << "timer0" << Value << Hex << v.timer0;
    es << Key << "timer1" << Value << Hex << v.timer1;
    es << Key << "timer2" << Value << Hex << v.timer2;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_smp_state& v)
{
    es << BeginMap;

    es << Key << "pc" << Value << Hex << v.pc;
    es << Key << "pf" << Value;
    {
        const auto& pf = v.pf;
        es << BeginMap
           << Key << "c" << Value << pf.c
           << Key << "z" << Value << pf.z
           << Key << "i" << Value << pf.i
           << Key << "h" << Value << pf.h
           << Key << "b" << Value << pf.b
           << Key << "p" << Value << pf.p
           << Key << "v" << Value << pf.v
           << Key << "n" << Value << pf.n
           << EndMap;
    }
    es << Key << "s" << Value << Hex << unsigned(v.s);
    es << Key << "ac" << Value << Hex << unsigned(v.ac);
    es << Key << "x" << Value << Hex << unsigned(v.x);
    es << Key << "y" << Value << Hex << unsigned(v.y);
    es << Key << "ir" << Value << Hex << unsigned(v.ir);
    es << Key << "aor" << Value << Hex << v.aor;
    es << Key << "dor" << Value << Hex << unsigned(v.dor);
    es << Key << "z" << Value << Hex << unsigned(v.z);
    es << Key << "tstate" << Value;
    {
        const auto& tstate = v.tstate;
        es << BeginMap
           << Key << "ts" << Value << unsigned(tstate.ts)
           << EndMap;
    }
    es << Key << "pstate" << Value;
    {
        const auto& pstate = v.pstate;
        es << BeginMap
           << Key << "rw" << Value << unsigned(pstate.rw)
           << Key << "sync" << Value << unsigned(pstate.sync)
           << EndMap;
    }
    es << Key << "periph" << Value << v.periph;
    es << Key << "internal" << Value << v.internal;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::regs_t& v)
{
    es << Hex << BeginMap;

    for (size_t i = 0; i < 128; i++)
        es << Key << i << Value << unsigned(v.regs[i]);

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           reg_file_t& v)
{
    es << BeginMap;

    es << Key << "kon_written" << Value << v.kon_written;
    es << Key << "flg_quick" << Value << v.flg_quick;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           clkgen_t& v)
{
    es << BeginMap;

    es << Key << "idivcnt" << Value << Hex << v.idivcnt;
    es << Key << "cccnt" << Value << v.cccnt;
    es << Key << "nres" << Value << v.nres;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           smp_if_t& v)
{
    es << BeginMap;

    es << Key << "d_o_d" << Value << Hex << v.d_o_d;
    es << Key << "ccken_d" << Value << v.ccken_d;
    es << Key << "spdrs" << Value << Hex << v.spdrs;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           sram_if_t& v)
{
    es << BeginMap;

    es << Key << "ima" << Value << Hex << v.ima;
    es << Key << "imdi" << Value << Hex << v.imdi;
    es << Key << "imrs" << Value << v.imrs;
    es << Key << "imws" << Value << v.imws;
    es << Key << "mad_ssmp" << Value << v.mad_ssmp;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           glb_ctr_t& v)
{
    es << BeginMap;

    es << Key << "cnt" << Value << Hex << v.cnt;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           sgenloop_t::voice_t::brrdec_t& v)
{
    es << BeginMap;

    es << Key << "ctx" << Value << Flow << Hex << BeginSeq;
    for (size_t i = 0; i < 8; i++)
        es << v.ctx[i];
    es << EndSeq;
    es << Key << "bbcnt" << Value << v.bbcnt;
    es << Key << "srcn0_load" << Value << v.srcn0_load;
    es << Key << "srcn1_load" << Value << v.srcn1_load;
    es << Key << "srcn_inc" << Value << v.srcn_inc;
    es << Key << "rd_dta1" << Value << v.rd_dta1;
    es << Key << "hdr" << Value << Hex << v.hdr;
    es << Key << "dat" << Value << Hex << v.dat;
    es << Key << "rd_brrfsm1" << Value << v.rd_brrfsm1;
    es << Key << "brrfsm1" << Value << v.brrfsm1;
    es << Key << "brrfsm2" << Value << v.brrfsm2;
    es << Key << "dec_buf_full" << Value << v.dec_buf_full;
    es << Key << "srcnc" << Value << Hex << v.srcnc;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           sgenloop_t::voice_t::decbuf_t& v)
{
    es << BeginMap;

    es << Key << "play" << Value << v.play;
    es << Key << "out_samp" << Value << Hex << v.out_samp;
    es << Key << "deccnt" << Value << Flow << BeginSeq;
    for (size_t i = 0; i < 8; i++)
        es << unsigned(v.deccnt[i]);
    es << EndSeq;
    es << Key << "intcnt" << Value << Flow << BeginSeq;
    for (size_t i = 0; i < 8; i++)
        es << unsigned(v.intcnt[i]);
    es << EndSeq;
    es << Key << "mem" << Value << Hex << BeginMap;
    for (size_t i = 0; i < 0x60; i++)
        es << Key << i << v.mem[i];
    es << EndMap;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           sgenloop_t::voice_t::interp_t& v)
{
    es << BeginMap;

    es << Key << "ctx" << Value << Flow << Hex << BeginSeq;
    for (size_t i = 0; i < 8; i++)
        es << v.ctx[i];
    es << EndSeq;
    es << Key << "pcnt" << Value << Hex << v.pcnt;
    es << Key << "pstep" << Value << Hex << v.pstep;
    es << Key << "acc" << Value << Hex << v.acc;
    es << Key << "gos" << Value << Hex << v.gos;
    es << Key << "sout" << Value << Hex << v.sout;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           sgenloop_t::voice_t::env_t& v)
{
    es << BeginMap;

    es << Key << "ctx" << Value << Flow << Hex << BeginSeq;
    for (size_t i = 0; i < 8; i++)
        es << v.ctx[i];
    es << EndSeq;
    es << Key << "envelope" << Value << Hex << v.envelope;
    es << Key << "adsr_st" << Value << v.adsr_st;
    es << Key << "ctx_swap" << Value << v.ctx_swap;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           sgenloop_t::voice_t::scale_pan_t& v)
{
    es << BeginMap;

    es << Key << "vml" << Value << Hex << v.vml;
    es << Key << "vmr" << Value << Hex << v.vmr;
    es << Key << "out" << Value << Hex << v.out;
    es << Key << "outl" << Value << Hex << v.outl;
    es << Key << "outr" << Value << Hex << v.outr;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           sgenloop_t::voice_t& v)
{
    es << BeginMap;

    es << Key << "srcn" << Value << Hex << unsigned(v.srcn);
    es << Key << "srcnd" << Value << Hex << unsigned(v.srcnd);
    es << Key << "pitch" << Value << Hex << unsigned(v.pitch);
    es << Key << "adsr1" << Value << Hex << unsigned(v.adsr1);
    es << Key << "adsr2_gain" << Value << Hex << unsigned(v.adsr2_gain);
    es << Key << "voll" << Value << Hex << unsigned(v.voll);
    es << Key << "volr" << Value << Hex << unsigned(v.volr);
    es << Key << "envx" << Value << Hex << unsigned(v.envx);

    es << Key << "brrdec" << Value << v.brrdec;
    es << Key << "decbuf" << Value << v.decbuf;
    es << Key << "interp" << Value << v.interp;
    es << Key << "env" << Value << v.env;
    es << Key << "scale_pan" << Value << v.scale_pan;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           sgenloop_t::echo_t::mixer_t& v)
{
    es << BeginMap;

    es << Key << "suml" << Value << Hex << v.suml;
    es << Key << "sumr" << Value << Hex << v.sumr;
    es << Key << "soutl" << Value << Hex << v.soutl;
    es << Key << "soutr" << Value << Hex << v.soutr;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           sgenloop_t::echo_t::fir_t& v)
{
    es << BeginMap;

    es << Key << "suml" << Value << Hex << v.suml;
    es << Key << "sp" << Value << v.sp;
    es << Key << "sumr" << Value << Hex << v.sumr;
    es << Key << "sl" << Value << Flow << Hex << BeginSeq;
    for (size_t i = 0; i < 8; i++)
        es << v.sl[i];
    es << EndSeq;
    es << Key << "sr" << Value << Flow << Hex << BeginSeq;
    for (size_t i = 0; i < 8; i++)
        es << v.sr[i];
    es << EndSeq;
    es << Key << "soutl" << Value << Hex << v.soutl;
    es << Key << "soutr" << Value << Hex << v.soutr;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           sgenloop_t::echo_t& v)
{
    es << BeginMap;

    es << Key << "efb" << Value << Hex << unsigned(v.efb);
    es << Key << "eon" << Value << Hex << unsigned(v.eon);
    es << Key << "edl" << Value << Hex << unsigned(v.edl);
    es << Key << "esa" << Value << Hex << unsigned(v.esa);
    es << Key << "fcrs" << Value << v.fcrs;
    es << Key << "fcls" << Value << v.fcls;
    es << Key << "fcl" << Value << v.fcl;
    es << Key << "idx" << Value << Hex << v.idx;
    es << Key << "ece" << Value << Hex << v.ece;
    es << Key << "idx_max" << Value << Hex << v.idx_max;
    es << Key << "buf_ptr" << Value << Hex << v.buf_ptr;
    es << Key << "eboutl" << Value << Hex << v.eboutl;
    es << Key << "eboutr" << Value << Hex << v.eboutr;
    es << Key << "mixer" << Value << v.mixer;
    es << Key << "fir" << Value << v.fir;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           sgenloop_t::out_mixer_t& v)
{
    es << BeginMap;

    es << Key << "suml" << Value << Hex << v.suml;
    es << Key << "sumr" << Value << Hex << v.sumr;
    es << Key << "soutl" << Value << Hex << v.soutl;
    es << Key << "soutr" << Value << Hex << v.soutr;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t::
                           sgenloop_t& v)
{
    es << BeginMap;

    es << Key << "sglcnt" << Value << v.sglcnt;
    es << Key << "pmon" << Value << Hex << unsigned(v.pmon);
    es << Key << "non" << Value << Hex << unsigned(v.non);
    es << Key << "dir" << Value << Hex << unsigned(v.dir);
    es << Key << "kon" << Value << Hex << unsigned(v.kon);
    es << Key << "kof" << Value << Hex << unsigned(v.kof);
    es << Key << "mvoll" << Value << Hex << unsigned(v.mvoll);
    es << Key << "mvolr" << Value << Hex << unsigned(v.mvolr);
    es << Key << "evoll" << Value << Hex << unsigned(v.evoll);
    es << Key << "evolr" << Value << Hex << unsigned(v.evolr);
    es << Key << "flgn" << Value << Hex << unsigned(v.flgn);
    es << Key << "noise_lfsr" << Value << Hex << v.noise_lfsr;
    es << Key << "voice" << Value << v.voice;
    es << Key << "echo" << Value << v.echo;
    es << Key << "out_mixer" << Value << v.out_mixer;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state::internal_t& v)
{
    es << BeginMap;

    es << Key << "reg_file" << Value << v.reg_file;
    es << Key << "clkgen" << Value << v.clkgen;
    es << Key << "smp_if" << Value << v.smp_if;
    es << Key << "sram_if" << Value << v.sram_if;
    es << Key << "glb_ctr" << Value << v.glb_ctr;
    es << Key << "sgenloop" << Value << v.sgenloop;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu_dsp_state& v)
{
    es << BeginMap;

    es << Key << "regs" << Value << v.regs;
    es << Key << "internal" << Value << v.internal;

    es << EndMap;
    return es;
}

static Emitter& operator<<(Emitter& es, const apu::state_t& v)
{
    es << BeginMap;

    es << Key << "smp" << Value << v.smp;
    es << Key << "dsp" << Value << v.dsp;
    es << Key << "ram" << Value << v.ram;

    es << EndMap;
    return es;
}

static void deyamlify(apu_smp_state::periph_t& v, const Node& node)
{
    {
        const auto& node_cpui = node["cpui"];
        for (size_t i = 0; i < 4; i++)
            v.cpui[i] = node_cpui[i].as<unsigned>();
    }
    {
        const auto& node_cpuo = node["cpuo"];
        for (size_t i = 0; i < 4; i++)
            v.cpuo[i] = node_cpuo[i].as<unsigned>();
    }
    v.spcr = node["spcr"].as<unsigned>();
    {
        const auto& node_spt = node["spt"];
        for (size_t i = 0; i < 3; i++)
            v.spt[i] = node_spt[i].as<unsigned>();
    }
    {
        const auto& node_spc = node["spc"];
        for (size_t i = 0; i < 3; i++)
            v.spc[i] = node_spc[i].as<unsigned>();
    }
}

static void deyamlify(apu_smp_state::internal_t& v, const Node& node)
{
    v.cken_d = node["cken_d"].as<unsigned>();
    v.cp1d = node["cp1d"].as<unsigned>();
    v.resp = node["resp"].as<unsigned>();
    v.am_done_d = node["am_done_d"].as<unsigned>();
    v.cco = node["cco"].as<unsigned>();
    v.cvo = node["cvo"].as<unsigned>();
    v.cho = node["cho"].as<unsigned>();
    v.cl_store_dor_d = node["cl_store_dor_d"].as<unsigned>();
    v.cl_int_op_d = node["cl_int_op_d"].as<unsigned>();
    v.t = node["t"].as<unsigned>();
    v.bra_t = node["bra_t"].as<unsigned>();
    v.divcnt = node["divcnt"].as<unsigned>();
    v.am_t = node["am_t"].as<unsigned>();
    v.dl = node["dl"].as<unsigned>();
    v.op_t = node["op_t"].as<unsigned>();
    v.ico = node["ico"].as<unsigned>();
    v.icoh = node["icoh"].as<unsigned>();
    v.mulscnt = node["mulscnt"].as<unsigned>();
    v.timer0 = node["timer0"].as<unsigned>();
    v.timer1 = node["timer1"].as<unsigned>();
    v.timer2 = node["timer2"].as<unsigned>();
}

static void deyamlify(apu_smp_state& v, const Node& node)
{
    v.pc = node["pc"].as<unsigned>();
    {
        auto& pf = v.pf;
        const Node& ns = node["pf"];

        pf.c = ns["c"].as<bool>();
        pf.z = ns["z"].as<bool>();
        pf.i = ns["i"].as<bool>();
        pf.h = ns["h"].as<bool>();
        pf.b = ns["b"].as<bool>();
        pf.p = ns["p"].as<bool>();
        pf.v = ns["v"].as<bool>();
        pf.n = ns["n"].as<bool>();
    }
    v.s = node["s"].as<unsigned>();
    v.ac = node["ac"].as<unsigned>();
    v.x = node["x"].as<unsigned>();
    v.y = node["y"].as<unsigned>();
    v.ir = node["ir"].as<unsigned>();
    v.aor = node["aor"].as<unsigned>();
    v.dor = node["dor"].as<unsigned>();
    v.z = node["z"].as<unsigned>();
    {
        auto& tstate = v.tstate;
        const Node& ns = node["tstate"];
        tstate.ts = ns["ts"].as<unsigned>();
    }
    {
        auto& pstate = v.pstate;
        const Node& ns = node["pstate"];
        pstate.rw = ns["rw"].as<unsigned>();
        pstate.sync = ns["sync"].as<unsigned>();
    }
    deyamlify(v.periph, node["periph"]);
    deyamlify(v.internal, node["internal"]);
}

static void deyamlify(apu_dsp_state::regs_t& v, const Node& node)
{
    for (size_t i = 0; i < 128; i++)
        v.regs[i] = node[i].as<unsigned>();
}

static void deyamlify(apu_dsp_state::internal_t::reg_file_t& v,
                      const Node& node)
{
    v.kon_written = node["kon_written"].as<unsigned>();
    v.flg_quick = node["flg_quick"].as<unsigned>();
}

static void deyamlify(apu_dsp_state::internal_t::clkgen_t& v,
                      const Node& node)
{
    v.idivcnt = node["idivcnt"].as<unsigned>();
    v.cccnt = node["cccnt"].as<unsigned>();
    v.nres = node["nres"].as<unsigned>();
}

static void deyamlify(apu_dsp_state::internal_t::smp_if_t& v,
                      const Node& node)
{
    v.d_o_d = node["d_o_d"].as<unsigned>();
    v.ccken_d = node["ccken_d"].as<unsigned>();
    v.spdrs = node["spdrs"].as<unsigned>();
}

static void deyamlify(apu_dsp_state::internal_t::sram_if_t& v,
                      const Node& node)
{
    v.ima = node["ima"].as<unsigned>();
    v.imdi = node["imdi"].as<unsigned>();
    v.imrs = node["imrs"].as<unsigned>();
    v.imws = node["imws"].as<unsigned>();
    v.mad_ssmp = node["mad_ssmp"].as<unsigned>();
}

static void deyamlify(apu_dsp_state::internal_t::glb_ctr_t& v,
                      const Node& node)
{
    v.cnt = node["cnt"].as<unsigned>();
}

static void deyamlify(apu_dsp_state::internal_t::sgenloop_t::
                      voice_t::brrdec_t& v, const Node& node)
{
    {
        const auto& n = node["ctx"];
        for (size_t i = 0; i < 8; i++)
            v.ctx[i] = n[i].as<unsigned>();
    }
    v.bbcnt = node["bbcnt"].as<unsigned>();
    v.srcn0_load = node["srcn0_load"].as<unsigned>();
    v.srcn1_load = node["srcn1_load"].as<unsigned>();
    v.srcn_inc = node["srcn_inc"].as<unsigned>();
    v.rd_dta1 = node["rd_dta1"].as<unsigned>();
    v.hdr = node["hdr"].as<unsigned>();
    v.dat = node["dat"].as<unsigned>();
    v.rd_brrfsm1 = node["rd_brrfsm1"].as<unsigned>();
    v.brrfsm1 = node["brrfsm1"].as<unsigned>();
    v.brrfsm2 = node["brrfsm2"].as<unsigned>();
    v.dec_buf_full = node["dec_buf_full"].as<unsigned>();
    v.srcnc = node["srcnc"].as<unsigned>();
}

static void deyamlify(apu_dsp_state::internal_t::sgenloop_t::
                      voice_t::decbuf_t& v, const Node& node)
{
    v.play = node["play"].as<unsigned>();
    v.out_samp = node["out_samp"].as<unsigned>();
    {
        const auto& n = node["deccnt"];
        for (size_t i = 0; i < 8; i++)
            v.deccnt[i] = n[i].as<unsigned>();
    }
    {
        const auto& n = node["intcnt"];
        for (size_t i = 0; i < 8; i++)
            v.intcnt[i] = n[i].as<unsigned>();
    }
    {
        const auto& n = node["mem"];
        for (size_t i = 0; i < 0x60; i++)
            v.mem[i] = n[i].as<unsigned>();
    }
}

static void deyamlify(apu_dsp_state::internal_t::sgenloop_t::
                      voice_t::interp_t& v, const Node& node)
{
    {
        const auto& n = node["ctx"];
        for (size_t i = 0; i < 8; i++)
            v.ctx[i] = n[i].as<unsigned>();
    }
    v.pcnt = node["pcnt"].as<unsigned>();
    v.pstep = node["pstep"].as<unsigned>();
    v.acc = node["acc"].as<unsigned>();
    v.gos = node["gos"].as<unsigned>();
    v.sout = node["sout"].as<unsigned>();
}

static void deyamlify(apu_dsp_state::internal_t::sgenloop_t::
                      voice_t::env_t& v, const Node& node)
{
    {
        const auto& n = node["ctx"];
        for (size_t i = 0; i < 8; i++)
            v.ctx[i] = n[i].as<unsigned>();
    }
    v.envelope = node["envelope"].as<unsigned>();
    v.adsr_st = node["adsr_st"].as<unsigned>();
    v.ctx_swap = node["ctx_swap"].as<unsigned>();
}

static void deyamlify(apu_dsp_state::internal_t::sgenloop_t::
                      voice_t::scale_pan_t& v, const Node& node)
{
    v.vml = node["vml"].as<unsigned>();
    v.vmr = node["vmr"].as<unsigned>();
    v.out = node["out"].as<unsigned>();
    v.outl = node["outl"].as<unsigned>();
    v.outr = node["outr"].as<unsigned>();
}

static void deyamlify(apu_dsp_state::internal_t::sgenloop_t::
                      voice_t& v, const Node& node)
{
    v.srcn = node["srcn"].as<unsigned>();
    v.srcnd = node["srcnd"].as<unsigned>();
    v.pitch = node["pitch"].as<unsigned>();
    v.adsr1 = node["adsr1"].as<unsigned>();
    v.adsr2_gain = node["adsr2_gain"].as<unsigned>();
    v.voll = node["voll"].as<unsigned>();
    v.volr = node["volr"].as<unsigned>();
    v.envx = node["envx"].as<unsigned>();

    deyamlify(v.brrdec, node["brrdec"]);
    deyamlify(v.decbuf, node["decbuf"]);
    deyamlify(v.interp, node["interp"]);
    deyamlify(v.env, node["env"]);
    deyamlify(v.scale_pan, node["scale_pan"]);
}

static void deyamlify(apu_dsp_state::internal_t::sgenloop_t::
                      echo_t::mixer_t& v, const Node& node)
{
    v.suml = node["suml"].as<unsigned>();
    v.sumr = node["sumr"].as<unsigned>();
    v.soutl = node["soutl"].as<unsigned>();
    v.soutr = node["soutr"].as<unsigned>();
}

static void deyamlify(apu_dsp_state::internal_t::sgenloop_t::
                      echo_t::fir_t& v, const Node& node)
{
    v.suml = node["suml"].as<unsigned>();
    v.sp = node["sp"].as<unsigned>();
    v.sumr = node["sumr"].as<unsigned>();
    {
        const auto& n = node["sl"];
        for (size_t i = 0; i < 8; i++)
            v.sl[i] = n[i].as<unsigned>();
    }
    {
        const auto& n = node["sr"];
        for (size_t i = 0; i < 8; i++)
            v.sr[i] = n[i].as<unsigned>();
    }
    v.soutl = node["soutl"].as<unsigned>();
    v.soutr = node["soutr"].as<unsigned>();
}

static void deyamlify(apu_dsp_state::internal_t::sgenloop_t::
                      echo_t& v, const Node& node)
{
    v.efb = node["efb"].as<unsigned>();
    v.eon = node["eon"].as<unsigned>();
    v.edl = node["edl"].as<unsigned>();
    v.esa = node["esa"].as<unsigned>();
    v.fcrs = node["fcrs"].as<unsigned>();
    v.fcls = node["fcls"].as<unsigned>();
    v.fcl = node["fcl"].as<unsigned>();
    v.idx = node["idx"].as<unsigned>();
    v.ece = node["ece"].as<unsigned>();
    v.idx_max = node["idx_max"].as<unsigned>();
    v.buf_ptr = node["buf_ptr"].as<unsigned>();
    v.eboutl = node["eboutl"].as<unsigned>();
    v.eboutr = node["eboutr"].as<unsigned>();

    deyamlify(v.mixer, node["mixer"]);
    deyamlify(v.fir, node["fir"]);
}

static void deyamlify(apu_dsp_state::internal_t::sgenloop_t::
                      out_mixer_t& v, const Node& node)
{
    v.suml = node["suml"].as<unsigned>();
    v.sumr = node["sumr"].as<unsigned>();
    v.soutl = node["soutl"].as<unsigned>();
    v.soutr = node["soutr"].as<unsigned>();
}

static void deyamlify(apu_dsp_state::internal_t::sgenloop_t& v,
                      const Node& node)
{
    v.sglcnt = node["sglcnt"].as<unsigned>();
    v.pmon = node["pmon"].as<unsigned>();
    v.non = node["non"].as<unsigned>();
    v.dir = node["dir"].as<unsigned>();
    v.kon = node["kon"].as<unsigned>();
    v.kof = node["kof"].as<unsigned>();
    v.mvoll = node["mvoll"].as<unsigned>();
    v.mvolr = node["mvolr"].as<unsigned>();
    v.evoll = node["evoll"].as<unsigned>();
    v.evolr = node["evolr"].as<unsigned>();
    v.flgn = node["flgn"].as<unsigned>();
    v.noise_lfsr = node["noise_lfsr"].as<unsigned>();

    deyamlify(v.voice, node["voice"]);
    deyamlify(v.echo, node["echo"]);
    deyamlify(v.out_mixer, node["out_mixer"]);
}

static void deyamlify(apu_dsp_state::internal_t& v, const Node& node)
{
    deyamlify(v.reg_file, node["reg_file"]);
    deyamlify(v.clkgen, node["clkgen"]);
    deyamlify(v.smp_if, node["smp_if"]);
    deyamlify(v.sram_if, node["sram_if"]);
    deyamlify(v.glb_ctr, node["glb_ctr"]);
    deyamlify(v.sgenloop, node["sgenloop"]);
}

static void deyamlify(apu_dsp_state& v, const Node& node)
{
    deyamlify(v.regs, node["regs"]);
    deyamlify(v.internal, node["internal"]);
}

static void deyamlify(apu::state_t& v, const Node& node)
{
    deyamlify(v.smp, node["smp"]);
    deyamlify(v.dsp, node["dsp"]);
    deyamlify(v.ram, node["ram"]);
}

//////////////////////////////////////////////////////////////////////
// cart

static Emitter& operator<<(Emitter& es, const cart::state_t& v)
{
    es << BeginMap;

    es << Key << "loaded" << Value << v.loaded;
    es << Key << "sram" << Value << v.sram;

    es << EndMap;
    return es;
}

static void deyamlify(cart::state_t& v, const Node& node)
{
    v.loaded = node["loaded"].as<bool>();
    deyamlify(v.sram, node["sram"]);
}

//////////////////////////////////////////////////////////////////////
// Root

static void yamlify_emu_versions(Emitter& es, const sys& sys)
{
    es << BeginMap;

    es << Key << "SW" << Value;
    {
        es << BeginMap
           << Key << "branch" << Value << sw_build_branch
           << Key << "rev" << Value << sw_build_rev
           << Key << "timestamp" << Value << sw_build_timestamp
           << EndMap;
    }
    es << Key << "HW" << Value;
    {
        auto hw_ver = sys.plat_mgr.get_rtl_version();
        es << BeginMap
           << Key << "branch" << Value << hw_ver.git_branch
           << Key << "rev" << Value << hw_ver.git_rev
           << Key << "timestamp" << Value << hw_ver.timestamp
           << EndMap;
    }

    es << EndMap;
}

void yamlify_set_printable(bool e)
{
    printable = e;
}

void yamlify_sys_state(sys& sys, const sys::state_t& st, std::ostream& os)
{
    Emitter es(os);

    es << Comment("SNES save state");

    es << BeginMap;

    es << Key << "emu versions" << Value;
    yamlify_emu_versions(es, sys);

    es << Key << "cpu" << Value << st._cpu;
    es << Key << "ppu" << Value << st._ppu;
    es << Key << "cpumem" << Value << st._cpumem;
    es << Key << "apu" << Value << st._apu;
    es << Key << "cart" << Value << st._cart;

    es << EndMap;
    es << Newline; // terminate last line

    es << Comment("EOF") << Newline;
}

void deyamlify_sys_state(std::istream& is, sys::state_t& st)
{
    Node root = YAML::Load(is);

    deyamlify(st._cpu, root["cpu"]);
    deyamlify(st._ppu, root["ppu"]);
    deyamlify(st._cpumem, root["cpumem"]);
    deyamlify(st._apu, root["apu"]);
    deyamlify(st._cart, root["cart"]);
}

} // namespace snes
