#pragma once

#include <stdexcept>
#include <memory>
#include "../crt.h"

namespace snes {

class cpumem;
class cart;

namespace cart_loader {

struct cart_loader_exception : public std::runtime_error
{
    cart_loader_exception(const char* what_arg) : std::runtime_error(what_arg) {}
};

class cart_loader
{
public:
    using PCrt = std::shared_ptr<Crt>;

    void init();

    cart_loader(PCrt&, cpumem&, cart&);
    virtual ~cart_loader();

    virtual void load();
    virtual void save();
    virtual void reset_ram(bool force_zero = false);

protected:
    void reset();

    PCrt _crt;
    cpumem& _cpumem;
    cart& _cart;
};

} // namespace cart_loader
} // namespace snes
