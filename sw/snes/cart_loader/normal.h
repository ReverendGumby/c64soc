#pragma once

/* Loader for a normal cartridge */

#include <stdexcept>
#include <stdint.h>
#include "cart_loader.h"

namespace snes {
namespace cart_loader {

class normal : public cart_loader
{
public:
    normal(PCrt&, cpumem&, cart&);

    void load() override;
    void save() override;
    void reset_ram(bool force_zero) override;

private:
};

} // namespace cart_loader
} // namespace snes
