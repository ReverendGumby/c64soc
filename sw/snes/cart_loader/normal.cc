#include "normal.h"

#include "internal.h"

#include <algorithm>
#include <stdint.h>
#include <util/iomem.h>
#include <util/math.h>
#include <string.h>

namespace snes {
namespace cart_loader {

normal::normal(PCrt& p, cpumem& m, cart& c)
    : cart_loader(p, m, c)
{
}

void normal::load()
{
    DBG_PRINTF(1, "entry\n");
    auto& crt = *_crt;

    cart::map_cfg_t map_cfg {};

    auto &chips = crt.chips();
    for (auto &c : chips)
    {
        auto len = c.len();
        auto type = c.type();
        auto mask = (1 << ceil_log2(len)) - 1;

        switch (type) {
        case Chip::ROM: {
            if (len > _cart.rom_len)
                throw cart_loader_exception("unsupported ROM size");
            memcpy(_cart.get_rom(), c.data(), len);
            map_cfg.rom_mask = mask;
            break;
        }
        case Chip::SRAM:
            if (c.data()) {
                memcpy(_cart.get_sram(), c.data(), len);
            }
            map_cfg.sram_mask = mask;
            map_cfg.sram_enable = true;
            break;
        default:
            throw cart_loader_exception("unsupported chip type");
        }
    }

    map_cfg.mem_map = crt.map_mode();
    _cart.set_map_cfg(map_cfg);
}

void normal::save()
{
    DBG_PRINTF(1, "entry\n");
    auto& crt = *_crt;
    auto &chips = crt.chips();
    for (auto &c : chips)
    {
        if (c.type() == Chip::SRAM && c.data() != nullptr) {
            memcpy(c.data(), _cart.get_sram(), c.len());
        }
    }
}

void normal::reset_ram(bool force_zero)
{
    DBG_PRINTF(1, "entry\n");
    auto& crt = *_crt;
    auto &chips = crt.chips();
    for (auto &c : chips)
    {
        if (c.type() == Chip::SRAM) {
            if (force_zero || c.data() == nullptr) {
                memset(_cart.get_sram(), 0, c.len());
            } else {
                memcpy(c.data(), _cart.get_sram(), c.len());
            }
        }
    }
}

} // namespace cart_loader
} // namespace snes
