#include "cart_loader.h"
#include "internal.h"

namespace snes {
namespace cart_loader {

void cart_loader::init()
{
    reset();
}

void cart_loader::reset()
{
    _cart.reset();
}

cart_loader::cart_loader(PCrt& crt, cpumem& cpumem, cart& cart)
    : _crt(crt), _cpumem(cpumem), _cart(cart)
{
    DBG_PRINTF(2, "\n");
}

cart_loader::~cart_loader()
{
    DBG_PRINTF(2, "\n");
}

void cart_loader::load()
{
}

void cart_loader::save()
{
}

void cart_loader::reset_ram(bool force_zero)
{
}

} // namespace cart_loader
} // namespace snes
