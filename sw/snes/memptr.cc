#include <stdexcept>
#include "util/string.h"
#include "util/debug.h"
#include "sys.h"
#include "memptr.h"

#define DBG_LVL 1
#define DBG_TAG "MEMP"

namespace snes {

class sys* sys;

const char* name(memtype e)
{
    switch (e)
    {
    case memtype::cpu: return "cpu";
    case memtype::ppu: return "ppu";
    case memtype::apu: return "apu";
    }
    return "???";
}

void memptr::attach_sys(class sys& in_sys)
{
    sys = &in_sys;
}

std::string memptr::to_string() const
{
    return string_printf("(%06X,%s)", (unsigned)addr(), name(type()));
}

memval memptr::operator*() const
{
    return memval{*this};
}

memval memptr::operator[](int i) const
{
    auto temp = *this + i;
    return *temp;
}

volatile uint8_t* memval::get_reg() const
{
    auto type = _p.type();
    if (type == memtype::cpu) {
        auto addr = _p.addr();
        auto* ptr = sys->cpumem.addr_to_ptr(addr);
        if (ptr == nullptr)
            throw memptr_exception("invalid address");
        return ptr;
    }
    else if (type == memtype::ppu) {
        return EMUBUS_PPU_VRAM + _p.addr();
    }
    else if (type == memtype::apu) {
        return EMUBUS_APU_MEM + _p.addr();
    }
    else
        throw memptr_exception("bad type");
}

memval::operator uint8_t()
{
    auto v = *get_reg();
    DBG_PRINTF_NOFUNC(10, "R [%s] = %02X\n", _p.to_string().c_str(), (unsigned)v);
    return v;
}

memval& memval::operator=(uint8_t v)
{
    DBG_PRINTF_NOFUNC(10, "W [%s] = %02X\n", _p.to_string().c_str(), (unsigned)v);
    *get_reg() = v;
    return *this;
}

memval& memval::operator^=(uint8_t w)
{
    uint8_t v = *this;
    *this = v ^ w;
    return *this;
}

} // namespace snes
