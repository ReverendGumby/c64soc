#include "sys.h"
#include "cart.h"

#include <cli/command.h>
#include <fat/path.h>
#include <fat/filesystem.h>

#include <iostream>

#include "cart_manager.h"

// NB: 'ptimer _save_tmr' isn't thread-safe, and CLI and timers are in different
// threads. Be careful not to race loading with saving.

namespace snes {

using namespace ::cli;

static void crt_info(snes::Crt& crt)
{
    std::cout << "Found ROM data spec: "
              << (crt.found_rom_data_spec() ? "yes" : "no") << '\n'
              << "ROM size: " << crt.chip_size_kb(snes::Chip::ROM) << " KB, "
              << "SRAM size: " << crt.chip_size_kb(snes::Chip::SRAM) << " KB, "
              << "Map mode: " << snes::name(crt.map_mode()) << ", "
              << "Battery: " << (crt.has_battery() ? "yes" : "no")
              << '\n';
}

static snes::cart::PCrt read_cart(const fat::path& p)
{
    fat::file f{p};
    auto s = f.size();
    snes::Crt::Buf buf{new uint8_t[s]};
    f.read(&buf[0], s);

    snes::cart::PCrt crt{new snes::Crt{std::move(buf), (size_t)s}};
    return crt;
}

static fat::path sav_path(const fat::path& cart_path)
{
    fat::path p = cart_path;
    p.replace_extension(fat::path { L".sav" });
    return p;
}

static std::pair<snes::Crt::Buf, size_t> read_sav(const fat::path& p)
{
    std::pair<snes::Crt::Buf, size_t> ret;
    if (fat::exists(p)) {
        fat::file f { p };
        auto s = f.size();
        std::cout << "Loading " << s << " bytes from " << p << std::endl;
        auto buf = snes::Crt::Buf { new uint8_t[s] };
        f.read(&buf[0], s);
        ret.first = std::move(buf);
        ret.second = s;
    }
    return ret;
}

cart_manager::cart_manager(sys& sys)
    : _sys(sys), _cart_inserted(false)
{
    _save_tmr.duration = 2 * timer::sec;
    _save_tmr.action = [this]() { save_cart(); };

    command::define("cart", [this](const command::args_t& args) {
        return cli_cart(args); });
    command::define("save", [this](const command::args_t& args) {
        return cli_save(args); });

    _sys.cart.talker<cart::listener>::add_listener(this);
}

void cart_manager::load_cart(const fat::path& p)
{
    auto sav_p = sav_path(p);

    std::unique_lock<std::recursive_mutex> lck { _mtx };

    auto crt = read_cart(p);
    auto sav = read_sav(sav_p);
    if (sav.first)
        crt->load_sram(std::move(sav.first), sav.second);
    insert_cart(crt);

    _cart_path = p;
    _cart_sav_path = sav_p;
}

size_t cart_manager::save_cart()
{
    std::unique_lock<std::recursive_mutex> lck { _mtx };

    size_t len = 0;
    const auto& sav_p = _cart_sav_path;
    if (_sys.cart.can_save() && !sav_p.empty()) {
        auto crt = _sys.cart.save();
        snes::Crt::Buf bin;
        crt->save_sram(bin, len);
        if (bin) {
            auto f { fat::open(sav_p, fat::omode::w, fat::oflags::create) };
            f.write(&bin[0], len);
        }
    }
    return len;
}

void cart_manager::insert_cart(snes::cart::PCrt& crt)
{
    remove_cart();

    crt_info(*crt);

    _sys.reset_snes(true);
    _sys.cart.load(crt);
    _sys.reset_snes(false);

    _cart_inserted = true;
}

void cart_manager::remove_cart()
{
    if (_cart_inserted)
    {
        _cart_inserted = false;
        _cart_path.clear();
        _cart_sav_path.clear();
        _save_tmr.cancel();

        _sys.reset_snes(true);
        _sys.cart.unload();
    }
}

void cart_manager::cart_modified(bool b)
{
    std::unique_lock<std::recursive_mutex> lck { _mtx };

    if (b)
        _save_tmr.start();
}

int cart_manager::cli_cart(const command::args_t& args)
{
    if (args.size() < 1)
        throw std::range_error("missing filename");

    const char* fname = args[0].c_str();
    
    try {
        fat::path p {fname};
        load_cart(p);
    }
    catch (std::runtime_error& e) {
        std::cout << e.what() << std::endl;
    }

    return 0;
}

int cart_manager::cli_save(const command::args_t&)
{
    auto len = save_cart();

    if (len)
        std::cout << "Saved " << len << " bytes to " << _cart_sav_path << std::endl;

    return 0;
}

} // namespace snes
