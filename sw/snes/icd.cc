#include <unistd.h>
#include "emubus.h"
#include "util/timer.h"

#include "icd.h"

namespace snes {

void icd::init()
{
    for (int i = 0, num = 1; i < num; i++) {
        match_units.emplace_back(i);
        auto& match = match_units.back();
        num = match.get_status().num_match;
    }

    reset();
}

void icd::reset()
{
    EMUBUS_ICD_CTRL = 0;
    EMUBUS_ICD_CTRL = EMUBUS_ICD_CTRL_SYS_ENABLE;
}

uint32_t icd::read_cpu_reg(uint8_t reg)
{
    EMUBUS_CPU_CORE_MON_SEL = reg;
    uint32_t val = EMUBUS_CPU_CORE_MON_DATA;
    return val;
}

void icd::write_cpu_reg(uint8_t reg, uint32_t val)
{
    EMUBUS_CPU_CORE_MON_SEL = reg;
    EMUBUS_CPU_CORE_MON_DATA = val;
}

#define XTRACT1(reg, name)      (((reg) & name) != 0)
#define XTRACTN(reg, name)      (((reg) & name) >> name##_S)

icd::cpu_regs icd::read_cpu_regs()
{
    auto arc0 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_ARC0);
    auto arc1 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_ARC1);
    auto arc2 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_ARC2);
    auto arc3 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_ARC3);
    auto uar0 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_UAR0);
    auto uar1 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_UAR1);
    auto ctcs = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_CTCS);
    auto cps  = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_CPS);
    auto int0 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT0);
    auto int1 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT1);
    auto int4 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT4);
    auto int5 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT5);
    auto int6 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT6);
    auto int7 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT7);
    auto int8 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT8);

    cpu_regs ret;
    ret.pc = XTRACTN(arc0, EMUBUS_CPU_CORE_MON_ARC0_PC);
    ret.pf.c = XTRACT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_C);
    ret.pf.z = XTRACT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_Z);
    ret.pf.i = XTRACT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_I);
    ret.pf.d = XTRACT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_D);
    ret.pf.x = XTRACT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_X_B);
    ret.pf.m = XTRACT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_M);
    ret.pf.v = XTRACT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_V);
    ret.pf.n = XTRACT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_N);
    ret.e = XTRACT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_E);
    ret.s = XTRACTN(arc1, EMUBUS_CPU_CORE_MON_ARC1_S);
    ret.a = XTRACTN(arc1, EMUBUS_CPU_CORE_MON_ARC1_AC);
    ret.d = XTRACTN(arc2, EMUBUS_CPU_CORE_MON_ARC2_D);
    ret.x = XTRACTN(arc2, EMUBUS_CPU_CORE_MON_ARC2_X);
    ret.y = XTRACTN(arc3, EMUBUS_CPU_CORE_MON_ARC3_Y);
    ret.dbr = XTRACTN(arc3, EMUBUS_CPU_CORE_MON_ARC3_DBR);
    ret.pbr = XTRACTN(arc3, EMUBUS_CPU_CORE_MON_ARC3_PBR);

    ret.ir = XTRACTN(uar0, EMUBUS_CPU_CORE_MON_UAR0_IR);
    ret.aor = (XTRACTN(uar0, EMUBUS_CPU_CORE_MON_UAR0_BAR) << 16) |
        XTRACTN(uar0, EMUBUS_CPU_CORE_MON_UAR0_AOR);
    ret.dor = XTRACTN(uar1, EMUBUS_CPU_CORE_MON_UAR1_DOR);
    ret.z = XTRACTN(uar1, EMUBUS_CPU_CORE_MON_UAR1_Z);

    ret.tstate.ts = XTRACTN(ctcs, EMUBUS_CPU_CORE_MON_CTCS_TS);

    ret.pstate.rw = XTRACT1(cps, EMUBUS_CPU_CORE_MON_CPS_RW);
    ret.pstate.sync = XTRACT1(cps, EMUBUS_CPU_CORE_MON_CPS_SYNC);

    ret.internal.cp2 = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_CP2);
    ret.internal.t_reset_pend = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_T_RESET_PEND);
    ret.internal.am_nostart = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_AM_NOSTART);
    ret.internal.am_done_d = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_AM_DONE_D);
    ret.internal.am_done_d2 = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_AM_DONE_D2);
    ret.internal.resp = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_RESP);
    ret.internal.nmip = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_NMIP);
    ret.internal.irqp = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_IRQP);
    ret.internal.resg = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_RESG);
    ret.internal.nmig = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_NMIG);
    ret.internal.respd = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_RESPD);
    ret.internal.nmil = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_NMIL);
    ret.internal.intg_pre2 = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_INTG_PRE2);
    ret.internal.intg = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_INTG);
    ret.internal.vec1 = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_VEC1);
    ret.internal.intr_id = XTRACTN(int0, EMUBUS_CPU_CORE_MON_INT0_INTR_ID);
    ret.internal.rwb_ext = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_RWB_EXT);
    ret.internal.mlb_ext = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_MLB_EXT);
    ret.internal.vpb_ext = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_VPB_EXT);
    ret.internal.vda_ext = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_VDA_EXT);
    ret.internal.vpa_ext = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_VPA_EXT);
    ret.internal.ac_z = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_AC_Z);
    ret.internal.dl_z = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_DL_Z);
    ret.internal.addc = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_ADDC);
    ret.internal.add16 = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_ADD16);
    ret.internal.accoh = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_ACCOH);
    ret.internal.daa = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_DAA);
    ret.internal.das = XTRACT1(int0, EMUBUS_CPU_CORE_MON_INT0_DAS);
    ret.internal.ors = XTRACT1(int1, EMUBUS_CPU_CORE_MON_INT1_ORS);
    ret.internal.ands = XTRACT1(int1, EMUBUS_CPU_CORE_MON_INT1_ANDS);
    ret.internal.eors = XTRACT1(int1, EMUBUS_CPU_CORE_MON_INT1_EORS);
    ret.internal.asls = XTRACT1(int1, EMUBUS_CPU_CORE_MON_INT1_ASLS);
    ret.internal.rols = XTRACT1(int1, EMUBUS_CPU_CORE_MON_INT1_ROLS);
    ret.internal.lsrs = XTRACT1(int1, EMUBUS_CPU_CORE_MON_INT1_LSRS);
    ret.internal.rors = XTRACT1(int1, EMUBUS_CPU_CORE_MON_INT1_RORS);
    ret.internal.ccod = XTRACT1(int1, EMUBUS_CPU_CORE_MON_INT1_CCOD);
    ret.internal.cvod = XTRACT1(int1, EMUBUS_CPU_CORE_MON_INT1_CVOD);
    ret.internal.op_am_page_wrap_d = XTRACT1(int1, EMUBUS_CPU_CORE_MON_INT1_OP_AM_PAGE_WRAP_D);
    ret.internal.rdy1 = XTRACT1(int1, EMUBUS_CPU_CORE_MON_INT1_RDY1);
    ret.internal.waitn0 = XTRACT1(int1, EMUBUS_CPU_CORE_MON_INT1_WAITN0);
    ret.internal.t = XTRACTN(int4, EMUBUS_CPU_CORE_MON_INT4_T);
    ret.internal.am_t = XTRACTN(int4, EMUBUS_CPU_CORE_MON_INT4_AM_T);
    ret.internal.op_tp = XTRACTN(int4, EMUBUS_CPU_CORE_MON_INT4_OP_TP);
    ret.internal.aab = XTRACTN(int4, EMUBUS_CPU_CORE_MON_INT4_AAB);
    ret.internal.upc = XTRACTN(int5, EMUBUS_CPU_CORE_MON_INT5_UPC);
    ret.internal.npc = XTRACTN(int5, EMUBUS_CPU_CORE_MON_INT5_NPC);
    ret.internal.idld = XTRACTN(int6, EMUBUS_CPU_CORE_MON_INT6_IDLD);
    ret.internal.aao = XTRACTN(int6, EMUBUS_CPU_CORE_MON_INT6_AAO);
    ret.internal.ai = XTRACTN(int7, EMUBUS_CPU_CORE_MON_INT7_AI);
    ret.internal.bi = XTRACTN(int7, EMUBUS_CPU_CORE_MON_INT7_BI);
    ret.internal.cod = XTRACTN(int8, EMUBUS_CPU_CORE_MON_INT8_COD);

    return ret;
}

#undef XTRACT1
#undef XTRACTN

#define INSERT1(reg, name, field)   \
    reg = (reg & ~name) | ((field) ? name : 0)
#define INSERTN(reg, name, field)   \
    reg = (reg & ~name) | ((static_cast<decltype(reg)>(field) << name##_S) & name)

void icd::write_cpu_regs(const icd::cpu_regs& regs)
{
    auto arc0 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_ARC0);
    auto arc1 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_ARC1);
    auto arc2 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_ARC2);
    auto arc3 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_ARC3);
    auto uar0 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_UAR0);
    auto uar1 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_UAR1);
    auto ctcs = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_CTCS);
    auto cps  = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_CPS);
    auto int0 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT0);
    auto int1 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT1);
    auto int4 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT4);
    auto int5 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT5);
    auto int6 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT6);
    auto int7 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT7);
    auto int8 = read_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT8);

    INSERTN(arc0, EMUBUS_CPU_CORE_MON_ARC0_PC, regs.pc);
    INSERT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_C, regs.pf.c);
    INSERT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_Z, regs.pf.z);
    INSERT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_I, regs.pf.i);
    INSERT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_D, regs.pf.d);
    INSERT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_X_B, regs.pf.x);
    INSERT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_M, regs.pf.m);
    INSERT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_V, regs.pf.v);
    INSERT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_PF_N, regs.pf.n);
    INSERT1(arc0, EMUBUS_CPU_CORE_MON_ARC0_E, regs.e);
    INSERTN(arc1, EMUBUS_CPU_CORE_MON_ARC1_S, regs.s);
    INSERTN(arc1, EMUBUS_CPU_CORE_MON_ARC1_AC, regs.a);
    INSERTN(arc2, EMUBUS_CPU_CORE_MON_ARC2_D, regs.d);
    INSERTN(arc2, EMUBUS_CPU_CORE_MON_ARC2_X, regs.x);
    INSERTN(arc3, EMUBUS_CPU_CORE_MON_ARC3_Y, regs.y);
    INSERTN(arc3, EMUBUS_CPU_CORE_MON_ARC3_DBR, regs.dbr);
    INSERTN(arc3, EMUBUS_CPU_CORE_MON_ARC3_PBR, regs.pbr);

    INSERTN(uar0, EMUBUS_CPU_CORE_MON_UAR0_IR, regs.ir);
    INSERTN(uar0, EMUBUS_CPU_CORE_MON_UAR0_BAR, regs.aor >> 16);
    INSERTN(uar0, EMUBUS_CPU_CORE_MON_UAR0_AOR, regs.aor);
    INSERTN(uar1, EMUBUS_CPU_CORE_MON_UAR1_DOR, regs.dor);
    INSERTN(uar1, EMUBUS_CPU_CORE_MON_UAR1_Z, regs.z);

    INSERTN(ctcs, EMUBUS_CPU_CORE_MON_CTCS_TS, regs.tstate.ts);

    //INSERT1(cps, EMUBUS_CPU_CORE_MON_CPS_RW, regs.pstate.rw);
    INSERT1(cps, EMUBUS_CPU_CORE_MON_CPS_SYNC, regs.pstate.sync);

    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_CP2, regs.internal.cp2);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_T_RESET_PEND, regs.internal.t_reset_pend);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_AM_NOSTART, regs.internal.am_nostart);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_AM_DONE_D, regs.internal.am_done_d);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_AM_DONE_D2, regs.internal.am_done_d2);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_RESP, regs.internal.resp);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_NMIP, regs.internal.nmip);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_IRQP, regs.internal.irqp);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_RESG, regs.internal.resg);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_NMIG, regs.internal.nmig);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_RESPD, regs.internal.respd);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_NMIL, regs.internal.nmil);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_INTG_PRE2, regs.internal.intg_pre2);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_INTG, regs.internal.intg);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_VEC1, regs.internal.vec1);
    INSERTN(int0, EMUBUS_CPU_CORE_MON_INT0_INTR_ID, regs.internal.intr_id);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_RWB_EXT, regs.internal.rwb_ext);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_MLB_EXT, regs.internal.mlb_ext);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_VPB_EXT, regs.internal.vpb_ext);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_VDA_EXT, regs.internal.vda_ext);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_VPA_EXT, regs.internal.vpa_ext);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_AC_Z, regs.internal.ac_z);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_DL_Z, regs.internal.dl_z);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_ADDC, regs.internal.addc);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_ADD16, regs.internal.add16);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_ACCOH, regs.internal.accoh);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_DAA, regs.internal.daa);
    INSERT1(int0, EMUBUS_CPU_CORE_MON_INT0_DAS, regs.internal.das);
    INSERT1(int1, EMUBUS_CPU_CORE_MON_INT1_ORS, regs.internal.ors);
    INSERT1(int1, EMUBUS_CPU_CORE_MON_INT1_ANDS, regs.internal.ands);
    INSERT1(int1, EMUBUS_CPU_CORE_MON_INT1_EORS, regs.internal.eors);
    INSERT1(int1, EMUBUS_CPU_CORE_MON_INT1_ASLS, regs.internal.asls);
    INSERT1(int1, EMUBUS_CPU_CORE_MON_INT1_ROLS, regs.internal.rols);
    INSERT1(int1, EMUBUS_CPU_CORE_MON_INT1_LSRS, regs.internal.lsrs);
    INSERT1(int1, EMUBUS_CPU_CORE_MON_INT1_RORS, regs.internal.rors);
    INSERT1(int1, EMUBUS_CPU_CORE_MON_INT1_CCOD, regs.internal.ccod);
    INSERT1(int1, EMUBUS_CPU_CORE_MON_INT1_CVOD, regs.internal.cvod);
    INSERT1(int1, EMUBUS_CPU_CORE_MON_INT1_OP_AM_PAGE_WRAP_D, regs.internal.op_am_page_wrap_d);
    INSERT1(int1, EMUBUS_CPU_CORE_MON_INT1_RDY1, regs.internal.rdy1);
    INSERT1(int1, EMUBUS_CPU_CORE_MON_INT1_WAITN0, regs.internal.waitn0);
    INSERTN(int4, EMUBUS_CPU_CORE_MON_INT4_T, regs.internal.t);
    INSERTN(int4, EMUBUS_CPU_CORE_MON_INT4_AM_T, regs.internal.am_t);
    INSERTN(int4, EMUBUS_CPU_CORE_MON_INT4_OP_TP, regs.internal.op_tp);
    INSERTN(int4, EMUBUS_CPU_CORE_MON_INT4_AAB, regs.internal.aab);
    INSERTN(int5, EMUBUS_CPU_CORE_MON_INT5_UPC, regs.internal.upc);
    INSERTN(int5, EMUBUS_CPU_CORE_MON_INT5_NPC, regs.internal.npc);
    INSERTN(int6, EMUBUS_CPU_CORE_MON_INT6_IDLD, regs.internal.idld);
    INSERTN(int6, EMUBUS_CPU_CORE_MON_INT6_AAO, regs.internal.aao);
    INSERTN(int7, EMUBUS_CPU_CORE_MON_INT7_AI, regs.internal.ai);
    INSERTN(int7, EMUBUS_CPU_CORE_MON_INT7_BI, regs.internal.bi);
    INSERTN(int8, EMUBUS_CPU_CORE_MON_INT8_COD, regs.internal.cod);

    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_ARC0, arc0);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_ARC1, arc1);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_ARC2, arc2);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_ARC3, arc3);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_UAR0, uar0);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_UAR1, uar1);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_CTCS, ctcs);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_CPS,  cps);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT0, int0);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT1, int1);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT4, int4);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT5, int5);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT6, int6);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT7, int7);
    write_cpu_reg(EMUBUS_CPU_CORE_MON_SEL_INT8, int8);
}

#undef INSERT1
#undef INSERTN

bool icd::is_halted()
{
    return EMUBUS_ICD_CTRL & EMUBUS_ICD_CTRL_SYS_HOLD;
}

void icd::force_halt()
{
    EMUBUS_ICD_CTRL |= EMUBUS_ICD_CTRL_SYS_FORCE_HALT;
    EMUBUS_ICD_CTRL &= ~EMUBUS_ICD_CTRL_SYS_FORCE_HALT;
}

void icd::resume()
{
    EMUBUS_ICD_CTRL |= EMUBUS_ICD_CTRL_SYS_RESUME;
    EMUBUS_ICD_CTRL &= ~EMUBUS_ICD_CTRL_SYS_RESUME;
    speak(&listener::resumed);
}

icd_match& icd::get_match(int idx)
{
    return match_units.at(idx);
}

bool icd::is_any_match_triggered()
{
    return EMUBUS_ICD_CTRL & EMUBUS_ICD_CTRL_MATCH_TRIGGER;
}

void icd::read_state(state_t& st)
{
    st.regs = read_cpu_regs();
}

void icd::write_state(const state_t& st)
{
    write_cpu_regs(st.regs);
    speak(&listener::on_state_loaded);
}

} // namespace snes
