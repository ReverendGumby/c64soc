#include "emubus.h"
#include "cpumem.h"
#include "sys.h"
#include "sys_state.h"

#include <cli/command.h>
#include <cli/manager.h>
#include <util/dump_to_str.h>
#include <util/reg_val.h>
#include <util/string.h>
#include <util/crc32.h>
#include <fat/path.h>
#include <fat/filesystem.h>
#include <fat/fstream.h>

#include <yaml-cpp/yaml.h>

#include <stdint.h>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <memory>

#include "cli.h"

struct ppu_inidisp_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_inidisp_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "Brightness=" << int(r.inidisp.bright)
       << ", Forced Blank=" << int(r.inidisp.forced_blank);
    return ss.str();
}

struct ppu_bgmode_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_bgmode_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "BG Mode=" << int(r.bgmode.mode)
       << ", Mode 1 BG3 Pri=" << int(r.bgmode.bg3_pri);
    return ss.str();
}

#define DECLARE_PPU_BGxSC_T(x)                                          \
    struct ppu_bg##x##sc_t { snes::ppu_regs regs; };                    \
    template <>                                                         \
    std::string reg_val<ppu_bg##x##sc_t>::to_str() const                \
    {                                                                   \
        const auto& r = val.regs;                                       \
        const char* ssize = "?";                                        \
        switch (r.bg##x##sc.size) {                                     \
        case 0: ssize = "32x32"; break;                                 \
        case 1: ssize = "64x32"; break;                                 \
        case 2: ssize = "32x64"; break;                                 \
        case 3: ssize = "64x64"; break;                                 \
        }                                                               \
        std::stringstream ss;                                           \
        ss << "Size=" << int(r.bg##x##sc.size) << '/' << ssize          \
        << ", Address=0x"                                               \
        << reg_val_of(r.bg##x##sc.base) << '/' <<                       \
            string_printf("$%04X", r.bg##x##sc.base * 1024);            \
        return ss.str();                                                \
    }

DECLARE_PPU_BGxSC_T(1)
DECLARE_PPU_BGxSC_T(2)
DECLARE_PPU_BGxSC_T(3)
DECLARE_PPU_BGxSC_T(4)

struct ppu_bg12nba_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_bg12nba_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "BG1 Tile Address=" << int(r.bg12nba.bg1nba)
       << '/' << string_printf("$%04X", r.bg12nba.bg1nba * 4096)
       << ", BG2 Tile Address=" << int(r.bg12nba.bg2nba)
       << '/' << string_printf("$%04X", r.bg12nba.bg2nba * 4096);
    return ss.str();
}

struct ppu_bg34nba_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_bg34nba_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "BG3 Tile Address=" << int(r.bg34nba.bg3nba)
       << '/' << string_printf("$%04X", r.bg34nba.bg3nba * 4096)
       << ", BG4 Tile Address=" << int(r.bg34nba.bg4nba)
       << '/' << string_printf("$%04X", r.bg34nba.bg4nba * 4096);
    return ss.str();
}

#define DECLARE_PPU_Tx_T(x)                             \
    struct ppu_##x##_t { snes::ppu_regs regs; };        \
    template <>                                         \
    std::string reg_val<ppu_##x##_t>::to_str() const    \
    {                                                   \
        const auto& r = val.regs;                       \
        std::stringstream ss;                           \
        ss << (r.x.obj ? 'O' : '_')                     \
           << (r.x.bg4 ? '4' : '_')                     \
           << (r.x.bg3 ? '3' : '_')                     \
           << (r.x.bg2 ? '2' : '_')                     \
           << (r.x.bg1 ? '1' : '_');                    \
        return ss.str();                                \
    }

DECLARE_PPU_Tx_T(tm)
DECLARE_PPU_Tx_T(ts)
DECLARE_PPU_Tx_T(tmw)
DECLARE_PPU_Tx_T(tsw)

struct ppu_setini_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_setini_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "Interlace=" << int(r.setini.interlacing)
//     << ", OBJ V-Disp=" << r.setini.obj_vdir_disp
       << ", BG V-Disp=" << int(r.setini.bg_vdir_disp)
       << '/' << (r.setini.bg_vdir_disp == 0 ? 224 : 239);
    return ss.str();
}

struct ppu_stat77_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_stat77_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "Range Over=" << int(r.stat77.range_over)
       << ", Time Over=" << int(r.stat77.time_over);
    return ss.str();
}

struct ppu_stat78_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_stat78_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "H/V-Latch=" << int(r.stat78.opl)
       << ", Field=" << int(r.stat78.field);
    return ss.str();
}

struct ppu_m7sel_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_m7sel_t>::to_str() const
{
    const auto& r = val.regs;
    const char* scover = "?";
    switch (r.m7sel.scover) {
    case 0: scover = "0/Wrap";          break;
    case 1: scover = "1/Wrap";          break;
    case 2: scover = "2/Transparent";   break;
    case 3: scover = "3/Tile 0";        break;
    }
    std::stringstream ss;
    ss << "H-Flip=" << int(r.m7sel.hflip)
       << ", V-Flip=" << int(r.m7sel.vflip)
       << ", Screen Over=" << scover;
    return ss.str();
}

#define DECLARE_PPU_M7x_ABCD_T(x)                       \
    struct ppu_m7##x##_t { snes::ppu_regs regs; };      \
    template <>                                         \
    std::string reg_val<ppu_m7##x##_t>::to_str() const  \
    {                                                   \
        const auto& r = val.regs;                       \
        std::stringstream ss;                           \
        double q = int16_t(r.m7##x) / 256.0;            \
        ss << "0x" << reg_val_of(r.m7##x)               \
           <<  " / " << q;                              \
        return ss.str();                                \
    }                                                   \

DECLARE_PPU_M7x_ABCD_T(a)
DECLARE_PPU_M7x_ABCD_T(b)
DECLARE_PPU_M7x_ABCD_T(c)
DECLARE_PPU_M7x_ABCD_T(d)

#define DECLARE_PPU_M7x_XY_T(x)                         \
    struct ppu_m7##x##_t { snes::ppu_regs regs; };      \
    template <>                                         \
    std::string reg_val<ppu_m7##x##_t>::to_str() const  \
    {                                                   \
        const auto& r = val.regs;                       \
        std::stringstream ss;                           \
        int q = r.m7##x;                                \
        if (q & (1 << 12))                              \
            q = q - (1 << 13);                          \
        ss << "0x" << reg_val_of(r.m7##x)               \
           <<  " / " << q;                              \
        return ss.str();                                \
    }                                                   \

DECLARE_PPU_M7x_XY_T(x)
DECLARE_PPU_M7x_XY_T(y)

struct ppu_mpylmh_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_mpylmh_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    int q = r.mpylmh;
    if (q & (1 << 23))
        q = q - (1 << 24);
    ss << string_printf("{$%02X,$%02X,$%02X}",
                        (r.mpylmh >> 0*8) & 0xFF,
                        (r.mpylmh >> 1*8) & 0xFF,
                        (r.mpylmh >> 2*8) & 0xFF)
       <<  " / " << q;
    return ss.str();
}

std::string winsel_to_str(bool inv, bool ena)
{
    std::string ret = "__";
    if (inv)
        ret[0] = 'I';
    if (ena)
        ret[1] = 'E';
    return ret;
}

struct ppu_w12sel_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_w12sel_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "BG1: Win1=" << winsel_to_str(r.w12sel.w1_inv_bg1, r.w12sel.w1_ena_bg1)
       << " Win2=" << winsel_to_str(r.w12sel.w2_inv_bg1, r.w12sel.w2_ena_bg1)
       << "; BG2: Win1=" << winsel_to_str(r.w12sel.w1_inv_bg2, r.w12sel.w1_ena_bg2)
       << " Win2=" << winsel_to_str(r.w12sel.w2_inv_bg2, r.w12sel.w2_ena_bg2);
    return ss.str();
}

struct ppu_w34sel_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_w34sel_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "BG3: Win1=" << winsel_to_str(r.w34sel.w1_inv_bg3, r.w34sel.w1_ena_bg3)
       << " Win2=" << winsel_to_str(r.w34sel.w2_inv_bg3, r.w34sel.w2_ena_bg3)
       << "; BG4: Win1=" << winsel_to_str(r.w34sel.w1_inv_bg4, r.w34sel.w1_ena_bg4)
       << " Win2=" << winsel_to_str(r.w34sel.w2_inv_bg4, r.w34sel.w2_ena_bg4);
    return ss.str();
}

struct ppu_wobjsel_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_wobjsel_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "OBJ: Win1=" << winsel_to_str(r.wobjsel.w1_inv_obj, r.wobjsel.w1_ena_obj)
       << " Win2=" << winsel_to_str(r.wobjsel.w2_inv_obj, r.wobjsel.w2_ena_obj)
       << "; Clr: Win1=" << winsel_to_str(r.wobjsel.w1_inv_clr, r.wobjsel.w1_ena_clr)
       << " Win2=" << winsel_to_str(r.wobjsel.w2_inv_clr, r.wobjsel.w2_ena_clr);
    return ss.str();
}

const char* name(snes::ppu_regs::wmlog_t e)
{
    switch (e) {
    case 0: return "0/OR";
    case 1: return "1/AND";
    case 2: return "2/XOR";
    case 3: return "3/XNOR";
    default: break;
    }
    return "?";
}

struct ppu_wbglog_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_wbglog_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    
    ss << "BG1=" << name(r.wbglog.bg1)
       << ", BG2=" << name(r.wbglog.bg2)
       << ", BG3=" << name(r.wbglog.bg3)
       << ", BG4=" << name(r.wbglog.bg4);
    return ss.str();
}

struct ppu_wobjlog_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_wobjlog_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    
    ss << "OBJ=" << name(r.wobjlog.obj)
       << ", Color=" << name(r.wobjlog.clr);
    return ss.str();
}

struct ppu_wh0_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_wh0_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "Window 1 left pos =" << int(r.wh0);
    return ss.str();
}

struct ppu_wh1_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_wh1_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "Window 1 right pos=" << int(r.wh1);
    return ss.str();
}

struct ppu_wh2_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_wh2_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "Window 2 left pos =" << int(r.wh2);
    return ss.str();
}

struct ppu_wh3_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_wh3_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "Window 2 right pos=" << int(r.wh3);
    return ss.str();
}

struct ppu_objsel_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_objsel_t>::to_str() const
{
    const auto& r = val.regs;
    const char* ssize = "?";
    switch (r.objsel.ss) {
    case 0: ssize = "0/8x8/16x16"; break;
    case 1: ssize = "1/8x8/32x32"; break;
    case 2: ssize = "2/8x8/64x64"; break;
    case 3: ssize = "3/16x16/32x32"; break;
    case 4: ssize = "4/16x16/64x64"; break;
    case 5: ssize = "5/32x32/64x64"; break;
    case 6: ssize = "6/16x32/32x64"; break;
    case 7: ssize = "7/16x32/32x32"; break;
    }
    std::stringstream ss;
    ss << "Address="
       << int(r.objsel.ba) << '/'
       << string_printf("$%04X", r.objsel.ba * 8192)
       << ", 2nd Address=0x"
       << int(r.objsel.n) << '/'
       << string_printf("$%04X", r.objsel.ba * 8192 + (r.objsel.n + 1) * 4096)
       << ", OBJ size=" << ssize;
    return ss.str();
}

const char* clr_win_rgn_to_str(uint8_t e)
{
    switch (e) {
    case 0: return "0/never";
    case 1: return "1/invert";
    case 2: return "2/normal";
    case 3: return "3/always";
    default: return "?";
    }
}

struct ppu_cgwsel_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_cgwsel_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "Direct color=" << int(r.cgwsel.direct)
       << ", Add subscr.=" << int(r.cgwsel.addend)
       << ", Sub.window rgn.=" << clr_win_rgn_to_str(r.cgwsel.sub_win_rgn)
       << ", Main window rgn.=" << clr_win_rgn_to_str(r.cgwsel.main_win_rgn);
    return ss.str();
}

struct ppu_cgadsub_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_cgadsub_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "Math enables: "
       << (r.cgadsub.back ? 'B' : '_')
       << (r.cgadsub.obj ? 'O' : '_')
       << (r.cgadsub.bg4 ? '4' : '_')
       << (r.cgadsub.bg3 ? '3' : '_')
       << (r.cgadsub.bg2 ? '2' : '_')
       << (r.cgadsub.bg1 ? '1' : '_')
       << ", Half=" << int(r.cgadsub.half)
       << ", Op=" << (r.cgadsub.sub ? "1/-" : "0/+");
    return ss.str();
}

struct ppu_coldata_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_coldata_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "R=" << int(r.coldata.r)
       << ", G=" << int(r.coldata.g)
       << ", B=" << int(r.coldata.b);
    return ss.str();
}

struct ppu_mosaic_t { snes::ppu_regs regs; };
template <>
std::string reg_val<ppu_mosaic_t>::to_str() const
{
    const auto& r = val.regs;
    std::stringstream ss;
    ss << "Size=0x" << reg_val_of(r.mosaic.sz)
       << '/' << int(r.mosaic.sz)
       << ", BG enables="
       << (r.mosaic.bg4 ? '4' : '_')
       << (r.mosaic.bg3 ? '3' : '_')
       << (r.mosaic.bg2 ? '2' : '_')
       << (r.mosaic.bg1 ? '1' : '_');
    return ss.str();
}

namespace snes {

using namespace ::cli;

cli::cli(class sys& sys)
    : sys(sys)
{
    command::define("reset", [this](const command::args_t& args) {
        return reset(args); });

    command::define("cmd", [this](const command::args_t& args) {
        return cpumem_store_display(args); });

    command::define("cds", [this](const command::args_t& args) {
        return cpumem_dump_stats(args); });

    command::define("pds", [this](const command::args_t& args) {
        return ppu_dump_state(args); });
    command::define("pmd", [this](const command::args_t& args) {
        return ppu_mem_display(args); });

    command::define("amrd", [this](const command::args_t& args) {
        return apu_mon_reg_dump(args); });
    command::define("add", [this](const command::args_t& args) {
        return apu_dsp_dump(args); });
    command::define("amd", [this](const command::args_t& args) {
        return apu_mem_display(args); });

    command::define("pst", [this](const command::args_t& args) {
        return print_state(args); });
    command::define("rst", [this](const command::args_t& args) {
        return read_state(args); });
    command::define("wst", [this](const command::args_t& args) {
        return write_state(args); },
        command::repeatable);
    command::define("sst", [this](const command::args_t& args) {
        return save_state(args); });
    command::define("lrsst", [this](const command::args_t& args) {
        return load_resave_state(args); });
    command::define("lst", [this](const command::args_t& args) {
        return load_state(args); });
    command::define("rswrsst", [this](const command::args_t& args) {
        return read_save_write_read_save_state(args); });
}

int cli::reset(const command::args_t& args)
{
    bool assert = false, deassert = false;

    if (args.size() >= 1) {
        assert = (args[0] == "assert");
        deassert = (args[0] == "deassert");
    } else {
        assert = deassert = true;
    }

    if (assert) {
        sys.reset_snes(true);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    if (deassert) {
        sys.reset_snes(false);
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    return 0;
}

int cli::cpumem_store_display(const command::args_t& args)
{
    auto it = args.begin();
    if (args.end() - it < 1)
        throw std::range_error("missing argument");
    const uint8_t* mem = nullptr;
    if (*it == "wram")
        mem = sys.cpumem.get_wram();
    else if (*it == "rom")
        mem = sys.cart.get_rom();
    else if (*it == "sram")
        mem = sys.cart.get_sram();
    else
        throw std::range_error("unknown memory type");

    it++;
    if (args.end() - it < 1)
        throw std::range_error("missing argument");
    auto addr = memaddr(strtoul(it->c_str(), NULL, 0));

    unsigned long len = 1;
    it++;
    if (it < args.end())
        len = strtoul(it->c_str(), NULL, 0);

    if (!mem)
        throw std::runtime_error("memory not allocated");

    uint8_t data[len];
    for (unsigned long i = 0; i < len; i++)
        data[i] = mem[addr + i];

    for (auto& line : dump_to_str(data, len, addr))
        std::cout << line << std::endl;

    return 0;
}

int cli::cpumem_dump_stats(const ::cli::command::args_t& args)
{
#define dump(x)                                             \
    do {                                                    \
        EMUBUS_CPUMEM_MON_SEL = EMUBUS_CPUMEM_MON_SEL_##x;  \
        auto v = EMUBUS_CPUMEM_MON_DATA;                    \
        std::cout << #x " = " << v << '\n';                 \
    } while (0)

    dump(ATM_RDONE_MAX);
    dump(ATM_RDONE_MIN);
    dump(ATM_WDONE_MAX);
    dump(ATM_WDONE_MIN);
    dump(ATM_NOT_READY_CNT);

#undef dump

    // Clear stats
    EMUBUS_CPUMEM_MON_SEL = EMUBUS_CPUMEM_MON_SEL_ATM_RDONE_MAX;
    EMUBUS_CPUMEM_MON_DATA = 0;
    EMUBUS_CPUMEM_MON_SEL = EMUBUS_CPUMEM_MON_SEL_ATM_NOT_READY_CNT;
    EMUBUS_CPUMEM_MON_DATA = 0;

    return 0;
}

int cli::ppu_dump_state(const command::args_t& args)
{
    struct {
        bool def;
        bool all;
        bool reg;
        bool cnt;
        bool bm;
        bool pal;
        bool oam;
    } opts = {};

    if (!args.size())
        opts.def = true;
    else
        for (auto& arg : args) {
            if (arg == "--all")
                opts.all = true;
            if (arg == "--reg")
                opts.reg = true;
            if (arg == "--cnt")
                opts.cnt = true;
            if (arg == "--bm")
                opts.bm = true;
            if (arg == "--pal")
                opts.pal = true;
            if (arg == "--oam")
                opts.oam = true;
        }

    auto regs = sys.ppu.read_regs();

    if (opts.all || opts.reg || opts.def) {
        std::cout << "Registers: \n"
                  << "  $2100 INIDISP = " << reg_val<ppu_inidisp_t>{regs} << '\n'
                  << "  $2101 OBJSEL  = " << reg_val<ppu_objsel_t>{regs} << '\n'
                  << "  $2105 BGMODE  = " << reg_val<ppu_bgmode_t>{regs} << '\n'
                  << "  $2106 MOSAIC  = " << reg_val<ppu_mosaic_t>{regs} << '\n'
                  << "  $2107 BG1SC   = " << reg_val<ppu_bg1sc_t>{regs} << '\n'
                  << "  $2108 BG2SC   = " << reg_val<ppu_bg2sc_t>{regs} << '\n'
                  << "  $2109 BG3SC   = " << reg_val<ppu_bg3sc_t>{regs} << '\n'
                  << "  $210A BG4SC   = " << reg_val<ppu_bg4sc_t>{regs} << '\n'
                  << "  $210B BG12NBA = " << reg_val<ppu_bg12nba_t>{regs} << '\n'
                  << "  $210C BG34NBA = " << reg_val<ppu_bg34nba_t>{regs} << '\n'
                  << "  $210D BG1HOFS = " << "0x" << reg_val_of(regs.bg1hofs) << '\n'
                  << "  $210D M7HOFS  = " << "0x" << reg_val_of(regs.m7hofs) << '\n'
                  << "  $210E BG1VOFS = " << "0x" << reg_val_of(regs.bg1vofs) << '\n'
                  << "  $210E M7VOFS  = " << "0x" << reg_val_of(regs.m7vofs) << '\n'
                  << "  $210F BG2HOFS = " << "0x" << reg_val_of(regs.bg2hofs) << '\n'
                  << "  $2110 BG2VOFS = " << "0x" << reg_val_of(regs.bg2vofs) << '\n'
                  << "  $2111 BG3HOFS = " << "0x" << reg_val_of(regs.bg3hofs) << '\n'
                  << "  $2112 BG3VOFS = " << "0x" << reg_val_of(regs.bg3vofs) << '\n'
                  << "  $2113 BG4HOFS = " << "0x" << reg_val_of(regs.bg4hofs) << '\n'
                  << "  $2114 BG4VOFS = " << "0x" << reg_val_of(regs.bg4vofs) << '\n'
                  << "  $211A M7SEL   = " << reg_val<ppu_m7sel_t>{regs} << '\n'
                  << "  $211B M7A     = " << reg_val<ppu_m7a_t>{regs} << '\n'
                  << "  $211C M7B     = " << reg_val<ppu_m7b_t>{regs} << '\n'
                  << "  $211D M7C     = " << reg_val<ppu_m7c_t>{regs} << '\n'
                  << "  $211E M7D     = " << reg_val<ppu_m7d_t>{regs} << '\n'
                  << "  $211F M7X     = " << reg_val<ppu_m7x_t>{regs} << '\n'
                  << "  $2120 M7Y     = " << reg_val<ppu_m7y_t>{regs} << '\n'
                  << "  $2123 W12SEL  = " << reg_val<ppu_w12sel_t>{regs} << '\n'
                  << "  $2124 W34SEL  = " << reg_val<ppu_w34sel_t>{regs} << '\n'
                  << "  $2125 WOBJSEL = " << reg_val<ppu_wobjsel_t>{regs} << '\n'
                  << "  $2126 WH0     = " << reg_val<ppu_wh0_t>{regs} << '\n'
                  << "  $2127 WH1     = " << reg_val<ppu_wh1_t>{regs} << '\n'
                  << "  $2128 WH2     = " << reg_val<ppu_wh2_t>{regs} << '\n'
                  << "  $2129 WH3     = " << reg_val<ppu_wh3_t>{regs} << '\n'
                  << "  $212A WBGLOG  = " << reg_val<ppu_wbglog_t>{regs} << '\n'
                  << "  $212B WOBJLOG = " << reg_val<ppu_wobjlog_t>{regs} << '\n'
                  << "  $212C TM      = " << reg_val<ppu_tm_t>{regs} << '\n'
                  << "  $212D TS      = " << reg_val<ppu_ts_t>{regs} << '\n'
                  << "  $212E TMW     = " << reg_val<ppu_tmw_t>{regs} << '\n'
                  << "  $212F TSW     = " << reg_val<ppu_tsw_t>{regs} << '\n'
                  << "  $2130 CGWSEL  = " << reg_val<ppu_cgwsel_t>{regs} << '\n'
                  << "  $2131 CGADSUB = " << reg_val<ppu_cgadsub_t>{regs} << '\n'
                  << "  $2132 COLDATA = " << reg_val<ppu_coldata_t>{regs} << '\n'
                  << "  $2133 SETINI  = " << reg_val<ppu_setini_t>{regs} << '\n'
                  << "  $2134-6 MPYLMH= " << reg_val<ppu_mpylmh_t>{regs} << '\n'
                  << "  $213C OPHCT   = " << "0x" << reg_val_of(regs.ophct) << '\n'
                  << "  $213D OPVCT   = " << "0x" << reg_val_of(regs.opvct) << '\n'
                  << "  $213E STAT77  = " << reg_val<ppu_stat77_t>{regs} << '\n'
                  << "  $213F STAT78  = " << reg_val<ppu_stat78_t>{regs} << '\n';
    }
    if (opts.all || opts.cnt || opts.def) {
        std::cout << "Video counter: " << std::dec
                  << "  ROW=" << regs.video_counter.row
                  << "/0x" << reg_val_of(regs.video_counter.row)
                  << "  COL=" << regs.video_counter.col
                  << "/0x" << reg_val_of(regs.video_counter.col)
                  << "  FRAME=" << regs.video_counter.frame
                  << "\n";
    }
    if (opts.all || opts.bm || opts.def) {
        std::cout << "Bus master:\n";
        for (int bg = 1; bg <= 4; bg++)
            std::cout << "  BG" << bg
                      << ": VAB=0x" << reg_val_of(regs.bus_master.bg[bg-1].vab)
                      << ", VAC=0x" << reg_val_of(regs.bus_master.bg[bg-1].vac)
                      << ", VAS=0x" << reg_val_of(regs.bus_master.bg[bg-1].vas)
                      << '\n';
        int mpya = regs.bus_master.m7.mpya;
        if (mpya & (1 << 15))
            mpya = mpya - (1 << 16);
        int mpyb = regs.bus_master.m7.mpyb;
        if (mpyb & (1 << 12))
            mpyb = mpyb - (1 << 13);
        int mpyc = regs.bus_master.m7.mpyc;
        if (mpyc & (1 << 26))
            mpyc = mpyc - (1 << 27);
        std::cout << "  M7 "
                  << ": MPYA=0x" << reg_val_of(regs.bus_master.m7.mpya)
                  << '/' << mpya
                  << ", MPYB=0x" << reg_val_of(regs.bus_master.m7.mpyb)
                  << '/' << mpyb
                  << ", MPYC=0x" << reg_val_of(regs.bus_master.m7.mpyc)
                  << '/' << mpyc
                  << '\n'
                  << "     "
                  << "  VAS=0x" << reg_val_of(regs.bus_master.m7.vas)
                  << '\n';
    }
    if (opts.all || opts.pal) {
        std::cout << "Palette (CGRAM):\n";
        for (auto& line : dump_to_str(regs.cgram, sizeof(regs.cgram), 0))
            std::cout << line << std::endl;
        std::cout << "\n";
    }
    if (opts.all || opts.oam) {
        std::cout << "OAM:\n";
        for (auto& line : dump_to_str(regs.oaml, sizeof(regs.oaml), 0))
            std::cout << line << std::endl;
        for (auto& line : dump_to_str(regs.oamh, sizeof(regs.oamh), 0 + sizeof(regs.oaml)))
            std::cout << line << std::endl;
        std::cout << "\n";
    }

    return 0;
}

int cli::ppu_mem_display(const ::cli::command::args_t& args)
{
    auto it = args.begin();
    if (args.end() - it < 1)
        throw std::range_error("missing argument");
    auto addr = memaddr(strtoul(it->c_str(), NULL, 0));
    memptr mp{addr, memtype::ppu};

    unsigned long len = 1;
    it++;
    if (it < args.end())
        len = strtoul(it->c_str(), NULL, 0);
    if (len >= 0x10000)
        throw std::range_error("length too long");

    auto data = std::unique_ptr<uint8_t[]>(new uint8_t[len]);
    for (unsigned long i = 0; i < len; i++)
        data[i] = mp[i];

    for (auto& line : dump_to_str(&data[0], len, addr))
        std::cout << line << std::endl;

    return 0;
}

static uint32_t apu_mon_read(uint8_t a)
{
    EMUBUS_APU_MON_SEL = a;
    return EMUBUS_APU_MON_DATA;
}

#if 0
static void apu_mon_read_until(uint8_t a, uint32_t v, uint32_t m)
{
    EMUBUS_APU_MON_SEL = a;
    while ((EMUBUS_APU_MON_DATA & m) != v)
        ;
}

static void apu_stall(bool e)
{
    if (e) {
        // Stall SMP and DSP
        apu_mon_write(EMUBUS_APU_MON_SEL_STCS, EMUBUS_APU_MON_STCS_STALL);
        apu_mon_write(EMUBUS_APU_MON_SEL_DTCS, EMUBUS_APU_MON_DTCS_STALL);
        apu_mon_read_until(EMUBUS_APU_MON_SEL_STCS,
                           EMUBUS_APU_MON_STCS_STALLED,
                           EMUBUS_APU_MON_STCS_STALLED);
        apu_mon_read_until(EMUBUS_APU_MON_SEL_DTCS,
                           EMUBUS_APU_MON_DTCS_STALLED,
                           EMUBUS_APU_MON_DTCS_STALLED);
    } else {
        // Release DSP stall
        apu_mon_write(EMUBUS_APU_MON_SEL_DTCS, 0);

        // Reset timing state, release SMP stall
        apu_mon_write(EMUBUS_APU_MON_SEL_STCS, EMUBUS_APU_MON_STCS_TS);
    }
}
#endif

int cli::apu_mon_reg_dump(const ::cli::command::args_t& args)
{
#define dump(x, lbl)                                            \
    do {                                                        \
        auto v = apu_mon_read(EMUBUS_APU_MON_SEL_##x);          \
        std::cout << lbl " = " <<                               \
            string_printf("0x%08x", v) << "  ";                 \
    } while(0)

    dump(ARC0, "PF,PC");
    dump(ARC1, "Y,X,AC,S");
    std::cout << '\n';
    dump(UAR0, "DOR,AOR,IR");
    dump(STCS, "STCS");
    dump(SPS, "SPS");
    std::cout << '\n';
    dump(CPUI, "CPUI3-0");
    dump(CPUO, "CPUO3-0");
    std::cout << '\n';
    dump(SPR0, "SPT2-0,SPCR");
    std::cout << '\n';
    dump(DTCS, "DTCS");
    dump(DRS, "DRS");
    dump(DRD, "DRD");
    std::cout << '\n';
    dump(SMP1, "SPDRS");
    std::cout << '\n';

#undef dump

    return 0;
}

int cli::apu_dsp_dump(const ::cli::command::args_t& args)
{
    //apu_stall(true);

    auto st = sys.apu.read_dsp_state(apu::dsp_state_regs);
    auto& regs = st.regs.regs;

    std::cout << "DSP regs:\n";
    for (auto& line : dump_to_str(regs, sizeof(regs), 0))
        std::cout << line << std::endl;

    //apu_stall(false);

    return 0;
}

int cli::apu_mem_display(const ::cli::command::args_t& args)
{
    auto it = args.begin();
    if (args.end() - it < 1)
        throw std::range_error("missing argument");
    auto addr = memaddr(strtoul(it->c_str(), NULL, 0));
    if (addr >= 0x10000)
        throw std::range_error("addr too large");
    memptr mp{addr, memtype::apu};

    unsigned long len = 1;
    it++;
    if (it < args.end())
        len = strtoul(it->c_str(), NULL, 0);
    if (addr + len - 1 >= 0x10000)
        throw std::range_error("length too long");

    auto data = std::unique_ptr<uint8_t[]>(new uint8_t[len]);

    //apu_stall(true);
    for (unsigned long i = 0; i < len; i++)
        data[i] = mp[i];
    //apu_stall(false);

    std::cout << "SPC RAM:\n";
    for (auto& line : dump_to_str(&data[0], len, addr))
        std::cout << line << std::endl;


    return 0;
}

int cli::print_state(const command::args_t& args)
{
    auto it = args.begin();
    std::vector<std::string> path;
    while (it < args.end()) {
        auto str = *(it++);
        path.push_back(str);
    }

    sys::state_t st;
    sys.pause();
    sys.read_state(st);
    sys.resume();

    yamlify_set_printable(true);

    if (path.size() == 0) {
        std::cout << "YAML output:\n"
                  << "--------------------\n";
        yamlify_sys_state(sys, st, std::cout);
        std::cout << "--------------------\n";
    } else {
        std::stringstream yst;
        yamlify_sys_state(sys, st, yst);
        const YAML::Node root = YAML::Load(yst);

        YAML::Node node = root;
        for (auto& str : path)
            node = ((const YAML::Node)(node))[str];

        YAML::Emitter es(std::cout);
        es << node;
        es << YAML::Newline;
    }

    return 0;
}

std::unique_ptr<sys::state_t> mem_state;

int cli::read_state(const command::args_t& args)
{
    mem_state = std::unique_ptr<sys::state_t>(new sys::state_t);

    sys.pause();
    sys.read_state(*mem_state);
    sys.resume();

    return 0;
}

int cli::write_state(const command::args_t& args)
{
    if (!mem_state) {
        std::cout << "No state available\n";
        return 1;
    }

    sys::state_t st;
    sys.pause();
    sys.write_state(*mem_state);
    sys.resume();

    return 0;
}

static fat::path default_state_path {L"\\snes-state.yaml"};

void save_state_to_file(class sys& sys, const sys::state_t& st,
                        const fat::path& state_p)
{
    fat::fstream f{state_p, std::ios::out | std::ios::trunc};
    if (!f)
        throw std::runtime_error("error opening file");
    yamlify_set_printable(false);
    yamlify_sys_state(sys, st, f);
    if (!f)
        throw std::runtime_error("error writing file");

    std::cout << "Saved state to " << state_p << "\n";
}

void load_state_from_file(const fat::path& state_p, sys::state_t& st)
{
    fat::fstream f{state_p, std::ios::in};
    if (!f)
        throw std::runtime_error("error opening file");
    deyamlify_sys_state(f, st);
    if (!f)
        throw std::runtime_error("error reading file");

    std::cout << "Loaded state from " << state_p << "\n";
}

int cli::save_state(const command::args_t& args)
{
    auto it = args.begin();
    fat::path state_p {default_state_path};
    if (it < args.end()) {
        auto str = *(it++);
        state_p = fat::path(str);
    }

    sys::state_t st;
    sys.pause();
    sys.read_state(st);
    sys.resume();
    save_state_to_file(sys, st, state_p);

    return 0;
}

int cli::load_resave_state(const command::args_t& args)
{
    auto it = args.begin();
    fat::path in_state_p {default_state_path};
    if (it < args.end()) {
        auto str = *(it++);
        in_state_p = fat::path(str);
    }
    fat::path out_state_p {L"\\snes-state2.yaml"};
    if (it < args.end()) {
        auto str = *(it++);
        out_state_p = fat::path(str);
    }

    sys::state_t st;
    {
        fat::fstream f{in_state_p, std::ios::in};
        if (!f)
            throw std::runtime_error("error opening load file");
        deyamlify_sys_state(f, st);
    }

    std::cout << "Loaded state from " << in_state_p << "\n";

    {
        fat::fstream f{out_state_p, std::ios::out | std::ios::trunc};
        if (!f)
            throw std::runtime_error("error opening save file");
        yamlify_set_printable(false);
        yamlify_sys_state(sys, st, f);
        if (!f)
            throw std::runtime_error("error writing file");
    }

    std::cout << "Re-saved state to " << out_state_p << "\n";

    {
        bool matched = true;
        fat::fstream fl{in_state_p, std::ios::in};
        fat::fstream fs{out_state_p, std::ios::in};

        if (!fl || !fs)
            throw std::runtime_error("error opening both files");
        while (fl.good() && fs.good()) {
            char buf[1024];
            fl.read(buf, sizeof(buf));
            auto len = fl.gcount();
            if (len) {
                auto fl_crc = calc_crc32(reinterpret_cast<uint8_t*>(buf), len);
                fs.read(buf, len);
                auto fs_crc = calc_crc32(reinterpret_cast<uint8_t*>(buf), len);
                if (fs.gcount() != len || fl_crc != fs_crc) {
                    matched = false;
                    break;
                }
            }
        }
        std::cout << "State files are "
                  << (matched ? "identical" : "different!") << '\n';
    }

    return 0;
}

int cli::load_state(const command::args_t& args)
{
    bool halt = false;
    auto it = args.begin();
    if (it != args.end()) {
        if (*it == "--halt") {
            it++;
            halt = true;
        }
    }
    fat::path state_p {default_state_path};
    if (it < args.end()) {
        auto str = *(it++);
        state_p = fat::path(str);
    }

    sys::state_t st;
    load_state_from_file(state_p, st);
    if (!halt)
        sys.pause();
    else
        sys.cpu.icd.force_halt();
    sys.write_state(st);
    if (!halt)
        sys.resume();

    return 0;
}

int cli::read_save_write_read_save_state(const command::args_t& args)
{
    auto it = args.begin();
    fat::path state1_p {default_state_path};
    if (it < args.end()) {
        auto str = *(it++);
        state1_p = fat::path(str);
    }
    fat::path state2_p {L"\\snes-state2.yaml"};
    if (it < args.end()) {
        auto str = *(it++);
        state2_p = fat::path(str);
    }

    sys::state_t st;
    sys.pause();
    sys.read_state(st);
    save_state_to_file(sys, st, state1_p);
    sys.write_state(st);
    sys.read_state(st);
    save_state_to_file(sys, st, state2_p);
    sys.resume();

    return 0;
}

} // namespace snes
