#include <sstream>
#include <iomanip>
#include <mutex>
#include "emubus.h"

#include "icd_match.h"

namespace snes {

std::mutex icd_mtx;

bool icd_match::trigger::is_a_set() const
{
    return is_set(EMUBUS_ICD_MATCH_REG_D0_CPU_A);
}

bool icd_match::trigger::is_db_set() const
{
    return is_set(EMUBUS_ICD_MATCH_REG_D0_CPU_DB);
}

bool icd_match::trigger::is_rw_set() const
{
    return is_set(EMUBUS_ICD_MATCH_REG_D1_CPU_RW, 1);
}

bool icd_match::trigger::is_sync_set() const
{
    return is_set(EMUBUS_ICD_MATCH_REG_D1_CPU_SYNC, 1);
}

bool icd_match::trigger::is_col_set() const
{
    return is_set(EMUBUS_ICD_MATCH_REG_D1_PPU_COL, 1);
}

bool icd_match::trigger::is_row_set() const
{
    return is_set(EMUBUS_ICD_MATCH_REG_D1_PPU_ROW, 1);
}

bool icd_match::trigger::is_frame_set() const
{
    return is_set(EMUBUS_ICD_MATCH_REG_D2_PPU_FRAME, 2);
}

uint32_t icd_match::trigger::get_a() const
{
    return uint32_t(get(EMUBUS_ICD_MATCH_REG_D0_CPU_A) >> EMUBUS_ICD_MATCH_REG_D0_CPU_A_S);
}

uint8_t icd_match::trigger::get_db() const
{
    return uint8_t(get(EMUBUS_ICD_MATCH_REG_D0_CPU_DB) >> EMUBUS_ICD_MATCH_REG_D0_CPU_DB_S);
}

bool icd_match::trigger::get_rw() const
{
    return get(EMUBUS_ICD_MATCH_REG_D1_CPU_RW, 1);
}

bool icd_match::trigger::get_sync() const
{
    return get(EMUBUS_ICD_MATCH_REG_D1_CPU_SYNC, 1);
}

uint16_t icd_match::trigger::get_col() const
{
    return uint16_t(get(EMUBUS_ICD_MATCH_REG_D1_PPU_COL, 1) >> EMUBUS_ICD_MATCH_REG_D1_PPU_COL_S);
}

uint16_t icd_match::trigger::get_row() const
{
    return uint16_t(get(EMUBUS_ICD_MATCH_REG_D1_PPU_ROW, 1) >> EMUBUS_ICD_MATCH_REG_D1_PPU_ROW_S);
}

uint32_t icd_match::trigger::get_frame() const
{
    return get(EMUBUS_ICD_MATCH_REG_D2_PPU_FRAME, 2) >> EMUBUS_ICD_MATCH_REG_D2_PPU_FRAME_S;
}

void icd_match::trigger::set_a(uint32_t v)
{
    uint32_t bits = uint32_t(v) << EMUBUS_ICD_MATCH_REG_D0_CPU_A_S;
    uint32_t mask = EMUBUS_ICD_MATCH_REG_D0_CPU_A;
    set(bits, mask);
}

void icd_match::trigger::set_db(uint8_t v)
{
    uint32_t bits = uint32_t(v) << EMUBUS_ICD_MATCH_REG_D0_CPU_DB_S;
    uint32_t mask = EMUBUS_ICD_MATCH_REG_D0_CPU_DB;
    set(bits, mask);
}

void icd_match::trigger::set_rw(bool v)
{
    uint32_t mask = EMUBUS_ICD_MATCH_REG_D1_CPU_RW;
    uint32_t bits = v * mask;
    set(bits, mask, 1);
}

void icd_match::trigger::set_sync(bool v)
{
    uint32_t mask = EMUBUS_ICD_MATCH_REG_D1_CPU_SYNC;
    uint32_t bits = v * mask;
    set(bits, mask, 1);
}

void icd_match::trigger::set_col(uint16_t v)
{
    uint32_t bits = uint32_t(v) << EMUBUS_ICD_MATCH_REG_D1_PPU_COL_S;
    uint32_t mask = EMUBUS_ICD_MATCH_REG_D1_PPU_COL;
    set(bits, mask, 1);
}

void icd_match::trigger::set_row(uint16_t v)
{
    uint32_t bits = uint32_t(v) << EMUBUS_ICD_MATCH_REG_D1_PPU_ROW_S;
    uint32_t mask = EMUBUS_ICD_MATCH_REG_D1_PPU_ROW;
    set(bits, mask, 1);
}

void icd_match::trigger::set_frame(uint32_t v)
{
    uint32_t bits = uint32_t(v) << EMUBUS_ICD_MATCH_REG_D2_PPU_FRAME_S;
    uint32_t mask = EMUBUS_ICD_MATCH_REG_D2_PPU_FRAME;
    set(bits, mask, 2);
}

void icd_match::trigger::clear_a()
{
    clear(EMUBUS_ICD_MATCH_REG_D0_CPU_A);
}

void icd_match::trigger::clear_db()
{
    clear(EMUBUS_ICD_MATCH_REG_D0_CPU_DB);
}

void icd_match::trigger::clear_rw()
{
    clear(EMUBUS_ICD_MATCH_REG_D1_CPU_RW, 1);
}

void icd_match::trigger::clear_sync()
{
    clear(EMUBUS_ICD_MATCH_REG_D1_CPU_SYNC, 1);
}

void icd_match::trigger::clear_col()
{
    clear(EMUBUS_ICD_MATCH_REG_D1_PPU_COL, 1);
}

void icd_match::trigger::clear_row()
{
    clear(EMUBUS_ICD_MATCH_REG_D1_PPU_ROW, 1);
}

void icd_match::trigger::clear_frame()
{
    clear(EMUBUS_ICD_MATCH_REG_D2_PPU_FRAME, 2);
}

std::string icd_match::trigger::to_string() const
{
    std::stringstream ss;
    ss << std::setfill('0');
    if (is_a_set())
        ss << "A=" << std::setw(6) << std::hex << get_a();
    if (is_db_set()) {
        if (ss.tellp())
            ss << ", ";
        ss << "DB=" << std::setw(2) << std::hex << unsigned(get_db());
    }
    if (is_rw_set()) {
        if (ss.tellp())
            ss << ", ";
        ss << "RW=" << (get_rw() ? 'R' : 'W');
    }
    if (is_sync_set()) {
        if (ss.tellp())
            ss << ", ";
        ss << "SYNC=" << std::dec << get_sync();
    }
    if (is_col_set()) {
        if (ss.tellp())
            ss << ", ";
        ss << "COL=" << std::dec << get_col();
    }
    if (is_row_set()) {
        if (ss.tellp())
            ss << ", ";
        ss << "ROW=" << std::dec << get_row();
    }
    if (is_frame_set()) {
        if (ss.tellp())
            ss << ", ";
        ss << "FRAME=" << std::dec << get_frame();
    }
    return ss.str();
}

bool icd_match::trigger::is_set(uint32_t mask, int idx) const
{
    return (den[idx] & mask) == mask;
}

uint32_t icd_match::trigger::get(uint32_t mask, int idx) const
{
    return din[idx] & mask;
}

void icd_match::trigger::set(uint32_t bits, uint32_t mask, int idx)
{
    din[idx] = (din[idx] & ~mask) | (bits & mask);
    den[idx] |= mask;
}

void icd_match::trigger::clear(uint32_t mask, int idx)
{
    din[idx] &= ~mask;
    den[idx] &= ~mask;
}

//////////////////////////////////////////////////////////////////////

icd_match::icd_match(int idx)
    : _idx(idx)
{
}

icd_match::status icd_match::get_status()
{
    std::unique_lock<std::mutex> lck{icd_mtx};
    EMUBUS_ICD_MATCH_SEL = _idx;

    EMUBUS_ICD_MATCH_REG_SEL = EMUBUS_ICD_MATCH_REG_SEL_STATUS;
    uint32_t reg = EMUBUS_ICD_MATCH_REG_DATA;

    status s;
    s.match_sel = (reg & EMUBUS_ICD_MATCH_REG_STATUS_MATCH_SEL) >> EMUBUS_ICD_MATCH_REG_STATUS_MATCH_SEL_S;
    s.num_match = (reg & EMUBUS_ICD_MATCH_REG_STATUS_NUM_MATCH) >> EMUBUS_ICD_MATCH_REG_STATUS_NUM_MATCH_S;

    return s;
}

bool icd_match::is_enabled()
{
    return EMUBUS_ICD_CTRL & ((EMUBUS_ICD_CTRL_MATCH_ENABLE / 255) << _idx);
}

bool icd_match::is_triggered()
{
    return EMUBUS_ICD_CTRL & ((EMUBUS_ICD_CTRL_MATCH_TRIGGER / 255) << _idx);
}

icd_match::data icd_match::get_data()
{
    std::unique_lock<std::mutex> lck{icd_mtx};
    EMUBUS_ICD_MATCH_SEL = _idx;

    uint32_t dout[4];
    EMUBUS_ICD_MATCH_REG_SEL = EMUBUS_ICD_MATCH_REG_SEL_D0OUT;
    dout[0] = EMUBUS_ICD_MATCH_REG_DATA;
    EMUBUS_ICD_MATCH_REG_SEL = EMUBUS_ICD_MATCH_REG_SEL_D1OUT;
    dout[1] = EMUBUS_ICD_MATCH_REG_DATA;
    EMUBUS_ICD_MATCH_REG_SEL = EMUBUS_ICD_MATCH_REG_SEL_D2OUT;
    dout[2] = EMUBUS_ICD_MATCH_REG_DATA;
    EMUBUS_ICD_MATCH_REG_SEL = EMUBUS_ICD_MATCH_REG_SEL_D3OUT;
    dout[3] = EMUBUS_ICD_MATCH_REG_DATA;

    data d;
    d.a = (dout[0] & EMUBUS_ICD_MATCH_REG_D0_CPU_A) >> EMUBUS_ICD_MATCH_REG_D0_CPU_A_S;
    d.db = (dout[0] & EMUBUS_ICD_MATCH_REG_D0_CPU_DB) >> EMUBUS_ICD_MATCH_REG_D0_CPU_DB_S;
    d.rw = dout[1] & EMUBUS_ICD_MATCH_REG_D1_CPU_RW;
    d.sync = dout[1] & EMUBUS_ICD_MATCH_REG_D1_CPU_SYNC;
    d.col = (dout[1] & EMUBUS_ICD_MATCH_REG_D1_PPU_COL) >> EMUBUS_ICD_MATCH_REG_D1_PPU_COL_S;
    d.row = (dout[1] & EMUBUS_ICD_MATCH_REG_D1_PPU_ROW) >> EMUBUS_ICD_MATCH_REG_D1_PPU_ROW_S;
    d.frame = (dout[2] & EMUBUS_ICD_MATCH_REG_D2_PPU_FRAME) >> EMUBUS_ICD_MATCH_REG_D2_PPU_FRAME_S;
    d.cpu_cyc = dout[3];

    return d;
}

void icd_match::set_trigger(const icd_match::trigger& t)
{
    std::unique_lock<std::mutex> lck{icd_mtx};
    EMUBUS_ICD_MATCH_SEL = _idx;

    EMUBUS_ICD_MATCH_REG_SEL = EMUBUS_ICD_MATCH_REG_SEL_D0IN;
    EMUBUS_ICD_MATCH_REG_DATA = t.din[0];
    EMUBUS_ICD_MATCH_REG_SEL = EMUBUS_ICD_MATCH_REG_SEL_D1IN;
    EMUBUS_ICD_MATCH_REG_DATA = t.din[1];
    EMUBUS_ICD_MATCH_REG_SEL = EMUBUS_ICD_MATCH_REG_SEL_D2IN;
    EMUBUS_ICD_MATCH_REG_DATA = t.din[2];
    EMUBUS_ICD_MATCH_REG_SEL = EMUBUS_ICD_MATCH_REG_SEL_D0EN;
    EMUBUS_ICD_MATCH_REG_DATA = t.den[0];
    EMUBUS_ICD_MATCH_REG_SEL = EMUBUS_ICD_MATCH_REG_SEL_D1EN;
    EMUBUS_ICD_MATCH_REG_DATA = t.den[1];
    EMUBUS_ICD_MATCH_REG_SEL = EMUBUS_ICD_MATCH_REG_SEL_D2EN;
    EMUBUS_ICD_MATCH_REG_DATA = t.den[2];
}

void icd_match::disable()
{
    std::unique_lock<std::mutex> lck{icd_mtx};
    EMUBUS_ICD_CTRL &= ~((EMUBUS_ICD_CTRL_MATCH_ENABLE / 255) << _idx);
}

void icd_match::enable()
{
    std::unique_lock<std::mutex> lck{icd_mtx};
    EMUBUS_ICD_CTRL |= ((EMUBUS_ICD_CTRL_MATCH_ENABLE / 255) << _idx);
}

void icd_match::reset_triggered()
{
    std::unique_lock<std::mutex> lck{icd_mtx};
    auto ctrl = EMUBUS_ICD_CTRL;
    auto bit = (EMUBUS_ICD_CTRL_MATCH_ENABLE / 255) << _idx;
    if (ctrl & bit) {
        // Clearing enable resets trigger.
        EMUBUS_ICD_CTRL = ctrl & ~bit;
        EMUBUS_ICD_CTRL = ctrl;
    }
}

} // namespace snes
