#include "apu.h"

#include "emubus.h"
#include "memptr.h"

#include <util/membuf.h>

#include <unistd.h>
#include <string.h>

namespace snes {

void apu::init()
{
}

uint32_t apu::read_reg(uint32_t sel)
{
    EMUBUS_APU_MON_SEL = sel;
    uint32_t val = EMUBUS_APU_MON_DATA;
    return val;
}

template<size_t Len, typename T>
void apu::read_array(uint32_t sel, T array[Len])
{
    for (size_t i = 0; i < Len; i++)
        array[i] = read_reg(sel + i);
}

void apu::write_reg(uint32_t sel, uint32_t val)
{
    EMUBUS_APU_MON_SEL = sel;
    EMUBUS_APU_MON_DATA = val;
}

template<size_t Len, typename T>
void apu::write_array(uint32_t sel, const T array[Len])
{
    for (size_t i = 0; i < Len; i++)
        write_reg(sel + i, array[i]);
}

#define XTRACT1(reg, name)      (((reg) & name) != 0)
#define XTRACTN(reg, name)      (((reg) & name) >> name##_S)

apu_smp_state apu::read_smp_state()
{
    apu_smp_state ret;

    auto arc0 = read_reg(EMUBUS_APU_MON_SEL_ARC0);
    auto arc1 = read_reg(EMUBUS_APU_MON_SEL_ARC1);
    auto uar0 = read_reg(EMUBUS_APU_MON_SEL_UAR0);
    auto uar1 = read_reg(EMUBUS_APU_MON_SEL_UAR1);
    auto stcs = read_reg(EMUBUS_APU_MON_SEL_STCS);
    auto sps  = read_reg(EMUBUS_APU_MON_SEL_SPS);
    auto cpui = read_reg(EMUBUS_APU_MON_SEL_CPUI);
    auto cpuo = read_reg(EMUBUS_APU_MON_SEL_CPUO);
    auto spr0 = read_reg(EMUBUS_APU_MON_SEL_SPR0);
    auto spr1 = read_reg(EMUBUS_APU_MON_SEL_SPR1);
    auto int0 = read_reg(EMUBUS_APU_MON_SEL_INT0);
    auto int1 = read_reg(EMUBUS_APU_MON_SEL_INT1);
    auto int2 = read_reg(EMUBUS_APU_MON_SEL_INT2);
    auto int3 = read_reg(EMUBUS_APU_MON_SEL_INT3);
    auto int4 = read_reg(EMUBUS_APU_MON_SEL_INT4);
    auto int5 = read_reg(EMUBUS_APU_MON_SEL_INT5);
    auto int6 = read_reg(EMUBUS_APU_MON_SEL_INT6);
    auto int7 = read_reg(EMUBUS_APU_MON_SEL_INT7);

    ret.pc = XTRACTN(arc0, EMUBUS_APU_MON_ARC0_PC);
    ret.pf.c = XTRACT1(arc0, EMUBUS_APU_MON_ARC0_PF_C);
    ret.pf.z = XTRACT1(arc0, EMUBUS_APU_MON_ARC0_PF_Z);
    ret.pf.i = XTRACT1(arc0, EMUBUS_APU_MON_ARC0_PF_I);
    ret.pf.h = XTRACT1(arc0, EMUBUS_APU_MON_ARC0_PF_H);
    ret.pf.b = XTRACT1(arc0, EMUBUS_APU_MON_ARC0_PF_B);
    ret.pf.p = XTRACT1(arc0, EMUBUS_APU_MON_ARC0_PF_P);
    ret.pf.v = XTRACT1(arc0, EMUBUS_APU_MON_ARC0_PF_V);
    ret.pf.n = XTRACT1(arc0, EMUBUS_APU_MON_ARC0_PF_N);
    ret.s = XTRACTN(arc1, EMUBUS_APU_MON_ARC1_S);
    ret.ac = XTRACTN(arc1, EMUBUS_APU_MON_ARC1_AC);
    ret.x = XTRACTN(arc1, EMUBUS_APU_MON_ARC1_X);
    ret.y = XTRACTN(arc1, EMUBUS_APU_MON_ARC1_Y);

    ret.ir = XTRACTN(uar0, EMUBUS_APU_MON_UAR0_IR);
    ret.aor = XTRACTN(uar0, EMUBUS_APU_MON_UAR0_AOR);
    ret.dor = XTRACTN(uar1, EMUBUS_APU_MON_UAR0_DOR);
    ret.z = XTRACTN(uar1, EMUBUS_APU_MON_UAR1_Z);

    ret.tstate.ts = XTRACTN(stcs, EMUBUS_APU_MON_STCS_TS);

    ret.pstate.rw = XTRACT1(sps, EMUBUS_APU_MON_SPS_RW);
    ret.pstate.sync = XTRACT1(sps, EMUBUS_APU_MON_SPS_SYNC);

    ret.periph.cpui[0] = XTRACTN(cpui, EMUBUS_APU_MON_CPUI_CPUI0);
    ret.periph.cpui[1] = XTRACTN(cpui, EMUBUS_APU_MON_CPUI_CPUI1);
    ret.periph.cpui[2] = XTRACTN(cpui, EMUBUS_APU_MON_CPUI_CPUI2);
    ret.periph.cpui[3] = XTRACTN(cpui, EMUBUS_APU_MON_CPUI_CPUI3);
    ret.periph.cpuo[0] = XTRACTN(cpuo, EMUBUS_APU_MON_CPUO_CPUO0);
    ret.periph.cpuo[1] = XTRACTN(cpuo, EMUBUS_APU_MON_CPUO_CPUO1);
    ret.periph.cpuo[2] = XTRACTN(cpuo, EMUBUS_APU_MON_CPUO_CPUO2);
    ret.periph.cpuo[3] = XTRACTN(cpuo, EMUBUS_APU_MON_CPUO_CPUO3);
    ret.periph.spcr = XTRACTN(spr0, EMUBUS_APU_MON_SPR0_SPCR);
    ret.periph.spt[0] = XTRACTN(spr0, EMUBUS_APU_MON_SPR0_SPT0);
    ret.periph.spt[1] = XTRACTN(spr0, EMUBUS_APU_MON_SPR0_SPT1);
    ret.periph.spt[2] = XTRACTN(spr0, EMUBUS_APU_MON_SPR0_SPT2);
    ret.periph.spc[0] = XTRACTN(spr1, EMUBUS_APU_MON_SPR1_SPC0);
    ret.periph.spc[1] = XTRACTN(spr1, EMUBUS_APU_MON_SPR1_SPC1);
    ret.periph.spc[2] = XTRACTN(spr1, EMUBUS_APU_MON_SPR1_SPC2);

    ret.internal.cken_d = XTRACT1(int0, EMUBUS_APU_MON_INT0_CKEN_D);
    ret.internal.cp1d = XTRACT1(int0, EMUBUS_APU_MON_INT0_CP1D);
    ret.internal.resp = XTRACT1(int0, EMUBUS_APU_MON_INT0_RESP);
    ret.internal.am_done_d = XTRACT1(int0, EMUBUS_APU_MON_INT0_AM_DONE_D);
    ret.internal.cco = XTRACT1(int0, EMUBUS_APU_MON_INT0_CCO);
    ret.internal.cvo = XTRACT1(int0, EMUBUS_APU_MON_INT0_CVO);
    ret.internal.cho = XTRACT1(int0, EMUBUS_APU_MON_INT0_CHO);
    ret.internal.cl_store_dor_d = XTRACT1(int0, EMUBUS_APU_MON_INT0_CL_STORE_DOR_D);
    ret.internal.cl_int_op_d = XTRACT1(int0, EMUBUS_APU_MON_INT0_CL_INT_OP_D);
    ret.internal.t = XTRACTN(int1, EMUBUS_APU_MON_INT1_T);
    ret.internal.bra_t = XTRACTN(int1, EMUBUS_APU_MON_INT1_BRA_T);
    ret.internal.divcnt = XTRACTN(int1, EMUBUS_APU_MON_INT1_DIVCNT);
    ret.internal.am_t = XTRACTN(int2, EMUBUS_APU_MON_INT2_AM_T);
    ret.internal.dl = XTRACTN(int2, EMUBUS_APU_MON_INT2_DL);
    ret.internal.op_t = XTRACTN(int3, EMUBUS_APU_MON_INT3_OP_T);
    ret.internal.ico = XTRACTN(int4, EMUBUS_APU_MON_INT4_ICO);
    ret.internal.icoh = XTRACTN(int4, EMUBUS_APU_MON_INT4_ICOH);
    ret.internal.mulscnt = XTRACTN(int4, EMUBUS_APU_MON_INT4_MULSCNT);
    ret.internal.timer0 = XTRACTN(int5, EMUBUS_APU_MON_INT5_TIMER0);
    ret.internal.timer1 = XTRACTN(int6, EMUBUS_APU_MON_INT6_TIMER1);
    ret.internal.timer2 = XTRACTN(int7, EMUBUS_APU_MON_INT7_TIMER2);

    return ret;
}

apu_dsp_state apu::read_dsp_state(int which)
{
    apu_dsp_state ret;

    if (which & dsp_state_regs) {
        for (int i = 0; i < 0x80; i++) {
            write_reg(EMUBUS_APU_MON_SEL_DRS, i);
            ret.regs.regs[i] = read_reg(EMUBUS_APU_MON_SEL_DRD);
        }
    }

    if (which & dsp_state_internal) {
        uint32_t reg;
        auto& i = ret.internal;

        reg = read_reg(EMUBUS_APU_MON_SEL_REGFILE);
        i.reg_file.kon_written = XTRACT1(reg, EMUBUS_APU_MON_REGFILE_KON_WRITTEN);
        i.reg_file.flg_quick = XTRACTN(reg, EMUBUS_APU_MON_REGFILE_FLG_QUICK);

        reg = read_reg(EMUBUS_APU_MON_SEL_CLKGEN);
        i.clkgen.idivcnt = XTRACTN(reg, EMUBUS_APU_MON_CLKGEN_IDIVCNT);
        i.clkgen.cccnt = XTRACTN(reg, EMUBUS_APU_MON_CLKGEN_CCCNT);
        i.clkgen.nres = XTRACT1(reg, EMUBUS_APU_MON_CLKGEN_NRES);

        reg = read_reg(EMUBUS_APU_MON_SEL_SMP0);
        i.smp_if.d_o_d = XTRACTN(reg, EMUBUS_APU_MON_SMP0_D_O_D);
        i.smp_if.ccken_d = XTRACT1(reg, EMUBUS_APU_MON_SMP0_CCKEN_D);
        reg = read_reg(EMUBUS_APU_MON_SEL_SMP1);
        i.smp_if.spdrs = XTRACTN(reg, EMUBUS_APU_MON_SMP1_SPDRS);

        reg = read_reg(EMUBUS_APU_MON_SEL_SRAM0);
        i.sram_if.ima = XTRACTN(reg, EMUBUS_APU_MON_SRAM0_IMA);
        i.sram_if.imdi = XTRACTN(reg, EMUBUS_APU_MON_SRAM0_IMDI);
        i.sram_if.imrs = XTRACT1(reg, EMUBUS_APU_MON_SRAM0_IMRS);
        i.sram_if.imws = XTRACT1(reg, EMUBUS_APU_MON_SRAM0_IMWS);
        i.sram_if.mad_ssmp = XTRACT1(reg, EMUBUS_APU_MON_SRAM0_MAD_SSMP);

        reg = read_reg(EMUBUS_APU_MON_SEL_GLB_CTR);
        i.glb_ctr.cnt = XTRACTN(reg, EMUBUS_APU_MON_GLB_CTR_CNT);

        auto& sl = i.sgenloop;
        reg = read_reg(EMUBUS_APU_MON_SEL_SL0);
        sl.sglcnt = XTRACTN(reg, EMUBUS_APU_MON_SL0_SGLCNT);
        sl.pmon = XTRACTN(reg, EMUBUS_APU_MON_SL0_PMON);
        sl.non = XTRACTN(reg, EMUBUS_APU_MON_SL0_NON);
        sl.dir = XTRACTN(reg, EMUBUS_APU_MON_SL0_DIR);
        reg = read_reg(EMUBUS_APU_MON_SEL_SL1);
        sl.kon = XTRACTN(reg, EMUBUS_APU_MON_SL1_KON);
        sl.kof = XTRACTN(reg, EMUBUS_APU_MON_SL1_KOF);
        sl.mvoll = XTRACTN(reg, EMUBUS_APU_MON_SL1_MVOLL);
        sl.mvolr = XTRACTN(reg, EMUBUS_APU_MON_SL1_MVOLR);
        reg = read_reg(EMUBUS_APU_MON_SEL_SL2);
        sl.evoll = XTRACTN(reg, EMUBUS_APU_MON_SL2_EVOLL);
        sl.evolr = XTRACTN(reg, EMUBUS_APU_MON_SL2_EVOLR);
        sl.flgn = XTRACTN(reg, EMUBUS_APU_MON_SL2_FLGN);

        reg = read_reg(EMUBUS_APU_MON_SEL_NOISE);
        sl.noise_lfsr = XTRACTN(reg, EMUBUS_APU_MON_NOISE_LFSR);

        auto& v = sl.voice;
        reg = read_reg(EMUBUS_APU_MON_SEL_VOICE0);
        v.srcn = XTRACTN(reg, EMUBUS_APU_MON_VOICE0_SRCN);
        v.srcnd = XTRACTN(reg, EMUBUS_APU_MON_VOICE0_SRCND);
        v.pitch = XTRACTN(reg, EMUBUS_APU_MON_VOICE0_PITCH);
        reg = read_reg(EMUBUS_APU_MON_SEL_VOICE1);
        v.adsr1 = XTRACTN(reg, EMUBUS_APU_MON_VOICE1_ADSR1);
        v.adsr2_gain = XTRACTN(reg, EMUBUS_APU_MON_VOICE1_ADSR2_GAIN);
        v.voll = XTRACTN(reg, EMUBUS_APU_MON_VOICE1_VOLL);
        v.volr = XTRACTN(reg, EMUBUS_APU_MON_VOICE1_VOLR);
        reg = read_reg(EMUBUS_APU_MON_SEL_VOICE3);
        v.envx = XTRACTN(reg, EMUBUS_APU_MON_VOICE3_ENVX);

        read_array<EMUBUS_APU_MON_SEL_BRRDEC_CTX_LEN>(EMUBUS_APU_MON_SEL_BRRDEC_CTX, v.brrdec.ctx);
        reg = read_reg(EMUBUS_APU_MON_SEL_BRRDEC0);
        v.brrdec.bbcnt = XTRACTN(reg, EMUBUS_APU_MON_BRRDEC0_BBCNT);
        v.brrdec.srcn0_load = XTRACT1(reg, EMUBUS_APU_MON_BRRDEC0_SRCN0_LOAD);
        v.brrdec.srcn1_load = XTRACT1(reg, EMUBUS_APU_MON_BRRDEC0_SRCN1_LOAD);
        v.brrdec.srcn_inc = XTRACT1(reg, EMUBUS_APU_MON_BRRDEC0_SRCN_INC);
        v.brrdec.rd_dta1 = XTRACT1(reg, EMUBUS_APU_MON_BRRDEC0_RD_DTA1);
        v.brrdec.hdr = XTRACTN(reg, EMUBUS_APU_MON_BRRDEC0_HDR);
        v.brrdec.dat = XTRACTN(reg, EMUBUS_APU_MON_BRRDEC0_DAT);
        v.brrdec.rd_brrfsm1 = XTRACT1(reg, EMUBUS_APU_MON_BRRDEC0_RD_BRRFSM1);
        v.brrdec.brrfsm1 = XTRACT1(reg, EMUBUS_APU_MON_BRRDEC0_BRRFSM1);
        v.brrdec.brrfsm2 = XTRACT1(reg, EMUBUS_APU_MON_BRRDEC0_BRRFSM2);
        v.brrdec.dec_buf_full = XTRACT1(reg, EMUBUS_APU_MON_BRRDEC0_DEC_BUF_FULL);
        reg = read_reg(EMUBUS_APU_MON_SEL_BRRDEC1);
        v.brrdec.srcnc = XTRACTN(reg, EMUBUS_APU_MON_BRRDEC1_SRCNC);
        
        reg = read_reg(EMUBUS_APU_MON_SEL_DECBUF0);
        v.decbuf.play = XTRACT1(reg, EMUBUS_APU_MON_DECBUF0_PLAY);
        v.decbuf.out_samp = XTRACT1(reg, EMUBUS_APU_MON_DECBUF0_OUT_SAMP);
        reg = read_reg(EMUBUS_APU_MON_SEL_DECBUF1);
        v.decbuf.deccnt[0] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT0);
        v.decbuf.deccnt[1] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT1);
        v.decbuf.deccnt[2] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT2);
        v.decbuf.deccnt[3] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT3);
        v.decbuf.deccnt[4] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT4);
        v.decbuf.deccnt[5] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT5);
        v.decbuf.deccnt[6] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT6);
        v.decbuf.deccnt[7] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT7);
        reg = read_reg(EMUBUS_APU_MON_SEL_DECBUF2);
        v.decbuf.intcnt[0] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT0);
        v.decbuf.intcnt[1] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT1);
        v.decbuf.intcnt[2] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT2);
        v.decbuf.intcnt[3] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT3);
        v.decbuf.intcnt[4] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT4);
        v.decbuf.intcnt[5] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT5);
        v.decbuf.intcnt[6] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT6);
        v.decbuf.intcnt[7] = XTRACTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT7);
        read_array<EMUBUS_APU_MON_SEL_DECBUF_MEM_LEN>(EMUBUS_APU_MON_SEL_DECBUF_MEM, v.decbuf.mem);

        read_array<EMUBUS_APU_MON_SEL_INTERP_CTX_LEN>(EMUBUS_APU_MON_SEL_INTERP_CTX, v.interp.ctx);
        reg = read_reg(EMUBUS_APU_MON_SEL_INTERP0);
        v.interp.pcnt = XTRACTN(reg, EMUBUS_APU_MON_INTERP0_PCNT);
        v.interp.pstep = XTRACTN(reg, EMUBUS_APU_MON_INTERP0_PSTEP);
        reg = read_reg(EMUBUS_APU_MON_SEL_INTERP1);
        v.interp.acc = XTRACTN(reg, EMUBUS_APU_MON_INTERP1_ACC);
        v.interp.gos = XTRACTN(reg, EMUBUS_APU_MON_INTERP1_GOS);
        reg = read_reg(EMUBUS_APU_MON_SEL_INTERP2);
        v.interp.sout = XTRACTN(reg, EMUBUS_APU_MON_INTERP2_SOUT);

        read_array<EMUBUS_APU_MON_SEL_ENV_CTX_LEN>(EMUBUS_APU_MON_SEL_ENV_CTX, v.env.ctx);
        reg = read_reg(EMUBUS_APU_MON_SEL_ENV0);
        v.env.envelope = XTRACTN(reg, EMUBUS_APU_MON_ENV0_ENVELOPE);
        v.env.adsr_st = XTRACTN(reg, EMUBUS_APU_MON_ENV0_ADSR_ST);
        v.env.ctx_swap = XTRACT1(reg, EMUBUS_APU_MON_ENV0_CTX_SWAP);
        
        reg = read_reg(EMUBUS_APU_MON_SEL_SP0);
        v.scale_pan.vml = XTRACT1(reg, EMUBUS_APU_MON_SP0_VML);
        v.scale_pan.vmr = XTRACT1(reg, EMUBUS_APU_MON_SP0_VMR);
        v.scale_pan.out = XTRACTN(reg, EMUBUS_APU_MON_SP0_OUT);
        reg = read_reg(EMUBUS_APU_MON_SEL_SP1);
        v.scale_pan.outl = XTRACTN(reg, EMUBUS_APU_MON_SP1_OUTL);
        v.scale_pan.outr = XTRACTN(reg, EMUBUS_APU_MON_SP1_OUTR);

        auto& e = sl.echo;
        reg = read_reg(EMUBUS_APU_MON_SEL_ECHO0);
        e.efb = XTRACTN(reg, EMUBUS_APU_MON_ECHO0_EFB);
        e.eon = XTRACTN(reg, EMUBUS_APU_MON_ECHO0_EON);
        e.edl = XTRACTN(reg, EMUBUS_APU_MON_ECHO0_EDL);
        e.esa = XTRACTN(reg, EMUBUS_APU_MON_ECHO0_ESA);
        reg = read_reg(EMUBUS_APU_MON_SEL_ECHO1);
        e.fcrs = XTRACTN(reg, EMUBUS_APU_MON_ECHO1_FCRS);
        e.fcls = XTRACTN(reg, EMUBUS_APU_MON_ECHO1_FCLS);
        e.fcl = XTRACT1(reg, EMUBUS_APU_MON_ECHO1_FCL);
        e.idx = XTRACTN(reg, EMUBUS_APU_MON_ECHO1_IDX);
        e.ece = XTRACT1(reg, EMUBUS_APU_MON_ECHO1_ECE);
        reg = read_reg(EMUBUS_APU_MON_SEL_ECHO2);
        e.idx_max = XTRACTN(reg, EMUBUS_APU_MON_ECHO2_IDX_MAX);
        e.buf_ptr = XTRACTN(reg, EMUBUS_APU_MON_ECHO2_BUF_PTR);
        reg = read_reg(EMUBUS_APU_MON_SEL_ECHO3);
        e.eboutl = XTRACTN(reg, EMUBUS_APU_MON_ECHO3_EBOUTL);
        e.eboutr = XTRACTN(reg, EMUBUS_APU_MON_ECHO3_EBOUTR);

        reg = read_reg(EMUBUS_APU_MON_SEL_EMIX0);
        e.mixer.suml = XTRACTN(reg, EMUBUS_APU_MON_EMIX0_SUML);
        e.mixer.vsel = XTRACTN(reg, EMUBUS_APU_MON_EMIX0_VSEL);
        reg = read_reg(EMUBUS_APU_MON_SEL_EMIX1);
        e.mixer.sumr = XTRACTN(reg, EMUBUS_APU_MON_EMIX1_SUMR);
        reg = read_reg(EMUBUS_APU_MON_SEL_EMIX2);
        e.mixer.soutl = XTRACTN(reg, EMUBUS_APU_MON_EMIX2_SOUTL);
        e.mixer.soutr = XTRACTN(reg, EMUBUS_APU_MON_EMIX2_SOUTR);

        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR0);
        e.fir.suml = XTRACTN(reg, EMUBUS_APU_MON_EFIR0_SUML);
        e.fir.sp = XTRACTN(reg, EMUBUS_APU_MON_EFIR0_SP);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR1);
        e.fir.sumr = XTRACTN(reg, EMUBUS_APU_MON_EFIR1_SUMR);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR2);
        e.fir.sl[0] = XTRACTN(reg, EMUBUS_APU_MON_EFIR2_SL0);
        e.fir.sl[1] = XTRACTN(reg, EMUBUS_APU_MON_EFIR2_SL1);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR3);
        e.fir.sl[2] = XTRACTN(reg, EMUBUS_APU_MON_EFIR3_SL2);
        e.fir.sl[3] = XTRACTN(reg, EMUBUS_APU_MON_EFIR3_SL3);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR4);
        e.fir.sl[4] = XTRACTN(reg, EMUBUS_APU_MON_EFIR4_SL4);
        e.fir.sl[5] = XTRACTN(reg, EMUBUS_APU_MON_EFIR4_SL5);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR5);
        e.fir.sl[6] = XTRACTN(reg, EMUBUS_APU_MON_EFIR5_SL6);
        e.fir.sl[7] = XTRACTN(reg, EMUBUS_APU_MON_EFIR5_SL7);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR6);
        e.fir.sr[0] = XTRACTN(reg, EMUBUS_APU_MON_EFIR6_SR0);
        e.fir.sr[1] = XTRACTN(reg, EMUBUS_APU_MON_EFIR6_SR1);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR7);
        e.fir.sr[2] = XTRACTN(reg, EMUBUS_APU_MON_EFIR7_SR2);
        e.fir.sr[3] = XTRACTN(reg, EMUBUS_APU_MON_EFIR7_SR3);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR8);
        e.fir.sr[4] = XTRACTN(reg, EMUBUS_APU_MON_EFIR8_SR4);
        e.fir.sr[5] = XTRACTN(reg, EMUBUS_APU_MON_EFIR8_SR5);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR9);
        e.fir.sr[6] = XTRACTN(reg, EMUBUS_APU_MON_EFIR9_SR6);
        e.fir.sr[7] = XTRACTN(reg, EMUBUS_APU_MON_EFIR9_SR7);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR10);
        e.fir.soutl = XTRACTN(reg, EMUBUS_APU_MON_EFIR10_SOUTL);
        e.fir.soutr = XTRACTN(reg, EMUBUS_APU_MON_EFIR10_SOUTR);

        reg = read_reg(EMUBUS_APU_MON_SEL_OMIX0);
        sl.out_mixer.suml = XTRACTN(reg, EMUBUS_APU_MON_OMIX0_SUML);
        reg = read_reg(EMUBUS_APU_MON_SEL_OMIX1);
        sl.out_mixer.sumr = XTRACTN(reg, EMUBUS_APU_MON_OMIX1_SUMR);
        reg = read_reg(EMUBUS_APU_MON_SEL_OMIX2);
        sl.out_mixer.soutl = XTRACTN(reg, EMUBUS_APU_MON_OMIX2_SOUTL);
        sl.out_mixer.soutr = XTRACTN(reg, EMUBUS_APU_MON_OMIX2_SOUTR);
    }

    return ret;
}

#undef XTRACT1
#undef XTRACTN

#define INSERT1(reg, name, field)   \
    reg = (reg & ~name) | ((field) ? name : 0)
#define INSERTN(reg, name, field)   \
    reg = (reg & ~name) | ((static_cast<decltype(reg)>(field) << name##_S) & name)

void apu::write_smp_state(const apu_smp_state& sst)
{
    auto arc0 = read_reg(EMUBUS_APU_MON_SEL_ARC0);
    auto arc1 = read_reg(EMUBUS_APU_MON_SEL_ARC1);
    auto uar0 = read_reg(EMUBUS_APU_MON_SEL_UAR0);
    auto uar1 = read_reg(EMUBUS_APU_MON_SEL_UAR1);
    auto stcs = read_reg(EMUBUS_APU_MON_SEL_STCS);
    auto sps  = read_reg(EMUBUS_APU_MON_SEL_SPS);
    auto cpui = read_reg(EMUBUS_APU_MON_SEL_CPUI);
    auto cpuo = read_reg(EMUBUS_APU_MON_SEL_CPUO);
    auto spr0 = read_reg(EMUBUS_APU_MON_SEL_SPR0);
    auto spr1 = read_reg(EMUBUS_APU_MON_SEL_SPR1);
    auto int0 = read_reg(EMUBUS_APU_MON_SEL_INT0);
    auto int1 = read_reg(EMUBUS_APU_MON_SEL_INT1);
    auto int2 = read_reg(EMUBUS_APU_MON_SEL_INT2);
    auto int3 = read_reg(EMUBUS_APU_MON_SEL_INT3);
    auto int4 = read_reg(EMUBUS_APU_MON_SEL_INT4);
    auto int5 = read_reg(EMUBUS_APU_MON_SEL_INT5);
    auto int6 = read_reg(EMUBUS_APU_MON_SEL_INT6);
    auto int7 = read_reg(EMUBUS_APU_MON_SEL_INT7);

    INSERTN(arc0, EMUBUS_APU_MON_ARC0_PC, sst.pc);
    INSERT1(arc0, EMUBUS_APU_MON_ARC0_PF_C, sst.pf.c);
    INSERT1(arc0, EMUBUS_APU_MON_ARC0_PF_Z, sst.pf.z);
    INSERT1(arc0, EMUBUS_APU_MON_ARC0_PF_I, sst.pf.i);
    INSERT1(arc0, EMUBUS_APU_MON_ARC0_PF_H, sst.pf.h);
    INSERT1(arc0, EMUBUS_APU_MON_ARC0_PF_B, sst.pf.b);
    INSERT1(arc0, EMUBUS_APU_MON_ARC0_PF_P, sst.pf.p);
    INSERT1(arc0, EMUBUS_APU_MON_ARC0_PF_V, sst.pf.v);
    INSERT1(arc0, EMUBUS_APU_MON_ARC0_PF_N, sst.pf.n);
    INSERTN(arc1, EMUBUS_APU_MON_ARC1_S, sst.s);
    INSERTN(arc1, EMUBUS_APU_MON_ARC1_AC, sst.ac);
    INSERTN(arc1, EMUBUS_APU_MON_ARC1_X, sst.x);
    INSERTN(arc1, EMUBUS_APU_MON_ARC1_Y, sst.y);

    INSERTN(uar0, EMUBUS_APU_MON_UAR0_IR, sst.ir);
    INSERTN(uar0, EMUBUS_APU_MON_UAR0_AOR, sst.aor);
    INSERTN(uar1, EMUBUS_APU_MON_UAR0_DOR, sst.dor);
    INSERTN(uar1, EMUBUS_APU_MON_UAR1_Z, sst.z);

    INSERT1(sps, EMUBUS_APU_MON_SPS_SYNC, sst.pstate.sync);

    INSERTN(cpui, EMUBUS_APU_MON_CPUI_CPUI0, sst.periph.cpui[0]);
    INSERTN(cpui, EMUBUS_APU_MON_CPUI_CPUI1, sst.periph.cpui[1]);
    INSERTN(cpui, EMUBUS_APU_MON_CPUI_CPUI2, sst.periph.cpui[2]);
    INSERTN(cpui, EMUBUS_APU_MON_CPUI_CPUI3, sst.periph.cpui[3]);
    INSERTN(cpuo, EMUBUS_APU_MON_CPUO_CPUO0, sst.periph.cpuo[0]);
    INSERTN(cpuo, EMUBUS_APU_MON_CPUO_CPUO1, sst.periph.cpuo[1]);
    INSERTN(cpuo, EMUBUS_APU_MON_CPUO_CPUO2, sst.periph.cpuo[2]);
    INSERTN(cpuo, EMUBUS_APU_MON_CPUO_CPUO3, sst.periph.cpuo[3]);
    INSERTN(spr0, EMUBUS_APU_MON_SPR0_SPCR, sst.periph.spcr);
    INSERTN(spr0, EMUBUS_APU_MON_SPR0_SPT0, sst.periph.spt[0]);
    INSERTN(spr0, EMUBUS_APU_MON_SPR0_SPT1, sst.periph.spt[1]);
    INSERTN(spr0, EMUBUS_APU_MON_SPR0_SPT2, sst.periph.spt[2]);

    INSERT1(int0, EMUBUS_APU_MON_INT0_CKEN_D, sst.internal.cken_d);
    INSERT1(int0, EMUBUS_APU_MON_INT0_CP1D, sst.internal.cp1d);
    INSERT1(int0, EMUBUS_APU_MON_INT0_RESP, sst.internal.resp);
    INSERT1(int0, EMUBUS_APU_MON_INT0_AM_DONE_D, sst.internal.am_done_d);
    INSERT1(int0, EMUBUS_APU_MON_INT0_CCO, sst.internal.cco);
    INSERT1(int0, EMUBUS_APU_MON_INT0_CVO, sst.internal.cvo);
    INSERT1(int0, EMUBUS_APU_MON_INT0_CHO, sst.internal.cho);
    INSERT1(int0, EMUBUS_APU_MON_INT0_CL_STORE_DOR_D, sst.internal.cl_store_dor_d);
    INSERT1(int0, EMUBUS_APU_MON_INT0_CL_INT_OP_D, sst.internal.cl_int_op_d);
    INSERTN(int1, EMUBUS_APU_MON_INT1_T, sst.internal.t);
    INSERTN(int1, EMUBUS_APU_MON_INT1_BRA_T, sst.internal.bra_t);
    INSERTN(int1, EMUBUS_APU_MON_INT1_DIVCNT, sst.internal.divcnt);
    INSERTN(int2, EMUBUS_APU_MON_INT2_AM_T, sst.internal.am_t);
    INSERTN(int2, EMUBUS_APU_MON_INT2_DL, sst.internal.dl);
    INSERTN(int3, EMUBUS_APU_MON_INT3_OP_T, sst.internal.op_t);
    INSERTN(int4, EMUBUS_APU_MON_INT4_ICO, sst.internal.ico);
    INSERTN(int4, EMUBUS_APU_MON_INT4_ICOH, sst.internal.icoh);
    INSERTN(int4, EMUBUS_APU_MON_INT4_MULSCNT, sst.internal.mulscnt);
    INSERTN(int5, EMUBUS_APU_MON_INT5_TIMER0, sst.internal.timer0);
    INSERTN(int6, EMUBUS_APU_MON_INT6_TIMER1, sst.internal.timer1);
    INSERTN(int7, EMUBUS_APU_MON_INT7_TIMER2, sst.internal.timer2);

    write_reg(EMUBUS_APU_MON_SEL_ARC0, arc0);
    write_reg(EMUBUS_APU_MON_SEL_ARC1, arc1);
    write_reg(EMUBUS_APU_MON_SEL_UAR0, uar0);
    write_reg(EMUBUS_APU_MON_SEL_UAR1, uar1);
    write_reg(EMUBUS_APU_MON_SEL_STCS, stcs);
    write_reg(EMUBUS_APU_MON_SEL_SPS, sps);
    write_reg(EMUBUS_APU_MON_SEL_CPUI, cpui);
    write_reg(EMUBUS_APU_MON_SEL_CPUO, cpuo);
    write_reg(EMUBUS_APU_MON_SEL_SPR0, spr0);
    write_reg(EMUBUS_APU_MON_SEL_SPR1, spr1);
    write_reg(EMUBUS_APU_MON_SEL_INT0, int0);
    write_reg(EMUBUS_APU_MON_SEL_INT1, int1);
    write_reg(EMUBUS_APU_MON_SEL_INT2, int2);
    write_reg(EMUBUS_APU_MON_SEL_INT3, int3);
    write_reg(EMUBUS_APU_MON_SEL_INT4, int4);
    write_reg(EMUBUS_APU_MON_SEL_INT5, int5);
    write_reg(EMUBUS_APU_MON_SEL_INT6, int6);
    write_reg(EMUBUS_APU_MON_SEL_INT7, int7);
}

void apu::write_dsp_state(const apu_dsp_state& sst, int which)
{
    if (which & dsp_state_regs) {
        for (int i = 0; i < 0x80; i++) {
            write_reg(EMUBUS_APU_MON_SEL_DRS, i);
            write_reg(EMUBUS_APU_MON_SEL_DRD, sst.regs.regs[i]);
        }
    }

    if (which & dsp_state_internal) {
        uint32_t reg;
        auto& i = sst.internal;

        reg = read_reg(EMUBUS_APU_MON_SEL_REGFILE);
        INSERT1(reg, EMUBUS_APU_MON_REGFILE_KON_WRITTEN, i.reg_file.kon_written);
        INSERTN(reg, EMUBUS_APU_MON_REGFILE_FLG_QUICK, i.reg_file.flg_quick);
        write_reg(EMUBUS_APU_MON_SEL_REGFILE, reg);

        reg = read_reg(EMUBUS_APU_MON_SEL_CLKGEN);
        INSERTN(reg, EMUBUS_APU_MON_CLKGEN_IDIVCNT, i.clkgen.idivcnt);
        INSERTN(reg, EMUBUS_APU_MON_CLKGEN_CCCNT, i.clkgen.cccnt);
        INSERT1(reg, EMUBUS_APU_MON_CLKGEN_NRES, i.clkgen.nres);
        write_reg(EMUBUS_APU_MON_SEL_CLKGEN, reg);

        reg = read_reg(EMUBUS_APU_MON_SEL_SMP0);
        INSERTN(reg, EMUBUS_APU_MON_SMP0_D_O_D, i.smp_if.d_o_d);
        INSERT1(reg, EMUBUS_APU_MON_SMP0_CCKEN_D, i.smp_if.ccken_d);
        write_reg(EMUBUS_APU_MON_SEL_SMP0, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_SMP1);
        INSERTN(reg, EMUBUS_APU_MON_SMP1_SPDRS, i.smp_if.spdrs);
        write_reg(EMUBUS_APU_MON_SEL_SMP1, reg);

        reg = read_reg(EMUBUS_APU_MON_SEL_SRAM0);
        INSERTN(reg, EMUBUS_APU_MON_SRAM0_IMA, i.sram_if.ima);
        INSERTN(reg, EMUBUS_APU_MON_SRAM0_IMDI, i.sram_if.imdi);
        INSERT1(reg, EMUBUS_APU_MON_SRAM0_IMRS, i.sram_if.imrs);
        INSERT1(reg, EMUBUS_APU_MON_SRAM0_IMWS, i.sram_if.imws);
        INSERT1(reg, EMUBUS_APU_MON_SRAM0_MAD_SSMP, i.sram_if.mad_ssmp);
        write_reg(EMUBUS_APU_MON_SEL_SRAM0, reg);

        reg = read_reg(EMUBUS_APU_MON_SEL_GLB_CTR);
        INSERTN(reg, EMUBUS_APU_MON_GLB_CTR_CNT, i.glb_ctr.cnt);
        write_reg(EMUBUS_APU_MON_SEL_GLB_CTR, reg);

        auto& sl = i.sgenloop;
        reg = read_reg(EMUBUS_APU_MON_SEL_SL0);
        INSERTN(reg, EMUBUS_APU_MON_SL0_SGLCNT, sl.sglcnt);
        INSERTN(reg, EMUBUS_APU_MON_SL0_PMON, sl.pmon);
        INSERTN(reg, EMUBUS_APU_MON_SL0_NON, sl.non);
        INSERTN(reg, EMUBUS_APU_MON_SL0_DIR, sl.dir);
        write_reg(EMUBUS_APU_MON_SEL_SL0, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_SL1);
        INSERTN(reg, EMUBUS_APU_MON_SL1_KON, sl.kon);
        INSERTN(reg, EMUBUS_APU_MON_SL1_KOF, sl.kof);
        INSERTN(reg, EMUBUS_APU_MON_SL1_MVOLL, sl.mvoll);
        INSERTN(reg, EMUBUS_APU_MON_SL1_MVOLR, sl.mvolr);
        write_reg(EMUBUS_APU_MON_SEL_SL1, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_SL2);
        INSERTN(reg, EMUBUS_APU_MON_SL2_EVOLL, sl.evoll);
        INSERTN(reg, EMUBUS_APU_MON_SL2_EVOLR, sl.evolr);
        INSERTN(reg, EMUBUS_APU_MON_SL2_FLGN, sl.flgn);
        write_reg(EMUBUS_APU_MON_SEL_SL2, reg);

        reg = read_reg(EMUBUS_APU_MON_SEL_NOISE);
        INSERTN(reg, EMUBUS_APU_MON_NOISE_LFSR, sl.noise_lfsr);
        write_reg(EMUBUS_APU_MON_SEL_NOISE, reg);

        auto& v = sl.voice;
        reg = read_reg(EMUBUS_APU_MON_SEL_VOICE0);
        INSERTN(reg, EMUBUS_APU_MON_VOICE0_SRCN, v.srcn);
        INSERTN(reg, EMUBUS_APU_MON_VOICE0_SRCND, v.srcnd);
        INSERTN(reg, EMUBUS_APU_MON_VOICE0_PITCH, v.pitch);
        write_reg(EMUBUS_APU_MON_SEL_VOICE0, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_VOICE1);
        INSERTN(reg, EMUBUS_APU_MON_VOICE1_ADSR1, v.adsr1);
        INSERTN(reg, EMUBUS_APU_MON_VOICE1_ADSR2_GAIN, v.adsr2_gain);
        INSERTN(reg, EMUBUS_APU_MON_VOICE1_VOLL, v.voll);
        INSERTN(reg, EMUBUS_APU_MON_VOICE1_VOLR, v.volr);
        write_reg(EMUBUS_APU_MON_SEL_VOICE1, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_VOICE3);
        INSERTN(reg, EMUBUS_APU_MON_VOICE3_ENVX, v.envx);
        write_reg(EMUBUS_APU_MON_SEL_VOICE3, reg);

        write_array<EMUBUS_APU_MON_SEL_BRRDEC_CTX_LEN>(EMUBUS_APU_MON_SEL_BRRDEC_CTX, v.brrdec.ctx);
        reg = read_reg(EMUBUS_APU_MON_SEL_BRRDEC0);
        INSERTN(reg, EMUBUS_APU_MON_BRRDEC0_BBCNT, v.brrdec.bbcnt);
        INSERT1(reg, EMUBUS_APU_MON_BRRDEC0_SRCN0_LOAD, v.brrdec.srcn0_load);
        INSERT1(reg, EMUBUS_APU_MON_BRRDEC0_SRCN1_LOAD, v.brrdec.srcn1_load);
        INSERT1(reg, EMUBUS_APU_MON_BRRDEC0_SRCN_INC, v.brrdec.srcn_inc);
        INSERT1(reg, EMUBUS_APU_MON_BRRDEC0_RD_DTA1, v.brrdec.rd_dta1);
        INSERTN(reg, EMUBUS_APU_MON_BRRDEC0_HDR, v.brrdec.hdr);
        INSERTN(reg, EMUBUS_APU_MON_BRRDEC0_DAT, v.brrdec.dat);
        INSERT1(reg, EMUBUS_APU_MON_BRRDEC0_RD_BRRFSM1, v.brrdec.rd_brrfsm1);
        INSERT1(reg, EMUBUS_APU_MON_BRRDEC0_BRRFSM1, v.brrdec.brrfsm1);
        INSERT1(reg, EMUBUS_APU_MON_BRRDEC0_BRRFSM2, v.brrdec.brrfsm2);
        INSERT1(reg, EMUBUS_APU_MON_BRRDEC0_DEC_BUF_FULL, v.brrdec.dec_buf_full);
        write_reg(EMUBUS_APU_MON_SEL_BRRDEC0, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_BRRDEC1);
        INSERTN(reg, EMUBUS_APU_MON_BRRDEC1_SRCNC, v.brrdec.srcnc);
        write_reg(EMUBUS_APU_MON_SEL_BRRDEC1, reg);
        
        reg = read_reg(EMUBUS_APU_MON_SEL_DECBUF0);
        INSERT1(reg, EMUBUS_APU_MON_DECBUF0_PLAY, v.decbuf.play);
        INSERT1(reg, EMUBUS_APU_MON_DECBUF0_OUT_SAMP, v.decbuf.out_samp);
        write_reg(EMUBUS_APU_MON_SEL_DECBUF0, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_DECBUF1);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT0, v.decbuf.deccnt[0]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT1, v.decbuf.deccnt[1]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT2, v.decbuf.deccnt[2]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT3, v.decbuf.deccnt[3]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT4, v.decbuf.deccnt[4]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT5, v.decbuf.deccnt[5]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT6, v.decbuf.deccnt[6]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF1_DECCNT7, v.decbuf.deccnt[7]);
        write_reg(EMUBUS_APU_MON_SEL_DECBUF1, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_DECBUF2);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT0, v.decbuf.intcnt[0]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT1, v.decbuf.intcnt[1]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT2, v.decbuf.intcnt[2]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT3, v.decbuf.intcnt[3]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT4, v.decbuf.intcnt[4]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT5, v.decbuf.intcnt[5]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT6, v.decbuf.intcnt[6]);
        INSERTN(reg, EMUBUS_APU_MON_DECBUF2_INTCNT7, v.decbuf.intcnt[7]);
        write_reg(EMUBUS_APU_MON_SEL_DECBUF2, reg);
        write_array<EMUBUS_APU_MON_SEL_DECBUF_MEM_LEN>(EMUBUS_APU_MON_SEL_DECBUF_MEM, v.decbuf.mem);

        write_array<EMUBUS_APU_MON_SEL_INTERP_CTX_LEN>(EMUBUS_APU_MON_SEL_INTERP_CTX, v.interp.ctx);
        reg = read_reg(EMUBUS_APU_MON_SEL_INTERP0);
        INSERTN(reg, EMUBUS_APU_MON_INTERP0_PCNT, v.interp.pcnt);
        INSERTN(reg, EMUBUS_APU_MON_INTERP0_PSTEP, v.interp.pstep);
        write_reg(EMUBUS_APU_MON_SEL_INTERP0, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_INTERP1);
        INSERTN(reg, EMUBUS_APU_MON_INTERP1_ACC, v.interp.acc);
        INSERTN(reg, EMUBUS_APU_MON_INTERP1_GOS, v.interp.gos);
        write_reg(EMUBUS_APU_MON_SEL_INTERP1, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_INTERP2);
        INSERTN(reg, EMUBUS_APU_MON_INTERP2_SOUT, v.interp.sout);
        write_reg(EMUBUS_APU_MON_SEL_INTERP2, reg);

        write_array<EMUBUS_APU_MON_SEL_ENV_CTX_LEN>(EMUBUS_APU_MON_SEL_ENV_CTX, v.env.ctx);
        reg = read_reg(EMUBUS_APU_MON_SEL_ENV0);
        INSERTN(reg, EMUBUS_APU_MON_ENV0_ENVELOPE, v.env.envelope);
        INSERTN(reg, EMUBUS_APU_MON_ENV0_ADSR_ST, v.env.adsr_st);
        INSERT1(reg, EMUBUS_APU_MON_ENV0_CTX_SWAP, v.env.ctx_swap);
        write_reg(EMUBUS_APU_MON_SEL_ENV0, reg);
        
        reg = read_reg(EMUBUS_APU_MON_SEL_SP0);
        INSERT1(reg, EMUBUS_APU_MON_SP0_VML, v.scale_pan.vml);
        INSERT1(reg, EMUBUS_APU_MON_SP0_VMR, v.scale_pan.vmr);
        INSERTN(reg, EMUBUS_APU_MON_SP0_OUT, v.scale_pan.out);
        write_reg(EMUBUS_APU_MON_SEL_SP0, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_SP1);
        INSERTN(reg, EMUBUS_APU_MON_SP1_OUTL, v.scale_pan.outl);
        INSERTN(reg, EMUBUS_APU_MON_SP1_OUTR, v.scale_pan.outr);
        write_reg(EMUBUS_APU_MON_SEL_SP1, reg);

        auto& e = sl.echo;
        reg = read_reg(EMUBUS_APU_MON_SEL_ECHO0);
        INSERTN(reg, EMUBUS_APU_MON_ECHO0_EFB, e.efb);
        INSERTN(reg, EMUBUS_APU_MON_ECHO0_EON, e.eon);
        INSERTN(reg, EMUBUS_APU_MON_ECHO0_EDL, e.edl);
        INSERTN(reg, EMUBUS_APU_MON_ECHO0_ESA, e.esa);
        write_reg(EMUBUS_APU_MON_SEL_ECHO0, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_ECHO1);
        INSERTN(reg, EMUBUS_APU_MON_ECHO1_FCRS, e.fcrs);
        INSERTN(reg, EMUBUS_APU_MON_ECHO1_FCLS, e.fcls);
        INSERT1(reg, EMUBUS_APU_MON_ECHO1_FCL, e.fcl);
        INSERTN(reg, EMUBUS_APU_MON_ECHO1_IDX, e.idx);
        INSERT1(reg, EMUBUS_APU_MON_ECHO1_ECE, e.ece);
        write_reg(EMUBUS_APU_MON_SEL_ECHO1, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_ECHO2);
        INSERTN(reg, EMUBUS_APU_MON_ECHO2_IDX_MAX, e.idx_max);
        INSERTN(reg, EMUBUS_APU_MON_ECHO2_BUF_PTR, e.buf_ptr);
        write_reg(EMUBUS_APU_MON_SEL_ECHO2, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_ECHO3);
        INSERTN(reg, EMUBUS_APU_MON_ECHO3_EBOUTL, e.eboutl);
        INSERTN(reg, EMUBUS_APU_MON_ECHO3_EBOUTR, e.eboutr);
        write_reg(EMUBUS_APU_MON_SEL_ECHO3, reg);

        reg = read_reg(EMUBUS_APU_MON_SEL_EMIX0);
        INSERTN(reg, EMUBUS_APU_MON_EMIX0_SUML, e.mixer.suml);
        INSERTN(reg, EMUBUS_APU_MON_EMIX0_VSEL, e.mixer.vsel);
        write_reg(EMUBUS_APU_MON_SEL_EMIX0, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_EMIX1);
        INSERTN(reg, EMUBUS_APU_MON_EMIX1_SUMR, e.mixer.sumr);
        write_reg(EMUBUS_APU_MON_SEL_EMIX1, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_EMIX2);
        INSERTN(reg, EMUBUS_APU_MON_EMIX2_SOUTL, e.mixer.soutl);
        INSERTN(reg, EMUBUS_APU_MON_EMIX2_SOUTR, e.mixer.soutr);
        write_reg(EMUBUS_APU_MON_SEL_EMIX2, reg);

        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR0);
        INSERTN(reg, EMUBUS_APU_MON_EFIR0_SUML, e.fir.suml);
        INSERTN(reg, EMUBUS_APU_MON_EFIR0_SP, e.fir.sp);
        write_reg(EMUBUS_APU_MON_SEL_EFIR0, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR1);
        INSERTN(reg, EMUBUS_APU_MON_EFIR1_SUMR, e.fir.sumr);
        write_reg(EMUBUS_APU_MON_SEL_EFIR1, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR2);
        INSERTN(reg, EMUBUS_APU_MON_EFIR2_SL0, e.fir.sl[0]);
        INSERTN(reg, EMUBUS_APU_MON_EFIR2_SL1, e.fir.sl[1]);
        write_reg(EMUBUS_APU_MON_SEL_EFIR2, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR3);
        INSERTN(reg, EMUBUS_APU_MON_EFIR3_SL2, e.fir.sl[2]);
        INSERTN(reg, EMUBUS_APU_MON_EFIR3_SL3, e.fir.sl[3]);
        write_reg(EMUBUS_APU_MON_SEL_EFIR3, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR4);
        INSERTN(reg, EMUBUS_APU_MON_EFIR4_SL4, e.fir.sl[4]);
        INSERTN(reg, EMUBUS_APU_MON_EFIR4_SL5, e.fir.sl[5]);
        write_reg(EMUBUS_APU_MON_SEL_EFIR4, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR5);
        INSERTN(reg, EMUBUS_APU_MON_EFIR5_SL6, e.fir.sl[6]);
        INSERTN(reg, EMUBUS_APU_MON_EFIR5_SL7, e.fir.sl[7]);
        write_reg(EMUBUS_APU_MON_SEL_EFIR5, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR6);
        INSERTN(reg, EMUBUS_APU_MON_EFIR6_SR0, e.fir.sr[0]);
        INSERTN(reg, EMUBUS_APU_MON_EFIR6_SR1, e.fir.sr[1]);
        write_reg(EMUBUS_APU_MON_SEL_EFIR6, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR7);
        INSERTN(reg, EMUBUS_APU_MON_EFIR7_SR2, e.fir.sr[2]);
        INSERTN(reg, EMUBUS_APU_MON_EFIR7_SR3, e.fir.sr[3]);
        write_reg(EMUBUS_APU_MON_SEL_EFIR7, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR8);
        INSERTN(reg, EMUBUS_APU_MON_EFIR8_SR4, e.fir.sr[4]);
        INSERTN(reg, EMUBUS_APU_MON_EFIR8_SR5, e.fir.sr[5]);
        write_reg(EMUBUS_APU_MON_SEL_EFIR8, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR9);
        INSERTN(reg, EMUBUS_APU_MON_EFIR9_SR6, e.fir.sr[6]);
        INSERTN(reg, EMUBUS_APU_MON_EFIR9_SR7, e.fir.sr[7]);
        write_reg(EMUBUS_APU_MON_SEL_EFIR9, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_EFIR10);
        INSERTN(reg, EMUBUS_APU_MON_EFIR10_SOUTL, e.fir.soutl);
        INSERTN(reg, EMUBUS_APU_MON_EFIR10_SOUTR, e.fir.soutr);
        write_reg(EMUBUS_APU_MON_SEL_EFIR10, reg);

        reg = read_reg(EMUBUS_APU_MON_SEL_OMIX0);
        INSERTN(reg, EMUBUS_APU_MON_OMIX0_SUML, sl.out_mixer.suml);
        write_reg(EMUBUS_APU_MON_SEL_OMIX0, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_OMIX1);
        INSERTN(reg, EMUBUS_APU_MON_OMIX1_SUMR, sl.out_mixer.sumr);
        write_reg(EMUBUS_APU_MON_SEL_OMIX1, reg);
        reg = read_reg(EMUBUS_APU_MON_SEL_OMIX2);
        INSERTN(reg, EMUBUS_APU_MON_OMIX2_SOUTL, sl.out_mixer.soutl);
        INSERTN(reg, EMUBUS_APU_MON_OMIX2_SOUTR, sl.out_mixer.soutr);
        write_reg(EMUBUS_APU_MON_SEL_OMIX2, reg);
    }
}

#undef INSERT1
#undef INSERTN

void apu::read_state(state_t& st)
{
    st.smp = read_smp_state();
    st.dsp = read_dsp_state();

    membuf<memptr> ram_buf(memptr(0x0000, memtype::apu),
                           memptr(0xFFFF, memtype::apu));
    ram_buf.fetch();
    st.ram = unique_buf(ram_buf.take_buffer());
}

void apu::write_state(const state_t& st)
{
    write_smp_state(st.smp);
    write_dsp_state(st.dsp);

    membuf<memptr> ram_buf(memptr(0x0000, memtype::apu),
                           memptr(0xFFFF, memtype::apu),
                           st.ram);
    ram_buf.store();
}

} // namespace snes
