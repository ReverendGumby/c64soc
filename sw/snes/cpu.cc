#include "emubus.h"

#include "cpu.h"

namespace snes {

void cpu::init()
{
    icd.init();
}

uint32_t cpu::read_reg(uint32_t sel)
{
    EMUBUS_CPU_MON_SEL = sel;
    uint32_t val = EMUBUS_CPU_MON_DATA;
    return val;
}

void cpu::write_reg(uint32_t sel, uint32_t val)
{
    EMUBUS_CPU_MON_SEL = sel;
    EMUBUS_CPU_MON_DATA = val;
}

#define XTRACT1(reg, name)      (((reg) & name) != 0)
#define XTRACTN(reg, name)      (((reg) & name) >> name##_S)

cpu_chip_state cpu::read_chip_state(int which)
{
    cpu_chip_state ret;
    uint32_t reg;

    if (which & state_io_regs) {
        auto& io = ret.io_regs;

        reg = read_reg(EMUBUS_CPU_MON_SEL_JOY0);
        io.joyout = XTRACTN(reg, EMUBUS_CPU_MON_JOY0_JOYOUT);
        io.joyser0 = XTRACTN(reg, EMUBUS_CPU_MON_JOY0_JOYSER0);
        io.joyser1 = XTRACTN(reg, EMUBUS_CPU_MON_JOY0_JOYSER1);

        reg = read_reg(EMUBUS_CPU_MON_SEL_AJPR0);
        io.hvbjoy.ajpr_busy = XTRACT1(reg, EMUBUS_CPU_MON_AJPR0_AJPR_BUSY);

        reg = read_reg(EMUBUS_CPU_MON_SEL_AJPR1);
        io.joy1 = XTRACTN(reg, EMUBUS_CPU_MON_AJPR1_JOY1);
        io.joy2 = XTRACTN(reg, EMUBUS_CPU_MON_AJPR1_JOY2);

        reg = read_reg(EMUBUS_CPU_MON_SEL_AJPR2);
        io.joy3 = XTRACTN(reg, EMUBUS_CPU_MON_AJPR2_JOY3);
        io.joy4 = XTRACTN(reg, EMUBUS_CPU_MON_AJPR2_JOY4);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN0);
        io.rdnmi.cpu_ver = XTRACTN(reg, EMUBUS_CPU_MON_GEN0_CPU_VER);
        io.rdnmi.flag = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_VBL_BUF);
        io.timeup.flag = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_HVT_BUF);
        io.hvbjoy.hblank = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_HBLANK);
        io.hvbjoy.vblank = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_VBLANK);
        io.nmitimen.ajpr_en = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_AJPR_EN);
        io.nmitimen.hvt_mode = XTRACTN(reg, EMUBUS_CPU_MON_GEN0_HVT_MODE);
        io.nmitimen.vbl_en = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_VBL_EN);
        io.memsel.memsel = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_MEMSEL);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN1);
        io.hdmaen = XTRACTN(reg, EMUBUS_CPU_MON_GEN1_DMA_HEN);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN2);
        io.rdio = XTRACTN(reg, EMUBUS_CPU_MON_GEN2_JPI);
        io.wrio = XTRACTN(reg, EMUBUS_CPU_MON_GEN2_JPO);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN3);
        io.htime = XTRACTN(reg, EMUBUS_CPU_MON_GEN3_HTIME);
        io.vtime = XTRACTN(reg, EMUBUS_CPU_MON_GEN3_VTIME);

        reg = read_reg(EMUBUS_CPU_MON_SEL_MULDIV0);
        io.wrmpya = XTRACTN(reg, EMUBUS_CPU_MON_MULDIV0_MPYA);
        io.wrdiva = XTRACTN(reg, EMUBUS_CPU_MON_MULDIV0_NUM);

        reg = read_reg(EMUBUS_CPU_MON_SEL_MULDIV2);
        io.rdmpy = XTRACTN(reg, EMUBUS_CPU_MON_MULDIV2_MPY);
        io.rddiv = XTRACTN(reg, EMUBUS_CPU_MON_MULDIV2_DIV);

        reg = read_reg(EMUBUS_CPU_MON_SEL_DMAC_INT0);
        io.mdmaen = XTRACTN(reg, EMUBUS_CPU_MON_DMAC_INT0_MEN);
    }

    if (which & state_dma_regs) {
        const auto ch_span = EMUBUS_CPU_MON_SEL_DMAC_CH1_REG0123
            - EMUBUS_CPU_MON_SEL_DMAC_CH0_REG0123;
        for (int i = 0; i < 8; i++) {
            auto& ch = ret.dma_regs.ch[i];

            reg = read_reg(EMUBUS_CPU_MON_SEL_DMAC_CH0_REG0123 + i * ch_span);
            ch.dmapx = reg >> (0 * 8);
            ch.bbadx = reg >> (1 * 8);
            ch.a1txl = reg >> (2 * 8);
            ch.a1txh = reg >> (3 * 8);

            reg = read_reg(EMUBUS_CPU_MON_SEL_DMAC_CH0_REG4567 + i * ch_span);
            ch.a1xb  = reg >> (0 * 8);
            ch.dasxl = reg >> (1 * 8);
            ch.dasxh = reg >> (2 * 8);
            ch.dasbx = reg >> (3 * 8);

            reg = read_reg(EMUBUS_CPU_MON_SEL_DMAC_CH0_REG89AB + i * ch_span);
            ch.a2axl = reg >> (0 * 8);
            ch.a2axh = reg >> (1 * 8);
            ch.ntrlx = reg >> (2 * 8);
            ch.unusedx = reg >> (3 * 8);
        }
    }

    if (which & state_dma_state) {
        auto& st = ret.dma_state;

        reg = read_reg(EMUBUS_CPU_MON_SEL_DMAC_ST0);
        st.active = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_ST0_ACTIVE);
        st.gpdma = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_ST0_GPDMA);
        st.hdma = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_ST0_HDMA);
        st.xfer = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_ST0_XFER);
        st.ch_active = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_ST0_CH_ACTIVE);
        st.done = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_ST0_DONE);
        st.hcdis = XTRACTN(reg, EMUBUS_CPU_MON_DMAC_ST0_HCDIS);
        st.hxfer = XTRACTN(reg, EMUBUS_CPU_MON_DMAC_ST0_HXFER);

        reg = read_reg(EMUBUS_CPU_MON_SEL_DMAC_BL0);
        st.a_o = XTRACTN(reg, EMUBUS_CPU_MON_DMAC_BL0_A_O);
        st.pa_o = XTRACTN(reg, EMUBUS_CPU_MON_DMAC_BL0_PA_O);

        reg = read_reg(EMUBUS_CPU_MON_SEL_DMAC_BL1);
        st.a_oe = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_BL1_A_OE);
        st.pa_oe = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_BL1_PA_OE);
        st.nwr_o = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_BL1_NWR_O);
        st.nrd_o = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_BL1_NRD_O);
        st.npawr_o = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_BL1_NPAWR_O);
        st.npard_o = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_BL1_NPARD_O);
    }

    if (which & state_pins) {
        auto& pins = ret.pins;

        reg = read_reg(EMUBUS_CPU_MON_SEL_JOY0);
        pins.jpout = XTRACTN(reg, EMUBUS_CPU_MON_JOY0_JPOUT);
        pins.jpclk = XTRACTN(reg, EMUBUS_CPU_MON_JOY0_JPCLK);
        pins.jp1d = XTRACTN(reg, EMUBUS_CPU_MON_JOY0_JP1D);
        pins.jp2d = XTRACTN(reg, EMUBUS_CPU_MON_JOY0_JP2D);
    }

    if (which & state_internal) {
        auto& i = ret.internal;

        reg = read_reg(EMUBUS_CPU_MON_SEL_CLKGEN);
        i.clkgen.cycle_ccnt = XTRACTN(reg, EMUBUS_CPU_MON_CLKGEN_CYCLE_CCNT);
        i.clkgen.cp2_clkcnt = XTRACTN(reg, EMUBUS_CPU_MON_CLKGEN_CP2_CLKCNT);
        i.clkgen.slow_ccnt = XTRACTN(reg, EMUBUS_CPU_MON_CLKGEN_SLOW_CCNT);
        i.clkgen.cpu_gate_dly = XTRACT1(reg, EMUBUS_CPU_MON_CLKGEN_CPU_GATE_DLY);
        i.clkgen.cpu_act = XTRACT1(reg, EMUBUS_CPU_MON_CLKGEN_CPU_ACT);
        i.clkgen.slowck_act = XTRACT1(reg, EMUBUS_CPU_MON_CLKGEN_SLOWCK_ACT);
        i.clkgen.cp2 = XTRACT1(reg, EMUBUS_CPU_MON_CLKGEN_CP2);
        i.clkgen.cpu_refresh_en = XTRACT1(reg, EMUBUS_CPU_MON_CLKGEN_CPU_REFRESH_EN);
        i.clkgen.slow_gate_dly = XTRACT1(reg, EMUBUS_CPU_MON_CLKGEN_SLOW_GATE_DLY);
        i.clkgen.slow_refresh_en = XTRACT1(reg, EMUBUS_CPU_MON_CLKGEN_SLOW_REFRESH_EN);

        reg = read_reg(EMUBUS_CPU_MON_SEL_AJPR0);
        i.ajpr.ccnt = XTRACTN(reg, EMUBUS_CPU_MON_AJPR0_CCNT);
        i.ajpr.cycle = XTRACTN(reg, EMUBUS_CPU_MON_AJPR0_CYCLE);
        i.ajpr.vblank_d = XTRACT1(reg, EMUBUS_CPU_MON_AJPR0_VBLANK_D);
        i.ajpr.jpclk = XTRACTN(reg, EMUBUS_CPU_MON_AJPR0_JPCLK);
        i.ajpr.jpout = XTRACT1(reg, EMUBUS_CPU_MON_AJPR0_JPOUT);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN0);
        i.gen.vblank_d = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_VBLANK_D);
        i.gen.vbl_set = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_VBL_SET);
        i.gen.vbl_clear_flags = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_VBL_CLEAR_FLAGS);
        i.gen.vbl_flag = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_VBL_FLAG);
        i.gen.vbl_flag_d = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_VBL_FLAG_D);
        i.gen.vbl_start = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_VBL_START);
        i.gen.vbl_end = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_VBL_END);
        i.gen.hblank_d = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_HBLANK_D);
        i.gen.hbl_start = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_HBL_START);
        i.gen.hbl_end = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_HBL_END);
        i.gen.hvt_set = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_HVT_SET);
        i.gen.hvt_clear_flags = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_HVT_CLEAR_FLAGS);
        i.gen.hvt_flag = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_HVT_FLAG);
        i.gen.hvt_flag_d = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_HVT_FLAG_D);
        i.gen.cc_d = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_CC_D);
        i.gen.refresh_en = XTRACT1(reg, EMUBUS_CPU_MON_GEN0_REFRESH_EN);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN1);
        i.gen.dma_mset = XTRACTN(reg, EMUBUS_CPU_MON_GEN1_DMA_MSET);
        i.gen.hcnt = XTRACTN(reg, EMUBUS_CPU_MON_GEN1_HCNT);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN2);
        i.gen.vcnt = XTRACTN(reg, EMUBUS_CPU_MON_GEN2_VCNT);

        reg = read_reg(EMUBUS_CPU_MON_SEL_MULDIV1);
        i.muldiv.shifter = XTRACTN(reg, EMUBUS_CPU_MON_MULDIV1_SHIFTER);

        reg = read_reg(EMUBUS_CPU_MON_SEL_MULDIV3);
        i.muldiv.st = XTRACTN(reg, EMUBUS_CPU_MON_MULDIV3_ST);
        i.muldiv.muldivnot = XTRACT1(reg, EMUBUS_CPU_MON_MULDIV3_MULNOTDIV);

        reg = read_reg(EMUBUS_CPU_MON_SEL_DMAC_INT0);
        i.dmac.ntrlz = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_INT0_NTRLZ);
        i.dmac.post_vbl_end = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_INT0_POST_VBL_END);
        i.dmac.hstartl = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_INT0_HSTARTL);
        i.dmac.hstart = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_INT0_HSTART);
        i.dmac.gprestart_d = XTRACT1(reg, EMUBUS_CPU_MON_DMAC_INT0_GPRESTART_D);
        i.dmac.aca = XTRACTN(reg, EMUBUS_CPU_MON_DMAC_INT0_ACA);
        i.dmac.dl = XTRACTN(reg, EMUBUS_CPU_MON_DMAC_INT0_DL);
        i.dmac.men = XTRACTN(reg, EMUBUS_CPU_MON_DMAC_INT0_MEN);
        i.dmac.hen_d = XTRACTN(reg, EMUBUS_CPU_MON_DMAC_INT0_HEN_D);

        reg = read_reg(EMUBUS_CPU_MON_SEL_DMAC_INT1);
        i.dmac.cen = XTRACTN(reg, EMUBUS_CPU_MON_DMAC_INT1_CEN);
        i.dmac.gptpos = XTRACTN(reg, EMUBUS_CPU_MON_DMAC_INT1_GPTPOS);
        i.dmac.hst = XTRACTN(reg, EMUBUS_CPU_MON_DMAC_INT1_HST);
        i.dmac.hbc = XTRACTN(reg, EMUBUS_CPU_MON_DMAC_INT1_HBC);
    }

    return ret;
}

#undef XTRACT1
#undef XTRACTN

#define INSERT1(reg, name, field)   \
    reg = (reg & ~name) | ((field) ? name : 0)
#define INSERTN(reg, name, field)   \
    reg = (reg & ~name) | ((static_cast<decltype(reg)>(field) << name##_S) & name)

void cpu::write_chip_state(const cpu_chip_state& cst, int which)
{
    // Do read-modify-write to (a) allow split writes to the same register, and
    // (b) avoid accidentally clearing newly implemented (but not yet saved)
    // state.

    uint32_t reg;

    if (which & state_io_regs) {
        const auto& io = cst.io_regs;

        reg = read_reg(EMUBUS_CPU_MON_SEL_JOY0);
        INSERTN(reg, EMUBUS_CPU_MON_JOY0_JOYOUT, io.joyout);
        INSERTN(reg, EMUBUS_CPU_MON_JOY0_JOYSER0, io.joyser0);
        INSERTN(reg, EMUBUS_CPU_MON_JOY0_JOYSER1, io.joyser1);
        write_reg(EMUBUS_CPU_MON_SEL_JOY0, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_AJPR0);
        INSERT1(reg, EMUBUS_CPU_MON_AJPR0_AJPR_BUSY, io.hvbjoy.ajpr_busy);
        write_reg(EMUBUS_CPU_MON_SEL_AJPR0, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_AJPR1);
        INSERTN(reg, EMUBUS_CPU_MON_AJPR1_JOY1, io.joy1);
        INSERTN(reg, EMUBUS_CPU_MON_AJPR1_JOY2, io.joy2);
        write_reg(EMUBUS_CPU_MON_SEL_AJPR1, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_AJPR2);
        INSERTN(reg, EMUBUS_CPU_MON_AJPR2_JOY3, io.joy3);
        INSERTN(reg, EMUBUS_CPU_MON_AJPR2_JOY4, io.joy4);
        write_reg(EMUBUS_CPU_MON_SEL_AJPR2, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN0);
        INSERTN(reg, EMUBUS_CPU_MON_GEN0_CPU_VER, io.rdnmi.cpu_ver);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_VBL_BUF, io.rdnmi.flag);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_HVT_BUF, io.timeup.flag);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_HBLANK, io.hvbjoy.hblank);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_VBLANK, io.hvbjoy.vblank);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_AJPR_EN, io.nmitimen.ajpr_en);
        INSERTN(reg, EMUBUS_CPU_MON_GEN0_HVT_MODE, io.nmitimen.hvt_mode);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_VBL_EN, io.nmitimen.vbl_en);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_MEMSEL, io.memsel.memsel);
        write_reg(EMUBUS_CPU_MON_SEL_GEN0, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN1);
        INSERTN(reg, EMUBUS_CPU_MON_GEN1_DMA_HEN, io.hdmaen);
        write_reg(EMUBUS_CPU_MON_SEL_GEN1, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN2);
        INSERTN(reg, EMUBUS_CPU_MON_GEN2_JPI, io.rdio);
        INSERTN(reg, EMUBUS_CPU_MON_GEN2_JPO, io.wrio);
        write_reg(EMUBUS_CPU_MON_SEL_GEN2, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN3);
        INSERTN(reg, EMUBUS_CPU_MON_GEN3_HTIME, io.htime);
        INSERTN(reg, EMUBUS_CPU_MON_GEN3_VTIME, io.vtime);
        write_reg(EMUBUS_CPU_MON_SEL_GEN3, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_MULDIV0);
        INSERTN(reg, EMUBUS_CPU_MON_MULDIV0_MPYA, io.wrmpya);
        INSERTN(reg, EMUBUS_CPU_MON_MULDIV0_NUM, io.wrdiva);
        write_reg(EMUBUS_CPU_MON_SEL_MULDIV0, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_MULDIV2);
        INSERTN(reg, EMUBUS_CPU_MON_MULDIV2_MPY, io.rdmpy);
        INSERTN(reg, EMUBUS_CPU_MON_MULDIV2_DIV, io.rddiv);
        write_reg(EMUBUS_CPU_MON_SEL_MULDIV2, reg);

        // io.mdmaen is DMAC_INT0_MEN, handled below.
    }

    if (which & state_dma_regs) {
        const auto ch_span = EMUBUS_CPU_MON_SEL_DMAC_CH1_REG0123
            - EMUBUS_CPU_MON_SEL_DMAC_CH0_REG0123;
        for (int i = 0; i < 8; i++) {
            const auto& ch = cst.dma_regs.ch[i];

            reg = ch.dmapx << (0 * 8)
                | ch.bbadx << (1 * 8)
                | ch.a1txl << (2 * 8)
                | ch.a1txh << (3 * 8);
            write_reg(EMUBUS_CPU_MON_SEL_DMAC_CH0_REG0123 + i * ch_span, reg);

            reg = ch.a1xb  << (0 * 8)
                | ch.dasxl << (1 * 8)
                | ch.dasxh << (2 * 8)
                | ch.dasbx << (3 * 8);
            write_reg(EMUBUS_CPU_MON_SEL_DMAC_CH0_REG4567 + i * ch_span, reg);

            reg = ch.a2axl << (0 * 8)
                | ch.a2axh << (1 * 8)
                | ch.ntrlx << (2 * 8)
                | ch.unusedx << (3 * 8);
            write_reg(EMUBUS_CPU_MON_SEL_DMAC_CH0_REG89AB + i * ch_span, reg);
        }
    }

    if (which & state_dma_state) {
        const auto& st = cst.dma_state;

        reg = 0;
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_ST0_ACTIVE, st.active);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_ST0_GPDMA, st.gpdma);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_ST0_HDMA, st.hdma);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_ST0_XFER, st.xfer);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_ST0_CH_ACTIVE, st.ch_active);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_ST0_DONE, st.done);
        INSERTN(reg, EMUBUS_CPU_MON_DMAC_ST0_HCDIS, st.hcdis);
        INSERTN(reg, EMUBUS_CPU_MON_DMAC_ST0_HXFER, st.hxfer);
        write_reg(EMUBUS_CPU_MON_SEL_DMAC_ST0, reg);

        reg = 0;
        INSERTN(reg, EMUBUS_CPU_MON_DMAC_BL0_A_O, st.a_o);
        INSERTN(reg, EMUBUS_CPU_MON_DMAC_BL0_PA_O, st.pa_o);
        write_reg(EMUBUS_CPU_MON_SEL_DMAC_BL0, reg);

        reg = 0;
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_BL1_A_OE, st.a_oe);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_BL1_PA_OE, st.pa_oe);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_BL1_NWR_O, st.nwr_o);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_BL1_NRD_O, st.nrd_o);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_BL1_NPAWR_O, st.npawr_o);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_BL1_NPARD_O, st.npard_o);
        write_reg(EMUBUS_CPU_MON_SEL_DMAC_BL1, reg);
    }

    if (which & state_pins) {
        const auto& pins = cst.pins;

        reg = read_reg(EMUBUS_CPU_MON_SEL_JOY0);
        INSERTN(reg, EMUBUS_CPU_MON_JOY0_JPOUT, pins.jpout);
        INSERTN(reg, EMUBUS_CPU_MON_JOY0_JPCLK, pins.jpclk);
        INSERTN(reg, EMUBUS_CPU_MON_JOY0_JP1D, pins.jp1d);
        INSERTN(reg, EMUBUS_CPU_MON_JOY0_JP2D, pins.jp2d);
        write_reg(EMUBUS_CPU_MON_SEL_JOY0, reg);
    }

    if (which & state_internal) {
        const auto& i = cst.internal;

        reg = 0;
        INSERTN(reg, EMUBUS_CPU_MON_CLKGEN_CYCLE_CCNT, i.clkgen.cycle_ccnt);
        INSERTN(reg, EMUBUS_CPU_MON_CLKGEN_CP2_CLKCNT, i.clkgen.cp2_clkcnt);
        INSERTN(reg, EMUBUS_CPU_MON_CLKGEN_SLOW_CCNT, i.clkgen.slow_ccnt);
        INSERT1(reg, EMUBUS_CPU_MON_CLKGEN_CPU_GATE_DLY, i.clkgen.cpu_gate_dly);
        INSERT1(reg, EMUBUS_CPU_MON_CLKGEN_CPU_ACT, i.clkgen.cpu_act);
        INSERT1(reg, EMUBUS_CPU_MON_CLKGEN_SLOWCK_ACT, i.clkgen.slowck_act);
        INSERT1(reg, EMUBUS_CPU_MON_CLKGEN_CP2, i.clkgen.cp2);
        INSERT1(reg, EMUBUS_CPU_MON_CLKGEN_CPU_REFRESH_EN, i.clkgen.cpu_refresh_en);
        INSERT1(reg, EMUBUS_CPU_MON_CLKGEN_SLOW_GATE_DLY, i.clkgen.slow_gate_dly);
        INSERT1(reg, EMUBUS_CPU_MON_CLKGEN_SLOW_REFRESH_EN, i.clkgen.slow_refresh_en);
        write_reg(EMUBUS_CPU_MON_SEL_CLKGEN, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_AJPR0);
        INSERTN(reg, EMUBUS_CPU_MON_AJPR0_CCNT, i.ajpr.ccnt);
        INSERTN(reg, EMUBUS_CPU_MON_AJPR0_CYCLE, i.ajpr.cycle);
        INSERT1(reg, EMUBUS_CPU_MON_AJPR0_VBLANK_D, i.ajpr.vblank_d);
        INSERTN(reg, EMUBUS_CPU_MON_AJPR0_JPCLK, i.ajpr.jpclk);
        INSERT1(reg, EMUBUS_CPU_MON_AJPR0_JPOUT, i.ajpr.jpout);
        write_reg(EMUBUS_CPU_MON_SEL_AJPR0, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN0);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_VBLANK_D, i.gen.vblank_d);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_VBL_SET, i.gen.vbl_set);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_VBL_CLEAR_FLAGS, i.gen.vbl_clear_flags);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_VBL_FLAG, i.gen.vbl_flag);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_VBL_FLAG_D, i.gen.vbl_flag_d);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_VBL_START, i.gen.vbl_start);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_VBL_END, i.gen.vbl_end);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_HBLANK_D, i.gen.hblank_d);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_HBL_START, i.gen.hbl_start);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_HBL_END, i.gen.hbl_end);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_HVT_SET, i.gen.hvt_set);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_HVT_CLEAR_FLAGS, i.gen.hvt_clear_flags);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_HVT_FLAG, i.gen.hvt_flag);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_HVT_FLAG_D, i.gen.hvt_flag_d);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_CC_D, i.gen.cc_d);
        INSERT1(reg, EMUBUS_CPU_MON_GEN0_REFRESH_EN, i.gen.refresh_en);
        write_reg(EMUBUS_CPU_MON_SEL_GEN0, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN1);
        INSERTN(reg, EMUBUS_CPU_MON_GEN1_DMA_MSET, i.gen.dma_mset);
        INSERTN(reg, EMUBUS_CPU_MON_GEN1_HCNT, i.gen.hcnt);
        write_reg(EMUBUS_CPU_MON_SEL_GEN1, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_GEN2);
        INSERTN(reg, EMUBUS_CPU_MON_GEN2_VCNT, i.gen.vcnt);
        write_reg(EMUBUS_CPU_MON_SEL_GEN2, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_MULDIV1);
        INSERTN(reg, EMUBUS_CPU_MON_MULDIV1_SHIFTER, i.muldiv.shifter);
        write_reg(EMUBUS_CPU_MON_SEL_MULDIV1, reg);

        reg = 0;
        INSERTN(reg, EMUBUS_CPU_MON_MULDIV3_ST, i.muldiv.st);
        INSERT1(reg, EMUBUS_CPU_MON_MULDIV3_MULNOTDIV, i.muldiv.muldivnot);
        write_reg(EMUBUS_CPU_MON_SEL_MULDIV3, reg);

        reg = read_reg(EMUBUS_CPU_MON_SEL_DMAC_INT0);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_INT0_NTRLZ, i.dmac.ntrlz);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_INT0_POST_VBL_END, i.dmac.post_vbl_end);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_INT0_HSTARTL, i.dmac.hstartl);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_INT0_HSTART, i.dmac.hstart);
        INSERT1(reg, EMUBUS_CPU_MON_DMAC_INT0_GPRESTART_D, i.dmac.gprestart_d);
        INSERTN(reg, EMUBUS_CPU_MON_DMAC_INT0_ACA, i.dmac.aca);
        INSERTN(reg, EMUBUS_CPU_MON_DMAC_INT0_DL, i.dmac.dl);
        INSERTN(reg, EMUBUS_CPU_MON_DMAC_INT0_MEN, i.dmac.men);
        INSERTN(reg, EMUBUS_CPU_MON_DMAC_INT0_HEN_D, i.dmac.hen_d);
        write_reg(EMUBUS_CPU_MON_SEL_DMAC_INT0, reg);

        reg = 0;
        INSERTN(reg, EMUBUS_CPU_MON_DMAC_INT1_CEN, i.dmac.cen);
        INSERTN(reg, EMUBUS_CPU_MON_DMAC_INT1_GPTPOS, i.dmac.gptpos);
        INSERTN(reg, EMUBUS_CPU_MON_DMAC_INT1_HST, i.dmac.hst);
        INSERTN(reg, EMUBUS_CPU_MON_DMAC_INT1_HBC, i.dmac.hbc);
        write_reg(EMUBUS_CPU_MON_SEL_DMAC_INT1, reg);
    }
}

#undef INSERT1
#undef INSERTN

void cpu::read_state(state_t& st)
{
    st.chip = read_chip_state();
    icd.read_state(st.core);
}

void cpu::write_state(const state_t& st)
{
    write_chip_state(st.chip);
    icd.write_state(st.core);
}

} // namespace snes
