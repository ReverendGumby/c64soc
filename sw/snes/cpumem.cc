#include <memory>
#include <array>
#include <stdexcept>
#include <stdint.h>
#include <string.h>
#include "emubus.h"
#include "cart.h"
#include <util/debug.h>
#include "cpumem.h"

#define DBG_LVL 0
#define DBG_TAG "CPUMEM"

namespace snes {

std::unique_ptr<uint8_t[]> cpumem::wram;

const char* name(cpumem::source_t e)
{
    switch (e) {
    case cpumem::source_t::unknown:     return "unknown";
    case cpumem::source_t::wram:        return "wram";
    case cpumem::source_t::cart_rom:    return "cart_rom";
    case cpumem::source_t::cart_sram:   return "cart_sram";
    default: break;
    }
    return "?";
}

cpumem::source_t from_name(const char* n)
{
    static const std::array<cpumem::source_t, 4> vals = {
        cpumem::source_t::unknown,
        cpumem::source_t::wram,
        cpumem::source_t::cart_rom,
        cpumem::source_t::cart_sram,
    };
    for (auto v : vals)
        if (strcmp(n, name(v)) == 0)
            return v;
    return cpumem::source_t::unknown;
}

cpumem::cpumem(class cart& cart)
    : _cart(cart)
{
}

void cpumem::init(void)
{
    wram = std::unique_ptr<uint8_t[]> {new uint8_t[wram_len]};

    EMUBUS_CPUMEM_BASE_WRAM = reinterpret_cast<uint32_t>(&wram[0]);

    _mems.push_back(mem_desc_t{source_t::wram, get_wram(), wram_len});
}

void cpumem::add_mem(const mem_desc_t& mem)
{
    _mems.push_back(mem);
}

const cpumem::mem_desc_t& cpumem::get_mem(source_t e)
{
    for (const auto& mem : _mems)
        if (mem.source == e)
            return mem;
    throw std::runtime_error("cpumem::get_mem: no such memory");
}

uint8_t* cpumem::get_wram()
{
    return wram.get();
}

uint8_t* cpumem::addr_to_ptr(memaddr a)
{
    uint16_t a16 = a & 0xFFFF;
    uint8_t ba = (a >> 16) & 0xFF;

    if (ba == 0x7E || ba == 0x7F)
        return get_wram() + (a - 0x7E0000);
    else if (((ba & 0x7F) < 0x40) && (a16 < 0x2000))
        return get_wram() + a16;
    else
        return _cart.addr_to_ptr(a);
}

uint32_t cpumem::read_reg(uint8_t sel)
{
    EMUBUS_CPUMEM_MON_SEL = sel;
    return EMUBUS_CPUMEM_MON_DATA;
}

void cpumem::write_reg(uint8_t sel, uint32_t val)
{
    EMUBUS_CPUMEM_MON_SEL = sel;
    EMUBUS_CPUMEM_MON_DATA = val;
}

void cpumem::find_last_reader(uint32_t araddr, source_t& source,
                              uint32_t& addr_off)
{
    source = source_t::unknown;
    addr_off = 0;

    uint8_t* ar = reinterpret_cast<uint8_t*>(araddr);

    for (const auto& mem : _mems) {
        if (ar >= mem.base && ar < mem.base + mem.len) {
            source = mem.source;
            addr_off = ar - mem.base;
            break;
        }
    }
}

void cpumem::read_state(state_t& st)
{
    st.wram = unique_buf(buffer(get_wram(), wram_len));
    st.wmadd = read_reg(EMUBUS_CPUMEM_MON_SEL_WMADD);

    // cpumem is always driving out the result of the last byte read from
    // it. Record the memory that was read and the offset to the byte read.
    auto& lr = st.last_read;
    auto araddr = read_reg(EMUBUS_CPUMEM_MON_SEL_ARADDR);
    lr.data = read_reg(EMUBUS_CPUMEM_MON_SEL_RDATA);
    find_last_reader(araddr, lr.source, lr.addr_off);
    DBG_PRINTF(2, "Last read @ {%s,+0x%lx} = 0x%02x\n",
               name(lr.source), lr.addr_off, lr.data);
}

void cpumem::pre_write_state()
{
    // We don't want cpumem to start any AXI transactions in response to any
    // unexpected CPU bus activity while the emulator state is being restored.
    auto reg = EMUBUS_CPUMEM_CTRL;
    reg &= ~EMUBUS_CPUMEM_CTRL_AXI_EN;    
    EMUBUS_CPUMEM_CTRL = reg;
}

void cpumem::post_write_state()
{
    auto reg = EMUBUS_CPUMEM_CTRL;
    reg |= EMUBUS_CPUMEM_CTRL_AXI_EN;    
    EMUBUS_CPUMEM_CTRL = reg;

    auto& lr = _write_state_last_read;
    if (lr.source != source_t::unknown) {
        // Instruct cpumem to read from a specific address, such that it
        // outputs (to the CPU subsystem) the last byte read at read_state()
        // time. NB: cart memory (sram) state must be restored before we can
        // do this.
        const auto& mem = get_mem(lr.source);
        auto ra = reinterpret_cast<uint32_t>(mem.base + lr.addr_off);
        write_reg(EMUBUS_CPUMEM_MON_SEL_MADDR, ra);
        write_reg(EMUBUS_CPUMEM_MON_SEL_MCMI, EMUBUS_CPUMEM_MON_MCMI_MRREQ);

        if (DBG_TEST_LEVEL(2)) {
            uint8_t wanted = lr.data;
            uint8_t got = read_reg(EMUBUS_CPUMEM_MON_SEL_RDATA);
            DBG_PRINTF(2, "Last read @ {%s,+0x%lx}: wanted 0x%02x, got 0x%02x\n",
                       name(lr.source), lr.addr_off, wanted, got);
        }
    }
}

void cpumem::write_state(const state_t& st)
{
    buffer(get_wram(), wram_len).copy_from(st.wram);
    write_reg(EMUBUS_CPUMEM_MON_SEL_WMADD, st.wmadd);

    _write_state_last_read = st.last_read;
}

} // namespace snes
