#pragma once

#include "monitor/w65c816s/asm.h"

#include <snes/memptr.h>
#include <snes/icd.h>
#include <cli/command.h>

namespace snes {
namespace monitor {

using namespace ::monitor;
using namespace w65c816s;

class cli
{
public:
    struct model
    {
        using insts = std::vector<inst>;
        struct ppu_pos_t {
            uint16_t col;
            uint16_t row;
            uint32_t frame;
        };

        virtual uint8_t get_byte(const memptr& mp) = 0;
        virtual void halt() = 0;
        virtual void resume() = 0;
        virtual bool halted() = 0;
        virtual void get_regs(cpu_regs& regs,
                              cpu_regs& last_regs) = 0;
        virtual insts get_insts(address_t ip, size_t max_insts,
                                size_t num_prev_inst) = 0;
        virtual void get_ppu_pos(ppu_pos_t& pos, bool& known) = 0;
        virtual uint32_t get_cpu_cyc() = 0;
        virtual void step() = 0;
        virtual bool break_get_enable() = 0;
        virtual icd_match::trigger break_get_trigger() = 0;
        virtual void break_set_trigger(const icd_match::trigger&) = 0;
        virtual void break_set_enable(bool) = 0;
        virtual void toggle_break(address_t a) = 0;
        virtual void on_state_loaded() = 0;
    };

    cli(model& model);

private:
    std::string disassemble_to_str(const inst&);
    void disassemble(address_t ip, int num);

    int mem_display(const ::cli::command::args_t& args);
    int mem_write(const ::cli::command::args_t& args);
    int inst_display(const ::cli::command::args_t& args);
    int halt();
    int step(const ::cli::command::args_t& args);
    int cont(const ::cli::command::args_t& args);
    int where();
    int fstep();
    int regs();

    int breakpoints(const ::cli::command::args_t& args);
    int breakpoints_list();
    int breakpoints_set(const ::cli::command::args_t& args);
    int breakpoints_clear();
    int breakpoints_toggle(const ::cli::command::args_t& args);

    model& model;
};

} // namespace monitor
} // namespace snes
