#pragma once

#include <string>
#include "nes/icd.h"
#include "nes/icd_match.h"
#include "nes/memptr.h"
#include "cli.h"
#include "monitor/w65c816s/asm.h"
#include "monitor/disassembler.h"

namespace snes {
namespace monitor {

using namespace ::monitor;
using namespace w65c816s;

class monitor :
        private disassembler<disassembler_ctx, inst, memptr>,
        private cli::model,
        icd::listener
{
public:
    monitor(class icd& icd);

private:
    using bytes = std::vector<byte_t>;
    using insts = std::vector<inst>;

    enum {
        match_idx_cpu_sync,
        match_idx_break,
        match_num
    };

    void poll();

    // interface for disassembly_view::model
    virtual bool is_break_set(address_t a);

    // interface for cli::model
    virtual uint8_t get_byte(const memptr& mp);
    virtual void halt();
    virtual void resume();
    virtual bool halted();
    virtual void get_regs(cpu_regs& regs,
                          cpu_regs& last_regs);
    virtual insts get_insts(address_t ip, size_t max_insts,
                            size_t num_prev_inst=0);
    virtual void get_ppu_pos(ppu_pos_t& pos, bool& known);
    virtual uint32_t get_cpu_cyc();
    virtual void step();
    virtual bool break_get_enable();
    virtual icd_match::trigger break_get_trigger();
    virtual void toggle_break(address_t a);

    // interface for break_view::model
    virtual void break_set_trigger(const icd_match::trigger&);
    virtual void break_set_enable(bool);

    // interface for icd::listener
    void resumed() override;
    void on_state_loaded() override;

    void enter();
    void exit();

    void update_regs();

    bool grab_match();
    void release_match();

    void disable_all_matches();
    void halt_cpu_on_sync();
    void match_break();

    class icd& icd;
    class cli cli;
    icd_match* matches[match_num];
    bool pc_known;
    address_t icd_pc;
    bool ppu_pos_known;
    ppu_pos_t ppu_pos;
    uint32_t cpu_cyc;
    bool stopped = false;
    cpu_regs regs, last_regs;
    bool got_regs;
    bool bp_en;
    icd_match::trigger bp_trig;
};

} // namespace monitor
} // namespace snes
