#pragma once

#include "emubus.h"

#include <util/unique_buf.h>

#include <stdint.h>
#include <unistd.h>
#include <array>

namespace snes {

struct ppu_regs
{
    // Write-only registers
    struct {                                    // $2100 INIDISP
        uint8_t bright : 4;
        //uint8_t _res3 : 3;
        uint8_t forced_blank : 1;
    } inidisp;
    struct {                                    // $2101 OBJSEL
        uint8_t ba : 3;
        uint8_t n : 2;
        uint8_t ss : 3;
    } objsel;
    struct {                                    // $2105 BGMODE
        uint8_t mode : 3;
        uint8_t bg3_pri : 1;
    } bgmode;
    struct {                                    // $2106 MOSAIC
        uint8_t sz : 4;
        uint8_t bg1 : 1;
        uint8_t bg2 : 1;
        uint8_t bg3 : 1;
        uint8_t bg4 : 1;
    } mosaic;
    struct bgnsc_t {                            // $2107 BG1SC
        uint8_t size : 2;
        uint8_t base : 6;
    } bg1sc;
    struct bgnsc_t bg2sc;                       // $2108 BG2SC
    struct bgnsc_t bg3sc;                       // $2109 BG3SC
    struct bgnsc_t bg4sc;                       // $210A BG4SC
    struct {                                    // $210B BG12NBA
        uint8_t bg1nba : 4;
        uint8_t bg2nba : 4;
    } bg12nba;
    struct {                                    // $210C BG34NBA
        uint8_t bg3nba : 4;
        uint8_t bg4nba : 4;
    } bg34nba;
    uint16_t bg1hofs;                           // $210D BG1HOFS
    uint16_t m7hofs;                            //   "   M7HOFS
    uint16_t bg1vofs;                           // $210E BG1VOFS
    uint16_t m7vofs;                            //   "   M7VOFS
    uint16_t bg2hofs;                           // $210F BG2HOFS
    uint16_t bg2vofs;                           // $2110 BG2VOFS
    uint16_t bg3hofs;                           // $2111 BG3HOFS
    uint16_t bg3vofs;                           // $2112 BG3VOFS
    uint16_t bg4hofs;                           // $2113 BG4HOFS
    uint16_t bg4vofs;                           // $2114 BG4VOFS
    struct {                                    // $211A M7SEL
        uint8_t hflip : 1;
        uint8_t vflip : 1;
        uint8_t scover : 2;
    } m7sel;
    uint16_t m7a;                               // $211B M7A
    uint16_t m7b;                               // $211C M7B
    uint16_t m7c;                               // $211D M7C
    uint16_t m7d;                               // $211E M7D
    uint16_t m7x;                               // $211F M7X
    uint16_t m7y;                               // $2120 M7Y
    struct {                                    // $2123 W12SEL
        uint8_t w1_inv_bg1 : 1;
        uint8_t w1_ena_bg1 : 1;
        uint8_t w2_inv_bg1 : 1;
        uint8_t w2_ena_bg1 : 1;
        uint8_t w1_inv_bg2 : 1;
        uint8_t w1_ena_bg2 : 1;
        uint8_t w2_inv_bg2 : 1;
        uint8_t w2_ena_bg2 : 1;
    } w12sel;
    struct {                                    // $2124 W34SEL
        uint8_t w1_inv_bg3 : 1;
        uint8_t w1_ena_bg3 : 1;
        uint8_t w2_inv_bg3 : 1;
        uint8_t w2_ena_bg3 : 1;
        uint8_t w1_inv_bg4 : 1;
        uint8_t w1_ena_bg4 : 1;
        uint8_t w2_inv_bg4 : 1;
        uint8_t w2_ena_bg4 : 1;
    } w34sel;
    struct {                                    // $2125 WOBJSEL
        uint8_t w1_inv_obj : 1;
        uint8_t w1_ena_obj : 1;
        uint8_t w2_inv_obj : 1;
        uint8_t w2_ena_obj : 1;
        uint8_t w1_inv_clr : 1;
        uint8_t w1_ena_clr : 1;
        uint8_t w2_inv_clr : 1;
        uint8_t w2_ena_clr : 1;
    } wobjsel;
    uint8_t wh0;                                // $2126 WH0
    uint8_t wh1;                                // $2127 WH1
    uint8_t wh2;                                // $2128 WH2
    uint8_t wh3;                                // $2129 WH3
    enum wmlog_t {
        WML_OR = 0,
        WML_AND = 1,
        WML_XOR = 2,
        WML_XNOR = 3
    };
    struct {                                    // $212A WBGLOG
        wmlog_t bg1, bg2, bg3, bg4;
    } wbglog;
    struct {                                    // $212B WOBJLOG
        wmlog_t obj, clr;
    } wobjlog;
    struct tms_t {                              // $212C TM
        uint8_t bg1 : 1;
        uint8_t bg2 : 1;
        uint8_t bg3 : 1;
        uint8_t bg4 : 1;
        uint8_t obj : 1;
    } tm;
    struct tms_t ts;                            // $212D TS
    struct tms_t tmw;                           // $212E TMW
    struct tms_t tsw;                           // $212F TSW
    struct {                                    // $2130 CGWSEL
        uint8_t direct : 1;
        uint8_t addend : 1;
        uint8_t sub_win_rgn : 2;
        uint8_t main_win_rgn : 2;
    } cgwsel;
    struct {                                    // $2131 CGADSUB
        uint8_t bg1 : 1;
        uint8_t bg2 : 1;
        uint8_t bg3 : 1;
        uint8_t bg4 : 1;
        uint8_t obj : 1;
        uint8_t back : 1;
        uint8_t half : 1;
        uint8_t sub : 1;
    } cgadsub;
    struct {                                    // $2132 COLDATA
        uint16_t r : 5;
        uint16_t g : 5;
        uint16_t b : 5;
    } coldata;
    struct {                                    // $2133 SETINI
        uint8_t interlacing : 1;
        uint8_t obj_vdir_disp : 1;
        uint8_t bg_vdir_disp : 1;
        uint8_t h512 : 1;
        uint8_t extbg : 1;
        uint8_t extsync : 1;
    } setini;

    // Read-only registers
    uint32_t mpylmh;                            // $2134/5/6 MPYL/M/H
    uint16_t ophct;                             // $213C OPHCT
    uint16_t opvct;                             // $213D OPVCT
    struct {                                    // $213E STAT77
        uint8_t range_over : 1;
        uint8_t time_over : 1;
    } stat77;
    struct {                                    // $213F STAT78
        uint8_t opl : 1;
        uint8_t field : 1;
    } stat78;

    // Internal status registers
    struct {
        uint16_t row;
        uint16_t col;
        uint32_t frame;
    } video_counter;
    struct bus_master_t {
        struct bg_t {
            uint32_t vab, vac, vas;
        } bg[4];
        struct m7_t {
            uint16_t mpya, mpyb;
            uint32_t mpyc;
            uint32_t vas;
        } m7;
    } bus_master;

    // Internal RAM
    uint16_t cgram[EMUBUS_PPU_MON_SEL_CGRAM_LEN / 2];
    uint16_t oaml[EMUBUS_PPU_MON_SEL_OAML_LEN / 2];
    uint8_t oamh[EMUBUS_PPU_MON_SEL_OAMH_LEN];
};

class ppu
{
public:
    void init();

    enum {
        regs_io = 1<<0,
        regs_video_counter = 1<<1,
        regs_bus_master = 1<<2,
        regs_palette = 1<<3,
        regs_oam = 1<<4,
        regs_all = -1,
    };
    ppu_regs read_regs(int which = regs_all);
    void write_regs(const ppu_regs&, int which = regs_all);

    struct state_t {
        ppu_regs regs;
        unique_buf vram;
    };
    void read_state(state_t&);
    void write_state(const state_t&);

private:
    uint8_t read_reg8(uint32_t sel);
    uint16_t read_reg16(uint32_t sel);
    uint32_t read_reg(uint32_t sel);
    template<size_t Len>
    std::array<uint8_t, Len> read_array8(uint32_t sel);
    template<size_t Len>
    std::array<uint16_t, Len> read_array16(uint32_t sel);

    void write_reg8(uint32_t sel, uint8_t val);
    void write_reg16(uint32_t sel, uint16_t val);
    void write_reg(uint32_t sel, uint32_t val);
    template<size_t Len>
    void write_array8(uint32_t sel, const uint8_t array[Len]);
    template<size_t Len>
    void write_array16(uint32_t sel, const uint16_t array[Len]);
};

} // namespace snes
