#pragma once

#include "sys.h"
#include <yaml-cpp/yaml.h>
#include <istream>
#include <ostream>

namespace snes {

void yamlify_set_printable(bool e); //xxx
void yamlify_sys_state(sys& sys, const sys::state_t& st, std::ostream& os);
void deyamlify_sys_state(std::istream& is, sys::state_t& st);

} // namespace snes
