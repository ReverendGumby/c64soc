#include <stdexcept>
#include "util/string.h"
#include "util/debug.h"
#include "memptr.h"
#include "mem.h"

#define DBG_LVL 1
#define DBG_TAG "MEM"

namespace snes {

mem::mem()
    : initial(memptr(0x7E0000, memtype::cpu),
              memptr(0x7FFFFF, memtype::cpu))
{
}

void mem::init()
{
    initial.fetch();
}

void mem::reset()
{
    DBG_PRINTF(1, "\n");
    initial.store();
}

} // namespace snes
