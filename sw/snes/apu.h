#pragma once

#include "icd.h"
#include <stdint.h>
#include <monitor/spc700/cpu_regs.h>
#include <util/unique_buf.h>
#include <array>

namespace snes {

using apu_smp_state = monitor::spc700::cpu_regs;

struct apu_dsp_state
{
    // DSP registers
    struct regs_t {
        uint8_t regs[128];
    } regs;

    // Internal state
    struct internal_t {
        struct reg_file_t {
            // APU_MON_REGFILE_*
            uint32_t kon_written : 1;
            uint32_t flg_quick : 3;
        } reg_file;

        struct clkgen_t {
            // APU_MON_CLKGEN_*
            uint32_t idivcnt : 3;
            uint32_t cccnt : 2;
            uint32_t nres : 1;
        } clkgen;

        struct smp_if_t {
            // APU_MON_SMP*
            uint32_t d_o_d : 8;
            uint32_t ccken_d : 1;
            uint32_t spdrs : 8;
        } smp_if;

        struct sram_if_t {
            // APU_MON_SRAM*
            uint32_t ima : 16;
            uint32_t imdi : 8;
            uint32_t imrs : 1;
            uint32_t imws : 1;
            uint32_t mad_ssmp : 1;
        } sram_if;

        struct glb_ctr_t {
            uint16_t cnt;
        } glb_ctr;

        struct sgenloop_t {
            // APU_MON_SL*
            uint32_t sglcnt : 6;
            uint8_t pmon;
            uint8_t non;
            uint8_t dir;
            uint8_t kon;
            uint8_t kof;
            uint8_t mvoll;
            uint8_t mvolr;
            uint8_t evoll;
            uint8_t evolr;
            uint8_t flgn : 5;

            // APU_MON_NOISE*
            uint32_t noise_lfsr : 15;

            struct voice_t {
                // APU_MON_VOICE*
                uint8_t srcn;
                uint8_t srcnd;
                uint16_t pitch;
                uint8_t adsr1;
                uint8_t adsr2_gain;
                uint8_t voll;
                uint8_t volr;
                uint8_t envx;

                struct brrdec_t {
                    // APU_MON_BRRDEC*
                    uint32_t ctx[8];
                    uint32_t bbcnt : 4;
                    uint32_t srcn0_load : 1;
                    uint32_t srcn1_load : 1;
                    uint32_t srcn_inc : 1;
                    uint32_t rd_dta1 : 1;
                    uint32_t hdr : 8;
                    uint32_t dat : 8;
                    uint32_t rd_brrfsm1 : 1;
                    uint32_t brrfsm1 : 1;
                    uint32_t brrfsm2 : 1;
                    uint32_t dec_buf_full : 1;
                    uint16_t srcnc;
                } brrdec;

                struct decbuf_t {
                    // APU_MON_DECBUF*
                    uint32_t play : 1;
                    uint32_t out_samp : 15;
                    uint8_t deccnt[8];
                    uint8_t intcnt[8];
                    uint16_t mem[0x60];
                } decbuf;

                struct interp_t {
                    // APU_MON_INTERP*
                    uint32_t ctx[8];
                    uint32_t pcnt : 15;
                    uint32_t pstep : 14;
                    uint32_t acc : 17;
                    uint32_t gos : 6;
                    uint16_t sout;
                } interp;

                struct env_t {
                    // APU_MON_ENV*
                    uint32_t ctx[8];
                    uint32_t envelope : 11;
                    uint32_t adsr_st : 2;
                    uint32_t ctx_swap : 1;
                } env;

                struct scale_pan_t {
                    // APU_MON_SP*
                    uint32_t vml : 1;
                    uint32_t vmr : 1;
                    uint32_t out : 15;
                    uint16_t outl, outr;
                } scale_pan;
            } voice;

            struct echo_t {
                // APU_MON_ECHO*
                uint8_t efb;
                uint8_t eon;
                uint8_t edl;
                uint8_t esa;
                uint32_t fcrs : 4;
                uint32_t fcls : 4;
                uint32_t fcl : 1;
                uint32_t idx : 13;
                uint32_t ece : 1;
                uint32_t idx_max : 13;
                uint32_t buf_ptr : 14;
                uint16_t eboutl, eboutr;

                struct mixer_t {
                    // APU_MON_EMIX*
                    uint32_t suml : 25;
                    uint32_t vsel : 3;
                    uint32_t sumr : 25;
                    uint16_t soutl, soutr;
                } mixer;

                struct fir_t {
                    // APU_MON_EFIR*
                    uint32_t suml : 17;
                    uint32_t sp : 3;
                    uint32_t sumr : 17;
                    uint16_t sl[8];
                    uint16_t sr[8];
                    uint16_t soutl, soutr;
                } fir;
            } echo;

            struct out_mixer_t {
                // APU_MON_OMIX*
                uint32_t suml : 25;
                uint32_t sumr : 25;
                uint16_t soutl, soutr;
            } out_mixer;
        } sgenloop;
    } internal;
};

class apu
{
public:
    void init();

    enum {
        dsp_state_regs = 1<<0,
        dsp_state_internal = 1<<1,
        dsp_state_all = -1,
    };

    apu_smp_state read_smp_state();
    apu_dsp_state read_dsp_state(int which = dsp_state_all);
    void write_smp_state(const apu_smp_state&);
    void write_dsp_state(const apu_dsp_state&, int which = dsp_state_all);

    struct state_t {
        apu_smp_state smp;
        apu_dsp_state dsp;
        unique_buf ram;
    };
    void read_state(state_t&);
    void write_state(const state_t&);

private:
    uint32_t read_reg(uint32_t sel);
    template<size_t Len, typename T>
    void read_array(uint32_t sel, T array[Len]);
    void write_reg(uint32_t sel, uint32_t val);
    template<size_t Len, typename T>
    void write_array(uint32_t sel, const T array[Len]);
};

} // namespace snes
