#include <stdint.h>
#include "phys_layer.h"

namespace sd {

// This is an SD protocol "block", the basic unit of data transfer.
static constexpr unsigned data_block_size = 512;

struct sd_xactn
{
    sd_cmd& cmd;
    sd_rsp& rsp;
    sd_data* dat;
};

class sdhc
{
public:
    void reset();
    void start();
    void stop();
    bool is_started();

    void stop_clock();
    void start_clock();
    void set_clock_freq(int hz);
    void set_data_xfer_4bit_mode(bool);
    void set_high_speed_enable(bool);

    bool is_card_inserted();

    void do_cmd(sd_cmdrsp& cr, sd_data* dat = nullptr);

    void dump();

private:
    bool is_clock_started();
    uint32_t get_isr_check_error();
    void check_error() { (void)get_isr_check_error(); }
    void set_data(sd_rsp& rsp, uint32_t rep[4]);

    void issue_cmd(sd_xactn& x);
    void finalize_cmd(sd_xactn& x);
    void xfer_data(sd_xactn& x);
    void error_recovery(uint32_t isr);
};

} // namespace sd
