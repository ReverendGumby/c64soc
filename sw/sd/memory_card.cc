#include <string.h>
#include <assert.h>
#include <string>
#include <sstream>
#include <iomanip>
#include "util/debug.h"
#include "util/timer.h"
#include "sdhc.h"
#include "phys_layer.h"
#include "sd_exception.h"
#include "fat/filesystem_error.h"
#include "memory_card.h"

namespace sd {

#define DBG_LVL 0
#define DBG_TAG "SD_MC"

memory_card::memory_card(sdhc* hc, os::mem::mempool* mp)
    : hc(hc), mp(mp)
{
    sectors_count = 0;
    spb = mp->get_mem_size() / fat::sector_size;
}

void memory_card::identify_card()
{
    reset();
    verify_voltage();
    set_voltage();

    sd_cmdrsp_r2_cid get_cid{CMD2_ALL_SEND_CID};
    do_cmd(get_cid);
    cid = get_cid.rsp;
    DBG_PRINTF(2, "MID=%02X\n", cid.mid);

    sd_cmdrsp_r6 get_rca{CMD3_SEND_RELATIVE_ADDR};
    do_cmd(get_rca);
    rca = get_rca.rsp.rca;
    DBG_PRINTF(2, "RCA=%04X\n", rca);

    // We're now in data transfer mode.
    // Lowest supported speed for all cards is Default Speed, 25 MHz.
    constexpr int fpp = 25000000;         // clock frequency, bus speed mode
    hc->set_clock_freq(fpp);

    // Move the card from stand-by to transfer state.
    sd_cmdrsp_r1 select{CMD7_SELECT_DESELECT_CARD, sd_cmd_arg(rca)<<16};
    select.rsp.busy = true;
    do_cmd(select);

    // Change bus width to 4-bit mode
    sd_cmdrsp_r1 bus_width{ACMD6_SET_BUS_WIDTH, sd_cmd_arg(ACMD6_4BIT)};
    do_app_cmd(bus_width);
    hc->set_data_xfer_4bit_mode(true);

    // Read the SD CARD Configuration Register.
    get_scr();

    if (scr.spec_ver() >= 110) {
        // Try to switch bus speed mode to High-speed.
        high_speed_mode();
    }
}

void memory_card::do_cmd(sd_cmdrsp_r1& cr, sd_data* dat)
{
    hc->do_cmd(cr, dat);
    check_csr(cr.rsp.csr);
}

void memory_card::do_cmd(sd_cmdrsp_r6& cr)
{
    hc->do_cmd(cr);
    auto status = cr.rsp.status;
    uint32_t csr = 0;
    if (status & R6_CSR_COM_CRC_ERROR)
        csr |= CSR_COM_CRC_ERROR;
    if (status & R6_CSR_ILLEGAL_COMMAND)
        csr |= CSR_ILLEGAL_COMMAND;
    if (status & R6_CSR_ERROR)
        csr |= CSR_ERROR;
    csr |= status & R6_CSR_CSR;
    check_csr(csr);
}

void memory_card::do_cmd(sd_cmdrsp& cr)
{
	hc->do_cmd(cr);
}

void memory_card::do_app_cmd(sd_cmdrsp_r1& cr, sd_data* dat)
{
    sd_cmdrsp_r1 f55{CMD55_APP_CMD, sd_cmd_arg(rca)<<16};
    do_cmd(f55);
    do_cmd(cr, dat);
}

void memory_card::do_app_cmd(sd_cmdrsp_r3& cr)
{
    sd_cmdrsp_r1 f55{CMD55_APP_CMD, sd_cmd_arg(rca)<<16};
    do_cmd(f55);
    do_cmd(cr);
}

void memory_card::check_csr(uint32_t csr)
{
#ifndef FSBL
    std::stringstream ss;
    if (csr & CSR_OUT_OF_RANGE)
        ss << "OUT_OF_RANGE ";
    if (csr & CSR_ADDRESS_ERROR)
        ss << "ADDRESS_ERROR ";
    if (csr & CSR_BLOCK_LEN_ERROR)
        ss << "BLOCK_LEN_ERROR ";
    if (csr & CSR_ERASE_SEQ_ERROR)
        ss << "ERASE_SEQ_ERROR ";
    if (csr & CSR_ERASE_PARAM)
        ss << "ERASE_PARAM ";
    if (csr & CSR_WP_VIOLATION)
        ss << "WP_VIOLATION ";
    if (csr & CSR_CARD_IS_LOCKED)
        ss << "CARD_IS_LOCKED ";
    if (csr & CSR_LOCK_UNLOCK_FAILED)
        ss << "LOCK_UNLOCK_FAILED ";
    if (csr & CSR_COM_CRC_ERROR)
        ss << "COM_CRC_ERROR ";
    if (csr & CSR_ILLEGAL_COMMAND)
        ss << "ILLEGAL_COMMAND ";
    if (csr & CSR_CARD_ECC_FAILED)
        ss << "CARD_ECC_FAILED ";
    if (csr & CSR_CC_ERROR)
        ss << "CC_ERROR ";
    if (csr & CSR_ERROR)
        ss << "ERROR ";
    if (csr & CSR_CSD_OVERWRITE)
        ss << "CSD_OVERWRITE ";
    if (csr & CSR_WP_ERASE_SKIP)
        ss << "WP_ERASE_SKIP ";
    if (csr & CSR_CARD_ECC_DISABLED)
        ss << "CARD_ECC_DISABLED ";
    if (csr & CSR_ERASE_RESET)
        ss << "ERASE_RESET ";

    ss << "CS=";
    switch (csr & CSR_CURRENT_STATE)
    {
    case CSR_CURRENT_STATE_IDLE:  ss << "IDLE ";  break;
    case CSR_CURRENT_STATE_READY: ss << "READY "; break;
    case CSR_CURRENT_STATE_IDENT: ss << "IDENT "; break;
    case CSR_CURRENT_STATE_STBY:  ss << "STBY ";  break;
    case CSR_CURRENT_STATE_TRAN:  ss << "TRAN ";  break;
    case CSR_CURRENT_STATE_DATA:  ss << "DATA ";  break;
    case CSR_CURRENT_STATE_RCV:   ss << "RCV ";   break;
    case CSR_CURRENT_STATE_PRG:   ss << "PRG ";   break;
    case CSR_CURRENT_STATE_DIS:   ss << "DIS ";   break;
    default:                      ss << "??? ";   break;
    }

    if (csr & CSR_APP_CMD)
        ss << "APP_CMD ";
    if (csr & CSR_AKE_SEQ_ERROR)
        ss << "AKE_SEQ_ERROR ";

    DBG_PRINTF(3, "CSR=%08lX: %s\n", csr, ss.str().c_str());
#else
    DBG_PRINTF(3, "CSR=%08lX\n", csr);
#endif
}

void memory_card::reset()
{
    rca = 0x0000;
    sdsc = false;

    constexpr int fod = 400000;      // clock frequency, identification mode
    hc->set_clock_freq(fod);

    sd_cmdrsp_r0 reset{CMD0_GO_IDLE_STATE};
    do_cmd(reset);
}

void memory_card::verify_voltage()
{
    f8 = false;
    sd_cmd_arg arg = CMD8_CHECK_PAT | CMD8_VHS_3V3;
    sd_cmdrsp_r7 cmd8{CMD8_SEND_IF_COND, arg};
    try
    {
        do_cmd(cmd8);
        DBG_PRINTF(10, "CMD8 rsp=%08lX\n", cmd8.rsp.arg);
        if (cmd8.rsp.arg == arg)
            f8 = true;
    }
    catch (sd_timeout&)
    {
        DBG_PRINTF(1, "CMD8 timeout\n");
    }
}

void memory_card::set_voltage()
{
    sd_cmdrsp_r3 get_ocr{ACMD41_SD_SEND_OP_COND, 0};
    do_app_cmd(get_ocr);
    auto ocr = get_ocr.rsp.ocr;
    DBG_PRINTF(2, "OCR=%08lX\n", ocr);

    bool busy;
    timer t;
    t.start(1 * timer::sec);
    do
    {
        ocr = OCR_VW_HOST;
        if (f8)
            ocr |= OCR_HCS;
        sd_cmdrsp_r3 set_ocr{ACMD41_SD_SEND_OP_COND, ocr};
        do_app_cmd(set_ocr);
        ocr = set_ocr.rsp.ocr;
        busy = !(ocr & OCR_BUSY);
    }
    while (busy && !t.expired());
    DBG_PRINTF(2, "busy=%d, time=%d ms\n", busy, int(t.elapsed() / timer::ms));
    if (busy)
        throw fat::filesystem_error("memory_card::set_voltage() timed out");

    sdsc = !(ocr & OCR_CCS);
}

void memory_card::get_scr()
{
    sd_cmdrsp_r1 get_scr{ACMD51_SEND_SCR};
    sd_data dat{&scr};
    do_app_cmd(get_scr, &dat);

    DBG_PRINTF(2, "SCR.SD_SPEC=%u\n", unsigned(scr.sd_spec));
}

void memory_card::high_speed_mode()
{
    auto func = sd_cmd6_arg::access_mode_high_speed;

    // Get supported functions
    auto check_arg = sd_cmd6_arg::make_check();
    sd_cmdrsp_r1 check_func{CMD6_SWITCH_FUNC, check_arg.v};
    sd_cmd6_status status;
    sd_data func_dat{&status};
    do_cmd(check_func, &func_dat);

    if (status.support_access_mode & (1 << func)) {
        // Switch card to high-speed mode
        auto switch_arg = sd_cmd6_arg::make_switch();
        switch_arg.r.access_mode = func;
        sd_cmdrsp_r1 switch_func{CMD6_SWITCH_FUNC, switch_arg.v};
        do_cmd(switch_func, &func_dat);

        if (status.sel_access_mode == func) {
            // Switch host to high-speed mode
            constexpr int fpp_hs = 50000000; // High-Speed, 50 MHz
            hc->set_clock_freq(fpp_hs);
            hc->set_high_speed_enable(true);
        }
    }
}

memory_card::block_buffer_t memory_card::read_block(fat::sector s)
{
    DBG_PRINTF(1, "%06u\n", (unsigned)s);

    auto* bufptr = mp->get_buffer();
    auto& buf = *bufptr;

    static_assert(data_block_size == fat::sector_size, "block / sector size mismatch");
    unsigned blk_cnt = spb;
    sd_cmd_arg arg = sdsc ? s * data_block_size : s;
    cmd_index ci = blk_cnt > 1 ? CMD18_READ_MULTIPLE_BLOCK : CMD17_READ_SINGLE_BLOCK;
    try
    {
        sd_cmdrsp_r1 rc{ci, arg};
        sd_data bd{buf.ptr, buf.len};
        bd.sdma = true;
        bd.blk_cnt = blk_cnt;
        do_cmd(rc, &bd);
    }
    catch (sd_exception& e)
    {
        hc->dump();
        throw fat::filesystem_error(e.what());
    }

    return block_buffer_t{bufptr};
}

void memory_card::write_block(fat::sector s, const block_buffer_t& buf)
{
    DBG_PRINTF(1, "%06u\n", (unsigned)s);

    static_assert(data_block_size == fat::sector_size, "block / sector size mismatch");
    unsigned blk_cnt = spb;
    sd_cmd_arg arg = sdsc ? s * data_block_size : s;
    cmd_index ci = blk_cnt > 1 ? CMD25_WRITE_MULTIPLE_BLOCK : CMD24_WRITE_BLOCK;
    try
    {
        sd_cmdrsp_r1 wc{ci, arg};
        wc.rsp.busy = true;
        sd_data bd{buf->ptr, buf->len};
        bd.sdma = true;
        bd.write = true;
        bd.blk_cnt = blk_cnt;
        do_cmd(wc, &bd);
    }
    catch (sd_exception& e)
    {
        hc->dump();
        throw fat::filesystem_error(e.what());
    }
}

void memory_card::flush_writes()
{
    // NOP; we never enabled card cache.
}

} // namespace sd
