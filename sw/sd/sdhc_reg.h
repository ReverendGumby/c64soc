#ifndef SDHC_REG__H
#define SDHC_REG__H

#include <stdint.h>
#include "os/soc/module_base.h"

namespace sd {

#define SDHC_BASE		            os::soc::module_base.sd0
#define REG(off,width)              (*((volatile uint##width##_t *)(SDHC_BASE + (off))))
#define MASK(base)                  (((1<<(base##_WIDTH))-1)<<(base##_OFFSET))

#define SDMA_SYS_ADDR               REG(0x00, 32) // System DMA Address Register

#define BLOCK_SIZE                  REG(0x04, 16)
#define BLOCK_SIZE_XFER_OFFSET      0
#define BLOCK_SIZE_XFER_WIDTH       12
#define BLOCK_SIZE_XFER_MASK        MASK(BLOCK_SIZE_XFER)
#define BLOCK_SIZE_SDMA_OFFSET      12
#define BLOCK_SIZE_SDMA_WIDTH       3
#define BLOCK_SIZE_SDMA_512K        (7<<12)

#define BLOCK_COUNT                 REG(0x06, 16)

#define ARGUMENT                    REG(0x08, 32)

#define XFER_MODE                   REG(0x0C, 16)
#define XFER_MODE_DMA_EN            (1<<0)
#define XFER_MODE_BLOCK_CNT_EN      (1<<1)
#define XFER_MODE_AUTO_CMD12_EN     (1<<2)
#define XFER_MODE_DAT_DIR           (1<<4)
#define XFER_MODE_MULTI_BLK         (1<<5)

#define COMMAND                     REG(0x0E, 16)
#define COMMAND_RESP_TYPE_MASK      (3<<0)
#define COMMAND_RESP_TYPE_NONE      (0<<0)
#define COMMAND_RESP_TYPE_136       (1<<0)
#define COMMAND_RESP_TYPE_48        (2<<0)
#define COMMAND_RESP_TYPE_48_BUSY   (3<<0)
#define COMMAND_CRC_CHECK_EN        (1<<3)
#define COMMAND_INDEX_CHECK_EN      (1<<4)
#define COMMAND_DATA_PRESENT        (1<<5)
#define COMMAND_TYPE_MASK           (3<<6)
#define COMMAND_TYPE_NORMAL         (0<<6)
#define COMMAND_TYPE_SUSPEND        (1<<6)
#define COMMAND_TYPE_RESUME         (2<<6)
#define COMMAND_TYPE_ABORT          (3<<6)
#define COMMAND_TYPE_INDEX_OFFSET   8
#define COMMAND_TYPE_INDEX_WIDTH    (13-8+1)
#define COMMAND_TYPE_INDEX_MASK     MASK(COMMAND_TYPE_INDEX)

#define RESPONSE0                   REG(0x10, 32)
#define RESPONSE1                   REG(0x14, 32)
#define RESPONSE2                   REG(0x18, 32)
#define RESPONSE3                   REG(0x1C, 32)

#define BUF_DATA_PORT               REG(0x20, 32)

#define STATE                       REG(0x24, 32)
#define STATE_CMD_LVL               (1<<24)
#define STATE_DAT3_LVL              (1<<23)
#define STATE_DAT2_LVL              (1<<22)
#define STATE_DAT1_LVL              (1<<21)
#define STATE_DAT0_LVL              (1<<20)
#define STATE_WP_LVL                (1<<19)
#define STATE_CD_LVL                (1<<18)
#define STATE_CARD_STATE_STABLE     (1<<17)
#define STATE_CARD_INSERTED         (1<<16)
#define STATE_BUFFER_READ_EN        (1<<11)
#define STATE_BUFFER_WRITE_EN       (1<<10)
#define STATE_BUFFER_WRITE_EN       (1<<10)
#define STATE_READ_XFER_ACTIVE      (1<<9)
#define STATE_DAT_ACTIVE            (1<<2)
#define STATE_DAT_INHIBIT           (1<<1)
#define STATE_CMD_INHIBIT           (1<<0)

#define HOST_CTRL                   REG(0x28, 8)
#define HOST_CTRL_HS_EN             (1<<2)
#define HOST_CTRL_DAT_4BIT          (1<<1)

#define POWER_CTRL                  REG(0x29, 8)
#define POWER_CTRL_POWER            (1<<0)
#define POWER_CTRL_VOLTAGE_3V3      (7<<1)

#define CLOCK                       REG(0x2C, 16)
#define CLOCK_SDCLK_DIV(x)          (((x)/2)<<8) // x = 1 - 256
#define CLOCK_SDCLK_MASK            (0xff<<8)

#define CLOCK_SDCLK_EN              (1<<2)
#define CLOCK_INT_CLK_STABLE        (1<<1)
#define CLOCK_INT_CLK_EN            (1<<0)

#define TIMEOUT_CTRL                REG(0x2E, 8)
#define TIMEOUT_CTRL_CTR(exp)       (((exp)-13)<<0)
#define TIMEOUT_CTRL_CTR_MASK       (15<<0)

#define SWRST                       REG(0x2F, 8)
#define SWRST_DAT                   (1<<2)
#define SWRST_CMD                   (1<<1)
#define SWRST_ALL                   (1<<0)
    
#define ISR                         REG(0x30, 32)
#define ISR_ERRORS                  (0x3ff<<16)
#define ISR_AUTO_CMD12              (1<<24)
#define ISR_CURRENT_LIMIT           (1<<23)
#define ISR_DAT_END                 (1<<22)
#define ISR_DAT_CRC                 (1<<21)
#define ISR_DAT_TIMEOUT             (1<<20)
#define ISR_CMD_INDEX               (1<<19)
#define ISR_CMD_END                 (1<<18)
#define ISR_CMD_CRC                 (1<<17)
#define ISR_CMD_TIMEOUT             (1<<16)
#define ISR_ERROR_INT               (1<<15)
#define ISR_CARD_INT                (1<<8)
#define ISR_CARD_REMOVAL            (1<<7)
#define ISR_CARD_INSERTION          (1<<6)
#define ISR_BUF_READ_RDY            (1<<5)
#define ISR_BUF_WRITE_RDY           (1<<4)
#define ISR_DMA_INT                 (1<<3)
#define ISR_XFER_COMPLETE           (1<<1)
#define ISR_CMD_COMPLETE            (1<<0)

#define ISR_STATUS_EN               REG(0x34, 32)

#define ISR_SIGNAL_EN               REG(0x38, 32)

#define AUTO_CMD12_ERR_STATUS       REG(0x3C, 16)
#define AUTO_CMD12_NOT_ISSUED       (1<<7)
#define AUTO_CMD12_INDEX            (1<<4)
#define AUTO_CMD12_END_BIT          (1<<3)
#define AUTO_CMD12_CRC              (1<<2)
#define AUTO_CMD12_TIMEOUT          (1<<1)
#define AUTO_CMD12_NOT_EXEC         (1<<0)

#define CAPS                        REG(0x40, 32)
#define CAPS_64BIT_SYS_BUS          (1<<28)
#define CAPS_1V8                    (1<<26)
#define CAPS_3V0                    (1<<25)
#define CAPS_3V3                    (1<<24)
#define CAPS_SUSPEND                (1<<23)
#define CAPS_SDMA                   (1<<22)
#define CAPS_HS                     (1<<21)
#define CAPS_ADMDA2                 (1<<19)
#define CAPS_MAX_BLOCK_MASK         (3<<16)
#define CAPS_MAX_BLOCK_512          (0<<16)
#define CAPS_MAX_BLOCK_1024         (1<<16)
#define CAPS_MAX_BLOCK_2048         (2<<16)
#define CAPS_SDCLK_OFFSET           8
#define CAPS_SDCLK_WIDTH            (13-8+1)
#define CAPS_SDCLK_MASK             MASK(CAPS_SDCLK)
#define CAPS_TOCLK_UNIT             (1<<7)
#define CAPS_TOCLK_KHZ              (1<<7)
#define CAPS_TOCLK_MHZ              (0<<7)
#define CAPS_TOCLK_FREQ_OFFSET      0
#define CAPS_TOCLK_FREQ_WIDTH       (5-0+1)
#define CAPS_TOCLK_FREQ_MASK        MASK(CAPS_TOCLK_FREQ)

#define MAX_CURRENT_CAPS            REG(0x48, 32)
#define MAX_CURRENT_MASK            255
#define MAX_CURRENT_1V8             ((MAX_CURRENT_CAPS>>16) & MAX_CURRENT_MASK)
#define MAX_CURRENT_3V0             ((MAX_CURRENT_CAPS>>8) & MAX_CURRENT_MASK)
#define MAX_CURRENT_3V3             ((MAX_CURRENT_CAPS>>0) & MAX_CURRENT_MASK)

#define HC_VERSION                  REG(0xFE, 8)
#define HC_VERSION_1P00             0x00
#define HC_VERSION_2P00             0x01


} // namespace sd

#endif /* SDHC_REG__H */
