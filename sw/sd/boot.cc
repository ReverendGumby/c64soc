#include <stdio.h>
#include "sdhc.h"
#include "memory_card.h"
#include "sd_exception.h"
#include "fat/fifo_block_cache.h"
#include "fat/fat32.h"
#include "fat/partitioner.h"
#include "fat/filesystem_error.h"
#include "boot.h"

namespace sd {

static constexpr unsigned dmabuf_blk_size = sd::data_block_size * 8;
static constexpr size_t dmabuf_count = 2;

__attribute__ ((aligned (sd::data_block_size)))
static uint8_t dmabuf[dmabuf_blk_size * dmabuf_count];

boot::boot()
    : dmapool(dmabuf, dmabuf_blk_size, dmabuf_count)
{
}

void boot::mount_card()
{
    hc = new sdhc();
    mc = std::shared_ptr<memory_card>{new memory_card(hc, &dmapool)};

    hc->reset();
    hc->start();
    mc->identify_card();
    find_part_dev();

    fs = fat::fat32::mount(dev);
}

void boot::find_part_dev()
{
    auto bc = std::shared_ptr<fat::block_device>
        {new fat::fifo_block_cache{mc, dmabuf_count}};

	fat::partitioner parter;
    parter.load(bc);
    const std::vector<fat::partitioner::part_dev>& parts = parter.partitions();
    auto& pd = parts[0];

    dev = pd.dev;
}

} // namespace sd
