#ifndef PHYS_LAYER_H_
#define PHYS_LAYER_H_

#include <string>

namespace sd {

//////////////////////////////////////////////////////////////////////
// Command index

enum cmd_index
{
    CMD0_GO_IDLE_STATE = 0,
    CMD2_ALL_SEND_CID = 2,
    CMD3_SEND_RELATIVE_ADDR = 3,
    CMD4_SET_DSR = 4,
    CMD6_SWITCH_FUNC = 6,
    CMD7_SELECT_DESELECT_CARD = 7,
    CMD8_SEND_IF_COND = 8,
    CMD9_SEND_CSD = 9,
    CMD10_SEND_CID = 10,
    CMD11_VOLTAGE_SWITCH = 11,
    CMD12_STOP_TRANSMISSION = 12,
    CMD13_SEND_STATUS = 13,
    CMD15_GO_INACTIVE_STATE = 15,
    CMD16_SET_BLOCKLEN = 16,
    CMD17_READ_SINGLE_BLOCK = 17,
    CMD18_READ_MULTIPLE_BLOCK = 18,
    CMD19_SEND_TUNING_BLOCK = 19,
    CMD20_SPEED_CLASS_CONTROL = 20,
    CMD23_SET_BLOCK_COUNT = 23,
    CMD24_WRITE_BLOCK = 24,
    CMD25_WRITE_MULTIPLE_BLOCK = 25,
    CMD27_PROGRAM_CSD = 27,
    CMD28_SET_WRITE_PROT = 28,
    CMD29_CLR_WRITE_PROT = 29,
    CMD30_SEND_WRITE_PROT = 30,
    CMD32_ERASE_WR_BLK_START = 32,
    CMD33_ERASE_WR_BLK_END = 33,
    CMD38_ERASE = 38,
    CMD55_APP_CMD = 55,
    CMD56_GEN_CMD = 56,
    ACMD6_SET_BUS_WIDTH = 6,
    ACMD13_SD_STATUS = 13,
    ACMD22_SEND_NUM_WR_BLOCKS = 22,
    ACMD23_SET_WR_BLK_ERASE_COUNT = 23,
    ACMD41_SD_SEND_OP_COND = 41,
    ACMD42_SET_CLR_CARD_DETECT = 42,
    ACMD51_SEND_SCR = 51,
};

//////////////////////////////////////////////////////////////////////
// Command arguments

union sd_cmd6_arg
{
    uint32_t v;
    struct {
        uint32_t access_mode : 4;               // function group 1
        uint32_t command_sys : 4;               // function group 2
        uint32_t drive_strength : 4;            // function group 3
        uint32_t power_limit : 4;               // function group 4
        uint32_t grp_5 : 4;                     // reserved (0h or Fh)
        uint32_t grp_6 : 4;                     // reserved (0h or Fh)
        uint32_t _res24 : 7;                    // reserved (0)
        uint32_t mode : 1;                      // 0 = check, 1 = switch
    } r;

    static constexpr uint32_t access_mode_default = 0x0;
    static constexpr uint32_t access_mode_high_speed = 0x1;
    static constexpr uint32_t no_influence = 0xf;

    static sd_cmd6_arg make_check();
    static sd_cmd6_arg make_switch();
};

#define CMD8_CHECK_PAT      (42<<0)
#define CMD8_VHS_3V3        (1<<8)

#define ACMD6_1BIT          (0<<0)
#define ACMD6_4BIT          (2<<0)

//////////////////////////////////////////////////////////////////////
// Command data blocks

// Wide width data (ACMD51(SCR), CMD6(status)) is transmitted on the wire(s) MSB
// first. Structures for these data declare members in the reverse order (LSB
// first). The SDHC will swap bytes to get things to line up.

struct sd_scr // ACMD51(SCR)
{
    uint32_t _res0 : 32;
    uint32_t cmd_support : 4;
    uint32_t _res36 : 2;
    uint32_t sd_specx : 4;
    uint32_t sd_spec4 : 1;
    uint32_t ex_security : 4;
    uint32_t sd_spec3 : 1;
    uint32_t sd_bus_widths : 4;
    uint32_t sd_security : 3;
    uint32_t data_stat_after_erase : 1;
    uint32_t sd_spec : 4;
    uint32_t scr_structure : 4;

    unsigned spec_ver() const;
};

struct sd_cmd6_status // CMD6(status) - Switch Function Status
{
    uint16_t _res0[16];
    uint16_t _res256;
    uint16_t busy_access_mode;
    uint16_t busy_command_sys;
    uint16_t busy_drive_strength;
    uint16_t busy_power_limit;
    uint16_t busy_grp_5;
    uint16_t busy_grp_6;
    uint16_t ver : 8;
    uint16_t sel_access_mode : 4;
    uint16_t sel_command_sys : 4;
    uint16_t sel_drive_strength : 4;
    uint16_t sel_power_limit : 4;
    uint16_t sel_grp_5 : 4;
    uint16_t sel_grp_6 : 4;
    uint16_t support_access_mode;
    uint16_t support_command_sys;
    uint16_t support_drive_strength;
    uint16_t support_power_limit;
    uint16_t support_grp_5;
    uint16_t support_grp_6;
    uint16_t max_current;
};

//////////////////////////////////////////////////////////////////////
// Command response

enum class rsp_name { R0, R1, R2, R3, R6, R7 };
// R0 = no response

using sd_cmd_arg = uint32_t;

#define CSR_OUT_OF_RANGE        (1<<31)
#define CSR_ADDRESS_ERROR       (1<<30)
#define CSR_BLOCK_LEN_ERROR     (1<<29)
#define CSR_ERASE_SEQ_ERROR     (1<<28)
#define CSR_ERASE_PARAM         (1<<27)
#define CSR_WP_VIOLATION        (1<<26)
#define CSR_CARD_IS_LOCKED      (1<<25)
#define CSR_LOCK_UNLOCK_FAILED  (1<<24)
#define CSR_COM_CRC_ERROR       (1<<23)
#define CSR_ILLEGAL_COMMAND     (1<<22)
#define CSR_CARD_ECC_FAILED     (1<<21)
#define CSR_CC_ERROR            (1<<20)
#define CSR_ERROR               (1<<19)
#define CSR_CSD_OVERWRITE       (1<<16)
#define CSR_WP_ERASE_SKIP       (1<<15)
#define CSR_CARD_ECC_DISABLED   (1<<14)
#define CSR_ERASE_RESET         (1<<13)
#define CSR_CURRENT_STATE       (15<<9)
#define CSR_CURRENT_STATE_IDLE  (0<<9)
#define CSR_CURRENT_STATE_READY (1<<9)
#define CSR_CURRENT_STATE_IDENT (2<<9)
#define CSR_CURRENT_STATE_STBY  (3<<9)
#define CSR_CURRENT_STATE_TRAN  (4<<9)
#define CSR_CURRENT_STATE_DATA  (5<<9)
#define CSR_CURRENT_STATE_RCV   (6<<9)
#define CSR_CURRENT_STATE_PRG   (7<<9)
#define CSR_CURRENT_STATE_DIS   (8<<9)
#define CSR_READY_FOR_DATA      (1<<8)
#define CSR_APP_CMD             (1<<5)
#define CSR_AKE_SEQ_ERROR       (1<<3)

struct sd_rsp
{
    sd_rsp() : busy(false) {}
    bool busy;
	virtual rsp_name name() = 0;
    virtual void unpack_data(uint32_t rep[4]) = 0;
};

struct sd_r0 : sd_rsp
{
	virtual rsp_name name() { return rsp_name::R0; }
    virtual void unpack_data(uint32_t rep[4]);
};

struct sd_r1 : public sd_rsp
{
	virtual rsp_name name() { return rsp_name::R1; }
    uint32_t csr;
    virtual void unpack_data(uint32_t rep[4]);
};

struct sd_r2 : sd_rsp
{
	virtual rsp_name name() { return rsp_name::R2; }
};

struct sd_r2_cid : sd_r2
{
    uint8_t mid;
    uint16_t oid;
    uint8_t pnm[5];
    uint8_t prv;
    uint32_t psn;
    uint16_t mdt;
    virtual void unpack_data(uint32_t rep[4]);
};

struct sd_csd_1v0
{
    unsigned padding : 8;
    unsigned csd_structure : 2;
    unsigned rsvd125 : 6;
    unsigned taac : 8;
    unsigned nsac : 8;
    unsigned tran_speed : 8;
    unsigned ccc : 12;
    unsigned read_bl_len : 4;
    unsigned read_bl_partial : 1;
    unsigned write_blk_misalign : 1;
    unsigned read_blk_misalign : 1;
    unsigned dsr_imp : 1;
    unsigned rsvd75 : 2;
    unsigned c_size : 12;
    unsigned vdd_r_curr_min : 3;
    unsigned vdd_r_curr_max : 3;
    unsigned vdd_w_curr_min : 3;
    unsigned vdd_w_curr_max : 3;
    unsigned c_size_mult : 3;
    unsigned erase_blk_en : 1;
    unsigned sector_size : 7;
    unsigned wp_grp_size : 7;
    unsigned wp_grp_enable : 1;
    unsigned rsvd30 : 2;
    unsigned r2w_factor : 3;
    unsigned write_bl_len : 4;
    unsigned write_bl_partial : 1;
    unsigned rsvd20 : 5;
    unsigned file_format_grp : 1;
    unsigned copy : 1;
    unsigned perm_write_protect : 1;
    unsigned tmp_write_protect : 1;
    unsigned file_format : 2;
    unsigned rsvd9 : 2;
};

struct sd_csd_2v0
{
    unsigned padding : 8;
    unsigned csd_structure : 2;
    unsigned rsvd125 : 6;
    unsigned taac : 8;
    unsigned nsac : 8;
    unsigned tran_speed : 8;
    unsigned ccc : 12;
    unsigned read_bl_len : 4;
    unsigned read_bl_partial : 1;
    unsigned write_blk_misalign : 1;
    unsigned read_blk_misalign : 1;
    unsigned dsr_imp : 1;
    unsigned rsvd75 : 6;
    unsigned c_size : 22;
    unsigned rsvd47 : 1;
    unsigned erase_blk_en : 1;
    unsigned sector_size : 7;
    unsigned wp_grp_size : 7;
    unsigned wp_grp_enable : 1;
    unsigned rsvd30 : 2;
    unsigned r2w_factor : 3;
    unsigned write_bl_len : 4;
    unsigned write_bl_partial : 1;
    unsigned rsvd20 : 5;
    unsigned file_format_grp : 1;
    unsigned copy : 1;
    unsigned perm_write_protect : 1;
    unsigned tmp_write_protect : 1;
    unsigned file_format : 2;
    unsigned rsvd9 : 2;
};

struct sd_r2_csd : sd_r2
{
    union
    {
        uint32_t raw[4];
        sd_csd_1v0 csd_1v0;
        sd_csd_2v0 csd_2v0;
    };
    virtual void unpack_data(uint32_t rep[4]);
};

#define OCR_VW_MASK (0xfff<<0)
#define OCR_VW_HOST (0x3f<<18)                  // 3.0-3.6
#define OCR_XPC     (1<<28)
#define OCR_HCS     (1<<30)
#define OCR_CCS     (1<<30)
#define OCR_BUSY    (1<<31)

struct sd_r3 : sd_rsp
{
	virtual rsp_name name() { return rsp_name::R3; }
    uint32_t ocr;
    virtual void unpack_data(uint32_t rep[4]);
};

#define R6_CSR_COM_CRC_ERROR        (1<<15)
#define R6_CSR_ILLEGAL_COMMAND      (1<<14)
#define R6_CSR_ERROR                (1<<13)
#define R6_CSR_CSR                  0x1fff

struct sd_r6 : sd_rsp
{
	virtual rsp_name name() { return rsp_name::R6; }
    uint16_t rca;
    /* status bits 15-13 = select CSR bits remapped: see R6_CSR_* */
    /* status bits 12-0 = CSR bits 12-0: see CSR_* */
    uint16_t status;
    virtual void unpack_data(uint32_t rep[4]);
};

struct sd_r7 : sd_rsp
{
	virtual rsp_name name() { return rsp_name::R7; }
    sd_cmd_arg arg;                               // should be echo of R8 arg
    virtual void unpack_data(uint32_t rep[4]);
};

//////////////////////////////////////////////////////////////////////
// All possible command / response pairs

struct sd_cmd
{
    sd_cmd(cmd_index i, sd_cmd_arg arg=0) : index(i), arg(arg), is_app(false) {}
    cmd_index index;
    sd_cmd_arg arg;
    bool is_app;
    std::string to_string() const;
};

struct sd_cmdrsp
{
    sd_cmd cmd;
    sd_rsp& rsp;
    sd_cmdrsp(cmd_index i, sd_cmd_arg arg, sd_rsp& rsp) : cmd(i, arg), rsp(rsp) {}
};

#define DEFINE_SD_CMDRSP(typ, n)                \
    struct typ : sd_cmdrsp                      \
    {                                           \
        typ(cmd_index i, sd_cmd_arg arg=0) :    \
            sd_cmdrsp(i, arg, rsp) {}           \
        sd_##n rsp;                             \
    }

DEFINE_SD_CMDRSP(sd_cmdrsp_r0, r0);
DEFINE_SD_CMDRSP(sd_cmdrsp_r1, r1);
DEFINE_SD_CMDRSP(sd_cmdrsp_r2_cid, r2_cid);
DEFINE_SD_CMDRSP(sd_cmdrsp_r2_csd, r2_csd);
DEFINE_SD_CMDRSP(sd_cmdrsp_r3, r3);
DEFINE_SD_CMDRSP(sd_cmdrsp_r6, r6);
DEFINE_SD_CMDRSP(sd_cmdrsp_r7, r7);

struct sd_data
{
    void* buf;
    size_t buf_len;
    unsigned blk_cnt = 0;
    bool sdma = false; // Use SDMA
    bool swap = false; // Wide width data; swap bytes
    bool write = false; // true if host -> card, else card -> host

    sd_data(void* buf, size_t buf_len)
        : buf(buf), buf_len(buf_len) {}

    template <typename WideType>
    sd_data(WideType* wide_data)
        : buf(wide_data), buf_len(sizeof(WideType)), swap(true) {}
};

} // namespace sd

#endif /* PHYS_LAYER_H_ */
