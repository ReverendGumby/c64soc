#pragma once

#include <stdbool.h>
#include <memory>
#include "os/mem/mempool.h"

namespace fat
{
class block_device;
class fat32;
};

namespace sd {

class sdhc;
class memory_card;

class boot
{
public:
    boot();

    void mount_card();

private:
    void find_part_dev();

    sdhc* hc;
    os::mem::mempool dmapool;
    std::shared_ptr<memory_card> mc;
    std::shared_ptr<fat::block_device> dev;
    std::shared_ptr<fat::fat32> fs;
};

} // namespace sd
