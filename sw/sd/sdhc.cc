#include <stdexcept>
#include <assert.h>
#include "util/debug.h"
#include "util/timer.h"
#include "sdhc_reg.h"
#include "sdhc.h"
#include "sd_exception.h"

namespace sd {

#define DBG_LVL 0
#define DBG_TAG "SDHC"

#define SDIO_FREQ  50000000

struct clock_runner
{
    sdhc* hc;
    clock_runner(sdhc* hc) : hc(hc) { hc->start_clock(); }
    ~clock_runner() { hc->stop_clock(); }
};

void sdhc::reset()
{
    SWRST = SWRST_ALL;
    while (SWRST & SWRST_ALL)
        ;
    ISR = 0xffffffff;
    ISR_STATUS_EN = 0xffffffff;
    // TMCLK = SDCLK
    // TMCLK * 2 ^ 25 ==> timeout @ fastest SDCLK should be 671 ms
    TIMEOUT_CTRL = TIMEOUT_CTRL_CTR(25);
}

bool sdhc::is_started()
{
    return (POWER_CTRL & POWER_CTRL_POWER) != 0;
}

bool sdhc::is_card_inserted()
{
    bool inserted = STATE & STATE_CARD_INSERTED;
    bool moved = ISR & ISR_CARD_REMOVAL;
    if (moved)
        ISR = ISR_CARD_REMOVAL;
    bool ret = inserted && !moved;
    DBG_PRINTF(30, "inserted=%d moved=%d ret=%d\n", inserted, moved, ret);
    return ret;
}

void sdhc::start()
{
    DBG_PRINTF(1, "\n");
    POWER_CTRL |= POWER_CTRL_VOLTAGE_3V3 | POWER_CTRL_POWER;
    ISR = 0xffffffff;
}

void sdhc::stop()
{
    DBG_PRINTF(1, "\n");
    CLOCK &= ~CLOCK_SDCLK_EN;
    POWER_CTRL &= ~POWER_CTRL_POWER;
}

bool sdhc::is_clock_started()
{
    DBG_PRINTF(10, "%d\n", (CLOCK & CLOCK_SDCLK_EN) != 0);
    return (CLOCK & CLOCK_SDCLK_EN) != 0;
}

void sdhc::set_clock_freq(int hz)
{
    DBG_PRINTF(10, "hz=%d\n", hz);
    bool clk_running = is_clock_started();
    if (clk_running)
        stop_clock();

    uint32_t base_clk_hz = SDIO_FREQ;
    int div;
    int sdclk_hz;
    for (div = 1; div <= 256; div *= 2)
    {
        sdclk_hz = base_clk_hz / div;
        if (sdclk_hz <= hz)
            break;
    }
    if (div > 256)
        throw sd_exception("cannot find SD clock divisor");

    auto clock = CLOCK;
    clock &= ~CLOCK_SDCLK_MASK;
    clock |= CLOCK_SDCLK_DIV(div) | CLOCK_INT_CLK_EN;
    CLOCK = clock;
    while ((CLOCK & CLOCK_INT_CLK_STABLE) == 0)
        ;
    DBG_PRINTF(10, "CLOCK=0x%08x\n", clock);

    if (clk_running)
        start_clock();
}

void sdhc::set_data_xfer_4bit_mode(bool e)
{
    DBG_PRINTF(10, "%d\n", e);
    if (e)
        HOST_CTRL |= HOST_CTRL_DAT_4BIT;
    else
        HOST_CTRL &= ~HOST_CTRL_DAT_4BIT;
}

void sdhc::set_high_speed_enable(bool e)
{
    DBG_PRINTF(10, "%d\n", e);
    if (e)
        HOST_CTRL |= HOST_CTRL_HS_EN;
    else
        HOST_CTRL &= ~HOST_CTRL_HS_EN;
}

void sdhc::stop_clock()
{
    DBG_PRINTF(15, "\n");
    CLOCK &= ~CLOCK_SDCLK_EN;
}

void sdhc::start_clock()
{
    DBG_PRINTF(15, "\n");
    CLOCK |= CLOCK_SDCLK_EN;
}

void sdhc::do_cmd(sd_cmdrsp& cr, sd_data* dat)
{
    sd_xactn x{cr.cmd, cr.rsp, dat};
#ifndef FSBL
    DBG_PRINTF(1, "%s\n", x.cmd.to_string().c_str());
#else
    DBG_PRINTF(1, "index=%d arg=0x%08lX\n", cr.cmd.index, cr.cmd.arg);
#endif
    {
        clock_runner cr{this};
        issue_cmd(x);
        finalize_cmd(x);
    }
}

uint32_t sdhc::get_isr_check_error()
{
    auto isr = ISR;
    DBG_PRINTF(10, "ISR=%08lx\n", isr);
    auto isr_masked = isr & (ISR_CARD_REMOVAL | ISR_ERROR_INT);
    if (isr_masked & ISR_CARD_REMOVAL)
        throw sd_exception("Card removed");

    auto error_isr = isr & ISR_ERRORS;
    if (isr_masked)
        error_recovery(error_isr);

    if ((error_isr & (ISR_CMD_TIMEOUT | ISR_CMD_CRC)) == ISR_CMD_CRC)
        throw sd_exception("Response CRC Error");
    if ((error_isr & (ISR_CMD_TIMEOUT | ISR_CMD_CRC)) == ISR_CMD_TIMEOUT)
        throw sd_timeout("Response Timeout Error");
    if ((error_isr & (ISR_CMD_TIMEOUT | ISR_CMD_CRC)) == (ISR_CMD_TIMEOUT | ISR_CMD_CRC))
        throw sd_exception("CMD line conflict");
    if (error_isr & ISR_CMD_END)
        throw sd_exception("Command End Bit Error");
    if (error_isr & ISR_CMD_INDEX)
        throw sd_exception("Command Index Error");
    if (error_isr & ISR_DAT_TIMEOUT)
        // Transfer Complete has higher priority than Data Timeout Error.
        if ((isr & ISR_XFER_COMPLETE) == 0)
            throw sd_timeout("Data Timeout Error");
    if (error_isr & ISR_DAT_CRC)
        throw sd_exception("Data CRC Error");
    if (error_isr & ISR_DAT_END)
        throw sd_exception("Data End Bit Error");
    if (error_isr & ISR_CURRENT_LIMIT)
        throw sd_exception("Current Limit Error");
    if (error_isr & ISR_AUTO_CMD12) {
        DBG_PRINTF(0, "AUTO_CMD12_ERR_STATUS=%08x\n", AUTO_CMD12_ERR_STATUS);
        throw sd_exception("Auto CMD12 Error");
    }

    return isr;
}

void sdhc::set_data(sd_rsp& rsp, uint32_t rep[4])
{
    rsp.unpack_data(rep);
}

void sdhc::issue_cmd(sd_xactn& x)
{
    uint16_t command = (x.cmd.index << COMMAND_TYPE_INDEX_OFFSET) & COMMAND_TYPE_INDEX_MASK;
    uint32_t argument = x.cmd.arg;

    if (x.cmd.index == CMD12_STOP_TRANSMISSION)
        command |= COMMAND_TYPE_ABORT;
    else
        command |= COMMAND_TYPE_NORMAL;
    if (x.rsp.name() == rsp_name::R0)
        command |= COMMAND_RESP_TYPE_NONE;
    else if (x.rsp.name() == rsp_name::R2)
        command |= COMMAND_CRC_CHECK_EN | COMMAND_RESP_TYPE_136;
    else if (x.rsp.name() == rsp_name::R3)
        command |= COMMAND_RESP_TYPE_48;
    else
    {
        command |= COMMAND_CRC_CHECK_EN | COMMAND_INDEX_CHECK_EN | COMMAND_RESP_TYPE_48;
        if (x.rsp.busy)
            command |= COMMAND_RESP_TYPE_48_BUSY;
    }
    if (x.dat)
        command |= COMMAND_DATA_PRESENT;
    command |= (x.cmd.index << COMMAND_TYPE_INDEX_OFFSET) & COMMAND_TYPE_INDEX_MASK;

    DBG_PRINTF(10, "STATE=%08lx\n", STATE);
    while ((STATE & STATE_CMD_INHIBIT) != 0)
        check_error();
    if ((command & COMMAND_RESP_TYPE_MASK) == COMMAND_RESP_TYPE_48_BUSY
        && (command & COMMAND_TYPE_ABORT) == 0)
    {
        while ((STATE & STATE_DAT_INHIBIT) != 0)
            check_error();
    }

    if (x.dat)
    {
        auto& dat = *x.dat;
        uint16_t xfer_mode = !dat.write * XFER_MODE_DAT_DIR;
        uint16_t block_size = dat.buf_len;
        uint16_t block_count = dat.blk_cnt;
        if (dat.sdma) {
            SDMA_SYS_ADDR = reinterpret_cast<uint32_t>(dat.buf);
            xfer_mode |= XFER_MODE_DMA_EN;
            // Buffer is contiguous, so DMA Interrupts are unneeded. But we
            // cannot just ignore them: the SDHC will still raise it (and halt
            // data transfer) when the boundary condition is met. So we set the
            // largest boundary to minimize their frequency.
            block_size |= BLOCK_SIZE_SDMA_512K;
        }
        if (x.cmd.index == CMD18_READ_MULTIPLE_BLOCK || x.cmd.index == CMD25_WRITE_MULTIPLE_BLOCK) {
            xfer_mode |= XFER_MODE_MULTI_BLK | XFER_MODE_AUTO_CMD12_EN;
        }
        if (block_count) {
            xfer_mode |= XFER_MODE_BLOCK_CNT_EN;
            block_size = data_block_size;
        }

        BLOCK_SIZE = block_size;
        BLOCK_COUNT = block_count;
        XFER_MODE = xfer_mode;
        DBG_PRINTF(10, "BLOCK_SIZE=%04X BLOCK_COUNT=%04X XFER_MODE=%04X\n", block_size, block_count, xfer_mode);
    }
    ARGUMENT = argument;
    COMMAND = command;
    DBG_PRINTF(10, "COMMAND=%04X ARGUMENT=%08lX\n", command, argument);
}

void sdhc::finalize_cmd(sd_xactn& x)
{
    // Wait for Command Complete Int
    timer t;
    t.start(500 * timer::ms);
    bool complete;
    do {
        complete = get_isr_check_error() & ISR_CMD_COMPLETE;
    } while (!complete && !t.expired());
    if (!complete)
        throw sd_exception("Command Complete Int timeout");

    // Clr Command Complete Status
    ISR = ISR_CMD_COMPLETE;

    // Get Response Data
    uint32_t rep[4] = { RESPONSE0, 0, 0, 0 };
    if (COMMAND & COMMAND_RESP_TYPE_136)
    {
        rep[1] = RESPONSE1;
        rep[2] = RESPONSE2;
        rep[3] = RESPONSE3;
    }
    DBG_PRINTF(5, "RESPONSE=%08lX_%08lX_%08lX_%08lX\n", rep[3], rep[2], rep[1], rep[0]);
    set_data(x.rsp, rep);

    // PIO data transfer?
    if (x.dat && !x.dat->sdma)
        xfer_data(x);

    // Command with Transfer Complete Int?
    bool tc = x.rsp.busy || x.dat;
    if (tc)
    {
        // Wait for Transfer Complete Int and DMA Int
        do {
            auto isr = get_isr_check_error();
            complete = isr & ISR_XFER_COMPLETE;
            if (!complete && (isr & ISR_DMA_INT)) {
                // Clr DMA Interrupt status
                ISR = ISR_DMA_INT;
                // Set System Address Reg
                SDMA_SYS_ADDR = SDMA_SYS_ADDR;
            }
        } while (!complete && !t.expired());
        if (!complete)
            throw sd_exception("Transfer Complete Int timeout");

        // Clr Transfer Complete Status & DMA Interrupt status
        ISR = ISR_XFER_COMPLETE | ISR_DMA_INT;
    }
    // Check Response Data
}

void sdhc::xfer_data(sd_xactn& x)
{
    // Wait for Buffer Read Ready Int
    while ((get_isr_check_error() & ISR_BUF_READ_RDY) == 0)
        ;
    // Clr Buffer Read Ready Status
    ISR = ISR_BUF_READ_RDY;
    // Get Data
    auto& dat = *x.dat;
    assert(dat.write == false); // not yet supported
    auto len = dat.buf_len;
    size_t words = (len + (sizeof(BUF_DATA_PORT) - 1))
        / sizeof(BUF_DATA_PORT);
    auto* p {reinterpret_cast<uint8_t*>(dat.buf)};
    auto swap = dat.swap;
    for (size_t i = 0; i < words; i++) {
        auto d = BUF_DATA_PORT;
        DBG_PRINTF(20, "BUF_DATA_PORT=%08lX (%d)\n", d, i);
        if (swap) {
            size_t off = len - i * 4;
            p[off - 1] = (d >> 0*8) & 0xff;
            p[off - 2] = (d >> 1*8) & 0xff;
            p[off - 3] = (d >> 2*8) & 0xff;
            p[off - 4] = (d >> 3*8) & 0xff;
        } else {
            *p++ = (d >> 0*8) & 0xff;
            *p++ = (d >> 1*8) & 0xff;
            *p++ = (d >> 2*8) & 0xff;
            *p++ = (d >> 3*8) & 0xff;
        }
    }
}

void sdhc::error_recovery(uint32_t isr)
{
    if (isr & (ISR_CMD_TIMEOUT | ISR_CMD_CRC | ISR_CMD_END | ISR_CMD_INDEX)) {
        DBG_PRINTF(1, "SW reset for CMD line\n");
        SWRST = SWRST_CMD;
        while (SWRST & SWRST_CMD)
            ;
    }
    if (isr & (ISR_DAT_TIMEOUT | ISR_DAT_CRC | ISR_DAT_END)) {
        DBG_PRINTF(1, "SW reset for DAT line\n");
        SWRST = SWRST_DAT;
        while (SWRST & SWRST_DAT)
            ;
    }
    // TODO: Issue Abort CMD, etc.
    ISR = isr;
}

void sdhc::dump()
{
    DBG_PRINTF(0, "00: SDMA_SYS_ADDR=%08lX, BLOCK_SIZE=%04X, BLOCK_COUNT=%04X\n", SDMA_SYS_ADDR, BLOCK_SIZE, BLOCK_COUNT);
    DBG_PRINTF(0, "08: ARGUMENT=%08lX, XFER_MODE=%04X, COMMAND=%04X\n", ARGUMENT, XFER_MODE, COMMAND);
    DBG_PRINTF(0, "10: RESPONSE=%08lX_%08lX_%08lX_%08lX\n", RESPONSE3, RESPONSE2, RESPONSE1, RESPONSE0);
    DBG_PRINTF(0, "20: STATE=%08lX\n", STATE);
    DBG_PRINTF(0, "28: HOST_CTRL=%02X, POWER_CTRL=%02X, CLOCK=%04X, TIMEOUT_CTRL=%02X, SWRST=%02X\n", HOST_CTRL, POWER_CTRL, CLOCK, TIMEOUT_CTRL, SWRST);
    DBG_PRINTF(0, "30: ISR=%08lX, ISR_STATUS_EN=%08lX\n", ISR, ISR_STATUS_EN);
    DBG_PRINTF(0, "38: ISR_SIGNAL_EN=%08lX, AUTO_CMD12_ERR=%04X\n", ISR_SIGNAL_EN, AUTO_CMD12_ERR_STATUS);
}

} // namespace sd
