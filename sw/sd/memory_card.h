#ifndef MEMORY_CARD__H
#define MEMORY_CARD__H

#include "os/mem/mempool.h"
#include "fat/block_device.h"

namespace sd {

class sdhc;

class memory_card : public fat::block_device
{
public:
    memory_card(sdhc* hc, os::mem::mempool* mp);
    virtual ~memory_card() {}
    void identify_card();

    // implement fat::block_device
    unsigned get_sectors_per_block() const override { return spb; }
    fat::sector get_capacity() const override { return sectors_count; }
    block_buffer_t read_block(fat::sector s) override;
    void write_block(fat::sector s, const block_buffer_t&) override;
    void flush_writes() override;

private:
    void do_cmd(sd_cmdrsp_r1& cr, sd_data* dat = nullptr);
    void do_cmd(sd_cmdrsp_r6& cr);
    void do_cmd(sd_cmdrsp& cr);
    void do_app_cmd(sd_cmdrsp_r1& cr, sd_data* dat = nullptr);
    void do_app_cmd(sd_cmdrsp_r3& cr);
    void check_csr(uint32_t csr);

    void reset();
    void verify_voltage();
    void set_voltage();
    void get_scr();
    void high_speed_mode();

    sdhc* hc;
    os::mem::mempool* mp;
    unsigned spb;
    fat::sector sectors_count;
    bool f8;
    uint16_t rca;
    bool sdsc;
    sd_r2_cid cid;
    sd_scr scr;
};

} // namespace sd

#endif /* MEMORY_CARD__H */
