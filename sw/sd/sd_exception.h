#ifndef SD_EXCEPTION_H_
#define SD_EXCEPTION_H_

#include <stdexcept>

namespace sd {

struct sd_exception : public std::runtime_error
{
    sd_exception(const char *what_arg) : std::runtime_error(what_arg) {}
};

struct sd_timeout : public sd_exception
{
    sd_timeout(const char *what_arg) : sd_exception(what_arg) {}
};

} // namespace sd

#endif /* SD_EXCEPTION_H_ */
