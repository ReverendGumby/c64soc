#include <stdio.h>
#include "util/debug.h"
#include "util/pollable.h"
#include "util/timer.h"
#include "sdhc.h"
#include "memory_card.h"
#include "sd_exception.h"
#include "fat/lru_block_cache.h"
#include "fat/fat32.h"
#include "fat/partitioner.h"
#include "fat/filesystem_error.h"
#include "sd_manager.h"

namespace sd {

#define DBG_LVL 1
#define DBG_TAG "SD_MGR"

constexpr timer::ticks debounce_time = 200 * timer::ms; // at least 2 ms

static constexpr unsigned dmabuf_blk_size = sd::data_block_size * 8;
static constexpr size_t dmabuf_count = 16;

__attribute__ ((aligned (sd::data_block_size)))
static uint8_t dmabuf[dmabuf_blk_size * dmabuf_count];

sd_manager::sd_manager()
    : dmapool(dmabuf, dmabuf_blk_size, dmabuf_count)
{
    hc = new sdhc();
    mc = nullptr;
    fs = nullptr;
    dev = nullptr;
    set_state(init);

    pollable_add(this);
}

bool sd_manager::is_card_inserted()
{
    return hc->is_card_inserted();
}

const char* sd_manager::name(state_t e)
{
    switch (e)
    {
    case init: return "init";
    case no_card:  return "no_card";
    case inserted:  return "inserted";
    case stable:  return "stable";
    case mounted:  return "mounted";
    case unmounted:  return "unmounted";
    case error:  return "error";
    default: return "???";
    }
}

void sd_manager::set_state(state_t s)
{
    state = s;
    DBG_PRINTF(3, "%s\n", name(s));
}

void sd_manager::poll()
{
    listener_list_purge();

    switch (state)
    {
    case init:
        hc->reset();
        set_state(no_card);
        break;
    case no_card:
        if (hc->is_card_inserted())
        {
            insert_debounce_start();
            set_state(inserted);
        }
        break;
    case inserted:
        if (!insert_debounce_done())
            break;
        if (debounce_is_inserted)
            set_state(stable);
        else
            set_state(no_card);
        break;
    case stable:
        if (mount_card())
        {
            set_state(mounted);
            speak(&listener::mounted);
        }
        else
            set_state(error);
        break;
    case mounted:
        if (!hc->is_card_inserted())
        {
            unmount_card();
            set_state(unmounted);
            speak(&listener::unmounted);
        }
        break;
    case unmounted:
        destroy_card();
        set_state(no_card);
        break;
    case error:
        if (!hc->is_card_inserted())
        {
            set_state(no_card);
            break;
        }
    }
}

void sd_manager::insert_debounce_start()
{
    debounce_tmr.start(debounce_time);
    debounce_is_inserted = true;
}

bool sd_manager::insert_debounce_done()
{
    bool new_is_inserted = hc->is_card_inserted();
    if (debounce_is_inserted != new_is_inserted)
    {
        debounce_is_inserted = new_is_inserted;
        debounce_tmr.start(debounce_time);
    }
    else if (debounce_tmr.expired())
    {
        return true;
    }
    return false;
}

bool sd_manager::mount_card()
{
    mc = std::shared_ptr<memory_card>{new memory_card(hc, &dmapool)};
    hc->start();
    try
    {
        mc->identify_card();
        find_part_dev();
        fs = fat::fat32::mount(dev);
        DBG_PRINTF(1, "card mounted\n");
        return true;
    }
    catch (fat::filesystem_error& e)
    {
        DBG_PRINTF(0, "%s\n", e.what());
    }
    catch (sd_exception& e)
    {
        DBG_PRINTF(0, "%s\n", e.what());
    }
    destroy_card();
    return false;
}

void sd_manager::find_part_dev()
{
    auto bc = std::shared_ptr<fat::lru_block_cache>
        {new fat::lru_block_cache{mc, dmabuf_count}};
    bc->set_writethru(false);

	fat::partitioner parter;
    parter.load(bc);
    const std::vector<fat::partitioner::part_dev>& parts = parter.partitions();
    if (parts.size() == 0)
    	throw sd_exception("no partitions");
    auto& pd = parts[0];
    const auto& part = pd.part;
    if (part._type != fat::partition::type::fat32)
        throw sd_exception("wrong FS type");

    dev = pd.dev;
}

void sd_manager::unmount_card()
{
    fs->unmount();
    DBG_PRINTF(1, "card ummounted\n");
}

void sd_manager::destroy_card()
{
    fs = nullptr;
	dev = nullptr;
    mc = nullptr;
    hc->stop();
    hc->reset();
}

} // namespace sd
