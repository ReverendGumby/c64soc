#include <string>
#include <sstream>
#include <iomanip>
#include "phys_layer.h"

namespace sd {

sd_cmd6_arg sd_cmd6_arg::make_check()
{
    sd_cmd6_arg ret{};
    ret.r.mode = 0;
    return ret;
}

sd_cmd6_arg sd_cmd6_arg::make_switch()
{
    sd_cmd6_arg ret{};
    ret.r.mode = 1;
    ret.r.access_mode = no_influence;
    ret.r.command_sys = no_influence;
    ret.r.drive_strength = no_influence;
    ret.r.power_limit = no_influence;
    ret.r.grp_5 = no_influence;
    ret.r.grp_6 = no_influence;
    return ret;
}

unsigned sd_scr::spec_ver() const
{
    if (sd_spec == 0)
        return 100; // Version 1.0 and 1.01
    if (sd_spec == 1)
        return 110; // Version 1.10
    if (sd_spec == 2) {
        if (sd_spec3 == 0)
            return 200; // Version 2.00
        if (sd_spec4 == 0 && sd_specx == 0)
            return 300; // Version 3.0x
        if (sd_spec4 == 1 && sd_specx == 0)
            return 400; // Version 4.xx
        if (sd_specx == 1)
            return 500; // Version 5.xx
        if (sd_specx == 2)
            return 600; // Version 5.xx
    }
    return 0; // reserved
}

#ifndef FSBL
std::string sd_cmd::to_string() const
{
    std::stringstream ss;
    if (is_app)
        ss << 'A';
    ss << "CMD" << (int)index;
    ss << ',' << std::setfill('0') << std::hex << std::setw(8) << arg;
    return ss.str();
}
#endif

void sd_r0::unpack_data(uint32_t rep[4])
{
}

void sd_r1::unpack_data(uint32_t rep[4])
{
    // R[39:8] = REP[0][31:0]
    csr = rep[0];
}

void sd_r2_cid::unpack_data(uint32_t rep[4])
{
    // MID = R[127:120] = REP[3][23:16]
    mid = (rep[3] >> 16) & 0xff;
    // OID = R[119:104] = REP[3][15: 0]
    oid = (rep[3] >> 0) & 0xffff;
    // PNM = R[103: 64] = REP[2][31: 0], REP[1][31:24]
    pnm[4] = (rep[2] >> 24) & 0xff;
    pnm[3] = (rep[2] >> 16) & 0xff;
    pnm[2] = (rep[2] >>  8) & 0xff;
    pnm[1] = (rep[2] >>  0) & 0xff;
    pnm[0] = (rep[1] >> 24) & 0xff;
    // PRV = R[ 63: 56] = REP[1][23:16]
    prv = (rep[1] >> 16) & 0xff;
    // PSN = R[ 55: 24] = REP[1][15: 0], REP[0][31:16]
    psn = (((rep[1] >> 0) & 0xffff) << 16)
        | (((rep[0] >> 16) & 0xffff) << 0);
    // MDT = R[ 19:  8] = REP[0][11: 0]
    mdt = (rep[0] << 0) & 0xfff;
}

void sd_r2_csd::unpack_data(uint32_t rep[4])
{
    // padding[7:0] = REP[3][31:24]
    // R[127:104] = REP[3][23:0]
    // R[103: 72] = REP[2][31:0]
    // R[ 71: 40] = REP[1][31:0]
    // R[ 39:  8] = REP[0][31:0]
    raw[3] = rep[3];
    raw[2] = rep[2];
    raw[1] = rep[1];
    raw[0] = rep[0];
}

void sd_r3::unpack_data(uint32_t rep[4])
{
    // R[39:8] = REP[0][31:0]
    ocr = rep[0];
}

void sd_r6::unpack_data(uint32_t rep[4])
{
    // R[39:8] = REP[0][31:0]
    rca = (rep[0] >> 16) & 0xffff;
    status = (rep[0] >> 0) & 0xffff;
}

void sd_r7::unpack_data(uint32_t rep[4])
{
    // R[39:8] = REP[0][31:0]
    arg = rep[0];
}

// sd_rsp& new_rsp(const sd_cmd& cmd)
// {
//     switch (cmd.index)
//     {
//     case CMD0_GO_IDLE_STATE:
//     case CMD4_SET_DSR:
//     case CMD15_GO_INACTIVE_STATE:
//         return new sd_rsp();
//     case CMD2_ALL_SEND_CID:
//     case CMD10_SEND_CID:
//         return new sd_r2_cid();
//     case CMD9_SEND_CSD:
//         return new sd_r2_csd();
//     case CMD3_SEND_RELATIVE_ADDR:
//         return new sd_r6();
//     case CMD8_SEND_IF_COND:
//         return new sd_r7();
//     case CMD7_SELECT_DESELECT_CARD:
//     case CMD12_STOP_TRANSMISSION:
//     case CMD20_SPEED_CLASS_CONTROL:
//     case CMD28_SET_WRITE_PROT:
//     case CMD29_CLR_WRITE_PROT:
//     case CMD38_ERASE:
//         return new sd_r1b();
//     default:
//         return new sd_r1();
//     }
// }

} // namespace sd
