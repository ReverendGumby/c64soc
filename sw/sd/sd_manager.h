#ifndef SD_MANAGER__H
#define SD_MANAGER__H

#include <memory>

#include "os/mem/mempool.h"
#include "util/listener.h"
#include "util/pollable.h"
#include "util/timer.h"

namespace fat
{
class block_device;
class fat32;
};

namespace sd {

namespace _sd_manager {

class listener
{
public:
    virtual void mounted() = 0;
    virtual void unmounted() = 0;
};

} // namespace _sd_manager

class sdhc;
class memory_card;

class sd_manager : private pollable,
                   public talker<_sd_manager::listener>
{
public:
    using listener = _sd_manager::listener;

    sd_manager();

    bool is_card_inserted();
    bool is_mounted() { return state == mounted; }

private:
    enum state_t { init, no_card, inserted, stable, mounted, unmounted, error };
    static const char* name(state_t);

    void set_state(state_t);
    void insert_debounce_start();
    bool insert_debounce_done();
    bool mount_card();
    void find_part_dev();
    void unmount_card();
    void destroy_card();

    // interface for pollable
    virtual const char* get_poll_name() const { return "sd_manager"; }
    void poll();

    state_t state;
    timer debounce_tmr;
    bool debounce_is_inserted;

    sdhc* hc;
    os::mem::mempool dmapool;
    std::shared_ptr<memory_card> mc;
    std::shared_ptr<fat::block_device> dev;
    std::shared_ptr<fat::fat32> fs;
};

} // namespace sd

#endif /* SD_MANAGER__H */
