#include <string>
#include <algorithm>
#include "util/debug.h"
#include "util/error.h"
#include "hub/root.h"
#include "usb_manager.h"

/* For find_class() */
#include "hub/device.h"
#include "hid/device.h"
#include "hid/xbox360.h"

namespace usb {

#define DBG_LVL 2
#define DBG_TAG "USB_MGR"

usb_manager::usb_manager()
{
    root = new hub::root(*this);
}

template <class Device>
device* special_factory(device* old_dev)
{
    if (!Device::match(*old_dev))
        return nullptr;
    return new Device(*old_dev);
}

device* usb_manager::find_class(device* dev)
{
    // Promote dev from the generic class 'device' to the specialized class that
    // corresponds to the USB device's class. If no specialized class is found,
    // keep the base class device, so it will error out on probe().

    static const std::function<device*(device*)> factories[] = {
        special_factory<hub::device>,
        special_factory<hid::device>,
        special_factory<hid::xbox360>,
    };

    for (size_t i = 0; i < sizeof(factories)/sizeof(factories[0]); i++) {
        auto* old_dev = dev;
        device* new_dev = factories[i](old_dev);
        if (new_dev) {
            dev = new_dev;
            delete old_dev;
            break;
        }
    }

    return dev;
}

int usb_manager::init()
{
    return root->init();
}

uint8_t usb_manager::get_next_device_addr()
{
    uint8_t addr = 1;
    for (auto* dev : devices) {
        uint8_t dev_addr = dev->get_dev_addr();
        if (addr <= dev_addr)
            addr = dev_addr + 1;
    }
    return addr;
}

void usb_manager::add(device* dev)
{
    devices.push_back(dev);
    speak(&listener::attached, dev);
}

void usb_manager::remove(device* dev)
{
    devices.erase(std::remove(devices.begin(), devices.end(), dev),
                  devices.end());
    speak(&listener::detached, dev);
}

bool usb_manager::is_root_error()
{
    return root->is_port_error();
}

} // namespace usb
