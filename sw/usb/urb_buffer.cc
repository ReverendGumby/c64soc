#include "urb_buffer.h"

#include <cstdlib>
#include "util/debug.h"
#include "usb.h"

#define DBG_LVL 1
#define DBG_TAG "URBF"

namespace usb {

urb_buffer::urb_buffer(dir_t d)
{
    if (d == read) {
        allocator = usb_malloc_read_buffer;
        deallocator = usb_free_read_buffer;
    } else {
        allocator = ::malloc;
        deallocator = ::free;
    }
}

urb_buffer::~urb_buffer()
{
    free();
}

void urb_buffer::alloc(size_t in_len)
{
    if (!ptr) {
        len = in_len;
        ptr = allocator(len);
        DBG_PRINTF(2, "alloc %u @ %p\n", (unsigned)len, ptr);
    }
}

void urb_buffer::free()
{
    if (ptr) {
        DBG_PRINTF(2, "free %p\n", ptr);
        deallocator(ptr);
        ptr = nullptr;
    }
}

void urb_buffer::set_used(size_t in_used)
{
    if (in_used > len) {
        DBG_PRINTF(2, "Asked for %u, buffer is only %u. Truncating.\n",
                   (unsigned)in_used, (unsigned)len);
        in_used = len;
    }
    used = in_used;
}

} // namespace usb
