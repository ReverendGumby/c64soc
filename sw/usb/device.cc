#include <stdint.h>
#include <assert.h>
#include <string.h>
#include "util/debug.h"
#include "util/error.h"
#include "util/string.h"
#include "util/wstring.h"
#include "hub/port.h"
#include "hci.h"
#include "usb_manager.h"
#include "device.h"

namespace usb {

#define DBG_LVL 1
#define DBG_TAG get_tag()

void device::update_nametag()
{
    auto port_name = get_port()->get_name();
    tag = string_printf("USB_DEV-%u", dev_addr);
    name = string_printf("[%d] %s", get_dev_addr(), port_name.c_str());
}

int device::create_endpoint(const usb_ep_descriptor *desc, usb_ep** pep)
{
    usb_ep* ep = *pep = new usb_ep();

    DBG_PRINTF(2, "create endpoint(addr=0x%x)\n",
               (unsigned)desc->bEndpointAddress);
    ep->enabled = false;
    ep->dev_addr = dev_addr;
    ep->port_num = port->get_port_index() + 1;  // 1-indexed
    ep->hub_addr = port->get_hub_addr();
    ep->eps = port->get_eps();
    ep->bEndpointAddress = desc->bEndpointAddress;
    ep->bEndpointNumber = desc->bEndpointAddress & 0xF;
    ep->out = (desc->bEndpointAddress & (1<<7)) == 0;
    ep->bmAttributes = desc->bmAttributes;
    ep->wMaxPacketSize = desc->wMaxPacketSize & 0x7FF;
    ep->bInterval = desc->bInterval;

    return hci_create_endpoint(ep);
}

static void free_endpoint(usb_ep** pep)
{
    usb_ep* ep = *pep;
    (void)hci_disable_endpoint(ep);
    hci_free_endpoint(ep);
    delete ep;
    *pep = 0;
}

device::device(usb_manager& mgr, hub::port* port)
    : mgr(mgr), port(port)
{
    update_nametag();
    dev_addr = 0;
}

device::device(const device& that)
    : mgr(that.mgr)
{
    port = that.port;
    name = that.name;
    tag = that.tag;
    dev_addr = that.dev_addr;
    manufacturer = that.manufacturer;
    product = that.product;
    ep0in = that.ep0in;
    ep0out = that.ep0out;
    memcpy(ep, that.ep, sizeof(ep));
    num_ep = that.num_ep;
    dev_desc = that.dev_desc;
    memcpy(cfg_desc_buf, that.cfg_desc_buf, sizeof(cfg_desc_buf));
    cfg_desc_len = that.cfg_desc_len;

    // Fix up the descriptor pointers.
    cfg_desc = (usb_cfg_descriptor *)cfg_desc_buf;
    intf_desc = nullptr;
    find_descriptor(UDT_INTERFACE, 0, (const usb_descriptor **)&intf_desc);
}

int device::init()
{
    usb_ep_descriptor ep0_desc = { 6, UDT_ENDPOINT, 0, UEDA_CONTROL, 8, 0 };
    usb_dev_request req;
    int ret = 0;
    
    ep0_desc.bEndpointAddress = UEDEA(0, IN);
    if ((ret = create_endpoint(&ep0_desc, &ep0in)) < 0)
    	return ret;
    if ((ret = hci_enable_endpoint(ep0in)) < 0)
    	return ret;
    ep0_desc.bEndpointAddress = UEDEA(0, OUT);
    if ((ret = create_endpoint(&ep0_desc, &ep0out)) < 0)
    	return ret;
    if ((ret = hci_enable_endpoint(ep0out)) < 0)
    	return ret;

    // Set the device address
    auto new_dev_addr = mgr.get_next_device_addr();
    DBG_PRINTF(2, "SET_ADDRESS(%u)\n", (unsigned)new_dev_addr);
    req.bmRequestType = UDRRT_DEVICE_STANDARD_OUT;
    req.bRequest = UDRR_SET_ADDRESS;
    req.wValue = new_dev_addr;
    req.wIndex = 0;
    req.wLength = 0;
    ret = ep0_write(&req, NULL);
    if (ret < 0)
        goto ep0_allocd;
    dev_addr = new_dev_addr;
    update_nametag();
    // update ep0* with new address
    ep0in->dev_addr = ep0out->dev_addr = dev_addr;
    
    // Get the device descriptor
    DBG_PRINTF(2, "GET_DESCRIPTOR(DEVICE)\n");
    req.bmRequestType = UDRRT_DEVICE_STANDARD_IN;
    req.bRequest = UDRR_GET_DESCRIPTOR;
    req.wValue = UDT_DEVICE << 8;
    req.wIndex = 0;
    req.wLength = 8;
    /* read just one packet's worth, to get ep0 max packet size */
    ret = ep0_read(&req, &dev_desc);
    if (ret < 0)
        goto ep0_allocd;
    ep0in->wMaxPacketSize = ep0out->wMaxPacketSize = dev_desc.bMaxPacketSize0;
    /* now read the whole descriptor */
    req.wLength = dev_desc.bLength;
    if (req.wLength > sizeof(dev_desc))
        req.wLength = sizeof(dev_desc);
    ret = ep0_read(&req, &dev_desc);
    if (ret < 0)
        goto ep0_allocd;

    // Get the configuration descriptor
    DBG_PRINTF(2, "GET_DESCRIPTOR(CONFIGURATION)\n");
    req.bmRequestType = UDRRT_DEVICE_STANDARD_IN;
    req.bRequest = UDRR_GET_DESCRIPTOR;
    req.wValue = (UDT_CONFIGURATION << 8) | 0;
    req.wIndex = 0;
    req.wLength = UCD_TOTAL_LENGTH_MAX;
    ret = ep0_read(&req, cfg_desc_buf);
    if (ret < 0)
        goto ep0_allocd;
    cfg_desc = (usb_cfg_descriptor *)cfg_desc_buf;
    cfg_desc_len = ret;

    // Find the first interface descriptor
    if (find_descriptor(UDT_INTERFACE, 0,
                        (const usb_descriptor **)&intf_desc) < 0) {
        DBG_PRINTF(1, "No interface descriptor\n");
        goto ep0_allocd;
    }

    // Create interface endpoints
    num_ep = 0;
    for (size_t i = 0; i < max_num_ep && i < intf_desc->bNumEndpoints; i++) {
        const usb_ep_descriptor *ep_desc;

        if (find_descriptor(UDT_ENDPOINT, i,
                            (const usb_descriptor **)&ep_desc) < 0)
            break;
        ret = create_endpoint(ep_desc, &ep[i]);
        if (ret < 0)
            goto epn_allocd;
        num_ep ++;
    }
    if (num_ep < intf_desc->bNumEndpoints) {
        DBG_PRINTF(1, "WARNING: not enough endpoints\n");
    }

    // Set the device's configuration and enable endpoints
    if ((ret = set_configuration()) < 0) {
        DBG_PRINTF(1, "Could not set configuration\n");
        goto epn_allocd;
    }

    DBG_PRINTF(2, "USB device initialized\n");
    return 0;

 epn_allocd:
    for (size_t i = num_ep; i > 0; i--)
        free_endpoint(&ep[i-1]);
 ep0_allocd:
    free_endpoint(&ep0out);
    free_endpoint(&ep0in);
    return ret;
}

void device::identify(int dp)
{
    auto& dev_desc = get_dev_desc();

    if (dp)
        DBG__PRINTF(" VID:PID %04X:%04X\n",
                    dev_desc.idVendor, dev_desc.idProduct);
    std::string str;
    if ((get_string(dev_desc.iManufacturer, str)) >= 0) {
        manufacturer = str;
        if (dp)
            DBG__PRINTF("  Manufacturer: %s\n", str.c_str());
    }
    if ((get_string(dev_desc.iProduct, str)) >= 0) {
        product = str;
        if (dp)
            DBG__PRINTF("  Product: %s\n", str.c_str());
    }
}

void device::destroy()
{
    /* Enable associated endpoints */
    for (int i = num_ep; i > 0; i--) {
        usb_ep* e = ep[i-1];
        free_endpoint(&e);
    }
    free_endpoint(&ep0out);
    free_endpoint(&ep0in);
}

usb_ep* device::get_ep(uint8_t addr)
{
    for (int i = 0; i < num_ep; i++) {
        if (ep[i]->bEndpointAddress == addr)
            return ep[i];
    }
    return nullptr;
}

int device::probe()
{
    // No specialized class was found for this USB device class.
    auto intf_class = get_intf_desc().bInterfaceClass;
    DBG_PRINTF(1, "Device class %u is not supported\n", intf_class);
    return -ENOENT;
}

void device::on_probed()
{
    DBG_PRINTF(1, "\n");
    mgr.add(this);
}

void device::on_removed()
{
    DBG_PRINTF(1, "\n");
    mgr.remove(this);
}

int device::get_wstring(uint8_t idx, std::wstring& buf)
{
    usb_dev_request req;
    int ret = 0;
    unsigned i, str_len;
    usb_str_descriptor desc;

    // Get a Unicode string
    DBG_PRINTF(2, "GET_DESCRIPTOR(STRING, idx=%u)\n", (unsigned)idx);
    req.bmRequestType = UDRRT_DEVICE_STANDARD_IN;
    req.bRequest = UDRR_GET_DESCRIPTOR;
    req.wValue = (UDT_STRING << 8) | idx;
    req.wIndex = 0x0409;
    req.wLength = sizeof(desc);
    ret = ep0_read(&req, &desc);
    if (ret < 0)
        return ret;

    // Translate to std::wstring
    if (desc.bLength < 2 || desc.bDescriptorType != UDT_STRING)
        return -EBADMSG;
    str_len = (desc.bLength - 2) / 2;
    wchar_t wca[str_len];
    for (i = 0; i < str_len; i++)
        wca[i] = (wchar_t)desc.bString[i * 2]
            | ((wchar_t)desc.bString[i * 2 + 1] << 8);
    buf.assign(wca, str_len);
    ret = str_len;

    return ret;
}

int device::get_string(uint8_t idx, std::string& buf)
{
    std::wstring wbuf;
    int ret = get_wstring(idx, wbuf);
    if (ret >= 0) {
        if (!wstring_to_string(wbuf, buf))
            ret = -EBADMSG;
    }
    return ret;
}

int device::find_descriptor(uint8_t bDescriptorType, unsigned index,
                           const usb_descriptor **result)
{
    usb_descriptor *d;

    for (d = (usb_descriptor *)cfg_desc; cfg_desc_len;
         d = (usb_descriptor *)((uint8_t *)d + d->bLength)) {
        if (d->bDescriptorType == bDescriptorType && index-- == 0) {
            if (result)
                *result = d;
            return 0;
        }
    }
    return -EINVAL;
}

int device::set_configuration()
{
    usb_dev_request req;
    int i;
    int ret = 0;
    
    /* Set the device's configuration */
    assert(cfg_desc);
    req.bmRequestType = UDRRT_DEVICE_STANDARD_OUT;
    req.bRequest = UDRR_SET_CONFIGURATION;
    req.wValue = cfg_desc->bConfigurationValue;
    req.wIndex = 0;
    req.wLength = 0;
    DBG_PRINTF(2, "SET_CONFIGURATION(wValue=%u)\n", req.wValue);
    ret = ep0_write(&req, NULL);
    if (ret < 0)
        return ret;

    /* Enable associated endpoints */
    for (i = 0; i < num_ep; i++)
        if ((ret = hci_enable_endpoint(ep[i])) < 0)
            break;

    return ret;
}

} // namespace usb
