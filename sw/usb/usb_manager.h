#ifndef USB__USB_MANAGER__H
#define USB__USB_MANAGER__H

#include <vector>
#include <stdint.h>
#include "util/listener.h"
#include "usb.h"

namespace usb {

class device;

namespace _usb_manager {

class listener
{
public:
    virtual void attached(device*) = 0;
    virtual void detached(device*) = 0;
};

} // namespace _usb_manager

namespace hub {
class root;
};

class usb_manager : public talker<class _usb_manager::listener>
{
public:
    using listener = _usb_manager::listener;

    static device* find_class(device* dev);

    usb_manager();

    int init();

    uint8_t get_next_device_addr();

    void add(device*);
    void remove(device*);

    bool is_root_error();
    bool has_devices() { return !devices.empty(); }
    const std::vector<device*>& get_devices() const { return devices; }

private:
    std::vector<device*> devices;
    hub::root* root;
};

} // namespace usb

#endif /* USB__USB_MANAGER__H */
