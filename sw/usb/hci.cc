#include <sstream>
#include <functional>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include "util/debug.h"
#include "util/error.h"
#include "util/string.h"
#include "util/timer.h"
#include "os/cpu/cpu.h"
#include "os/soc/module_base.h"
#include "usb.h"
#include "hci.h"

namespace usb {

#define DBG_LVL 1
#define DBG_TAG "HCI"

// Define to watch QH changes over time.
#undef WATCH_QH

// Define to force one TD per packet. (This gets crazy for LS devices, where
// wMaxPacketSize = 8 bytes.)
#undef ONE_TD_PER_PACKET

#define ASSERT_DUMP(cond)                       \
    do                                          \
        if (!(cond)) {                          \
            DUMP_EHCI(0);                       \
            assert(cond);                       \
        }                                       \
    while (0)

#define USB_BASE        os::soc::module_base.usb0

#define USB_REG(off)    (*((volatile uint32_t *)(USB_BASE + (off))))

// EHCI registers
#define USB_ID              USB_REG(0x0000)
#define USB_ID_VALUE        0xE441FA05

#define USBCMD              USB_REG(0x0140)
#define USBCMD_RS               (1<<0)
#define USBCMD_RST              (1<<1)
#define USBCMD_FS01             (3<<2)
#define USBCMD_PSE              (1<<4)
#define USBCMD_ASE              (1<<5)
#define USBCMD_FS2              (1<<15)

#define USBCMD_FS               (USBCMD_FS01 | USBCMD_FS2)
#define USBCMD_FS_1024          0
#define USBCMD_FS_8             (USBCMD_FS01 | USBCMD_FS2)

#define USBSTS              USB_REG(0x0144)
#define USBSTS_USBINT           (1<<0)
#define USBSTS_USBERRINT        (1<<1)
#define USBSTS_PCI              (1<<2)
#define USBSTS_HCH				(1<<12)
#define USBSTS_REC				(1<<13)
#define USBSTS_PSS				(1<<14)
#define USBSTS_ASS				(1<<15)

#define USBINTR             USB_REG(0x0148)

#define FRINDEX             USB_REG(0x014C)

#define PERIODICLISTBASE    USB_REG(0x0154)

#define ASYNCLISTADDR       USB_REG(0x0158)

#define TTCTRL              USB_REG(0x015C)
#define TTCTRL_TTHA             (0x7F<<24)      // TT Hub Address
#define TTCTRL_TTAC             (1<<1)          // Async. buffers clear
#define TTCTRL_TTAS             (1<<0)          // Async. buffers status

#define PORTSC1             USB_REG(0x0184)
#define PORTSC1_CCS             (1<<0)          // Current Connect Status
#define PORTSC1_CSC             (1<<1)          // Connect Status Change
#define PORTSC1_PE              (1<<2)          // Port Enabled
#define PORTSC1_PEC             (1<<3)          // Port Enable/Disable Change
#define PORTSC1_OCA             (1<<4)          // Over-current Active
#define PORTSC1_OCC             (1<<5)          // Over-current Change
#define PORTSC1_SUSP            (1<<7)          // Suspend
#define PORTSC1_PR              (1<<8)          // Port Reset
#define PORTSC1_LS              (3<<10)         // Line State
#define PORTSC1_PP              (1<<12)         // Port Power enable
#define PORTSC1_PSPD            (3<<26)         // Port Speed operating mode
#define PORTSC1_PSPD_FS         (0<<26)         // Full Speed
#define PORTSC1_PSPD_LS         (1<<26)         // Low Speed
#define PORTSC1_PSPD_HS         (2<<26)         // High Speed
#define PORTSC1_PSPD_NC         (3<<26)         // Not connected
#define PORTSC1_PSPD_OFFSET     26

#define USBMODE             USB_REG(0x01A8)
#define USBMODE_CM              (3<<0)
#define USBMODE_CM_HOST         (3<<0)

/*
 * Queue Element Transfer Descriptor
 * Reused in Queue Head Transfer Overlay Area
 * Fields marked $ are reserved 0 in qTD, valid in hQH
 */
typedef struct {
	// DWord 0: Next pointer
	unsigned Next_qTD_Ptr : 32;
	// DWord 1: Alternate next pointer
	unsigned Alt_Next_qTD_Ptr : 32;
	// DWord 2: Token
    unsigned Status : 8;
	unsigned PID : 2;
	unsigned Cerr : 2;
	unsigned C_Page : 3;
	unsigned IOC : 1;
	unsigned Total_Bytes : 15;
	unsigned DT : 1;
	// DWord 3-7: Buffer pointer list
	unsigned Buf_Ptr0 : 32;
	// DWord 4
	unsigned Buf_Ptr1 : 32;                     /* must be 4K aligned */
	// DWord 5
	unsigned Buf_Ptr2 : 32;                     /* must be 4K aligned */
	// DWord 6
	unsigned Buf_Ptr3 : 32;                     /* must be 4K aligned */
	// DWord 7
	unsigned Buf_Ptr4 : 32;                     /* must be 4K aligned */
} __attribute__ ((packed)) qTD;

#define PTR_END                 ((uint32_t)0x00000001)

/* qTD.Status bit definitions */
#define QTDS_PERR           (1<<0)             /* Ping State (P)/ERR */
#define QTDS_SPLITXSTATE    (1<<1)             /* Split Transaction State */
#define QTDS_MMF            (1<<2)             /* Missed Micro-Frame */
#define QTDS_XACTERR        (1<<3)             /* Transaction Error */
#define QTDS_BD             (1<<4)             /* Babble Detected */
#define QTDS_DBE            (1<<5)             /* Data Buffer Error */
#define QTDS_HALTED         (1<<6)
#define QTDS_ACTIVE         (1<<7)

/* qTD.PID bit definitions */
#define QTDP_OUT    0
#define QTDP_IN     1
#define QTDP_SETUP  2

/* qTD.DT bit definitions */
#define QTDT_0      0
#define QTDT_1      1

/*
 * Queue Head
 * Used for control and interrupt frames to directly connected FS/LS devices
 */
typedef struct {
	// DWord 0: Horizontal link pointer
	unsigned Horiz_Link_Ptr : 32;
	// DWord 1: Endpoint capabilities and characteristics
	unsigned dev_addr : 7;
	unsigned I : 1;
	unsigned EndPt : 4;
	unsigned EPS : 2;
	unsigned DTC : 1;
	unsigned H : 1;
	unsigned Max_Pkt_Len : 11;
	unsigned C : 1;
	unsigned RL : 4;
	// DWord 2: Endpoint capabilities and characteristics
	unsigned uFrame_Smask : 8;
	unsigned uFrame_Cmask : 8;
	unsigned Hub_Addr : 7;
	unsigned Port_Num : 7;
	unsigned Mult : 2;
	// DWord 3-11: Transfer Overlay Area
    unsigned Cur_qTD_Link_Ptr : 32;
	qTD TOA;
} __attribute__ ((packed, aligned (32))) hQH;

#define TYP_QH                  1
#define PTR_TYP(ptr, typ)       ((uint32_t)(ptr) | ((typ) << 1))
#define PTR_QH(ptr)             PTR_TYP(ptr, TYP_QH)
#define HQH_PTR(dword0)         ((dword0) & ~0x1f)
#define HQH_TYP(dword0)         (((dword0) & 6) >> 1)

#define PERIODIC_LIST_NUM	8
uint32_t periodic_list[PERIODIC_LIST_NUM] __attribute__ ((aligned (4096)));

#define NUM_QH  32
hQH usb_qh[NUM_QH];
usb_ep* usb_qh_ep[NUM_QH];

#ifdef ONE_TD_PER_PACKET
#define NUM_TD  34                            // SETUP + 32x8-byte DATA + STATUS
#else
#define NUM_TD  8
#endif
qTD __attribute__ ((aligned (32))) usb_td[NUM_TD];

uint8_t usb_td_used[NUM_TD];

#ifdef WATCH_QH
hQH last_qh[10];
const void* last_qh_ptr[10];
int last_qh_used = 0;
uint32_t last_frindex[10];
#endif

static bool prs;                                // port reset start
static bool prc;                                // port reset change

static void watch_qh(const hQH *qh)
{
#ifdef WATCH_QH
    last_qh_used = 0;
    last_qh[last_qh_used] = *qh;
    last_qh_ptr[last_qh_used] = qh;
    last_frindex[last_qh_used] = FRINDEX;
#endif
}

static void check_qh(const hQH *new_qh)
{
#ifdef WATCH_QH
    os::cpu::cpu.cache.invalidate((unsigned int)new_qh, sizeof(*new_qh));
    if (memcmp(new_qh, &last_qh[last_qh_used], sizeof(hQH))) {
        if (last_qh_used == sizeof(last_qh)/sizeof(last_qh[0]) - 1) {
        	DBG_PRINTF(13, "No more last_qh slots\n");
            return;
        }
        last_qh_used++;
        last_qh[last_qh_used] = *new_qh;
        last_qh_ptr[last_qh_used] = new_qh;
        last_frindex[last_qh_used] = FRINDEX;
    }
#endif
}

static qTD *alloc_td(void)
{
    int i;
    for (i = 0; i < NUM_TD; i++)
        if (!usb_td_used[i]) {
        	usb_td_used[i] = 1;
            DBG_PRINTF(5, "alloc_td %p\n", &usb_td[i]);
            return &usb_td[i];
        }
    return NULL;
}

static void free_td(qTD *td)
{
    int i;
    for (i = 0; i < NUM_TD; i++)
        if (&usb_td[i] == td) {
            DBG_PRINTF(5, "free_td %p\n", &usb_td[i]);
            usb_td_used[i] = 0;
            break;
        }
}

static int count_used_tds(void)
{
    int ret = 0;
    for (int i = 0; i < NUM_TD; i++) {
        if (usb_td_used[i])
            ret ++;
    }
    return ret;
}

static hQH *alloc_qh(usb_ep *ep)
{
    int i;
    for (i = 0; i < NUM_QH; i++)
        if (!usb_qh_ep[i]) {
        	usb_qh_ep[i] = ep;
            return &usb_qh[i];
        }
    return NULL;
}    

static void free_qh(hQH *qh, usb_ep *ep)
{
    int i = qh - &usb_qh[0];
    assert(i < NUM_QH);
    assert(usb_qh_ep[i] == ep);
    usb_qh_ep[i] = 0;
}

#if 0
static usb_ep* get_qh_ep(hQH *qh)
{
    int i = qh - &usb_qh[0];
    assert(i < NUM_QH);
    return usb_qh_ep[i];
}
#endif

static void dump_td(const char* fn, const qTD *td, int lv=11)
{
    DBG_PRINTF_FUNC(lv, fn, "qTD @ %08X: Next_qTD/T=%08X, Alt_Next_qTD/T=%08X\n",
               (unsigned)td, td->Next_qTD_Ptr, td->Alt_Next_qTD_Ptr);
    DBG_PRINTF_FUNC(lv, fn, "  DT=%d, Total=%d, IOC=%d, C_Page=%d, Cerr=%d, PID=%d, Status=%02X\n",
               td->DT, td->Total_Bytes, td->IOC, td->C_Page, td->Cerr, td->PID, td->Status);
    DBG_PRINTF_FUNC(lv, fn, "  Buf0-4=%08X %08X %08X %08X %08X\n",
               td->Buf_Ptr0, td->Buf_Ptr1, td->Buf_Ptr2, td->Buf_Ptr3, td->Buf_Ptr4);
}

#define DUMP_TD(td) dump_td(__FUNCTION__, td)

struct dump_state {
    std::stringstream ss;
    bool print;
    void init()
    {
        ss.str("");
        print = false;
    }
};

static void dump_p(dump_state& s, const char* lbl, const char* fmt,
                   unsigned int fld)
{
    if (s.print)
        s.ss << ", ";
    s.ss << lbl << '=' << string_printf(fmt, fld);
    s.print = true;
}

static int find_lsb(unsigned int bits)
{
    int ret = 0;
    assert(bits);
    while ((bits & 1) == 0) {
        bits >>= 1;
        ret ++;
    }
    return ret;
}

static void dump_pd(dump_state& s, const char* lbl, const char* fmt,
                    unsigned int fld0, bool delta, unsigned int fld1)
{
    if (!delta || fld0 != fld1) {
        if (s.print)
            s.ss << ", ";
        s.ss << lbl << '=' << string_printf(fmt, fld0);
        if (delta)
            s.ss << "->" << string_printf(fmt, fld1);
        s.print = true;
    }
}

static void dump_qh_fields(int lv, const char *fn, const void* orig_hQH,
                           const hQH *qh0, bool delta=false,
                           const hQH *qh1=nullptr)
{
#define PD(lbl, fmt, fld)   \
    dump_pd(s, lbl, fmt, qh0->fld, delta, delta ? qh1->fld : 0)

    orig_hQH = orig_hQH ? : qh0;

    dump_state s;
    s.init();
    PD("Horiz_Link/TYP/T", "%08X", Horiz_Link_Ptr);
    DBG_PRINTF_FUNC(lv, fn, "hQH @ %p: %s\n", orig_hQH, s.ss.str().c_str());

    s.init();
    PD("RL", "%d", RL);
    PD("C", "%d", C);
    PD("MaxLen", "%d", Max_Pkt_Len);
    PD("H", "%d", H);
    PD("DTC", "%d", DTC);
    PD("EPS", "%d", EPS);
    PD("EP", "%d", EndPt);
    PD("I", "%d",  I);
    PD("Addr", "%d", dev_addr);
    if (s.print)
        DBG_PRINTF_FUNC(lv, fn, "  %s\n", s.ss.str().c_str());

    s.init();
    PD("Mult", "%d", Mult);
    PD("PortNum", "%d", Port_Num);
    PD("Hub_Addr", "%d", Hub_Addr);
    PD("Cmask", "%02x", uFrame_Cmask);
    PD("Smask", "%02x", uFrame_Smask);
    if (s.print)
        DBG_PRINTF_FUNC(lv, fn, "  %s\n", s.ss.str().c_str());

    s.init();
    PD("Cur_qTD_Link", "%08X", Cur_qTD_Link_Ptr);
    if (s.print)
        DBG_PRINTF_FUNC(lv, fn, "  %s\n", s.ss.str().c_str());

    s.init();
    PD("Next_qTD/T", "%08X", TOA.Next_qTD_Ptr);
    PD("Alt_Next_qTD/T", "%08X", TOA.Alt_Next_qTD_Ptr);
    if (s.print)
        DBG_PRINTF_FUNC(lv, fn, "  %s\n", s.ss.str().c_str());

    s.init();
    PD("DT", "%d", TOA.DT);
    PD("Total", "%d", TOA.Total_Bytes);
    PD("IOC", "%d", TOA.IOC);
    PD("C_Page", "%d", TOA.C_Page);
    PD("Cerr", "%d", TOA.Cerr);
    PD("PID", "%d", TOA.PID);
    PD("Status", "%02X", TOA.Status);
    if (s.print)
        DBG_PRINTF_FUNC(lv, fn, "  %s\n", s.ss.str().c_str());

    s.init();
    PD("0", "%08X", TOA.Buf_Ptr0);
    PD("1", "%08X", TOA.Buf_Ptr1);
    PD("2", "%08X", TOA.Buf_Ptr2);
    PD("3", "%08X", TOA.Buf_Ptr3);
    PD("4", "%08X", TOA.Buf_Ptr4);
    if (s.print)
        DBG_PRINTF_FUNC(lv, fn, "  Buf%s\n", s.ss.str().c_str());
#undef PD
}

#ifdef WATCH_QH
static void dump_qh_delta(const char* fn, const hQH *qh0, const hQH *qh1, int lv, const void* orig_hQH)
{
    if (!DBG_TEST_LEVEL(lv))
        return;
    dump_qh_fields(lv, fn, orig_hQH, qh0, true, qh1);
}
#endif

static void dump_qh(const char* fn, const hQH *qh, int lv=10, const void* orig_hQH=nullptr)
{
    if (!DBG_TEST_LEVEL(lv))
        return;
    dump_qh_fields(lv, fn, orig_hQH, qh);
}

static void dump_qh_and_tds(const char* fn, const hQH *qh, int lv)
{
    dump_qh(fn, qh, lv);

    if (!DBG_TEST_LEVEL(lv))
        return;
    uint32_t pt = qh->TOA.Status & QTDS_ACTIVE ? qh->Cur_qTD_Link_Ptr : qh->TOA.Next_qTD_Ptr;
    for (qTD *td = (qTD*)pt; (uint32_t)td != PTR_END;
         td = (qTD*)td->Next_qTD_Ptr) {
        os::cpu::cpu.cache.invalidate((unsigned int)td, sizeof(*td));
        dump_td(fn, td, lv);
    }
}

static void dump_watched_qh(const char* fn, const hQH *qh, int lv)
{
#ifdef WATCH_QH
    check_qh(qh);
    for (int i = 0; i <= last_qh_used; i++) {
        DBG_PRINTF(lv, "-- last_qh[%d] FRINDEX=%08x:\n", i, (unsigned)last_frindex[i]);
        if (i == 0)
            dump_qh(fn, &last_qh[i], lv, last_qh_ptr[i]);
        else
            dump_qh_delta(fn, &last_qh[i-1], &last_qh[i], lv, last_qh_ptr[i]);
    }
#else
    dump_qh(fn, qh, lv);
#endif
}

#define DUMP_QH(qh) dump_qh(__FUNCTION__, qh)
#define DUMP_QH_LVL(qh, lv) dump_qh(__FUNCTION__, qh, lv)
#define DUMP_WATCHED_QH(qh, lv) dump_watched_qh(__FUNCTION__, qh, lv)

static void dump_periodic_list(const char* fn, int lv)
{
    DBG_PRINTF_FUNC(lv, fn, "-- periodic_list[] @ %p:\n", periodic_list);
    for (int i = 0; i < PERIODIC_LIST_NUM; i++) {
        uint32_t p = periodic_list[i];
        DBG_PRINTF_FUNC(lv, fn, "[%d] = 0x%lx:\n", i, p);
        while (p != PTR_END) {
            hQH *qh = (hQH *)HQH_PTR(p);
            os::cpu::cpu.cache.invalidate((unsigned int)qh, sizeof(*qh));
            dump_qh_and_tds(fn, qh, lv);
            p = qh->Horiz_Link_Ptr;
        }
    }
}

static void dump_async_list(const char* fn, int lv)
{
    hQH *qh = (hQH *)ASYNCLISTADDR;
    DBG_PRINTF_FUNC(lv, fn, "-- async list = %p:\n", qh);
    if ((uint32_t)qh != PTR_END && qh != NULL) {
        os::cpu::cpu.cache.invalidate((unsigned int)qh, sizeof(*qh));
        dump_qh_and_tds(fn, qh, lv);
    }
}

static void dump_ehci(const char* fn, int lv)
{
#define PB(reg, b, fmt)                                 \
    dump_p(s, #b, fmt, ((reg & reg##_##b) >> find_lsb(reg##_##b)))

    dump_state s;
    s.init();
    PB(USBCMD, ASE, "%u");
    PB(USBCMD, PSE, "%u");
    PB(USBCMD, RST, "%u");
    PB(USBCMD, RS, "%u");
    DBG_PRINTF_FUNC(lv, fn, "USBCMD: %s\n", s.ss.str().c_str());

    s.init();
    PB(USBSTS, ASS, "%u");
    PB(USBSTS, PSS, "%u");
    PB(USBSTS, REC, "%u");
    PB(USBSTS, HCH, "%u");
    DBG_PRINTF_FUNC(lv, fn, "USBSTS: %s\n", s.ss.str().c_str());
    s.init();
    PB(USBSTS, PCI, "%u");
    PB(USBSTS, USBERRINT, "%u");
    PB(USBSTS, USBINT, "%u");
    DBG_PRINTF_FUNC(lv, fn, "        %s\n", s.ss.str().c_str());

    s.init();
    dump_p(s, "PERIODICLISTBASE", "0x%x", PERIODICLISTBASE);
    dump_p(s, "ASYNCLISTADDR", "0x%x", ASYNCLISTADDR);
    DBG_PRINTF_FUNC(lv, fn, "%s\n", s.ss.str().c_str());

    s.init();
    PB(PORTSC1, PSPD, "%u");
    PB(PORTSC1, PP, "%u");
    PB(PORTSC1, LS, "%u");
    PB(PORTSC1, PR, "%u");
    PB(PORTSC1, SUSP, "%u");
    DBG_PRINTF_FUNC(lv, fn, "PORTSC1: %s\n", s.ss.str().c_str());
    s.init();
    PB(PORTSC1, OCC, "%u");
    PB(PORTSC1, OCA, "%u");
    PB(PORTSC1, PEC, "%u");
    PB(PORTSC1, PE, "%u");
    PB(PORTSC1, CSC, "%u");
    PB(PORTSC1, CCS, "%u");
    DBG_PRINTF_FUNC(lv, fn, "         %s\n", s.ss.str().c_str());

    s.init();
    PB(TTCTRL, TTHA, "0x%x");
    PB(TTCTRL, TTAC, "%u");
    PB(TTCTRL, TTAS, "%u");
    DBG_PRINTF_FUNC(lv, fn, "TTCTRL: %s\n", s.ss.str().c_str());

#undef PB

    dump_periodic_list(fn, lv);
    dump_async_list(fn, lv);
}

#define DUMP_EHCI(lv)  dump_ehci(__FUNCTION__, lv)

static void set_td(qTD *td, unsigned pid, unsigned dt, const void *buf, uint16_t len)
{
    memset(td, 0, sizeof(*td));
    td->Status = QTDS_ACTIVE;
    td->PID = pid;
    td->Cerr = 3;
    td->Total_Bytes = len;
    td->DT = dt;
    td->Buf_Ptr0 = (unsigned)buf;
    td->Buf_Ptr1 = (td->Buf_Ptr0 & ~0xfff) + 0x1000;
    td->Buf_Ptr2 = td->Buf_Ptr1 + 0x1000;
    td->Buf_Ptr3 = td->Buf_Ptr2 + 0x1000;
    td->Buf_Ptr4 = td->Buf_Ptr3 + 0x1000;

    if (buf)
        os::cpu::cpu.cache.clean((unsigned int)buf, len);
}

static int set_schedule_enable(uint32_t cmd_bit, int enable)
{
    const char* sched;
    uint32_t sts_bit, sts_want;
    int timeout;

    if (cmd_bit == USBCMD_ASE) {
        sts_bit = USBSTS_ASS;
        sched = "Async";
    } else if (cmd_bit == USBCMD_PSE) {
        sts_bit = USBSTS_PSS;
        sched = "Periodic";
    } else
        return -EINVAL;
    sts_want = enable ? sts_bit : 0;

    // Confirm the current state doesn't match the wanted state
    if ((USBSTS & sts_bit) == sts_want) {
        DBG_PRINTF(1, "%s schedule already %s\n", sched,
                   enable ? "started" : "stopped");
        return -EALREADY;
    }

    // Toggle the command bit
    if (enable)
        USBCMD |= cmd_bit;
    else
        USBCMD &= ~cmd_bit;

    // Wait for the state to change to match
    timeout = 16;
    do {
        if ((USBSTS & sts_bit) == sts_want)
            return 0;
        timer::sleep(125 * timer::us);
    } while (--timeout);
    DBG_PRINTF(1, "%s schedule didn't %s\n", sched,
               enable ? "start" : "stop");
    DUMP_EHCI(1);
    return -ETIMEDOUT;
}

static int set_periodic_schedule_enable(int enable)
{
    return set_schedule_enable(USBCMD_PSE, enable);
}

static int set_async_schedule_enable(int enable)
{
    return set_schedule_enable(USBCMD_ASE, enable);
}

static int init_periodic_list(void)
{
    int i;

    for (i = 0; i < PERIODIC_LIST_NUM; i++)
        periodic_list[i] = PTR_END;
    os::cpu::cpu.cache.clean((unsigned int)periodic_list, sizeof(periodic_list));

    PERIODICLISTBASE = (uint32_t)periodic_list;
    USBCMD = (USBCMD & ~USBCMD_FS) | USBCMD_FS_8;

    return set_periodic_schedule_enable(1);
}

static void init_periodic_qh(hQH *qh, usb_ep *ep)
{
    memset(qh, 0, sizeof(*qh));
    qh->Horiz_Link_Ptr = PTR_END;
    qh->dev_addr = ep->dev_addr;
    qh->EndPt = ep->bEndpointNumber;
    qh->EPS = ep->eps;
    qh->DTC = 0;                                /* QH preserves data toggle */
    qh->Max_Pkt_Len = UED_MPS_LEN(ep->wMaxPacketSize);
    qh->C = 0;
    if (ep->bmAttributes == UEDA_INTERRUPT) {
        qh->uFrame_Smask = (1<<1);
        qh->uFrame_Cmask = (1<<3) | (1<<4) | (1<<5);
    } else {
        qh->RL = 15;                            /* reload nak counter */
    }
    qh->Hub_Addr = ep->hub_addr;
    qh->Port_Num = ep->port_num;
    qh->Mult = UED_MPS_MULT(ep->wMaxPacketSize) + 1;
    qh->TOA.Next_qTD_Ptr = PTR_END;

    DUMP_QH(qh);
}

/* We do not yet compute Smask/Cmask to properly schedule FS/LS split
   transactions. If multiple QHs were to execute transactions in the same frame,
   then there's a chance to improperly order splits, seriously confusing TTs and
   eventually taking down the whole bus. The current workaround: do not
   (horizontally) link QHs. */

static int link_periodic_list(int idx, hQH *qh, usb_ep *ep)
{
    /* Find nearest free slot to requested idx */
    uint32_t *p = nullptr;
    for (; idx < PERIODIC_LIST_NUM; idx++) {
        if (periodic_list[idx] == PTR_END) {
            p = &periodic_list[idx];
            break;
        }
    }
    if (!p) {
        DBG_PRINTF(2, "No free slots\n");
        return -ENOMEM;
    }

    /* And store our QH in it */
    *p = PTR_QH(qh);

    os::cpu::cpu.cache.clean((unsigned int)p, sizeof(p));
    os::cpu::cpu.cache.clean((unsigned int)qh, sizeof(*qh));

    return 0;
}

static void unlink_periodic_list(int idx, hQH *qh)
{
    uint32_t *p = &periodic_list[idx];

    /* Find pointer to qh */
    while (HQH_PTR(*p) != (uint32_t)qh) {
        if (*p == PTR_END)
            return;
        p = (uint32_t *)HQH_PTR(*p);
    }

    /* And set it to the next QH or PTR_END */
    *p = qh->Horiz_Link_Ptr;
    os::cpu::cpu.cache.clean((unsigned int)p, sizeof(p));
}

static void prep_tds(qTD **tds, int td_used, bool ioc)
{
    int i;
    qTD *last_td = tds[td_used - 1];

    /* Link the Transfer Descriptors together */
    for (i = 0; i < td_used; i++) {
        qTD *td = tds[i];
        if (td == last_td) {
            td->Next_qTD_Ptr = PTR_END;
            td->Alt_Next_qTD_Ptr = PTR_END;
            if (ioc)
                td->IOC = 1;
        } else {
            td->Next_qTD_Ptr = (unsigned)(tds[i + 1]);
            td->Alt_Next_qTD_Ptr = (unsigned)last_td;
        }
        os::cpu::cpu.cache.clean((unsigned int)td, sizeof(*td));
        DUMP_TD(td);
    }
}

static int do_async_qh(const usb_ep *ep, qTD **tds, int td_used)
{
    int i;
    qTD *td;
    qTD *first_td = tds[0];
    hQH *qh = (hQH *)ep->qh;
    uint8_t status;
    unsigned timeout;
    int ret;

    prep_tds(tds, td_used, true);

    /* Setup the Queue Head */
    memset(qh, 0, sizeof(*qh));
    qh->Horiz_Link_Ptr = PTR_QH(qh);
    qh->dev_addr = ep->dev_addr;
    qh->EndPt = ep->bEndpointNumber;
    qh->EPS = ep->eps;
    qh->DTC = 1;                          /* qTD sets initial data toggle */
    qh->H = 1;                            /* head of reclamation list */
    qh->Max_Pkt_Len = UED_MPS_LEN(ep->wMaxPacketSize);
    qh->C = qh->EPS != EPS_HS && ep->bmAttributes == UEDA_CONTROL;
    qh->RL = 15;                                /* reload nak counter */
    qh->Hub_Addr = ep->hub_addr;
    qh->Port_Num = ep->port_num;
    qh->Mult = UED_MPS_MULT(ep->wMaxPacketSize) + 1;
    qh->TOA.Next_qTD_Ptr = (unsigned)first_td;

    DUMP_QH(qh);
    watch_qh(qh);
    os::cpu::cpu.cache.clean((unsigned int)qh, sizeof(*qh));

    if ((USBSTS & USBSTS_HCH) != 0) {
        DBG_PRINTF(1, "Controller halted itself before xactn: USBSTS=%08x\n", (unsigned int)USBSTS);
        return -ENETDOWN;
    }

    /* Add the QH to the asynchronous schedule. */
    USBSTS = USBSTS_USBINT | USBSTS_USBERRINT;  /* clear interrupts */
    ASYNCLISTADDR = (uint32_t)qh;
    if ((ret = set_async_schedule_enable(1)) < 0)
        return ret;
    check_qh(qh);

    /* Wait for transaction completion or error. */
    uint32_t sts;
    constexpr uint32_t sts_mask = USBSTS_USBINT | USBSTS_USBERRINT | USBSTS_HCH;
    timeout = 10000000;
    while (((sts = USBSTS) & sts_mask) == 0)
        if (--timeout == 0)
            break;
    if (sts & sts_mask) {
        DBG_PRINTF(2, "USBSTS=0x%08x\n", (unsigned)sts);
        if (sts & USBSTS_HCH)
            DBG_PRINTF(1, "Controller halted itself during xactn\n");
        if (sts & USBSTS_USBERRINT)
            DBG_PRINTF(2, "USB transaction error interrupt\n");
        if (sts & USBSTS_USBINT)
            DBG_PRINTF(2, "USB transaction completed\n");
    }

    /* Stop the asynchronous schedule */
    if ((ret = set_async_schedule_enable(0)) < 0)
        return ret;

    ASSERT_DUMP(ASYNCLISTADDR == (uint32_t)qh);
    if ((sts & USBSTS_HCH) != 0) {
        return -ENETDOWN;
    }
    if (timeout == 0) {
        DBG_PRINTF(1, "Transaction timed out\n");
        DUMP_EHCI(0);
        DUMP_WATCHED_QH(qh, 0);
        return -ETIMEDOUT;
    }

    os::cpu::cpu.cache.invalidate((unsigned int)qh, sizeof(*qh));
    DUMP_WATCHED_QH(qh, 12);
    ret = 0;
    status = qh->TOA.Status;
    if (status) {
        if ((status & QTDS_ACTIVE) == 0) {
            if ((status & QTDS_HALTED) != 0 && qh->TOA.Cerr != 0) {
                ret = -EPIPE;                   /* stall */
                DBG_PRINTF(2, "Endpoint STALL\n");
            } else {
                DBG_PRINTF(1, "Error occurred: Status=%02x, USBSTS=0x%08x\n", status,
                           (unsigned int)sts);
                DUMP_QH_LVL(qh, 1);
                ret = -EIO;                     /* transaction error */
            }
        } else {
            DBG_PRINTF(10, "Short packet\n");
        }
    }
    for (i = 0; i < td_used; i++) {
        td = tds[i];
        os::cpu::cpu.cache.invalidate((unsigned int)td, sizeof(*td));
        DUMP_TD(td);
    }
    return ret;
}

static void reset_port_complete(void)
{
    prs = false;
    prc = true;
}

int hci_init(void)
{
    uint32_t usb_id;

    memset(usb_qh_ep, 0, sizeof(usb_qh_ep));
    memset(usb_td_used, 0, sizeof(usb_td_used));
    
    usb_id = USB_ID;
    if (usb_id != USB_ID_VALUE) {
        DBG_PRINTF(1, "Don't see a USB controller there: ID = %08x\n", (unsigned int)usb_id);
        return -EFAULT;
    }
    
    // Reset controller
    USBCMD &= ~USBCMD_RS;
    USBCMD |= USBCMD_RST;
    while ((USBCMD & USBCMD_RST) != 0)
        ;

    // Set controller to HOST mode
    USBMODE = (USBMODE & ~USBMODE_CM) | USBMODE_CM_HOST;

    // Turn the host controller on
    USBCMD |= USBCMD_RS;

    if ((USBSTS & USBSTS_HCH) != 0) {
        DBG_PRINTF(1, "Controller didn't start; still halted\n");
        return -ENETDOWN;
    }

    // Initialize and enable periodic schedule
    EC(init_periodic_list());

    DUMP_EHCI(10);

    return 0;
}

uint16_t hci_get_frame_index(void)
{
    return FRINDEX;
}

void hci_set_port_power(bool e)
{
    if (e) {
        PORTSC1 |= PORTSC1_PP;
    } else {
        PORTSC1 &= ~PORTSC1_PP;
    }
}

hci_status hci_get_status(void)
{
    // Watch for port reset completion.
    if (prs && ((PORTSC1 & PORTSC1_PR) == 0))
        reset_port_complete();

    hci_status ret = {};
    ret.CCS = (PORTSC1 & PORTSC1_CCS) != 0;
    ret.CSC = (PORTSC1 & PORTSC1_CSC) != 0;
    ret.PE  = (PORTSC1 & PORTSC1_PE)  != 0;
    ret.PEC = (PORTSC1 & PORTSC1_PEC) != 0;
    ret.OCA = (PORTSC1 & PORTSC1_OCA) != 0;
    ret.OCC = (PORTSC1 & PORTSC1_OCC) != 0;
    ret.PR  = (PORTSC1 & PORTSC1_PR)  != 0;
    ret.PP  = (PORTSC1 & PORTSC1_PP)  != 0;
    ret.LS  = (PORTSC1 & PORTSC1_PSPD_LS) != 0;
    ret.HS  = (PORTSC1 & PORTSC1_PSPD_HS) != 0;
    ret.PRC = prc;
    return ret;
}

void hci_clear_status(hci_status s)
{
    uint32_t sc = PORTSC1 & ~(PORTSC1_CSC | PORTSC1_PEC | PORTSC1_OCC);
    if (s.CSC)
        sc |= PORTSC1_CSC;
    if (s.PEC)
        sc |= PORTSC1_PEC;
    if (s.OCC)
        sc |= PORTSC1_OCC;
    if (s.PRC)
        prc = false;
    PORTSC1 = sc;
}

void hci_reset_port(void)
{
    // Reset the port
    PORTSC1 = (PORTSC1 & ~PORTSC1_PE) | PORTSC1_PR;
    prs = true;
}

int hci_create_endpoint(usb_ep *ep)
{
    if ((ep->qh = alloc_qh(ep)) == NULL)
        return -ENOMEM;
    return 0;
}

void hci_free_endpoint(usb_ep *ep)
{
    hQH *qh = (hQH *)ep->qh;
    free_qh(qh, ep);
    ep->qh = NULL;
}

int hci_enable_endpoint(usb_ep *ep)
{
	hQH *qh = (hQH *)ep->qh;
	int i;

    assert(!ep->enabled);
    if (ep->bmAttributes == UEDA_INTERRUPT) {
        int interval = ep->bInterval;

        init_periodic_qh(qh, ep);

        if (interval < 2)
            interval = 1;
        else if (interval < 4)
            interval = 2;
        else if (interval < 8)
            interval = 4;
        else
            interval = 8;

        EC(set_periodic_schedule_enable(0));
        bool scheduled = false;
        for (i = 0; i < PERIODIC_LIST_NUM; i += interval) {
            if (link_periodic_list(i, qh, ep) < 0) {
                DBG_PRINTF(1, "Could not link @ %d, interval %d\n", i, interval);
                continue;
            }
            scheduled = true;
        }
        EC(set_periodic_schedule_enable(1));
        if (!scheduled)
            return -ENOTCONN;
    }
    ep->enabled = true;
    return 0;
}

int hci_disable_endpoint(usb_ep *ep)
{
	hQH *qh = (hQH *)ep->qh;

    if (!ep->enabled)
    	return 0;

    if (ep->bmAttributes == UEDA_INTERRUPT) {
        EC(set_periodic_schedule_enable(0));
        for (int i = 0; i < PERIODIC_LIST_NUM; i ++)
            unlink_periodic_list(i, qh);
        EC(set_periodic_schedule_enable(1));

        DBG_PRINTF(3, "count_used_tds: %d\n", count_used_tds());
    }
    ep->enabled = false;

    return 0;
}


void *hci_malloc_read_buffer(unsigned size)
{
    void *buf, *dma_buf;
    void **base_ptr;

    // Ensure the buffer doesn't share a cacheline with anything else.
    size += 64;

    buf = malloc(size);
    dma_buf = (void *)(((unsigned)buf + 32) & ~31);

    base_ptr = (void **)dma_buf - 1;
    *base_ptr = buf;

    return dma_buf;
}

void hci_free_read_buffer(void *dma_buf)
{
    void **base_ptr = (void **)dma_buf - 1;
    void *buf = *base_ptr;

    free(buf);
}

static unsigned get_max_td_bytes(const usb_ep *ep)
{
#ifdef ONE_TD_PER_PACKET
    return ep->wMaxPacketSize;
#else
    return 0x5000;
#endif
}

static int get_num_control_tds(const usb_ep *ep, const usb_dev_request *req)
{
    uint16_t data_len = req->wLength;
    auto max_td_len = get_max_td_bytes(ep);
    int num_data_tds = (data_len + max_td_len - 1) / max_td_len;
    return 2 /* SETUP + STATUS */ + num_data_tds /* DATA */;
}

static int setup_control_tds(const usb_ep* ep, const usb_dev_request *req,
                             unsigned num_td, qTD **tds, uint8_t* buf)
{
    assert(num_td < 3 || buf != NULL);

    auto data_len = req->wLength;
    decltype(data_len) max_td_len = get_max_td_bytes(ep);

    memset(tds, 0, sizeof(*tds) * num_td);
    for (unsigned i = 0; i < num_td; i++) {
        if ((tds[i] = alloc_td()) == NULL) {
            while (i--)
                free_td(tds[i]);
            return -ENOMEM;
        }
    }

    bool out = (req->bmRequestType & UDRRT_DIRECTION) == UDRRT_DIRECTION_OUT;
    unsigned data_pid = out ? QTDP_OUT : QTDP_IN;
    unsigned status_pid = out ? QTDP_IN : QTDP_OUT;

    set_td(tds[0], QTDP_SETUP, QTDT_0, req, 8); /* SETUP */
    for (unsigned i = 1; i < num_td - 1; i++) {
        auto td_len = std::min(data_len, max_td_len);
        unsigned dt = (i & 1) ? QTDT_1 : QTDT_0;
        set_td(tds[i], data_pid, dt, buf, td_len); /* DATA */
        buf += td_len;
        data_len -= td_len;
    }
    set_td(tds[num_td - 1], status_pid, QTDT_1, NULL, 0); /* STATUS */

    return 0;
}

int hci_ep_control_write(const usb_ep *ep, const usb_dev_request *req,
                         const void *data)
{
    DBG_PRINTF(2, "bmRequestType=0x%x, bRequest=%d\n"
               "wValue=0x%x, wIndex=0x%x, wLength=%d\n",
               req->bmRequestType, req->bRequest,
               req->wValue, req->wIndex, req->wLength);
    int ret = 0;
    assert(ep->enabled);

    auto num_td = get_num_control_tds(ep, req);
    qTD *tds[num_td];
    ret = setup_control_tds(ep, req, num_td, tds, (uint8_t*)data);
    if (ret < 0)
        goto tds_allocd;

    ret = do_async_qh(ep, tds, num_td);

 tds_allocd:
    for (int i = 0; i < num_td; i++)
        free_td(tds[i]);

    return ret;
}

int hci_ep_control_read(const usb_ep *ep, const usb_dev_request *req,
                        void *read_buf)
{
    DBG_PRINTF(2, "bmRequestType=0x%x, bRequest=%d,\n"
               "  wValue=0x%x, wIndex=0x%x, wLength=%x\n",
               req->bmRequestType, req->bRequest,
               req->wValue, req->wIndex, req->wLength);
    auto data_len = req->wLength;
    int ret = 0;
    assert(ep->enabled);

    auto num_td = get_num_control_tds(ep, req);
    qTD *tds[num_td];
    ret = setup_control_tds(ep, req, num_td, tds, (uint8_t*)read_buf);
    if (ret < 0)
        goto tds_allocd;

    ret = do_async_qh(ep, tds, num_td);
    if (ret < 0)
        goto tds_allocd;

    /* Compute number of bytes actually received */
    ret = data_len;
    if (data_len) {
        os::cpu::cpu.cache.invalidate((unsigned int)read_buf, data_len);
        for (int i = 1; i < num_td - 1; i++)
            ret -= tds[i]->Total_Bytes;
        if (ret < 0) {
            DBG_PRINTF(1, "Received more bytes than requested?\n");
            ret = -EIO;
            goto tds_allocd;
        }
    }

 tds_allocd:
    for (int i = 0; i < num_td; i++)
        free_td(tds[i]);
    return ret;
}

int hci_submit_urb(hci_urb *urb)
{
    const usb_ep *ep = urb->ep;
    hQH *qh = (hQH *)ep->qh;
    DBG_PRINTF(3, "urb=%p, ep=%p, qh=%p\n", urb, ep, qh);

    assert(ep->enabled);
    DBG_PRINTF(4, "ep->bmAttributes=0x%x\n", ep->bmAttributes);
    if (ep->bmAttributes == UEDA_INTERRUPT) {
        qTD *td;
        uint8_t pid;

        if ((td = alloc_td()) == NULL)
            return -ENOMEM;
        urb->td = td;
        pid = ep->out ? QTDP_OUT : QTDP_IN;
        set_td(td, pid, QTDT_0, urb->buf, urb->buf_len);
        prep_tds(&td, 1, false);

        qh->TOA.Next_qTD_Ptr = (unsigned)td;
        DUMP_QH(qh);
        os::cpu::cpu.cache.clean((unsigned int)qh, sizeof(*qh));
    }

    return 0;
}

int hci_complete_urb(hci_urb *urb)
{
    int ret = -EINPROGRESS;
    const usb_ep *ep = urb->ep;
    hQH *qh = (hQH *)ep->qh;
    DBG_PRINTF(6, "urb=%p, ep=%p, qh=%p\n", urb, ep, ep->qh);

    assert(ep->enabled);
    if (ep->bmAttributes == UEDA_INTERRUPT) {
        qTD *td = (qTD *)urb->td;
        os::cpu::cpu.cache.invalidate((unsigned int)td, sizeof(*td));
        os::cpu::cpu.cache.invalidate((unsigned int)qh, sizeof(*qh));
        uint8_t status = td->Status;
        if ((status & QTDS_ACTIVE) == 0) {
            DUMP_TD(td);
            if (status) {
                if ((status & QTDS_HALTED) != 0 && td->Cerr != 0) {
                    ret = -EPIPE;                   /* stall */
                    DBG_PRINTF(1, "Endpoint STALL\n");
                } else {
                    if (status != QTDS_XACTERR)
                        DBG_PRINTF(1, "Error occurred: Status=%02x\n", status);
                    ret = -EIO;                     /* transaction error */
                }
            } else {
                ret = urb->buf_len - td->Total_Bytes;
                if (ep->out) {
                    DBG_PRINTF(3, "OUT URB sent %d bytes\n", ret);
                } else {
                    os::cpu::cpu.cache.invalidate((unsigned int)urb->buf, urb->buf_len);
                    DBG_PRINTF(3, "IN URB received %d bytes\n", ret);
                }
            }
            free_td(td);
            urb->td = NULL;
        }
    }
    return ret;
}

void hci_abort_urb(hci_urb* urb)
{
    const usb_ep *ep = urb->ep;
    qTD *td = (qTD *)urb->td;
    hQH *qh = (hQH *)ep->qh;
    DBG_PRINTF(2, "urb=%p, ep=%p, qh=%p, td=%p\n", urb, ep, qh, td);
    if (td) {
        free_td(td);
        urb->td = NULL;
    }
}

} // namespace usb
