#ifndef __USB__URB_H
#define __USB__URB_H

#include <stdint.h>
#include "util/pollable.h"
#include "hci.h"
#include "usb.h"

namespace usb {

class urb : private pollable
{
public:
    enum state_t { undef, ready, active, complete };

    urb();
    virtual ~urb();

    // Initialize a USB request block (urb). For reads, use
    // usb_malloc_read_buffer() to get a data buffer.
    //   urb: urb to initialize
    //   ep: endpoint to use for request
    //   data: data buffer to read (IN) or write (OUT)
    //   data_len: length of data buffer in bytes
    void init(const usb_ep* ep, void* data, unsigned data_len);

    // Submit a read or write request, to complete IN THE FUTURE (future
    // future...)
    int submit();

    // Afer complete, restore to ready state.
    void reinit();

    // Test state: ready, active, or complete.
    bool is_ready() const { return state == state_t::ready; }
    bool is_active() const { return state == state_t::active; }
    bool is_complete() const { return state == state_t::complete; }

    // If active: -EINPROGRESS
    // If complete: error, or:
    //    IN ep: actual # bytes read
    int get_result() const { return result; }

    // Submit and wait for completion.
    int transfer();

    // Repeatedly submit, poll for completion, and call a completion function.
    // Used for receiving on interrupt endpoints.
    void transfer(std::function<void(int)> on_complete);

private:
    // Interface for pollable
    virtual const char* get_poll_name() const { return "urb"; }
    virtual void poll();

    //const usb_ep *ep;
    hci_urb hci;
    state_t state;
    int result;
};

} // namespace

#endif
