#include <stdint.h>
#include <string.h>
#include <assert.h>
#include "util/debug.h"
#include "util/error.h"
#include "urb.h"

namespace usb {

#define DBG_LVL 1
#define DBG_TAG "USB_URB"

urb::urb()
{
    state = state_t::undef;
    result = -ENOTCONN;
}

urb::~urb()
{
    if (state == state_t::active) {
        DBG_PRINTF(2, "Aborting active urb %p\n", this);
        hci_abort_urb(&hci);

        pollable_remove(this);
    }
}

void urb::init(const usb_ep* ep, void* data, unsigned data_len)
{
    assert(!is_active());
    state = state_t::ready;

    memset(&hci, 0, sizeof(hci));
    hci.ep = ep;
    hci.buf = data;
    hci.buf_len = data_len;

    result = -ENOTCONN;
}

int urb::submit()
{
    DBG_PRINTF(2, "%p\n", this);
    if (!is_ready())
        return -EAGAIN;

    int ret = hci_submit_urb(&hci);
    if (ret == 0) {
        state = state_t::active;
        result = -EINPROGRESS;
        pollable_add(this);
    }
    return ret;
}

void urb::reinit()
{
    DBG_PRINTF(3, "%p\n", this);
    assert(is_ready() || is_complete());
    state = state_t::ready;
}

void urb::poll()
{
    DBG_PRINTF(3, "%p\n", this);
    if (!is_active())
        return;
    result = hci_complete_urb(&hci);
    if (result != -EINPROGRESS) {
        state = state_t::complete;
        pollable_remove(this);
    }
}

int urb::transfer()
{
    DBG_PRINTF(2, "%p\n", this);
    if (!is_ready())
        return -EAGAIN;

    int ret = hci_submit_urb(&hci);
    if (ret < 0)
        return ret;

    while ((result = hci_complete_urb(&hci)) == -EINPROGRESS)
        ;

    state = state_t::complete;
    return result;
}

void urb::transfer(std::function<void(int)> on_complete)
{
    if (is_ready()) {
        if (submit() < 0)
            return;
    }
    if (!is_complete())
        return;
    int ret = get_result();
    if (ret < 0) {
        // TODO: handle errors?
    } else {
        on_complete(ret);
    }
    reinit();
}

} // namespace
