#ifndef __USB__DEVICE_H
#define __USB__DEVICE_H

#include <string>
#include <stdint.h>
#include "ch9.h"
#include "usb.h"

namespace usb {

namespace hub {
class port;
};

class usb_manager;

class device
{
public:
    device(usb_manager& mgr, hub::port* port);
    device(const device& dev);
    virtual ~device() {}

    // Set the device address
    // Get the device and configuration descriptors
    // Create endpoints
    int init();

    // Print the device vitals
    const std::string& get_name() const { return name; }
    void identify(int dbg_print);

    // Destroy endpoints
    void destroy();

    uint8_t get_dev_addr() const { return dev_addr; }

    // Get the given endpoint
    usb_ep* get_ep(uint8_t addr);               // UEDEA

    // Initialize the device in a class-specific manner. Implemented by derived
    // classes.
    virtual int probe();

    // Device was added
    virtual void on_probed();
    // Device was removed
    virtual void on_removed();

    // Get vitals
    const usb_dev_descriptor& get_dev_desc() const { return dev_desc; }
    const std::string& get_manufacturer() const { return manufacturer; }
    const std::string& get_product() const { return product; }

    // Get string descriptor
    int get_wstring(uint8_t idx, std::wstring& buf);
    int get_string(uint8_t idx, std::string& buf);

    // Get interface descriptor
    const usb_intf_descriptor& get_intf_desc() const { return *intf_desc; }

    // Find descriptor by type
    int find_descriptor(uint8_t bDescriptorType, unsigned index,
                        const usb_descriptor **result);

    // Set the device configuration
    int set_configuration();

    // Get the USB port
    hub::port* get_port() const { return port; }

protected:
    static constexpr size_t max_num_ep = 2;

    const char* get_tag() const { return tag.c_str(); }
    void update_nametag();

    int create_endpoint(const usb_ep_descriptor *desc, usb_ep** ep);

    int ep0_read(const usb_dev_request *req, void *data)
    {
        return usb_control_read(ep0in, req, data);
    }
    int ep0_write(const usb_dev_request *req, const void *data)
    {
        return usb_control_write(ep0out, req, data);
    }

    usb_manager& mgr;
    hub::port* port;
    std::string name;
    std::string tag;
    uint8_t dev_addr;
    std::string manufacturer, product;
    usb_ep* ep0in;
    usb_ep* ep0out;
    usb_ep* ep[max_num_ep];
    int num_ep;
    usb_dev_descriptor dev_desc;
    uint8_t cfg_desc_buf[UCD_TOTAL_LENGTH_MAX];
    usb_cfg_descriptor *cfg_desc = nullptr;
    uint16_t cfg_desc_len;
    const usb_intf_descriptor *intf_desc;
};

} // namespace usb

#endif
