#include <stdint.h>
#include <string.h>
#include "util/debug.h"
#include "util/error.h"
#include "util/dump_to_str.h"
#include "hci.h"
#include "usb.h"

namespace usb {

#define DBG_LVL 1
#define DBG_TAG "USB"

static void dump_mem(const char *msg, const void *data, unsigned data_len)
{
    DBG_PRINTF_NOFUNC(12, "%s\n", msg);
    for (auto line : dump_mem_to_str(data, data_len))
        DBG_PRINTF_NOFUNC(12, "%s\n", line.c_str());
}

void *usb_malloc_read_buffer(size_t size)
{
    return hci_malloc_read_buffer(size);
}

void usb_free_read_buffer(void *dma_buf)
{
    return hci_free_read_buffer(dma_buf);
}

int usb_control_read(const usb_ep *ep, const usb_dev_request *req, void *data)
{
    DBG_PRINTF(2, "ep#%u %s\n", (unsigned)ep->bEndpointNumber,
               ep->out ? "out" : "in");
    uint16_t data_len = req->wLength;
    void *read_buf = NULL;

    if (ep->bmAttributes != UEDA_CONTROL)
        return -EINVAL;

    if (data) {
        read_buf = usb_malloc_read_buffer(data_len);
        if (read_buf == NULL)
            return -ENOMEM;
    }

    dump_mem("usb_control_read: req", req, sizeof(*req));

    int ret = hci_ep_control_read(ep, req, read_buf);
    if (ret < 0)
        goto read_buf_allocd;
    if (ret > 0) {
        memcpy(data, read_buf, ret);
        dump_mem("usb_control_read: data", data, ret);
    }

 read_buf_allocd:
    usb_free_read_buffer(read_buf);
    return ret;
}

int usb_control_write(const usb_ep *ep, const usb_dev_request *req,
                      const void *data)
{
    DBG_PRINTF(2, "ep#%u %s\n", (unsigned)ep->bEndpointNumber,
               ep->out ? "out" : "in");
    uint16_t data_len = req->wLength;

    if (ep->bmAttributes != UEDA_CONTROL)
        return -EINVAL;

    dump_mem("usb_control_write: req", req, sizeof(*req));
    if (data && data_len)
        dump_mem("usb_control_write: data", data, data_len);

    return hci_ep_control_write(ep, req, data);
}

} // namespace usb
