/*
 * USB hub base class
 * From usb_20.pdf Chapter 11
 */

#ifndef USB__HUB__BASE_H
#define USB__HUB__BASE_H

#include <string>
#include "util/pollable.h"

namespace usb {

class usb_manager;

namespace hub {

class port;

class base
{
public:
    /* Hub bits in status.wStatus, wChange (C_*) */
    enum {
        HS_LOCAL_POWER          = 1<<0,
        HS_OVER_CURRENT         = 1<<1,
    };

    /* Port bits in status.wPortStatus, wPortChange (C_*) */
    enum {
        PS_CONNECTION           = 1<<0,
        PS_ENABLE               = 1<<1,
        PS_SUSPEND              = 1<<2,
        PS_OVER_CURRENT         = 1<<3,
        PS_RESET                = 1<<4,
        PS_POWER                = 1<<8,
        PS_LOW_SPEED            = 1<<9,
        PS_HIGH_SPEED           = 1<<10,
        PS_TEST                 = 1<<11,
        PS_INDICATOR            = 1<<12,
    };

    /* Selectors for CLEAR_FEATURE, SET_FEATURE */
    enum {
        C_HUB_LOCAL_POWER           = 0,
        C_HUB_OVER_CURRENT          = 1,
    };

    enum {
        PORT_CONNECTION             = 0,
        PORT_ENABLE                 = 1,
        PORT_SUSPEND                = 2,
        PORT_OVER_CURRENT           = 3,
        PORT_RESET                  = 4,
        PORT_POWER                  = 8,
        PORT_LOW_SPEED              = 9,
        PORT_HIGH_SPEED             = 10,
        C_PORT_CONNECTION           = 16,
        C_PORT_ENABLE               = 17,       // PORT_ENABLE 1->0
        C_PORT_SUSPEND              = 18,       // PORT_SUSPEND 1->0
        C_PORT_OVER_CURRENT         = 19,
        C_PORT_RESET                = 20,       // PORT_RESET 1->0
        PORT_TEST                   = 21,
        PORT_INDICATOR              = 22,
    };

    /* Port / Hub Status */
    struct status {
        uint16_t wStatus;
        uint16_t wChange;
    } __attribute__ ((packed));

    base(usb_manager& mgr);
    virtual ~base();

    virtual uint8_t get_hub_addr() const = 0;
    virtual int init();

    bool get_hub_status_bit(int bit);                // bit = HS_*
    bool get_port_status_bit(uint8_t port, int bit); // bit = PS_*

    bool is_port_error();

protected:
    friend class port;

    struct port_info_t {
        class port* port;
        uint16_t status;
    };

    const char* get_tag() const { return tag.c_str(); }
    virtual void update_tag() = 0;
    virtual const char* get_name() = 0;

    virtual int set_port_feature(uint8_t port, uint8_t feature,
                                 uint8_t sel=0) = 0;
    virtual int clear_port_feature(uint8_t port, uint8_t feature,
                                   uint8_t sel=0) = 0;
    virtual int set_hub_feature(uint8_t feature) = 0;
    virtual int clear_hub_feature(uint8_t feature) = 0;

    virtual int get_hub_status(struct status& s) = 0;
    virtual int get_port_status(uint8_t port, struct status& s) = 0;

    int on_hub_status_change();
    int on_port_status_change(uint8_t port);

    usb_manager& mgr;
    std::string tag;
    port_info_t* ports;
    int num_ports;
    uint16_t hub_status;
};

const char* hub_feature_name(uint8_t e);
const char* port_feature_name(uint8_t e);

} // namespace hub
} // namespace usb

#endif
