/*
 * USB hub requests and related items
 * From usb_20.pdf Chapter 11
 */

#ifndef USB__HUB__DEVICE_H
#define USB__HUB__DEVICE_H

#include <memory>
#include "../device.h"
#include "../urb.h"
#include "../urb_buffer.h"
#include "util/pollable.h"
#include "base.h"

namespace usb {
namespace hub {

class device : public usb::device,
               private base,
               private pollable
{
public:
    device(const usb::device& dev);
    virtual ~device();

    // Test if the device is handled by this class.
    static bool match(usb::device& dev);

    virtual int probe();
    virtual void on_removed();

    virtual uint8_t get_hub_addr() const;

private:
    int get_and_parse_hub_descriptor();
    void enable_interrupts();

    void on_status_change(const uint8_t* bm);

    int set_clear_feature(bool set, uint8_t idx, uint8_t feature,
                          uint8_t sel=0);
    int get_status(uint8_t idx, struct status& s);

    // Interface for base
    virtual void update_tag();
    virtual const char* get_name();

    virtual int set_port_feature(uint8_t port, uint8_t feature,
                                 uint8_t sel=0);
    virtual int clear_port_feature(uint8_t port, uint8_t feature,
                                   uint8_t sel=0);
    virtual int set_hub_feature(uint8_t feature);
    virtual int clear_hub_feature(uint8_t feature);

    virtual int get_hub_status(struct status& s);
    virtual int get_port_status(uint8_t port, struct status& s);

    std::shared_ptr<usb::urb_buffer> intr_buf;
    urb intr_urb;

private:
    // Interface for pollable
    virtual const char* get_poll_name() const { return "hub::device"; }
    virtual void poll();

    std::string name;
};

} // namespace hub
} // namespace usb

#endif
