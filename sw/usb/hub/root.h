/*
 * USB controller root hub class
 */

#ifndef USB__HUB__ROOT_H
#define USB__HUB__ROOT_H

#include "base.h"
#include "util/pollable.h"

namespace usb {

class usb_manager;

namespace hub {

class root : public base,
             private pollable
{
public:
    root(usb_manager& mgr);
    virtual int init();

    virtual uint8_t get_hub_addr() const;

private:    
    // Interface for base
    virtual void update_tag();
    virtual const char* get_name();

    virtual int set_port_feature(uint8_t port, uint8_t feature,
                                 uint8_t sel=0);
    virtual int clear_port_feature(uint8_t port, uint8_t feature,
                                   uint8_t sel=0);
    virtual int set_hub_feature(uint8_t feature);
    virtual int clear_hub_feature(uint8_t feature);

    virtual int get_hub_status(struct status& s);
    virtual int get_port_status(uint8_t port, struct status& s);

    // Interface for pollable
    virtual const char* get_poll_name() const { return "hub::root"; }
    virtual void poll();
};

} // namespace hub
} // namespace usb

#endif
