/*
 * USB hub port class
 */

#ifndef USB__HUB__PORT_H
#define USB__HUB__PORT_H

#include "util/pollable.h"
#include "util/timer.h"
#include "../device.h"

namespace usb {
namespace hub {

class base;

class port : private pollable
{
public:
    enum state_t { unconnected, resetting, connecting,
                   connected, error };

    port(base& hub, int idx);
    virtual ~port();

    uint8_t get_port_index() const { return (uint8_t)idx; }
    uint8_t get_hub_addr() const;
    const std::string& get_name() const { return name_; }

    int power();
    bool is_connected();
    state_t get_state() { return state; }
    uint8_t get_eps() { return eps; }

    bool get_feature(uint8_t feature);
    int set_feature(uint8_t feature, uint8_t sel=0);
    int clear_feature(uint8_t feature, uint8_t sel=0);

    int on_feature_change(uint8_t feature);

private:
    const char* get_tag() const { return tag.c_str(); }
    void update_tag();

    void set_state(state_t);
    int reset_start();
    int reset_complete();
    int probe();
    void deprobe();

    // interface for pollable
    virtual const char* get_poll_name() const { return "usb::hub::port"; }
    void poll();

    std::string tag;
    std::string name_;
    base& hub;
    int idx;
    state_t state;
    uint8_t eps;
    timer treset;
    device* dev = nullptr;
};

const char* name(port::state_t);

} // namespace hub
} // namespace usb

#endif
