#include <string>
#include "util/timer.h"
#include "util/debug.h"
#include "util/error.h"
#include "util/string.h"
#include "../usb_manager.h"
#include "base.h"
#include "port.h"

namespace usb {
namespace hub {

#define DBG_LVL 1
#define DBG_TAG get_tag()

void port::update_tag()
{
    name_ = string_printf("%s.%u", hub.get_name(), idx);
    tag = "USB_HUB_PORT-";
    tag += name_;
}

port::port(base& hub, int idx)
    : hub(hub), idx(idx)
{
    update_tag();
    state = unconnected;
    eps = EPS_NC;
    pollable_add(this);
}

port::~port()
{
    deprobe();
    pollable_remove(this);
}

uint8_t port::get_hub_addr() const
{
    return hub.get_hub_addr();
}

int port::power()
{
    return set_feature(base::PORT_POWER);
}

bool port::is_connected()
{
    return get_feature(base::PORT_CONNECTION);
}

bool port::get_feature(uint8_t feature)
{
    return hub.get_port_status_bit(idx, feature);
}

int port::set_feature(uint8_t feature, uint8_t sel)
{
    return hub.set_port_feature(idx, feature, sel);
}

int port::clear_feature(uint8_t feature, uint8_t sel)
{
    return hub.clear_port_feature(idx, feature, sel);
}

int port::on_feature_change(uint8_t feature)
{
    switch (feature) {
    case base::C_PORT_RESET:
        if (get_state() == resetting)
            set_state(connecting);
        break;
    default:
        break;
    }
    return clear_feature(feature);
}

void port::poll()
{
    switch(state)
    {
    case unconnected:
        if (!is_connected())
            break;
        if (reset_start() < 0)
        {
            set_state(error);
            break;
        }
        set_state(resetting);
    case resetting:
        if (treset.expired()) {
            DBG_PRINTF(1, "Reset timeout\n");
            set_state(error);
        }
        break;
    case connecting:
        if (reset_complete() < 0
            || probe() < 0)
        {
            set_state(error);
            break;
        }
        dev->on_probed();
        set_state(connected);
    case connected:
        if (is_connected())
            break;
        deprobe();
        set_state(unconnected);
        break;
    case error:
        if (is_connected())
            break;
        deprobe();
        set_state(unconnected);
        break;
    }
}

void port::set_state(state_t s)
{
    DBG_PRINTF(2, "%s -> %s\n", name(state), name(s));
    state = s;
}

int port::reset_start()
{
    DBG_PRINTF(2, "\n");
    EC(set_feature(base::PORT_RESET));
    treset.start(1000 * timer::ms);             // HACK: should be 100 ms
    return 0;
}

int port::reset_complete()
{
    if (!get_feature(base::PORT_ENABLE)) {
        DBG_PRINTF(1, "Following port reset, port not enabled\n");
        return -ENOTCONN;
    }
    DBG_PRINTF(2, "USB reset successful\n");

    timer::sleep(10 * timer::ms);               // Reset recovery time

    // Get port speed
    bool ls = get_feature(base::PORT_LOW_SPEED);
    bool hs = get_feature(base::PORT_HIGH_SPEED);
    const char *speed = "?";
    if      (!ls && !hs) { speed = "FS"; eps = EPS_FS; }
    else if ( ls && !hs) { speed = "LS"; eps = EPS_LS; }
    else if (!ls &&  hs) { speed = "HS"; eps = EPS_HS; }
    DBG_PRINTF(1, "Port reset to %s\n", speed);

    return 0;
}

int port::probe()
{
	int ret;

    dev = new usb::device(hub.mgr, this);
    if ((ret = dev->init()) < 0)
        goto dev_new;
    dev->identify(DBG_TEST_LEVEL(2));
    dev = usb_manager::find_class(dev);
    if ((ret = dev->probe()) < 0)
        goto dev_init;

    return 0;

 dev_init:
    dev->destroy();
 dev_new:
    delete dev;
    dev = nullptr;

    DBG_PRINTF(1, "%s\n", strerror(-ret));
    return ret;
}

void port::deprobe()
{
    if (dev) {
        dev->on_removed();
        dev->destroy();
        delete dev;
        dev = nullptr;
    }
}

const char* name(port::state_t e)
{
    switch (e)
    {
    case port::unconnected: return "unconnected";
    case port::resetting: return "resetting";
    case port::connecting: return "connecting";
    case port::connected: return "connected";
    case port::error: return "error";
    default: return "???";
    }
}

} // namespace hub
} // namespace usb
