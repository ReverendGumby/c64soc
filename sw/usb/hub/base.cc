/*
 * USB hub base class
 */

#include "util/debug.h"
#include "util/error.h"
#include "port.h"
#include "base.h"

namespace usb {
namespace hub {

#define DBG_LVL 2
#define DBG_TAG get_tag()

base::base(usb_manager& mgr)
    : mgr(mgr),
      ports(nullptr),
      num_ports(0)
{
    hub_status = 0;
}

base::~base()
{
    if (ports) {
        for (int i = 0; i < num_ports; i++) {
            auto& p = ports[i];
            delete p.port;
        }
        delete [] ports;
    }
}

int base::init()
{
    ports = new port_info_t[num_ports];
    for (int i = 0; i < num_ports; i++) {
        auto& p = ports[i];
        p.port = new port(*this, i);
        p.status = 0;
        p.port->power();
    }

    return 0;
}

bool base::get_hub_status_bit(int bit)
{
    return hub_status & (1 << bit);
}

bool base::get_port_status_bit(uint8_t port, int bit)
{
    if (!(ports && port < num_ports))
        return false;
    return ports[port].status & (1 << bit);
}

bool base::is_port_error()
{
    for (int i = 0; i < num_ports; i++) {
        auto* p = ports[i].port;
        if (p->get_state() == port::error)
            return true;
    }
    return false;
}

int base::on_hub_status_change()
{
    DBG_PRINTF(3, "\n");

    struct status s;
    EC(get_hub_status(s));

    if (s.wChange & HS_LOCAL_POWER) {
        EC(clear_hub_feature(C_HUB_LOCAL_POWER));
    }
    if (s.wChange & HS_OVER_CURRENT) {
        EC(clear_hub_feature(C_HUB_OVER_CURRENT));
    }

    hub_status = s.wStatus;

    return 0;
}

int base::on_port_status_change(uint8_t idx)
{
    int ret = 0;
    DBG_PRINTF(3, "port %u\n", idx);

    auto& pi = ports[idx];

    struct status s;
    EC(get_port_status(idx, s));

    if (s.wChange & PS_CONNECTION) {
        ret = pi.port->on_feature_change(C_PORT_CONNECTION);
    }
    if (s.wChange & PS_ENABLE) {
        ret = pi.port->on_feature_change(C_PORT_ENABLE);
    }
    if (s.wChange & PS_SUSPEND) {
        ret = pi.port->on_feature_change(C_PORT_SUSPEND);
    }
    if (s.wChange & PS_OVER_CURRENT) {
        ret = pi.port->on_feature_change(C_PORT_OVER_CURRENT);
    }
    if (s.wChange & PS_RESET) {
        ret = pi.port->on_feature_change(C_PORT_RESET);
    }

    ports[idx].status = s.wStatus;

    return ret;
}

const char* hub_feature_name(uint8_t e)
{
    switch (e) {
    case base::C_HUB_LOCAL_POWER: return "C_HUB_LOCAL_POWER";
    case base::C_HUB_OVER_CURRENT: return "C_HUB_OVER_CURRENT";
    default: break;
    }
    return "?";
}

const char* port_feature_name(uint8_t e)
{
    switch (e) {
    case base::PORT_CONNECTION: return "PORT_CONNECTION";
    case base::PORT_ENABLE: return "PORT_ENABLE";
    case base::PORT_SUSPEND: return "PORT_SUSPEND";
    case base::PORT_OVER_CURRENT: return "PORT_OVER_CURRENT";
    case base::PORT_RESET: return "PORT_RESET";
    case base::PORT_POWER: return "PORT_POWER";
    case base::PORT_LOW_SPEED: return "PORT_LOW_SPEED";
    case base::PORT_HIGH_SPEED: return "PORT_HIGH_SPEED";
    case base::C_PORT_CONNECTION: return "C_PORT_CONNECTION";
    case base::C_PORT_ENABLE: return "C_PORT_ENABLE";
    case base::C_PORT_SUSPEND: return "C_PORT_SUSPEND";
    case base::C_PORT_OVER_CURRENT: return "C_PORT_OVER_CURRENT";
    case base::C_PORT_RESET: return "C_PORT_RESET";
    case base::PORT_TEST: return "PORT_TEST";
    case base::PORT_INDICATOR: return "PORT_INDICATOR";
    default: break;
    }
    return "?";
}

} // namespace hub
} // namespace usb
