/*
 * USB hub base class
 */

#include <assert.h>
#include "util/debug.h"
#include "util/error.h"
#include "../hci.h"
#include "root.h"

namespace usb {
namespace hub {

#define DBG_LVL 1
#define DBG_TAG "USB_HUB_ROOT"

void root::update_tag()
{
}

const char* root::get_name()
{
    return "root";
}

root::root(usb_manager& mgr)
    : base(mgr)
{
    num_ports = 1;
    base::tag = "USB_HUB_ROOT";
}

int root::init()
{
    DBG_PRINTF(2, "\n");
    EC(hci_init());
    EC(base::init());
    pollable_add(this);
    return 0;
}

uint8_t root::get_hub_addr() const
{
    return 0;                                   // invalid address
}

int root::set_port_feature(uint8_t port, uint8_t feature, uint8_t)
{
    int ret = 0;
    assert(port == 0);

    switch (feature) {
    case PORT_POWER:
        hci_set_port_power(true);
        break;
    case PORT_RESET:
        hci_reset_port();
        break;
    default:
        break;
    }
    return ret;
}

int root::clear_port_feature(uint8_t port, uint8_t feature, uint8_t)
{
    int ret = 0;
    assert(port == 0);
    struct hci_status hs = {};

    switch (feature) {
    case PORT_POWER:
        hci_set_port_power(false);
        break;
    case C_PORT_CONNECTION:
        hs.CSC = true;
        hci_clear_status(hs);
        break;
    case C_PORT_ENABLE:
        hs.PEC = true;
        hci_clear_status(hs);
        break;
    case C_PORT_OVER_CURRENT:
        hs.OCC = true;
        hci_clear_status(hs);
        break;
    case C_PORT_RESET:
        hs.PRC = true;
        hci_clear_status(hs);
        break;
    default:
        break;
    }
    return ret;
}

int root::set_hub_feature(uint8_t)
{
    // Ignore this.
    return 0;
}

int root::clear_hub_feature(uint8_t)
{
    // Ignore this.
    return 0;
}

int root::get_hub_status(struct status& s)
{
    s.wStatus = s.wChange = 0;
    return 0;
}

int root::get_port_status(uint8_t port, struct status& s)
{
    assert(port == 0);
    hci_status hs = hci_get_status();
    static hci_status lhs;
    s.wStatus = s.wChange = 0;

    s.wStatus |= hs.CCS * PS_CONNECTION;
    s.wStatus |= hs.PE  * PS_ENABLE;
    s.wStatus |= hs.OCA * PS_OVER_CURRENT;
    s.wStatus |= hs.PR  * PS_RESET;
    s.wStatus |= hs.PP  * PS_POWER;
    s.wStatus |= hs.LS  * PS_LOW_SPEED;
    s.wStatus |= hs.HS  * PS_HIGH_SPEED;

    s.wChange |= hs.CSC * PS_CONNECTION;
    s.wChange |= hs.PEC * PS_ENABLE;
    s.wChange |= hs.OCC * PS_OVER_CURRENT;
    s.wChange |= hs.PRC * PS_RESET;

    if (memcmp(&lhs, &hs, sizeof(lhs)) != 0) {
        DBG_PRINTF(5, "wStatus=0x%04x wChange=0x%04x\n", s.wStatus, s.wChange);
        lhs = hs;
    }

    return 0;
}

void root::poll()
{
    on_port_status_change(0);
}

} // namespace hub
} // namespace usb
