/*
 * USB hub device class
 */

#include <assert.h>
#include "util/string.h"
#include "util/debug.h"
#include "util/error.h"
#include "device.h"

namespace usb {
namespace hub {

/* usb_intf_descriptor.bDeviceClass */
static constexpr uint8_t uid_class = 0x09;

#define DBG_LVL 1
#define DBG_TAG "USB_HUB_DEV"

/***********************************************************************
 * Hub Requests
 */

/* usb_dev_request.bRequest */
#define HUB_UDRR_GET_STATUS     0x00
#define HUB_UDRR_CLEAR_FEATURE  0x01
#define HUB_UDRR_SET_FEATURE    0x03
#define HUB_UDRR_GET_DESCRIPTOR 0x06

/* usb_descriptor.bDescriptorType */
#define UDT_HUB         0x29

/* Hub Descriptor */
struct hub_desc {
    uint8_t bLength;
    uint8_t bDescriptorType;                    /* HUB_DESC_TYPE */
    uint8_t bNbrPorts;                          /* # downstream ports */
    uint16_t wHubCharacteristics;
    uint8_t bPwrOn2PwrGood;
    uint8_t bHubContrCurrent;
    //uint8_t DeviceRemovable[];                /* var. width: (port + 1) bits */
    //uint8_t PortPwrCtrlMask[];                /* var. width: (port) bits */
} __attribute__ ((packed));

void device::update_tag()
{
    name = string_printf("%u", dev_addr);
    base::tag = string_printf("USB_HUB_DEV-%s", get_name());
}

const char* device::get_name()
{
    return name.c_str();
}

device::device(const usb::device& dev)
    : usb::device(dev),
      base(usb::device::mgr),
      intr_buf{new usb::urb_buffer{usb::urb_buffer::read}}
{
}

device::~device()
{
}

bool device::match(usb::device& dev)
{
    auto& dd = dev.get_dev_desc();

    return (dd.bDeviceClass == uid_class);
}

uint8_t device::get_hub_addr() const
{
    return get_dev_addr();
}

int device::probe()
{
    update_tag();
    EC(get_and_parse_hub_descriptor());
    EC(base::init());
    enable_interrupts();

    return 0;
}

int device::get_and_parse_hub_descriptor()
{
    /* Get the hub descriptor. */
    struct hub_desc desc;

    usb_dev_request req;
    DBG_PRINTF(2, "GET_DESCRIPTOR(CLASS=HUB)\n");
    req.bmRequestType = UDRRT(DEVICE, CLASS, IN);
    req.bRequest = HUB_UDRR_GET_DESCRIPTOR;
    req.wValue = (UDT_HUB << 8) | 0;
    req.wIndex = 0;
    req.wLength = sizeof(hub_desc);
    EC(ep0_read(&req, &desc));

    num_ports = desc.bNbrPorts;

    return 0;
}

void device::enable_interrupts()
{
    // Receive Hub and Status Change Bitmaps on the interrupt endpoint.
    auto* ep = get_ep(UEDEA(1, IN));
    assert (ep != nullptr);
    size_t status_change_bitmap_size = ((num_ports + 1) + 7) / 8;
    intr_buf->alloc(status_change_bitmap_size);
    intr_urb.init(ep, intr_buf->ptr, intr_buf->len);

    // Initiate polling for interrupts.
    pollable_add(this);
}

void device::poll()
{
    intr_urb.transfer([this](int) { on_status_change(intr_buf->get_ptr()); });
}

void device::on_status_change(const uint8_t* bm)
{
    for (int i = 0; i < num_ports + 1; i++) {
        int off = i / 8;
        int shift = i % 8;
        if (bm[off] & (1 << shift)) {
            if (i == 0) {                       // hub status
                (void)on_hub_status_change();
            } else {                            // port status
                uint8_t port = uint8_t(i - 1);
                (void)on_port_status_change(port);
            }
        }
    }
}

void device::on_removed()
{
    DBG_PRINTF(2, "\n");

    pollable_remove(this);

    usb::device::on_removed();
}

int device::get_status(uint8_t idx, struct status& s)
{
    usb_dev_request req;
    req.bmRequestType = idx ? UDRRT(OTHER, CLASS, IN)
        : UDRRT(DEVICE, CLASS, IN);
    req.bRequest = HUB_UDRR_GET_STATUS;
    req.wValue = 0;
    req.wIndex = idx;
    req.wLength = sizeof(s);
    int ret = ep0_read(&req, &s);
    DBG_PRINTF(2, "GET_STATUS(Port=%u): wStatus=0x%04x wChange=0x%04x\n",
               idx, s.wStatus, s.wChange);
    return ret;
}

int device::get_hub_status(struct status& s)
{
    return get_status(0, s);
}

int device::get_port_status(uint8_t port, struct status& s)
{
    return get_status(port + 1, s);
}

int device::set_clear_feature(bool set, uint8_t idx, uint8_t feature,
                              uint8_t sel)
{
    DBG_PRINTF(2, "%s(Feature=%s, Port=%u, Sel=%u)\n",
               set ? "SET_FEATURE" : "CLEAR_FEATURE",
               port_feature_name(feature), idx, sel);
    usb_dev_request req;
    req.bmRequestType = idx ? UDRRT(OTHER, CLASS, OUT)
        : UDRRT(DEVICE, CLASS, OUT);
    req.bRequest = set ? HUB_UDRR_SET_FEATURE : HUB_UDRR_CLEAR_FEATURE;
    req.wValue = feature;
    req.wIndex = ((uint16_t)sel << 8) | idx;
    req.wLength = 0;
    return ep0_write(&req, NULL);
}

int device::set_port_feature(uint8_t port, uint8_t feature, uint8_t sel)
{
    return set_clear_feature(true, port + 1, feature, sel);
}

int device::clear_port_feature(uint8_t port, uint8_t feature, uint8_t sel)
{
    return set_clear_feature(false, port + 1, feature, sel);
}

int device::set_hub_feature(uint8_t feature)
{
    return set_clear_feature(true, 0, feature);
}

int device::clear_hub_feature(uint8_t feature)
{
    return set_clear_feature(false, 0, feature);
}

} // namespace hub
} // namespace usb
