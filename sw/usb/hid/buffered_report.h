/*
 * Buffered Report: Combines a buffer with the HID Report Descriptor that
 * describes it.
 */

#ifndef USB__HID__BUFFERED_REPORT_H
#define USB__HID__BUFFERED_REPORT_H

#include <memory>
#include <stdint.h>
#include "../urb_buffer.h"
#include "hrd/report.h"

namespace usb {
namespace hid {

class buffered_report : public hrd::report
{
public:
    using value_t = uint64_t;

    buffered_report() = delete;
    buffered_report(std::shared_ptr<usb::urb_buffer>);
    buffered_report(const buffered_report& rhs);

    buffered_report& operator=(const hrd::report&);

    bool vector_bounds_check(int first_bit, int bit_width) const;
    value_t get_vector(int first_bit, int bit_width) const;
    void set_vector(int first_bit, int bit_width, value_t v) const;

    buffer& get_buffer() const { return *buf; }

    void dump() const;

private:
    std::shared_ptr<usb::urb_buffer> buf;
};

} // namespace hid
} // namespace usb

#endif
