/*
 * USB HID Usages
 */

#include <sstream>
#include "util/string.h"
#include "util/debug.h"
#include "usage.h"

namespace usb {
namespace hid {

usage::usage(value_t v)
{
    page = v >> 16;
    id = v & ((1 >> 16) - 1);
}

usage::usage(page_t in_page, value_t v)
    : usage(v)
{
    if (page == undefined_page)
        page = in_page;
}

std::string usage::to_string() const
{
    if (is_vendor_page(page))
        return to_id_string();
    std::stringstream ss;
    ss << to_page_string();
    if (page != undefined_page)
        ss << ": " << to_id_string();
    return ss.str();
}

std::string usage::to_page_string() const
{
    if (page == undefined_page)
        return "(undefined)";
    if (is_vendor_page(page)) {
        std::stringstream ss;
        ss << "0x" << std::hex << (unsigned)page;
        return ss.str();
    }
    const char* name = page_name(page);
    if (name)
        return name;
    return string_printf("%u", (unsigned)page);
}

std::string usage::to_id_string() const
{
    std::stringstream ss;
    if (is_vendor_page(page)) {
        ss << "0x" << std::hex << (unsigned)value();
    } else {
        ss << (unsigned)id;
        const char* name = id_name(page, id);
        if (name)
            ss << ' ' << name;
    }
    return ss.str();
}

const char* usage::page_name(page_t page)
{
    switch ((pages)page) {
    case pages::undefined:
        return "(undefined)";
    case pages::generic_desktop_controls:
        return "Generic Desktop Controls";
    case pages::game_controls:
        return "Game Controls";
    case pages::keyboard_keypad:
        return "Keyboard / Keypad";
    case pages::leds:
        return "LEDs";
    case pages::button:
        return "Button";
    default:
        break;
    }
    return nullptr;
}

const char* usage::id_name(page_t page, id_t id)
{
    switch ((pages)page) {
    case pages::generic_desktop_controls:
        switch ((gdc_ids)id) {
        case gdc_ids::pointer:
            return "Pointer";
        case gdc_ids::mouse:
            return "Mouse";
        case gdc_ids::joystick:
            return "Joystick";
        case gdc_ids::game_pad:
            return "Game Pad";
        case gdc_ids::keyboard:
            return "Keyboard";
        case gdc_ids::start:
            return "Start";
        case gdc_ids::x:
            return "X";
        case gdc_ids::y:
            return "Y";
        case gdc_ids::z:
            return "Z";
        case gdc_ids::rx:
            return "Rx";
        case gdc_ids::ry:
            return "Ry";
        case gdc_ids::rz:
            return "Rz";
        case gdc_ids::hat_switch:
            return "Hat switch";
        case gdc_ids::select:
            return "Select";
        case gdc_ids::system_main_menu:
            return "System Main Menu";
        case gdc_ids::dpad_up:
            return "D-pad Up";
        case gdc_ids::dpad_down:
            return "D-pad Down";
        case gdc_ids::dpad_left:
            return "D-pad Left";
        case gdc_ids::dpad_right:
            return "D-pad Right";
        default:
            break;
        }
        break;
    case pages::game_controls:
        switch ((game_ids)id) {
        case game_ids::gamepad_fire:
            return "Gamepad Fire/Jump";
        default:
            break;
        }
        break;
    case pages::keyboard_keypad:
        switch ((key_ids)id) {
        case key_ids::none:
            return "Reserved (no event indicated)";
        case key_ids::error_roll_over:
            return "Keyboard ErrorRollOver";
        case key_ids::application:
            return "Keyboard Application";
        case key_ids::keypad_decimal:
            return "Keypad Decimal";
        case key_ids::lctrl:
            return "Keyboard LeftControl";
        case key_ids::lshift:
            return "Keyboard LeftShift";
        case key_ids::lalt:
            return "Keyboard LeftAlt";
        case key_ids::lgui:
            return "Keyboard Left GUI";
        case key_ids::rctrl:
            return "Keyboard RightControl";
        case key_ids::rshift:
            return "Keyboard RightShift";
        case key_ids::ralt:
            return "Keyboard RightAlt";
        case key_ids::rgui:
            return "Keyboard Right GUI";
        };
        break;
    case pages::leds:
        switch ((leds_ids)id) {
        case leds_ids::num_lock:
            return "Num Lock";
        case leds_ids::caps_lock:
            return "Caps Lock";
        case leds_ids::scroll_lock:
            return "Scroll Lock";
        case leds_ids::compose:
            return "Compose";
        case leds_ids::kana:
            return "Kana";
        default:
            break;
        }
        break;
    case pages::button:
        switch ((button_ids)id) {
        case button_ids::none:
            return "None";
        case button_ids::b1:
            return "Button 1 (primary)";
        case button_ids::b2:
            return "Button 2 (secondary)";
        case button_ids::b3:
            return "Button 3 (tertiary)";
        case button_ids::b4:
            return "Button 4";
        default:
            break;
        }
        break;
    default:
        break;
    }
    return nullptr;
}

} // namespace hid
} // namespace usb
