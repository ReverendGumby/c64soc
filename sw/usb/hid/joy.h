/*
 * USB HID Joystick / Gamepad sub-device
 */

#ifndef USB__HID__JOY_H
#define USB__HID__JOY_H

#include <string>
#include <stdint.h>
#include "../urb.h"
#include "usage_set.h"
#include "field.h"
#include "device.h"
#include "ui/joy.h"

namespace usb {
namespace hid {

class field_getter;

class joy : public sub_device,
            public ui::joy
{
public:
    static const usage_set& app_usages();

    joy(device&, const hrd::report_set&);
    virtual ~joy() {}

    std::string get_name();
    virtual const char* get_name_type();

    // Interface for sub_device
    virtual int probe();
    virtual void on_removed();
    virtual void translate_report(void);

    // Interface for ui::joy
    virtual void set_port(int);
    bool is_idle() override;

private:
    // Hat switch translation table entry
    struct hat_sw_out_t { int up, dn, lt, rt; };

    struct axis {
        variable_field f;
        int off;                                // logical val. at center
        int lim;                                // stick maximum extent

        bool probe(field_getter&, const usage&);
        int get();
        int to_digital(int analog);
    };

    bool probe_hat_switch();
    bool probe_gamepad();
    bool probe_analog();

    void translate_report_analog(report&);
    void translate_report_buttons(report&);
    int analog_to_digital(int v);

    void event(const report&);

    // Analog stick
    bool has_analog;
    axis ax, ay;                                // usage::gdc_ids::x .. y

    // Hat switch
    variable_field hat_sw;                      // gdc_ids::hat_switch
    const hat_sw_out_t* hat_sw_tt;

    // Buttons
    variable_field btn_du;
    variable_field btn_dd;
    variable_field btn_dl;
    variable_field btn_dr;
    variable_field btn_f1;
    variable_field btn_f2;
    variable_field btn_f3;
    variable_field btn_f4;
    variable_field btn_start;
    variable_field btn_select;
    variable_field btn_home;                    // gdc_ids::system_main_menu
    variable_field btn_tl;                      // trigger left
    variable_field btn_tr;                      // trigger right
    variable_field btn_bl;                      // button left
    variable_field btn_br;                      // button right
    variable_field btn_al;                      // left analog stick
    variable_field btn_ar;                      // right analog stick

    report last_report;
};

// constexpr operator bool(joy::leds a)
// {
//     return static_cast<int>(a) != 0;
// }

} // namespace hid
} // namespace usb

#endif
