/*
 * USB HID Keyboard sub-device
 */

#include <array>
#include <algorithm>
#include <stdio.h>
#include "util/debug.h"
#include "util/error.h"
#include "usage_set.h"
#include "field_getter.h"
#include "kbd.h"

namespace usb {
namespace hid {

#define DBG_LVL 1
#define DBG_TAG "HID_KBD"

const usage_set& kbd::app_usages()
{
    static const usage_set us {usage{usage::gdc_ids::keyboard}};
    return us;
}

kbd::kbd(device& dev, const hrd::report_set& subreports)
    : sub_device(dev, subreports),
      last_report{}
{
}

int kbd::probe()
{
    DBG_PRINTF(2, "Device appears to be a HID keyboard\n");

    if (!in_report.empty()) {
        field_getter fg{in_report};
        key_lctrl = fg.get_variable(usage{usage::key_ids::lctrl});
        key_lshift = fg.get_variable(usage{usage::key_ids::lshift});
        key_lalt = fg.get_variable(usage{usage::key_ids::lalt});
        key_lwin = fg.get_variable(usage{usage::key_ids::lgui});
        key_rctrl = fg.get_variable(usage{usage::key_ids::rctrl});
        key_rshift = fg.get_variable(usage{usage::key_ids::rshift});
        key_ralt = fg.get_variable(usage{usage::key_ids::ralt});
        key_rwin = fg.get_variable(usage{usage::key_ids::rgui});

#if 0
        using item_t = field_getter::item_t;
        key_array = fg.get_array(
            [](const item_t& i) {
                const auto kbd =
                    (item_t::usage_page_t)usage::pages::keyboard_keypad;
                return i.usage_min_id == 0
                    && i.usage_page == kbd
                    && i.logical_max >= 101;
            });
#else
        key_array = fg.get_array(usage{usage::key_ids::none});
#endif
        if (!key_array.valid()) {
            DBG_PRINTF(1, "No key array found!\n");
            return -ENODEV;
        }
    } else {
        DBG_PRINTF(1, "No input report!\n");
        return -ENODEV;
    }

    if (!out_report.empty()) {
        field_getter fg{out_report};
        leds_num_lock = fg.get_variable(usage{usage::leds_ids::num_lock});
        leds_caps_lock = fg.get_variable(usage{usage::leds_ids::caps_lock});
        leds_scroll_lock = fg.get_variable(usage{usage::leds_ids::scroll_lock});
    } else {
        DBG_PRINTF(1, "No output report. LEDs won't work.\n");
    }

    leds_num_lock.set(1);
    EC(send_output_report());

    DBG_PRINTF(1, "HID keyboard initialized\n");
    return 0;
}

void kbd::toggle_leds(leds toggle)
{
    if (toggle & leds::num_lock)
        leds_num_lock.set(!leds_num_lock.get());
    if (toggle & leds::caps_lock)
        leds_caps_lock.set(!leds_caps_lock.get());
    if (toggle & leds::scroll_lock)
        leds_scroll_lock.set(!leds_scroll_lock.get());
    send_output_report();
}

int kbd::send_output_report()
{
    return dev.send_output_report();
}

void kbd::translate_code(kbd::kbcode code, const kbd::mods& mods)
{
#define MIDCODES    "\n\033\177\t "             /* 40 - 44 */
    const static char *unshift_table =
        "abcdefghijklmnopqrstuvwxyz1234567890"  /* 4 - 39 */
        MIDCODES "-=[]\\ ;'`,./";               /* 40 - 56 */
    const static char *shift_table =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()"  /* 4 - 39 */
        MIDCODES "_+{}| :\"~<>?";               /* 40 - 56 */

    if (code < kbcode::A)
        return;

    char c = 0;
    auto shift = mods.shift;
    auto caps = mods.caps_lock;
    if (code >= kbcode::A && code <= kbcode::SLA) {
        c = shift ? shift_table[code - kbcode::A] : unshift_table[code - kbcode::A];
        if (caps && code <= kbcode::Z)
            c = shift ? unshift_table[code - kbcode::A] : shift_table[code - kbcode::A];
    }

    keypress kp;
    kp.mods = mods;
    kp.code = code;
    kp.ch = c;
    kp.down = true;
    keypresses.push_back(kp);
    speak(&listener::event, kp);
}

bool kbd::check_lock_code(kbd::kbcode code)
{
    using namespace ui;

    if (code == kbcode::NUM)
    {
        toggle_leds(leds::num_lock);
        return true;
    }
    if (code == kbcode::CAP)
    {
        toggle_leds(leds::caps_lock);
        return true;
    }
    if (code == kbcode::SCL)
    {
        toggle_leds(leds::scroll_lock);
        return true;
    }
    return false;
}

void kbd::translate_report(void)
{
    using namespace ui;

    report report = {};
    mods& mods = report.mods;

    if ((usage::key_ids)key_array.get(0) == usage::key_ids::error_roll_over)
        return;

    // Translate modifier keys
    if (key_lctrl.get())  mods.ctrl  |= K_LEFT;
    if (key_lshift.get()) mods.shift |= K_LEFT;
    if (key_lalt.get())   mods.alt   |= K_LEFT;
    if (key_lwin.get())   mods.win   |= K_LEFT;
    if (key_rctrl.get())  mods.ctrl  |= K_RIGHT;
    if (key_rshift.get()) mods.shift |= K_RIGHT;
    if (key_ralt.get())   mods.alt   |= K_RIGHT;
    if (key_rwin.get())   mods.win   |= K_RIGHT;

    // Translate normal keys
    int i;
    int num = std::min(key_array.count, (int)report::max_codes);
    for (i = 0; i < num; i++) {
        kbcode c = (kbcode)key_array.get(i);
        if (c == kbcode::RSV)
        	break;
        report.codes[i] = c;
    }
    report.num_codes = i;

    // Include current lock key states
    mods.caps_lock = leds_caps_lock.get();
    mods.num_lock = leds_num_lock.get();
    mods.scroll_lock = leds_scroll_lock.get();

    // In case someone calls is_idle() on events..
    auto old_report = last_report;
    last_report = report;

    // Send events
    handle_released_keys(report);
    for (i = 0; i < report.num_codes; i++) {
    	auto c = report.codes[i];
        if (old_report.has_code(c))            /* key still pressed */
            continue;
        if (!check_lock_code(c))
            translate_code(c, mods);
    }
    speak(&listener::event, report);
}

bool kbd::is_idle()
{
    return last_report.is_idle();
}

void kbd::handle_released_keys(const report& report)
{
    auto& v = keypresses;
    auto ri = std::remove_if(v.begin(), v.end(),
                             [&report](const keypress& x)
                             { return !report.has_code(x.code); });
    for (auto i = ri; i != v.end(); i++) {
        auto& kp = *i;
        kp.down = false;
        speak(&listener::event, kp);
    }
    v.erase(ri, v.end());
}

void kbd::on_removed(void)
{
    DBG_PRINTF(2, "\n");
}

void kbd::blink(void)
{
    toggle_leds(leds::num_lock | leds::caps_lock | leds::scroll_lock);
}

} // namespace hid
} // namespace usb
