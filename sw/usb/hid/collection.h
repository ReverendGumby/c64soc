/*
 * USB HID Collection
 */

#ifndef USB__HID__COLLECTION_H
#define USB__HID__COLLECTION_H

/*
 * Collections identify a relationship between two or more Main items, or other
 * Collections. A collection has a type (e.g., application) and a usage (e.g.,
 * keyboard).
 */

#include <string>
#include <memory>
#include "hrd/item_spec.h"
#include "usage.h"

namespace usb {
namespace hid {

struct collection {
    using type_t = hrd::collection::type_t;
    using base_t = std::shared_ptr<collection>;

    type_t type;
    struct usage usage;

    /* Nested collections are represented by a reverse linked list. All items
       inside a given collection have a pointer to that collection. */
    base_t base;

    collection();
    explicit collection(type_t, struct usage, base_t);

    std::string to_string() const;
};

const char* name(collection::type_t e);

} // namespace hid
} // namespace usb

#endif
