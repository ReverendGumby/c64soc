/*
 * XBox 360 wired (USB) controller
 * Microsoft-specific game pad device
 */

#ifndef USB__HID__XBOX360_H
#define USB__HID__XBOX360_H

#include <stdint.h>
#include "../device.h"
#include "device.h"
#include "joy.h"
#include "ui/joy.h"

namespace usb {
namespace hid {

class xbox360 : public hid::device
{
public:
    xbox360(const usb::device& dev);

    // Test if the device is handled by this class.
    static bool match(usb::device& dev);

private:
    class xbox360_joy : public joy
    {
    public:
        xbox360_joy(device& dev, const hrd::report_set& subreports);
        virtual ~xbox360_joy();

        virtual const char* get_name_type();

        // Interface for ui::joy
        virtual void set_port(int mode);
    };
    friend xbox360_joy;

    virtual int find_and_parse_report_descriptor();
    virtual int prepare_device();
    virtual void create_subdevices();

    void define_in_report();
    void define_out_report();

    int set_leds(uint8_t);

    virtual void on_in_report(int len);
};

} // namespace hid
} // namespace usb

#endif
