/*
 * USB HID (Human Interface Devices)
 * Buffered Report
 */

#include <stdint.h>
#include "util/debug.h"
#include "util/dump_to_str.h"
#include "buffered_report.h"

namespace usb {
namespace hid {

#define DBG_LVL 1
#define DBG_TAG "USB_HID_BUFR"

buffered_report::buffered_report(std::shared_ptr<usb::urb_buffer> buf) :
    buf(buf)
{
}

buffered_report::buffered_report(const buffered_report& rhs) :
    hrd::report(rhs), buf(rhs.buf)
{
}

buffered_report& buffered_report::operator=(const hrd::report& rhs)
{
    (hrd::report&)*this = rhs;
    return *this;
}

bool buffered_report::vector_bounds_check(int b0, int w) const
{
    return b0 + w <= (int)buf->used * 8;
}

buffered_report::value_t buffered_report::get_vector(int b0, int w) const
{
    int bn = b0 + w - 1;
    if (!vector_bounds_check(b0, w)) {
        DBG_PRINTF(1, "vector [%d:%d] out of bounds\n", bn, b0);
        return 0;
    }

    DBG_PRINTF(2, "[%d:%d] => ", bn, b0);
    value_t ret = 0;
    int o = 0;
    uint8_t* p = buf->get_ptr();

    // Shift over to byte with first bit.
    for (; b0 >= 8; b0 -= 8)
        p ++;

    // Extract whole bytes.
    for (; w >= 8; p ++, o += 8, w -= 8) {
        uint16_t c = p[0] | ((uint16_t)p[1] << 8);
        value_t v = (c >> b0) & 0xFF;
        ret |= v << o;
    }

    // Extract last partial byte.
    value_t v = (p[0] >> b0) & ((1 << w) - 1);
    ret |= v << o;

    DBG_PRINTF_CONT(2, "0x%lx\n", (unsigned long)ret);
    return ret;
}

void buffered_report::set_vector(int b0, int w, value_t v) const
{
    int bn = b0 + w - 1;
    if (!vector_bounds_check(b0, w)) {
        DBG_PRINTF(1, "vector [%d:%d] out of bounds\n", bn, b0);
        return;
    }

    DBG_PRINTF(2, "[%d:%d] <= 0x%lx\n", bn, b0, (unsigned long)v);
    uint8_t* p = buf->get_ptr();

    // Shift over to byte with first bit.
    for (; b0 >= 8; b0 -= 8)
        p ++;

    // Insert first partial byte.
    if (w > 8 && b0 > 0) {
        auto c = *p;
        uint8_t m = 0xFF << b0;
        *(p++) = (c & ~m) | (uint8_t)(v << b0);
        v >>= b0;
        w -= b0;
        b0 = 0;
    }

    // Insert whole bytes.
    for (; w >= 8; w -= 8) {
        *(p++) = (uint8_t)v;
        v >>= 8;
    }

    // Insert last partial byte.
    if (w > 0) {
        auto c = *p;
        uint8_t m = ((1 << w) - 1) << b0;
        *(p++) = (c & ~m) | ((v << b0) & m);
    }
}

void buffered_report::dump() const
{
    for (auto line : dump_buf_to_str(buf->ptr, buf->used))
        DBG_PRINTF(0, "%s\n", line.c_str());
}

} // namespace hid
} // namespace usb
