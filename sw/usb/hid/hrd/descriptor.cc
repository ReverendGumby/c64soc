/*
 * USB HID Report Descriptor
 * Report Descriptor
 */

#include "descriptor.h"

namespace usb {
namespace hid {
namespace hrd {

descriptor::iterator::iterator(const descriptor& d, const uint8_t* p)
    : _d(d), _p(p), _e(item(p))
{
}

descriptor::iterator descriptor::begin() const
{
    return iterator(*this, p);
}

descriptor::iterator descriptor::end() const
{
    return iterator(*this, nullptr);
}

void descriptor::iterator::increment()
{
    _p += _e.len;
    if (_p >= _d.p + _d.len)
        _p = nullptr;
    _e = item(_p);
}

} // namespace hrd
} // namespace hid
} // namespace usb
