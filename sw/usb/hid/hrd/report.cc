/*
 * USB HID Report Descriptor
 * Report
 */

#include <sstream>
#include <assert.h>
#include "util/debug.h"
#include "../usage.h"
#include "../collection.h"
//#include "item_spec.h"
#include "report.h"

namespace usb {
namespace hid {
namespace hrd {

#define DBG_LVL 2
#define DBG_TAG "USB_HRD"

report::report()
    : is_defined(false),
      type(type_t::undefined),
      id(id_undefined),
      bit_width(0)
{
}

report::report(type_t type, id_t id)
    : is_defined(false),
      type(type),
      id(id),
      bit_width(0)
{
}

void report::add(const item_state& ist)
{
    dump();
    if (!is_defined) {
        DBG_PRINTF(5, "Adding first report\n");
        is_defined = true;
        type = ist.report_type;
        id = ist.report_id;
    } else {
        DBG_PRINTF(5, "Updating report\n");
        assert(type == ist.report_type);
        assert(id == ist.report_id);
    }

    data.push_back(ist);
    bit_width += ist.get_bit_width();
}

void report::update_and_add(item_state& ist)
{
    // Update item's bit position.
    ist.position = bit_width;

    // Add the item.
    add(ist);
}

bool report::has_collection(const hid::usage& usage) const
{
    for (auto& d : data) {
        if (d.has_collection(usage))
            return true;
    }
    return false;
}

std::string report::to_string() const
{
    std::stringstream ss;
    ss << "Report ID ";
    if (id == id_undefined)
        ss << "(undefined)";
    else
        ss << (unsigned)id;
    ss << ", Type " << name(type) << '\n';
    for (auto& d : data) {
        ss << "  Data # " << &d - &data.front() << " : "
           << d.report_count << " fields x " << d.report_size << " bits ["
           << d.get_last_bit() << ':' << d.get_first_bit() << "]. "
           << hrd::to_string(d.main_bits, type == type_t::input) << '\n';
        if (d.collection)
            ss << "    Collection: " << d.collection->to_string() << '\n';
    }
    return ss.str();
}

void report::dump() const
{
	DBG_PRINTF(4, "");
	DBG_DO(4, _dump());
}

void report::_dump() const
{
    DBG__PRINTF("%s", to_string().c_str());
}

} // namespace hrd
} // namespace hid
} // namespace usb
