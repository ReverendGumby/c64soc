/*
 * USB HID Report Descriptor
 *
 * Item State Table: Items are combined here.
 */

#ifndef USB__HID__HRD__ITEM_STATE_H
#define USB__HID__HRD__ITEM_STATE_H

#include <memory>
#include <stdint.h>
#include "../usage_set.h"

namespace usb {
namespace hid {
struct collection;
namespace hrd {

struct item_state
{
    using report_id_t = uint8_t;
    enum class report_type_t { undefined, input, output, feature };
    struct main_bits_t {                        // from Main item value
        bool constant;                          // bit 0
        bool variable;                          // bit 1
        bool relative;                          // bit 2
        bool wrap;                              // bit 3
        bool nonlinear;                         // bit 4
        bool no_preferred;                      // bit 5
        bool null_state;                        // bit 6
        bool is_volatile;                       // bit 7
        bool buffered_bytes;                    // bit 8
    };
    using usage_page_t = uint16_t;
    using usage_id_t = uint32_t;

    // From non-data-type Main items
    int depth;                                  // how many collections
    std::shared_ptr<struct collection> collection;
    // From data-type Main items
    report_id_t report_id;
    report_type_t report_type;
    int report_count;
    int report_size;
    main_bits_t main_bits;
    // From Global items
    usage_page_t usage_page;
    int logical_min;
    int logical_max;
    int physical_min;
    int physical_max;
    // From Local items
    hid::usage_set usages;

    int position;                               // bit pos. of item in report

    static constexpr report_id_t report_id_undefined = 0;

    item_state();

    const hid::usage_set& get_usages() const { return usages; }
    int get_usage_index(const hid::usage& u) const
    {
        return usages.find_usage(u);
    }
    void add_usage(const hid::usage&);

    int get_bit_width() const { return report_size * report_count; }
    int get_first_bit() const { return position; }
    int get_last_bit() const { return get_first_bit() + get_bit_width() - 1; }

    // Test for the presence of a collection with the given usage.
    bool has_collection(const hid::usage& usage) const;

    // Remove all local items, leaving only global items.
    void reset_locals();

    // Convert a logical value to a physical value.
    int logical_to_physical(int) const;
};

const char* name(item_state::report_type_t);
std::string to_string(const item_state::main_bits_t&, bool is_input);

} // namespace hrd
} // namespace hid
} // namespace usb

#endif
