/*
 * USB HID Report Descriptor
 * Report set
 */

#include <stdexcept>
#include <sstream>
#include "util/debug.h"
#include "../usage.h"
//#include "../collection.h"
//#include "item_spec.h"
#include "report_set.h"

namespace usb {
namespace hid {
namespace hrd {

#define DBG_LVL 1
#define DBG_TAG "USB_HRD"

void report_set::clear()
{
    v.clear();
}

report& report_set::get(report::type_t type, report::id_t id)
{
    for (auto& r : v) {
        if (r.type == type && r.id == id) {
            DBG_PRINTF(5, "Found report # %d: %s\n", int(&r - &v.front()),
                       r.to_string().c_str());
            return r;
        }
    }
    DBG_PRINTF(5, "not found\n");
    throw std::domain_error("report_set::get(): not found");
}

report& report_set::get_or_create(report::type_t type, report::id_t id)
{
    try {
        return get(type, id);
    }
    catch (std::domain_error&) {
        v.push_back(report {type, id});
        auto& r = v.back();
        DBG_PRINTF(5, "Created new report # %d: %s\n", int(&r - &v.front()),
                   r.to_string().c_str());
        return r;
    }
}

report& report_set::get_or_create(const report& r)
{
    return get_or_create(r.type, r.id);
}

bool report_set::has_collection(const hid::usage& usage) const
{
    for (auto& r : v)
        if (r.has_collection(usage))
            return true;
    return false;
}

report_set report_set::subset(const hid::usage& usage) const
{
    report_set ret;
    for (auto& r : v) {
        for (auto& d : r.data) {
            if (d.has_collection(usage)) {
                auto& nr = ret.get_or_create(r);
                nr.add(d);
            }
        }
    }
    return ret;
}

void report_set::dump() const
{
    if (v.size() == 0) {
        DBG_PRINTF(2, "Empty!\n");
        return;
    }
    for (unsigned i = 0; i < v.size(); i++) {
        DBG_PRINTF(2, "Report # %u: \n", i);
        DBG_DO(2, v[i]._dump());
    }
}

} // namespace hrd
} // namespace hid
} // namespace usb
