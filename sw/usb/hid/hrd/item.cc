/*
 * USB HID Report Descriptor
 * Report Item
 */

#include <assert.h>
#include "util/debug.h"
#include "../parser.h"
#include "item_spec.h"
#include "item.h"

namespace usb {
namespace hid {
namespace hrd {

#define DBG_LVL 2
#define DBG_TAG "USB_HRD"

item::item(const uint8_t* pb0)
    : pb0(pb0)
{
    if (pb0) {
        if (*pb0 == item_b0_long) {
            const item_long_t* pli = reinterpret_cast<const item_long_t*>(pb0);
            len = sizeof(*pli) + pli->bDataSize;
        } else {
            auto bSize = ((*pb0 & item_b0_size) >> item_b0_size_shift);
            len = sizeof(*pb0) + bSize;
        }
    } else
        len = 0;
}

item::type_t item::type() const
{
    return static_cast<enum type_t>((*pb0 & item_b0_type) >> item_b0_type_shift);
}

item::tag_t item::tag() const
{
    return (*pb0 & item_b0_tag) >> item_b0_tag_shift;
}

item::type_tag_t item::type_tag() const
{
    type_tag_t ret;
    ret.type = type();
    switch (ret.type) {
    case type_t::main:
        ret.main_tag = (main_tag_t)tag();
        break;
    case type_t::global:
        ret.global_tag = (global_tag_t)tag();
        break;
    case type_t::local:
        ret.local_tag = (local_tag_t)tag();
        break;
    default:
        break;
    }
    return ret;
}

item::size_t item::size() const
{
    if (*pb0 == item_b0_long) {
        const item_long_t* pli = reinterpret_cast<const item_long_t*>(pb0);
        return pli->bDataSize;
    } else {
        return ((*pb0 & item_b0_size) >> item_b0_size_shift);
    }
}

const uint8_t* item::data() const
{
    if (*pb0 == item_b0_long) {
        const item_long_t* pli = reinterpret_cast<const item_long_t*>(pb0);
        return pli->data;
    } else {
        return pb0 + 1;
    }
}

int item::data_int() const
{
    auto v = data_ui();
    return (int)static_cast<int32_t>(v);
}

uint32_t item::data_ui() const
{
    assert(size() <= 4);
    uint32_t ret = 0;
    for (decltype(size()) i = 0; i < size(); i++)
        ret |= ((uint32_t)(data()[i])) << (8 * i);
    return ret;
}

const char* item::name() const
{
    if (*pb0 == item_b0_long)
        return "Long";
    auto tt = type_tag();
    switch (tt.type) {
    case type_t::main:
        switch (tt.main_tag) {
        case main_tag_t::input:               return "Input";
        case main_tag_t::output:              return "Output";
        case main_tag_t::feature:             return "Feature";
        case main_tag_t::collection:          return "Collection";
        case main_tag_t::end_collection:      return "End Collection";
        default:                              break;
        }
        break;
    case type_t::global:
        switch (tt.global_tag) {
        case global_tag_t::usage_page:        return "Usage Page";
        case global_tag_t::logical_minimum:   return "Logical Minimum";
        case global_tag_t::logical_maximum:   return "Logical Maximum";
        case global_tag_t::physical_minimum:  return "Physical Minimum";
        case global_tag_t::physical_maximum:  return "Physical Maximum";
        case global_tag_t::unit_exponent:     return "Unit Exponent";
        case global_tag_t::unit:              return "Unit";
        case global_tag_t::report_size:       return "Report Size";
        case global_tag_t::report_id:         return "Report ID";
        case global_tag_t::report_count:      return "Report Count";
        case global_tag_t::push:              return "Push";
        case global_tag_t::pop:               return "Pop";
        default:                              break;
        }
        break;
    case type_t::local:
        switch (tt.local_tag) {
        case local_tag_t::usage:              return "Usage";
        case local_tag_t::usage_minimum:      return "Usage Minimum";
        case local_tag_t::usage_maximum:      return "Usage Maximum";
        case local_tag_t::designator_index:   return "Designator Index";
        case local_tag_t::designator_minimum: return "Designator Minimum";
        case local_tag_t::designator_maximum: return "Designator Maximum";
        case local_tag_t::string_index:       return "String Index";
        case local_tag_t::string_minimum:     return "String Minimum";
        case local_tag_t::string_maximum:     return "String Maximum";
        case local_tag_t::delimiter:          return "Delimiter";
        default:                              break;
        }
        break;
    case type_t::reserved:                  break;
    }
    return "(Reserved)";
}

std::string item::to_string(parser_state*) const
{
    return std::string {name()};
}

item* item::specialize(const item& u)
{
    item* ret = nullptr;

    auto tt = u.type_tag();
    switch (tt.type) {
    case type_t::main:
        switch (tt.main_tag) {
        case main_tag_t::input:
            ret = new input(u);
            break;
        case main_tag_t::output:
            ret = new output(u);
            break;
        case main_tag_t::feature:
            ret = new feature(u);
            break;
        case main_tag_t::collection:
            ret = new collection(u);
            break;
        case main_tag_t::end_collection:
            ret = new end_collection(u);
            break;
        default:
            break;
        }
        break;
    case type_t::global:
        switch (tt.global_tag) {
        case global_tag_t::usage_page:
            ret = new usage_page(u);
            break;
        case global_tag_t::logical_minimum:
            ret = new logical_min(u);
            break;
        case global_tag_t::logical_maximum:
            ret = new logical_max(u);
            break;
        case global_tag_t::physical_minimum:
            ret = new physical_min(u);
            break;
        case global_tag_t::physical_maximum:
            ret = new physical_max(u);
            break;
        case global_tag_t::unit:
            ret = new unit(u);
            break;
        case global_tag_t::report_count:
            ret = new report_count(u);
            break;
        case global_tag_t::report_size:
            ret = new report_size(u);
            break;
        // case global_tag::unit_exponent:
        // case global_tag::unit:
        // case global_tag::report_id:
        case global_tag_t::push:
            ret = new push(u);
            break;
        case global_tag_t::pop:
            ret = new pop(u);
            break;
        default:
            break;
        }
        break;
    case type_t::local:
        switch (tt.local_tag) {
        case local_tag_t::usage:
            ret = new usage(u);
            break;
        case local_tag_t::usage_minimum:
            ret = new usage_min(u);
            break;
        case local_tag_t::usage_maximum:
            ret = new usage_max(u);
            break;
        // case local_tag::designator_index:
        // case local_tag::designator_minimum:
        // case local_tag::designator_maximum:
        // case local_tag::string_index:
        // case local_tag::string_minimum:
        // case local_tag::string_maximum:
        // case local_tag::delimiter:
        default:
            break;
        }
        break;
    case type_t::reserved:
        break;
    }

    if (!ret) {
        // No specialization: just copy it.
        DBG_PRINTF(5, "%s: no specialization\n", u.name());
        ret = new item(u);
    }
    return ret;
}

} // namespace hrd
} // namespace hid
} // namespace usb
