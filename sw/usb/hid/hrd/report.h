/*
 * USB HID Report Descriptor
 *
 * Report: Defines an Input, Output, or Feature report's structure.
 */

#ifndef USB__HID__HRD__REPORT_H
#define USB__HID__HRD__REPORT_H

#include <vector>
#include <string>
#include <functional>
#include "item_state.h"

namespace usb {
namespace hid {
struct usage;
namespace hrd {

struct report
{
    using type_t = item_state::report_type_t;
    using id_t = item_state::report_id_t;

    // This report is defined in the descriptor.
    bool is_defined;

    // The type of this report: Input, Output, or Feature.
    type_t type;

    // The descriptor may use a Report ID tag. When it does, Byte 0 of the
    // report transfer is the Report ID field and must be equal to the value of
    // the Report ID tag.
    id_t id;
    static constexpr id_t id_undefined = item_state::report_id_undefined;

    // How many bits in this report.
    int bit_width;

    // Data fields, defined by data-type Main items in the descriptor, occupy
    // subsequent bytes in the report. Data fields are packed at bit boundaries.
    std::vector<item_state> data;

    report();
    explicit report(type_t type, id_t id);
    virtual ~report() {}

    size_t num_fields() const { return data.size(); }
    bool empty() const { return num_fields() == 0; }

    void add(const item_state&);
    void update_and_add(item_state&);

    int get_byte_width() const { return (bit_width + 7) / 8; }
    bool has_collection(const hid::usage& usage) const;

    std::string to_string() const;
    void dump() const;
    void _dump() const;
};

} // namespace hrd
} // namespace hid
} // namespace usb

#endif
