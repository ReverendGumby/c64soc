/*
 * USB HID Report Descriptor
 *
 * Report set: A collection of reports. Each report is uniquely identified by
 * type and ID.
 */

#ifndef USB__HID__HRD__REPORT_SET_H
#define USB__HID__HRD__REPORT_SET_H

#include <vector>
#include "report.h"

namespace usb {
namespace hid {
struct usage;
namespace hrd {

class report_set
{
public:
    size_t size() const { return v.size(); }
    bool empty() const { return size() == 0; }
    void clear();

    // Find the report by type and ID and return it. If not found, throw
    // 'std::domain_error'.
    report& get(report::type_t type,
                report::id_t id = report::id_undefined);
    // Find the report, or create one if it doesn't exist, and return it.
    report& get_or_create(report::type_t type,
                          report::id_t id = report::id_undefined);
    report& get_or_create(const report&);

    // Test for the presence of a collection with the given usage.
    bool has_collection(const hid::usage& usage) const;

    // Return the subset of reports from the collection with the given usage.
    report_set subset(const hid::usage& usage) const;

    void dump() const;

private:
    std::vector<report> v;
};

} // namespace hrd
} // namespace hid
} // namespace usb

#endif
