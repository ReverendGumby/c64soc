/*
 * USB HID Report Descriptor
 * Item State Table
 */

#include <sstream>
#include "util/debug.h"
#include "../usage.h"
#include "../collection.h"
//#include "item_spec.h"
#include "item_state.h"

namespace usb {
namespace hid {
namespace hrd {

#define DBG_LVL 2
#define DBG_TAG "USB_HRD"

item_state::item_state()
    : depth(0),
      collection(),
      report_id(report_id_undefined),
      report_type(report_type_t::undefined),
      report_count(0),
      report_size(0),
      main_bits(),
      usage_page(0),
      logical_min(0),
      logical_max(0),
      physical_min(0),
      physical_max(0),
      usages{},
      position(0)
{
}

void item_state::add_usage(const hid::usage& u)
{
    usages.add(u);
}

bool item_state::has_collection(const hid::usage& usage) const
{
    for (auto c = collection; c; c = c->base) {
        if (c->usage == usage)
            return true;
    }
    return false;
}

void item_state::reset_locals()
{
    usages = hid::usage_set {};
}

int item_state::logical_to_physical(int v) const
{
    // Default transformation
    if ((physical_min == 0 && physical_max == 0) ||
        (physical_min == logical_min && physical_max == logical_max))
        return v;

    using wint = int64_t;
    wint ld = logical_max - logical_min;
    wint pd = physical_max - physical_min;
    wint lv = ((wint(v) - logical_min) * pd / ld) + physical_min;
    return int(lv);
}

const char* name(item_state::report_type_t e)
{
    switch (e) {
    case item_state::report_type_t::undefined: return "(undefined)";
    case item_state::report_type_t::input: return "Input";
    case item_state::report_type_t::output: return "Output";
    case item_state::report_type_t::feature: return "Feature";
    default: break;
    }
    return "?";    
}

std::string to_string(const item_state::main_bits_t& bits, bool is_input)
{
    std::stringstream ss;
    ss << (bits.constant ? "Constant" : "Data");
    if (!bits.constant) {
        ss << ", " << (bits.variable ? "Variable" : "Array")
           << ", " << (bits.relative ? "Relative" : "Absolute");
        if (!is_input) {
            if (bits.wrap)
                ss << ", Wrap";
            if (bits.nonlinear)
                ss << ", Nonlinear";
            if (bits.no_preferred)
                ss << ", No Preferred";
            if (bits.null_state)
                ss << ", Null State";
            if (bits.is_volatile)
                ss << ", Volatile";
            if (bits.buffered_bytes)
                ss << ", Buffered Bytes";
        }
    }
    return ss.str();
}

} // namespace hrd
} // namespace hid
} // namespace usb
