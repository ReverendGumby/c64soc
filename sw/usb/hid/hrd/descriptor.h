/*
 * USB HID Report Descriptor
 *
 * The Report Descriptor describes the data field(s) in the USB device's
 * report(s). It is a collection (byte array) of items.
 */

#ifndef USB__HID__HRD__DESCRIPTOR_H
#define USB__HID__HRD__DESCRIPTOR_H

#include <stdint.h>
#include "item.h"

namespace usb {
namespace hid {
namespace hrd {

struct descriptor
{
    using size = uint16_t;

    const uint8_t* p;
    size len;

    descriptor(const uint8_t* p, size len)      // pass the descriptor
        : p(p), len(len) {}

    // iterators
    class iterator;
    using const_iterator = iterator;

    iterator begin() const;
    iterator end() const;
};

/***********************************************************************
 * Iterate each item in the Report Descriptor.
   Invoked using 'for (item* i : descriptor)'. */
class descriptor::iterator
    : public std::iterator<std::input_iterator_tag, const item>
{
public:
    iterator() = default;
    iterator(const iterator&) = default;
    iterator(iterator&&) = default;
    iterator& operator=(const iterator&) = default;
    reference operator*() const { return _e; }
    pointer operator->() const { return &_e; }

    iterator& operator++() { increment(); return *this; }
    //iterator operator++(int) { auto ret = *this; increment(); return ret; }

    friend bool operator!=(const iterator& lhs, const iterator& rhs)
    {
        return lhs._p != rhs._p;
    }

private:
    friend struct descriptor;

    explicit iterator(const descriptor& d, const uint8_t* p);
    void increment();

    const descriptor& _d;                       // descriptor being iter'd
    const uint8_t* _p;                          // pointer to current item
    item _e;                                    // current item
};

} // namespace hrd
} // namespace hid
} // namespace usb

#endif
