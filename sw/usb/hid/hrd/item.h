/*
 * USB HID Report Descriptor
 *
 * The Report Item. Each item is of format short or long, and encodes a type,
 * tag (item function), and size.
 */

#ifndef USB__HID__HRD__ITEM_H
#define USB__HID__HRD__ITEM_H

#include <string>
#include <stdint.h>

namespace usb {
namespace hid {
struct parser_state;
namespace hrd {

/* A generic (unspecialized) item. It is contained by the Resource Descriptor,
   i.e., pb0 is the descriptor's pointer plus some offset. */
struct item {
    virtual const char* DBG_CLS() const { return "item"; }

    using size_t = uint16_t;
    using tag_t = uint8_t;

    /* Item types */
    enum class type_t {
        main = 0x0,
        global = 0x01,
        local = 0x02,
        reserved = 0x03,
    };

    /* Item sizes */
    static constexpr size_t sizes[] = { 0, 1, 2, 4 };

    /* Main item tags */
    enum class main_tag_t {
        input = 0x8,                            /* data from control */
        output = 0x9,                           /* data to control */
        feature = 0xb,                          /* non-user I/O */
        collection = 0xa,                       /* grouping of above 3 */
        end_collection = 0xc,
    };

    static inline bool is_data_type_main(main_tag_t t)
    {
        return t == main_tag_t::input ||
            t == main_tag_t::output ||
            t == main_tag_t::feature;
    }

    /* Global item tags */
    enum class global_tag_t {
        usage_page = 0x0,
        logical_minimum = 0x1,
        logical_maximum = 0x2,
        physical_minimum = 0x3,
        physical_maximum = 0x4,
        unit_exponent = 0x5,
        unit = 0x6,
        report_size = 0x7,
        report_id = 0x8,
        report_count = 0x9,
        push = 0xa,
        pop = 0xb,
    };

    /* Local item tags */
    enum class local_tag_t {
        usage = 0x0,
        usage_minimum = 0x1,
        usage_maximum = 0x2,
        designator_index = 0x3,
        designator_minimum = 0x4,
        designator_maximum = 0x5,
        string_index = 0x7,
        string_minimum = 0x8,
        string_maximum = 0x9,
        delimiter = 0xa,
    };

    // Type and tag combined
    struct type_tag_t {
        type_t type;
        union {
            main_tag_t main_tag;
            global_tag_t global_tag;
            local_tag_t local_tag;
        };
    };

    /* Item byte 0 bitfields */
    enum {
        item_b0_size = 0x03,
        item_b0_size_shift = 0,
        item_b0_type = 0x0C,
        item_b0_type_shift = 2,
        item_b0_tag = 0xF0,
        item_b0_tag_shift = 4,
        item_b0_long = 0xFE,                    // denotes long item
    };

    struct item_long_t {
        uint8_t b0;
        uint8_t bDataSize;
        uint8_t bLongItemTag;
        uint8_t data[0];                        // [bDataSize]
    };

    const uint8_t* pb0;                         // pointer to byte 0
    size_t len;                                 // length incl. byte 0

    item() : pb0(nullptr), len(0) {}
    item(const uint8_t* pb0);
    item(const item&) = default;
    item(item&&) = default;
    item& operator=(const item&) = default;
    virtual ~item() {}

    type_t type() const;
    tag_t tag() const;
    type_tag_t type_tag() const;
    size_t size() const;
    const uint8_t* data() const;
    int data_int() const;
    uint32_t data_ui() const;

    const char* name() const;
    virtual std::string to_string(parser_state* hps = nullptr) const;

    // Convert to the specialized item class.
    static item* specialize(const item& u);

    // Apply to the parser state.
    virtual void apply(parser_state&) const {};
};

} // namespace hid
} // namespace hrd
} // namespace usb

#endif
