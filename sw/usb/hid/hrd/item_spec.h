/*
 * USB HID Report Descriptor (item definitions)
 */

#ifndef USB__HID__HRD__ITEM_SPEC_H
#define USB__HID__HRD__ITEM_SPEC_H

#include <string>
#include "item_state.h"
#include "item.h"

namespace usb {
namespace hid {
struct parser_state;
namespace hrd {

/* Item specializations are numerous, and not everyone cares about them. */

//////////////////////////////////////////////////////////////////////
// Item base classes

/* Items whose data fields represent integers. */
struct numeric_item : item
{
    virtual const char* DBG_CLS() const { return "numeric_item"; }

    int value;

    numeric_item(const item& i);
    virtual std::string to_string(parser_state*) const;
};

//////////////////////////////////////////////////////////////////////
// Main items

// Data-type Main item: tags Input, Output, Feature
struct main_data : item
{
    item_state::main_bits_t bits;

    main_data(const item& i);
    virtual std::string to_string(parser_state*) const;
    virtual void apply(parser_state&) const;
};

// Non-data-type Main item
struct main_non_data : item
{
    main_non_data(const item& i) : item(i) {}
};

struct input : main_data
{
    virtual const char* DBG_CLS() const { return "input"; }
    input(const item& i) : main_data(i) {}
    virtual void apply(parser_state&) const;
};

struct output : main_data
{
    virtual const char* DBG_CLS() const { return "output"; }
    output(const item& i) : main_data(i) {}
    virtual void apply(parser_state&) const;
};

struct feature : main_data
{
    virtual const char* DBG_CLS() const { return "feature"; }
    feature(const item& i) : main_data(i) {}
    virtual void apply(parser_state&) const;
};

struct collection : main_non_data
{
    virtual const char* DBG_CLS() const { return "collection"; }
    enum class type_t {
        physical = 0x0,
        application = 0x1,
        logical = 0x2,
        report = 0x3,
        named_array = 0x4,
        usage_switch = 0x5,
        usage_modifier = 0x6
    };
    collection(const item& i) : main_non_data(i) {}
    type_t type() const { return (type_t)*data(); }
    const char* type_name() const;
    virtual std::string to_string(parser_state*) const;
    virtual void apply(parser_state&) const;
};

const char* name(collection::type_t);

struct end_collection : main_non_data
{
    virtual const char* DBG_CLS() const { return "end_collection"; }
    end_collection(const item& i) : main_non_data(i) {}
    virtual void apply(parser_state&) const;
};

//////////////////////////////////////////////////////////////////////
// Global items

struct usage_page : numeric_item
{
    usage_page(const item& i) : numeric_item(i) {}
    virtual std::string to_string(parser_state*) const;
    virtual void apply(parser_state&) const;
};

struct report_count : numeric_item
{
    virtual const char* DBG_CLS() const { return "report_count"; }
    report_count(const item& i) : numeric_item(i) {}
    virtual void apply(parser_state&) const;
};

struct report_size : numeric_item
{
    virtual const char* DBG_CLS() const { return "report_size"; }
    report_size(const item& i) : numeric_item(i) {}
    virtual void apply(parser_state&) const;
};

struct logical_min : numeric_item
{
    virtual const char* DBG_CLS() const { return "logical_min"; }
    logical_min(const item& i) : numeric_item(i) {}
    virtual void apply(parser_state&) const;
};

struct logical_max : numeric_item
{
    virtual const char* DBG_CLS() const { return "logical_max"; }
    logical_max(const item& i) : numeric_item(i) {}
    virtual void apply(parser_state&) const;
};

struct physical_min : numeric_item
{
    virtual const char* DBG_CLS() const { return "physical_min"; }
    physical_min(const item& i) : numeric_item(i) {}
    virtual void apply(parser_state&) const;
};

struct physical_max : numeric_item
{
    virtual const char* DBG_CLS() const { return "physical_max"; }
    physical_max(const item& i) : numeric_item(i) {}
    virtual void apply(parser_state&) const;
};

struct unit : numeric_item
{
    enum class known_unit {
        none = 0x0,
        degrees = 0x14,
    };
    virtual const char* DBG_CLS() const { return "unit"; }
    unit(const item& i) : numeric_item(i) {}
    virtual std::string to_string(parser_state*) const;
};

const char* name(unit::known_unit);

struct push : item
{
    virtual const char* DBG_CLS() const { return "push"; }
    push(const item& i) : item(i) {}
    virtual void apply(parser_state&) const;
};

struct pop : item
{
    virtual const char* DBG_CLS() const { return "pop"; }
    pop(const item& i) : item(i) {}
    virtual void apply(parser_state&) const;
};

//////////////////////////////////////////////////////////////////////
// Local items

struct usage : numeric_item
{
    usage(const item& i) : numeric_item(i) {}
    virtual std::string to_string(parser_state*) const;
    virtual void apply(parser_state&) const;
};

struct usage_min : usage
{
    usage_min(const item& i) : usage(i) {}
    virtual void apply(parser_state&) const;
};

struct usage_max : usage
{
    usage_max(const item& i) : usage(i) {}
    virtual void apply(parser_state&) const;
};

} // namespace hrd
} // namespace hid
} // namespace usb

#endif
