/*
 * USB HID Report Descriptor (item definitions)
 */

#include <sstream>
#include "util/string.h"
#include "util/debug.h"
#include "../parser.h"
#include "../usage.h"
#include "../collection.h"
#include "item_spec.h"

namespace usb {
namespace hid {
namespace hrd {

#define DBG_LVL 2
#define DBG_TAG "USB_HRD"

#define ITEM_PRINTF(fmt, ...) DBG_PRINTF_T(3, fmt, ## __VA_ARGS__ );

//////////////////////////////////////////////////////////////////////
// Item base classes

numeric_item::numeric_item(const item& i)
    : item(i)
{
    value = data_ui();
}

std::string numeric_item::to_string(parser_state*) const
{
    return string_printf("%s (%u)", name(), data_ui());
}

//////////////////////////////////////////////////////////////////////
// Main items

main_data::main_data(const item& i) : item(i)
{
    auto d = data_ui();
    bool b[9];
    for (unsigned i = 0; i < sizeof(b) / sizeof(b[0]); i++)
        b[i] = (d & (1 << i)) != 0;

    bits.constant = b[0];
    bits.variable = b[1];
    bits.relative = b[2];
    bits.wrap = b[3];
    bits.nonlinear = b[4];
    bits.no_preferred = b[5];
    bits.null_state = b[6];
    bits.is_volatile = b[7];
    bits.buffered_bytes = b[8];
}

std::string main_data::to_string(parser_state*) const
{
    bool is_input = (main_tag_t)tag() == main_tag_t::input;
    return string_printf("%s (%s)", name(),
                         hrd::to_string(bits, is_input).c_str());
}

void main_data::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");

    // Update the item state table.
    hps.ist.main_bits = bits;

    // Add the item state table to the appropriate report.
    auto& r = hps.rs.get_or_create(hps.ist.report_type, hps.ist.report_id);
    r.update_and_add(hps.ist);

    // Remove all local items, leaving only global items.
    hps.ist.reset_locals();
}

void input::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hps.ist.report_type = item_state::report_type_t::input;
    main_data::apply(hps);
}

void output::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hps.ist.report_type = item_state::report_type_t::output;
    main_data::apply(hps);
}

void feature::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hps.ist.report_type = item_state::report_type_t::feature;
    main_data::apply(hps);
}

const char* name(collection::type_t e)
{
    switch (e) {
    case collection::type_t::physical:        return "Physical";
    case collection::type_t::application:     return "Application";
    case collection::type_t::logical:         return "Logical";
    case collection::type_t::report:          return "Report";
    case collection::type_t::named_array:     return "Named Array";
    case collection::type_t::usage_switch:    return "Usage Switch";
    case collection::type_t::usage_modifier:  return "Usage Modifier";
    default: break;
    }
    return "Reserved";
}

const char* collection::type_name() const
{
    return hrd::name((type_t)*data());
}

std::string collection::to_string(parser_state*) const
{
    return string_printf("%s (%s)", name(), type_name());
}

void collection::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hid::usage u;
    // Despite HID spec., collection usage is optional.
    if (hps.ist.get_usages().size()) {
        u = hps.ist.get_usages()[0];
    }
    ITEM_PRINTF("usage: %s\n", u.to_string().c_str());
    auto base_c = hps.ist.collection;
    auto* c = new hid::collection{type(), u, base_c};
    hps.ist.collection.reset(c);
    hps.ist.depth ++;

    // Remove all local items, leaving only global items.
    hps.ist.reset_locals();
}

void end_collection::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hps.ist.depth --;
}

//////////////////////////////////////////////////////////////////////
// Global items

std::string usage_page::to_string(parser_state*) const
{
    hid::usage::page_t page = value;
    auto u = hid::usage(page);
    return string_printf("%s (%s)", name(), u.to_page_string().c_str());
}

void usage_page::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hps.ist.usage_page = value;
}

void report_count::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hps.ist.report_count = value;
}

void report_size::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hps.ist.report_size = value;
}

void logical_min::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hps.ist.logical_min = value;
}

void logical_max::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hps.ist.logical_max = value;
}

void physical_min::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hps.ist.physical_min = value;
}

void physical_max::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hps.ist.physical_max = value;
}

std::string unit::to_string(parser_state*) const
{
    auto ku = known_unit(value);
    const char* kun = hrd::name(ku);
    if (kun)
        return string_printf("%s (%s)", name(), kun);
    return string_printf("%s (0x%x)", name(), value);
}

const char* name(unit::known_unit e)
{
    using ku = unit::known_unit;
    switch (e) {
    case ku::none: return "None";
    case ku::degrees: return "Degrees";
    default: break;
    }
    return nullptr;
}

void push::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    // Push the item state table to the stack.
    hps.ists.push_back(hps.ist);
}

void pop::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    // Pop and restore the item state table from the stack.
    hps.ist = hps.ists.back();
    hps.ists.pop_back();
}

//////////////////////////////////////////////////////////////////////
// Local items

std::string usage::to_string(parser_state* hps) const
{
    if (!hps)
        return numeric_item::to_string(hps);

    hid::usage::page_t page = hps->ist.usage_page;
    auto u = hid::usage(page, (hid::usage::value_t)value);
    std::stringstream ss;
    ss << name() << " (";
    if (u.page != page)
        ss << u.to_string();
    else
        ss << u.to_id_string();
    ss << ")";
    return ss.str();
}

void usage::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hid::usage::page_t& ist_page = hps.ist.usage_page;
    auto u = hid::usage(ist_page, (hid::usage::value_t)value);
    if (ist_page != u.page)
        ist_page = u.page;
    hps.ist.usages.add(u);
}

void usage_min::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hid::usage::page_t& ist_page = hps.ist.usage_page;
    auto u = hid::usage(ist_page, (hid::usage::value_t)value);
    if (ist_page != u.page)
        ist_page = u.page;
    hps.ist.usages.set_min(u);
}

void usage_max::apply(parser_state& hps) const
{
    ITEM_PRINTF("\n");
    hid::usage::page_t& ist_page = hps.ist.usage_page;
    auto u = hid::usage(ist_page, (hid::usage::value_t)value);
    if (ist_page != u.page)
        ist_page = u.page;
    hps.ist.usages.set_max(u);
}

} // namespace hrd
} // namespace hid
} // namespace usb
