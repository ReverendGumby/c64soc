/*
 * USB HID Joystick / Gamepad sub-device
 */

#include <array>
#include <algorithm>
#include <sstream>
#include <stdio.h>
#include "util/debug.h"
#include "util/error.h"
#include "usage.h"
#include "field_getter.h"
#include "joy.h"

namespace usb {
namespace hid {

#define DBG_LVL 1
#define DBG_TAG "HID_JOY"

bool joy::axis::probe(field_getter& fg, const usage& u)
{
    f = fg.get_variable(u);

    if (!f.valid()) {
        off = 0;                                // safe defaults
        lim = 10;
        return false;
    }

    // Compute stick center and extents
    const auto& i = *f.item;
    auto min = f.to_physical(i.logical_min);
    auto max = f.to_physical(i.logical_max);
    off = (min + max) / 2;
    lim = off - min;
    if (lim < 0)
        lim = -lim;

    return true;
}

int joy::axis::get()
{
    if (!f.valid())
        return off;
    auto v = f.get();
    return f.to_physical(v);
}

int joy::axis::to_digital(int v)
{
    int t = lim * 0.6;                          // trigger threshold
    if (v >= off + t)
        return 1;
    if (v <= off - t)
        return -1;
    return 0;
}

const usage_set& joy::app_usages()
{
    static const usage_set us {
        usage{usage::gdc_ids::game_pad},
        usage{usage::gdc_ids::joystick},
    };
    return us;
}

joy::joy(device& dev, const hrd::report_set& subreports)
    : sub_device(dev, subreports), last_report{}
{
}

bool joy::probe_hat_switch()
{
    // Based solely on Logitech Gamepad F310

    static const hat_sw_out_t quad[] = {        // log.in   degrees
        { 1, 0, 0, 0 },                         // 0        0
        { 0, 0, 0, 1 },                         // 1        90
        { 0, 1, 0, 0 },                         // 2        180
        { 0, 0, 1, 0 },                         // 3        270
        { 0, 0, 0, 0 },                         // 4        center
    };
    static const hat_sw_out_t oct[] = {         // log.in   degrees
        { 1, 0, 0, 0 },                         // 0        0
        { 1, 0, 0, 1 },                         // 1        45
        { 0, 0, 0, 1 },                         // 2        90
        { 0, 1, 0, 1 },                         // 3        135
        { 0, 1, 0, 0 },                         // 4        180
        { 0, 1, 1, 0 },                         // 5        225
        { 0, 0, 1, 0 },                         // 6        270
        { 1, 0, 1, 0 },                         // 7        315
        { 0, 0, 0, 0 },                         // 8        center
    };

    hat_sw_tt = nullptr;

    field_getter fg{in_report};

    hat_sw = fg.get_variable(usage{usage::gdc_ids::hat_switch});
    if (!hat_sw.valid())
        return false;

    const auto& item = *hat_sw.item;
    switch (item.logical_max) {
    case 3: hat_sw_tt = quad; break;
    case 7: hat_sw_tt = oct;  break;
    default:
        DBG_PRINTF(1, "Unsupported hat switch\n");
        return false;
    }

    usage u {usage::button_ids::b1};
    btn_f3 = fg.get_variable(u++);
    btn_f1 = fg.get_variable(u++);
    btn_f2 = fg.get_variable(u++);
    btn_f4 = fg.get_variable(u++);
    btn_bl = fg.get_variable(u++);              // left button
    btn_br = fg.get_variable(u++);              // right button
    btn_tl = fg.get_variable(u++);              // left trigger
    btn_tr = fg.get_variable(u++);              // right trigger
    btn_select = fg.get_variable(u++);
    btn_start = fg.get_variable(u++);
    btn_al = fg.get_variable(u++);
    btn_ar = fg.get_variable(u++);

    return true;
}

bool joy::probe_gamepad()
{
    field_getter fg{in_report};
    btn_du = fg.get_variable(usage{usage::gdc_ids::dpad_up});
    btn_dd = fg.get_variable(usage{usage::gdc_ids::dpad_down});
    btn_dl = fg.get_variable(usage{usage::gdc_ids::dpad_left});
    btn_dr = fg.get_variable(usage{usage::gdc_ids::dpad_right});

    // Find and assign the first viable button
    btn_f1 = fg.get_variable(usage{usage::button_ids::b1});
    btn_f2 = fg.get_variable(usage{usage::button_ids::b2});
    btn_f3 = fg.get_variable(usage{usage::button_ids::b3});
    btn_f4 = fg.get_variable(usage{usage::button_ids::b4});
    if (!btn_f1.valid())
        btn_f1 = fg.get_variable(usage{usage::game_ids::gamepad_fire});

    // Assign other buttons
    btn_start = fg.get_variable(usage{usage::gdc_ids::start});
    btn_select = fg.get_variable(usage{usage::gdc_ids::select});
    btn_home = fg.get_variable(usage{usage::gdc_ids::system_main_menu});
    btn_bl = fg.get_variable(usage{usage::button_ids::b5});
    btn_br = fg.get_variable(usage{usage::button_ids::b6});

    return btn_du.valid();
}

bool joy::probe_analog()
{
    field_getter fg{in_report};
    bool ret = ax.probe(fg, usage{usage::gdc_ids::x})
        && ay.probe(fg, usage{usage::gdc_ids::y});
    return ret;
}

std::string joy::get_name()
{
    std::stringstream ss;
    ss << "USB " << dev.get_name() << ": " << get_name_type();
    return ss.str();
}

const char* joy::get_name_type()
{
    return "HID joystick";
}

int joy::probe()
{
    DBG_PRINTF(2, "Device appears to be a HID joystick / gamepad\n");

    if (!in_report.empty()) {
        probe_hat_switch() || probe_gamepad();
        has_analog = probe_analog();

        if (!hat_sw.valid() && !btn_du.valid()) {
            DBG_PRINTF(1, "No directional buttons found!\n");
            return -ENODEV;
        }
        if (!btn_f1.valid()) {
            DBG_PRINTF(1, "No fire buttons found!\n");
            return -ENODEV;
        }
    } else {
        DBG_PRINTF(1, "No input report!\n");
        return -ENODEV;
    }

    DBG_PRINTF(1, "HID joystick / gamepad initialized\n");
    return 0;
}

void joy::translate_report_analog(report& r)
{
    int xa = ax.get();
    int ya = ay.get();
    int x = ax.to_digital(xa);
    int y = ay.to_digital(ya);
    DBG_PRINTF(6, "(%6d,%6d) -> (%2d,%2d)\n", xa, ya, x, y);
    r.up |= y < 0;
    r.dn |= y > 0;
    r.lt |= x < 0;
    r.rt |= x > 0;
}

void joy::translate_report_buttons(report& r)
{
    if (hat_sw.valid() && hat_sw_tt) {
        const auto& t = hat_sw_tt[hat_sw.get()];
        r.up |= t.up;
        r.dn |= t.dn;
        r.lt |= t.lt;
        r.rt |= t.rt;
    } else {
        r.up |= btn_du.get();
        r.dn |= btn_dd.get();
        r.lt |= btn_dl.get();
        r.rt |= btn_dr.get();
    }
    r.f1 = btn_f1.get();
    r.f2 = btn_f2.get();
    r.f3 = btn_f3.get();
    r.f4 = btn_f4.get();
    r.sl = btn_select.get();
    r.st = btn_start.get();
    r.bl = btn_bl.get();
    r.br = btn_br.get();
}

void joy::translate_report(void)
{
    using namespace ui;

    report r {};
    r.id = id;

    // Translate analog stick
    if (has_analog)
        translate_report_analog(r);

    // Translate buttons
    translate_report_buttons(r);

    // Send report on change
    if (last_report != r) {
        last_report = r;
        event(r);
    }
}

void joy::event(const report& r)
{
    DBG_PRINTF(5, "id=%d %c %c %c %c %c %c %c %c %c %c\n", id,
               r.up ? 'U' : '_',
               r.dn ? 'D' : '_',
               r.lt ? 'L' : '_',
               r.rt ? 'R' : '_',
               r.f1 ? '1' : '_',
               r.f2 ? '2' : '_',
               r.f3 ? '3' : '_',
               r.f4 ? '4' : '_',
               r.sl ? '<' : '_',
               r.st ? '>' : '_'
    );
    speak(&listener::event, r);
}

void joy::on_removed(void)
{
    DBG_PRINTF(2, "\n");
}

void joy::set_port(int)
{
    // NOP -- for now
}

bool joy::is_idle()
{
    return last_report.is_idle();
}

} // namespace hid
} // namespace usb
