/*
 * USB HID (Human Interface Devices)
 *
 * Field Getter: Finds Item fields within a Report.
 */

#ifndef USB__HID__FIELD_GETTER_H
#define USB__HID__FIELD_GETTER_H

#include <functional>
#include <algorithm>
#include <stdint.h>
#include "buffered_report.h"
#include "hrd/item_state.h"
#include "field.h"

namespace usb {
namespace hid {

struct usage;

class field_getter
{
public:
    using item_t = field::item_t;
    using get_predicate_t = std::function<bool(const item_t&)>;

    field_getter(const buffered_report&);

    bool check_array(const usage& u) { return check(false, u); }
    bool check_variable(const usage& u) { return check(true, u); }

    array_field get_array(const usage&);
    variable_field get_variable(const usage&);

    array_field get_array(const get_predicate_t&);
    variable_field get_variable(const get_predicate_t&);

private:
    const buffered_report& report;

    bool check(bool variable, const usage&);
    const item_t* find_item(bool variable, const usage& u);
    const item_t* find_item(bool variable, const get_predicate_t& p);
    array_field get_array(const item_t*);
};

} // namespace hid
} // namespace usb

#endif
