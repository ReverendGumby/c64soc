/*
 * USB HID Report Descriptor Parser
 */

#ifndef USB__HID__PARSER_H
#define USB__HID__PARSER_H

#include <stdint.h>
#include <vector>
#include "hrd/descriptor.h"
#include "hrd/item_state.h"
#include "hrd/report_set.h"

namespace usb {
namespace hid {

struct parser_state
{
    using item_state_stack = std::vector<hrd::item_state>;

    // Inputs
    const hrd::descriptor& desc;

    // Temporary data
    hrd::item_state ist;                        // the table
    item_state_stack ists;                      // the table stack

    // Outputs
    hrd::report_set rs;

    parser_state() = delete;
    explicit parser_state(const hrd::descriptor&);
};

class parser
{
public:
    hrd::report_set parse(const hrd::descriptor&);

private:
    void process(const hrd::item&, parser_state&);
};

} // namespace hid
} // namespace usb

#endif
