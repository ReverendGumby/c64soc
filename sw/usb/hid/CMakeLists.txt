add_subdirectory(hrd)
target_sources(emu PRIVATE
  buffered_report.cc
  collection.cc
  device.cc
  field.cc
  field_getter.cc
  joy.cc
  kbd.cc
  parser.cc
  usage.cc
  usage_set.cc
  xbox360.cc
  )
