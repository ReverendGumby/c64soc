/*
 * USB HID (Human Interface Devices)
 * Field
 */

#include <stdint.h>
#include "util/debug.h"
#include "util/error.h"
#include "usage.h"
#include "field_getter.h"

namespace usb {
namespace hid {

#define DBG_LVL 1
#define DBG_TAG "USB_HIDF"

field::value_t field::bit_width() const
{
    if (!valid())
        return 0;
    return item->report_size;
}

field::value_t field::get(int b0, int bw)
{
    value_t v = report->get_vector(b0, bw);
    // Maybe do sign extension
    if (item->logical_min < 0 || item->logical_max < 0) {
        int s = 1 << (bw - 1);
        if (v & s)
            v -= (2 * s);
    }
    return v;
}

void field::set(int b0, int bw, buffered_report::value_t v)
{
    report->set_vector(b0, bw, v);
    DBG_DO(2, report->dump());
}

array_field::value_t array_field::first_bit(int idx) const
{
    if (!valid())
        return 0;
    auto bw = bit_width();
    return item->get_first_bit() + idx * bw;
}

bool array_field::can_get(int idx) const
{
    if (!valid())
        return false;
    auto bw = bit_width();
    auto b0 = first_bit(idx);
    return report->vector_bounds_check(b0, bw);
}

array_field::value_t array_field::get(int idx)
{
    if (!valid())
        return 0;
    auto bw = bit_width();
    auto b0 = first_bit(idx);
    return field::get(b0, bw);
}

void array_field::set(int idx, value_t v)
{
    if (!valid())
        return;
    auto bw = bit_width();
    auto b0 = first_bit(idx);
    field::set(b0, bw, v);
}

variable_field::value_t variable_field::first_bit() const
{
    if (!valid())
        return 0;
    return item->get_first_bit() + offset * bit_width();
}

variable_field::value_t variable_field::mask() const
{
    auto b0 = first_bit();
    auto bw = bit_width();
    value_t mask = ((value_t)1 << bw) - 1;
    mask <<= b0;
    return mask;
}

bool variable_field::can_get() const
{
    if (!valid())
        return false;
    auto b0 = first_bit();
    auto bw = bit_width();
    return report->vector_bounds_check(b0, bw);
}

variable_field::value_t variable_field::get()
{
    if (!valid())
        return 0;
    auto b0 = first_bit();
    auto bw = bit_width();
    return field::get(b0, bw);
}

void variable_field::set(variable_field::value_t v)
{
    if (!valid())
        return;
    auto b0 = first_bit();
    auto bw = bit_width();
    field::set(b0, bw, v);
}

} // namespace hid
} // namespace usb
