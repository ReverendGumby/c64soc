/*
 * USB HID (Human Interface Devices)
 * Field Getter
 */

#include <string.h>
#include <stdint.h>
#include "util/debug.h"
#include "util/error.h"
#include "usage.h"
#include "field_getter.h"

namespace usb {
namespace hid {

#define DBG_LVL 1
#define DBG_TAG "USB_HIDFG"

field_getter::field_getter(const buffered_report& r)
    : report(r)
{
}

const field::item_t* field_getter::find_item(bool variable, const usage& u)
{
    auto item = find_item(variable,
        [&u](const item_t& i)
        {
            const auto& s = i.get_usages();
            return s.has_usage(u);
        });
    DBG_PRINTF(2, "usage (%s) %sfound\n", u.to_string().c_str(),
               item ? "" : "not ");
    return item;
}

const field::item_t* field_getter::find_item(bool variable,
                                             const get_predicate_t& p)
{
    const item_t* item = nullptr;
    for (const auto& d : report.data) {
        if (!d.main_bits.constant
            && d.main_bits.variable == variable
            && p(d)) {
            item = &d;
            break;
        }
    }
    if (item)
        DBG_PRINTF(2, "item found @ data #%u\n",
                   (unsigned)(item - &report.data[0]));
    else
        DBG_PRINTF(2, "item not found\n");
    return item;
}

bool field_getter::check(bool variable, const usage& u)
{
    auto* item = find_item(variable, u);
    return item;
}

array_field field_getter::get_array(const usage& u)
{
    return get_array(find_item(false, u));
}

array_field field_getter::get_array(const get_predicate_t& p)
{
    return get_array(find_item(false, p));
}

array_field field_getter::get_array(const item_t* item)
{
    array_field f{};
    f.item = item;
    f.report = &report;
    if (item) {
        f.count = item->report_count;
        DBG_PRINTF(2, "b0 @ %d, count %d\n", f.first_bit(), f.count);
    }
    return f;
}

variable_field field_getter::get_variable(const usage& u)
{
    auto* item = find_item(true, u);
    variable_field f{};
    f.item = item;
    f.report = &report;
    if (item && item->report_count > 1) {
        f.offset = item->get_usage_index(u);
        DBG_PRINTF(2, "b0 @ %d, offset %d\n", f.first_bit(), f.offset);
    }
    return f;
}

variable_field field_getter::get_variable(const get_predicate_t& p)
{
    auto* item = find_item(true, p);
    variable_field f{};
    f.item = item;
    f.report = &report;
    return f;
}

} // namespace hid
} // namespace usb
