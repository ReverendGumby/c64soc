/*
 * USB HID Collection
 */

#include <sstream>
#include "collection.h"

namespace usb {
namespace hid {

collection::collection()
    : type(type_t::physical), usage(), base()
{
}

collection::collection(type_t t, struct usage u, base_t b)
    : type(t), usage(u), base(b)
{
}

const char* name(collection::type_t e)
{
    return hrd::name(e);
}

std::string collection::to_string() const
{
    std::stringstream ss;
    if (base)
        ss << base->to_string() << " : ";
    ss << hrd::name(type) << '(' << usage.to_string() << ')';
    return ss.str();
}

} // namespace hid
} // namespace usb
