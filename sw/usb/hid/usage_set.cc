/*
 * USB HID Usage Set
 */

#include <algorithm>
#include <stdexcept>
#include <assert.h>
#include "usage_set.h"

namespace usb {
namespace hid {

bool usage_set::has_usage(const usage& u) const
{
    switch (type) {
    case type_t::vec:
        return std::find(vec.begin(), vec.end(), u) != vec.end();
    case type_t::minmax:
        return min <= u && u <= max;
    }
    assert(false); // shouldn't get here
}

int usage_set::find_usage(const usage& u) const
{
    switch (type) {
    case type_t::vec:
        {
            const auto& i = std::find(vec.begin(), vec.end(), u);
            if (i != vec.end())
                return i - vec.begin();
        }
        break;
    case type_t::minmax:
        if (min <= u && u <= max)
            return u.value() - min.value();
        break;
    }
    throw std::domain_error("usage_set::find_usage(): not found");
}

size_t usage_set::size() const
{
    switch (type) {
    case type_t::vec:
        return vec.size();
    case type_t::minmax:
        {
            int s = size_t(max - min + 1);
            assert(s > 0);
            return size_t(s);
        }
    }
    assert(false); // shouldn't get here
}

usage usage_set::operator[](int i) const
{
    if (i < 0 || i >= (int)size())
        throw std::out_of_range("usage_set::operator[](): i out of range");
    switch (type) {
    case type_t::vec:
        return vec[i];
    case type_t::minmax:
        return min + i;
    }
    assert(false); // shouldn't get here
}

void usage_set::add(const usage& u)
{
    assert(type == type_t::vec);
    if (std::find(vec.begin(), vec.end(), u) == vec.end())
        vec.push_back(u);
}

void usage_set::set_min(const usage& u)
{
    assert(type == type_t::minmax || vec.empty());
    type = type_t::minmax;
    min = u;
}

void usage_set::set_max(const usage& u)
{
    assert(type == type_t::minmax || vec.empty());
    type = type_t::minmax;
    max = u;
}

usage_set::iterator usage_set::begin() const
{
    iterator i {*this};
    return i;
}

usage_set::iterator usage_set::end() const
{
    iterator i = begin();
    i._i = i._s;
    return i;
}

usage_set::iterator::iterator(const usage_set& us)
    : _us(us), _i(0), _s(us.size())
{
}

void usage_set::iterator::increment()
{
    if (_i < _s)
        _i ++;
}

} // namespace hid
} // namespace usb
