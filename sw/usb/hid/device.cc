/*
 * USB HID (Human Interface Devices)
 * From HID1_11.pdf
 */

#include <stdexcept>
#include <stdint.h>
#include <assert.h>
#include "util/debug.h"
#include "util/error.h"
#include "util/dump_to_str.h"
#include "util/pollable.h"
#include "../usb.h"
#include "parser.h"
#include "kbd.h"
#include "joy.h"
#include "usage.h"
#include "device.h"

namespace usb {
namespace hid {

/* usb_intf_descriptor.bInterfaceClass */
static constexpr uint8_t uid_class = 0x03;

#define DBG_LVL 1
#define DBG_TAG "USB_HID"

/***********************************************************************
 * HID Requests
 */

/* usb_dev_request.bRequest */
#define HID_UDRR_SET_REPORT     0x09
#define HID_UDRR_SET_PROTOCOL   0x0b

/* usb_descriptor.bDescriptorType */
#define UDT_HID         0x21
#define UDT_REPORT      0x22
#define UDT_PHYSICAL    0x23

/***********************************************************************
 * HID Descriptor
 */

struct hid_desc {
    uint8_t bLength;
    uint8_t bDescriptorType;                    /* UDT_HID */
    uint16_t bcdHID;
    uint8_t bCountryCode;
    uint8_t bNumDescriptors;
    struct {
        uint8_t bDescriptorType;
        uint16_t wDescriptorLength;
    }  __attribute__ ((packed)) desc_opt[];     /* [bNumDescriptors] */
} __attribute__ ((packed));

device::device(const usb::device& dev)
    : usb::device(dev),
      in_buf{new usb::urb_buffer{usb::urb_buffer::read}},
      out_buf{new usb::urb_buffer{usb::urb_buffer::write}},
      in_report(in_buf),
      out_report(out_buf)
{
}

device::~device()
{
}

int device::get_report_descriptor(uint8_t* desc, uint16_t len)
{
    usb_dev_request req;

    DBG_PRINTF(2, "GET_DESCRIPTOR(REPORT)\n");
    req.bmRequestType = UDRRT_INTERFACE_STANDARD_IN;
    req.bRequest = UDRR_GET_DESCRIPTOR;
    req.wValue = UDT_REPORT << 8 | 0; /* Report #0 */
    req.wIndex = intf_desc->bInterfaceNumber;
    req.wLength = len;
    return ep0_read(&req, desc);
}

int device::set_report_protocol(void)
{
    usb_dev_request req;
    int ret;

    DBG_PRINTF(2, "SET_PROTOCOL(REPORT)\n");
    req.bmRequestType = UDRRT_DIRECTION_OUT | UDRRT_TYPE_CLASS | UDRRT_RECIPIENT_INTERFACE;
    req.bRequest = HID_UDRR_SET_PROTOCOL;
    req.wValue = 1;                             /* report protocol */
    req.wIndex = 0;
    req.wLength = 0;
    ret = ep0_write(&req, NULL);
    if (ret == -EPIPE)
        ret = 0;                                /* ignore STALL */
    return ret;
}

int device::set_report(report_type_t type, report_id_t id,
                    void* data, size_t data_len)
{
    DBG_PRINTF(2, "SET_REPORT\n");
    for (auto line : dump_buf_to_str(data, data_len))
        DBG_PRINTF_NOFUNC(3, "%s\n", line.c_str());

    if (use_interrupt_out_ep) {
        out_urb.reinit();
        return out_urb.transfer();
    } else {
        usb_dev_request req;

        req.bmRequestType = UDRRT_DIRECTION_OUT | UDRRT_TYPE_CLASS | UDRRT_RECIPIENT_INTERFACE;
        req.bRequest = HID_UDRR_SET_REPORT;
        req.wValue = ((uint16_t)type << 8) | id;
        req.wIndex = intf_desc->bInterfaceNumber;
        req.wLength = data_len;
        return ep0_write(&req, data);
    }
}

int device::send_output_report()
{
    auto& buf = *out_buf;
    if (buf.used == 0)
        return 0;
    return set_report(hid::report_type_t::output, out_report.id,
                      buf.ptr, buf.used);
}

bool device::match(usb::device& dev)
{
    auto& dd = dev.get_dev_desc();
    auto& id = dev.get_intf_desc();

    // Proper HID class devices identify at the interface level.
    if (dd.bDeviceClass == UDDDC_INDEPENDENT_INTERFACES
        && dd.bDeviceProtocol == UDDDP_INTERFACE
        && id.bInterfaceClass == uid_class)
            return true;

    return false;
}

int device::probe()
{
    EC(find_and_parse_report_descriptor());

    init_reports();

    EC(prepare_device());

    // Create a sub-device for each application found.
    create_subdevices();

    if (!in_report.empty()) {
        pollable_add(this);
    }

    return 0;
}

int device::find_and_parse_report_descriptor()
{
    /* Find the HID descriptor. */
    struct hid_desc *hid_desc;
    EC(find_descriptor(UDT_HID, 0, (const usb_descriptor **)&hid_desc));
    DBG_PRINTF(2, "Device appears to be HID class\n");

    /* Find the report descriptor. */
    uint16_t rd_len = 0;
    for (uint8_t i = 0; i < hid_desc->bNumDescriptors; i++) {
        const auto& o = hid_desc->desc_opt[i];
        if (o.bDescriptorType == UDT_REPORT)
            rd_len = o.wDescriptorLength;
    }
    if (rd_len == 0) {
        DBG_PRINTF(1, "Unable to find report descriptor\n");
        return -ENODEV;
    }

    /* Parse the report descriptor to determine the application(s), e.g.,
       keyboard or joystick. */
    EC(parse_report_descriptor(rd_len));

    return 0;
}

template <class SubDevice>
sub_device* subdev_factory(device& dev)
{
    for (auto u : SubDevice::app_usages()) {
        if (dev.report_descriptor_has_collection(u)) {
            auto subreports = dev.get_report_descriptor_subset(u);
            auto* sd = new SubDevice(dev, subreports);
            return sd;
        }
    }
    return nullptr;
}

void device::create_subdevices()
{
    static const std::function<sub_device*(device&)> factories[] = {
        subdev_factory<kbd>,
        subdev_factory<joy>,
    };

    for (size_t i = 0; i < sizeof(factories)/sizeof(factories[0]); i++) {
        sub_device* sd = factories[i](*this);
        if (sd)
            add_subdevice(sd);
    }
}

int device::add_subdevice(sub_device* psd)
{
    std::unique_ptr<sub_device> sd {psd};
    EC(sd->probe());
    subdev.push_back(std::move(sd));
    return 0;
}

int device::parse_report_descriptor(uint16_t desc_len)
{
    uint8_t buf[desc_len];
    auto len = get_report_descriptor(buf, desc_len);
    EC(len);
    desc_len = len;

    hrd::descriptor desc {buf, desc_len};
    parser hp;
    reports = hp.parse(desc);
    reports.dump();

    return 0;
}

bool device::report_descriptor_has_collection(const usage& u) const
{
    return reports.has_collection(u);
}

hrd::report_set device::get_report_descriptor_subset(const usage& u) const
{
    return reports.subset(u);
}

void device::on_removed()
{
    DBG_PRINTF(2, "\n");

    if (!in_report.empty()) {
        pollable_remove(this);
    }

    usb::device::on_removed();

    // Doing this last ensures that get_sub_device() continues to work.
    while (subdev.size()) {
        auto& sd = subdev.back();
        sd->on_removed();
        subdev.pop_back();
    }
}

void device::poll(void)
{
    in_urb.transfer([this](int len) { this->on_in_report(len); });
}

void device::on_in_report(int len)
{
    in_buf->set_used(len);
    DBG_DO(3, in_report.dump());
    for (auto& sd : subdev) {
        sd->translate_report();
    }
}

void device::init_reports()
{
    DBG_PRINTF(2, "\n");
    try {
        in_report = reports.get(hrd::report::type_t::input);
        in_buf->alloc(in_report.get_byte_width());
    }
    catch (std::domain_error&)
    {
        DBG_PRINTF(1, "No input reports found\n");
    }
    try {
        out_report = reports.get(hrd::report::type_t::output);
        out_buf->alloc(out_report.get_byte_width());
    }
    catch (std::domain_error&)
    {
        DBG_PRINTF(1, "No output reports found\n");
    }
    DBG_PRINTF(2, "Report width (bytes): %u in, %u out\n",
               (unsigned)in_buf->len, (unsigned)out_buf->len);

    if (!in_report.empty()) {
        auto* ep = get_ep(UEDEA(1, IN));
        if (ep != nullptr)
            in_urb.init(ep, in_buf->ptr, in_buf->len);
    }
    if (!out_report.empty()) {
        // Maybe use an optional Interrupt Out endpoint. Else, use (control)
        // endpoint 0.
        auto* ep = get_ep(UEDEA(1, OUT));
        if (ep) {
            use_interrupt_out_ep = true;
            out_urb.init(ep, out_buf->ptr, out_buf->len);
        } else {
            use_interrupt_out_ep = false;
        }
        out_buf->set_used(out_report.get_byte_width());
    }
}

int device::prepare_device()
{
    /* Prepare device for use. */
    EC(set_report_protocol());
    return 0;
}

sub_device::sub_device(device& dev, const hrd::report_set& subreports)
    : dev(dev),
      reports(subreports),
      in_report{dev.in_report},
      out_report{dev.out_report}
{
    in_report = reports.get_or_create(hrd::report::type_t::input);
    out_report = reports.get_or_create(hrd::report::type_t::output);
}

} // namespace hid
} // namespace usb
