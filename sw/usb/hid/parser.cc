/*
 * USB HID Report Descriptor Parser
 */

#include <stdio.h>
#include <stdint.h>
#include "util/debug.h"
#include "util/error.h"
#include "parser.h"

namespace usb {
namespace hid {

#define DBG_LVL 1
#define DBG_TAG "USB_HID_PARSER"

parser_state::parser_state(const hrd::descriptor& desc)
    : desc(desc),
      ist(),
      ists(),
      rs()
{
}

/***********************************************************************/

hrd::report_set parser::parse(const hrd::descriptor& desc)
{
    // Initialize parser state.
    parser_state hps(desc);
    DBG_PRINTF(2, "descriptor length: %lu\n", (unsigned long)hps.desc.len);

    // Process each item in the descriptor.
    for (const hrd::item& i : desc) {
        DBG_PRINTF(12, "%s\n", i.name());
        process(i, hps);
    }

    return hps.rs;
}

void parser::process(const hrd::item& i, parser_state& hps)
{
    // Apply the item to the item state table.
    hrd::item* is = hrd::item::specialize(i);
    DBG_PRINTF(10, "%*s%s\n", hps.ist.depth * 4, "",
               is->to_string(&hps).c_str());
    is->apply(hps);
    delete is;
}

} // namespace hid
} // namespace usb
