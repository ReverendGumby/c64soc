/*
 * USB HID Usage Set
 *
 * A collection of usages, expressed either as (1) a sequential range or (2) an
 * ordered list.
 */

#ifndef USB__HID__USAGE_SET_H
#define USB__HID__USAGE_SET_H

#include <vector>
#include "usage.h"

namespace usb {
namespace hid {

struct usage_set {
    explicit usage_set(std::initializer_list<usage> l) : vec{l} {}

    bool has_usage(const usage&) const;
    int find_usage(const usage&) const;
    size_t size() const;
    usage operator[](int) const;

    void add(const usage&);
    void set_min(const usage&);
    void set_max(const usage&);

    enum class type_t { vec, minmax } type = type_t::vec;

    usage min;
    usage max;
    std::vector<usage> vec;

    // iterators
    class iterator;

    iterator begin() const;
    iterator end() const;
};

class usage_set::iterator
    : public std::iterator<std::input_iterator_tag, usage>
{
    explicit iterator(const usage_set&);
    void increment();

    const usage_set& _us;                       // the set we're iterating
    size_t _i;                                  // current list position
    size_t _s;                                  // size of list

    friend usage_set;

public:
    iterator() = default;
    iterator(const iterator&) = default;
    iterator(iterator&&) = default;
    iterator& operator=(const iterator&) = default;
    usage operator*() { return _us[_i]; }

    iterator& operator++() { increment(); return *this; }

    friend bool operator==(const iterator& lhs, const iterator& rhs)
    {
        return lhs._i == rhs._i;
    }

    friend bool operator!=(const iterator& lhs, const iterator& rhs)
    {
        return !(lhs == rhs);
    }
};

} // namespace hid
} // namespace usb

#endif
