/*
 * USB HID (Human Interface Devices)
 * From HID1_11.pdf
 */

#ifndef USB__HID__DEVICE_H
#define USB__HID__DEVICE_H

#include <vector>
#include <memory>
#include <stdint.h>
#include "../device.h"
#include "../urb.h"
#include "../urb_buffer.h"
#include "util/pollable.h"
#include "hrd/report_set.h"
#include "buffered_report.h"
#include "usage.h"

namespace usb {
namespace hid {

/* HID Get_Report / Set_Report fields */

/* Report Type (wValue, high byte) */
enum class report_type_t {
    input = 0x01,
    output = 0x02,
    feature = 0x03,
};

/* Report ID (wValue, low byte) */
using report_id_t = uint8_t;

class sub_device;

class device : public usb::device,
               private pollable
{
public:
    device(const usb::device& dev);
    virtual ~device();

    // Test if the device is handled by this class.
    static bool match(usb::device& dev);

    virtual int probe();

    template<class SubDevice>
    SubDevice* get_sub_device() const;

    virtual void on_removed();

    bool report_descriptor_has_collection(const usage&) const;
    int get_report_descriptor(uint8_t* desc, uint16_t len);
    int parse_report_descriptor(uint16_t len);

    hrd::report_set get_report_descriptor_subset(const usage&) const;

    int set_report(report_type_t type, report_id_t id,
                   void* data, size_t data_len);
    int send_output_report();

protected:
    friend sub_device;

    virtual int find_and_parse_report_descriptor();
    virtual void init_reports();
    virtual int prepare_device();
    virtual int set_report_protocol(void);
    virtual void create_subdevices();
    int add_subdevice(sub_device*);

    virtual void on_in_report(int len);

    hrd::report_set reports;                    // for whole device
    std::shared_ptr<usb::urb_buffer> in_buf;
    std::shared_ptr<usb::urb_buffer> out_buf;
    buffered_report in_report;
    buffered_report out_report;
    std::vector<std::unique_ptr<sub_device> > subdev;

    urb in_urb;
    bool use_interrupt_out_ep;
    urb out_urb;

private:
    // Interface for pollable
    virtual const char* get_poll_name() const { return "hid::sub_device"; }
    virtual void poll();
};

// An application-specific sub-device
class sub_device
{
public:
    sub_device(device&, const hrd::report_set& subreports);
    virtual ~sub_device() {}

    virtual int probe() = 0;
    virtual void on_removed() = 0;

    virtual void translate_report() = 0;

protected:
    device& dev;
    hrd::report_set reports;                    // subset for this app.
    buffered_report in_report;
    buffered_report out_report;
};

template<class SubDevice>
SubDevice* device::get_sub_device() const
{
    for (auto& sd : subdev)
        if (auto* csd = dynamic_cast<SubDevice*>(&*sd))
            return csd;
    return nullptr;
}

} // namespace hid
} // namespace usb

#endif
