/*
 * USB HID Keyboard sub-device
 */

#ifndef USB__HID__KBD_H
#define USB__HID__KBD_H

#include <stdint.h>
#include <vector>
#include "usage_set.h"
#include "field.h"
#include "device.h"
#include "ui/kbd.h"
#include "util/listener.h"

namespace usb {
namespace hid {

class kbd : public sub_device,
            public ui::kbd
{
public:
    enum class leds {
        num_lock = 0x1,
        caps_lock = 0x2,
        scroll_lock = 0x4,
    };

    static const usage_set& app_usages();

    kbd(device&, const hrd::report_set&);
    virtual ~kbd() {}

    // Interface for sub_device
    virtual int probe();
    virtual void on_removed();
    virtual void translate_report(void);

    // Interface for ui::kbd
    bool is_idle() override;

    void blink();

private:
    void toggle_leds(leds);
    void translate_code(kbcode, const mods&);
    bool check_lock_code(kbcode c);
    int send_output_report();
    void handle_released_keys(const report&);

    report last_report;
    std::vector<keypress> keypresses;

    // Modifier keys
    variable_field key_lctrl;
    variable_field key_lshift;
    variable_field key_lalt;
    variable_field key_lwin;
    variable_field key_rctrl;
    variable_field key_rshift;
    variable_field key_ralt;
    variable_field key_rwin;

    // Key array
    array_field key_array;

    // LEDs
    variable_field leds_num_lock;
    variable_field leds_caps_lock;
    variable_field leds_scroll_lock;
};

constexpr kbd::leds operator|(kbd::leds a, kbd::leds b)
{
    return static_cast<kbd::leds>(static_cast<int>(a) | static_cast<int>(b));
}

constexpr bool operator&(kbd::leds a, kbd::leds b)
{
    return (static_cast<int>(a) & static_cast<int>(b)) != 0;
}

// constexpr operator bool(kbd::leds a)
// {
//     return static_cast<int>(a) != 0;
// }

} // namespace hid
} // namespace usb

#endif
