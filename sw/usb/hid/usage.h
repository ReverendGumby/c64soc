/*
 * USB HID Usages
 */

#ifndef USB__HID__USAGE_H
#define USB__HID__USAGE_H

/*
 * Usages supply an application developer with information about what a control
 * is actually measuring or reporting.
 */

#include <string>

namespace usb {
namespace hid {

struct usage {
    // A usage is represented by a 32-bit value.
    using value_t = uint32_t;
    using page_t = uint16_t;                    // upper 16 bits
    using id_t = uint16_t;                      // lower 16 bits

    static constexpr page_t undefined_page = 0x0;
    static constexpr bool is_vendor_page(page_t p)
    {
        return p >= 0xFF00;
    }

    page_t page = undefined_page;
    id_t id;

    // Known pages
    enum class pages {
        undefined = 0x0,
        generic_desktop_controls = 0x1,
        game_controls = 0x5,
        keyboard_keypad = 0x7,
        leds = 0x8,
        button = 0x9,
    };

    // Known IDs for pages::generic_desktop_controls (0x1)
    enum class gdc_ids : id_t {
        pointer = 0x1,
        mouse = 0x2,
        joystick = 0x4,
        game_pad = 0x5,
        keyboard = 0x6,
        start = 0x3d,
        x = 0x30,
        y = 0x31,
        z = 0x32,
        rx = 0x33,
        ry = 0x34,
        rz = 0x35,
        hat_switch = 0x39,
        select = 0x3e,
        system_main_menu = 0x85,
        dpad_up = 0x90,
        dpad_down = 0x91,
        dpad_left = 0x92,
        dpad_right = 0x93,
    };

    // Known IDs for pages::game_controls (0x5)
    enum class game_ids : id_t {
        gamepad_fire = 0x37,
    };

    // Known IDs for pages::keyboard_keypad (0x7)
    enum class key_ids {
        none = 0,
        error_roll_over = 1,
        application = 101,
        keypad_decimal = 220,
        lctrl = 224,
        lshift = 225,
        lalt = 226,
        lgui = 227,
        rctrl = 228,
        rshift = 229,
        ralt = 230,
        rgui = 231,
    };

    // Known IDs for pages::leds (0x8)
    enum class leds_ids {
        num_lock = 0x1,
        caps_lock = 0x2,
        scroll_lock = 0x3,
        compose = 0x4,
        kana = 0x5,
    };

    // Known IDs for pages::button (0x9)
    enum class button_ids {
        none = 0,
        b1 = 1,
        b2 = 2,
        b3 = 3,
        b4 = 4,
        b5 = 5,
        b6 = 6,
    };

    usage() : id(0) {}
    explicit usage(page_t page, id_t id = 0) : page(page), id(id) {}
    explicit usage(value_t);
    explicit usage(page_t, value_t);            // value may not have page
    explicit usage(pages page, id_t id = 0) : page(page_t(page)), id(id) {}

    // Friendly shortcuts that derive page from id type
    explicit usage(gdc_ids id) : usage((page_t)pages::generic_desktop_controls,
                                       (id_t)id) {}
    explicit usage(game_ids id) : usage((page_t)pages::game_controls,
                                        (id_t)id) {}
    explicit usage(key_ids id) : usage((page_t)pages::keyboard_keypad,
                                       (id_t)id) {}
    explicit usage(leds_ids id) : usage((page_t)pages::leds,
                                        (id_t)id) {}
    explicit usage(button_ids id) : usage((page_t)pages::button,
                                          (id_t)id) {}

    bool operator==(const usage& that) const { return value() == that.value(); }
    bool operator>(const usage& that) const { return value() > that.value(); }
    bool operator>=(const usage& that) const { return value() >= that.value(); }
    bool operator<(const usage& that) const { return value() < that.value(); }
    bool operator<=(const usage& that) const { return value() <= that.value(); }

    int operator-(const usage& that) const
    {
        return (int)that.value() - (int)value();
    }
    usage operator+(int a) const
    {
        return usage{value_t(value() + a)};
    }
    usage& operator++()                         // prefix
    {
        id++;
        return *this;
    }
    usage operator++(int)                       // postfix
    {
        auto ret = *this;
        id++;
        return ret;
    }

    value_t value() const { return ((value_t)page << 16) | id; }
    std::string to_string() const;
    std::string to_page_string() const;
    std::string to_id_string() const;

    static const char* page_name(page_t page);
    static const char* id_name(page_t page, id_t id);
};

} // namespace hid
} // namespace usb

#endif

