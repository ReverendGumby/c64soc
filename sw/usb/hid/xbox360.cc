/*
 * XBox 360 wired (USB) controller
 * Microsoft-specific game pad sub-device
 *
 * Sources:
 * http://tattiebogle.net/index.php/ProjectRoot/Xbox360Controller/UsbInfo
 */

#include <stdint.h>
#include <assert.h>
#include "hrd/report_set.h"
#include "collection.h"
#include "joy.h"
#include "util/debug.h"
#include "util/error.h"
#include "util/dump_to_str.h"
#include "field_getter.h"
#include "xbox360.h"

#define DBG_LVL 1
#define DBG_TAG "USB_XBOX360"

namespace usb {
namespace hid {

/* usb_dev_descriptor.idVendor */
constexpr uint16_t udd_vendor_microsoft = 0x045E;
/* usb_intf_descriptor.bInterfaceSubClass */
constexpr uint8_t uid_subclass = 0x5D;

xbox360::xbox360_joy::xbox360_joy(device& dev, const hrd::report_set& subreports)
    : joy(dev, subreports)
{
}

xbox360::xbox360_joy::~xbox360_joy()
{
}

const char* xbox360::xbox360_joy::get_name_type()
{
    return "XBox 360 controller";
}

void xbox360::xbox360_joy::set_port(int port)
{
    if (auto* pdev = dynamic_cast<xbox360*>(&dev)) {
        if (port == ui::joy::port_none)
            pdev->set_leds(0x0A);
        else
            pdev->set_leds(0x02 + port - ui::joy::port_first);
    }
}

//////////////////////////////////////////////////////////////////////

bool xbox360::match(usb::device& dev)
{
    auto& dd = dev.get_dev_desc();
    auto& id = dev.get_intf_desc();

    // It might be a HID device underneath, but it identifies as something else.
    if (dd.bDeviceClass == UDDDC_VENDOR_SPECIFIC
        && dd.bDeviceProtocol == UDDDP_VENDOR_SPECIFIC
        && dd.idVendor == udd_vendor_microsoft
        && id.bInterfaceClass == UIDIC_VENDOR_SPECIFIC
        && id.bInterfaceSubClass == uid_subclass) {
        DBG_PRINTF(2, "Device appears to be a Microsoft Xbox 360 controller\n");
        return true;
    }

    return false;
}

xbox360::xbox360(const usb::device& dev)
    : device(dev)
{
    DBG_PRINTF(2, "(constructor)\n");
}

static void report_add_padding(struct hrd::report& r, struct hrd::item_state& i,
                               int size)
{
    i.main_bits.constant = true;
    i.main_bits.variable = false;
    i.report_count = 1;
    i.report_size = size;
    r.update_and_add(i);
}

static void set_analog_stick(struct hrd::item_state& i, bool invert)
{
    i.logical_min = -0x8000;
    i.logical_max =  0x7FFF;
    if (invert) {
        i.physical_min = -i.logical_min;
        i.physical_max = -i.logical_max;
    }
}

static void report_add_usage(struct hrd::report& r, struct hrd::item_state& i,
                             int size, const usage& u)
{
    i.main_bits.constant = false;
    i.main_bits.variable = true;
    i.report_count = 1;
    i.report_size = size;
    i.add_usage(u);
    r.update_and_add(i);
}

int xbox360::find_and_parse_report_descriptor()
{
    define_in_report();
    define_out_report();
    reports.dump();

    return 0;
}

void xbox360::define_in_report()
{
    struct hrd::item_state i;
    i.report_type = hrd::report::type_t::input;
    auto& in = reports.get_or_create(i.report_type);
    auto *col = new collection {collection::type_t::application,
                                usage{usage::gdc_ids::game_pad}, nullptr};
    i.collection.reset(col);

    // Input report structure:              Usage
    //   byte     0: report ID (0)
    //   byte     1: report length (20)
    //   bit     16: D-pad up               gdc_ids::dpad_up
    //   bit     17: D-pad down             gdc_ids::dpad_down
    //   bit     18: D-pad left             gdc_ids::dpad_left
    //   bit     19: D-pad right            gdc_ids::dpad_right
    //   bit     20: Start                  gdc_ids::start
    //   bit     21: Back                   gdc_ids::select
    //   bit     22: Left hat button
    //   bit     23: Right hat button
    //   bit     24: left shoulder
    //   bit     25: right shoulder
    //   bit     26: Xbox button            gdc_ids::system_main_menu
    //   bit     27: constant 0
    //   bit     28: A                      button_ids::b1
    //   bit     29: B                      button_ids::b2
    //   bit     30: X                      button_ids::b3
    //   bit     31: Y                      button_ids::b4
    //   byte     4: left trigger
    //   byte     5: right trigger
    //   byte   6-7: left stick X           gdc_ids:x
    //   byte   8-9: left stick Y           gdc_ids:y
    //   byte 10-11: right stick X          gdc_ids:z
    //   byte 12-13: right stick Y          gdc_ids:Rz
    //   byte 14-19: ?

    report_add_padding(in, i, 16 /* bytes 0 - 1 */);
    // D-pad buttons
    report_add_usage(in, i, 1, usage{usage::gdc_ids::dpad_up});
    report_add_usage(in, i, 1, usage{usage::gdc_ids::dpad_down});
    report_add_usage(in, i, 1, usage{usage::gdc_ids::dpad_left});
    report_add_usage(in, i, 1, usage{usage::gdc_ids::dpad_right});
    // Other buttons
    report_add_usage(in, i, 1, usage{usage::gdc_ids::start});
    report_add_usage(in, i, 1, usage{usage::gdc_ids::select});
    report_add_padding(in, i, 23 - 22 + 1);
    report_add_usage(in, i, 1, usage{usage::button_ids::b5});  // l shoulder
    report_add_usage(in, i, 1, usage{usage::button_ids::b6});  // r shoulder
    report_add_usage(in, i, 1, usage{usage::gdc_ids::system_main_menu});
    report_add_padding(in, i, 27 - 27 + 1);
    // Buttons A, B, X, Y
    report_add_usage(in, i, 1, usage{usage::button_ids::b1});  // A
    report_add_usage(in, i, 1, usage{usage::button_ids::b2});  // B
    report_add_usage(in, i, 1, usage{usage::button_ids::b3});  // X
    report_add_usage(in, i, 1, usage{usage::button_ids::b4});  // Y
    // Analog stuff
    report_add_padding(in, i, 2 * 8 /* bytes 4 - 5 */);
    set_analog_stick(i, false);
    report_add_usage(in, i, 16, usage{usage::gdc_ids::x});
    set_analog_stick(i, true);
    report_add_usage(in, i, 16, usage{usage::gdc_ids::y});
    set_analog_stick(i, false);
    report_add_usage(in, i, 16, usage{usage::gdc_ids::z});
    set_analog_stick(i, true);
    report_add_usage(in, i, 16, usage{usage::gdc_ids::rz});
    report_add_padding(in, i, 16 * 8 /* bytes 14 - 19 */);
}

void xbox360::define_out_report()
{
    struct hrd::item_state i;
    i.report_type = hrd::report::type_t::output;
    auto& out = reports.get_or_create(i.report_type);

    // Output report structure:
    //   byte     0: report ID (1)
    //   byte     1: report length (3)
    //   byte     2: LED pattern

    report_add_padding(out, i, 8);
    report_add_padding(out, i, 8);
    report_add_padding(out, i, 8);
}

int xbox360::prepare_device()
{
    EC(set_leds(0x0A));
    return 0;
}

void xbox360::create_subdevices()
{
    /* Create a joystick sub-device. */
    const usage u {usage::gdc_ids::game_pad};
    assert(report_descriptor_has_collection(u));
    const auto &us = joy::app_usages();
    assert(us.has_usage(u));
    auto subreports = get_report_descriptor_subset(u);
    auto* sd = new xbox360_joy(*this, subreports);
    if (sd) {
        add_subdevice(sd);
    }
}

void xbox360::on_in_report(int len)
{
    auto& b = *in_buf;
    auto* bp = b.get_ptr();
    if (len >= 2 && bp[0] == 0 && bp[1] == 20)
        device::on_in_report(len);
}

int xbox360::set_leds(uint8_t pat)
{
    auto& b = *out_buf;
    auto* bp = b.get_ptr();
    int i = 0;
    bp[i++] = 0x01;
    bp[i++] = 0x03;
    bp[i++] = pat;
    b.set_used(i);
    return send_output_report();
}

} // namespace hid
} // namespace usb
