/*
 * USB HID (Human Interface Devices)
 *
 * Field: Selects a field from an Item within a Report. Provides methods to
 * encode and decode the field in a Report buffer.
 */

#ifndef USB__HID__FIELD_H
#define USB__HID__FIELD_H

#include <stdint.h>
#include "hrd/item_state.h"
#include "buffered_report.h"
#include "usage.h"

namespace usb {
namespace hid {

struct field
{
    using item_t = hrd::item_state;
    using value_t = int;

    const buffered_report* report = nullptr;
    const item_t* item = nullptr;
    int offset = 0;

    bool valid() const { return report && item; }
    int bit_width() const;

    value_t to_physical(value_t v) { return item->logical_to_physical(v); }

protected:
    value_t get(int b0, int bw);
    void set(int b0, int bw, buffered_report::value_t);
};

struct array_field : public field
{
    int count = 0;
    int size = 0;

    int first_bit(int idx = 0) const;

    bool can_get(int idx) const;
    value_t get(int idx);
    void set(int idx, value_t);
};

struct variable_field : public field
{
    int first_bit() const;
    value_t mask() const;

    bool can_get() const;
    value_t get();
    void set(value_t);
};

} // namespace hid
} // namespace usb

#endif
