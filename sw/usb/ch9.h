/*
 * Standard USB device requests and related items
 * From usb_20.pdf Chapter 9
 */

#ifndef __USB_CH9_H
#define __USB_CH9_H

#include <stdint.h>

namespace usb {

typedef struct
{
    uint8_t bLength;
    uint8_t bDescriptorType;
} __attribute__ ((packed)) usb_descriptor;

/* Standard descriptor types */
#define UDT_DEVICE              1
#define UDT_CONFIGURATION       2
#define UDT_STRING              3
#define UDT_INTERFACE           4
#define UDT_ENDPOINT            5
#define UDT_DEVICE_QUALIFIER    6
#define UDT_OTHER_SPEED_CONFIG  7
#define UDT_INTERFACE_POWER     8

typedef struct
{
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint16_t bcdUSB;
    uint8_t bDeviceClass;
    uint8_t bDeviceSubClass;
    uint8_t bDeviceProtocol;
    uint8_t bMaxPacketSize0;
    uint16_t idVendor;
    uint16_t idProduct;
    uint16_t bcdDevice;
    uint8_t iManufacturer;
    uint8_t iProduct;
    uint8_t iSerialNumber;
    uint8_t bNumConfigurations;
} __attribute__ ((packed)) usb_dev_descriptor;

/* usb_dev_descriptor.bDeviceClass */
#define UDDDC_INDEPENDENT_INTERFACES    0x00
#define UDDDC_VENDOR_SPECIFIC           0xFF

/* usb_dev_descriptor.bDeviceProtocol */
#define UDDDP_INTERFACE                 0x00
#define UDDDP_VENDOR_SPECIFIC           0xFF

typedef struct
{
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint16_t wTotalLength;
    uint8_t bNumInterfaces;
    uint8_t bConfigurationValue;
    uint8_t iConfiguration;
    uint8_t bmAttributes;
    uint8_t bMaxPower;
} __attribute__ ((packed)) usb_cfg_descriptor;

#define UCD_TOTAL_LENGTH_MAX    256

typedef struct
{
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint8_t bInterfaceNumber;
    uint8_t bAlternateSetting;
    uint8_t bNumEndpoints;
    uint8_t bInterfaceClass;
    uint8_t bInterfaceSubClass;
    uint8_t bInterfaceProtocol;
    uint8_t iInterface;
} __attribute__ ((packed)) usb_intf_descriptor;

/* usb_intf_descriptor.bInterfaceClass */
#define UIDIC_VENDOR_SPECIFIC           0xFF

struct __attribute__ ((packed)) usb_ep_descriptor
{
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint8_t bEndpointAddress;
    uint8_t bmAttributes;
    uint16_t wMaxPacketSize;
    uint8_t bInterval;
};

// wMaxPacketSize has 2 bit fields.
#define UED_MPS_LEN(v)      ((v) & 0x7ff)
#define UED_MPS_MULT(v)     (((v) >> 11) & 3)

/* usb_ep_descriptor.bEndpointAddress */
#define UEDEA_NUMBER        0x0F
#define UEDEA_DIRECTION     (1<<7)
#define UEDEA_DIRECTION_IN      (1<<7)
#define UEDEA_DIRECTION_OUT     (0<<7)

#define UEDEA(num, dir)  (((num) & UEDEA_NUMBER) | UEDEA_DIRECTION_##dir)

#define UEDA_CONTROL    0x0
#define UEDA_ISOCH      0x1
#define UEDA_BULK       0x2
#define UEDA_INTERRUPT  0x3

typedef struct
{
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint8_t bString[254];
} __attribute__ ((packed)) usb_str_descriptor;

typedef struct
{
    uint8_t bmRequestType;
    uint8_t bRequest;
    uint16_t wValue;
    uint16_t wIndex;
    uint16_t wLength;
} __attribute__ ((packed)) usb_dev_request;

/* usb_dev_request.bmRequestType bit definitions */
#define UDRRT_RECIPIENT         (0x1f<<0)
#define UDRRT_RECIPIENT_DEVICE      (0<<0)
#define UDRRT_RECIPIENT_INTERFACE   (1<<0)
#define UDRRT_RECIPIENT_ENDPOINT    (2<<0)
#define UDRRT_RECIPIENT_OTHER       (3<<0)

#define UDRRT_TYPE              (3<<5)
#define UDRRT_TYPE_STANDARD         (0<<5)
#define UDRRT_TYPE_CLASS            (1<<5)
#define UDRRT_TYPE_VENDOR           (2<<5)

#define UDRRT_DIRECTION         (1<<7)
#define UDRRT_DIRECTION_OUT         (0<<7)
#define UDRRT_DIRECTION_IN          (1<<7)

#define UDRRT(rec, type, dir)                   \
    (UDRRT_RECIPIENT_##rec |                    \
     UDRRT_TYPE_##type |                        \
     UDRRT_DIRECTION_##dir)

/* Some useful abbreviations for usb_dev_request.bmRequestType */
#define UDRRT_DEVICE_STANDARD_OUT       UDRRT(DEVICE, STANDARD, OUT)
#define UDRRT_DEVICE_STANDARD_IN        UDRRT(DEVICE, STANDARD, IN)
#define UDRRT_INTERFACE_STANDARD_OUT    UDRRT(INTERFACE, STANDARD, OUT)
#define UDRRT_INTERFACE_STANDARD_IN     UDRRT(INTERFACE, STANDARD, IN)
#define UDRRT_ENDPOINT_STANDARD_OUT     UDRRT(ENDPOINT, STANDARD, OUT)
#define UDRRT_ENDPOINT_STANDARD_IN      UDRRT(ENDPOINT, STANDARD, IN)

/* Standard request codes */
#define UDRR_GET_STATUS         0
#define UDRR_CLEAR_FEATURE      1
#define UDRR_SET_FEATURE        3
#define UDRR_SET_ADDRESS        5
#define UDRR_GET_DESCRIPTOR     6
#define UDRR_SET_DESCRIPTOR     7
#define UDRR_GET_CONFIGURATION  8
#define UDRR_SET_CONFIGURATION  9
#define UDRR_GET_INTERFACE      10
#define UDRR_SET_INTERFACE      11
#define UDRR_SWITCH_FRAME       12

} // namespace usb

#endif
