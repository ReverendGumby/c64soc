#ifndef __HCI_H
#define __HCI_H

#include <stdint.h>
#include "ch9.h"

namespace usb {

struct usb_ep_descriptor;
struct usb_ep;

struct hci_urb
{
    const usb_ep *ep;
    void *buf;
    uint16_t buf_len;
    void *td;
};

struct hci_status
{
    bool CCS;                                   // Current Connect Status
    bool CSC;                                   // Connect Status Change
    bool PE;                                    // Port Enabled
    bool PEC;                                   // Port Enable/Disable Change
    bool OCA;                                   // Over-current Active
    bool OCC;                                   // Over-current Change
    bool PR;                                    // Port Reset
    bool PRC;                                   // Port Reset Change
    bool PP;                                    // Port Power
    bool LS;                                    // Low Speed
    bool HS;                                    // High Speed
};

// Configure host parameters
// Initialize (empty) periodic and asynchronous schedules
// Initialize host controller to HOST mode
// Enable port power
int hci_init(void);

uint16_t hci_get_frame_index(void);

// Enable or disable port power
void hci_set_port_power(bool e);

// Get port status / change indicators
hci_status hci_get_status(void);

// Clear change indicators
void hci_clear_status(hci_status s);

// Issue a reset to the downstream device
void hci_reset_port(void);

// DMA buffers
void *hci_malloc_read_buffer(unsigned size);
void hci_free_read_buffer(void *dma_buf);

// Endpoint access
// Note: ep must not move between create_endpoint() and free_endpoint().
int hci_create_endpoint(usb_ep *ep);
void hci_free_endpoint(usb_ep *ep);
int hci_enable_endpoint(usb_ep *ep);
int hci_disable_endpoint(usb_ep *ep);
int hci_ep_control_read(const usb_ep *ep, const usb_dev_request *req,
                        void *data);
int hci_ep_control_write(const usb_ep *ep, const usb_dev_request *req,
                         const void *data);

// Non-blocking data transfer
int hci_submit_urb(hci_urb* urb);               // queue it
int hci_complete_urb(hci_urb* urb);             // result or -EINPROGRESS
void hci_abort_urb(hci_urb* urb);

} // namespace usb

#endif
