#ifndef __USB_H
#define __USB_H

#include <stdint.h>
#include "ch9.h"

namespace usb {

// Endpoint speed
enum {
    EPS_FS = 0,
    EPS_LS = 1,
    EPS_HS = 2,
    EPS_NC = 3,                                 // reserved / not connected
};

struct usb_ep
{
    bool enabled;
    uint8_t dev_addr;
    uint8_t port_num;                           // hQH.Port_Num
    uint8_t hub_addr;                           // hQH.Hub_Addr
    uint8_t eps;                                // EPS_*
    uint8_t bEndpointAddress;
    uint8_t bEndpointNumber;                    // lower nibble of address
    int out;
    uint8_t bmAttributes;                       // UEDA_*
    uint16_t wMaxPacketSize;
    uint8_t bInterval;
    void *qh;
};

// Allocate a buffer for doing USB reads. Such a buffer must not share a
// cacheline, lest it be stomped on by the CPU.
//   return: the start of the aligned, DMA-safe buffer
void *usb_malloc_read_buffer(size_t size);

void usb_free_read_buffer(void *dma_buf);

// Do a control read transfer
//   ep: control endpoint
//   req: SETUP phase, sent to device
//   data: optional DATA phase, received from device
//   STATUS phase completed before return
int usb_control_read(const usb_ep *ep, const usb_dev_request *req, void *data);

// Do a control write transfer
//   ep: control endpoint
//   req: SETUP phase, sent to device
//   data: optional DATA phase, sent to device
//   STATUS phase completed before return
int usb_control_write(const usb_ep *ep, const usb_dev_request *req,
                      const void *data);

} // namespace usb

#endif
