#pragma once

#include "util/buffer.h"

namespace usb {

class urb_buffer : public buffer
{
public:
    enum dir_t { read, write };

    size_t used = 0;

    void* (*allocator)(size_t);
    void (*deallocator)(void*);

    urb_buffer(dir_t);
    virtual ~urb_buffer();

    void alloc(size_t len);
    void free();

    void set_used(size_t);
};

} // namespace usb
